
// ========================================================
// textureconversion.cpp
// Copyright (c) 2010 Rockstar North.  All Rights Reserved.
// ========================================================

// external
#include "../../../../../base/tools/libs/devil/il.h"
#include "../../../../../base/tools/libs/devil/il_internal.h"
#include "squish/squish.h"

#include "configParser/configParser.h"
#include "configParser/configGameView.h"

// rage includes
#include "file/serialize.h"
#include "system/endian.h"
#include "system/memory.h"
#include "system/nelem.h"
#include "system/timer.h"
#include "system/ipc.h"
#include "parser/manager.h"
#include "paging/rscbuilder.h"
#include "grcore/image.h"
#include "grcore/image_dxt.h"
#include "grcore/texture.h"
#include "grcore/texturedebugflags.h"
#include "string/stringutil.h"

// project includes
#include "../utils.h"
#include "../rage_ps3.h"
#include "../textureconversion/imageutil.h"
#include "../textureconversion/imagegraph.h"
#include "../textureconversion/textureconversion.h"
#include "../textureconversion/textureconversion_parser.h"
#include "../textureconversion/texturefactory.h"
#include "../textureconversion/terrainmapconversion.h"
#include "../textureconversion/imagetiff.h"
#include "../textureconversion/texentry.h"

// libtiff headers
#include "libtiff/tiff.h"
#include "libtiff/tiffio.h"

RAGE_DEFINE_CHANNEL(texconv)
PARAM(texconvlog, "texconvlog");

#define texconvDisplay(fmt, ...) RAGE_DISPLAYF(texconv, fmt, ##__VA_ARGS__)
#define texconvWarning(fmt, ...) RAGE_WARNINGF(texconv, fmt, ##__VA_ARGS__)
#define texconvDebug(  fmt, ...) RAGE_DEBUGF1 (texconv, fmt, ##__VA_ARGS__)
#define texconvError(  fmt, ...) RAGE_ERRORF  (texconv, fmt, ##__VA_ARGS__)
#define texconvLog(    fmt, ...) gTexConvLog.Write     (fmt, ##__VA_ARGS__)

namespace rage {

__COMMENT(static) char CTextureConversionParams::sm_platform = 0;
__COMMENT(static) std::map<rage::u32, CTextureConversionParams*> CTextureConversionParams::ms_CachedTextureSpecifications;

u32 gTextureConversionUniqueHash = 0;

class CTextureConversionLog
{
public:
	CTextureConversionLog() : m_file(NULL) {}

	void Write(const char* fmt, ...)
	{
		char temp[4096] = "";
		va_list args;
		va_start(args, fmt);
		_vsnprintf(temp, sizeof(temp), fmt, args);
		va_end(args);

		texconvDisplay(temp);

		if (PARAM_texconvlog.Get())
		{
			for (int i = 0; i < 10 && m_file == NULL; i++)
			{
				const utimer_t t = sysTimer::GetTicks(); // QueryPerformanceCounter
				const u32 hash = atDataHash((const char*)&t, sizeof(t), (u32)i);
				char path[RAGE_MAX_PATH] = "";
				sprintf(path, "c:/dump/texconvlog_%s_%08X.txt", grcImage::sm_platformString, hash);

				FILE* temp = fopen(path, "r");

				if (temp) // file exists, try another hash .. very unlikely to happen
				{
					fclose(temp);
					continue;
				}

				m_file = fopen(path, "w");
				gTextureConversionUniqueHash = hash;
			}

			if (m_file)
			{
				fprintf(m_file, "%s\n", temp);
				fflush (m_file);
			}
		}
	}

	FILE* m_file;
};

CTextureConversionLog gTexConvLog;

} // namespace rage

// ================================================================================================

namespace rage {

template <typename T> static T* LoadTempObject(const char* path, const char* ext = NULL, T* obj = NULL)
{
	if (path)
	{
		sysMemStartTemp();

		if (ext == NULL)
		{
			ext = T::GetExtension();
		}

		if (strrchr(path, '.') && stricmp(strrchr(path, '.'), ext) == 0)
		{
			ext = NULL;
		}

		if (ASSET.Exists(path, ext))
		{
			bool bCreated = false;

			if (obj == NULL)
			{
				obj = rage_new T;
				bCreated = true;
			}

			if (PARSER.LoadObject(path, ext, *obj))
			{
				// ok
			}
			else if (bCreated)
			{
				delete obj;
				obj = NULL;
			}
		}
		else
		{
			// file not found
		}

		sysMemEndTemp();
	}

	return obj;
}

template <typename T> static T* CloneTempObject(T* obj, const T* pattern)
{
	sysMemStartTemp();

	if (pattern)
	{
		if (obj == NULL)
		{
			obj = rage_new T;
		}

		*obj = *pattern;
	}

	sysMemEndTemp();

	return obj;
}

// ================================================================================================

__COMMENT(static) int CTextureConversionSpecification::sm_stackCount = 0;
__COMMENT(static) char CTextureConversionSpecification::sm_uniqueContainerPath[RAGE_MAX_PATH] = {0};
__COMMENT(static) CTextureConversionSpecification* CTextureConversionSpecification::sm_customLoadImageSpec = NULL;
__COMMENT(static) std::map<rage::u32, CTextureConversionSpecification*> CTextureConversionSpecification::ms_CachedTextureSpecifications;

CTextureConversionSpecification::CTextureConversionSpecification()
{
	m_skipProcessing = false;
}

__COMMENT(static) CTextureConversionSpecification* CTextureConversionSpecification::Load(const char* path, bool bPreload)
{
	sysMemStartTemp(); // apparently it's not enough to wrap explicit allocations ... because atString and atMap (etc.) allocate too

	char objectPath[RAGE_MAX_PATH] = "";
	bool errorOnTcs = false; // flag thats set if we have a TCL

	CTextureConversionParams::ResolvePath(path,objectPath,bPreload);
	CTextureConversionSpecification* obj = LoadTempObject<CTextureConversionSpecification>(objectPath,"tcl");

	if(obj)
	{
		CTextureConversionParams::ResolvePath(obj->m_parent.c_str(),objectPath,bPreload);
		obj->Release();
		obj = NULL;
		errorOnTcs = true;
	}

	parTree* tree = NULL;
	bool foundFile = false;
	obj = rage_new CTextureConversionSpecification;

	if ( ASSET.Exists(objectPath,GetExtension()) )
	{
		tree = PARSER.LoadTree(objectPath,GetExtension());

		if(tree)
		{
			foundFile = true;
			const char* parentPath = NULL;
			parTreeNode* rootNode = tree->GetRoot();
			parTreeNode* parentValNode = rootNode->FindFromXPath("parent");

			if (parentValNode)
				parentPath = parentValNode->GetData();

			if (parentPath)
				CTextureConversionParams* parent = CTextureConversionParams::Load(parentPath,bPreload,(CTextureConversionParams**)&obj);

			obj = LoadTempObject<CTextureConversionSpecification>(objectPath,NULL,obj);
		}
	}
	else
	{
		// fall back on a global default so that we don't end up with loads of uncompressed textures like through the ped export GRS
		char tempPath[RAGE_MAX_PATH];
		char defaultPath[RAGE_MAX_PATH];
		strcpy(tempPath,"$(core_assets)/metadata/textures/templates/globals/Default.tcp");
		CTextureConversionParams::ResolvePath(tempPath,defaultPath,bPreload);

		if (errorOnTcs)
			Errorf("CTextureConversionSpecification::Load: \"%s.%s\" does not exist, falling back on global default %s!",objectPath,GetExtension(),defaultPath);
		else
			Warningf("CTextureConversionSpecification::Load: \"%s.%s\" does not exist, falling back on global default %s!",objectPath,GetExtension(),defaultPath);

		if(ASSET.Exists(defaultPath,CTextureConversionParams::GetExtension()))
		{
			CTextureConversionParams* parent = CTextureConversionParams::Load(defaultPath,bPreload,(CTextureConversionParams**)&obj);
			foundFile = true;
		}
		else
		{
			Errorf("CTextureConversionSpecification::Load: \"%s\" global default does not exist!",defaultPath);
		}

		// keep track of this, if metadata didn't load correctly we want to be able to see this in game
		obj->m_conversionFlags |= TEXTURE_CONVERSION_FLAG_INVALID_METADATA;
	}

	if(!foundFile)
	{
		obj->m_skipProcessing = true;
	}

	for (int i = 0; i < obj->m_imageFiles.size(); i++)
	{
		if (strnicmp(obj->m_imageFiles[i].m_filename.c_str(), "pack:/", strlen("pack:/")) != 0)
		{
			char temp[RAGE_MAX_PATH] = "";
			sprintf(temp, "pack:/%s", obj->m_imageFiles[i].m_filename.c_str());
			obj->m_imageFiles[i].m_filename = temp;
		}

		if (strrchr(obj->m_imageFiles[i].m_filename.c_str(), '.') == NULL)
		{
			obj->m_imageFiles[i].m_filename += ".dds";
		}
	}

	sysMemEndTemp();

	if (obj->m_skipProcessing)
	{
		obj->m_conversionFlags |= TEXTURE_CONVERSION_FLAG_SKIPPED;
	}

	return obj;
}

__COMMENT(static) void CTextureConversionSpecification::ClearCache( )
{
	ms_CachedTextureSpecifications.clear( );
}

bool CTextureConversionSpecification::Save(const char* path) const
{
	char objectPath[RAGE_MAX_PATH] = "";

	strcpy(objectPath, path);

	if (strrchr(objectPath, '.'))
	{
		strrchr(objectPath, '.')[0] = '\0';
	}

	return PARSER.SaveObject(objectPath, GetExtension(), this);
}

void CTextureConversionSpecification::Release()
{
	if (this)
	{
		sysMemStartTemp();
		delete this;
		sysMemEndTemp();
	}
}

__COMMENT(static) void CTextureConversionSpecification::Begin(char platform, const char* uniqueContainerPath, CTextureConversionSpecification* spec /*= NULL*/ )
{
	sm_platform = platform;
	strcpy_s( sm_uniqueContainerPath, RAGE_MAX_PATH, uniqueContainerPath );

	// Only install our callbacks if we're the first stacked call.
	if (0 == sm_stackCount)
	{
		grcImage::SetCustomLoadFunc(CustomLoadImageFunc);
		grcTexture::SetCustomProcessFunc(CustomProcessTextureFunc);
		grmShaderGroup::SetCreateTextureDictionaryCallback( CustomCreateTextureDictionary );
	}
	
	++sm_stackCount;
	sm_customLoadImageSpec = spec;
}

__COMMENT(static) void CTextureConversionSpecification::End()
{
	--sm_stackCount;
	strcpy_s( sm_uniqueContainerPath, RAGE_MAX_PATH, "" );

	// Only clear our callbacks if we're the last stacked call.
	if (sm_stackCount <= 0)
	{
		grcImage::SetCustomLoadFunc(NULL);
		grcTexture::SetCustomProcessFunc(NULL);
		grmShaderGroup::SetCreateTextureDictionaryCallback( NULL );
		sm_stackCount = 0;
	}
	sm_customLoadImageSpec = NULL;
}

__COMMENT(static) grcImage* CTextureConversionSpecification::LoadImageFormat( const char* path )
{
	grcImage* pImage = NULL;
	const char* pExt = ASSET.FindExtensionInPath( path );
	if ( pExt && 0 == stricmp( pExt, ".dds" ) )
		pImage = grcImage::LoadDDS( path ); 
	else if ( pExt && 0 == stricmp( pExt, ".tif" ) )
		pImage = TIFFImage::LoadTIFF( path );
	else if ( !pExt )
	{
		// Check magic header bytes for TIFF or DDS.
		fiSafeStream ddsStrm = ASSET.Open( path, "dds", true, true );
		fiSafeStream tifStrm = ASSET.Open( path, "tif", true, true );
		if ( ddsStrm )
		{
			atString pathname = atString( path );
			pathname += ".dds";
			pImage = grcImage::LoadDDS( pathname );
		}
		else if ( tifStrm )
		{
			atString pathname = atString( path );
			pathname += ".tif";
			pImage = TIFFImage::LoadTIFF( pathname );
		}
	}
	
	if ( !pImage )
	{
		texconvError( "Unrecognised texture format or extension: %s.  Not loaded.", path );
	}

	return ( pImage );
}

__COMMENT(static) bool CTextureConversionSpecification::CustomLoadImageFunc(const char* path, grcImage::ImageList& outputs, void** outParams)
{
	if (1)
	{
		switch (sm_platform)
		{
		case platform::XENON   : grcImage::sm_platformString = "XBOX360"; break;
		case platform::DURANGO : grcImage::sm_platformString = "XBOXONE"; break;
		case platform::PS3     : grcImage::sm_platformString = "PS3"    ; break;
		case platform::ORBIS   : grcImage::sm_platformString = "PS4"    ; break;
		case platform::WIN32PC : grcImage::sm_platformString = "WIN32PC"; break;
		case platform::WIN64PC : grcImage::sm_platformString = "WIN64PC"; break;
		default                : grcImage::sm_platformString = "UNKNOWN"; break;
		}
	}
	bool lookup_result = false;

	// Use the cached spec if it's available, otherwise load it - JWR
	CTextureConversionSpecification* spec;
	if(sm_customLoadImageSpec != NULL)
	{
		spec = sm_customLoadImageSpec;
	}
	else
	{
		spec = CTextureConversionSpecification::Load(path, false);
	}

	if (spec)
	{
		spec->FinaliseConversionParams();

		if (spec->m_exportWarning.length() > 0)
		{
			// this seems to make the process fail, how can i produce a warning that isn't fatal?

			// GRS this isn't fatal to ragebuilder running; Quitf is but Warningf should be. It just affects the
			// return code from the ragebuilder exe GRS 18/3/2011

			Warningf("EXPORT WARNING (%s): %s", path, spec->m_exportWarning.c_str());
		}

		// For the texture links to work; we may have been passed a TCL filename here.
		// We need to convert to a texture file; this is more evidence of this pipeline
		// not being well abstracted, trying to do everything in a single layer.
		sysMemStartTemp( );
		char texturePath[RAGE_MAX_PATH] = "";
		for ( atArray<CTextureConversionResourceTexture>::const_iterator it = spec->m_resourceTextures.begin();
			it != spec->m_resourceTextures.end();
			++it )
		{
			const CTextureConversionResourceTexture& resourceTexture = (*it);
			CTextureConversionParams::ResolvePath(resourceTexture.m_pathname, texturePath, true);
			if ( strcmp(texturePath,"") != 0 )
				break;
		}
		sysMemEndTemp( );
	
		if ( strcmp(texturePath,"") != 0 )
			lookup_result = spec->LookupProcessedImage( texturePath, outputs );
		else
			lookup_result = spec->LookupProcessedImage( path, outputs );

		if ( 0 == outputs.size() )
		{
			Errorf( "Image at '%s' not loaded through processing", path );
		}

		// DHM 2011/05/25; we always pass back the specification for use
		// by the texture factory; for the custom process.
		if (spec->m_swizzle == NULL)
		{
			grcImage::ImageList::iterator pImage = outputs.find(TextureFactory::TEXTURE_ID_MIP_CHAIN_DEFAULT);
			if ( pImage != outputs.end() && pImage->second != NULL )
				spec->m_swizzle = pImage->second->GetConstructorInfo().m_swizzle;
		}

		*outParams = spec;
	}

	return ( lookup_result && ( outputs.size() > 0 ) );
}

__COMMENT(static) bool CTextureConversionSpecification::CustomProcessTextureFunc(grcTexture* texture, const void* params)
{
	if (params)
	{
		const CTextureConversionParams* _this = (const CTextureConversionParams*)params;
		const char* swizzle = _this->m_swizzle;

		if (swizzle[0] != '\0' && (sm_platform == platform::XENON || sm_platform == platform::PS3))
		{
			if ((_this->m_conversionFlags & TEXTURE_CONVERSION_FLAG_NO_PROCESSING) == 0 ||
				(_this->m_conversionFlags & TEXTURE_CONVERSION_FLAG_OPTIMISED_DXT) != 0)
			{
				grcTexture::eTextureSwizzle temp[4]; // r,g,b,a

				for (int i = 0; i < 4; i++)
				{
					switch (swizzle[i])
					{
					case 'R' : temp[i] = grcTexture::TEXTURE_SWIZZLE_R; break;
					case 'G' : temp[i] = grcTexture::TEXTURE_SWIZZLE_G; break;
					case 'B' : temp[i] = grcTexture::TEXTURE_SWIZZLE_B; break;
					case 'A' : temp[i] = grcTexture::TEXTURE_SWIZZLE_A; break;
					case '0' : temp[i] = grcTexture::TEXTURE_SWIZZLE_0; break;
					case '1' : temp[i] = grcTexture::TEXTURE_SWIZZLE_1; break;
					default  : temp[i] = grcTexture::TEXTURE_SWIZZLE_1; break;
					}
				}

				texture->SetTextureSwizzle(temp[0], temp[1], temp[2], temp[3], true);
			}
		}

		// save private flags so they can be seen in the game
		texture->SetConversionFlags(_this->m_conversionFlags);
		texture->SetTemplateType(_this->m_templateType);
		return true;
	}

	return false;
}

__COMMENT(static) pgDictionary<grcTexture>* CTextureConversionSpecification::CustomCreateTextureDictionary( const char** inputs, int inputCount, bool stripPath, grcTextureFactory::TextureCreateParams** params )
{
	const bool bRemoveTintPalettesFromRsc	= (RageBuilder::GetPlatform()==RageBuilder::pltPs3);
	const bool bResizeTintPalettesRsc		= (RageBuilder::GetPlatform()==RageBuilder::pltXenon);

	// The reason we need to override the embedded Create Texture Dictionary
	// call is to correctly determine texture sizes to sort them prior to
	// loading and creating the resource.  Since these texture DDS files may
	// be linked using the texture specification we need to do this in the
	// resource compiler.

	// DHM TODO: pass in the CTextureConversionSpecification for these inputs?!
	// Or get CTextureConversionSpecification to cache them; we seem to reload
	// them quite a bit.
	grcTextureFactory::GetInstance().ClearFreeList();

	// Transform the inputs list to a list of TexEntry items
	sysMemStartTemp();
	TexEntry* te = Alloca( TexEntry, inputCount );
	sysMemSet( te, 0, inputCount * sizeof( TexEntry ) );
	sysMemEndTemp();
	int finalCount = 0;
	for ( int n = 0; n < inputCount; ++n )
	{
		bool foundLinkedTexture = false;
		te[finalCount].name = inputs[n];
		te[finalCount].fileSize = 0;
		te[finalCount].index = n;

		// If the TCL wasn't present, we skip any further processing
		sysMemStartTemp();
		char objectPath[RAGE_MAX_PATH] = "";
		CTextureConversionParams::ResolvePath(inputs[n],objectPath,false);
		bool assetExists = ASSET.Exists(objectPath, "tcl");
		sysMemEndTemp();
		if (!assetExists)
		{
			grcWarningf( "TCL '%s' not found.  This texture may have been parented.", inputs[n] );
		}
		else
		{
			foundLinkedTexture = te[finalCount].LoadSpecification( inputs[n] );
			// If there is no linked texture then we are relying on a packed texture.
			// Once all our TCS files contain a resourceTextures element we could make this an error.
			if ( !foundLinkedTexture )
				grcWarningf( "Linked texture '%s' not found via TCL.", inputs[n] );

			++finalCount;
		}
	}

	if ( !finalCount )
		return (NULL);

	// sort by size
	sysMemStartTemp( );
	std::sort(te, te+finalCount, TexEntrySorter());
	sysMemEndTemp();

	grcTextureFactory& textureFactory = grcTextureFactory::GetInstance();
	pgDictionary<grcTexture>* result = rage_new pgDictionary<grcTexture>(finalCount);
	for (int i=0; i<finalCount; i++)
	{
		char hashName[RAGE_MAX_PATH];
		safecpy(hashName,stripPath? ASSET.FileName(te[i].name) : te[i].name);
		char *ext = strrchr(hashName,'.');
		if (ext && !stricmp(ext,".dds"))
			*ext = '\0';
	
		if (result->Lookup(hashName))
		{
			texconvWarning("Duplicate texture %s not added to the texture dictionary", hashName);
		}
		else 
		{
			bool bResizeTintPal360 = false;
			// Pass the full filename into the create method.
			grcTexture* pTexture = NULL;
			if (te[i].linked)
			{
				//sysMemStartTemp( );
				// This highlights the entire system is not abstracted well enough...
				// Passing its own filename into a member method.  *Sigh*.
				//bool processed = te[i].pSpec->Process( images, te[i].filename );


				// This may have led to unprocessed textures.
				//grcImage* pImage = LoadImageFormat( te[i].filename );
				//sysMemEndTemp( );
	
				// DHM FIX ME
				//pTexture = textureFactory.Create( images[TextureFactory::TEXTURE_ID_MIP_CHAIN_DEFAULT], 
				//	(params?params[te[i].index]:NULL) );

				//const char* pFilename = te[i].filename;
				sysMemStartTemp( );
				TextureList &textureList = *rage_new TextureList;
				sysMemEndTemp( );

				// is it a tint palette?
				bool bRemoveTintPalPs3 = false;
				if(bRemoveTintPalettesFromRsc || bResizeTintPalettesRsc)
				{
					CTextureConversionSpecification *spec = te[i].pSpec.get();
					if(spec && strstr((const char*)spec->m_parent, "TintPalette"))	//"${RS_ASSETS}/metadata/textures/templates/maps_other/TintPalette"
					{
						bRemoveTintPalPs3 = bRemoveTintPalettesFromRsc;
						bResizeTintPal360 = bResizeTintPalettesRsc;
					}
				}

				if(bRemoveTintPalPs3 || bResizeTintPal360)
				{
					sysMemStartTemp();	// put ps3 tint palettes to temp heap
				}

				bool haveBumped = false;
				if (g_sysPlatform == platform::ORBIS)
				{
					if(strstr(te[i].name, "mp_headtargets"))
					{
						g_useVirtualMemory++;
						haveBumped = true;
					}
					else if(strstr(te[i].name, "script_rt_"))
					{
						g_useVirtualMemory++;
						haveBumped = true;
					}
					else if(stricmp(te[i].name, "mp_crewpalette.dds") == 0)
					{
						g_useVirtualMemory++;
						haveBumped = true;
					}
					else if (RagePlatform::GetHeadBlendWriteBuffer())
					{
						g_useVirtualMemory++;
						haveBumped = true;
					}
				}

				CTextureConversionSpecification::Begin( g_sysPlatform, te[i].filename, te[i].pSpec.get() );
				if ( TextureFactory::Create( te[i].specificationFilename, TextureFactory::TEXTURE_ID_MIP_CHAIN_DEFAULT,
					textureList, (params?params[te[i].index]:NULL) ) )
					pTexture = textureList[TextureFactory::TEXTURE_ID_MIP_CHAIN_DEFAULT];				
				CTextureConversionSpecification::End( );

				if(bRemoveTintPalPs3 || bResizeTintPal360)
				{
					sysMemEndTemp();
				}

				sysMemStartTemp( );
				textureList.clear( );
				//images[0]->Release( );
				delete &textureList;
				sysMemEndTemp( );

				if (haveBumped && g_sysPlatform == platform::ORBIS)
				{
					g_useVirtualMemory--;
					if (g_useVirtualMemory < 0)
					{
						Errorf("g_useVirtualMemory underrun");
					}
				}
			}
			else
			{
				bool bRemoveTintPalPs3 = false;

				// do not allow unlinked tint palettes on ps3:
				if(bRemoveTintPalettesFromRsc || bResizeTintPalettesRsc)
				{
					if(te[i].filename && strstr(te[i].filename, "_pal."))
					{
						bRemoveTintPalPs3 = bRemoveTintPalettesFromRsc;
						bResizeTintPal360 = bResizeTintPalettesRsc;
						Warningf("Tint palette '%s' without TCL. To fix re-export this assets with latest tools.", te[i].name);
					}
					
					if(te[i].name && (strlen(te[i].name)>4))
					{
						if(strstr(te[i].name+strlen(te[i].name)-4, "_pal"))
						{
							bRemoveTintPalPs3 = bRemoveTintPalettesFromRsc;
							bResizeTintPal360 = bResizeTintPalettesRsc;
							Warningf("Tint palette '%s' without TCL. To fix re-export this assets with latest tools.", te[i].name);
						}
					}
				}

				if(bRemoveTintPalPs3 || bResizeTintPal360)
				{
					sysMemStartTemp();	// put ps3 tint palettes to temp heap
				}

				pTexture = textureFactory.Create(te[i].name, (params?params[te[i].index]:NULL));

				if(bRemoveTintPalPs3 || bResizeTintPal360)
				{
					sysMemEndTemp();	// put ps3 tint palettes to temp heap
				}
			}

			if(bResizeTintPal360)
			{
				extern grcTexture* ResizeTintPalXenon(grcTexture *pTintTexture, grcTextureFactory::TextureCreateParams *params);	// defined in ragebuilder.cpp
				pTexture = ResizeTintPalXenon(pTexture, params? params[te[i].index] : NULL);
			}

			if (!(result->AddEntry(hashName, pTexture)))
				texconvError("Could not add texture %s to the texture dictionary", hashName);
		}
	}

	return result;
}

static void SetImageConstructorInfo(const char* ASSERT_ONLY(filename), grcImage* pImage, const char swizzle[5], bool bForceContiguousMips, u8 virtualHdMips, u32 overrideAlignment, bool bIsLdMipChain)
{
	if (pImage->GetConstructorInfo().m_debugTag == 0)
	{
		pImage->GetConstructorInfo().m_debugTag = 'conv';
		pImage->GetConstructorInfo().m_forceContiguousMips = bForceContiguousMips;
		pImage->GetConstructorInfo().m_virtualHdMips = virtualHdMips;
		pImage->GetConstructorInfo().m_overrideAlignment = overrideAlignment;
		pImage->GetConstructorInfo().m_bIsLdMipChain = bIsLdMipChain;
	}
#if __ASSERT
	else
	{
		const char* tag = (const char*)&pImage->GetConstructorInfo().m_debugTag;

		if (filename == NULL)
		{
			filename = "NULL";
		}

		Assertf(0, "### ERROR (textureconversion): %s constructor tag is %c%c%c%c", filename, tag[3], tag[2], tag[1], tag[0]);
	}
#endif // __ASSERT

	strcpy(pImage->GetConstructorInfo().m_swizzle, swizzle);
}

bool CTextureConversionSpecification::LookupProcessedImage( const char* filename, grcImage::ImageList& images )
{
	sysMemStartTemp();

	grcImage::sm_customLoad = !m_skipProcessing;
	grcImage* pImage = LoadImageFormat( filename );
	grcImage::sm_customLoad = false;

	if ( pImage )
	{
		// Insert the default mip chain into the image atMap.
		images[TextureFactory::TEXTURE_ID_MIP_CHAIN_DEFAULT] = pImage;
		images[TextureFactory::TEXTURE_ID_CHANNEL_012] = NULL;
		images[TextureFactory::TEXTURE_ID_CHANNEL_345] = NULL;

		if ((m_conversionFlags & TEXTURE_CONVERSION_FLAG_NO_PROCESSING) == 0)
		{
			if (Process(images, filename))
			{
				m_conversionFlags |= TEXTURE_CONVERSION_FLAG_PROCESSED;
			}
			else // Process() failed
			{
				// reload image with sm_customLoad false (so grcImage can skip top mips if necessary)
				pImage->Release();
				pImage = LoadImageFormat( filename );

				if (OptimiseCompressed(pImage))
				{
					m_conversionFlags |= TEXTURE_CONVERSION_FLAG_OPTIMISED_DXT;
					m_swizzle = pImage->GetConstructorInfo().m_swizzle;
				}

				images[TextureFactory::TEXTURE_ID_MIP_CHAIN_DEFAULT] = pImage;

				m_conversionFlags |= TEXTURE_CONVERSION_FLAG_FAILED_PROCESSING;
			}
		}
		else // even if we skip processing, we can still do simple optimisations to the image
		{
			if (OptimiseCompressed(pImage))
			{
				images[TextureFactory::TEXTURE_ID_MIP_CHAIN_DEFAULT] = pImage;

				m_conversionFlags |= TEXTURE_CONVERSION_FLAG_OPTIMISED_DXT;
				m_swizzle = pImage->GetConstructorInfo().m_swizzle;
			}
		}
	}
	else
	{
		texconvError( "Texture %s not found through link or packed.", filename );
	}

	sysMemEndTemp();

	return ( images.size() > 0 );
}

// ================================================================================================

CTextureConversionParams::CTextureConversionParams()
{
	sysMemSet(&m_FIRST_MEMBER, 0, offsetof(CTextureConversionParams, m_LAST_MEMBER) - offsetof(CTextureConversionParams, m_FIRST_MEMBER) + sizeof(m_LAST_MEMBER));
}

__COMMENT(static) void
CTextureConversionParams::SetPlatform( RageBuilder::PLATFORM platform )
{
	// DHM FIX ME: this sucks as we should just use ::rage::g_sysPlatform.
	// We have a platform abstraction in RageCore, Ragebuilder and Texture Conversion! Argh.
	// That is for another day.
	switch ( platform )
	{
	case RageBuilder::pltWin32:
		sm_platform = 'w';
		Assertf( sm_platform == g_sysPlatform, "Platform mismatch [%c].", sm_platform );
		break;
	case RageBuilder::pltWin64:
		sm_platform = 'y';
		Assertf( sm_platform == g_sysPlatform, "Platform mismatch [%c].", sm_platform );
		break;
	case RageBuilder::pltXenon:
		sm_platform = 'x';
		Assertf( sm_platform == g_sysPlatform, "Platform mismatch [%c].", sm_platform );
		break;
	case RageBuilder::pltPs3:
		sm_platform = 'c';
		Assertf( sm_platform == g_sysPlatform, "Platform mismatch [%c].", sm_platform );
		break;
	case RageBuilder::pltDurango:
		sm_platform = 'd';
		Assertf( sm_platform == g_sysPlatform, "Platform mismatch [%c].", sm_platform );
		break;
	case RageBuilder::pltOrbis:
		sm_platform = 'o';
		Assertf( sm_platform == g_sysPlatform, "Platform mismatch [%c].", sm_platform );
		break;
	case RageBuilder::pltIndependent:
	case RageBuilder::pltNeutral:
		texconvError( "Attempting to set platform incorrectly.  Internal error." );
		break;
	}
}

__COMMENT(static) void CTextureConversionParams::ResolvePath(const char* path, char* resolvedPath, bool bPreload)
{
	strcpy(resolvedPath, path);

	if (!bPreload)
	{
		if (strrchr(resolvedPath, '.'))
		{
			strrchr(resolvedPath, '.')[0] = '\0';
		}
	}

	atString newPath(resolvedPath);
	const char* coreAssetPath = RagePlatform::GetCoreAssetsPath( );
	const char* assetPath = RagePlatform::GetAssetsPath( );

	newPath.Replace("${RS_ASSETS}", coreAssetPath); // Compatibility; DHM FIX ME remove in future.
	newPath.Replace("$(core_assets)", coreAssetPath);
	newPath.Replace("$(assets)", assetPath);
	newPath.Trim();
	strcpy(resolvedPath, newPath.c_str());
}

bool CTextureConversionParams::Save(const char* path) const
{
	char objectPath[RAGE_MAX_PATH] = "";

	strcpy(objectPath, path);

	if (strrchr(objectPath, '.'))
	{
		strrchr(objectPath, '.')[0] = '\0';
	}

	return PARSER.SaveObject(objectPath, GetExtension(), this);
}

void CTextureConversionParams::Release()
{
	if (this)
	{
		sysMemStartTemp();
		delete this;
		sysMemEndTemp();
	}
}

static u16 GetTemplateTypeFromTemplatePath(const char* templatePath)
{
	u16 templateType = TEXTURE_TEMPLATE_TYPE_UNKNOWN;

	// NOTE -- the order of these tests is designed to catch cases where multiple keywords are used
	// e.g. "diffusedetail" is tested before "diffuse" and "detail"
	if      (stristr(templatePath, "default"             )) { templateType = TEXTURE_TEMPLATE_TYPE_DEFAULT           ; }
	else if (stristr(templatePath, "terrain"             )) { templateType = TEXTURE_TEMPLATE_TYPE_TERRAIN           ; }
	else if (stristr(templatePath, "clouddensity"        )) { templateType = TEXTURE_TEMPLATE_TYPE_CLOUDDENSITY      ; }
	else if (stristr(templatePath, "cloudnormal"         )) { templateType = TEXTURE_TEMPLATE_TYPE_CLOUDNORMAL       ; }
	else if (stristr(templatePath, "cable"               )) { templateType = TEXTURE_TEMPLATE_TYPE_CABLE             ; }
	else if (stristr(templatePath, "fence"               )) { templateType = TEXTURE_TEMPLATE_TYPE_FENCE             ; }
	else if (stristr(templatePath, "enveff"              )) { templateType = TEXTURE_TEMPLATE_TYPE_ENVEFF            ; }
	else if (stristr(templatePath, "script"              )) { templateType = TEXTURE_TEMPLATE_TYPE_SCRIPT            ; }
	else if (stristr(templatePath, "waterflow"           )) { templateType = TEXTURE_TEMPLATE_TYPE_WATERFLOW         ; }
	else if (stristr(templatePath, "waterfoam"           )) { templateType = TEXTURE_TEMPLATE_TYPE_WATERFOAM         ; }
	else if (stristr(templatePath, "waterfog"            )) { templateType = TEXTURE_TEMPLATE_TYPE_WATERFOG          ; }
	else if (stristr(templatePath, "waterocean"          )) { templateType = TEXTURE_TEMPLATE_TYPE_WATEROCEAN        ; }
	else if (stristr(templatePath, "water"               )) { templateType = TEXTURE_TEMPLATE_TYPE_WATER             ; }
	else if (stristr(templatePath, "foamopacity"         )) { templateType = TEXTURE_TEMPLATE_TYPE_FOAMOPACITY       ; }
	else if (stristr(templatePath, "foam"                )) { templateType = TEXTURE_TEMPLATE_TYPE_FOAM              ; }
	else if (stristr(templatePath, "diffusemipsharpen"   )) { templateType = TEXTURE_TEMPLATE_TYPE_DIFFUSEMIPSHARPEN ; }
	else if (stristr(templatePath, "diffusedetail"       )) { templateType = TEXTURE_TEMPLATE_TYPE_DIFFUSEDETAIL     ; }
	else if (stristr(templatePath, "diffuse_dark"        )) { templateType = TEXTURE_TEMPLATE_TYPE_DIFFUSEDARK       ; }
	else if (stristr(templatePath, "diffuse_alpha_opaque")) { templateType = TEXTURE_TEMPLATE_TYPE_DIFFUSEALPHAOPAQUE; }
	else if (stristr(templatePath, "diffuse"             )) { templateType = TEXTURE_TEMPLATE_TYPE_DIFFUSE           ; }
	else if (stristr(templatePath, "detail"              )) { templateType = TEXTURE_TEMPLATE_TYPE_DETAIL            ; }
	else if (stristr(templatePath, "normal"              )) { templateType = TEXTURE_TEMPLATE_TYPE_NORMAL            ; }
	else if (stristr(templatePath, "specular"            )) { templateType = TEXTURE_TEMPLATE_TYPE_SPECULAR          ; }
	else if (stristr(templatePath, "emissive"            )) { templateType = TEXTURE_TEMPLATE_TYPE_EMISSIVE          ; }
	else if (stristr(templatePath, "tintpalette"         )) { templateType = TEXTURE_TEMPLATE_TYPE_TINTPALETTE       ; }
	else if (stristr(templatePath, "skipprocessing"      )) { templateType = TEXTURE_TEMPLATE_TYPE_SKIPPROCESSING    ; }
	else if (stristr(templatePath, "donotoptimize"       )) { templateType = TEXTURE_TEMPLATE_TYPE_DONOTOPTIMIZE     ; }
	else if (stristr(templatePath, "test"                )) { templateType = TEXTURE_TEMPLATE_TYPE_TEST              ; } // shouldn't be using these in the final assets

	if (stristr(templatePath, "nothalf") || stristr(templatePath, "not_half")) { templateType |= TEXTURE_TEMPLATE_TYPE_FLAG_NOT_HALF ; }
	if (stristr(templatePath, "hdsplit") || stristr(templatePath, "hd_split")) { templateType |= TEXTURE_TEMPLATE_TYPE_FLAG_HD_SPLIT ; }
	if (stristr(templatePath, "/FULL_"))                                       { templateType |= TEXTURE_TEMPLATE_TYPE_FLAG_FULL     ; }
	if (stristr(templatePath, "/maps_half/"))                                  { templateType |= TEXTURE_TEMPLATE_TYPE_FLAG_MAPS_HALF; }

	return templateType | TEXTURE_TEMPLATE_TYPE_VERSION_CURRENT;
}

__COMMENT(static) CTextureConversionParams* CTextureConversionParams::Load(const char* path, bool bPreload, CTextureConversionParams** obj)
{
	char objectPathPlatform[RAGE_MAX_PATH] = "";
	char objectPath[RAGE_MAX_PATH] = "";
	parTree* tree = NULL;

	ResolvePath( path, objectPath, bPreload );

	if ( ASSET.Exists(objectPath,GetExtension() ) )
	{
		tree = PARSER.LoadTree(objectPath,GetExtension());
		if ( tree )
		{
			const char* parentPath = NULL;
			parTreeNode* rootNode = tree->GetRoot();
			parTreeNode* parentValNode = rootNode->FindFromXPath("parent");

			if(parentValNode)
				const char* parentPath = parentValNode->GetData();

			if (parentPath)
				*obj = CTextureConversionParams::Load(parentPath,bPreload,obj);

			*obj = LoadTempObject<CTextureConversionParams>(objectPath,NULL,*obj);

			if (*obj)
			{
				(*obj)->m_templateType = GetTemplateTypeFromTemplatePath(objectPath);
			}

			// After resolving the path; lets see if we have a valid platform-specific template.
			// Which we load to override any existing settings.
			//  i.e. Diffuse.tcp loaded, then Diffuse.win32.tcp loaded.
			switch ( RageBuilder::GetPlatform() )
			{
			case RageBuilder::pltWin32:
				sprintf_s( objectPathPlatform, RAGE_MAX_PATH, "%s.win32", objectPath );
				break;
			case RageBuilder::pltWin64:
				sprintf_s( objectPathPlatform, RAGE_MAX_PATH, "%s.win64", objectPath );
				break;
			case RageBuilder::pltDurango:
				sprintf_s( objectPathPlatform, RAGE_MAX_PATH, "%s.xb1", objectPath );
				break;
			case RageBuilder::pltOrbis:
				sprintf_s( objectPathPlatform, RAGE_MAX_PATH, "%s.ps4", objectPath );
				break;
			case RageBuilder::pltXenon:
				sprintf_s( objectPathPlatform, RAGE_MAX_PATH, "%s.xenon", objectPath );
				break;
			case RageBuilder::pltPs3:
				sprintf_s( objectPathPlatform, RAGE_MAX_PATH, "%s.ps3", objectPath );
				break;
			case RageBuilder::pltIndependent:
			case RageBuilder::pltNeutral:
			default:
				Warningf( "Unexpected platform during template resolution." );
			}

			// Attempt to use the platform template over the default template.
			if ( ASSET.Exists( objectPathPlatform, GetExtension() ) )
			{
				texconvDisplay( "Using platform texture template override: %s.", objectPathPlatform );
				tree = PARSER.LoadTree( objectPathPlatform, GetExtension() );
				if ( tree )
				{
					const char* parentPath = NULL;
					parTreeNode* rootNode = tree->GetRoot();
					parTreeNode* parentValNode = rootNode->FindFromXPath("parent");

					if(parentValNode)
						const char* parentPath = parentValNode->GetData();

					if ( parentPath )
						texconvError( "Unexpected 'parent' in platform-specific template: %s.", objectPathPlatform );

					*obj = LoadTempObject<CTextureConversionParams>( objectPathPlatform, NULL, *obj );
				}
			}
		}
	}
	else
	{
		Errorf("CTextureConversionParams::Load: \"%s.%s\" does not exist!",objectPath,GetExtension());
	}
	return ( *obj );
}

__COMMENT(static) void CTextureConversionParams::ClearCache( )
{
	ms_CachedTextureSpecifications.clear( );
}

// ================================================================================================

enum eTextureFormatChannelGroup
{
	tfcg_RGB     , // R,G,B,1
	tfcg_RGB_A0  , // R,G,B,0
	tfcg_RGBA    , // R,G,B,A
	tfcg_L       , // L,L,L,1
	tfcg_L_A0    , // L,L,L,0
	tfcg_A       , // 0,0,0,A
	tfcg_A_RGB100, // 1,0,0,A
	tfcg_A_RGB111, // 1,1,1,A
	tfcg_LA      , // L,L,L,A
	tfcg_RG      , // R,G,0,1
	tfcg_RG_B1   , // R,G,1,1
	tfcg_R       , // R,0,0,1
};

static const char* eTextureFormatChannelGroup_GetString(eTextureFormatChannelGroup chg)
{
	switch (chg)
	{
	case tfcg_RGB      : return "RGB";
	case tfcg_RGB_A0   : return "RGB(a=0)";
	case tfcg_RGBA     : return "RGBA";
	case tfcg_L        : return "L";
	case tfcg_L_A0     : return "L(a=0)";
	case tfcg_A        : return "A";
	case tfcg_A_RGB100 : return "A(rgb=100)";
	case tfcg_A_RGB111 : return "A(rgb=111)";
	case tfcg_LA       : return "LA";
	case tfcg_RG       : return "RG";
	case tfcg_RG_B1    : return "RG(b=1)";
	case tfcg_R        : return "R";
	}

	return "";
}

class CTextureFormatHelper // constructed from examining all pixels, including mips (ConsiderPixel)
{
public:
	CTextureFormatHelper(const char* filename);

	__forceinline void ConsiderPixel(const Vector4& v);

	const char* m_filename;
	Vector4     m_min;
	Vector4     m_max;
	Vector4     m_avg; // for computing average colour for mip fading (e.g. detail maps)
	float       m_maxRGBSaturation;
	float       m_maxAlphaDiffFrom01; // maximum absolute alpha difference from 0 or 1
	float       m_maxLumaMinusAlpha0; // maximum difference luminance - max(0,alpha)
	int         m_count;
};

CTextureFormatHelper::CTextureFormatHelper(const char* filename)
{
	sysMemSet(this, 0, sizeof(*this));

	m_filename = filename;
}

__forceinline void CTextureFormatHelper::ConsiderPixel(const Vector4& v)
{
	if (m_count == 0)
	{
		m_min = v;
		m_max = v;
	}
	else
	{
		m_min.x = Min<float>(m_min.x, v.x);
		m_min.y = Min<float>(m_min.y, v.y);
		m_min.z = Min<float>(m_min.z, v.z);
		m_min.w = Min<float>(m_min.w, v.w);
		m_max.x = Max<float>(m_max.x, v.x);
		m_max.y = Max<float>(m_max.y, v.y);
		m_max.z = Max<float>(m_max.z, v.z);
		m_max.w = Max<float>(m_max.w, v.w);
	}

	const float minRGB = Min<float>(v.x, v.y, v.z);
	const float maxRGB = Max<float>(v.x, v.y, v.z); // luma

	m_maxRGBSaturation   = Max<float>(m_maxRGBSaturation, maxRGB - minRGB);
	m_maxAlphaDiffFrom01 = Max<float>(m_maxAlphaDiffFrom01, Min<float>(Abs<float>(v.w), Abs<float>(1.0f - v.w)));
	m_maxLumaMinusAlpha0 = Max<float>(m_maxLumaMinusAlpha0, maxRGB - Abs<float>(v.w));

	// note that m_maxLumaMinusAlpha0 really should be computed using max *absolute* RGB minus
	// abs(alpha), not max RGB. this will only make a difference if RGB values can be < 0, which
	// is probably not going to happen.

	m_count++;
}

// ================================================================================================

class CTextureConversion : public CTextureConversionParams
{
public:
	static grcImage::Format GetTextureFormat      (eTextureFormat tf);
	static const char*      GetTextureFormatString(eTextureFormat tf);

	CImage4F*        ResampleSource      (const grcImage* image, int mip, int scaleX, int scaleY, bool bSkipDistanceField) const;
	int              ChooseMipCount      (const CImage4F& image) const;
	void             PreprocessImage     (const CImage4F& image) const;
	bool             BuildTerrainBlendMap(grcImage* pImage, eTextureFormatUsage usage, const char* filename, CTextureFormatHelper& helper);
	int              BuildMipChain       (CImage4F**& mipChain, const grcImage* image, const grcImage* diffuseImage, const char* filename) const; // returns mipCount
	void             BuildMip            (CImage4F**  mipChain, int mipIndex, int mipCount) const;
	void             PostprocessMip      (CImage4F**  mipChain, int mipIndex, int mipCount, CTextureFormatHelper& helper) const;
	grcImage::Format ChooseTextureFormat (const char*& swizzle, const CTextureFormatHelper& helper) const;
	grcImage::Format PreConvert          (CImage4F**  mipChain, int mipCount, const CTextureFormatHelper& helper);
	grcImage*        Convert             (CImage4F**  mipChain, int mipCount, grcImage::Format formatFlags, bool bForceContiguousMips, u8 virtualHdMips, u32 texAlignment, bool bIsLdMipChain);

	// note that this class gets cast directly from CTextureConversionParams, so it must not contain any data
};

__COMMENT(static) grcImage::Format CTextureConversion::GetTextureFormat(eTextureFormat tf)
{
	switch (tf)
	{
	#define DEF_SUPPORTED_IMAGE_EXPORT_COMPRESSED_FORMAT(f) case tf_##f: return grcImage::f;
	FOREACH(DEF_SUPPORTED_IMAGE_EXPORT_COMPRESSED_FORMAT)
	#undef  DEF_SUPPORTED_IMAGE_EXPORT_COMPRESSED_FORMAT

	#define DEF_SUPPORTED_IMAGE_EXPORT_FORMAT(f) case tf_##f: return grcImage::f;
	FOREACH(DEF_SUPPORTED_IMAGE_EXPORT_FORMAT)
	#undef  DEF_SUPPORTED_IMAGE_EXPORT_FORMAT

	default: return grcImage::UNKNOWN;
	}
}

__COMMENT(static) const char* CTextureConversion::GetTextureFormatString(eTextureFormat tf)
{
	// shame we can't just use the string table in textureconversion_parser.h directly?
	const char* parser_rage__CTextureConversionParams__eTextureFormat_Strings[] = {
		"tf_UNKNOWN",
		"tf_DXT1",
		"tf_DXT3",
		"tf_DXT5",
		"tf_CTX1",
		"tf_DXT3A",
		"tf_DXT3A_1111",
		"tf_DXT5A",
		"tf_DXN",
		"tf_BC6",
		"tf_BC7",
		"tf_A8R8G8B8",
		"tf_A8B8G8R8",
		"tf_A8",
		"tf_L8",
		"tf_A8L8",
		"tf_A4R4G4B4",
		"tf_A1R5G5B5",
		"tf_R5G6B5",
		"tf_R3G3B2",
		"tf_A8R3G3B2",
		"tf_A4L4",
		"tf_A2R10G10B10",
		"tf_A2B10G10R10",
		"tf_A16B16G16R16",
		"tf_G16R16",
		"tf_L16",
		"tf_A16B16G16R16F",
		"tf_G16R16F",
		"tf_R16F",
		"tf_A32B32G32R32F",
		"tf_G32R32F",
		"tf_R32F",
		"tf_D15S1",
		"tf_D24S8",
		"tf_D24FS8",
		"tf_P4",
		"tf_P8",
		"tf_A8P8",
		"tf_R8",
		"tf_R16",
		"tf_G8R8",
		NULL
	};
	CompileTimeAssert(NELEM(parser_rage__CTextureConversionParams__eTextureFormat_Strings) == tf_COUNT + 1);

	if (tf >= tf_UNKNOWN && tf < tf_COUNT)
	{
		return parser_rage__CTextureConversionParams__eTextureFormat_Strings[tf] + strlen("tf_");
	}

	return "[NULL]";
}

#if defined(_DEBUG)
	__pragma(optimize("", on)); // PRAGMA-OPTIMIZE-ALLOW
#endif

namespace EDT {

// Euclidean Distance Transform (EDT)
// ----------------------------------------------------------------
// Algorithm based on Ricardo Fabbri's implementation of Maurer EDT
// http://www.lems.brown.edu/~rfabbri/stuff/fabbri-EDT-survey-ACMCSurvFeb2008.pdf
// http://tc18.liris.cnrs.fr/subfields/distance_skeletons/DistanceTransform.pdf
// http://www.lems.brown.edu/vision/people/leymarie/Refs/CompVision/DT/DTpaper.pdf
// http://www.comp.nus.edu.sg/~tants/jfa/i3d06.pdf (jump flooding)
// http://www.comp.nus.edu.sg/~tants/jfa/rong-guodong-phd-thesis.pdf
// http://en.wikipedia.org/wiki/Distance_transform

static __forceinline void MaurerEDT_Horizontal(int* img, int w, int h, int y)
{
	int* row = &img[y*w];

	if (row[0] != 0)
	{
		row[0] = w;
	}

	for (int x = 1; x < w; x++)
	{
		if (row[x] != 0)
		{
			row[x] = row[x - 1] + 1;
		}
	}

	for (int x = w - 2; x >= 0; x--)
	{
		if (row[x] > row[x + 1] + 1)
		{
			row[x] = row[x + 1] + 1;
		}
	}

	for (int x = 0; x < w; x++)
	{
		if (row[x] >= w)
		{
			row[x] = -1; // -1 is "infinity"
		}
		else
		{
			row[x] = row[x]*row[x]; // distance squared
		}
	}
}

static __forceinline bool MaurerEDT_Remove(int du, int dv, int dw, int u, int v, int w)
{
	s64 a = v - u; // these need to be 64-bit ints so the calculation below doesn't overflow
	s64 b = w - v;
	s64 c = w - u;

	return (c*dv - b*du - a*dw) > a*b*c;
}

static void MaurerEDT_Vertical(int* img, int w, int h, int x, int* G, int* H)
{
	int l1 = -1;
	int l2 = 0;

	int* col = img + x;

	for (int y = 0; y < h; y++)
	{
		const int fi = *col; col += w;

		if (fi != -1)
		{
			while (l1 > 0 && MaurerEDT_Remove(G[l1-1], G[l1], fi, H[l1-1], H[l1], y))
			{
				l1--;
			}

			l1++;
			G[l1] = fi;
			H[l1] = y;
		}
	}

	if (l1 == -1)
	{
		return;
	}

	col = img + x;

	for (int y = 0; y < h; y++)
	{
		int tmp0 = H[l2] - y;
		int tmp1 = G[l2] + tmp0*tmp0;

		while (l2 < l1)
		{
			const int tmp2 = H[l2+1] - y;

			if (tmp1 <= G[l2+1] + tmp2*tmp2)
			{
				break;
			}

			l2++;
			tmp0 = H[l2] - y;
			tmp1 = G[l2] + tmp0*tmp0;
		}

		*col = tmp1; col += w;
	}
}

static void MaurerEDT(int* img, int w, int h)
{
	int* G = rage_new int[h];
	int* H = rage_new int[h];

	for (int y = 0; y < h; y++)
	{
		MaurerEDT_Horizontal(img, w, h, y);
	}

	for (int x = 0; x < w; x++)
	{
		MaurerEDT_Vertical(img, w, h, x, G, H);
	}

	delete[] G;
	delete[] H;
}

} // namespace EDT

static __forceinline float exp2f(float x)
{
	const float kLog2 = logf(2.0f);
	return expf(x*kLog2);
}

static __forceinline Vector4 std_VecPow(const Vector4& v, const Vector4& e)
{
	return Vector4(
		powf(v.x, e.x),
		powf(v.y, e.y),
		powf(v.z, e.z),
		powf(v.w, e.w)
	);
}

static __forceinline Vector4 std_VecExp2(const Vector4& v)
{
	return Vector4(
		exp2f(v.x),
		exp2f(v.y),
		exp2f(v.z),
		exp2f(v.w)
	);
}

static void FilteredDownsample(const CImage4F& dst, const CImage4F& src, CTextureConversionParams::eTextureAddress texAddr, const Vector4& texAddrBorderColour, const Vector4& gammaExp, const Vector4& filterAmount, const atArray<float>& filterCoefficients)
{
	const int scaleX = src.mW/dst.mW;
	const int scaleY = src.mH/dst.mH;

	const Vector4 gamma    = std_VecExp2( gammaExp); // 2^gammaExp
	const Vector4 gammaInv = std_VecExp2(-gammaExp);

	int   filterWidth = Max<int>(2, filterCoefficients.GetCount());
	float filter[100] = {0.5f, 0.5f};

	if (filterWidth > 2)
	{
		float filterSum = 0.0f;

		for (int i = 0; i < filterWidth; i++)
		{
			filterSum += filterCoefficients[i];
		}

		for (int i = 0; i < filterWidth; i++)
		{
			filter[i] = filterCoefficients[i]/filterSum;
		}
	}

	const int x0 = scaleX/2 - filterWidth/2;
	const int y0 = scaleY/2 - filterWidth/2;
	const int x1 = x0 + filterWidth;
	const int y1 = y0 + filterWidth;

	for (int y = 0; y < dst.mH; y++)
	{
		for (int x = 0; x < dst.mW; x++)
		{
			Vector4 sumFiltered(0.0f);
			Vector4 sum(0.0f);

			for (int yy = y0; yy < y1; yy++) // general separable filter
			{
				for (int xx = x0; xx < x1; xx++)
				{
					Vector4 c;

					switch (texAddr)
					{
					case CTextureConversionParams::ta_Wrap         : c = src.GetPixelWrapped(x*scaleX + xx, y*scaleY + yy); break;
					case CTextureConversionParams::ta_ClampToEdge  : c = src.GetPixelClamped(x*scaleX + xx, y*scaleY + yy); break;
					case CTextureConversionParams::ta_ClampToBorder: c = src.GetPixelBorder (x*scaleX + xx, y*scaleY + yy, texAddrBorderColour); break;
					}

					sumFiltered += std_VecPow(c, gammaInv)*(filter[xx - x0]*filter[yy - y0]);
				}
			}

			// clamp result, since filter might go outside [0..1] range even though it's normalised
			sumFiltered.x = Clamp<float>(sumFiltered.x, 0.0f, 1.0f);
			sumFiltered.y = Clamp<float>(sumFiltered.y, 0.0f, 1.0f);
			sumFiltered.z = Clamp<float>(sumFiltered.z, 0.0f, 1.0f);
			sumFiltered.w = Clamp<float>(sumFiltered.w, 0.0f, 1.0f);

			for (int yy = 0; yy < scaleY; yy++) // box filter
			{
				for (int xx = 0; xx < scaleX; xx++)
				{
					sum += std_VecPow(src.GetPixel(x*scaleX + xx, y*scaleY + yy), gammaInv);
				}
			}

			sum *= 1.0f/(float)(scaleX*scaleY);

			// blend between general filtered result and box filtered result
			sum.x += filterAmount.x*(sumFiltered.x - sum.x);
			sum.y += filterAmount.y*(sumFiltered.y - sum.y);
			sum.z += filterAmount.z*(sumFiltered.z - sum.z);
			sum.w += filterAmount.w*(sumFiltered.w - sum.w);

			dst.SetPixel(x, y, std_VecPow(sum, gamma));
		}
	}
}

#if defined(_DEBUG)
	__pragma(optimize("", off)); // PRAGMA-OPTIMIZE-ALLOW
#endif

#define MIPMAPS_CANNOT_BE_SMALLER_THAN_4x4 (1) // not really handled very well here or in grcImage .. need to merge with SUPPORT_MIPMAPS_SMALLER_THAN_DXT_BLOCK_SIZE

CImage4F* CTextureConversion::ResampleSource(const grcImage* image, int mip, int scaleX, int scaleY, bool bSkipDistanceField) const
{
	for (int i = 0; i < mip; i++)
	{
		image = image->GetNext();
	}

	const int w0 = image->GetWidth ();
	const int h0 = image->GetHeight();

	scaleX = (int)(((float)(scaleX*m_imageDownsampleScaleX))*m_imageDownsampleScaleRelX);
	scaleY = (int)(((float)(scaleY*m_imageDownsampleScaleY))*m_imageDownsampleScaleRelY);

#if MIPMAPS_CANNOT_BE_SMALLER_THAN_4x4
	while (Min<int>(w0, h0)/scaleX < 4 && scaleX > 1) { scaleX /= 2; }
	while (Min<int>(w0, h0)/scaleY < 4 && scaleY > 1) { scaleY /= 2; }
#endif // MIPMAPS_CANNOT_BE_SMALLER_THAN_4x4

	while (Max<int>(w0, h0)/scaleX > m_imageMaxSizeX || Max<int>(w0, h0)/scaleX > m_imageMaxSize)
	{
#if MIPMAPS_CANNOT_BE_SMALLER_THAN_4x4
		if (Min<int>(w0, h0)/scaleX <= 4)
		{
			break;
		}
#endif // MIPMAPS_CANNOT_BE_SMALLER_THAN_4x4
		scaleX *= 2;
	}

	while (Max<int>(w0, h0)/scaleY > m_imageMaxSizeY || Max<int>(w0, h0)/scaleY > m_imageMaxSize)
	{
#if MIPMAPS_CANNOT_BE_SMALLER_THAN_4x4
		if (Min<int>(w0, h0)/scaleY <= 4)
		{
			break;
		}
#endif // MIPMAPS_CANNOT_BE_SMALLER_THAN_4x4
		scaleY *= 2;
	}

	CImage4F* source = rage_new CImage4F(w0, h0); // resulting image will be w0/scale, h0/scale

	source->ConvertFrom(image);

	if (m_distanceField && m_distanceFieldRange > 0.0f && !bSkipDistanceField) // distance field
	{
		const float distanceScale = 1.0f/m_distanceFieldRange;
		const int n = w0*h0; // area
		Vector4* img = source->GetPixelRowAddr(0);
		int* temp0 = NULL;
		int* temp1 = NULL;

		if (1) // positive range
		{
			temp0 = rage_new int[w0*h0]; for (int i = 0; i < n; i++) { temp0[i] = img[i].x > 0.5f ? 1 : 0; } EDT::MaurerEDT(temp0, w0, h0);
		}

		if (!m_distanceFieldUnsigned) // negative range
		{
			temp1 = rage_new int[w0*h0]; for (int i = 0; i < n; i++) { temp1[i] = img[i].x > 0.5f ? 0 : 1; } EDT::MaurerEDT(temp1, w0, h0);
		}

		if (1) // positive range
		{
			for (int i = 0; i < n; i++)
			{
				const int d2 = temp0[i];
				Vector4& c = img[i];

				c.x = sqrtf((float)d2);
			}
		}

		if (!m_distanceFieldUnsigned) // negative range
		{
			for (int i = 0; i < n; i++)
			{
				const int d2 = temp1[i];
				Vector4& c = img[i];

				c.x -= sqrtf((float)d2);
			}
		}

		for (int i = 0; i < n; i++)
		{
			Vector4& c = img[i];

			c.x = 0.5f + c.x*distanceScale;
			c.y = c.x;
			c.z = c.x;
			c.w = 1.0f;
		}

		if (temp0) { delete[] temp0; }
		if (temp1) { delete[] temp1; }
	}

	if (!m_preprocessAfterDownsample) // do some processing on top-level mipmap
	{
		PreprocessImage(*source);
	}

	if (scaleX > 1 || scaleY > 1)
	{
		const int w = w0/scaleX;
		const int h = h0/scaleY;

		CImage4F* temp = rage_new CImage4F(w, h);

		if (IsZeroAll(RCC_VEC4V(m_downsampleGammaExponent)) && m_downsampleFilterCoefficients.GetCount() == 0)
		{
			const Vector4 scaleAreaInv(1.0f/(float)(scaleX*scaleY));

			for (int y = 0; y < h; y++)
			{
				for (int x = 0; x < w; x++)
				{
					Vector4 sum(0.0f);

					for (int yy = 0; yy < scaleY; yy++)
					{
						for (int xx = 0; xx < scaleX; xx++)
						{
							sum += source->GetPixel(x*scaleX + xx, y*scaleY + yy);
						}
					}

					temp->SetPixel(x, y, sum*scaleAreaInv);
				}
			}
		}
		else
		{
			FilteredDownsample(*temp, *source, m_texAddr, m_texAddrBorderColour, m_downsampleGammaExponent, m_downsampleFilterAmount, m_downsampleFilterCoefficients);
		}

		delete source;
		source = temp;
	}

	return source;
}

int CTextureConversion::ChooseMipCount(const CImage4F& image) const
{
	const int w0 = image.mW;
	const int h0 = image.mH;

	int mipCount = 1;

	while (true)
	{
		const int w = Max<int>(1, w0 >> (mipCount - 1));
		const int h = Max<int>(1, h0 >> (mipCount - 1));

		if (Min<int>(w, h) < m_imageMinMinMipSize ||
			Max<int>(w, h) < m_imageMinMaxMipSize)
		{
			mipCount--;
			break;
		}
		else if (mipCount == m_imageMaxMips)
		{
			break;
		}
		else
		{
			mipCount++;
		}
	}

	return Max<int>(1, mipCount);
}

static __forceinline float SRGBToLinear(float Cgam);

void CTextureConversion::PreprocessImage(const CImage4F& image) const
{
	const int w = image.mW;
	const int h = image.mH;

	if (m_sRGBInputR || m_sRGBInputG || m_sRGBInputB || m_sRGBInputA) // apply sRGB to channels if needed
	{
		for (int x = 0; x < w; x++)
		{
			for (int y = 0; y < h; y++)
			{
				Vector4& c = image.GetPixel(x, y);

				if (m_colourKey.GetColor() != 0 &&
					m_colourKey.GetRed  () == (int)(c.x*255.0f + 0.5f) &&
					m_colourKey.GetGreen() == (int)(c.y*255.0f + 0.5f) &&
					m_colourKey.GetBlue () == (int)(c.z*255.0f + 0.5f))
				{
					continue;
				}

				c.x = m_sRGBInputR ? SRGBToLinear(c.x) : c.x;
				c.y = m_sRGBInputG ? SRGBToLinear(c.y) : c.y;
				c.z = m_sRGBInputB ? SRGBToLinear(c.z) : c.z;
				c.w = m_sRGBInputA ? SRGBToLinear(c.w) : c.w;
			}
		}
	}

	if (m_texFormatUsage == tfu_NormalMap || m_texFormatUsage == tfu_VectorMap)
	{
		for (int y = 0; y < h; y++)
		{
			for (int x = 0; x < w; x++)
			{
				Vector4& c = image.GetPixel(x, y);

				float nx = c.x*2.0f - 1.0f;
				float ny = c.y*2.0f - 1.0f;
				float nz = c.z*2.0f - 1.0f;
				float nw = nx*nx + ny*ny + nz*nz;

				if (nw > 0.0f)
				{
					nw = 1.0f/sqrtf(nw);

					c.x = (nx*nw + 1.0f)/2.0f;
					c.y = (ny*nw + 1.0f)/2.0f;
					c.z = (nz*nw + 1.0f)/2.0f;
				}
			}
		}
	}

	// debuggery
	{
		if (m_debugParams.m_testLinearRamp)
		{
			for (int x = 0; x < w; x++)
			{
				const float xf = (float)x/(float)(w - 1);

				for (int y = 0; y < h; y++)
				{
					Vector4& c = image.GetPixel(x, y);

					c.x = c.y = c.z = xf;
					c.w = 1.0f;
				}
			}
		}

		if (m_debugParams.m_testColourGradients)
		{
			const int size = Min<int>(w, h);

			for (int y = 0; y < size; y++)
			{
				for (int x = 0; x < size; x++)
				{
					Vector4& c = image.GetPixel(x, y);

					c.x = (float)x/(float)(size - 1);
					c.y = (float)y/(float)(size - 1);
					c.z = 0.0f;
					c.w = 1.0f;

					if (x > size/4 && x <= (3*size)/4 &&
						y > size/4 && y <= (3*size)/4)
					{
						c.x = 0;
						c.y = 0;
						c.z = (float)(x - size/4)/(float)(size/2 - 1);

						if (x > (3*size)/8 && x <= (5*size)/8 &&
							y > (3*size)/8 && y <= (5*size)/8)
						{
							c.w = (float)(x - (3*size)/8)/(float)(size/4 - 1);
						}
					}
				}
			}
		}

		if (m_debugParams.m_testColourGraph)
		{
			const Vector4 edgeColour (0.0f, 0.0f, 1.0f, 1.0f);
			const Vector4 fillColour0(0.0f, 0.0f, 0.0f, 0.0f);
			const Vector4 fillColour1(0.5f, 0.5f, 1.0f, 1.0f);
			const Vector4 blurColour0(0.0f, 0.0f, 0.0f, 0.0f);
			const Vector4 blurColour1(0.0f, 0.0f, 0.0f, 1.0f);
			const Vector4 clearColour(1.0f, 1.0f, 1.0f, 1.0f);

			float* data0 = rage_new float[w];
			float* data1 = rage_new float[w];

			for (int x = 0; x < w; x++)
			{
				const float f = (float)x/(float)(w - 1);

				const float a = 0.5f + 0.2f*f*sinf(powf(1.0f - f, 3.0f)*2.0f*PI*5.0f);
				const float b = powf(f, 5.0f);

				data0[x] = a + (b - a)*f*f;
				data1[x] = powf(9.0f*f*powf(1.0f - f, 3.0f), 4.0f);
			}

			GraphOverlay(
				image.GetPixelRowAddr(0),
				w,
				Min<int>(w, h),
				data0,
				0.0f,
				1.0f,
				edgeColour,
				blurColour0,
				blurColour1,
				fillColour0,
				fillColour1,
				clearColour,
				false
			);
			GraphOverlay(
				image.GetPixelRowAddr(0),
				w,
				Min<int>(w, h),
				data1,
				0.0f,
				1.0f,
				Vector4(0.5f, 0.0f, 0.0f, 1.0f),
				Vector4(1.0f, 0.0f, 0.0f, 1.0f),
				Vector4(1.0f, 0.0f, 0.0f, 1.0f),
				Vector4(0.0f),
				Vector4(1.0f, 0.0f, 0.0f, 0.2f),
				Vector4(0.0f),
				true
			);

			delete[] data0;
			delete[] data1;
		}
	}
}

bool CTextureConversion::BuildTerrainBlendMap(grcImage* pImage, eTextureFormatUsage usage, const char* filename, CTextureFormatHelper& helper)
{
	grcImage* diffuseImage = CTextureConversionSpecification::LoadImageFormat( filename );
	CImage4F* channelMap = rage_new CImage4F(diffuseImage->GetWidth(), diffuseImage->GetHeight());

	pImage = grcImage::Create(diffuseImage->GetWidth(), diffuseImage->GetHeight(), diffuseImage->GetDepth(), grcImage::A32B32G32R32F, grcImage::STANDARD, 0, 0);

	TerrainMapTexture2* terrainBlendTexture = rage_new TerrainMapTexture2();
	if ( usage == tfu_TerrainBlendMap0 )
		terrainBlendTexture->SetChannelMode(TerrainMapTexture2::kMode012);
	else if ( usage == tfu_TerrainBlendMap1 )
		terrainBlendTexture->SetChannelMode(TerrainMapTexture2::kMode345);
	else
		return false;

	if ( !terrainBlendTexture->Start() )
	{
		return false;
	}

	float currentVector[4];
	for(int widthIndex = 0; widthIndex < diffuseImage->GetWidth(); ++widthIndex)
	{
		for(int heightIndex = 0; heightIndex < diffuseImage->GetHeight(); ++heightIndex)
		{
			terrainBlendTexture->Convert(diffuseImage->GetPixelVector4(widthIndex, heightIndex), channelMap->GetPixel(widthIndex, heightIndex));

			Vector4& pixel = channelMap->GetPixel(widthIndex, heightIndex);
			currentVector[0] = pixel.x;
			currentVector[1] = pixel.y;
			currentVector[2] = pixel.z;
			currentVector[3] = pixel.w;

			pImage->SetPixelVector(widthIndex, heightIndex, currentVector);
		}
	}

	CImage4F** mipChain = NULL;
	const int  mipCount = BuildMipChain(mipChain, pImage, NULL, NULL);

	for (int i = mipCount - 1; i >= 0; i--) // postprocess mipmaps in reverse order
	{
		PostprocessMip(mipChain, i, mipCount, helper);
	}

	pImage->Release();
	pImage = Convert(mipChain, mipCount, PreConvert(mipChain, mipCount, helper), false, 0, 0, false);

	delete terrainBlendTexture;

	return true;
}

int CTextureConversion::BuildMipChain(CImage4F**& mipChain, const grcImage* image, const grcImage* diffuseImage, const char* filename) const
{
	if (!IsSupportedImageImportFormat(image))
	{
		mipChain = NULL;
		return 0; // [ERROR]
	}

	int scaleX = 1;
	int scaleY = 1;

	if (diffuseImage)
	{
		scaleX = image->GetWidth ()/diffuseImage->GetWidth ();
		scaleY = image->GetHeight()/diffuseImage->GetHeight();
	}

	CImage4F* mip0 = ResampleSource(image, 0, scaleX, scaleY, false);

	if (diffuseImage) // apply diffuse image, if present
	{
		CImage4F* diffuse = ResampleSource(diffuseImage, 0, 1, 1, true);

		if (diffuse->mW == mip0->mW &&
			diffuse->mH == mip0->mH)
		{
			for (int y = 0; y < mip0->mH; y++)
			{
				for (int x = 0; x < mip0->mW; x++)
				{
					Vector4& c = mip0->GetPixel(x, y);
					const float d = c.x;

					c = diffuse->GetPixel(x, y);
					c.w = d;
				}
			}
		}
		else
		{
			// diffuse image is not the right size for some reason
		}

		delete diffuse;
	}

	if (m_preprocessAfterDownsample) // do some processing on top-level mipmap
	{
		PreprocessImage(*mip0);
	}

	const int mipCount = ChooseMipCount(*mip0);

	mipChain = rage_new CImage4F*[mipCount];
	mipChain[0] = mip0;

	for (int i = 1; i < mipCount; i++)
	{
		if (m_respectMips && i <= image->GetExtraMipCount())
		{
			mipChain[i] = ResampleSource(image, i, scaleX, scaleY, false);
		}
		else
		{
			const int mipW = Max<int>(1, mip0->mW >> i);
			const int mipH = Max<int>(1, mip0->mH >> i);

			mipChain[i] = rage_new CImage4F(mipW, mipH);
		}
	}

	for (int i = 1; i < mipCount; i++) // build mipmaps
	{
		if (m_respectMips && i <= image->GetExtraMipCount())
		{
			PreprocessImage(*mipChain[i]);
		}
		else
		{
			BuildMip(mipChain, i, mipCount);
		}
	}

	return mipCount;
}

void CTextureConversion::BuildMip(CImage4F** mipChain, int mipIndex, int mipCount) const // TODO -- template this loop to eliminate branches
{
	const CImage4F& dst = *mipChain[mipIndex];
	const CImage4F& src = *mipChain[mipIndex - 1];

	const int w = dst.mW;
	const int h = dst.mH;

	const int scaleX = src.mW / w;
	const int scaleY = src.mH / h;

	const Vector4 scaleInv(1.0f/(float)(scaleX*scaleY));

	Assert(scaleX >= 1 && scaleX <= 2); // only supports downsampling by 1x2, 2x1 or 2x2
	Assert(scaleY >= 1 && scaleY <= 2);
	Assert(scaleX == 2 || scaleY == 2);

	if (m_texFormatUsage == tfu_NormalMap || m_texFormatUsage == tfu_VectorMap) // use simple linear downsampling .. renormalisation and sharpening are applied in postprocess
	{
		for (int y = 0; y < h; y++)
		{
			for (int x = 0; x < w; x++)
			{
				Vector4 sum(0.0f);

				for (int yy = 0; yy < scaleY; yy++)
				{
					for (int xx = 0; xx < scaleX; xx++)
					{
						sum += src.GetPixel(x*scaleX + xx, y*scaleY + yy);
					}
				}

				dst.SetPixel(x, y, sum*scaleInv);
			}
		}
	}
	else if (m_texFormatUsage == tfu_CableMap) // special mode for cables
	{
		for (int y = 0; y < h; y++)
		{
			for (int x = 0; x < w; x++)
			{
				const int xx = x + ((x < w/2) ? 0 : (w*scaleX - w));
				const int yy = y + ((y < h/2) ? 0 : (h*scaleY - h));

				dst.SetPixel(x, y, src.GetPixel(xx, yy));
			}
		}
	}
	// TODO -- this might be useful, but needs to happen only on the channel(s) which store the distance field
	/*else if (m_distanceField) // use special downsampling for distance fields
	{
		const float distanceFieldSlope = 2.0f; // >1 applies contrast to each mip, while keeping 0.5 threshold the same

		for (int y = 0; y < h; y++)
		{
			for (int x = 0; x < w; x++)
			{
				Vector4 sum(0.0f);

				for (int yy = 0; yy < scaleY; yy++)
				{
					for (int xx = 0; xx < scaleX; xx++)
					{
						const Vector4& c = src.GetPixel(x*scaleX + xx, y*scaleY + yy);

						sum.x += Clamp<float>((c.x*2.0f - 1.0f)*distanceFieldSlope, -1.0f, 1.0f)*0.5f + 0.5f;
						sum.y += Clamp<float>((c.y*2.0f - 1.0f)*distanceFieldSlope, -1.0f, 1.0f)*0.5f + 0.5f;
						sum.z += Clamp<float>((c.z*2.0f - 1.0f)*distanceFieldSlope, -1.0f, 1.0f)*0.5f + 0.5f;
						sum.w += Clamp<float>((c.w*2.0f - 1.0f)*distanceFieldSlope, -1.0f, 1.0f)*0.5f + 0.5f;
					}
				}

				dst.SetPixel(x, y, sum*scaleInv);
			}
		}
	}*/
	else
	{
		const Vector4 mipGamma    = std_VecExp2( m_mipGammaExponent); // 2^mipGamma
		const Vector4 mipGammaInv = std_VecExp2(-m_mipGammaExponent);

		if (m_mipFilterCoefficients.GetCount() == 0)
		{
			for (int y = 0; y < h; y++)
			{
				for (int x = 0; x < w; x++)
				{
					Vector4 sum(0.0f);

					for (int yy = 0; yy < scaleY; yy++)
					{
						for (int xx = 0; xx < scaleX; xx++)
						{
							sum += std_VecPow(src.GetPixel(x*scaleX + xx, y*scaleY + yy), mipGammaInv);
						}
					}

					sum *= scaleInv;
					sum.x = Clamp<float>(sum.x, 0.0f, 1.0f);
					sum.y = Clamp<float>(sum.y, 0.0f, 1.0f);
					sum.z = Clamp<float>(sum.z, 0.0f, 1.0f);
					sum.w = Clamp<float>(sum.w, 0.0f, 1.0f);

					dst.SetPixel(x, y, std_VecPow(sum, mipGamma));
				}
			}
		}
		else
		{
			FilteredDownsample(dst, src, m_texAddr, m_texAddrBorderColour, m_mipGammaExponent, m_mipFilterAmount, m_mipFilterCoefficients);
		}
	}
}

static Vector4 NormalisePixelVector(const Vector4& n)
{
	const float nx = n.x*2.0f - 1.0f;
	const float ny = n.y*2.0f - 1.0f;
	const float nz = n.z*2.0f - 1.0f;
	const float nw = nx*nx + ny*ny + nz*nz;

	if (nw > 0.0f)
	{
		const float q = 1.0f/sqrtf(nw);
		return Vector4(q*nx, q*ny, q*nz, n.w); // [xyz is -1..1]
	}
	else
	{
		return Vector4(0.0f, 0.0f, 0.0f, n.w);
	}
}

static float PackSpecOld(float x)
{
	// Adjust range (0..500 <- 0..1500 and 501..512 <- 1500..8196)
	if (x <= 1500.0f)
		x = x/3.0f;
	else
		x = (x + 277500.0f)/558.0f;

	return x/512.0f; // output range is [0..1]
}

static float PackGlossValue(float variance, float specMin, float specMax, float oneOverLogSpecMaxOverSpecMin, bool bUseOldPacking)
{
	if (variance > 0.0f)
	{
		const float specPow = Clamp<float>(1.0f/variance - 1.0f, specMin, specMax);

		if (bUseOldPacking)
		{
			return PackSpecOld(Clamp<float>(specPow, 1.0f, 8192.0f));
		}
		else
		{
			return Clamp<float>(logf(specPow/specMin)*oneOverLogSpecMaxOverSpecMin, 0.0f, 1.0f);
		}
	}
	else
	{
		return 1.0f;
	}
}

static void ComputeGloss(const CImage4F& top, int topFilterRadius, const CImage4F& mip, int mipFilterRadius, int texAddr, float specVar, float specMin, float specMax, float normalVarianceScale, bool bStoreVarianceInAlpha, bool bUseOldPacking)
{
	const int mw = mip.mW;
	const int mh = mip.mH;
	const int sx = top.mW/mw;
	const int sy = top.mH/mh;

	const float topScale = 1.0f/(float)((sx + topFilterRadius*2)*(sy + topFilterRadius*2));
	const float mipScale = 1.0f/(float)((1  + mipFilterRadius*2)*(1  + mipFilterRadius*2));

	const float oneOverLogSpecMaxOverSpecMin = 1.0f/logf(specMax/specMin);

	for (int y = 0; y < mh; y++)
	{
		for (int x = 0; x < mw; x++)
		{
			Vector4 n = mip.GetPixel(x, y);

			if (mipFilterRadius > 0)
			{
				Vector4 sum(0.0f, 0.0f, 0.0f, 0.0f);

				for (int yy = -mipFilterRadius; yy <= mipFilterRadius; yy++)
				{
					for (int xx = -mipFilterRadius; xx <= mipFilterRadius; xx++)
					{
						switch (texAddr)
						{
						case CTextureConversion::ta_Wrap         : sum += mip.GetPixelWrapped(x + xx, y + yy); break;
						case CTextureConversion::ta_ClampToEdge  : sum += mip.GetPixelClamped(x + xx, y + yy); break;
						case CTextureConversion::ta_ClampToBorder: sum += mip.GetPixelBorder (x + xx, y + yy, Vector4(0.5f, 0.5f, 1.0f, 1.0f)); break;
						}						
					}
				}

				n = sum*mipScale;
			}

			n = NormalisePixelVector(n);

			float variance = 0.0f;

			for (int yy = -topFilterRadius; yy < sy + topFilterRadius; yy++)
			{
				for (int xx = -topFilterRadius; xx < sx + topFilterRadius; xx++)
				{
					Vector4 m(0.0f, 0.0f, 0.0f, 0.0f);

					switch (texAddr)
					{
					case CTextureConversion::ta_Wrap         : m = top.GetPixelWrapped(x*sx + xx, y*sy + yy); break;
					case CTextureConversion::ta_ClampToEdge  : m = top.GetPixelClamped(x*sx + xx, y*sy + yy); break;
					case CTextureConversion::ta_ClampToBorder: m = top.GetPixelBorder (x*sx + xx, y*sy + yy, Vector4(0.5f, 0.5f, 1.0f, 1.0f)); break;
					}

					m = NormalisePixelVector(m);

					const float d = m.x*n.x + m.y*n.y + m.z*n.z;

					variance += 1.0f - d*d;
				}
			}

			variance = Clamp<float>(variance*topScale*normalVarianceScale, 0.0f, 1.0f);

			if (bStoreVarianceInAlpha)
			{
				mip.GetPixel(x, y).w = variance;
			}
			else
			{
				mip.GetPixel(x, y).w = PackGlossValue(variance + specVar, specMin, specMax, oneOverLogSpecMaxOverSpecMin, bUseOldPacking);
			}
		}
	}
}

static void ComputeGlossFromAvgLength(const CImage4F& base, int baseFilterRadius, const CImage4F& mip, int texAddr, float specVar, float specMin, float specMax, float normalVarianceScale, bool bStoreVarianceInAlpha, bool bUseOldPacking)
{
	const int mw = mip.mW;
	const int mh = mip.mH;

	const float oneOverLogSpecMaxOverSpecMin = 1.0f/logf(specMax/specMin);

	if (baseFilterRadius == 0)
	{
		for (int y = 0; y < mh; y++)
		{
			for (int x = 0; x < mw; x++)
			{
				const Vector4& n = mip.GetPixel(x, y);

				const float nx = n.x*2.0f - 1.0f; // linear downsampled vector
				const float ny = n.y*2.0f - 1.0f;
				const float nz = n.z*2.0f - 1.0f;

				const float variance = Clamp<float>((1.0f - nx*nx - ny*ny - nz*nz)*normalVarianceScale, 0.0f, 1.0f);

				if (bStoreVarianceInAlpha)
				{
					mip.GetPixel(x, y).w = variance;
				}
				else
				{
					mip.GetPixel(x, y).w = PackGlossValue(variance + specVar, specMin, specMax, oneOverLogSpecMaxOverSpecMin, bUseOldPacking);
				}
			}
		}
	}
	else
	{
		const int sx = base.mW/mw;
		const int sy = base.mH/mh;

		const float scale = 1.0f/(float)((sx + baseFilterRadius*2)*(sy + baseFilterRadius*2));

		for (int y = 0; y < mh; y++)
		{
			for (int x = 0; x < mw; x++)
			{
				Vector4 sum(0.0f, 0.0f, 0.0f, 0.0f);

				for (int yy = -baseFilterRadius; yy < sy + baseFilterRadius; yy++)
				{
					for (int xx = -baseFilterRadius; xx < sx + baseFilterRadius; xx++)
					{
						switch (texAddr)
						{
						case CTextureConversion::ta_Wrap         : sum += base.GetPixelWrapped(x*sx + xx, y*sy + yy); break;
						case CTextureConversion::ta_ClampToEdge  : sum += base.GetPixelClamped(x*sx + xx, y*sy + yy); break;
						case CTextureConversion::ta_ClampToBorder: sum += base.GetPixelBorder (x*sx + xx, y*sy + yy, Vector4(0.5f, 0.5f, 1.0f, 1.0f)); break;
						}
					}
				}

				const float nx = scale*sum.x*2.0f - 1.0f; // linear downsampled vector
				const float ny = scale*sum.y*2.0f - 1.0f;
				const float nz = scale*sum.z*2.0f - 1.0f;

				const float variance = Clamp<float>((1.0f - nx*nx - ny*ny - nz*nz)*normalVarianceScale, 0.0f, 1.0f);

				if (bStoreVarianceInAlpha)
				{
					mip.GetPixel(x, y).w = variance;
				}
				else
				{
					mip.GetPixel(x, y).w = PackGlossValue(variance + specVar, specMin, specMax, oneOverLogSpecMaxOverSpecMin, bUseOldPacking);
				}
			}
		}
	}
}

static void ApplyAlphaBlur(const CImage4F& mip, int texAddr, float amount, int r, int numPasses, float minFactor)
{
	if (amount > 0.0f)
	{
		const int w = mip.mW;
		const int h = mip.mH;

		sysMemStartTemp();
		float* temp = rage_new float[w*h];
		sysMemEndTemp();

		for (int pass = 0; pass < numPasses; pass++)
		{
			for (int y = 0; y < h; y++)
			{
				for (int x = 0; x < w; x++)
				{
					float sum = 0.0f;

					for (int yy = -r; yy <= r; yy++)
					{
						for (int xx = -r; xx <= r; xx++)
						{
							switch (texAddr)
							{
							case CTextureConversion::ta_Wrap         : sum += mip.GetPixelWrapped(x + xx, y + yy).w; break;
							case CTextureConversion::ta_ClampToEdge  : sum += mip.GetPixelClamped(x + xx, y + yy).w; break;
							case CTextureConversion::ta_ClampToBorder: sum += mip.GetPixelBorder (x + xx, y + yy, Vector4(0.5f, 0.5f, 1.0f, 1.0f)).w; break;
							}
						}
					}

					sum /= (float)((r*2 + 1)*(r*2 + 1));
					sum += (Min<float>(sum, mip.GetPixel(x, y).w) - sum)*minFactor;

					temp[x + y*w] = sum;
				}
			}

			for (int y = 0; y < h; y++)
			{
				for (int x = 0; x < w; x++)
				{
					mip.GetPixel(x, y).w = temp[x + y*w];
				}
			}
		}

		sysMemStartTemp();
		delete[] temp;
		sysMemEndTemp();
	}
}

static float ComputeAlphaCoverage(const CImage4F& image, float alphaRef)
{
	const int w = image.mW;
	const int h = image.mH;
	int coverage = 0;

	for (int y = 0; y < h; y++)
	{
		for (int x = 0; x < w; x++)
		{
			if (image.GetPixel(x, y).w > alphaRef)
			{
				coverage++;
			}
		}
	}

	return (float)coverage/(float)(w*h);
}

void CTextureConversion::PostprocessMip(CImage4F** mipChain, int mipIndex, int mipCount, CTextureFormatHelper& helper) const
{
	const CImage4F& mip = *mipChain[mipIndex];

	const int w = mip.mW;
	const int h = mip.mH;

	if (m_texFormatUsage == tfu_NormalMap || m_texFormatUsage == tfu_VectorMap)
	{
		if (m_normalMapGlossInAlpha)
		{
			int glossSrcFilterRadius = 1;
			int glossDstFilterRadius = 1;

			if (m_normalMapGlossSrcFilterRadius.size() > 0)
			{
				glossSrcFilterRadius = m_normalMapGlossSrcFilterRadius[Min<int>(mipIndex, m_normalMapGlossSrcFilterRadius.size() - 1)];
			}

			if (m_normalMapGlossDstFilterRadius.size() > 0)
			{
				glossDstFilterRadius = m_normalMapGlossDstFilterRadius[Min<int>(mipIndex, m_normalMapGlossDstFilterRadius.size() - 1)];
			}

			const float specMin = m_normalMapGlossSpecularMin;
			const float specMax = m_normalMapGlossSpecularMax;
			float       specVar = m_normalMapGlossSpecularPower;

			if (m_normalMapGlossSpecPowScale.size() > 0)
			{
				if (m_normalMapGlossSpecPowScaleToLastMip)
				{
					specVar *= m_normalMapGlossSpecPowScale[Max<int>(0, mipIndex - mipCount + m_normalMapGlossSpecPowScale.size())];
				}
				else
				{
					specVar *= m_normalMapGlossSpecPowScale[Min<int>(mipIndex, m_normalMapGlossSpecPowScale.size() - 1)];
				}
			}

			specVar = 1.0f/(specVar + 1.0f); // convert specular power to variance

			float normalVarianceScale = 1.0f;

			if (m_normalMapGlossVarianceScale.size() > 0)
			{
				if (m_normalMapGlossVarianceScaleToLastMip)
				{
					normalVarianceScale = m_normalMapGlossVarianceScale[Max<int>(0, mipIndex - mipCount + m_normalMapGlossVarianceScale.size())];
				}
				else
				{
					normalVarianceScale = m_normalMapGlossVarianceScale[Min<int>(mipIndex, m_normalMapGlossVarianceScale.size() - 1)];
				}
			}

			if (m_normalMapGlossUseAverageLength)
			{
				ComputeGlossFromAvgLength(*mipChain[Max<int>(0, mipIndex - 1)], glossSrcFilterRadius, mip, m_texAddr, specVar, specMin, specMax, normalVarianceScale, m_normalMapGlossStoreVarianceAlpha, m_normalMapGlossUseOldPWLPacking);
			}
			else
			{
				ComputeGloss(*mipChain[0], glossSrcFilterRadius, mip, glossDstFilterRadius, m_texAddr, specVar, specMin, specMax, normalVarianceScale, m_normalMapGlossStoreVarianceAlpha, m_normalMapGlossUseOldPWLPacking);
			}

			// you have to set all three of these to use alpha blur
			// note that 'MinFactor' is designed to be used with variance maps .. for gloss maps you probably want 'MaxFactor'
			if (m_normalMapGlossAlphaBlur.size() > 0 &&
				m_normalMapGlossAlphaBlurRadius > 0 &&
				m_normalMapGlossAlphaBlurPasses > 0)
			{
				ApplyAlphaBlur(mip, m_texAddr, m_normalMapGlossAlphaBlur[Min<int>(mipIndex, m_normalMapGlossAlphaBlur.size() - 1)], m_normalMapGlossAlphaBlurRadius, m_normalMapGlossAlphaBlurPasses, m_normalMapGlossAlphaBlurMinFactor);
			}

			if (m_normalMapGlossAlphaOnly)
			{
				for (int y = 0; y < h; y++)
				{
					for (int x = 0; x < w; x++)
					{
						Vector4& n = mip.GetPixel(x, y);
						
						n.x = 0.0f;
						n.y = 0.0f;
						n.z = 0.0f;
					}
				}
			}

			if (m_normalMapGlossAlphaExponent != 1.0f)
			{
				for (int y = 0; y < h; y++)
				{
					for (int x = 0; x < w; x++)
					{
						Vector4& n = mip.GetPixel(x, y);

						n.w = powf(n.w, m_normalMapGlossAlphaExponent);
					}
				}
			}
		}

		if (mipIndex > 0)
		{
			float sharpen = m_normalMapSharpenAmount;
			float scale   = (float)(1<<mipIndex);

			if (m_normalMapSharpen.size() > 0)
			{
				if (m_normalMapSharpenToLastMip)
				{
					sharpen *= m_normalMapSharpen[Max<int>(0, mipIndex - mipCount + m_normalMapSharpen.size())];
				}
				else
				{
					sharpen *= m_normalMapSharpen[Min<int>(mipIndex, m_normalMapSharpen.size() - 1)];
				}
			}

			for (int y = 0; y < h; y++)
			{
				for (int x = 0; x < w; x++)
				{
					Vector4& n = mip.GetPixel(x, y);

					float nx = n.x*2.0f - 1.0f;
					float ny = n.y*2.0f - 1.0f;
					float nz = n.z*2.0f - 1.0f;

					const float r2 = nx*nx + ny*ny;

					if (r2 > 0.0f)
					{
						if (sharpen > 0.0f)
						{
							const float k = powf(scale, (m_normalMapSharpenAlphaMask ? n.w : 1.0f)*sharpen);

							nz = powf(sqrtf(Max<float>(0.0f, 1.0f - r2)), k);

							const float q = sqrtf(Max<float>(0.0f, 1.0f - nz*nz)/r2);

							nx *= q;
							ny *= q;
						}
						else
						{
							nz = sqrtf(Max<float>(0.0f, 1.0f - r2));
						}
					}
					else
					{
						nz = 1.0f;
					}

					n.x = (nx + 1.0f)/2.0f;
					n.y = (ny + 1.0f)/2.0f;
					n.z = (nz + 1.0f)/2.0f;
				}
			}
		}
	}
	else // not a normal map
	{
		Vector4 filterSharpen = m_mipFilterSharpenAmount;

		if (m_mipFilterSharpen.size() > 0)
		{
			if (m_mipFilterSharpenToLastMip)
			{
				filterSharpen *= m_mipFilterSharpen[Max<int>(0, mipIndex - mipCount + m_mipFilterSharpen.size())];
			}
			else
			{
				filterSharpen *= m_mipFilterSharpen[Min<int>(mipIndex, m_mipFilterSharpen.size() - 1)];
			}
		}

		if (filterSharpen.x != 0.0f ||
			filterSharpen.y != 0.0f ||
			filterSharpen.z != 0.0f ||
			filterSharpen.w != 0.0f)
		{
			CImage4F blr(w, h);

			for (int y = 0; y < h; y++) // create a blurred version of mip (using simple 3x3 box filter)
			{
				for (int x = 0; x < w; x++)
				{
					Vector4 sum(0.0f);

					for (int yy = -1; yy <= 1; yy++)
					{
						for (int xx = -1; xx <= 1; xx++)
						{
							switch (m_texAddr)
							{
							case ta_Wrap         : sum += mip.GetPixelWrapped(x + xx, y + yy); break;
							case ta_ClampToEdge  : sum += mip.GetPixelClamped(x + xx, y + yy); break;
							case ta_ClampToBorder: sum += mip.GetPixelBorder (x + xx, y + yy, m_texAddrBorderColour); break;
							}
						}
					}

					blr.SetPixel(x, y, sum*(1.0f/9.0f));
				}
			}

			for (int y = 0; y < h; y++) // apply the mip sharpening as "colour + (colour - blurred)*sharpen"
			{
				for (int x = 0; x < w; x++)
				{
					Vector4& c0 = mip.GetPixel(x, y);
					Vector4& c1 = blr.GetPixel(x, y);

					c0 += (c0 - c1)*filterSharpen;

					c0.x = Clamp<float>(c0.x, 0.0f, 1.0f);
					c0.y = Clamp<float>(c0.y, 0.0f, 1.0f);
					c0.z = Clamp<float>(c0.z, 0.0f, 1.0f);
					c0.w = Clamp<float>(c0.w, 0.0f, 1.0f);
				}
			}
		}

		Vector3 filterDesaturate = m_mipFilterDesaturateAmount;

		if (m_mipFilterDesaturate.size() > 0)
		{
			if (m_mipFilterDesaturateToLastMip)
			{
				filterDesaturate *= m_mipFilterDesaturate[Max<int>(0, mipIndex - mipCount + m_mipFilterDesaturate.size())];
			}
			else
			{
				filterDesaturate *= m_mipFilterDesaturate[Min<int>(mipIndex, m_mipFilterDesaturate.size() - 1)];
			}
		}

		if (filterDesaturate.x != 0.0f ||
			filterDesaturate.y != 0.0f ||
			filterDesaturate.z != 0.0f)
		{
			for (int y = 0; y < h; y++) // apply the mip saturation as "colour + (luma - colour)*(1 - saturation)"
			{
				for (int x = 0; x < w; x++)
				{
					Vector4& c0 = mip.GetPixel(x, y);
					Vector3 rgb = Vector3(c0.x, c0.y, c0.z);
					const float l = Max<float>(c0.x, c0.y, c0.z);

					rgb += (Vector3(l, l, l) - rgb)*filterDesaturate;

					c0.x = rgb.x;
					c0.y = rgb.y;
					c0.z = rgb.z;
				}
			}
		}

		float alphaScale = 1.0f;

		if (m_alphaScale.size() > 0)
		{
			if (m_alphaScaleToLastMip)
			{
				alphaScale *= m_alphaScale[Max<int>(0, mipIndex - mipCount + m_alphaScale.size())];
			}
			else
			{
				alphaScale *= m_alphaScale[Min<int>(mipIndex, m_alphaScale.size() - 1)];
			}
		}

		if (m_alphaCoverageCompensationFactor > 0.0f)
		{
			static float baseAlphaCoverage = 0.0f;

			if (mipIndex == 0)
			{
				baseAlphaCoverage = ComputeAlphaCoverage(mip, m_alphaCoverageCompensationReference);
			}
			else
			{
				float alphaRef1 = 0.5f;
				float alphaStep = 0.25f;

				for (int k = 0; k < m_alphaCoverageCompensationIterations; k++) // binary search for find new alpha reference such that coverage = base coverage
				{
					const float alphaCoverage1 = ComputeAlphaCoverage(mip, alphaRef1);

					alphaRef1 += alphaStep*(alphaCoverage1 < baseAlphaCoverage ? -1.0f : 1.0f);
					alphaStep *= 0.5f;
				}

				const float alphaCoverageScale = powf(m_alphaCoverageCompensationReference/alphaRef1, m_alphaCoverageCompensationFactor);

				if (alphaCoverageScale > 1.0f || m_alphaCoverageCompensationAllowScaleByLessThanOne)
				{
					alphaScale *= alphaCoverageScale;
				}
			}
		}

		if (alphaScale != 1.0f) // apply alpha scale
		{
			for (int y = 0; y < h; y++)
			{
				for (int x = 0; x < w; x++)
				{
					Vector4& c0 = mip.GetPixel(x, y);

					c0.w = Clamp<float>(c0.w*alphaScale, 0.0f, 1.0f);
				}
			}
		}
	}

	Vector4 fadeColour(0.0f);
	Vector4 fadeAmount(0.0f);
	bool bApplyMipFade = false;

	if (mipIndex > 0)
	{
		fadeColour = m_mipFadeColourAuto ? helper.m_avg : m_mipFadeColour;
		fadeAmount = m_mipFadeAmount;

		if (m_mipFade.size() > 0)
		{
			if (m_mipFadeToLastMip)
			{
				fadeAmount *= m_mipFade[Max<int>(0, mipIndex - mipCount + m_mipFade.size())];
			}
			else
			{
				fadeAmount *= m_mipFade[Min<int>(mipIndex, m_mipFade.size() - 1)];
			}
		}

		bApplyMipFade = !IsZeroAll(RCC_VEC4V(fadeAmount));
	}

	const Vector4 finalGamma = std_VecExp2(m_finalGammaExponent); // 2^finalGamma

	for (int y = 0; y < h; y++)
	{
		for (int x = 0; x < w; x++)
		{
			Vector4& c = mip.GetPixel(x, y);

			if (m_colourKey.GetColor() != 0 &&
				m_colourKey.GetRed  () == (int)(c.x*255.0f + 0.5f) &&
				m_colourKey.GetGreen() == (int)(c.y*255.0f + 0.5f) &&
				m_colourKey.GetBlue () == (int)(c.z*255.0f + 0.5f))
			{
				continue;
			}

			c = std_VecPow(c, finalGamma);
			c *= m_finalMultiplier;

			if (mipIndex == 0) // use fadeColour to accumulate sum of pixel values in top-level mip, for helper.m_avg
			{
				fadeColour += c;
			}
			else if (bApplyMipFade)
			{
				if (IsLessThanAll   (RCC_VEC4V(c), RCC_VEC4V(m_mipFadeThresholdMin)) |
					IsGreaterThanAll(RCC_VEC4V(c), RCC_VEC4V(m_mipFadeThresholdMax)))
				{
					// no mip fade
				}
				else
				{
					c += fadeAmount*(fadeColour - c);
				}
			}

			if (x == 0 || x == w - 1 ||
				y == 0 || y == h - 1)
			{
				if (m_applyBorderColourR) { c.x = m_texAddrBorderColour.x; }
				if (m_applyBorderColourG) { c.y = m_texAddrBorderColour.y; }
				if (m_applyBorderColourB) { c.z = m_texAddrBorderColour.z; }
				if (m_applyBorderColourA) { c.w = m_texAddrBorderColour.w; }
			}

			if (m_alphaThreshold > 0.0f)
			{
				c.w = (c.w < m_alphaThreshold) ? 0.0f : 1.0f;
			}

			if (m_alphaPremultiply)
			{
				c.x *= c.w;
				c.y *= c.w;
				c.z *= c.w;
			}
		}
	}

	if (mipIndex == 0)
	{
		helper.m_avg = fadeColour*(1.0f/(float)(w*h));
	}

	// debuggery
	{
		if (m_debugParams.m_mipFillAmount > 0.0f &&
			m_debugParams.m_mipFillStart <= mipIndex &&
			m_debugParams.m_mipFillBorderThickness != 0)
		{
			const int border = m_debugParams.m_mipFillBorderThickness;

			const int     kDebugColourCount = 6;
			const Vector4 kDebugColours[kDebugColourCount] =
			{
				Vector4(1.0f, 0.0f, 0.0f, 1.0f), // red
				Vector4(1.0f, 1.0f, 0.0f, 1.0f), // yellow
				Vector4(0.0f, 1.0f, 0.0f, 1.0f), // green
				Vector4(0.0f, 1.0f, 1.0f, 1.0f), // cyan
				Vector4(0.0f, 0.0f, 1.0f, 1.0f), // blue
				Vector4(1.0f, 0.0f, 1.0f, 1.0f), // magenta
			};

			const Vector4& debugColour = kDebugColours[(mipIndex - m_debugParams.m_mipFillStart)%kDebugColourCount];

			for (int y = 0; y < h; y++)
			{
				for (int x = 0; x < w; x++)
				{
					if (border < 0 || x < border || y < border || (w - x - 1) < border || (h - y - 1) < border)
					{
						Vector4& c = mip.GetPixel(x, y);

						c += (debugColour - c)*m_debugParams.m_mipFillAmount;
					}
				}
			}
		}
	}

	for (int y = 0; y < h; y++) // final pass - consider texels to choose texture format
	{
		for (int x = 0; x < w; x++)
		{
			helper.ConsiderPixel(mip.GetPixel(x, y));
		}
	}
}

#if 0

// from xenon docs
static __forceinline int degamma(int Cgam)
{
	int Clin;

	if      (Cgam <  64) { Clin =   0 + (Cgam -   0)*1; }
	else if (Cgam <  96) { Clin =  64 + (Cgam -  64)*2; }
	else if (Cgam < 192) { Clin = 128 + (Cgam -  96)*4; }
	else                 { Clin = 513 + (Cgam - 192)*8; }

	return Clin;
}

// from xenon docs
static __forceinline int gamma(int Clin)
{
	int Cgam;

	if      (Clin <  64) { Cgam =   0 + (Clin -   0)/1; }
	else if (Clin < 128) { Cgam =  64 + (Clin -  64)/2; }
	else if (Clin < 513) { Cgam =  96 + (Clin - 128)/4; }
	else                 { Cgam = 192 + (Clin - 512)/8; }

	return Cgam;
}

// from PS3 docs
static __forceinline float ApplyGamma_PS3(float Cgam)
{
	if      (Cgam < 0.00000f) { return 0.0f; }
	else if (Cgam < 0.00318f) { return 12.92f*Cgam; }
	else if (Cgam < 1.00000f) { return 1.055f*powf(Cgam, 1.0f/2.4f) - 0.055f; }

	return 1.0f;
}

static __forceinline float GammaToLinearPS3(float Cgam)
{
	if      (Cgam <= 0.00000f) { return 0.0f; }
	else if (Cgam <= 0.03928f) { return Cgam/12.92f; } // website says 0.04045f (as does wikipedia), PS3 docs say 0.03928f
	else if (Cgam <= 1.00000f) { return powf((Cgam + 0.055f)/1.055f, 2.4f); }

	return 1.0f;
}

static __forceinline float GammaToLinearXenon(float Cgam)
{
	float Clin;

	if      (Cgam < 0.250f) { Clin = 0.0000f + (Cgam - 0.000f)*0.25f; }
	else if (Cgam < 0.375f) { Clin = 0.0625f + (Cgam - 0.250f)*0.50f; }
	else if (Cgam < 0.750f) { Clin = 0.1250f + (Cgam - 0.375f)*1.00f; }
	else                    { Clin = 0.5000f + (Cgam - 0.750f)*2.00f; }

	return Clin;
}

// http://en.wikipedia.org/wiki/SRGB
static __forceinline float LinearToSRGB(float Clin)
{
	if      (Clin <= 0.0000000f) { return 0.0f; }
	else if (Clin <= 0.0031308f) { return 12.92f*Clin; }
	else if (Clin <= 1.0000000f) { return 1.055f*powf(Clin, 1.0f/2.4f) - 0.055f; }

	return 1.0f;
}

#endif // 0

// http://en.wikipedia.org/wiki/SRGB
static __forceinline float SRGBToLinear(float Cgam)
{
	if      (Cgam <= 0.00000f) { return 0.0f; }
	else if (Cgam <= 0.04045f) { return Cgam/12.92f; }
	else if (Cgam <= 1.00000f) { return powf((Cgam + 0.055f)/1.055f, 2.4f); }

	return 1.0f;
}

// http://filmicgames.com/archives/14
static __forceinline float LinearToGammaXenon(float Clin)
{
	float Cgam;

	if      (Clin < 0.0625f) { Cgam = 0.000f + (Clin - 0.0000f)*4.0f; }
	else if (Clin < 0.1250f) { Cgam = 0.250f + (Clin - 0.0625f)*2.0f; }
	else if (Clin < 0.5000f) { Cgam = 0.375f + (Clin - 0.1250f)*1.0f; }
	else                     { Cgam = 0.750f + (Clin - 0.5000f)*0.5f; }

	return Cgam;
}

static __forceinline float LinearToGammaPS3(float Clin)
{
	if      (Clin < 0.00000f) { return 0.0f; }
	else if (Clin < 0.00318f) { return 12.92f*Clin; }
	else if (Clin < 1.00000f) { return 1.055f*powf(Clin, 1.0f/2.4f) - 0.055f; }

	return 1.0f;
}

static void LinearToGamma(const CImage4F& image, char platform_)
{
	const int w = image.mW;
	const int h = image.mH;

	for (int y = 0; y < h; y++)
	{
		if (platform_ == platform::XENON)
		{
			for (int x = 0; x < w; x++)
			{
				Vector4& c = image.GetPixel(x, y);

				c.x = LinearToGammaXenon(c.x);
				c.y = LinearToGammaXenon(c.y);
				c.z = LinearToGammaXenon(c.z);
			}
		}
		else // sRGB is approximately powf(x, 2.2f) with a small linear segment near 0, so .. could apply final gamma exponent of -1.1375 instead, but this is more straightforward
		{
			for (int x = 0; x < w; x++)
			{
				Vector4& c = image.GetPixel(x, y);

				c.x = LinearToGammaPS3(c.x);
				c.y = LinearToGammaPS3(c.y);
				c.z = LinearToGammaPS3(c.z);
			}
		}
	}
}

static Vector4 PixelPreSwizzle(const Vector4& v, const char* swizzle) // this happens right before compression
{
	const float* src = (const float*)&v;

	bool r_valid = false;
	bool g_valid = false;
	bool b_valid = false;
	bool a_valid = false;

	float r = 0.0f;
	float g = 0.0f;
	float b = 0.0f;
	float a = 1.0f;

	for (int i = 0; i < 4; i++) // r,g,b,a
	{
		switch (swizzle[i])
		{
		case 'R' : r = r_valid ? Max<float>(src[i], r) : src[i]; r_valid = true; break;
		case 'G' : g = g_valid ? Max<float>(src[i], g) : src[i]; g_valid = true; break;
		case 'B' : b = b_valid ? Max<float>(src[i], b) : src[i]; b_valid = true; break;
		case 'A' : a = a_valid ? Max<float>(src[i], a) : src[i]; a_valid = true; break;
		case '0' : break;
		case '1' : break;
		}
	}

	return Vector4(r, g, b, a);
}

static Vector4 PixelSwizzle(const Vector4& v, const char* swizzle) // this happens automatically on the GPU, provided here for reference
{
	float dst[4] = {0.0f, 0.0f, 0.0f, 0.0f};

	for (int i = 0; i < 4; i++) // r,g,b,a
	{
		switch (swizzle[i])
		{
		case 'R' : dst[i] = v.x; break;
		case 'G' : dst[i] = v.y; break;
		case 'B' : dst[i] = v.z; break;
		case 'A' : dst[i] = v.w; break;
		case '0' : dst[i] = 0.f; break;
		case '1' : dst[i] = 1.f; break;
		}
	}

	return Vector4(dst[0], dst[1], dst[2], dst[3]);
}

static void ImagePreSwizzle(const CImage4F& image, const char* swizzle)
{
	if (strlen(swizzle) >= 4)
	{
		const int w = image.mW;
		const int h = image.mH;

		for (int y = 0; y < h; y++)
		{
			for (int x = 0; x < w; x++)
			{
				Vector4& c = image.GetPixel(x, y);

				c = PixelPreSwizzle(c, swizzle);
			}
		}
	}
}

static void ImageSwizzle(const CImage4F& image, const char* swizzle)
{
	if (strlen(swizzle) >= 4)
	{
		const int w = image.mW;
		const int h = image.mH;

		for (int y = 0; y < h; y++)
		{
			for (int x = 0; x < w; x++)
			{
				Vector4& c = image.GetPixel(x, y);

				c = PixelSwizzle(c, swizzle);
			}
		}
	}
}

grcImage::Format CTextureConversion::ChooseTextureFormat(const char*& swizzle, const CTextureFormatHelper& helper) const
{
	eTextureFormat format = m_texFormatOverride;

	if (format == tf_UNKNOWN)
	{
		const float epsilon         = 1.0f/127.0f;
		const bool bIsRedZero       = (helper.m_min.x >= 0.0f - epsilon && helper.m_max.x <= 0.0f + epsilon);
		const bool bIsRedOne        = (helper.m_min.x >= 1.0f - epsilon && helper.m_max.x <= 1.0f + epsilon);
		const bool bIsGreenZero     = (helper.m_min.y >= 0.0f - epsilon && helper.m_max.y <= 0.0f + epsilon);
		const bool bIsGreenOne      = (helper.m_min.y >= 1.0f - epsilon && helper.m_max.y <= 1.0f + epsilon);
		const bool bIsBlueZero      = (helper.m_min.z >= 0.0f - epsilon && helper.m_max.z <= 0.0f + epsilon);
		const bool bIsBlueOne       = (helper.m_min.z >= 1.0f - epsilon && helper.m_max.z <= 1.0f + epsilon);
		const bool bIsAlphaZero     = (helper.m_min.w >= 0.0f - epsilon && helper.m_max.w <= 0.0f + epsilon);
		const bool bIsAlphaOne      = (helper.m_min.w >= 1.0f - epsilon && helper.m_max.w <= 1.0f + epsilon);
		const bool bIsMonochromeRed = (helper.m_max.x - helper.m_min.x > epsilon && bIsGreenZero && bIsBlueZero && bIsAlphaOne); // R001 format
		const bool bIsMonochrome    = (helper.m_maxRGBSaturation   <= epsilon);
		const bool bAlphaIs1bit     = (helper.m_maxAlphaDiffFrom01 <= epsilon);
		const bool bAlphaIsBlack    = (helper.m_maxLumaMinusAlpha0 <= epsilon);
		const bool bXenon           = (sm_platform == platform::XENON) && !m_texFormatHintNoXenonFormats;

		bool bXenonCTX1 = bXenon;

		if (m_texFormatUsage == tfu_NormalMap ||
			m_texFormatUsage == tfu_VectorMap)
		{
			if (!m_texFormatHintAllowCTX1NormalMaps)
			{
				bXenonCTX1 = false;
			}
		}
		else
		{
			if (!m_texFormatHintAllowCTX1ColourMaps)
			{
				bXenonCTX1 = false;
			}
		}

		eTextureFormatChannelGroup channelGroup;

		if (bIsRedZero && bIsGreenZero && bIsBlueZero)
		{
			channelGroup = tfcg_A;
		}
		else if (bIsMonochromeRed)
		{
			channelGroup = tfcg_R;
		}
		else if (((m_texFormatUsage == tfu_NormalMap && m_texFormatHint == tfh_Compressed) || bIsBlueOne) && bIsAlphaOne)
		{
			// Normal maps are special .. during processing they have r,g,b channels (and potentially alpha)
			// but before conversion we are going to overwrite the blue channel with 1.0f, so _if_ alpha is
			// uniform 1.0f too then we can use the 'RG_B1' channel group (i.e. RG11).
			channelGroup = tfcg_RG_B1;
		}
		else if (((m_texFormatUsage == tfu_NormalMap && m_texFormatHint != tfh_Compressed) || bIsBlueZero) && bIsAlphaOne)
		{
			// Normal maps compressed with high quality (DXT5, DXN, G8R8 etc.) will have RG01 swizzle, so
			// that they look different in the texture viewer. Also, next-gen platforms output RG01 for both
			// DXN and G8R8 but they don't support RG11 swizzle.
			channelGroup = tfcg_RG;
		}
		else if (bIsAlphaOne)
		{
			channelGroup = bIsMonochrome ? tfcg_L : tfcg_RGB;
		}
		else
		{
			channelGroup = bIsMonochrome ? tfcg_LA : tfcg_RGBA;
		}

		if (channelGroup == tfcg_RGBA)
		{
			if (bIsRedOne && bIsGreenZero && bIsBlueZero)
			{
				channelGroup = tfcg_A_RGB100; // alpha, rgb={1,0,0}
			}
			else if (bIsRedOne && bIsGreenOne && bIsBlueOne)
			{
				channelGroup = tfcg_A_RGB111; // alpha, rgb={1,1,1}
			}
			else if (bIsAlphaZero)
			{
				channelGroup = tfcg_RGB_A0; // rgb, alpha=0
			}
		}
		else if (channelGroup == tfcg_LA)
		{
			if (bIsAlphaZero)
			{
				channelGroup = tfcg_L_A0; // luminance, alpha=0
			}
		}

		// ========================================================================================
		// ========================================================================================
		//                                    RGB            RGBA          L/A         LA/RG
		// -----------------------------+----------------------------------------------------------
		// tfh_Compressed               |[  4]DXT1      [  8]DXT5      [ 4]DXT5A   [ 4]CTX1       8
		// tfh_CompressedHighQuality    |[  8]DXT5(GAR) [  8]DXT5      [ 4]DXT5A   [ 8]DXN        8
		// tfh_Quantised                |[ 16]565       [ 16]4444      [ 4]DXT5A   [ 8]DXN       16
		// tfh_8bpc                     |[ 32]8888      [ 32]8888      [ 8]8       [16]88        32
		// tfh_16bpc                    |[ 64]16161616  [ 64]16161616  [16]16      [32]1616
		// tfh_16bpc_float              |[ 64]16161616f [ 64]16161616f [16]16f     [32]1616f
		// tfh_32bpc_float              |[128]32323232f [128]32323232f [32]32f     [64]3232f
		//
		// note: PS3 does not support CTX1,DXN,DXT3A,DXT5A ..
		//  * uses DXT1(RG) instead of CTX1
		//  * uses DXT5(AG) instead of DXN
		//  * uses DXT1(G)  instead of DXT5A
		// ========================================================================================
		// ========================================================================================

		if (sm_platform == platform::XENON ||
			sm_platform == platform::PS3)
		{
			switch (channelGroup)
			{
			case tfcg_RGB: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT1         ; swizzle = "RGB1"; break;
				case tfh_CompressedHighQuality : format = tf_DXT5         ; swizzle = "GAR1"; break;
				case tfh_Quantised             : format = tf_R5G6B5       ; swizzle = "RGB1"; break;
				case tfh_8bpc                  : format = tf_A8R8G8B8     ; swizzle = "RGB1"; break;
				case tfh_16bpc                 : format = tf_A16B16G16R16 ; swizzle = "RGB1"; break;
				case tfh_16bpc_float           : format = tf_A16B16G16R16F; swizzle = "RGB1"; break;
				case tfh_32bpc_float           : format = tf_A32B32G32R32F; swizzle = "RGB1"; break;
				}
				break;

			case tfcg_RGB_A0: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT1         ; swizzle = "RGB0"; break;
				case tfh_CompressedHighQuality : format = tf_DXT5         ; swizzle = "GAR0"; break;
				case tfh_Quantised             : format = tf_R5G6B5       ; swizzle = "RGB0"; break;
				case tfh_8bpc                  : format = tf_A8R8G8B8     ; swizzle = "RGB0"; break;
				case tfh_16bpc                 : format = tf_A16B16G16R16 ; swizzle = "RGB0"; break;
				case tfh_16bpc_float           : format = tf_A16B16G16R16F; swizzle = "RGB0"; break;
				case tfh_32bpc_float           : format = tf_A32B32G32R32F; swizzle = "RGB0"; break;
				}
				break;

			case tfcg_RGBA: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT5         ; swizzle = "RGBA"; break;
				case tfh_CompressedHighQuality : format = tf_DXT5         ; swizzle = m_texFormatHintHighQualityWithAlpha ? "GARB" : "RGBA"; break;
				case tfh_Quantised             : format = tf_A4R4G4B4     ; swizzle = "RGBA"; break;
				case tfh_8bpc                  : format = tf_A8R8G8B8     ; swizzle = "RGBA"; break;
				case tfh_16bpc                 : format = tf_A16B16G16R16 ; swizzle = "RGBA"; break;
				case tfh_16bpc_float           : format = tf_A16B16G16R16F; swizzle = "RGBA"; break;
				case tfh_32bpc_float           : format = tf_A32B32G32R32F; swizzle = "RGBA"; break;
				}
				break;

			case tfcg_L: switch (m_texFormatHint)
				{
				case tfh_Compressed            : if (bXenon) { format = tf_DXT5A; swizzle = "AAA1"; } else { format = tf_DXT1; swizzle = "GGG1"; } break;
				case tfh_CompressedHighQuality : if (bXenon) { format = tf_DXT5A; swizzle = "AAA1"; } else { format = tf_DXT1; swizzle = "GGG1"; } break;
				case tfh_Quantised             : if (bXenon) { format = tf_DXT3A; swizzle = "AAA1"; } else { format = tf_L8  ; swizzle = "RRR1"; } break;
				case tfh_8bpc                  :             { format = tf_L8   ; swizzle = "RRR1"; } break; // could also be tf_A8 "AAA1" or tf_R8 "RRR1"
				case tfh_16bpc                 :             { format = tf_L16  ; swizzle = "RRR1"; } break; // could also be tf_R16 "RRR1"
				case tfh_16bpc_float           :             { format = tf_R16F ; swizzle = "RRR1"; } break;
				case tfh_32bpc_float           :             { format = tf_R32F ; swizzle = "RRR1"; } break;
				}
				break;

			case tfcg_L_A0: switch (m_texFormatHint)
				{
				case tfh_Compressed            : if (bXenon) { format = tf_DXT5A; swizzle = "AAA0"; } else { format = tf_DXT1; swizzle = "GGG0"; } break;
				case tfh_CompressedHighQuality : if (bXenon) { format = tf_DXT5A; swizzle = "AAA0"; } else { format = tf_DXT1; swizzle = "GGG0"; } break;
				case tfh_Quantised             : if (bXenon) { format = tf_DXT3A; swizzle = "AAA0"; } else { format = tf_L8  ; swizzle = "RRR0"; } break;
				case tfh_8bpc                  :             { format = tf_L8   ; swizzle = "RRR0"; } break; // could also be tf_A8 "AAA0" or tf_R8 "RRR0"
				case tfh_16bpc                 :             { format = tf_L16  ; swizzle = "RRR0"; } break; // could also be tf_R16 "RRR0"
				case tfh_16bpc_float           :             { format = tf_R16F ; swizzle = "RRR0"; } break;
				case tfh_32bpc_float           :             { format = tf_R32F ; swizzle = "RRR0"; } break;
				}
				break;

			case tfcg_A: switch (m_texFormatHint)
				{
				case tfh_Compressed            : if (bXenon) { format = tf_DXT5A; swizzle = "000A"; } else { format = tf_DXT1; swizzle = "000G"; } break;
				case tfh_CompressedHighQuality : if (bXenon) { format = tf_DXT5A; swizzle = "000A"; } else { format = tf_DXT1; swizzle = "000G"; } break;
				case tfh_Quantised             : if (bXenon) { format = tf_DXT3A; swizzle = "000A"; } else { format = tf_L8  ; swizzle = "000R"; } break;
				case tfh_8bpc                  :             { format = tf_L8   ; swizzle = "000R"; } break; // could also be tf_A8 "000A" or tf_R8 "000R"
				case tfh_16bpc                 :             { format = tf_L16  ; swizzle = "000R"; } break; // could also be tf_R16 "000R"
				case tfh_16bpc_float           :             { format = tf_R16F ; swizzle = "000R"; } break;
				case tfh_32bpc_float           :             { format = tf_R32F ; swizzle = "000R"; } break;
				}
				break;

			case tfcg_A_RGB100: switch (m_texFormatHint)
				{
				case tfh_Compressed            : if (bXenon) { format = tf_DXT5A; swizzle = "100A"; } else { format = tf_DXT1; swizzle = "100G"; } break;
				case tfh_CompressedHighQuality : if (bXenon) { format = tf_DXT5A; swizzle = "100A"; } else { format = tf_DXT1; swizzle = "100G"; } break;
				case tfh_Quantised             : if (bXenon) { format = tf_DXT3A; swizzle = "100A"; } else { format = tf_L8  ; swizzle = "100R"; } break;
				case tfh_8bpc                  :             { format = tf_L8   ; swizzle = "100R"; } break; // could also be tf_A8 "100A" or tf_R8 "100R"
				case tfh_16bpc                 :             { format = tf_L16  ; swizzle = "100R"; } break; // could also be tf_R16 "100R"
				case tfh_16bpc_float           :             { format = tf_R16F ; swizzle = "100R"; } break;
				case tfh_32bpc_float           :             { format = tf_R32F ; swizzle = "100R"; } break;
				}
				break;

			case tfcg_A_RGB111: switch (m_texFormatHint)
				{
				case tfh_Compressed            : if (bXenon) { format = tf_DXT5A; swizzle = "111A"; } else { format = tf_DXT1; swizzle = "111G"; } break;
				case tfh_CompressedHighQuality : if (bXenon) { format = tf_DXT5A; swizzle = "111A"; } else { format = tf_DXT1; swizzle = "111G"; } break;
				case tfh_Quantised             : if (bXenon) { format = tf_DXT3A; swizzle = "111A"; } else { format = tf_L8  ; swizzle = "111R"; } break;
				case tfh_8bpc                  :             { format = tf_L8   ; swizzle = "111R"; } break; // could also be tf_A8 "111A" or tf_R8 "111R"
				case tfh_16bpc                 :             { format = tf_L16  ; swizzle = "111R"; } break; // could also be tf_R16 "111R"
				case tfh_16bpc_float           :             { format = tf_R16F ; swizzle = "111R"; } break;
				case tfh_32bpc_float           :             { format = tf_R32F ; swizzle = "111R"; } break;
				}
				break;

			case tfcg_LA: switch (m_texFormatHint)
				{
				case tfh_Compressed            : if (bXenonCTX1) { format = tf_CTX1    ; swizzle = "RRRG"; } else { format = tf_DXT1; swizzle = "RRRG"; } break;
				case tfh_CompressedHighQuality : if (bXenon)     { format = tf_DXN     ; swizzle = "RRRG"; } else { format = tf_DXT5; swizzle = "GGGA"; } break;
				case tfh_Quantised             :                 { format = tf_A8L8    ; swizzle = "RRRA"; } break; // could also be tf_G8R8 "RRRG"
				case tfh_8bpc                  :                 { format = tf_A8L8    ; swizzle = "RRRA"; } break; // could also be tf_G8R8 "RRRG"
				case tfh_16bpc                 :                 { format = tf_G16R16  ; swizzle = "RRRG"; } break;
				case tfh_16bpc_float           :                 { format = tf_G16R16F ; swizzle = "RRRG"; } break;
				case tfh_32bpc_float           :                 { format = tf_G32R32F ; swizzle = "RRRG"; } break;
				}
				break;

			case tfcg_RG: switch (m_texFormatHint)
				{
				case tfh_Compressed            : if (bXenonCTX1) { format = tf_CTX1   ; swizzle = "RG01"; } else { format = tf_DXT1; swizzle = "RG01"; } break;
				case tfh_CompressedHighQuality : if (bXenon)     { format = tf_DXN    ; swizzle = "RG01"; } else { format = tf_DXT5; swizzle = "GA01"; } break;
				case tfh_Quantised             :                 { format = tf_A8L8   ; swizzle = "RA01"; } break; // could also be tf_G8R8 "RG01"
				case tfh_8bpc                  :                 { format = tf_A8L8   ; swizzle = "RA01"; } break; // could also be tf_G8R8 "RG01"
				case tfh_16bpc                 :                 { format = tf_G16R16 ; swizzle = "RG01"; } break;
				case tfh_16bpc_float           :                 { format = tf_G16R16F; swizzle = "RG01"; } break;
				case tfh_32bpc_float           :                 { format = tf_G32R32F; swizzle = "RG01"; } break;
				}
				break;

			case tfcg_RG_B1: switch (m_texFormatHint)
				{
				case tfh_Compressed            : if (bXenonCTX1) { format = tf_CTX1   ; swizzle = "RG11"; } else { format = tf_DXT1; swizzle = "RG11"; } break;
				case tfh_CompressedHighQuality : if (bXenon)     { format = tf_DXN    ; swizzle = "RG11"; } else { format = tf_DXT5; swizzle = "GA11"; } break;
				case tfh_Quantised             :                 { format = tf_A8L8   ; swizzle = "RA11"; } break; // could also be tf_G8R8 "RG11"
				case tfh_8bpc                  :                 { format = tf_A8L8   ; swizzle = "RA11"; } break; // could also be tf_G8R8 "RG11"
				case tfh_16bpc                 :                 { format = tf_G16R16 ; swizzle = "RG11"; } break;
				case tfh_16bpc_float           :                 { format = tf_G16R16F; swizzle = "RG11"; } break;
				case tfh_32bpc_float           :                 { format = tf_G32R32F; swizzle = "RG11"; } break;
				}
				break;

			case tfcg_R: switch (m_texFormatHint)
				{
				case tfh_Compressed            : if (bXenon) { format = tf_DXT5A; swizzle = "A001"; } else { format = tf_DXT1; swizzle = "G001"; } break;
				case tfh_CompressedHighQuality : if (bXenon) { format = tf_DXT5A; swizzle = "A001"; } else { format = tf_DXT5; swizzle = "A001"; } break;
				case tfh_Quantised             : if (bXenon) { format = tf_DXT3A; swizzle = "A001"; } else { format = tf_L8  ; swizzle = "R001"; } break;
				case tfh_8bpc                  :             { format = tf_L8   ; swizzle = "R001"; } break;
				case tfh_16bpc                 :             { format = tf_L16  ; swizzle = "R001"; } break;
				case tfh_16bpc_float           :             { format = tf_R16F ; swizzle = "R001"; } break;
				case tfh_32bpc_float           :             { format = tf_R32F ; swizzle = "R001"; } break;
				}
				break;
			}
		}
		else // no support for swizzle
		{
			swizzle = "RGBA";

			switch (channelGroup)
			{
			case tfcg_RGB: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT1         ; break;
				case tfh_CompressedHighQuality : format = tf_DXT1         ; break; // TODO -- use BC7
				case tfh_Quantised             : format = tf_R5G6B5       ; break;
				case tfh_8bpc                  : format = tf_A8B8G8R8     ; break;
				case tfh_16bpc                 : format = tf_A16B16G16R16 ; break;
				case tfh_16bpc_float           : format = tf_A16B16G16R16F; break;
				case tfh_32bpc_float           : format = tf_A32B32G32R32F; break;
				}
				break;

			case tfcg_RGB_A0: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT5         ; break;
				case tfh_CompressedHighQuality : format = tf_DXT5         ; break; // TODO -- use BC7
				case tfh_Quantised             : format = tf_A1R5G5B5     ; break; // alpha does not require precision
				case tfh_8bpc                  : format = tf_A8B8G8R8     ; break;
				case tfh_16bpc                 : format = tf_A16B16G16R16 ; break;
				case tfh_16bpc_float           : format = tf_A16B16G16R16F; break;
				case tfh_32bpc_float           : format = tf_A32B32G32R32F; break;
				}
				break;

			case tfcg_RGBA: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT5         ; break;
				case tfh_CompressedHighQuality : format = tf_DXT5         ; break; // TODO -- use BC7
				case tfh_Quantised             : format = tf_A8B8G8R8     ; break; // ouch .. no 4444 support, use 8888 instead
				case tfh_8bpc                  : format = tf_A8B8G8R8     ; break;
				case tfh_16bpc                 : format = tf_A16B16G16R16 ; break;
				case tfh_16bpc_float           : format = tf_A16B16G16R16F; break;
				case tfh_32bpc_float           : format = tf_A32B32G32R32F; break;
				}
				break;

			case tfcg_L: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT1         ; break; // low quality, would be better to use DXT5A but need to swizzle
				case tfh_CompressedHighQuality : format = tf_DXT1         ; break; // low quality, would be better to use DXT5A but need to swizzle
				case tfh_Quantised             : format = tf_A1R5G5B5     ; break; // low quality, would be better to use R8 or A8 but need to swizzle
				case tfh_8bpc                  : format = tf_A8B8G8R8     ; break; // ouch .. would be better to use R8 or A8 but need to swizzle
				case tfh_16bpc                 : format = tf_A16B16G16R16 ; break; // ouch .. would be better to use R16 but need to swizzle
				case tfh_16bpc_float           : format = tf_A16B16G16R16F; break; // ouch .. would be better to use R16F but need to swizzle
				case tfh_32bpc_float           : format = tf_A32B32G32R32F; break; // ouch .. would be better to use R32F but need to swizzle
				}
				break;

			case tfcg_A: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_A8           ; break; // ouch .. would be better to use DXT5A but need to swizzle
				case tfh_CompressedHighQuality : format = tf_A8           ; break; // ouch .. would be better to use DXT5A but need to swizzle
				case tfh_Quantised             : format = tf_A8           ; break;
				case tfh_8bpc                  : format = tf_A8           ; break;
				case tfh_16bpc                 : format = tf_A16B16G16R16 ; break; // ouch .. would be better to use R16 but need to swizzle
				case tfh_16bpc_float           : format = tf_A16B16G16R16F; break; // ouch .. would be better to use R16F but need to swizzle
				case tfh_32bpc_float           : format = tf_A32B32G32R32F; break; // ouch .. would be better to use R32F but need to swizzle
				}
				break;

			case tfcg_A_RGB100:
			case tfcg_A_RGB111: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT5         ; break; // ouch .. would be better to use DXT5A but need to swizzle
				case tfh_CompressedHighQuality : format = tf_DXT5         ; break; // ouch .. would be better to use DXT5A but need to swizzle
				case tfh_Quantised             : format = tf_DXT3         ; break; // ouch .. alpha channel is 4-bit, this is not very efficient though
				case tfh_8bpc                  : format = tf_A8B8G8R8     ; break; // ouch .. would be better to use R8 or A8 but need to swizzle
				case tfh_16bpc                 : format = tf_A16B16G16R16 ; break; // ouch .. would be better to use R16 but need to swizzle
				case tfh_16bpc_float           : format = tf_A16B16G16R16F; break; // ouch .. would be better to use R16F but need to swizzle
				case tfh_32bpc_float           : format = tf_A32B32G32R32F; break; // ouch .. would be better to use R32F but need to swizzle
				}
				break;

			case tfcg_L_A0:
			case tfcg_LA: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT5         ; break; // ouch .. would be better to use DXT5A (DXT1 for LA) but need to swizzle
				case tfh_CompressedHighQuality : format = tf_DXT5         ; break; // ouch .. would be better to use DXT5A (DXN for LA) but need to swizzle
				case tfh_Quantised             : format = tf_DXT3         ; break; // note that luminance is actually DXT compressed, not 'quantised' .. oh well
				case tfh_8bpc                  : format = tf_A8B8G8R8     ; break; // ouch .. would be better to use R8 or A8 but need to swizzle
				case tfh_16bpc                 : format = tf_A16B16G16R16 ; break; // ouch .. would be better to use R16 but need to swizzle
				case tfh_16bpc_float           : format = tf_A16B16G16R16F; break; // ouch .. would be better to use R16F but need to swizzle
				case tfh_32bpc_float           : format = tf_A32B32G32R32F; break; // ouch .. would be better to use R32F but need to swizzle
				}
				break;

			case tfcg_RG: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT1   ; break;
				case tfh_CompressedHighQuality : format = tf_DXN    ; break;
				case tfh_Quantised             : format = tf_G8R8   ; break; // too bad there's no G4R4
				case tfh_8bpc                  : format = tf_G8R8   ; break;
				case tfh_16bpc                 : format = tf_G16R16 ; break;
				case tfh_16bpc_float           : format = tf_G16R16F; break;
				case tfh_32bpc_float           : format = tf_G32R32F; break;
				}
				break;

			case tfcg_RG_B1: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT1         ; break;
				case tfh_CompressedHighQuality : format = tf_DXT1         ; break; // note that this will _not_ be used for high quality normal maps (it should select tfcg_RG instead)
				case tfh_Quantised             : format = tf_R5G6B5       ; break; // can't use G8R8 since that would imply blue=0 ..
				case tfh_8bpc                  : format = tf_A8B8G8R8     ; break; // can't use G8R8 since that would imply blue=0 ..
				case tfh_16bpc                 : format = tf_A16B16G16R16 ; break; // can't use G16R16 since that would imply blue=0 ..
				case tfh_16bpc_float           : format = tf_A16B16G16R16F; break; // can't use G16R16F since that would imply blue=0 ..
				case tfh_32bpc_float           : format = tf_A32B32G32R32F; break; // can't use G32R32F since that would imply blue=0 ..
				}
				break;

			case tfcg_R: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT5A; break; // DXT5A is BC4, which comes through as "R001"
				case tfh_CompressedHighQuality : format = tf_DXT5A; break; // DXT5A is BC4, which comes through as "R001"
				case tfh_Quantised             : format = tf_R8   ; break;
				case tfh_8bpc                  : format = tf_R8   ; break;
				case tfh_16bpc                 : format = tf_R16  ; break;
				case tfh_16bpc_float           : format = tf_R16F ; break;
				case tfh_32bpc_float           : format = tf_R32F ; break;
				}
				break;
			}
		}

		do // special formats (1555, DXT3, DXT1a, etc.)
		{
			if (bAlphaIs1bit)
			{
				if (format == tf_A4R4G4B4)
				{
					format = tf_A1R5G5B5; // better precision in RGB
					break;
				}
			}

			if (bAlphaIs1bit && bAlphaIsBlack && strcmp(swizzle, "RGBA") == 0)
			{
				if (format == tf_DXT3 || format == tf_DXT5)
				{
					format  = tf_DXT1; // equivalent, and uses half the memory
					swizzle = "RGBA*"; // not very elegant way to indicate DXT1 with alpha is being used, so that XGCompressSurface can be passed an appropriate AlphaRef
					break;
				}
			}

			if (m_texFormatHintUseDXT3)
			{
				if (format == tf_DXT5)
				{
					format = tf_DXT3;
					break;
				}
				else if (format == tf_DXT5A && sm_platform == platform::XENON)
				{
					format = tf_DXT3A;
					break;
				}
			}

			if (m_texFormatHintUse555)
			{
				if (format == tf_R5G6B5)
				{
					format  = tf_A1R5G5B5; // force 555 for better monochrome values
					break;
				}
			}
		}
		while (false);
	}

	if (swizzle == NULL)
	{
		swizzle = "RGBA";
	}
	else if (m_texFormatOverrideSwizzle.length() >= 4)
	{
		swizzle = m_texFormatOverrideSwizzle.c_str();
	}

	if (sm_platform == platform::PS3)
	{
		// PS3 has some limitations in the available texture formats:
		//
		// 16bpc       - 1 or 2 channel formats, but not 4 channel
		// 16bpc_float - 2 or 4 channel formats, but not 1 channel (except DEPTH16_FLOAT)
		// 32bpc_float - 1 or 4 channel formats, but not 2 channel

		// also, on ps3 the 16-bit/component formats don't seem to come through properly (i don't know how to fix this)
		// L16 format comes through as '111L', instead of LLL1
		// R16 format comes through as '1R1R', instead of R001
		// G16R16 format comes through as 'GRGR', instead of RG01

		if (format == tf_A16B16G16R16) { format = tf_A16B16G16R16F; } // filtering may be affected ..
		if (format == tf_R16F        ) { format = tf_L16          ; } // note that swizzle is not handled here, might need to be changed
		if (format == tf_G32R32F     ) { format = tf_A32B32G32R32F; } // note that this doubles the size of the texture!

		if (grcImage::IsFormatGreaterThan8BitsPerComponent(GetTextureFormat(format))) // also we have to turn off swizzle for textures with > 8 bits per component to prevent RSX from crashing .. need to handle XYXY/XXXY swizzling
		{
			swizzle = "";
		}
	}

	grcImage::Format imageFormat = GetTextureFormat(format);

	if (imageFormat == grcImage::UNKNOWN)
	{
		imageFormat = grcImage::A8R8G8B8;
		swizzle = "";
	}

	return imageFormat;
}

static void AutoOpenFunc(void* data)
{
	if (data)
	{
		char* str = (char*)data;
		system(str);
		delete[] str;
	}

	//exit(0);
}

static void AutoOpen(const char* str, bool bThreaded = false) // TODO -- wtf, threaded version only seems to work from the debugger?
{
	if (bThreaded)
	{
		const int len = strlen(str);
		char* buffer = rage_new char[len + 1];
		sysMemCpy(buffer, str, len + 1);

		sysIpcCreateThread(AutoOpenFunc, buffer, 1024, PRIO_NORMAL, "AutoOpen");
	}
	else
	{
		system(str);
	}
}

grcImage::Format CTextureConversion::PreConvert(CImage4F** mipChain, int mipCount, const CTextureFormatHelper& helper)
{
	Assert(mipCount > 0);

	grcImage::Format format = ChooseTextureFormat(m_swizzle, helper);
	grcImage::Format formatFlags = format;

	if (m_sRGB)
	{
		if (m_swizzle)
		{
			if (m_swizzle[0] == 'A' || m_swizzle[1] == 'A' || m_swizzle[2] == 'A' ||
				m_swizzle[3] == 'R' || m_swizzle[3] == 'G' || m_swizzle[3] == 'B')
			{
				m_sRGB = false; // can't use sRGB if RGB and ALPHA are sharing channels
			}
		}

		if (format != grcImage::DXT1 &&
			format != grcImage::DXT3 &&
			format != grcImage::DXT5 &&
		//	format != grcImage::R5G6B5 &&
		//	format != grcImage::A1R5G5B5 &&
		//	format != grcImage::A4R4G4B4 &&
			format != grcImage::A8R8G8B8 &&
			format != grcImage::A8B8G8R8) // TODO -- also include BC7
		{
			m_sRGB = false; // can't use sRGB unless format is compatible
		}
	}

	if (m_sRGB || m_sRGB_raw)
	{
		formatFlags = (grcImage::Format)(formatFlags | (u32)grcImage::FORMAT_FLAG_sRGB);
	}

	if (m_linear) // <-- force this to 1 to make all textures linear, useful for GetPixel test (also need to force bLinear = true in grcTextureGCM::Init, etc.)
	{
		formatFlags = (grcImage::Format)(formatFlags | (u32)grcImage::FORMAT_FLAG_LINEAR);
	}

	if (m_sysmem)
	{
		formatFlags = (grcImage::Format)(formatFlags | (u32)grcImage::FORMAT_FLAG_SYSMEM);
	}

	for (int i = 0; i < mipCount; i++)
	{
		const CImage4F& mip = *mipChain[i];

		const int w = mip.mW;
		const int h = mip.mH;

		if (m_texFormatUsage == tfu_NormalMap) // force blue channel to 1.0f for slightly better compression quality, since it will be reconstructed anyway
		{
			for (int y = 0; y < h; y++)
			{
				for (int x = 0; x < w; x++)
				{
					mip.GetPixel(x, y).z = 1.0f;
				}
			}
		}

		ImagePreSwizzle(mip, m_swizzle); // apply preswizzle, immediately before conversion/compression

		if (m_sRGB)
		{
			LinearToGamma(mip, sm_platform);
		}
	}

	return formatFlags;
}

static const char* g_currentImageConvertFilename = NULL;

static void DumpImage(const char* dir, const grcImage* image, bool bSaveDDS, bool bSavePlate)
{
	char name[512] = "";
	strcpy(name, g_currentImageConvertFilename);

	for (char* s = name; *s; s++)
	{
		if (*s == '/' || *s == '\\' || *s == ':' || *s == '.')
		{
			*s = '_';
		}
	}

	char path[512] = "";
	sprintf(path, "%s/%s", dir, name);

	if (bSaveDDS)
	{
		image->SaveDDS(atVarString("%s.dds", path).c_str());
	}

	if (bSavePlate) // also save a 'plate' image showing all mips and alpha channel in PNG format
	{
		const int w = image->GetWidth();
		const int h = image->GetHeight();
		const int pw = w + w/2;
		const int ph = h*2;

		grcImage* plate = grcImage::Create(pw, ph, 1, grcImage::A8R8G8B8, grcImage::STANDARD, 0, 0);

		if (plate)
		{
			sysMemSet(plate->GetBits(), 0, w*h*sizeof(Color32));

			int oi = 0; // offset into plate
			int oj = 0;

			for (const grcImage* mip = image; mip; mip = mip->GetNext())
			{
				const int mw = mip->GetWidth();
				const int mh = mip->GetHeight();

				for (int j = 0; j < mh; j++)
				{
					for (int i = 0; i < mw; i++)
					{
						Color32 c = mip->GetPixelColor32(i, j);
						const int a = c.GetAlpha();
						c.SetAlpha(255);
						const Color32 ca(a, a, a, 255);

						plate->SetPixel(oi + i, oj + j, c.GetColor());
						plate->SetPixel(oi + i, oj + j + h, ca.GetColor());
					}
				}

				if (mip == image)
				{
					oi += mw;
				}
				else
				{
					oj += mh;
				}
			}

			plate->SavePNG(atVarString("%s.png", path).c_str(), 0.0f);
			plate->Release();
		}
	}
}

grcImage* CTextureConversion::Convert(CImage4F** mipChain, int mipCount, grcImage::Format formatFlags, bool bForceContiguousMips, u8 virtualHdMips, u32 texAlignment, bool bIsLdMipChain)
{
	// discard top mips if requested, but we don't want to scale by a non-uniform amount ..
	int topMipDiscard = m_imageTopMipDiscard;

	while ((Min<int>(mipChain[0]->mW, mipChain[0]->mH) >> topMipDiscard) < 4)
	{
		topMipDiscard--;
	}

	const int w = mipChain[0]->mW >> topMipDiscard;
	const int h = mipChain[0]->mH >> topMipDiscard;
	grcImage* image = grcImage::Create(w, h, 1, formatFlags, grcImage::STANDARD, mipCount - 1 - topMipDiscard, 0);
	grcImage* dst = image;

	Assert(image);

	CDXTCompressorParams cp;

	cp.m_hybridErrorMetric   = (CDXTCompressorParams::eCompressorHybridErrorMetric)m_texCompressionHybridErrorMetric;
	cp.m_hybridDebugTintMin  = m_texCompressionHybridDebugTintMin;
	cp.m_hybridDebugTintMax  = m_texCompressionHybridDebugTintMax;
	cp.m_hybridZeroSum       = m_texCompressionHybridZeroSum;
	cp.m_bVerbose            = false;
	cp.m_bDither             = m_texCompressionDither;
	cp.m_bDirtyDXT1AlphaHack = (m_swizzle[strlen(m_swizzle) - 1] == '*');
	cp.m_bNoAlphaPremultiply = false;
	cp.m_bIsNormalMap        = (m_texFormatUsage == tfu_NormalMap || m_texFormatUsage == tfu_VectorMap);
	cp.m_CTX1_from_DXT1      = m_CTX1_from_DXT1; // experimental ..
	cp.m_compressionWeights  = m_texCompressionWeights;

	// pick a compatible compressor
	{
		for (int i = 0; i < m_texCompressors.size(); i++)
		{
			CDXTCompressorParams::eCompressorType ct = CDXTCompressorParams::CT_NONE;

			switch ((int)m_texCompressors[i])
			{
			case tc_DEVIL   : ct = CDXTCompressorParams::CT_DEVIL  ; break;
			case tc_SQUISH  : ct = CDXTCompressorParams::CT_SQUISH ; break;
			case tc_ATI     : ct = CDXTCompressorParams::CT_ATI    ; break;
			case tc_ATI_EXE : ct = CDXTCompressorParams::CT_ATI_EXE; break;
			case tc_STB     : ct = CDXTCompressorParams::CT_STB    ; break;
			case tc_XG      : ct = CDXTCompressorParams::CT_XG     ; break;
			case tc_HYBRID  : ct = CDXTCompressorParams::CT_HYBRID ; break;
			}

			if (cp.SetCompatible(dst->GetFormat(), ct))
			{
				break;
			}
		}

		if (cp.m_compressor == CDXTCompressorParams::CT_NONE)
		{
			do // default compressors
			{
				if (m_texFormatUsage == tfu_ColourMap)
				{
					if (cp.SetCompatible(dst->GetFormat(), CDXTCompressorParams::CT_HYBRID)) { break; }
				}

				if (cp.SetCompatible(dst->GetFormat(), CDXTCompressorParams::CT_ATI)) { break; }
				if (cp.SetCompatible(dst->GetFormat(), CDXTCompressorParams::CT_XG )) { break; }
			}
			while (0);
		}
	}

#if __64BIT
	// for some unknown reason, XGCompressSurface sometimes crashes on x64 builds (see BS#464562)
	// so let's just force ATI for now .. eventually we could make a 32-bit command line app and
	// call it through sytem(), similarly to how CDXTCompressor::Compress_ATI_EXE works currently

	if (cp.SetCompatible(dst->GetFormat(), CDXTCompressorParams::CT_ATI))
	{
		// ok
	}
	else
	{
		// just pray this works ..
		cp.m_compressor = CDXTCompressorParams::CT_ATI;

		//Quitf("failed to set ATI compressor for format '%s'!", grcImage::GetFormatString(dst->GetFormat()));
	}
#endif // __64BIT

	bool bInvertRGB = false; // experimental .. invert RGB before and after compression

	if (m_texCompressionInvertRGB)
	{
		if (dst->GetFormat() == grcImage::DXT1 ||
			dst->GetFormat() == grcImage::DXT3 ||
			dst->GetFormat() == grcImage::DXT5)
		{
			bInvertRGB = true;
		}
	}

	for (int i = topMipDiscard; i < mipCount; i++)
	{
		const CImage4F* src = mipChain[i];

		Assert(src->mW == dst->GetWidth ());
		Assert(src->mH == dst->GetHeight());

		if (bInvertRGB)
		{
			for (int y = 0; y < src->mH; y++)
			{
				for (int x = 0; x < src->mW; x++)
				{
					Vector4& c = src->GetPixel(x, y);

					c.x = 1.0f - c.x;
					c.y = 1.0f - c.y;
					c.z = 1.0f - c.z;
				}
			}
		}

		cp.m_bVerbose = __DEV && (i == topMipDiscard); // verbose on first mip only
		ConvertImage(dst, src, cp);

		if (bInvertRGB)
		{
			for (int y = 0; y < src->mH; y++)
			{
				for (int x = 0; x < src->mW; x++)
				{
					Vector4& c = src->GetPixel(x, y);

					c.x = 1.0f - c.x;
					c.y = 1.0f - c.y;
					c.z = 1.0f - c.z;

					if (((x|y)&3) == 0)
					{
						switch ((int)dst->GetFormat())
						{
						case grcImage::DXT1: reinterpret_cast<DXT::DXT1_BLOCK*>(dst->GetPixelAddr(x, y))->         InvertRGB(); break;
						case grcImage::DXT3: reinterpret_cast<DXT::DXT3_BLOCK*>(dst->GetPixelAddr(x, y))->m_colour.InvertRGB(); break;
						case grcImage::DXT5: reinterpret_cast<DXT::DXT5_BLOCK*>(dst->GetPixelAddr(x, y))->m_colour.InvertRGB(); break;
						}
					}
				}
			}
		}

		dst = dst->GetNext();
	}

	SetImageConstructorInfo(g_currentImageConvertFilename, image, m_swizzle, bForceContiguousMips, virtualHdMips, texAlignment, bIsLdMipChain);

	if (0 && stristr(g_currentImageConvertFilename, "")) // enable this to dump images
	{
		DumpImage(RS_ASSETS "/non_final/dump", image, true, true);
	}

	return image;
}

// ================================================================================================

static bool IsPowerOfTwo(int x)
{
	return x > 0 && ((x - 1)&x) == 0;
}

static int PowerOfTwoRoundDown(int x) // returns 0 if x is 0
{
	int y = 1;

	while (y < x)
	{
		y *= 2;
	}

	if (y > x)
	{
		y /= 2;
	}

	return y;
}

void CTextureConversionParams::FinaliseConversionParams()
{
	// Some validation that we have a platform set before hitting this.
	// Ideally I'd like to remove the platform from CTextureConversionParams
	// and use Ragebuilder::GetPlatform instead but thats for another day.
	Assertf( 0 != sm_platform, "CTextureConversionParams Platform not set!" );
	if ( 0 == sm_platform )
		Errorf( "CTextureConversionParams platform not set.  Internal error." );

	// m_imageDownsampleScale should be a power of 2, but let's enforce it
	if (m_imageDownsampleScale  > 0 && !IsPowerOfTwo(m_imageDownsampleScale )) { m_imageDownsampleScale  = PowerOfTwoRoundDown(m_imageDownsampleScale ); }
	if (m_imageDownsampleScaleX > 0 && !IsPowerOfTwo(m_imageDownsampleScaleX)) { m_imageDownsampleScaleX = PowerOfTwoRoundDown(m_imageDownsampleScaleX); }
	if (m_imageDownsampleScaleY > 0 && !IsPowerOfTwo(m_imageDownsampleScaleY)) { m_imageDownsampleScaleY = PowerOfTwoRoundDown(m_imageDownsampleScaleY); }

	if (m_imageSplitHD || m_imageSplitHD2)
	{
		m_imageTopMipDiscard = false; // TODO -- we should handle this properly
	}

	// Adam said we don't want to reduce the downsampling of HD textures ..
	if (0 && m_imageSplitHD)
	{
		if (m_imageDownsampleScale  >= 2) { m_imageDownsampleScale  /= 2; }
		if (m_imageDownsampleScaleX >= 2) { m_imageDownsampleScaleX /= 2; }
		if (m_imageDownsampleScaleY >= 2) { m_imageDownsampleScaleY /= 2; }
	}

	if (m_imageDownsampleScale  == 0) { m_imageDownsampleScale  = 1; }
	if (m_imageDownsampleScaleX == 0) { m_imageDownsampleScaleX = m_imageDownsampleScale; }
	if (m_imageDownsampleScaleY == 0) { m_imageDownsampleScaleY = m_imageDownsampleScale; }

	if (m_imageDownsampleScaleRel  == 0.0f) { m_imageDownsampleScaleRel  = 1.0f; }
	if (m_imageDownsampleScaleRelX == 0.0f) { m_imageDownsampleScaleRelX = m_imageDownsampleScaleRel; }
	if (m_imageDownsampleScaleRelY == 0.0f) { m_imageDownsampleScaleRelY = m_imageDownsampleScaleRel; }

	const int maxMipSize = (int)grcImage::GetMaxMipSize();
	const int maxMips    = (int)grcImage::GetMaxMipLevels();

	if (maxMipSize > 0)
	{
		if (m_imageMaxSize  > 0) { m_imageMaxSize  = Min<int>(maxMipSize, m_imageMaxSize ); } else { m_imageMaxSize  = maxMipSize; }
		if (m_imageMaxSizeX > 0) { m_imageMaxSizeX = Min<int>(maxMipSize, m_imageMaxSizeX); } else { m_imageMaxSizeX = maxMipSize; }
		if (m_imageMaxSizeY > 0) { m_imageMaxSizeY = Min<int>(maxMipSize, m_imageMaxSizeY); } else { m_imageMaxSizeY = maxMipSize; }
	}

	if (maxMips > 0)
	{
		if (m_imageMaxMips > 0) { m_imageMaxMips = Min<int>(maxMips, m_imageMaxMips); } else { m_imageMaxMips = maxMips; }
	}

	if (m_imageMaxSize  == 0) { m_imageMaxSize  = 8192; }
	if (m_imageMaxSizeX == 0) { m_imageMaxSizeX = m_imageMaxSize; }
	if (m_imageMaxSizeY == 0) { m_imageMaxSizeY = m_imageMaxSize; }
	if (m_imageMaxMips  == 0) { m_imageMaxMips  = 9999; }

	if (sm_platform == platform::XENON) // apply xenon-specific overrides
	{
		if (m_imageMinMinMipSize_XENON != 0) { m_imageMinMinMipSize = m_imageMinMinMipSize_XENON; }
		if (m_imageMinMaxMipSize_XENON != 0) { m_imageMinMaxMipSize = m_imageMinMaxMipSize_XENON; }
	}

	// apply defaults
	{
		const int defaultMinMinMipSize = (sm_platform == platform::XENON ? 32 : 4);
		const int defaultMinMaxMipSize = (sm_platform == platform::XENON ? 32 : 4);

		if (m_imageMinMinMipSize == 0) { m_imageMinMinMipSize = defaultMinMinMipSize; }
		if (m_imageMinMaxMipSize == 0) { m_imageMinMaxMipSize = defaultMinMaxMipSize; }
	}

	if (m_imageMinMaxMipSize == 0)
	{
		m_imageMinMaxMipSize = m_imageMinMinMipSize;
	}
	else if (m_imageMinMinMipSize > m_imageMinMaxMipSize)
	{
		Warningf( "Minimum MIP size was greater than maximum MIP size." );
		m_imageMinMinMipSize = m_imageMinMaxMipSize;
	}

	if (!IsPowerOfTwo(m_imageMinMinMipSize))
	{
		m_imageMinMinMipSize = PowerOfTwoRoundDown(m_imageMinMinMipSize); // warning?
	}

	if (!IsPowerOfTwo(m_imageMinMaxMipSize))
	{
		m_imageMinMaxMipSize = PowerOfTwoRoundDown(m_imageMinMaxMipSize); // warning?
	}

#if MIPMAPS_CANNOT_BE_SMALLER_THAN_4x4
	m_imageMinMinMipSize = Max<int>(4, m_imageMinMinMipSize);
	m_imageMinMaxMipSize = Max<int>(4, m_imageMinMaxMipSize);
#else
	m_imageMinMinMipSize = Max<int>(1, m_imageMinMinMipSize);
	m_imageMinMaxMipSize = Max<int>(1, m_imageMinMaxMipSize);
#endif

	if (m_texCompressionWeights.x == 0.0f &&
		m_texCompressionWeights.y == 0.0f &&
		m_texCompressionWeights.z == 0.0f)
	{
		m_texCompressionWeights = Vector3(1.0f, 1.0f, 1.0f);
	}

	if (m_texCompressionHybridErrorMetric == tchem_Default)
	{
		m_texCompressionHybridErrorMetric = tchem_MaxSumAbs;
	}

	if (m_downsampleFilterAmount.x == 0.0f &&
		m_downsampleFilterAmount.y == 0.0f &&
		m_downsampleFilterAmount.z == 0.0f &&
		m_downsampleFilterAmount.w == 0.0f &&
		m_downsampleFilterCoefficients.size() > 0)
	{
		m_downsampleFilterAmount = Vector4(1.0f, 1.0f, 1.0f, 1.0f);
	}

	if (m_mipFilterAmount.x == 0.0f &&
		m_mipFilterAmount.y == 0.0f &&
		m_mipFilterAmount.z == 0.0f &&
		m_mipFilterAmount.w == 0.0f &&
		m_mipFilterCoefficients.size() > 0)
	{
		m_mipFilterAmount = Vector4(1.0f, 1.0f, 1.0f, 1.0f);
	}

	if (m_mipFilterSharpenAmount.x == 0.0f &&
		m_mipFilterSharpenAmount.y == 0.0f &&
		m_mipFilterSharpenAmount.z == 0.0f &&
		m_mipFilterSharpenAmount.w == 0.0f &&
		m_mipFilterSharpen.size() > 0)
	{
		m_mipFilterSharpenAmount = Vector4(1.0f, 1.0f, 1.0f, 1.0f);
	}

	if (m_mipFilterDesaturateAmount.x == 0.0f &&
		m_mipFilterDesaturateAmount.y == 0.0f &&
		m_mipFilterDesaturateAmount.z == 0.0f &&
		m_mipFilterDesaturate.size() > 0)
	{
		m_mipFilterDesaturateAmount = Vector3(1.0f, 1.0f, 1.0f);
	}

	if (m_alphaCoverageCompensationFactor > 0.0f)
	{
		if (m_alphaCoverageCompensationReference == 0.0f)
		{
			m_alphaCoverageCompensationReference = 0.5f; // default
		}

		if (m_alphaCoverageCompensationIterations == 0)
		{
			m_alphaCoverageCompensationIterations = 10; // default
		}
	}

	if (m_normalMapGlossInAlpha)
	{
		if (m_normalMapGlossSpecularMin == 0.0f)
		{
			m_normalMapGlossSpecularMin = 1.0f;
		}

		if (m_normalMapGlossSpecularMax == 0.0f)
		{
			m_normalMapGlossSpecularMax = 8192.0f;
		}

		if (m_normalMapGlossSpecularPower == 0.0f)
		{
			m_normalMapGlossSpecularPower = m_normalMapGlossSpecularMax;
		}

		if (m_normalMapGlossAlphaExponent == 0.0f)
		{
			if (m_normalMapGlossStoreVarianceAlpha)
			{
				m_normalMapGlossAlphaExponent = 0.5F; // sqrt before compression for better precision near 0
			}
			else
			{
				m_normalMapGlossAlphaExponent = 1.0f;
			}
		}
	}

	if (m_mipFadeThresholdMax.x == 0.0f &&
		m_mipFadeThresholdMax.y == 0.0f &&
		m_mipFadeThresholdMax.z == 0.0f &&
		m_mipFadeThresholdMax.w == 0.0f)
	{
		if (m_mipFadeThresholdMin.x == 0.0f &&
			m_mipFadeThresholdMin.y == 0.0f &&
			m_mipFadeThresholdMin.z == 0.0f &&
			m_mipFadeThresholdMin.w == 0.0f)
		{
			// if neither thresholds are set, set them to +/-FLT_MAX so we never apply a threshold
			m_mipFadeThresholdMin = Vector4(-FLT_MAX, -FLT_MAX, -FLT_MAX, -FLT_MAX);
			m_mipFadeThresholdMax = Vector4(+FLT_MAX, +FLT_MAX, +FLT_MAX, +FLT_MAX);
		}
		else
		{
			m_mipFadeThresholdMax = Vector4(1.0f, 1.0f, 1.0f, 1.0f);
		}
	}

	if (m_finalMultiplier.x == 0.0f &&
		m_finalMultiplier.y == 0.0f &&
		m_finalMultiplier.z == 0.0f &&
		m_finalMultiplier.w == 0.0f)
	{
		m_finalMultiplier = Vector4(1.0f, 1.0f, 1.0f, 1.0f);
	}

	if (sm_platform == platform::PS3   && m_linear_PS3  ) { m_linear = true; }
	if (sm_platform == platform::XENON && m_linear_XENON) { m_linear = true; }

	// we could also fix stupid values that may have come from the tools .. ideally there would be warnings reported, etc.
}

static void BlendImage(CImage4F* imageA, const CImage4F* imageB, float blend) // consider moving this to imageutil.cpp
{
	const int w = imageA->mW;
	const int h = imageA->mH;

	if (w != imageB->mW ||
		h != imageB->mH)
	{
		return;
	}

	if (blend >= 1.0f)
	{
		sysMemCpy(imageA->GetPixelAddr(0, 0), imageB->GetPixelAddr(0, 0), w*h*sizeof(Vector4));
	}
	else if (blend > 0.0f)
	{
		for (int y = 0; y < h; y++)
		{
			for (int x = 0; x < w; x++)
			{
				Vector4      & c0 = imageA->GetPixel(x, y);
				Vector4 const& c1 = imageB->GetPixel(x, y);

				c0 += (c1 - c0)*blend;
			}
		}
	}
}

static void DeleteMipChain(CImage4F**& mipChain, int mipCount) // consider moving this to imageutil.cpp
{
	if (mipChain)
	{
		for (int i = 0; i < mipCount; i++)
		{
			if (mipChain[i])
			{
				delete mipChain[i];
			}
		}

		delete[] mipChain;
	}

	mipChain = NULL;
}

static const char* FindImageFile(const atArray<CTextureConversionImage>& imageFiles, const char* usage) // consider making this a method of CTextureConversion
{
	for (int i = 0; i < imageFiles.size(); i++)
	{
		if (stricmp(imageFiles[i].m_usage.c_str(), usage) == 0)
		{
			return imageFiles[i].m_filename.c_str();
		}
	}

	return NULL;
}

template <typename DXT_BlockType> static void CTextureConversionParams__OptimiseCompressed_GetRange(
	u32& minR, u32& minG, u32& minB, u32& minA,
	u32& maxR, u32& maxG, u32& maxB, u32& maxA,
	const grcImage* image)
{
	const int w = (image->GetWidth () + 3)/4;
	const int h = (image->GetHeight() + 3)/4;

	const DXT_BlockType* blocks = (const DXT_BlockType*)image->GetBits();

	for (int y = 0; y < h; y++)
	{
		for (int x = 0; x < w; x++)
		{
			DXT::ARGB8888 blockPixels[4*4];
			blocks->Decompress(blockPixels);
			blocks++;

			for (int j = 0; j < 4*4; j++)
			{
				const DXT::ARGB8888 p = blockPixels[j];

				minR = Min<u32>(p.r, minR);
				minG = Min<u32>(p.g, minG);
				minB = Min<u32>(p.b, minB);
				minA = Min<u32>(p.a, minA);

				maxR = Max<u32>(p.r, maxR);
				maxG = Max<u32>(p.g, maxG);
				maxB = Max<u32>(p.b, maxB);
				maxA = Max<u32>(p.a, maxA);
			}
		}
	}
}

bool CTextureConversionParams::OptimiseCompressed( grcImage*& image ) const
{
	if (m_doNotOptimise || image->GetType() != grcImage::STANDARD)
	{
		return false;
	}

	if (image->GetFormat() == grcImage::DXT1 ||
		image->GetFormat() == grcImage::DXT3 ||
		image->GetFormat() == grcImage::DXT5)
	{
		const int   origWidth  = image->GetWidth();
		const int   origHeight = image->GetHeight();
		const char* origFormat = grcImage::GetFormatString(image->GetFormat());

		u32 minR = 255, maxR = 0;
		u32 minG = 255, maxG = 0;
		u32 minB = 255, maxB = 0;
		u32 minA = 255, maxA = 0;

		for (grcImage* mip = image; mip; mip = mip->GetNext())
		{
			switch ((int)image->GetFormat())
			{
			case grcImage::DXT1: CTextureConversionParams__OptimiseCompressed_GetRange<DXT::DXT1_BLOCK>(minR, minG, minB, minA, maxR, maxG, maxB, maxA, mip); break;
			case grcImage::DXT3: CTextureConversionParams__OptimiseCompressed_GetRange<DXT::DXT3_BLOCK>(minR, minG, minB, minA, maxR, maxG, maxB, maxA, mip); break;
			case grcImage::DXT5: CTextureConversionParams__OptimiseCompressed_GetRange<DXT::DXT5_BLOCK>(minR, minG, minB, minA, maxR, maxG, maxB, maxA, mip); break;
			}
		}

		bool bHasAlphaBlock          = (image->GetFormat() == grcImage::DXT3 || image->GetFormat() == grcImage::DXT5);
		bool bSupportTextureSwizzle  = (sm_platform == platform::XENON || sm_platform == platform::PS3);
		bool bSupportTexFormatDXT3A  = (sm_platform == platform::XENON);
		// TODO -- support DXT5A as alpha texture on consoles once we have swizzle support
		bool bSupportTexFormatDXT5A  = (sm_platform == platform::XENON);// || sm_platform == platform::DURANGO || sm_platform == platform::ORBIS || sm_platform == platform::WIN64PC);
		bool bConvertToDXT1_alpha_0  = (bHasAlphaBlock && minA == maxA && minA == 0 && bSupportTextureSwizzle);
		bool bConvertToDXT1_alpha_1  = (bHasAlphaBlock && minA == maxA && minA == 255);
		bool bConvertToDXT3A_rgb_000 = false;
		bool bConvertToDXT3A_rgb_111 = false;
		bool bConvertToDXT5A_rgb_000 = false;
		bool bConvertToDXT5A_rgb_111 = false;
		bool bConvertTo4x4           = false;

		if (minR == maxR && minG == maxG && minB == maxB)
		{
			if (minR == 0 && minG == 0 && minB == 0)
			{
				if (image->GetFormat() == grcImage::DXT3 && bSupportTexFormatDXT3A)
				{
					bConvertToDXT3A_rgb_000 = true;
				}
				else if (image->GetFormat() == grcImage::DXT5 && bSupportTexFormatDXT5A)
				{
					bConvertToDXT5A_rgb_000 = true;
				}
			}
			else if (minR == 255 && minG == 255 && minB == 255 && bSupportTextureSwizzle)
			{
				if (image->GetFormat() == grcImage::DXT3 && bSupportTexFormatDXT3A)
				{
					bConvertToDXT3A_rgb_111 = true;
				}
				else if (image->GetFormat() == grcImage::DXT5 && bSupportTexFormatDXT5A)
				{
					bConvertToDXT5A_rgb_111 = true;
				}
			}

			if (minA == maxA && Min<int>(image->GetWidth(), image->GetHeight()) > 4) // no variation
			{
				bConvertTo4x4 = true;
			}
		}

		if (bConvertToDXT1_alpha_0 || bConvertToDXT1_alpha_1)
		{
			grcImage* image2 = NULL;

			if (bConvertTo4x4)
			{
				image2 = grcImage::Create(4, 4, image->GetDepth(), grcImage::DXT1, grcImage::STANDARD, 0, 0);
			}
			else
			{
				image2 = grcImage::Create(image->GetWidth(), image->GetHeight(), image->GetDepth(), grcImage::DXT1, grcImage::STANDARD, image->GetExtraMipCount(), 0);
			}

			for (grcImage* mip1 = image, *mip2 = image2; mip2; mip1 = mip1->GetNext(), mip2 = mip2->GetNext())
			{
				const int w = bConvertTo4x4 ? 1 : (mip1->GetWidth () + 3)/4;
				const int h = bConvertTo4x4 ? 1 : (mip1->GetHeight() + 3)/4;

				DXT::DXT5_BLOCK* src = (DXT::DXT5_BLOCK*)mip1->GetBits();
				DXT::DXT1_BLOCK* dst = (DXT::DXT1_BLOCK*)mip2->GetBits();

				for (int y = 0; y < h; y++)
				{
					for (int x = 0; x < w; x++)
					{
						*(dst++) = (src++)->m_colour;
					}
				}
			}

			image->Release();
			image = image2;

			if (bConvertToDXT1_alpha_0)
			{
				strcpy(image->GetConstructorInfo().m_swizzle, "RGB0");

				if (bConvertTo4x4)
				{
					texconvLog("optimised %dx%d %s to 4x4 DXT1 alpha=0 (no variation)", origWidth, origHeight, origFormat);
				}
				else
				{
					texconvLog("optimised %dx%d %s to DXT1 alpha=0", origWidth, origHeight, origFormat);
				}
			}
			else if (bConvertToDXT1_alpha_1)
			{
				strcpy(image->GetConstructorInfo().m_swizzle, "RGB1");

				if (bConvertTo4x4)
				{
					texconvLog("optimised %dx%d %s to 4x4 DXT1 alpha=1 (no variation)", origWidth, origHeight, origFormat);
				}
				else
				{
					texconvLog("optimised %dx%d %s to DXT1 alpha=1", origWidth, origHeight, origFormat);
				}
			}

			return true;
		}
		else if (bConvertToDXT3A_rgb_000 || bConvertToDXT3A_rgb_111)
		{
			grcImage* image2 = NULL;

			if (bConvertTo4x4)
			{
				image2 = grcImage::Create(4, 4, image->GetDepth(), grcImage::DXT3A, grcImage::STANDARD, 0, 0);
			}
			else
			{
				image2 = grcImage::Create(image->GetWidth(), image->GetHeight(), image->GetDepth(), grcImage::DXT3A, grcImage::STANDARD, image->GetExtraMipCount(), 0);
			}

			for (grcImage* mip1 = image, *mip2 = image2; mip2; mip1 = mip1->GetNext(), mip2 = mip2->GetNext())
			{
				const int w = bConvertTo4x4 ? 1 : (mip1->GetWidth () + 3)/4;
				const int h = bConvertTo4x4 ? 1 : (mip1->GetHeight() + 3)/4;

				DXT::DXT3_BLOCK* src = (DXT::DXT3_BLOCK*)mip1->GetBits();
				DXT::DXT3_ALPHA* dst = (DXT::DXT3_ALPHA*)mip2->GetBits();

				for (int y = 0; y < h; y++)
				{
					for (int x = 0; x < w; x++)
					{
						*(dst++) = (src++)->m_alpha;
					}
				}
			}

			image->Release();
			image = image2;

			if (bConvertToDXT3A_rgb_000)
			{
				strcpy(image->GetConstructorInfo().m_swizzle, "000A");

				if (bConvertTo4x4)
				{
					texconvLog("optimised %dx%d %s to 4x4 DXT3A rgb=000 (no variation)", origWidth, origHeight, origFormat);
				}
				else
				{
					texconvLog("optimised %dx%d %s to DXT3A rgb=000", origWidth, origHeight, origFormat);
				}
			}
			else if (bConvertToDXT3A_rgb_111)
			{
				strcpy(image->GetConstructorInfo().m_swizzle, "111A");

				if (bConvertTo4x4)
				{
					texconvLog("optimised %dx%d %s to 4x4 DXT3A rgb=111 (no variation)", origWidth, origHeight, origFormat);
				}
				else
				{
					texconvLog("optimised %dx%d %s to DXT3A rgb=111", origWidth, origHeight, origFormat);
				}
			}

			return true;
		}
		else if (bConvertToDXT5A_rgb_000 || bConvertToDXT5A_rgb_111)
		{
			grcImage* image2 = NULL;

			if (bConvertTo4x4)
			{
				image2 = grcImage::Create(4, 4, image->GetDepth(), grcImage::DXT5A, grcImage::STANDARD, 0, 0);
			}
			else
			{
				image2 = grcImage::Create(image->GetWidth(), image->GetHeight(), image->GetDepth(), grcImage::DXT5A, grcImage::STANDARD, image->GetExtraMipCount(), 0);
			}

			for (grcImage* mip1 = image, *mip2 = image2; mip2; mip1 = mip1->GetNext(), mip2 = mip2->GetNext())
			{
				const int w = bConvertTo4x4 ? 1 : (mip1->GetWidth () + 3)/4;
				const int h = bConvertTo4x4 ? 1 : (mip1->GetHeight() + 3)/4;

				DXT::DXT5_BLOCK* src = (DXT::DXT5_BLOCK*)mip1->GetBits();
				DXT::DXT5_ALPHA* dst = (DXT::DXT5_ALPHA*)mip2->GetBits();

				for (int y = 0; y < h; y++)
				{
					for (int x = 0; x < w; x++)
					{
						*(dst++) = (src++)->m_alpha;
					}
				}
			}

			image->Release();
			image = image2;

			if (bConvertToDXT5A_rgb_000)
			{
				strcpy(image->GetConstructorInfo().m_swizzle, "000A");

				if (bConvertTo4x4)
				{
					texconvLog("optimised %dx%d %s to 4x4 DXT5A rgb=000 (no variation)", origWidth, origHeight, origFormat);
				}
				else
				{
					texconvLog("optimised %dx%d %s to DXT5A rgb=000", origWidth, origHeight, origFormat);
				}
			}
			else if (bConvertToDXT5A_rgb_111)
			{
				strcpy(image->GetConstructorInfo().m_swizzle, "111A");

				if (bConvertTo4x4)
				{
					texconvLog("optimised %dx%d %s to 4x4 DXT5A rgb=111 (no variation)", origWidth, origHeight, origFormat);
				}
				else
				{
					texconvLog("optimised %dx%d %s to DXT5A rgb=111", origWidth, origHeight, origFormat);
				}
			}

			return true;
		}
		else if (bConvertTo4x4)
		{
			grcImage* image2 = grcImage::Create(4, 4, image->GetDepth(), image->GetFormat(), grcImage::STANDARD, 0, 0);
			const int bitsPerBlock = 4*4*grcImage::GetFormatBitsPerPixel(image->GetFormat());

			sysMemCpy(image2->GetBits(), image->GetBits(), bitsPerBlock/8);

			image->Release();
			image = image2;

			strcpy(image->GetConstructorInfo().m_swizzle, "RGBA");
			texconvLog("optimised %dx%d %s to 4x4 (no variation)", origWidth, origHeight, origFormat);

			return true;
		}
	}

	return false;
}

bool CTextureConversionParams::Process( grcImage::ImageList& pImages, const char* filename )
{
	CTextureConversion* process = (CTextureConversion*)this;
	CTextureFormatHelper helper(filename);

	const  bool bVerbose  = true && __DEV;
	static bool bInitOnce = true;

	if (bInitOnce)
	{
		ilInit();
		bInitOnce = false;
	}

	if (bVerbose)
	{
		texconvLog("processing \"%s\"", filename);
	}

	sysMemStartTemp();

	if ( m_texFormatUsage == tfu_TerrainBlendMap0 )
	{
		if ( process->BuildTerrainBlendMap(pImages[TextureFactory::TEXTURE_ID_CHANNEL_012], m_texFormatUsage, filename, helper) == false )
		{
			sysMemEndTemp();
			return false;
		}

		pImages[TextureFactory::TEXTURE_ID_MIP_CHAIN_DEFAULT]->Release();
		pImages[TextureFactory::TEXTURE_ID_MIP_CHAIN_DEFAULT] = NULL;
	}
	else if ( m_texFormatUsage == tfu_TerrainBlendMap1)
	{
		if ( process->BuildTerrainBlendMap(pImages[TextureFactory::TEXTURE_ID_CHANNEL_345], m_texFormatUsage, filename, helper) == false )
		{
			sysMemEndTemp();
			return false;
		}

		pImages[TextureFactory::TEXTURE_ID_MIP_CHAIN_DEFAULT]->Release();
		pImages[TextureFactory::TEXTURE_ID_MIP_CHAIN_DEFAULT] = NULL;
	}
	else  //Default texture conversion pipeline.
	{
		char      diffuseFilename[RAGE_MAX_PATH] = "";
		grcImage* diffuseImage  = NULL; // color (diffuse) image for distance fields

		char      mipBlendFilename[RAGE_MAX_PATH] = "";
		grcImage* mipBlendImage = NULL;

		if (m_distanceField && m_distanceFieldRange > 0.0f)
		{
			const char* temp = FindImageFile(m_imageFiles, "DIFFUSE");

			if (temp)
			{
				strcpy(diffuseFilename, temp);
			}

			if (strlen(diffuseFilename) != 0 && ASSET.Exists(diffuseFilename, ""))
			{
				grcImage::sm_customLoad = true;
				diffuseImage = CTextureConversionSpecification::LoadImageFormat( filename );
				grcImage::sm_customLoad = false;
			}
		}

		if (m_mipBlendAmountStart != 0.0f ||
			m_mipBlendAmountEnd   != 0.0f)
		{
			const char* temp = FindImageFile(m_imageFiles, "MIPBLEND");

			if (temp)
			{
				strcpy(mipBlendFilename, temp);
			}

			if (strlen(mipBlendFilename) != 0 && ASSET.Exists(mipBlendFilename, ""))
			{
				grcImage::sm_customLoad = true;
				mipBlendImage = CTextureConversionSpecification::LoadImageFormat(mipBlendFilename);
				grcImage::sm_customLoad = false;
			}
		}

		CImage4F** mipChain = NULL;
		int        mipCount = process->BuildMipChain(mipChain, pImages[TextureFactory::TEXTURE_ID_MIP_CHAIN_DEFAULT], diffuseImage, filename);

		if (diffuseImage)
		{
			diffuseImage->Release();
		}

		if (mipCount == 0)
		{
			sysMemEndTemp();
			return false;
		}

		if (mipBlendImage)
		{
			if (mipBlendImage->GetWidth () == pImages[TextureFactory::TEXTURE_ID_MIP_CHAIN_DEFAULT]->GetWidth () &&
				mipBlendImage->GetHeight() == pImages[TextureFactory::TEXTURE_ID_MIP_CHAIN_DEFAULT]->GetHeight())
			{
				CImage4F** mipBlendChain = NULL;
				int        mipBlendCount = process->BuildMipChain(mipBlendChain, mipBlendImage, NULL, mipBlendFilename);

				const int i0 = m_mipBlendIndexStart;
				const int i1 = Max<int>(i0 + 1, m_mipBlendIndexEnd > 0 ? m_mipBlendIndexEnd : (m_mipBlendIndexEnd + mipBlendCount));

				for (int i = i0; i < mipBlendCount && i < mipCount; i++)
				{
					const float blend = m_mipBlendAmountStart + (m_mipBlendAmountEnd - m_mipBlendAmountStart)*Clamp<float>((float)(i - i0)/(float)(i1 - i0), 0.0f, 1.0f);

					if (blend > 0.0f)
					{
						BlendImage(mipChain[i], mipBlendChain[i], blend);
					}
				}

				DeleteMipChain(mipBlendChain, mipBlendCount);
			}
			else
			{
				// mipBlendImage is not the same size!
			}

			mipBlendImage->Release();
		}

		for (int i = mipCount - 1; i >= 0; i--) // postprocess mipmaps in reverse order
		{
			process->PostprocessMip(mipChain, i, mipCount, helper);
		}

		pImages[TextureFactory::TEXTURE_ID_MIP_CHAIN_DEFAULT]->Release();

		// determine if every mip is "flat" (i.e. no variation) and this is not a
		// script_rt texture - if so, throw away all the higher mips. we could do
		// this on the source textures instead of the processed texture, but then
		// we have to do it for all the inputs.
		bool bHasNoVariation = false;

		if (!m_doNotOptimise && mipChain[0]->mW > 4 && mipChain[0]->mH > 4)
		{
			const Vec4V c0 = VECTOR4_TO_VEC4V(mipChain[0]->GetPixel(0, 0));

			bHasNoVariation = true;

			for (int i = 0; i < mipCount && bHasNoVariation; i++)
			{
				const CImage4F* mip = mipChain[i];
				const int w = mip->mW;
				const int h = mip->mH;

				for (int y = 0; y < h && bHasNoVariation; y++)
				{
					for (int x = 0; x < w; x++)
					{
						if (MaxElement(Abs(VECTOR4_TO_VEC4V(mip->GetPixel(x, y)) - c0)).Getf() > 1.0f/256.0f)
						{
							bHasNoVariation = false;
							break;
						}
					}
				}
			}

			if (bHasNoVariation)
			{
				texconvLog("texture has no variation! replacing %dx%d (%d mips) with 4x4 (1 mip)", mipChain[0]->mW, mipChain[0]->mH, mipCount);

				DeleteMipChain(mipChain, mipCount);

				mipCount = 1;
				mipChain = rage_new CImage4F*[mipCount];
				mipChain[0] = rage_new CImage4F(4, 4);

				for (int y = 0; y < mipChain[0]->mH; y++)
				{
					for (int x = 0; x < mipChain[0]->mH; x++)
					{
						mipChain[0]->SetPixel(x, y, RCC_VECTOR4(c0));
					}
				}
			}
		}

		grcImage::Format formatFlags = process->PreConvert(mipChain, mipCount, helper);
		bool bAllowSplitHD = Min<int>(mipChain[0]->mW, mipChain[0]->mH) >= 128 || sm_platform != platform::XENON;
		if (platform::ORBIS || platform::DURANGO)
		{
			grcImage::Format texType = (grcImage::Format)(formatFlags & (u32)grcImage::FORMAT_MASK);
			bAllowSplitHD = (Min<int>(mipChain[0]->mW, mipChain[0]->mH) >= 256 && (texType == grcImage::DXT3 || texType == grcImage::DXT5)) || (Min<int>(mipChain[0]->mW, mipChain[0]->mH) >= 512 && texType == grcImage::DXT1);
		}

		g_currentImageConvertFilename = filename;

		if ( bAllowSplitHD && m_imageSplitHD2 && mipCount > 2 )
		{
			pImages[TextureFactory::TEXTURE_ID_MIP_CHAIN_HIGH] =
				process->Convert(mipChain, mipCount, formatFlags, false, 0, 0, false);
			pImages[TextureFactory::TEXTURE_ID_MIP_CHAIN_DEFAULT] =
				process->Convert(mipChain+2, mipCount-2, formatFlags, false, 0, 0, true);
		}
		else if ( bAllowSplitHD && ( m_imageSplitHD || m_imageSplitHD2 ) && mipCount > 1 )
		{
			const bool bNoMipsInHD = ((sm_platform == platform::XENON || sm_platform == platform::ORBIS || sm_platform == platform::DURANGO) && m_imageSplitHD);
			const bool bForceContiguousMips = bNoMipsInHD;

			u32 alignment = 0;
			if (platform::ORBIS || platform::DURANGO)
				alignment = 64<<10;

			pImages[TextureFactory::TEXTURE_ID_MIP_CHAIN_HIGH] =
				process->Convert(mipChain, bNoMipsInHD ? 1 : mipCount, formatFlags, false, bNoMipsInHD ? mipCount-1 : 0, alignment, false);
			pImages[TextureFactory::TEXTURE_ID_MIP_CHAIN_DEFAULT] =
				process->Convert(mipChain+1, mipCount-1, formatFlags, bForceContiguousMips, 0, alignment, true);
		}
		else
		{
			pImages[TextureFactory::TEXTURE_ID_MIP_CHAIN_DEFAULT] =
				process->Convert(mipChain, mipCount, formatFlags, false, 0, 0, false);
		}

		g_currentImageConvertFilename = NULL;

		DeleteMipChain(mipChain, mipCount);
	}

	sysMemEndTemp();

	return true;
}

} // namespace rage
