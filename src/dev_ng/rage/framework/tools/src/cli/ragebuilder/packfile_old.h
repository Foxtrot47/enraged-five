// 
// file/packfile.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PACKFILE_OLD_H
#define PACKFILE_OLD_H

#include "atl/array.h"
#include "file/device.h"
#include "string/string.h"

namespace rage {

struct fiPackHeader6 {
	u32 m_Magic;			// 'RPF6'
	u32 m_EntryCount;		// number of fiPackEntry structures
	u32 m_MetaDataOffset;	// offset of the metadata (m_EntryCount entries there too, up to end of file
	u32 m_Encrypted;		// nonzero if it's AES-encrypted.  Key is project-specific.
};

// This is the shift amount used by the new large archive support, which allows files up to 16G.
const int fiBulkOffsetShift = 3;

CompileTimeAssert(sizeof(fiPackHeader6)==16);

/*
	We use a full 32bit hash for the name to minimize chance of collision.
	An entry can be a directory, normal file, or resource.

	If the entry is a directory, we need the index of the directory, and count of files in the directory.
	Otherwise we decode two bits to figure out if the file is compressed or not, and if it's a resource or not.
	If the file is normal, we remember its compressed size and uncompressed size.  If the file is a resource,
	it's never externally compressed; just remember its uncompressed size.  

	The maximum size of an rpf is 16 gigabytes.
*/
struct fiPackEntry6 {
	u32 m_NameHash;
	u32 DECLARE_BITFIELD_2(m_ConsumedSize,26,unused_zero,6);				// Probably could use only 24 bits here.
	union {
		u32 words[3];
		struct Any {
			u32 DECLARE_BITFIELD_2(
				opaque_0,31,
				m_IsDir,1);
			u32 DECLARE_BITFIELD_3(
				opaque_1,30,
				m_IsCompressed,1,
				m_IsResource,1);
			u32 opaque_2;
		} any;
		struct Directory {
			u32 DECLARE_BITFIELD_2(
				m_DirectoryIndex,31,	// index of the first entry in the directory
				m_IsDir,1);				// always one
			u32 DECLARE_BITFIELD_3(
				m_DirectoryCount,30,	// count of items in directory
				m_IsCompressed,1,		// always zero
				m_IsResource,1);		// always zero
			u32 m_Pad;
		} directory;
		struct File {
			u32 DECLARE_BITFIELD_2(
				m_FileOffset,31,		// offset within the archive of the file data
				m_IsDir,1);				// always zero
			u32 DECLARE_BITFIELD_3(
				m_UncompressedSize,30,
				m_IsCompressed,1,		// if one, m_CompSize is valid, else m_CompSize is zero
				m_IsResource,1);		// always zero
			u32 m_Pad;
		} file;
		struct Resource {
			u32 DECLARE_BITFIELD_3(
				m_Version,8,			// Least sigificant bits of file version information
				m_ResourceOffset,23,	// offset within the archive of the file data (in 256byte blocks)
				m_IsDir,1);				// always zero
			datResourceInfo m_Info;		//
		} resource;
	} u;

	bool IsDir() const { return u.any.m_IsDir != 0; }

	bool IsFile() const { return u.any.m_IsDir == 0; }

	// This looks strange because resources are never externally compressed; since they are
	// read atomically, they are internally compressed and are always read via pgStreamer.
	bool IsCompressed() const { return u.any.m_IsCompressed && !u.any.m_IsResource; }

	bool IsResource() const { return u.any.m_IsResource; }

	// Returns amount of space consumed on the disk, whether it's compressed or not
	u32 GetConsumedSize() const { return m_ConsumedSize; }

	u32 GetUncompressedSize() const { return !u.any.m_IsDir && !u.any.m_IsResource && u.any.m_IsCompressed? u.file.m_UncompressedSize : m_ConsumedSize; }

	// Returns offset of the file within the archive
	u32 GetFileOffset() const { FastAssert(!u.any.m_IsDir); return u.any.m_IsResource? (u.resource.m_ResourceOffset<<8) : u.file.m_FileOffset; }

	void SetFileOffset(u32 offset) {
		FastAssert(!u.any.m_IsDir);
		FastAssert(offset <= 0x7FFFFFFF);	// we only use 31 unsigned bits for the offset.
		if (u.any.m_IsResource) {
			FastAssert((offset & 255) == 0);
			u.resource.m_ResourceOffset = offset >> 8;
		}
		else
			u.file.m_FileOffset = offset;
	}

	int GetVersion() const { FastAssert(IsResource()); return (int) u.resource.m_Version; }
};

CompileTimeAssert(sizeof(fiPackEntry6) == 20);

struct fiPackMetaData {
	u32 m_NameOffset;
	u32 m_FileTime;			// Seconds since January 1, 2000.
};


struct fiPackfileOldState;


class fiPackfileOld: public fiDevice {
public:
	fiPackfileOld();
	~fiPackfileOld();

	static void SetReadAheadCacheCPU(int cpu);

	enum CacheMode { CACHE_NONE, CACHE_HARDDRIVE, CACHE_MEMORY };
	bool Init(const char *filename);
	bool MountAs(const char *filename);

	// PURPOSE: Returns true if this packfile is initialized, false if UnInit() had been called on it.
	bool IsInitialized() const							{ return m_Entries != NULL; }

	void SetDrmKey(const void * pDrmKey);

	virtual const fiPackEntry6* FindEntry(const char *name) const;

	virtual fiHandle Open(const char *filename,bool readOnly) const;
	virtual fiHandle Create(const char *filename) const;
	virtual int Read(fiHandle handle,void *outBuffer,int bufferSize) const;
	virtual int Write(fiHandle handle,const void *buffer,int bufferSize) const;
	virtual int Seek(fiHandle handle,int offset,fiSeekWhence whence) const;
	virtual u64 Seek64(fiHandle handle,s64 offset,fiSeekWhence whence) const;
	virtual int Size(fiHandle handle) const;
	virtual u64 Size64(fiHandle handle) const;
	virtual int Close(fiHandle handle) const;
	virtual u64 GetFileSize(const char *filename) const;
	virtual u64 GetFileTime(const char *filename) const;
	virtual bool SetFileTime(const char *filename,u64 timestamp) const;
	virtual u32 GetAttributes(const char *filename) const;
	virtual fiHandle OpenBulk(const char *filename,u64 &outBias) const;
	virtual int ReadBulk(fiHandle handle,u64 offset,void *outBuffer,int bufferSize) const;
	virtual int CloseBulk(fiHandle handle) const;

	virtual fiHandle FindFileBegin(const char *directoryName,fiFindData &outData) const;
	virtual bool FindFileNext(fiHandle handle,fiFindData &outData) const;
	virtual int FindFileEnd(fiHandle handle) const;

	virtual const char *GetDebugName() const;
	virtual RootDeviceId GetRootDeviceId(const char * /*name*/) const { return UNKNOWN; }


protected:
	void		FillData(fiFindData &outData,u32 entryIndex) const;
	fiHandle	OpenInternal(const char *lookupName,bool readOnly) const;
	int			ReadInternal(fiPackfileOldState& state,void *outBuffer,int bufferSize) const;
	int			ReadInternalBulk(const fiPackfileOldState& state,u32 relativeOffset,void *outBuffer,int bufferSize) const;
	bool		InitFromZip(const char *filename,bool keepNameHeap);
	bool		InitFromImg(const char *filename);			// not in final builds.

	char *							m_NameHeap;				// nameheap for all filenames in the zipfile.
	fiPackEntry6 *					m_Entries;				// master entry table; entry 0 is the root directory
	fiPackMetaData *				m_MetaData;
	u32								m_EntryCount;
	fiHandle						m_Handle;				// (bulk) handle on the device of the zipfile itself
	u64								m_FileTime;				// file time of the zipfile itself
	u64								m_ArchiveBias;			// for nested zipfiles (sigh)
	u64								m_ArchiveSize;
	const fiDevice *				m_Device;				// device the zipfile itself lives on
	int								m_RelativeOffset;		// amount of incoming pathname to strip off
	char							m_Name[32];				// debug name

	u8								m_OffsetShift;			// 0 for 2G rpf's, 3 for 16G rpf's.
	u32								m_SortKey;				// sort key used to define position on a DVD
	const void *					m_pDrmKey;				// Drmkey used to decode drm content (ps3 only?)
};

};	// namespace rage

#endif
