/**
 * RageBuilder GTA Functions
 *
 * This header automatically configures the correct function linkage for
 * the following functions depending on the RageBuilder_DLL.h header file
 * defined RAGEBUILDER_DLL.
 */

#ifdef __cplusplus
extern "C"
{
#endif

RAGEBUILDER_DLL bool RBLoadDefinitions(const char* filename);
RAGEBUILDER_DLL bool RBLoadPlacements(const char* filename);
RAGEBUILDER_DLL bool RBSaveDefinitions(const char* filename);
RAGEBUILDER_DLL bool RBSavePlacements(const char* filename);
RAGEBUILDER_DLL void RBClearDefinitions(void);
RAGEBUILDER_DLL void RBClearPlacements(void);

RAGEBUILDER_DLL int RBGetNumMilos(void);
RAGEBUILDER_DLL const char* RBGetMiloName(int miloidx);
RAGEBUILDER_DLL int RBGetMiloNumInstances(int miloidx);
RAGEBUILDER_DLL const char* RBGetMiloInstanceName(int miloidx,int instidx);
RAGEBUILDER_DLL int RBGetMiloNumRooms(int miloidx);
RAGEBUILDER_DLL const char* RBGetMiloRoomName(int miloidx,int roomidx);
RAGEBUILDER_DLL int RBGetMiloRoomNumInstances(int miloidx,int roomidx);
RAGEBUILDER_DLL int RBGetMiloRoomInstance(int miloidx,int roomidx,int instidx);

RAGEBUILDER_DLL int RBGetNumDefinitions(void);
RAGEBUILDER_DLL const char* RBGetObjectName(int objectidx);
RAGEBUILDER_DLL const char* RBGetObjectTxdName(int objectidx);
RAGEBUILDER_DLL float RBGetObjectLodDistance(int objectidx);
RAGEBUILDER_DLL int RBGetObjectFlags(int objectidx);
RAGEBUILDER_DLL const char* RBGetDDName(int objectidx);

RAGEBUILDER_DLL int RBGetNumTimeDefinitions(void);
RAGEBUILDER_DLL const char* RBGetTimeObjectName(int objectidx);

RAGEBUILDER_DLL int RBGetNumInstances(void);
RAGEBUILDER_DLL const char* RBGetInstanceName(int instidx);
RAGEBUILDER_DLL const char* RBGetInstanceTransform(int instidx);
RAGEBUILDER_DLL int RBGetInstanceIsLod(int instidx);
RAGEBUILDER_DLL int RBGetInstanceBlocks(int instidx);

RAGEBUILDER_DLL int RBGetNumBlocks(void);
RAGEBUILDER_DLL const char* RBGetBlockName(int blockidx);
RAGEBUILDER_DLL const char* RBGetBlockPoints(int blockidx);
RAGEBUILDER_DLL float RBGetBlockArea(int blockidx);

RAGEBUILDER_DLL int RBGetNumPeds(void);
RAGEBUILDER_DLL const char* RBGetPedName(int pedidx);

RAGEBUILDER_DLL bool RBInfoModelLoad(const char* filename);
RAGEBUILDER_DLL bool RBInfoModelClose(void);
RAGEBUILDER_DLL bool RBInfoFragmentLoad(const char* filename);
RAGEBUILDER_DLL bool RBInfoFragmentClose(void);
RAGEBUILDER_DLL int RBInfoModelNumPolys(void);
RAGEBUILDER_DLL int RBInfoModelNumShaders(void);
RAGEBUILDER_DLL int RBInfoModelNumTextures(void);
RAGEBUILDER_DLL int RBInfoModelNumGeoms(void);
RAGEBUILDER_DLL const char* RBInfoModelShaderName(int index);
RAGEBUILDER_DLL int RBInfoModelShaderDrawBucket(int index);
RAGEBUILDER_DLL const char* RBInfoModelTextureName(int index);
RAGEBUILDER_DLL float RBInfoModelVolume(void);

#ifdef __cplusplus
}
#endif
