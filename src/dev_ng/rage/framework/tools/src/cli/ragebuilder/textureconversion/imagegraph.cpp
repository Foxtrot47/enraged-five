// ========================================================
// imagegraph.cpp
// Copyright (c) 2010 Rockstar North.  All Rights Reserved. 
// ========================================================

// rage includes
#include "vector/vector4.h"

// project includes
#include "../textureconversion/imagegraph.h"

namespace rage {

float* Graph(float* dst, const float* src, int w, int h)
{
	if (h == 0)
	{
		h = w/2;
	}

	for (int x = 0; x < w; x++)
	{
		const float v0 = Clamp<float>(1.0f - src[Max<int>(0, x - 1)], 0.0f, 1.0f - 0.00001f)*(float)h; // [0..h)
		const float v1 = Clamp<float>(1.0f - src[Min<int>(x, w - 1)], 0.0f, 1.0f - 0.00001f)*(float)h; // [0..h)
		const float h0 = Min<float>(v0, v1);
		const float h1 = Max<float>(v0, v1);
		const float h0_floor = floorf(h0);

		const int minY = Clamp<int>((int)floorf(h0), 0, h - 1);
		const int maxY = Clamp<int>((int)floorf(h1), 0, h - 1) + 1;

		float* col = &dst[x]; // pointer to column data (dst)

		for (int y = 0; y < minY; y++) // over
		{
			col[0] = 0.0f;
			col += w;
		}

		if (maxY - minY == 1)
		{
			float coverage = (h1 + h0)/2.0f - h0_floor;

			col[0] = 1.0f - coverage;
			col += w;
		}
		else
		{
			float y0 = h0_floor + 0.0f;
			float y1 = h0_floor + 1.0f;
			float dx = 1.0f/(h1 - h0);
			float x0 = (y0 - h0)*dx;
			float x1 = (y1 - h0)*dx;

			for (int y = minY; y < maxY; y++)
			{
				// compute coverage for pixel (x,y) - "bounds" of pixel is [0..1],[y..y+1]
				// intersect with line from (0,h0) to (1,h1)

				float coverage = 1.0f - (x1 + x0)/2.0f;

				coverage -= Max<float>(0.0f, -x0       )*(h0 - y0)/2.0f;
				coverage -= Max<float>(0.0f,  x1 - 1.0f)*(h1 - y1)/2.0f;

				col[0] = 1.0f - coverage;
				col += w;

				y0 = y1;
				x0 = x1;
				y1 += 1.0f;
				x1 += dx;
			}
		}

		for (int y = maxY; y < h; y++) // under
		{
			col[0] = 1.0f;
			col += w;
		}
	}

	return dst;
}

static __forceinline Vector4 AlphaBlendRGB(const Vector4& fromColour, const Vector4& toColour, float blendAmount = 1.0f)
{
	const float alpha = blendAmount*toColour.w;

	return Vector4(
		fromColour.x + (toColour.x - fromColour.x)*alpha,
		fromColour.y + (toColour.y - fromColour.y)*alpha,
		fromColour.z + (toColour.z - fromColour.z)*alpha,
		fromColour.w
	);
}

static __forceinline void AlphaBlendRGB(Vector4* image, int area, const Vector4& colour, const float* maskImage = NULL)
{
	if (colour.w != 0.0f)
	{
		if (maskImage)
		{
			for (int i = 0; i < area; i++)
			{
				image[i] = AlphaBlendRGB(image[i], colour, maskImage[i]);
			}
		}
		else if (colour.w != 1.0f)
		{
			for (int i = 0; i < area; i++)
			{
				image[i] = AlphaBlendRGB(image[i], colour);
			}
		}
		else // fill
		{
			for (int i = 0; i < area; i++)
			{
				image[i] = colour;
				//image[i].x = colour.x;
				//image[i].y = colour.y;
				//image[i].z = colour.z;
			}
		}
	}
}

static __forceinline void AlphaBlendRGB(Vector4* image, int area, const Vector4& colour0, const Vector4& colour1, const float* maskImage)
{
	if (colour0.w != 0.0f || colour1.w != 0.0f && maskImage)
	{
		for (int i = 0; i < area; i++)
		{
			const Vector4 colourAlpha = colour0 + (colour1 - colour0)*Vector4(maskImage[i]);
			const Vector4 colour(colourAlpha.x, colourAlpha.y, colourAlpha.z, 1.0f);

			image[i] = AlphaBlendRGB(image[i], colour, colourAlpha.w);
		}
	}
}

static __forceinline void AlphaBlendRGB(Vector4* image, int area, const Vector4& colour0, const Vector4& colour1, const float* maskImage, const float* lerpImage)
{
	if (colour0.w != 0.0f || colour1.w != 0.0f && lerpImage && maskImage)
	{
		for (int i = 0; i < area; i++)
		{
			image[i] = AlphaBlendRGB(image[i], colour0 + (colour1 - colour0)*Vector4(lerpImage[i]), maskImage[i]);
		}
	}
}

Vector4* GraphOverlay(
	Vector4*       image,
	int            w,
	int            h,
	const float*   data,
	float          rangeMin,
	float          rangeMax,
	const Vector4& edgeColour,
	const Vector4& blurColour0,
	const Vector4& blurColour1,
	const Vector4& fillColour0,
	const Vector4& fillColour1,
	const Vector4& clearColour,
	bool           bEdgeSharp
	)
{
	const int area = w*h;

	if (image == NULL)
	{
		image = rage_new Vector4[area];

		if (clearColour.w == 0.0f)
		{
			memset(image, 0, area*sizeof(Vector4));
		}
	}

	float* fillImage = rage_new float[area];
	float* blurImage = NULL;
	float* edgeImage = NULL;
	float* temp = const_cast<float*>(data);

	if (rangeMin == 0.0f && rangeMax == 0.0f) // find range
	{
		temp = rage_new float[w];

		rangeMin = data[0];
		rangeMax = data[0];

		for (int x = 1; x < w; x++)
		{
			rangeMin = Min<float>(data[x], rangeMin);
			rangeMax = Max<float>(data[x], rangeMax);
		}

		if (rangeMin != rangeMax && (rangeMin != 0.0f || rangeMax != 1.0f))
		{
			for (int x = 0; x < w; x++)
			{
				temp[x] = (data[x] - rangeMin)/(rangeMax - rangeMin);
			}
		}
	}
	else if (rangeMin != rangeMax && (rangeMin != 0.0f || rangeMax != 1.0f))
	{
		temp = rage_new float[w];

		for (int x = 0; x < w; x++)
		{
			temp[x] = (data[x] - rangeMin)/(rangeMax - rangeMin);
		}
	}

	Graph(fillImage, temp, w, h);

	if (temp != data)
	{
		delete[] temp;
	}

	const bool bNeedsEdge = (edgeColour.w != 0.0f || blurColour0.w != 0.0f || blurColour1.w != 0.0f);

	if (bNeedsEdge)
	{
		edgeImage = rage_new float[area];

		if (bEdgeSharp) // 2x2 (X-filter)
		{
			for (int y = 0; y < h; y++)
			{
				for (int x = 0; x < w; x++)
				{
					const int x0 = Max<int>(0, x - 1);
					const int y0 = Max<int>(0, y - 1);
					const int x1 = x;
					const int y1 = y;

					const float g00 = fillImage[x0 + y0*w];
					const float g10 = fillImage[x1 + y0*w];
					const float g01 = fillImage[x0 + y1*w];
					const float g11 = fillImage[x1 + y1*w];

					const float dx = Abs<float>(g11 - g00);
					const float dy = Abs<float>(g10 - g01);

					edgeImage[x + y*w] = Max<float>(dx, dy);
				}
			}
		}
		else // 3x3 (Sobel filter)
		{
			for (int y = 0; y < h; y++)
			{
				for (int x = 0; x < w; x++)
				{
					const int x0 = Max<int>(0, x - 1);
					const int y0 = Max<int>(0, y - 1);
					const int x1 = x;
					const int y1 = y;
					const int x2 = Min<int>(w - 1, x + 1);
					const int y2 = Min<int>(h - 1, y + 1);

					const float g00 = fillImage[x0 + y0*w];
					const float g10 = fillImage[x1 + y0*w];
					const float g20 = fillImage[x2 + y0*w];
					const float g01 = fillImage[x0 + y1*w];
					const float g21 = fillImage[x2 + y1*w];
					const float g02 = fillImage[x0 + y2*w];
					const float g12 = fillImage[x1 + y2*w];
					const float g22 = fillImage[x2 + y2*w];

					const float dx = Abs<float>((g20 + 2.0f*g21 + g22 - g00 - 2.0f*g01 - g02)/4.0f);
					const float dy = Abs<float>((g02 + 2.0f*g12 + g22 - g00 - 2.0f*g10 - g20)/4.0f);

					edgeImage[x + y*w] = Max<float>(dx, dy);
				}
			}
		}
	}

	if (blurColour0.w != 0.0f || blurColour1.w != 0.0f)
	{
		blurImage = rage_new float[area];
		const int r = 2;

		float filter[2*r + 1][2*r + 1] =
		{
			{0,1,2,1,0},
			{1,2,3,2,1},
			{2,3,4,3,2},
			{1,2,3,2,1},
			{0,1,2,1,0},
		};

		// normalise filter
		{
			float sum = 0.0f;

			for (int yy = -r; yy <= r; yy++)
			{
				for (int xx = -r; xx <= r; xx++)
				{
					sum += Max<float>(0.0f, filter[xx + r][yy + r]);
				}
			}

			if (sum != 0.0f)
			{
				for (int yy = -r; yy <= r; yy++)
				{
					for (int xx = -r; xx <= r; xx++)
					{
						filter[xx + r][yy + r] /= sum;
					}
				}
			}
		}

		for (int y = 0; y < h; y++)
		{
			for (int x = 0; x < w; x++)
			{
				float sum = 0.0f;

				for (int yy = -r; yy <= r; yy++)
				{
					for (int xx = -r; xx <= r; xx++)
					{
						const int x1 = Clamp<int>(x + xx, 0, w - 1);
						const int y1 = Clamp<int>(y + yy, 0, h - 1);

						sum += filter[xx + r][yy + r]*edgeImage[x1 + y1*w];
					}
				}

				blurImage[x + y*w] = sqrtf(sum);
			}
		}
	}

	AlphaBlendRGB(image, area, clearColour);
	AlphaBlendRGB(image, area, fillColour0, fillColour1, fillImage);
	AlphaBlendRGB(image, area, blurColour0, blurColour1, blurImage, fillImage);
	AlphaBlendRGB(image, area, edgeColour, edgeImage);

	if (fillImage) { delete[] fillImage; }
	if (blurImage) { delete[] blurImage; }
	if (edgeImage) { delete[] edgeImage; }

	//if (w >= 16*3 && h > 16)
	//{
	//	for (int y = 0; y < 16; y++)
	//	{
	//		for (int x = 0; x < 16; x++)
	//		{
	//			image[(x + 16*0) + (y + 0)*w] = Vector4(1.0f, 0.0f, 0.0f, 1.0f);
	//			image[(x + 16*1) + (y + 0)*w] = Vector4(0.0f, 1.0f, 0.0f, 1.0f);
	//			image[(x + 16*2) + (y + 0)*w] = Vector4(0.0f, 0.0f, 1.0f, 1.0f);
	//		}
	//	}
	//}

	return image;
}

} // namespace rage
