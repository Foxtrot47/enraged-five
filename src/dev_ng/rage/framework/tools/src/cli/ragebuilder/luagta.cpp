//
// luagta.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "scriptgta.h"
#include "ragebuilder.h"
#include "log.h"
#include "gta.h"
#include "resource.h"

#include "grmodel/geometry.h"
#include "grcore/vertexbuffer.h"
#include "grcore/vertexbuffereditor.h"
#include "rmcore/drawable.h"
#include "fragment/drawable.h"
#include "atl/string.h"

extern "C" {
#include "lua.h"
}

// Rage headers
#include "atl/array.h"
#include "atl/string.h"

using namespace rage;

#include <string>

int LoadDefinitions(lua_State* L)
{
	GetArguments(L, 1, "load_definitions");
	CheckArgument(L, 1, lua_isstring, "load_definitions");

	const char* toload = lua_tostring(L,1);

	RBLoadDefinitions(toload);

	LUA_RETURN_SUCCESS();
}

int LoadPlacements(lua_State* L)
{
	GetArguments(L, 1, "load_placements");
	CheckArgument(L, 1, lua_isstring, "load_placements");

	const char* toload = lua_tostring(L,1);

	RBLoadPlacements(toload);

	LUA_RETURN_SUCCESS();
}

int SaveDefinitions(lua_State* L)
{
	GetArguments(L, 1, "save_definitions");
	CheckArgument(L, 1, lua_isstring, "save_definitions");

	RBSaveDefinitions(lua_tostring(L,1));

	LUA_RETURN_SUCCESS();
}

int SavePlacements(lua_State* L)
{
	GetArguments(L, 1, "save_placements");
	CheckArgument(L, 1, lua_isstring, "save_placements");

	RBSavePlacements(lua_tostring(L,1));

	LUA_RETURN_SUCCESS();
}

int ClearDefinitions(lua_State* L)
{
	RBClearDefinitions();

	LUA_RETURN_SUCCESS();
}

int ClearPlacements(lua_State* L)
{
	RBClearPlacements();

	LUA_RETURN_SUCCESS();
}

int GetNumMilos(lua_State* L)
{
	lua_pushnumber(L, RBGetNumMilos());

	return 1;
}

int GetMiloName(lua_State* L)
{
	GetArguments(L, 1, "get_milo_name");
	CheckArgument(L, 1, lua_isnumber, "get_milo_name");

	lua_pushstring(L, RBGetMiloName((s32)lua_tonumber(L,1) - 1));

	return 1;
}

int GetMiloNumInstances(lua_State* L)
{
	GetArguments(L, 1, "get_milo_num_instances");
	CheckArgument(L, 1, lua_isnumber, "get_milo_num_instances");

	lua_pushnumber(L, RBGetMiloNumInstances((s32)lua_tonumber(L,1) - 1));

	return 1;
}

int GetMiloInstanceName(lua_State* L)
{
	GetArguments(L, 2, "get_milo_instance_name");
	CheckArgument(L, 2, lua_isnumber, "get_milo_instance_name");

	lua_pushstring(L, RBGetMiloInstanceName((s32)lua_tonumber(L,1) - 1,(s32)lua_tonumber(L,2) - 1));

	return 1;
}

int GetMiloNumRooms(lua_State* L)
{
	GetArguments(L, 1, "get_milo_num_rooms");
	CheckArgument(L, 1, lua_isnumber, "get_milo_num_rooms");

	lua_pushnumber(L, RBGetMiloNumRooms((s32)lua_tonumber(L,1) - 1));

	return 1;
}

int GetMiloRoomName(lua_State* L)
{
	GetArguments(L, 2, "get_milo_room_name");
	CheckArgument(L, 2, lua_isnumber, "get_milo_room_name");

	lua_pushstring(L, RBGetMiloRoomName((s32)lua_tonumber(L,1) - 1,(s32)lua_tonumber(L,2) - 1));

	return 1;
}

int GetMiloRoomNumInstances(lua_State* L)
{
	GetArguments(L, 2, "get_milo_room_num_instances");
	CheckArgument(L, 2, lua_isnumber, "get_milo_room_num_instances");

	lua_pushnumber(L, RBGetMiloRoomNumInstances((s32)lua_tonumber(L,1) - 1,(s32)lua_tonumber(L,2) - 1));

	return 1;
}

int GetMiloRoomInstance(lua_State* L)
{
	GetArguments(L, 3, "get_milo_room_instance");
	CheckArgument(L, 3, lua_isnumber, "get_milo_room_instance");

	lua_pushnumber(L, RBGetMiloRoomInstance((s32)lua_tonumber(L,1) - 1,(s32)lua_tonumber(L,2) - 1,(s32)lua_tonumber(L,3) - 1));

	return 1;
}

int GetNumDefinitions(lua_State* L)
{
	lua_pushnumber(L, RBGetNumDefinitions());

	return 1;
}

int GetObjectName(lua_State* L)
{
	GetArguments(L, 1, "get_object_name");
	CheckArgument(L, 1, lua_isnumber, "get_object_name");

	lua_pushstring(L, RBGetObjectName((s32)lua_tonumber(L,1) - 1));

	return 1;
}

int GetObjectTxdName(lua_State* L)
{
	GetArguments(L, 1, "get_object_txdname");
	CheckArgument(L, 1, lua_isnumber, "get_object_txdname");

	lua_pushstring(L, RBGetObjectTxdName((s32)lua_tonumber(L,1) - 1));

	return 1;
}

int GetObjectLodDistance(lua_State* L)
{
	GetArguments(L, 1, "get_object_loddistance");
	CheckArgument(L, 1, lua_isnumber, "get_object_loddistance");

	lua_pushnumber(L, RBGetObjectLodDistance((s32)lua_tonumber(L,1) - 1));

	return 1;
}

int GetObjectFlags(lua_State* L)
{
	GetArguments(L, 1, "get_object_flags");
	CheckArgument(L, 1, lua_isnumber, "get_object_flags");

	lua_pushnumber(L, RBGetObjectFlags((s32)lua_tonumber(L,1) - 1));

	return 1;
}

int GetDDName(lua_State* L)
{
	GetArguments(L, 1, "get_dd_name");
	CheckArgument(L, 1, lua_isnumber, "get_dd_name");

	lua_pushstring(L, RBGetDDName((s32)lua_tonumber(L,1) - 1));

	return 1;
}

int GetNumTimeDefinitions(lua_State* L)
{
	LUA_RETURN(RBGetNumTimeDefinitions());
}

int GetTimeObjectName(lua_State* L)
{
	GetArguments(L, 1, "get_timeobject_name");
	CheckArgument(L, 1, lua_isnumber, "get_timeobject_name");

	lua_pushstring(L, RBGetTimeObjectName((s32)lua_tonumber(L,1)-1));
	
	return 1;
}

int GetNumInstances(lua_State* L)
{
	lua_pushnumber(L, RBGetNumInstances());

	return 1;
}

int GetInstanceName(lua_State* L)
{
	GetArguments(L, 1, "get_instance_name");
	CheckArgument(L, 1, lua_isnumber, "get_instance_name");

	lua_pushstring(L, RBGetInstanceName((s32)lua_tonumber(L,1) - 1));

	return 1;
}

int GetInstanceTransform(lua_State* L)
{
	GetArguments(L, 1, "get_instance_transform");
	CheckArgument(L, 1, lua_isnumber, "get_instance_transform");

	lua_pushstring(L, RBGetInstanceTransform((s32)lua_tonumber(L,1) - 1));

	return 1;
}

int GetInstanceIsLod(lua_State* L)
{
	GetArguments(L, 1, "get_instance_islod");
	CheckArgument(L, 1, lua_isnumber, "get_instance_islod");

	lua_pushboolean(L, RBGetInstanceIsLod((s32)lua_tonumber(L,1) - 1) == 1 ? true : false);

	return 1;
}

int GetInstanceBlock(lua_State* L)
{
	GetArguments(L, 1, "get_instance_block");
	CheckArgument(L, 1, lua_isnumber, "get_instance_block");

	lua_pushnumber(L, RBGetInstanceBlocks((s32)lua_tonumber(L,1) - 1));

	return 1;
}

int GetNumBlocks(lua_State* L)
{
	lua_pushnumber(L, RBGetNumBlocks());

	return 1;
}

int GetBlockName(lua_State* L)
{
	GetArguments(L, 1, "get_block_name");
	CheckArgument(L, 1, lua_isnumber, "get_block_name");

	lua_pushstring(L, RBGetBlockName((s32)lua_tonumber(L,1) - 1));

	return 1;
}

int GetBlockPoints(lua_State* L)
{
	GetArguments(L, 1, "get_block_points");
	CheckArgument(L, 1, lua_isnumber, "get_block_points");

	lua_pushstring(L, RBGetBlockPoints((s32)lua_tonumber(L,1) - 1));

	return 1;
}

int GetBlockArea(lua_State* L)
{
	GetArguments(L, 1, "get_block_area");
	CheckArgument(L, 1, lua_isnumber, "get_block_area");

	lua_pushnumber(L, RBGetBlockArea((s32)lua_tonumber(L,1) - 1));

	return 1;
}

int GetNumPeds(lua_State* L)
{
	lua_pushnumber(L, RBGetNumPeds());

	return 1;
}

int GetPedName(lua_State* L)
{
	GetArguments(L, 1, "get_pedname");
	CheckArgument(L, 1, lua_isnumber, "get_pedname");

	lua_pushstring(L, RBGetPedName((s32)lua_tonumber(L,1)));

	return 1;
}

int InfoModelLoad(lua_State* L)
{
	if(RBInfoModelLoad(lua_tostring(L,1)))
		lua_pushnumber(L, 1);
	else
		lua_pushnumber(L, 0);

	return 1;
}

int InfoModelClose(lua_State* L)
{
	RBInfoModelClose();

	lua_pushnumber(L, 1);

	return 1;
}

int InfoFragmentLoad(lua_State* L)
{
	if(RBInfoFragmentLoad(lua_tostring(L,1)))
		lua_pushnumber(L, 1);
	else
		lua_pushnumber(L, 0);

	return 1;
}

int InfoFragmentClose(lua_State* L)
{
	RBInfoFragmentClose();

	lua_pushnumber(L, 1);

	return 1;
}

int InfoModelNumPolys(lua_State* L)
{
	lua_pushnumber(L, RBInfoModelNumPolys());

	return 1;
}

int InfoModelNumShaders(lua_State* L)
{
	lua_pushnumber(L, RBInfoModelNumShaders());
	return 1;
}

int InfoModelNumTextures(lua_State* L)
{
	lua_pushnumber(L, RBInfoModelNumTextures());
	return 1;
}

int InfoModelNumGeoms(lua_State* L)
{
	lua_pushnumber(L, RBInfoModelNumGeoms());
	return 1;
}

int InfoModelShaderName(lua_State* L)
{
	GetArguments(L, 1, "model_info_shadername");
	CheckArgument(L, 1, lua_isnumber, "model_info_shadername");

	const char* shadername = RBInfoModelShaderName((int)lua_tonumber(L,1) - 1);

	if(shadername == NULL)
	{
		lua_pushstring( L, "undefined" );
		return 0;
	}

	lua_pushstring( L, shadername );
	return 1;
}

int InfoModelShaderDrawBucket(lua_State* L)
{
	GetArguments( L, 1, "model_info_shader_drawbucket" );
	CheckArgument( L, 1, lua_isnumber, "model_info_shader_drawbucket" );

	int retval = RBInfoModelShaderDrawBucket((int)lua_tonumber(L,1) - 1);

	LUA_RETURN(retval);
}

int InfoModelTextureName(lua_State* L)
{
	GetArguments(L, 1, "model_info_texturename");
	CheckArgument(L, 1, lua_isnumber, "model_info_texturename");

	const char* texturename = RBInfoModelTextureName((int)lua_tonumber(L,1) - 1);

	if(texturename == NULL)
	{
		lua_pushstring( L, "undefined" );
		return 0;
	}

	lua_pushstring( L, texturename );
	return 1;
}

int InfoModelVolume(lua_State* L)
{
	lua_pushnumber(L, RBInfoModelVolume());
	return 1;
}

int GetMissingObjects(lua_State* L)
{
	atArray<atString> missingObjects;
	GtaData::m_iplSet.checkAllInstances(GtaData::m_ideSet,missingObjects);

	string_list missinglist;

	int32 i,Count = missingObjects.GetCount();

	for(i=0;i<Count;i++)
	{
		missinglist.push_back( ((const char*)(missingObjects[i])) );
	}

	// return the files as a table to Lua
	lua_newtable(L);

	int iCount = 1;
	for (string_list::iterator it = missinglist.begin(); it != missinglist.end(); ++it)
	{
		lua_pushnumber(L, iCount++ );
		lua_pushstring(L, (*it).c_str() );
		lua_settable(L, -3);
	}

	return 1;
}

int GetUnusedObjects(lua_State* L)
{
	atArray<atString> unusedObjects;
	GtaData::m_iplSet.getUnusedObjects(GtaData::m_ideSet,unusedObjects);

	string_list unusedlist;

	int32 i,Count = unusedObjects.GetCount();

	for(i=0;i<Count;i++)
	{
		unusedlist.push_back( ((const char*)(unusedObjects[i])) );
	}

	// return the files as a table to Lua
	lua_newtable(L);

	int iCount = 1;
	for (string_list::iterator it = unusedlist.begin(); it != unusedlist.end(); ++it)
	{
		lua_pushnumber(L, iCount++ );
		lua_pushstring(L, (*it).c_str() );
		lua_settable(L, -3);
	}

	return 1;
}

int GetUsage(lua_State* L)
{
	atString MapCheck(lua_tostring(L,1));
	atArray<atString> usageList;
	GtaData::m_iplSet.checkUsage(GtaData::m_ideSet,usageList,MapCheck);

	string_list unusedlist;

	int32 i,Count = usageList.GetCount();

	for(i=0;i<Count;i++)
	{
		unusedlist.push_back( ((const char*)(usageList[i])) );
	}

	// return the files as a table to Lua
	lua_newtable(L);

	int iCount = 1;
	for (string_list::iterator it = unusedlist.begin(); it != unusedlist.end(); ++it)
	{
		lua_pushnumber(L, iCount++ );
		lua_pushstring(L, (*it).c_str() );
		lua_settable(L, -3);
	}

	return 1;
}

static atString g_strMsg;

INT_PTR CALLBACK GetLatestDialogProc(HWND hwndDlg,UINT uMsg,WPARAM wParam,LPARAM lParam)
{
	switch(uMsg)
	{
	case WM_INITDIALOG:
		{
			HWND hwndOwner;
			RECT rc, rcDlg, rcOwner;

			if ((hwndOwner = GetParent(hwndDlg)) == NULL)
			{
				hwndOwner = GetDesktopWindow();
			}

			GetWindowRect(hwndOwner, &rcOwner);
			GetWindowRect(hwndDlg, &rcDlg);
			CopyRect(&rc, &rcOwner);

			// Offset the owner and dialog box rectangles so that
			// right and bottom values represent the width and
			// height, and then offset the owner again to discard
			// space taken up by the dialog box.

			OffsetRect(&rcDlg, -rcDlg.left, -rcDlg.top);
			OffsetRect(&rc, -rc.left, -rc.top);
			OffsetRect(&rc, -rcDlg.right, -rcDlg.bottom);

			// The new position is the sum of half the remaining
			// space and the owner's original position.

			SetWindowPos(hwndDlg,
				HWND_TOP,
				rcOwner.left + (rc.right / 2),
				rcOwner.top + (rc.bottom / 2),
				0, 0,          // ignores size arguments
				SWP_NOSIZE);

			SetWindowText(GetDlgItem(hwndDlg,IDC_STC_PRINT),g_strMsg);
		}
		break;
	case WM_COMMAND:

		switch(LOWORD(wParam))
		{
		case IDC_CMD_YES:
			EndDialog(hwndDlg,1);
			return TRUE;
		case IDC_CMD_YESTOALL:
			EndDialog(hwndDlg,2);
			return TRUE;
		case IDC_CMD_NO:
			EndDialog(hwndDlg,3);
			return TRUE;
		case IDC_CMD_NOTOALL:
			EndDialog(hwndDlg,4);
			return TRUE;
		case IDC_CMD_CANCEL:
			EndDialog(hwndDlg,0);
			return TRUE;
		}

		break;
	}

	return FALSE;
}

int GetLatestMessage(lua_State* L)
{
	GetArguments(L, 1, "get_latest_message");
	CheckArgument(L, 1, lua_isstring, "get_latest_message");

	g_strMsg = lua_tostring(L,1);

	s32 RetVal = DialogBox(GetModuleHandle(NULL),MAKEINTRESOURCE(IDD_DLG_GETLATEST),NULL,GetLatestDialogProc);

	lua_pushnumber(L, RetVal);

	return 1;
}

INT_PTR CALLBACK QueryDialogProc(HWND hwndDlg,UINT uMsg,WPARAM wParam,LPARAM lParam)
{
	switch(uMsg)
	{
	case WM_INITDIALOG:
		{
			HWND hwndOwner;
			RECT rc, rcDlg, rcOwner;

			if ((hwndOwner = GetParent(hwndDlg)) == NULL)
			{
				hwndOwner = GetDesktopWindow();
			}

			GetWindowRect(hwndOwner, &rcOwner);
			GetWindowRect(hwndDlg, &rcDlg);
			CopyRect(&rc, &rcOwner);

			// Offset the owner and dialog box rectangles so that
			// right and bottom values represent the width and
			// height, and then offset the owner again to discard
			// space taken up by the dialog box.

			OffsetRect(&rcDlg, -rcDlg.left, -rcDlg.top);
			OffsetRect(&rc, -rc.left, -rc.top);
			OffsetRect(&rc, -rcDlg.right, -rcDlg.bottom);

			// The new position is the sum of half the remaining
			// space and the owner's original position.

			SetWindowPos(hwndDlg,
				HWND_TOP,
				rcOwner.left + (rc.right / 2),
				rcOwner.top + (rc.bottom / 2),
				0, 0,          // ignores size arguments
				SWP_NOSIZE);

			SetWindowText(GetDlgItem(hwndDlg,IDC_STC_PRINT),g_strMsg);
		}
		break;
	case WM_COMMAND:

		switch(LOWORD(wParam))
		{
		case IDC_CMD_YES:
			EndDialog(hwndDlg,1);
			return TRUE;
		case IDC_CMD_NO:
			EndDialog(hwndDlg,2);
			return TRUE;
		case IDC_CMD_CANCEL:
			EndDialog(hwndDlg,0);
			return TRUE;
		}

		break;
	}

	return FALSE;
}


int QueryMessage(lua_State* L)
{
	GetArguments(L, 1, "query_message");
	CheckArgument(L, 1, lua_isstring, "query_message");

	g_strMsg = lua_tostring(L,1);

	s32 RetVal = DialogBox(GetModuleHandle(NULL),MAKEINTRESOURCE(IDD_DLG_QUERY),NULL,QueryDialogProc);

	lua_pushnumber(L, RetVal);

	return 1;
}

static CONSOLE_SCREEN_BUFFER_INFO csbiInitial;
static uint32 m_Size;

int ProgressSizeCallback(lua_State* L)
{
	m_Size = (uint32)lua_tonumber(L,1);
	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE),&csbiInitial);
	return 0;
}

int ProgressPosCallback(lua_State* L)
{
	uint32 pos = (uint32)lua_tonumber(L,1);

	static bool bSwitch = false;

	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),csbiInitial.dwCursorPosition);

	if(bSwitch)
	{
		printf("\\ ");
		bSwitch = false;
	}
	else
	{
		printf("/ ");
		bSwitch = true;
	}

	int iPercent = (int)(((float)pos / (float)(m_Size)) * 100.0f);
	printf("%d%% done",iPercent);

	return true;
}

typedef struct _LuaCommand
{
	const char*		name;
	LuaCommandFn*	command;
	const char*		description;
} LuaCommand;

LuaCommand gs_commandsGTA[] =
{
	{ "load_definitions",			LoadDefinitions,		"Load ide file."},
	{ "load_placements",			LoadPlacements,			"Load ipl file."},
	{ "save_definitions",			SaveDefinitions,		"Save ide file."},
	{ "save_placements",			SavePlacements,			"Save ipl file."},

	{ "clear_definitions",			ClearDefinitions,		"Clear ide definition data" },
	{ "clear_placements",			ClearPlacements,		"Clear ipl placement data" },

	{ "get_num_milos",				GetNumMilos,			"Get the number of milos loaded."},
	{ "get_milo_name",				GetMiloName,			"Get the milo name by index."},
	{ "get_milo_num_instances",		GetMiloNumInstances,	"Get the milo instance count."},
	{ "get_milo_instance_name",		GetMiloInstanceName,	"Get the milo instance name by index."},

	{ "get_milo_num_rooms",			GetMiloNumRooms,		"Get the number of milo rooms in a milo."},
	{ "get_milo_room_name",			GetMiloRoomName,		"Get the name of a milo room."},
	{ "get_milo_room_num_instances",GetMiloRoomNumInstances,"Get the instance count used by a milo room."},
	{ "get_milo_room_instance",		GetMiloRoomInstance,	"Get the instance in a milo room by index."},

	{ "get_num_definitions",		GetNumDefinitions,		"Get the number of definitions loaded."},
	{ "get_object_name",			GetObjectName,			"Get the object name by index."},
	{ "get_object_txdname",			GetObjectTxdName,		"Get the txd name by index."},
	{ "get_object_loddistance",		GetObjectLodDistance,	"Get the txd name by index."},
	{ "get_object_flags",			GetObjectFlags,			"Get the flags by index."},
	{ "get_dd_name",				GetDDName,				"Get the dd name by index."},
	{ "get_num_timedefinitions",	GetNumTimeDefinitions,	"Get the number of time object definitions loaded."},
	{ "get_timeobject_name",		GetTimeObjectName,		"Get the name of a time object definition."},
	{ "get_num_instances",			GetNumInstances,		"Get the number of instances loaded."},
	{ "get_instance_name",			GetInstanceName,		"Get the instance name by index."},
	{ "get_instance_transform",		GetInstanceTransform,	"Get the instance transform."},
	{ "get_instance_islod",			GetInstanceIsLod,		"Get the instance name by index."},
	{ "get_instance_block",			GetInstanceBlock,		"Get the instance name by index."},

	{ "get_num_blocks",				GetNumBlocks,			"Get the number of instances loaded."},
	{ "get_block_name",				GetBlockName,			"Get the block name"},
	{ "get_block_points",			GetBlockPoints,			"Get the block points"},
	{ "get_block_area",				GetBlockArea,			"Get the block area"},

	{ "get_numpeds",				GetNumPeds,				"returns the number of peds currently loaded" },
	{ "get_pedname",				GetPedName,				"returns the nth ped name" },

	{ "progress_size",				ProgressSizeCallback,	"finds any missing objects in the map sections" },
	{ "progress_pos",				ProgressPosCallback,	"finds any unused objects in the map sections" },

	{ "get_missing_objects",		GetMissingObjects,		"finds any missing objects in the map sections" },
	{ "get_unused_objects",			GetUnusedObjects,		"finds any unused objects in the map sections" },
	{ "get_latest_message",			GetLatestMessage,		"show the get latest message" },
	{ "query_message",				QueryMessage,			"show the query message" },
	{ "get_usage",					GetUsage,				"get the object usage" },

	{ "info_model_load",			InfoModelLoad,			"load model directly for info parsing"},
	{ "info_model_close",			InfoModelClose,			"close model"},

	{ "info_fragment_load",			InfoFragmentLoad,		"load model directly for info parsing"},
	{ "info_fragment_close",		InfoFragmentClose,		"close model"},

	{ "info_model_numpolys",		InfoModelNumPolys,		"get the number of polys used by an object"},
	{ "info_model_numshaders",		InfoModelNumShaders,	"get the number of shaders used by an object"},
	{ "info_model_numtextures",		InfoModelNumTextures,	"get the number of textures used by an object"},
	{ "info_model_numgeoms",		InfoModelNumGeoms,		"get the number of geometries for an object"},
	{ "info_model_shadername",		InfoModelShaderName,	"get a shader name string by shader index"},
	{ "info_model_shader_drawbucket", InfoModelShaderDrawBucket, "get a shader draw bucket index by shader index"},
	{ "info_model_texturename",		InfoModelTextureName,	"get a texture name string by texture index"},
	{ "info_model_volume",			InfoModelVolume,		"get the volume of the model"},
};

int gs_numCommandsGTA = sizeof(gs_commandsGTA) / sizeof(gs_commandsGTA[0]);

// register all the tool commands
void RegisterCommandsGta(CLuaCore& lua)
{
	for(int i = 0; i < gs_numCommandsGTA; ++i)
	{
		lua.RegisterCommand(gs_commandsGTA[i].name, gs_commandsGTA[i].command);
	}
}
