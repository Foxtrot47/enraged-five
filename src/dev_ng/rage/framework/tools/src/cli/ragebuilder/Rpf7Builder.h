//
// Rpf7Builder.h
//
// Copyright (C) 1999-2005 Rockstar North.  All Rights Reserved. 
//

#ifndef RPF7BUILDER_H 
#define RPF7BUILDER_H

// Local headers
#include "IRpfBuilder.h"

// RAGE headers
#include "file/packfile_builder.h"

// Builds pack files (.rpf) from raw resources. 
// Also see zip2rpf, which creates pack files from zip files.
class Rpf7Builder : public IRpfBuilder
{
public:
	Rpf7Builder( bool compress = true, bool keepNameHeap = true, int nameShift = 0 );
	~Rpf7Builder( );

	virtual bool Add( const char* source, const char* destination );
	virtual bool Save( fiStream* pack );

private:
	fiRpf7Builder m_Builder;
};

#endif // RPF7BUILDER_H
