#ifndef COMMON_RAGEFSTREAM_H
#define COMMON_RAGEFSTREAM_H

#include "system/xtl.h"
#include "file/stream.h"
#include "libcore/fstream.h"

#include "utils.h"

///////////////////////////////////////////////////////////////////////////////////////////////
class RageFileStream : public WinLib::FileStream
///////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	RageFileStream();
	virtual ~RageFileStream();

	bool isOpen();
	bool open(const char* p_cFilename,const char* p_cOpenFlags);
	void close();

	uint32 seek(uint32 uOffset,int32 seekOrigin);
	uint32 getPos();
	uint32 write(void* p_cWriteFrom,uint32 uiBytes);
	uint32 read(void* p_cReadTo,uint32 uiBytes);
	uint32 getPending();
	uint64 getFileID() { return (uint64)m_hFile; }

	bool createDirectory(const char* p_cDirPath);
	bool isReadOnly(const char* p_cFilePath);
	bool deleteFile(const char* p_cFilePath);
	bool moveFile(const char* p_cFilePathSource,const char* p_cFilePathDest);
	bool getTempFilename(const char* p_cPath,const char* p_cPrefix,uint32 Unique,char* p_cOutput);

	static int32 runCmd(const char* p_cCommand);
	static bool fileExists(const char* p_cFilePath);
	static bool makeWritable(const char* p_cFilePath);
	static bool deleteDirectory(const char* p_cDirPath);
	static bool createFile(const char* p_cFilePath);
	static bool copyFile(const char* p_cSourcePath,const char* p_cTargetPath);
	static int32 getFileSize(const char* p_cFilePath);
	static uint8* readAll(const char* p_cFilename,uint32& r_uiSize);
	static void makeJustDir(char* p_cFilename);

	static uint64 findFirstFile(char* p_cFilename,const char* p_cWildCard = NULL);
	static bool findNextFile(char* p_cFilename,uint64 iHandle);
	static void findEnd(uint64 iHandle);

protected:
	enum OpenState
	{
		IS_CLOSED = 0,
		IS_READING = 1,
		IS_WRITING = 2,
	};

	OpenState m_osCurrent;

	fiHandle		m_hFile;
	const rage::fiDevice* m_pDevice; // using device, otherwise using streams we run out of stream buffers (32)
	static string_list* ms_pFindlist;	
};

#endif //COMMON_RAGEFSTREAM_H