// ========================================================
// imageutil.cpp
// Copyright (c) 2010 Rockstar North.  All Rights Reserved. 
// ========================================================

#include "vector/quaternion.h"

#ifdef DBG
#undef DBG
#endif
#define DBG 0
#if !__64BIT
#define _X86_
#endif
#include "system/xtl.h"
#include "xdk.h"
#pragma warning(push)
#pragma warning(disable: 4668)
#pragma warning(disable: 4062)
#include "d3d9.h"
#pragma warning(pop)
#include "xboxmath.h"
#include "xgraphics.h"

// external
#include "../../../../../base/tools/libs/devil/il.h"
#include "../../../../../base/tools/libs/devil/il_internal.h"
#include "squish/squish.h"
#include "ATI_Compress.h"

// rage includes
#include "atl/string.h"
#include "grcore/image.h"
#include "grcore/image_dxt.h"
#include "math/float16.h"

// project includes
#include "../utils.h"
#include "../textureconversion/imageutil.h"
#include "../textureconversion/imageutil_D3DFMT.inl" // required for A4R4G4B4, etc. (search for "GlueInterface")
#include "../textureconversion/stb_dxt.h"
#include "../textureconversion/textureconversion.h"

namespace rage {

// generic "pixel types"
// =====================
// * ABGR = {R,G,B,A}
// * ARGB = {R,G,B,A}
// * XRGB = {R,G,B,1}
// * RGB  = {R,G,B,1}
// * GR   = {R,G,0,1} // D3D defines this as {R,G,1,1}, consider changing ..
// * R    = {R,0,0,1} // D3D defines this as {R,1,1,1}, consider changing ..
// * A    = {0,0,0,A}
// * L    = {L,L,L,1}
// * AL   = {L,L,L,A}

typedef Float16 f16;

template <typename T> __forceinline T ImagePixelComponent_ConvertTo(const u8 & src);
template <typename T> __forceinline T ImagePixelComponent_ConvertTo(const u16& src);
template <typename T> __forceinline T ImagePixelComponent_ConvertTo(const u32& src);
template <typename T> __forceinline T ImagePixelComponent_ConvertTo(const f16& src);
template <typename T> __forceinline T ImagePixelComponent_ConvertTo(const f32& src);

template <> __forceinline u8 ImagePixelComponent_ConvertTo(const u8 & src) { return src; }
template <> __forceinline u8 ImagePixelComponent_ConvertTo(const u16& src) { return (u8)(src>>8); }
template <> __forceinline u8 ImagePixelComponent_ConvertTo(const u32& src) { return (u8)(src>>24); }
template <> __forceinline u8 ImagePixelComponent_ConvertTo(const f16& src) { return (u8)Clamp<f32>(floorf(src.GetFloat32_FromFloat16()*255.0f + 0.5f), 0.0f, 255.0f); }
template <> __forceinline u8 ImagePixelComponent_ConvertTo(const f32& src) { return (u8)Clamp<f32>(floorf(src                         *255.0f + 0.5f), 0.0f, 255.0f); }

template <> __forceinline u16 ImagePixelComponent_ConvertTo(const u8 & src) { return ((u16)src) | (((u16)src)<<8); }
template <> __forceinline u16 ImagePixelComponent_ConvertTo(const u16& src) { return src; }
template <> __forceinline u16 ImagePixelComponent_ConvertTo(const u32& src) { return (u16)(src>>16); }
template <> __forceinline u16 ImagePixelComponent_ConvertTo(const f16& src) { return (u16)Clamp<f32>(floorf(src.GetFloat32_FromFloat16()*65535.0f + 0.5f), 0.0f, 65535.0f); }
template <> __forceinline u16 ImagePixelComponent_ConvertTo(const f32& src) { return (u16)Clamp<f32>(floorf(src                         *65535.0f + 0.5f), 0.0f, 65535.0f); }

template <> __forceinline u32 ImagePixelComponent_ConvertTo(const u8 & src) { return 0x01010101*(u32)src; }
template <> __forceinline u32 ImagePixelComponent_ConvertTo(const u16& src) { return ((u32)src) | (((u32)src)<<16); }
template <> __forceinline u32 ImagePixelComponent_ConvertTo(const u32& src) { return src; }
template <> __forceinline u32 ImagePixelComponent_ConvertTo(const f16& src) { return (u32)Clamp<f64>(floor((f64)src.GetFloat32_FromFloat16()*4294967295.0 + 0.5), 0.0, 4294967295.0); }
template <> __forceinline u32 ImagePixelComponent_ConvertTo(const f32& src) { return (u32)Clamp<f64>(floor((f64)src                         *4294967295.0 + 0.5), 0.0, 4294967295.0); }

template <> __forceinline f16 ImagePixelComponent_ConvertTo(const u8 & src) { return f16((f32)src/255.0f); }
template <> __forceinline f16 ImagePixelComponent_ConvertTo(const u16& src) { return f16((f32)src/65535.0f); }
template <> __forceinline f16 ImagePixelComponent_ConvertTo(const u32& src) { return f16((f32)((f64)src/4294967295.0)); }
template <> __forceinline f16 ImagePixelComponent_ConvertTo(const f16& src) { return src; }
template <> __forceinline f16 ImagePixelComponent_ConvertTo(const f32& src) { return f16(src); }

template <> __forceinline f32 ImagePixelComponent_ConvertTo(const u8 & src) { return (f32)src/255.0f; }
template <> __forceinline f32 ImagePixelComponent_ConvertTo(const u16& src) { return (f32)src/65535.0f; }
template <> __forceinline f32 ImagePixelComponent_ConvertTo(const u32& src) { return (f32)((f64)src/4294967295.0); }
template <> __forceinline f32 ImagePixelComponent_ConvertTo(const f16& src) { return src.GetFloat32_FromFloat16(); }
template <> __forceinline f32 ImagePixelComponent_ConvertTo(const f32& src) { return src; }

template <typename T> __forceinline T ImagePixelComponent_Default_0();

template <> __forceinline u8  ImagePixelComponent_Default_0() { return 0x00; }
template <> __forceinline u16 ImagePixelComponent_Default_0() { return 0x0000; }
template <> __forceinline u32 ImagePixelComponent_Default_0() { return 0x00000000; }
template <> __forceinline f16 ImagePixelComponent_Default_0() { return f16(u16(Float16::BINARY_0)); }
template <> __forceinline f32 ImagePixelComponent_Default_0() { return 0.0f; }

template <typename T> __forceinline T ImagePixelComponent_Default_1();

template <> __forceinline u8  ImagePixelComponent_Default_1() { return 0xff; }
template <> __forceinline u16 ImagePixelComponent_Default_1() { return 0xffff; }
template <> __forceinline u32 ImagePixelComponent_Default_1() { return 0xffffffff; }
template <> __forceinline f16 ImagePixelComponent_Default_1() { return f16(u16(Float16::BINARY_1)); }
template <> __forceinline f32 ImagePixelComponent_Default_1() { return 1.0f; }

template <typename T> class CImagePixelType_RGB ;
template <typename T> class CImagePixelType_ARGB;
template <typename T> class CImagePixelType_ABGR;
template <typename T> class CImagePixelType_GR  ;
template <typename T> class CImagePixelType_R   ;
template <typename T> class CImagePixelType_A   ;
template <typename T> class CImagePixelType_L   ;
template <typename T> class CImagePixelType_AL  ;

template <typename T> class CImagePixelType_RGB
{
public:
	typedef typename T T;

	T b;
	T g;
	T r;

	__forceinline void SetRGBA(T R, T G, T B, T A) { r = R; g = G; b = B; }
	__forceinline void SetRGB (T R, T G, T B     ) { r = R; g = G; b = B; }
	__forceinline void SetA   (T A               ) { r =    g =    b = ImagePixelComponent_Default_0<T>(); }
	__forceinline void SetL   (T L               ) { r = L; g = L; b = L; }
	__forceinline void SetAL  (T A, T L          ) { r = L; g = L; b = L; }

	template <typename DstPixelType> __forceinline DstPixelType ConvertTo() const
	{
		DstPixelType dst; dst.SetRGB
		(
			ImagePixelComponent_ConvertTo<DstPixelType::T>(r),
			ImagePixelComponent_ConvertTo<DstPixelType::T>(g),
			ImagePixelComponent_ConvertTo<DstPixelType::T>(b)
		);
		return dst;
	}
};

template <typename T> class CImagePixelType_ARGB
{
public:
	typedef typename T T;

	T b;
	T g;
	T r;
	T a;

	__forceinline void SetRGBA(T R, T G, T B, T A) { r = R; g = G; b = B; a = A; }
	__forceinline void SetRGB (T R, T G, T B     ) { r = R; g = G; b = B; a = ImagePixelComponent_Default_1<T>(); }
	__forceinline void SetA   (T A               ) { r =    g =    b =        ImagePixelComponent_Default_0<T>(); a = A; }
	__forceinline void SetL   (T L               ) { r = L; g = L; b = L; a = ImagePixelComponent_Default_1<T>(); }
	__forceinline void SetAL  (T A, T L          ) { r = L; g = L; b = L; a = A; }

	template <typename DstPixelType> __forceinline DstPixelType ConvertTo() const
	{
		DstPixelType dst; dst.SetRGBA
		(
			ImagePixelComponent_ConvertTo<DstPixelType::T>(r),
			ImagePixelComponent_ConvertTo<DstPixelType::T>(g),
			ImagePixelComponent_ConvertTo<DstPixelType::T>(b),
			ImagePixelComponent_ConvertTo<DstPixelType::T>(a)
		);
		return dst;
	}
};

template <typename T> class CImagePixelType_ABGR
{
public:
	typedef typename T T;

	T r;
	T g;
	T b;
	T a;

	__forceinline void SetRGBA(T R, T G, T B, T A) { r = R; g = G; b = B; a = A; }
	__forceinline void SetRGB (T R, T G, T B     ) { r = R; g = G; b = B; a = ImagePixelComponent_Default_1<T>(); }
	__forceinline void SetA   (T A               ) { r =    g =    b =        ImagePixelComponent_Default_0<T>(); a = A; }
	__forceinline void SetL   (T L               ) { r = L; g = L; b = L; a = ImagePixelComponent_Default_1<T>(); }
	__forceinline void SetAL  (T A, T L          ) { r = L; g = L; b = L; a = A; }

	template <typename DstPixelType> __forceinline DstPixelType ConvertTo() const
	{
		DstPixelType dst; dst.SetRGBA
		(
			ImagePixelComponent_ConvertTo<DstPixelType::T>(r),
			ImagePixelComponent_ConvertTo<DstPixelType::T>(g),
			ImagePixelComponent_ConvertTo<DstPixelType::T>(b),
			ImagePixelComponent_ConvertTo<DstPixelType::T>(a)
		);
		return dst;
	}
};

template <typename T> class CImagePixelType_GR
{
public:
	typedef typename T T;

	T r;
	T g;

	__forceinline void SetRGBA(T R, T G, T B, T A) { r = R; g = G; }
	__forceinline void SetRGB (T R, T G, T B     ) { r = R; g = G; }
	__forceinline void SetA   (T A               ) { r =    g = ImagePixelComponent_Default_0<T>(); }
	__forceinline void SetL   (T L               ) { r = L; g = L; }
	__forceinline void SetAL  (T A, T L          ) { r = L; g = L; }

	template <typename DstPixelType> __forceinline DstPixelType ConvertTo() const
	{
		DstPixelType dst; dst.SetRGB
		(
			ImagePixelComponent_ConvertTo<DstPixelType::T>(r),
			ImagePixelComponent_ConvertTo<DstPixelType::T>(g),
			ImagePixelComponent_Default_0<DstPixelType::T>()
		);
		return dst;
	}
};

template <typename T> class CImagePixelType_R
{
public:
	typedef typename T T;

	T r;

	__forceinline void SetRGBA(T R, T G, T B, T A) { r = R; }
	__forceinline void SetRGB (T R, T G, T B     ) { r = R; }
	__forceinline void SetA   (T A               ) { r = ImagePixelComponent_Default_0<T>(); }
	__forceinline void SetL   (T L               ) { r = L; }
	__forceinline void SetAL  (T A, T L          ) { r = L; }

	template <typename DstPixelType> __forceinline DstPixelType ConvertTo() const
	{
		DstPixelType dst; dst.SetRGB
		(
			ImagePixelComponent_ConvertTo<DstPixelType::T>(r),
			ImagePixelComponent_Default_0<DstPixelType::T>(),
			ImagePixelComponent_Default_0<DstPixelType::T>()
		);
		return dst;
	}
};

template <typename T> class CImagePixelType_A
{
public:
	typedef typename T T;

	T a;

	__forceinline void SetRGBA(T R, T G, T B, T A) { a = A; }
	__forceinline void SetRGB (T R, T G, T B     ) { a = ImagePixelComponent_Default_1<T>(); }
	__forceinline void SetA   (T A               ) { a = A; }
	__forceinline void SetL   (T L               ) { a = ImagePixelComponent_Default_1<T>(); }
	__forceinline void SetAL  (T A, T L          ) { a = A; }

	template <typename DstPixelType> __forceinline DstPixelType ConvertTo() const
	{
		DstPixelType dst; dst.SetA
		(
			ImagePixelComponent_ConvertTo<DstPixelType::T>(a)
		);
		return dst;
	}
};

template <typename T> class CImagePixelType_L
{
public:
	typedef typename T T;

	T l;

	__forceinline void SetRGBA(T R, T G, T B, T A) { l = Max<T>(R, G, B); } // should be avg(R,G,B) or max(R,G,B)?
	__forceinline void SetRGB (T R, T G, T B     ) { l = R; }
	__forceinline void SetA   (T A               ) { l = ImagePixelComponent_Default_0<T>(); }
	__forceinline void SetL   (T L               ) { l = L; }
	__forceinline void SetAL  (T A, T L          ) { l = L; }

	template <typename DstPixelType> __forceinline DstPixelType ConvertTo() const
	{
		DstPixelType dst; dst.SetL
		(
			ImagePixelComponent_ConvertTo<DstPixelType::T>(l)
		);
		return dst;
	}
};

template <typename T> class CImagePixelType_AL
{
public:
	typedef typename T T;

	T l;
	T a;

	__forceinline void SetRGBA(T R, T G, T B, T A) { l = Max<T>(R, G); a = A; } // should be avg(R,G,B) or max(R,G,B)?
	__forceinline void SetRGB (T R, T G, T B     ) { l = R; a = ImagePixelComponent_Default_1<T>(); }
	__forceinline void SetA   (T A               ) { l =        ImagePixelComponent_Default_0<T>(); a = A; }
	__forceinline void SetL   (T L               ) { l = L; a = ImagePixelComponent_Default_1<T>(); }
	__forceinline void SetAL  (T A, T L          ) { l = L; a = A; }

	template <typename DstPixelType> __forceinline DstPixelType ConvertTo() const
	{
		DstPixelType dst; dst.SetAL
		(
			ImagePixelComponent_ConvertTo<DstPixelType::T>(a),
			ImagePixelComponent_ConvertTo<DstPixelType::T>(l)
		);
		return dst;
	}
};

typedef CImagePixelType_RGB<u8> CImagePixelType_R8G8B8;

typedef CImagePixelType_ARGB<u8> CImagePixelType_A8R8G8B8;

typedef CImagePixelType_ABGR<u8 > CImagePixelType_A8B8G8R8;
typedef CImagePixelType_ABGR<u16> CImagePixelType_A16B16G16R16;
typedef CImagePixelType_ABGR<f16> CImagePixelType_A16B16G16R16F;
typedef CImagePixelType_ABGR<f32> CImagePixelType_A32B32G32R32F;

typedef CImagePixelType_GR<u8 > CImagePixelType_G8R8;
typedef CImagePixelType_GR<u16> CImagePixelType_G16R16;
typedef CImagePixelType_GR<f16> CImagePixelType_G16R16F;
typedef CImagePixelType_GR<f32> CImagePixelType_G32R32F;

typedef CImagePixelType_R<u8 > CImagePixelType_R8;
typedef CImagePixelType_R<u16> CImagePixelType_R16;
typedef CImagePixelType_R<f16> CImagePixelType_R16F;
typedef CImagePixelType_R<f32> CImagePixelType_R32F;

typedef CImagePixelType_A<u8 > CImagePixelType_A8;
typedef CImagePixelType_A<u16> CImagePixelType_A16;
typedef CImagePixelType_A<f16> CImagePixelType_A16F;
typedef CImagePixelType_A<f32> CImagePixelType_A32F;

typedef CImagePixelType_L<u8 > CImagePixelType_L8;
typedef CImagePixelType_L<u16> CImagePixelType_L16;
typedef CImagePixelType_L<f16> CImagePixelType_L16F;
typedef CImagePixelType_L<f32> CImagePixelType_L32F;

typedef CImagePixelType_AL<u8 > CImagePixelType_A8L8;
typedef CImagePixelType_AL<u16> CImagePixelType_A16L16;
typedef CImagePixelType_AL<f16> CImagePixelType_A16L16F;
typedef CImagePixelType_AL<f32> CImagePixelType_A32L32F;

// this is pretty messed up .. seriously this is some convoluted shit
template <DDS_D3DFORMAT fmt> class CImagePixelType_GlueInterface : public CPixel_D3DFMT<fmt>
{
protected:
	typedef typename CPixel_D3DFMT<fmt>::T ComponentType;

public:
	__forceinline void SetRGBA(
		ComponentType r_,
		ComponentType g_,
		ComponentType b_,
		ComponentType a_
		)
	{
		SetV(Vector4//SetRGBAComponents__NOT_WORKING_WTF
		(
			ImagePixelComponent_ConvertTo<f32>(r_),
			ImagePixelComponent_ConvertTo<f32>(g_),
			ImagePixelComponent_ConvertTo<f32>(b_),
			ImagePixelComponent_ConvertTo<f32>(a_)
		));
	}
};

template <DDS_D3DFORMAT fmt> class CImagePixelType_GlueInterfaceRGBA;
template <DDS_D3DFORMAT fmt> class CImagePixelType_GlueInterfaceRGB ;
template <DDS_D3DFORMAT fmt> class CImagePixelType_GlueInterfaceA   ;
template <DDS_D3DFORMAT fmt> class CImagePixelType_GlueInterfaceL   ;
template <DDS_D3DFORMAT fmt> class CImagePixelType_GlueInterfaceAL  ;

template <DDS_D3DFORMAT fmt> class CImagePixelType_GlueInterfaceRGBA : public CImagePixelType_GlueInterface<fmt>
{
public:
	template <typename DstPixelType> __forceinline DstPixelType ConvertTo() const // RGBA
	{
		DstPixelType dst; dst.SetRGBA
		(
			ImagePixelComponent_ConvertTo<DstPixelType::T>((float)r/(float)((1<<CPixel_D3DFMT<fmt>::R) - 1)),
			ImagePixelComponent_ConvertTo<DstPixelType::T>((float)g/(float)((1<<CPixel_D3DFMT<fmt>::G) - 1)),
			ImagePixelComponent_ConvertTo<DstPixelType::T>((float)b/(float)((1<<CPixel_D3DFMT<fmt>::B) - 1)),
			ImagePixelComponent_ConvertTo<DstPixelType::T>((float)a/(float)((1<<CPixel_D3DFMT<fmt>::A) - 1))
		);
		return dst;
	}
};

template <DDS_D3DFORMAT fmt> class CImagePixelType_GlueInterfaceRGB : public CImagePixelType_GlueInterface<fmt>
{
public:
	template <typename DstPixelType> __forceinline DstPixelType ConvertTo() const // RGB
	{
		DstPixelType dst; dst.SetRGB
		(
			ImagePixelComponent_ConvertTo<DstPixelType::T>((float)r/(float)((1<<CPixel_D3DFMT<fmt>::R) - 1)),
			ImagePixelComponent_ConvertTo<DstPixelType::T>((float)g/(float)((1<<CPixel_D3DFMT<fmt>::G) - 1)),
			ImagePixelComponent_ConvertTo<DstPixelType::T>((float)b/(float)((1<<CPixel_D3DFMT<fmt>::B) - 1))
		);
		return dst;
	}
};

template <DDS_D3DFORMAT fmt> class CImagePixelType_GlueInterfaceA : public CImagePixelType_GlueInterface<fmt>
{
public:
	template <typename DstPixelType> __forceinline DstPixelType ConvertTo() const // A
	{
		DstPixelType dst; dst.SetA
		(
			ImagePixelComponent_ConvertTo<DstPixelType::T>((float)a/(float)((1<<CPixel_D3DFMT<fmt>::A) - 1))
		);
		return dst;
	}
};

template <DDS_D3DFORMAT fmt> class CImagePixelType_GlueInterfaceL : public CImagePixelType_GlueInterface<fmt>
{
public:
	template <typename DstPixelType> __forceinline DstPixelType ConvertTo() const // L
	{
		DstPixelType dst; dst.SetL
		(
			ImagePixelComponent_ConvertTo<DstPixelType::T>((float)l/(float)((1<<CPixel_D3DFMT<fmt>::L) - 1))
		);
		return dst;
	}
};

template <DDS_D3DFORMAT fmt> class CImagePixelType_GlueInterfaceAL : public CImagePixelType_GlueInterface<fmt>
{
public:
	template <typename DstPixelType> __forceinline DstPixelType ConvertTo() const // AL
	{
		DstPixelType dst; dst.SetAL
		(
			ImagePixelComponent_ConvertTo<DstPixelType::T>((float)a/(float)((1<<CPixel_D3DFMT<fmt>::A) - 1)),
			ImagePixelComponent_ConvertTo<DstPixelType::T>((float)l/(float)((1<<CPixel_D3DFMT<fmt>::L) - 1))
		);
		return dst;
	}
};

class CImagePixelType_A4R4G4B4    : public CImagePixelType_GlueInterfaceRGBA<DDS_D3DFMT_A4R4G4B4   > {};
class CImagePixelType_A1R5G5B5    : public CImagePixelType_GlueInterfaceRGBA<DDS_D3DFMT_A1R5G5B5   > {};
class CImagePixelType_R5G6B5      : public CImagePixelType_GlueInterfaceRGB <DDS_D3DFMT_R5G6B5     > {};
class CImagePixelType_R3G3B2      : public CImagePixelType_GlueInterfaceRGB <DDS_D3DFMT_R3G3B2     > {};
class CImagePixelType_A8R3G3B2    : public CImagePixelType_GlueInterfaceRGBA<DDS_D3DFMT_A8R3G3B2   > {};
class CImagePixelType_A4L4        : public CImagePixelType_GlueInterfaceAL  <DDS_D3DFMT_A4L4       > {};
class CImagePixelType_A2R10G10B10 : public CImagePixelType_GlueInterfaceRGBA<DDS_D3DFMT_A2R10G10B10> {};
class CImagePixelType_A2B10G10R10 : public CImagePixelType_GlueInterfaceRGBA<DDS_D3DFMT_A2B10G10R10> {};

// etc.

template <typename T> class CImagePixelType {};

template <> class CImagePixelType<Color32> { public: typedef CImagePixelType_A8R8G8B8      T; };
template <> class CImagePixelType<Vector4> { public: typedef CImagePixelType_A32B32G32R32F T; };
template <> class CImagePixelType<float  > { public: typedef CImagePixelType_L32F          T; };

// ================================================================================================

bool IsSupportedImageImportFormat(const grcImage* image)
{
	if (image == NULL)
	{
		return false;
	}

	const grcImage::Format imageFormat = image->GetFormat();
	const int w = image->GetWidth ();
	const int h = image->GetHeight();

	const bool bSupportedFormat =
	(
		0
		#define DEF_SUPPORTED_IMAGE_IMPORT_FORMAT(f) || imageFormat == grcImage::f
		FOREACH(DEF_SUPPORTED_IMAGE_IMPORT_FORMAT)
		#undef  DEF_SUPPORTED_IMAGE_IMPORT_FORMAT
	);
	const bool bSupportedType = // cubemaps and volume textures not supported yet
	(
		image->GetType() == grcImage::STANDARD
	);
	const bool bSupportedSize =
	(
		IsPow2<int>(w) && w >= 4 && w <= 4096 &&
		IsPow2<int>(h) && h >= 4 && h <= 4096
	);

	return bSupportedFormat && bSupportedType && bSupportedSize;
}

bool IsSupportedImageExportFormat(const grcImage* image, bool bHandleCompressedFormats)
{
	if (image == NULL)
	{
		return false;
	}

	const grcImage::Format imageFormat = image->GetFormat();
	const int w = image->GetWidth ();
	const int h = image->GetHeight();

	const bool bSupportedCompressedFormat = bHandleCompressedFormats &&
	(
		0
		#define DEF_SUPPORTED_IMAGE_EXPORT_COMPRESSED_FORMAT(f) || imageFormat == grcImage::f
		FOREACH(DEF_SUPPORTED_IMAGE_EXPORT_COMPRESSED_FORMAT)
		#undef  DEF_SUPPORTED_IMAGE_EXPORT_COMPRESSED_FORMAT
	);
	const bool bSupportedFormat = bSupportedCompressedFormat ||
	(
		0
		#define DEF_SUPPORTED_IMAGE_EXPORT_FORMAT(f) || imageFormat == grcImage::f
		FOREACH(DEF_SUPPORTED_IMAGE_EXPORT_FORMAT)
		#undef  DEF_SUPPORTED_IMAGE_EXPORT_FORMAT
	);
	const bool bSupportedType = // cubemaps and volume textures not supported yet
	(
		image->GetType() == grcImage::STANDARD
	);
	const bool bSupportedSize =
	(
		IsPow2<int>(w) && w >= 4 && w <= 4096 &&
		IsPow2<int>(h) && h >= 4 && h <= 4096
	);

	return bSupportedFormat && bSupportedType && bSupportedSize;
}

template <typename T> __COMMENT(static) CImage<T>* CImage<T>::Load(const char* path)
{
	sysMemStartTemp();

	grcImage* src = grcImage::Load(path);
	CImage<T>* image = NULL;

	if (src)
	{
		image = Load(src);
		src->Release();
	}

	sysMemEndTemp();

	return image;
}

template <typename T> __COMMENT(static) CImage<T>* CImage<T>::Load(const grcImage* src)
{
	sysMemStartTemp();

	CImage<T>* image = NULL;

	if (IsSupportedImageImportFormat(src))
	{
		const int w = src->GetWidth ();
		const int h = src->GetHeight();

		image = rage_new CImage<T>(w, h);

		if (image->IsValid() && image->ConvertFrom(src))
		{
			// ok
		}
		else// wasn't valid?
		{
			delete image;
			image = NULL;
		}
	}

	sysMemEndTemp();

	return image;
}

#define CONVERT_FROM_IMAGE_FORMAT(DstPixelType,dst,srcFormat,src) \
	{ \
		typedef CImagePixelType_##srcFormat SrcPixelType; \
		\
		if (src->GetFormat() == grcImage::srcFormat) \
		{ \
			for (int y = 0; y < h; y++) \
			{ \
				DstPixelType* dstRow = reinterpret_cast<DstPixelType*>(dst->GetPixelRowAddr(y)); \
				SrcPixelType* srcRow = reinterpret_cast<SrcPixelType*>(src->GetPixelRowAddr(y)); \
				\
				for (int x = 0; x < w; x++) \
				{ \
					dstRow[x] = srcRow[x].ConvertTo<DstPixelType>(); \
				} \
			} \
			break; \
		} \
	} \
	// end.

#define CONVERT_TO_IMAGE_FORMAT(dstFormat,dst,SrcPixelType,src) \
	{ \
		typedef CImagePixelType_##dstFormat DstPixelType; \
		\
		if (dst->GetFormat() == grcImage::dstFormat) \
		{ \
			for (int y = 0; y < h; y++) \
			{ \
				DstPixelType* dstRow = reinterpret_cast<DstPixelType*>(dst->GetPixelRowAddr(y)); \
				SrcPixelType* srcRow = reinterpret_cast<SrcPixelType*>(src->GetPixelRowAddr(y)); \
				\
				for (int x = 0; x < w; x++) \
				{ \
					dstRow[x] = srcRow[x].ConvertTo<DstPixelType>(); \
				} \
			} \
			break; \
		} \
	} \
	// end.

template <> bool CImage32::ConvertFrom(const grcImage* src)
{
	const int w = src->GetWidth ();
	const int h = src->GetHeight();

	if (IsSupportedImageImportFormat(src) && mW == w && mH == h)
	{
		do
		{
			#define DEF_SUPPORTED_IMAGE_IMPORT_FORMAT(f) CONVERT_FROM_IMAGE_FORMAT(CImagePixelType_A8R8G8B8, this, f, src);
			FOREACH(DEF_SUPPORTED_IMAGE_IMPORT_FORMAT)
			#undef  DEF_SUPPORTED_IMAGE_IMPORT_FORMAT

			Assert(false);
			return false;
		}
		while (false);

		return true;
	}

	Assertf(false, "unsupported image format");
	return false;
}

template <> bool CImage4F::ConvertFrom(const grcImage* src)
{
	const int w = src->GetWidth ();
	const int h = src->GetHeight();

	if (IsSupportedImageImportFormat(src) && mW == w && mH == h)
	{
		do
		{
			#define DEF_SUPPORTED_IMAGE_IMPORT_FORMAT(f) CONVERT_FROM_IMAGE_FORMAT(CImagePixelType_A32B32G32R32F, this, f, src);
			FOREACH(DEF_SUPPORTED_IMAGE_IMPORT_FORMAT)
			#undef  DEF_SUPPORTED_IMAGE_IMPORT_FORMAT

			Assert(false);
			return false;
		}
		while (false);

		return true;
	}

	Assertf(false, "unsupported image format");
	return false;
}

template <> bool CImage1F::ConvertFrom(const grcImage* src)
{
	const int w = src->GetWidth ();
	const int h = src->GetHeight();

	if (IsSupportedImageImportFormat(src) && mW == w && mH == h)
	{
		do
		{
			#define DEF_SUPPORTED_IMAGE_IMPORT_FORMAT(f) CONVERT_FROM_IMAGE_FORMAT(CImagePixelType_L32F, this, f, src);
			FOREACH(DEF_SUPPORTED_IMAGE_IMPORT_FORMAT)
			#undef  DEF_SUPPORTED_IMAGE_IMPORT_FORMAT

			Assert(false);
			return false;
		}
		while (false);

		return true;
	}

	Assertf(false, "unsupported image format");
	return false;
}

template <typename T> template <typename Q> void CImage<T>::ConvertFrom(const CImage<Q>* src) const
{
	typedef CImagePixelType<T>::T DstPixelType;
	typedef CImagePixelType<Q>::T SrcPixelType;

	STATIC_ASSERT(sizeof(DstPixelType) == sizeof(T), dst_pixel_types_do_not_match);
	STATIC_ASSERT(sizeof(SrcPixelType) == sizeof(Q), src_pixel_types_do_not_match);

	const int w = src->mW;
	const int h = src->mH;

	if (w == mW && h == mH)
	{
		for (int y = 0; y < h; y++)
		{
			DstPixelType* dstRow = reinterpret_cast<DstPixelType*>(     GetPixelRowAddr(y));
			SrcPixelType* srcRow = reinterpret_cast<SrcPixelType*>(src->GetPixelRowAddr(y));

			for (int x = 0; x < w; x++)
			{
				dstRow[x] = srcRow[x].ConvertTo<DstPixelType>();
			}
		}
	}
}

// ================================================================================================

class CDXTCompressor : public CDXTCompressorParams
{
public:
	CDXTCompressor(const CDXTCompressorParams& cp) : CDXTCompressorParams(cp) {}

	template <typename Q> bool Compress(grcImage* dst, const CImage<Q>* src) const;

private:
	bool Compress_DEVIL  (grcImage* dst, const void* src, grcImage::Format srcFormat) const;
	bool Compress_SQUISH (grcImage* dst, const void* src, grcImage::Format srcFormat) const;
	bool Compress_ATI    (grcImage* dst, const void* src, grcImage::Format srcFormat) const;
	bool Compress_ATI_EXE(grcImage* dst, const void* src, grcImage::Format srcFormat) const;
	bool Compress_STB    (grcImage* dst, const void* src, grcImage::Format srcFormat) const;
	bool Compress_XG     (grcImage* dst, const void* src, grcImage::Format srcFormat) const;
	bool Compress_HYBRID (grcImage* dst, const void* src, grcImage::Format srcFormat) const;

	static u32 GetLinearD3DFormatForXGCompress(grcImage::Format format);
};

template <> bool CDXTCompressor::Compress(grcImage* dst, const CImage32* src) const
{
	switch ((int)m_compressor)
	{
	case CT_DEVIL   : return Compress_DEVIL  (dst, src->GetPixelAddr(0, 0), grcImage::A8R8G8B8);
	case CT_SQUISH  : return Compress_SQUISH (dst, src->GetPixelAddr(0, 0), grcImage::A8R8G8B8);
	case CT_ATI     : return Compress_ATI    (dst, src->GetPixelAddr(0, 0), grcImage::A8R8G8B8);
	case CT_ATI_EXE : return Compress_ATI_EXE(dst, src->GetPixelAddr(0, 0), grcImage::A8R8G8B8);
	case CT_STB     : return Compress_STB    (dst, src->GetPixelAddr(0, 0), grcImage::A8R8G8B8);
	case CT_XG      : return Compress_XG     (dst, src->GetPixelAddr(0, 0), grcImage::A8R8G8B8);
	case CT_HYBRID  : return Compress_HYBRID (dst, src->GetPixelAddr(0, 0), grcImage::A8R8G8B8);
	}

	return false;
}

template <> bool CDXTCompressor::Compress(grcImage* dst, const CImage4F* src) const
{
	if (m_compressor != CT_ATI     &&
		m_compressor != CT_ATI_EXE &&
		m_compressor != CT_XG      &&
		m_compressor != CT_HYBRID) // not compatible with float data, convert to ARGB
	{
		const CImage32 temp(src->mW, src->mH);

		temp.ConvertFrom(src);

		return Compress(dst, &temp);
	}

	switch ((int)m_compressor)
	{
	case CT_DEVIL   : return Compress_DEVIL  (dst, src->GetPixelAddr(0, 0), grcImage::A32B32G32R32F);
	case CT_SQUISH  : return Compress_SQUISH (dst, src->GetPixelAddr(0, 0), grcImage::A32B32G32R32F);
	case CT_ATI     : return Compress_ATI    (dst, src->GetPixelAddr(0, 0), grcImage::A32B32G32R32F);
	case CT_ATI_EXE : return Compress_ATI_EXE(dst, src->GetPixelAddr(0, 0), grcImage::A32B32G32R32F);
	case CT_STB     : return Compress_STB    (dst, src->GetPixelAddr(0, 0), grcImage::A32B32G32R32F);
	case CT_XG      : return Compress_XG     (dst, src->GetPixelAddr(0, 0), grcImage::A32B32G32R32F);
	case CT_HYBRID  : return Compress_HYBRID (dst, src->GetPixelAddr(0, 0), grcImage::A32B32G32R32F);
	}

	return false;
}

// ripped from devil-1.7.8/src/il_dds-save.c
bool CDXTCompressor::Compress_DEVIL(grcImage* dst, const void* src, grcImage::Format srcFormat) const
{
	if (srcFormat != grcImage::A8R8G8B8)
	{
		return false;
	}

	const int w = dst->GetWidth ();
	const int h = dst->GetHeight();

	const grcImage::Format dstFormat      = dst->GetFormat();
	const int              dstBlockSize   = grcImage::GetFormatBitsPerPixel(dstFormat)*16/8; // bytes per block
	const int              dstBlockStride = dstBlockSize*((w + 3)/4);
	const int              srcStride      = grcImage::GetFormatBitsPerPixel(srcFormat)*w/8;

	ILimage* prevImage = ilGetCurImage();
	ILimage  tempImage_;
	ILimage* tempImage = &tempImage_;

	memset(tempImage, 0, sizeof(ILimage));

	tempImage->Width       = w;
	tempImage->Height      = h;
	tempImage->Depth       = 1;
	tempImage->Bpp         = 4;
	tempImage->Format      = IL_BGRA;
	tempImage->Bpc         = 1;
	tempImage->Type        = IL_UNSIGNED_BYTE;
	tempImage->Bps         = w*tempImage->Bpp; 
	tempImage->SizeOfPlane = h*tempImage->Bps;
	tempImage->SizeOfData  = 1*tempImage->SizeOfPlane;
	tempImage->Origin      = IL_ORIGIN_UPPER_LEFT;
	tempImage->Data        = (ILubyte*)src;

	ilSetCurImage(tempImage);

	const int dstSize = ilGetDXTCData(NULL, 0, dstFormat);

	ilGetDXTCData(dst->GetBits(), dstSize, dstFormat);

	ilSetCurImage(prevImage);

	return true;
}

// the squish compressor does not appear to be working at the moment ..
bool CDXTCompressor::Compress_SQUISH(grcImage* dst, const void* src, grcImage::Format srcFormat) const
{
	if (srcFormat != grcImage::A8R8G8B8)
	{
		return false;
	}

	const int w = dst->GetWidth ();
	const int h = dst->GetHeight();

	const grcImage::Format dstFormat      = dst->GetFormat();
	const int              dstBlockSize   = grcImage::GetFormatBitsPerPixel(dstFormat)*16/8; // bytes per block
	const int              dstBlockStride = dstBlockSize*((w + 3)/4);
	const int              srcStride      = grcImage::GetFormatBitsPerPixel(srcFormat)*w/8;

	int flags = 0;

	switch ((int)dstFormat)
	{
	case grcImage::DXT1 : flags |= squish::kDxt1; break;
	case grcImage::DXT3 : flags |= squish::kDxt3; break;
	case grcImage::DXT5 : flags |= squish::kDxt5; break;
	}

	switch (1) // squish compressor (expose this to metadata?)
	{
	case 0 : flags |= squish::kColourRangeFit           ; break;
	case 1 : flags |= squish::kColourClusterFit         ; break; // slow
	case 2 : flags |= squish::kColourIterativeClusterFit; break; // very very slow!
	}

	if (m_bIsNormalMap) // normal map or vector map
	{
		flags |= squish::kColourMetricUniform;
	}
	else // colour map .. but what if it's swizzled?
	{
		flags |= squish::kColourMetricPerceptual;
	}

	if (0) // is there a way to weight by channel instead?
	{
		flags |= squish::kWeightColourByAlpha;
	}

	const void* src1 = src;

	if (1) // squish (at least the rage flavour) is swapping r,b channels, so swap them on input
	{
		Color32* temp = rage_new Color32[w*h];
		const int area = w*h;

		for (int i = 0; i < area; i++)
		{
			const Color32 c = ((const Color32*)src1)[i];
			temp[i].Set(c.GetBlue(), c.GetGreen(), c.GetRed(), c.GetAlpha());
		}

		src1 = temp;
	}

	squish::CompressImage((squish::u8 const*)src1, w, h, srcStride, dstBlockStride, dst->GetBits(), flags, (const float*)&m_compressionWeights);

	if (src1 != src)
	{
		delete[] const_cast<void*>(src1);
	}

	return true;
}

bool CDXTCompressor::Compress_ATI(grcImage* dst, const void* src, grcImage::Format srcFormat) const
{
	if (srcFormat != grcImage::A8R8G8B8 &&
		srcFormat != grcImage::A32B32G32R32F)
	{
		return false;
	}

	const int w = dst->GetWidth ();
	const int h = dst->GetHeight();

	const grcImage::Format dstFormat      = dst->GetFormat();
	const int              dstBlockSize   = grcImage::GetFormatBitsPerPixel(dstFormat)*16/8; // bytes per block
	const int              dstBlockStride = dstBlockSize*((w + 3)/4);
	const int              srcStride      = grcImage::GetFormatBitsPerPixel(srcFormat)*w/8;

	ATI_TC_Texture         dstDesc;
	ATI_TC_Texture         srcDesc;
	ATI_TC_CompressOptions options;

	memset(&dstDesc, 0, sizeof(dstDesc));
	memset(&srcDesc, 0, sizeof(srcDesc));
	memset(&options, 0, sizeof(options));

	switch ((int)dstFormat)
	{
	case grcImage::DXT1  : dstDesc.format = ATI_TC_FORMAT_DXT1    ; break;
	case grcImage::DXT3  : dstDesc.format = ATI_TC_FORMAT_DXT3    ; break;
	case grcImage::DXT5  : dstDesc.format = ATI_TC_FORMAT_DXT5    ; break;
	case grcImage::DXT5A : dstDesc.format = ATI_TC_FORMAT_ATI1N   ; break; // ATI_TC_FORMAT_BC4
	case grcImage::DXN   : dstDesc.format = ATI_TC_FORMAT_ATI2N_XY; break; // ATI_TC_FORMAT_BC5
	}

	switch ((int)srcFormat)
	{
	case grcImage::A8R8G8B8      : srcDesc.format = ATI_TC_FORMAT_ARGB_8888; break;
	case grcImage::A32B32G32R32F : srcDesc.format = ATI_TC_FORMAT_ARGB_32F ; break;
	}

	dstDesc.dwSize     = sizeof(dstDesc);
	dstDesc.dwWidth    = w;
	dstDesc.dwHeight   = h;
	dstDesc.dwPitch    = 0;
	dstDesc.dwDataSize = ATI_TC_CalculateBufferSize(&dstDesc);
	dstDesc.pData      = (u8*)dst->GetBits();

	srcDesc.dwSize     = sizeof(srcDesc);
	srcDesc.dwWidth    = w;
	srcDesc.dwHeight   = h;
	srcDesc.dwPitch    = srcStride;
	srcDesc.dwDataSize = ATI_TC_CalculateBufferSize(&srcDesc);
	srcDesc.pData      = (u8*)src;

	options.bDXT1UseAlpha   = TRUE;
	options.nAlphaThreshold = 128;

	if (0) // these don't seem to work, why is that?
	{
		options.bUseChannelWeighting  = (m_compressionWeights.x != 1.0f || m_compressionWeights.y != 1.0f || m_compressionWeights.z != 1.0f) ? TRUE : FALSE;
		options.bUseAdaptiveWeighting = FALSE;
		options.fWeightingRed         = (double)m_compressionWeights.x;
		options.fWeightingGreen       = (double)m_compressionWeights.y;
		options.fWeightingBlue        = (double)m_compressionWeights.z;
	}

	const unsigned prev_controlfp = _controlfp(~0, _MCW_EM);
	const ATI_TC_ERROR err = ATI_TC_ConvertTexture(&srcDesc, &dstDesc, &options, NULL, NULL, NULL);
	_controlfp(prev_controlfp, _MCW_EM);

	if (err)
	{
		Errorf("ATI_TC_ConvertTexture returned %d", err);
		return false;
	}

	return true;
}

extern u32 gTextureConversionUniqueHash;

bool CDXTCompressor::Compress_ATI_EXE(grcImage* dst, const void* src, grcImage::Format srcFormat) const
{
	if (srcFormat != grcImage::A8R8G8B8 &&
		srcFormat != grcImage::A32B32G32R32F)
	{
		return false;
	}

	const int w = dst->GetWidth ();
	const int h = dst->GetHeight();

	const grcImage::Format dstFormat      = dst->GetFormat();
	const int              dstBlockSize   = grcImage::GetFormatBitsPerPixel(dstFormat)*16/8; // bytes per block
	const int              dstBlockStride = dstBlockSize*((w + 3)/4);
	const int              srcStride      = grcImage::GetFormatBitsPerPixel(srcFormat)*w/8;

	const char* formatStr = NULL;

	switch ((int)dstFormat)
	{
	case grcImage::DXT1  : formatStr = "DXT1"; break;
	case grcImage::DXT3  : formatStr = "DXT3"; break;
	case grcImage::DXT5  : formatStr = "DXT5"; break;
	case grcImage::DXT5A : formatStr = "ATI1"; break; // this doesn't work!
	case grcImage::DXN   : formatStr = "ATI2"; break;
	}

	const float wr = m_compressionWeights.x;
	const float wg = m_compressionWeights.y;
	const float wb = m_compressionWeights.z;

	const atVarString srcPath("c:/dump/temp/temp_%s_%08X_%dx%d_src.dds", grcImage::sm_platformString, gTextureConversionUniqueHash, w, h);
	const atVarString dstPath("c:/dump/temp/temp_%s_%08X_%dx%d_dst.dds", grcImage::sm_platformString, gTextureConversionUniqueHash, w, h);
	const atVarString command("c:/dump/ATI_Compress/TheCompressonator.exe -convert -overwrite %s %s +fourCC %s +red %f +green %f +blue %f", srcPath.c_str(), dstPath.c_str(), formatStr, wr, wg, wb);
	// +alpha_threshold <0-255>

	bool bSuccess = false;

	if (grcImage::SaveDDS(srcPath, src, w, h, srcFormat))
	{
		system(command);

		FILE* fp = fopen(dstPath, "rb");

		if (fp)
		{
			fseek(fp, sizeof(u32) + 124, SEEK_SET); // seek past DDS header
			fread(dst->GetBits(), 1, dstBlockStride*((h + 3)/4), fp);
			fclose(fp);

			if (dst->GetFormat() == grcImage::DXN) // 'ATI2' format is actually y,x instead of x,y, so it has to be swapped here
			{
				DXT::DXN_BLOCK* blocks = (DXT::DXN_BLOCK*)dst->GetBits();
				const int numBlocks = ((w + 3)/4)*((h + 3)/4);

				for (int i = 0; i < numBlocks; i++)
				{
					const DXT::DXT5_ALPHA temp_x = blocks[i].m_x;
					blocks[i].m_x = blocks[i].m_y;
					blocks[i].m_y = temp_x;
				}
			}

			bSuccess = true;
		}

		system(atVarString("del %s", srcPath.c_str()));
		system(atVarString("del %s", dstPath.c_str()));
	}

	return bSuccess;
}

bool CDXTCompressor::Compress_STB(grcImage* dst, const void* src, grcImage::Format srcFormat) const
{
	if (srcFormat != grcImage::A8R8G8B8)
	{
		return false;
	}

	const int w = dst->GetWidth ();
	const int h = dst->GetHeight();

	const grcImage::Format dstFormat      = dst->GetFormat();
	const int              dstBlockSize   = grcImage::GetFormatBitsPerPixel(dstFormat)*16/8; // bytes per block
	const int              dstBlockStride = dstBlockSize*((w + 3)/4);
	const int              srcStride      = grcImage::GetFormatBitsPerPixel(srcFormat)*w/8;

	const bool bIsDXT5 = (dstFormat == grcImage::DXT5); // stb only supports DXT1 and DXT5
	const int  dstStep = (16*(bIsDXT5 ? 8 : 4))/(8*4); // block size divided by 4

	for (int y = 0; y < h; y += 4)
	{
		u8* dstRow = (u8*)dst->GetPixelRowAddr(y);

		const u8* srcRow0 = (const u8*)src + (y + 0)*srcStride;
		const u8* srcRow1 = (const u8*)src + (y + 1)*srcStride;
		const u8* srcRow2 = (const u8*)src + (y + 2)*srcStride;
		const u8* srcRow3 = (const u8*)src + (y + 3)*srcStride;

		for (int x = 0; x < w; x += 4)
		{
			DXT::ARGB8888 temp[16];

			temp[0x00] = ((const DXT::ARGB8888*)srcRow0)[x + 0];
			temp[0x01] = ((const DXT::ARGB8888*)srcRow0)[x + 1];
			temp[0x02] = ((const DXT::ARGB8888*)srcRow0)[x + 2];
			temp[0x03] = ((const DXT::ARGB8888*)srcRow0)[x + 3];
			temp[0x04] = ((const DXT::ARGB8888*)srcRow1)[x + 0];
			temp[0x05] = ((const DXT::ARGB8888*)srcRow1)[x + 1];
			temp[0x06] = ((const DXT::ARGB8888*)srcRow1)[x + 2];
			temp[0x07] = ((const DXT::ARGB8888*)srcRow1)[x + 3];
			temp[0x08] = ((const DXT::ARGB8888*)srcRow2)[x + 0];
			temp[0x09] = ((const DXT::ARGB8888*)srcRow2)[x + 1];
			temp[0x0a] = ((const DXT::ARGB8888*)srcRow2)[x + 2];
			temp[0x0b] = ((const DXT::ARGB8888*)srcRow2)[x + 3];
			temp[0x0c] = ((const DXT::ARGB8888*)srcRow3)[x + 0];
			temp[0x0d] = ((const DXT::ARGB8888*)srcRow3)[x + 1];
			temp[0x0e] = ((const DXT::ARGB8888*)srcRow3)[x + 2];
			temp[0x0f] = ((const DXT::ARGB8888*)srcRow3)[x + 3];

			for (int i = 0; i < 16; i++)
			{
				const u8 r = temp[i].r;
				temp[i].r = temp[i].b;
				temp[i].b = r;
			}

			stb_compress_dxt_block(dstRow + x*dstStep, (const u8*)temp, bIsDXT5 ? 1 : 0, STB_DXT_HIGHQUAL);
		}
	}

	return true;
}

bool CDXTCompressor::Compress_XG(grcImage* dst, const void* src, grcImage::Format srcFormat) const
{
	if (srcFormat != grcImage::A8R8G8B8 &&
		srcFormat != grcImage::A32B32G32R32F)
	{
		return false;
	}

	const int w = dst->GetWidth ();
	const int h = dst->GetHeight();

	const grcImage::Format dstFormat      = dst->GetFormat();
	const int              dstBlockSize   = grcImage::GetFormatBitsPerPixel(dstFormat)*16/8; // bytes per block
	const int              dstBlockStride = dstBlockSize*((w + 3)/4);
	const int              srcStride      = grcImage::GetFormatBitsPerPixel(srcFormat)*w/8;

	Vector4* srcTemp = (Vector4*)const_cast<void*>(src);

	// is the input to XGCompressSurface actually what we need to feed it? .. no .. silly programmer
	{
		const int srcSize = srcStride*h;

		srcTemp = (Vector4*)(rage_new u8[srcSize]);

		memcpy(srcTemp, src, srcSize);

		grcImage::ByteSwapData(srcTemp, srcSize, 4);
	}

	u32 flags = 0;

	if (!m_bDither)
	{
		flags |= XGCOMPRESS_NO_DITHERING;
	}

	if ((srcFormat == grcImage::DXT3 || srcFormat == grcImage::DXT5) && !m_bNoAlphaPremultiply)
	{
		flags |= XGCOMPRESS_PREMULTIPLY; // what is this??
	}

	if (dstFormat == grcImage::CTX1 && m_CTX1_from_DXT1) // experimental ..
	{
		XGCompressSurface(
			dst->GetBits(),
			dstBlockStride,
			w,
			h,
			(D3DFORMAT)GetLinearD3DFormatForXGCompress(grcImage::DXT1),
			NULL,
			srcTemp,
			srcStride,
			(D3DFORMAT)GetLinearD3DFormatForXGCompress(srcFormat),
			NULL,
			flags,
			m_bDirtyDXT1AlphaHack ? 0.5f : 0.0f // AlphaRef
		);

		DXT::CTX1_BLOCK* blocks = (DXT::CTX1_BLOCK*)dst->GetBits();

		for (int i = 0; i < ((w + 3)/4)*((h + 3)/4); i++)
		{
			blocks[i] = DXT::CTX1_BLOCK::ConvertFromDXT1(*(DXT::DXT1_BLOCK*)&blocks[i]);
		}
	}
	else
	{
		XGCompressSurface(
			dst->GetBits(),
			dstBlockStride,
			w,
			h,
			(D3DFORMAT)GetLinearD3DFormatForXGCompress(dstFormat),
			NULL,
			srcTemp,
			srcStride,
			(D3DFORMAT)GetLinearD3DFormatForXGCompress(srcFormat),
			NULL,
			flags,
			m_bDirtyDXT1AlphaHack ? 0.5f : 0.0f // AlphaRef
		);
	}

	if (srcTemp != src)
	{
		delete[] srcTemp;
	}

	// is the output of XGCompressSurface actually the data we need? .. no, that would be too easy
	{
		grcImage::ByteSwapData(dst->GetBits(), dst->GetSize(), 2);
	}

	return true;
}

static void CalcHybridCompressorErrors(float err[CDXTCompressor::CHEM_COUNT], float dr, float dg, float db)
{
	const float ar = Abs<float>(dr);
	const float ag = Abs<float>(dg);
	const float ab = Abs<float>(db);

	const float err_SumSqr = dr*dr + dg*dg + db*db;
	const float err_SumAbs = ar + ag + ab;
	const float err_MaxAbs = Max<float>(ar, ag, ab);

	err[CDXTCompressor::CHEM_SumSumSqr] += err_SumSqr/16.0f;
	err[CDXTCompressor::CHEM_SumSumAbs] += err_SumAbs/16.0f;
	err[CDXTCompressor::CHEM_SumMaxAbs] += err_MaxAbs/16.0f;
	err[CDXTCompressor::CHEM_MaxSumSqr] = Max<float>(err_SumSqr, err[CDXTCompressor::CHEM_MaxSumSqr]);
	err[CDXTCompressor::CHEM_MaxSumAbs] = Max<float>(err_SumAbs, err[CDXTCompressor::CHEM_MaxSumAbs]);
	err[CDXTCompressor::CHEM_MaxMaxAbs] = Max<float>(err_MaxAbs, err[CDXTCompressor::CHEM_MaxMaxAbs]);
}

bool CDXTCompressor::Compress_HYBRID(grcImage* dst, const void* src, grcImage::Format srcFormat) const
{
	if (srcFormat != grcImage::A8R8G8B8 &&
		srcFormat != grcImage::A32B32G32R32F)
	{
		return false;
	}

	const int w = dst->GetWidth ();
	const int h = dst->GetHeight();

	const grcImage::Format dstFormat      = dst->GetFormat();
	const int              dstBlockSize   = grcImage::GetFormatBitsPerPixel(dstFormat)*16/8; // bytes per block
	const int              dstBlockStride = dstBlockSize*((w + 3)/4);
	const int              srcStride      = grcImage::GetFormatBitsPerPixel(srcFormat)*w/8;

	grcImage* tmp = grcImage::Create(w, h, 1, dstFormat, grcImage::STANDARD, 0, 0);
	bool bOk = false;

	if (Compress_ATI(tmp, src, srcFormat) &&
		Compress_XG (dst, src, srcFormat))
	{
		float* blockErrors = rage_new float[(w*h)/16];
		float  blockErrMin = +9999.0f;
		float  blockErrMax = -9999.0f;

		int numBlocks = 0;

		for (int pass = 0; pass < 2; pass++) // first pass collects block errors
		{
			for (int y = 0; y < h; y += 4)
			{
				for (int x = 0; x < w; x += 4)
				{
					if (pass == 0)
					{
						Vector4 dstAvg = Vector4(0, 0, 0, 0);
						Vector4 tmpAvg = Vector4(0, 0, 0, 0);
						Vector4 srcAvg = Vector4(0, 0, 0, 0);

						Vector3 srcMin = Vector3(+9999.0f, +9999.0f, +9999.0f);
						Vector3 srcMax = Vector3(-9999.0f, -9999.0f, -9999.0f);

						Vector4 dstBlock[4*4];
						Vector4 tmpBlock[4*4];
						Vector4 srcBlock[4*4];

						dst->GetPixelBlock(dstBlock, x, y);
						tmp->GetPixelBlock(tmpBlock, x, y);

						for (int yy = 0; yy < 4; yy++)
						{
							for (int xx = 0; xx < 4; xx++)
							{
								Vector4 c = Vector4(0, 0, 0, 0);

								if (srcFormat == grcImage::A8R8G8B8)
								{
									c = VEC4V_TO_VECTOR4(((const Color32*)src)[(x + xx) + (y + yy)*w].GetRGBA());
								}
								else if (srcFormat == grcImage::A32B32G32R32F)
								{
									c = ((const Vector4*)src)[(x + xx) + (y + yy)*w];
								}

								srcBlock[xx + yy*4] = c;

								dstAvg += dstBlock[xx + yy*4]/16.0f;
								tmpAvg += tmpBlock[xx + yy*4]/16.0f;
								srcAvg += c/16.0f;

								srcMin.x = Min<float>(srcMin.x, c.x);
								srcMin.y = Min<float>(srcMin.y, c.y);
								srcMin.z = Min<float>(srcMin.z, c.z);

								srcMax.x = Max<float>(srcMax.x, c.x);
								srcMax.y = Max<float>(srcMax.y, c.y);
								srcMax.z = Max<float>(srcMax.z, c.z);
							}
						}

						float dstErr[CHEM_COUNT];
						float tmpErr[CHEM_COUNT];

						memset(dstErr, 0, sizeof(dstErr));
						memset(tmpErr, 0, sizeof(tmpErr));

						for (int yy = 0; yy < 4; yy++)
						{
							for (int xx = 0; xx < 4; xx++)
							{
								Vector4 dstDiff = dstBlock[xx + yy*4] - srcBlock[xx + yy*4];
								Vector4 tmpDiff = tmpBlock[xx + yy*4] - srcBlock[xx + yy*4];

								if (m_hybridZeroSum)
								{
									dstDiff -= (dstAvg - srcAvg);
									tmpDiff -= (tmpAvg - srcAvg);
								}

								CalcHybridCompressorErrors(dstErr, dstDiff.x, dstDiff.y, dstDiff.z);
								CalcHybridCompressorErrors(tmpErr, tmpDiff.x, tmpDiff.y, tmpDiff.z);
							}
						}

						const Vector3 srcSat = srcMax - srcMin;

						dstErr[CHEM_GreenSat] = 1.0f*srcSat.y;
						tmpErr[CHEM_GreenSat] = 1.0f*Max<float>(srcSat.x, srcSat.z, 0.001f);

						const float blockErr0 = dstErr[m_hybridErrorMetric];
						const float blockErr1 = tmpErr[m_hybridErrorMetric];

						float blockErr = blockErr0 - blockErr1;

						//if (m_hybridErrRelative)
						//{
						//	const float blockErrM = Max<float>(blockErr0, blockErr1);
						//
						//	if (blockErrM != 0.0f)
						//	{
						//		blockErr /= blockErrM;
						//	}
						//}

						blockErrors[(x/4) + (y/4)*(w/4)] = blockErr;
						blockErrMin = Min<float>(blockErr, blockErrMin);
						blockErrMax = Max<float>(blockErr, blockErrMax);
					}
					else
					{
						const float blockErr = blockErrors[(x/4) + (y/4)*(w/4)];

						if (blockErr > 0.0f)
						{
							switch ((int)dstFormat)
							{
							case grcImage::DXT1: *reinterpret_cast<DXT::DXT1_BLOCK*>(dst->GetPixelAddr(x, y))           = *reinterpret_cast<const DXT::DXT1_BLOCK*>(tmp->GetPixelAddr(x, y)); break;
							case grcImage::DXT3:  reinterpret_cast<DXT::DXT3_BLOCK*>(dst->GetPixelAddr(x, y))->m_colour =  reinterpret_cast<const DXT::DXT3_BLOCK*>(tmp->GetPixelAddr(x, y))->m_colour; break;
							case grcImage::DXT5:  reinterpret_cast<DXT::DXT5_BLOCK*>(dst->GetPixelAddr(x, y))->m_colour =  reinterpret_cast<const DXT::DXT5_BLOCK*>(tmp->GetPixelAddr(x, y))->m_colour; break;
							}

							numBlocks++;
						}

						if (m_hybridDebugTintMin > 0.0f ||
							m_hybridDebugTintMax > 0.0f)
						{
							const int   tintR      = (blockErr >  0.0f) ? 255 : 0;
							const int   tintG      = 0;
							const int   tintB      = (blockErr <= 0.0f) ? 255 : 0;
							const float tint       = (blockErr <= 0.0f) ? (blockErr/blockErrMin) : (blockErr/blockErrMax);
							const float tintAmount = m_hybridDebugTintMin + (m_hybridDebugTintMax - m_hybridDebugTintMin)*tint;

							switch ((int)dstFormat)
							{
							case grcImage::DXT1: reinterpret_cast<DXT::DXT1_BLOCK*>(dst->GetPixelAddr(x, y))->         TintRGB(tintR, tintG, tintB, tintAmount); break;
							case grcImage::DXT3: reinterpret_cast<DXT::DXT3_BLOCK*>(dst->GetPixelAddr(x, y))->m_colour.TintRGB(tintR, tintG, tintB, tintAmount); break;
							case grcImage::DXT5: reinterpret_cast<DXT::DXT5_BLOCK*>(dst->GetPixelAddr(x, y))->m_colour.TintRGB(tintR, tintG, tintB, tintAmount); break;
							}
						}
					}
				}
			}
		}

		if (m_bVerbose)
		{
			Displayf(
				"  hybrid compressor changed %d of %d blocks (%.2f%%), blockErrMin = %f, blockErrMax = %f",
				numBlocks,
				(w*h)/16,
				100.0f*(float)numBlocks/(float)((w*h)/16),
				blockErrMin,
				blockErrMax
			);
		}

		delete[] blockErrors;

		bOk = true;
	}

	tmp->Release();

	return bOk;
}

__COMMENT(static) u32 CDXTCompressor::GetLinearD3DFormatForXGCompress(grcImage::Format format) // handles formats relevant for offline compression
{
	switch (format)
	{
	case grcImage::DXT1          : return 0x1a200052; // D3DFMT_LIN_DXT1         
	case grcImage::DXT3          : return 0x1a200053; // D3DFMT_LIN_DXT3         
	case grcImage::DXT5          : return 0x1a200054; // D3DFMT_LIN_DXT5         
	case grcImage::CTX1          : return 0x1a20007c; // D3DFMT_LIN_CTX1         
	case grcImage::DXT3A         : return 0x1a20007a; // D3DFMT_LIN_DXT3A        
	case grcImage::DXT3A_1111    : return 0x1a20007d; // D3DFMT_LIN_DXT3A_1111 (not actually valid for XGCompressSurface)
	case grcImage::DXT5A         : return 0x1a20007b; // D3DFMT_LIN_DXT5A        
	case grcImage::DXN           : return 0x1a200071; // D3DFMT_LIN_DXN          
	case grcImage::A8R8G8B8      : return 0x18280086; // D3DFMT_LIN_A8R8G8B8     
	case grcImage::A32B32G32R32F : return 0x1a22aaa6; // D3DFMT_LIN_A32B32G32R32F
	default                      : return 0x00000000;
	}
}

CDXTCompressorParams::CDXTCompressorParams()
{
	memset(this, 0, sizeof(*this));

	m_compressionWeights = Vector3(1.0f, 1.0f, 1.0f);
}

bool CDXTCompressorParams::SetCompatible(int format, eCompressorType compressor)
{
	if (m_compressionWeights.x != 1.0f ||
		m_compressionWeights.y != 1.0f ||
		m_compressionWeights.z != 1.0f)
	{
		if (compressor == CT_ATI) // the ATI_Compress static lib does not support per-channel weighting for some reason, so if we need this then switch to ATI_EXE
		{
			compressor = CT_ATI_EXE;
		}
		else
		{
			// oops, the other compressors don't support per-channel weighting either, so ignore it
		}
	}

	if (compressor == CT_DEVIL)
	{
		if (m_bDirtyDXT1AlphaHack) { return false; }

		if (format == grcImage::DXT1) { m_compressor = compressor; return true; }
		if (format == grcImage::DXT3) { m_compressor = compressor; return true; }
		if (format == grcImage::DXT5) { m_compressor = compressor; return true; }
	}
	else if (compressor == CT_SQUISH)
	{
		if (format == grcImage::DXT1) { m_compressor = compressor; return true; }
		if (format == grcImage::DXT3) { m_compressor = compressor; return true; }
		if (format == grcImage::DXT5) { m_compressor = compressor; return true; }
	}
	else if (compressor == CT_ATI || compressor == CT_ATI_EXE || compressor == CT_HYBRID)
	{
		if (m_bDirtyDXT1AlphaHack) { return false; } // ATI_Compress is supposed to handle DXT1 with 1-bit alpha, but it doesn't appear to work .. not sure why

		if (format == grcImage::DXT1 ) { m_compressor = compressor; return true; }
		if (format == grcImage::DXT3 ) { m_compressor = compressor; return true; }
		if (format == grcImage::DXT5 ) { m_compressor = compressor; return true; }

		if (compressor != CT_HYBRID) // don't pick hybrid compressor for formats that don't contain colour
		{
		//	if (format == grcImage::DXT5A) { m_compressor = compressor; return true; } // seems to be broken ..
			if (format == grcImage::DXN  ) { m_compressor = compressor; return true; }
		}
	}
	else if (compressor == CT_STB)
	{
		if (m_bDirtyDXT1AlphaHack) { return false; }

		if (format == grcImage::DXT1) { m_compressor = compressor; return true; }
		if (format == grcImage::DXT5) { m_compressor = compressor; return true; }
	}
	else if (compressor == CT_XG)
	{
		m_compressor = CT_XG;

		return true; // compatible with all formats
	}

	return false;
}

// ================================================================================================
// ================================================================================================

template <typename Q> bool ConvertImage(grcImage* dst, const CImage<Q>* src, const CDXTCompressorParams& cp) // convert to grcImage from CImage<Q>
{
	typedef CImagePixelType<Q>::T SrcPixelType;

	const int w = dst->GetWidth ();
	const int h = dst->GetHeight();

	if (IsSupportedImageExportFormat(dst, true) && src->mW == w && src->mH == h)
	{
		const grcImage::Format dstFormat = dst->GetFormat();

		if (grcImage::IsFormatDXTBlockCompressed(dstFormat))
		{
			return CDXTCompressor(cp).Compress(dst, src);
		}
		else do // handle non-compressed formats
		{
			#define DEF_SUPPORTED_IMAGE_EXPORT_FORMAT(f) CONVERT_TO_IMAGE_FORMAT(f, dst, SrcPixelType, src);
			FOREACH(DEF_SUPPORTED_IMAGE_EXPORT_FORMAT)
			#undef  DEF_SUPPORTED_IMAGE_EXPORT_FORMAT

			Assert(false);
			return false;
		}
		while (false);

		return true;
	}

	Assertf(false, "unsupported image format");
	return false;
}

template <typename Q> bool ConvertImage(CImage<Q>* dst, const grcImage* src) // convert to CImage<Q> from grcImage
{
	typedef CImagePixelType<Q>::T SrcPixelType;

	const int w = src->GetWidth ();
	const int h = src->GetHeight();

	if (IsSupportedImageExportFormat(src, true) && dst->mW == w && dst->mH == h)
	{
		const grcImage::Format srcFormat = src->GetFormat();

		if (grcImage::IsFormatDXTBlockCompressed(srcFormat))
		{
			CImage32 temp(w, h);
#if 0
			int ilFormat = 0;

			switch (srcFormat)
			{
			case grcImage::DXT1: ilFormat = IL_DXT1; break; // TODO -- handle DXT1 with 1-bit alpha
			case grcImage::DXT3: ilFormat = IL_DXT3; break;
			case grcImage::DXT5: ilFormat = IL_DXT5; break;
			default: Assert(false); break;
			}

			ilDecompressDXT(
				temp.GetPixelAddr(0, 0),
				src->GetPixelAddr(0, 0),
				src->GetWidth (),
				src->GetHeight(),
				ilFormat
			);

#else // fuck it, use squish

			int flags = 0;

			switch (srcFormat)
			{
			case grcImage::DXT1: flags = squish::kDxt1; break;
			case grcImage::DXT3: flags = squish::kDxt3; break;
			case grcImage::DXT5: flags = squish::kDxt5; break;
			default: Assert(false); break;
			}

			squish::DecompressImage(
				(squish::u8*)temp.GetPixelAddr(0, 0),
				src->GetWidth (),
				src->GetHeight(),
				src->GetPixelAddr(0, 0),
				flags
			);

			if (1) // squish is fucked, swap R/B channels
			{
				for (int y = 0; y < h; y++)
				{
					for (int x = 0; x < w; x++)
					{
						CImagePixelType_A8R8G8B8* pixel = (CImagePixelType_A8R8G8B8*)temp.GetPixelAddr(x, y);

						const u8 r = pixel->r;
						const u8 b = pixel->b;

						pixel->r = b;
						pixel->b = r;
					}
				}
			}
#endif
			dst->ConvertFrom(&temp); // convert from Color32 -> Q
			return true;
		}
		else do
		{
			#define DEF_SUPPORTED_IMAGE_EXPORT_FORMAT(f) CONVERT_FROM_IMAGE_FORMAT(CImagePixelType<Q>::T, dst, f, src);
			FOREACH(DEF_SUPPORTED_IMAGE_EXPORT_FORMAT)
			#undef  DEF_SUPPORTED_IMAGE_EXPORT_FORMAT

			Assert(false);
			return false;
		}
		while (false);

		return true;
	}

	Assertf(false, "unsupported image format");
	return false;
}

#if 1
void foo() // this is lame .. i just need to instantiate these templates .. must be a cleaner way?
{
	ConvertImage((grcImage*)NULL, (CImage32*)NULL, CDXTCompressorParams());
	ConvertImage((grcImage*)NULL, (CImage4F*)NULL, CDXTCompressorParams());
//	ConvertImage((grcImage*)NULL, (CImage1F*)NULL, CDXTCompressorParams()); // <-- this was causing a compiler warning ..

	ConvertImage((CImage32*)NULL, (grcImage*)NULL);
	ConvertImage((CImage4F*)NULL, (grcImage*)NULL);
	ConvertImage((CImage1F*)NULL, (grcImage*)NULL);

	((CImage32*)NULL)->ConvertFrom((CImage32*)NULL);
	((CImage4F*)NULL)->ConvertFrom((CImage32*)NULL);
	((CImage1F*)NULL)->ConvertFrom((CImage32*)NULL);

	((CImage32*)NULL)->ConvertFrom((CImage4F*)NULL);
	((CImage4F*)NULL)->ConvertFrom((CImage4F*)NULL);
	((CImage1F*)NULL)->ConvertFrom((CImage4F*)NULL);

	((CImage32*)NULL)->ConvertFrom((CImage1F*)NULL);
	((CImage4F*)NULL)->ConvertFrom((CImage1F*)NULL);
	((CImage1F*)NULL)->ConvertFrom((CImage1F*)NULL);
}
#endif

#undef CONVERT_TO_IMAGE_FORMAT
#undef CONVERT_FROM_IMAGE_FORMAT

} // namespace rage
