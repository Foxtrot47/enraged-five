//
// luaanim.cpp
//
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved.
//

#include "ragebuilder.h"
#include "log.h"
#include "gta.h"
#include "resource.h"

extern "C" {
#include "lua.h"
}

#include "cranimation/animation.h"
#include "cranimation/animtolerance.h"
#include "cranimation/frame.h"
#include "file/asset.h"
#include "file/serialize.h"

using namespace rage;

#include <string>

int DoAnimCombine(	const char** animFilenames,
					s32 numAnims,
					const char* outputFilename,
					const char* assetfolder,
					s32 combineType = 0, //0 = add, 1 = subtract, 2 = blend
					f32 weight = 1.f,
					s32 alignmentType = 0, //0 = none, 1 = sync, 2 = align, 3 = time, 4 = frame, 5 = phase
					f32 alignmentValue = 0.f,
					f32 compression = -1.f,
					f32 samplerate = 1.f / 30.f)
{
	// initialization:
	crAnimation::InitClass();

	// process path:
	ASSET.SetPath(assetfolder);

	// process anim list:
	if(numAnims <= 0)
	{
		Errorf("anim_combine: No source animation(s) specified\n");
	}
	else if(numAnims != 2)
	{
		Errorf("anim_combine: Currently exactly 2 animations must be specified\n");
	}

	// process output filename:
	if(!outputFilename)
	{
		Errorf("anim_combine: No output filename specified\n");
	}

	// process operation:

	bool add = false;
	bool subtract = false;
	bool blend = false;

	switch(combineType)
	{
	case 0:
		add = true;
		break;
	case 1:
		subtract = true;
		break;
	case 2:
		blend = true;
		break;
	}

	// process alignment:

	bool sync = false;
	bool align = false;
	bool time = false;
	bool frame = false;
	bool phase = false;

	switch(alignmentType)
	{
	case 0:
		break;
	case 1:
		sync = true;
		break;
	case 2:
		align = true;
		break;
	case 3:
		time = true;
		break;
	case 4:
		frame = true;
		break;
	case 5:
		phase = true;
		break;
	}

	// load animations:
	atArray<crAnimation*> srcAnims;
	for(int i=0; i<numAnims; ++i)
	{
		crAnimation* anim = crAnimation::AllocateAndLoad(animFilenames[i]);
		if(!anim)
		{
			Errorf("anim_combine: Failed to load animation '%s'", animFilenames[i]);
		}
		srcAnims.Grow() = anim;
	}

	// create destination animation:
	crAnimation* destAnim = new crAnimation;
	float duration = srcAnims[0]->GetDuration();
	int numFrames = int((duration / samplerate)+0.5f) + 1;
	destAnim->Create(numFrames, duration, srcAnims[0]->IsLooped(), srcAnims[0]->HasMoverTracks());

	// sample frames:
	atArray<const crFrame*> destFrames;
	for(int i=0; i<numFrames; ++i)
	{
		// TEMP - support multiple animations in a loop here
		int j = 1;

		// calculate times
		float destTime = Clamp(float(i)*samplerate, 0.f, duration);
		float srcTime = 0.f;
		if(sync)
		{
			srcTime = destTime;
		}
		else if(align)
		{
			srcTime = srcAnims[j]->ConvertPhaseToTime(srcAnims[0]->ConvertTimeToPhase(destTime));
		}
		else if(time)
		{
			srcTime = alignmentValue;
		}
		else if(frame)
		{
			srcTime = srcAnims[j]->Convert30FrameToTime(alignmentValue);
		}
		else if(phase)
		{
			srcTime = srcAnims[j]->ConvertPhaseToTime(alignmentValue);
		}

		// composite frames
		crFrame* destFrame = new crFrame;
		destFrame->InitCreateAnimationDofs(*srcAnims[0]);
		destFrame->Composite(*srcAnims[0], destTime);

		crFrame srcFrame;
		srcFrame.InitCreateAnimationDofs(*srcAnims[j]);
		srcFrame.Composite(*srcAnims[j], srcTime);

		// perform operation
		if(add)
		{
			destFrame->Add(weight, srcFrame);
		}
		else if(subtract)
		{
			destFrame->Subtract(weight, srcFrame);
		}
		else if(blend)
		{
			destFrame->Blend(weight, srcFrame);
		}

		destFrames.Grow() = destFrame;
	}

	// export animation:
	if(compression >= 0.f)
	{
		crAnimToleranceSimple tolerance(compression, compression, compression);
		destAnim->CreateFromFramesFast(destFrames, tolerance);
	}
	else
	{
		destAnim->CreateFromFramesFast(destFrames, DEFAULT_ANIM_TOLERANCE);
	}

	if(!destAnim->Save(outputFilename))
	{
		Errorf("anim_combine: Failed to save output file '%s'", outputFilename);
	}

	// shutdown:
	for(int i=0; i<destFrames.GetCount(); ++i)
	{
		if(destFrames[i])
		{
			delete destFrames[i];
		}
	}
	destFrames.Reset();

	for(int i=0; i<srcAnims.GetCount(); ++i)
	{
		if(srcAnims[i])
		{
			delete srcAnims[i];
		}
	}
	srcAnims.Reset();

	if(destAnim)
	{
		delete destAnim;
	}

	crAnimation::ShutdownClass();

	return 0;
}

#define BUFFER_SIZE 8192

int AnimCombine(lua_State* L)
{
	s32 RetVal = 0;

	GetArguments(L, 9, "anim_combine");
	CheckArgument(L, 1, lua_isstring, "anim_combine");	
	CheckArgument(L, 2, lua_isstring, "anim_combine");	
	CheckArgument(L, 3, lua_isstring, "anim_combine");	
	CheckArgument(L, 4, lua_isnumber, "anim_combine");	
	CheckArgument(L, 5, lua_isnumber, "anim_combine");	
	CheckArgument(L, 6, lua_isnumber, "anim_combine");	
	CheckArgument(L, 7, lua_isnumber, "anim_combine");	
	CheckArgument(L, 8, lua_isnumber, "anim_combine");	
	CheckArgument(L, 9, lua_isnumber, "anim_combine");	

	const char* animFilenames[64];
	char buffer[BUFFER_SIZE];

	int numAnims = 0;
	safecpy(buffer,lua_tostring(L,1),BUFFER_SIZE);
	char *comma = buffer - 1;
	while (comma && numAnims < 64) {
		animFilenames[numAnims++] = comma+1;
		comma = strchr(comma+1,',');
		if (comma) {
			*comma = 0;
		}
	}

	RetVal = DoAnimCombine(	animFilenames,
							numAnims,
							lua_tostring(L,2),
							lua_tostring(L,3),
							(s32)lua_tonumber(L,4),
							(f32)lua_tonumber(L,5),
							(s32)lua_tonumber(L,6),
							(f32)lua_tonumber(L,7),
							(f32)lua_tonumber(L,8),
							(f32)lua_tonumber(L,9));

	lua_pushnumber(L, RetVal);

	return 1;
}

typedef struct _LuaCommand
{
	const char*		name;
	LuaCommandFn*	command;
	const char*		description;
} LuaCommand;

LuaCommand gs_commandsAnim[] =
{
	{ "anim_combine",			AnimCombine,		"combine a set of .anim files"}
};

int gs_numCommandsAnim = sizeof(gs_commandsAnim) / sizeof(gs_commandsAnim[0]);

// register all the tool commands
void RegisterCommandsAnim(CLuaCore& lua)
{
	for(int i = 0; i < gs_numCommandsAnim; ++i)
	{
		lua.RegisterCommand(gs_commandsAnim[i].name, gs_commandsAnim[i].command);
	}
}
