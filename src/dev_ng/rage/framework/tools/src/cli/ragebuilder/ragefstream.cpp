#include "system/xtl.h"
#include "file/asset.h"
#include "file/device.h"

using namespace rage;

#include "ragefstream.h"
#include "libcore/exception.h"
#include "utils.h"

using namespace WinLib;

// Intentionally hiding the implementation of this
// To support FindFirstFile, etc functionality
//#include <vector>
//#include <string>
//using namespace std;
//typedef vector< string > string_list;
string_list* RageFileStream::ms_pFindlist = NULL;	// the list of all found files.

//static void AddFilesToList(const char* pFilter, string_list& list);

///////////////////////////////////////////////////////////////////////////////////////////////
RageFileStream::RageFileStream():
	m_hFile(INVALID_HANDLE_VALUE),
	m_pDevice(NULL),
	m_osCurrent(IS_CLOSED)
{

}

///////////////////////////////////////////////////////////////////////////////////////////////
RageFileStream::~RageFileStream()
{
	close();
}

///////////////////////////////////////////////////////////////////////////////////////////////
bool RageFileStream::isOpen()
{
	return fiIsValidHandle(m_hFile);
}

/////////////////////////////////////////////////////////////////////////////////////////////
bool RageFileStream::open(const char* p_cFilename, const char* p_cOpenFlags)
{
	close();

	char fullFilename[MAX_PATH];
	ASSET.FullPath(fullFilename, MAX_PATH, p_cFilename, "");

	if(strchr(p_cOpenFlags,'w') || strchr(p_cOpenFlags,'W'))
	{
		m_osCurrent = IS_WRITING;

		m_pDevice = fiDevice::GetDevice( fullFilename, false );
		Assert(m_pDevice);

		if(strchr(p_cOpenFlags,'t') || strchr(p_cOpenFlags,'T'))
		{
			// Create always
			m_hFile = m_pDevice->Create( fullFilename );
		}
		else
		{
			// Open always
			m_hFile = m_pDevice->Open( fullFilename, false );
		}
	}
	else if(strchr(p_cOpenFlags,'r') || strchr(p_cOpenFlags,'R'))
	{
		m_osCurrent = IS_READING;

		m_pDevice = fiDevice::GetDevice( fullFilename, true );
		Assert(m_pDevice);

		// Open always
		m_hFile = m_pDevice->Open( fullFilename, true );
	}

	if((m_hFile == INVALID_HANDLE_VALUE))
	{
		return false;
	}

	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////////
void RageFileStream::close()
{
	m_osCurrent = IS_CLOSED;

	if( m_hFile != INVALID_HANDLE_VALUE )
	{
		m_pDevice->Close(m_hFile);
		m_hFile = INVALID_HANDLE_VALUE;
		m_pDevice = NULL;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////
uint32 RageFileStream::getPending()
{
	int current = m_pDevice->Seek(m_hFile,0,seekCur);
	int size = m_pDevice->Seek(m_hFile,0,seekEnd);
	m_pDevice->Seek(m_hFile,current,seekSet);
	return (size - current);
}

///////////////////////////////////////////////////////////////////////////////////////////////
uint32 RageFileStream::seek(uint32 uOffset,int32 seekOrigin)
{
	switch(seekOrigin)
	{
	case STREAM_BEGIN:
		m_pDevice->Seek( m_hFile, uOffset, seekSet );
		break;
	case STREAM_CURRENT:
		m_pDevice->Seek( m_hFile, uOffset, seekCur );
		break;
	case STREAM_END:
		m_pDevice->Seek( m_hFile, uOffset, seekEnd );
		break;
	default:
		m_pDevice->Seek( m_hFile, uOffset, seekCur );
		break;
	}

	return getPos();
}

///////////////////////////////////////////////////////////////////////////////////////////////
uint32 RageFileStream::getPos()
{
	return m_pDevice->Seek(m_hFile,0,seekCur);
}

///////////////////////////////////////////////////////////////////////////////////////////////
uint32 RageFileStream::write(void* p_cWriteFrom,uint32 uiBytes)
{
	if(m_osCurrent != IS_WRITING)
	{
		Assert(false);
		return 0;
	}

	return m_pDevice->Write( m_hFile, p_cWriteFrom, uiBytes );
}

///////////////////////////////////////////////////////////////////////////////////////////////
uint32 RageFileStream::read(void* p_cReadTo,uint32 uiBytes)
{
	if(m_osCurrent != IS_READING)
	{
		Assert(false);
		return 0;
	}

	return m_pDevice->Read(m_hFile,p_cReadTo, uiBytes );
}

///////////////////////////////////////////////////////////////////////////////////////////////
uint8* RageFileStream::readAll(const char* p_cFilename,uint32& r_uiSize)
{
	RageFileStream fsTemp;

	uint8* p_cData;

	fsTemp.open(p_cFilename,"r");

	r_uiSize = fsTemp.getPending();

	p_cData = new uint8[r_uiSize];

	fsTemp.read(p_cData,r_uiSize);

	fsTemp.close();

	return p_cData;
}

///////////////////////////////////////////////////////////////////////////////////////////////
bool RageFileStream::createFile(const char* p_cFilePath)
{
	fiStream* pStream = ASSET.Create( p_cFilePath, "" );

	if( pStream )
	{
		pStream->Close();
		return true;
	}

	return false;
}

///////////////////////////////////////////////////////////////////////////////////////////////
int32 RageFileStream::getFileSize(const char* p_cFilePath)
{
	fiStream* file = ASSET.Open(p_cFilePath, "", true );

	if( file )
	{
		int32 iSize = file->Size();
		file->Close();
		return iSize;
	}

	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////
bool RageFileStream::copyFile(const char* p_cSourcePath,const char* p_cTargetPath)
{
	RageFileStream fsSource;
	RageFileStream fsDestination;

	uint8* p_cData;

	if( !fsSource.open(p_cSourcePath,"r") )
	{
		Errorf( "Could not open stream %s", p_cSourcePath );
		return false;
	}
	if( !fsDestination.open( p_cTargetPath, "wt" ) )
	{
		Errorf( "Could not open stream %s", p_cTargetPath );
		return false;
	}

	uint32 r_uiSize = fsSource.getPending();
	p_cData = new uint8[r_uiSize];

	if( !p_cData )
	{
		Errorf("Out of memory");
		return false;
	}

	fsSource.read(p_cData,r_uiSize);
	fsSource.close();

	fsDestination.write( p_cData, r_uiSize );
	return false;
}

///////////////////////////////////////////////////////////////////////////////////////////////
uint64 RageFileStream::findFirstFile(char* p_cFilename,const char* p_cWildCard)
{
	ms_pFindlist = new string_list; 
	Assert( ms_pFindlist );
	ms_pFindlist->clear();
	AddFilesToList( p_cWildCard, *ms_pFindlist );

	if( !ms_pFindlist->empty() )
	{
		strncpy(p_cFilename, (*ms_pFindlist->rend()).c_str(), MAX_PATH );
		return 0;
	}

	return -1;
}

/////////////////////////////////////////////////////////////////////////////////////////////
bool RageFileStream::findNextFile(char* p_cFilename,uint64 iHandle)
{
	if( !ms_pFindlist->empty() )
	{
		ms_pFindlist->pop_back();
		if( !ms_pFindlist->empty() )
		{
			strncpy(p_cFilename, (*ms_pFindlist->rend()).c_str(), MAX_PATH );
			return true;
		}
		else
		{
			return false;
		}
	}
	return false;
}

///////////////////////////////////////////////////////////////////////////////////////////////
void RageFileStream::findEnd(uint64 iHandle)
{
	ms_pFindlist->clear();
	delete ms_pFindlist;
	ms_pFindlist = NULL;
}

///////////////////////////////////////////////////////////////////////////////////////////////
bool RageFileStream::createDirectory(const char* p_cDirPath)
{
	return fiDeviceLocal::GetInstance().MakeDirectory( p_cDirPath );
}

///////////////////////////////////////////////////////////////////////////////////////////////
bool RageFileStream::deleteDirectory(const char* p_cDirPath)
{
	return fiDeviceLocal::GetInstance().DeleteDirectory( p_cDirPath, false );
}

///////////////////////////////////////////////////////////////////////////////////////////////
bool RageFileStream::moveFile(const char* p_cFilePathSource,const char* p_cFilePathDest)
{
	if(MoveFile(p_cFilePathSource,p_cFilePathDest) == 0)
	{
		return false;
	}
	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////////
bool RageFileStream::deleteFile(const char* p_cFilePath)
{
	// deprecated since rage contains this
	return false;
}

///////////////////////////////////////////////////////////////////////////////////////////////
bool RageFileStream::getTempFilename(const char* p_cPath,const char* p_cPrefix,uint32 Unique,char* p_cOutput)
{
	return false;
}

///////////////////////////////////////////////////////////////////////////////////////////////
bool RageFileStream::fileExists(const char* p_cFilePath)
{
	return ASSET.Exists( p_cFilePath, "" );
}

///////////////////////////////////////////////////////////////////////////////////////////////
bool RageFileStream::isReadOnly(const char* p_cFilePath)
{
	if(!fileExists(p_cFilePath))
	{
		return false;
	}

	u32	attrib = fiDeviceLocal::GetInstance().GetAttributes(p_cFilePath );

	return (( attrib & FILE_ATTRIBUTE_READONLY ) == FILE_ATTRIBUTE_READONLY );
}

///////////////////////////////////////////////////////////////////////////////////////////////
bool RageFileStream::makeWritable(const char* p_cFilePath)
{
	u32	attrib = fiDeviceLocal::GetInstance().GetAttributes(p_cFilePath );

	attrib &= ~FILE_ATTRIBUTE_READONLY;

	return fiDeviceLocal::GetInstance().SetAttributes( p_cFilePath, attrib );
}

///////////////////////////////////////////////////////////////////////////////////////////////
void RageFileStream::makeJustDir(char* p_cFilename)
{
	char* p_FirstFind = strrchr(p_cFilename,'\\');
	char* p_SecondFind = strrchr(p_cFilename,'/');

	if((p_FirstFind == NULL) && (p_SecondFind == NULL))
	{
		*p_cFilename = '\0';
	}
	else if((p_SecondFind == NULL) || ((p_FirstFind != NULL) && (p_FirstFind > p_SecondFind)))
	{
		p_FirstFind++;
		*p_FirstFind = NULL;
	}
	else if((p_FirstFind == NULL) || ((p_SecondFind != NULL) && (p_SecondFind > p_FirstFind)))
	{
		p_SecondFind++;
		*p_SecondFind = NULL;
	}
	else
	{
		*p_cFilename = '\0';
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////
int32 RageFileStream::runCmd(const char* p_cCommand)
{
//	return system(p_cCommand);
	return 0;
}
