//
// packmount.h
//
// Copyright (C) 1999-2005 Rockstar North.  All Rights Reserved. 
//

#if !defined( PACKMOUNT_H )
#define PACKMOUNT_H

namespace rage 
{
	class fiDevice;
}

#include "atl/array.h"
#include "system/xtl.h"


class CPackMountManager
{
public:
	enum ArchiveType
	{
		RPF,	// RAGE fiPackfile
		ZIP		// PKWARE Zip file
	};

	static void Init();
	static bool Mount(const char* mountName, const char* mountedAs, ArchiveType type );
	static bool Unmount(const char* mountName);
	
	static uint16 GetLastRageVersion() { return ms_RageVersion; }

	static bool ExtractFileToMemory( const char* filename, char* outMemoryName);
	static bool ReleaseFileFromMemory(const char* filename);

	static bool IsCompressed(const char* filename);
private:
	enum 
	{
		MAX_MOUNTED = 200 // Currently set to maximum open handles in a pack @see packfile
	};
	typedef struct
	{
		ArchiveType			m_ArchiveType;
		rage::fiDevice*		m_pack;
		
		char				m_packname[MAX_PATH];
		char				m_mountAs[MAX_PATH];
	} SPackMount;
	
	static rage::atFixedArray<SPackMount,MAX_MOUNTED>		ms_mountedPacks;
	static rage::u16 ms_RageVersion;
};

#endif // PACKMOUNT_H
