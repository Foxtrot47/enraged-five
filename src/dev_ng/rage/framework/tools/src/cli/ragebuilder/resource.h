//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ragebuilder.rc
//
#define IDC_CMD_NOTOALL                 3
#define IDC_CMD_NO                      4
#define IDC_CANCEL                      5
#define IDC_CMD_CANCEL                  5
#define IDD_DLG_GETLATEST               101
#define IDD_DLG_GETLATEST1              102
#define IDD_DLG_QUERY                   102
#define IDC_STC_PRINT                   1001
#define IDC_CMD_YES                     1002
#define IDC_CMD_YESTOALL                1003

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1004
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
