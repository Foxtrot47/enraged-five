// ========================================================
// StatsSerializer.h
// Copyright (c) 2012 Rockstar North.  All Rights Reserved.
// ========================================================

#ifndef __RAGEBUILDER_STATS_SERIALIZER_H__
#define __RAGEBUILDER_STATS_SERIALIZER_H__

#include <fstream>

// rage includes
#include "../gta_res.h"

// local headers

namespace rage {

// ================================================================================================

class StatsSerializer
{
public:
	StatsSerializer(const char* outputFilename);
	~StatsSerializer();

	void Serialize(gtaDrawable& drawable);
	void Serialize(gtaFragType& fragment);

private:
	std::ofstream outputStream_;
};

} // namespace rage

#endif __RAGEBUILDER_STATS_SERIALIZER_H__
