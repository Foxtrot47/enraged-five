//
// luapack.cpp
//
// Copyright (C) 1999-2005 Rockstar North.  All Rights Reserved.
//

#include "luapack.h"

#include "scriptpack.h"
#include "utils.h"

// Start a new pack file
int StartPack( lua_State* L )
{
	int iNumArguments = GetArgumentsRange( L, 0, 1, "start_pack" );
	bool bIncludeMetaData = true;
	if ( 1 == iNumArguments )
	{
		if ( LUA_TBOOLEAN != lua_type(L,1) )
			PushError( L, "start_pack", "incorrect argument type" );
		bIncludeMetaData = lua_toboolean(L,1) != 0 ? true : false;
	}

	RBStartPack( bIncludeMetaData );
	return 0;
}

// Start a new xcompress pack file
int StartPackXCompress( lua_State* L )
{
	int iNumArguments = GetArgumentsRange( L, 0, 1, "start_pack_xcompress" );
	bool bIncludeMetaData = true;
	if ( 1 == iNumArguments )
	{
		if ( LUA_TBOOLEAN != lua_type(L,1) )
			PushError( L, "start_pack_xcompress", "incorrect argument type" );
		bIncludeMetaData = lua_toboolean(L,1) != 0 ? true : false;
	}

	RBStartPackXCompress( bIncludeMetaData );
	return 0;
} 

// Start a new zlib pack file
int StartPackZlib( lua_State* L )
{
	int iNumArguments = GetArgumentsRange( L, 0, 1, "start_pack_zlib" );
	bool bIncludeMetaData = true;
	if ( 1 == iNumArguments )
	{
		if ( LUA_TBOOLEAN != lua_type(L,1) )
			PushError( L, "start_pack_zlib", "incorrect argument type" );
		bIncludeMetaData = lua_toboolean(L,1) != 0 ? true : false;
	}

	RBStartPackZlib( bIncludeMetaData );
	return 0;
}

// Start a new pack file
int StartUncompressedPack( lua_State* L )
{
	int iNumArguments = GetArgumentsRange( L, 0, 1, "start_uncompressed_pack" );
	bool bIncludeMetaData = true;
	if ( 1 == iNumArguments )
	{
		if ( LUA_TBOOLEAN != lua_type(L,1) )
			PushError( L, "start_uncompressed_pack", "incorrect argument type" );
		bIncludeMetaData = lua_toboolean(L,1) != 0 ? true : false;
	}

	RBStartUncompressedPack( bIncludeMetaData );
	return 0;
}

// Add files to a pack. The files are stored in the pack with the relative path from the set root.
// Has an optional second argument to specify the destination path and filename within the pack.
int AddToPack( lua_State* L )
{
	int iNumArguments = GetArgumentsRange(L, 1, 3, "add_to_pack_path");
	CheckArgument(L, 1, lua_isstring, "add_to_pack_path");

	const char* source;
	const char* destination = "";

	source = lua_tostring(L, 1);

	if( iNumArguments > 1 )
		destination = lua_tostring(L, 2);

	// Check for wildcards in the source basename and/or extension
	char sourceBasename[260];
	char sourceExtension[260];
	SplitFilename(source, NULL, sourceBasename, sourceExtension);

	if((strchr(sourceBasename, '*') != 0) || (strchr(sourceExtension, '*') != 0))
	{
		// Let's find the files in this directory
		string_list sourcePathnames;
		AddFilesToList( source, sourcePathnames, false );
		for(string_list::const_iterator itSourcePathname = sourcePathnames.begin() ; itSourcePathname != sourcePathnames.end() ; ++itSourcePathname)
		{
			SplitFilename(itSourcePathname->c_str(), NULL, sourceBasename, sourceExtension);

			// If destination is simply *, we copy to the root.  
			// Otherwise we respect the destination specified and append the filename of itSourcePathname
			char destinationThisFile[260];
			if (strchr(destination, '*') != 0)
				sprintf(destinationThisFile, "%s.%s", sourceBasename, sourceExtension);
			else
				sprintf(destinationThisFile, "%s/%s.%s", destination, sourceBasename, sourceExtension);
			
			if( iNumArguments > 2 )
			{
				bool compress = lua_toboolean(L,3) != 0 ? true : false;
				RBAddToPackForceCompression(itSourcePathname->c_str(),destinationThisFile, compress);
			}
			else
				RBAddToPack(itSourcePathname->c_str(), destinationThisFile);
		}
	}
	else
	{
		// Plain old wildcard free pathname
		if( iNumArguments > 2 )
		{
			bool compress = lua_toboolean(L,3) != 0 ? true : false;
			RBAddToPackForceCompression(source,destination, compress);
		}
		else
			RBAddToPack(source,destination);
	}

	return 0;
}

// Saves to pack to a file.
int SavePack( lua_State* L )
{
	GetArguments(L, 1, "save_pack");
	CheckArgument(L, 1, lua_isstring, "save_pack");
	RBSavePack(lua_tostring(L, 1));
	return 0;
}

// Close the pack file.
int ClosePack( lua_State* L )
{
	GetArguments(L, 0, "close_pack");
	RBClosePack();
	return 0;
}

// Mount a pack file for reading.
int MountPack( lua_State* L )
{
	int iArgs = GetArgumentsRange(L, 1, 2, "mount_image");
	CheckArgument(L, 1, lua_isstring, "mount_image");
	const char* packname = lua_tostring(L, 1);
	const char* mountAs = NULL;
	if( iArgs == 2 )
		mountAs = lua_tostring(L, 2);

	RBMountPack(packname,mountAs);

	return 0;
}

// Unmount a pack file.
int UnmountPack( lua_State* L )
{
	GetArguments(L, 1, "unmount_pack");
	CheckArgument(L, 1, lua_isstring, "unmount_pack");
	RBUnmountPack(lua_tostring(L, 1));
	return 0;
}


// Is a packfile file compressed?
int IsCompressed( lua_State* L )
{
	GetArguments(L, 1, "is_compressed");
	CheckArgument(L, 1, lua_isstring, "is_compressed");
	bool result = RBIsCompressed(lua_tostring(L, 1));
	lua_pushboolean(L, result);
	return 0;
}

// Copied from ragebuilder.h
#ifndef RAGE_MAX_PATH
#define RAGE_MAX_PATH 4096
#endif //RAGE_MAX_PATH

int ExtractFileToMemory( lua_State* L )
{
	GetArguments(L, 1, "extract_file_to_memory");
	CheckArgument(L, 1, lua_isstring, "extract_file_to_memory");
	char outFileName[RAGE_MAX_PATH];
	bool success = RBExtractFileToMemory(lua_tostring(L, 1), outFileName);
	if (success)
	{
		lua_pushstring(L, outFileName);
	}
	else
	{
		lua_pushnil(L);
	}
	return 1;
}

int ReleaseFileFromMemory( lua_State* L )
{
	GetArguments(L, 1, "release_file_from_memory");
	CheckArgument(L, 1, lua_isstring, "release_file_from_memory");
	lua_pushboolean(L, RBReleaseFileFromMemory(lua_tostring(L, 1)));
	return 1;
}
