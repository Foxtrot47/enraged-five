//
// zipfile.h; class for constructing zip files.
//
// Copyright (C) 2010 Rockstar North.  All Rights Reserved. 
//

#if !defined( ZIPFILE_H_ )
#define ZIPFILE_H_

#include "utils.h"
#include "contrib/minizip/zip.h"

class CZipFile
{
public:
	static bool				Open( bool compress );
	static bool				Save( const char* filename, bool bSaveEmpty = false );
	static bool				Close();

	static bool				AddFile( const char* source, const char* destination );
	static bool				AddFilesByFilter( const char* source, const char* destination );
private:
	static bool				WriteFileData( zipFile& zf, const char* source, const char* destination );
	static unsigned long	FileTime( const char* source, tm_zip* tmzip, unsigned long* dt );

	static std::map<rage::atString, rage::atString> ms_mFileList;
	static bool				ms_bCompress;
	static bool				ms_bEmpty;
};

#endif // ZIPFILE_H_
