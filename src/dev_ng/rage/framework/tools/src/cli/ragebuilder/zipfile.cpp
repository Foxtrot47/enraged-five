
#include "zipfile.h"
#include "librb.h"

// RAGE headers
#include "file/asset.h"
#include "file/limits.h"
#include "system/xtl.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <errno.h>
#include <fcntl.h>
#include <direct.h>
#include <io.h>

#include <Windows.h>
#include "contrib/minizip/iowin32.h"

#define WRITE_BUFFER_SIZE (16384)

std::map<atString, atString> CZipFile::ms_mFileList;
bool CZipFile::ms_bCompress = true;
bool CZipFile::ms_bEmpty = true;


bool 
CZipFile::Open( bool compress )
{
	ms_mFileList.clear();
	ms_bEmpty = true;
	ms_bCompress = compress;

	return (true);
}

bool 
CZipFile::Save( const char* filename, bool bSaveEmpty )
{
	if (ms_bEmpty && !bSaveEmpty)
	{
		Warningf("Not saving an empty zipfile - %s", filename);
		return (true);
	}

	zipFile zf = zipOpen( filename, APPEND_STATUS_CREATE );

	for ( std::map<atString, atString>::const_iterator it = ms_mFileList.begin();
		it != ms_mFileList.end();
		++it )
	{
		WriteFileData( zf, it->second, it->first );
	}

	char comment[RAGE_MAX_PATH] = {0};
	sprintf_s( comment, RAGE_MAX_PATH, "Created with Ragebuilder %d.%d.", 
		RAGE_RELEASE, BUILD_VERSION );
	zipClose( zf, comment );
	
	return (true);
}

bool 
CZipFile::Close()
{
	ms_bEmpty = true;
	ms_mFileList.clear();
	return (true);
}

bool 
CZipFile::AddFile( const char* source, const char* destination )
{
	ms_mFileList[atString(destination)] = atString( source );
	ms_bEmpty = false;
	return (true);
}

bool 
CZipFile::AddFilesByFilter( const char* source, const char* destination )
{
	char packname[RAGE_MAX_PATH];
	string_list tempList;

	AddFilesToList(source, tempList, false);

	for (string_list::iterator it = tempList.begin(); it != tempList.end(); ++it)
	{
		const char* filename = (*it).c_str();
		const char* name = ASSET.FileName(filename);

		if (destination)
		{
			sprintf(packname, "%s/%s", destination, name);
		}
		else
		{
			sprintf(packname, "%s", name);
		}

		if ( CZipFile::AddFile(filename, packname) == false )
		{
			return false;
		}
	}

	return true;
}

bool
CZipFile::WriteFileData( zipFile& zf, const char* source, const char* destination )
{
	FILE* fp;
	zip_fileinfo zi;
	memset( &zi, 0, sizeof(zip_fileinfo) );
	FileTime( source, &zi.tmz_date, &zi.dosDate );

	const char *ext = strrchr(source, '.');
	bool canCompress = !ext || stricmp(ext,".zip");

	int result = zipOpenNewFileInZip( zf, destination, &zi,
		NULL, 0, NULL, 0, NULL,
		canCompress && ms_bCompress ? Z_DEFLATED : Z_NO_COMPRESSION,
		Z_DEFAULT_COMPRESSION );
	Verifyf( ZIP_OK == result, "Error adding file."	);

	fp = fopen( source, "rb" );
	if ( NULL == fp )
	{
		Errorf( "Error reading source file '%s'.", source );
		return (false);
	}

	int size_read = 0;
	unsigned char* buffer = new unsigned char[WRITE_BUFFER_SIZE];
	memset( buffer, 0, sizeof( unsigned char ) * WRITE_BUFFER_SIZE );

	do
	{
		result = ZIP_OK;
		size_read = (int)fread( buffer, 1, WRITE_BUFFER_SIZE, fp );
		if (size_read < WRITE_BUFFER_SIZE)
			if ( 0 == feof( fp ) )
			{
				Errorf( "Error in reading %s\n", source );
				result = ZIP_ERRNO;
			}

			if (size_read>0)
			{
				result = zipWriteInFileInZip( zf, buffer, size_read );
				if ( result < 0 )
				{
					Errorf( "Error in writing %s in the zipfile\n",
						source);
				}

			}
	} while ((result == ZIP_OK) && (size_read>0));

	fclose( fp );
	result = zipCloseFileInZip( zf );
	Verifyf( ZIP_OK == result, "Error in closing '%s'.", destination );

	return ( ZIP_OK == result );
}

uLong 
CZipFile::FileTime( const char* source, tm_zip* tmzip, unsigned long* dt )
{
#if defined _WIN32
	int ret = 0;
	{
		FILETIME ftLocal;
		HANDLE hFind;
		WIN32_FIND_DATA  ff32;

		hFind = FindFirstFile( source, &ff32 );
		if (hFind != INVALID_HANDLE_VALUE)
		{
			FileTimeToLocalFileTime(&(ff32.ftLastWriteTime),&ftLocal);
			FileTimeToDosDateTime(&ftLocal,((LPWORD)dt)+1,((LPWORD)dt)+0);
			FindClose(hFind);
			ret = 1;
		}
	}
	return ret;
#else
	return (0L);
#endif // _WIN32
}
