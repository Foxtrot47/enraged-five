//
// A class for creating rage pack files (.rpf)
//
// Copyright (C) 1999-2005 Rockstar North.  All Rights Reserved. 
//

#if !defined( PACK_H_ )
#define PACK_H_

// Local headers
#include "utils.h"

// Forward declarations
class IRpfBuilder;

class CRagePackFileBuilder
{
public:
	enum Version
	{
		INVALID,
		RPF4,
		RPF6,
		RPF7,
		// Additional version constants go here.
		RPF_MAX,
	};

	static Version			GetVersion( const char* filename );

	static bool				Open( Version ver = RPF6, bool compress = true, bool includeMetaData = true );
	static bool				Save( const char* filename, bool bSaveEmpty = false );
	static bool				Close();

	static void				PushCompression(bool compress);
	static void				PopCompression();

	static bool				AddFile(const char* source, const char* destination);
	static bool				AddFilesByFilter(const char* source, const char* destination);
private:
	static IRpfBuilder*		ms_pBuilder;
	static bool				ms_bEmpty;
	static tFileMTimeList	ms_mFileList;
	static Version			ms_eVersion;
	static bool				ms_bIncludeMetaData;
};

#endif // PACK_H_
