// ========================================================
// imageutil.h
// Copyright (c) 2010 Rockstar North.  All Rights Reserved. 
// ========================================================

#ifndef TEXTURECONVERSION_IMAGEUTIL_H
#define TEXTURECONVERSION_IMAGEUTIL_H

// rage includes
#include "vector/vector4.h"
#include "vector/color32.h"

// ================================================================================================
// ================================================================================================
// ================================================================================================
// TODO -- move to generic utility header? (e.g. renderer/util/util.h .. but not renderer)

namespace rage {

#define __COMMENT(x)
#define COMMA ,
#define EMPTY
#define STRING(x) #x
#define FOREACH(DEF) FOREACH_##DEF(DEF)
#define MACRO_EXPAND(...) __VA_ARGS__
#define SIZEOF(v) ((int)sizeof(v))
#define NUMBEROF(v) (SIZEOF(v)/SIZEOF((v)[0]))
#define INDEX_NONE (-1)
#define STATIC_ASSERT(expr,msg) typedef char STATIC_ASSERT_##msg[1][(expr)?1:0]

typedef double f64; // TODO -- should be declared with f32

template <int N> class CType // TODO -- move to generic utility header? (e.g. renderer/util/util.h .. but not renderer)
{
public:
	typedef void T_unsigned;
	typedef void T_signed;
	typedef void T_float;
};
template <> class CType<(8<<0)> { public: typedef u8  T_unsigned; typedef s8  T_signed; };
template <> class CType<(8<<1)> { public: typedef u16 T_unsigned; typedef s16 T_signed; };
template <> class CType<(8<<2)> { public: typedef u32 T_unsigned; typedef s32 T_signed; typedef f32 T_float; };
template <> class CType<(8<<3)> { public: typedef u64 T_unsigned; typedef s64 T_signed; typedef f64 T_float; };

template <typename T> __forceinline bool IsPow2(T x)
{
	return x > 0 && (x & (x - 1)) == 0;
}

// http://guru.multimedia.cx/avoiding-branchesifconditionals
template <typename T> __forceinline T GetNonZeroMask(T x) // returns (x ? 0xffffffff : 0) without branching
{
	typedef CType<8*sizeof(T)>::T_signed S;
	return (T)( ( (S)(x) | -(S)(x) ) >> (8*sizeof(T) - 1) );
}

} // namespace rage

// ================================================================================================
// ================================================================================================
// ================================================================================================

#define __x(x) // skip

#define FOREACH_DEF_SUPPORTED_IMAGE_IMPORT_FORMAT(DEF) \
	__x(UNKNOWN      ) \
	__x(DXT1         ) \
	__x(DXT3         ) \
	__x(DXT5         ) \
	__x(CTX1         ) \
	__x(DXT3A        ) \
	__x(DXT3A_1111   ) \
	__x(DXT5A        ) \
	__x(DXN          ) \
	DEF(A8R8G8B8     ) \
	DEF(A8B8G8R8     ) \
	DEF(A8           ) \
	DEF(L8           ) \
	DEF(A8L8         ) \
	DEF(A4R4G4B4     ) \
	DEF(A1R5G5B5     ) \
	DEF(R5G6B5       ) \
	DEF(R3G3B2       ) \
	DEF(A8R3G3B2     ) \
	DEF(A4L4         ) \
	DEF(A2R10G10B10  ) \
	DEF(A2B10G10R10  ) \
	DEF(A16B16G16R16 ) \
	DEF(G16R16       ) \
	DEF(L16          ) \
	DEF(A16B16G16R16F) \
	DEF(G16R16F      ) \
	DEF(R16F         ) \
	DEF(A32B32G32R32F) \
	DEF(G32R32F      ) \
	DEF(R32F         ) \
	__x(D15S1        ) \
	__x(D24S8        ) \
	__x(D24FS8       ) \
	__x(P4           ) \
	__x(P8           ) \
	__x(A8P8         ) \
	DEF(R8           ) \
	DEF(R16          ) \
	DEF(G8R8         ) \
	// end.

#define FOREACH_DEF_SUPPORTED_IMAGE_EXPORT_COMPRESSED_FORMAT(DEF) \
	DEF(DXT1         ) \
	DEF(DXT3         ) \
	DEF(DXT5         ) \
	DEF(CTX1         ) \
	DEF(DXT3A        ) \
	__x(DXT3A_1111   ) \
	DEF(DXT5A        ) \
	DEF(DXN          ) \
	// end.

#define FOREACH_DEF_SUPPORTED_IMAGE_EXPORT_FORMAT(DEF) \
	__x(UNKNOWN      ) \
	__x(DXT1         ) __COMMENT(compressed formats handled separately) \
	__x(DXT3         ) __COMMENT(compressed formats handled separately) \
	__x(DXT5         ) __COMMENT(compressed formats handled separately) \
	__x(CTX1         ) __COMMENT(compressed formats handled separately) \
	__x(DXT3A        ) __COMMENT(compressed formats handled separately) \
	__x(DXT3A_1111   ) __COMMENT(compressed formats handled separately) \
	__x(DXT5A        ) __COMMENT(compressed formats handled separately) \
	__x(DXN          ) __COMMENT(compressed formats handled separately) \
	DEF(A8R8G8B8     ) \
	DEF(A8B8G8R8     ) \
	DEF(A8           ) \
	DEF(L8           ) \
	DEF(A8L8         ) \
	DEF(A4R4G4B4     ) \
	DEF(A1R5G5B5     ) \
	DEF(R5G6B5       ) \
	DEF(R3G3B2       ) \
	DEF(A8R3G3B2     ) \
	DEF(A4L4         ) \
	DEF(A2R10G10B10  ) \
	DEF(A2B10G10R10  ) \
	DEF(A16B16G16R16 ) \
	DEF(G16R16       ) \
	DEF(L16          ) \
	DEF(A16B16G16R16F) \
	DEF(G16R16F      ) \
	DEF(R16F         ) \
	DEF(A32B32G32R32F) \
	DEF(G32R32F      ) \
	DEF(R32F         ) \
	__x(D15S1        ) \
	__x(D24S8        ) \
	__x(D24FS8       ) \
	__x(P4           ) \
	__x(P8           ) \
	__x(A8P8         ) \
	DEF(R8           ) \
	DEF(R16          ) \
	DEF(G8R8         ) \
	// end.

namespace rage {

class grcImage;

bool IsSupportedImageImportFormat(const grcImage* image);
bool IsSupportedImageExportFormat(const grcImage* image, bool bHandleCompressedFormats = false);

template <typename T> class CImage
{
public:
	typedef typename T T;

	int mW;
	int mH;
	T *mPixels; // for some reason atArray crashes?

	__forceinline CImage();
	__forceinline CImage(int w, int h);
	__forceinline CImage(int w, int h, const T& fill);
	__forceinline CImage(const void* data, int w, int h, int bytesPerRow = 0);
	__forceinline CImage(FILE* f, int off, int w, int h, int bytesPerRow = 0);
	__forceinline ~CImage();

	static CImage<T>* Load(const char* path);
	static CImage<T>* Load(const grcImage* src);

	bool ConvertFrom(const grcImage* src);

	template <typename Q> explicit __forceinline CImage(const CImage<Q>& src);
	template <typename Q> void ConvertFrom(const CImage<Q>* src) const;

	static __forceinline bool IsValid(int w, int h);

	__forceinline bool IsValid() const;

	__forceinline T* GetPixelRowAddr       (int y) const;
	__forceinline T* GetPixelRowAddrClamped(int y) const;
	__forceinline T* GetPixelRowAddrWrapped(int y) const;

	__forceinline void* GetPixelAddr(int x, int y) const { return &GetPixel(x, y); }

	__forceinline T& GetPixel       (int x, int y) const;
	__forceinline T& GetPixelClamped(int x, int y) const;
	__forceinline T& GetPixelWrapped(int x, int y) const;
	__forceinline T  GetPixelBorder (int x, int y, const T& border) const;

	__forceinline T& GetPixelAtLinearAddr(int addr) const;

	__forceinline void SetPixel       (int x, int y, const T& p) const;
	__forceinline bool SetPixelDiscard(int x, int y, const T& p) const;
};

class CDXTCompressorParams
{
public:
	enum eCompressorType
	{
		CT_NONE = 0,
		CT_DEVIL   , // DevIL, supports DXT1,DXT3,DXT5 (uses ATI Compressonator internally, static lib version)
		CT_SQUISH  , // not supported currently, supports DXT1,DXT3,DXT5
		CT_ATI     , // ATI Compressonator static lib, supports DXT1,DXT3,DXT5,DXT5A,DXN (DXT5A appears to be broken, compress options do not work)
		CT_ATI_EXE , // ATI Compressonator command line, runs TheCompressonator.exe (which I assume uses ATICompressor.dll)
		CT_STB     , // STB, supports DXT1,DXT5 only (amazingly fast!)
		CT_XG      , // Microsoft's XGCompressSurface, which supports all formats but doesn't have many options
		CT_HYBRID  , // runs both ATI and XG and picks the best for each 4x4 block
		CT_COUNT
	};

	// note that CTextureConversionParams::eTextureCompressionHEM and CDXTCompressorParams::eCompressorHybridErrorMetric must match exactly
	enum eCompressorHybridErrorMetric
	{
		CHEM_Default = 0,
		CHEM_SumSumSqr  , // err' = err + (dr^2 + dg^2 + db^2)
		CHEM_SumSumAbs  , // err' = err + (|dr| + |dg| + |db|)
		CHEM_SumMaxAbs  , // err' = err + max(|dr|, |dg|, |db|)
		CHEM_MaxSumSqr  , // err' = max(err, dr^2 + dg^2 + db^2)
		CHEM_MaxSumAbs  , // err' = max(err, |dr| + |dg| + |db|)
		CHEM_MaxMaxAbs  , // err' = max(err, max(|dr|, |dg|, |db|))
		CHEM_GreenSat   , // pick XG over ATI if block is "primarily green" .. doesn't seem to work well in all cases though, so don't use it
		CHEM_COUNT
	};

	CDXTCompressorParams();

	bool SetCompatible(int format, eCompressorType compressor); // format is grcImage::Format

	eCompressorType              m_compressor;
	eCompressorHybridErrorMetric m_hybridErrorMetric;
	float                        m_hybridDebugTintMin;
	float                        m_hybridDebugTintMax;
	bool                         m_hybridZeroSum;
	bool                         m_bVerbose;
	bool                         m_bDither;
	bool                         m_bDirtyDXT1AlphaHack; // nasty way to tell XGCompressSurface that AlphaRef should be 0.5f, not 0.0f (otherwise DXT1 comes out black)
	bool                         m_bNoAlphaPremultiply; // don't pass XGCOMPRESS_PREMULTIPLY to XGCompress for DXT3/DXT5
	bool                         m_bIsNormalMap;        // for squish
	bool                         m_CTX1_from_DXT1;      // experimental ..
	Vector3                      m_compressionWeights;  // ..
};

template <typename Q> bool ConvertImage(grcImage* dst, const CImage<Q>* src, const CDXTCompressorParams& cp);
template <typename Q> bool ConvertImage(CImage<Q>* dst, const grcImage* src);

typedef CImage<Color32> CImage32; // equivalent to A8R8G8B8
typedef CImage<Vector4> CImage4F; // equivalent to A32B32G32R32F
typedef CImage<float  > CImage1F; // equivalent to L32F

} // namespace rage

#include "imageutil.inl"

#endif TEXTURECONVERSION_IMAGEUTIL_H
