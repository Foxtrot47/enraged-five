
#include "ragebuilder.h"
#include "log.h"
#include "scriptrage.h"

#include "system/param.h"
#include "file/asset.h"
#include "grcore/image.h"

//extern "C" {
//#include "lua.h"
//}

using namespace rage;

#include <string>


#if !__64BIT
namespace rage {
	XPARAM(nointerleavemips);
}
#endif

bool RBInitRage()
{
	if( !RageBuilder::GetPlatformImpl()->InitRage() )
	{
		Errorf( "failed to initialise rage" );
		return false;
	}

	return true;
}

bool RBShutdownRage()
{
	RageBuilder::GetPlatformImpl()->ShutdownRage();
	return true;
}

bool RBSetRootDirectory(const char* path)
{
	if (!RageBuilder::GetPlatformImpl()->SetRootDirectory(path))
	{
		Errorf( "failed to set root - %s", path );
		return false;
	}

	rbTracef2("set root as %s", path);
	return true;
}

bool RBSetAutoTexDict(bool autotexdict)
{
	RageBuilder::GetPlatformImpl()->SetAutoTexDict(autotexdict);
	return true;
}

bool RBRegisterTextureName(const char* texname)
{
	RageBuilder::GetPlatformImpl()->RegisterTextureName(texname);
	return false;
}

bool RBLoadProceduralDefs(const char* filename)
{
	if (!RageBuilder::GetPlatformImpl()->LoadProceduralDefs(filename))
	{
		Errorf( "failed to load procedurals - %s", filename );
		return false;
	}

	rbTracef2("loaded procedural defs - %s", filename);
	return true;
}

bool RBLoadMaterialDefs(const char* filename)
{
	if (!RageBuilder::GetPlatformImpl()->LoadMaterialDefs(filename))
	{
		Errorf( "failed to load materials - %s", filename );
		return false;
	}

	rbTracef2("loaded materials defs - %s", filename);
	return true;
}

bool RBSetEffectPath(const char* path)
{
	RageBuilder::GetPlatformImpl()->SetEffectPath(path);
	return true;
}

bool RBSetShaderPath(const char* path)
{
	RageBuilder::GetPlatformImpl()->SetShaderPath(path);
	return true;
}

bool RBSetShaderDbPath(const char* path)
{
	RageBuilder::GetPlatformImpl()->SetShaderDbPath(path);
	return true;
}

bool RBSetBuildPath(const char* path)
{
	RageBuilder::GetPlatformImpl()->SetBuildPath(path);
	return (true);
}

bool RBSetAssetsPath(const char* path)
{
	RageBuilder::GetPlatformImpl()->SetAssetsPath(path);
	return (true);
}

bool RBSetCoreAssetsPath(const char* path)
{
	RageBuilder::GetPlatformImpl()->SetCoreAssetsPath(path);
	return (true);
}

bool RBSetMetadataDefinitionsPath(const char* path)
{
	RageBuilder::GetPlatformImpl()->SetMetadataDefinitionsPath(path);
	return (true);
}

const char* RBGetPlatform()
{
	RageBuilder::PLATFORM ePlatform = RageBuilder::GetPlatform();

	switch(ePlatform)
	{
	case RageBuilder::pltIndependent:
		return "independent";
		break;
	case RageBuilder::pltWin32:
		return "win32";
		break;
	case RageBuilder::pltXenon:
		return "xenon";
		break;
	case RageBuilder::pltPs3:
		return "ps3";
		break;
	case RageBuilder::pltWin64:
		return "win64";
		break;
	case RageBuilder::pltDurango:
		return "durango";
		break;
	case RageBuilder::pltOrbis:
		return "orbis";
		break;
	default:
		break;
	}

	return "unknown";
}

bool RBSetPlatform(const char* platform)
{
	RageBuilder::PLATFORM ePlatform;

	// rbTracef2("setting platform %s", platform);

	if( stricmp( platform, "independent" ) == 0 )
	{
		ePlatform = RageBuilder::pltIndependent;
	}
	else if( stricmp( platform, "win32" ) == 0 )
	{
		ePlatform = RageBuilder::pltWin32;
	}
#if __64BIT
	else if( stricmp( platform, "win64" ) == 0 )
	{
		ePlatform = RageBuilder::pltWin64;
	}
	else if( stricmp( platform, "durango" ) == 0 )
	{
		ePlatform = RageBuilder::pltDurango;
	}
	else if( stricmp( platform, "orbis" ) == 0 )
	{
		ePlatform = RageBuilder::pltOrbis;
	}
#endif
	else if ( stricmp( platform, "xenon" ) == 0 )
	{
		ePlatform = RageBuilder::pltXenon;
	}
	else if ( stricmp( platform, "ps3" ) == 0 )
	{
		ePlatform = RageBuilder::pltPs3;
	}
	else
	{
		Errorf( "unknown platform - %s", platform );
		return false;
	}

	if( !RageBuilder::SetPlatform( ePlatform ) )
	{
		Errorf( "failed to set platform - %s", platform );
		return false;
	}

	return true;
}

const char* RBGetPlatformExtension(const char* filename)
{
	const char* extension = filename;

	if( extension[0] == '.' )
		extension++;

	return RageBuilder::GetPlatformExtension(extension);
}

static char converted[256];

const char* RBConvertFilenameToPlatform(const char* filename,const char* platform)
{
	RageBuilder::PLATFORM ePlatform;

	//rbTracef3("converting filename %s to platform %s", filename, platform);

	if( stricmp( platform, "independent" ) == 0 )
		ePlatform = RageBuilder::pltIndependent;
	else if( stricmp( platform, "win32" ) == 0 )
		ePlatform = RageBuilder::pltWin32;
#if __64BIT
	else if( stricmp( platform, "win64" ) == 0 )
		ePlatform = RageBuilder::pltWin64;
	else if( stricmp( platform, "durango" ) == 0 )
		ePlatform = RageBuilder::pltDurango;
	else if( stricmp( platform, "orbis" ) == 0 )
		ePlatform = RageBuilder::pltOrbis;
#endif
	else if ( stricmp( platform, "xenon" ) == 0 )
		ePlatform = RageBuilder::pltXenon;
	else if ( stricmp( platform, "ps3" ) == 0 )
		ePlatform = RageBuilder::pltPs3;
	else
	{
		Errorf( "unknown platform - %s", platform );
		return NULL;
	}

	const char* extension = ASSET.FindExtensionInPath(filename);

	if (extension == NULL)
	{
		Errorf( "Filename does not contain extension - %s", filename );
		return NULL;
	}

	if( extension[0] == '.' )
		extension++;

	ASSET.RemoveExtensionFromPath(converted, 255, filename);

	strcat(converted, ".");
	strcat(converted, RageBuilder::ConvertExtensionToPlatform(extension, ePlatform));

	return converted;
}

bool RBStartBuild()
{
	if( !RageBuilder::GetPlatformImpl()->StartBuild() )
	{
		Errorf( "failed to start build" );
		return false;
	}

	return true;
}

// GunnarD: Retiring in favour of Configmanager setting
// bool RBSetTuningFolder(const char* path)
// {
// 	RageBuilder::GetPlatformImpl()->SetTuningFolder(path);
// 	return true;
// }

bool RBLoadTextureTemplates(const char* filefilter)
{
	RageBuilder::GetPlatformImpl()->LoadTextureTemplates(filefilter);

	return true;
}

bool RBLoadTexture(const char* texture)
{
	if( !RageBuilder::GetPlatformImpl()->LoadTexture(texture) )
	{
		Errorf( "failed to load texture - %s", texture );
		return false;
	}

	rbTracef3("loaded texture - %s", texture);
	return true;
}

bool RBLoadTextures(const char* textures, int& count)
{
	if( !RageBuilder::GetPlatformImpl()->LoadTextures(textures, count) )
	{
		Errorf( "failed to load textures - %s", textures );
		return false;
	}

	rbTracef3("loaded textures - %s", textures);
	return true;
}

bool RBLoadAnim(const char* anim)
{
	if( !RageBuilder::GetPlatformImpl()->LoadAnim(anim) )
	{
		Errorf( "failed to load anim - %s", anim );
		return false;
	}

	rbTracef3("loaded anim - %s", anim);
	return true;
}

bool RBLoadAnims(const char* anims)
{
	if( !RageBuilder::GetPlatformImpl()->LoadAnims(anims) )
	{
		Errorf( "failed to load anims - %s", anims );
		return false;
	}

	rbTracef3("loaded anims - %s", anims);
	return true;
}

bool RBLoadClip(const char* clip)
{
	if( !RageBuilder::GetPlatformImpl()->LoadClip(clip) )
	{
		Errorf( "failed to load clip - %s", clip );
		return false;
	}

	rbTracef3("loaded clip - %s", clip);
	return true;
}

bool RBLoadClips(const char* clips)
{
	if( !RageBuilder::GetPlatformImpl()->LoadClips(clips) )
	{
		Errorf( "failed to load clips - %s", clips );
		return false;
	}

	rbTracef3("loaded clips - %s", clips);
	return true;
}

bool RBLoadParamMotion(const char* parammotion)
{
	if( !RageBuilder::GetPlatformImpl()->LoadParamMotion(parammotion) )
	{
		Errorf( "failed to load param motions - %s", parammotion );
		return false;
	}

	rbTracef3("loaded param motion - %s", parammotion);
	return true;
}

bool RBLoadParamMotions(const char* parammotions)
{
	if( !RageBuilder::GetPlatformImpl()->LoadParamMotions(parammotions) )
	{
		Errorf( "failed to load param motions - %s", parammotions );
		return false;
	}

	rbTracef3("loaded param motions - %s", parammotions);
	return true;
}

bool RBLoadExpressions(const char* expressions)
{
	if( !RageBuilder::GetPlatformImpl()->LoadExpressions(expressions) )
	{
		Errorf( "failed to load expressions - %s", expressions );
		return false;
	}

	rbTracef3("loaded expressions - %s", expressions);
	return true;
}

bool RBLoadExpressionsList(const char* expressionslist)
{
	if( !RageBuilder::GetPlatformImpl()->LoadExpressionsList(expressionslist) )
	{
		Errorf( "failed to load expressions list - %s", expressionslist );
		return false;
	}

	rbTracef3("loaded expressions list - %s", expressionslist);
	return true;
}

bool RBLoadModel(const char* model)
{
	if( !RageBuilder::GetPlatformImpl()->LoadModel(model) )
	{
		Errorf( "failed to load model - %s", model  );
		return false;
	}

	rbTracef3("loaded model - %s", model);
	return true;
}

bool RBLoadBlendShape(const char* blendshape)
{
	if( !RageBuilder::GetPlatformImpl()->LoadBlendShape(blendshape) )
	{
		Errorf( "failed to load blendshape - %s", blendshape  );
		return false;
	}

	rbTracef3("loaded blendshape - %s", blendshape);
	return true;
}

bool RBLoadFragment(const char* fragment)
{
	if( !RageBuilder::GetPlatformImpl()->LoadFragment(fragment) )
	{
		Errorf( "failed to load fragment - %s", fragment  );
		return false;
	}

	rbTracef3("loaded fragment - %s", fragment);
	return true;
}

bool RBLoadNMData(const char* nmdata)
{
	if( !RageBuilder::GetPlatformImpl()->LoadNMData(nmdata) )
	{
		Errorf( "failed to load nmdata - %s", nmdata  );
		return false;
	}

	rbTracef3("loaded nm data - %s", nmdata);
	return true;
}

bool RBLoadModels(const char* models)
{
	if( !RageBuilder::GetPlatformImpl()->LoadModels(models) )
	{
		Errorf( "failed to load models - %s", models );
		return false;
	}

	return true;
}

bool RBLoadBound(const char* bound)
{
	if( !RageBuilder::GetPlatformImpl()->LoadBound(bound) )
	{
		Errorf( "failed to load bound - %s", bound );
		return false;
	}

	return true;
}

bool RBLoadBounds(const char* bounds)
{
	if( !RageBuilder::GetPlatformImpl()->LoadBounds(bounds) )
	{
		Errorf("Failed to load bounds -  %s", bounds);
		return false;
	}

	return true;
}

bool RBLoadPathRegion(const char* pathRegion)
{
	if( !RageBuilder::GetPlatformImpl()->LoadPathRegion(pathRegion) )
	{
		Errorf("Failed to load pathRegion -  %s", pathRegion);
		return false;
	}

	return true;
}

bool RBLoadNavMesh(const char* navmesh)
{
	if( !RageBuilder::GetPlatformImpl()->LoadNavMesh(navmesh) )
	{
		Errorf("Failed to load navmesh -  %s", navmesh);
		return false;
	}

	return true;
}

bool RBLoadHeightMesh(const char* navmesh)
{
	if( !RageBuilder::GetPlatformImpl()->LoadHeightMesh(navmesh) )
	{
		Errorf("Failed to load heightmesh -  %s", navmesh);
		return false;
	}

	return true;
}

bool RBLoadAudMesh(const char* audmesh)
{
	if( !RageBuilder::GetPlatformImpl()->LoadAudMesh(audmesh))
	{
		Errorf("Failed to load audmesh - %s", audmesh);
		return false;
	}

	return true;
}

bool RBLoadMeta(const char* meta)
{
	if( !RageBuilder::GetPlatformImpl()->LoadMeta(meta) )
	{
		Errorf("Failed to load meta -  %s", meta);
		return false;
	}

	return true;
}

bool RBLoadScript( const char* script )
{
	if ( !RageBuilder::GetPlatformImpl()->LoadScript( script ) )
	{
		Errorf( "Failed to load script - %s", script );
		return ( false );
	}
	return ( true );
}

bool RBLoadOccMeshes( const char* occMeshes )
{
	if ( !RageBuilder::GetPlatformImpl()->LoadOccMeshes( occMeshes ) )
	{
		Errorf( "Failed to load occluder meshes - %s", occMeshes );
		return ( false );
	}
	return ( true );
}

bool RBLoadFrameFilter(const char *framefilter)
{
	if( !RageBuilder::GetPlatformImpl()->LoadFrameFilter(framefilter) )
	{
		Errorf( "failed to load frame filter - %s", framefilter );
		return false;
	}

	rbTracef3("loaded frame filter - %s", framefilter);
	return true;
}

bool RBLoadFrameFilterList(const char *framefilterlist)
{
	if( !RageBuilder::GetPlatformImpl()->LoadFrameFilterList(framefilterlist) )
	{
		Errorf( "failed to load frame filter list - %s", framefilterlist );
		return false;
	}

	rbTracef3("loaded frame filter list - %s", framefilterlist);
	return true;
}

bool RBLoadHierarchicalNav(const char* hiernav)
{
	if( !RageBuilder::GetPlatformImpl()->LoadHierarchicalNav(hiernav) )
	{
		Errorf("Failed to load hierarchicalnav -  %s", hiernav);
		return false;
	}

	return true;
}

bool RBLoadWaypointRecording(const char* wptrec)
{
	if( !RageBuilder::GetPlatformImpl()->LoadWaypointRecording(wptrec) )
	{
		Errorf("Failed to load waypointrecording -  %s", wptrec);
		return false;
	}

	return true;
}

bool RBLoadEffect(const char* effect)
{
	if( !RageBuilder::GetPlatformImpl()->LoadEffect(effect) )
	{
		Errorf("Failed to load effect -  %s", effect);
		return false;
	}

	return true;
}

bool RBLoadVehicleRecording(const char* recording)
{
	if( !RageBuilder::GetPlatformImpl()->LoadVehicleRecording(recording) )
	{
		Errorf("Failed to load vehicle recording -  %s", recording);
		return false;
	}

	return true;
}

bool RBSaveTextureDictionary(const char* texdict, const char* hi_suffix)
{
	if( !RageBuilder::GetPlatformImpl()->SaveTextureDictionary(texdict, hi_suffix) )
	{
		Errorf( "failed to save texture dictionary - %s", texdict );
		return false;
	}

	Displayf("saved texture dictionary - %s", texdict );

	return true;
}

bool RBSaveTextureDictionaryHiMip(const char* texdict, const char* suffix)
{
	if ( !RageBuilder::GetPlatformImpl()->SaveTextureDictionaryHiMip(texdict, suffix) )
	{
		Errorf( "failed to save HiMip texture dictionary - %s", texdict );
		return ( false );
	}

	Displayf( "saved HiMip texture dictionary - %s", texdict );
	return ( true );
}

bool RBSaveClipDictionary(const char* clipdict, bool usePackLoader)
{
	if( !RageBuilder::GetPlatformImpl()->SaveClipDictionary(clipdict, usePackLoader) )
	{
		Errorf( "failed to save clip dictionary", clipdict );
		return false;
	}

	Displayf("saved clip dictionary - %s", clipdict);
	return true;
}

bool RBSaveParamMotionDictionary(const char* parammotiondict)
{
	if( !RageBuilder::GetPlatformImpl()->SaveParamMotionDictionary(parammotiondict) )
	{
		Errorf( "failed to save parameterised motion dictionary", parammotiondict );
		return false;
	}

	Displayf("saved parameterised motion dictionary - %s", parammotiondict);
	return true;
}

bool RBSaveExpressionsDictionary(const char* expressionsdict)
{
	if( !RageBuilder::GetPlatformImpl()->SaveExpressionsDictionary(expressionsdict) )
	{
		Errorf( "failed to save expressions dictionary", expressionsdict );
		return false;
	}

	Displayf("saved expressions dictionary - %s", expressionsdict);
	return true;
}

bool RBSaveModelDictionary(const char* modeldict)
{
	if( !RageBuilder::GetPlatformImpl()->SaveModelDictionary(modeldict) )
	{
		Errorf( "failed to save model dictionary -  %s", modeldict );
		return false;
	}

	Displayf("saved model dictionary - %s", modeldict);
	return true;
}

bool RBSaveClothDictionary(const char* modeldict)
{
	if( !RageBuilder::GetPlatformImpl()->SaveClothDictionary(modeldict) )
	{
// if there is no cloth we don't want to save dictionary
//		Errorf( "failed to save cloth dictionary -  %s", modeldict );
		return false;
	}

	Displayf("saved cloth dictionary - %s", modeldict);
	return true;
}


bool RBSaveBlendShapeDictionary(const char* blendshapedict)
{
	if( !RageBuilder::GetPlatformImpl()->SaveBlendShapeDictionary(blendshapedict) )
	{
		Errorf( "failed to save blendshape dictionary -  %s", blendshapedict );
		return false;
	}

	Displayf("saved blendshape dictionary - %s", blendshapedict);
	return true;
}

bool RBSaveBoundDictionary(const char* bounddict)
{
	if( !RageBuilder::GetPlatformImpl()->SaveBoundDictionary(bounddict) )
	{
		Errorf( "failed to save bound dictionary - %s", bounddict );
		return false;
	}

	Displayf("saved bound dictionary - %s", bounddict);
	return true;
}

bool RBSavePhysicsDictionary(const char* physicsdict)
{
	if( !RageBuilder::GetPlatformImpl()->SavePhysicsDictionary(physicsdict) )
	{
		Errorf( "failed to save physics dictionary - %s", physicsdict );
		return false;
	}

	Displayf("saved physics dictionary - %s", physicsdict);
	return true;
}

bool RBSaveTexture(const char* texture)
{
	if( !RageBuilder::GetPlatformImpl()->SaveTexture(texture) )
	{
		Errorf( "failed to save texture - %s", texture );
		return false;
	}

	Displayf("saved model - %s", texture);
	return true;
}

bool RBSaveAnim(const char* anim)
{
	if( !RageBuilder::GetPlatformImpl()->SaveAnim(anim) )
	{
		Errorf( "failed to save anim - %s", anim );
		return false;
	}

	Displayf("saved model - %s", anim);
	return true;
}

bool RBSaveModel(const char* model)
{
	if( !RageBuilder::GetPlatformImpl()->SaveModel(model) )
	{
		Errorf( "failed to save model - %s", model );
		return false;
	}

	Displayf("saved model - %s", model);
	return true;
}

bool RBSaveBlendShape(const char* blendshapeIn, const char* blendshapeOut)
{
	if( !RageBuilder::GetPlatformImpl()->SaveBlendShape(blendshapeIn, blendshapeOut) )
	{
		Errorf( "failed to save blendshape - %s", blendshapeOut );
		return false;
	}

	Displayf("saved blendshape - %s", blendshapeOut);
	return true;
}

bool RBSaveFragment(const char* fragment)
{
	if( !RageBuilder::GetPlatformImpl()->SaveFragment(fragment) )
	{
		Errorf( "failed to save fragment - %s", fragment );
		return false;
	}

	Displayf("saved fragment - %s", fragment);
	return true;
}

bool RBSaveBound(const char* bound)
{
	if( !RageBuilder::GetPlatformImpl()->SaveBound(bound) )
	{
		Errorf( "failed to save bound - %s", bound );
		return false;
	}

	Displayf("saved bound - %s", bound);
	return true;
}

bool RBSavePathRegion(const char* pathRegion)
{
	if( !RageBuilder::GetPlatformImpl()->SavePathRegion(pathRegion) )
	{
		Errorf( "failed to save pathRegion file - %s", pathRegion );
		return false;
	}

	Displayf("saved pathRegion file - %s", pathRegion);
	return true;
}

bool RBSaveNavMesh(const char* navmesh)
{
	if( !RageBuilder::GetPlatformImpl()->SaveNavMesh(navmesh) )
	{
		Errorf( "failed to save navmesh file - %s", navmesh );
		return false;
	}

	Displayf("saved navmesh file - %s", navmesh);
	return true;
}

bool RBSaveHeightMesh(const char* navmesh)
{
	if( !RageBuilder::GetPlatformImpl()->SaveHeightMesh(navmesh) )
	{
		Errorf( "failed to save heightmesh file - %s", navmesh );
		return false;
	}

	Displayf("saved heightmesh file - %s", navmesh);
	return true;
}

bool RBSaveAudMesh(const char* audmesh)
{

	if(!RageBuilder::GetPlatformImpl()->SaveAudMesh(audmesh))
	{
		Errorf("failed to save audmesh file - %s", audmesh);
		return false;
	}
	
	return true;
}

bool RBSaveRbf(const char* rbf)
{
	if(!RageBuilder::GetPlatformImpl()->SaveRbf(rbf))
	{
		Errorf("failed to save rbf file - %s", rbf);
		return false;
	}

	return true;
}

bool RBSavePso(const char* pso)
{
	return RBSavePsoAs(pso, "iff");
}

bool RBSavePsoAs(const char* pso, const char* fmt)
{
	RagePlatform::PsoFormat format = RagePlatform::PSO_IFF_FORMAT;
	if (!stricmp(fmt, "rsc") || !stricmp(fmt, "resource"))
	{
		format = RagePlatform::PSO_RSC_FORMAT;
	}

	if(!RageBuilder::GetPlatformImpl()->SavePso(pso, format))
	{
		Errorf("failed to save pso file - %s", pso);
		return false;
	}

	return true;
}

int RBIsPsoInPlaceLoadable(const char* pso, bool printMismatches)
{
	return RageBuilder::GetPlatformImpl()->IsPsoInPlaceLoadable(pso, printMismatches);
}

int RBPsoDebugStringSize(const char* pso)
{
	return RageBuilder::GetPlatformImpl()->PsoDebugStringSize(pso);
}

bool RBSaveMapData( const char* mapdata )
{
	if(!RageBuilder::GetPlatformImpl()->SaveMapData( mapdata ))
	{
		Errorf("failed to save mapdata file - %s", mapdata );
		return false;
	}

	return true;
}

bool RBSaveMapTypes( const char* maptypes )
{
	if(!RageBuilder::GetPlatformImpl()->SaveMapTypes( maptypes ))
	{
		Errorf("failed to save mapdata file - %s", maptypes );
		return false;
	}

	return true;
}

bool RBSaveImf( const char* imf )
{
	if(!RageBuilder::GetPlatformImpl()->SaveImf( imf ) )
	{
		Errorf("failed to save mapdata file - %s", imf );
		return false;
	}

	return true;
}

bool RBSaveScript( const char* script )
{
	if ( !RageBuilder::GetPlatformImpl()->SaveScript( script ) )
	{
		Errorf( "failed to save script file - %s", script );
		return (false);
	}
	return (true);
}

bool RBSaveOccImap( const char* occMeshes )
{
	if ( !RageBuilder::GetPlatformImpl()->SaveOccImap( occMeshes ) )
	{
		Errorf( "failed to save occluder imap file - %s", occMeshes );
		return (false);
	}
	return (true);
}

bool RBSaveOccXml( const char* occMeshes )
{
	if ( !RageBuilder::GetPlatformImpl()->SaveOccXml( occMeshes ) )
	{
		Errorf( "failed to save occluder xml file - %s", occMeshes );
		return (false);
	}
	return (true);
}

bool RBSaveCutfile( const char* cutfile )
{
	if ( !RageBuilder::GetPlatformImpl()->SaveCutfile( cutfile ) )
	{
		Errorf( "failed to save cutfile - %s", cutfile );
		return (false);
	}
	return (true);
}

bool RBSaveFrameFilterDictionary(const char* framefilterdict)
{
	if( !RageBuilder::GetPlatformImpl()->SaveFrameFilterDictionary(framefilterdict) )
	{
		Errorf( "failed to save frame filter dictionary", framefilterdict );
		return false;
	}

	Displayf("saved frame filter dictionary - %s", framefilterdict);
	return true;
}

bool RBSaveTextDatabase( const char* textdatabase )
{
	if(!RageBuilder::GetPlatformImpl()->SaveTextDatabase( textdatabase ) )
	{
		Errorf("failed to save text database file - %s", textdatabase );
		return false;
	}

	return true;
}

bool RBSaveHierarchicalNav(const char* hiernav)
{
	if( !RageBuilder::GetPlatformImpl()->SaveHierarchicalNav(hiernav) )
	{
		Errorf( "failed to save hierarchical nav file - %s", hiernav );
		return false;
	}

	Displayf("saved hierarchical nav file - %s", hiernav);
	return true;
}

bool RBSaveWaypointRecording(const char* wptrec)
{
	if( !RageBuilder::GetPlatformImpl()->SaveWaypointRecording(wptrec) )
	{
		Errorf( "failed to save waypointrecording file - %s", wptrec );
		return false;
	}

	Displayf("saved waypointrecording file - %s", wptrec);
	return true;
}

bool RBSaveEffect(const char* effect)
{
	if( !RageBuilder::GetPlatformImpl()->SaveEffect(effect) )
	{
		Errorf( "failed to save effect file - %s", effect );
		return false;
	}

	Displayf("saved effect file - %s", effect);
	return true;
}

bool RBSaveVehicleRecording(const char* recording)
{
	if( !RageBuilder::GetPlatformImpl()->SaveVehicleRecording(recording) )
	{
		Errorf( "failed to save vehicle recording file - %s", recording );
		return false;
	}

	Displayf("saved vehicle recording file - %s", recording);
	return true;
}

bool RBSetMaxMipMapCount(int maxmipmap)
{
	RagePlatform::SetMaxMipMapLevels(maxmipmap);

	return true;
}

#if __64BIT		// impossible on 64 bit platforms
bool RBSetMipMapInterleaving(bool) { return true; }
#else
bool RBSetMipMapInterleaving(bool interleaving)
{
	if(interleaving)
		PARAM_nointerleavemips.Set(0);
	else
		PARAM_nointerleavemips.Set("1");

	return true;
}
#endif

bool RBSetTextureMin(int min)
{
	RagePlatform::SetMinTextureSize(min);

	return true;
}

bool RBSetTextureMax(int max)
{
	RagePlatform::SetMaxTextureSize(max);

	return true;
}

bool RBSetOverloadTextureSrc(const char* scale)
{
	RagePlatform::SetOverloadTextureSrc(scale);

	return true;
}

bool RBSetLimitExemptTextures(const char* texnamelist)
{
	sysMemStartTemp();

	s32 buffsize = strlen(texnamelist) + 1;
	char* buffer = new char[buffsize];
	memset(buffer,0,buffsize);
	strcpy(buffer,texnamelist);

	char* p_texname = strtok(buffer,"|");
	string_list texnames;

	while(p_texname)
	{
		texnames.push_back(p_texname);

		p_texname = strtok(NULL,"|");
	}

	RageBuilder::GetPlatformImpl()->SetLimitExemptTextures(texnames);

	delete buffer;

	sysMemEndTemp();

	return true;
}

bool RBSetTextureScale(float scale)
{
	RagePlatform::SetTextureRatio(scale);

	return true;
}

bool RBSetCompressResources(bool compress)
{
	Displayf("Set compressresources to %s", compress ? "true" : "false");
	RagePlatform::SetCompressResources(compress);

	return true;
}

bool RBSetTriStripping(bool tristrip)
{
	Displayf("Set_tristripping to %s", tristrip?"true":"false");
	RagePlatform::SetTriStripping(tristrip);

	return true;
}

bool RBPushBuildVars()
{
	RagePlatform::PushBuildVars();
	return true;
}

bool RBPopBuildVars()
{
	RagePlatform::PopBuildVars();
	return true;
}

bool RBLoadTextureList(const char* texlist)
{
	RageBuilder::LoadTextureList(texlist);
	return true;
}

bool RBDoesTexHaveMips(const char* filename)
{
	bool retval = false;

	grcImage* checkfile = grcImage::Load(filename);

	if(checkfile && checkfile->GetExtraMipCount() > 0)
		retval = true;

	if(checkfile)
		checkfile->Release();

	return retval;
}

bool RBPostProcessVehicle(bool postprocess)
{
	Displayf("Set post process vehicles to %s", postprocess?"true":"false");
	RagePlatform::SetPostProcessVehicles(postprocess);

	return true;
}

bool RBHeadBlendGeometry(bool enable)
{
	Displayf("Set head blend geometry flag to %s", enable?"true":"false");
	RagePlatform::SetHeadBlendGeometry(enable);

	return true;
}

bool RBHeadBlendWriteBuffer(bool enable)
{
	Displayf("Set head blend write buffer flag to %s", enable?"true":"false");
	RagePlatform::SetHeadBlendWriteBuffer(enable);

	return true;
}

bool RBMicroMorphMesh(bool enable)
{
	Displayf("Set micro morph mesh flag to %s", enable?"true":"false");
	RagePlatform::SetMicroMorphMesh(enable);

	return true;
}

bool RBEnableFragMatrixSet(bool enable)
{
	Displayf("Set frag matrix set flag to %s", enable?"true":"false");
	RagePlatform::EnableFragMatrixSet(enable);

	return true;
}

bool RBPPUOnlyHint(bool onOff)
{
	Displayf("%segister complex entity treatment", onOff?"R":"Unr");
	RagePlatform::SetPPUOnlyHint(onOff);

	return true;
}

bool RBLodSkeleton(bool enable)
{
	Displayf("Set lod skeleton flag to %s", enable?"true":"false");
	RagePlatform::SetLodSkeleton(enable);

	return true;
}

static char settings[1024];

const char* RBGetSettings()
{
	strcpy(settings,"");

	if(RagePlatform::GetPostProcessVehicles())
		strcpy(settings,"ppv=true;");
	else
		strcpy(settings,"ppv=false;");

	if(RagePlatform::GetPPUOnlyHint())
		strcpy(settings,"cetc=true;");
	else
		strcpy(settings,"cetc=false;");

	return settings;
}

bool RBSetSettings(const char* newsettings)
{
	if(stricmp(newsettings,"ppv=true;") == 0)
		RagePlatform::SetPostProcessVehicles(true);
	else
		RagePlatform::SetPostProcessVehicles(false);

	if(stricmp(newsettings,"cetc=true;") == 0)
		RagePlatform::SetPPUOnlyHint(true);
	else
		RagePlatform::SetPPUOnlyHint(false);

	return true;
}

bool RBCompressAnim(const char* srcfile, const char* skelfile, const char* outfile, int maxblocksize, float compressionrotation, float compressiontranslation, float compressionscale, float compressiondefault, int compressioncost, int decompressioncost, const char* compressionrules, bool rawonly )
{
	return RagePlatform::CompressAnim(srcfile, skelfile, outfile, maxblocksize, compressionrotation, compressiontranslation, compressionscale, compressiondefault, compressioncost, decompressioncost, compressionrules, rawonly );
}

const char* RBGetClipPropertyString(const char* propertyname, const char* clipfile, bool useLoader)
{
	return RagePlatform::GetClipPropertyString( propertyname, clipfile, useLoader);
}

int RBGetClipPropertyInt( const char* propertyname, const char* clipfile, bool useLoader )
{
	return RagePlatform::GetClipPropertyInt( propertyname, clipfile, useLoader );
}

float RBGetClipPropertyFloat( const char* propertyname, const char* clipfile, bool useLoader )
{
	return RagePlatform::GetClipPropertyFloat( propertyname, clipfile, useLoader);
}

bool RBSetClipPropertyString(const char* propertyname, const char* val, const char* clipfile, bool useLoader )
{
	return RagePlatform::SetClipPropertyString( propertyname, val, clipfile, useLoader );
}

bool RBConvertAndSaveDDS( const char* ddsFileSrc, const char* ddsFileDest, const char* templateName, const char* platform )
{
	return RagePlatform::ConvertAndSaveDDS( ddsFileSrc, ddsFileDest, templateName, platform );
}

bool RBValidateClip( const char* clipname )
{
	return RagePlatform::ValidateClip( clipname );
}

bool RBSetDwdType( const char * dwdtype )
{
	return RagePlatform::SetDwdType( dwdtype );
}

bool RBGenerateDrawableStats( const char* drawablePathname, const char* outputPathname )
{
	return RagePlatform::GenerateDrawableStats( drawablePathname, outputPathname );
}

bool RBGenerateFragmentStats( const char* fragmentPathname, const char* outputPathname )
{
	return RagePlatform::GenerateFragmentStats( fragmentPathname, outputPathname );
}

bool RBSetEdgify( bool value )
{
	return RagePlatform::SetEdgify( value );
}
