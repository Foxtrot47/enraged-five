//
// Generic utilities.
//
// Copyright (C) 1999-2005 Rockstar North.  All Rights Reserved. 
//

#include "utils.h"

// rage includes
#include "file/asset.h"
#include "file/device.h"
#include "file/stream.h"
#include "system/xtl.h"
#include "system/platform.h"
#include "system/param.h"

#include "ragebuilder.h"
#include "log.h"

using namespace rage;

namespace rage
{
	XPARAM(nopopups);
}

#define CopyFile_BUFFER_SIZE (10 * 1024 * 1024)
static char pBuffer[CopyFile_BUFFER_SIZE];

// Return the relative path to a file as dest.
void RelativePath(char *dest, int maxLen, const char *base) 
{
	char path[MAX_PATH];
	char stacked[MAX_PATH];

	ASSET.FullPath(path, MAX_PATH, base, "");
	ASSET.GetStackedPath(stacked, MAX_PATH);

	int s = 0;
	while(path[s] == stacked[s] && path[s] != '\0')
	{
		s++;
	}

	strncpy( dest, &path[s], maxLen );
}

// Return a file time
u64	Get_FileTime(const char* file)
{
	const fiDevice* pDevice = fiDevice::GetDevice( file );
	if( pDevice )
	{
		return pDevice->GetFileTime( file );
	}
	return 0;
}

rage::u64	Get_FileSize(const char* file)
{
	const fiDevice* pDevice = fiDevice::GetDevice( file );
	if( pDevice )
	{
		return pDevice->GetFileSize( file );
	}
	return 0;
}

// Push the relative path to a file. Use ASSET.PopFolder to 
// return to the old path.
void SetRelativePathToFile(const char* filename,const char* extra)
{
	char path[MAX_PATH];
	char relative[MAX_PATH];

	ASSET.SetPath("");

	SplitFilename( filename, path, NULL, NULL );
	RelativePath( relative, MAX_PATH, path );

	if((extra != NULL) && (extra[0] != '\0'))
	{
		sprintf(path,"%s;%s",extra,relative);
		strcpy(relative,path);
	}

	ASSET.SetPath(relative);

	//ASSET.PushFolder(relative);
}


// Push the relative path to a file. Use ASSET.PopFolder to 
// return to the old path.
void PushRelativePathToFile(const char* filename,const char* extra)
{
	char path[MAX_PATH];
	char relative[MAX_PATH];

	SplitFilename( filename, path, NULL, NULL );
	RelativePath( relative, MAX_PATH, path );

	ASSET.PushFolder(relative);
}

// Split a filename into constituent parts.
// Can specify NULL for each of path, basename or extension.
void SplitFilename( const char* filename, char* path, char* basename, char* extension )
{
	char fullname[MAX_PATH];

	strcpy( fullname, filename );

	// extract extension
	char* pDot = strrchr(fullname, '.');
	if( pDot )
	{
		if( extension )
		{
			strcpy( extension, pDot + 1 );
		}
		
		*pDot = '\0';
	}
	else
	{
		if( extension )
		{
			extension[0] = '\0';
		}
	}

	// Construct the full path for all the files that this filter will find
	char* pSlash = strrchr(fullname, '\\');
	if( pSlash == NULL )
	{
		pSlash = strrchr(fullname, '/');
	}

	if( pSlash )
	{
		if( basename )
		{
			strcpy( basename, pSlash + 1 );
		}
		*(pSlash+1) = '\0';
		if( path )
		{
			strcpy( path, fullname );
		}
	}
	else
	{
		if( path )
		{
			path[0] = '\0';
		}
		if( basename )
		{
			strcpy( basename, fullname );
		}
	}
}

#if 0 // __WIN32PC // KS - Hack - always use device independent version
#pragma warning(disable: 4668)	
#include <windows.h>

// Split into a WIN32PC Version and a device independent version below. The device independent version has 
// weaker pattern (*.*) searching of files. Neither descends recursively into subfolders.

//
// name:		AddFilesToList
// description:	Add files covered by a file filter to a list.
void AddFilesToList(const char* pFilter, string_list& list)
{
	char fullpath[256];

	char filterpath[256];
	char filterbase[256];
	char filterextension[256];
	SplitFilename( pFilter, filterpath, filterbase, filterextension );

	// Construct the full path for FindFirstFile
	ASSET.FullPath(fullpath, 256, pFilter, "");
	WIN32_FIND_DATA findData;
	HANDLE handle;

	handle = FindFirstFile(fullpath, &findData);
	while(handle != INVALID_HANDLE_VALUE)
	{
		if(findData.cFileName[0] != '.' && !(findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
		{
			// construct relative filename
			char filename[256];
			strcpy(filename, filterpath);
			strcat(filename, findData.cFileName);

			list.push_back(filename);
		}
		if(!FindNextFile(handle, &findData))
			break;
	}
	FindClose(handle);
}

#else // __WIN32PC


#include "file/asset.h"

// structure to pass some user data to CB_AddFileToList.
typedef struct _CB_AddFileToList_UserData
{	
	const char*					m_szPath;
	const char*					m_szBase;
	const char*					m_szExtension;
	string_list*				m_pList;
	bool						m_bRelative;
	bool						m_bSorted;
	bool						m_bFilterDuplicateBasenames;
} CB_AddFileToList_UserData;

bool MatchPattern(const char* pattern, const char* name)
{
	bool match = false;
	const char* wildcard = strchr(pattern, '*');

	if (wildcard)
	{
		if(strcmp(wildcard, pattern) == 0)
		{
			++wildcard;
			const char* hit = (strstr(name, wildcard));
			if(hit)
			{
				while(hit && strlen(hit) > strlen(wildcard) && strlen(wildcard) != 0)
				{
					for(unsigned int i=0; i<strlen(wildcard); ++i)
						++hit;
					hit = (strstr(hit, wildcard));
				}
			}
			if(hit)
				match = true;
		}
		else
		{
			int length = wildcard - pattern;

			int cmpResult = (strnicmp(pattern, name, length));
			match = (length > 0)?(cmpResult==0):true;
		}
	}
	else
	{
		match = (stricmp(pattern, name)==0);
	}

	return match;
}

// Return true when the found file matches the filter.
bool FileMatchesPattern(const fiFindData &findData, CB_AddFileToList_UserData* pUserData)
{
	char findname[MAX_PATH];
	strcpy( findname, pUserData->m_szPath );
	strcat( findname, findData.m_Name );

	char dirpath[MAX_PATH];
	char basename[MAX_PATH];
	char extension[MAX_PATH]; // extensions are no longer necessarily 3 char long
	SplitFilename( findname, dirpath, basename, extension );

	if(findData.m_Name[0] != '.' && !( findData.m_Attributes & FILE_ATTRIBUTE_DIRECTORY ))	// don't know whether this one even occurs, but there...
	{
		return (MatchPattern(pUserData->m_szBase, basename) && MatchPattern(pUserData->m_szExtension, extension));
	}

	return false;
}

// Return true when the found file matches the platform.
bool FileMatchesPlatform(const fiFindData &findData, CB_AddFileToList_UserData* pUserData)
{
	const char* pExt = ASSET.FindExtensionInPath(findData.m_Name);
	if( pExt )
	{
		return RageBuilder::DoesExtensionMatchPlatform(pExt + 1); // extension includes the '.'
	}

	return true;
}

// Add the file to the list - check for duplicates.
void AddFileToList(const fiFindData &findData, CB_AddFileToList_UserData* pUserData)
{
	char findname[MAX_PATH];
	strcpy( findname, pUserData->m_szPath );
	strcat( findname, findData.m_Name );

	char path[MAX_PATH];
	if( !pUserData->m_bRelative )
	{
		ASSET.FullPath( path, MAX_PATH, findname, "" );
	}
	else
	{
		//strncpy(path, findname, MAX_PATH);
		strncpy(path, findData.m_Name, MAX_PATH);
	}

	// don't add duplicates
	if(pUserData->m_bSorted)
		InsertIntoSortedList( *pUserData->m_pList, &path[0] );
	else
		InsertIntoUnSortedList( *pUserData->m_pList, &path[0] );

	/* for( string_list::iterator it = pUserData->m_pList->begin(); it != pUserData->m_pList->end(); ++it )
	{
		if( stricmp( (*it).c_str(), path ) == 0 )
		{
			return; 
		}
	}

	pUserData->m_pList->push_back( path );*/
}

// Add a file covered by a filter to a list
void CB_AddFileToList(const fiFindData &findData, void *userArg)
{
	CB_AddFileToList_UserData* pUserData = (CB_AddFileToList_UserData*) userArg;

	// Determine if we already have a basename match if no duplicates are allowed.
	bool skip = false;
	if ( pUserData->m_bFilterDuplicateBasenames )
	{
		char findname[MAX_PATH];
		strcpy( findname, pUserData->m_szPath );
		strcat( findname, findData.m_Name );

		char dirpath[MAX_PATH];
		char basename[MAX_PATH];
		char extension[MAX_PATH]; // extensions are no longer necessarily 3 char long
		SplitFilename( findname, dirpath, basename, extension );

		for ( string_list::iterator it = pUserData->m_pList->begin();
			it != pUserData->m_pList->end();
			++it )
		{
			char dir[MAX_PATH];
			char base[MAX_PATH];
			char ext[MAX_PATH]; // extensions are no longer necessarily 3 char long
			SplitFilename( (*it).c_str(), dir, base, ext );

			// If basenames match then skip.
			if ( 0 == stricmp( base, basename ) )
			{
				// Displayf( "Skipping: %s", findData.m_Name );
				skip = true;
			}
		}
	}

	if (!skip && FileMatchesPattern(findData, pUserData))
	{
		// Displayf( "Adding: %s", findData.m_Name );
		AddFileToList( findData, pUserData );
	}
}

// Add a file covered by a filter to a list if platform specific or neutral.
void CB_AddPlatformFileToList(const fiFindData &findData, void *userArg)
{
	CB_AddFileToList_UserData* pUserData = (CB_AddFileToList_UserData*) userArg;

	if (FileMatchesPattern(findData, pUserData) &&
		FileMatchesPlatform(findData, pUserData))
	{
		AddFileToList( findData, pUserData );
	}
}

//
// name:		AddFilesToList
// description:	Add files covered by a file filter to a list
//
// This device independent function is less powerful than the original 
// windows version, as it only supports a more basic filter at the end
// of a path using *.* or *.ext only! It'll all be handled on PC later
// anyway.
// If bRelative is true, the file names will be added as relative paths. 
// If false, the full path will be used.
void AddFilesToList(const char* pFilter, string_list& list, bool bRelative, bool bSorted, bool bFilterDuplicateBasenames )
{
	char dirpath[MAX_PATH];
	char basename[MAX_PATH];
	char extension[MAX_PATH]; // extensions are no longer necessarily 3 char long

	SplitFilename( pFilter, dirpath, basename, extension );

	CB_AddFileToList_UserData userData = 
		{
			dirpath,
			basename,
			extension,
			&list,
			bRelative,
			bSorted,
			bFilterDuplicateBasenames
		};

	ASSET.EnumFiles( dirpath, CB_AddFileToList, &userData );
}

// Add only platform specific and neutral files to a list of files according to filter.
void AddPlatformFilesToList(const char* pFilter, string_list& list, bool bRelative, bool bSorted )
{
	char dirpath[MAX_PATH];
	char basename[MAX_PATH];
	char extension[MAX_PATH]; // extensions are no longer necessarily 3 char long

	SplitFilename( pFilter, dirpath, basename, extension );

	CB_AddFileToList_UserData userData = 
	{
		dirpath,
		basename,
		extension,
		&list,
		bRelative,
		bSorted,
		false
	};

	ASSET.EnumFiles( dirpath, CB_AddPlatformFileToList, &userData );
}

void AddSubDirToList(const char* pRoot, string_list& list)
{
	char dest[256];
	fiFindData data;

	sprintf(dest, "%s", pRoot);
	const fiDevice *d = fiDevice::GetDevice(dest, true);

	if (d) 
	{
		fiHandle handle = d->FindFileBegin(dest, data);
		if (fiIsValidHandle(handle)) 
		{
			do 
			{
				if ((data.m_Name[0] != '.') && 
					(data.m_Name[0] != '..') && 
					(data.m_Attributes & FILE_ATTRIBUTE_DIRECTORY)) 
				{

					list.push_back( data.m_Name );
				}
			} while (d->FindFileNext(handle,data));

			d->FindFileEnd(handle);
		}
	}
}

// Insert a string into an unsorted list - copies are discarded.
void InsertIntoUnSortedList( string_list& list, const char* name )
{
	string_list::iterator iBegin  = list.begin();
	string_list::iterator iEnd    = list.end();

	while (iBegin != iEnd)
	{
		if(stricmp(name, (*iBegin).c_str()) == 0)
			return;

		iBegin++;
	}

	list.insert(list.end(), name);
}

// Insert a string into a sorted list - copies are discarded.
void InsertIntoSortedList( string_list& list, const char* name )
{
	string_list::iterator iBegin  = list.begin();
	string_list::iterator iEnd    = list.end();
	string_list::iterator iSorter = iBegin;
	int steps   = list.size();

	while (iBegin != iEnd)
	{
		iSorter = iBegin;
		steps = (int)(steps / 2);

		for (int i = 0; i < steps; i++)
		{
			++iSorter;
		}

		int comp = stricmp(name, (*iSorter).c_str());

		if (comp == 0)
		{
			return;
		}
		else if (comp > 0)
		{
			iBegin = iSorter;
			if (steps == 0 && iBegin != iEnd)
			{
				++iBegin;
			}
		}
		else
		{
			iEnd = iSorter;
			if (steps == 0 && iEnd != iBegin)
			{
				--iEnd;
			}
		}
	}
	iSorter = iEnd;
	list.insert(iSorter, name);
}


// Fix a path to a normalised form - from rage::file/asset.h
void NormalisePathBack(char *dest, int destSize, const char *src) 
{
	safecpy(dest, src, destSize);
	if (dest[0] == 0)
	{
		return;
	}

	int sl = StringLength(dest);
	char *path = dest;

	// Convert to expected slash type
	while (*path) 
	{
		if (*path == SEPARATOR)
		{
			*path = OTHER_SEPARATOR;
		}
		path++;
	}

	// Tack on trailing appropriate slash if necessary
	if (sl && dest[--sl] != OTHER_SEPARATOR) 
	{
		dest[++sl] = OTHER_SEPARATOR;
		dest[++sl] = 0;
	}
}

// Fix a path to a normalised form - from rage::file/asset.h
void NormalisePath(char *dest, int destSize, const char *src) 
{
	safecpy(dest, src, destSize);
	if (dest[0] == 0)
	{
		return;
	}

	int sl = StringLength(dest);
	char *path = dest;

	// Convert to expected slash type
	while (*path) 
	{
		if (*path == OTHER_SEPARATOR)
		{
			*path = SEPARATOR;
		}
		path++;
	}

	// Tack on trailing appropriate slash if necessary
	if (sl && dest[--sl] != SEPARATOR) 
	{
		dest[++sl] = SEPARATOR;
		dest[++sl] = 0;
	}
}


// Does the directory exist.
bool DirExists(const char* path)
{
	fiFindData data;
	char normalised[256];

	const fiDevice* d = fiDevice::GetDevice(path, true);

	// in case of duff data
	ASSET.RemoveNameFromPath(normalised, 256, path);

	if (d) 
	{
		fiHandle handle = d->FindFileBegin(normalised, data);
		if (fiIsValidHandle(handle)) 
		{
			d->FindFileEnd(handle);
			return true; // if the folder contains anything, it must exist
		}
	}

	return false;
}

// copy a file by funneling the contents
bool CopyFileFunnel(const char* szSource, const char* szDestination)
{
	fiStream* pSource = ASSET.Open(szSource, "" );
	if (!pSource)
	{
		return false;
	}

	fiStream* pDestination = ASSET.Create(szDestination, "");
	if (!pDestination)
	{
		pSource->Close();
		Errorf( "CopyFileFunnel: failed to open destination file %s", szDestination );
		return false;
	}

	int iSize = pSource->Size();
	while (iSize)
	{
		int iChunk = (iSize > CopyFile_BUFFER_SIZE)?CopyFile_BUFFER_SIZE:iSize;
		int iRead = pSource->Read(pBuffer, iChunk );
		if( iRead != iChunk )
		{
			Errorf( "CopyFileFunnel: failed to read from source file" );
			pSource->Close();
			pDestination->Close();
			return false;
		}
		int iWrote = pDestination->Write( pBuffer, iRead );
		if( iWrote != iChunk )
		{
			Errorf( "CopyFileFunnel: failed to write to destination file" );
			pSource->Close();
			pDestination->Close();
			return false;
		}
		iSize -= iRead;
	}

	pSource->Close();
	pDestination->Close();

	// Lets ensure we set the modified time for the file.
	const fiDevice* pSrcDevice = fiDevice::GetDevice( szSource, true );
	if ( pSrcDevice )
	{
		u64 mtime = pSrcDevice->GetFileTime( szSource );
		const fiDevice* pDstDevice = fiDevice::GetDevice( szDestination );
		
		if ( pDstDevice && ( mtime > 0 ) )
			pDstDevice->SetFileTime( szDestination, mtime );
	}

	return true;
}

bool ReadLine(rage::fiStream* fileRead,char* buffer,int maxSize)
{
	bool StartedLine = false;
	int index = 0;

	while(fileRead->Tell() != fileRead->Size())
	{
		char In = (char) fileRead->GetCh();

		if(!StartedLine)
			if(In != '\n')
				StartedLine = true;	

		if(StartedLine)
		{
			if(In == '\n')
			{
				buffer[index] = '\0';
				return true;
			}
			else
			{
				buffer[index] = In;
				index++;

				if(index == maxSize)
					return true;
			}
		}
	}

	return false;
}


// copied from rscbuilder.cpp - handles the # in platform specific extensions.
char* ConstructName(char *dest,int destSize,const char *path,const char *ext) {
	char extBuf[16];
	extBuf[0] = g_sysPlatform;
	Assertf(ext[0] == '#',"Extension should start with '#', replaced by platform id");
	safecpy(extBuf+1,ext+1,sizeof(extBuf)-1);
	ASSET.FullPath(dest,destSize,path,extBuf);
	char *s = dest;
	// Replace platform spec anywhere else it may appear in the path, useful for directory names.
	while (*s) {
		if (*s == '#')
			*s = g_sysPlatform;
		++s;
	}
	return dest;
}

void CheckFileSize(const char* filename, const char* ext, size_t size)
{
	unsigned int count = 0;
	bool success = false;

	char fullpath[RAGE_MAX_PATH];
	ConstructName(fullpath, RAGE_MAX_PATH, filename, ext);

	do
	{
		const fiDevice* pDevice = fiDevice::GetDevice(fullpath);
		Assert(pDevice);
		uint64 filesize = pDevice->GetFileSize(fullpath);

		success = (filesize == size);
		
		if (!success)
		{
			int errorcode = GetLastError();
			char message[1024];
			sprintf(message, "File size on disk does not match for %s (contact Greg) (%d) - attempt %d", filename, errorcode, ++count);

			if (!PARAM_nopopups.Get())
			{
				char caption[512];
				sprintf(caption, "Fatal Error: Size mismatch (errorcode %d)", errorcode);
				MessageBox(0, message, caption, MB_ICONERROR);
			}

			if (count < 5)
			{
				Warningf(message);
				sysIpcSleep(1000);
			}
			else
			{
				Quitf(message);
			}
		}
	}
	while (!success);
}

/**
 * @brief Return std::string representing a file modified date/time.
 *
 * The format of output is "YYYY-MM-DD HH:MM:SS".
 */
std::string
Get_FileTimeString( const char* filename )
{
	rage::u64 time = Get_FileTime( filename );
	FILETIME ft;
	ft.dwHighDateTime = (DWORD)(( time & 0xFFFFFFFF00000000 ) >> 32);
	ft.dwLowDateTime = (DWORD)( time & 0x00000000FFFFFFFF );
	SYSTEMTIME stUtc, stLocal;
	FileTimeToSystemTime( &ft, &stUtc );
	SystemTimeToTzSpecificLocalTime( NULL, &stUtc, &stLocal );

	char data[MAX_PATH];
	memset( data, 0, sizeof(char) * MAX_PATH );
	sprintf_s( data, MAX_PATH, 
		"%d-%02d-%02d %02d:%02d:%02d",
		stLocal.wYear, stLocal.wMonth, stLocal.wDay,
		stLocal.wHour, stLocal.wMinute, stLocal.wSecond );
	return ( std::string( data ) );
}

/**
 * @brief Write file modified list (tFileMTimeList) to a specified file.
 *
 * The modified time list is written out to then be packed with the IMG or RPF
 * file.  It is currently in a format supported by our Ruby resourcing
 * class.
 */
void
WriteFileMTimeList( const char* filename, const tFileMTimeList& filelist )
{
	rage::fiStream* ofs = rage::fiStream::Create( filename );
	if ( !ofs )
		return;

	for ( tFileMTimeListConstIter it = filelist.begin();
		it != filelist.end();
		++it )
	{
		fprintf( ofs, "%s|%s\r\n", (*it).first.c_str(), (*it).second.c_str() );
	}
	ofs->Close( );
}

/**
/*
 *
**/
bool IsPowerOfTwo(u32 x)
{
	return ((x != 0) && !(x & (x - 1)));
}
/**
/*
 *
**/
bool IsMultipleOfFour(u32 x)
{
	return ((x != 0) && (x%4 ==0));
}

#endif // __WIN32PC
