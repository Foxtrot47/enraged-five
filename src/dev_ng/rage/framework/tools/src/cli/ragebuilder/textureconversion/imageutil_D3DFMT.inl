// ========================================================
// imageutil_D3DFMT.h
// Copyright (c) 2010 Rockstar North.  All Rights Reserved.
// ========================================================

// here be dragons ...

#include "grcore/dds.h"

namespace rage {

class CFloat32 // TODO -- merge with CFloat16_opt, and Float16, and whatever else we have .. move into rage/math?
{
public:
#pragma warning(disable : 4201)
	union
	{
		struct { f32 f; };
		struct { u32 u; };
#if __WIN32PC
		struct { u32 m:23, e:8, s:1; };
#else
		struct { u32 s:1, e:8, m:23; };
#endif
	};
#pragma warning(default : 4201)

	__forceinline CFloat32() {}
	__forceinline CFloat32(f32 f_) : f(f_) {}
	__forceinline CFloat32(u32 u_) : u(u_) {}
#if __WIN32PC
	__forceinline CFloat32(u32 m_, u32 e_, u32 s_) : m(m_), e(e_), s(s_) {}
#else
	__forceinline CFloat32(u32 m_, u32 e_, u32 s_) : s(s_), e(e_), m(m_) {}
#endif

	__forceinline float GetFloat() const
	{
		return f;
	}

	__forceinline void SetFloat(float x)
	{
		f = x;
	}
};

class CFloat16 // TODO -- merge with CFloat16_opt, and Float16, and whatever else we have .. move into rage/math?
{
public:
#pragma warning(disable : 4201)
	union
	{
		struct { u16 u; };
#if __WIN32PC
		struct { u16 m:10, e:5, s:1; };
#else
		struct { u16 s:1, e:5, m:10; };
#endif
	};
#pragma warning(default : 4201)

	__forceinline CFloat16() {}
	__forceinline CFloat16(f32 f_) { SetFloat(f_); }
	__forceinline CFloat16(u16 u_) : u(u_) {}
#if __WIN32PC
	__forceinline CFloat16(u16 m_, u16 e_, u16 s_) : m(m_), e(e_), s(s_) {}
#else
	__forceinline CFloat16(u16 m_, u16 e_, u16 s_) : s(s_), e(e_), m(m_) {}
#endif

	__forceinline float GetFloat() const
	{
		return (m|e) ? CFloat32(m<<(23-10),e+127-15,s).GetFloat() : (s ? -0.0f : +0.0f);
	}

	__forceinline void SetFloat(float x)
	{
		CFloat32 temp(x);

		const int b = (int)temp.e - (127-15);

		if      (b < 0x0000L) { m = 0x0000; e = 0x0000; } // underflow 
		else if (b > 0x001fL) { m = 0x03ff; e = 0x001f; } // overflow
		else
		{
			m = u16(temp.m>>(23-10));
			e = u16(b);
		}

		s = u16(temp.s);
	}
};

#define CPixel_D3DFMT_INTERFACE_V() \
	__forceinline Vector4 GetV() const \
	{ \
		return Vector4(GetR(), GetG(), GetB(), GetA()); \
	} \
	__forceinline void SetV(const Vector4& v_) \
	{ \
		SetR(v_.x); \
		SetG(v_.y); \
		SetB(v_.z); \
		SetA(v_.w); \
	} \
	__forceinline void SetRGBAComponents__NOT_WORKING_WTF(float r_, float g_, float b_, float a_) \
	{ \
		SetR(r_); \
		SetG(r_); \
		SetB(r_); \
		SetA(r_); \
	} \
	// end.

#define CPixel_D3DFMT_INTERFACE_RGBA(F) \
	    CPixel_D3DFMT_INTERFACE_RGB (F) \
	    CPixel_D3DFMT_INTERFACE_A   (F) \
	    // end.

#define CPixel_D3DFMT_INTERFACE_RGB(F) \
	__forceinline float GetR() const { return Dequantise<T,R,F>(r); } \
	__forceinline float GetG() const { return Dequantise<T,G,F>(g); } \
	__forceinline float GetB() const { return Dequantise<T,B,F>(b); } \
	__forceinline float GetL() const \
	{ \
		return Max<float>(GetR(), GetG(), GetB()); \
	} \
	__forceinline void SetR(float r_) { r = Quantise<T,R,F>(r_); } \
	__forceinline void SetG(float g_) { g = Quantise<T,G,F>(g_); } \
	__forceinline void SetB(float b_) { b = Quantise<T,B,F>(b_); } \
	__forceinline void SetL(float l_) \
	{ \
		SetR(l_); \
		SetG(l_); \
		SetB(l_); \
	} \
	CPixel_D3DFMT_INTERFACE_V() \
	// end.

#define CPixel_D3DFMT_INTERFACE_RG(F) \
	__forceinline float GetR() const { return Dequantise<T,R,F>(r); } \
	__forceinline float GetG() const { return Dequantise<T,G,F>(g); } \
	__forceinline float GetL() const \
	{ \
		return Max<float>(GetR(), GetG()); \
	} \
	__forceinline void SetR(float r_) { r = Quantise<T,R,F>(r_); } \
	__forceinline void SetG(float g_) { g = Quantise<T,G,F>(g_); } \
	__forceinline void SetL(float l_) \
	{ \
		SetR(l_); \
		SetG(l_); \
		SetB(l_); \
	} \
	CPixel_D3DFMT_INTERFACE_V() \
	// end.

#define CPixel_D3DFMT_INTERFACE_R(F) \
	__forceinline float GetR() const { return Dequantise<T,R,F>(r); } \
	__forceinline float GetL() const \
	{ \
		return GetR(); \
	} \
	__forceinline void SetR(float r_) { r = Quantise<T,R,F>(r_); } \
	__forceinline void SetL(float l_) \
	{ \
		SetR(l_); \
	} \
	CPixel_D3DFMT_INTERFACE_V() \
	// end.

#define CPixel_D3DFMT_INTERFACE_A(F) \
	__forceinline float GetA() const \
	{ \
		return Dequantise<T,A,F>(a); \
	} \
	__forceinline void SetA(float a_) \
	{ \
		a = Quantise<T,A,F>(a_); \
	} \
	// end.

#define CPixel_D3DFMT_INTERFACE_L(F) \
	__forceinline float GetR() const { return GetL(); } \
	__forceinline float GetG() const { return GetL(); } \
	__forceinline float GetB() const { return GetL(); } \
	__forceinline float GetL() const { return Dequantise<T,L,F>(l); } \
	__forceinline Vector4 GetV() const \
	{ \
		const float x = GetL(); \
		const float y = GetA(); \
		return Vector4(x, x, x, y); \
	} \
	__forceinline void SetR(float r_) { l = Quantise<T,L,F>(r_); } \
	__forceinline void SetG(float g_) { l = Quantise<T,L,F>(g_); } \
	__forceinline void SetB(float b_) { l = Quantise<T,L,F>(b_); } \
	__forceinline void SetL(float l_) { l = Quantise<T,L,F>(l_); } \
	__forceinline void SetV(const Vector4& v_) \
	{ \
		SetL(Max<float>(v_.x, v_.y, v_.z)); \
		SetA((v_.w)); \
	} \
	// end.

template <        int R_, int G_, int B_, bool F_ = false> class CPixel_D3DFMT_RGB ;
template <int A_, int R_, int G_, int B_, bool F_ = false> class CPixel_D3DFMT_ARGB;
template <int A_, int R_, int G_, int B_, bool F_ = false> class CPixel_D3DFMT_XRGB;
template <int A_, int B_, int G_, int R_, bool F_ = false> class CPixel_D3DFMT_ABGR;
template <int A_, int B_, int G_, int R_, bool F_ = false> class CPixel_D3DFMT_XBGR;
template <int A_                        , bool F_ = false> class CPixel_D3DFMT_A   ;
template <        int L_                , bool F_ = false> class CPixel_D3DFMT_L   ;
template <                        int R_, bool F_ = false> class CPixel_D3DFMT_R   ;
template <int A_, int L_                , bool F_ = false> class CPixel_D3DFMT_AL  ;
template <                int G_, int R_, bool F_ = false> class CPixel_D3DFMT_GR  ;

class CPixel_D3DFMT_
{
protected:
	__forceinline float GetR() const { return 0.0f; }
	__forceinline float GetG() const { return 0.0f; }
	__forceinline float GetB() const { return 0.0f; }
	__forceinline float GetA() const { return 1.0f; }
	__forceinline float GetL() const { return 0.0f; }

	__forceinline void SetR(float r_) {}
	__forceinline void SetG(float g_) {}
	__forceinline void SetB(float b_) {}
	__forceinline void SetA(float a_) {}
	__forceinline void SetL(float l_) {}

	template <typename T, int N_, int shl_> static __forceinline T BitMask()
	{
		STATIC_ASSERT(N_ <= 8*SIZEOF(T), oops);

		if      (N_ == 0) { return T(0);         }
		else if (N_ == 1) { return T(1) << shl_; }

		return ((((T(1) << (N_ - 1)) - T(1)) << 1) | T(1)) << shl_;
	}

	template <typename T, int N_, bool F_> static __forceinline float Dequantise(T x)
	{
		STATIC_ASSERT(!F_, dequantise_only_supports_16_or_32_bit_floats);
		STATIC_ASSERT(N_ <= 8*SIZEOF(T), oops);

		return (float)x/(float)BitMask<T,N_,0>();
	}

	template <> static __forceinline float Dequantise<u16,16,true>(u16 x) { return CFloat16(u16(x)).GetFloat(); }
	template <> static __forceinline float Dequantise<u32,16,true>(u32 x) { return CFloat16(u16(x)).GetFloat(); }
	template <> static __forceinline float Dequantise<u64,16,true>(u64 x) { return CFloat16(u16(x)).GetFloat(); }

	template <> static __forceinline float Dequantise<u32,32,true>(u32 x) { return CFloat32(u32(x)).GetFloat(); }
	template <> static __forceinline float Dequantise<u64,32,true>(u64 x) { return CFloat32(u32(x)).GetFloat(); }

	template <typename T, int N_, bool F_> static __forceinline T Quantise(float x)
	{
		STATIC_ASSERT(!F_, quantise_only_supports_16_or_32_bit_floats);
		STATIC_ASSERT(N_ <= 8*SIZEOF(T), oops);

		const float maxVal = (float)BitMask<T,N_,0>();
		return T(Clamp<float>(x*maxVal + 0.5f, 0.0f, maxVal));
	}

	template <> static __forceinline u16 Quantise<u16,16,true>(float x) { return CFloat16(x).u; }
	template <> static __forceinline u32 Quantise<u32,16,true>(float x) { return CFloat16(x).u; }
	template <> static __forceinline u64 Quantise<u64,16,true>(float x) { return CFloat16(x).u; }

	template <> static __forceinline u32 Quantise<u32,32,true>(float x) { return CFloat32(x).u; }
	template <> static __forceinline u64 Quantise<u64,32,true>(float x) { return CFloat32(x).u; }
};

template <int R_, int G_, int B_, bool F_> class CPixel_D3DFMT_RGB : public CPixel_D3DFMT_
{
public:
	CPixel_D3DFMT_INTERFACE_RGB(F_);

	enum { N = R_+G_+B_ }; typedef typename CType<N>::T_unsigned T;
	enum { B = B_ }; T b:B;
	enum { G = G_ }; T g:G;
	enum { R = R_ }; T r:R;

	__forceinline CPixel_D3DFMT_RGB() { STATIC_ASSERT(SIZEOF(*this) == SIZEOF(T), CPixel_D3DFMT_RGB_wrong_sizeof); }
	__forceinline CPixel_D3DFMT_RGB(int) : b(0), g(0), r(0) {}
};

// this specialisation is required because CType<24>::T_unsigned is void ..
template <> class CPixel_D3DFMT_RGB<8,8,8,false> : public CPixel_D3DFMT_
{
public:
	CPixel_D3DFMT_INTERFACE_RGB(false);

	typedef CType<8>::T_unsigned T;
	enum { B = 8 }; T b;
	enum { G = 8 }; T g;
	enum { R = 8 }; T r;

	__forceinline CPixel_D3DFMT_RGB() { STATIC_ASSERT(SIZEOF(*this) == 3*SIZEOF(T), CPixel_D3DFMT_RGB_8_8_8_false_wrong_sizeof); }
	__forceinline CPixel_D3DFMT_RGB(int) : b(0), g(0), r(0) {}
};

// this specialisation is required because CType<48>::T_unsigned is void ..
template <> class CPixel_D3DFMT_RGB<16,16,16,false> : public CPixel_D3DFMT_
{
public:
	CPixel_D3DFMT_INTERFACE_RGB(false);

	typedef CType<16>::T_unsigned T;
	enum { B = 16 }; T b;
	enum { G = 16 }; T g;
	enum { R = 16 }; T r;

	__forceinline CPixel_D3DFMT_RGB() { STATIC_ASSERT(SIZEOF(*this) == 3*SIZEOF(T), CPixel_D3DFMT_RGB_16_16_16_false_wrong_sizeof); }
	__forceinline CPixel_D3DFMT_RGB(int) : b(0), g(0), r(0) {}
};

template <int A_, int R_, int G_, int B_, bool F_> class CPixel_D3DFMT_ARGB : public CPixel_D3DFMT_
{
public:
	CPixel_D3DFMT_INTERFACE_RGBA(F_);

	enum { N = A_+R_+G_+B_ }; typedef typename CType<N>::T_unsigned T;
	enum { B = B_ }; T b:B;
	enum { G = G_ }; T g:G;
	enum { R = R_ }; T r:R;
	enum { A = A_ }; T a:A;

	__forceinline CPixel_D3DFMT_ARGB() { STATIC_ASSERT(SIZEOF(*this) == SIZEOF(T), CPixel_D3DFMT_ARGB_wrong_sizeof); }
	__forceinline CPixel_D3DFMT_ARGB(int) : b(0), g(0), r(0), a(0) {}
};

template <int A_, int R_, int G_, int B_, bool F_> class CPixel_D3DFMT_XRGB : public CPixel_D3DFMT_
{
public:
	CPixel_D3DFMT_INTERFACE_RGB(F_);

	enum { N = A_+R_+G_+B_ }; typedef typename CType<N>::T_unsigned T;
	enum { B = B_ }; T b:B;
	enum { G = G_ }; T g:G;
	enum { R = R_ }; T r:R;
	enum { A = A_ }; T x:A;

	__forceinline CPixel_D3DFMT_XRGB() { STATIC_ASSERT(SIZEOF(*this) == SIZEOF(T), CPixel_D3DFMT_XRGB_wrong_sizeof); }
	__forceinline CPixel_D3DFMT_XRGB(int) : b(0), g(0), r(0), x(0) {}
};

template <int A_, int B_, int G_, int R_, bool F_> class CPixel_D3DFMT_ABGR : public CPixel_D3DFMT_
{
public:
	CPixel_D3DFMT_INTERFACE_RGBA(F_);

	enum { N = A_+B_+G_+R_ }; typedef typename CType<N>::T_unsigned T;
	enum { R = R_ }; T r:R;
	enum { G = G_ }; T g:G;
	enum { B = B_ }; T b:B;
	enum { A = A_ }; T a:A;

	__forceinline CPixel_D3DFMT_ABGR() { STATIC_ASSERT(SIZEOF(*this) == SIZEOF(T), CPixel_D3DFMT_ABGR_wrong_sizeof); }
	__forceinline CPixel_D3DFMT_ABGR(int) : r(0), g(0), b(0), a(0) {}
};

// this specialisation is required because CType<128>::T_unsigned is void ..
template <> class CPixel_D3DFMT_ABGR<32,32,32,32,true> : public CPixel_D3DFMT_
{
public:
	CPixel_D3DFMT_INTERFACE_V();

	typedef CType<32>::T_float T;
	T r; __forceinline float GetR() const { return float(r); } __forceinline void SetR(float r_) { r = T(r_); }
	T g; __forceinline float GetG() const { return float(g); } __forceinline void SetG(float g_) { g = T(g_); }
	T b; __forceinline float GetB() const { return float(b); } __forceinline void SetB(float b_) { b = T(b_); }
	T a; __forceinline float GetA() const { return float(a); } __forceinline void SetA(float a_) { a = T(a_); }

	__forceinline CPixel_D3DFMT_ABGR() { STATIC_ASSERT(SIZEOF(*this) == 4*SIZEOF(T), CPixel_D3DFMT_ABGR_32_32_32_true_wrong_sizeof); }
	__forceinline CPixel_D3DFMT_ABGR(int) : r(0), g(0), b(0), a(0) {}
};

template <int A_, int B_, int G_, int R_, bool F_> class CPixel_D3DFMT_XBGR : public CPixel_D3DFMT_
{
public:
	CPixel_D3DFMT_INTERFACE_RGB(F_);

	enum { N = A_+B_+G_+R_ }; typedef typename CType<N>::T_unsigned T;
	enum { R = R_ }; T r:R;
	enum { G = G_ }; T g:G;
	enum { B = B_ }; T b:B;
	enum { A = A_ }; T x:A;

	__forceinline CPixel_D3DFMT_XBGR() { STATIC_ASSERT(SIZEOF(*this) == SIZEOF(T), CPixel_D3DFMT_XBGR_wrong_sizeof); }
	__forceinline CPixel_D3DFMT_XBGR(int) : r(0), g(0), b(0), x(0) {}
};

template <int A_, bool F_> class CPixel_D3DFMT_A : public CPixel_D3DFMT_
{
public:
	CPixel_D3DFMT_INTERFACE_A(F_);
	CPixel_D3DFMT_INTERFACE_V();

	enum { N = A_ }; typedef typename CType<N>::T_unsigned T;
	enum { A = A_ }; T a:A;

	__forceinline CPixel_D3DFMT_A() { STATIC_ASSERT(SIZEOF(*this) == SIZEOF(T), CPixel_D3DFMT_A_wrong_sizeof); }
	__forceinline CPixel_D3DFMT_A(int) : a(0) {}
};

template <int L_, bool F_> class CPixel_D3DFMT_L : public CPixel_D3DFMT_
{
public:
	CPixel_D3DFMT_INTERFACE_L(F_);

	enum { N = L_ }; typedef typename CType<N>::T_unsigned T;
	enum { L = L_ }; T l:L;

	__forceinline CPixel_D3DFMT_L() { STATIC_ASSERT(SIZEOF(*this) == SIZEOF(T), CPixel_D3DFMT_L_wrong_sizeof); }
	__forceinline CPixel_D3DFMT_L(int) : l(0) {}
};

template <int R_, bool F_> class CPixel_D3DFMT_R : public CPixel_D3DFMT_
{
public:
	CPixel_D3DFMT_INTERFACE_R(F_);

	enum { N = R_ }; typedef typename CType<N>::T_unsigned T;
	enum { R = R_ }; T r:R;

	__forceinline CPixel_D3DFMT_R() { STATIC_ASSERT(SIZEOF(*this) == SIZEOF(T), CPixel_D3DFMT_R_wrong_sizeof); }
	__forceinline CPixel_D3DFMT_R(int) : r(0) {}
};

template <int A_, int L_, bool F_> class CPixel_D3DFMT_AL : public CPixel_D3DFMT_
{
public:
	CPixel_D3DFMT_INTERFACE_A(F_);
	CPixel_D3DFMT_INTERFACE_L(F_);

	enum { N = A_+L_ }; typedef typename CType<N>::T_unsigned T;
	enum { L = L_ }; T l:L;
	enum { A = A_ }; T a:A;

	__forceinline CPixel_D3DFMT_AL() { STATIC_ASSERT(SIZEOF(*this) == SIZEOF(T), CPixel_D3DFMT_AL_wrong_sizeof); }
	__forceinline CPixel_D3DFMT_AL(int) : l(0), a(0) {}
};

template <int G_, int R_, bool F_> class CPixel_D3DFMT_GR : public CPixel_D3DFMT_
{
public:
	CPixel_D3DFMT_INTERFACE_RG(F_);

	enum { N = G_+R_ }; typedef typename CType<N>::T_unsigned T;
	enum { R = R_ }; T r:R;
	enum { G = G_ }; T g:G;

	__forceinline CPixel_D3DFMT_GR() { STATIC_ASSERT(SIZEOF(*this) == SIZEOF(T), CPixel_D3DFMT_GR_wrong_sizeof); }
	__forceinline CPixel_D3DFMT_GR(int) : r(0), g(0) {}
};

#undef CPixel_D3DFMT_INTERFACE_V
#undef CPixel_D3DFMT_INTERFACE_RGB
#undef CPixel_D3DFMT_INTERFACE_RG
#undef CPixel_D3DFMT_INTERFACE_R
#undef CPixel_D3DFMT_INTERFACE_A
#undef CPixel_D3DFMT_INTERFACE_L

// ================================================================================================

template <DDS_D3DFORMAT fmt> class CPixel_D3DFMT {};
template <DDS_D3DFORMAT fmt> class CImage_D3DFMT {};

#if 1

#define DEF_CPixel_D3DFMT_RGB(     R_,G_,B_) template <> class CPixel_D3DFMT<DDS_D3DFMT_R       ##R_##G##G_##B##B_   > : public CPixel_D3DFMT_RGB <   R_,G_,B_     > { public: CPixel_D3DFMT() {} CPixel_D3DFMT(int) : CPixel_D3DFMT_RGB <   R_,G_,B_     >(0) {} }
#define DEF_CPixel_D3DFMT_ARGB( A_,R_,G_,B_) template <> class CPixel_D3DFMT<DDS_D3DFMT_A##A_##R##R_##G##G_##B##B_   > : public CPixel_D3DFMT_ARGB<A_,R_,G_,B_     > { public: CPixel_D3DFMT() {} CPixel_D3DFMT(int) : CPixel_D3DFMT_ARGB<A_,R_,G_,B_     >(0) {} }
#define DEF_CPixel_D3DFMT_XRGB( A_,R_,G_,B_) template <> class CPixel_D3DFMT<DDS_D3DFMT_X##A_##R##R_##G##G_##B##B_   > : public CPixel_D3DFMT_XRGB<A_,R_,G_,B_     > { public: CPixel_D3DFMT() {} CPixel_D3DFMT(int) : CPixel_D3DFMT_XRGB<A_,R_,G_,B_     >(0) {} }
#define DEF_CPixel_D3DFMT_ABGR( A_,B_,G_,R_) template <> class CPixel_D3DFMT<DDS_D3DFMT_A##A_##B##B_##G##G_##R##R_   > : public CPixel_D3DFMT_ABGR<A_,B_,G_,R_     > { public: CPixel_D3DFMT() {} CPixel_D3DFMT(int) : CPixel_D3DFMT_ABGR<A_,B_,G_,R_     >(0) {} }
#define DEF_CPixel_D3DFMT_XBGR( A_,B_,G_,R_) template <> class CPixel_D3DFMT<DDS_D3DFMT_X##A_##B##B_##G##G_##R##R_   > : public CPixel_D3DFMT_XBGR<A_,B_,G_,R_     > { public: CPixel_D3DFMT() {} CPixel_D3DFMT(int) : CPixel_D3DFMT_XBGR<A_,B_,G_,R_     >(0) {} }
#define DEF_CPixel_D3DFMT_A(    A_         ) template <> class CPixel_D3DFMT<DDS_D3DFMT_A##A_                        > : public CPixel_D3DFMT_A   <A_              > { public: CPixel_D3DFMT() {} CPixel_D3DFMT(int) : CPixel_D3DFMT_A   <A_              >(0) {} }
#define DEF_CPixel_D3DFMT_L(       L_      ) template <> class CPixel_D3DFMT<DDS_D3DFMT_L       ##L_                 > : public CPixel_D3DFMT_L   <   L_           > { public: CPixel_D3DFMT() {} CPixel_D3DFMT(int) : CPixel_D3DFMT_L   <   L_           >(0) {} }
#define DEF_CPixel_D3DFMT_R(             R_) template <> class CPixel_D3DFMT<DDS_D3DFMT_R                     ##R_   > : public CPixel_D3DFMT_R   <         R_     > { public: CPixel_D3DFMT() {} CPixel_D3DFMT(int) : CPixel_D3DFMT_R   <         R_     >(0) {} }
#define DEF_CPixel_D3DFMT_AL(   A_,L_      ) template <> class CPixel_D3DFMT<DDS_D3DFMT_A##A_##L##L_                 > : public CPixel_D3DFMT_AL  <A_,L_           > { public: CPixel_D3DFMT() {} CPixel_D3DFMT(int) : CPixel_D3DFMT_AL  <A_,L_           >(0) {} }
#define DEF_CPixel_D3DFMT_GR(         G_,R_) template <> class CPixel_D3DFMT<DDS_D3DFMT_G              ##G_##R##R_   > : public CPixel_D3DFMT_GR  <      G_,R_     > { public: CPixel_D3DFMT() {} CPixel_D3DFMT(int) : CPixel_D3DFMT_GR  <      G_,R_     >(0) {} }
#define DEF_CPixel_D3DFMT_RF(            R_) template <> class CPixel_D3DFMT<DDS_D3DFMT_R                     ##R_##F> : public CPixel_D3DFMT_R   <         R_,true> { public: CPixel_D3DFMT() {} CPixel_D3DFMT(int) : CPixel_D3DFMT_R   <         R_,true>(0) {} }
#define DEF_CPixel_D3DFMT_GRF(        G_,R_) template <> class CPixel_D3DFMT<DDS_D3DFMT_G              ##G_##R##R_##F> : public CPixel_D3DFMT_GR  <      G_,R_,true> { public: CPixel_D3DFMT() {} CPixel_D3DFMT(int) : CPixel_D3DFMT_GR  <      G_,R_,true>(0) {} }
#define DEF_CPixel_D3DFMT_ABGRF(A_,B_,G_,R_) template <> class CPixel_D3DFMT<DDS_D3DFMT_A##A_##B##B_##G##G_##R##R_##F> : public CPixel_D3DFMT_ABGR<A_,B_,G_,R_,true> { public: CPixel_D3DFMT() {} CPixel_D3DFMT(int) : CPixel_D3DFMT_ABGR<A_,B_,G_,R_,true>(0) {} }

#elif 1 // doesn't work ??

#define DEF_CPixel_D3DFMT(pat,args,fmt) \
	class CPixel_D3DFMT_##fmt : public CPixel_D3DFMT_##pat<MACRO_EXPAND args> \
	{ \
	public: \
		__forceinline CPixel_D3DFMT_##fmt() {} \
		__forceinline CPixel_D3DFMT_##fmt(int) : CPixel_D3DFMT_##pat<MACRO_EXPAND args>(0) {} \
	}; \
	typedef CImage<CPixel_D3DFMT_##fmt> CImage_D3DFMT_##fmt \
	// end.

#define DEF_CPixel_D3DFMT_RGB(     R_,G_,B_) DEF_CPixel_D3DFMT(RGB ,(   R_,G_,B_     ),R       ##R_##G##G_##B##B_   )
#define DEF_CPixel_D3DFMT_ARGB( A_,R_,G_,B_) DEF_CPixel_D3DFMT(ARGB,(A_,R_,G_,B_     ),A##A_##R##R_##G##G_##B##B_   )
#define DEF_CPixel_D3DFMT_XRGB( A_,R_,G_,B_) DEF_CPixel_D3DFMT(XRGB,(A_,R_,G_,B_     ),X##A_##R##R_##G##G_##B##B_   )
#define DEF_CPixel_D3DFMT_ABGR( A_,B_,G_,R_) DEF_CPixel_D3DFMT(ABGR,(A_,B_,G_,R_     ),A##A_##B##B_##G##G_##R##R_   )
#define DEF_CPixel_D3DFMT_XBGR( A_,B_,G_,R_) DEF_CPixel_D3DFMT(XBGR,(A_,B_,G_,R_     ),X##A_##B##B_##G##G_##R##R_   )
#define DEF_CPixel_D3DFMT_A(    A_         ) DEF_CPixel_D3DFMT(A   ,(A_              ),A##A_                        )
#define DEF_CPixel_D3DFMT_L(       L_      ) DEF_CPixel_D3DFMT(L   ,(   L_           ),L       ##L_                 )
#define DEF_CPixel_D3DFMT_R(             R_) DEF_CPixel_D3DFMT(R   ,(         R_     ),R                     ##R_   )
#define DEF_CPixel_D3DFMT_AL(   A_,L_      ) DEF_CPixel_D3DFMT(AL  ,(A_,L_           ),A##A_##L##L_                 )
#define DEF_CPixel_D3DFMT_GR(         G_,R_) DEF_CPixel_D3DFMT(GR  ,(      G_,R_     ),G              ##G_##R##R_   )
#define DEF_CPixel_D3DFMT_RF(            R_) DEF_CPixel_D3DFMT(R   ,(         R_,true),R                     ##R_##F)
#define DEF_CPixel_D3DFMT_GRF(        G_,R_) DEF_CPixel_D3DFMT(GR  ,(      G_,R_,true),G              ##G_##R##R_##F)
#define DEF_CPixel_D3DFMT_ABGRF(A_,B_,G_,R_) DEF_CPixel_D3DFMT(ABGR,(A_,B_,G_,R_,true),A##A_##B##B_##G##G_##R##R_##F)

#endif

DEF_CPixel_D3DFMT_RGB (  8,8,8);
DEF_CPixel_D3DFMT_ARGB(8,8,8,8);
DEF_CPixel_D3DFMT_XRGB(8,8,8,8);
DEF_CPixel_D3DFMT_ABGR(8,8,8,8);
DEF_CPixel_D3DFMT_XBGR(8,8,8,8);

DEF_CPixel_D3DFMT_ARGB(2,10,10,10);
DEF_CPixel_D3DFMT_ABGR(2,10,10,10);

DEF_CPixel_D3DFMT_RGB (  5,6,5);
DEF_CPixel_D3DFMT_ARGB(1,5,5,5);
DEF_CPixel_D3DFMT_XRGB(1,5,5,5);
DEF_CPixel_D3DFMT_ARGB(4,4,4,4);
DEF_CPixel_D3DFMT_XRGB(4,4,4,4);

DEF_CPixel_D3DFMT_RGB (  3,3,2);
DEF_CPixel_D3DFMT_ARGB(8,3,3,2);

DEF_CPixel_D3DFMT_A (8);
DEF_CPixel_D3DFMT_L (8);
DEF_CPixel_D3DFMT_AL(8,8);
DEF_CPixel_D3DFMT_AL(4,4);

DEF_CPixel_D3DFMT_L   (16);
DEF_CPixel_D3DFMT_GR  (16,16);
DEF_CPixel_D3DFMT_ABGR(16,16,16,16);

DEF_CPixel_D3DFMT_RF   (16);
DEF_CPixel_D3DFMT_GRF  (16,16);
DEF_CPixel_D3DFMT_ABGRF(16,16,16,16);

DEF_CPixel_D3DFMT_RF   (32);
DEF_CPixel_D3DFMT_GRF  (32,32);
DEF_CPixel_D3DFMT_ABGRF(32,32,32,32);

// ================================================================================================
// not supported - palletised formats
// ==================================
//     P8
//   A8P8
//
// not supported - signed formats
// ==============================
//           V8U8
//         CxV8U8
//         L6V5U5
//       X8L8V8U8
//       Q8W8V8U8
//    A2W10V10U10
//         V16U16
//   Q16W16V16U16
//
// not supported - video formats
// =============================
//   UYVY
//   YUY2
//   R8G8_B8G8
//   G8R8_G8B8
//   MULTI2_ARGB8
//
// not supported - depth/stencil formats
// =====================================
//   D16_LOCKABLE
//   D32
//   D15S1
//   D24S8
//   D24X8
//   D24X4S4
//   D16
//   D32F_LOCKABLE
//   D24FS8
//   D32_LOCKABLE
//   S8_LOCKABLE
//
// not supported
// =============
//   VERTEXDATA
//   INDEX16
//   INDEX32
//   A1
//   BINARYBUFFER

#undef DEF_CPixel_D3DFMT_RGB
#undef DEF_CPixel_D3DFMT_ARGB
#undef DEF_CPixel_D3DFMT_XRGB
#undef DEF_CPixel_D3DFMT_ABGR
#undef DEF_CPixel_D3DFMT_XBGR
#undef DEF_CPixel_D3DFMT_A
#undef DEF_CPixel_D3DFMT_L
#undef DEF_CPixel_D3DFMT_R
#undef DEF_CPixel_D3DFMT_AL
#undef DEF_CPixel_D3DFMT_GR
#undef DEF_CPixel_D3DFMT_RF
#undef DEF_CPixel_D3DFMT_GRF
#undef DEF_CPixel_D3DFMT_ABGRF

#undef DEF_CPixel_D3DFMT

} // namespace rage
