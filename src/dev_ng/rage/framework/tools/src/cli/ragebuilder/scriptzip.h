/**
 * RageBuilder Zip Functions
 *
 * This header automatically configures the correct function linkage for
 * the following functions depending on the RageBuilder_DLL.h header file
 * defined RAGEBUILDER_DLL.
 */

#ifdef __cplusplus
extern "C"
{
#endif

RAGEBUILDER_DLL bool RBStartZip( );
RAGEBUILDER_DLL bool RBStartUncompressedZip( );
RAGEBUILDER_DLL bool RBAddToZip( const char* source, const char* destination );
RAGEBUILDER_DLL bool RBSaveZip( const char* packname );
RAGEBUILDER_DLL bool RBCloseZip( void );

RAGEBUILDER_DLL bool RBMountZip( const char* packname, const char* mountpoint );
RAGEBUILDER_DLL bool RBUnmountZip( const char* packname );

#ifdef __cplusplus
}
#endif