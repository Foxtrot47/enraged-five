//
// luagta.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef INC_LUA_GTA_H_
#define INC_LUA_GTA_H_

int LoadDefinitions(lua_State* L);
int LoadPlacements(lua_State* L);
int SaveDefinitions(lua_State* L);
int SavePlacements(lua_State* L);
int ClearDefinitions(lua_State* L);
int ClearPlacements(lua_State* L);

int GetNumMilos(lua_State* L);
int GetMiloName(lua_State* L);
int GetMiloNumInstances(lua_State* L);
int GetMiloInstanceName(lua_State* L);
int GetMiloNumRooms(lua_State* L);
int GetMiloRoomName(lua_State* L);
int GetMiloRoomNumInstances(lua_State* L);
int GetMiloRoomInstance(lua_State* L);

int GetNumDefinitions(lua_State* L);
int GetObjectName(lua_State* L);
int GetObjectTxdName(lua_State* L);
int GetObjectLodDistance(lua_State* L);
int GetObjectFlags(lua_State* L);
int GetDDName(lua_State* L);

int GetNumTimeDefinitions(lua_State* L);
int GetTimeObjectName(lua_State* L);

int GetNumInstances(lua_State* L);
int GetInstanceName(lua_State* L);
int GetInstanceTransform(lua_State* L);
int GetInstanceIsLod(lua_State* L);
int GetInstanceBlock(lua_State* L);
int GetNumBlocks(lua_State* L);
int GetBlockName(lua_State* L);
int GetBlockPoints(lua_State* L);
int GetBlockArea(lua_State* L);
int GetNumPeds(lua_State* L);
int GetPedName(lua_State* L);
int GetMissingObjects(lua_State* L);
int GetUnusedObjects(lua_State* L);
int GetUsage(lua_State* L);

int InfoModelLoad(lua_State* L);
int InfoModelClose(lua_State* L);
int InfoFragmentLoad(lua_State* L);
int InfoFragmentClose(lua_State* L);
int InfoModelNumPolys(lua_State* L);
int InfoModelNumShaders(lua_State* L);
int InfoModelNumTextures(lua_State* L);
int InfoModelNumGeoms(lua_State* L);
int InfoModelShaderName(lua_State* L);
int InfoModelShaderDrawBucket(lua_State* L);
int InfoModelTextureName(lua_State* L);
int InfoModelVolume(lua_State* L);

int GetLatestMessage(lua_State* L);

int ProgressSizeCallback(lua_State* L);
int ProgressPosCallback(lua_State* L);

void RegisterCommandsGta(CLuaCore& lua);

#endif // INC_LUA_GTA_H_
