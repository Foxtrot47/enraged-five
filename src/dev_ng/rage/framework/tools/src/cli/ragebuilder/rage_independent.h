//
// rage_independent.h
//
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//

#ifndef INC_RAGE_INDEPENDENT_H_
#define INC_RAGE_INDEPENDENT_H_

#include "ragebuilder.h"

class RagePlatformIndependent : public RagePlatform
{
public:
	virtual void Init();
	virtual void Shutdown();

	virtual bool LoadModels(const char* filefilter);
	virtual bool SaveTextureDictionary(const char* filename, const char* hi_suffix);
	virtual bool SaveClipDictionary(const char* filename, bool usePackLoader);
	virtual bool SaveParamMotionDictionary(const char* filename);
	virtual bool SaveExpressionsDictionary(const char* filename);
	virtual bool SaveModelDictionary(const char* filename);
	virtual bool SaveClothDictionary(const char* filename);
	virtual bool SaveBoundDictionary(const char* filename);
	virtual bool SavePhysicsDictionary(const char* filename);
	virtual bool SaveBlendShapeDictionary(const char* filename);
	virtual bool SaveTexture(const char* filename);
	virtual bool SaveModel(const char* filename);
	virtual bool SaveBlendShape(const char* filenameIn, const char* filenameOut);
	virtual bool SaveFragment(const char* filename);
	virtual bool SaveBound(const char* filename);
	virtual bool SavePathRegion(const char* filename);
	virtual bool SaveNavMesh(const char* filename);
	virtual bool SaveHeightMesh(const char* filename);
	virtual bool SaveAudMesh(const char* filename);
	virtual bool SaveHierarchicalNav(const char* filename);
	virtual bool SaveWaypointRecording(const char* filename);
	virtual bool SaveEffect(const char* filename);
	virtual bool SaveFrameFilterDictionary(const char* filename);
private:
	bool AddModelFiles(const char* filefilter, const char* destination);
};

#endif // INC_RAGE_INDEPENDENT_H_
