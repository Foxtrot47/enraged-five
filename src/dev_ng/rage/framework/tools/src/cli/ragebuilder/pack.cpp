//
// Rage pack file (.rpf) builder.
//
// The format for this packing approach is slightly abnormal. Code exists
// to convert a zip file into an rbf, but no code exists to make an rbf
// from scratch. For that reason, this pack system will construct a
// compressed buffer using zip.exe, and then let the existing code handle
// the conversion to an rbf.
//
// The files ziploader.h and ziploader.cpp are part of the zip2rpf project
// and have been copied from $(rage)/base/tools/ziptools.
//
// @todo KS - create an .rpf from scratch.
//
// Copyright (C) 1999-2005 Rockstar North.  All Rights Reserved.
//

// Local headers
#include "pack.h"
#include "Rpf7Builder.h"
#include "utils.h"

// RAGE includes
#include "file/asset.h"
#include "system/magicnumber.h"
#include "system/platform.h"
#include "system/xtl.h"

#define VERIFY_RPF_BUILDER( ret ) if ( !ms_pBuilder ) return (ret)
#define VERIFY_NO_RPF_BUILDER( ret ) if ( ms_pBuilder ) return (ret)
#define MAGIC_SIZE (4)

using namespace rage;

IRpfBuilder*	CRagePackFileBuilder::ms_pBuilder = NULL;
bool			CRagePackFileBuilder::ms_bEmpty = true;
tFileMTimeList	CRagePackFileBuilder::ms_mFileList;
CRagePackFileBuilder::Version CRagePackFileBuilder::ms_eVersion;
bool			CRagePackFileBuilder::ms_bIncludeMetaData = true;

// Return the RPF version of a particular file, or INVALID if unknown.
CRagePackFileBuilder::Version
CRagePackFileBuilder::GetVersion( const char* filename )
{
	const fiDevice* pDevice = fiDevice::GetDevice( filename, true );
	if ( !pDevice )
		return ( INVALID );

	fiHandle fp = pDevice->Open( filename, true );
	if ( fiHandleInvalid != fp )
		return ( INVALID );

	char magic[MAGIC_SIZE];
	memset( &magic, 0, MAGIC_SIZE );
	int bytesRead = pDevice->Read( fp, &magic, MAGIC_SIZE );
	Assertf( MAGIC_SIZE == bytesRead, "Invalid number of bytes read for magic number." );
	pDevice->Close( fp );

	if ( MAGIC_SIZE != bytesRead )
		return ( INVALID );

	u32 rpf4 = MAKE_MAGIC_NUMBER( 'R', 'P', 'F', '4' );
	u32 rpf6 = MAKE_MAGIC_NUMBER( 'R', 'P', 'F', '6' );
	if ( 0 == memcmp( magic, &rpf4, MAGIC_SIZE ) )
		return ( RPF4 );
	else if ( 0 == memcmp( magic, &rpf6, MAGIC_SIZE ) )
		return ( RPF6 );
	else
		return ( INVALID );
}

// Open a new pack file.
bool CRagePackFileBuilder::Open( Version ver, bool compress, bool includeMetaData )
{
	Assertf( ver < RPF_MAX && ver >= RPF4, "Invalid RPF Version specified.  Aborting." );
	if ( ver >= RPF_MAX && ver < RPF4 )
		return (false);

	VERIFY_NO_RPF_BUILDER( false );

	switch ( ver )
	{
	case RPF7:
		ms_pBuilder = new Rpf7Builder();
		break;
	case RPF_MAX:
	default:
		Assertf( false, "Invalid RPF version specified.  Aborting." );
		return (false);
	}
	
	Assertf( ms_pBuilder, "Failed to construct RPF builder object.  Aborting." );
	if ( !ms_pBuilder )
		return (false);

	ms_eVersion = ver;
	ms_bEmpty = true;
	ms_bIncludeMetaData = ( includeMetaData && 	RPF6 != ms_eVersion );
	return true;
}

// Save the pack file.
bool CRagePackFileBuilder::Save(const char* filename, bool bSaveEmpty)
{
	VERIFY_RPF_BUILDER( false );

	if (ms_bEmpty && !bSaveEmpty)
	{
		Warningf("Not saving an empty packfile - %s", filename);
		return true;
	}

	fiStream* pack = ASSET.Create( filename, "" );
	if(pack)
	{
		// TEMPORARY HACK - deduce the platform from the destination directory name
		char oldPlatform = g_sysPlatform;
		bool oldSwap = g_ByteSwap;

		char fileTemp[MAX_PATH];
		StringNormalize(fileTemp,filename,sizeof(fileTemp));
		// These tests need to be consistent with game/system/FileMgr.cpp
		if (strstr(fileTemp,"/pc/"))
			g_sysPlatform = platform::WIN32PC;
		else if (strstr(fileTemp,"/x64/"))
			g_sysPlatform = platform::WIN64PC;
		else if (strstr(fileTemp,"/durango/")) // This may need to be xboxone!
			g_sysPlatform = platform::DURANGO;
		else if (strstr(fileTemp,"ps4/"))
			g_sysPlatform = platform::ORBIS;
		else if (strstr(fileTemp,"/xbox360/"))
			g_sysPlatform = platform::XENON;
		else if (strstr(fileTemp,"/ps3/"))
			g_sysPlatform = platform::PS3;
		// otherwise don't touch g_sysPlatform if we have no clue, hope it's correct.
		Displayf("%s: autodetecting platform %c...",fileTemp,g_sysPlatform);

		g_ByteSwap = g_sysPlatform != platform::WIN32PC && g_sysPlatform != platform::WIN64PC || g_sysPlatform != platform::DURANGO || g_sysPlatform != platform::ORBIS;

		ms_pBuilder->Save( pack );

		g_sysPlatform = oldPlatform;
		g_ByteSwap = oldSwap;

		pack->Flush( );
		pack->Close( );
	}
	else
	{
		return false;
	}

	return true;
}

// Close the pack file.
bool CRagePackFileBuilder::Close()
{
	VERIFY_RPF_BUILDER( false );

	delete ms_pBuilder;
	ms_pBuilder = NULL;

	return true;
}

void CRagePackFileBuilder::PushCompression(bool compress)
{
	if (!ms_pBuilder)
		return;

	ms_pBuilder->PushCompression(compress);
}

void CRagePackFileBuilder::PopCompression()
{
	if (!ms_pBuilder)
		return;

	ms_pBuilder->PopCompression();
}

// Add an entry to the pack file.
bool CRagePackFileBuilder::AddFile(const char* source, const char* destination)
{
	VERIFY_RPF_BUILDER( false );

	if (ms_pBuilder->Add(source, destination))
	{
		ms_bEmpty = false;
	}
	else
	{
		return false;
	}

	ms_mFileList[atString(destination)] = rage::atString( Get_FileTimeString( source ).c_str() );
	return true;
}


// Add a set of files according to a files to a folder location within the pack.
bool CRagePackFileBuilder::AddFilesByFilter(const char* source, const char* destination)
{
	VERIFY_RPF_BUILDER( false );

	char packname[MAX_PATH];
	string_list tempList;

	AddFilesToList(source, tempList, false);

	for (string_list::iterator it = tempList.begin(); it != tempList.end(); ++it)
	{
		const char* filename = (*it).c_str();
		const char* name = ASSET.FileName(filename);

		if (destination)
		{
			sprintf(packname, "%s/%s", destination, name);
		}
		else
		{
			sprintf(packname, "%s", name);
		}

		if (CRagePackFileBuilder::AddFile(filename, packname) == false)
		{
			return false;
		}
		ms_mFileList[atString(packname)] = rage::atString( Get_FileTimeString( filename ).c_str() );
	}

	return true;
}
