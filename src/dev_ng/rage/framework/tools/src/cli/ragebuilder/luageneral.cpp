//
// luageneral.cpp - A number of general file, path and other utility lua
//					callbacks.
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved. 
//

#include "luageneral.h"
#include "scriptgeneral.h"

# pragma warning(disable: 4668)
// C headers
#include <conio.h>
// windows headers
#if __XENON
#define MAX_PATH          260
#endif

#include "system/xtl.h"

// rage headers
#include "system/param.h"
#include "file/asset.h"
#include "file/device.h"
#include "system/performancetimer.h"

// project headers
#include "luacore.h"
#include "ragebuilder.h"
#include "utils.h"
#include "log.h"
#include "librb.h"

using namespace rage;

// Set an environment variable
int SetEnv(lua_State *L)
{
	GetArguments(L, 1, "set_env");
	CheckArgument(L, 1, lua_isstring, "set_env");

	int result = -1;

#ifdef __WIN32PC
	result = _putenv( lua_tostring(L, 1) );
#endif // __WIN32PC

	if( result == 0 )
	{
		LUA_RETURN_SUCCESS();
	}

	LUA_RETURN_FAILURE();
}

// Get an environment variable
int GetEnv(lua_State *L)
{
	GetArguments(L, 1, "get_env");
	CheckArgument(L, 1, lua_isstring, "get_env");

	char* result = NULL;
#ifdef __WIN32PC
	result = getenv( lua_tostring(L, 1) );
#endif // __WIN32PC

	if( result )
	{
		lua_pushstring( L, result );
	}
	else
	{
		lua_pushstring( L, "" );
	}

	return 1;
}


// Get a command line parameter 
int GetParam(lua_State *L)
{
	GetArguments(L, 1, "get_param");
	CheckArgument(L, 1, lua_isstring, "get_param");
	int results = 0;

	// either it's a register parameter or it is an extra parameter not covered by any of the registered parameters
	const char* param = lua_tostring(L, 1);

	const char* result = rage::sysParam::GetByName( param, NULL );

	if( result )
	{
		lua_pushstring( L, result );
		results++;
	}
	else
	{
		// an extra parameter - here we operate in a restricted way
		// we search through all the arguments until we hit one of the form
		// -<name> <argument>, otherwise it ain't there
		char dashParam[256] = "-";
		strcat( dashParam, param );
		for(int i = 0; i < sysParam::GetArgCount(); ++i )
		{
			if(stricmp(dashParam, sysParam::GetArg(i)) == 0)
			{
				if( ( i + 1 ) < sysParam::GetArgCount() )
				{
					lua_pushstring( L, sysParam::GetArg(i+1) );
					results++;
					break;
				}
			}
		}
	}

	return results;
}

// Get command line parameter count.
int GetParamCount( lua_State* L )
{
	GetArguments( L, 0, "get_paramcount" );
	lua_pushnumber( L, sysParam::GetArgCount() );
	return 1;
}

// Get command line parameter by index.
int GetParamByIndex( lua_State* L )
{
	GetArguments( L, 1, "get_parambyindex" );
	CheckArgument( L, 1, lua_isnumber, "get_parambyindex" );

	int index = (int)lua_tonumber( L, 1 );
	if ( index < 0 || index >= sysParam::GetArgCount() )
	{
		lua_pushnil( L );
		return 1;
	}
	else
	{
		const char* param  = rage::sysParam::GetArg( index );
		lua_pushstring( L, param );
	}
	return 1;
}

// Check whether a file exists
int FileExists(lua_State* L)
{
	GetArguments(L, 1, "file_exists");
	CheckArgument(L, 1, lua_isstring, "file_exists");

	lua_pushboolean( L, RBFileExists( lua_tostring(L, 1) ) );
	return 1;
}

// Check whether a directory exists
int DirectoryExists(lua_State* L)
{
	GetArguments(L, 1, "dir_exists");
	CheckArgument(L, 1, lua_isstring, "dir_exists");

	lua_pushboolean( L, RBDirectoryExists( lua_tostring(L, 1) ) );
	return 1;
}

int Get_FileTime(lua_State* L)
{
	GetArguments(L, 1, "filetime");
	CheckArgument(L, 1, lua_isstring, "filetime");
	lua_pushnumber(L, (u32)Get_FileTime(lua_tostring(L, 1)));
	return 1;
}

// Copy a file 
int Copy_File(lua_State* L)
{
	GetArguments(L, 2, "copy_file");
	CheckArgument(L, 1, lua_isstring, "copy_file");
	CheckArgument(L, 2, lua_isstring, "copy_file");

	if(RBCopy_File(lua_tostring(L, 1),lua_tostring(L, 2)))
		LUA_RETURN_SUCCESS();

	LUA_RETURN_FAILURE();
}

// Move a file to another location on the same volume.
// Only works with a local device.
int Move_File(lua_State* L)
{
	GetArguments(L, 2, "move_file");
	CheckArgument(L, 1, lua_isstring, "move_file");
	CheckArgument(L, 2, lua_isstring, "move_file");

	if(RBMove_File(lua_tostring(L, 1),lua_tostring(L, 2)))
		LUA_RETURN_SUCCESS();

	LUA_RETURN_FAILURE();
}


// Compare two files for time.
int FileTimeCompare(lua_State* L)
{
	GetArguments(L, 2, "filetime_compare");
	CheckArgument(L, 2, lua_isstring, "filetime_compare");
	lua_pushnumber(L, RBFileTimeCompare(lua_tostring(L, 1),lua_tostring(L, 2)));
	return 1;
}

// Return the file size.
int FileSize(lua_State* L)
{
	GetArguments(L, 1, "filesize");
	CheckArgument(L, 1, lua_isstring, "filesize");
	lua_pushnumber(L, RBFileSize(lua_tostring(L, 1)));
	return 1;
}

// Return the resource size (-1 if not a resource).
int ResourceSize(lua_State* L)
{
	GetArguments(L, 1, "resourcesize");
	CheckArgument(L, 1, lua_isstring, "resourcesize");
	lua_pushnumber(L, RBResourceSize(lua_tostring(L, 1)));
	return (1);
}

int ResourcePhysicalSize(lua_State* L)
{
	GetArguments(L, 1, "resourcephysicalsize");
	CheckArgument(L, 1, lua_isstring, "resourcephysicalsize");
	lua_pushnumber(L, RBResourcePhysicalSize(lua_tostring(L, 1)));
	return (1);
}

int ResourceVirtualSize(lua_State* L)
{
	GetArguments(L, 1, "resourcevirtualsize");
	CheckArgument(L, 1, lua_isstring, "resourcevirtualsize");
	lua_pushnumber(L, RBResourceVirtualSize(lua_tostring(L, 1)));
	return (1);
}

// Set a file attribute.
int SetFileAttrib(lua_State* L)
{
	GetArguments(L, 3, "set_file_attrib");
	CheckArgument(L, 1, lua_isstring, "set_file_attrib");
	CheckArgument(L, 2, lua_isstring, "set_file_attrib");

	bool val = lua_toboolean(L, 3) != 0 ? true : false;
	if(RBSetFileAttrib(lua_tostring(L, 1),lua_tostring(L, 2), val))
		LUA_RETURN_SUCCESS();
	
	LUA_RETURN_FAILURE();
}

// Get a file attribute.
int GetFileAttrib(lua_State* L)
{
	GetArguments(L, 2, "get_file_attrib");
	CheckArgument(L, 1, lua_isstring, "get_file_attrib");
	CheckArgument(L, 2, lua_isstring, "get_file_attrib");
	lua_pushboolean( L, RBGetFileAttrib(lua_tostring(L, 1),lua_tostring(L, 2)));
	return 1;
}

// clone the timestamp on a source file file to another file.
int CloneTimestamp(lua_State* L)
{
	GetArguments(L, 2, "clone_timestamp");
	CheckArgument(L, 1, lua_isstring, "clone_timestamp");
	CheckArgument(L, 2, lua_isstring, "clone_timestamp");

	if(RBCloneTimestamp(lua_tostring(L, 1),lua_tostring(L, 2)))
		LUA_RETURN_SUCCESS();

	LUA_RETURN_FAILURE();
}


// Wait for a user keypress
int Wait(lua_State* L)
{
	RBWait();
	LUA_RETURN(true);
}

// Sleep for a specified number of milliseconds.
int Sleep(lua_State* L)
{
	GetArguments(L, 1, "sleep");
	CheckArgument(L, 1, lua_isnumber, "sleep");
	RBSleep( (int)lua_tonumber(L,1) );

	LUA_RETURN(true);
}

// Set the log level.
int SetLogLevel(lua_State* L)
{
	GetArguments(L, 1, "set_loglevel");
	CheckArgument(L, 1, lua_isnumber, "set_loglevel");

	RBSetLogLevel( (int)lua_tonumber(L,1) );

	LUA_RETURN(true);
}

// Set stop on error
int SetStopOnError(lua_State* L)
{
	GetArguments(L, 1, "set_stoponerror");

	RBSetStopOnError(lua_toboolean(L,1) != 0);

	LUA_RETURN(true);
}

// Report an error.
int ReportError(lua_State* L)
{
	GetArguments(L, 1, "error");
	CheckArgument(L, 1, lua_isstring, "error");

	RBReportError(lua_tostring(L, 1));
	LUA_RETURN(true);
}

int ReportQuit(lua_State* L)
{
	GetArguments(L, 1, "quit");
	CheckArgument(L, 1, lua_isstring, "quit");

	RBReportQuit(lua_tostring(L, 1));
	LUA_RETURN(true);
}

// Report a warning.
int ReportWarning(lua_State* L)
{
	GetArguments(L, 1, "error");
	CheckArgument(L, 1, lua_isstring, "error");

	RBReportWarning(lua_tostring(L, 1));
	LUA_RETURN(true);
}

// Report a display message.
int ReportDisplay(lua_State* L)
{
	GetArguments(L, 1, "display");
	CheckArgument(L, 1, lua_isstring, "display");

	RBReportDisplay(lua_tostring(L, 1));
	LUA_RETURN(true);
}

// Report a trace message which will only show if it doesn't exceed a threshold.
int ReportTrace1(lua_State* L)
{
	GetArguments(L, 1, "trace1");
	CheckArgument(L, 1, lua_isstring, "trace1");
	RBReportTrace1(lua_tostring(L, 1));
	LUA_RETURN(true);
}

// Report a trace message which will only show if it doesn't exceed a threshold.
int ReportTrace2(lua_State* L)
{
	GetArguments(L, 1, "trace2");
	CheckArgument(L, 1, lua_isstring, "trace2");
	RBReportTrace2(lua_tostring(L, 1));
	LUA_RETURN(true);
}

// Report a trace message which will only show if it doesn't exceed a threshold.
int ReportTrace3(lua_State* L)
{
	GetArguments(L, 1, "trace3");
	CheckArgument(L, 1, lua_isstring, "trace3");
	RBReportTrace3(lua_tostring(L, 1));
	LUA_RETURN(true);
}

int GetBuildLogFilename(lua_State* L)
{
	lua_pushstring( L, RBGetBuildLogFilename() );
	return 1;
}

int GetErrorLogFilename(lua_State* L)
{
	lua_pushstring( L, RBGetErrorLogFilename() );
	return 1;
}

// Return a list of sub directories of a passed in directory
int FindDirs(lua_State* L)
{
	GetArguments(L, 1, "find_dirs");
	CheckArgument(L, 1, lua_isstring, "find_dirs");

	string_list dirlist;

	AddSubDirToList( lua_tostring(L, 1), dirlist );

	// return the files as a table to Lua
	lua_newtable(L);

	int iCount = 1;
	for (string_list::iterator it = dirlist.begin(); it != dirlist.end(); ++it)
	{
		lua_pushnumber(L, iCount++ );   
		lua_pushstring(L, (*it).c_str() ); 
		lua_settable(L, -3);      
	}

	return 1;
}

// Deletes a file by the passed in filename
int DelFile(lua_State* L)
{
	GetArgumentsRange(L, 1, 2, "del_file");
	CheckArgument(L, 1, lua_isstring, "del_file");

	if(RBDelFile(lua_tostring(L, 1)))
		LUA_RETURN_SUCCESS();

	LUA_RETURN_FAILURE();
}


// Deletes a directory.
int DelDir(lua_State* L)
{
	GetArguments(L, 2, "del_dir");
	CheckArgument(L, 1, lua_isstring, "del_dir");

	bool contents = lua_toboolean(L, 2) != 0 ? true : false;
	if(RBDelDir(lua_tostring(L, 1), contents))
		LUA_RETURN_SUCCESS();

	LUA_RETURN_FAILURE();
}

// Return a list of files according to a file filter.
int FindFiles(lua_State* L)
{
	int numArgs = GetArgumentsRange(L, 1, 3, "find_files");
	CheckArgument(L, 1, lua_isstring, "find_files");

	string_list filelist;

	bool bRelative = true;
	bool bSorted = true;
	if (numArgs > 1)
	{
		bRelative = (lua_toboolean(L, 2) != 0);

		if (numArgs > 2)
		{
			bSorted = (lua_toboolean(L, 3) != 0);
		}
	}

	AddFilesToList( lua_tostring(L, 1), filelist, bRelative, bSorted );

	// return the files as a table to Lua
	lua_newtable(L);

	int iCount = 1;
	for (string_list::iterator it = filelist.begin(); it != filelist.end(); ++it)
	{
		lua_pushnumber(L, iCount++);   
		lua_pushstring(L, (*it).c_str()); 
		lua_settable(L, -3);      
	}

	return 1;
}

int FindPlatformFiles(lua_State* L)
{
	int numArgs  = GetArgumentsRange(L, 1, 3, "find_files");
	CheckArgument(L, 1, lua_isstring, "find_files");

	string_list filelist;

	bool bRelative = true;
	bool bSorted = true;
	if (numArgs > 1)
	{
		bRelative = (lua_toboolean(L, 2) != 0); 

		if (numArgs > 2)
		{
			bSorted = (lua_toboolean(L, 3) != 0); 
		}
	}

	AddPlatformFilesToList( lua_tostring(L, 1), filelist, bRelative, bSorted );

	// return the files as a table to Lua
	lua_newtable(L);

	int iCount = 1;
	for (string_list::iterator it = filelist.begin(); it != filelist.end(); ++it)
	{
		lua_pushnumber(L, iCount++);   
		lua_pushstring(L, (*it).c_str()); 
		lua_settable(L, -3);      
	}

	return 1;
}

// Return the path without the tailing extension.
int RemoveExtensionFromPath(lua_State* L)
{
	GetArguments(L, 1, "remove_extension_from_path");
	CheckArgument(L, 1, lua_isstring, "remove_extension_from_path");

	lua_pushstring(L, RBRemoveExtensionFromPath(lua_tostring(L, 1)) );

	return 1;
}

int RemoveAllExtensionsFromPath(lua_State* L)
{
	GetArguments(L, 1, "remove_all_extensions_from_path" );
	CheckArgument(L, 1, lua_isstring, "remove_all_extensions_from_path" );

	const char* s = lua_tostring( L, 1 );
	while ( RBGetExtensionFromPath( s ) )
	{
		s = RBRemoveExtensionFromPath( s );
	}

	lua_pushstring( L, s );

	return 1;
}

// Get the extension from the path.
int GetExtensionFromPath(lua_State* L)
{ 
	GetArguments(L, 1, "get_extension_from_path");
	CheckArgument(L, 1, lua_isstring, "get_extension_from_path");

	lua_pushstring( L, RBGetExtensionFromPath(lua_tostring(L, 1)) );
	return 1;

}

// Get the filename part of the path.
int GetFilenameFromPath(lua_State* L)
{ 
	GetArguments(L, 1, "get_filename_from_path");
	CheckArgument(L, 1, lua_isstring, "get_filename_from_path");

	lua_pushstring( L, RBGetFilenameFromPath(lua_tostring(L, 1)) );
	return 1;

}

// Remove the filename part of the path.
int RemoveNameFromPath(lua_State* L)
{ 
	GetArguments(L, 1, "get_rootpath_from_path");
	CheckArgument(L, 1, lua_isstring, "get_rootpath_from_path");
	lua_pushstring( L, RBRemoveNameFromPath(lua_tostring(L, 1)) );
	return 1;

}


// Create the leading path to a full filename. 
int CreateLeadingPath(lua_State* L)
{
	GetArguments(L, 1, "create_leadingpath");
	CheckArgument(L, 1, lua_isstring, "create_leadingpath");

	if( !RBCreateLeadingPath( lua_tostring(L, 1) ) )
	{
		LUA_RETURN_FAILURE();
	}

	LUA_RETURN_SUCCESS();
}

int PushTimer(lua_State* L)
{
	RBPushTimer();
	LUA_RETURN_SUCCESS();
}

int PopTimer(lua_State* L)
{
	GetArguments(L, 1, "pop_timer");
	CheckArgument(L, 1, lua_isstring, "pop_timer");
	RBPopTimer(lua_tostring(L, 1));
	LUA_RETURN_SUCCESS();
}

int GetVersion(lua_State* L)
{
	lua_pushstring(L, RBGetVersion()); 
	return 1;
}

#if __XENON
// Print a string.
int Print(lua_State* L)
{
	GetArguments( L, 1, "print" );

	rbDisplayf( lua_tostring(L, 1));

	return 0;
}
#endif // __XENON


// Include another lua file.
int Require(lua_State* L)
{
	GetArguments( L, 1, "require" );
	CheckArgument(L, 1, lua_isstring, "include");

	CLuaCore tempLua(L);

	// rbTracef3( "require(%s)", lua_tostring(L, 1) );
	tempLua.ExecuteFile( lua_tostring(L, 1) );

	return 0;
}

int	FindXMLMatch(lua_State* L)
{
	GetArguments(L, 2, "find_xml_match");
	CheckArgument(L, 1, lua_isstring, "find_xml_match");
	CheckArgument(L, 2, lua_isstring, "find_xml_match");

	char dst[MAX_PATH] = {0};
	RBFindXMLMatch( lua_tostring(L, 1), lua_tostring(L,2), dst, MAX_PATH );

	lua_pushstring( L, dst );
	return 1;
}

