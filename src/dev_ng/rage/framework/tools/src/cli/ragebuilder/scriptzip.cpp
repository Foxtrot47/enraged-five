
#include "scriptzip.h"
#include "packmount.h"
#include "log.h"
#include "zipfile.h"

// RAGE headers
#include "file/asset.h"

bool 
RBStartZip( )
{
	rbTracef2( "starting zip" );

	if( CZipFile::Open( true ) == false )
	{
		Errorf( "Failed to start zip" );
		return false;
	}

	return true;
}

bool
RBStartUncompressedZip( )
{
	rbTracef2( "starting uncompressed zip" );

	if ( CZipFile::Open( false ) == false )
	{
		Errorf( "Failed to start uncompressed zip" );
		return (false);
	}
	return (true);
}

bool 
RBAddToZip( const char* source, const char* destin )
{
	string_list fileList;
	bool isPath = true;
	char destination[MAX_PATH];

	if(destin)
		strcpy(destination,destin);
	else
		destination[0] = '\0';

	if( destination[0] != '\0' )
	{
		const char* endOfDestination = destination + StringLength(destination) - 1;
		isPath = ( *endOfDestination == '\\' || *endOfDestination == '/' );
	}

	AddFilesToList( source, fileList, false );

	if( fileList.empty() )
		Warningf( "no files found for add_to_pack_path(%s, %s)", source, destination);

	for( string_list::iterator it = fileList.begin(); it != fileList.end(); ++it )
	{
		char fileDestination[MAX_PATH] = {0};
		strncpy( fileDestination, destination, MAX_PATH );

		if(isPath)
			strcat( fileDestination, ASSET.FileName( (*it).c_str() ) );

		if( !CZipFile::AddFile( (*it).c_str(), fileDestination ) )
			Warningf("failed to add file '%s' to zip as '%s'", (*it).c_str(), fileDestination );
		else
			rbTracef2("adding %s to zip as %s", (*it).c_str(), fileDestination);
	}

	return true;
}

bool 
RBSaveZip( const char* packname )
{
	if( !CZipFile::Save( packname ) )
	{
		Errorf( "failed to save_zip(%s)", packname);
		return false;
	}

	Displayf( "saved zip as %s", packname);
	return true;
}

bool 
RBCloseZip( void )
{
	if( CZipFile::Close() == false )
	{
		Warningf("failed to close zip file");
		return false;
	}

	rbTracef2( "closed zip" );
	return true;
}

bool 
RBMountZip( const char* packname, const char* mountpoint )
{
	const char* mountAs = packname;
	if( mountpoint != NULL )
		mountAs = mountpoint;

	if(	CPackMountManager::Mount( packname, mountAs, CPackMountManager::ZIP ) == false )
	{
		Errorf( "failed to mount zip %s", packname );
		return false;
	}

	rbTracef3("mounted zip - %s", packname);
	return true;
}

bool 
RBUnmountZip( const char* packname )
{
	if(	CPackMountManager::Unmount( packname ) == false )
	{
		Errorf( "failed to unmount zip %s", packname );
		return false;
	}

	rbTracef3("unmounted zip - %s", packname);
	return true;
}
