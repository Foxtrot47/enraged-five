//
// luazip.cpp
//
// Copyright (C) 2010 Rockstar North.  All Rights Reserved.
//

#include "luazip.h"
#include "scriptzip.h"

// RAGE headers
#include "file/limits.h"

int 
StartZip( lua_State* L )
{
	GetArguments( L, 0, "start_zip" );

	RBStartZip( );
	LUA_RETURN_SUCCESS();
}

int
StartUncompressedZip( lua_State* L )
{
	GetArguments( L, 0, "start_uncompressed_zip" );

	RBStartUncompressedZip( );
	LUA_RETURN_SUCCESS( );
}

int 
AddToZip( lua_State* L )
{
	int iNumArgs = GetArgumentsRange( L, 1, 2, "add_to_zip" );
	CheckArgument( L, 1, lua_isstring, "add_to_zip" );

	const char* source;
	const char* destination = "";

	source = lua_tostring( L, 1 );
	if ( iNumArgs > 1 )
	{
		CheckArgument( L, 2, lua_isstring, "add_to_zip" );
		destination = lua_tostring( L, 2 );
	}

	RBAddToZip( source, destination );
	LUA_RETURN_SUCCESS();
}

int 
SaveZip( lua_State* L )
{
	GetArguments( L, 1, "save_zip" );
	CheckArgument( L, 1, lua_isstring, "save_zip" );

	if (!RBSaveZip(lua_tostring(L, 1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

int 
CloseZip( lua_State* L )
{
	GetArguments( L, 0, "close_zip" );
	RBCloseZip( );

	LUA_RETURN_SUCCESS();
}

int 
MountZip( lua_State* L )
{
	int nArgs = GetArgumentsRange( L, 1, 2, "mount_zip" );
	CheckArgument( L, 1, lua_isstring, "mount_zip" );
	const char* packname = lua_tostring( L, 1 );
	const char* mountAs = NULL;
	if ( 2 == nArgs )
		mountAs = lua_tostring( L, 2 );

	RBMountZip( packname, mountAs );
	LUA_RETURN_SUCCESS();
}

int 
UnmountZip( lua_State* L )
{
	GetArguments( L, 1, "unmount_zip" );
	CheckArgument( L, 1, lua_isstring, "unmount_zip" );
	RBUnmountZip( lua_tostring( L, 1 ) );
	LUA_RETURN_SUCCESS();
}
