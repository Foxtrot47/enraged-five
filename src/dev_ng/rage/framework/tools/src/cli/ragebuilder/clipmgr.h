#ifndef RAGEBUILDER_CLIPMGR_H_
#define RAGEBUILDER_CLIPMGR_H_


#include "cranimation/animdictionary.h"
#include "crclip/clipdictionary.h"

namespace rage {

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class CAnimLoader : public crAnimLoaderBasic
{
public:
 
	// PURPOSE: Constructor
	CAnimLoader () {}
 
	// PURPOSE: Destructor
	virtual ~CAnimLoader () {}
};

class CAnimLoaderPack : public crAnimLoaderBasic
{
public:
 
	// PURPOSE: Constructor
	CAnimLoaderPack () {}
 
	// PURPOSE: Destructor
	virtual ~CAnimLoaderPack () {}
 
	// PURPOSE: Override allocate and load animation function
	virtual crAnimation* AllocateAndLoadAnimation(const char* animFilename)
	{
		const int maxNameBufSize = RAGE_MAX_PATH;
		char nameBuf[maxNameBufSize];
		const char* basename = ASSET.FileName(animFilename);
		sprintf(nameBuf, "pack:/%s", basename);
		
		return crAnimLoaderBasic::AllocateAndLoadAnimation(nameBuf);
	}
 
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class CClipLoaderPack : public crClipLoaderBasic
{
public:

	// PURPOSE: Constructor
	CClipLoaderPack () {}

	// PURPOSE: Destructor
	virtual ~CClipLoaderPack () {}

	// PURPOSE: Override allocate and load clip function
	virtual crClip* AllocateAndLoadClip(const char* clipFilename, crAnimLoader* loader)
	{
		const int maxNameBufSize = RAGE_MAX_PATH;
		char nameBuf[maxNameBufSize];
		const char* basename = ASSET.FileName(clipFilename);
		sprintf(nameBuf, "pack:/%s", basename);

		return crClipLoaderBasic::AllocateAndLoadClip(nameBuf, loader);
	}

};

}

#endif // RAGEBUILDER_CLIPMGR_H_