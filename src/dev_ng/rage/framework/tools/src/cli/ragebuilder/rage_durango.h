//
// rage_win32.h
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
//

#ifndef INC_RAGE_DURANGO_H_
#define INC_RAGE_DURANGO_H_

#include "ragebuilder.h"

class RagePlatformDurango : public RagePlatform
{
	virtual void Init();
};

#endif // INC_RAGE_DURANGO_H_
