#include "gta_res.h"
#include "data/safestruct.h"
#include "system/nelem.h"

using namespace rage;

#if __WIN32
#pragma optimize ("", off) // PRAGMA-OPTIMIZE-ALLOW
#endif

#if !__FINAL

void ColourData::DeclareStruct(datTypeStruct &s)
{
	SSTRUCT_BEGIN(ColourData)
	SSTRUCT_FIELD(ColourData, red)
	SSTRUCT_FIELD(ColourData, green)
	SSTRUCT_FIELD(ColourData, blue)
	SSTRUCT_END(ColourData)
}

void VectorData::DeclareStruct(datTypeStruct &s)
{
	SSTRUCT_BEGIN(VectorData)
	SSTRUCT_FIELD(VectorData, x)
	SSTRUCT_FIELD(VectorData, y)
	SSTRUCT_FIELD(VectorData, z)
	SSTRUCT_END(VectorData)
}

void C2dEffect::DeclareStruct(datTypeStruct &s)
{
	SSTRUCT_BEGIN(C2dEffect)
	SSTRUCT_FIELD(C2dEffect, m_posn)
	SSTRUCT_END(C2dEffect)
}

void CLightAttr::DeclareStruct(datTypeStruct &s)
{
	C2dEffect::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(CLightAttr, C2dEffect)
	SSTRUCT_FIELD(CLightAttr, m_colour)
	SSTRUCT_FIELD(CLightAttr, m_flashiness)
	SSTRUCT_FIELD(CLightAttr, m_intensity)
	SSTRUCT_FIELD(CLightAttr, m_flags)
	SSTRUCT_FIELD(CLightAttr, m_boneTag)
	SSTRUCT_FIELD(CLightAttr, m_lightType)
	SSTRUCT_FIELD(CLightAttr, m_groupId)
	SSTRUCT_FIELD(CLightAttr, m_timeFlags)
	SSTRUCT_FIELD(CLightAttr, m_falloff)
	SSTRUCT_FIELD(CLightAttr, m_falloffExponent)
	SSTRUCT_CONTAINED_ARRAY_COUNT(CLightAttr, m_cullingPlane, NELEM(m_cullingPlane))
	SSTRUCT_FIELD(CLightAttr, m_shadowBlur)
	SSTRUCT_IGNORE(CLightAttr, m_padding1)
	SSTRUCT_IGNORE(CLightAttr, m_padding2)
	SSTRUCT_IGNORE(CLightAttr, m_padding3)
	SSTRUCT_FIELD(CLightAttr, m_volIntensity)
	SSTRUCT_FIELD(CLightAttr, m_volSizeScale)
	SSTRUCT_FIELD(CLightAttr, m_volOuterColour)
	SSTRUCT_FIELD(CLightAttr, m_lightHash)
	SSTRUCT_FIELD(CLightAttr, m_volOuterIntensity)
	SSTRUCT_FIELD(CLightAttr, m_coronaSize)
	SSTRUCT_FIELD(CLightAttr, m_volOuterExponent)
	SSTRUCT_FIELD(CLightAttr, m_lightFadeDistance)
	SSTRUCT_FIELD(CLightAttr, m_shadowFadeDistance)
	SSTRUCT_FIELD(CLightAttr, m_specularFadeDistance)
	SSTRUCT_FIELD(CLightAttr, m_volumetricFadeDistance)
	SSTRUCT_FIELD(CLightAttr, m_shadowNearClip)
	SSTRUCT_FIELD(CLightAttr, m_coronaIntensity)
	SSTRUCT_FIELD(CLightAttr, m_coronaZBias)
	SSTRUCT_FIELD(CLightAttr, m_direction)
	SSTRUCT_FIELD(CLightAttr, m_tangent)
	SSTRUCT_FIELD(CLightAttr, m_coneInnerAngle)
	SSTRUCT_FIELD(CLightAttr, m_coneOuterAngle)
	SSTRUCT_FIELD(CLightAttr, m_extents)
	SSTRUCT_FIELD(CLightAttr, m_projectedTextureKey)
	SSTRUCT_END(CLightAttr)
}

#endif // !__FINAL

///////////////////////////////////
// gtaFragType

gtaFragType::gtaFragType(datResource& rsc)
: fragType(rsc),m_lights(rsc),m_vehicleWindowData(NULL),m_pTintData(rsc)
{
}

#if !__FINAL
void gtaFragType::DeclareStruct(datTypeStruct &s)
{
	fragType::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(gtaFragType, fragType)
		SSTRUCT_FIELD(gtaFragType, m_lights)
		SSTRUCT_FIELD_VP(gtaFragType, m_vehicleWindowData)
		SSTRUCT_FIELD(gtaFragType, m_pTintData)
	SSTRUCT_END(gtaFragType)
}
#endif

///////////////////////////////////
// gtaDrawable

gtaDrawable::gtaDrawable(datResource& rsc)
: rmcDrawable(rsc),m_lights(rsc,true),m_pTintData(rsc),m_pPhBound(rsc)
{
}

#if !__FINAL

void gtaDrawable::DeclareStruct(datTypeStruct &s)
{
	rmcDrawable::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(gtaDrawable, rmcDrawable)
		SSTRUCT_FIELD(gtaDrawable, m_lights)
		SSTRUCT_FIELD(gtaDrawable, m_pTintData)
		SSTRUCT_FIELD(gtaDrawable, m_pPhBound)
	SSTRUCT_END(gtaDrawable)
}

#endif // !__FINAL
