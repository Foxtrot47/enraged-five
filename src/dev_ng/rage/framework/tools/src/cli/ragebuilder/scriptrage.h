/**
 * RageBuilder RAGE Functions
 *
 * This header automatically configures the correct function linkage for
 * the following functions depending on the RageBuilder_DLL.h header file
 * defined RAGEBUILDER_DLL.
 */

#ifdef __cplusplus
extern "C"
{
#endif

RAGEBUILDER_DLL bool RBInitRage(void);
RAGEBUILDER_DLL bool RBShutdownRage(void);
RAGEBUILDER_DLL bool RBSetRootDirectory(const char* path);
RAGEBUILDER_DLL bool RBSetAutoTexDict(bool autotexdict);
RAGEBUILDER_DLL bool RBRegisterTextureName(const char* texname);
RAGEBUILDER_DLL bool RBLoadProceduralDefs(const char* filename);
RAGEBUILDER_DLL bool RBLoadMaterialDefs(const char* filename);
RAGEBUILDER_DLL bool RBSetEffectPath(const char* path);
RAGEBUILDER_DLL bool RBSetShaderPath(const char* path);
RAGEBUILDER_DLL bool RBSetShaderDbPath(const char* path);
RAGEBUILDER_DLL bool RBSetBuildPath(const char* path);
RAGEBUILDER_DLL bool RBSetAssetsPath(const char* path);
RAGEBUILDER_DLL bool RBSetCoreAssetsPath(const char* path);
RAGEBUILDER_DLL bool RBSetMetadataDefinitionsPath(const char* path);
RAGEBUILDER_DLL bool RBSetDefinitionsPath(const char* path);
RAGEBUILDER_DLL const char* RBGetPlatform(void);
RAGEBUILDER_DLL bool RBSetPlatform(const char* platform);
RAGEBUILDER_DLL const char* RBGetPlatformExtension(const char* filename);
RAGEBUILDER_DLL const char* RBConvertFilenameToPlatform(const char* filename,const char* platform);
RAGEBUILDER_DLL bool RBStartBuild(void);
// GunnarD: Retiring in favour of Configmanager setting
//RAGEBUILDER_DLL bool RBSetTuningFolder(const char* path);
RAGEBUILDER_DLL bool RBLoadTextureTemplates(const char* files);
RAGEBUILDER_DLL bool RBLoadTexture(const char* texture);
RAGEBUILDER_DLL bool RBLoadTextures(const char* textures, int& count);
RAGEBUILDER_DLL bool RBLoadTexturesTCLFallback(const char* texturesFileFilter, const char* tclsFileFilter);
RAGEBUILDER_DLL bool RBLoadAnim(const char* anim);
RAGEBUILDER_DLL bool RBLoadAnims(const char* anims);
RAGEBUILDER_DLL bool RBLoadClip(const char* anim);
RAGEBUILDER_DLL bool RBLoadClips(const char* anims);
RAGEBUILDER_DLL bool RBLoadParamMotion(const char* parammotion);
RAGEBUILDER_DLL bool RBLoadParamMotions(const char* parammotions);
RAGEBUILDER_DLL bool RBLoadExpressions(const char* expressions);
RAGEBUILDER_DLL bool RBLoadExpressionsList(const char* expressionsList);
RAGEBUILDER_DLL bool RBLoadModel(const char* model);
RAGEBUILDER_DLL bool RBLoadBlendShape(const char* blendshape);
RAGEBUILDER_DLL bool RBLoadFragment(const char* fragment);
RAGEBUILDER_DLL bool RBLoadNMData(const char* fragment);
RAGEBUILDER_DLL bool RBLoadModels(const char* models);
RAGEBUILDER_DLL bool RBLoadBound(const char* bound);
RAGEBUILDER_DLL bool RBLoadBounds(const char* bounds);
RAGEBUILDER_DLL bool RBLoadPathRegion(const char* pathRegion);
RAGEBUILDER_DLL bool RBLoadNavMesh(const char* navmesh);
RAGEBUILDER_DLL bool RBLoadHeightMesh(const char* navmesh);
RAGEBUILDER_DLL bool RBLoadHierarchicalNav(const char* hiernav);
RAGEBUILDER_DLL bool RBLoadWaypointRecording(const char* wptrec);
RAGEBUILDER_DLL bool RBLoadEffect(const char* effect);
RAGEBUILDER_DLL bool RBLoadVehicleRecording(const char* recording);
RAGEBUILDER_DLL bool RBLoadAudMesh(const char* audmesh);
RAGEBUILDER_DLL bool RBLoadMeta(const char* meta);
RAGEBUILDER_DLL bool RBLoadScript( const char* script );
RAGEBUILDER_DLL bool RBLoadOccMeshes( const char* occMeshes );
RAGEBUILDER_DLL bool RBLoadFrameFilter(const char *framefilter);
RAGEBUILDER_DLL bool RBLoadFrameFilterList(const char *framefilterlist);


RAGEBUILDER_DLL bool RBSaveTextureDictionary(const char* texdict, const char* hi_suffix);
RAGEBUILDER_DLL bool RBSaveTextureDictionaryHiMip(const char* texdict, const char* suffix);
RAGEBUILDER_DLL bool RBSaveClipDictionary(const char* clipdict, bool usePackLoader);
RAGEBUILDER_DLL bool RBSaveParamMotionDictionary(const char* parammotiondict);
RAGEBUILDER_DLL bool RBSaveExpressionsDictionary(const char* expressionsdict);
RAGEBUILDER_DLL bool RBSaveModelDictionary(const char* modeldict);
RAGEBUILDER_DLL bool RBSaveClothDictionary(const char* modeldict);
RAGEBUILDER_DLL bool RBSaveBlendShapeDictionary(const char* blendshapedict);
RAGEBUILDER_DLL bool RBSaveBoundDictionary(const char* bounddict);
RAGEBUILDER_DLL bool RBSavePhysicsDictionary(const char* physicsdict);
RAGEBUILDER_DLL bool RBSaveTexture(const char* texture);
RAGEBUILDER_DLL bool RBSaveModel(const char* model);
RAGEBUILDER_DLL bool RBSaveBlendShape(const char* blendshapeIn, const char* blendshapeOut);
RAGEBUILDER_DLL bool RBSaveFragment(const char* fragment);
RAGEBUILDER_DLL bool RBSaveBound(const char* bound);
RAGEBUILDER_DLL bool RBSavePathRegion(const char* pathRegion);
RAGEBUILDER_DLL bool RBSaveNavMesh(const char* navmesh);
RAGEBUILDER_DLL bool RBSaveHeightMesh(const char* navmesh);
RAGEBUILDER_DLL bool RBSaveHierarchicalNav(const char* hiernav);
RAGEBUILDER_DLL bool RBSaveWaypointRecording(const char* wptrec);
RAGEBUILDER_DLL bool RBSaveAnim(const char* anim);
RAGEBUILDER_DLL bool RBSaveEffect(const char* effect);
RAGEBUILDER_DLL bool RBSaveVehicleRecording(const char* recording);
RAGEBUILDER_DLL bool RBSaveAudMesh(const char* audmesh);
RAGEBUILDER_DLL bool RBSaveRbf(const char* rbf);
RAGEBUILDER_DLL bool RBSavePso(const char* pso);
RAGEBUILDER_DLL bool RBSavePsoAs(const char* pso, const char* format);
RAGEBUILDER_DLL int	 RBIsPsoInPlaceLoadable(const char* pso, bool printMismatches);
RAGEBUILDER_DLL int  RBPsoDebugStringSize(const char* pso);
RAGEBUILDER_DLL bool RBSaveMapData( const char* mapdata );
RAGEBUILDER_DLL bool RBSaveMapTypes( const char* mapdata );
RAGEBUILDER_DLL bool RBSaveCutfile( const char* cutfile );
RAGEBUILDER_DLL bool RBSaveImf( const char* imf );
RAGEBUILDER_DLL bool RBSaveScript( const char* script );
RAGEBUILDER_DLL bool RBSaveOccImap( const char* occMeshes );
RAGEBUILDER_DLL bool RBSaveOccXml( const char* occMeshes );
RAGEBUILDER_DLL bool RBSaveFrameFilterDictionary(const char* framefilterdict);
RAGEBUILDER_DLL bool RBSaveTextDatabase(const char* textdatabase);


RAGEBUILDER_DLL bool RBSetMaxMipMapCount(int maxmipmap);
RAGEBUILDER_DLL bool RBSetMipMapInterleaving(bool interleaving);
RAGEBUILDER_DLL bool RBSetTextureMin(int min);
RAGEBUILDER_DLL bool RBSetTextureMax(int max);
RAGEBUILDER_DLL bool RBSetTextureScale(float scale);
RAGEBUILDER_DLL bool RBSetLimitExemptTextures(const char* scale);
RAGEBUILDER_DLL bool RBSetOverloadTextureSrc(const char* scale);
RAGEBUILDER_DLL bool RBSetCompressResources(bool compress);
RAGEBUILDER_DLL bool RBSetTriStripping(bool tristrip);
RAGEBUILDER_DLL bool RBPushBuildVars(void);
RAGEBUILDER_DLL bool RBPopBuildVars(void);
RAGEBUILDER_DLL bool RBLoadTextureList(const char* texlist);
RAGEBUILDER_DLL bool RBDoesTexHaveMips(const char* filename);
RAGEBUILDER_DLL bool RBPostProcessVehicle(bool postprocess);
RAGEBUILDER_DLL bool RBHeadBlendGeometry(bool enable);
RAGEBUILDER_DLL bool RBHeadBlendWriteBuffer(bool enable);
RAGEBUILDER_DLL bool RBMicroMorphMesh(bool enable);
RAGEBUILDER_DLL bool RBEnableFragMatrixSet(bool enable);
RAGEBUILDER_DLL bool RBPPUOnlyHint(bool onOff);
RAGEBUILDER_DLL bool RBLodSkeleton(bool enable);
RAGEBUILDER_DLL const char* RBGetSettings();
RAGEBUILDER_DLL bool RBSetSettings(const char* newsettings);
RAGEBUILDER_DLL bool RBCompressAnim(const char* srcfile, const char* skelfile, const char* outfile, int maxblocksize, float compressionrotation, float compressiontranslation, float compressionscale, float compressiondefault, int compressioncost, int decompressioncost, const char* compressionrules, bool rawonly );
RAGEBUILDER_DLL const char* RBGetClipPropertyString(const char* propertyname, const char* clipfile, bool useLoader);
RAGEBUILDER_DLL int RBGetClipPropertyInt(const char* propertyname, const char* clipfile, bool useLoader );
RAGEBUILDER_DLL float RBGetClipPropertyFloat(const char* propertyname, const char* clipfile, bool useLoader );
RAGEBUILDER_DLL bool RBSetClipPropertyString(const char* propertyname, const char* val, const char* clipfile, bool useLoader );
RAGEBUILDER_DLL bool RBConvertAndSaveDDS( const char* ddsFileSrc, const char* ddsFileDest, const char* templateName, const char* platform );
RAGEBUILDER_DLL bool RBValidateClip( const char* clipname );
RAGEBUILDER_DLL bool RBSetDwdType( const char* dwdtype );
RAGEBUILDER_DLL bool RBGenerateDrawableStats( const char* drawablePathname, const char* outputPathname );
RAGEBUILDER_DLL bool RBGenerateFragmentStats( const char* fragmentPathname, const char* outputPathname );
RAGEBUILDER_DLL bool RBSetEdgify( bool value );


#ifdef __cplusplus
}
#endif
