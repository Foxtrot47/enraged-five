
// Local headers
#include "gta.h"
#include "utils.h"
#include "progress.h"

// Rage headers
#include "file/device.h"
#include "string/stringhash.h"
#include "system/endian.h"
#include "vector/matrix34.h"
#include "vector/quaternion.h"

#include <stdio.h>

#define MAX_PATH 2048

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
IdeSet GtaData::m_ideSet;
IplSet GtaData::m_iplSet;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool SetReader::readLine(fiStream* p_stream,char* p_line,s32 lineMax)
{
	s32 index = 0;
	bool started = false;

	while(p_stream->ReadByte(&p_line[index],1))
	{
		if(!started)
		{
			if((p_line[index] != '\n') && (p_line[index] != '\r'))
				started = true;
		}

		if(started)
		{
			if((p_line[index] == '\n') || (p_line[index] == '\r'))
				break;

			index++;

			if(index == lineMax)
				break;
		}
	}

	if(index == 0)
		return false;

	p_line[index] = '\0';

	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void SetReader::removeCommas(char* p_line)
{
	s32 readIndex = 0;
	s32 writeIndex = 0;

	while(p_line[readIndex] != '\0')
	{
		p_line[writeIndex] = p_line[readIndex];

		if(p_line[readIndex] != ',')
			writeIndex++;

		readIndex++;
	}

	p_line[writeIndex] = '\0';
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

IdeSet::~IdeSet()
{
	reset();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void IdeSet::reset()
{
	// Object definitions
	m_listObjectHash.Reset();
	m_listTxdNames.Reset();
	m_listDdNames.Reset();
	m_listObjectNames.Reset();
	m_listObjectDefs.Reset();

	// Time Object definitions
	m_listTimeObjectHash.Reset();
	m_listTimeObjectNames.Reset();
	m_listTimeObjectTxdNames.Reset();
	m_listTimeObjectDdNames.Reset();
	m_listTimeObjectDefs.Reset();

	m_listTreeHash.Reset();
	m_listTreeNames.Reset();
	m_listTreeDefs.Reset();

	// Milo definitions
	m_listMiloHash.Reset();
	m_listMiloNames.Reset();
	m_listMiloDefs.Reset();
	m_listMiloObjNames.Reset();

	// Ped definitions
	m_listPedDefs.Reset();

	//Effect definitions
	m_listEffectDefs.Reset();
}

// Needed for flag -> flashiness conversion
#define OLD_LIGHTFLAG_ELECTRIC						(1<<7)
#define OLD_LIGHTFLAG_STROBE						(1<<9)
#define OLD_LIGHTFLAG_PLANE							(1<<12)

enum eLightFlashiness
{
	FL_CONSTANT = 0,				// on all the time
	FL_RANDOM,						// flickers randomly
	FL_RANDOM_OVERRIDE_IF_WET,		// flickers randomly (forced if road is wet)
	FL_ONCE_SECOND,					// on/off once every second
	FL_TWICE_SECOND,				// on/off twice every second
	FL_FIVE_SECOND,					// on/off five times every second
	FL_RANDOM_FLASHINESS,			// might flicker, might not
	FL_OFF,							// always off. really only used for traffic lights
	FL_UNUSED1,						// Not used
	FL_ALARM,						// Flashes on briefly
	FL_ON_WHEN_RAINING,				// Only render when it's raining.
	FL_CYCLE_1,						// Fades in and out in so that the three of them together always are on (but cycle through colours)
	FL_CYCLE_2,						// Fades in and out in so that the three of them together always are on (but cycle through colours)
	FL_CYCLE_3,						// Fades in and out in so that the three of them together always are on (but cycle through colours)
	FL_DISCO,						// In tune with the music
	FL_CANDLE,						// Just like a candle
	FL_PLANE,						// The little lights on planes/helis. They flash briefly
	FL_FIRE,						// A more hectic version of the candle
	FL_THRESHOLD,					// experimental
	FL_ELECTRIC,					// Change colour slightly
	FL_STROBE,						// Strobe light
	FL_COUNT
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void IdeSet::load(const char* pFilename)
{
	enum
	{
		STATE_NONE,
		STATE_OBJS,
		STATE_TOBJ,
		STATE_ANMS,
		STATE_PEDS,
		STATE_TREE,
		STATE_MILO,
		STATE_2DFX
	};

	s32 i;
	s32 State = STATE_NONE;
	fiStream* p_ideFile = fiStream::Open(pFilename);

	if(!p_ideFile)
		return;

	char lineData[1024];

	while(readLine(p_ideFile,lineData,1024))
	{
		if(lineData[0] == '#')
			continue;

		if(stricmp(lineData,"end") == 0)
		{
			State = STATE_NONE;
			continue;
		}

		switch(State)
		{
		case STATE_NONE:

			if(stricmp(lineData,"objs") == 0)
				State = STATE_OBJS;
			if(stricmp(lineData,"tobj") == 0)
				State = STATE_TOBJ;
			if(stricmp(lineData,"peds") == 0)
				State = STATE_PEDS;
			if(stricmp(lineData,"tree") == 0)
				State = STATE_TREE;
			if(stricmp(lineData,"mlo") == 0)
				State = STATE_MILO;
			if(stricmp(lineData,"anim") == 0)
				State = STATE_ANMS;
			if(stricmp(lineData,"2dfx") == 0)
				State = STATE_2DFX;

			break;

		case STATE_2DFX:
			{
				s32 CurrentSize = m_listEffectDefs.GetCount();
				EffectDef& effectDef = m_listEffectDefs.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));
				removeCommas(lineData);

				char name[48];
				char projectedTextureNameTemp[256];
				char projectedTextureName[64];

				int32 lightType;
				int32 colourR, colourG, colourB;
				int32 flashiness;
				int32 boneTag;
				int32 flags, newFlags;
				int32 lightHash;
				int32 volFadeDist;
				int32 specFadeDist;
				int32 shadowFadeDist;
				int32 lightFadeDist;
// 				float tempCullPlane[4];
// 				uint8 tempOuterColour[3];
// 				float tempOuterColourInten;
				float capsuleWidth;
				int32 lightShadowBlur = 0;

				int32 numArgs = sscanf(lineData, 
					"%s \
					%f %f %f \
					%*d %*f %*f %*f %*f \
					%d \
					%d %d %d \
					%*d %*s \
					%s \
					%*f %*d \
					%f \
					%f \
					%d %d \
					%f \
					%f %d %f %f %f %f %f \
					%d \
					%f %f %f %f %f %f %f %d \
					%*f %*f \
					%f %d %f %f %f %f %f %d %d %d %f %f \
					%d %d %d %d \
					%d",
					&name[0],
					&effectDef.l.m_posn.x, &effectDef.l.m_posn.y, &effectDef.l.m_posn.z,
					//,
					//, 
					//,
					//,
					//,
					&lightType,
					&colourR, &colourG, &colourB, 
					//,
					//,
					&projectedTextureNameTemp[0],
					//,
					//,
					&effectDef.l.m_coronaSize,
					&capsuleWidth,
					&lightHash,
					&flashiness,
					&effectDef.l.m_shadowNearClip,
					&effectDef.l.m_volOuterExponent,
					&flags,
					&effectDef.l.m_coneInnerAngle,
					&effectDef.l.m_coneOuterAngle,
					&effectDef.l.m_intensity,
					&effectDef.l.m_volIntensity,
					&effectDef.l.m_volSizeScale,
					&lightShadowBlur,
					&effectDef.l.m_falloff,
					&effectDef.l.m_direction.x,
					&effectDef.l.m_direction.y,
					&effectDef.l.m_direction.z,
					&effectDef.l.m_tangent.x,
					&effectDef.l.m_tangent.y,
					&effectDef.l.m_tangent.z,
					&boneTag,
					//,
					//,
					&effectDef.l.m_coronaIntensity,
					&effectDef.l.m_timeFlags,
					&effectDef.l.m_falloffExponent,
					&effectDef.l.m_cullingPlane[0],
					&effectDef.l.m_cullingPlane[1],
					&effectDef.l.m_cullingPlane[2],
					&effectDef.l.m_cullingPlane[3],
					&effectDef.l.m_volOuterColour.red,
					&effectDef.l.m_volOuterColour.green,
					&effectDef.l.m_volOuterColour.blue,
					&effectDef.l.m_volOuterIntensity,
					&effectDef.l.m_coronaZBias,
					&lightFadeDist,
					&shadowFadeDist,
					&specFadeDist,
					&volFadeDist,
					&newFlags
					);

				const int expectedArgCountMax = 47;
				const int expectedArgCountMin = expectedArgCountMax-15;

 				if (numArgs > expectedArgCountMax || numArgs < expectedArgCountMin)
				{
					Assertf(false, "Light with incorrect number of arguments: Got %d and expected 30 or 40", numArgs);
					Errorf("Light with incorrect number of arguments: Got %d and expected 30 or 40", numArgs);
				}
				else 
				{
					if(strlen(projectedTextureNameTemp) > 2)
					{
						strcpy_s(projectedTextureName, 64, projectedTextureNameTemp + 1);
						projectedTextureName[strlen(projectedTextureName) - 1] = '\0';
					}
					else
					{
						projectedTextureName[0] = '\0';
					}

					if (projectedTextureName[0] == '\0' || strcmp(projectedTextureName,"null") == 0 || strcmp(projectedTextureName,"shad_exp") == 0)
						effectDef.l.m_projectedTextureKey = 0;
					else
						effectDef.l.m_projectedTextureKey = atStringHash(projectedTextureName);
					
					// Had issues scanning these in with scanf (everything thats not float/u32)
					effectDef.l.m_flashiness = static_cast<u8>(flashiness);

					effectDef.l.m_lightType = static_cast<u8>(lightType);
					effectDef.l.m_boneTag = static_cast<s16>(boneTag);
					effectDef.l.m_lightHash = static_cast<u8>(lightHash);

					effectDef.l.m_colour.red = static_cast<u8>(colourR);
					effectDef.l.m_colour.green = static_cast<u8>(colourG);
					effectDef.l.m_colour.blue = static_cast<u8>(colourB);	

					effectDef.l.m_lightFadeDistance = static_cast<u8>(lightFadeDist);
					effectDef.l.m_shadowFadeDistance = static_cast<u8>(shadowFadeDist);
					effectDef.l.m_specularFadeDistance = static_cast<u8>(specFadeDist);
					effectDef.l.m_volumetricFadeDistance = static_cast<u8>(volFadeDist);

					effectDef.l.m_shadowBlur = static_cast<u8>(lightShadowBlur);

					if(numArgs < expectedArgCountMax)
					{
						// PD_TODO: Remove once export side sorted.
						newFlags = 0;
						u32 oldToNewFlags[14] = { 2,3,4,8,10,11,13,14,15,16,17,18,19,20 };
#define IS_SET(x, y) ((y & (1 << x)) != 0)

						for (int i = 0; i < 32; i++) 
						{
							if (i < 14)
							{
								// Old flags so we need to convert
								newFlags |= (IS_SET(oldToNewFlags[i], flags) << i);
							}
							else
							{
								// New flags so just shift them
								u32 oldFlagBit = oldToNewFlags[13] + 1 + (i - 14);
								if (oldFlagBit < 32)
								{
									newFlags |= (IS_SET(oldFlagBit, flags) << i);
								}
							}
						}

						// PD_TODO: Remove once export side sorted.
						if ((flags & OLD_LIGHTFLAG_ELECTRIC) != 0)
						{
							Assertf(flashiness == 0, "Overwriting flashiness with ELECTRIC flag");
							flashiness = FL_ELECTRIC;
						}

						if ((flags & OLD_LIGHTFLAG_STROBE) != 0)
						{
							Assertf(flashiness == 0, "Overwriting flashiness with STROBE flag");
							flashiness = FL_STROBE;
						}

						if ((flags & OLD_LIGHTFLAG_PLANE) != 0)
						{
							Assertf(flashiness == 0, "Overwriting flashiness with PLANE flag");
							flashiness = FL_PLANE;
						}
					}
					if(numArgs < (expectedArgCountMax-1))
					{
						effectDef.l.m_lightFadeDistance			= 0U;
						effectDef.l.m_shadowFadeDistance		= 0U;
						effectDef.l.m_specularFadeDistance		= 0U;
						effectDef.l.m_volumetricFadeDistance	= 0U;
					}
					if(numArgs < (expectedArgCountMax-5))
					{
						effectDef.l.m_coronaZBias = 0.1f;
					}
					if(numArgs < (expectedArgCountMax-9))
					{
						effectDef.l.m_cullingPlane[0] = 0.0f;
						effectDef.l.m_cullingPlane[1] = 0.0f;
						effectDef.l.m_cullingPlane[2] = 1.0f;
						effectDef.l.m_cullingPlane[3] = effectDef.l.m_falloff;

						effectDef.l.m_volOuterColour.red = 
							effectDef.l.m_volOuterColour.green = 
								effectDef.l.m_volOuterColour.blue = 255;
						effectDef.l.m_volOuterIntensity = 1.0f;
						
						if(numArgs == (expectedArgCountMax-16))
						{
							effectDef.l.m_falloffExponent = 8.0f;
						}					
					}
					effectDef.l.m_flags = newFlags;

					// JWR - push the capsuleWidth into the extents
					effectDef.l.m_extents.x = effectDef.l.m_extents.y = effectDef.l.m_extents.z = capsuleWidth;
				}
			}

			break;

		case STATE_OBJS:

			{
				s32 CurrentSize = m_listObjectDefs.GetCount();
				ObjectDef& objectDef = m_listObjectDefs.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));
				atString& objectName = m_listObjectNames.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));
				atString& a_txdName = m_listTxdNames.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));
				atString& a_ddName = m_listDdNames.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));

				removeCommas(lineData);

				char name[128];
				char txdName[128];
				char ddName[128];

				sscanf(lineData,"%s %s %f %d %f %f %f %f %f %f %f %f %f %f %f %s",	&name[0],
																					&txdName[0],
																					&objectDef.lodDist,
																					&objectDef.flags,
																					&objectDef.specialAttribute,
																					&objectDef.bbMinX, &objectDef.bbMinY, &objectDef.bbMinZ,
																					&objectDef.bbMaxX, &objectDef.bbMaxY, &objectDef.bbMaxZ,
																					&objectDef.bsCentreX, &objectDef.bsCentreY, &objectDef.bsCentreZ,
																					&objectDef.bsRadius,
																					&ddName[0]);

				a_txdName = txdName;
				a_ddName = ddName;

				objectName = name;
				objectName += "-";
				objectName += pFilename;

				strupr(name);
				objectDef.modelHashKey = atStringHash(&name[0]);
				objectDef.txdHashKey = atStringHash(&txdName[0]);
				objectDef.ddModelHashKey = atStringHash(&ddName[0]);

				m_listObjectHash.SafeInsert(objectDef.modelHashKey,CurrentSize);
			}

			break;

		case STATE_TOBJ:

			{
				s32 CurrentSize = m_listTimeObjectDefs.GetCount();
				ObjectDef& objectDef = m_listTimeObjectDefs.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));
				atString& objectName = m_listTimeObjectNames.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));
				atString& a_txdName = m_listTimeObjectTxdNames.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));
				atString& a_ddName = m_listTimeObjectDdNames.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));

				removeCommas(lineData);

				char name[128];
				char txdName[128];
				char ddName[128];

				sscanf(lineData,"%s %s %f %d %f %f %f %f %f %f %f %f %f %f %f %s",	&name[0],
					&txdName[0],
					&objectDef.lodDist,
					&objectDef.flags,
					&objectDef.specialAttribute,
					&objectDef.bbMinX, &objectDef.bbMinY, &objectDef.bbMinZ,
					&objectDef.bbMaxX, &objectDef.bbMaxY, &objectDef.bbMaxZ,
					&objectDef.bsCentreX, &objectDef.bsCentreY, &objectDef.bsCentreZ,
					&objectDef.bsRadius,
					&ddName[0]);

				a_txdName = txdName;
				a_ddName = ddName;

				objectName = name;
				objectName += "-";
				objectName += pFilename;
				strupr(name);
				objectDef.modelHashKey = atStringHash(&name[0]);
				objectDef.txdHashKey = atStringHash(&txdName[0]);
				objectDef.ddModelHashKey = atStringHash(&ddName[0]);

				m_listTimeObjectHash.SafeInsert(objectDef.modelHashKey,CurrentSize);
			}

			break;

		case STATE_ANMS:

			{
				s32 CurrentSize = m_listObjectDefs.GetCount();
				ObjectDef& objectDef = m_listObjectDefs.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));
				atString& objectName = m_listObjectNames.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));
				atString& a_txdName = m_listTxdNames.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));
				atString& a_ddName = m_listDdNames.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));

				removeCommas(lineData);

				char name[128];
				char txdName[128];
				char iadName[128];
				char ddName[128];

				sscanf(lineData,"%s %s %s %f %d %f %f %f %f %f %f %f %f %f %f %f %s",	&name[0],
					&txdName[0],
					&iadName[0],
					&objectDef.lodDist,
					&objectDef.flags,
					&objectDef.specialAttribute,
					&objectDef.bbMinX, &objectDef.bbMinY, &objectDef.bbMinZ,
					&objectDef.bbMaxX, &objectDef.bbMaxY, &objectDef.bbMaxZ,
					&objectDef.bsCentreX, &objectDef.bsCentreY, &objectDef.bsCentreZ,
					&objectDef.bsRadius,
					&ddName[0]);

				a_ddName = ddName;
				a_txdName = txdName;

				objectName = name;
				objectName += "-";
				objectName += pFilename;
				strupr(name);
				objectDef.modelHashKey = atStringHash(&name[0]);
				objectDef.txdHashKey = atStringHash(&txdName[0]);
				objectDef.ddModelHashKey = atStringHash(&ddName[0]);

				m_listObjectHash.SafeInsert(objectDef.modelHashKey,CurrentSize);
			}

			break;

		case STATE_PEDS:

			{
				s32 CurrentSize = m_listPedDefs.GetCount();
				PedDef& pedDef = m_listPedDefs.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));

				removeCommas(lineData);

				sscanf(lineData,"%s",&pedDef.name[0]);
			}

			break;

		case STATE_TREE:
			{
				s32 CurrentSize = m_listTreeDefs.GetCount();
				TreeDef& treeDef = m_listTreeDefs.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));
				atString& objectName = m_listTreeNames.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));

				removeCommas(lineData);

				char name[128];

				sscanf(lineData,"%s",name);

				objectName = name;
				strupr(name);
				treeDef.modelHashKey = atStringHash(&name[0]);

				m_listObjectHash.SafeInsert(treeDef.modelHashKey,CurrentSize);
			}

			break;

		case STATE_MILO:
			{
				s32 CurrentSize = m_listMiloDefs.GetCount();
				MiloDef& miloDef = m_listMiloDefs.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));
				MiloObjNames& miloNames = m_listMiloObjNames.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));
				atString& objectName = m_listMiloNames.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));

				removeCommas(lineData);

				char name[128];
				s32 ValA;
				s32 ValB;
				s32 ValC;
				s32 ObjectCount;

				s32 VarCount = sscanf(lineData,"%s %d %d %d %d",name,&ValA,&ValB,&ValC,&ObjectCount);

				objectName = name;
				objectName += "-";
				objectName += pFilename;
				strupr(name);
				miloDef.modelHashKey = atStringHash(&name[0]);

				if(VarCount == 5)
				{
					for(i=0;i<ObjectCount;i++)
					{
						char objname[128];

						readLine(p_ideFile,lineData,1024);
						removeCommas(lineData);

						MiloItemDef item;

						sscanf(lineData,"%s %f %f %f %f %f %f %f",	objname,
																	&item.posx,
																	&item.posy,
																	&item.posz,
																	&item.quatx,
																	&item.quaty,
																	&item.quatz,
																	&item.quatw);

						item.objectHashKey = atStringHash(&objname[0]);

						miloDef.m_objects.PushAndGrow(item);
						miloNames.m_names.PushAndGrow(atString(objname));
					}

					m_listMiloHash.SafeInsert(miloDef.modelHashKey,CurrentSize);

					bool startRooms = false;

					while(readLine(p_ideFile,lineData,1024))
					{
						if(stricmp(lineData,"mloroomstart") == 0)
						{
							startRooms = true;
							break;
						}

						if(stricmp(lineData,"mloend") == 0)
							break;

						MiloItemDef item;
						char objname[128];
						removeCommas(lineData);

						if(sscanf(lineData,"%s %f %f %f %f %f %f %f",	objname,
								&item.posx,
								&item.posy,
								&item.posz,
								&item.quatx,
								&item.quaty,
								&item.quatz,
								&item.quatw) == 8)
						{
							item.objectHashKey = atStringHash(&objname[0]);

							miloDef.m_objects.PushAndGrow(item);
							miloNames.m_names.PushAndGrow(atString(objname));
						}
					}

					if(startRooms)
					{
						while(readLine(p_ideFile,lineData,1024))
						{
							if(stricmp(lineData,"mloportalstart") == 0)
								break;

							MiloRoomDef roomDef;

							removeCommas(lineData);

							//first line of room is here
							char roomname[64];
							sscanf(lineData,"%s",roomname);

							strcpy(roomDef.name,roomname);

							while(readLine(p_ideFile,lineData,1024))
							{
								removeCommas(lineData);

								if(stricmp(lineData,"\troomend") == 0)
									break;

								//subsequent lines here
								s32 objects[16];
								sscanf(lineData,"%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d",	&objects[0],
																									&objects[1],
																									&objects[2],
																									&objects[3],
																									&objects[4],
																									&objects[5],
																									&objects[6],
																									&objects[7],
																									&objects[8],
																									&objects[9],
																									&objects[10],
																									&objects[11],
																									&objects[12],
																									&objects[13],
																									&objects[14],
																									&objects[15]);

								for(s32 i=0;i<16;i++)
								{
									if(objects[i] != -1)
									{
										roomDef.instanceList.PushAndGrow(objects[i]);
									}
								}
							}

							miloDef.m_rooms.PushAndGrow(roomDef);
						}

						while(readLine(p_ideFile,lineData,1024))
						{
							if(stricmp(lineData,"mloend") == 0)
								break;
						}
					}
				}
				else
				{
					while(readLine(p_ideFile,lineData,1024))
					{
						if(stricmp(lineData,"mloend") == 0)
							break;

						char objname[128];

						readLine(p_ideFile,lineData,1024);
						removeCommas(lineData);

						MiloItemDef item;

						sscanf(lineData,"%s %f %f %f %f %f %f %f",	objname,
							&item.posx,
							&item.posy,
							&item.posz,
							&item.quatx,
							&item.quaty,
							&item.quatz,
							&item.quatw);

						item.objectHashKey = atStringHash(&objname[0]);

						miloDef.m_objects.PushAndGrow(item);
					}

					m_listMiloHash.SafeInsert(miloDef.modelHashKey,CurrentSize);
				}
			}

			break;
		}
	}

	p_ideFile->Close();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void IdeSet::loadBinary(const char* pFilename,bool SwapEndian)
{
	char path[MAX_PATH];
	char basename[MAX_PATH];
	char extension[MAX_PATH];

	SplitFilename(pFilename,path,basename,extension);

	strcat(path,basename);
	strcat(path,".bde");

	fiStream* p_ideFile = fiStream::Open(path);

	s32 Count;

	p_ideFile->ReadInt(&Count,1);

	m_listObjectDefs.Reset();

	if(Count)
	{
		m_listObjectDefs.Resize(Count);
		p_ideFile->Read(&m_listObjectDefs[0],sizeof(ObjectDef) * Count);
	}

	p_ideFile->Close();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void IdeSet::save(const char* pFilename)
{

}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void IdeSet::saveBinary(const char* pFilename,bool SwapEndian)
{
	fiDeviceLocal::GetInstance().Delete(pFilename);
	fiStream* p_ideFile = fiStream::Create(pFilename);

	IdeFileHeader ideFileHeader;
	ideFileHeader.Version = IDE_VERSION;
	ideFileHeader.NumObjects = m_listObjectDefs.GetCount();
	ideFileHeader.NumExtra2 = 0;
	ideFileHeader.NumExtra3 = 0;
	ideFileHeader.NumExtra4 = 0;
	ideFileHeader.NumExtra5 = 0;
	ideFileHeader.NumExtra6 = 0;
	ideFileHeader.NumExtra7 = 0;
	ideFileHeader.NumExtra8 = 0;
	ideFileHeader.NumExtra9 = 0;
	ideFileHeader.NumExtra10 = 0;
	ideFileHeader.NumExtra11 = 0;
	ideFileHeader.NumExtra12 = 0;
	ideFileHeader.NumExtra13 = 0;
	ideFileHeader.NumExtra14 = 0;
	ideFileHeader.NumExtra15 = 0;
	ideFileHeader.NumExtra16 = 0;

	p_ideFile->Write(&ideFileHeader,sizeof(IdeFileHeader));

	if(ideFileHeader.NumObjects)
		p_ideFile->Write(&m_listObjectDefs[0],ideFileHeader.NumObjects * sizeof(ObjectDef));

	p_ideFile->Close();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool IdeSet::doesObjectExist(u32 objectHash)
{
	return (m_listObjectHash.Has(objectHash) || m_listTreeHash.Has(objectHash) || m_listMiloHash.Has(objectHash) || m_listTimeObjectHash.Has(objectHash));
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool IdeSet::isObjectAMilo(u32 objectHash)
{
	return m_listMiloHash.Has(objectHash);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool IdeSet::isObjectInMilo(u32 objectHash)
{
	s32 i,Count = m_listMiloDefs.GetCount();

	for(i=0;i<Count;i++)
	{
		s32 j,ObjCount = m_listMiloDefs[i].m_objects.GetCount();

		for(j=0;j<ObjCount;j++)
		{
			if(m_listMiloDefs[i].m_objects[j].objectHashKey == objectHash)
				return true;
		}
	}

	return false;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
s32 IdeSet::getMiloNumInstances(s32 index)
{
	return m_listMiloDefs[index].m_objects.GetCount();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
atString& IdeSet::getMiloInstanceName(s32 index,s32 instanceindex)
{
	return m_listMiloObjNames[index].m_names[instanceindex];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
s32 IdeSet::getMiloNumRooms(s32 index)
{
	return m_listMiloDefs[index].m_rooms.GetCount();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const char* IdeSet::getMiloRoomName(s32 index,s32 roomindex)
{
	return m_listMiloDefs[index].m_rooms[roomindex].name;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
s32 IdeSet::getMiloRoomNumInstances(s32 index,s32 roomindex)
{
	return m_listMiloDefs[index].m_rooms[roomindex].instanceList.GetCount();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
s32 IdeSet::getMiloRoomInstance(s32 index,s32 roomindex,s32 instanceindex)
{
	return m_listMiloDefs[index].m_rooms[roomindex].instanceList[instanceindex];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
IplSet::IplSet()
	: m_nIplVersion( IPL4_VERSION )	// Init default version
	, m_fMaxLodDistance( 0.0f )
{
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

IplSet::~IplSet()
{
	reset();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void IplSet::reset()
{
	m_fMaxLodDistance = 0.0f;
	m_listInstanceNames.Reset();
	m_listInstanceDefs.Reset();
	m_listInstance5Defs.Reset();
	m_listMiloInstance6Defs.Reset();
	m_listMiloInstanceNames.Reset();
	m_listBoundDefs.Reset();
	m_listZoneDefs.Reset();
	m_listGarageDefs.Reset();
	m_listCarGenDefs.Reset();
	m_listTimeCycDefs.Reset();
	m_listPathNodeDefs.Reset();
	m_listPathLinkDefs.Reset();
	m_listBlockDefs.Reset();
	m_listRootScriptDefs.Reset();
	m_listMiloPlusInstanceDefs.Reset();
	m_listMiloPlusNameHashes.Reset();
	m_lodmodifiersDefs.Reset();
	m_slowzoneDefs.Reset();
	m_listPatrolNodeDefs.Reset();
	m_listPatrolLinkDefs.Reset();
	m_listLodChildDefs.Reset();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void IplSet::load(const char* pFilename)
{
	enum
	{
		STATE_NONE,
		STATE_VERS,
		STATE_INSTANCES,
//		STATE_MULTI,
		STATE_ZONES,
//		STATE_CULLZONES,
		STATE_PATHNODES,
		STATE_PATHLINKS,
//		STATE_OCCLUSION,
		STATE_GARAGE,
//		STATE_ENTRYEXIT,
//		STATE_PICKUPS,
		STATE_CARGEN,
//		STATE_STUNTJUMP,
		STATE_TIMECYCMOD,
//		STATE_AUDIOZON,
		STATE_BLOCKS,
		STATE_ROOTSCRIPTS,
		STATE_MILOPLUS,
		STATE_LODMODIFIERS,
		STATE_SLOWZONES,
		STATE_PATROLNODES,
		STATE_PATROLLINKS,
		STATE_BOUND,
		STATE_INTERIORS,
		STATE_LODCHILDREN,
//		STATE_LIGHTINST,
		STATE_MAXLODDISTANCE,
	};

	s32 State = STATE_NONE;
	fiStream* p_ideFile = fiStream::Open(pFilename);

	if(!p_ideFile)
		return;

	char lineData[8192];

	while(readLine(p_ideFile,lineData,8192))
	{
		switch(State)
		{
		case STATE_NONE:

			if		(stricmp(lineData, "vers")        == 0) State = STATE_VERS;
			else if	(stricmp(lineData, "inst")        == 0) State = STATE_INSTANCES;
			else if	(stricmp(lineData, "zone")        == 0) State = STATE_ZONES;
			else if	(stricmp(lineData, "vnod")        == 0) State = STATE_PATHNODES;
			else if	(stricmp(lineData, "link")        == 0) State = STATE_PATHLINKS;
			else if	(stricmp(lineData, "grge")        == 0) State = STATE_GARAGE;
			else if	(stricmp(lineData, "cars")        == 0) State = STATE_CARGEN;
			else if	(stricmp(lineData, "tcyc")        == 0) State = STATE_TIMECYCMOD;
			else if	(stricmp(lineData, "blok")        == 0) State = STATE_BLOCKS;
			else if	(stricmp(lineData, "rtfx")        == 0) State = STATE_ROOTSCRIPTS;
			else if	(stricmp(lineData, "mlo+")        == 0) State = STATE_MILOPLUS;
			else if	(stricmp(lineData, "lodm")        == 0) State = STATE_LODMODIFIERS;
			else if	(stricmp(lineData, "slow")        == 0) State = STATE_SLOWZONES;
			else if	(stricmp(lineData, "pnod")        == 0) State = STATE_PATROLNODES;
			else if	(stricmp(lineData, "plnk")        == 0) State = STATE_PATROLLINKS;
			else if	(stricmp(lineData, "bounds")      == 0) State = STATE_BOUND;
			else if	(stricmp(lineData, "miloinst")    == 0) State = STATE_INTERIORS;
			else if	(stricmp(lineData, "lodchildren") == 0) State = STATE_LODCHILDREN;
			else if	(stricmp(lineData, "maxlod")      == 0) State = STATE_MAXLODDISTANCE;

			break;

		case STATE_VERS:
			{
				sscanf(lineData, "%d", &m_nIplVersion);
				switch (m_nIplVersion)
				{
				case IPL6_VERSION:
					m_nIplVersion = IPL6_VERSION;
					break;
				case IPL5_VERSION:
					m_nIplVersion = IPL5_VERSION;
					break;
				case IPL4_VERSION:
				default:
					m_nIplVersion = IPL4_VERSION;
					break;
				}
				State = STATE_NONE;
			}
			break;

		case STATE_INSTANCES:
			{
				if(stricmp(lineData,"end") == 0)
					State = STATE_NONE;
				else
				{
					s32 CurrentSize = -1;
					removeCommas(lineData);

					if (IPL5_VERSION == m_nIplVersion || IPL6_VERSION == m_nIplVersion)
					{
						CurrentSize = m_listInstance5Defs.GetCount();
						Instance5Def& instanceDef = m_listInstance5Defs.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));
						atString& instanceName = m_listInstanceNames.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));
						m_listInstanceIsLod.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1)) = false;

						char name[128];
						instanceDef.lodDistance = -1;

						int paramCount = sscanf(lineData, "%s %d %f %f %f %f %f %f %f %d %d %f %d %d %d %f %f",
							&name,
							&instanceDef.areaCodeAndFlags,
							&instanceDef.posx,
							&instanceDef.posy,
							&instanceDef.posz,
							&instanceDef.quatx,
							&instanceDef.quaty,
							&instanceDef.quatz,
							&instanceDef.quatw,
							&instanceDef.lodIndex,
							&instanceDef.flagsExtra,
							&instanceDef.lodDistance,
							&instanceDef.typeOrNumAncestors,
							&instanceDef.numChildren,
							&instanceDef.ambOcclMultiplier,
							&instanceDef.scaleXY,
							&instanceDef.scaleZ);
						if( paramCount == 14 )
							instanceDef.ambOcclMultiplier = 255;
						if( paramCount <= 15)
						{
							instanceDef.scaleXY = 1.0f;
							instanceDef.scaleZ = 1.0f;
						}

						instanceName = name;
						instanceName += "-";
						instanceName += pFilename;
						strupr(name);
						instanceDef.modelHashKey = atStringHash(&name[0]);

						if((instanceDef.lodIndex != -1)&&(instanceDef.lodIndex < m_listInstanceIsLod.GetCount()))
							m_listInstanceIsLod[instanceDef.lodIndex] = true;
					}
					else
					{
						CurrentSize = m_listInstanceDefs.GetCount();
						InstanceDef& instanceDef = m_listInstanceDefs.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));
						atString& instanceName = m_listInstanceNames.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));
						m_listInstanceIsLod.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1)) = false;

						char name[128];

						instanceDef.lodDistance = -1;

						sscanf(lineData, "%s %d %f %f %f %f %f %f %f %d %d %f",	&name,
							&instanceDef.areaCodeAndFlags,
							&instanceDef.posx,
							&instanceDef.posy,
							&instanceDef.posz,
							&instanceDef.quatx,
							&instanceDef.quaty,
							&instanceDef.quatz,
							&instanceDef.quatw,
							&instanceDef.lodIndex,
							&instanceDef.blockIndex,
							&instanceDef.lodDistance);
						instanceName = name;
						instanceName += "-";
						instanceName += pFilename;
						strupr(name);
						instanceDef.modelHashKey = atStringHash(&name[0]);

						if((instanceDef.lodIndex != -1)&&(instanceDef.lodIndex < m_listInstanceIsLod.GetCount()))
							m_listInstanceIsLod[instanceDef.lodIndex] = true;
					}
				}
			}

			break;

		case STATE_ZONES:

			{
				if(stricmp(lineData,"end") == 0)
					State = STATE_NONE;
				else
				{
					s32 CurrentSize = m_listZoneDefs.GetCount();
					ZoneDef& zoneDef = m_listZoneDefs.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));

					removeCommas(lineData);

					sscanf(lineData, "%s %d %f %f %f %f %f %f %d %s",	zoneDef.name,
						&zoneDef.type,
						&zoneDef.minx, &zoneDef.miny, &zoneDef.minz,
						&zoneDef.maxx, &zoneDef.maxy, &zoneDef.maxz,
						&zoneDef.level,
						zoneDef.text_label);
				}

				return;
			}

			break;

		case STATE_PATHNODES:

			{
				if(stricmp(lineData,"end") == 0)
					State = STATE_NONE;
				else
				{
					s32 CurrentSize = m_listPathNodeDefs.GetCount();
					PathNodeDef& pathNodeDef = m_listPathNodeDefs.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));
					u32 highway = 0;

					removeCommas(lineData);

					sscanf(lineData,"%f %f %f %d %d %d %d %d %f %d %d",	&pathNodeDef.x,
						&pathNodeDef.y,
						&pathNodeDef.z,
						&pathNodeDef.switchedOff,
						&pathNodeDef.waterNode,
						&pathNodeDef.roadBlock,
						&pathNodeDef.speed,
						&pathNodeDef.specialFunction,
						&pathNodeDef.density,
						&pathNodeDef.streetNameHash,
						&highway);

					pathNodeDef.flags = (highway & PathNodeDef::FLAG_HIGHWAY);
				}
			}

			break;

		case STATE_PATHLINKS:

			{
				if(stricmp(lineData,"end") == 0)
					State = STATE_NONE;
				else
				{
					s32 CurrentSize = m_listPathLinkDefs.GetCount();
					PathLinkDef& pathLinkDef = m_listPathLinkDefs.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));

					removeCommas(lineData);

					sscanf(lineData,"%d %d %f %d %d",	&pathLinkDef.node1,
						&pathLinkDef.node2,
						&pathLinkDef.width,
						&pathLinkDef.lanes1To2,
						&pathLinkDef.lanes2To1);
				}
			}

			break;

		case STATE_GARAGE:

			{
				if(stricmp(lineData,"end") == 0)
					State = STATE_NONE;
				else
				{
					s32 CurrentSize = m_listGarageDefs.GetCount();
					GarageDef& garageDef = m_listGarageDefs.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));

					removeCommas(lineData);

					char nameBuff[32];

					sscanf(lineData, "%f %f %f %f %f %f %f %f %d %d %s",	&garageDef.x1,
						&garageDef.y1,
						&garageDef.z1,
						&garageDef.x2,
						&garageDef.y2,
						&garageDef.x3,
						&garageDef.y3,
						&garageDef.ztop,
						&garageDef.flag,
						&garageDef.type,
						nameBuff);

					strncpy(garageDef.name,nameBuff,GARAGE_NAME_LENGTH);
				}
			}

			break;

		case STATE_CARGEN:

			{
				if(stricmp(lineData,"end") == 0)
					State = STATE_NONE;
				else
				{
					s32 CurrentSize = m_listCarGenDefs.GetCount();
					CarGenDef& carGenDef = m_listCarGenDefs.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));

					removeCommas(lineData);

					sscanf(lineData, "%f %f %f %f %f %f %d %d %d %d %d %d %d %d",	&carGenDef.x,
						&carGenDef.y,
						&carGenDef.z,
						&carGenDef.vec1x,
						&carGenDef.vec1y,
						&carGenDef.lengthVec2,
						&carGenDef.modelHashKey,
						&carGenDef.col1,
						&carGenDef.col2,
						&carGenDef.col3,
						&carGenDef.col4,
						&carGenDef.flagVal,
						&carGenDef.alarmChance,
						&carGenDef.lockedChance);
				}
			}

			break;

		case STATE_TIMECYCMOD:

			{
				if(stricmp(lineData,"end") == 0)
					State = STATE_NONE;
				else
				{
					s32 CurrentSize = m_listTimeCycDefs.GetCount();
					TimeCycDef& timeCycDef = m_listTimeCycDefs.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));

					removeCommas(lineData);

					char nameBuff[128];

					sscanf(lineData,"%f %f %f %f %f %f %f %f %d %d %s",	&timeCycDef.minX,
																		&timeCycDef.minY,
																		&timeCycDef.minZ,
																		&timeCycDef.maxX,
																		&timeCycDef.maxY,
																		&timeCycDef.maxZ,
																		&timeCycDef.range,
																		&timeCycDef.percentage,
																		&timeCycDef.startHour,
																		&timeCycDef.endHour,
																		&nameBuff);

					strupr(nameBuff);
					timeCycDef.hashCode = atStringHash(&nameBuff[0]);
				}
			}

			break;

		case STATE_BLOCKS:

			{
				if(stricmp(lineData,"end") == 0)
					State = STATE_NONE;
				else
				{
					s32 CurrentSize = m_listBlockDefs.GetCount();
					BlockDef& blockDef = m_listBlockDefs.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));

					blockDef.version = 1;

					removeCommas(lineData);

					char name[128];
					char owner[128];
					char exported[128];
					char time[128];
					unsigned int numPoints;

					name[0] = '\0';
					owner[0] = '\0';
					exported[0] = '\0';
					time[0] = '\0';

					sscanf(lineData, "%s %s %s %d %d %f %f %f %f %f %f %f %f %s",	name,
																					exported,
																					time,
																					&blockDef.flags,
																					&numPoints,
																					&blockDef.x1, &blockDef.y1,
																					&blockDef.x2, &blockDef.y2,
																					&blockDef.x3, &blockDef.y3,
																					&blockDef.x4, &blockDef.y4,
																					owner);
					if(numPoints == 0)
					{
						sscanf(lineData, "%s %s %s %d %d %s",	name,
							exported,
							time,
							&blockDef.flags,
							&numPoints,
							owner);
					}

					if(owner[0] == '\0')
						strcpy(owner,"none");

					sprintf(blockDef.name_owner_export_time,"%s,%s,%s,%s",name,owner,exported,time);

					if(strlen(blockDef.name_owner_export_time) >= 92)
					{
						Errorf("block %s exceeds 92 character limit",name);
					}
				}
			}

			break;

		case STATE_ROOTSCRIPTS:

			{
				if(stricmp(lineData,"end") == 0)
					State = STATE_NONE;
				else
				{
					s32 CurrentSize = m_listRootScriptDefs.GetCount();
					RootScriptDef& blockDef = m_listRootScriptDefs.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));

					removeCommas(lineData);

					char seperators[] = " ,\t\n";

					blockDef.x = (f32)atof(strtok(lineData,seperators));
					blockDef.y = (f32)atof(strtok(NULL,seperators));
					blockDef.z = (f32)atof(strtok(NULL,seperators));
					blockDef.q_x = (f32)atof(strtok(NULL,seperators));
					blockDef.q_y = (f32)atof(strtok(NULL,seperators));
					blockDef.q_z = (f32)atof(strtok(NULL,seperators));
					blockDef.q_w = (f32)atof(strtok(NULL,seperators));
					strncpy(blockDef.script_name, strtok(NULL,seperators), 24);
					blockDef.number_of_extra_coords = atoi(strtok(NULL,seperators));

					if(blockDef.number_of_extra_coords > 4)
						blockDef.number_of_extra_coords = 4;

					for(s32 i=0;i<blockDef.number_of_extra_coords;i++)
					{
						blockDef.extra_coord[i].x = (f32)atof(strtok(NULL,seperators));
						blockDef.extra_coord[i].y = (f32)atof(strtok(NULL,seperators));
						blockDef.extra_coord[i].z = (f32)atof(strtok(NULL,seperators));
					}

					blockDef.WorkType = (u8) atoi(strtok(NULL,seperators));
					blockDef.Home = (u8) atoi(strtok(NULL,seperators));
					blockDef.LeisureType = (u8) atoi(strtok(NULL,seperators));
					blockDef.FoodType = (u8) atoi(strtok(NULL,seperators));
				}
			}

			break;

		case STATE_MILOPLUS:

			{
				if(stricmp(lineData,"end") == 0)
					State = STATE_NONE;
				else
				{
					s32 CurrentSize = m_listMiloPlusInstanceDefs.GetCount();
					MiloPlusInstanceDef& instanceDef = m_listMiloPlusInstanceDefs.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));
					u32& nameHash = m_listMiloPlusNameHashes.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));
					removeCommas(lineData);

					sscanf(lineData, "%s %d %d %d %f %f %f %f %f %f %f",&instanceDef.name,
																		&instanceDef.flags,
																		&instanceDef.id,
																		&instanceDef.count,
																		&instanceDef.posx,
																		&instanceDef.posy,
																		&instanceDef.posz,
																		&instanceDef.quatx,
																		&instanceDef.quaty,
																		&instanceDef.quatz,
																		&instanceDef.quatw);

					nameHash = atStringHash(&instanceDef.name[0]);
				}
			}

			break;

		case STATE_LODMODIFIERS:

			{
				if(stricmp(lineData,"end") == 0)
					State = STATE_NONE;
				else
				{
					LodModifierDef lodmodDef;

					removeCommas(lineData);

					const char* p_Token;

					lodmodDef.minx = (rage::f32) atof(p_Token = strtok(lineData," "));
					lodmodDef.miny = (rage::f32) atof(p_Token = strtok(NULL," "));
					lodmodDef.minz = (rage::f32) atof(p_Token = strtok(NULL," "));
					lodmodDef.maxx = (rage::f32) atof(p_Token = strtok(NULL," "));
					lodmodDef.maxy = (rage::f32) atof(p_Token = strtok(NULL," "));
					lodmodDef.maxz = (rage::f32) atof(p_Token = strtok(NULL," "));

					s32 Count = (unsigned int)atof(p_Token = strtok(NULL," "));
					s32 CurrentChunk = 0;
					s32 currlods = m_lodmodifiersDefs.GetCount();

					for(s32 curval=0;curval<Count;curval++)
					{
						if(CurrentChunk == MAX_LODPERMODIFIER)
						{
							s32 CurrentSize = m_lodmodifiersDefs.GetCount();
							LodModifierDef& setlodmodDef = m_lodmodifiersDefs.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));
							setlodmodDef = lodmodDef;
							setlodmodDef.numlods = MAX_LODPERMODIFIER;
							CurrentChunk = 0;
						}

						p_Token = strtok(NULL," ");
						lodmodDef.lodid[CurrentChunk] = atStringHash(p_Token);
						memset(lodmodDef.names[CurrentChunk],0,32);
						strncpy(lodmodDef.names[CurrentChunk],p_Token,32);
						CurrentChunk++;
					}

					u32 flags = 0;
					p_Token = strtok(NULL," ");
					if(p_Token)
						flags = (rage::u32) atoi(p_Token);

					s32 newcount = m_lodmodifiersDefs.GetCount();

					for(s32 i=currlods;i<newcount;i++)
						m_lodmodifiersDefs[i].numlods |= (flags << LODMOD_FLAGSHIFT);

					if(CurrentChunk > 0)
					{
						s32 CurrentSize = m_lodmodifiersDefs.GetCount();
						LodModifierDef& setlodmodDef = m_lodmodifiersDefs.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));
						setlodmodDef = lodmodDef;
						setlodmodDef.numlods = CurrentChunk | (flags << LODMOD_FLAGSHIFT);
					}
				}
			}

			break;
		case STATE_SLOWZONES:
			{
				if(stricmp(lineData,"end") == 0)
					State = STATE_NONE;
				else
				{
					removeCommas(lineData);

					const char* p_Token;
					s32 CurrentSize = m_slowzoneDefs.GetCount();
					SlowZoneDef& slowzoneDef = m_slowzoneDefs.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));

					slowzoneDef.minx = (rage::f32) atof(p_Token = strtok(lineData," "));
					slowzoneDef.miny = (rage::f32) atof(p_Token = strtok(NULL," "));
					slowzoneDef.minz = (rage::f32) atof(p_Token = strtok(NULL," "));
					slowzoneDef.maxx = (rage::f32) atof(p_Token = strtok(NULL," "));
					slowzoneDef.maxy = (rage::f32) atof(p_Token = strtok(NULL," "));
					slowzoneDef.maxz = (rage::f32) atof(p_Token = strtok(NULL," "));
				}
			}

			break;

		case STATE_PATROLNODES:
			{
				if (0 == stricmp( lineData, "end" ) )
					State = STATE_NONE;
				else
				{
					removeCommas( lineData );
					s32 CurrentSize = m_listPatrolNodeDefs.GetCount();
					PatrolNodeDef& patrolNodeDef = m_listPatrolNodeDefs.Grow( CurrentSize == 0 ? ARRAY_INITSIZE : ( CurrentSize << 1 ) );

					s32 retcount = sscanf( lineData, "%f %f %f %f %f %f %d %d %d %d",
										&patrolNodeDef.x, &patrolNodeDef.y, &patrolNodeDef.z,
										&patrolNodeDef.fHeadingX, &patrolNodeDef.fHeadingY, &patrolNodeDef.fHeadingZ,
										&patrolNodeDef.nDuration,
										&patrolNodeDef.hNodeTypeHash,
										&patrolNodeDef.hAssociatedBaseHash,
										&patrolNodeDef.hRouteNameHash );
					Verifyf( (10 == retcount),  "Invalid Patrol Node parameters (%d).", retcount );
				}
			}
			break;

		case STATE_PATROLLINKS:
			{
				if (0 == stricmp( lineData, "end" ) )
					State = STATE_NONE;
				else
				{
					removeCommas( lineData );
					s32 CurrentSize = m_listPatrolLinkDefs.GetCount();
					PatrolLinkDef& patrolLinkDef = m_listPatrolLinkDefs.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));
					
					s32 retcount = sscanf( lineData, "%d %d", &patrolLinkDef.node1, &patrolLinkDef.node2 );
					Verifyf( (2 == retcount),  "Invalid Patrol Link node indices (%d).", retcount );
				}
			}
			break;

		case STATE_BOUND:
			{
				if ( 0 == stricmp( lineData, "end" ) )
					State = STATE_NONE;
				else
				{
					Assertf( m_nIplVersion >= IPL6_VERSION, "bound section only supported in IPL6+." );

					removeCommas( lineData );
					s32 CurrentSize = m_listBoundDefs.GetCount();
					BoundDef& boundDef = m_listBoundDefs.Grow( CurrentSize == 0 ? ARRAY_INITSIZE : ( CurrentSize << 1 ) );

					s32 retcount = sscanf( lineData, "%d %f %f %f %f %f %f", 
							&boundDef.index,
							&boundDef.minx, &boundDef.miny, &boundDef.minz,
							&boundDef.maxx, &boundDef.maxy, &boundDef.maxz);
					Verifyf( ( 7 == retcount ), "Invalid bound definition (%d).", retcount );
				}
			}
			break;

		case STATE_INTERIORS:
			{
				if ( 0 == stricmp( lineData, "end" ) )
					State = STATE_NONE;
				else
				{
					Assertf( m_nIplVersion >= IPL6_VERSION, "miloinst section only supported in IPL6+." );

					s32 CurrentSize = -1;
					removeCommas(lineData);

					CurrentSize = m_listMiloInstance6Defs.GetCount();
					MiloInstance6Def& instanceDef = m_listMiloInstance6Defs.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));
					atString& instanceName = m_listMiloInstanceNames.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));
					m_listInstanceIsLod.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1)) = false;

					char name[128];
					instanceDef.lodDistance = -1;

					sscanf(lineData, "%s %d %f %f %f %f %f %f %f %d %d %f %d %d %d %d",
						&name,
						&instanceDef.areaCodeAndFlags,
						&instanceDef.posx,
						&instanceDef.posy,
						&instanceDef.posz,
						&instanceDef.quatx,
						&instanceDef.quaty,
						&instanceDef.quatz,
						&instanceDef.quatw,
						&instanceDef.lodIndex,
						&instanceDef.blockIndex,
						&instanceDef.lodDistance,
						&instanceDef.numAncestors,
						&instanceDef.numChildren,
						&instanceDef.ambOcclMultiplier,
						&instanceDef.groupId);
					

					instanceName = name;
					instanceName += "-";
					instanceName += pFilename;
					strupr(name);
					instanceDef.modelHashKey = atStringHash(&name[0]);

					if((instanceDef.lodIndex != -1)&&(instanceDef.lodIndex < m_listInstanceIsLod.GetCount()))
						m_listInstanceIsLod[instanceDef.lodIndex] = true;
				}
			}
			break;

		case STATE_LODCHILDREN:
			{
				if ( 0 == stricmp( lineData, "end" ) )
					State = STATE_NONE;
				else
				{
					Assertf( m_nIplVersion >= IPL6_VERSION, "lodchildren section only supported in IPL6+." );

					s32 CurrentSize = -1;
					removeCommas(lineData);

					CurrentSize = m_listLodChildDefs.GetCount();
					LodChildDef& lodDef = m_listLodChildDefs.Grow(CurrentSize == 0 ? ARRAY_INITSIZE : (CurrentSize << 1));

					char name[128];
					sscanf(lineData, "%s %d",
						&name,
						&lodDef.index);
					
					lodDef.containerHash = atStringHash( name );
				}
			}
			break;

			// Metadata that ends up in the binary IPL headers.
		case STATE_MAXLODDISTANCE:
			{
				if ( 0 == stricmp( lineData, "end" ) )
					State = STATE_NONE;
				else
				{
					Assertf( m_nIplVersion >= IPL6_VERSION, "maxlod section only supported in IPL6+." );
					removeCommas( lineData );
					int retcount = sscanf( lineData, "%f", &m_fMaxLodDistance );
					// this goes into the Ipl6FileHeader structure.

					Verifyf( 1 == retcount, "Invalid number of flags read in maxlod section." );
				}
			}
			break;
		}

	}

	p_ideFile->Close();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
atString IplSet::getInstanceTransform(s32 index)
{
	atString ret;
	char buffer[1024];

	sprintf(buffer,"%f,%f,%f,%f,%f,%f,%f",	m_listInstanceDefs[index].posx,
											m_listInstanceDefs[index].posy,
											m_listInstanceDefs[index].posz,
											m_listInstanceDefs[index].quatx,
											m_listInstanceDefs[index].quaty,
											m_listInstanceDefs[index].quatz,
											m_listInstanceDefs[index].quatw);

	ret = buffer;

	return ret;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
s32 IplSet::getInstanceBlock(s32 index) 
{ 
	switch (m_nIplVersion)
	{
	case IPL5_VERSION:
		return ( 0 );
	case IPL4_VERSION:
	default:
		return m_listInstanceDefs[index].blockIndex;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void IplSet::loadBinary(const char* pFilename,bool SwapEndian)
{
	char path[MAX_PATH];
	char basename[MAX_PATH];
	char extension[MAX_PATH];

	SplitFilename(pFilename,path,basename,extension);

	strcat(path,basename);
	strcat(path,".bpl");

	fiStream* p_ideFile = fiStream::Open(path);

	IplFileHeader iplFileHeader;

	p_ideFile->Read(&iplFileHeader,sizeof(IplFileHeader));

	m_listInstanceDefs.Reset();
	if(iplFileHeader.NumInstances)
	{
		m_listInstanceDefs.Resize(iplFileHeader.NumInstances);
		p_ideFile->Read(&m_listInstanceDefs[0],sizeof(InstanceDef) * iplFileHeader.NumInstances);
	}

	m_listZoneDefs.Reset();
	if(iplFileHeader.NumZones)
	{
		m_listZoneDefs.Resize(iplFileHeader.NumZones);
		p_ideFile->Read(&m_listZoneDefs[0],sizeof(ZoneDef) * iplFileHeader.NumZones);
	}

	m_listGarageDefs.Reset();
	if(iplFileHeader.NumGarages)
	{
		m_listGarageDefs.Resize(iplFileHeader.NumGarages);
		p_ideFile->Read(&m_listGarageDefs[0],sizeof(GarageDef) * iplFileHeader.NumGarages);
	}

	m_listCarGenDefs.Reset();
	if(iplFileHeader.NumCarGens)
	{
		m_listCarGenDefs.Resize(iplFileHeader.NumCarGens);
		p_ideFile->Read(&m_listCarGenDefs[0],sizeof(CarGenDef) * iplFileHeader.NumCarGens);
	}

	m_listTimeCycDefs.Reset();
	if(iplFileHeader.NumTimeCycs)
	{
		m_listTimeCycDefs.Resize(iplFileHeader.NumTimeCycs);
		p_ideFile->Read(&m_listTimeCycDefs[0],sizeof(TimeCycDef) * iplFileHeader.NumTimeCycs);
	}

	m_listPathNodeDefs.Reset();
	if(iplFileHeader.NumPathNodes)
	{
		m_listPathNodeDefs.Resize(iplFileHeader.NumPathNodes);
		p_ideFile->Read(&m_listPathNodeDefs[0],sizeof(PathNodeDef) * iplFileHeader.NumPathNodes);
	}

	m_listPathLinkDefs.Reset();
	if(iplFileHeader.NumPathLinks)
	{
		m_listPathLinkDefs.Resize(iplFileHeader.NumPathLinks);
		p_ideFile->Read(&m_listPathLinkDefs[0],sizeof(PathLinkDef) * iplFileHeader.NumPathLinks);
	}

	m_listRootScriptDefs.Reset();
	if(iplFileHeader.NumRootScripts)
	{
		m_listRootScriptDefs.Resize(iplFileHeader.NumRootScripts);
		p_ideFile->Read(&m_listRootScriptDefs[0],sizeof(RootScriptDef) * iplFileHeader.NumRootScripts);
	}

	m_listBlockDefs.Reset();
	if(iplFileHeader.NumPathLinks)
	{
		m_listBlockDefs.Resize(iplFileHeader.NumBlocks);
		p_ideFile->Read(&m_listBlockDefs[0],sizeof(BlockDef) * iplFileHeader.NumBlocks);
	}

	m_listMiloPlusInstanceDefs.Reset();
	if(iplFileHeader.NumMiloPlus)
	{
		m_listMiloPlusInstanceDefs.Resize(iplFileHeader.NumMiloPlus);
		p_ideFile->Read(&m_listMiloPlusInstanceDefs[0],sizeof(MiloPlusInstanceDef) * iplFileHeader.NumMiloPlus);
	}

	m_listPatrolNodeDefs.Reset();
	if ( iplFileHeader.NumPatrolNodes )
	{
		m_listPatrolNodeDefs.Resize( iplFileHeader.NumPatrolNodes );
		p_ideFile->Read( &m_listPatrolNodeDefs[0], sizeof( PatrolNodeDef ) * iplFileHeader.NumPatrolNodes );
	}

	m_listPatrolLinkDefs.Reset();
	if ( iplFileHeader.NumPatrolLinks )
	{
		m_listPatrolLinkDefs.Resize( iplFileHeader.NumPatrolLinks );
		p_ideFile->Read( &m_listPatrolLinkDefs[0], sizeof( PatrolLinkDef ) * iplFileHeader.NumPatrolLinks );
	}

	p_ideFile->Close();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void IplSet::writeString(fiStream* p_Stream,atString& r_String)
{
	p_Stream->Write(r_String.c_str(),r_String.GetLength());
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void IplSet::save(const char* pFilename)
{
	GtaData::m_ideSet.m_listObjectHash.FinishInsertion();
	GtaData::m_ideSet.m_listMiloHash.FinishInsertion();

	fiDeviceLocal::GetInstance().Delete(pFilename);
	fiStream* p_ideFile = fiStream::Create(pFilename);

	atString str("inst\n");
	writeString(p_ideFile, str);

	for(int32 i=0;i<m_listInstanceDefs.GetCount();i++)
	{
		char lineData[8192];
		char buffer[1024];

		if(GtaData::m_ideSet.isObjectAMilo(m_listInstanceDefs[i].modelHashKey))
		{
			Matrix34 matMilo;

			matMilo.Identity();
			matMilo.FromQuaternion(Quaternion(m_listInstanceDefs[i].quatx,m_listInstanceDefs[i].quaty,m_listInstanceDefs[i].quatz,m_listInstanceDefs[i].quatw));
			matMilo.Translate(m_listInstanceDefs[i].posx,m_listInstanceDefs[i].posy,m_listInstanceDefs[i].posz);

			u32* p_Index = GtaData::m_ideSet.m_listMiloHash.SafeGet(m_listInstanceDefs[i].modelHashKey);

			if(p_Index)
			{
				atArray<MiloItemDef>& r_MiloItems = GtaData::m_ideSet.m_listMiloDefs[*p_Index].m_objects;

				u32 Count = r_MiloItems.GetCount();

				for(u32 j=0;j<Count;j++)
				{
					u32* p_IndexObject = GtaData::m_ideSet.m_listObjectHash.SafeGet(r_MiloItems[j].objectHashKey);

					Matrix34 matCombine;
					Matrix34 matLocal;

					matLocal.Identity();
					matLocal.FromQuaternion(Quaternion(r_MiloItems[j].quatx,r_MiloItems[j].quaty,r_MiloItems[j].quatz,r_MiloItems[j].quatw));
					matLocal.Translate(r_MiloItems[j].posx,r_MiloItems[j].posy,r_MiloItems[j].posz);

					matCombine.Dot(matLocal,matMilo);

					if(p_IndexObject)
					{
						strcpy(buffer,"");
						strcat(buffer,"milo-");
						strcat(buffer,m_listInstanceNames[i]);
						strcat(buffer,"-");
						strcat(buffer,GtaData::m_ideSet.m_listObjectNames[*p_IndexObject]);

//						char* p_Found = strchr(buffer,'-');
//						if(p_Found)
//							*p_Found = '\0';

						Quaternion q;
						matCombine.ToQuaternion(q);
						Vector3 pos = matCombine.GetVector(3);

						sprintf(lineData, "%s, 0, %f, %f, %f, %f, %f, %f, %f, -1, -1, -1, -1.000000\n",buffer,pos.x,pos.y,pos.z,q.x,q.y,q.z,q.w);
						atString lineDataStr(lineData);
						writeString(p_ideFile, lineDataStr);
					}
				}

				atString lineDataStr(lineData);
				writeString(p_ideFile,lineDataStr);
			}

			continue;
		}

		strcpy(buffer,m_listInstanceNames[i]);
//		char* p_Found = strchr(buffer,'-');
//		if(p_Found)
//			*p_Found = '\0';

		sprintf(lineData, "%s, %d, %f, %f, %f, %f, %f, %f, %f, %d, %d, %f\n",	buffer,
																				m_listInstanceDefs[i].areaCodeAndFlags,
																				m_listInstanceDefs[i].posx,
																				m_listInstanceDefs[i].posy,
																				m_listInstanceDefs[i].posz,
																				m_listInstanceDefs[i].quatx,
																				m_listInstanceDefs[i].quaty,
																				m_listInstanceDefs[i].quatz,
																				m_listInstanceDefs[i].quatw,
																				m_listInstanceDefs[i].lodIndex,
																				m_listInstanceDefs[i].blockIndex,
																				m_listInstanceDefs[i].lodDistance);

		atString lineDataStr(lineData);
		writeString(p_ideFile,lineDataStr);
	}

	atString endStr("end\n");
	writeString(p_ideFile, endStr);

	p_ideFile->Close();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief Wrapper around the saveBinary4 and saveBinary5 methods.
 *
 * This method saves the correct binary format based on the version of the 
 * previously loaded ASCII IPL file.  Defaulting to IPL4_VERSION.
 */
void IplSet::saveBinary(const char* pFilename, bool SwapEndian)
{
	switch (this->m_nIplVersion)
	{
	case IPL6_VERSION:
		saveBinary6(pFilename, SwapEndian);
		break;
	case IPL5_VERSION:
		saveBinary5(pFilename, SwapEndian);
		break;
	case IPL4_VERSION:
	default:
		saveBinary4(pFilename, SwapEndian);
		break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void IplSet::saveBinary4(const char* pFilename,bool SwapEndian)
{
	fiDeviceLocal::GetInstance().Delete(pFilename);
	fiStream* p_ideFile = fiStream::Create(pFilename);

	IplFileHeader iplFileHeader;
	IplFileHeader iplWriteFileHeader;

	iplFileHeader.Version = IPL4_VERSION;
	iplFileHeader.NumInstances = m_listInstanceDefs.GetCount();
	iplFileHeader.NumZones = m_listZoneDefs.GetCount();
	iplFileHeader.NumGarages = m_listGarageDefs.GetCount();
	iplFileHeader.NumCarGens = m_listCarGenDefs.GetCount();
	iplFileHeader.NumTimeCycs = m_listTimeCycDefs.GetCount();
	iplFileHeader.NumPathNodes = m_listPathNodeDefs.GetCount();
	iplFileHeader.NumPathLinks = m_listPathLinkDefs.GetCount();
	iplFileHeader.NumRootScripts = m_listRootScriptDefs.GetCount();
	iplFileHeader.NumMiloPlus = m_listMiloPlusInstanceDefs.GetCount();
	iplFileHeader.NumLodModifiers = m_lodmodifiersDefs.GetCount();
	iplFileHeader.NumSlowZones = m_slowzoneDefs.GetCount();
	iplFileHeader.NumExtra12 = 0;
	iplFileHeader.NumExtra13 = 0;
	iplFileHeader.NumExtra14 = 0;
	iplFileHeader.NumExtra15 = 0;
	iplFileHeader.NumBlocks = m_listBlockDefs.GetCount();
	iplFileHeader.NumPatrolNodes = m_listPatrolNodeDefs.GetCount();
	iplFileHeader.NumPatrolLinks = m_listPatrolLinkDefs.GetCount();

	iplWriteFileHeader = iplFileHeader;

	//swap endianness for platforms like the 360

	if(SwapEndian)
	{
		s32 i;

		for(i=0;i<iplFileHeader.NumInstances;i++)
			m_listInstanceDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumZones;i++)
			m_listZoneDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumGarages;i++)
			m_listGarageDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumCarGens;i++)
			m_listCarGenDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumTimeCycs;i++)
			m_listTimeCycDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumPathNodes;i++)
			m_listPathNodeDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumPathLinks;i++)
			m_listPathLinkDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumBlocks;i++)
			m_listBlockDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumRootScripts;i++)
			m_listRootScriptDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumMiloPlus;i++)
			m_listMiloPlusInstanceDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumLodModifiers;i++)
			m_lodmodifiersDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumSlowZones;i++)
			m_slowzoneDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumPatrolNodes;i++)
			m_listPatrolNodeDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumPatrolLinks;i++)
			m_listPatrolLinkDefs[i].swawEndian();

		iplWriteFileHeader.swawEndian();
	}

	//write it out

	p_ideFile->Write(&iplWriteFileHeader,sizeof(IplFileHeader));

	if(iplFileHeader.NumInstances)
		p_ideFile->Write(&m_listInstanceDefs[0],sizeof(InstanceDef) * iplFileHeader.NumInstances);
	if(iplFileHeader.NumZones)
		p_ideFile->Write(&m_listZoneDefs[0],sizeof(ZoneDef) * iplFileHeader.NumZones);
	if(iplFileHeader.NumGarages)
		p_ideFile->Write(&m_listGarageDefs[0],sizeof(GarageDef) * iplFileHeader.NumGarages);
	if(iplFileHeader.NumCarGens)
		p_ideFile->Write(&m_listCarGenDefs[0],sizeof(CarGenDef) * iplFileHeader.NumCarGens);
	if(iplFileHeader.NumTimeCycs)
		p_ideFile->Write(&m_listTimeCycDefs[0],sizeof(TimeCycDef) * iplFileHeader.NumTimeCycs);
	if(iplFileHeader.NumPathNodes)
		p_ideFile->Write(&m_listPathNodeDefs[0],sizeof(PathNodeDef) * iplFileHeader.NumPathNodes);
	if(iplFileHeader.NumPathLinks)
		p_ideFile->Write(&m_listPathLinkDefs[0],sizeof(PathLinkDef) * iplFileHeader.NumPathLinks);
	if(iplFileHeader.NumRootScripts)
		p_ideFile->Write(&m_listRootScriptDefs[0],sizeof(RootScriptDef) * iplFileHeader.NumRootScripts);
	if(iplFileHeader.NumBlocks)
		p_ideFile->Write(&m_listBlockDefs[0],sizeof(BlockDef) * iplFileHeader.NumBlocks);
	if(iplFileHeader.NumMiloPlus)
		p_ideFile->Write(&m_listMiloPlusInstanceDefs[0],sizeof(MiloPlusInstanceDef) * iplFileHeader.NumMiloPlus);
	if(iplFileHeader.NumLodModifiers)
		p_ideFile->Write(&m_lodmodifiersDefs[0],sizeof(LodModifierDef) * iplFileHeader.NumLodModifiers);
	if(iplFileHeader.NumSlowZones)
		p_ideFile->Write(&m_slowzoneDefs[0],sizeof(SlowZoneDef) * iplFileHeader.NumSlowZones);
	if(iplFileHeader.NumPatrolNodes)
		p_ideFile->Write(&m_listPatrolNodeDefs[0],sizeof(PatrolNodeDef) * iplFileHeader.NumPatrolNodes);
	if(iplFileHeader.NumPatrolLinks)
		p_ideFile->Write(&m_listPatrolLinkDefs[0],sizeof(PatrolLinkDef) * iplFileHeader.NumPatrolLinks);

	p_ideFile->Close();

	//swap endianness back if we need to

	if(SwapEndian)
	{
		s32 i;

		for(i=0;i<iplFileHeader.NumInstances;i++)
			m_listInstanceDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumZones;i++)
			m_listZoneDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumGarages;i++)
			m_listGarageDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumCarGens;i++)
			m_listCarGenDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumTimeCycs;i++)
			m_listTimeCycDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumPathNodes;i++)
			m_listPathNodeDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumPathLinks;i++)
			m_listPathLinkDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumBlocks;i++)
			m_listBlockDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumRootScripts;i++)
			m_listRootScriptDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumMiloPlus;i++)
			m_listMiloPlusInstanceDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumLodModifiers;i++)
			m_lodmodifiersDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumSlowZones;i++)
			m_slowzoneDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumPatrolNodes;i++)
			m_listPatrolNodeDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumPatrolLinks;i++)
			m_listPatrolLinkDefs[i].swawEndian();
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void IplSet::saveBinary5(const char* pFilename, bool SwapEndian)
{
	fiDeviceLocal::GetInstance().Delete(pFilename);
	fiStream* p_ideFile = fiStream::Create(pFilename);

	Ipl5FileHeader iplFileHeader;
	Ipl5FileHeader iplWriteFileHeader;
	memset( &iplFileHeader, 0, sizeof( Ipl5FileHeader ) );

	iplFileHeader.Version = IPL5_VERSION;
	iplFileHeader.NumInstances = m_listInstance5Defs.GetCount();
	iplFileHeader.NumZones = m_listZoneDefs.GetCount();
	iplFileHeader.NumGarages = m_listGarageDefs.GetCount();
	iplFileHeader.NumCarGens = m_listCarGenDefs.GetCount();
	iplFileHeader.NumTimeCycs = m_listTimeCycDefs.GetCount();
	iplFileHeader.NumPathNodes = m_listPathNodeDefs.GetCount();
	iplFileHeader.NumPathLinks = m_listPathLinkDefs.GetCount();
	iplFileHeader.NumRootScripts = m_listRootScriptDefs.GetCount();
	iplFileHeader.NumMiloPlus = m_listMiloPlusInstanceDefs.GetCount();
	iplFileHeader.NumLodModifiers = m_lodmodifiersDefs.GetCount();
	iplFileHeader.NumSlowZones = m_slowzoneDefs.GetCount();
	iplFileHeader.NumBlocks = m_listBlockDefs.GetCount();
	iplFileHeader.NumPatrolNodes = m_listPatrolNodeDefs.GetCount();
	iplFileHeader.NumPatrolLinks = m_listPatrolLinkDefs.GetCount();

	iplWriteFileHeader = iplFileHeader;

	// Swap endianness for platforms like the 360
	if(SwapEndian)
	{
		s32 i;

		for(i=0;i<iplFileHeader.NumInstances;i++)
			m_listInstance5Defs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumZones;i++)
			m_listZoneDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumGarages;i++)
			m_listGarageDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumCarGens;i++)
			m_listCarGenDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumTimeCycs;i++)
			m_listTimeCycDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumPathNodes;i++)
			m_listPathNodeDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumPathLinks;i++)
			m_listPathLinkDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumBlocks;i++)
			m_listBlockDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumRootScripts;i++)
			m_listRootScriptDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumMiloPlus;i++)
			m_listMiloPlusInstanceDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumLodModifiers;i++)
			m_lodmodifiersDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumSlowZones;i++)
			m_slowzoneDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumPatrolNodes;i++)
			m_listPatrolNodeDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumPatrolLinks;i++)
			m_listPatrolLinkDefs[i].swawEndian();

		iplWriteFileHeader.swawEndian();
	}

	//write it out

	p_ideFile->Write(&iplWriteFileHeader, sizeof(Ipl5FileHeader));

	if(iplFileHeader.NumInstances)
		p_ideFile->Write(&m_listInstance5Defs[0],sizeof(Instance5Def) * iplFileHeader.NumInstances);
	if(iplFileHeader.NumZones)
		p_ideFile->Write(&m_listZoneDefs[0],sizeof(ZoneDef) * iplFileHeader.NumZones);
	if(iplFileHeader.NumGarages)
		p_ideFile->Write(&m_listGarageDefs[0],sizeof(GarageDef) * iplFileHeader.NumGarages);
	if(iplFileHeader.NumCarGens)
		p_ideFile->Write(&m_listCarGenDefs[0],sizeof(CarGenDef) * iplFileHeader.NumCarGens);
	if(iplFileHeader.NumTimeCycs)
		p_ideFile->Write(&m_listTimeCycDefs[0],sizeof(TimeCycDef) * iplFileHeader.NumTimeCycs);
	if(iplFileHeader.NumPathNodes)
		p_ideFile->Write(&m_listPathNodeDefs[0],sizeof(PathNodeDef) * iplFileHeader.NumPathNodes);
	if(iplFileHeader.NumPathLinks)
		p_ideFile->Write(&m_listPathLinkDefs[0],sizeof(PathLinkDef) * iplFileHeader.NumPathLinks);
	if(iplFileHeader.NumRootScripts)
		p_ideFile->Write(&m_listRootScriptDefs[0],sizeof(RootScriptDef) * iplFileHeader.NumRootScripts);
	if(iplFileHeader.NumBlocks)
		p_ideFile->Write(&m_listBlockDefs[0],sizeof(BlockDef) * iplFileHeader.NumBlocks);
	if(iplFileHeader.NumMiloPlus)
		p_ideFile->Write(&m_listMiloPlusInstanceDefs[0],sizeof(MiloPlusInstanceDef) * iplFileHeader.NumMiloPlus);
	if(iplFileHeader.NumLodModifiers)
		p_ideFile->Write(&m_lodmodifiersDefs[0],sizeof(LodModifierDef) * iplFileHeader.NumLodModifiers);
	if(iplFileHeader.NumSlowZones)
		p_ideFile->Write(&m_slowzoneDefs[0],sizeof(SlowZoneDef) * iplFileHeader.NumSlowZones);
	if(iplFileHeader.NumPatrolNodes)
		p_ideFile->Write(&m_listPatrolNodeDefs[0],sizeof(PatrolNodeDef) * iplFileHeader.NumPatrolNodes);
	if(iplFileHeader.NumPatrolLinks)
		p_ideFile->Write(&m_listPatrolLinkDefs[0],sizeof(PatrolLinkDef) * iplFileHeader.NumPatrolLinks);

	p_ideFile->Close();

	//swap endianness back if we need to

	if(SwapEndian)
	{
		s32 i;

		for(i=0;i<iplFileHeader.NumInstances;i++)
			m_listInstance5Defs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumZones;i++)
			m_listZoneDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumGarages;i++)
			m_listGarageDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumCarGens;i++)
			m_listCarGenDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumTimeCycs;i++)
			m_listTimeCycDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumPathNodes;i++)
			m_listPathNodeDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumPathLinks;i++)
			m_listPathLinkDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumBlocks;i++)
			m_listBlockDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumRootScripts;i++)
			m_listRootScriptDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumMiloPlus;i++)
			m_listMiloPlusInstanceDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumLodModifiers;i++)
			m_lodmodifiersDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumSlowZones;i++)
			m_slowzoneDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumPatrolNodes;i++)
			m_listPatrolNodeDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumPatrolLinks;i++)
			m_listPatrolLinkDefs[i].swawEndian();
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void IplSet::saveBinary6( const char* pFilename, bool SwapEndian )
{
	fiDeviceLocal::GetInstance().Delete(pFilename);
	fiStream* p_ideFile = fiStream::Create(pFilename);

	Ipl6FileHeader iplFileHeader;
	Ipl6FileHeader iplWriteFileHeader;

	// Verify our bounds does not contain multiple entries for the same type.
	for ( int i = 0; i < m_listBoundDefs.GetCount(); ++i )
	{
		for ( int j = 0; j < m_listBoundDefs.GetCount(); ++j )
		{
			if ( i == j )
				continue;

			BoundDef& ibnd = m_listBoundDefs[i];
			BoundDef& jbnd = m_listBoundDefs[j];
			
			if ( ibnd.index == jbnd.index )
				Errorf( "IPL bound data contains multiple entries for the same bound type (%d)!", ibnd.index );
		}
	}

	iplFileHeader.Version = IPL6_VERSION;
	iplFileHeader.NumBounds = m_listBoundDefs.GetCount();
	iplFileHeader.NumInstances = m_listInstance5Defs.GetCount();
	iplFileHeader.NumMiloInstances = m_listMiloInstance6Defs.GetCount();
	iplFileHeader.NumZones = m_listZoneDefs.GetCount();
	iplFileHeader.NumGarages = m_listGarageDefs.GetCount();
	iplFileHeader.NumCarGens = m_listCarGenDefs.GetCount();
	iplFileHeader.NumTimeCycs = m_listTimeCycDefs.GetCount();
	iplFileHeader.NumPathNodes = m_listPathNodeDefs.GetCount();
	iplFileHeader.NumPathLinks = m_listPathLinkDefs.GetCount();
	iplFileHeader.NumRootScripts = m_listRootScriptDefs.GetCount();
	iplFileHeader.NumMiloPlus = m_listMiloPlusInstanceDefs.GetCount();
	iplFileHeader.NumSlowZones = m_slowzoneDefs.GetCount();
	iplFileHeader.NumBlocks = m_listBlockDefs.GetCount();
	iplFileHeader.NumPatrolNodes = m_listPatrolNodeDefs.GetCount();
	iplFileHeader.NumPatrolLinks = m_listPatrolLinkDefs.GetCount();
	iplFileHeader.NumLodChildren = m_listLodChildDefs.GetCount();
	iplFileHeader.NumLightInsts_DEPRECATED = 0;
	memset( &iplFileHeader.reserved, 0, sizeof( u8 ) * IPL6_HEADER_RESERVED );
	iplFileHeader.MaxLodDistance = m_fMaxLodDistance;

	iplWriteFileHeader = iplFileHeader;

	// Swap endianness for platforms like the 360
	if(SwapEndian)
	{
		s32 i;

		for(i=0;i<iplFileHeader.NumBounds;i++)
			m_listBoundDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumInstances;i++)
			m_listInstance5Defs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumMiloInstances;i++)
			m_listMiloInstance6Defs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumZones;i++)
			m_listZoneDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumGarages;i++)
			m_listGarageDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumCarGens;i++)
			m_listCarGenDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumTimeCycs;i++)
			m_listTimeCycDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumPathNodes;i++)
			m_listPathNodeDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumPathLinks;i++)
			m_listPathLinkDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumBlocks;i++)
			m_listBlockDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumRootScripts;i++)
			m_listRootScriptDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumMiloPlus;i++)
			m_listMiloPlusInstanceDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumSlowZones;i++)
			m_slowzoneDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumPatrolNodes;i++)
			m_listPatrolNodeDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumPatrolLinks;i++)
			m_listPatrolLinkDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumLodChildren;i++)
			m_listLodChildDefs[i].swawEndian();

		iplWriteFileHeader.swawEndian();
	}

	//write it out

	p_ideFile->Write(&iplWriteFileHeader, sizeof(Ipl6FileHeader));

	if (iplFileHeader.NumBounds)
		p_ideFile->Write(&m_listBoundDefs[0],sizeof(BoundDef) * iplFileHeader.NumBounds);
	if(iplFileHeader.NumInstances)
		p_ideFile->Write(&m_listInstance5Defs[0],sizeof(Instance5Def) * iplFileHeader.NumInstances);
	if(iplFileHeader.NumMiloInstances)
		p_ideFile->Write(&m_listMiloInstance6Defs[0],sizeof(MiloInstance6Def) * iplFileHeader.NumMiloInstances);
	if(iplFileHeader.NumZones)
		p_ideFile->Write(&m_listZoneDefs[0],sizeof(ZoneDef) * iplFileHeader.NumZones);
	if(iplFileHeader.NumGarages)
		p_ideFile->Write(&m_listGarageDefs[0],sizeof(GarageDef) * iplFileHeader.NumGarages);
	if(iplFileHeader.NumCarGens)
		p_ideFile->Write(&m_listCarGenDefs[0],sizeof(CarGenDef) * iplFileHeader.NumCarGens);
	if(iplFileHeader.NumTimeCycs)
		p_ideFile->Write(&m_listTimeCycDefs[0],sizeof(TimeCycDef) * iplFileHeader.NumTimeCycs);
	if(iplFileHeader.NumPathNodes)
		p_ideFile->Write(&m_listPathNodeDefs[0],sizeof(PathNodeDef) * iplFileHeader.NumPathNodes);
	if(iplFileHeader.NumPathLinks)
		p_ideFile->Write(&m_listPathLinkDefs[0],sizeof(PathLinkDef) * iplFileHeader.NumPathLinks);
	if(iplFileHeader.NumRootScripts)
		p_ideFile->Write(&m_listRootScriptDefs[0],sizeof(RootScriptDef) * iplFileHeader.NumRootScripts);
	if(iplFileHeader.NumBlocks)
		p_ideFile->Write(&m_listBlockDefs[0],sizeof(BlockDef) * iplFileHeader.NumBlocks);
	if(iplFileHeader.NumMiloPlus)
		p_ideFile->Write(&m_listMiloPlusInstanceDefs[0],sizeof(MiloPlusInstanceDef) * iplFileHeader.NumMiloPlus);
	if(iplFileHeader.NumSlowZones)
		p_ideFile->Write(&m_slowzoneDefs[0],sizeof(SlowZoneDef) * iplFileHeader.NumSlowZones);
	if(iplFileHeader.NumPatrolNodes)
		p_ideFile->Write(&m_listPatrolNodeDefs[0],sizeof(PatrolNodeDef) * iplFileHeader.NumPatrolNodes);
	if(iplFileHeader.NumPatrolLinks)
		p_ideFile->Write(&m_listPatrolLinkDefs[0],sizeof(PatrolLinkDef) * iplFileHeader.NumPatrolLinks);
	if (iplFileHeader.NumLodChildren)
		p_ideFile->Write( &m_listLodChildDefs[0], sizeof(LodChildDef)* iplFileHeader.NumLodChildren );

	p_ideFile->Close();

	//swap endianness back if we need to

	if(SwapEndian)
	{
		s32 i;

		for(i=0;i<iplFileHeader.NumBounds;i++)
			m_listBoundDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumInstances;i++)
			m_listInstance5Defs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumMiloInstances;i++)
			m_listMiloInstance6Defs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumZones;i++)
			m_listZoneDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumGarages;i++)
			m_listGarageDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumCarGens;i++)
			m_listCarGenDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumTimeCycs;i++)
			m_listTimeCycDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumPathNodes;i++)
			m_listPathNodeDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumPathLinks;i++)
			m_listPathLinkDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumBlocks;i++)
			m_listBlockDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumRootScripts;i++)
			m_listRootScriptDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumMiloPlus;i++)
			m_listMiloPlusInstanceDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumSlowZones;i++)
			m_slowzoneDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumPatrolNodes;i++)
			m_listPatrolNodeDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumPatrolLinks;i++)
			m_listPatrolLinkDefs[i].swawEndian();
		for(i=0;i<iplFileHeader.NumLodChildren;i++)
			m_listLodChildDefs[i].swawEndian();
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void IplSet::checkAllInstances(IdeSet& r_ideSet,atArray<atString>& r_Missing)
{
	int32 i,Count = m_listInstanceDefs.GetCount();

	for(i=0;i<Count;i++)
	{
		if(!r_ideSet.doesObjectExist(m_listInstanceDefs[i].modelHashKey))
		{
			r_Missing.PushAndGrow(m_listInstanceNames[i]);
		}
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void IplSet::getUnusedObjects(IdeSet& r_ideSet,atArray<atString>& r_Unused)
{
	int32 i,Count = r_ideSet.m_listObjectDefs.GetCount();

	for(i=0;i<Count;i++)
	{
		if(!isObjectUsed(r_ideSet.m_listObjectDefs[i].modelHashKey))
		{
			if(!r_ideSet.isObjectInMilo(r_ideSet.m_listObjectDefs[i].modelHashKey))
			{
				r_Unused.PushAndGrow(r_ideSet.m_listObjectNames[i]);
			}
		}
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool IplSet::isObjectUsed(u32 objectHash)
{
	int32 i,Count = m_listInstanceDefs.GetCount();

	for(i=0;i<Count;i++)
	{
		if(m_listInstanceDefs[i].modelHashKey == objectHash)
			return true;
	}

	Count = m_listMiloPlusInstanceDefs.GetCount();

	for(i=0;i<Count;i++)
	{
		if(m_listMiloPlusNameHashes[i] == objectHash)
			return true;
	}

	return false;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void GetMapName(const atString& r_InstanceName,atString& r_Out)
{
	char buffer[1024];

	char* p_index = (char*)strrchr(r_InstanceName,'/');

	if(p_index)
	{
		p_index++;
		strcpy(buffer,p_index);

		p_index = strstr(buffer,"_stream");

		if(p_index)
			*p_index = '\0';
		else
		{
			p_index = strstr(buffer,".ipl");

			if(p_index)
				*p_index = '\0';
		}

		r_Out = buffer;
	}
	else
	{
		r_Out = r_InstanceName;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void IplSet::checkUsageObject(u32 hashKey,const char* p_Name,IdeSet& r_ideSet,atArray<atString>& r_Usage,const atString& r_MapCheck)
{
	s32 NumUses = 0;
	bool MultipleMaps = false;
	atString FirstMapName("");

	if((r_MapCheck != "all") && (strstr(p_Name,r_MapCheck) == NULL))
		return;

	s32 j,Count2 = m_listInstanceDefs.GetCount();

	for(j=0;j<Count2;j++)
	{
		if(hashKey == m_listInstanceDefs[j].modelHashKey)
		{
			atString MapName;
			GetMapName(m_listInstanceNames[j],MapName);

			if(NumUses == 0)
			{
				FirstMapName = MapName;
			}
			else
			{
				if(FirstMapName != MapName)
					MultipleMaps = true;
			}

			NumUses++;
		}
	}

	Count2 = r_ideSet.m_listMiloDefs.GetCount();

	for(j=0;j<Count2;j++)
	{
		s32 k,ObjCount = r_ideSet.m_listMiloDefs[j].m_objects.GetCount();

		for(k=0;k<ObjCount;k++)
		{
			if(hashKey == r_ideSet.m_listMiloDefs[j].m_objects[k].objectHashKey)
			{
				atString MapName("milo");

				if(NumUses == 0)
					FirstMapName = MapName;
				else
				{
					if(FirstMapName != MapName)
						MultipleMaps = true;
				}

				NumUses++;
			}
		}
	}

	char buffer[256];
	sprintf(buffer,"-%d-",NumUses);

	if(MultipleMaps)
		strcat(buffer,"multiple");
	else
		strcat(buffer,FirstMapName);

	atString out;
	out += p_Name;
	out += buffer;

	r_Usage.PushAndGrow(out);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void IplSet::checkUsage(IdeSet& r_ideSet,atArray<atString>& r_Usage,const atString& r_MapCheck)
{
	s32 i,Count = r_ideSet.m_listObjectDefs.GetCount();
	s32 TreeCount = r_ideSet.m_listTreeDefs.GetCount();

	Progress::ProgressSizeCallback(Count + TreeCount);

	for(i=0;i<Count;i++)
	{
		Progress::ProgressPosCallback(i);

		checkUsageObject(	r_ideSet.m_listObjectDefs[i].modelHashKey,
							r_ideSet.m_listObjectNames[i].c_str(),
							r_ideSet,
							r_Usage,
							r_MapCheck);
	}

	for(i=0;i<TreeCount;i++)
	{
		Progress::ProgressPosCallback(Count+i);

		checkUsageObject(	r_ideSet.m_listTreeDefs[i].modelHashKey,
							r_ideSet.m_listTreeNames[i].c_str(),
							r_ideSet,
							r_Usage,
							r_MapCheck);
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void IplSet::getBlockName(s32 index,atString& r_blockname)
{
	char buffer[128];

	strcpy(buffer,m_listBlockDefs[index].name_owner_export_time);

	char* p_found = strchr(buffer,',');
	if(p_found)
		*p_found = '\0';

	r_blockname = buffer;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void IplSet::getBlockPoints(s32 index,atString& r_blockpoints)
{
	char buffer[128];

	sprintf(buffer,"4,%f,%f,%f,%f,%f,%f,%f,%f",	m_listBlockDefs[index].x1,
												m_listBlockDefs[index].y1,
												m_listBlockDefs[index].x2,
												m_listBlockDefs[index].y2,
												m_listBlockDefs[index].x3,
												m_listBlockDefs[index].y3,
												m_listBlockDefs[index].x4,
												m_listBlockDefs[index].y4);

	r_blockpoints = buffer;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
f32 IplSet::getBlockArea(s32 index)
{
	Vector2 a(m_listBlockDefs[index].x1,m_listBlockDefs[index].y1);
	Vector2 b(m_listBlockDefs[index].x2,m_listBlockDefs[index].y2);
	Vector2 c(m_listBlockDefs[index].x3,m_listBlockDefs[index].y3);

	Vector2 sideA = b - c;
	Vector2 sideB = b - a;

	return (sideA.Mag() * sideB.Mag());
}

using namespace sysEndian;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void IplFileHeader::swawEndian()
{
	SwapMe(Version);
	SwapMe(NumInstances);
	SwapMe(NumZones);
	SwapMe(NumGarages);
	SwapMe(NumCarGens);
	SwapMe(NumTimeCycs);
	SwapMe(NumPathNodes);
	SwapMe(NumPathLinks);
	SwapMe(NumRootScripts);
	SwapMe(NumMiloPlus);
	SwapMe(NumLodModifiers);
	SwapMe(NumSlowZones);
	SwapMe(NumExtra12);
	SwapMe(NumExtra13);
	SwapMe(NumExtra14);
	SwapMe(NumExtra15);
	SwapMe(NumBlocks);
	SwapMe(NumPatrolNodes);
	SwapMe(NumPatrolLinks);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Ipl5FileHeader::swawEndian()
{
	SwapMe(Version);
	SwapMe(NumInstances);
	SwapMe(NumZones);
	SwapMe(NumGarages);
	SwapMe(NumCarGens);
	SwapMe(NumTimeCycs);
	SwapMe(NumPathNodes);
	SwapMe(NumPathLinks);
	SwapMe(NumRootScripts);
	SwapMe(NumMiloPlus);
	SwapMe(NumLodModifiers);
	SwapMe(NumSlowZones);
	SwapMe(NumExtra12);
	SwapMe(NumExtra13);
	SwapMe(NumExtra14);
	SwapMe(NumExtra15);
	SwapMe(NumBlocks);
	SwapMe(NumPatrolNodes);
	SwapMe(NumPatrolLinks);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Ipl6FileHeader::swawEndian()
{
	SwapMe(Version);
	SwapMe(NumBounds);
	SwapMe(NumInstances);
	SwapMe(NumMiloInstances);
	SwapMe(NumZones);
	SwapMe(NumGarages);
	SwapMe(NumCarGens);
	SwapMe(NumTimeCycs);
	SwapMe(NumPathNodes);
	SwapMe(NumPathLinks);
	SwapMe(NumRootScripts);
	SwapMe(NumMiloPlus);
	SwapMe(NumSlowZones);
	SwapMe(NumBlocks);
	SwapMe(NumPatrolNodes);
	SwapMe(NumPatrolLinks);
	SwapMe(NumLodChildren);
	SwapMe(MaxLodDistance);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void BoundDef::swawEndian()
{
	SwapMe(index);
	SwapMe(minx);
	SwapMe(miny);
	SwapMe(minz);
	SwapMe(maxx);
	SwapMe(maxy);
	SwapMe(maxz);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void InstanceDef::swawEndian()
{
	SwapMe(posx);
	SwapMe(posy);
	SwapMe(posz);
	SwapMe(quatx);
	SwapMe(quaty);
	SwapMe(quatz);
	SwapMe(quatw);
	SwapMe(lodDistance);
	SwapMe(modelHashKey);
	SwapMe(areaCodeAndFlags);
	SwapMe(lodIndex);
	SwapMe(blockIndex);
}

void Instance5Def::swawEndian()
{
	SwapMe(posx);
	SwapMe(posy);
	SwapMe(posz);
	SwapMe(quatx);
	SwapMe(quaty);
	SwapMe(quatz);
	SwapMe(quatw);
	SwapMe(lodDistance);
	SwapMe(modelHashKey);
	SwapMe(areaCodeAndFlags);
	SwapMe(lodIndex);
	SwapMe((u32&)flagsExtra);
	SwapMe(typeOrNumAncestors);
	SwapMe(numChildren);
	SwapMe(ambOcclMultiplier);
	SwapMe(scaleXY);
	SwapMe(scaleZ);

}

void MiloInstance6Def::swawEndian()
{
	SwapMe(posx);
	SwapMe(posy);
	SwapMe(posz);
	SwapMe(quatx);
	SwapMe(quaty);
	SwapMe(quatz);
	SwapMe(quatw);
	SwapMe(lodDistance);
	SwapMe(modelHashKey);
	SwapMe(areaCodeAndFlags);
	SwapMe(lodIndex);
	SwapMe(blockIndex);
	SwapMe(numAncestors);
	SwapMe(numChildren);
	SwapMe(ambOcclMultiplier);
	SwapMe(groupId);
	SwapMe(reserved1);
	SwapMe(reserved2);
	SwapMe(reserved3);
	SwapMe(reserved4);
}

void ZoneDef::swawEndian()
{
	SwapMe(minx);
	SwapMe(miny);
	SwapMe(minz);
	SwapMe(maxx);
	SwapMe(maxy);
	SwapMe(maxz);
	SwapMe(type);
	SwapMe(level);
}

void GarageDef::swawEndian()
{
	SwapMe(x1);
	SwapMe(y1);
	SwapMe(z1);
	SwapMe(x2);
	SwapMe(y2);
	SwapMe(x3);
	SwapMe(y3);
	SwapMe(ztop);
	SwapMe(flag);
	SwapMe(type);
}

void CarGenDef::swawEndian()
{
	SwapMe(x);
	SwapMe(y);
	SwapMe(z);
	SwapMe(vec1x);
	SwapMe(vec1y);
	SwapMe(lengthVec2);
	SwapMe(modelHashKey);
	SwapMe(col1);
	SwapMe(col2);
	SwapMe(col3);
	SwapMe(col4);
	SwapMe(flagVal);
	SwapMe(alarmChance);
	SwapMe(lockedChance);
}

void TimeCycDef::swawEndian()
{
	SwapMe(minX);
	SwapMe(minY);
	SwapMe(minZ);
	SwapMe(maxX);
	SwapMe(maxY);
	SwapMe(maxZ);
	SwapMe(percentage);
	SwapMe(range);
	SwapMe(startHour);
	SwapMe(endHour);
	SwapMe(hashCode);
}

void PathNodeDef::swawEndian()
{
	SwapMe(x);
	SwapMe(y);
	SwapMe(z);
	SwapMe(switchedOff);
	SwapMe(waterNode);
	SwapMe(roadBlock);
	SwapMe(speed);
	SwapMe(specialFunction);
	SwapMe(density);
	SwapMe(streetNameHash);
	SwapMe(flags);
}

void PathLinkDef::swawEndian()
{
	SwapMe(node1);
	SwapMe(node2);
	SwapMe(width);
	SwapMe(lanes1To2);
	SwapMe(lanes2To1);
}

void BlockDef::swawEndian()
{
	SwapMe(version);
	SwapMe(flags);
	SwapMe(x1);
	SwapMe(y1);
	SwapMe(x2);
	SwapMe(y2);
	SwapMe(x3);
	SwapMe(y3);
	SwapMe(x4);
	SwapMe(y4);
}

void RootScriptDef::swawEndian()
{
	SwapMe(x);
	SwapMe(y);
	SwapMe(z);
	SwapMe(q_x);
	SwapMe(q_y);
	SwapMe(q_z);
	SwapMe(q_w);
	SwapMe(number_of_extra_coords);

	for(s32 i=0;i<MAX_EXTRA_COORDS;i++)
	{
		SwapMe(extra_coord[i].x);
		SwapMe(extra_coord[i].y);
		SwapMe(extra_coord[i].z);
	}

	SwapMe(WorkType);
	SwapMe(Home);
	SwapMe(LeisureType);
	SwapMe(FoodType);
}

void MiloPlusInstanceDef::swawEndian()
{
	SwapMe(posx);
	SwapMe(posy);
	SwapMe(posz);
	SwapMe(quatx);
	SwapMe(quaty);
	SwapMe(quatz);
	SwapMe(quatw);
	SwapMe(flags);
	SwapMe(id);
	SwapMe(count);
}

void LodModifierDef::swawEndian()
{
	sysEndian::SwapMe(minx);
	sysEndian::SwapMe(miny);
	sysEndian::SwapMe(minz);
	sysEndian::SwapMe(maxx);
	sysEndian::SwapMe(maxy);
	sysEndian::SwapMe(maxz);
	sysEndian::SwapMe(numlods);

	for(s32 i=0;i<MAX_LODPERMODIFIER;i++)
	{
		sysEndian::SwapMe(lodid[i]);
	}
}

void SlowZoneDef::swawEndian()
{
	sysEndian::SwapMe(minx);
	sysEndian::SwapMe(miny);
	sysEndian::SwapMe(minz);
	sysEndian::SwapMe(maxx);
	sysEndian::SwapMe(maxy);
	sysEndian::SwapMe(maxz);
}

void PatrolNodeDef::swawEndian()
{
	SwapMe(x);
	SwapMe(y);
	SwapMe(z);
	SwapMe(fHeadingX);	
	SwapMe(fHeadingY);	
	SwapMe(fHeadingZ);
	SwapMe(nDuration);
	SwapMe(hNodeTypeHash);
	SwapMe(hAssociatedBaseHash);
	SwapMe(hRouteNameHash);
}

void PatrolLinkDef::swawEndian()
{
	SwapMe(node1);
	SwapMe(node2);
}

void LodChildDef::swawEndian()
{
	SwapMe(containerHash);
	SwapMe(index);
}
