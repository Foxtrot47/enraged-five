//
// rage_xenon.cpp
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
//

#include "rage_xenon.h"

#if !__64BIT

// rage includes
#include "grcore/texturexenonproxy.h"
#include "grmodel/modelfactory.h"
#include "grmodel/shader.h"
#include "grmodel/shadergroup.h"
#include "phcore/materialmgrimpl.h"
#include "system/platform.h"
#include "system/param.h"
#include "paging/rscbuilder.h"


static void GeometryParamsHookXenon(const grmModel& model, const mshMesh& UNUSED_PARAM(mesh), const grmShaderGroup* pShaderGroup, int index, GeometryCreateParams* params)
{
	Assert(g_sysPlatform == platform::XENON);

	params->ReadWriteVtxBuf = RagePlatform::GetHeadBlendWriteBuffer();
	params->IsMicroMorph = RagePlatform::GetMicroMorphMesh();

	const int shaderIndex = model.GetShaderIndex(index);
	if(pShaderGroup->GetShader(shaderIndex).IsInstanced())
	{
		params->Edge = false;	//Probably not necessary to set this on Xbox... but doesn't hurt either
		//IsGpuInstanced basically means "replicate index buffer". This only needs to happen on Xbox.
		params->IsGpuInstanced = true;
	}

	if(!stricmp("grass_batch", pShaderGroup->GetShader(shaderIndex).GetName()))
	{
		params->CreateIndexBufferVertStream = true;
	}
}

void RagePlatformXenon::Init()
{
	g_sysPlatform = sysGetPlatform("xenon");
	g_ByteSwap = sysGetByteSwap(g_sysPlatform);

	grcTextureFactoryXenonProxy::CreatePagedTextureFactory();

	BaseInit();

	grmModelFactory::GetVertexConfigurator()->SetPackNormals(true);
	grmModelFactory::GetVertexConfigurator()->SetPackTexCoords(true);

	rage::atFunctor5<void, const grmModel&, const mshMesh&, const grmShaderGroup*, int, GeometryCreateParams*> functor;
	functor.Reset<&GeometryParamsHookXenon>();
	grmModel::SetGeometryParamsHook(functor);
}

#endif
