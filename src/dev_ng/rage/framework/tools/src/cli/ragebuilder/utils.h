//
// Generic utilities.
//
// Copyright (C) 1999-2005 Rockstar North.  All Rights Reserved. 
//

#ifndef RAGEBUILDER_UTILS_H_
#define RAGEBUILDER_UTILS_H_

#include "atl/binmap.h"
#include "atl/binmap_struct.h"
#include "atl/string.h"
#include "file/stream.h"

#include <string>
#include <vector>
#include <list>
#include <map>

// For normalised paths
#define SEPARATOR			'/'
#define SEPARATOR_STRING	"/"
#define OTHER_SEPARATOR		'\\'

typedef std::list<std::string>						string_list;
typedef std::map<rage::atString, rage::atString>	tFileMTimeList;
typedef tFileMTimeList::iterator					tFileMTimeListIter;
typedef tFileMTimeList::const_iterator				tFileMTimeListConstIter;

void		AddSubDirToList(const char* pRoot,string_list& list);
void		AddFilesToList(const char* pFilter, string_list& list, bool bRelative = true, bool bSorted = true, bool bFilterDuplicateBasenames = false );
void		AddPlatformFilesToList(const char* pFilter, string_list& list, bool bRelative = true, bool bSorted = true );
void		SplitFilename( const char* filename, char* path, char* basename, char* extension );
void		PushRelativePathToFile(const char* filename);
void		SetRelativePathToFile(const char* filename,const char* extra = NULL);
void		RelativePath(char *dest, int maxLen, const char *base);
void		InsertIntoUnSortedList(string_list& list, const char* name);
void		InsertIntoSortedList(string_list& list, const char* name);
rage::u64	Get_FileTime(const char* file);
rage::u64	Get_FileSize(const char* file);
void		NormalisePath(char *dest, int destSize, const char *src);
void		NormalisePathBack(char *dest, int destSize, const char *src);
bool		DirExists(const char* path);
bool		CopyFileFunnel(const char* szSource, const char* szDestination);
bool		ReadLine(rage::fiStream* fileRead,char* buffer,int maxSize);
void		CheckFileSize(const char* filename, const char* ext, size_t resource);
std::string	Get_FileTimeString( const char* filename );
void		WriteFileMTimeList( const char* filename, const tFileMTimeList& filelist );
bool		IsPowerOfTwo(u32 x);
bool		IsMultipleOfFour(u32 x);

#endif RAGEBUILDER_UTILS_H_
