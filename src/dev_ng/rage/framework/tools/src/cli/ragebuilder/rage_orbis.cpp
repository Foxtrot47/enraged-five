//
// rage_win32.cpp
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#include "rage_orbis.h"

#include "grcore/texturedefault.h"
#include "grcore/texturefactory_d3d11.h"
#include "grmodel/model.h"
#include "grmodel/modelfactory.h"
#include "system/platform.h"


static void GeometryParamsHookOrbis(const grmModel& UNUSED_PARAM(model), const mshMesh& UNUSED_PARAM(mesh), const grmShaderGroup* UNUSED_PARAM(pShaderGroup), int UNUSED_PARAM(index), GeometryCreateParams* params)
{
	Assert(g_sysPlatform == platform::ORBIS);

	params->ReadWriteVtxBuf = RagePlatform::GetHeadBlendWriteBuffer();
	params->IsMicroMorph = RagePlatform::GetMicroMorphMesh();
}

void RagePlatformOrbis::Init()
{
	g_sysPlatform = sysGetPlatform("orbis");
	g_ByteSwap = sysGetByteSwap(g_sysPlatform);

	// Orbis loads in Durango textures. grcTextureFactoryDX11 creates Durango textures when compiled into ragebuilder.
	grcTextureFactoryDX11::CreatePagedTextureFactory(); 

	BaseInit();

	grmModelFactory::GetVertexConfigurator()->SetPackNormals(false);

	rage::atFunctor5<void, const grmModel&, const mshMesh&, const grmShaderGroup*, int, GeometryCreateParams*> functor;
	functor.Reset<&GeometryParamsHookOrbis>();
	grmModel::SetGeometryParamsHook(functor);
}
