//
// rage_win32.cpp
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#include "rage_durango.h"

#include "grcore/texturedefault.h"
#include "grcore/texturefactory_d3d11.h"
#include "grmodel/model.h"
#include "grmodel/modelfactory.h"
#include "system/platform.h"


static void GeometryParamsHookDurango(const grmModel& UNUSED_PARAM(model), const mshMesh& UNUSED_PARAM(mesh), const grmShaderGroup* UNUSED_PARAM(pShaderGroup), int UNUSED_PARAM(index), GeometryCreateParams* params)
{
	Assert(g_sysPlatform == platform::DURANGO);

	params->ReadWriteVtxBuf = RagePlatform::GetHeadBlendWriteBuffer();
	params->IsMicroMorph = RagePlatform::GetMicroMorphMesh();
}

void RagePlatformDurango::Init()
{
	g_sysPlatform = sysGetPlatform("durango");
	g_ByteSwap = sysGetByteSwap(g_sysPlatform);

	grcTextureFactoryDX11::CreatePagedTextureFactory();

	BaseInit();

	grmModelFactory::GetVertexConfigurator()->SetPackNormals(false);

	rage::atFunctor5<void, const grmModel&, const mshMesh&, const grmShaderGroup*, int, GeometryCreateParams*> functor;
	functor.Reset<&GeometryParamsHookDurango>();
	grmModel::SetGeometryParamsHook(functor);
}
