#ifndef IMAGETIFF_H
#define IMAGETIFF_H

// RAGE headers
#include "grcore/image.h"

//
// TIFF Image Loading abstraction.
//
class TIFFImage
{
public:
	static void					Init( );
	static ::rage::grcImage*	LoadTIFF( const char* filename );

private:
	static bool ms_bInitialised;
};

#endif // IMAGETIFF_H
