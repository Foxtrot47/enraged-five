#ifdef NOT_USED_AT_THE_MOMENT_AND_WONT_COMPILE_IN_NEW_RAGE
#include "gtashaderfactory.h"
#include "grmodel/shadergroup.h"

#include "file/asset.h"
#include "file/token.h"
#include "system/param.h"

// Removed from Rage#include "grmodel/shaderbasic.h" //causes compile promblems in WR217
#include "grmodel/shaderfx.h"

#include <stdlib.h>

using namespace rage;

XPARAM(cheapshaders);

grmShader* grmShaderFactoryRagebuilder::Create( const char *filename, int argCount, const char** args ) {
	if (!stricmp(filename,"rage_default.shadert") || !stricmp(filename,"rage_alpha.shadert")) {
		int drawBucket = -1;
		if(argCount > 1)
		{
			// draw bucket index or name should be the second parameter
			const char *buf = args[1];
			if (buf[0] >= '0' && buf[0] <= '9')
				drawBucket = atoi(buf);
			else {
				const char *drawBucketName;
				for (int i=0; i<32; i++) {
					drawBucketName = grmShader::GetDrawBucketName(i);
					if (drawBucketName && !stricmp(drawBucketName,buf)) {
						drawBucket = i;
						break;
					}
				}
			}
		}
		return new grmShaderBasic(drawBucket);
	}
	else if (!stricmp(filename,"rage_clamped.shadert")) {
		return new grmShaderBasicClamped;
	}
	else if (!strchr(filename,'.')) {
#if GRMODEL_SHADERFX
# if __DEV
		if ( PARAM_cheapshaders.Get() ) {
			int tmpl = grmShaderFxTemplate::Lookup(filename);
			grcEffect& effect = grmShaderFxTemplate::Get(tmpl)->GetEffectBasis();
			grcEffectVar db = effect.LookupVar("__rage_drawbucket",false);
			int drawBucket = 0;
			if(db)
			{
#if __XENON || __PPU
				drawBucket = effect.GetAnnotationData(db,"Bucket",0);
#else
				effect.GetVar(db,drawBucket);
#endif
			}
			return new grmShaderBasic(drawBucket);
		}
		else
# endif		// __DEV
			return new grmShaderFx;
#else		// __WIN32
		return new grmShaderBasic;
#endif		// __WIN32
	}
	else {
		return new grmShaderBasic;
	}
}

void grmShaderFactoryRagebuilder::PlaceShader(datResource& rsc,grmShader **shaders,int count) {
	for (int i=0; i<count; i++) {
		rsc.PointerFixup(shaders[i]);
		if (shaders[i]->GetType() == grmShader::BASIC)
			::new (shaders[i]) grmShaderBasic(rsc);
#if GRMODEL_SHADERFX
		else if (shaders[i]->GetType() == grmShader::FX)
			::new (shaders[i]) grmShaderFx(rsc);
#endif
		else
			Quitf("Invalid type %d in grmShaderFactoryStandard::PlaceShaders",shaders[i]->GetType());
	}
}

void grmShaderFactoryRagebuilder::PlaceShaderGroup(datResource &rsc,grmShaderGroup* &group) {
	if (group) {
		rsc.PointerFixup(group);
		::new (group) grmShaderGroup(rsc);
	}
}



void grmShaderFactoryRagebuilder::PreloadShaders( const char *path, bool /* resetindex */) {
	ASSET.PushFolder(path);
	fiStream *S = ASSET.Open("preload","list");
	if (S) {
		Displayf("Preloading shaders from '%s'...",path);
		fiTokenizer T(path,S);
		char shaderName[128];
		while (T.GetToken(shaderName,sizeof(shaderName))) {
			char *ext = strrchr(shaderName,'.');
			if (ext && !stricmp(ext,".shadert"))
				Quitf(".shadert isn't supported any longer");
				// grmShaderTemplate::Load(shadertSlot++,shaderName,NULL,NULL);
#if GRMODEL_SHADERFX
			if (ext && (!stricmp(ext,".fx") || !stricmp(ext,".fx2"))) {
				*ext = 0;
				grmShaderFxTemplate::LoadIntoSlot(shaderName);
			}
#endif
		}
		S->Close();
	}
	else
	{
		Errorf("preload.list file missing in '%s'",path);
		// We need to load at least two shaders (for non-alpha and alpha fallbacks)
		grmShaderFxTemplate::LoadIntoSlot("embedded:/rage_im"); // Non-alpha fallback
		grmShaderFxTemplate::LoadIntoSlot("embedded:/rage_im"); // Alpha fallback
	}
	ASSET.PopFolder();
}


grmShaderGroup* grmShaderFactoryRagebuilder::CreateGroup() {
	return new grmShaderGroup;
}


grmShaderGroup* grmShaderFactoryRagebuilder::LoadGroup(fiTokenizer & T) {

	if (T.CheckIToken("shadinggroup"))
	{
		T.GetDelimiter("{");
		if (T.CheckToken("Count"))
			T.IgnoreToken();
		grmShaderGroup *g = CreateGroup();
		if (!g->Load(T))
		{
			Errorf("Failed to load shading group");
			return NULL;
		}
		T.GetDelimiter("}");
		return g;
	}
	else
	{
		if (T.CheckToken("Count"))
			T.IgnoreToken();
		grmShaderGroup *g = CreateGroup();
		if (!g->Load(T))
		{
			Errorf("Failed to load shading group");
			return NULL;
		}
		return g;
	}
}

grmShaderGroup* grmShaderFactoryRagebuilder::LoadGroup(const char *filename) {
	fiSafeStream S(ASSET.Open(filename,"shadergroup"));
	if (!S)
		return NULL;
	fiTokenizer T;
	T.Init(filename,S);
	return LoadGroup(T);
}


grmShaderFactory *grmShaderFactoryRagebuilder::SetActiveInstance() {
	grmShaderFactoryRagebuilder* pFactory = new grmShaderFactoryRagebuilder;

	grmShaderFactory::SetInstance(*pFactory);

	return(pFactory);
}
#endif
