setlocal
pushd "%~dp0"

CALL setenv.bat

set RAGE_DIR=%CD%\..\..\..\..\..

set SCE_PS3_ROOT=X:/ps3sdk/dev/usr/local/430_001/cell

SET VISUAL_STUDIO="C:\Program Files\Microsoft Visual Studio 9.0\Common7\IDE\devenv.exe"
SET VISUAL_STUDIO_X86="C:\Program Files (x86)\Microsoft Visual Studio 9.0\Common7\IDE\devenv.exe"
SET SOLUTION="%cd%\ragebuilder_2008.sln

IF /I "%PROCESSOR_ARCHITECTURE%"=="AMD64" ( 
	ECHO x64 detected, starting Visual Studio 2008
	start "" %VISUAL_STUDIO_X86% %SOLUTION%
) ELSE (
	ECHO x86 detected, starting Visual Studio 2008
	start "" %VISUAL_STUDIO% %SOLUTION%
)

popd
