//
// Logging support.
//
// Copyright (C) 1999-2005 Rockstar North.  All Rights Reserved.
//

#include "log.h"

#include "system/xtl.h"
#include "system/param.h"
#include "file/asset.h"
#include "file/device.h"
#include "diag/output.h"
#include "system/memory.h"

using namespace rage;

#include <conio.h>

bool		LogListener::ms_bStopOnError =  true;
#ifdef _DEBUG
int			LogListener::ms_logLevel = DIAG_SEVERITY_DEBUG3;
#else
int			LogListener::ms_logLevel = DIAG_SEVERITY_DISPLAY;
#endif
char		LogListener::ms_buildlog[MAX_PATH];
char		LogListener::ms_errorlog[MAX_PATH];
fiStream*	LogListener::ms_buildlogStream = NULL;
fiStream*	LogListener::ms_errorlogStream = NULL;
bool		LogListener::ms_bBlockWarningsAndErrors = false;
bool		LogListener::ms_bFileLogging = false;
int			LogListener::ms_errorCount;
bool		LogListener::ms_bErrorLogProduced;
UniversalLogFile *LogListener::ms_pUniLog = NULL;

#define MAX_LOG_SIZE	(2 * 1024 * 1024)
#define CLAMP_LOG_SIZE	(1 * 1024 * 1024)


PARAM(stoponerror, "Stop the build process when an error occurs");
PARAM(nologging, "");
PARAM(neverstoponerror, "Never allow the build process to stop when an error occurs");
PARAM(loglevel, "Stop the build process when an error occurs");
PARAM(buildlog, "Path for the log file");
PARAM(errorlog, "Path for the error log");
PARAM(appendlog, "Append the log, rather than wipe it on start up");
PARAM(uLogFileName, "Filename for logging to universal log from max." );

// Initialise.
// Uses command line arguments that can override the values used as defaults.
void LogListener::Init( const char* buildlog, const char* errorlog )
{

	const char *uLogFile = NULL;
#if !defined(RUBYRAGE)
	if(PARAM_uLogFileName.Get(uLogFile))
	{
		sysMemStartTemp( );
		ms_pUniLog = new UniversalLogFile(atString(uLogFile), atString("Ragebuilder"));
		sysMemEndTemp( );
	}
#endif

#if defined(RAGEBUILDER_LIB) || defined(RUBYRAGE)

	diagOutput::SetOutputMask( diagOutput::OUTPUT_ALL );
	diagLogCallback = LogFn;

#else

	ms_errorCount = 0;
	ms_bErrorLogProduced = false;

	ms_bFileLogging = false;

	ms_bStopOnError = PARAM_stoponerror.Get();
	PARAM_loglevel.Get(ms_logLevel);
	PARAM_buildlog.Get(buildlog);
	strncpy(ms_buildlog, buildlog, MAX_PATH);
	PARAM_errorlog.Get(errorlog);
	strncpy(ms_errorlog, errorlog, MAX_PATH);

	// setup logging
	diagLogCallback = LogFn;

	int NoLogging = 0;

	PARAM_nologging.Get(NoLogging);

	if(NoLogging != 1)
	{
		// open the log files
		ms_buildlogStream = WaitToCreateStream(ms_buildlog, false, PARAM_appendlog.Get());

		if (ms_buildlogStream == NULL)
		{
			char * message = "A previous instance of ragebuilder is still running."
					"\nDo you want to wait until it has completed?"
					"\n\nPress Yes to wait (recommended)."
					"\nPress No if you want to continue."
					"\n\nPressing NO will disable logging functions, so the errorlog and buildlog"
					"\nwill NOT be produced!";

			ms_bFileLogging = ( MessageBox(GetActiveWindow(), message, "Ragebuilder warning", MB_YESNO | MB_ICONERROR) == IDYES );

			if (ms_bFileLogging)
			{
				ms_buildlogStream = WaitToCreateStream(ms_buildlog, true, PARAM_appendlog.Get());
			}
		}
		else
		{
			ms_bFileLogging = true;
		}
	}

	if (ms_bFileLogging)
	{
		if (ms_buildlogStream->Size() > MAX_LOG_SIZE)
		{
			ms_buildlogStream = ClampStreamSize(ms_buildlog, ms_buildlogStream, CLAMP_LOG_SIZE, true);
		}

		// only creates the error log when an error occurs - but needs to delete any old error log
		// no fiDevice::Exists is implemented, but this works as well
		// only delete if we're logging
		if (fiDeviceLocal::GetInstance().GetFileTime(ms_errorlog) != 0)
		{
			if (fiDeviceLocal::GetInstance().Delete(ms_errorlog) == false)
			{
				Quitf("Could not delete the existing error log - %s", ms_errorlog);
			}
		}

		// only write this to the file
		char* pStart =	"===============================================================================\r\n"
			"================================== LOG START ==================================\r\n"
			"===============================================================================\r\n";

		ms_buildlogStream->Write( pStart, strlen(pStart));

		char preamble[512];
		GetTimeStamp( preamble, sizeof(preamble) );
		int len = strlen(strcat(preamble, " Log started.\r\n"));
		ms_buildlogStream->Write(preamble, len);
		ms_buildlogStream->Flush();
	}
#endif
}

// Create a filestream, but wait until able to create the file.
fiStream* LogListener::WaitToCreateStream(const char* name, bool bWait, bool bAppend)
{
	HANDLE hStdout;
	CONSOLE_SCREEN_BUFFER_INFO csbiInfo;

	ms_bBlockWarningsAndErrors = true;

	hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	GetConsoleScreenBufferInfo(hStdout, &csbiInfo);

	fiStream* stream = NULL;
	bool bExists = ASSET.Exists(name, "");
	if (bAppend && bExists)
	{
		stream = fiStream::Open(name, fiDeviceLocal::GetInstance(), false);
	}
	else
	{
		stream = fiStream::Create(name, fiDeviceLocal::GetInstance());
	}

	bool dots = false;

	while (bWait && stream == NULL)
	{
		Sleep(1000);

		if (dots)
		{
			printf("Waiting for access to log   ");
		}
		else
		{
			printf("Waiting for access to log...");
		}
		dots = !dots;
		SetConsoleCursorPosition(hStdout, csbiInfo.dwCursorPosition);

		if (bAppend && bExists)
		{
			stream = fiStream::Open(name, fiDeviceLocal::GetInstance(), false);
		}
		else
		{
			stream = fiStream::Create(name, fiDeviceLocal::GetInstance());
		}
	}
	ms_bBlockWarningsAndErrors = false;

	if (stream && bAppend)
	{
		stream->Seek(stream->Size());
	}

	return stream;
}

// Clamp a stream to a certain size.
// Due to some complication with other instances of ragebuilder waiting for the file to become available,
// this function has become slightly horrible.
fiStream* LogListener::ClampStreamSize(const char* name, fiStream* pStream, int size, bool bFindNewLine)
{
	if (pStream->Size() < size)
	{
		return pStream;
	}

	char* pBuffer = new char[size];

	if (pStream->Seek(pStream->Size() - size) == -1)
	{
		delete pBuffer;
		Quitf("Unable to seek in log");
		return NULL;
	}

	if (pStream->Read(pBuffer, size) != size)
	{
		delete pBuffer;
		Quitf("Unable to read in log");
		return NULL;
	}

	pStream->Close();
	pStream = fiStream::Create(name, fiDeviceLocal::GetInstance());

	if (pStream == NULL)
	{
		delete pBuffer;
		Quitf("Unable to recreate the build log");
		return NULL;
	}

	char* pPos = pBuffer;
	int32 realsize = size;
	if (bFindNewLine)
	{
		pPos = strchr(pBuffer, '\n');
		if (pPos == NULL)
		{
			pPos = pBuffer;
		}
		else
		{
			++pPos;
			realsize = size - (pPos - pBuffer);
		}

	}
	char* pDivider =	"===============================================================================\r\n"
						"=================================== CLAMPED ===================================\r\n"
						"===============================================================================\r\n";

	pStream->Write(pDivider, strlen(pDivider));

	if (pStream->Write(pPos, realsize) != realsize)
	{
		delete pBuffer;
		Quitf("Unable to write to log");
		return NULL;
	}

	delete pBuffer;

	return pStream;
}

// Close.
void LogListener::Close()
{
	diagLogCallback = diagLogDefault;

	if ( ms_pUniLog )
	{
		sysMemStartTemp( );
		delete ms_pUniLog;
		sysMemEndTemp( );
	}

	if( ms_errorlogStream )
	{
		ms_errorlogStream->Close();
		ms_errorlogStream = NULL;
	}

	if( ms_buildlogStream )
	{
		ms_buildlogStream->Close();
		ms_buildlogStream = NULL;
	}
}

// Enable/disable stop on error.
void LogListener::SetStopOnError(bool bEnable)
{
	ms_bStopOnError = bEnable;
}

// Set the level of logging.
void LogListener::SetLogLevel(int level)
{
	ms_logLevel = level + DIAG_SEVERITY_DISPLAY;
}


///////////////////////////////////////////////////////////////////////////////

// The prefix to add to messages, depending on the mode
static const char* sPrintMessage[DIAG_SEVERITY_COUNT] =
{
	"",
	"",
	"",
	"Warning: ",
	"Error: ",
	"Fatal Error: "
};

// The ending to stick on messages, depending on the mode
static const char *sTail[DIAG_SEVERITY_COUNT] =
{
	"",
	"\r\n",
	"\r\n",
	"\r\n",
	"\r\n",
	"\r\n"
};

// Return a timestamp string
void LogListener::GetTimeStamp(char* timestamp, int maxLen)
{
	static SYSTEMTIME time;
	GetLocalTime( &time );

	formatf( timestamp, maxLen, "[%04d.%02d.%02d|%02d:%02d:%02d]",
		time.wYear,
		time.wMonth,
		time.wDay,
		time.wHour,
		time.wMinute,
		time.wSecond );
}

#if defined(RAGEBUILDER_LIB) || defined( RUBYRAGE )

void FnStdOut( const char* msg )
{
	printf( msg );
}

static LogOutFn* g_LogStdOutFn = FnStdOut;
static LogOutFn* g_LogStdErrFn = FnStdOut;

RAGEBUILDER_DLL void RBSetStdOut(LogOutFn* fn)
{
	g_LogStdOutFn = fn;
}

RAGEBUILDER_DLL void RBSetStdErr(LogOutFn* fn)
{
	g_LogStdErrFn = fn;
}

bool LogListener::LogFn(const rage::diagChannel &channel,rage::diagSeverity severity,const char *file,int line,const char *fmt, va_list args)
{
	const size_t msg_sz = 4096;
	const size_t svr_sz = 50;
	char message[msg_sz];
	char severity_str[svr_sz];
	memset( message, 0, sizeof(char) * msg_sz );
	memset( severity_str, 0, sizeof(char) * svr_sz );

	switch (severity)
	{
	case DIAG_SEVERITY_ERROR:
		sprintf_s( severity_str, svr_sz, "%s", "Error" );
		break;
	case DIAG_SEVERITY_FATAL:
		sprintf_s( severity_str, svr_sz, "%s", "Fatal Error" );
		break;
	case DIAG_SEVERITY_ASSERT:
		sprintf_s( severity_str, svr_sz, "%s", "Assert" );
		break;
	case DIAG_SEVERITY_WARNING:
		sprintf_s( severity_str, svr_sz, "%s", "Warning" );
		break;
	case DIAG_SEVERITY_DEBUG1:
	case DIAG_SEVERITY_DEBUG2: 
	case DIAG_SEVERITY_DEBUG3:
		sprintf_s( severity_str, svr_sz, "%s", "Debug" );
		break;
	case DIAG_SEVERITY_DISPLAY:
	default:
		sprintf_s( severity_str, svr_sz, "%s", "Info" );
		break;
	}
	sprintf_s( message, msg_sz, "___rage: %s: %s\n", severity_str, msg );

	switch (severity)
	{
	case DIAG_SEVERITY_FATAL:
	case DIAG_SEVERITY_ERROR:
	case DIAG_SEVERITY_WARNING:
	case DIAG_SEVERITY_ASSERT:
		g_LogStdErrFn( message );
		break;
	case DIAG_SEVERITY_DEBUG1:
	case DIAG_SEVERITY_DEBUG2: 
	case DIAG_SEVERITY_DEBUG3:
	case DIAG_SEVERITY_DISPLAY:
	default:
		g_LogStdOutFn( message );
		break;
	}
	return ( true );
}

#else

// A log function callback.
bool LogListener::LogFn(const diagChannel &channel,diagSeverity severity,const char *file,int line,const char* fmt,va_list args)
{
	if (ms_logLevel < severity)
		return true;

	static char buffer[512];

	if (ms_bBlockWarningsAndErrors && (severity == DIAG_SEVERITY_ERROR || severity == DIAG_SEVERITY_WARNING))
	{
		return true;
	}

	//this crashes if the application has quit because of an exception
	//so the bit ive added in above stops it from trying to print out
	//after an exit GRS

	// diagOutput::GetPrinterDefaultFn()(mode, fmt, args);
	bool rt = diagLogDefault(channel, severity, file, line, fmt, args);

	if (!ms_bFileLogging)
	{
		return rt;
	}

	char msg[512];
	vformatf(msg, sizeof(msg), fmt, args);

	if(ms_pUniLog)
	{
		switch(severity)
		{
		case DIAG_SEVERITY_FATAL:
		case DIAG_SEVERITY_ASSERT:
		case DIAG_SEVERITY_ERROR:
			ms_pUniLog->LogError(UniversalLogFile::sLogMessageEventArgs(atString(msg)));
				break;
		case DIAG_SEVERITY_WARNING:
			ms_pUniLog->LogWarning(UniversalLogFile::sLogMessageEventArgs(atString(msg)));
			break;
		case DIAG_SEVERITY_DISPLAY: 
			ms_pUniLog->LogMessage(UniversalLogFile::sLogMessageEventArgs(atString(msg)));
			break;
		case DIAG_SEVERITY_DEBUG1:
		case DIAG_SEVERITY_DEBUG2: 
		case DIAG_SEVERITY_DEBUG3: 
		default:
#ifdef _DEBUG
			ms_pUniLog->LogDebug(UniversalLogFile::sLogMessageEventArgs(atString(msg)));
#endif
			break;
		}
	}

	if(severity == DIAG_SEVERITY_FATAL)
	{
		ms_buildlogStream->Close();
		ms_buildlogStream = NULL;
		ms_bBlockWarningsAndErrors = true;
		return rt;
	}

	static char stamp[22];
	GetTimeStamp( stamp, sizeof(stamp) );

	int iLen = strlen( formatf(buffer, sizeof(buffer), "%s %s%s%s", stamp, sPrintMessage[severity], msg, sTail[severity]) );

	if (ms_buildlogStream)
	{
		ms_buildlogStream->Write( buffer, iLen );
		ms_buildlogStream->Flush();
	}

	if (severity == DIAG_SEVERITY_ERROR || severity == DIAG_SEVERITY_WARNING)
	{
		if (severity == DIAG_SEVERITY_ERROR)
		{
			++ms_errorCount;
		}

		// create an error log on demand
		if( !ms_errorlogStream )
		{
			static bool bTriedBefore = false;
			if( !bTriedBefore )
			{
				bTriedBefore = true;

				ms_errorlogStream = WaitToCreateStream(ms_errorlog, false);
				if (ms_errorlogStream == NULL)
				{
					Quitf("Could not create the error log '%s'",ms_errorlog);
					return false;
				}
				ms_bErrorLogProduced = true;
			}
		}

		ms_errorlogStream->Write(buffer,iLen);
		ms_errorlogStream->Flush();
	}

	// stop on error
	if( !PARAM_neverstoponerror.Get() && ms_bStopOnError && severity == DIAG_SEVERITY_ERROR )
	{
		_getch();
	}

	return rt;
}

#endif
