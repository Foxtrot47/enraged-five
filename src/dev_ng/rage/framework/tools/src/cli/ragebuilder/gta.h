//
// utilities for loading gta files (ide and ipl)
//
// Copyright (C) 1999-2005 Rockstar North.  All Rights Reserved.
//

#ifndef RAGEBUILDER_GTA_H_
#define RAGEBUILDER_GTA_H_

#include "file/stream.h"
#include "atl/array.h"
#include "atl/string.h"
#include "atl/binmap.h"
#include "vector/vector3.h"

#include "gta_res.h"

using namespace rage;

#define IDE_VERSION 1
#define IPL4_VERSION 4
#define IPL5_VERSION 5
#define IPL6_VERSION 6
#define MODEL_NAME_LEN 24
#define GARAGE_NAME_LENGTH 8
#define ARRAY_INITSIZE 32


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class SetReader
{
protected:
	bool readLine(fiStream* p_stream,char* p_line,s32 lineMax);
	void removeCommas(char* p_line);
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct IdeFileHeader
{
	s32 Version;
	s32 NumObjects;
	s32 NumExtra2;
	s32 NumExtra3;
	s32 NumExtra4;
	s32 NumExtra5;
	s32 NumExtra6;
	s32 NumExtra7;
	s32 NumExtra8;
	s32 NumExtra9;
	s32 NumExtra10;
	s32 NumExtra11;
	s32 NumExtra12;
	s32 NumExtra13;
	s32 NumExtra14;
	s32 NumExtra15;
	s32 NumExtra16;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct ObjectDef
{
	u32 modelHashKey;
	s32 txdHashKey;
	s32 ddModelHashKey;
	f32 lodDist;
	s32 flags;
	s32 specialAttribute;
	f32 bbMinX;
	f32 bbMinY;
	f32 bbMinZ;
	f32 bbMaxX;
	f32 bbMaxY;
	f32 bbMaxZ;
	f32 bsCentreX;
	f32 bsCentreY;
	f32 bsCentreZ;
	f32 bsRadius;
};

struct TimeObjectDef : public ObjectDef
{

};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct TreeDef
{
	u32 modelHashKey;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct PedDef
{
	char name[64];
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct MiloItemDef
{
	f32 posx;
	f32 posy;
	f32 posz;
	f32 quatx;
	f32 quaty;
	f32 quatz;
	f32 quatw;
	u32 objectHashKey;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct MiloRoomDef
{
	char name[64];
	atArray<s32> instanceList;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct MiloDef
{
	u32 modelHashKey;
	atArray<MiloItemDef> m_objects;
	atArray<MiloRoomDef> m_rooms;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct MiloObjNames
{
	atArray<atString> m_names;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct EffectDef
{
	CLightAttr  l;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class IdeSet : public SetReader
{
public:
	~IdeSet();

	void reset();
	void load(const char* pFilename);
	void loadBinary(const char* pFilename,bool SwapEndian);
	void save(const char* pFilename);
	void saveBinary(const char* pFilename,bool SwapEndian);
	bool doesObjectExist(u32 objectHash);
	bool isObjectInMilo(u32 objectHash);
	bool isObjectAMilo(u32 objectHash);

	s32 getNumMilos() { return m_listMiloNames.GetCount(); }
	atString& getMiloName(s32 index) { return m_listMiloNames[index]; }
	s32 getMiloNumInstances(s32 index);
	atString& getMiloInstanceName(s32 index,s32 instanceindex);
	s32 getMiloNumRooms(s32 index);
	const char* getMiloRoomName(s32 index,s32 roomindex);
	s32 getMiloRoomNumInstances(s32 index,s32 roomindex);
	s32 getMiloRoomInstance(s32 index,s32 roomindex,s32 instanceindex);

	s32 getNumObjects() { return m_listObjectNames.GetCount(); }
	atString& getObjectName(s32 index) { return m_listObjectNames[index]; }
	atString& getTxdName(s32 index) { return m_listTxdNames[index]; }
	f32 getLodDistance(s32 index)  { return m_listObjectDefs[index].lodDist; }
	atString& getDdName(s32 index) { return m_listDdNames[index]; }
	s32 getObjectFlags(s32 index) { return m_listObjectDefs[index].flags; }

	s32 getNumTimeObjects() { return m_listTimeObjectNames.GetCount(); }
	atString& getTimeObjectName(s32 index) { return m_listTimeObjectNames[index]; }
	atString& getTimeObjectTxdName(s32 index) { return m_listTimeObjectTxdNames[index]; }
	f32 getTimeObjectLodDistance(s32 index)  { return m_listTimeObjectDefs[index].lodDist; }
	atString& getTimeObjectDdName(s32 index) { return m_listTimeObjectDdNames[index]; }
	s32 getTimeObjectFlags(s32 index) { return m_listTimeObjectDefs[index].flags; }

	s32 getNumEffects() { return m_listEffectDefs.GetCount(); }
	const EffectDef& getEffect(s32 index) { return m_listEffectDefs[index]; }

	atBinaryMap<u32, u32> m_listObjectHash;
	atArray<atString,0,u32> m_listObjectNames;
	atArray<atString,0,u32> m_listTxdNames;
	atArray<atString,0,u32> m_listDdNames;
	atArray<ObjectDef,0,u32> m_listObjectDefs;

	atBinaryMap<u32, u32> m_listTimeObjectHash;
	atArray<atString,0,u32> m_listTimeObjectNames;
	atArray<atString,0,u32> m_listTimeObjectTxdNames;
	atArray<atString,0,u32> m_listTimeObjectDdNames;
	atArray<TimeObjectDef,0,u32> m_listTimeObjectDefs;

	atBinaryMap<u32, u32> m_listTreeHash;
	atArray<atString> m_listTreeNames;
	atArray<TreeDef> m_listTreeDefs;

	atBinaryMap<u32, u32> m_listMiloHash;
	atArray<atString> m_listMiloNames;
	atArray<MiloDef> m_listMiloDefs;
	atArray<MiloObjNames> m_listMiloObjNames;

	atArray<PedDef> m_listPedDefs;

	atArray<EffectDef> m_listEffectDefs;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct IplFileHeader
{
	s32 Version;
	s32 NumInstances;
	s32 NumZones;
	s32 NumGarages;
	s32 NumCarGens;
	s32 NumTimeCycs;
	s32 NumPathNodes;
	s32 NumPathLinks;
	s32 NumRootScripts;
	s32 NumMiloPlus;
	s32 NumLodModifiers;
	s32 NumSlowZones;
	s32 NumExtra12;
	s32 NumExtra13;
	s32 NumExtra14;
	s32 NumExtra15;
	s32 NumBlocks;
	s32 NumPatrolNodes;
	s32 NumPatrolLinks;

	void swawEndian();
};
CompileTimeAssert(sizeof(IplFileHeader) == 76);

struct Ipl5FileHeader
{
	s32 Version;
	s32 NumInstances;
	s32 NumZones;
	s32 NumGarages;
	s32 NumCarGens;
	s32 NumTimeCycs;
	s32 NumPathNodes;
	s32 NumPathLinks;
	s32 NumRootScripts;
	s32 NumMiloPlus;
	s32 NumLodModifiers;
	s32 NumSlowZones;
	s32 NumExtra12;
	s32 NumExtra13;
	s32 NumExtra14;
	s32 NumExtra15;
	s32 NumBlocks;
	s32 NumPatrolNodes;
	s32 NumPatrolLinks;

	void swawEndian();
};
CompileTimeAssert(sizeof(Ipl5FileHeader) == 76);

#define IPL6_HEADER_RESERVED (52)
struct Ipl6FileHeader
{
	s32 Version;
	s32 NumBounds;
	s32 NumInstances;
	s32 NumMiloInstances;
	s32 NumZones;
	s32 NumGarages;
	s32 NumCarGens;
	s32 NumTimeCycs;
	s32 NumPathNodes;
	s32 NumPathLinks;
	s32 NumRootScripts;
	s32 NumMiloPlus;
	s32 NumSlowZones;
	s32 NumBlocks;
	s32 NumPatrolNodes;
	s32 NumPatrolLinks;
	s32 NumLodChildren;
	s32 NumLightInsts_DEPRECATED;

	// Reserved data area.
	u8 reserved[IPL6_HEADER_RESERVED];	// Attempt to be future-proof.

	// Meta-data in header grows backwards.
	f32 MaxLodDistance;

	Ipl6FileHeader()
		: Version( 0 )
		, NumBounds( 0 )
		, NumInstances( 0 )
		, NumMiloInstances( 0 )
		, NumZones( 0 )
		, NumGarages( 0 )
		, NumCarGens( 0 )
		, NumTimeCycs( 0 )
		, NumPathNodes( 0 )
		, NumPathLinks( 0 )
		, NumRootScripts( 0 )
		, NumMiloPlus( 0 )
		, NumSlowZones( 0 )
		, NumBlocks( 0 )
		, NumPatrolNodes( 0 )
		, NumPatrolLinks( 0 )
		, NumLodChildren( 0 )
		, NumLightInsts_DEPRECATED( 0 )
		, MaxLodDistance( 0.0f )
	{
		memset( &reserved, 0, sizeof( u8 ) * IPL6_HEADER_RESERVED );
	}

	void swawEndian();
};
CompileTimeAssert(sizeof(Ipl6FileHeader) == 128);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct BoundDef
{
	u32 index;
	f32 minx;
	f32 miny;
	f32 minz;
	f32 maxx;
	f32 maxy;
	f32 maxz;

	BoundDef()
		: minx( 0.0f )
		, miny( 0.0f )
		, minz( 0.0f )
		, maxx( 0.0f )
		, maxy( 0.0f )
		, maxz( 0.0f )
	{
	}

	void swawEndian();
};
CompileTimeAssert(sizeof(BoundDef) == 28);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct InstanceDef
{
	f32 posx;
	f32 posy;
	f32 posz;
	f32 quatx;
	f32 quaty;
	f32 quatz;
	f32 quatw;
	u32 modelHashKey;
	u32 areaCodeAndFlags;
	s32 lodIndex;
	s32 blockIndex;
	f32 lodDistance;

	void swawEndian();
};
CompileTimeAssert(sizeof(InstanceDef) == 48);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief IPL5 Instance Definition
 */
struct Instance5Def
{
	f32 posx;
	f32 posy;
	f32 posz;
	f32 quatx;
	f32 quaty;
	f32 quatz;
	f32 quatw;
	u32 modelHashKey;
	u32 areaCodeAndFlags;
	s32 lodIndex;
	struct  
	{
		u32 unused		: 28;	// currently unused
		u32 lightGroup	: 4;	// "Light Group"
	} flagsExtra;			// Was blockIndex, now additional flags (incl. Light Group)
	f32 lodDistance;
	s32 typeOrNumAncestors;	// IPL5: Instance type flag (SLOD 0, LOD 1, HD 2).
							// IPL6: Instance num ancestors
	s32 numChildren;		//!< Number of children (if SLOD or LOD, 0 otherwise).
	u32 ambOcclMultiplier;	// Ambient Occlusion Multiplier (default 255)
	f32 scaleXY;
	f32 scaleZ;

	void swawEndian();
};
CompileTimeAssert(sizeof(Instance5Def) == 68);

/**
* @brief IPL6 MILO Instance Definition
*/
struct MiloInstance6Def
{
	f32 posx;
	f32 posy;
	f32 posz;
	f32 quatx;
	f32 quaty;
	f32 quatz;
	f32 quatw;
	u32 modelHashKey;
	u32 areaCodeAndFlags;
	s32 lodIndex;
	s32 blockIndex;
	f32 lodDistance;
	s32 numAncestors;		//!< Number of ancestor parents (i.e. depth of tree).
	s32 numChildren;		//!< Number of children (if SLOD or LOD, 0 otherwise).
	u32 ambOcclMultiplier;	// Ambient Occlusion Multiplier (default 255)
	u32 groupId;			// Milo interior group identifier.
	u32 reserved1;
	u32 reserved2;
	u32 reserved3;
	u32 reserved4;

	MiloInstance6Def()
		: reserved1( 0 )
		, reserved2( 0 )
		, reserved3( 0 )
		, reserved4( 0 )
	{
	}

	void swawEndian();
};
CompileTimeAssert(sizeof(MiloInstance6Def) == 80);



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct ZoneDef
{
	char name[MODEL_NAME_LEN];
	char text_label[10];
	f32 minx;
	f32 miny;
	f32 minz;
	f32 maxx;
	f32 maxy;
	f32 maxz;
	int32 type;
	int32 level;

	void swawEndian();
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct GarageDef
{
	float x1,y1,z1;
	float x2,y2;
	float x3,y3;
	float ztop;
	int32 flag;
	int32 type;
	char name[GARAGE_NAME_LENGTH];

	void swawEndian();
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct CarGenDef
{
	float x;
	float y;
	float z;
	float vec1x;
	float vec1y;
	float lengthVec2;
	int32 modelHashKey;
	int32 col1;
	int32 col2;
	int32 col3;
	int32 col4;
	int32 flagVal;
	int32 alarmChance;
	int32 lockedChance;

	void swawEndian();
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct TimeCycDef
{
	f32 minX;
	f32 minY;
	f32 minZ;
	f32 maxX;
	f32 maxY;
	f32 maxZ;
	f32 percentage;
	f32 range;
	int32 startHour;
	int32 endHour;
	int32 hashCode;

	void swawEndian();
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct PathNodeDef
{
	f32 x;
	f32 y;
	f32 z;
	int32 switchedOff;
	int32 waterNode;
	int32 roadBlock;
	int32 speed;
	int32 specialFunction;
	f32 density;
	uint32 streetNameHash;

	enum
	{
		FLAG_NONE 		= 0x0000,
		FLAG_HIGHWAY	= 0x0001
	};

	uint32 flags;

	void swawEndian();
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct PathLinkDef
{
	int32 node1;
	int32 node2;
	f32 width;
	int32 lanes1To2;
	int32 lanes2To1;

	void swawEndian();
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct MiloPlusInstanceDef
{
	char name[MODEL_NAME_LEN];
	int32 flags;
	int32 id;
	int32 count;
	f32 posx;
	f32 posy;
	f32 posz;
	f32 quatx;
	f32 quaty;
	f32 quatz;
	f32 quatw;

	void swawEndian();
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct PatrolNodeDef
{
	f32 x;
	f32 y;
	f32 z;
	f32 fHeadingX;
	f32 fHeadingY;
	f32 fHeadingZ;
	int32 nDuration;
	uint32 hNodeTypeHash;
	uint32 hAssociatedBaseHash;
	uint32 hRouteNameHash;

	void swawEndian();
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct PatrolLinkDef
{
	int32 node1;
	int32 node2;

	void swawEndian();
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define SCRIPT_NAME_LENGTH (24)
#define MAX_EXTRA_COORDS	(4)

struct extra_coord_struct
{
	f32 x;
	f32 y;
	f32 z;
};

struct RootScriptDef	//	script brain attached to a point on the map
{
	f32 x;
	f32 y;
	f32 z;
	f32 q_x;
	f32 q_y;
	f32 q_z;
	f32 q_w;
	char script_name[SCRIPT_NAME_LENGTH];
	int32 number_of_extra_coords;
	extra_coord_struct extra_coord[MAX_EXTRA_COORDS];

	uint8	WorkType;
	uint8	Home;
	uint8	LeisureType;
	uint8	FoodType;

	void swawEndian();
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct BlockDef
{
	int32 version;
	char name_owner_export_time[92];
	int32 flags;
	f32 x1,y1;
	f32 x2,y2;
	f32 x3,y3;
	f32 x4,y4;

	void swawEndian();
};

#define MAX_LODPERMODIFIER 10
#define LODMOD_NUMBERMASK (0x0FFF)
#define LODMOD_FLAGMASK (0xF000)
#define LODMOD_FLAGSHIFT (24)

struct LodModifierDef
{
	f32 minx,miny,minz;
	f32 maxx,maxy,maxz;
	u32 numlods;
	s32 lodid[MAX_LODPERMODIFIER];
	char names[MAX_LODPERMODIFIER][32];

	void swawEndian();
};

struct SlowZoneDef
{
	f32 minx,miny,minz;
	f32 maxx,maxy,maxz;

	void swawEndian();
};

#define LOD_CHILD_RESERVED (8)
struct LodChildDef
{
	u32 containerHash;
	u32 index;
	u8 reserved[LOD_CHILD_RESERVED];

	LodChildDef()
		: containerHash( 0L )
		, index( 0L )
	{
		memset( &reserved, 0, sizeof(u8) * LOD_CHILD_RESERVED );
	}

	void swawEndian();
};
CompileTimeAssert(sizeof(LodChildDef) == 16);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class IplSet : public SetReader
{
public:
	IplSet();
	~IplSet();

	void reset();
	void load(const char* pFilename);
	void loadBinary(const char* pFilename,bool SwapEndian);
	void save(const char* pFilename);
	void saveBinary(const char* pFilename, bool SwapEndian);
	void checkAllInstances(IdeSet& r_ideSet,atArray<atString>& r_Missing);
	void getUnusedObjects(IdeSet& r_ideSet,atArray<atString>& r_Unused);
	bool isObjectUsed(u32 objectHash);
	void checkUsage(IdeSet& r_ideSet,atArray<atString>& r_Usage,const atString& r_MapCheck);
	void checkUsageObject(u32 hashKey,const char* p_Name,IdeSet& r_ideSet,atArray<atString>& r_Usage,const atString& r_MapCheck);

	s32 getNumInstances() { return m_listInstanceNames.GetCount(); }
	atString& getInstanceName(s32 index) { return m_listInstanceNames[index]; }
	atString getInstanceTransform(s32 index);
	bool getInstanceIsLod(s32 index) { return m_listInstanceIsLod[index]; }
	s32 getInstanceBlock(s32 index);

	s32 getNumBlocks() { return m_listBlockDefs.GetCount(); }
	void getBlockName(s32 index,atString& r_blockname);
	void getBlockPoints(s32 index,atString& r_blockpoints);
	f32 getBlockArea(s32 index);

	void writeString(fiStream*,atString&);

private:
	void saveBinary4(const char* pFilename,bool SwapEndian);
	void saveBinary5(const char* pFilename, bool SwapEndian);
	void saveBinary6(const char* pFilename, bool SwapEndian);

	s32 m_nIplVersion;
	f32 m_fMaxLodDistance;
	atArray<atString,0,u32> m_listInstanceNames;
	atArray<atString,0,u32> m_listMiloInstanceNames;
	atArray<BoundDef,0,u32> m_listBoundDefs;
	atArray<InstanceDef,0,u32> m_listInstanceDefs;
	atArray<Instance5Def,0,u32> m_listInstance5Defs;
	atArray<MiloInstance6Def,0,u32> m_listMiloInstance6Defs;
	atArray<bool,0,u32> m_listInstanceIsLod;
	atArray<bool,0,u32> m_listMiloInstanceIsLod;
	atArray<ZoneDef> m_listZoneDefs;
	atArray<GarageDef> m_listGarageDefs;
	atArray<CarGenDef> m_listCarGenDefs;
	atArray<TimeCycDef> m_listTimeCycDefs;
	atArray<PathNodeDef> m_listPathNodeDefs;
	atArray<PathLinkDef> m_listPathLinkDefs;
	atArray<BlockDef> m_listBlockDefs;
	atArray<RootScriptDef> m_listRootScriptDefs;
	atArray<MiloPlusInstanceDef> m_listMiloPlusInstanceDefs;
	atArray<u32> m_listMiloPlusNameHashes;
	atArray<LodModifierDef> m_lodmodifiersDefs;
	atArray<SlowZoneDef> m_slowzoneDefs;
	atArray<PatrolNodeDef> m_listPatrolNodeDefs;
	atArray<PatrolLinkDef> m_listPatrolLinkDefs;
	atArray<LodChildDef> m_listLodChildDefs;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class GtaData
{
public:
	static void reset()
	{
		m_ideSet.reset();
		m_iplSet.reset();
	}

	static IdeSet m_ideSet;
	static IplSet m_iplSet;
};

#endif //RAGEBUILDER_GTA_H_
