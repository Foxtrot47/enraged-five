// ========================================================
// textureconversion.h
// Copyright (c) 2010 Rockstar North.  All Rights Reserved.
// ========================================================

#ifndef TEXTURECONVERSION_TEXTURECONVERSION_H
#define TEXTURECONVERSION_TEXTURECONVERSION_H

// rage includes
#include "atl/map.h"
#include "file/limits.h"
#include "parser/macros.h"
#include "vector/vector4.h"
#include "grcore/image.h"

// local headers
#include "texturefactory.h"
#include "../ragebuilder.h"

// STL headers
#include <map>

namespace rage {

class grcTexture;
class parTreeNode;

// ================================================================================================

class CTextureConversionImage
{
public:
	CTextureConversionImage(const char* filename = NULL, const char* usage = NULL) : m_filename(filename), m_usage(usage) {}

	atString m_filename;
	atString m_usage;

	PAR_SIMPLE_PARSABLE;
};

class CTextureConversionDebugParams // shared params for debugging (DO NOT USE IN PRODUCTION ASSETS!)
{
public:
	float m_mipFillAmount;          // amount to fade mips to fixed colours, useful for debugging mip levels
	int   m_mipFillStart;           // first mip index where debug fill starts
	int   m_mipFillBorderThickness; // 0 for no borders, -1 to fill entire mip, n>0 for n-texel borders
	bool  m_testLinearRamp;         // ..
	bool  m_testColourGradients;    // when testing image formats, it helps to have a known reference gradient
	bool  m_testColourGraph;        // ..

	PAR_SIMPLE_PARSABLE;
};

enum eTextureCompressor
{
	tc_DEVIL  ,
	tc_SQUISH ,
	tc_ATI    ,
	tc_ATI_EXE,
	tc_STB    ,
	tc_XG     ,
	tc_HYBRID ,
};

// ================================================================================================

class CTextureConversionParams
{
public:
	enum eTextureFormatUsage
	{
		tfu_ColourMap, // {r,g,b,a} are [0..1] representing any combination of colour/alpha/luminance/mask information
		tfu_NormalMap, // {r,g,b} are [-1..1] representing a xyz vector, but z will be reconstructed in the pixel shader, {a} is [0..1]
		tfu_VectorMap, // {r,g,b} are [-1..1] representing a xyz vector, z will not be reconstructed in the pixel shader, {a} is [0..1]
		tfu_CableMap , // special mode for cables
		tfu_TerrainBlendMap0, // special mode for terrain channel maps.
		tfu_TerrainBlendMap1, // special mode for terrain channel maps.
	};

	// tfu_NormalMap sets {b} channel to 1.0f in order to (slightly) improve compression quality

	enum eTextureFormatHint
	{
		tfh_Compressed           , // use DXT formats
		tfh_CompressedHighQuality, // use DXT formats, uses DXT5 "GAB1" swizzle for higher quality when appropriate
		tfh_Quantised            , // use 4 (or 5) bits per component for colour images
		tfh_8bpc                 , // use 8 bits per component for colour images
		tfh_16bpc                ,
		tfh_16bpc_float          ,
		tfh_32bpc_float          ,
	};

	enum eTextureFormat // for overriding to specific formats, should only be used for debugging (matches grcImage::Format exactly)
	{
		tf_UNKNOWN      ,
		tf_DXT1         ,
		tf_DXT3         ,
		tf_DXT5         ,
		tf_CTX1         ,
		tf_DXT3A        ,
		tf_DXT3A_1111   ,
		tf_DXT5A        ,
		tf_DXN          ,
		tf_BC6          ,
		tf_BC7          ,
		tf_A8R8G8B8     ,
		tf_A8B8G8R8     ,
		tf_A8           ,
		tf_L8           ,
		tf_A8L8         ,
		tf_A4R4G4B4     ,
		tf_A1R5G5B5     ,
		tf_R5G6B5       ,
		tf_R3G3B2       ,
		tf_A8R3G3B2     ,
		tf_A4L4         ,
		tf_A2R10G10B10  ,
		tf_A2B10G10R10  ,
		tf_A16B16G16R16 ,
		tf_G16R16       ,
		tf_L16          ,
		tf_A16B16G16R16F,
		tf_G16R16F      ,
		tf_R16F         ,
		tf_A32B32G32R32F,
		tf_G32R32F      ,
		tf_R32F         ,
		tf_D15S1        ,
		tf_D24S8        ,
		tf_D24FS8       ,
		tf_P4           ,
		tf_P8           ,
		tf_A8P8         ,
		tf_R8           ,
		tf_R16          ,
		tf_G8R8         ,
		tf_COUNT
	};

	enum eTextureAddress
	{
		ta_Wrap         ,
		ta_ClampToEdge  ,
		ta_ClampToBorder,
	};

	// note that CTextureConversionParams::eTextureCompressionHEM and CDXTCompressorParams::eCompressorHybridErrorMetric must match exactly
	enum eTextureCompressionHEM
	{
		tchem_Default  ,
		tchem_SumSumSqr, // err' = err + (dr^2 + dg^2 + db^2)
		tchem_SumSumAbs, // err' = err + (|dr| + |dg| + |db|)
		tchem_SumMaxAbs, // err' = err + max(|dr|, |dg|, |db|)
		tchem_MaxSumSqr, // err' = max(err, dr^2 + dg^2 + db^2)
		tchem_MaxSumAbs, // err' = max(err, |dr| + |dg| + |db|)
		tchem_MaxMaxAbs, // err' = max(err, max(|dr|, |dg|, |db|))
		tchem_GreenSat , // pick XG over ATI if block is "primarily green" .. doesn't seem to work well in all cases though, so don't use it
	};

	int                              m_FIRST_MEMBER;                     // DO NOT USE

	atString                         m_parent;                           // parent params
	atString                         m_exportWarning;                    // ..

	bool                             m_respectMips;                      // respect artist-generated mips

	bool                             m_imageSplitHD;                     // split top mip into separate _HD txd, and downsample less
	bool                             m_imageSplitHD2;                    // split top two mips into separate _HD txd, and downsample less
	int                              m_imageDownsampleScale;             // scaling factor to apply to image initially, image will be resized using simple linear downsampling
	int                              m_imageDownsampleScaleX;            // scaling factor in x dimension, or zero to use m_imageDownsampleScale
	int                              m_imageDownsampleScaleY;            // scaling factor in y dimension, or zero to use m_imageDownsampleScale
	float                            m_imageDownsampleScaleRel;          // relative scaling factor applied on top of m_imageDownsampleScale (typically specified in a non-parent TCP)
	float                            m_imageDownsampleScaleRelX;         // relative scaling factor in x dimension, or zero to use m_imageDownsampleScaleRel
	float                            m_imageDownsampleScaleRelY;         // relative scaling factor in y dimension, or zero to use m_imageDownsampleScaleRel
	int                              m_imageMaxSize;                     // maximum resolution of top-level image, if source is larger will be resized using simple linear downsampling
	int                              m_imageMaxSizeX;                    // maximum resolution of top-level image in x dimension, or zero to use m_imageMaxSize
	int                              m_imageMaxSizeY;                    // maximum resolution of top-level image in y dimension, or zero to use m_imageMaxSize
	int                              m_imageMaxMips;                     // maximum number of mips including top-level, or 0 to indicate full mip chain
	int                              m_imageMinMinMipSize_XENON;         // minimum allowable mip size computed as min(w,h) for xenon, or zero to use m_imageMinMinMipSize
	int                              m_imageMinMaxMipSize_XENON;         // minimum allowable mip size computed as max(w,h) for xenon, or zero to use m_imageMinMaxMipSize
	int                              m_imageMinMinMipSize;               // minimum allowable mip size computed as min(w,h), or zero to ignore
	int                              m_imageMinMaxMipSize;               // minimum allowable mip size computed as max(w,h), or zero to ignore (must be >= m_imageMinMinMipSize)
	int                              m_imageTopMipDiscard;               // number of top mips to discard (useful to make LOD textures match lower mips of HD textures)

	eTextureFormatUsage              m_texFormatUsage;                   // ..
	eTextureFormatHint               m_texFormatHint;                    // ..
	bool                             m_texFormatHintUseDXT3;             // ..
	bool                             m_texFormatHintUse555;              // ..
	bool                             m_texFormatHintHighQualityWithAlpha;// if the format hint is tfh_CompressedHighQuality and the texture uses four channels, use 'GARB' swizzle instead of 'RGBA'
	bool                             m_texFormatHintAllowCTX1ColourMaps; // enable CTX1 format for colour maps, disabled by default since the XG CTX1 compressor is problematic
	bool                             m_texFormatHintAllowCTX1NormalMaps; // enable CTX1 format for normal maps, disabled by default since the XG CTX1 compressor is problematic
	bool                             m_texFormatHintNoXenonFormats;      // hack to disable xenon-specific formats, even on xenon (useful for comparisons)
	eTextureFormat                   m_texFormatOverride;                // for overriding to specific formats, should probably only be used for debugging
	atString                         m_texFormatOverrideSwizzle;         //
	atArray<eTextureCompressor>      m_texCompressors;                   // texture compressors in order of preference
	bool                             m_texCompressionDither;             // ..
	Vector3                          m_texCompressionWeights;            // per-channel compression weights, not supported for all compressors
	bool                             m_texCompressionInvertRGB;          // experimental .. invert RGB before and after compression
	eTextureCompressionHEM           m_texCompressionHybridErrorMetric;  // hybrid error metric
	bool                             m_texCompressionHybridZeroSum;      // subtract average before computing dr,dg,db
	float                            m_texCompressionHybridDebugTintMin; // use debug colouring for hybrid compression
	float                            m_texCompressionHybridDebugTintMax; // use debug colouring for hybrid compression
	eTextureAddress                  m_texAddr;                          // used for mip generation/filtering
	Vector4                          m_texAddrBorderColour;              // typically zero=[0,0,0,0]

	bool                             m_preprocessAfterDownsample;        // old behaviour of calling PreprocessImage after initial downsample

	bool                             m_sRGBInputR;                       // apply sRGB to red   channel of top-level image (these do not affect texture sRGB flags)
	bool                             m_sRGBInputG;                       // apply sRGB to green channel of top-level image
	bool                             m_sRGBInputB;                       // apply sRGB to blue  channel of top-level image
	bool                             m_sRGBInputA;                       // apply sRGB to alpha channel of top-level image

	Color32                          m_colourKey;                        // don't do sRGB input conversion or finalgamma on colours which match this colour exactly (unless m_colourKey is zero)

	Vector4                          m_downsampleGammaExponent;          // ..
	Vector4                          m_downsampleFilterAmount;           // ..
	atArray<float>                   m_downsampleFilterCoefficients;     // ..

	Vector4                          m_mipGammaExponent;                 // when creating mips, colours will be raised to 1/2^mipGammaExponent power before downsampling and 2^mipGammaExponent power after
	Vector4                          m_mipFilterAmount;                  // ..
	atArray<float>                   m_mipFilterCoefficients;            // ..
	Vector4                          m_mipFilterSharpenAmount;           // simple unsharp-mask amount applied to each mipmap (blur is [1,1,1],[1,1,1],[1,1,1]/9)
	atArray<float>                   m_mipFilterSharpen;                 // ..
	bool                             m_mipFilterSharpenToLastMip;        // if true, m_mipFilterSharpen array ends at last mip instead of starting at first mip
	Vector3                          m_mipFilterDesaturateAmount;        // simple saturation amount applied to each mipmap
	atArray<float>                   m_mipFilterDesaturate;              // ..
	bool                             m_mipFilterDesaturateToLastMip;     // if true, m_mipFilterDesaturate array ends at last mip instead of starting at first mip

	Vector4                          m_finalGammaExponent;               // colours will be raised to 1/2^finalGammaExponent power before saving texture
	Vector4                          m_finalMultiplier;                  // ..

	bool                             m_mipFadeColourAuto;                // use the average colour of the top-level image
	Vector4                          m_mipFadeColour;                    // colour to fade to (e.g. for detail maps)
	Vector4                          m_mipFadeAmount;                    // fading amount, 0 indicates no fading, 1 indicates full fading
	atArray<float>                   m_mipFade;                          // ..
	Vector4                          m_mipFadeThresholdMin;              // min threshold to apply mip fading
	Vector4                          m_mipFadeThresholdMax;              // max threshold to apply mip fading
	bool                             m_mipFadeToLastMip;                 // if true, m_mipFade array ends at last mip instead of starting at first mip

	float                            m_mipBlendAmountStart;              // mip blend will be used if m_mipBlendAmountStart and m_mipBlendAmountEnd are not both zero
	float                            m_mipBlendAmountEnd;                // ..
	int                              m_mipBlendIndexStart;               // ..
	int                              m_mipBlendIndexEnd;                 // ..

	float                            m_alphaCoverageCompensationReference;               // if 0, use default 0.5f
	float                            m_alphaCoverageCompensationFactor;                  // exponent applied to scale factor used to correct alpha coverage � 0=no effect, 1=full effect
	bool                             m_alphaCoverageCompensationAllowScaleByLessThanOne; // normally we don�t want to apply a scale < 1, but we can enable this if needed
	int                              m_alphaCoverageCompensationIterations;              // if 0, use default 10
	atArray<float>                   m_alphaScale;                                       // optional per-mip alpha scale
	bool                             m_alphaScaleToLastMip;                              // ..

	bool                             m_normalMapGlossInAlpha;            // ..
	bool                             m_normalMapGlossUseOldPWLPacking;   // ..
	bool                             m_normalMapGlossUseAverageLength;   // ..
	atArray<int>                     m_normalMapGlossSrcFilterRadius;    // expand region of source mip
	atArray<int>                     m_normalMapGlossDstFilterRadius;    // pre-filter radius of dest mip
	float                            m_normalMapGlossSpecularMin;        // if 0, use 1 (default)
	float                            m_normalMapGlossSpecularMax;        // if 0, use 8192 (default)
	float                            m_normalMapGlossSpecularPower;      // desired specular power
	atArray<float>                   m_normalMapGlossSpecPowScale;       // scales specular power per mip, if desired
	bool                             m_normalMapGlossSpecPowScaleToLastMip;
	atArray<float>                   m_normalMapGlossVarianceScale;
	bool                             m_normalMapGlossVarianceScaleToLastMip;
	bool                             m_normalMapGlossStoreVarianceAlpha; // stores variance instead of gloss in alpha)
	atArray<float>                   m_normalMapGlossAlphaBlur;          // per-mip blurring of alpha channel
	int                              m_normalMapGlossAlphaBlurRadius;    // ..
	int                              m_normalMapGlossAlphaBlurPasses;    // ..
	float                            m_normalMapGlossAlphaBlurMinFactor; // how much to apply min factor to alpha blur during each pass 
	bool                             m_normalMapGlossAlphaOnly; 		 // forces rgb=0 so that texture will be monochrome
	float                            m_normalMapGlossAlphaExponent;      // if 0, use 0.5 (default)

	float                            m_normalMapSharpenAmount;           // ..
	atArray<float>                   m_normalMapSharpen;                 // ..
	bool                             m_normalMapSharpenToLastMip;        // if true, m_normalMapSharpen array ends at last mip instead of starting at first mip
	bool                             m_normalMapSharpenAlphaMask;        // ..

	bool                             m_applyBorderColourR;               // apply border colour to border texels (red channel)
	bool                             m_applyBorderColourG;               // apply border colour to border texels (green channel)
	bool                             m_applyBorderColourB;               // apply border colour to border texels (blue channel)
	bool                             m_applyBorderColourA;               // apply border colour to border texels (alpha channel)

	float                            m_alphaThreshold;                   // if threshold > 0, any texel with alpha < threshold will have alpha set to zero (useful for DXT1)
	bool                             m_alphaPremultiply;                 // RGB is multiplied by alpha before saving texture

	bool                             m_distanceField;                    // red channel is a boolean in/out map
	bool                             m_distanceFieldUnsigned;            // compute unsigned distance field (default is signed)
	float                            m_distanceFieldRange;               // range, in pixels of the high-res image

	bool                             m_sRGB;                             // specifies texture should set sRGB flag (does NOT affect mipmap processing etc.)
	bool                             m_sRGB_raw;                         // debugging flag, turns on sRGB at the texture level without applying inverse sRGB to the pixel data
	bool                             m_linear;                           // specifies texture should be forced to linear (not tiled)
	bool                             m_linear_PS3;                       // linear on PS3 (but not necessarily other platforms)
	bool                             m_linear_XENON;                     // linear on XENON (but not necessarily other platforms)
	bool                             m_sysmem;                           // specifies texture should be in system memory on PS3 (dynamic)

	bool                             m_doNotOptimise;                    // do not change DXT3 or DXT5 to DXT1 or reduce to 4x4 if the texture is "flat" (i.e. there is no variation), this is needed for script rt's

	bool                             m_CTX1_from_DXT1;                   // shameful experimental hackery

	atArray<CTextureConversionImage> m_imageFiles;                       // ..

	CTextureConversionDebugParams    m_debugParams;                      // ..

	// ================================================================= //

	const char*                      m_swizzle;                          // local member, not part of parcogen definition
	u8                               m_conversionFlags;                  // local member, not part of parcogen definition
	u16                              m_templateType;                     // local member, not part of parcogen definition

	int                              m_LAST_MEMBER;                      // DO NOT USE

	// ================================================================= //

	// DHM FIX ME: can we not just use Ragebuilder's platform here?
	//  Ragebuilder::GetPlatform()
	static char sm_platform;

	CTextureConversionParams();
	virtual ~CTextureConversionParams() {} // force vtable

	// DHM FIX ME: this should use Ragebuilder::GetPlatform instead!
	static void SetPlatform( RageBuilder::PLATFORM platform );

	static const char* GetExtension() { return "tcp"; }
	static void ResolvePath(const char* path, char* resolvedPath, bool bPreload);
	static CTextureConversionParams* Load(const char* path, bool bPreload, CTextureConversionParams** obj = NULL);
	
	static void ClearCache( );

	bool Save(const char* path) const;
	void Release();

	void FinaliseConversionParams();

	bool OptimiseCompressed( grcImage*& image ) const;
	bool Process( grcImage::ImageList& images, const char* filename );

protected:
	void PreLoad(parTreeNode* pNode);

	PAR_PARSABLE;

private:
	static std::map<rage::u32, CTextureConversionParams*> ms_CachedTextureSpecifications;
};

class CTextureConversionSourceTexture
{
public:
	atString m_pathname;

	PAR_SIMPLE_PARSABLE;
};


class CTextureConversionResourceTexture
{
public:
	enum eUsage
	{
		usage_Default,
		usage_Diffuse,
		usage_MipBlend,
		usage_Source
	};

	atString m_pathname;
	eUsage m_usage;
	atArray<CTextureConversionSourceTexture> m_sourceTextures;

	PAR_SIMPLE_PARSABLE;
};

// ================================================================================================
// This is a specialised version of the specification that enforces a 'template' to be used
// as a starting point. There will eventually be one of these for every single texture in the game

class CTextureConversionSpecification : public CTextureConversionParams
{
public:
	CTextureConversionSpecification();
	virtual ~CTextureConversionSpecification() {} // force vtable

	static const char* GetExtension() { return "tcs"; }
	static CTextureConversionSpecification* Load(const char* path, bool bPreload);

	static void ClearCache( );

	bool Save(const char* path) const;
	void Release();	
	bool LookupProcessedImage( const char* filename, grcImage::ImageList& images );

	static void Begin( char platform, const char* uniqueContainerPath, CTextureConversionSpecification* spec = NULL );
	static void End();

	static grcImage* LoadImageFormat( const char* path );
	static bool CustomLoadImageFunc(const char* path, grcImage::ImageList& outputs, void** outParams);
	static bool CustomProcessTextureFunc(grcTexture* texture, const void* params);

	// grmShaderGroup CreateTextureDictionary override.
	static pgDictionary<grcTexture>* CustomCreateTextureDictionary( const char** inputs, 
		int inputCount, bool stripPath, grcTextureFactory::TextureCreateParams** params );

	bool m_skipProcessing; // skip processing, convert DDS to texture directly (preserving format, mips, etc.)
	atArray<CTextureConversionResourceTexture> m_resourceTextures; // details of textures to be resourced

	static char sm_uniqueContainerPath[RAGE_MAX_PATH];
	static CTextureConversionSpecification* sm_customLoadImageSpec;// To prevent unnecessary duplicate loading (and associated issues)

	PAR_PARSABLE;

private:

	static int sm_stackCount;
	static std::map<rage::u32, CTextureConversionSpecification*> ms_CachedTextureSpecifications;
};

} // namespace rage

#endif // TEXTURECONVERSION_TEXTURECONVERSION_H
