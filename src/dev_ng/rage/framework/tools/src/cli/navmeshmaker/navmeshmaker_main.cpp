#define LARGE_BUDDY_HEAP

#if __WIN32PC

#define SIMPLE_HEAP_SIZE		(200*1024)
#define SIMPLE_PHYSICAL_SIZE	(352*1024)

#else

#error "Not supported for this platform.")

#endif	// __WIN32PC


#include "fwnavgen/toolnavmesh.h"
#include "fwnavgen/config.h"
#include "navmeshmaker_main.h"
#include "system/buddyheap.h"
#include "system/buddyallocator.h"
#include "diag/channel.h"


#if 1 //__ASSERT
RAGE_DEFINE_CHANNEL(net, rage::DIAG_SEVERITY_DEBUG3, rage::DIAG_SEVERITY_WARNING, rage::DIAG_SEVERITY_ASSERT)
#endif

namespace rage
{
	int NavMeshMakerMain();

	// This gets used by 'system/main.cpp' as the size of pgBaseKnownReferencePool.
/*
#ifdef PGKNOWNREFPOOLSIZE
	int g_KnownRefPoolSize = PGKNOWNREFPOOLSIZE;
#else
	int g_KnownRefPoolSize = -1;
#endif
*/

//	u32* g_DebugData;
}

//int Main()
//{
//	return 1;
//}

//int main(int argc, char *argv[])
int Main()
{
//	sysParam::Init(argc, argv);

//	size_t simplePhysicalSize = 1024*1024;
//	static u8 *Workspace = (u8*) sysMemVirtualAllocate(COMPUTE_BUDDYHEAP_WORKSPACE_SIZE((simplePhysicalSize<<10)/g_rscPhysicalLeafSize));
//	static sysMemBuddyAllocator physicalAllocator(sysMemPhysicalAllocate((simplePhysicalSize) << 10),g_rscPhysicalLeafSize,(simplePhysicalSize<<10)/g_rscPhysicalLeafSize,Workspace);
//	theAllocator.AddAllocator(physicalAllocator); // Add the DEBUG allocator

	return rage::NavMeshMakerMain();
}

/*
void* operator new[](unsigned int h, unsigned int a)
{
	return :: operator new( h );
}
*/







