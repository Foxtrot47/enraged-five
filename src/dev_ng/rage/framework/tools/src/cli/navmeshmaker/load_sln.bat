@echo off

setlocal
pushd "%~dp0"

set RAGE_ASSET_ROOT=x:\rage\assets\
set RAGE_DIR=x:\gta5\src\dev\rage
set RS_BUILDBRANCH=x:\gta5\build\dev
set RS_CODEBRANCH=x:\gta5\src\dev
set RS_PROJECT=gta5
set RS_PROJROOT=x:\gta5
set RS_TOOLSROOT=X:\gta5\tools
set RUBYLIB=X:\gta5\tools\lib
set RUBYOPT=rubygems

REM Detect if current SDK is either unset or older than 350 and switch.
REM IF NOT EXIST %SCE_PS3_ROOT%\info\old\340.001\Bugfix_SDK_e.txt set SCE_PS3_ROOT=X:/ps3sdk/dev/usr/local/430_001/cell

REM ECHO LOAD_SLN ENVIRONMENT
ECHO RAGE_DIR:               %RAGE_DIR%
REM ECHO SCE_PS3_ROOT:           %SCE_PS3_ROOT%
REM ECHO END LOAD_SLN ENVIRONMENT

start "" %cd%\navmeshmaker.sln

popd
