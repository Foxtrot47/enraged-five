﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using SIO = System.IO;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.Configuration;
using RSG.Pipeline.Core;
using RSG.Pipeline.Content;
using RSG.SceneXml;
using System.IO.Compression;
using RSG.Platform;
using RSG.Base.Extensions;

namespace TestLightTexturePacking
{
    class Program
    {
        static int Main(string[] args)
        {
            LogFactory.Initialize();
            IUniversalLog log = LogFactory.CreateUniversalLog("TestLightTexturePacking");
            LogFactory.CreateUniversalLogFile(log);
            int result = Constants.Exit_Success;

            try
            {
                IConfig config = ConfigFactory.CreateConfig();

                ProcessorCollection processorCollection = new ProcessorCollection(config);
                IContentTree contentTree = Factory.CreateTree(config.Project.DefaultBranch, processorCollection);

                ContentTreeHelper contentTreeHelper = new ContentTreeHelper(contentTree, processorCollection);

//                SceneCollection scenes = new SceneCollection(config.Project.DefaultBranch, contentTree, processorCollection);

                string[] mapSceneXMLNodes = SIO.Directory.GetFiles(config.Project.DefaultBranch.Export, "*.xml",SIO.SearchOption.AllDirectories);
                foreach (string xmlFilepath in mapSceneXMLNodes)
                {
                    string zipFilePath = SIO.Path.ChangeExtension(xmlFilepath, ".zip");

                    if (!SIO.File.Exists(xmlFilepath) || !SIO.File.Exists(zipFilePath))
                    {
                        log.Warning("Couln't find either SceneXML file {0} or according export zip {1}.", xmlFilepath, zipFilePath);
                        continue;
                    }

                    log.Message("loading SceneXML file {0}", xmlFilepath);
                    Scene scene = new Scene(contentTree, contentTreeHelper, xmlFilepath, LoadOptions.All, false);

                    log.Message("loading export zip file {0}", zipFilePath);
                    using (ZipArchive sceneZip = ZipFile.Open(zipFilePath, ZipArchiveMode.Read))
                    {
                        foreach (TargetObjectDef obj in scene.Walk(RSG.SceneXml.Scene.WalkMode.BreadthFirst))
                        {
                            string texMapName = obj.GetParameter("LightShadowMap", "");
                            if (!obj.Is2dfxLightEffect() || texMapName.Length<=0)
                                continue;

                            ObjectDef exportRoot = obj.ObjectDef.GetExportRoot();
                            if (null == exportRoot ||
                                    exportRoot.IsRefObject() ||
                                    exportRoot.GetAttribute("Dont Export", false) ||
                                    exportRoot.GetAttribute("Dont Add To IPL", false))
                            {
                                continue;
                            }

                            string drawableName = exportRoot.Name;
                            drawableName = drawableName.Replace("_frag_", "");
                            drawableName = drawableName.Replace("_anim_", "");

                            string drawableFilename = drawableName + "." + FileTypeUtils.GetExportExtension(FileType.Drawable) + ".zip";
                            string fragmentFilename = drawableName + "." + FileTypeUtils.GetExportExtension(FileType.Fragment) + ".zip";
                            ZipArchiveEntry drawableEntry = sceneZip.Entries.First(entry =>
                                                                0 == String.Compare(drawableFilename, entry.Name, true) ||
                                                                0 == String.Compare(fragmentFilename, entry.Name, true) );

                            if (null == drawableEntry)
                            {
                                log.Error("Couldn't find drawable entry for light drawable {0} in zip archive!", drawableName);
                                continue;
                            }

                            using (ZipArchive drawableStream = new ZipArchive(drawableEntry.Open()))
                            {
                                if (!drawableStream.Entries.Any(entry => FileType.MaterialTemplateExport.GetRegexValue().IsMatch(entry.Name)))
                                    log.ErrorCtx(SIO.Path.GetFileNameWithoutExtension(xmlFilepath),
                                        "Light projection texture not packed with drawable!\n"+
                                                                "\nTexture: {0}"+
                                                                "\nDrawable: {1}"+
                                                                "\nScene: {2}", 
                                                                texMapName,
                                                                drawableName,
                                                                xmlFilepath);
                            }
                        }
                    }                    
                }
                //Directory exportDirs = new Directory config.Project.DefaultBranch.Export

                Console.WriteLine(config.Studios.ThisStudio.Name);
            }
            catch (Exception ex)
            {
                log.ToolException(ex, "Unhandled exception.");
                result = Constants.Exit_Failure;
            }

            if (LogFactory.HasError())
                result = Constants.Exit_Failure;

            return (result);
        }
    }
}
