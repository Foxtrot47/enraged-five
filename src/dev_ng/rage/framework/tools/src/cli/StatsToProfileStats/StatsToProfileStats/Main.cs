﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Rockstar.Ros;

namespace Stats_Uploader
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string outputFile = "";
                string platform = "";

                for (int i = 0; i < args.Length; i++)
                {
                    if (!outputFile.Equals("") && !platform.Equals(""))
                        break;

                    if (args[i].ToString() == "-out")
                    {
                        outputFile = args[i+1].ToString();
                    }
                    else if (args[i].ToString() == "-platform")
                    {
                        platform = args[i+1].ToString();
                    }
                }

                if (outputFile.Equals("") || platform.Equals(""))
                {
                    Console.Write("\n Usage: -in <stats_file_name.xml> -out <profile_stats_filename.csv> -platform <psn>");
                }
                else
                {
                    MetadataParser xmlParser = new MetadataParser();

                    for (int i = 0; i < args.Length; i++)
                    {
                        if (args[i].ToString() == "-in")
                        {
                            string infile = args[i + 1].ToString();

                            //Read in xml Files
                            if (File.Exists(infile))
                            {
                                Console.Write("\n Input File - " + infile);

                                if (!xmlParser.ReadInXml(infile, platform))
                                {
                                    throw new Exception("Error parsing file " + infile);
                                }
                            }
                        }
                    }

                    // Write out csv file
                    FileInfo f = new FileInfo(outputFile);
                    StreamWriter Tex = f.CreateText();
                    xmlParser.WriteToCsv(Tex);
                    Tex.Close();
                }
            }
            catch (Exception ex)
            {
                Console.Write("\n");
                Console.Write("\n");
                Console.Write("\n Error - " + ex.ToString());
            }
        }
    }
}
