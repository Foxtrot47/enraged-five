﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using RSG.Base.Collections;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Pipeline.Core;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.Platform;
using RSG.Pipeline.Services.Platform.Manifest;

namespace RSG.Pipeline.RpfCreate
{

    /// <summary>
    /// RPF creation utility; wrapper around Ragebuilder's RPF creation features.
    /// </summary>
    /// This wrapper was created to neatly abstract the manifest; ensuring that
    /// its only added to the RPF if it contains data.
    /// 
    class Program
    {
        #region Constants
        private static readonly String LOG_CTX = "RPF Create";
        private static readonly String OPTION_TASKNAME = "taskname";
        private static readonly String OPTION_OUTPUT = "output";
        private static readonly String OPTION_FILELIST = "filelist";
        private static readonly String OPTION_MANIFEST = "manifest";
        private static readonly String OPTION_PLATFORM = "platform";
        private static readonly String OPTION_LOGFILE = "logfile";
        private static readonly String OPTION_METADATA = "metadata";
        private static readonly String ARG_PACKRPF_SCRIPT = "$(toolslib)/util/ragebuilder/build_rpf.rbs";
        #endregion // Constants

        /// <summary>
        /// RPF constructor entry-point.
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        [STAThread]
        static int Main(String[] args)
        {
            // Merge our options with the default pipeline command options.
            // Define our executable options.  Stick with the long options
            // (e.g. --opt) rather than the single character versions.
            LongOption[] opts = new LongOption[] {
                new LongOption(OPTION_TASKNAME, LongOption.ArgType.Required,
                    "XGE taskname; echoed to TTY for XGE error log parsing in AP3."),
                new LongOption(OPTION_OUTPUT, LongOption.ArgType.Required, 
                    "Output RPF file (.rpf)."),
                new LongOption(OPTION_FILELIST, LongOption.ArgType.Required,
                    "Input file containing sorted input file mapping."),
                new LongOption(OPTION_MANIFEST, LongOption.ArgType.Required,
                    "Input manifest file (IMF, target file only added if it contains data)."),
                new LongOption(OPTION_PLATFORM, LongOption.ArgType.Required,
                    "Ragebuilder platform string (e.g. ps3, xenon)."),
                new LongOption(OPTION_LOGFILE, LongOption.ArgType.Required,
                    "Ragebuilder log filename."),
                new LongOption(OPTION_METADATA, LongOption.ArgType.Required,
                    "Metadata directory for ITYP asset sorting (otherwise default sort applied)."),
            };
            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            IUniversalLog log = LogFactory.CreateUniversalLog("RpfManifest");
            CommandOptions options = new CommandOptions(args, opts);

            // Output task name for AP3 XGE logging compatibility.
            if (options.ContainsOption(OPTION_TASKNAME))
            {
                Console.WriteLine("TASK: {0}", (String)options[OPTION_TASKNAME]);
                Console.Out.Flush();
            }
            
            if (options.ShowHelp)
            {
                Console.WriteLine(options.GetUsage());
                return (Constants.Exit_Failure);
            }

            if (!options.ContainsOption(OPTION_OUTPUT) ||
                String.IsNullOrWhiteSpace((String)options[OPTION_OUTPUT]))
            {
                log.ErrorCtx(LOG_CTX, "RPF output file not specified or invalid format.");
                return (Constants.Exit_Failure);
            }
            if (!options.ContainsOption(OPTION_FILELIST) ||
                String.IsNullOrWhiteSpace((String)options[OPTION_FILELIST]) ||
                !File.Exists((String)options[OPTION_FILELIST]))
            {
                log.ErrorCtx(LOG_CTX, "RPF filelist file not specified or does not exist.");
                return (Constants.Exit_Failure);
            }

            ITarget target = null;
            if (options.ContainsOption(OPTION_PLATFORM) &&
                !(String.IsNullOrWhiteSpace((String)options[OPTION_PLATFORM])))
            {
                String platformStr = (String)options[OPTION_PLATFORM];
                RSG.Platform.Platform platform =
                    RSG.Platform.PlatformUtils.PlatformFromString(platformStr);
                if (!options.Branch.Targets.ContainsKey(platform))
                {
                    log.ErrorCtx(LOG_CTX, "Platform target '{0}' not found.  Aborting.",
                        platformStr);
                    return (Constants.Exit_Failure);
                }
                else
                {
                    log.MessageCtx(LOG_CTX, "Platform target set: {0}.", platform);
                    target = options.Branch.Targets[platform];
                }
            }
            else
            {
                log.ErrorCtx(LOG_CTX, "No platform specified for RPF construction.  Aborting.");
                return (Constants.Exit_Failure);
            }

            int exit_code = Constants.Exit_Success;
            try
            {
                // Parse our options.
                String manifestFilename = String.Empty;
                if (options.ContainsOption(OPTION_MANIFEST))
                    manifestFilename = (String)options[OPTION_MANIFEST];
                bool manifestHasData = false;
                if (File.Exists(manifestFilename))
                {
                    ManifestFile manifest = new ManifestFile(log, manifestFilename);
                    manifestHasData = manifest.HasData();
                }
                else
                {
                    log.WarningCtx(LOG_CTX, "Manifest file does not exist: {0}.", 
                        manifestFilename);
                }

                // The entire point in this wrapper is to nicely abstract adding
                // the optional manifest file so lets handle that before invoking
                // Ragebuilder to do the packing.
                String rpfFilename = (String)options[OPTION_OUTPUT];
                String filelistFilename = (String)options[OPTION_FILELIST];
                String logFilename = (String)options[OPTION_LOGFILE];
                String metadataDirectory = null; 
                if (options.ContainsOption(OPTION_METADATA))
                    metadataDirectory = (String)options[OPTION_METADATA];
                            
                if (options.ContainsOption(OPTION_MANIFEST))
                {
                    String platformManifestFilename =
                        PlatformPathConversion.ConvertFilenameToPlatform(target, manifestFilename, false);
                    if (manifestHasData)
                    {
                        log.MessageCtx(LOG_CTX, "Manifest file {0} contains data; appending {1} to RPF.",
                            manifestFilename, platformManifestFilename);
                        // All manifest files are called "_manifest"
                        String destName = String.Format("_manifest{0}",
                            Path.GetExtension(platformManifestFilename));
                        // Flo 13.11.2013 : AppendAllText does not add a new line
                        // forcing the new line to prevent appending twice and generating a wrong last line as in B*1685952
                        File.AppendAllText(filelistFilename,
                            String.Format("{0}{1}\t{2}", Environment.NewLine , platformManifestFilename, destName));
                    }
                    else
                    {
                        log.MessageCtx(LOG_CTX, "Manifest file {0} does not contain data.  Ignoring.",
                            manifestFilename);
                    }
                }
                
                // DHM 2013/07/13 - we can remove the Ragebuilder wildcard expansion
                // We expand here so the RPF sorting algorithm works.
                IEnumerable<Pair<String, String>> fileList = ReadFileListAndEvalWildcards(log, filelistFilename);
                IEnumerable<Pair<String, String>> sortedFileList;
                String sortedFileListFilename = Path.ChangeExtension(filelistFilename, ".sorted.txt");
                RpfSort.SortFileList(log, fileList, out sortedFileList, metadataDirectory);
                WriteFileList(sortedFileList, sortedFileListFilename);

                log.MessageCtx(LOG_CTX, "Packing RPF {0}.", rpfFilename);
                Directory.CreateDirectory(Path.GetDirectoryName(rpfFilename));
                exit_code = PackRpfDirectlyWithRagebuilder(log, target,
                    rpfFilename, sortedFileListFilename, logFilename);
                if (exit_code != Constants.Exit_Success)
                {
                    log.ErrorCtx(
                        LOG_CTX,
                        "Unable to create RPF at '{0}' via RAGEbuilder (exit code: {1}).  Is the file locked?  Is the game running?",
                        rpfFilename,
                        exit_code);
                }

                String rpfCreateProcessorXml = Path.Combine(options.Branch.Project.Config.ToolsConfig,
                    "processors", "RSG.Pipeline.Processor.Platform.RpfCreateProcessor.xml");
                if (File.Exists(rpfCreateProcessorXml))
                {
                    IDictionary<String, Object> rpfCreateParams = new Dictionary<String, Object>();
                    if (RSG.Base.Xml.Parameters.Load(rpfCreateProcessorXml, ref rpfCreateParams))
                    {
                        if (rpfCreateParams.ContainsKey("Delete Temp Files") &&
                            (bool)rpfCreateParams["Delete Temp Files"])
                            File.Delete(sortedFileListFilename);
                    }                    
                }
            }
            catch (Exception ex)
            {
                log.ToolExceptionCtx(LOG_CTX, ex, "Unhandled exception during RPF creation.");
                exit_code = Constants.Exit_Failure;
            }

            return (exit_code);
        }

        #region Private Methods
        /// <summary>
        /// Read filelist.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        private static IEnumerable<Pair<String, String>> ReadFileListAndEvalWildcards(IUniversalLog log, String filename)
        {
            List<Pair<String, String>> fileList = new List<Pair<String, String>>();
            char[] splitChars = new char[] {',' ,'\t', ' '};
            string[] lines = File.ReadAllLines(filename);
            foreach (String line in lines)
            {
                // Flo 13.11.2013: workaround because of the new line added when we append the manifest (see other comment, same date)
                // because of B*1685952
                if(string.IsNullOrWhiteSpace(line))
                    continue;

                String[] entries = line.Split(splitChars, StringSplitOptions.RemoveEmptyEntries);
                Debug.Assert(2 == entries.Length);
                
                if (2 != entries.Length)
                {
                    log.ErrorCtx(LOG_CTX, "Invalid filename: '{0}'.  Does it contain non-standard characters?", line);
                    continue; // Skip.
                }
                    
                // Expand wildcard entries.
                if (entries[0].Contains("*"))
                {
                    // Wildcard expansion required.
                    String entryDirectory = Path.GetDirectoryName(entries[0]);
                    String entryWildcard = Path.GetFileName(entries[0]);
                    if (Directory.Exists(entryDirectory))
                    {
                        String[] files = Directory.GetFiles(entryDirectory, entryWildcard);
                        files.ForEach(f => fileList.Add(new Pair<String, String>(f, Path.GetFileName(f))));
                    }
                    else
                    {
                        log.MessageCtx(LOG_CTX, "Directory not found for wildcard expansion.  Not neccessarily a problem because of the Prebuild up-front evaluation required.");
                    }
                }
                else
                {
                    // Single entry added; no wildcard expansion required.
                    fileList.Add(new Pair<String, String>(entries[0], entries[1]));
                }
            }
            return (fileList);
        }

        /// <summary>
        /// Write filelist to disk.
        /// </summary>
        /// <param name="fileList"></param>
        /// <param name="filename"></param>
        private static void WriteFileList(IEnumerable<Pair<String, String>> fileList, String filename)
        {
            using (FileStream fs = new FileStream(filename, FileMode.Create, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.ASCII))
                {
                    foreach (Pair<String, String> filePair in fileList)
                        sw.WriteLine("{0}\t{1}", filePair.First, filePair.Second);
                }
            }
        }

        /// <summary>
        /// Invoke Ragebuilder directly to build an RPF.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="target"></param>
        /// <param name="rpfFilename"></param>
        /// <param name="filelistFilename"></param>
        /// <returns></returns>
        private static int PackRpfDirectlyWithRagebuilder(IUniversalLog log, 
            ITarget target, String rpfFilename, String filelistFilename,
            String logfileFilename)
        {
            StringBuilder arguments = new StringBuilder();
            arguments.Append(GetRagebuilderPackRpfArguments(target));
            arguments.Append(" ");
            arguments.AppendFormat("-platform {0} ", RSG.Platform.PlatformUtils.PlatformToRagebuilderPlatform(target.Platform));
            arguments.AppendFormat("-filelist {0} ", filelistFilename);
            arguments.AppendFormat("-output {0} ", rpfFilename);
            arguments.AppendFormat("-logfile {0} ", logfileFilename);

            String exe = target.GetRagebuilderExecutable();
            Command process = new Command(exe, arguments.ToString());

            log.DebugCtx(LOG_CTX, "Command line: \"{0} {1}\".", exe, arguments.ToString());
            int result = process.Run(log);

            return (result);
        }

        /// <summary>
        /// Return Ragebuilder arguments string for building an RPF.
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        private static String GetRagebuilderPackRpfArguments(ITarget target)
        {
            IBranch branch = target.Branch;
            RSG.Platform.Platform platform = target.Platform;
            String buildDir = Path.GetFullPath(branch.Environment.Subst("$(build)"));
            String shadersDir = Path.GetFullPath(branch.Environment.Subst("$(shaders)"));
            String shadersDbDir = Path.GetFullPath(branch.Environment.Subst(Path.Combine("$(shaders)", "db")));

            StringBuilder parameters = new StringBuilder();
            parameters.AppendFormat("{0} ", branch.Environment.Subst(ARG_PACKRPF_SCRIPT));
            parameters.Append("-nopopups ");
            parameters.Append("-nominidump ");
            parameters.AppendFormat("-build {0} ", buildDir);
            parameters.AppendFormat("-shader {0} ", shadersDir);
            parameters.AppendFormat("-shaderdb {0} ", shadersDbDir);

            String conversionToolArgs = branch.PlatformConversionTools[platform].Arguments;
            conversionToolArgs = branch.Environment.Subst(conversionToolArgs);

            return (parameters.ToString());
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.RpfCreate namespace
