using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;
using RSG.Base;
using RSG.Base.Logging;
using RSG.SourceControl.Perforce;
using RSG.Base.Configuration;

namespace dialog
{

    /// <summary>
    /// 
    /// </summary>
    class Program
    {
        #region Constants
        private static readonly int EXIT_UNDEFINED = -1;
        private static readonly int EXIT_OK = 0;
        private static readonly int EXIT_YES = 0;
        private static readonly int EXIT_NO = 1;
        private static readonly int EXIT_CANCEL = 2;
        private static readonly int EXIT_ABORT = 3;
        private static readonly int EXIT_IGNORE = 4;
        private static readonly int EXIT_RETRY = 5;
        // Help/Usage options
        private static readonly String CMD_HELP = "help";
        private static readonly String CMD_HELP_SHORT = "h";
        // Version options
        private static readonly String CMD_VERSION = "version";
        private static readonly String CMD_VERSION_SHORT = "v";
        // Special options
        private static readonly String CMD_SPECIAL = "special";
        private static readonly String CMD_SPECIAL_P4LOGIN = "p4login";
        private static readonly String CMD_SPECIAL_EXCEPTION = "exception";
        private static readonly String CMD_SPECIAL_ASSET_DEPENDENCIES = "asset_dependencies";
        // Button options
        private static readonly String CMD_BUTTONS = "buttons";
        private static readonly String CMD_BUTTONS_SHORT = "b";
        // Icon options
        private static readonly String CMD_ICON = "icon";
        private static readonly String CMD_ICON_SHORT = "i";
        #endregion // Constants

        [STAThread]
        static int Main(String[] args)
        {
            ConsoleLog log = new ConsoleLog();
            cCommandLineParser parser = new cCommandLineParser(args);
            DialogResult dlgResult = ParseOptions(parser);

            switch (dlgResult)
            {
                case DialogResult.Abort:
                    return (EXIT_ABORT);
                case DialogResult.Cancel:
                    return (EXIT_CANCEL);
                case DialogResult.Ignore:
                    return (EXIT_IGNORE);
                case DialogResult.Yes:
                    return (EXIT_YES);
                case DialogResult.No:
                    return (EXIT_NO);
                case DialogResult.OK:
                    return (EXIT_OK);
                case DialogResult.Retry:
                    return (EXIT_RETRY);
                case DialogResult.None:
                default:
                    return (EXIT_UNDEFINED);
            }
        }

        #region Command Option Parsing Methods
        /// <summary>
        /// Parse any command line options we have been specified
        /// </summary>
        /// <param name="parser"></param>
        static DialogResult ParseOptions(cCommandLineParser parser)
        {
            if (null == parser)
                throw new ArgumentNullException();

            if (null != parser.Parameters[CMD_VERSION] || 
                null != parser.Parameters[CMD_VERSION_SHORT])
            {
                version();
                return (DialogResult.None);
            }
            else if (null != parser.Parameters[CMD_HELP] ||
                null != parser.Parameters[CMD_HELP_SHORT])
            {
                usage();
                return (DialogResult.None);
            }
            else if (null != parser.Parameters[CMD_SPECIAL])
            {
                return (special(parser));
            }
            else
            {
                // Determine button and icon parameters
                String sButtons = String.Empty;
                String sIcon = String.Empty;
                if (null != parser.Parameters[CMD_BUTTONS])
                    sButtons = parser.Parameters[CMD_BUTTONS];
                else if (null != parser.Parameters[CMD_BUTTONS_SHORT])
                    sButtons = parser.Parameters[CMD_BUTTONS_SHORT];
                if (null != parser.Parameters[CMD_ICON])
                    sIcon = parser.Parameters[CMD_ICON];
                else if (null != parser.Parameters[CMD_ICON_SHORT])
                    sIcon = parser.Parameters[CMD_ICON_SHORT];

                // Parse button and icon strings
                MessageBoxButtons buttons = MessageBoxButtons.OK; // Default
                MessageBoxIcon icon = MessageBoxIcon.None; // Default
                if (sButtons.ToLower() == "abortretryignore")
                    buttons = MessageBoxButtons.AbortRetryIgnore;
                else if (sButtons.ToLower() == "ok")
                    buttons = MessageBoxButtons.OK;
                else if (sButtons.ToLower() == "okcancel")
                    buttons = MessageBoxButtons.OKCancel;
                else if (sButtons.ToLower() == "retrycancel")
                    buttons = MessageBoxButtons.RetryCancel;
                else if (sButtons.ToLower() == "yesno")
                    buttons = MessageBoxButtons.YesNo;
                else if (sButtons.ToLower() == "yesnocancel")
                    buttons = MessageBoxButtons.YesNoCancel;

                if (sIcon.ToLower() == "asterisk")
                    icon = MessageBoxIcon.Asterisk;
                else if (sIcon.ToLower() == "error")
                    icon = MessageBoxIcon.Error;
                else if (sIcon.ToLower() == "exclamation")
                    icon = MessageBoxIcon.Exclamation;
                else if (sIcon.ToLower() == "hand")
                    icon = MessageBoxIcon.Hand;
                else if (sIcon.ToLower() == "information")
                    icon = MessageBoxIcon.Information;
                else if (sIcon.ToLower() == "none")
                    icon = MessageBoxIcon.None;
                else if (sIcon.ToLower() == "question")
                    icon = MessageBoxIcon.Question;
                else if (sIcon.ToLower() == "stop")
                    icon = MessageBoxIcon.Stop;
                else if (sIcon.ToLower() == "warning")
                    icon = MessageBoxIcon.Warning;

                if (parser.TrailingArguments.Count == 0)
                {
                    usage();
                    return (DialogResult.None);
                }

                // Post-process our message string
                String sMessage = parser.TrailingArguments[0];
                String sTitle = String.Empty;
                if (parser.TrailingArguments.Count == 2)
                    sTitle = parser.TrailingArguments[1];
                sMessage = sMessage.Replace("|", Environment.NewLine);

                // Display dialog message
                return (MessageBox.Show(sMessage, sTitle, buttons, icon));
            }
        }

        /// <summary>
        /// Special dialog handling cases.
        /// </summary>
        /// <param name="parser"></param>
        /// <returns></returns>
        static DialogResult special(cCommandLineParser parser)
        {
            Debug.Assert(null != parser.Parameters[CMD_SPECIAL],
                "Internal error: no special flag specified.  Aborting.");
            if (null == parser.Parameters[CMD_SPECIAL])
                return (DialogResult.Abort);

            IConfig config = ConfigFactory.CreateConfig();

            String specialDialog = (parser.Parameters[CMD_SPECIAL] as String);
            if (0 == String.Compare(specialDialog, CMD_SPECIAL_P4LOGIN, true))
            {
                Debug.Assert(2 == parser.TrailingArguments.Count,
                    "Two trailing arguments expected for 'p4login'. Aborting.");
                if (2 != parser.TrailingArguments.Count)
                    return (DialogResult.Abort);

                try
                {
                    RSG.SourceControl.Perforce.P4 p4 =
                        new RSG.SourceControl.Perforce.P4();
                    p4.Port = parser.TrailingArguments[0];
                    p4.User = parser.TrailingArguments[1];
                    p4.Connect();
                    if (p4.IsValidConnection(true, true))
                    {
                        return (DialogResult.OK);
                    }
                    else
                    {
                        return (DialogResult.Cancel);
                    }
                }
                catch (P4API.Exceptions.P4APIExceptions ex)
                {
                    RSG.Base.Windows.ExceptionStackTraceDlg stackDlg =
                        new RSG.Base.Windows.ExceptionStackTraceDlg(ex, config.EmailAddress);
                    stackDlg.ShowDialog();
                }
            }
            else if (0 == String.Compare(specialDialog, CMD_SPECIAL_EXCEPTION, true))
            {
                Debug.Assert(3 <= parser.TrailingArguments.Count,
                    "At least three trailing arguments expected for 'exception'.  Aborting.");
                if (3 > parser.TrailingArguments.Count)
                    return (DialogResult.Abort);

                String message = parser.TrailingArguments[0];
                String exceptionType = parser.TrailingArguments[1];
                String stacktrace = parser.TrailingArguments[2];
                String script = parser.TrailingArguments[3];
                String version = (parser.TrailingArguments.Count > 4) ? parser.TrailingArguments[4] : String.Empty;
                String author = (parser.TrailingArguments.Count > 5) ? parser.TrailingArguments[5] : String.Empty;
                String email = (parser.TrailingArguments.Count > 6) ? parser.TrailingArguments[6] : String.Empty;

                RSG.Base.Windows.ExceptionStackTraceDlg stackDlg =
                    new RSG.Base.Windows.ExceptionStackTraceDlg(message, exceptionType, stacktrace, script, version, config.EmailAddress, author, email);
                stackDlg.ShowDialog();
            }
            else if (0 == String.Compare(specialDialog, CMD_SPECIAL_ASSET_DEPENDENCIES, true))
            {
                Debug.Assert(1 == parser.TrailingArguments.Count,
                    "Expecting single argument filename for 'asset_dependencies'.  Aborting.");
                if (1 != parser.TrailingArguments.Count)
                    return (DialogResult.Abort);

                using (RSG.Base.Forms.AssetDependencyDialog assetDlg =
                    new RSG.Base.Forms.AssetDependencyDialog(parser.TrailingArguments[0]))
                {
                    return (assetDlg.ShowDialog());
                }
            }
            else
            {
                Console.WriteLine("Unsupported special dialog '{0}'.  Aborting.",
                    specialDialog);
                return (DialogResult.Abort);
            }
            return (DialogResult.OK);
        }
        #endregion // Command Option Parsing Methods

        #region Utility Methods
        /// <summary>
        /// Print out usage information to stdout
        /// </summary>
        static void usage()
        {
            Console.WriteLine("dialog [options] message [title]");
            Console.WriteLine("Display message dialog and set errorlevel return code depending on user choice.");
            Console.WriteLine(String.Format(" --h|--help\tDisplay this usage information (then quit)\n" +
                                            " --v|--version\tDisplay version information (then quit)\n\n" +
                                            " --b|--buttons\tSpecify buttons to display on dialog (case-insensitive)\n" +
                                            "              \tAbortRetryIgnore - Abort, Retry and Ignore buttons\n" +
                                            "              \tOK - (default) OK button only\n" +
                                            "              \tOKCancel - OK and Cancel buttons\n" +
                                            "              \tRetryCancel - Retry and Cancel buttons\n" +
                                            "              \tYesNo - Yes and No buttons\n" +
                                            "              \tYesNoCancel - Yes, No and Cancel buttons\n\n" +
                                            " --i|--icon   \tSpecify icon to display on dialog (case-insensitive)\n" +
                                            "              \tasterisk - lowercase letter i in a circle\n" +
                                            "              \terror - white X in a circle with a red background\n" +
                                            "              \texclamation - exclamation point in a triangle with a yellow background\n" +
                                            "              \thand - white X in a circle with a red background\n" +
                                            "              \tinformation - lowercase letter i in a circle\n" +
                                            "              \tnone - (default) no icon\n" +
                                            "              \tquestion - question mark in a circle\n" +
                                            "              \tstop - white X in a circle with a red background\n" +
                                            "              \twarning - exclamation point in a triangle with a yellow background\n\n" +
                                            " --s|--special\tSpecify a special-dialog\n" +
                                            "              \tp4login <port> <user> - Perforce login\n\n" +
                                            "              \texception <message> <stack> <script> [author] [email]\n\n" +
                                            " In the message string the '|' character can be used to create a new line.\n" +
                                            " \n" +
                                            "Return codes are as follows (use %ERRORLEVEL% in batch scripts to test):\n" +
                                            "\t-1 - Undefined\n" +
                                            "\t 0 - OK\n" +
                                            "\t 0 - Yes\n" +
                                            "\t 1 - No\n" +
                                            "\t 2 - Cancel\n" +
                                            "\t 3 - Abort\n" +
                                            "\t 4 - Ignore\n" +
                                            "\t 5 - Retry\n" +
                                            "\n"));
        }

        /// <summary>
        /// Print out version information to stdout
        /// </summary>
        static void version()
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();

            Console.WriteLine("dialog version " + assembly.GetName().Version.ToString());
            Console.WriteLine("Copyright � 2007-2010 Rockstar Games");
            Console.WriteLine("by David Muir <david.muir@rockstarnorth.com>\n");
            Console.WriteLine("Display message dialog and set ERRORLEVEL return code depending on user choice.\n");
        }
        #endregion // Utility Methods
    }
     
} // dialog namespace
