///
/// File:        cCommandLineParser.cs
/// Description: Implementation of a RSG.Base.cCommandLineParser class
/// Author:      David Muir <David.Muir@rockstarnorth.com>
///

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Text.RegularExpressions;

namespace RSG.Base
{

    /// <summary>
    /// Command line option parser class
    /// </summary>
    /// This class will construct a Dictionary of options and their values
    /// (if applicable) to be read by the application.  To ease this classes
    /// use we have constructors for both a string array and a regular string.
    /// 
    /// Valid parameter forms:
    /// {-,/,--}param_name[ ,=]value
    /// 
    /// E.g.
    /// --param=value
    /// -param value
    /// /param=value
    /// --param
    public class cCommandLineParser
    {
        #region Properties
        /// <summary>
        /// Read-only access to the StringDictionary of arguments parsed
        /// </summary>
        public StringDictionary Parameters
        {
            get { return m_Parameters; }
        }

        /// <summary>
        /// Read-only access to the remainder non-prefixed arguments (e.g. list of files)
        /// </summary>
        public List<String> TrailingArguments
        {
            get { return m_asTrailingArguments; }
        }

        /// <summary>
        /// Read-only access to the Arguments used to initialise object
        /// </summary>
        public String[] Arguments
        {
            get { return m_asArguments; }
        }
        #endregion

        #region Member Data
        private StringDictionary m_Parameters;
        private List<String> m_asTrailingArguments;
        private String[] m_asArguments;
        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="asArgStrings">Array of strings representing each command line argument</param>
        public cCommandLineParser(string[] asArgStrings)
        {
            m_asArguments = asArgStrings;
            Process();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sArgString">String representing all command line arguments</param>
        public cCommandLineParser(string sArgString)
        {
            char[] charSeps = new char[] {' '};
            m_asArguments = sArgString.Split(charSeps);
            Process();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sParam"></param>
        /// <returns></returns>
        public string this[string sParam]
        {
            get { return (Parameters[sParam]); }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void Process()
        {
            m_Parameters = new StringDictionary();
            m_asTrailingArguments = new List<String>();
            Regex Spliter = new Regex(@"^-{1,2}|^/|=",
                                      RegexOptions.IgnoreCase | RegexOptions.Compiled);
            Regex Remover = new Regex(@"^['""]?(.*?)['""]?$",
                                      RegexOptions.IgnoreCase | RegexOptions.Compiled);
            string Parameter = null;
            string[] Parts;

            foreach (string sArg in m_asArguments)
            {
                // Look for new parameter characters
                Parts = Spliter.Split(sArg, 3);

                switch (Parts.Length)
                {
                    case 1:
                        // Found a value for the last parameter found
                        // using a space separator
                        if (null != Parameter)
                        {
                            if (!Parameters.ContainsKey(Parameter))
                            {
                                Parts[0] = Remover.Replace(Parts[0], "$1");
                                Parameters.Add(Parameter, Parts[0]);
                            }
                            Parameter = null;
                        }
                        else 
                        {
                            // No waiting parameter so we assume its for our trailing
                            // args list.
                            m_asTrailingArguments.Add(Parts[0]);
                        }
                        break;
                    case 2:
                        // Found just a parameter
                        if (null != Parameter)
                        {
                            // Last parameter is still waiting, with no 
                            // associated value so set it to "true"
                            if (!Parameters.ContainsKey(Parameter))
                                Parameters.Add(Parameter, "true");
                        }
                        Parameter = Parts[1];
                        break;
                    case 3:
                        // Parameter with enclosed value
                        if (null != Parameter)
                        {
                            // Last parameter is still waiting, with no
                            // associated value so set it to "true"
                            if (!Parameters.ContainsKey(Parameter))
                                Parameters.Add(Parameter, "true");
                        }
                        Parameter = Parts[1];

                        // Remove possible enclosing characters (",')
                        if (!Parameters.ContainsKey(Parameter))
                        {
                            Parts[2] = Remover.Replace(Parts[2], "$1");
                            Parameters.Add(Parameter, Parts[2]);
                        }

                        Parameter = null;
                        break;
                }
            } // End of switch
            // Mop up the case that there is a parameter still waiting
            // Has no associated value so set it to "true"
            if (null != Parameter)
            {
                if (!Parameters.ContainsKey(Parameter))
                    Parameters.Add(Parameter, "true");
            }
        }
        #endregion
    }

} // End of RSG.Base namespace

// End of file
