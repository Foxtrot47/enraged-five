REM
@ECHO OFF
REM File:: unit_test.bat
REM Description:: Unit tests for the little dialog executable.
REM
REM Author:: David Muir <david.muir@rockstarnorth.com>
REM Date:: 17 November 2010
REM

SET DIALOG=dialog.exe

PUSHD bin\x86\Release

%DIALOG% --help
ECHO Return: %ERRORLEVEL%
%DIALOG% --version
ECHO Return: %ERRORLEVEL%

REM %DIALOG% --special p4login rsgedip4d2:1666 david.muir
REM ECHO Return: %ERRORLEVEL%

%DIALOG% --special exception "Unhandled exception" "FileNotFoundException" "this is|the stack|trace" "script.rb" "Michael Taschler" "michael.taschler@rockstarnorth.com"
ECHO Return: %ERRORLEVEL%

POPD
PAUSE

REM unit_test.bat
