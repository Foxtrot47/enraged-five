//
//
//    Filename: Iqueue.h
//     Creator: Greg Smith
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Interface header to the Stunt Jump Node
//
//

#ifndef INC_IqueueNODE_H_
#define INC_IqueueNODE_H_

#define queue_CLASS_ID		Class_ID(0x40bf1df5, 0x380d70c9)
#define queueNODE_CLASS_ID	Class_ID(0x7dbc3cd8, 0x736969f7)

// --- ObjPath data -------------------------------------------------------------------------------------------------

// reference ID's
#define queue_PBLOCK_REF_ID			0

#define MAX_NUM_queueNODE 4

//parameter block IDs
enum {queue_params};

// reference ID's
#define queueNODE_PBLOCK_REF_ID	0
#define queueNODE_MESH_REF_ID 1

//parameter block IDs
enum { queuenode_params };

enum {	queuenode_length };

#endif // INC_IqueueNODE_H_