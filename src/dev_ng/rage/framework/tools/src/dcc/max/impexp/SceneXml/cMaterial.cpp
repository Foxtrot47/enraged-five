//
// filename:	cMaterial.cpp
// description:
//

// --- Include Files ------------------------------------------------------------

// TinyXML headers
#include "tinyxml.h"

// Win32HelperLib headers
#include "Filesystem/Path.h"
#include "Win32HelperLib/stringutil.h"
#include "rexMaxUtility/IRsExportTexture.h"

// SceneXml headers
#include "cTexture.h"
#include "cMaterial.h"
#include "Resource.h"

// RstMaxMtl headers
#include "rstMaxMtl/IRageShaderMaterial.h"
#include "rstMaxMtl/Mtl.h"

// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

// --- Globals ------------------------------------------------------------------

// --- Static Globals -----------------------------------------------------------

// --- Static class members -----------------------------------------------------

// --- Code ---------------------------------------------------------------------

std::string cMaterial::m_sTextureTypes[12] = { "Ambient Texture", "Diffuse Texture", "Specular Texture", "Glossiness"
											, "Specular Level", "Self-illumination", "Opacity Texture"
											, "Filter color Texture", "Bump Texture", "Reflection Texture"
											, "Refraction Texture", "Displacement Texture"  };

cMaterial::cMaterial( )
	: m_sFullname( "" )
	, m_sName( "" )
	, m_sType( "" )
	, m_sPreset( "" )
	, m_bRageShader( false )
{
	m_guidID = cGuid::Empty();
}

cMaterial::cMaterial( Mtl* material )
	: m_sFullname( "" )
	, m_sName( "" )
	, m_sType( "" )
	, m_sPreset( "" )
	, m_bRageShader( false )
{
	std::string fullname = material->GetFullName( );
	m_sFullname = fullname;

	m_guidID = cGuid::Create( );

	size_t pos_open = fullname.find_last_of( '(' );
	size_t pos_close = fullname.find_last_of( ')' );
	m_sName = fullname.substr( 0, pos_open - 1 );
	m_sType = fullname.substr( pos_open + 1, pos_close - pos_open - 1 );
	trim( m_sName, " " );
	trim( m_sType, " " );

	if ( material->ClassID() == RSTMATERIAL_CLASS_ID )
	{
		IRageShaderMaterial* pInterface = GetRageShaderMtlInterface( material );
		m_sPreset = pInterface->FpGetShaderName( );
		m_bRageShader = true;
	}
}

cMaterial::~cMaterial( )
{
	m_mChildren.clear( );
	m_mTextures.clear();
}

TiXmlElement*
cMaterial::ToXmlElement( const std::string& export_as ) const
{
	TiXmlElement* pMaterial = new TiXmlElement( export_as.c_str() );
	if ( !pMaterial )
		return ( NULL );

	pMaterial->SetAttribute( "name", this->m_sName.c_str() );
	pMaterial->SetAttribute( "type", this->m_sType.c_str() );
	if ( m_bRageShader )
	{
		pMaterial->SetAttribute( "preset", this->m_sPreset.c_str() );
	}
	pMaterial->SetAttribute( "guid", this->m_guidID.ToString().c_str() );

	// Texture Maps
	if ( this->m_mTextures.size() > 0 )
	{
		TiXmlElement* pTextures = new TiXmlElement( "textures" );
		pMaterial->LinkEndChild( pTextures );

		for ( std::map<std::string, cTexture>::const_iterator itTexMap1 = this->m_mTextures.begin();
			itTexMap1 != this->m_mTextures.end();
			++itTexMap1 )
		{
			TiXmlElement* pTexture =(*itTexMap1).second.ToXmlElement( "texture" );
			pTextures->LinkEndChild( pTexture );
		}
	}

	// Child Materials
	if ( this->m_mChildren.size() > 0 )
	{
		TiXmlElement* pSubMaterials = new TiXmlElement( "submaterials" );
		pMaterial->LinkEndChild( pSubMaterials );
		for ( itMaterialListConst itMat = this->m_mChildren.begin();
			itMat != this->m_mChildren.end();
			++itMat )
		{
			TiXmlElement* pSubMat = (*itMat).second.ToXmlElement( export_as );
			pSubMaterials->LinkEndChild( pSubMat );
		}
	}

	return ( pMaterial );
}

void
cMaterial::PopulateMaterialMapList( Mtl* pMaterial )
{
	assert( pMaterial );
	if ( !pMaterial )
		return;

	if ( pMaterial->ClassID() == RSTMATERIAL_CLASS_ID )
	{
		IRageShaderMaterial* pInterface = GetRageShaderMtlInterface( pMaterial );
		int varaibleCount = pInterface->FpGetVariableCount( );
		int textureIndexCount = 0;
		int alphaTextureIndexCount = 0;
		for ( int i = 0; i < varaibleCount; i++ )
		{
			std::string variableType = pInterface->FpGetVariableType( i );
			if (variableType == "VT_TEXTURE")
			{
				std::string variableName = pInterface->FpGetVariableName( i );
				std::string texmap = pInterface->FpGetRawTextureValue( textureIndexCount );
				if ( texmap.length( ) != 0 && variableName.length( ) != 0  )
				{
					Filesystem::cPath::normalise( texmap );
					m_mTextures[texmap] = cTexture( texmap, variableName );

					if ( pInterface->FpGetRawTextureHasAlpha( alphaTextureIndexCount ) )
					{
						texmap = pInterface->FpGetRawTextureAlphaValue( alphaTextureIndexCount );
						if ( texmap.length( ) != 0 )
						{
							Filesystem::cPath::normalise( texmap );
							variableName += " Alpha";
							m_mTextures[texmap] = cTexture( texmap, variableName );
						}

						alphaTextureIndexCount++;
					}
				}
				else if ( variableName.length( ) != 0 )
				{
					rage::RstMtlMax* mtl = dynamic_cast<rage::RstMtlMax*>(pInterface);
					if (mtl != NULL)
					{
						rage::ShaderInstanceData* shaderData = mtl->GetShaderInstanceData();
						if ( shaderData && textureIndexCount < shaderData->GetTextureCount() )
						{				
							Texmap* textureMap = shaderData->GetTexture( textureIndexCount );
							if ( textureMap )
							{
								int subTextureCount = textureMap->NumSubTexmaps();
								for (int i = 0; i < subTextureCount; i++)
								{
									Texmap* subMap = textureMap->GetSubTexmap(i);
									if ( subMap )
									{
										BitmapTex* pBmpTex = dynamic_cast<BitmapTex*>( subMap );
										if ( pBmpTex )
										{
											texmap = pBmpTex->GetMapName( );
											if ( texmap.length( ) != 0 )
											{
												Filesystem::cPath::normalise( texmap );
												if (m_mTextures.find(texmap) == m_mTextures.end())
												{
													m_mTextures[texmap] = cTexture( texmap, variableName );
												}
											}
										}
									}
								}
							}
						}
						else
						{
							
						}
					}
				}

				textureIndexCount++;
			}
		}
	}
	else
	{
		for ( int chan_id = ID_AM; chan_id <= ID_DP; ++chan_id )
		{
			Texmap* pTexmap = pMaterial->GetSubTexmap(chan_id);
			IRsExportTexture *pExpTex = dynamic_cast<IRsExportTexture*>(pTexmap);
			if(pExpTex)
				pTexmap = pExpTex->GetResultMap();
			if ( !pTexmap )
				continue;

			if ( Class_ID(BMTEX_CLASS_ID, 0) == pTexmap->ClassID() )
			{
				BitmapTex* pBmpTex = (BitmapTex*)pTexmap;
				std::string texmap = pBmpTex->GetMapName( );
				Filesystem::cPath::normalise( texmap );
				m_mTextures[texmap] = cTexture( texmap, this->m_sTextureTypes[chan_id] );
			}
		}
	}
}

void
cMaterial::AddChildMaterial( const cMaterial& material )
{
	if ( m_mChildren.end() != m_mChildren.find( material.RetID().ToString() ) )
		return;

	m_mChildren[material.RetID().ToString()] = material;
}