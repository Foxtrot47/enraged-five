//
//
//    Filename: MeshSelect.h
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 7/06/99 14:50 $
//   $Revision: 5 $
// Description: Standard Mesh select classes. Including restore classes
//
//
#ifndef _MESH_SELECT_H_
#define _MESH_SELECT_H_

// Max headers
#include <Max.h>
#include <istdplug.h>
#include <meshadj.h>
// My headers
#include <dma.h>


// face selection
#define FACESEL_SINGLE	0
#define FACESEL_POLY	1
#define FACESEL_ELEMENT	2


// class prototypes
class StdMeshSelectData;


class StdMeshSelectModifier : public Modifier, public IMeshSelect
{
protected:
	static IObjParam *m_ip;			//Access to the interface
	static SelectModBoxCMode *m_pSelectMode;

	int32 m_selLevel;
	// face selection parameters
	int32 m_faceSel;
	bool m_selectByVertex;
	float m_selectThreshold;

public:

	StdMeshSelectModifier();
	~StdMeshSelectModifier();

	void* GetInterface(int id) {if(id == I_MESHSELECT) return (IMeshSelect *)this; return Modifier::GetInterface(id);}
	virtual const char **GetSelTypeNames() = 0;
	virtual const int32 *GetSelTypes() = 0;
	virtual int32 GetNumSelTypes() = 0;

	// called in ModifyObject
	void ModifyMesh(Mesh &mesh, StdMeshSelectData *pData);
	// called in BeginEditParams
	void SetupSubObjEdit(IObjParam *ip);
	// called in EndEditParams
	void ClearSubObjEdit(void);
	
	virtual uint32 GetFaceSelLevel() {return m_faceSel;}
	virtual void SetFaceSelLevel(uint32 level) {m_faceSel = level;}
	bool GetSelectByVertex(void) {return m_selectByVertex;}
	void SetSelectByVertex(bool byVertex) {m_selectByVertex = byVertex;}
	float GetSelectThreshold(void) {return m_selectThreshold;}
	void SetSelectThreshold(float threshold) {m_selectThreshold = threshold;}

	// from IMeshSelect
	virtual DWORD GetSelLevel();
	virtual void SetSelLevel(uint32 level);
	virtual void LocalDataChanged();


	// from Animatable
	void DeleteThis() { delete this; }

	// from BaseObject
	CreateMouseCallBack* GetCreateMouseCallBack() {return NULL;}
	int GetSubObjectLevel() {return GetSelLevel();}

	// from Modifier
	ChannelMask ChannelsUsed()  {return PART_GEOM|PART_TOPO;}
	ChannelMask ChannelsChanged() {return PART_SELECT|PART_SUBSEL_TYPE|PART_TOPO;} 
	Class_ID InputType() {return triObjectClassID;}
	BOOL DependOnTopology(ModContext &mc) {return TRUE;}
	void NotifyInputChanged(Interval changeInt, PartID partID, RefMessage message, ModContext *mc);
	Interval LocalValidity(TimeValue t) {return GetValidity(t);}
	Interval GetValidity(TimeValue t);
	// save selection arrays
	IOResult LoadLocalData(ILoad *iload, LocalModData **ppModData);
	IOResult SaveLocalData(ISave *isave, LocalModData *pModData);

	// Sub object selection
	int HitTest(TimeValue t, INode* inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt, ModContext* mc);
	void ActivateSubobjSel(int level, XFormModes& modes);
	virtual void SelectSubComponent(HitRecord *hitRec, BOOL selected, BOOL all, BOOL invert=FALSE);
	void ClearSelection(int selLevel);//
	void SelectAll(int selLevel);
	void InvertSelection(int selLevel);
	void GetSubObjectCenters(SubObjAxisCallback *pCallback,TimeValue t,INode *pNode,ModContext *mc);
	void GetSubObjectTMs(SubObjAxisCallback *pCallback,TimeValue t,INode *pNode,ModContext *mc);


	// face selection functions
	HitRecord *VertHitsToFaceHits(HitRecord *pHitRec,BOOL all);
};


//
//   Class Name: StdMeshSelectData
// Base Classes: HeldMeshSelectData
//  Description: Standard implementation of mesh selection data
//    Functions: 
//
//
class StdMeshSelectData : public LocalModData, public IMeshSelectData
{
protected:
	GenericNamedSelSetList m_vertSelSet;
	GenericNamedSelSetList m_faceSelSet;
	GenericNamedSelSetList m_edgeSelSet;

	BitArray m_vertSel;
	BitArray m_faceSel;
	BitArray m_edgeSel;
	Mesh *m_pMesh;
	bool m_isHeld;

public:
	// constructors
	StdMeshSelectData(void) : m_isHeld(false) {m_pMesh = NULL;}
	StdMeshSelectData(const StdMeshSelectData &meshSelData);
	StdMeshSelectData(Mesh &mesh);

	// destructor
	~StdMeshSelectData();

	// clone
	virtual LocalModData *Clone(void);

	void* GetInterface(int id) {if(id == I_MESHSELECTDATA) return (IMeshSelectData *)this; return NULL;}

	// return current cached mesh
	Mesh *GetMesh() {return m_pMesh;}
	virtual void SetCache(Mesh &mesh);
	virtual void FreeCache();

	void SetHeld(bool isHeld) {m_isHeld = isHeld;}
	bool IsHeld() {return m_isHeld;}

	BitArray GetTempVertSel(int32 selLevel);
	void GetLocalBoundBox(StdMeshSelectModifier *pMod, Box3& box);
	void GetTransformedBoundBox(StdMeshSelectModifier *pMod, Matrix3& mat, Box3& box);

	// access functions
	virtual BitArray GetVertSel() {return m_vertSel;}
	virtual void SetVertSel(BitArray &set, IMeshSelect *pMod, TimeValue t);
	virtual BitArray GetFaceSel() {return m_faceSel;}
	virtual void SetFaceSel(BitArray &set, IMeshSelect *pMod, TimeValue t);
	virtual BitArray GetEdgeSel() {return m_edgeSel;}
	virtual void SetEdgeSel(BitArray &set, IMeshSelect *pMod, TimeValue t);

	GenericNamedSelSetList& GetNamedVertSelList() {return m_vertSelSet;}
	GenericNamedSelSetList& GetNamedFaceSelList() {return m_faceSelSet;}
	GenericNamedSelSetList& GetNamedEdgeSelList() {return m_edgeSelSet;}
};


//
//   Class Name: MeshSelRestore
// Base Classes: RestoreObj
//  Description: 
//    Functions: 
//
//
class MeshSelRestore : public RestoreObj
{
private:
	BitArray m_usel, m_rsel;
	IMeshSelect *m_pMod;
	StdMeshSelectData *m_pSelData;
	int32 m_level;

public:
	// constructor (level is not index into SelType array. It is one of IMESHSEL_OBJECT,SEL_FACE etc
	MeshSelRestore(IMeshSelect *pM, StdMeshSelectData *pData, int32 level);
	void Restore(int isUndo);
	void Redo();
	int Size() { return 1; }
	void EndHold() {m_pSelData->SetHeld(false);}
	TSTR Description() { return TSTR(_T("Mesh Select Restore")); }
};


//
// useful face selection functions
//
AdjFaceList *BuildAdjFaceList(Mesh &mesh);



#endif // _MESH_SELECT_H_