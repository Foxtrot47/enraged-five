//
//
//    Filename: StuntJumpCM.h
//     Creator: Greg Smith
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Declaration of the command modes used in creating a object path
//
//

#ifndef INC_ladder_CM_H
#define INC_ladder_CM_H


#include <Max.h>

#define CREATEPATH_CID		CID_USER + 0x4012

class ladder;
class ladderNode;

using namespace rage;

//
//   Class Name: CreatePathMouseCallback
// Base Classes: MouseCallBack
//  Description: Mouse callback class to create an ObjPath
//
class CreatePathMouseCallback : public MouseCallBack, public ReferenceMaker
{
public:
	
	CreatePathMouseCallback() : m_ignoreSelectionChange(false) {}

	void Begin( IObjCreate *ioc, ClassDesc *desc );
	void End();
	void CreateNewPath();

	// reference to object array being created
	int NumRefs() { return 1; }
	RefTargetHandle GetReference(int i);
	void SetReference(int i, RefTargetHandle rtarg);
	RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,  RefMessage message);

	int proc( HWND hwnd, int msg, int point, int flag, IPoint2 m );

	void SetFxType(int fxType) { m_fxType = fxType; }
	template <class T> T GetFxObj();

private:
	
	bool m_ignoreSelectionChange;
	bool m_isAttachedToNode;
	s32 m_lastPutCount;
	s32 m_numPathNodes;
	s32 m_currentPathNode;
	s32 m_previousPathNode;
	Point3 m_pt0;
	Point3 m_pt1;
	IObjCreate* m_ip;
	ladder* m_pladder;
	
	
	INode *m_pNode;
	int m_fxType;
};



//
//   Class Name: CreatePathCM
// Base Classes: CommandMode
//  Description: Command Mode class to create an ObjPath
//
class CreatePathCM : public CommandMode
{
public:
	int Class() { return CREATE_COMMAND; }
	int ID() { return CREATEPATH_CID; }
	void Begin( IObjCreate *ioc, ClassDesc *desc, int fxtype ) {m_proc.Begin( ioc, desc ); } 
	void SetFxType(int fxType) { m_proc.SetFxType(fxType); }
	void End() { m_proc.End(); }
	MouseCallBack *MouseProc(int *pNumPoints) { *pNumPoints = 10000; return &m_proc; }
	ChangeForegroundCallback *ChangeFGProc() { return CHANGE_FG_SELECTED; }
	BOOL ChangeFG( CommandMode *oldMode ) { return (oldMode->ChangeFGProc() != CHANGE_FG_SELECTED); }
	void EnterMode() {}
	void ExitMode() {}

private:
	CreatePathMouseCallback m_proc;

};

extern CreatePathCM theCreatePathCM1;

#endif  // INC_ladder_CM_H