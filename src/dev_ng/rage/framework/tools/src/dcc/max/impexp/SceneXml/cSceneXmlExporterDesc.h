//
// filename:	cSceneXmlExporterDesc.h
// description:	
//

#ifndef INC_CSCENEXMLEXPORTERDESC_H_
#define INC_CSCENEXMLEXPORTERDESC_H_

// --- Include Files ------------------------------------------------------------

// Max headers
#include "Max.h"
#include "resource.h"
#include "iparamm2.h"

// SceneXml Headers
#include "cSceneXmlExporter.h"
#include "Win32HelperLib/Win32Res.h"

// --- Defines ------------------------------------------------------------------

#define SCENEXML_CLASS_ID Class_ID( 0x59681c9f, 0x4af510f5 )

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

class cSceneXmlExporterDesc : public ClassDesc2
{
public:
	int 			IsPublic( );
	
	void*			Create( BOOL loading = FALSE );
	const TCHAR*	ClassName();
	SClass_ID		SuperClassID();
	Class_ID		ClassID();
	const TCHAR* 	Category();
	const TCHAR*	InternalName();
	HINSTANCE		HInstance();
};

// --- Globals ------------------------------------------------------------------

extern HINSTANCE g_hInstance;

extern cSceneXmlExporterDesc* GetSceneXmlExporterDesc();

#endif // !INC_CSCENEXMLEXPORTERDESC_H_
