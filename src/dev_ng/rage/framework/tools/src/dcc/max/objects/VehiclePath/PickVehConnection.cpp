//
//
//    Filename: PickVehConnection.cpp
//     Creator: 
//     $Author: Adam $
//       $Date: 29/04/99 9:44 $
//   $Revision: 1 $
// Description: 
//
//

#include "PickVehConnection.h"
#include "VehicleNode.h"


//
//        name: PickVehConnection::Filter
// description: Return if Node is valid to be picked
//          in: pNode = pointer to node
//         out: valid? (TRUE/FALSE)
//
BOOL PickVehConnection::Filter(INode *pNode)
{
	if(pNode && pNode->GetObjectRef() != m_pVehicleNode)
	{
		Object* pObj = pNode -> EvalWorldState(0).obj;
		if (pObj && pObj->ClassID() == VEHICLE_NODE_CLASS_ID)
		{
#ifdef ONLY_LINEAR_PATHS
			if(m_oper == MERGE && 
				m_pVehicleNode->GetLinkBetween((VehicleNode *)pObj) != NULL)
				return TRUE;
			if(((m_pVehicleNode* )pObj)->GetNumberOfLinks() < 2 &&
				m_pVehicleNode->GetNumberOfLinks() < 2 &&
				m_pVehicleNode->GetLinkBetween((m_pVehicleNode* )pObj) == NULL)
				return TRUE;
#else
//			if(m_oper == MERGE || m_pVehicleNode->GetLinkBetween((VehiclePath *)pObj) == NULL)
				return TRUE;
#endif
		}
	}
	return FALSE;
}

//
//        name: PickVehConnection::HitTest
// description: Called to check what has been hit by the mouse cursor
//
BOOL PickVehConnection::HitTest(IObjParam *ip, HWND hWnd, ViewExp *vpt, IPoint2 m, int flags)
{
	INode *pNode = ip->PickNode(hWnd, m, this);
	
	if(pNode)
	{
		return TRUE;
	}
	return FALSE;
}


//
//        name: PickVehConnection::Pick
// description: Called when the mouse button is pressed
//
BOOL PickVehConnection::Pick(IObjParam *ip, ViewExp *vpt)
{
	INode *pNode = vpt->GetClosestHit();
	
	if (pNode)
	{
		switch(m_oper)
		{
		case CONNECT:
			m_pVehicleNode->ConnectTo(pNode);
			m_pVehicleNode->UpdateAllLinks(ip);
			break;
		case MERGE:
//			assert(false);
//			m_pVehicleNode->MergeWith(pNode);
			break;
		}
	}
	return FALSE;
}

//
//        name: PickVehConnection::EnterMode
// description: Called when mode is entered
//
void PickVehConnection::EnterMode(IObjParam *ip)
{
	m_pButton->SetCheck(TRUE);
}

//
//        name: PickVehConnection::ExitMode
// description: Called when mode is exited
//
void PickVehConnection::ExitMode(IObjParam *ip)
{
	m_pButton->SetCheck(FALSE);
}

