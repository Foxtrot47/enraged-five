#include <max.h>

////////////////////////////////////////////////////////////////////////////////////////////
class CueLocalRestore : public RestoreObj 
////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	EditMeshMod *emesh;

	CueLocalRestore (EditMeshMod *em) { emesh = em; }
	void Restore(int isUndo) { emesh->DragMoveRestore(); }
	void Redo() { }
	TSTR Description() {return TSTR(_T("Cue internal Restore"));}
	int Size() { return sizeof(int) + sizeof(void *); }
};

////////////////////////////////////////////////////////////////////////////////////////////
class MeshSelectRestore : public RestoreObj 
////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	BitArray undo, redo;
	EditMeshData *meshData;
	EditMeshMod *mod;
	DWORD selLevel;
	TimeValue t;

	MeshSelectRestore (EditMeshData * md, EditMeshMod * mm);
	MeshSelectRestore (EditMeshData * md, EditMeshMod * mm, DWORD selLev);
	void Restore(int isUndo);
	void Redo();
	int Size() { return 2*sizeof(void *) + 2*sizeof(BitArray) + sizeof(DWORD) + sizeof(TimeValue); }
	TSTR Description() { return TSTR(_T("Edit Mesh Selection")); }
};

////////////////////////////////////////////////////////////////////////////////////////////
class VertexEditRestore : public RestoreObj 
////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	Tab<VertMove> oMove, nMove;
	//Tab<Point3> oCreate, nCreate;
	Tab<VertMove> oClone, nClone;
	EditMeshData *meshData;
	EditMeshMod	 *mod;

	Point3* p_oTVerts,* p_nTVerts;
	int m_oITVSize,m_nITVSize;

	VertexEditRestore(EditMeshData* md, EditMeshMod* mod);
	~VertexEditRestore();
	void Restore(int isUndo);
	void Redo();
	int Size() { return 2*sizeof(void *) + 4*sizeof(Tab<Point3>); }
			
	TSTR Description() { return TSTR(_T("Move Vertices")); }
};

////////////////////////////////////////////////////////////////////////////////////////////
class MeshEditRestore : public RestoreObj 
////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	MeshDelta	omdelta, nmdelta;
	EditMeshData *meshData;
	EditMeshMod * mod;
	DWORD changeFlags;
	Tab<DWORD> mapChanges;

	Point3* p_oTVerts,* p_nTVerts;
	int m_oITVSize,m_nITVSize;

	MeshEditRestore (EditMeshData* md, EditMeshMod *mod, MeshDelta & changer);
	MeshEditRestore (EditMeshData* md, EditMeshMod *mod, DWORD cflags);
	~MeshEditRestore();

	void Restore(int isUndo);
	void Redo();
	int Size () { return 2*sizeof(MeshDelta) + 2*sizeof (void *) + sizeof(DWORD); }
	TSTR Description() { return TSTR(_T("Mesh Topological Edit")); }
};
