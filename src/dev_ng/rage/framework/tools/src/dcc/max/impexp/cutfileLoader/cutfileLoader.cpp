

#include "cutfileLoader.h"

using namespace rage;

#define cutfileLoader_CLASS_ID	Class_ID(0x5c257560, 0x24f90082)

cutfileLoader g_cutfileLoader;

class cutfileLoaderClassDesc : public ClassDesc2 
{
public:
	virtual int IsPublic() 							{ return TRUE; }
	virtual void* Create(BOOL /*loading = FALSE*/) 		{ return &cutfileLoader::GetInst(); }
	virtual const TCHAR *	ClassName() 			{ return GetString(IDS_CLASS_NAME); }
	virtual SClass_ID SuperClassID() 				{ return GUP_CLASS_ID; }
	virtual Class_ID ClassID() 						{ return cutfileLoader_CLASS_ID; }
	virtual const TCHAR* Category() 				{ return GetString(IDS_CATEGORY); }

	virtual const TCHAR* InternalName() 			{ return _T("cutfileLoader"); }	// returns fixed parsable name (scripter-visible name)
	virtual HINSTANCE HInstance() 					{ return hInstance; }					// returns owning module handle
};


bool cutfileLoader::initialised = false;
bool cutfileLoader::m_bCutFileLoaded = false;
bool cutfileLoader::m_bInteractionMode = false;
int cutfileLoader::m_iEndRange = 0;

static cutfileLoaderClassDesc cutfileLoaderDesc;


ClassDesc2* GetclipImporterDesc() { return &cutfileLoaderDesc; }

void PrintToListener(char* pPrefix, char* pMsg)
{
	char cMsg[MAX_PATH];
	if(pPrefix != NULL)
	{
		sprintf(cMsg, "%s : %s\n", pPrefix, pMsg);
		mprintf(cMsg);
	}
	else
	{
		sprintf(cMsg, "%s\n", pMsg);
		mprintf(cMsg);
	}
}


cutfileLoader::cutfileLoader()
{
	
	initialised = false;
}

cutfileLoader::~cutfileLoader()
{
	
}

// Activate and Stay Resident
DWORD cutfileLoader::Start( ) {
	
	#pragma message(TODO("Do plugin initialization here"))
	
	#pragma message(TODO("Return if you want remain loaded or not"))
	return GUPRESULT_KEEP;
}

void cutfileLoader::Stop( ) {
	#pragma message(TODO("Do plugin un-initialization here"))
}

DWORD_PTR cutfileLoader::Control( DWORD parameter ) {
	return 0;
}

IOResult cutfileLoader::Save(ISave *isave)
{
	return IO_OK;
}

IOResult cutfileLoader::Load(ILoad *iload)
{
	return IO_OK;
}

cutfileLoader& cutfileLoader::GetInst()
{
	return g_cutfileLoader;
}

void cutfileLoader::InitClass()
{
	initialised = true;
	m_bCutFileLoaded = false;
	m_bInteractionMode = true;
}

int cutfileLoader::CreateLightExporter(const float fStartTime, const float fEndTime, const int iFrames, const char* pNodeName, const char* pDirectory, int iLightType)
{
	CLightAnimationExporter* pLightExporter = rage_new CLightAnimationExporter();
	pLightExporter->Create(fStartTime, fEndTime, iFrames, pNodeName, pDirectory, iLightType);
	m_lightExporter.PushAndGrow(pLightExporter);

	return m_lightExporter.GetCount()-1;
}

CLightAnimationExporter* cutfileLoader::GetLightExporter(int index)
{
	return m_lightExporter[index];
}

cutfCutsceneFile2* cutfileLoader::GetCutFile()
{
	return &m_cutFile;
}

cutfCutsceneFile2* cutfileLoader::GetLightCutFile()
{
	return &m_lightCutFile;
}

bool cutfileLoader::IsCutFileLoaded()
{
	return m_bCutFileLoaded;
}

void cutfileLoader::SetCutFileLoaded(bool bLoaded)
{
	m_bCutFileLoaded = bLoaded;
}

bool cutfileLoader::GetInteractionMode()
{
	return m_bInteractionMode;
}

void cutfileLoader::SetInteractionMode(bool bMode)
{
	m_bInteractionMode = bMode;
}

void cutfileLoader::SetRangeEnd(int iEndRange)
{
	m_iEndRange = iEndRange;
}

int cutfileLoader::GetRangeEnd()
{
	return m_iEndRange;
}

void cutfileLoader::Reset()
{
	for(int i=0; i < m_lightExporter.GetCount(); ++i)
	{
		delete m_lightExporter[i];
	}

	m_lightExporter.Reset();
}

void cutfileLoader::ShutdownClass()
{
 
}
