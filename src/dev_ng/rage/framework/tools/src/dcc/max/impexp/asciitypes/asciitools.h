//
//
//    Filename: asciitools.h
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Tools for loading/saving ascii files
//
//
#ifndef INC_ASCII_TOOLS_H_
#define INC_ASCII_TOOLS_H_

#include <dma.h>
// STD headers
#pragma warning (disable : 4786)
#include <fstream>
#include <list>
#include <map>
#include <string>


namespace ascii {

// declarations of classes
class AsciiObject;


class PointerRedirector
{
public:
	void Clear() {m_namePtrMap.clear(); m_redirRequest.clear();}
	void AddRequest(const std::string& name, void** ppObj) {m_redirRequest.push_back(std::make_pair(name, ppObj));}
	bool AddRedirection(const std::string& name, void* pObj);
	void* Request(const std::string& name);
	bool Process();

private:
	std::map<std::string, void*> m_namePtrMap;
	std::list<std::pair<std::string, void**> > m_redirRequest;
};


//
//   Class Name: TabSpaces
//  Description: Class used to output tabs
//
class TabSpaces
{
public:
	TabSpaces() : m_tabs(0) {}
	void Add() {m_tabs++;}
	void Delete() {m_tabs--;}

	int m_tabs;
};

//
//   Class Name: Check
//  Description: Class used to check the next input from a stream
//
template<class T> 
class Check
{
public:
	Check(const T t) : m_check(t) {}
	T m_check;
};

extern PointerRedirector thePointerRedirector;


} // namespace ascii

std::ostream& operator<<(std::ostream &os, ascii::TabSpaces& tabs);

template<class T> 
std::istream& operator>>(std::istream &is, ascii::Check<T>& check)
{
	T t;
	is >> std::ws;
	is >> t;
	if(check.m_check != t)
		is.setstate(std::ios_base::failbit);
	return is;
}


#endif // INC_ASCII_TOOLS_H_