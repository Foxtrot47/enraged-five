//
//
//    Filename: VehicleLink.h
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Sub object class describing a link between two VehicleConnections
//
//
#if !defined(VEH_LINK_H_INCLUDED)
#define VEH_LINK_H_INCLUDED


#include "VehicleNode.h"
#include "IFpMaxDataStore.h"

#define VEHICLE_L_DISPLAY_EXTENTS	0x10000

class VehicleLink : public HelperObject
{
private:
	IParamBlock2 *m_pBlock2;
	Control *m_pControl[2];
	VehicleLink* m_pTheOtherDir;

	static Interface*		m_ip;			//Access to the interface
	static HWND				m_hObjWnd;
	static ICustButton*		m_pFlipButton;
	static ICustButton*		m_pMergeButton;
	static ICustButton*		m_pInsertButton;
	HWND					m_sAttrWnd;
public:
	VehicleLink();
	~VehicleLink();

	s32 GetNumConnections();
	bool GetEndPoints(TimeValue t, Point3 pt[2], float radii[2]);
	bool GetEndPointsNew(TimeValue t, Point3 pt[2]);
	void Delete(VehicleNode* pConnection);

	// Animatable methods
	int NumParamBlocks() {return 1;}
	IParamBlock2* GetParamBlock(int i) {assert(i == 0); return m_pBlock2;}
	IParamBlock2* GetParamBlockByID(short id) {assert(id == vehicle_link_params); return m_pBlock2;}
	Class_ID ClassID();// {return VEHICLE_LINK_CLASS_ID;}
	void GetClassName(TSTR& s) {s = _T("VehicleLink");}
	void BeginEditParams( IObjParam  *ip, ULONG flags,Animatable *prev);
	void EndEditParams( IObjParam *ip, ULONG flags,Animatable *next);
	void DeleteThis();// {delete this;}

	// ReferenceTarget methods
#if MAX_VERSION_MAJOR >= 9 
	ReferenceTarget* Clone(RemapDir &remap = DefaultRemapDir());
#else
	ReferenceTarget* Clone(RemapDir &remap = NoRemap());
#endif

	IOResult Load(ILoad *iload);
	IOResult Save(ISave *isave);
	int NumRefs();
	RefTargetHandle GetReference(int i);
	void SetReference(int i, RefTargetHandle pTarg);
	RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,  RefMessage message);
	void RefDeleted();
		
	//From BaseObject
	CreateMouseCallBack* GetCreateMouseCallBack() {return NULL;}
	void GetLocalBoundBox(TimeValue t, INode* pNode, ViewExp* pVp, Box3& box);
	void GetWorldBoundBox(TimeValue t, INode* pNode, ViewExp* pVp, Box3& box);
	int Display(TimeValue t, INode* pNode, ViewExp *pVpt, int flags);
	int HitTest(TimeValue t, INode* pNode, int type, int crossing, int flags, IPoint2 *pSp, ViewExp *pVpt);

	ObjectState Eval(TimeValue t) {return ObjectState(this);}
	void InitNodeName(TSTR &s) { s = _T("vlink"); }
	TCHAR *GetObjectName() { return _T("VehicleLink"); }

	Interface*	GetInterface() { return m_ip; }

	INode*	GetINode();
	void	SetPos(Interface *ip);
	void	GetVehicleNodes(VehicleNode* pNodes[2]);
	bool	ReplaceController(Control* pControlDest, Control* pControlSource);
	VehicleNode*	InsertNode(Interface* ip);
	VehicleNode*	InsertNodeAuto(Interface* ip, float fMinLength);
	VehicleNode*	AutoCreate(Interface* ip, float fMinLength);
	bool			IsLongEnoughForAutoCreate(Interface* ip, float fMinLength);
	float			GetLength(Interface* ip);

	// UI
	void InitObjUI();
	void CloseObjUI();
	void Flip();
	void Merge();

	VehicleNode* GetOtherNode(VehicleNode* pCaller);
	void CleanMeUp();

	bool IsDifferentFrom(VehicleLink* pLinkToCheck);

};


//
//   Class Name: VehicleLinkClassDesc
// Base Classes: ClassDesc
//  Description: 
//    Functions: 
//
//
class VehicleLinkClassDesc : public ClassDesc2
{

public:
	int IsPublic() {return /*1; } */0;}
	void* Create(BOOL loading = FALSE);// {return new VehicleLink;}
	const TCHAR *ClassName() {return _T("VehicleLink");}
	SClass_ID SuperClassID() {return HELPER_CLASS_ID;}
	Class_ID ClassID() {return VEHICLE_LINK_CLASS_ID;}
	const TCHAR* Category() {return _T("Standard");}
	void ResetClassParams (BOOL fileReset) {}

	// Hardwired name, used by MAX Script as unique identifier
	const TCHAR*	InternalName() { return _T("VehicleLink"); }
	HINSTANCE		HInstance()	{ return hInstance; }
};



#endif	// !defined(VEH_LINK_H_INCLUDED)

