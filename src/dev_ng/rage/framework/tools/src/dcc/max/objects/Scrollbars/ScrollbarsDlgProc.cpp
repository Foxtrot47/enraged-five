//
// filename:	ScrollbarsDlgProc.cpp
// author:		David Muir
// date:		24 January 2007
// description:	
//

// --- Include Files ------------------------------------------------------------
#include "ScrollbarsDlgProc.h"

// Local headers
#include "IScrollbars.h"
#include "Win32Res.h"
#include "resource.h"

// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------

// --- Globals ------------------------------------------------------------------
extern HINSTANCE g_hInstance;
CScrollbarsDlgProc g_ScrollbarsDlgProc;

// --- Code ---------------------------------------------------------------------

void 
CScrollbarsDlgProc::DeleteThis( )
{
	// Intentionally empty
	return;
}

BOOL 
CScrollbarsDlgProc::DlgProc( TimeValue t, IParamMap2* map, HWND hWnd,
							 UINT msg, WPARAM wParam, LPARAM lParam )
{
	switch ( msg )
	{
	case WM_INITDIALOG:
		{
			TSTR sVersion = TSTR( "Version " ) + GetStringResource( g_hInstance, IDS_VERSION );
			SetDlgItemText( hWnd, IDC_VERSION, sVersion );

			float fHeight = 1.0f;
			Interval ivalid;
			IParamBlock2* pParamBlock = map->GetParamBlock( );
			assert( pParamBlock );
			if ( !pParamBlock )
				return FALSE;

			pParamBlock->GetValue( scrollbars_height, t, fHeight, ivalid );
			SetDlgItemFloat( hWnd, IDC_SCROLLBARS_HEIGHT_EDIT, fHeight );
		}
		break;

	case WM_COMMAND:
		switch ( LOWORD( wParam ) )
		{
		case IDC_SCROLLBARS_HEIGHT_EDIT:
			{
				float fNewHeight = GetDlgItemFloat( hWnd, IDC_SCROLLBARS_HEIGHT_EDIT );
				IParamBlock2* pParamBlock = map->GetParamBlock( );
				assert( pParamBlock );
				if ( !pParamBlock )
					return FALSE;

				pParamBlock->SetValue( scrollbars_height, t, fNewHeight );
			}
			break;
		case IDC_SCROLLBARS_TYPE_COMBO:
			break;
		}
		break;

	default:
		return FALSE;
	} // End switch(msg)

	return TRUE;
}

void 
CScrollbarsDlgProc::Update( TimeValue t )
{
	// Intentionally empty
	return;
}

// End of file
