//
// filename:	ScrollbarsCommandMode.cpp
// author:		David Muir
// date:		24 January 2007
// description:	Command Mode interface for creating Scrollbars objects
//

// --- Include Files ------------------------------------------------------------
#include "ScrollbarsCommandMode.h"

// --- Defines ------------------------------------------------------------------
const int sC_cid_CreateScrollbars = ( CID_USER + 0x4013 );

// --- Globals ------------------------------------------------------------------
CScrollbarsCommandMode g_ScrollbarsCommandMode;

// --- Code ---------------------------------------------------------------------

int 
CScrollbarsCommandMode::Class( ) 
{ 
	return CREATE_COMMAND; 
}

int 
CScrollbarsCommandMode::ID( ) 
{ 
	return sC_cid_CreateScrollbars; 
}

void 
CScrollbarsCommandMode::Begin( IObjCreate* pObjCreate, ClassDesc* pDescriptor ) 
{ 
	m_proc.Begin( pObjCreate, pDescriptor ); 
}

void CScrollbarsCommandMode::End( ) 
{ 
	m_proc.End(); 
}

MouseCallBack* 
CScrollbarsCommandMode::MouseProc( int* pNumPoints ) 
{ 
	*pNumPoints = 10000; 
	return &m_proc; 
}

ChangeForegroundCallback* 
CScrollbarsCommandMode::ChangeFGProc( ) 
{ 
	return CHANGE_FG_SELECTED; 
}

BOOL 
CScrollbarsCommandMode::ChangeFG( CommandMode *oldMode ) 
{ 
	return ( CHANGE_FG_SELECTED != oldMode->ChangeFGProc( ) ); 
}

void 
CScrollbarsCommandMode::EnterMode( ) 
{
	// Intentionally empty
	return;
}

void 
CScrollbarsCommandMode::ExitMode( )
{
	// Intentionally empty
	return;
}

// End of file
