
//----------------------------------------------------

// 3dsmax SDK headers
#include "max.h"
#include "modstack.h"
#include "maxscript/maxscript.h"
#include "maxscript/maxwrapper/maxclasses.h"
#include "maxscript/foundation/numbers.h"
#include "maxscript/foundation/3dmath.h"
#include "maxscript/foundation/strings.h"
#include "maxscript/macros/define_instantiation_functions.h"

// STL headers
#include <math.h>
#include <vector>

#include "CollisionMesh.h"
#include "binarytree.h"
#include "libcore/rect.h"

#define COL_SPLITLIMIT	(1)
#define EPSILON			(0.001f)

static int g_SplitPolyCount = 5000;

//---------------------------------------------------------------------------
// MaxScript Function Definitions
//---------------------------------------------------------------------------

def_visible_primitive( create_binarytree,	"CreateBinaryTree" );
def_visible_primitive( raytriintersect,		"raytriintersect" );

//---------------------------------------------------------------------------
// MaxScript Function Implementation
//---------------------------------------------------------------------------


static int GetCollPolyCount(INode* pNode)
{
	if(!pNode)
		return 0;

	//find all children of this node that are collision meshes

	int iNumChildren = pNode->NumberOfChildren();
	int iCollCount = 0;

	Object* p_obj = pNode->GetObjectRef();

	if(p_obj->ClassID() == COLLISION_MESH_CLASS_ID)
	{
		CollisionMesh* p_coll = (CollisionMesh*)p_obj;

		if(p_coll->m_pTriObject)
		{
			iCollCount += p_coll->m_pTriObject->GetMesh().getNumFaces();
		}
	}
	else if(p_obj->ClassID() == COLLISION_BOX_CLASS_ID)
	{
		iCollCount += 12;
	}
	else if ((p_obj->ClassID() == COLLISION_SPHERE_CLASS_ID) || 
		(p_obj->ClassID() == COLLISION_CAPSULE_CLASS_ID) ||
		(p_obj->ClassID() == COLLISION_CYLINDER_CLASS_ID) )
	{
		iCollCount++;
	}

	for(int i=0;i<iNumChildren;i++)
	{
		INode* pChild = pNode->GetChildNode(i);

		iCollCount += GetCollPolyCount(pChild);
	}

	return iCollCount;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
// This function will take an array of inode and split them up into a list of inode arrays, split spatially
// Used to split the collision up into groups by their location
///////////////////////////////////////////////////////////////////////////////////////////////////////////
Value* create_binarytree_cf(Value** arg_list, int count)
{
	if(!is_array(arg_list[0]))
	{
		return &false_value;
	}

	if(count > 1)
	{
		g_SplitPolyCount = arg_list[1]->to_int();
	}

	Array* p_Array = (Array*)arg_list[0];

	if(p_Array->size < 1)
	{
		return &false_value;
	}

	//get the total bounds

	int32 i;
	CRect bounds;

	for (i = 1;i<=p_Array->size + 1;i++)
	{
		if(is_node(p_Array->get(i)))
		{
			break;
		}
	}

	INode* p_node = p_Array->get(i)->to_node();
	Matrix3 matA = p_node->GetNodeTM(0);
	Point3 transA = matA.GetTrans();

	bounds.left = transA.x;
	bounds.top = transA.y;
	bounds.right = transA.x;
	bounds.bottom = transA.y;

	for(;i<=p_Array->size + 1;i++)
	{
		if(!is_node(p_Array->get(i)))
		{
			continue;
		}

		p_node = p_Array->get(i)->to_node();
		matA = p_node->GetNodeTM(0);
		transA = matA.GetTrans();

		if(transA.x < bounds.left)
		{
			bounds.left = transA.x;
		}

		if(transA.y < bounds.bottom)
		{
			bounds.bottom = transA.y;
		}

		if(transA.x > bounds.right)
		{
			bounds.right = transA.x;
		}

		if(transA.y > bounds.top)
		{
			bounds.top = transA.y;
		}
	}

	//add the passed in items into the binary tree

	CBinaryTreeNode* p_qtnRoot = new CBinaryTreeNode(bounds,32);

	for(i=1;i<=p_Array->size;i++)
	{
		if(!is_node(p_Array->get(i)))
		{
			continue;
		}

		p_node = p_Array->get(i)->to_node();
		matA = p_node->GetNodeTM(0);
		transA = matA.GetTrans();

		p_qtnRoot->AddItem(p_node,CVector2D(transA.x,transA.y));
	}

	//create a list of binary tree nodes we use as the basis of the new lists
	std::vector<CBinaryTreeNode*> llToAdd;

	int32 iCurrent = 1;
	int32 iAdded = p_qtnRoot->GetTotalNumItems();
	std::vector<CBinaryTreeNode*>::iterator begin;
	std::vector<CBinaryTreeNode*>::iterator end;

	llToAdd.push_back(p_qtnRoot);

	while(true)
	{
		//our splitting criteria us currently the node
		//with most objects in it...

		int32 iIndex = -1;
		int32 i;
		CBinaryTreeNode* p_removed = NULL;

		for(i=0;i<iCurrent;i++)
		{
			std::list<void*> llValList;

			p_removed = llToAdd[i];

			llToAdd[i]->GetAll(llValList);

			if(llValList.size() > 1)
			{
				int32 iNumPolys = 0;

				std::list<void*>::iterator valbegin = llValList.begin();
				std::list<void*>::iterator valend = llValList.end();

				while(valbegin != valend)
				{
					INode* pNode = (INode*)(*valbegin);

					iNumPolys += GetCollPolyCount(pNode);

					valbegin++;
				}

				if(	p_removed->mp_Children[CBinaryTreeNode::FIRST] ||
					p_removed->mp_Children[CBinaryTreeNode::SECOND])
				{
					if(p_removed->mp_Children[CBinaryTreeNode::FIRST])
					{
						if(p_removed->mp_Children[CBinaryTreeNode::FIRST]->GetTotalNumItems() < COL_SPLITLIMIT)
						{
							continue;
						}
					}

					if(p_removed->mp_Children[CBinaryTreeNode::SECOND])
					{
						if(p_removed->mp_Children[CBinaryTreeNode::SECOND]->GetTotalNumItems() < COL_SPLITLIMIT)
						{
							continue;
						}
					}

					if(iNumPolys > g_SplitPolyCount)
					{
						iIndex = i;
					}
				}
			}
		}

		if(iIndex == -1)
		{
			break;
		}

		//remove the node and add its two children, assuming they both
		//have objects in them

		begin = llToAdd.begin();
		end = llToAdd.end();

		i = 0;
		while(begin!=end)
		{
			if(i == iIndex)
			{
				p_removed = llToAdd[i];
				iAdded -= p_removed->GetTotalNumItems();
				llToAdd.erase(begin);
				break;
			}

			i++;
			begin++;
		}

		iCurrent--;

		int32 iChildItems = 0;

		if(p_removed)
		{
			if(p_removed->mp_Children[CBinaryTreeNode::FIRST])
			{
				std::list<void*> llValList;

				p_removed->mp_Children[CBinaryTreeNode::FIRST]->GetAll(llValList);

				if(llValList.size() > 0)
				{
					int32 iNumPolys = 0;

					std::list<void*>::iterator valbegin = llValList.begin();
					std::list<void*>::iterator valend = llValList.end();

					while(valbegin != valend)
					{
						INode* pNode = (INode*)(*valbegin);

						iNumPolys += GetCollPolyCount(pNode);

						valbegin++;
					}

					//if(iNumPolys > 0)
					{
						iCurrent++;
						iAdded += p_removed->mp_Children[CBinaryTreeNode::FIRST]->GetTotalNumItems();
						iChildItems += p_removed->mp_Children[CBinaryTreeNode::FIRST]->GetTotalNumItems();
						llToAdd.push_back(p_removed->mp_Children[CBinaryTreeNode::FIRST]);
					}
				}
			}
		}
		
		if(p_removed)
		{
			if(p_removed->mp_Children[CBinaryTreeNode::SECOND])
			{
				std::list<void*> llValList;

				p_removed->mp_Children[CBinaryTreeNode::SECOND]->GetAll(llValList);

				if(llValList.size() > 0)
				{
					int32 iNumPolys = 0;

					std::list<void*>::iterator valbegin = llValList.begin();
					std::list<void*>::iterator valend = llValList.end();

					while(valbegin != valend)
					{
						INode* pNode = (INode*)(*valbegin);

						iNumPolys += GetCollPolyCount(pNode);

						valbegin++;
					}

					//if(iNumPolys > 0)
					{
						iCurrent++;
						iAdded += p_removed->mp_Children[CBinaryTreeNode::SECOND]->GetTotalNumItems();
						iChildItems += p_removed->mp_Children[CBinaryTreeNode::SECOND]->GetTotalNumItems();
						llToAdd.push_back(p_removed->mp_Children[CBinaryTreeNode::SECOND]);
					}
				}
			}
		}
/*
		if(iChildItems < p_removed->GetTotalNumItems())
		{
			int iVals = p_removed->m_DataItems.size();
			int iAll = p_removed->GetTotalNumItems();
			int iChildA = p_removed->mp_Children[CBinaryTreeNode::FIRST]->GetTotalNumItems();
			int iChildB = p_removed->mp_Children[CBinaryTreeNode::SECOND]->GetTotalNumItems();

			printf("valsdf");
		}
*/
	}

	//we should now have a list of binary tree nodes that 
	//we can use as the basis of our new lists
	begin = llToAdd.begin();
	end = llToAdd.end();

	int32 iSize = 0;

	Array* p_newList = new Array((Int32)llToAdd.size());

	while(begin != end)
	{
		std::list<void*> llValList;

		(*begin)->GetAll(llValList);

		Array* p_add = new Array((Int32)llValList.size());

		std::list<void*>::iterator valbegin = llValList.begin();
		std::list<void*>::iterator valend = llValList.end();

		iSize += (Int32)llValList.size();

		while(valbegin != valend)
		{
			p_add->append(new MAXNode((INode*)(*valbegin)));

			valbegin++;
		}

		p_newList->append(p_add);

		begin++;
	}

	return p_newList;
}

bool 
intersect_triangle( Point3 orig,Point3 dir,Point3 vert0,Point3 vert1,Point3 vert2,float& r_dist)
{
	if((orig == vert0) || (orig == vert1) || (orig == vert2))
		return false;

	Point3 e1,e2,h,s,q,n;
	float a,f,u,d,v,t,denom;	
	bool cull_backfaces = true;
	bool extend_to_infinity = true;

	e1 = vert2 - vert0;
	e2 = vert1 - vert0;

	h = CrossProd(dir,e2);
	a = DotProd(e1,h);

	if((cull_backfaces) && (a >= 0.0f)) return false;
	if((a > -EPSILON) && (a < EPSILON)) return false;

	f = 1.0f / a;
	s = orig - vert0;
	u = f * DotProd(s,h);

	if((u < 0.0f) || (u > 1.0f)) return false;

	q = CrossProd(s,e1);
	v = f * DotProd(dir,q);

	if((v < 0.0f) || ((u + v) > 1.0f)) return false;

	t = f * DotProd(e2,q);

	if(t < 0.0f) return false;

	if((extend_to_infinity == false) && (t > 1.0f)) return false;

	n = CrossProd(e1,e2);
	d = -DotProd(n,vert0);
	denom = DotProd(n,dir);
	r_dist = -(d + DotProd(n,orig)) / denom;

	return true;
}

Value* 
raytriintersect_cf( Value** arg_list, int count )
{
	Point3 orig,dir,vert0,vert1,vert2;	
	float result;

	orig = arg_list[0]->to_point3();
	dir = arg_list[1]->to_point3();
	vert0 = arg_list[2]->to_point3();
	vert1 = arg_list[3]->to_point3();
	vert2 = arg_list[4]->to_point3();

	if(intersect_triangle(orig,dir,vert0,vert1,vert2,result))
	{
		return_protected(Float::intern(result));
	}

	return &undefined;
}

