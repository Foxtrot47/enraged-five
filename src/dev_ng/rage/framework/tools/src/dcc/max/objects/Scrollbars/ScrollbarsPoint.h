//
// filename:	ScrollbarsPoint.h
// author:		David Muir
// date:		23 January 2007
// description:	Point Sub-object and associated descriptor class definitions
//

#ifndef INC_SCROLLBARSPOINT_H_
#define INC_SCROLLBARSPOINT_H_

// --- Include Files ------------------------------------------------------------

// 3D Studio Max SDK headers
#include "Max.h"
#include "plugapi.h"
#include "iparamb2.h"

// --- Defines ------------------------------------------------------------------
#define SCROLLBAR_POINT_LEVEL	(1)

// --- External -----------------------------------------------------------------
extern HINSTANCE g_hInstance;

// --- Structure/Class Definitions ----------------------------------------------

//
// class:		CScrollbarsPoint
// description:	Scrollbars Point sub-object
//
class CScrollbarsPoint : public HelperObject
{
public:
	CScrollbarsPoint( );
	~CScrollbarsPoint( );

	void					SetDrawCol( const Point3& pntCol );
	void					Draw( GraphicsWindow* pGW, TimeValue t, Matrix3& objMat, float fHeight );

	// Animatable methods
	Class_ID				ClassID( );
	void					GetClassName( TSTR& sClassName );
	void					DeleteThis( ) { delete this; }

	// ReferenceTarget methods
#if ( MAX_VERSION_MAJOR >= 9 )
	ReferenceTarget*		Clone( RemapDir &remap = DefaultRemapDir() );
#else
	ReferenceTarget*		Clone( RemapDir &remap = NoRemap() );
#endif

	IOResult				Load( ILoad *iload );
	IOResult				Save( ISave *isave );
	int						NumRefs( );
	RefResult				NotifyRefChanged( Interval changeInt, RefTargetHandle hTarget, 
											  PartID& partID, RefMessage message );

	// Object Methods
	ObjectState				Eval( TimeValue t );
	Interval				ObjectValidity( TimeValue t );
	void					InitNodeName( TSTR &s );
	BOOL					IsWorldSpaceObject( );

	// BaseObject Methods
	CreateMouseCallBack*	GetCreateMouseCallBack( );
	void					GetLocalBoundBox( TimeValue t, INode* pNode, ViewExp* pVp, Box3& box );
	void					GetWorldBoundBox( TimeValue t, INode* pNode, ViewExp* pVp, Box3& box );

#if MAX_RELEASE > MAX_RELEASE_R12
	int						Display( TimeValue t, INode* pNode, ViewExp *pVpt, int flags );
	int						DisplayPoint( TimeValue t, INode* pNode, ViewExp *pVpt, int flags, float fHeight );
#else
	int						Display( TimeValue t, INode* pNode, ViewExp *pVpt, int flags, float fHeight );
#endif //#if MAX_RELEASE > MAX_RELEASE_R12

	int						HitTest( TimeValue t, INode* pNode, int type, int crossing, int flags, IPoint2* pSp, ViewExp* pVpt );
	TCHAR*					GetObjectName( );
	int						GetSubObjectLevel( ) { return ( SCROLLBAR_POINT_LEVEL ); }

private:
	Point3			m_rgbDrawColour;	//!< Render colour RGB
};

//
// class:		CScrollbarsPointDesc
// description:	CScrollbarsPoint Max Descriptior Class
//
class CScrollbarsPointDesc : public ClassDesc2
{
public:
	int				IsPublic( );
	void*			Create( BOOL bLoading = FALSE );
	const TCHAR*	ClassName( );
	SClass_ID		SuperClassID( );
	Class_ID		ClassID( );
	const TCHAR*	Category( );
};

// --- Globals ------------------------------------------------------------------
ClassDesc2* GetScrollbarsPointDesc( );

#endif // !INC_SCROLLBARSPOINT_H_

// End of file
