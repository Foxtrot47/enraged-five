#ifndef GTA_BINARYTREE_H
#define GTA_BINARYTREE_H

#include "dma.h"
#include <list>
#include "libcore/rect.h"

class CBinaryTreeNode
{
public:
	enum
	{
		FIRST,
		SECOND,
		NUM_CHILDREN
	};

	CBinaryTreeNode(const CRect& bb,Int32 Depth = 0);
	~CBinaryTreeNode();

	void AddItem(void* p_vData, const CVector2D& pos);

	Int32 GetTotalNumItems();
	void GetAll(std::list<void*>& r_DataItems);

//protected:

	void GetTotalNumItemsRec(Int32& r_iTotal);

	Int32 FindSector(const CVector2D& Position);

	CRect m_rect;
	std::list<void*> m_DataItems;
	CBinaryTreeNode* mp_Children[NUM_CHILDREN];
	Int32 m_Depth;
};

//////////////////////////////////////////////////////////////////////////////
class CDensityBTreeNode
//////////////////////////////////////////////////////////////////////////////
{
public:
	enum
	{
		FIRST,
		SECOND,
		NUM_CHILDREN
	};

	struct DataItem
	{
		CVector2D m_vecPos;
		void* mp_DataItem;
	};

	CDensityBTreeNode(const CRect& bb,Int32 Depth = 0);
	~CDensityBTreeNode();

	void AddItem(void* p_vData, const CVector2D& pos);
	void Fixup();
	Int32 GetTotalNumItems();
	void GetAll(std::list<void*>& r_DataItems);

//protected:

	void GetTotalNumItemsRec(Int32& r_iTotal);

	CRect m_rect;
	std::list<DataItem> m_DataItems;
	CDensityBTreeNode* mp_Children[NUM_CHILDREN];
	Int32 m_Depth;
};

#endif //GTA_BINARYTREE_H