#pragma once
#include "water.h"

#define RIVER_HELPER_CLASS_ID	Class_ID(0x285b377e, 0x67a425b1)

class RiverHelper :
	public Water
{
public:
	RiverHelper(void);

	RefTargetHandle Clone(RemapDir& remap);

	TCHAR *GetObjectName()
	{ 
		return GetString(IDS_RIVER_HELPER_NAME); 
	}

	int GetType();
	bool	ShowTypeButton()
	{
		return true;
	}
	float	GetAlphaFloat(int index)
	{
		return 1.0f;
	}
	int	GetAlpha(int index)
	{
		return 255;
	}

	Class_ID ClassID() 
	{ 
		return RIVER_HELPER_CLASS_ID;
	}  
	void GetClassName(TSTR& s)
	{	
		s = GetString(IDS_RIVER_HELPER_NAME);
	}

	void BeginEditParams( IObjParam *ip, ULONG flags,Animatable *prev);
	void EndEditParams( IObjParam *ip, ULONG flags,Animatable *next);

	// From object
	void InitNodeName(TSTR& s)
	{	
		s = GetString(IDS_RIVER_HELPER_NAME);
	}

	void BuildSpecificMesh(TimeValue t, Mesh &mesh, Point3 centre, Point3 dim);
};
