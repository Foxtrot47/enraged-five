
#include "atl/string.h"
//#include "init/moduleSettings.h"
#include "parser/manager.h"
#include "system/param.h"

#define GAMEMAXSCRIPT_API __declspec( dllexport )

using namespace rage;

HINSTANCE	g_hDllInstance;
int			g_controlsInit = FALSE;
bool		g_libInitialized = FALSE;

//-----------------------------------------------------------------------------

BOOL WINAPI DllMain(HINSTANCE hinstDLL,ULONG fdwReason,LPVOID lpvReserved)
{
	g_hDllInstance = hinstDLL;

#if ( MAX_RELEASE < 9000 ) 
	if (!g_controlsInit) 
	{
		g_controlsInit = TRUE;
		InitCustomControls(g_hDllInstance);
		InitCommonControls();
	}
#endif

	return (TRUE);
}

//-----------------------------------------------------------------------------

GAMEMAXSCRIPT_API ClassDesc* LibClassDesc( int i )
{
	return ( NULL );
}

//-----------------------------------------------------------------------------

GAMEMAXSCRIPT_API int LibNumberClasses( )
{
	return ( 0 );
}

//-----------------------------------------------------------------------------

GAMEMAXSCRIPT_API const TCHAR* LibDescription()
{
	return ( "Rockstar Games Additional Game-Specific MaxScript Functions" );
}


//-----------------------------------------------------------------------------

GAMEMAXSCRIPT_API ULONG LibVersion()
{
	return VERSION_3DSMAX;
}

//-----------------------------------------------------------------------------

#if ( MAX_RELEASE >= 9000 ) 

GAMEMAXSCRIPT_API int 
LibInitialize( )
{
	return 1;
}


GAMEMAXSCRIPT_API int LibShutdown(void)
{
	return 1;
}

#endif //( MAX_RELEASE >= 9000 ) 

//-----------------------------------------------------------------------------

TCHAR *GetString(int id)
{
	static TCHAR buf[1024];

	if (g_hDllInstance)
		return LoadString(g_hDllInstance, id, buf, sizeof(buf)) ? buf : NULL;

	return NULL;
}

//-----------------------------------------------------------------------------

