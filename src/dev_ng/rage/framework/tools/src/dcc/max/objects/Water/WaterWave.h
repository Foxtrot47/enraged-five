#pragma once
#include "water.h"

#define WATER_WAVE_CLASS_ID	Class_ID(0x25d24fc6, 0x1d007a46)

class WaterWave :
	public Water
{
public:
	WaterWave(void);

	RefTargetHandle Clone(RemapDir& remap);

	TCHAR *GetObjectName()
	{ 
		return GetString(IDS_WATER_WAVE_NAME); 
	}

	int GetType();
	bool	ShowTypeButton()
	{
		return false;
	}
	float	GetAlphaFloat(int index)
	{
		return 1.0f;
	}
	int	GetAlpha(int index)
	{
		return 255;
	}

	Class_ID ClassID() 
	{ 
		return WATER_WAVE_CLASS_ID;
	}  
	void GetClassName(TSTR& s)
	{	
		s = GetString(IDS_WATER_WAVE_NAME);
	}

	void BeginEditParams( IObjParam *ip, ULONG flags,Animatable *prev);
	void EndEditParams( IObjParam *ip, ULONG flags,Animatable *next);

	// From object
	void InitNodeName(TSTR& s)
	{	
		s = GetString(IDS_WATER_WAVE_NAME);
	}

	void BuildSpecificMesh(TimeValue t, Mesh &mesh, Point3 centre, Point3 dim);
};
