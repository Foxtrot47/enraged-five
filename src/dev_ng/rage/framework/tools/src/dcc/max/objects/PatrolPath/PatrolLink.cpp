//
//
//    Filename: PatrolLink.cpp
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Code for connector link sub-object
//
//
#include "PatrolLink.h"
#include "IMaxDataStore.h"
#include "NodeCreateMgr.h"

#include "DataInstance.h"
#include "Attribute.h"

#include "basetypes.h"

PatrolLinkClassDesc thePatrolLinkDesc;

ClassDesc* GetPatrolLinkDesc() {return &thePatrolLinkDesc;}

HWND PatrolLink::m_hObjWnd = NULL;
ICustButton *PatrolLink::m_pFlipButton = NULL;
ICustButton *PatrolLink::m_pMergeButton = NULL;
ICustButton *PatrolLink::m_pInsertButton = NULL;

Interface *PatrolLink::m_ip = NULL;

#define PATROL_LINK_FROMCONN_CHUNK	0x763
#define OLD_DETECTION
#if !defined( _WIN64 )
BOOL CALLBACK EditPatrolLinkProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
#else
INT_PTR CALLBACK EditPatrolLinkProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
#endif

//
// Max3 paramblock2 version
//
ParamBlockDesc2 patrolLinkDesc(patrol_link_params, _T("PatrolLink parameters"), IDS_CLASS_NAME, &thePatrolLinkDesc, P_AUTO_CONSTRUCT | P_AUTO_UI, 2,
						IDD_PATROL_LINK_PARAMS_PANEL, IDS_PARAMS, 0, 0, NULL,
						end
						);

// --- PatrolLinkClassDesc --------------------------------------------------------------------------------------------------------

void* PatrolLinkClassDesc::Create(BOOL loading/*= FALSE*/)
{
	return new PatrolLink;
}

// --- PatrolLink --------------------------------------------------------------------------------------------------------

PatrolLink::PatrolLink():
	m_pBlock2(NULL)
{
	thePatrolLinkDesc.MakeAutoParamBlocks(this);
	m_pControl[0] = NULL;
	m_pControl[1] = NULL;

	theIMaxDataStore.AddData(this);	
}

PatrolLink::~PatrolLink()
{
}

RefTargetHandle PatrolLink::Clone(RemapDir& remap)
{
	PatrolLink *pClone = new PatrolLink;

	pClone->ReplaceReference(0, remap.CloneRef(GetReference(0)));
	pClone->ReplaceReference(1, remap.CloneRef(GetReference(1)));
	pClone->ReplaceReference(2, remap.CloneRef(GetReference(2)));
	pClone->ReplaceReference(3, remap.CloneRef(GetReference(3)));

	BaseClone(this, pClone, remap);

	return pClone;
}

//
//        name: PatrolLink::BeginEditParams
// description: Called when parameters are about to be edited
//			in:	prev = this is called with the connection that owns this link	
//
void PatrolLink::BeginEditParams(IObjParam *ip,ULONG flags,Animatable *prev)
{
	m_ip = ip;

	InitObjUI();
}

//
//        name: PatrolLink::EndEditParams
// description: Called when finished editing parameters
//
void PatrolLink::EndEditParams( IObjParam *ip, ULONG flags,Animatable *next )
{
	CloseObjUI();
	m_ip = NULL;
}

//
//        name: InitObjUI, CloseObjUI
//
void PatrolLink::InitObjUI()
{
	if(m_hObjWnd == NULL)
	{
		m_hObjWnd = m_ip->AddRollupPage(
			hInstance, 
			MAKEINTRESOURCE(IDD_PATROL_LINK_PANEL),
			EditPatrolLinkProc,
			_T("Vehicle Link"),
			(LPARAM)this);
		m_pFlipButton = GetICustButton(GetDlgItem(m_hObjWnd, IDC_FLIP_LINK_BUTTON));
		m_pFlipButton->SetType(CBT_PUSH);
		m_pFlipButton->SetHighlightColor(GREEN_WASH);
		m_pMergeButton = GetICustButton(GetDlgItem(m_hObjWnd, IDC_MERGE_LINK_BUTTON));
		m_pMergeButton->SetType(CBT_PUSH);
		m_pMergeButton->SetHighlightColor(GREEN_WASH);
		m_pInsertButton = GetICustButton(GetDlgItem(m_hObjWnd, IDC_LINK_INSERT_NODE_BUTTON));
		m_pInsertButton->SetType(CBT_PUSH);
		m_pInsertButton->SetHighlightColor(GREEN_WASH);
	}
}

void PatrolLink::CloseObjUI()
{
	if(m_hObjWnd)
	{
		m_ip->ClearPickMode();
		ReleaseICustButton(m_pFlipButton);
		ReleaseICustButton(m_pMergeButton);
		m_ip->DeleteRollupPage(m_hObjWnd);
		m_hObjWnd = NULL;
	}
}


//
//        name: PatrolLink::NumRefs etc
// description: Reference functions
//
int PatrolLink::NumRefs()
{
	return ail_numrefs;
}
RefTargetHandle PatrolLink::GetReference(int i)
{
	assert(i < ail_numrefs);
	switch(i)
	{
	case ail_control1:
	case ail_control2:
		return m_pControl[i];
	case ail_paramblock:
		return m_pBlock2;
	}
	return NULL;
}
void PatrolLink::SetReference(int i, RefTargetHandle pTarg)
{
	assert(i < ail_numrefs);
	switch(i)
	{
	case ail_control1:
	case ail_control2:
		m_pControl[i] = (Control *)pTarg;
		break;
	case ail_paramblock:
		m_pBlock2 = (IParamBlock2 *)pTarg;
		break;
	}
}

RefResult PatrolLink::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,  RefMessage message)
{
	switch (message) 
	{
		case REFMSG_REF_DELETED:
		{
#if (MAX_RELEASE >= 12000)
			ReferenceTarget *rt = (ReferenceTarget*)this;
			DependentIterator pItem(rt);
			ReferenceMaker* maker = NULL;

			while(NULL != (maker = pItem.Next()))
			{
				if(maker->ClassID() == PATROL_NODE_CLASS_ID)
				{
					PatrolNode *pConnection = static_cast<PatrolNode*>(maker);
				}
			}
#else
			RefList& refList = GetRefList();
			RefListItem *pItem = refList.first;

			while(pItem)
			{
				if(pItem->maker->ClassID() == PATROL_NODE_CLASS_ID)
				{
					PatrolNode *pConnection = static_cast<PatrolNode*>(pItem->maker);
				}
				pItem = pItem->next;
			}
#endif  //MAX_RELEASE >= 12000
		}
		break;
	}
	return REF_SUCCEED;
}

void PatrolLink::RefDeleted()
{
	// if number of connections is 1 then a connection has been deleted. If the number of connection is 0 then this
	// link has already been deleted by this function
	int nConnects = GetNumConnections();
	if(theHold.Holding() && nConnects == 2)
	{
		INode *pNode = GetINode();
//		DeleteMe();
//		if(pNode)
//			pNode->Delete(0, TRUE);
	}
	if(theHold.Holding() && nConnects == 1)
	{
		INode *pNode = GetINode();
		DeleteMe();
		if(pNode)
			pNode->Delete(0, TRUE);
	}
}

//
//        name: PatrolLink::GetLocalBoundBox
// description: Bounding boxes
//
void PatrolLink::GetLocalBoundBox(TimeValue t, INode* pNode, ViewExp* pVp, Box3& box)
{
	box.Init();
}
void PatrolLink::GetWorldBoundBox(TimeValue t, INode* pNode, ViewExp* pVp, Box3& box)
{
	Point3 pt[2];
	float radii[2];

	GetEndPoints(t, pt, radii);

	box.Init();
	box += pt[0];
	box += pt[1];

	box.EnlargeBy(MAX(radii[0], radii[1]));
}

int PatrolLink::Display(TimeValue t, INode* pNode, ViewExp *pVpt, int flags)
{
	GraphicsWindow *gw = pVpt->getGW();
	Point3 pt[2];
	Point3 extPt[2];
	float radii[2];
	DrawLineProc lineProc(gw);

	if(!GetEndPoints(t, pt, radii))
	{
	//	DeleteMe();
		CleanMeUp();
	}
	Matrix3 mtx(true);
	gw->setTransform(mtx);

	bool bDifferent = false;
	if(true == PatrolNodeCreateMgr::m_bViewDifferent)
	{
		if(true == IsDifferentFrom(PatrolNodeCreateMgr::GetDefaultPatrolLink()))
		{
			gw->setColor(LINE_COLOR, 0.0f, 0.0f, 5.0f);
			bDifferent = true;
		}
	}
	if(false == bDifferent)
	{
		if(pNode->Selected())
		{
			gw->setColor(LINE_COLOR, 0.8f, 0.0f, 0.3f);
		}
		else
		{
			gw->setColor(LINE_COLOR, 0.0f, 0.7f, 0.0f);
		}
	}

	Point3 vec;
	vec.x = pt[1].y - pt[0].y;
	vec.y = pt[0].x - pt[1].x;
	vec.z = 0.0f;

	vec = FNormalize(vec);

///////////////////////////////
	Matrix3 mat = pNode->GetNodeTM(t);
	float nonScale = pVpt->NonScalingObjectSize();
	float worldWidth = pVpt->GetVPWorldWidth(mat.GetTrans());
	float radius = ((nonScale*worldWidth) * (1.0f/80.0f));
	if(radius < 2.0f)
		radius = 2.0f;
////////////////////////////////
	extPt[0] = pt[1]; //+ radius * vec;
	extPt[1] = pt[0] + radius * vec;

	lineProc.proc(extPt, 2);

	extPt[0] = pt[1]; // - radius * vec;
	extPt[1] = pt[0] - radius * vec;
	lineProc.proc(extPt, 2);

	return 0;
}

int PatrolLink::HitTest(TimeValue t, INode* pNode, int type, int crossing, int flags, IPoint2 *pSp, ViewExp *pVpt)
{
	HitRegion hitRegion;
	DWORD savedLimits;
	GraphicsWindow *gw = pVpt->getGW();
	Point3 pt[2];
	float radii[2];
	DrawLineProc lineProc(gw);
	
	GetEndPoints(t, pt, radii);

	MakeHitRegion(hitRegion, type, crossing, 4, pSp);

	gw->setTransform(Matrix3(1));
	gw->setRndLimits(((savedLimits = gw->getRndLimits()) | GW_PICK) & ~GW_ILLUM);
	gw->setHitRegion(&hitRegion);
	gw->clearHitCode();	

	lineProc.proc(pt, 2);

	int res = gw->checkHitCode();
	if(1 == res)
	{
		int i = 0;
		i++;
	}
	gw->setRndLimits(savedLimits);

	return res;
}

//
//        name: PatrolLink::GetNumConnections
// description: Returns the number of connections referencing this link
//
s32 PatrolLink::GetNumConnections()
{
	s32 numConnect = 0;

#if (MAX_RELEASE >= 12000)
	ReferenceTarget *rt = (ReferenceTarget*)this;
	DependentIterator pItem(rt);
	ReferenceMaker* maker = NULL;

	while(NULL != (maker = pItem.Next()))
	{
		if(maker)
			if(maker->ClassID() == PATROL_NODE_CLASS_ID)
				numConnect++;
	}
#else
	RefList& refList = GetRefList();
	RefListItem *pItem = refList.first;

	while(pItem)
	{
		if(pItem->maker)
			if(pItem->maker->ClassID() == PATROL_NODE_CLASS_ID)
				numConnect++;
		pItem = pItem->next;
	}
#endif  //MAX_RELEASE >= 12000
	return numConnect;
}

//
//        name: PatrolLink::GetEndPoints
// description: Return the two end points of the link
//
bool PatrolLink::GetEndPoints(TimeValue t, Point3 pt[2], float radii[2])
{
	Interval ivalid;
	Matrix3 mtx;
	s32 index = 0;

	if(m_pControl[0] == NULL || m_pControl[1] == NULL)
		return false;

	Matrix3 mat(true);
	Interval i;
	m_pControl[0]->GetValue(t, &mat, i, CTRL_RELATIVE);
	pt[0] = mat.GetTrans();
	mat.IdentityMatrix();
	m_pControl[1]->GetValue(t, &mat, i, CTRL_RELATIVE);
	pt[1] = mat.GetTrans();
	radii[0] = 1.0f;
	radii[1] = 1.0f;
	return true;
	// Get the two nodes referencing the two Vehicle Connections referencing this link
/*	RefListItem *pItem = GetRefList().FirstItem();

	while(pItem)
	{
		if(pItem->maker->ClassID() == PATROL_NODE_CLASS_ID)
		{
			assert(index < 2);
			PatrolNode *pConnection = static_cast<PatrolNode*>(pItem->maker);
			INode *pNode = pConnection->GetINode();

			if(pNode)
			{
				pt[index] = pNode->GetNodeTM(t).GetTrans();
				IParamBlock2* pBlock = pConnection->GetParamBlockByID(vehicle_node_params);
				radii[index] = 1.0f; //pBlock->GetFloat(ai_radius);
				index++;
			}
		}
		pItem = pItem->next;
	}*/

	if(index != 2)
	{
		pt[1] = pt[0];
		radii[1] = radii[0];
		return false;
	}
	return true;
}

//
//        name: PatrolLink::GetEndPointsNew
// description: Return the two end points of the link
//
bool PatrolLink::GetEndPointsNew(TimeValue t, Point3 pt[2])
{
	Interval ivalid;
	Matrix3 mtx;
	s32 index = 0;

	if(m_pControl[0] == NULL || m_pControl[1] == NULL)
		return false;

	Matrix3 mat(true);
	Interval i;
	m_pControl[0]->GetValue(t, &mat, i, CTRL_RELATIVE);
	pt[0] = mat.GetTrans();
	mat.IdentityMatrix();
	m_pControl[1]->GetValue(t, &mat, i, CTRL_RELATIVE);
	pt[1] = mat.GetTrans();
	return true;
}

IOResult PatrolLink::Load(ILoad *iload)
{
	return IO_OK;
}

IOResult PatrolLink::Save(ISave *isave)
{
	return IO_OK;
}

//
//        name: PatrolLink::DeleteMe
// description: Delete link
//
void PatrolLink::Delete(PatrolNode *pConnection)
{
	DeleteMe();
}

void PatrolLink::DeleteThis()
{
	delete this;
}
Class_ID PatrolLink::ClassID()
{
	return PATROL_LINK_CLASS_ID;
}

//
//        name: *PatrolNode::GetINode
// description: Get the INode that references this object. Assumes only one INode ie object hasn't been instanced
//
INode *PatrolLink::GetINode()
{
	INode *pThisNode = NULL;

	// parse dependents to find the INode
#if (MAX_RELEASE >= 12000)
	ReferenceTarget *rt = (ReferenceTarget*)this;
	DependentIterator pItem(rt);
	ReferenceMaker* maker = NULL;

	while(NULL != (maker = pItem.Next()))
	{
		if(maker->SuperClassID() == BASENODE_CLASS_ID)
		{
			pThisNode = (INode *)maker;
			break;
		}
	}
#else
	RefList &refList = GetRefList();
	RefListItem *pItem;

	pItem = refList.FirstItem();
	while(pItem)
	{
		if(pItem->maker->SuperClassID() == BASENODE_CLASS_ID)
		{
			pThisNode = (INode *)pItem->maker;
			break;
		}
		pItem = pItem->next;
	}
#endif  //MAX_RELEASE >= 12000

	//assert(pThisNode);

	return pThisNode;
}

void PatrolLink::SetPos(Interface *ip)
{
	Point3 pt[2];
	float radii[2];
//	pt[0] = pNode->GetNodeTM(m_ip->GetTime()).GetTrans();
//	pt[1] = pThisNode->GetNodeTM(m_ip->GetTime()).GetTrans();

	GetEndPoints(ip->GetTime(), pt, radii);
//	Box3 bbox(pt[1], pt[2]);
//	Point3 ptTemp = bbox.Center();
	Point3 ptTemp = (pt[0] + pt[1]) / (float)2.0f;
	Matrix3 mat(1);
	mat.SetTrans(ptTemp);
	INode* pNode = GetINode();
	assert(pNode);
	ip->SetNodeTMRelConstPlane(pNode, mat);
}

void PatrolLink::Flip()
{
//	 
	assert(m_pControl[0]);
	assert(m_pControl[1]);
	assert(2 == GetNumConnections());
	Control* pTemp = m_pControl[0];
	theHold.Begin();
	m_pControl[0] = m_pControl[1];
	m_pControl[1] = pTemp;
	theHold.Accept("Vehicle Link Flip");

	m_ip->RedrawViews(m_ip->GetTime());
}

void PatrolLink::Merge()
{
	theHold.Begin();

	TimeValue t = m_ip->GetTime();
	// Store Position
	INode* pNode = GetINode();
	Matrix3 mat = pNode->GetNodeTM(t);

	//Get the vehicle nodes (in order of m_pControllers)
	PatrolNode* pVehicleNodes[2];
	GetVehicleNodes(pVehicleNodes);

	// Get Links from Node0
	std::vector<RefTargetHandle>  rth = pVehicleNodes[0]->GetRefListVector();
	for(s32 i=0; i<rth.size(); i++)
	{
		PatrolLink* pLink = (PatrolLink*) rth[i];
		if(pLink && pLink != this)
		{
			// Add Links to Node1
			pVehicleNodes[1]->AddReference(pLink );
			// Remove Links from Node0
			pVehicleNodes[0]->RemoveReference(pLink);
			// Swap Links Contoller
			pLink->ReplaceController(m_pControl[0], m_pControl[1]);
		}
	}

	// Remove my m_pControllers
	ReplaceReference(ail_control1, NULL);
	ReplaceReference(ail_control2, NULL);

	pNode = pVehicleNodes[1]->GetINode();
	pNode->SetNodeTM(t, mat);
	pVehicleNodes[1]->UpdateAllLinks(m_ip);

	pNode= GetINode();
	m_ip->DeSelectNode(pNode);
//	pVehicleNodes[0]->GetINode()->Delete(m_ip->GetTime(), TRUE);
	// Remove me from Nodes
	pVehicleNodes[0]->RemoveReference(this);
	pVehicleNodes[0]->GetINode()->Delete(t, TRUE);
//	pVehicleNodes[1]->RemoveReference(this);

//	pNode->Delete(m_ip->GetTime(), TRUE);
	DeleteMe();

	theHold.Accept("Merge Nodes");

//	m_ip->RedrawViews(m_ip->GetTime());
}

PatrolNode* PatrolLink::GetOtherNode(PatrolNode* pCaller)
{
	PatrolNode* pOtherNode = NULL;
	s32 numNodes=0;

#if (MAX_RELEASE >= 12000)
	ReferenceTarget *rt = (ReferenceTarget*)this;
	DependentIterator pItem(rt);
	ReferenceMaker* maker = NULL;

	while(NULL != (maker = pItem.Next()))
	{
		if(maker->ClassID() == PATROL_NODE_CLASS_ID)
		{
			pOtherNode = static_cast<PatrolNode*>(maker);
			if(pOtherNode != pCaller)
			{
				numNodes++;
			}
		}
	}
#else
	RefList& refList = GetRefList();
	RefListItem *pItem = refList.first;
	while(pItem)
	{
		if(pItem->maker->ClassID() == PATROL_NODE_CLASS_ID)
		{
			pOtherNode = static_cast<PatrolNode*>(pItem->maker);
			if(pOtherNode != pCaller)
			{
				numNodes++;
			}
		}
		pItem = pItem->next;
	}
#endif  //MAX_RELEASE >= 12000
	//assert(numNodes < 1);
	return pOtherNode;
}

void PatrolLink::GetVehicleNodes(PatrolNode*pNodes[2])
{
	s32 numNodes=0;
#if (MAX_RELEASE >= 12000)
	ReferenceTarget *rt = (ReferenceTarget*)this;
	DependentIterator pItem(rt);
	ReferenceMaker* maker = NULL;

	while(NULL != (maker = pItem.Next()))
	{
		if(maker->ClassID() == PATROL_NODE_CLASS_ID)
		{
			// We have a PatrolNode
			// Need to Get the INode* from the PatrolNode
			INode* pPNode = ((PatrolNode*)maker)->GetINode();
			Control* pController = pPNode->GetTMController();

			if(pController==this->m_pControl[0])
			{
				pNodes[0]=((PatrolNode*)maker);
				numNodes++;
			}
			if(pController==this->m_pControl[1])
			{
				pNodes[1]=((PatrolNode*)maker);
				numNodes++;
			}
		}
	}
#else
	RefListItem *pItem = GetRefList().FirstItem();
	while(pItem)
	{
		if(pItem->maker->ClassID() == PATROL_NODE_CLASS_ID)
		{
			// We have a PatrolNode
			// Need to Get the INode* from the PatrolNode
			INode* pPNode = ((PatrolNode*)pItem->maker)->GetINode();
			Control* pController = pPNode->GetTMController();

			if(pController==this->m_pControl[0])
			{
				pNodes[0]=((PatrolNode*)pItem->maker);
				numNodes++;
			}
			if(pController==this->m_pControl[1])
			{
				pNodes[1]=((PatrolNode*)pItem->maker);
				numNodes++;
			}
		}
		pItem = pItem->next;
	}
#endif  //MAX_RELEASE >= 12000

	assert(2==numNodes);

}

void PatrolLink::CleanMeUp()
{
	s32 numNodes=0;

#if (MAX_RELEASE >= 12000)
	ReferenceTarget *rt = (ReferenceTarget*)this;
	DependentIterator pItem(rt);
	ReferenceMaker* maker = NULL;

	while(NULL != (maker = pItem.Next()))
	{
		if(maker->ClassID() == PATROL_NODE_CLASS_ID)
		{
			PatrolNode* pNode = static_cast<PatrolNode*>(maker);
			numNodes++;
		}
	}
#else
	RefList& refList = GetRefList();
	RefListItem *pItem = refList.first;
	while(pItem)
	{
		if(pItem->maker->ClassID() == PATROL_NODE_CLASS_ID)
		{
			PatrolNode* pNode = static_cast<PatrolNode*>(pItem->maker);
			numNodes++;
		}
		pItem = pItem->next;
	}
#endif  //MAX_RELEASE >= 12000
}

bool PatrolLink::ReplaceController(Control* pControlDest, Control* pControlSource)
{
	if(m_pControl[0] == pControlDest)
	{
		ReplaceReference(0, pControlSource);
		return true;
	}
	else if(m_pControl[1] == pControlDest)
	{
		ReplaceReference(1, pControlSource);
		return true;
	}
	return false;
}

// --- Windows callback ------------------------------------------------------------------------------------
#if !defined( _WIN64 )
BOOL CALLBACK EditPatrolLinkProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
#else
INT_PTR CALLBACK EditPatrolLinkProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
#endif
{
	static PatrolLink *pObject;
	if(pObject == NULL && message != WM_INITDIALOG)
		return TRUE;

	switch(message)
	{
	case WM_INITDIALOG:
		pObject = (PatrolLink *)lParam;
		break;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDC_FLIP_LINK_BUTTON:
			pObject->Flip();
			break;
		case IDC_MERGE_LINK_BUTTON:
			pObject->Merge();
			break;
		case IDC_LINK_INSERT_NODE_BUTTON:
			pObject->InsertNode(pObject->GetInterface());
			break;
		default:
			break;
		}
		break;

	case WM_CLOSE:
		break;

	default:
		return FALSE;
	}
	return TRUE;

}

float PatrolLink::GetLength(Interface* ip)
{
	Point3 pt[2];
	GetEndPointsNew(ip->GetTime(), pt);

	return Length(pt[1] - pt[0]);
}

bool PatrolLink::IsLongEnoughForAutoCreate(Interface* ip, float fMinLength)
{
	float fLen = GetLength(ip);
	if(fabs(fLen) < fMinLength)
	{
		return false;
	}
	return true;
}

PatrolNode* PatrolLink::AutoCreate(Interface* ip, float fMinLength)
{
	PatrolLink* pLinks[2];
	PatrolNode* pRoot = InsertNodeAuto(ip, fMinLength);
	if(NULL != pRoot)
	{
		bool bRet = pRoot->GetBothLinks(pLinks);
		assert(bRet);
		pRoot = pLinks[0]->AutoCreate(ip, fMinLength);
		pRoot = pLinks[1]->AutoCreate(ip, fMinLength);
	}
	return pRoot;
}


//        name: PatrolLink::InsertNodeAuto
// description: Insert a PatrolNode in this link if long enough
//
PatrolNode* PatrolLink::InsertNodeAuto(Interface* ip, float fMinLength)
{
	if(false == IsLongEnoughForAutoCreate(ip, fMinLength))
	{
		return NULL;
	}
	return InsertNode(ip);
}

//        name: PatrolLink::InsertNode
// description: Insert an PatrolNode in this link
//
PatrolNode* PatrolLink::InsertNode(Interface* ip)
{
	Point3 pt[2];
	float radii[2];
	GetEndPoints(ip->GetTime(), pt, radii);

	//Get the vehicle nodes (in order of m_pControllers)
	PatrolNode* pVehicleNodes[2];
	GetVehicleNodes(pVehicleNodes);

	bool bAlreadyHolding=true;
	if(0 == theHold.Holding())
	{
		theHold.Begin();
		bAlreadyHolding=false;
	}

	// Create New Node
	PatrolNode* pNewConnection = NULL; // (PatrolNode *)ip->CreateInstance(HELPER_CLASS_ID, PATROL_NODE_CLASS_ID);
	INode* pNewNode = NULL; // ip->CreateObjectNode(pNewConnection);
	PatrolNodeCreateMgr::CreateVehicleNodeDefault((IObjCreate*)ip, pNewConnection, pNewNode, NULL);

	// position new connection at node1's old position
	pNewNode->Move(0, Matrix3(1), (pt[0] + pt[1]) / 2.0f);
//	IMaxDataStore max;
//	max.AddData(pNewConnection);

	// Create New Link
	PatrolLink *pNewLink = NULL; // (PatrolLink *)ip->CreateInstance(HELPER_CLASS_ID, PATROL_LINK_CLASS_ID);
	INode *pLinkNode = NULL; // ip->CreateObjectNode(pNewLink);
	PatrolNodeCreateMgr::CreateVehicleLinkDefault((IObjCreate*)ip, pNewLink, pLinkNode);
//	max.AddData(pNewLink);

	// Add References to New Link
	pNewLink->ReplaceReference(0, pNewNode->GetTMController());
	pNewLink->ReplaceReference(1, pVehicleNodes[1]->GetINode()->GetTMController());

	// Replace pVehicleNodes[1] with pNewNode as reference 1
	assert(GetReference(1) == pVehicleNodes[1]->GetINode()->GetTMController());
	ReplaceReference(1, pNewNode->GetTMController());

	pNewConnection->AddReference(this);
	pNewConnection->AddReference(pNewLink);
	pNewConnection->UpdateAllLinks(ip);

	// Remove (this) reference from pVehicleNodes[1] and add new Link
	pVehicleNodes[1]->RemoveReference(this);
	pVehicleNodes[1]->AddReference(pNewLink);

	NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);

	if(false == bAlreadyHolding)
	{
		theHold.Accept("Link:Insert Node");
	}
	ip->RedrawViews(ip->GetTime());

	return pNewConnection;
}

//        name: PatrolLink::IsDifferentFrom
// description: If this node is different from the Node passed 
//				then return true.
//          in: Node to check differences with
//
bool PatrolLink::IsDifferentFrom(PatrolLink* pLinkToCheck)
{
	DataInstance* pThisData = theIMaxDataStore.GetData(this);
	DataInstance* pDefData = theIMaxDataStore.GetData(pLinkToCheck);
	if( (NULL ==pThisData) || (NULL == pDefData) )
	{
		return false;
	}
	dmat::AttributeInst* pThisAttr = pThisData->GetAttributes();
	dmat::AttributeInst* pDefAttr = pDefData->GetAttributes();

	dmat::AttributeClass defClass = pDefAttr->GetClass();
	dmat::AttributeClass thisClass = pThisAttr->GetClass();

	s32 sizeDef = defClass.GetSize();
	s32 sizeThis = defClass.GetSize();
	if(sizeThis != sizeDef)
	{
		return true;
	}
	s32 matching=0;
	for(s32 i=0; i<sizeDef; i++)
	{
		dmat::Attribute defAttr = defClass.GetItem(i);
		switch(defAttr.GetType())
		{
			case dmat::Attribute::INT :
			{
				s32 val1 = pDefAttr->GetAttributeValue(i);
				s32 val2 = pThisAttr->GetAttributeValue(i);
				{
					if(val1 == val2)
					{
						matching++;
					}
				}
			}
			break;
			case dmat::Attribute::FLOAT :
			{
				float val1 = pDefAttr->GetAttributeValue(i);
				float val2 = pThisAttr->GetAttributeValue(i);
				{
					if(val1 == val2)
					{
						matching++;
					}
				}
			}
			break;
			case dmat::Attribute::BOOL	:
			{
				bool val1 = pDefAttr->GetAttributeValue(i);
				bool val2 = pThisAttr->GetAttributeValue(i);
				{
					if(val1 == val2)
					{
						matching++;
					}
				}
			}
			break;
			case dmat::Attribute::STRING :
			{	
				char* val1 = pDefAttr->GetAttributeValue(i);
				char* val2 = pThisAttr->GetAttributeValue(i);
				{
					if(0 == strcmp(val1, val2))
					{
						matching++;
					}
				}
			}
			break;
		}
	}

	if(matching == sizeDef)
	{
		return false;
	}
	return true;
}
