//
//
//    Filename: ladder.cpp
//     Creator: Greg Smith
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Definition of ladder class
//
//

#include "ladder.h"
#include "ladderNode.h"
#include "ladderCM.h"
#include "notify.h"
#include "resource.h"

static ladderClassDesc theladderDesc;


IObjParam* ladder::m_ip = NULL;
HWND ladder::m_hPanel = NULL;
HWND ladder::m_hNodePanel = NULL;
MoveModBoxCMode* ladder::m_moveMode = NULL;
SelectModBoxCMode* ladder::m_selectMode = NULL;
bool ladder::m_creating = false;
Object* ladder::m_pNodeEdited = NULL;
#if !defined( _WIN64 )
BOOL CALLBACK EditladderNodeDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
#else
INT_PTR CALLBACK EditladderNodeDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
#endif

ClassDesc* GetladderDesc() {return &theladderDesc;}


// --- ObjPathClassDesc ------------------------------------------------------------------------------------

void *ladderClassDesc::Create(BOOL loading)
{
	return new ladder;
}



void ladderClassDesc::ResetClassParams (BOOL fileReset)
{

}

int ladderClassDesc::BeginCreate (Interface *ip)
{
	IObjCreate *pIob = ip->GetIObjCreate();

	theCreatePathCM1.SetFxType(FX_LADDER);
	theCreatePathCM1.Begin( pIob, this, FX_LADDER);
	pIob->PushCommandMode( &theCreatePathCM1 );

	return TRUE;
}

int ladderClassDesc::EndCreate (Interface *ip)
{
	theCreatePathCM1.End();
	ip->RemoveMode( &theCreatePathCM1 );
	return TRUE;
}


//
// Max3 paramblock2
//
ParamBlockDesc2 pathDesc(path_params, _T("ladder Parameters"), IDS_CLASS_NAME, &theladderDesc, P_AUTO_CONSTRUCT | P_AUTO_UI,
						LADDER_PBLOCK_REF_ID,
						IDD_PATH_PANEL, IDS_PARAMS, 0,0, NULL,
						end);

//--- ObjPath -------------------------------------------------------

ladder::ladder() : m_pBlock2(NULL), m_selLevel(0)
{
	for(s32 i=0; i<MAX_NUM_LADDERNODE; i++)
	{
		m_pPathNodes[i] = NULL;
		m_pControls[i] = NULL;
		m_nodeSelected[i] = NULL;
	}

	theladderDesc.MakeAutoParamBlocks(this);
}

ladder::~ladder()
{
}

void ladder::BeginEditParams(IObjParam *ip,ULONG flags,Animatable *prev)
{
	m_ip = ip;

	if(m_selLevel)
		InitPathNodeUI();
	else
		InitPathUI();

	if (flags&BEGIN_EDIT_CREATE) {
		m_creating = true;
	} else {
		m_creating = false;
		// Create sub object editing modes.
		m_moveMode       = new MoveModBoxCMode(this,ip);
		m_selectMode     = new SelectModBoxCMode(this,ip);

		// Add our sub object type
		m_ip->SetSubObjectLevel(m_selLevel);
	}

}

void ladder::EndEditParams( IObjParam *ip, ULONG flags,Animatable *next )
{
	if(!m_creating)
	{
		ip->DeleteMode(m_moveMode);
		ip->DeleteMode(m_selectMode);
		if ( m_moveMode )
			delete m_moveMode;
		m_moveMode = NULL;
		if ( m_selectMode )
			delete m_selectMode;
		m_selectMode = NULL;
	}

	ClosePathUI();
	ClosePathNodeUI();

	m_ip = NULL;
}

RefTargetHandle ladder::Clone(RemapDir& remap)
{
	ladder* pClone = new ladder();
	s32 i;

	for(i=0; i<NumRefs(); i++)
		pClone->ReplaceReference(i, remap.CloneRef(GetReference(i)));

	BaseClone(this, pClone, remap);
	return pClone;
}

Interval ladder::ObjectValidity(TimeValue t)
{
	Interval ivalid = FOREVER;
	Matrix3 mtx;

	for(s32 i=0; i<MAX_NUM_LADDERNODE; i++)
	{
		if(m_pPathNodes[i])
			ivalid &= m_pPathNodes[i]->ObjectValidity(t);
		if(m_pControls[i])
			m_pControls[i]->GetValue(t, &mtx, ivalid, CTRL_RELATIVE);
	}
	return ivalid;
}

IOResult ladder::Load(ILoad *iload)
{
	return IO_OK;
}

IOResult ladder::Save(ISave *isave)
{
	return IO_OK;
}

int ladder::NumRefs()
{
	return 1 + 2 * MAX_NUM_LADDERNODE;
}

RefTargetHandle ladder::GetReference(int i)
{
	if(i == LADDER_PBLOCK_REF_ID)
		return m_pBlock2;
	else if(i > 0 && i <= MAX_NUM_LADDERNODE)
		return m_pPathNodes[i - 1];
	else if(i > MAX_NUM_LADDERNODE && i <= (MAX_NUM_LADDERNODE * 2))
		return m_pControls[i - (MAX_NUM_LADDERNODE + 1)];
	return NULL;
}

void ladder::SetReference(int i, RefTargetHandle rtarg)
{
	if(i == LADDER_PBLOCK_REF_ID)
		m_pBlock2 = (IParamBlock2 *)rtarg;
	else if(i > 0 && i <= MAX_NUM_LADDERNODE)
		m_pPathNodes[i - 1] = (ladderNode*)rtarg;
	else if(i > MAX_NUM_LADDERNODE && i <= (MAX_NUM_LADDERNODE * 2))
		m_pControls[i - (MAX_NUM_LADDERNODE + 1)] = (Control*)rtarg;
}

RefResult ladder::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget,
										   PartID& partID,  RefMessage message)
{
	s32 i;

	switch(message)
	{
	case REFMSG_TARGET_DELETED:
		for(i=0; i<MAX_NUM_LADDERNODE; i++)
		{
			if(m_pPathNodes[i] == hTarget)
			{
				m_pPathNodes[i] = NULL;
				TidyAfterDelete(i);
			}
		}
		break;
	}
	return REF_SUCCEED;
}

int ladder::NumSubs()
{
	return MAX_NUM_LADDERNODE;
}

Animatable* ladder::SubAnim(int i)
{
	return m_pPathNodes[i];
}

MSTR ladder::SubAnimName(int i)
{
	MSTR name;
	name.printf("ladder%2d", i+1);
	return name;
}

CreateMouseCallBack* ladder::GetCreateMouseCallBack()
{
	return NULL;
}

void ladder::GetLocalBoundBox(TimeValue t, INode* pNode, ViewExp* pVpt, Box3& box)
{
	s32 i;
	box.Init();

	Box3 firstBox(Point3(-5.0f,-5.0f,-5.0f),Point3(5.0f,5.0f,5.0f));

	box += firstBox;

	for(i=0; i<MAX_NUM_LADDERNODE; i++)
	{
		if(m_pPathNodes[i] == NULL)
			continue;

		Point3 posn = GetPathNodePosn(i);

		Box3 localBox(Point3(-0.0f,-0.0f,-0.0f),Point3(0.0f,0.0f,0.0f));

		m_pPathNodes[i]->GetLocalBoundBox(t,NULL,pVpt,localBox);

		localBox.pmin += posn;
		localBox.pmax += posn;

		box += localBox;
	}
}

void ladder::GetWorldBoundBox(TimeValue t, INode* pNode, ViewExp* pVpt, Box3& box)
{
	Matrix3 local2world = pNode->GetObjectTM(t);
	Box3 localBox;

	GetLocalBoundBox(t, pNode, pVpt, localBox);

	box = localBox * local2world;
}

int ladder::Display(TimeValue t, INode* pNode, ViewExp *pVpt, int flags)
{
	GraphicsWindow *gw = pVpt->getGW();

	Draw(t, pNode, pVpt, flags);

	return 0;
}

int ladder::HitTest(TimeValue t, INode* pNode, int type, int crossing, int flags, IPoint2 *pSp, ViewExp *pVpt)
{
	HitRegion hitRegion;
	DWORD savedLimits;
	GraphicsWindow *gw = pVpt->getGW();

	MakeHitRegion(hitRegion, type, crossing, 4, pSp);

	gw->setRndLimits(((savedLimits = gw->getRndLimits()) | GW_PICK) & ~GW_ILLUM);
	gw->setHitRegion(&hitRegion);
	gw->clearHitCode();

	Draw(t, pNode, pVpt, flags);

	int res = gw->checkHitCode();

	gw->setRndLimits(savedLimits);

	return res;
}

void ladder::Draw(TimeValue t, INode* pNode, ViewExp *pVpt, int flags)
{
	GraphicsWindow *gw = pVpt->getGW();
	Matrix3 objMtx = pNode->GetObjectTM(t);
	DrawLineProc lineProc(gw);
	Point3 pt[6];

	Matrix3 tmFirst(1);
	Interval ivalid;

	if(!m_pControls[0])
	{
		return;
	}

	m_pControls[0]->GetValue(t, &tmFirst, ivalid, CTRL_RELATIVE);

	gw->setTransform(objMtx);

	int iLines = 0;

	int i = 0;

	for(i=1; i<MAX_NUM_LADDERNODE; i++)
	{
		Matrix3 tm(1);
		Interval ivalid;

		if(m_pPathNodes[i] == NULL)
			continue;

		m_pControls[i]->GetValue(t, &tm, ivalid, CTRL_RELATIVE);

		pt[(iLines * 2)] = tmFirst.GetTrans(); 
		pt[(iLines * 2) + 1] = tm.GetTrans();
		iLines++;
	}

	lineProc.proc(pt,iLines * 2);

	for(i=0; i<MAX_NUM_LADDERNODE; i++)
	{
		Matrix3 tm(1);
		Interval ivalid;

		if(m_pPathNodes[i] == NULL)
			continue;

		m_pControls[i]->GetValue(t, &tm, ivalid, CTRL_RELATIVE);

		INodeTransformed node(pNode, tm);
		m_pPathNodes[i]->Display(t, &node, pVpt, flags);
	}
}

ISubObjType* ladder::GetSubObjType(int i)
{
	static GenSubObjType subObjType("ladderNode", "SubObjectIcons", 29);
	if(i == -1)
		return NULL;
	return &subObjType;
}

void ladder::SelectAll( int selLevel )
{
	switch ( selLevel )
	{
	case LADDER_NODE_LEVEL:
		for ( int i = 0; i < MAX_NUM_LADDERNODE; ++i )
		{
			SelectPathNode( i, true );
		}
	}
}

void ladder::Move(TimeValue t, Matrix3& partm, Matrix3& tmAxis,
					  Point3& val, BOOL localOrigin)
{
	for(s32 i=0; i<MAX_NUM_LADDERNODE; i++)
	{
		if(m_nodeSelected[i] && m_pControls[i])
		{
			SetXFormPacket pckt(val,partm,tmAxis);
			m_pControls[i]->SetValue(t,&pckt,TRUE,CTRL_RELATIVE);
		}
	}
	NotifyDependents(FOREVER, PART_GEOM, REFMSG_CHANGE);
}

int ladder::HitTest(TimeValue t, INode* pNode, int type, int crossing,int flags, IPoint2 *p, ViewExp *pVpt, ModContext* mc)
{
	int res = FALSE;

	for(s32 i=0; i<MAX_NUM_LADDERNODE; i++)
	{
		if(m_pPathNodes[i] &&
		!(flags&HIT_SELONLY && m_nodeSelected[i] == false) &&
		!(flags&HIT_UNSELONLY && m_nodeSelected[i] == true) )
		{
			if(m_pControls[i])
			{
				Matrix3 tm(1);
				Interval ivalid;

				m_pControls[i]->GetValue(t, &tm, ivalid, CTRL_RELATIVE);

				INodeTransformed node(pNode, tm);

				if(m_pPathNodes[i]->HitTest(t, &node, type, crossing, flags, p, pVpt)) {
					pVpt->LogHit(pNode,mc,0,i,NULL);
					res = TRUE;
					if (flags & HIT_ABORTONHIT)
						return TRUE;
				}
			}
		}
	}

	return res;
}

int ladder::SubObjectIndex(HitRecord *hitRec)
{
	return hitRec->hitInfo;
}

void ladder::GetSubObjectCenters(SubObjAxisCallback *cb,TimeValue t,INode *node,ModContext *mc)
{
	for(s32 i=0; i<MAX_NUM_LADDERNODE; i++)
	{
		if (m_nodeSelected[i] && m_pControls[i]) {
			Matrix3 tm(1);
			Interval ivalid;

			m_pControls[i]->GetValue(t, &tm, ivalid, CTRL_RELATIVE);
			tm *= node->GetObjectTM(t);
			cb->Center(tm.GetTrans(),i);
		}
	}
}

void ladder::GetSubObjectTMs(SubObjAxisCallback *cb,TimeValue t,INode *node,ModContext *mc)
{
	for(s32 i=0; i<MAX_NUM_LADDERNODE; i++)
	{
		if (m_nodeSelected[i] && m_pControls[i]) {
			Matrix3 tm(1);
			Interval ivalid;

			m_pControls[i]->GetValue(t, &tm, ivalid, CTRL_RELATIVE);
			tm *= node->GetObjectTM(t);
			cb->TM(tm,i);
		}
	}
}

void ladder::ActivateSubobjSel(int selLevel, XFormModes& modes)
{
	if (selLevel) {
		modes = XFormModes(m_moveMode,NULL,NULL,NULL,NULL,m_selectMode);
		NotifyDependents(FOREVER, PART_SUBSEL_TYPE|PART_DISPLAY, REFMSG_CHANGE);
		m_ip->PipeSelLevelChanged();
	}

	if(m_selLevel != selLevel)
	{
		if(selLevel)
		{
			ClosePathUI();
			InitPathNodeUI();
		}
		else
		{
			ClosePathNodeUI();
			InitPathUI();
		}

	}
	m_selLevel = selLevel;

	UpdatePathNodeUI();
}

s32 ladder::AddPathNode()
{
	bool coreInterface = false;

	s32 i=0;

	for(i=0; i<MAX_NUM_LADDERNODE; i++)
	{
		if(m_pPathNodes[i] == NULL)
			break;
	}
	if(i == MAX_NUM_LADDERNODE)
		return -1;

	// if interface pointer is NULL use getCOREInterface to get a pointer
	if(m_ip == NULL)
	{
		m_ip = GetCOREInterface()->GetIObjParam();
		if(m_ip == NULL)
			return -1;
		coreInterface = true;
	}

	ReplaceReference(1 + i, static_cast<Object *>(m_ip->CreateInstance(HELPER_CLASS_ID, LADDERNODE_CLASS_ID)));
	ReplaceReference(1 + MAX_NUM_LADDERNODE + i, static_cast<Control*>(m_ip->CreateInstance(CTRL_MATRIX3_CLASS_ID, Class_ID(PRS_CONTROL_CLASS_ID, 0))));

	NotifyDependents(FOREVER, PART_ALL, REFMSG_SUBANIM_STRUCTURE_CHANGED);

	// if got interface pointer thru getCOREInterface set pointer to NULL
	if(coreInterface)
		m_ip = NULL;

	return i;
}

void ladder::SetPathNodePosn(s32 i, Point3 posn)
{
	Point3 oldPosn = GetPathNodePosn(i);

	SetXFormPacket xform(posn - oldPosn);
	m_pControls[i]->SetValue(0, &xform, 1, CTRL_ABSOLUTE);
}

Point3 ladder::GetPathNodePosn(s32 i)
{
	if((i == -1) || (m_pPathNodes[i] == NULL) || (m_pControls[i] == NULL))
		return Point3(0.0f,0.0f,0.0f);

	Matrix3 tm(1);
	Interval ivalid;

	m_pControls[i]->GetValue(0, &tm, ivalid, CTRL_RELATIVE);
	return tm.GetTrans();
}

Matrix3 ladder::GetPathNodeMatrix(s32 i)
{
	Matrix3 tm(1);
	Interval ivalid;

	if((i == -1) || (m_pPathNodes[i] == NULL) || (m_pControls[i] == NULL))
		return tm;

	m_pControls[i]->GetValue(0, &tm, ivalid, CTRL_RELATIVE);
	return tm;
}

Object* ladder::GetPathNode(s32 i)
{
	return m_pPathNodes[i];
}

void ladder::DeletePathNode(s32 i)
{
	return;
#if 0
	assert(i != -1);
	assert(m_pPathNodes[i] != NULL);
	assert(m_pControls[i] != NULL);

	DeleteReference(1 + i);
	TidyAfterDelete(i);
	NotifyDependents(FOREVER, PART_ALL, REFMSG_SUBANIM_STRUCTURE_CHANGED);
#endif
}

s32 ladder::GetPathNodeId(Object *pPathNode)
{
	for(s32 i=0; i<MAX_NUM_LADDERNODE; i++)
	{
		if(m_pPathNodes[i] == pPathNode)
			return i;
	}
	return -1;
}

s32 ladder::GetControlId(Control *pControl)
{
	for(s32 i=0; i<MAX_NUM_LADDERNODE; i++)
	{
		if(m_pControls[i] == pControl)
			return i;
	}
	return -1;
}

void ladder::TidyAfterDelete(s32 i)
{
	DeleteReference(3 + i);
	m_nodeSelected[i] = false;

	for(s32 j=0; j<MAX_NUM_LADDERNODE; j++)
	{
		if(m_pPathNodes[j] == NULL)
			continue;
		IParamBlock2 *pBlock2 = m_pPathNodes[j]->GetParamBlockByID(laddernode_params);
	}
}

ObjectState ladder::Eval(TimeValue t)
{
	for(int i=0;i<MAX_NUM_LADDERNODE;i++)
	{
		if(m_pPathNodes[i])
		{
			m_pPathNodes[i]->Eval(t);
		}
	}

	return ObjectState(this);
}

int ladder::HitTest(TimeValue t, INode* pNode, IPoint2 p, s32 flags, ViewExp *pVpt, s32 ignore)
{
	for(s32 i=0; i<MAX_NUM_LADDERNODE; i++)
	{
		if(i == ignore)
			continue;
		if(m_pPathNodes[i] &&
			!(flags&HIT_SELONLY && m_nodeSelected[i] == false) &&
				!(flags&HIT_UNSELONLY && m_nodeSelected[i] ==true))
		{
			if(m_pControls[i])
			{
				Matrix3 tm(1);
				Interval ivalid;

				m_pControls[i]->GetValue(t, &tm, ivalid, CTRL_RELATIVE);

				INodeTransformed node(pNode, tm);

				if(m_pPathNodes[i]->HitTest(t, &node, HITTYPE_POINT, FALSE, flags, &p, pVpt)) {
					return i;
				}
			}
		}
	}

	return -1;
}
void ladder::Snap(TimeValue t, INode* inode, SnapInfo *snap, IPoint2 *p, ViewExp *vpt)
{
	Matrix3 tm;
	tm = inode->GetObjectTM(t);	
	
	GraphicsWindow *gw = vpt->getGW();	
	gw->setTransform(tm);
	
	Mesh* pMesh = NULL;
	TriObject* pTri = NULL;
	BOOL del = FALSE;
	if(this->IsSubClassOf(triObjectClassID))
	{
		pTri = (TriObject *) this;
		pMesh = &pTri->GetMesh();
	}
	else if (this->CanConvertToType(triObjectClassID))
	{
		pTri = (TriObject *) this->ConvertToType(t, Class_ID(TRIOBJ_CLASS_ID,0));
		pMesh = &pTri->GetMesh();
		del = TRUE;
	}
	
	pMesh->snap( gw, snap, p, tm );

	if (del) 
	{
		// May reference a controller but no one should reference it.
		pTri->DeleteAllRefsFromMe();
		pTri->DeleteThis();
	}
}

void ladder::InitPathUI()
{
	theladderDesc.BeginEditParams(m_ip, this, BEGIN_EDIT_CREATE, NULL);

	IParamMap2 *pMapParam = m_pBlock2->GetMap();
	m_hPanel = pMapParam->GetHWnd();
	SetDlgItemText(m_hPanel, IDC_VERSION, TSTR("Version ") + GetString(IDS_VERSION));
}

void ladder::ClosePathUI()
{
	if(m_hPanel)
	{
		theladderDesc.EndEditParams(m_ip, this, END_EDIT_REMOVEUI, NULL);
		m_creating = false;
		m_hPanel = NULL;
	}
}

void ladder::SetSelectedPathNodeData()
{
	if(!m_pNodeEdited)
	{
		return;
	}

	IParamBlock2 *pNodeBlock2 = m_pNodeEdited->GetParamBlockByID(laddernode_params);

//	pNodeBlock2->SetValue(laddernode_length,0,mp_spinLength->GetFVal());
//	pNodeBlock2->SetValue(laddernode_width,0,mp_spinWidth->GetFVal());
//	pNodeBlock2->SetValue(laddernode_height,0,mp_spinHeight->GetFVal());

	m_ip->RedrawViews(m_ip->GetTime(), REDRAW_NORMAL, NULL);
}

void ladder::UpdatePathNodeUI()
{
	s32 numSelected = 0;
	Object* pEdited = NULL;
	int iType = 0.0f;
//	float fWidth = 0.0f;
//	float fHeight = 0.0f;

	if(m_selLevel == 0)
		return;

	for(s32 i=0; i<MAX_NUM_LADDERNODE; i++)
	{
		if(m_nodeSelected[i] && m_pPathNodes[i])
		{
			numSelected++;
			pEdited = m_pPathNodes[i];
		}
	}

	if(numSelected != 1)
		pEdited = NULL;

	m_pNodeEdited = pEdited;

	if(m_pNodeEdited)
	{
		IParamBlock2 *pNodeBlock2 = m_pNodeEdited->GetParamBlockByID(laddernode_params);

		iType = pNodeBlock2->GetInt(laddernode_length);
	}

	HWND hDlgItem = GetDlgItem(m_hNodePanel,IDC_EDT_NAME);

	if(iType == 0)
	{
		SetWindowText(hDlgItem,"Bottom");
	}
	else if(iType == 1)
	{
		SetWindowText(hDlgItem,"Top");
	}
	else if(iType == 2)
	{
		SetWindowText(hDlgItem,"Normal");
	}
	

	m_ip->RedrawViews(m_ip->GetTime(), REDRAW_NORMAL, this);
}

void ladder::SelectSubComponent(HitRecord *hitRec, BOOL selected, BOOL all, BOOL invert)
{
	SetSelectedPathNodeData();

	while(hitRec)
	{
		if(selected)
			m_nodeSelected[hitRec->hitInfo] = true;
		else
			m_nodeSelected[hitRec->hitInfo] = false;

		if (all)
			hitRec = hitRec->Next();
		else
			break;
	}

	UpdatePathNodeUI();
	NotifyDependents(FOREVER, PART_SELECT, REFMSG_CHANGE);
}

void ladder::ClearSelection(int selLevel)
{
	SetSelectedPathNodeData();

	if(selLevel)
	{
		for(s32 i=0; i<MAX_NUM_LADDERNODE; i++)
			m_nodeSelected[i] = false;

		UpdatePathNodeUI();
		NotifyDependents(FOREVER, PART_SELECT, REFMSG_CHANGE);
	}
}

void ladder::InitPathNodeUI()
{
	m_hNodePanel = m_ip->AddRollupPage(	hInstance,
										MAKEINTRESOURCE(IDD_NODE_PANEL),
										EditladderNodeDlgProc,
										_T("ladderNode Parameters"),
										(LPARAM)this);

	SetDlgItemText(m_hNodePanel, IDC_VERSION, TSTR("Version ") + GetString(IDS_VERSION));

	UpdatePathNodeUI();
}

void ladder::ClosePathNodeUI()
{
	if(m_hNodePanel)
	{
		m_ip->DeleteRollupPage(m_hNodePanel);

		m_hNodePanel = NULL;
		m_pNodeEdited = NULL;
	}
}
#if !defined( _WIN64 )
BOOL CALLBACK EditladderNodeDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
#else
INT_PTR CALLBACK EditladderNodeDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
#endif
{
	switch(msg)
	{
	case WM_INITDIALOG:
		SetDlgItemText(hWnd, IDC_VERSION, TSTR("Version ") + GetString(IDS_VERSION));
		break;
	case WM_CLOSE:
		break;
	default:
		ladder::SetSelectedPathNodeData();
		return FALSE;
	}
	return TRUE;

}



