//
//
//    Filename: asciitools.cpp
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Utility classes used in ASCII input and output
//
//
#include "asciitools.h"
#include <iostream>
using namespace ascii;


PointerRedirector ascii::thePointerRedirector;

// --- PointerRedirector -------------------------------------------------------------------------------------------

//
//        name: PointerRedirector::AddRedirection
// description: Add redirection into map
//
bool PointerRedirector::AddRedirection(const std::string& name, void* pObj)
{
#if 1
	
	bool bRet = m_namePtrMap.insert(std::make_pair(name, pObj)).second;

	if(bRet == false)
	{
		std::cerr << "error: entry already exists for " << name << std::endl;
	}

	return bRet;

#else
	 return (m_namePtrMap.insert(std::make_pair(name, pObj)).second);
#endif
}
//
//        name: PointerRedirector::Process
// description: Update all the pointers in the request list
//         out: success
//
bool PointerRedirector::Process()
{
	std::list<std::pair<std::string, void**> >::iterator iPtr;
	std::map<std::string, void*>::iterator iName;

	for(iPtr = m_redirRequest.begin(); iPtr != m_redirRequest.end(); iPtr++)
	{
		iName = m_namePtrMap.find((*iPtr).first);
		if(iName == m_namePtrMap.end())
			return false;
		*((*iPtr).second) = (*iName).second;
	}
	Clear();
	return true;
}


//
//        name: Request
// description: Request a pointer from the pointer redirector
//
void* PointerRedirector::Request(const std::string& name)
{
	std::map<std::string, void*>::iterator iName = m_namePtrMap.find(name);
	if(iName == m_namePtrMap.end())
		return NULL;
	return (*iName).second;
}

// --- TabSpaces ---------------------------------------------------------------------------------------------------

std::ostream& operator<<(std::ostream &os, TabSpaces& tabs)
{
	int t = tabs.m_tabs;

	while(t--) 
		os << "    "; 
	return os;
}

