/**********************************************************************
 *<
	FILE: BoxObj2.h

	DESCRIPTION:	Template Utility

	CREATED BY:

	HISTORY:

 *>	Copyright (c) 1997, All Rights Reserved.
 **********************************************************************/

#ifndef __BOXOBJ2__H
#define __BOXOBJ2__H

#include "Max.h"
#include "resource.h"
#include "istdplug.h"
#include "iparamm2.h"
#include "Simpobj.h"


extern TCHAR *GetString(int id);

extern HINSTANCE hInstance;


#define BOXOBJ2_CLASSID	Class_ID(0x32726484, 0x4f90d89)

// Paramblock2 name
enum { box2_params, }; 
// Paramblock2 parameter list
enum { 
	box2_length, 
	box2_width, 
	box2_height,
	box2_widthSegs,
	box2_lengthSegs,
	box2_heightSegs,
	box2_genUVs,
	box2_top,
	box2_bottom,
	box2_front,
	box2_back,
	box2_left,
	box2_right
};


class BoxObject2 : public SimpleObject2
{
public:
	// Class vars
//	static IParamMap *pmapCreate;
//	static IParamMap *pmapTypeIn;
//	static IParamMap *pmapParam;		
	static IObjParam *ip;
	static int createMeth;
	static Point3 crtPos;		
	static float crtWidth, crtHeight, crtLength;
	
	BoxObject2();
	
	// From Object
	int CanConvertToType(Class_ID obtype);
	Object* ConvertToType(TimeValue t, Class_ID obtype);
	void GetCollapseTypes(Tab<Class_ID> &clist,Tab<TSTR*> &nlist);
	
	// From BaseObject
	CreateMouseCallBack* GetCreateMouseCallBack();
	void BeginEditParams( IObjParam *ip, ULONG flags,Animatable *prev);
	void EndEditParams( IObjParam *ip, ULONG flags,Animatable *next);
	TCHAR *GetObjectName() { return GetString(IDS_OBJ_NAME); }
	BOOL HasUVW();
	void SetGenUVW(BOOL sw);
	
	// Animatable methods
	int NumParamBlocks() {return 1;}
	IParamBlock2* GetParamBlock(int i) {assert(i == 0); return pblock2;}
	IParamBlock2* GetParamBlockByID(short id) {assert(id == box2_params); return pblock2;}
	void DeleteThis() { delete this; }
	Class_ID ClassID() { return BOXOBJ2_CLASSID; }  
	
	// From ref
#if MAX_VERSION_MAJOR >= 9 
	RefTargetHandle Clone(RemapDir& remap = DefaultRemapDir());
#else
	RefTargetHandle Clone(RemapDir& remap = NoRemap());
#endif
	IOResult Load(ILoad *iload);
	
	// From SimpleObject
	void BuildMesh(TimeValue t);
	BOOL OKtoDisplay(TimeValue t);
	void InvalidateUI();
};				

//--- ClassDescriptor -----------------------------------------------------------------------------------

class BoxObj2ClassDesc2 : public ClassDesc2 {
public:
	int 			IsPublic() { return 1; }
	void *			Create(BOOL loading = FALSE) {return new BoxObject2;}
	const TCHAR *	ClassName() { return GetString(IDS_CLASS_NAME); }
	SClass_ID		SuperClassID() { return GEOMOBJECT_CLASS_ID; }
	Class_ID		ClassID() { return BOXOBJ2_CLASSID; }
	const TCHAR* 	Category() { return GetString(IDS_CATEGORY);}	
	void			ResetClassParams(BOOL fileReset);

	// Hardwired name, used by MAX Script as unique identifier
	const TCHAR*	InternalName()				{ return _T("Box2"); } // can't use Box2 as it is already a script command
	HINSTANCE		HInstance()					{ return hInstance; }
};


#endif // __BOXOBJ2__H
