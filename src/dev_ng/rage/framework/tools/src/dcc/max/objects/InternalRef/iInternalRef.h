

#ifndef __IINTERNALREF__H
#define __IINTERNALREF__H

#include "iFnPub.h"
#include "maxtypes.h"


class IInternalRef : public FPMixinInterface 
{
	public:
    
		virtual void		SetRefdObj(float value, TimeValue time) = 0;
		virtual float		GetRefdObj(TimeValue time, Interval& valid = FOREVER) const = 0 ;

		
		enum 
		{ 
			kLUM_SET_REFD_OBJ, 
			kLUM_GET_REFD_OBJ, 
		}; 
	
}; 

// Luminaire interface ID
#define INTERNREF_INTERFACE Interface_ID(0x7e631fe1, 0x7163389b)

inline IInternalRef* GetInternRefInterface(BaseInterface* baseIfc)	
{ DbgAssert( baseIfc != NULL); return static_cast<IInternalRef*>(baseIfc->GetInterface(INTERNREF_INTERFACE)); }


#endif
