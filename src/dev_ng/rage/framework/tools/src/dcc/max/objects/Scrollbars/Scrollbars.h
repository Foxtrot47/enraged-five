//
// filename:	Scrollbars.h
// author:		David Muir
// date:		23 January 2007
// description:	Scrollbars Object and associated descriptor class definitions
//

#ifndef INC_SCROLLBARS_H_
#define INC_SCROLLBARS_H_

#pragma warning( push )
#pragma warning( disable : 4275 )

// --- Include Files ------------------------------------------------------------

// 3D Studio Max SDK headers
#include "Max.h"
#include "plugapi.h"
#include "iparamb2.h"

// Local headers
class CScrollbarsPoint;

// --- Defines ------------------------------------------------------------------
#ifdef SCROLLBARS_EXPORTS
#define SCROLLBARS_EXPORTS_API __declspec(dllexport)
#else
#define SCROLLBARS_EXPORTS_API __declspec(dllimport)
#endif

using namespace rage;

// --- Constants ----------------------------------------------------------------
const int sC_cnt_MaxNumberOfPoints = 100;
const int sC_cnt_DefaultNumberOfPoints	= 8;

// --- External -----------------------------------------------------------------
extern HINSTANCE g_hInstance;

// --- Constants ----------------------------------------------------------------
extern const Class_ID sC_cid_Scrollbars;

// --- Structure/Class Definitions ----------------------------------------------

//
// class:		Scrollbars Helper Object class definition
// description:	Main Scrollbars object
//
class SCROLLBARS_EXPORTS_API CScrollbars : public HelperObject
{
public:
	CScrollbars( );
	~CScrollbars( );

	// Sub-Object Stuff
	void			Move( TimeValue t, Matrix3& partm, Matrix3& tmAxis, 
						  Point3& val, BOOL bLocalOrigin );
	int				HitTest( TimeValue t, INode* pNode, int type, int crossing, 
							 int flags, IPoint2* p, ViewExp* pVpt, ModContext* pMc );
	void			SelectSubComponent( HitRecord* pHitRec, BOOL bSelected, 
										BOOL bAll, BOOL bInvert );
	void			ClearSelection( int nSelLevel );
	int				SubObjectIndex( HitRecord* pHitRec );
	void			GetSubObjectCenters( SubObjAxisCallback* pCB, TimeValue t, 
										 INode* pNode, ModContext* pMc );
	void			GetSubObjectTMs( SubObjAxisCallback* pCB, TimeValue t, 
									 INode* pNode, ModContext* pMc );
	void			ActivateSubobjSel( int level, XFormModes& modes );

	static void		SetSelectedPointData( );

	// Sub-Anims
	int				NumSubs( );
	Animatable*		SubAnim( int nIndex );
	TSTR			SubAnimName( int nIndex );

	// Animatable methods
	int				NumParamBlocks( );
	IParamBlock2*	GetParamBlock( int nIndex );
	IParamBlock2*	GetParamBlockByID( short id );
	Class_ID		ClassID( );
	void			GetClassName( TSTR& sClassName );
	void			BeginEditParams( IObjParam* ip, ULONG flags, Animatable* pPrev );
	void			EndEditParams( IObjParam* ip, ULONG flags, Animatable* pNext );

	// Rendering
	void			Draw( TimeValue t, INode* pNode, ViewExp* pVpt, int flags );

	// ReferenceTarget methods
#if MAX_VERSION_MAJOR >= 9 
	ReferenceTarget* Clone( RemapDir &remap = DefaultRemapDir() );
#else
	ReferenceTarget* Clone( RemapDir &remap = NoRemap() );
#endif
	IOResult		Load( ILoad* iload );
	IOResult		Save( ISave* isave );
	int				NumRefs( );
	RefTargetHandle	GetReference( int nIndex );
	void			SetReference( int nIndex, RefTargetHandle pTArg );
	RefResult		NotifyRefChanged( Interval changeInt, RefTargetHandle hTarget, 
									  PartID& partID, RefMessage message );

	// User-Interface
	void			InitScrollbarsUI( );
	void			UpdateScrollbarsUI( );
	void			CloseScrollbarsUI( );
	void			InitScrollbarsPointUI( );
	void			CloseScrollbarsPointUI( );

	// Object
	ObjectState		Eval( TimeValue t );
	Interval		ObjectValidity(TimeValue t);
	void			InitNodeName( TSTR& sName );

	// BaseObject
	CreateMouseCallBack* GetCreateMouseCallBack();
	void			GetLocalBoundBox(TimeValue t, INode* pNode, ViewExp* pVp, Box3& box);
	void			GetWorldBoundBox(TimeValue t, INode* pNode, ViewExp* pVp, Box3& box);
	int				Display(TimeValue t, INode* pNode, ViewExp *pVpt, int flags);
	int				HitTest(TimeValue t, INode* pNode, int type, int crossing, int flags, IPoint2 *pSp, ViewExp *pVpt);
	TCHAR*			GetObjectName( );
	int				NumSubObjTypes( );
	ISubObjType*	GetSubObjType( int nIndex );
	void			SelectAll( int selLevel );
		
	// Sub-Object Construction, Selection etc.
	s32			GetNumPoints( );
	s32			AddPoint( );
	void			SetPointPosition( s32 nIndex, Point3 pos );
	Point3			GetPointPosition( s32 nIndex );
	void			SelectPoint( s32 nIndex, bool bSelect = true );
	Matrix3			GetPointMatrix( s32 nIndex );
	CScrollbarsPoint* GetScrollbarPoint( s32 nIndex );
	void			DeletePoint( s32 nIndex );
	s32			GetPointId( Object* pPointObject );
	s32			GetControlId( Control* pControl );
	void			TidyAfterDelete(s32 i);
	int				HitTest( TimeValue t, INode* pNnode, IPoint2 p, s32 flags, ViewExp* vpt, s32 ignore = -1 );

	int				GetVisibleNumPoints( );
	void			SetVisibleNumPoints( int nPoints );

private:
	IParamBlock2*					m_pParamBlock;	//!< Parameter Block for each Point
	CScrollbarsPoint*				m_vpPoints[sC_cnt_MaxNumberOfPoints];	//!< Scrollbars Points
	Control*						m_vpControls[sC_cnt_MaxNumberOfPoints];	//!< Controls
	bool							m_vbSelected[sC_cnt_MaxNumberOfPoints];	//!< Selected nodes

	s32							m_SelLevel;		//!< Sub-object Selection Level
	
	// Static Member Data
	static HWND						m_hScrollbarUI;	//!< Scrollbars Editing Dialog	
	static HWND						m_hPointUI;		//!< Scrollbars Point Editing Dialog
	static IObjParam*				m_pInterface;	//!< Object Parameter Interface
	static Object*					m_pPointEdited;	//!< Scrollbar point being edited
	static bool						m_bCreating;	//!< Scrollbars object being created?
	static MoveModBoxCMode*			m_pMoveMode;	//!< Move Scollbars sub-object modifier
	static SelectModBoxCMode*		m_pSelectMode;	//!< Select Scrollbars sub-object modifier
};

//
// class:		CScrollbarsDesc
// description:	CScrollbars Max Descriptior Class
//
class CScrollbarsDesc : public ClassDesc2
{
public:
	int				IsPublic( );
	void*			Create( BOOL bLoading = FALSE );
	const TCHAR*	ClassName( );
	SClass_ID		SuperClassID( );
	Class_ID		ClassID( );
	const TCHAR*	Category( );

	void			ResetClassParams( BOOL bFileReset );
	int				BeginCreate( Interface* pInterface );
	int				EndCreate( Interface* pInterface );

	const TCHAR*	InternalName( );
	HINSTANCE		HInstance( );
};

// --- Globals ------------------------------------------------------------------
ClassDesc2* GetScrollbarsDesc( );

#pragma warning( pop )

#endif // !INC_SCROLLBARS_H_

// End of file
