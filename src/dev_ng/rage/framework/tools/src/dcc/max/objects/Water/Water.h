#ifndef __INTERIOR__H
#define __INTERIOR__H

#include "Max.h"
#include "resource.h"
#include "istdplug.h"
#include "iparamb2.h"
#include "iparamm2.h"

extern TCHAR *GetString(int id);

extern HINSTANCE hInstance;

#define WATER_BASE_CLASS_ID Class_ID(0x7c01069f, 0x548426c8)
enum
{
	WATER_BASE_PBLOCK_REF,
	WATER_EXTEND_PBLOCK_REF,
	WATER_MESH_REF
};

#define IMAGE_W 16
#define IMAGE_H IMAGE_W
#define WATER_MOD_FACTOR 10000
#define WATER_TILE_SIZE 4

#define WATER_INITIAL_VERSION 1
#define WATER_HELPER_CURRENT_VERSION WATER_INITIAL_VERSION
#define WATER_CALMER_CURRENT_VERSION WATER_INITIAL_VERSION
#define WATER_WAVE_CURRENT_VERSION WATER_INITIAL_VERSION

#ifdef WATER_EXPORTS
#define WATER_API __declspec(dllexport)
#else
#define WATER_API __declspec(dllimport)
#endif

extern ParamBlockDesc2 waterPBlockDesc;

/////////////////////////////////////////////////////////////////////////////////////////////////
enum
{
	water_base_params,
	water_prim_params,
	water_calm_params,
	water_wave_params,
	water_river_params
};

/////////////////////////////////////////////////////////////////////////////////////////////////
enum 
{ 
	water_length, 
	water_width,
	water_snapGrid,
	water_mesh,
	water_type,
	water_calming,
	water_alpha0,
	water_alpha1,
	water_alpha2,
	water_alpha3,
	water_amplitude,
	water_wave_direction,
	water_alpha_int0,
	water_alpha_int1,
	water_alpha_int2,
	water_alpha_int3,
};	

class Water;

class WaterPostLoad : public PostLoadCallback {
public:
	Water *wh;
	WaterPostLoad(Water *b) {wh=b;}
	void proc(ILoad *iload);
};
/////////////////////////////////////////////////////////////////////////////////////////////////
class Water : public HelperObject
/////////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	Water(ClassDesc2 *derivedDesc);
	~Water();

	// From BaseObject
	CreateMouseCallBack* GetCreateMouseCallBack();
	void BeginEditParams( IObjParam *ip, ULONG flags,Animatable *prev);
	void EndEditParams( IObjParam *ip, ULONG flags,Animatable *next);
	TCHAR *GetObjectName() { return GetString(IDS_BASE_CLASS_NAME); }
	int Display(TimeValue t, INode* inode, ViewExp *vpt, int flags);
	void GetWorldBoundBox(TimeValue t, INode* inode, ViewExp* vp, Box3& box );
	void GetLocalBoundBox(TimeValue t, INode* inode, ViewExp* vp, Box3& box );
	int HitTest(TimeValue t, INode* inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt);

	// Animatable methods
	int NumParamBlocks() {return 2;}
	IParamBlock2* GetParamBlock(int i) 
	{
		assert(i < 2); 
		switch(i)
		{
		case 0:
			return pBasePblock2;
		default:
			return pExtendPblock2;
		}
	}
	IParamBlock2* GetParamBlockByID(short id) 
	{
		switch(id)
		{
		case water_base_params:
			return pBasePblock2;
		case water_calm_params:
		case water_prim_params:
		case water_wave_params:
		case water_river_params:
			return pExtendPblock2;
		default:
			assert(0);
		}
		return NULL;
	}
	void DeleteThis() { delete this; }
	Class_ID ClassID() { return WATER_BASE_CLASS_ID; }  
	void GetClassName(TSTR& s){	s = GetString(IDS_BASE_CLASS_NAME);}

	int NumRefs() { return 3; }
	RefTargetHandle GetReference(int i);
	void SetReference(int i, RefTargetHandle rtarg);
	RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,RefMessage message);
	ObjectState Eval(TimeValue t);
	
	virtual void PostLoadHook(ILoad *iload)
	{
		/**/
	}
	IOResult Load(ILoad *iload)
	{
		iload->RegisterPostLoadCallback(new WaterPostLoad(this));
		return IO_OK;
	}

	// From ref
	RefTargetHandle Clone(RemapDir& remap = DefaultRemapDir()) = 0;
	
	// From object
	void InitNodeName(TSTR& s){	s = GetString(IDS_BASE_CLASS_NAME);}

	// From SimpleObject
	void BuildMesh(TimeValue t);
	BOOL OKtoDisplay(TimeValue t);
	void InvalidateUI();

	float SnapValue(float fVal);
	void DrawMesh(GraphicsWindow *gw, TimeValue t, Matrix3& objMat);

	IParamBlock2* pBasePblock2;
	IParamBlock2* pExtendPblock2;
	TriObject* m_pTriObject;

	// Custom funcs
//	void	addBox(Point3 pntCentre,Point3 pntDimension,int& iVertOffset,int& iFaceOffset);
	virtual int		GetType() = 0;
	virtual bool	ShowTypeButton() = 0;
	virtual float	GetAlphaFloat(int index) = 0;
	virtual int		GetAlpha(int index) = 0;
	// Should be protected
	virtual void	BuildSpecificMesh(TimeValue t, Mesh &mesh, Point3 centre, Point3 dim) = 0; //{};

	HIMAGELIST hImageFO;
	ICustButton * typeButton;

	Point3 mLineColour;
};	

class WaterDlgProc : public ParamMap2UserDlgProc 
{ 
public:  
	Water *m_pw;  
	WaterDlgProc(Water *pw) 
	{
		m_pw=pw;
	}
	INT_PTR DlgProc(TimeValue t,IParamMap2 *map, HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam); 
	void DeleteThis() {delete this;} 
};

ParamBlockDesc2 *CreateWaterBasePBlock2(ClassDesc2 *cd, char *paramBlockName);
// #define CREATE_WATER_PBLOCK(cd,res) 																	\
// 	new ParamBlockDesc2(																				\
// 	water_base_params,																					\
// 	_T("Water Parameters"), 																			\
// 	IDS_WATER_NAME, 																					\
// 	cd, 																								\
// 	P_AUTO_CONSTRUCT | P_AUTO_UI, 																		\
// 	WATER_BASE_PBLOCK_REF,																				\
// 	res, IDS_PARAMS, 0, 0, NULL,																		\
// 	water_length, _T("Length"), TYPE_WORLD, P_ANIMATABLE, IDS_LENGTH,									\
// 	p_default,		4.0,																				\
// 	p_range, 		0.0, 444444.0, 																		\
// 	p_ui,			TYPE_SPINNER, EDITTYPE_INT, IDC_LENGTHEDIT, IDC_LENSPINNER, float(WATER_TILE_SIZE),	\
// 	end,																								\
// 	water_width, _T("Width"), TYPE_WORLD, P_ANIMATABLE, IDS_WIDTH,										\
// 	p_default,		4.0,																				\
// 	p_range, 		0.0, 444444.0, 																		\
// 	p_ui,			TYPE_SPINNER, EDITTYPE_INT, IDC_WIDTHEDIT, IDC_WIDTHSPINNER, float(WATER_TILE_SIZE),\
// 	end, 																								\
// 	water_snapGrid, _T("snapGrid"), TYPE_INT, P_ANIMATABLE, IDS_SNAPGRID,								\
// 	p_default,		WATER_TILE_SIZE,																	\
// 	p_range, 		1, 1000, 																			\
// 	p_ui,			TYPE_SPINNER, EDITTYPE_INT, IDC_SNAPGRID, IDC_SPIN_SNAPGRID, 1,						\
// 	end,																								\
// 	end																									\
// 	);

#endif // __INTERIOR__H
