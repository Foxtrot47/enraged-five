// Author : Mike Wilson <mike.wilson@rockstartoronto.com>

#include "animationLoader.h"

#include "math/amath.h"
#include "vector/matrix34.h"
#include "vectormath/legacyconvert.h"
#include "math/simplemath.h"

using namespace rage;

void CopyMaxMatrixToRageMatrix(const Matrix3& r_SrcMat,Matrix34& r_DstMat)
{
	Point4 SrcRow;

	SrcRow = r_SrcMat.GetRow(0);
	r_DstMat.a = Vector3(SrcRow[0],SrcRow[1],SrcRow[2]);
	SrcRow = r_SrcMat.GetRow(1);
	r_DstMat.b = Vector3(SrcRow[0],SrcRow[1],SrcRow[2]);
	SrcRow = r_SrcMat.GetRow(2);
	r_DstMat.c = Vector3(SrcRow[0],SrcRow[1],SrcRow[2]);
	SrcRow = r_SrcMat.GetRow(3);
	r_DstMat.d = Vector3(SrcRow[0],SrcRow[1],SrcRow[2]);
}

void CopyRageMatrixToMaxMatrix(const Matrix34& r_SrcMat,Matrix3& r_DstMat)
{
	Point3 SrcRow;

	SrcRow = Point3(r_SrcMat.a.x,r_SrcMat.a.y,r_SrcMat.a.z);
	r_DstMat.SetRow(0,SrcRow);
	SrcRow = Point3(r_SrcMat.b.x,r_SrcMat.b.y,r_SrcMat.b.z);
	r_DstMat.SetRow(1,SrcRow);
	SrcRow = Point3(r_SrcMat.c.x,r_SrcMat.c.y,r_SrcMat.c.z);
	r_DstMat.SetRow(2,SrcRow);
	SrcRow = Point3(r_SrcMat.d.x,r_SrcMat.d.y,r_SrcMat.d.z);
	r_DstMat.SetRow(3,SrcRow);
}

void PrintToListener(const char * pMessage, ...)
{
	char cMsg[256*4];
	memset(cMsg,0,256*4);
	va_list args;
	va_start (args, pMessage);
	vsprintf_s (cMsg, 256*4, pMessage, args);
	va_end (args);

	mprintf(cMsg);
	mprintf("\n");
}

CAnimationLoader::CAnimationLoader()
{
	crAnimation::InitClass();
}

CAnimationLoader::~CAnimationLoader(){}

void CAnimationLoader::Apply(INode* pNode, const char* pAnimationFile)
{
	PrintToListener("*****************************************");
	PrintToListener("Animation Importer");
	PrintToListener("*****************************************");

	Assert(pNode != NULL);
	Assert(pAnimationFile != NULL);

	atMap<unsigned short, atString> mapBoneHashs;
	if(!GetBoneHashsFromNode(pNode, mapBoneHashs)) return;

	//crAnimation* pAnim = crAnimation::AllocateAndLoad("X:/gta5/art/anim/export_mb/Map_Objects/BarrelSDT_front_25mph.anim");
	crAnimation* pAnim = crAnimation::AllocateAndLoad(pAnimationFile);

	if(pAnim)
	{
		PrintToListener("Loaded animation '%s'", pAnimationFile);
		PrintToListener("'%d' internal frames.", pAnim->GetNumInternalFrames());
		PrintToListener("'%d' animation tracks.", pAnim->GetNumTracks());

		// Calculate the local matrices of each bone and store if needed later. We could do this on the fly but Max is crap and returns garbage 
		// transforms when setting keys also.
		atMap<atString, Matrix34> mapBoneNameToLocal;
		GetLocalMatrices(mapBoneHashs, mapBoneNameToLocal);

		//PlotOnObject(pNode, pAnim, 0, atString("SKEL_ROOT"), mapBoneNameToLocal);

		// Iterate each bone and plot the key frames from the anim file
		atMap<unsigned short, atString>::Iterator entry = mapBoneHashs.CreateIterator();
		for (entry.Start(); !entry.AtEnd(); entry.Next())
		{
			PlotOnObject(pNode, pAnim, entry.GetKey(), entry.GetData(), mapBoneNameToLocal);
		}
	}
	else
	{
		char cMsg[256];
		sprintf(cMsg, "Unable to load animation file '%s'.", pAnimationFile);
		MessageBox(NULL,cMsg, "Error", MB_ICONERROR );
	}
}

void CAnimationLoader::GetLocalMatrices(atMap<unsigned short, atString>& mapBoneHashs, atMap<atString, Matrix34>& mapBoneNameToLocal)
{
	// Get local matrix from frame 0 on every bone.
	atMap<unsigned short, atString>::Iterator entry = mapBoneHashs.CreateIterator();
	for (entry.Start(); !entry.AtEnd(); entry.Next())
	{
		INode* pBoneNode = GetCOREInterface()->GetINodeByName(entry.GetData());
		if(pBoneNode)
		{
			Matrix3 mBoneMatrix = pBoneNode->GetNodeTM(0);
			Matrix34 BoneTransform;
			BoneTransform.Identity();
			CopyMaxMatrixToRageMatrix(mBoneMatrix, BoneTransform);

			Matrix3 mParentBoneMatrix = pBoneNode->GetParentTM(0);
			Matrix34 ParentBoneTransform;
			ParentBoneTransform.Identity();
			CopyMaxMatrixToRageMatrix(mParentBoneMatrix, ParentBoneTransform);

			BoneTransform.DotTranspose(ParentBoneTransform);

			mapBoneNameToLocal.Insert(atString(pBoneNode->GetName()), BoneTransform);
		}
	}
}

// plot the keyframes from the tracks in the anim on the specific bone
void CAnimationLoader::PlotOnObject(INode* pNode, crAnimation* pAnim, unsigned short usHash, atString& strName, atMap<atString, Matrix34> mapBoneNameToLocal)
{
	int iNumTracks = pAnim->GetNumTracks();

	PrintToListener("Attempting to process bone '%s'/%d.", strName.c_str(), usHash);

	for(int i=0; i < iNumTracks; ++i)
	{
		crAnimTrack* pAnimTrack = pAnim->GetTrack(i);

		if(pAnimTrack->GetId() == usHash)
		{
			INode* pBoneNode = GetCOREInterface()->GetINodeByName(strName.c_str());
			if(pBoneNode)
			{
				Control* pCtrl = NULL;

				switch(pAnimTrack->GetTrack())
				{
				case kTrackBoneTranslation:
					{
/*						if(strncmp("mover",strName.c_str(),strlen("mover")) == 0)
							break;		*/	

						PrintToListener("Processing kTrackBoneTranslation track for bone '%s'/%d.", strName.c_str(), usHash);
						
						pCtrl = pBoneNode->GetTMController()->GetPositionController();
					}
					break;
				case kTrackMoverTranslation:
					{
						PrintToListener("Processing kTrackMoverTranslation track for bone '%s'/%d.", strName.c_str(), usHash);

						pCtrl = pBoneNode->GetTMController()->GetPositionController();
					}
					break;
				case kTrackBoneRotation:
					{
/*						if(strncmp("mover",strName.c_str(),strlen("mover")) == 0)
							break;	*/

						PrintToListener("Processing kTrackBoneRotation track for bone '%s'/%d.", strName.c_str(), usHash);
						
						pCtrl = pBoneNode->GetTMController()->GetRotationController();
					}
					break;
				case kTrackMoverRotation:
					{
						PrintToListener("Processing kTrackMoverRotation track for bone '%s'/%d.", strName.c_str(), usHash);

						pCtrl = pBoneNode->GetTMController()->GetRotationController();
					}
					break;
				case kTrackBoneScale:
					{
						//pCtrl = pBoneNode->GetTMController()->GetScaleController();
					}
					break;
				}
				
				if(pCtrl)
				{
					Control* pCtrlX = (Control*)pCtrl->SubAnim(0);
					Control* pCtrlY = (Control*)pCtrl->SubAnim(1);
					Control* pCtrlZ = (Control*)pCtrl->SubAnim(2);

					if(pCtrlX && pCtrlY && pCtrlZ)
					{
						IKeyControl* pKeysX = GetKeyControlInterface(pCtrlX);
						IKeyControl* pKeysY = GetKeyControlInterface(pCtrlY);
						IKeyControl* pKeysZ = GetKeyControlInterface(pCtrlZ);

						bool bZeroControl = false;

						if(!pKeysX || !pKeysY || !pKeysZ)
						{
							// Zero based controllers - if we have a zero based controller then the bones local matrix is baked. We need to remove
							// the local matrix from the key value and key the remainder. For non zero controls we just key the animation value.
							Control* pZero = (Control*)pCtrl->SubAnim(1);
							if(pZero)
							{
								pCtrlX = (Control*)pZero->SubAnim(0);
								pCtrlY = (Control*)pZero->SubAnim(1);
								pCtrlZ = (Control*)pZero->SubAnim(2);

								if(pCtrlX && pCtrlY && pCtrlZ)
								{
									pKeysX = GetKeyControlInterface(pCtrlX);
									pKeysY = GetKeyControlInterface(pCtrlY);
									pKeysZ = GetKeyControlInterface(pCtrlZ);
									bZeroControl = true;
								}	
							}
						}

						if(pKeysX && pKeysY && pKeysZ)
						{
							pKeysX->SetNumKeys(pAnim->GetNumInternalFrames());
							pKeysY->SetNumKeys(pAnim->GetNumInternalFrames());
							pKeysZ->SetNumKeys(pAnim->GetNumInternalFrames());

							for(int j=0; j<pAnim->GetNumInternalFrames(); ++j)
							{
								float t = float(j);

								switch(pAnimTrack->GetType())
								{
								case kFormatTypeVector3:
									{	
										Vec3V v;
										pAnimTrack->EvaluateVector3(t, v);

										TimeValue tValue = (int)((float(j/30.0f))*4800.0f);

										Matrix34* ptrMatrix = mapBoneNameToLocal.Access(atString(strName.c_str()));
										if(ptrMatrix)
										{
											IBezFloatKey tcbPosKeyX;
											tcbPosKeyX.time = tValue;
											if(bZeroControl)
											{
												tcbPosKeyX.val = v.GetX().Getf() - ptrMatrix->d.x;
											}
											else
											{
												tcbPosKeyX.val = v.GetX().Getf();
											}

											pKeysX->SetKey(j, &tcbPosKeyX);

											IBezFloatKey tcbPosKeyY;
											tcbPosKeyY.time = tValue;
											if(bZeroControl)
											{
												tcbPosKeyY.val = v.GetY().Getf() - ptrMatrix->d.y;
											}
											else
											{
												tcbPosKeyY.val = v.GetY().Getf();
											}

											pKeysY->SetKey(j, &tcbPosKeyY);

											IBezFloatKey tcbPosKeyZ;
											tcbPosKeyZ.time = tValue;
											if(bZeroControl)
											{
												tcbPosKeyZ.val = v.GetZ().Getf() - ptrMatrix->d.z;
											}
											else
											{
												tcbPosKeyZ.val = v.GetZ().Getf();
											}

											pKeysZ->SetKey(j, &tcbPosKeyZ);
										}
									}
									break;
								case kFormatTypeQuaternion:
									{
										QuatV q;
										pAnimTrack->EvaluateQuaternion(t, q);
										Vec3V e = QuatVToEulersXYZ(q);
										
										TimeValue tValue = (int)((float(j/30.0f))*4800.0f);

										Matrix34* ptrMatrix = mapBoneNameToLocal.Access(atString(strName.c_str()));
										if(ptrMatrix)
										{
											Vector3 vec;
											vec.Zero();
											ptrMatrix->ToEulersXYZ(vec);
										
										
											IBezFloatKey tcbRotKeyX;
											tcbRotKeyX.time = tValue;
											if(bZeroControl)
											{
												tcbRotKeyX.val = e[0] - vec.x;
											}
											else
											{
												tcbRotKeyX.val = e[0];
											}

											pKeysX->SetKey(j, &tcbRotKeyX);

											IBezFloatKey tcbRotKeyY;
											tcbRotKeyY.time = tValue;
											if(bZeroControl)
											{
												tcbRotKeyY.val = e[1] - vec.y;
											}
											else
											{
												tcbRotKeyY.val = e[1];
											}

											pKeysY->SetKey(j, &tcbRotKeyY);

											IBezFloatKey tcbRotKeyZ;
											tcbRotKeyZ.time = tValue;
											if(bZeroControl)
											{
												tcbRotKeyZ.val = e[2] - vec.z;
											}
											else
											{
												tcbRotKeyZ.val = e[2];
											}

											pKeysZ->SetKey(j, &tcbRotKeyZ);
										}
										
									}
									break;
								}
							}
						}
					}
				}
			}
		}
	}
}

// get all the bones from the node, then create a map of hash to bone name. we will use this to lookup the bone in the anim file
bool CAnimationLoader::GetBoneHashsFromNode(INode* pNode, atMap<unsigned short, atString>& mapBoneHashs)
{
	ISkin* pSkin = GetSkin(pNode);
	if(pSkin)
	{
		int iNumBones = pSkin->GetNumBones();
		for(int i=0 ; i < iNumBones; ++i)
		{
			INode* pBoneNode = GetCOREInterface()->GetINodeByName(pSkin->GetBoneName(i));
			if(pBoneNode)
			{
				// get the id from the udp, else hash the bone name
				std::string outId;
				if(GetBoneID(pBoneNode, outId))
				{
					mapBoneHashs[atHash16U(outId.c_str())] = pSkin->GetBoneName(i);
				}
				else
				{
					mapBoneHashs[atHash16U(pSkin->GetBoneName(i))] = pSkin->GetBoneName(i);
				}
			}
		}
	}
	else
	{
		char cMsg[256];
		sprintf(cMsg, "Object '%s' has no skin modifier applied.", pNode->GetName());
		MessageBox(NULL,cMsg,"Error", MB_ICONERROR );

		return false;
	}

	return true;
}

INode* CAnimationLoader::GetSkinRootBone(ISkin* p_Skin)
{
	//come back to this, but works in the test...

	if(!p_Skin)
		return NULL;

	// DHM 26 May 2009
	// Fix for when a mesh has a Skin modifier with no bones defined.
	// This could be integrated with the NumBones int below.
	if ( 0 == p_Skin->GetNumBones() )
		return NULL;

	INode* p_RootMostBone = p_Skin->GetBone(0);
	int i,NumBones = p_Skin->GetNumBones();

	for(i=1;i<NumBones;i++)
	{
		INode* p_NewBone = p_Skin->GetBone(i);

		int NumBones = GetNumLevels(p_NewBone);
		int NumRootBones = GetNumLevels(p_RootMostBone);

		TCHAR* pName = p_NewBone->GetName();
		TCHAR* pRootName = p_RootMostBone->GetName();

		if(NumBones < NumRootBones)	
		{
			p_RootMostBone = p_NewBone;
		}
	}

	return p_RootMostBone;
}

s32 CAnimationLoader::GetNumLevels(INode* p_Node)
{
	s32 Levels = 0;
	INode* p_RootNode = GetCOREInterface()->GetRootNode();

	while(p_Node != p_RootNode)
	{
		p_Node = p_Node->GetParentNode();
		Levels++;
	}

	return Levels;
}

bool CAnimationLoader::GetBoneID(INode* pNode, std::string& outId)
{
	outId = "";

	TSTR userProp;
	if(pNode->GetUserPropString("tag",userProp))
	{
		if(strcmp(userProp,"0") == 0)
		{
			outId = "#0";
		}
		else
		{
			outId = userProp;
		}

		return true;
	}

	return false;
}

// get the skin modifier from the node
ISkin* CAnimationLoader::GetSkin(INode* p_Node)
{
	Object* p_Obj = p_Node->GetObjectRef();

	if(!p_Obj)
		return NULL;

	while(p_Obj->SuperClassID() == GEN_DERIVOB_CLASS_ID)
	{
		IDerivedObject* p_DerObj = (IDerivedObject*)(p_Obj);

		s32 Idx = 0,Count = p_DerObj->NumModifiers();

		while(Idx < Count)
		{
			Modifier* p_Mod = p_DerObj->GetModifier(Idx);

			if(p_Mod->ClassID() == SKIN_CLASSID)
				return ((ISkin*)p_Mod->GetInterface(I_SKIN));

			Idx++;
		}

		p_Obj = p_DerObj->GetObjRef();
	}

	return NULL;
}