//
//
//    Filename: DllEntry.cpp
//     Creator: SDKAPWZ
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Dll functions for 3dsmax plugin
//
//
#ifndef INC_DLL_ENTRY_H_
#define INC_DLL_ENTRY_H_

#include <Max.h>
#include "resource.h"

TCHAR *GetString(int id);
ClassDesc* GetPatrolNodeDesc();
ClassDesc* GetPatrolLinkDesc();

HINSTANCE hInstance;
int controlsInit = FALSE;

// This function is called by Windows when the DLL is loaded.  This 
// function may also be called many times during time critical operations
// like rendering.  Therefore developers need to be careful what they
// do inside this function.  In the code below, note how after the DLL is
// loaded the first time only a few statements are executed.

BOOL WINAPI DllMain(HINSTANCE hinstDLL,ULONG fdwReason,LPVOID lpvReserved)
{
	hInstance = hinstDLL;				// Hang on to this DLL's instance handle.

	return (TRUE);
}

// This function returns a string that describes the DLL and where the user
// could purchase the DLL if they don't have it.
__declspec( dllexport ) const TCHAR* LibDescription()
{
	return _T("Object describing scene for Vehicle code");
}

// This function returns the number of plug-in classes this DLL
//TODO: Must change this number when adding a new class
__declspec( dllexport ) int LibNumberClasses()
{
	return 2;
}

// This function returns the number of plug-in classes this DLL
__declspec( dllexport ) ClassDesc* LibClassDesc(int i)
{
	switch(i) {
		case 0: 
			return GetPatrolNodeDesc();
		case 1: 
			return GetPatrolLinkDesc();
		case 2: 
//			return GetVehiclePathDesc();
		default: 
			return 0;
	}
}

// This function returns a pre-defined constant indicating the version of 
// the system under which it was compiled.  It is used to allow the system
// to catch obsolete DLLs.
__declspec( dllexport ) ULONG LibVersion()
{
	return VERSION_3DSMAX;
}

__declspec( dllexport ) ULONG CanAutoDefer()
{
#ifdef _DEBUG
	return 0;
#else
	return 1;
#endif
}

TCHAR *GetString(int id)
{
	static TCHAR buf[256];

	if (hInstance)
		return LoadString(hInstance, id, buf, sizeof(buf)) ? buf : NULL;
	return NULL;
}

#endif // INC_DLL_ENTRY_H_
