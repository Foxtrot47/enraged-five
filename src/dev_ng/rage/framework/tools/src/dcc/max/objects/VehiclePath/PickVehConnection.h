//
//
//    Filename: PickVehConnection.h
//     Creator: 
//     $Author: Adam $
//       $Date: 29/04/99 9:44 $
//   $Revision: 1 $
// Description: 
//
//


#if !defined (PICK_VEH_CONNECTION_H_INCLUDED)
#define PICK_VEH_CONNECTION_H_INCLUDED


#include "Max.h"
#include "resource.h"

// class prototypes
class VehicleNode;

extern HINSTANCE hInstance;

//
//   Class Name: PickVehConnection
// Base Classes: 
//  Description: 
//    Functions: 
//
//
class PickVehConnection : public PickModeCallback, public PickNodeCallback 
{
public:				
	enum Operation {CONNECT, MERGE};

	PickVehConnection() {}

	void SetPickOperation(Operation oper) {m_oper = oper;}
	void SetButton(ICustButton *pButton) {m_pButton = pButton;}
	void SetVehicleNode(VehicleNode* pVehicleNode) {m_pVehicleNode = pVehicleNode;}

	BOOL HitTest(IObjParam *ip, HWND hWnd, ViewExp *vpt, IPoint2 m, int flags);
	BOOL Pick(IObjParam *ip, ViewExp *vpt);
	void EnterMode(IObjParam *ip);
	void ExitMode(IObjParam *ip);
	BOOL Filter(INode *node);

	BOOL RightClick(IObjParam *ip, ViewExp *vpt) {return TRUE;}
	PickNodeCallback *GetFilter() {return this;}
	BOOL AllowMultiSelect() {return FALSE;}

	// Cursors
	HCURSOR GetDefCursor(IObjParam *ip) {return LoadCursor(hInstance, MAKEINTRESOURCE(IDC_CONNECT_CURSOR));}
	HCURSOR GetHitCursor(IObjParam *ip) {return LoadCursor(hInstance, MAKEINTRESOURCE(IDC_CONNECT_CURSOR1));}

private:
	VehicleNode*	m_pVehicleNode;

	ICustButton *m_pButton;
	Operation m_oper;
};



#endif // !defined (PICK_VEH_CONNECTION_H_INCLUDED)
