#pragma once
#include "rexRAGE/serializerAnimation.h"
#include "rexGeneric/objectAnimation.h"
#include "atl/array.h"
#include "cutfile/cutfile2.h"

#include <IGame/IGame.h>
#include "IGame/IGameModifier.h"
#include <IGame/IGameProperty.h>

using namespace rage;

void CopyMaxMatrixToRageMatrix(const Matrix3& r_SrcMat,Matrix34& r_DstMat);

enum ERageLightType
{
	OMNI = 1,
	FREESPOT,
	TARGETSPOT,
	FREEDIRECT,
	TARGETDIRECT,
	SAUSAGE
};

class CLightAnimationExporter
{
public:
	CLightAnimationExporter();
	void Create(const float fStartTime, const float fEndTime, const int iFrames, const char* pNodeName, const char* pDirectory, int iLightType);
	void AddFrameData(INode* pNode, TimeValue t, rage::cutfCutsceneFile2* pCutFile);
	bool SaveAnim();

private:
	bool SaveClip();
	void Reset();
	void InitTracks( float fStartTime, float fEndTime, int nFrames, rage::atArray<rage::rexObjectGenericAnimation::TrackInfo*>& trackList );
	bool InitTrack( float fStartTime, float fEndTime, int nFrames, rage::rexObjectGenericAnimation::TrackInfo& track );
	rage::rexSerializerRAGEAnimation* CreateAnimationSerializer();
	bool CreateTracks();
	void AddAnimDataToTracks( Matrix3 transform, Point3 colour, float intensity, float falloff, float fExpFallOff, float fConeAngle, rage::atArray<rage::rexObjectGenericAnimation::TrackInfo*> &trackList, rage::cutfCutsceneFile2* pCutFile );
	void CreateAnimData(INode* pNode, TimeValue t, atArray<rexObjectGenericAnimation::TrackInfo*> &trackList, rage::cutfCutsceneFile2* pCutFile);

	rage::atArray<rage::atString> m_trackNameList;
	rage::atArray<int> m_trackIDList;
	float m_fStartTime;
	float m_fEndTime;
	rage::rexObjectGenericAnimation::ChunkInfo* m_pChunk;
	rage::rexObjectGenericAnimationSegment* m_pAnimationSegment ;
	rage::rexObjectGenericAnimation* m_pAnimation;  
	char m_cAnimFilename[rage::RAGE_MAX_PATH];
	int m_iLightType;
	IGameScene		*m_iGameScene;
};