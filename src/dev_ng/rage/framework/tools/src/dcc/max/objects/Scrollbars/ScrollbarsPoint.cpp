//
// filename:	ScrollbarsPoint.cpp
// author:		David Muir
// date:		23 January 2007
// description:	Point Sub-object and associated descriptor class definitions
//

// --- Include Files ------------------------------------------------------------

// STL headers
#include <cassert>

// Local headers
#include "ScrollbarsPoint.h"
#include "ScrollbarsDlgProc.h"
#include "IScrollbars.h"
#include "Win32Res.h"
#include "resource.h"

// --- Constants ----------------------------------------------------------------
const int sC_cnt_NumberOfReferences = 0;
const float sC_m_PointSize = 0.1f;

// --- Static Globals -----------------------------------------------------------
static CScrollbarsPointDesc g_ScrollbarsPointObjectDescriptor;

// Parameter Block Descriptor
static ParamBlockDesc2 scrollbarsPointDesc( 0, _T("Scrollbars Point Parameters"),
								     IDS_SCROLLBARS_CLASSNAME, GetScrollbarsPointDesc(),
								     P_AUTO_CONSTRUCT | P_AUTO_UI, SCROLLBARS_PBLOCK_REF_ID,
								     IDD_SCROLLBARS_PANEL, IDS_PARAMS, 0, 0, 
								     NULL,

								     end );

// --- Code ---------------------------------------------------------------------

CScrollbarsPoint::CScrollbarsPoint( )
	: m_rgbDrawColour( 1.0f, 1.0f, 1.0f )
{
}

CScrollbarsPoint::~CScrollbarsPoint( )
{
}

// ------------------------------------------------------------------------------
// New Methods
// ------------------------------------------------------------------------------

void 
CScrollbarsPoint::SetDrawCol( const Point3& pntCol ) 
{ 
	m_rgbDrawColour = pntCol; 
}

void					
CScrollbarsPoint::Draw( GraphicsWindow* pGW, TimeValue t, Matrix3& objMat, float fHeight )
{
	assert( pGW );
	if ( !pGW )
		return;

	pGW->setTransform( objMat );
	Point3 vUpperMarker( 0.0f, 0.0f, 0.0f );
	Point3 vLowerMarker( 0.0f, 0.0f, -fHeight );

	DrawLineProc lineProc( pGW );
	lineProc.SetLineColor( m_rgbDrawColour );
	lineProc.Marker( &vUpperMarker, CIRCLE_MRKR );
	lineProc.Marker( &vLowerMarker, CIRCLE_MRKR );
}

// ------------------------------------------------------------------------------
// Animatable Super-Class Overridden Methods
// ------------------------------------------------------------------------------

Class_ID		
CScrollbarsPoint::ClassID( )
{
	return GetScrollbarsPointDesc( )->ClassID( );
}

void			
CScrollbarsPoint::GetClassName( TSTR& sClassName )
{
	sClassName = GetScrollbarsPointDesc( )->ClassName( );
}

// ------------------------------------------------------------------------------
// ReferenceTarget Super-Class Overridden Methods
// ------------------------------------------------------------------------------

ReferenceTarget*	
CScrollbarsPoint::Clone( RemapDir &remap )
{
	CScrollbarsPoint* pClone = new CScrollbarsPoint;
	BaseClone( this, pClone, remap );

	return pClone;
}

IOResult				
CScrollbarsPoint::Load( ILoad *iload )
{
	return IO_OK;
}

IOResult				
CScrollbarsPoint::Save( ISave *isave )
{
	return IO_OK;
}

int
CScrollbarsPoint::NumRefs( )
{
	return sC_cnt_NumberOfReferences;
}

RefResult 
CScrollbarsPoint::NotifyRefChanged( Interval changeInt, RefTargetHandle hTarget, 
									PartID& partID, RefMessage message )
{
	return REF_SUCCEED;
}

// ------------------------------------------------------------------------------
// Object Super-Class Overridden Methods
// ------------------------------------------------------------------------------

ObjectState 
CScrollbarsPoint::Eval( TimeValue t )
{
	return ObjectState( this );
}

Interval				
CScrollbarsPoint::ObjectValidity( TimeValue t )
{
	return FOREVER;
}

void					
CScrollbarsPoint::InitNodeName( TSTR &s ) 
{ 
	s = _T( "Point" ); 
}

BOOL					
CScrollbarsPoint::IsWorldSpaceObject( )
{
	return TRUE;
}

// ------------------------------------------------------------------------------
// BaseObject Super-Class Overridden Methods
// ------------------------------------------------------------------------------

CreateMouseCallBack* 
CScrollbarsPoint::GetCreateMouseCallBack( )
{
	return NULL;
}

void					 
CScrollbarsPoint::GetLocalBoundBox( TimeValue t, INode* pNode, ViewExp* pVp, Box3& box )
{
	box.Init( );
	box.MakeCube( Point3( 0.0f, 0.0f, 0.0f ), 0.01f );
}

void					 
CScrollbarsPoint::GetWorldBoundBox( TimeValue t, INode* pNode, ViewExp* pVp, Box3& box )
{
	Matrix3 local2world = pNode->GetObjectTM( t );
	Box3 localBox;

	GetLocalBoundBox( t, pNode, pVp, localBox );
	box = localBox * local2world;
}

#if MAX_RELEASE > MAX_RELEASE_R12
int						 
CScrollbarsPoint::Display(TimeValue t, INode* pNode, ViewExp *pVpt, int flags )
{	return 0;	}

int						 
CScrollbarsPoint::DisplayPoint( TimeValue t, INode* pNode, ViewExp *pVpt, int flags, float fHeight )
{
	GraphicsWindow* pGW = pVpt->getGW( );
	Matrix3			mat = pNode->GetObjectTM( t );

	Draw( pGW, t, mat, fHeight );

	return 0;
}
#else
int						 
CScrollbarsPoint::Display( TimeValue t, INode* pNode, ViewExp *pVpt, int flags, float fHeight )
{
	GraphicsWindow* pGW = pVpt->getGW( );
	Matrix3			mat = pNode->GetObjectTM( t );

	Draw( pGW, t, mat, fHeight );

	return 0;
}
#endif //#if MAX_RELEASE > MAX_RELEASE_R12

int						 
CScrollbarsPoint::HitTest( TimeValue t, INode* pNode, int type, int crossing, int flags, IPoint2* pSp, ViewExp* pVpt )
{
	HitRegion hitRegion;
	DWORD savedLimits;
	GraphicsWindow *gw = pVpt->getGW();
	Matrix3 objMat = pNode->GetObjectTM(t);
	MakeHitRegion(hitRegion, type, crossing, 4, pSp);

	gw->setTransform(objMat);
	gw->setRndLimits(((savedLimits = gw->getRndLimits()) | GW_PICK) & ~GW_ILLUM);
	gw->setHitRegion(&hitRegion);
	gw->clearHitCode();

	// Passing default height down here, as not sure how I can read
	// from the parent's ParamBlock.  Doesn't seem to affect the
	// picking of the sub-objects however.
	Draw( gw, t, objMat, 1.0f );

	int res = gw->checkHitCode();

	gw->setRndLimits(savedLimits);

	return res;
}

TCHAR*					 
CScrollbarsPoint::GetObjectName( ) 
{ 
	return _T( "ScrollbarsPoint" ); 
}

int CScrollbarsPointDesc::IsPublic( )
{
	return 0;
}

void* CScrollbarsPointDesc::Create( BOOL bLoading )
{
	return new CScrollbarsPoint;
}

const TCHAR* CScrollbarsPointDesc::ClassName( )
{
	return GetStringResource( g_hInstance, IDS_SCROLLBARSPOINT_CLASSNAME );
}

SClass_ID CScrollbarsPointDesc::SuperClassID( ) 
{
	return HELPER_CLASS_ID;
}

Class_ID CScrollbarsPointDesc::ClassID( ) 
{ 
	return SCROLLBARS_POINTS_CLASSID; 
} 

const TCHAR* CScrollbarsPointDesc::Category( )
{
	return GetStringResource( g_hInstance, IDS_CATEGORY );
}

ClassDesc2* GetScrollbarsPointDesc( )
{
	return &g_ScrollbarsPointObjectDescriptor;
}

// End of file
