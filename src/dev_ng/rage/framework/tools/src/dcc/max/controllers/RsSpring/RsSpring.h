/**********************************************************************
 *<
	FILE: jiggle.h

	DESCRIPTION:	A procedural Mass/Spring Position controller 
					Main header

	CREATED BY:		Adam Felt

	HISTORY: 

 *>	Copyright (c) 1999-2000, All Rights Reserved.
 **********************************************************************/

#ifndef __RS_SPRING__H
#define __RS_SPRING__H

#include "Max.h"
#include "resource.h"
#include "istdplug.h"
#include "iparamm2.h"
#include "custcont.h"
#include "SpringSys.h"
#include "IRsSpringCtrl.h"
#include "Notify.h"
#include <ILockedTracks.h>
#include <ISceneEventManager.h>

extern TCHAR *GetString(int id);
extern HINSTANCE hInstance;

#define HAS_NO_PARENT  (1<<0)

// http://download.autodesk.com/global/docs/3dsmaxsdk2012/en_us/files/GUID-F35959BB-2660-492F-B082-56304C70293-605.htm
// First implementing of versioning with changed UI to support axis' for all parameters so upping to 2 already
#define VERSION_CURRENT 0x2

template <class T>
class RsSpring;
class RsSpringDlg;

//dialog stuff 
class DynMapDlgProc : public ParamMap2UserDlgProc {
	public:
		IRsSpring *cont;
		IParamMap2 *paramMap;

		ICustButton		*iButtLink;
		ISpinnerControl *iStrengthX;
		ISpinnerControl *iStrengthY;
		ISpinnerControl *iStrengthZ;
		ISpinnerControl *iDampeningX;
		ISpinnerControl *iDampeningY;
		ISpinnerControl *iDampeningZ;

		ISpinnerControl *xLimitSpinMin;
		ISpinnerControl *yLimitSpinMin;
		ISpinnerControl *zLimitSpinMin;
		ISpinnerControl *xLimitSpinMax;
		ISpinnerControl *yLimitSpinMax;
		ISpinnerControl *zLimitSpinMax;

		ISpinnerControl *xGravitySpin;
		ISpinnerControl *yGravitySpin;
		ISpinnerControl *zGravitySpin;

		ISpinnerControl *xGravityAxisSpin;
		ISpinnerControl *yGravityAxisSpin;
		ISpinnerControl *zGravityAxisSpin;

		DynMapDlgProc(IRsSpring *c) {cont = c; paramMap = NULL;}	
		INT_PTR DlgProc(TimeValue t,IParamMap2 *map,HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam);		
		void DeleteThis() {delete this;}
		
		void InitParams();
		void DestroyParams();
//		void Update(TimeValue t);
	};

//About dialog handler
INT_PTR CALLBACK AboutRollupDialogProc( HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam );
void UnRegisterRsSpringWindow(HWND hWnd);

//Function Publishing stuff
#define GetSpringControllerInterface(cd) \
			(RsSpring *)(cd)->GetInterface(RS_SPRING_CONTROLLER_INTERFACE)

#define RS_SPRING_PARAM_MIN 0.0f
#define RS_SPRING_PARAM_STEP 0.1f

#define RS_SPRING_STRENGTH_DEFAULT 100.0f
#define RS_SPRING_STRENGTH_MAX 1000.0f

#define RS_SPRING_DAMPEN_MAX 100.0f
#define RS_SPRING_DAMPEN_DEFAULT 10.0f

#define RS_SPRING_MIN_LIMIT_DEFAULT -0.1f
#define RS_SPRING_MIN_LIMIT_MIN -100.0f
#define RS_SPRING_MIN_LIMIT_MAX 0.0f
#define RS_SPRING_MIN_LIMIT_STEP 0.01f

#define RS_SPRING_MAX_LIMIT_DEFAULT 0.1f
#define RS_SPRING_MAX_LIMIT_MIN 0.0f
#define RS_SPRING_MAX_LIMIT_MAX 100.0f
#define RS_SPRING_MAX_LIMIT_STEP 0.01f

#define RS_SPRING_GRAVITY_DEFAULT -9.8f

#define RS_SPRING_GRAVITY_AXIS_LIMIT_MIN -1000.0f
#define RS_SPRING_GRAVITY_AXIS_LIMIT_MAX 1000.0f
#define RS_SPRING_GRAVITY_AXIS_STEP 0.1f

template <class T=Point3>
class RsSpring : public LockableControl, public IRsSpring, public INodeEventCallback //, public SpringSysClient 
{
	public:
		
		int type;						//the type of controller it is
		bool validStart;
		INode *selfNode;           // We are a controller on this object
//		INode *parentNode;         // This object is the parent of the above
		BOOL ctrlValid;

		Interval	ivalid;
		Interval	range;
		IParamBlock2 *dyn_pb;

		Control *theCtrl;			// ref 0
		byte flags;

//		RsSpringDlg* dlg;
//		DynMapDlgProc* pmap;

//		HWND	hParams1;  //dynamics dialog handle
		
		static IObjParam *ip;

		RsSpring(int type, BOOL loading);
		~RsSpring();

		RsSpring& operator=(const RsSpring& from);

		int	NumParamBlocks() { return 1; }					// return number of ParamBlocks in this instance
		IParamBlock2* GetParamBlock(int i) 
		{
			if (i==0) return dyn_pb; 
			else return NULL;
		} // return i'th ParamBlock
		IParamBlock2* GetParamBlockByID(BlockID id) 
		{ 
			if (dyn_pb->ID() == id) return dyn_pb;  
			else return NULL; 
		} // return id'd ParamBlock
		void BeginEditParams(IObjParam *ip, ULONG flags,Animatable *prev); 
		void EndEditParams(IObjParam *ip, ULONG flags,Animatable *next); 

		// Animatable's Schematic View methods
		SvGraphNodeReference SvTraverseAnimGraph(IGraphObjectManager *gom, Animatable *owner, int id, DWORD flags);
		TSTR SvGetRelTip(IGraphObjectManager *gom, IGraphNode *gNodeTarger, int id, IGraphNode *gNodeMaker);
		bool SvHandleRelDoubleClick(IGraphObjectManager *gom, IGraphNode *gNodeTarget, int id, IGraphNode *gNodeMaker);

		void EditTrackParams(
			TimeValue t,
			ParamDimensionBase *dim,
			TCHAR *pname,
			HWND hParent,
			IObjParam *ip,
			DWORD flags);
		int TrackParamsType(){if(GetLocked()==false) return TRACKPARAMS_WHOLE; else return TRACKPARAMS_NONE; }	

		void DeleteThis() {delete this;}		
		int IsKeyable() {return 0;}		
		BOOL IsAnimated() {return TRUE;}  //this could be done better

		int NumSubs()  {return 2;}
		Animatable* SubAnim(int i);
		TSTR SubAnimName(int i);
		int SubNumToRefNum(int subNum);
		BOOL ChangeParents(TimeValue t,const Matrix3& oldP,const Matrix3& newP,const Matrix3& tm);

		//From Animatable
		void CopyKeysFromTime(TimeValue src,TimeValue dst,DWORD flags) {theCtrl->CopyKeysFromTime(src,dst,flags);}
		BOOL IsKeyAtTime(TimeValue t,DWORD flags) {return theCtrl->IsKeyAtTime(t,flags);}
		BOOL GetNextKeyTime(TimeValue t,DWORD flags,TimeValue &nt) {return theCtrl->GetNextKeyTime(t,flags,nt);}
		int GetKeyTimes(Tab<TimeValue> &times,Interval range,DWORD flags) {return theCtrl->GetKeyTimes(times,range,flags);}
		int GetKeySelState(BitArray &sel,Interval range,DWORD flags) {return theCtrl->GetKeySelState(sel,range,flags);}

		// Reference methods
		int NumRefs() {return 2;}		
		RefTargetHandle GetReference(int i);
		void SetReference(int i, RefTargetHandle rtarg);
		RefResult NotifyRefChanged(Interval, RefTargetHandle, PartID&, RefMessage);
		virtual RefResult NotifyDependents  (  Interval  changeInt,  
			PartID  partID,  
			RefMessage  message,  
			SClass_ID  sclass = NOTIFY_ALL,  
			BOOL  propagate = TRUE,  
			RefTargetHandle  hTarg = NULL 
			)
		{
			switch(message)
			{
				case REFMSG_CHANGE:
					DeletCache();
					if(dlg)
					{
						dlg->Invalidate(true);
						dlg->Update();
					}
				break;
			}
			return ReferenceTarget::NotifyDependents(changeInt,partID,message,sclass,propagate,hTarg);
		}

		// SceneEvent Callbacks
		virtual void  	ControllerOtherEvent (NodeKeyTab &nodes)
		{
			if(!selfNode)
				return;

			bool found = false;
			for(int ni=0;ni<nodes.Count();ni++)
			{
				INodePtr pChainNode = selfNode;
				while(pChainNode!=NULL && !found)
				{
					if(NodeEventNamespace::GetNodeByKey(nodes[ni])==pChainNode)
						found = true;
					pChainNode = pChainNode->GetParentNode();
				}
			}

			if(found)
			{
				DeletCache();
			}
		}
		virtual void  	CallbackBegin ()
		{
			DebugPrint("Do ANYthing?!\n");
		}

		IOResult Save(ISave *isave);
		IOResult Load(ILoad *iload);
		BOOL AssignController(Animatable *control,int subAnim);

		// Control methods				
		void Copy(Control *from);
		BOOL IsLeaf() {return FALSE;}
		void SetValue(TimeValue t, void *val, int commit, GetSetMethod method);
		void CommitValue(TimeValue t) {theCtrl->CommitValue(t); ctrlValid = true; }
		void RestoreValue(TimeValue t) {theCtrl->RestoreValue(t);}	
		
		//FPMixinInterface methods
		BaseInterface* GetInterface(Interface_ID id) 
		{ 
			if (id == RS_SPRING_CONTROLLER_INTERFACE)
				return (IRsSpring*)this; 

			BaseInterface* l_interface = FPMixinInterface::GetInterface(id);
			if (l_interface == NULL)
				l_interface = INodeEventCallback::GetInterface(id);

			return l_interface;
		} 

		//Local Methods
//		virtual Point3 GetCurrVelocity(TimeValue t, float DeltaT);
		
		virtual T GetVectorDifference(T a, T b)
		{
			return a - b;
		}
		
		BOOL SetSelfReference();
		virtual T GetCurrValue(int referenceTime)=0;
		void ComputeValue(TimeValue t);
		void ApplyLimits(TimeValue t, T &curval);
	
		//From IRsSpring
		virtual float	GetStrength(int axis);
		virtual float	GetDampening(int axis);
		virtual Point3	GetStrengthVec();
		virtual Point3	GetDampeningVec();
		virtual float	GetGravity(int axis);
		virtual void	SetGravity(int axis, float gravity, bool update=true);
				void	SetPreserveXForms(bool doIt);
				BOOL	GetPreserveXForms();
		virtual void	SetStrength(int axis, float strength, bool update=true);
		virtual void	SetDampening(int axis, float dampen, bool update=true);
		virtual float	GetLimit(int axis, int minMax);
		virtual void	SetLimit(int axis, int minMax, float limit, bool update, bool linkForward=false);
				BOOL	IsParamsLinked();
				void	SetParamsLinked(BOOL linked);
		virtual int		GetSpringType(){return type;}
				float	GetGravityAxis(int axis);
				void	SetGravityAxis(int axis, float gravity, bool update);
//		virtual SpringSys* GetSpringSystem() { return partsys; }
		
//////////////////////////////////////////////////////////////////////////
// calculations from x:\gta5\src\dev\rage\base\src\pheffects\spring.cpp
//////////////////////////////////////////////////////////////////////////
		virtual void Integrate(TimeValue t, float DeltaT) = 0;
		Point3 SolveDisplacement(float dt, Point3 prevDelta, Point3 springOffset);
		void Solve(int t, float DeltaT);

		//From SpringSysClient
// 		Tab<Matrix3> GetForceMatrices(TimeValue t);
		void DeletCache()
		{
			pos_cache.ZeroCount();
			pos_cache.Shrink();
		}

		// integration values
		T currPos;
		T lastPos;
		Tab<T> pos_cache;
		Tab<T> vel_cache;


		// new member data
		int				m_lastTime;
		BOOL			m_valid;
		float LimitButtDownVal[3];

};

		
class PosRsSpring : public RsSpring<Point3> {
	public:
		
		PosRsSpring(BOOL loading) : RsSpring(RS_SPRINGPOS, loading) {}
		~PosRsSpring() {}

		Class_ID ClassID() { return RS_SPRING_POS_CLASS_ID; }  
		SClass_ID SuperClassID() { return CTRL_POSITION_CLASS_ID; } 
		void GetClassName(TSTR& s) {s = GetString(IDS_CLASS_NAME);}

		// Control methods
		RefTargetHandle Clone(RemapDir& remap);
		Point3 GetCurrValue(TimeValue t);
		void GetValue(TimeValue t, void *val, Interval &valid, GetSetMethod method);

		virtual void Integrate(TimeValue t, float DeltaT);
	};
class PosRsSpringClassDesc:public ClassDesc2 {
public:
	int 			IsPublic() {return 1;}
	void *			Create(BOOL loading) {return new PosRsSpring(loading);}
	const TCHAR *	ClassName() {return GetString(IDS_CLASS_NAME);}
	SClass_ID		SuperClassID() {return CTRL_POSITION_CLASS_ID;}
	Class_ID		ClassID() {return RS_SPRING_POS_CLASS_ID;}
	const TCHAR* 	Category() {return GetString(IDS_CATEGORY);}
	void			ResetClassParams (BOOL fileReset);

	// JBW: new descriptor data accessors added.  Note that the 
	//      internal name is hardwired since it must not be localized.
	const TCHAR*	InternalName() { return _T("PositionSpring"); }	// returns fixed parsable name (scripter-visible name)
	HINSTANCE		HInstance() { return hInstance; }			// returns owning module handle

};
static PosRsSpringClassDesc PosRsSpringDesc;

//Mixin descripter
static FPInterfaceDesc spring_controller_interface_pos(
	RS_SPRING_CONTROLLER_INTERFACE, _T("RsSpring"), 0, &PosRsSpringDesc, FP_MIXIN,
	IRsSpring::get_strength, _T("getStrength"), 0, TYPE_FLOAT, 0, 0,
	IRsSpring::set_strength, _T("setStrength"), 0, TYPE_VOID, 0, 1,
	_T("strength"), 0, TYPE_FLOAT,
	IRsSpring::get_dampening, _T("getDampening"), 0, TYPE_FLOAT, 0, 0,
	IRsSpring::set_dampening, _T("setDampening"), 0, TYPE_VOID, 0, 1,
	_T("dampening"), 0, TYPE_FLOAT,
	IRsSpring::get_influence, _T("getInfluence"), 0, TYPE_FLOAT, 0, 1,
	_T("axis"), 0, TYPE_INT,
	IRsSpring::set_influence, _T("setInfluence"), 0, TYPE_VOID, 0, 2,
	_T("axis"), 0, TYPE_INT,
	_T("influence"), 0, TYPE_FLOAT,
	IRsSpring::get_limit, _T("getLimit"), 0, TYPE_FLOAT, 0, 2,
	_T("axis"), 0, TYPE_INT,
	_T("minMax"), 0, TYPE_INT,
	IRsSpring::set_limit, _T("setLimit"), 0, TYPE_VOID, 0, 3,
	_T("axis"), 0, TYPE_INT,
	_T("minMax"), 0, TYPE_INT,
	_T("influence"), 0, TYPE_FLOAT,
	end
	);

// block IDs
enum { jig_dynamics_params};

// jig param IDs
enum {	jig_control_node, 
		jig_strength_x, jig_dampen_x, 
		jig_params_linked, 
		jig_xfactor, jig_yfactor, jig_zfactor,
		jig_xlimitMin, jig_ylimitMin, jig_zlimitMin,
		jig_xlimitMax, jig_ylimitMax, jig_zlimitMax,
		jig_preserveXForms, 
		jig_gravityX, jig_gravityY, jig_gravityZ,
		jig_strength_y, jig_strength_z, 
		jig_dampen_y, jig_dampen_z,
		jig_gravity_axisX, jig_gravity_axisY, jig_gravity_axisZ,
};

const static float ninetyDegInRad = 1.5707963267948966192313216916398f;

class RotRsSpring : public RsSpring<Quat> {
public:

	RotRsSpring(BOOL loading) 
		: RsSpring(RS_SPRINGROT, loading)
	{}
	~RotRsSpring() {}

	Class_ID ClassID() { return RS_SPRING_ROT_CLASS_ID; } 
	SClass_ID SuperClassID() { return CTRL_ROTATION_CLASS_ID; }
	void GetClassName(TSTR& s) {s = GetString(IDS_CLASS_NAME);}

	// Control methods
	RefTargetHandle Clone(RemapDir& remap);
	void GetValue(TimeValue t, void *val, Interval &valid, GetSetMethod method);

	virtual Quat GetVectorDifference(Quat to, Quat from);

	void Integrate(TimeValue t, float DeltaT);
	// 
	Quat GetCurrValue(TimeValue t);
};

class RotRsSpringClassDesc:public ClassDesc2 {
public:
	int 			IsPublic() {return 1;}
	void *			Create(BOOL loading) {return new RotRsSpring(loading);}
	const TCHAR *	ClassName() {return GetString(IDS_CLASS_NAME);}
	SClass_ID		SuperClassID() {return CTRL_ROTATION_CLASS_ID;}
	Class_ID		ClassID() {return RS_SPRING_ROT_CLASS_ID;}
	const TCHAR* 	Category() {return GetString(IDS_CATEGORY);}
	void			ResetClassParams (BOOL fileReset);

	// JBW: new descriptor data accessors added.  Note that the 
	//      internal name is hardwired since it must not be localized.
	const TCHAR*	InternalName() { return _T("RotationSpring"); }	// returns fixed parsable name (scripter-visible name)
	HINSTANCE		HInstance() { return hInstance; }			// returns owning module handle

};
static RotRsSpringClassDesc RotRsSpringDesc;

//Mixin descripter
static FPInterfaceDesc spring_controller_interface_rot(
	RS_SPRING_CONTROLLER_INTERFACE, _T("RsSpring"), 0, &RotRsSpringDesc, FP_MIXIN,
	IRsSpring::get_strength, _T("getStrength"), 0, TYPE_FLOAT, 0, 0,
	IRsSpring::set_strength, _T("setStrength"), 0, TYPE_VOID, 0, 1,
	_T("strength"), 0, TYPE_FLOAT,
	IRsSpring::get_dampening, _T("getDampening"), 0, TYPE_FLOAT, 0, 0,
	IRsSpring::set_dampening, _T("setDampening"), 0, TYPE_VOID, 0, 1,
	_T("dampening"), 0, TYPE_FLOAT,
	IRsSpring::get_influence, _T("getInfluence"), 0, TYPE_FLOAT, 0, 1,
	_T("axis"), 0, TYPE_STRING,
	IRsSpring::set_influence, _T("setInfluence"), 0, TYPE_VOID, 0, 2,
	_T("axis"), 0, TYPE_INT,
	_T("influence"), 0, TYPE_FLOAT,
	IRsSpring::get_limit, _T("getLimit"), 0, TYPE_FLOAT, 0, 2,
	_T("axis"), 0, TYPE_INT,
	_T("minMax"), 0, TYPE_INT,
	IRsSpring::set_limit, _T("setLimit"), 0, TYPE_VOID, 0, 3,
	_T("axis"), 0, TYPE_INT,
	_T("minMax"), 0, TYPE_INT,
	_T("influence"), 0, TYPE_FLOAT,
	end
	);


//Dialog Proc stuff
//*********************************************
#define RS_SPRINGDLG_CONTREF	0
#define RS_SPRINGDLG_CLASS_ID	0x1c353f9f
class RsSpringDlgTimeChangeCallback;

class RsSpringDlg : public ReferenceMaker {
	public:
		static IObjParam *ip;
		IRsSpring *cont;
		HWND hWnd, dynDlg, forceDlg;
		BOOL blockRedraw;//, valid;
		RsSpringDlgTimeChangeCallback *timeChangeCallback;

		ICustButton		*iButtLink;

		ISpinnerControl *iStrengthX;
		ISpinnerControl *iStrengthY;
		ISpinnerControl *iStrengthZ;
		ISpinnerControl *iDampeningX;
		ISpinnerControl *iDampeningY;
		ISpinnerControl *iDampeningZ;

		ISpinnerControl *xLimitSpinMin;
		ISpinnerControl *yLimitSpinMin;
		ISpinnerControl *zLimitSpinMin;
		ISpinnerControl *xLimitSpinMax;
		ISpinnerControl *yLimitSpinMax;
		ISpinnerControl *zLimitSpinMax;

		ISpinnerControl *xGravitySpin;
		ISpinnerControl *yGravitySpin;
		ISpinnerControl *zGravitySpin;

		ISpinnerControl *xGravityAxisSpin;
		ISpinnerControl *yGravityAxisSpin;
		ISpinnerControl *zGravityAxisSpin;

		RsSpringDlg(IObjParam *i,IRsSpring *c);
		~RsSpringDlg();

		Class_ID ClassID() {return Class_ID(RS_SPRINGDLG_CLASS_ID,0);}
		SClass_ID SuperClassID() {return REF_MAKER_CLASS_ID;}

		void Init(HWND hParent);
		void InitDynamicsParams();
		void DestroyDynamicsParams();
		void Reset(IObjParam *i,IRsSpring *c);
		void Invalidate(BOOL force=false);
		void Update();

		void SetupUI();
		void SetupList();
		static IRollupWindow *TVRollUp;

		virtual HWND CreateWin(HWND hParent)=0;
		virtual void MouseMessage(UINT /*message*/,WPARAM /*wParam*/,LPARAM /*lParam*/) {};	
		virtual void MaybeCloseWindow() {}

		RefResult NotifyRefChanged(Interval, RefTargetHandle, PartID&, RefMessage);
		int NumRefs();
		RefTargetHandle GetReference(int i);
		void SetReference(int i, RefTargetHandle rtarg);
	};


class RsSpringTrackDlg : public RsSpringDlg {
	public:
		
		RsSpringTrackDlg(IObjParam *i,IRsSpring *c)
			: RsSpringDlg(i,c) {}
					
		HWND CreateWin(HWND hParent);
		void MouseMessage(UINT /*message*/,WPARAM /*wParam*/,LPARAM /*lParam*/) {};
		void MaybeCloseWindow();
	};

class RsSpringDlgTimeChangeCallback : public TimeChangeCallback
{
	RsSpringDlg* dlg;
	public:
		RsSpringDlgTimeChangeCallback(RsSpringDlg* d){dlg = d;}
		void TimeChanged(TimeValue t);
};

class CheckForNonRsSpringDlg : public DependentEnumProc {
	public:		
		BOOL non;
		ReferenceMaker *me;
		CheckForNonRsSpringDlg(ReferenceMaker *m) {non = FALSE;me = m;}
		int proc(ReferenceMaker *rmaker) {
			if (rmaker==me) return DEP_ENUM_CONTINUE;
			if (rmaker->SuperClassID()!=REF_MAKER_CLASS_ID &&
				rmaker->ClassID()!=Class_ID(RS_SPRINGDLG_CLASS_ID,0)) {
				non = TRUE;
				return DEP_ENUM_HALT;
				}
			return DEP_ENUM_SKIP; // just look at direct dependents
			}
	};

//Window mananger class
class RsSpringWindow 
{
	public:
		HWND hWnd;
		HWND hParent;
		Control *cont;
		RsSpringWindow() {assert(0);}
		RsSpringWindow(HWND hWnd,HWND hParent,Control *cont)
		{this->hWnd=hWnd; this->hParent=hParent; this->cont=cont; }
};
static Tab<RsSpringWindow> jiggleWindows;

void RegisterRsSpringWindow(HWND hWnd, HWND hParent, Control *cont);

void UnRegisterRsSpringWindow(HWND hWnd);

HWND FindOpenRsSpringWindow(HWND hParent,Control *cont);



#endif // __RS_SPRING__H
