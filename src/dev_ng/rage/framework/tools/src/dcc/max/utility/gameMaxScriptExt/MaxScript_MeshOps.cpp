
// 3dsmax SDK headers
#include "max.h"
#include "maxscript/maxscript.h"
#include "maxscript/foundation/numbers.h"
#include "maxscript/foundation/3dmath.h"
#include "maxscript/macros/define_instantiation_functions.h"

//---------------------------------------------------------------------------
// MaxScript Function Definitions
//---------------------------------------------------------------------------

def_visible_primitive( getIllumVertColor,	"getIllumVertColor" );
def_visible_primitive( setIllumVertColor,	"setIllumVertColor" );
def_visible_primitive( getNumIVCVerts,		"getNumIVCVerts" );
def_visible_primitive( setNumIVCVerts,		"setNumIVCVerts" );
def_visible_primitive( getIVFace,			"getIVFace" );
def_visible_primitive( setIVFace,			"setIVFace" );
def_visible_primitive( buildIVFaces,		"buildIVFaces" );

//---------------------------------------------------------------------------
// MaxScript Function Implementation
//---------------------------------------------------------------------------

Value*
getIllumVertColor_cf( Value** arg_list, int count )
{
	Mesh* p_mesh = arg_list[0]->to_mesh();
	int iIndex = arg_list[1]->to_int();

	//count will be a 1 based index
	iIndex--;

	int iSize = p_mesh->getNumMapVerts(MAP_SHADING);

	if(iIndex >= iSize)
	{
		return &false_value;
	}
	
	Point3 get = p_mesh->mapVerts(MAP_SHADING)[iIndex];
	get[0] *= 255.0f;
	get[1] *= 255.0f;
	get[2] *= 255.0f;

	return new Point3Value(get);
}

Value* 
setIllumVertColor_cf( Value** arg_list, int count )
{
	Mesh* p_mesh = arg_list[0]->to_mesh();
	int iIndex = arg_list[1]->to_int();
	Point3 set = arg_list[2]->to_point3();

	//count will be a 1 based index
	iIndex--;

	int iSize = p_mesh->getNumMapVerts(MAP_SHADING);

	if(iIndex >= iSize)
	{
		return &false_value;
	}

	set[0] /= 255.0f;
	set[1] /= 255.0f;
	set[2] /= 255.0f;

	p_mesh->setMapVert(MAP_SHADING,iIndex,set);

	return &true_value;
//	return_protected(new MeshValue(p_mesh));
}

Value*
getNumIVCVerts_cf( Value** arg_list, int count )
{
	Mesh* p_mesh = arg_list[0]->to_mesh();

	int iSize = p_mesh->getNumMapVerts(MAP_SHADING);

	return_protected(Integer::intern(iSize));
}

Value* 
setNumIVCVerts_cf( Value** arg_list, int count )
{
	Mesh* p_mesh = arg_list[0]->to_mesh();
	int iSize = arg_list[1]->to_int();

	if(p_mesh->mapSupport(MAP_SHADING) == FALSE)
	{
		p_mesh->setMapSupport(MAP_SHADING,TRUE);
	}

	p_mesh->setNumMapVerts(MAP_SHADING,iSize);

	return &true_value;
}

Value*
getIVFace_cf( Value** arg_list, int count )
{
	Mesh* p_mesh = arg_list[0]->to_mesh();
	int iIndex = arg_list[1]->to_int();
	iIndex--;

	if(iIndex >= p_mesh->Map(MAP_SHADING).fnum)
	{
		return &false_value;
	}

	return new Point3Value(p_mesh->mapFaces(MAP_SHADING)[iIndex].t[0] + 1,p_mesh->mapFaces(MAP_SHADING)[iIndex].t[1] + 1,p_mesh->mapFaces(MAP_SHADING)[iIndex].t[2] + 1);
}

Value*
setIVFace_cf( Value** arg_list, int count )
{
	Mesh* p_mesh = arg_list[0]->to_mesh();
	int iIndex = arg_list[1]->to_int();
	iIndex--;

	Point3 face = arg_list[2]->to_point3();

	if(iIndex >= p_mesh->Map(MAP_SHADING).fnum)
	{
		return &false_value;
	}

	p_mesh->mapFaces(MAP_SHADING)[iIndex] = TVFace(face[0] - 1,face[1] - 1,face[2] - 1);

	return &true_value;
}

Value*
buildIVFaces_cf( Value** arg_list, int count )
{
	Mesh* p_mesh = arg_list[0]->to_mesh();

	p_mesh->setNumMapFaces(MAP_SHADING,p_mesh->numFaces);

	return &true_value;
}
