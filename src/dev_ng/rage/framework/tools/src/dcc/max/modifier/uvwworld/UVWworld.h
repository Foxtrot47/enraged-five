#ifndef __EDITMESH_H__
#define __EDITMESH_H__

#ifdef _DEBUG
//#define EM_DEBUG
#endif

#include "namesel.h"
#include "nsclip.h"
#include "sbmtlapi.h"
#include "istdplug.h"
#include "ActionTable.h"
#include <meshdelta.h>
#include <objmode.h>
#include "modsres.h"
#include "edmdata.h"

////////////////////////////////////////////////////////////////////////////////////////////
#define EDITMESH_CLASS_ID Class_ID(0x4da844b9, 0x6d4c1e6c)

#define IDC_SELVERTEX 0x3260
#define IDC_SELEDGE 0x3261
#define IDC_SELFACE 0x3262
#define IDC_SELPOLY 0x3263
#define IDC_SELELEMENT 0x3264

// Available selection levels
#define SL_OBJECT EM_SL_OBJECT	//0
#define SL_VERTEX EM_SL_VERTEX	//1
#define SL_EDGE EM_SL_EDGE	//2
#define SL_FACE EM_SL_FACE	//3
#define SL_POLY EM_SL_POLYGON	//4
#define SL_ELEMENT EM_SL_ELEMENT	//5

#define DEF_PICKBOX_SIZE	4

// Alignment types:
#define ALIGN_CONST 0
#define ALIGN_VIEW 1

#define EDITMESH_CHANNELS (PART_GEOM|SELECT_CHANNEL|PART_SUBSEL_TYPE|PART_DISPLAY|PART_TOPO|TEXMAP_CHANNEL|PART_VERTCOLOR)

#define EM_TEMPFLAGS 0xff
#define EM_SWITCH_SUBOBJ_VERSIONS 0x01
#define EM_EDITING 0x02
#define EM_KEEPFLAGS (~0xff)
#define EM_DISP_RESULT 0x0100

// EditmeshData flags
#define EMD_BEENDONE			(1<<0)
#define EMD_UPDATING_CACHE		(1<<1)
#define EMD_HASDATA				(1<<2)

////////////////////////////////////////////////////////////////////////////////////////////
class XFormProc;
class EditMeshData;

////////////////////////////////////////////////////////////////////////////////////////////

// Tables based on Edit Mesh's selLevel:
// Mesh selection level:
const int meshLevel[] = {MESH_OBJECT,MESH_VERTEX,MESH_EDGE,MESH_FACE,MESH_FACE,MESH_FACE};
// Display flags:
const DWORD levelDispFlags[] = {0,DISP_VERTTICKS|DISP_SELVERTS,DISP_SELEDGES,DISP_SELFACES,DISP_SELPOLYS,DISP_SELPOLYS};
// Hit testing...
const int hitLevel[] = { 0, SUBHIT_VERTS, SUBHIT_EDGES, SUBHIT_FACES, SUBHIT_FACES, SUBHIT_FACES };

// Conversion from selLevel to named selection level:
static int namedSetLevel[] = { NS_VERTEX, NS_VERTEX, NS_EDGE, NS_FACE, NS_FACE, NS_FACE };
static int namedClipLevel[] = { CLIP_VERT, CLIP_VERT, CLIP_EDGE, CLIP_FACE, CLIP_FACE, CLIP_FACE };

////////////////////////////////////////////////////////////////////////////////////////////
TCHAR *GetString(int id);
void ResetEditMeshUI();

////////////////////////////////////////////////////////////////////////////////////////////
class EditMeshMod : public Modifier, 
					public IMeshSelect, 
					public MeshDeltaUser
////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	// Window handles & an interface:
	static HWND hSel;
	static IObjParam *ip;		

	// Command modes
	static MoveModBoxCMode *moveMode;
	static RotateModBoxCMode *rotMode;
	static UScaleModBoxCMode *uscaleMode;
	static NUScaleModBoxCMode *nuscaleMode;
	static SquashModBoxCMode *squashMode;
	static SelectModBoxCMode *selectMode;

	static BOOL selByVert;
	static BOOL ignoreBackfaces, ignoreVisEdge;
	static BOOL rsSel;
	static int pickBoxSize;

	// Named selection set info:
	Tab<TSTR*> namedSel[3];
	Tab<DWORD> ids[3];
	int affectRegion, arIgBack, useEdgeDist, edgeIts;
	int selLevel;

	DWORD emFlags;
	void SetFlag (DWORD fl, BOOL val=TRUE) { if (val) emFlags |= fl; else emFlags &= ~fl; }
	void ClearFlag (DWORD fl) { emFlags &= (~fl); }
	bool GetFlag (DWORD fl) { return (emFlags&fl) ? TRUE : FALSE; }

	EditMeshMod();
	~EditMeshMod();

	Interval LocalValidity(TimeValue t);
	ChannelMask ChannelsUsed()  { return EDITMESH_CHANNELS; }
	ChannelMask ChannelsChanged() { return EDITMESH_CHANNELS; }
	void ModifyObject(TimeValue t, ModContext &mc, ObjectState *os, INode *node);
	void NotifyInputChanged(Interval changeInt, PartID partID, RefMessage message, ModContext *mc);
	Class_ID InputType() { return triObjectClassID; }
	
	int CompMatrix(TimeValue t, ModContext& mc, Matrix3& tm, Interval& valid);
	
	// From Animatable
	void DeleteThis() { delete this; }
	Class_ID ClassID() { return EDITMESH_CLASS_ID;}
	void GetClassName(TSTR& s) { s = GetString(IDS_RB_EDITMESHMOD); }
	void RescaleWorldUnits(float f);

	// From BaseObject
	int HitTest(TimeValue t, INode* inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt, ModContext* mc);
	int Display(TimeValue t, INode* inode, ViewExp *vpt, int flagst, ModContext *mc);
	void GetWorldBoundBox(TimeValue t,INode* inode, ViewExp *vpt, Box3& box, ModContext *mc);

	void GetSubObjectCenters(SubObjAxisCallback *cb,TimeValue t,INode *node,ModContext *mc);
	void GetSubObjectTMs(SubObjAxisCallback *cb,TimeValue t,INode *node,ModContext *mc);
	int SubObjectIndex(HitRecord *hitRec);

	void ShowEndResultChanged (BOOL showEndResult);

	BOOL DependOnTopology(ModContext &mc);

	// Transform stuff:
	void Transform (TimeValue t, Matrix3& partm, Matrix3 tmAxis, BOOL localOrigin, Matrix3 xfrm, int type);
	void Move( TimeValue t, Matrix3& partm, Matrix3& tmAxis, Point3& val, BOOL localOrigin=FALSE );
	void Rotate( TimeValue t, Matrix3& partm, Matrix3& tmAxis, Quat& val, BOOL localOrigin=FALSE );
	void Scale( TimeValue t, Matrix3& partm, Matrix3& tmAxis, Point3& val, BOOL localOrigin=FALSE );
	void TransformStart(TimeValue t);
	void TransformHoldingFinish(TimeValue t);
	void TransformFinish(TimeValue t);
	void TransformCancel(TimeValue t);

	void DragMoveInit (TimeValue t, bool doMaps=false);
	void DragMoveRestore ();
	void DragMove (MeshDelta & md, MeshDeltaUserData *mdu);
	void DragMoveAccept ();

	// UI code
	void UpdateSurfType();
	void RefreshSelType();
	void SetSelDlgEnables();
	void InvalidateNumberSelected();
	void SetNumSelLabel();
	void ExitAllCommandModes(bool exSlice=TRUE, bool exStandardModes=true);
	float GetPolyFaceThresh();

	void ClearMeshDataFlag(ModContextList& mcList,DWORD f);
	void DeleteMeshDataTempData();		
	void CreateMeshDataTempData();

	int NumRefs() { return 0; }
	RefTargetHandle GetReference(int i) { return NULL; }
	void SetReference(int i, RefTargetHandle rtarg) {}
	RefResult NotifyRefChanged( Interval changeInt,RefTargetHandle hTarget, 
	   PartID& partID, RefMessage message ) { return REF_SUCCEED; }

	// IO
	IOResult Save(ISave *isave);
	IOResult Load(ILoad *iload);
	IOResult SaveLocalData(ISave *isave, LocalModData *ld);
	IOResult LoadLocalData(ILoad *iload, LocalModData **pld);

	CreateMouseCallBack* GetCreateMouseCallBack() { return NULL; } 
	void BeginEditParams( IObjParam  *ip, ULONG flags,Animatable *prev);
	void EndEditParams( IObjParam *ip, ULONG flags,Animatable *next);
#if MAX_VERSION_MAJOR >= 9 
	RefTargetHandle Clone(RemapDir& remap = DefaultRemapDir());
#else
	RefTargetHandle Clone(RemapDir& remap = NoRemap());
#endif
	TCHAR *GetObjectName() { return GetString(IDS_RB_EDITMESH); }
	void ActivateSubobjSel(int level, XFormModes& modes );
	int NeedUseSubselButton() { return 0; }
	void SelectSubComponent(HitRecord *hitRec, BOOL selected, BOOL all, BOOL invert);
	void ClearSelection(int selLevel);
	void SelectAll(int selLevel);
	void InvertSelection(int selLevel);

	void InvalidateDistances ();
	void InvalidateAffectRegion();

	// IMeshSelect methods:
	DWORD GetSelLevel();
	void SetSelLevel(DWORD level);
	void LocalDataChanged();
	void LocalDataChanged (DWORD parts);

	// MeshDeltaUser methods:
	void GetUIParam (meshUIParam uiCode, float & ret);
	void SetUIParam (meshUIParam uiCode, float val);
	void GetUIParam (meshUIParam uiCode, int & ret);
	void SetUIParam (meshUIParam uiCode, int val);
	void ToggleCommandMode (meshCommandMode mode);
	void ButtonOp (meshButtonOp opcode);
	void ExitCommandModes () { ExitAllCommandModes (); }

	bool Editing () { return (ip && GetFlag (EM_EDITING)) ? TRUE : FALSE; }
	DWORD GetEMeshSelLevel () { return selLevel; }
	void SetEMeshSelLevel (DWORD sl) { if (ip) ip->SetSubObjectLevel (sl); else selLevel = sl; }

	void*	GetInterface(ULONG id);

	bool HasActiveSelection ();

	int NumSubObjTypes();
	ISubObjType *GetSubObjType(int i);
private:
	// starts color picker for editing vert colors on the given map channel
	void EditVertColor(int mp, int ctrlID, int strID );
};

////////////////////////////////////////////////////////////////////////////////////////////
class EditMeshClassDesc:public ClassDesc 
////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	int 			IsPublic() { return GetSystemSetting(SYSSET_ENABLE_EDITMESHMOD); }
	void *			Create(BOOL loading = FALSE ) { return new EditMeshMod; }
	const TCHAR *	ClassName() { return GetString(IDS_RB_EDITMESH_CLASS); }
	SClass_ID		SuperClassID() { return OSM_CLASS_ID; }
	Class_ID		ClassID() { return EDITMESH_CLASS_ID; }
	const TCHAR* 	Category() { return GetString(IDS_RB_DEFEDIT);}
	void			ResetClassParams(BOOL fileReset);
};

////////////////////////////////////////////////////////////////////////////////////////////
class XFormProc 
////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	virtual Point3 proc(Point3& p, Matrix3 &mat, Matrix3 &imat)=0;
	virtual void SetMat( Matrix3& mat ) {}
};

////////////////////////////////////////////////////////////////////////////////////////////
class MoveXForm : public XFormProc 
////////////////////////////////////////////////////////////////////////////////////////////
{
private:
	Point3 delta, tdelta;		
public:
	Point3 proc(Point3& p, Matrix3 &mat, Matrix3 &imat) { return p + tdelta; }
	void SetMat( Matrix3& mat ) { tdelta = VectorTransform(Inverse(mat),delta); }
	MoveXForm(Point3 d) { delta = d; }
};

////////////////////////////////////////////////////////////////////////////////////////////
class RotateXForm : public XFormProc 
////////////////////////////////////////////////////////////////////////////////////////////
{
private:
	Matrix3 rot, trot;
public:
	Point3 proc(Point3& p, Matrix3 &mat, Matrix3 &imat) { return (trot*p)*imat; }
	void SetMat( Matrix3& mat ) { trot = mat * rot; }
	RotateXForm(Quat q) { q.MakeMatrix(rot); }
};

////////////////////////////////////////////////////////////////////////////////////////////
class ScaleXForm : public XFormProc 
////////////////////////////////////////////////////////////////////////////////////////////
{
private:
	Matrix3 scale, tscale;
public:
	Point3 proc(Point3& p, Matrix3 &mat, Matrix3 &imat) { return (p*tscale)*imat; }
	void SetMat( Matrix3& mat ) { tscale = mat*scale; }
	ScaleXForm(Point3 s) { scale = ScaleMatrix(s); }
};

////////////////////////////////////////////////////////////////////////////////////////////
class NamedSetSizeChange : public RestoreObj 
////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	EditMeshData *emd;
	int nsl;
	int oldsize, change;

	NamedSetSizeChange (EditMeshData *e, int n, int old, int inc) { emd=e; nsl=n; oldsize=old; change=inc; }
	void Restore (int isUndo) { emd->selSet[nsl].SetSize (oldsize); }
	void Redo () { emd->selSet[nsl].SetSize (oldsize+change); }
	int GetSize () { return 3*sizeof(int) + sizeof (void *); }
	TSTR Description () { return _T("Named Selection Set Size Change"); }
};

////////////////////////////////////////////////////////////////////////////////////////////
class NamedSetDelete : public RestoreObj 
////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	EditMeshData *emd;
	int nsl;
	Tab<BitArray *> oldSets;
	BitArray del;

	NamedSetDelete (EditMeshData *em, int n, BitArray &d);
	~NamedSetDelete ();
	void Restore (int isUndo);
	void Redo () { emd->selSet[nsl].DeleteSetElements (del, (nsl==NS_EDGE) ? 3 : 1); }
	int GetSize () { return 3*sizeof(int) + sizeof (void *); }
	TSTR Description () { return _T("Named Selection Set Subset Deletion"); }
};

////////////////////////////////////////////////////////////////////////////////////////////
class ScaleVertDeltas : public ModContextEnumProc 
////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	float f;
	EditMeshMod *mod;
	ScaleVertDeltas(float ff,EditMeshMod *m) {f=ff;mod=m;}
	BOOL proc(ModContext *mc);
};

#endif
