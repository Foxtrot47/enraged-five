//
//
//    Filename: uvwui.cpp
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 3/06/99 12:36 $
//   $Revision: 5 $
// Description: 
//
//
#include "UVWmachine.h"
#include "UVWMode.h"
#include "uvwui.h"


#define IDC_SELSINGLE	1100
#define IDC_SELPOLY		1101
#define IDC_SELELEMENT	1102


// static member variables
HWND UVWmachineUI::m_hVtxParams = NULL;
HWND UVWmachineUI::m_hFaceParams = NULL;
HIMAGELIST UVWmachineUI::m_hImageList = NULL;

// prototype for window's functions
static INT_PTR CALLBACK UVWmachineVtxDlgProc(	HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK UVWmachineFaceDlgProc(	HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);


int32 coordSpinnerIds[NUM_COORD_SPINNERS] =
{
	IDC_UCOORD_SPIN,
	IDC_UCOORD_SPIN2,
	IDC_UCOORD_SPIN3,
	IDC_UCOORD_SPIN4,
	IDC_VCOORD_SPIN,
	IDC_VCOORD_SPIN2,
	IDC_VCOORD_SPIN3,
	IDC_VCOORD_SPIN4
};
int32 coordEditIds[NUM_COORD_SPINNERS] =
{
	IDC_UCOORD_EDIT,
	IDC_UCOORD_EDIT2,
	IDC_UCOORD_EDIT3,
	IDC_UCOORD_EDIT4,
	IDC_VCOORD_EDIT,
	IDC_VCOORD_EDIT2,
	IDC_VCOORD_EDIT3,
	IDC_VCOORD_EDIT4
};
int32 coordLabelIds[NUM_COORD_SPINNERS] =
{
	IDC_UCOORD_LABEL,
	IDC_UCOORD_LABEL2,
	IDC_UCOORD_LABEL3,
	IDC_UCOORD_LABEL4,
	IDC_VCOORD_LABEL,
	IDC_VCOORD_LABEL2,
	IDC_VCOORD_LABEL3,
	IDC_VCOORD_LABEL4
};

//
//        name: UVWmachineUI::InitVertexDialog,UVWmachineUI::DestroyVertexDialog
// description: Functions called at the initialisation and destruction of the vertex dialog
//          in: hWnd = window handle
//
void UVWmachineUI::InitVertexDialog(HWND hWnd)
{
	m_hVtxParams = hWnd;

	TSTR buf;
	buf.printf("Version %s", GetString(IDS_VERSION));
	SetDlgItemText(hWnd, IDC_VERSION, buf);

	m_pTranslateButton = GetICustButton(GetDlgItem(hWnd, IDC_MOUSETRANSLATE_BUTTON));
	m_pTranslateButton->SetType(CBT_CHECK);
	m_pTranslateButton->SetHighlightColor(GREEN_WASH);

	m_pCoordSpin[0] = SetupFloatSpinner(hWnd, coordSpinnerIds[0], coordEditIds[0], -10.0f, 10.0f, 0.0f);
	m_pCoordSpin[0]->SetScale(0.01f);
	m_pCoordSpin[4] = SetupFloatSpinner(hWnd, coordSpinnerIds[4], coordEditIds[4], -10.0f, 10.0f, 0.0f);
	m_pCoordSpin[4]->SetScale(0.01f);
}

void UVWmachineUI::DestroyVertexDialog(HWND hWnd)
{
	m_pMod->EndMouseOperation();

	ReleaseICustButton(m_pTranslateButton);
	ReleaseISpinner(m_pCoordSpin[0]);
	ReleaseISpinner(m_pCoordSpin[4]);
}

//
//        name: UVWmachineUI::InitFaceDialog,UVWmachineUI::DestroyFaceDialog
// description: Functions called at the initialisation and destruction of the face dialog
//          in: hWnd = window handle
//
void UVWmachineUI::InitFaceDialog(HWND hWnd)
{
	m_hFaceParams = hWnd;

	TSTR buf;
	buf.printf("Version %s", GetString(IDS_VERSION));
	SetDlgItemText(hWnd, IDC_VERSION, buf);

	// face selection toolbar
	LoadFaceToolbar();

	m_pToolbar = GetICustToolbar(GetDlgItem(hWnd, IDC_FACE_TOOLBAR));
	m_pToolbar->SetImage(m_hImageList);
	m_pToolbar->AddTool(ToolButtonItem(CTB_CHECKBUTTON, 0,0,0,0, 16,15, 24,23,IDC_SELSINGLE));
	m_pToolbar->AddTool(ToolButtonItem(CTB_CHECKBUTTON, 1,1,1,1, 16,15, 24,23,IDC_SELPOLY));
	m_pToolbar->AddTool(ToolButtonItem(CTB_CHECKBUTTON, 2,2,2,2, 16,15, 24,23,IDC_SELELEMENT));

	m_pSingleButton = m_pToolbar->GetICustButton(IDC_SELSINGLE);
	m_pPolyButton = m_pToolbar->GetICustButton(IDC_SELPOLY);
	m_pElementButton = m_pToolbar->GetICustButton(IDC_SELELEMENT);

	// threshold spinnter
	//m_pThresholdSpin = SetupFloatSpinner(hWnd, IDC_SELECTTHRESHOLD_SPIN, IDC_SELECTTHRESHOLD_EDIT,
	//									0.0f, 180.0f, m_pMod->GetSelectThreshold());
	// select by vertex check
	if(m_pMod->GetSelectByVertex())
		CheckDlgButton(hWnd, IDC_BYVERTEX_CHECK, TRUE);
	else
		CheckDlgButton(hWnd, IDC_BYVERTEX_CHECK, FALSE);
	
	SetFaceSelection(m_pMod->GetFaceSelLevel());

	// explicit u and v coordinate spinners
	for(int32 i=0; i<NUM_COORD_SPINNERS; i++)
	{
		m_pCoordSpin[i] = SetupFloatSpinner(hWnd, coordSpinnerIds[i], coordEditIds[i], -10.0f, 10.0f, 0.0f);
		m_pCoordSpin[i]->SetScale(0.01f);
	}

	m_pRotateButton = GetICustButton(GetDlgItem(hWnd, IDC_MOUSEROTATE_BUTTON));
	m_pScaleButton = GetICustButton(GetDlgItem(hWnd, IDC_MOUSESCALE_BUTTON));
	m_pScaleButtonU = GetICustButton(GetDlgItem(hWnd, IDC_MOUSESCALEU_BUTTON));
	m_pScaleButtonV = GetICustButton(GetDlgItem(hWnd, IDC_MOUSESCALEV_BUTTON));
	m_pTranslateButton = GetICustButton(GetDlgItem(hWnd, IDC_MOUSETRANSLATE_BUTTON));
	m_pTranslateButtonU = GetICustButton(GetDlgItem(hWnd, IDC_MOUSETRANSLATEU_BUTTON));
	m_pTranslateButtonV = GetICustButton(GetDlgItem(hWnd, IDC_MOUSETRANSLATEV_BUTTON));
	m_pRotateButton->SetType(CBT_CHECK);
	m_pScaleButton->SetType(CBT_CHECK);
	m_pScaleButtonU->SetType(CBT_CHECK);
	m_pScaleButtonV->SetType(CBT_CHECK);
	m_pTranslateButton->SetType(CBT_CHECK);
	m_pTranslateButtonU->SetType(CBT_CHECK);
	m_pTranslateButtonV->SetType(CBT_CHECK);
	m_pRotateButton->SetHighlightColor(GREEN_WASH);
	m_pScaleButton->SetHighlightColor(GREEN_WASH);
	m_pScaleButtonU->SetHighlightColor(GREEN_WASH);
	m_pScaleButtonV->SetHighlightColor(GREEN_WASH);
	m_pTranslateButton->SetHighlightColor(GREEN_WASH);
	m_pTranslateButtonU->SetHighlightColor(GREEN_WASH);
	m_pTranslateButtonV->SetHighlightColor(GREEN_WASH);

	// rotate and scale origin spinners
	m_pUOriginSpin = SetupFloatSpinner(hWnd, IDC_UORIGIN_SPIN, IDC_UORIGIN_EDIT,-10.0f, 10.0f, m_uOrigin);
	m_pVOriginSpin = SetupFloatSpinner(hWnd, IDC_VORIGIN_SPIN, IDC_VORIGIN_EDIT,-10.0f, 10.0f, m_vOrigin);
	m_pUOriginSpin->SetScale(0.01f);
	m_pVOriginSpin->SetScale(0.01f);
}

void UVWmachineUI::DestroyFaceDialog(HWND hWnd)
{
	m_pMod->EndMouseOperation();

	UpdateOrigin();
	ReleaseISpinner(m_pUOriginSpin);
	ReleaseISpinner(m_pVOriginSpin);
	ReleaseICustButton(m_pRotateButton);
	ReleaseICustButton(m_pScaleButton);
	ReleaseICustButton(m_pScaleButtonU);
	ReleaseICustButton(m_pScaleButtonV);
	ReleaseICustButton(m_pTranslateButton);
	ReleaseICustButton(m_pTranslateButtonU);
	ReleaseICustButton(m_pTranslateButtonV);
	for(int32 i=0; i<NUM_COORD_SPINNERS; i++)
	{
		ReleaseISpinner(m_pCoordSpin[i]);
	}
	//ReleaseISpinner(m_pThresholdSpin);
	ReleaseICustButton(m_pSingleButton);
	ReleaseICustButton(m_pPolyButton);
	ReleaseICustButton(m_pElementButton);
	ReleaseICustToolbar(m_pToolbar);
}

//
//        name: UVWmachine::AddRollupPage,UVWmachine::DeleteRollupPage
// description: Add/remove rollup pages
//
void UVWmachineUI::AddRollupPage(void)
{
	int32 selLevel = m_pMod->GetSelTypes()[m_pMod->GetSelLevel()];

	DeleteRollupPage();
	if(selLevel == IMESHSEL_FACE)
	{
		m_hFaceParams = m_ip->AddRollupPage( 
			hInstance, 
			MAKEINTRESOURCE(IDD_FACE_PANEL),
			UVWmachineFaceDlgProc,
			"Face editing", 
			(LPARAM)m_pMod );		
		m_ip->RegisterDlgWnd(m_hFaceParams);
	}
	else if(selLevel == IMESHSEL_VERTEX)
	{
		m_hVtxParams = m_ip->AddRollupPage( 
			hInstance, 
			MAKEINTRESOURCE(IDD_VTX_PANEL),
			UVWmachineVtxDlgProc,
			"Vertex editing", 
			(LPARAM)m_pMod );		
		m_ip->RegisterDlgWnd(m_hVtxParams);
	}
}

void UVWmachineUI::DeleteRollupPage(void)
{
	if(m_hFaceParams)
		m_ip->DeleteRollupPage(m_hFaceParams);
	m_hFaceParams = NULL;
	if(m_hVtxParams)
		m_ip->DeleteRollupPage(m_hVtxParams);
	m_hVtxParams = NULL;
}

//
//        name: UVWmachine::LoadFaceToolbar
// description: Loads the imagelist for the face selection toolbar
//
void UVWmachineUI::LoadFaceToolbar(void)
{
	if(m_hImageList == NULL)
	{
		HBITMAP hBitmap = LoadBitmap(hInstance,MAKEINTRESOURCE(IDB_FACESEL));
		HBITMAP hMask = LoadBitmap(hInstance,MAKEINTRESOURCE(IDB_FACESEL_MASK));
		
		m_hImageList = ImageList_Create(16,15,TRUE, 3, 0);
		ImageList_Add(m_hImageList,hBitmap,hMask);
		DeleteObject(hBitmap);
		DeleteObject(hMask);
	}
}

//
//        name: UVWmachine::SetSelType
// description: Set the face selection type
//          in: type = selection type (single poly,face,element)
//
void UVWmachineUI::SetFaceSelection(int32 selection)
{	
	int32 old = m_pMod->GetFaceSelLevel();
	BOOL fourthVtxVisibilty = TRUE;
	
	m_pMod->SetFaceSelLevel(selection);

	switch (selection) {
	case FACESEL_SINGLE:
		m_pSingleButton->SetCheck(TRUE);
		m_pPolyButton->SetCheck(FALSE);
		m_pElementButton->SetCheck(FALSE);
		fourthVtxVisibilty = FALSE;
		break;
	case FACESEL_POLY:
		m_pSingleButton->SetCheck(FALSE);
		m_pPolyButton->SetCheck(TRUE);
		m_pElementButton->SetCheck(FALSE);
		break;
	case FACESEL_ELEMENT:
		m_pSingleButton->SetCheck(FALSE);
		m_pPolyButton->SetCheck(FALSE);
		m_pElementButton->SetCheck(TRUE);
		break;
	}
	
	ShowWindow(GetDlgItem(m_hFaceParams, IDC_UCOORD_SPIN4), fourthVtxVisibilty);
	ShowWindow(GetDlgItem(m_hFaceParams, IDC_UCOORD_EDIT4), fourthVtxVisibilty);
	ShowWindow(GetDlgItem(m_hFaceParams, IDC_UCOORD_LABEL4), fourthVtxVisibilty);
	ShowWindow(GetDlgItem(m_hFaceParams, IDC_VCOORD_SPIN4), fourthVtxVisibilty);
	ShowWindow(GetDlgItem(m_hFaceParams, IDC_VCOORD_EDIT4), fourthVtxVisibilty);
	ShowWindow(GetDlgItem(m_hFaceParams, IDC_VCOORD_LABEL4), fourthVtxVisibilty);
	
	if (old != selection) {
		m_pMod->NotifyDependents(FOREVER, PART_DISPLAY, REFMSG_CHANGE);
		UpdateWindow(m_hFaceParams);
		m_ip->RedrawViews(m_ip->GetTime(),REDRAW_NORMAL);
	}
}



// -- windows proc ---------------------------------------------------------------------------------

static INT_PTR CALLBACK UVWmachineVtxDlgProc(	HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	UVWEnumProc proc;
	UVWmachine *pInstance = (UVWmachine*)GetWindowLong(hWnd,GWLP_USERDATA);
	UVWmachineUI *pUI;
	if (!pInstance && msg!=WM_INITDIALOG) 
		return FALSE;
	
	if(pInstance)
		pUI = &(pInstance->m_ui);

	switch (msg) {
	case WM_INITDIALOG:
		pInstance = (UVWmachine*)lParam;
		SetWindowLong(hWnd,GWLP_USERDATA,lParam);
		pInstance->m_ui.InitVertexDialog(hWnd);
		break;
		
	case WM_DESTROY:
		pInstance->m_ui.DestroyVertexDialog(hWnd);
		break;
		
	case WM_COMMAND:
		// make sure command mode is set back to the standard select and move
		if(LOWORD(wParam) != IDC_MOUSETRANSLATE_BUTTON)
			pInstance->EndMouseOperation();

		switch (LOWORD(wParam)) {
		case IDC_MOUSETRANSLATE_BUTTON:
			if(pUI->GetTranslateButton()->IsChecked())
				pInstance->StartMouseOperation(MOUSE_TRANSLATE);
			else
				pInstance->EndMouseOperation();
			break;
		}
		break;
		
	case CC_SPINNER_CHANGE:
		{
			float values[2];
			proc.SetValuesPtr(&values[0]);
			char *pUndoTitle = "Set texture UV";
			char *pUndo = NULL;

			if(!HIWORD(wParam))
				pUndo = pUndoTitle;

			switch (LOWORD(wParam)) {
			case IDC_UCOORD_SPIN:
				proc.SetOperation(UVWEnumProc::EXPLICITSETU);
				values[0] = pUI->GetCoordSpinValue(0);
				pInstance->ProcessTextureCoords(proc, pUndo);
				break;
			case IDC_VCOORD_SPIN:
				proc.SetOperation(UVWEnumProc::EXPLICITSETV);
				values[1] = pUI->GetCoordSpinValue(4);
				pInstance->ProcessTextureCoords(proc, pUndo);
				break;
			}
			break;
		}
	case CC_SPINNER_BUTTONDOWN:
		if (LOWORD(wParam) == IDC_UCOORD_SPIN ||
			LOWORD(wParam) == IDC_VCOORD_SPIN)
			theHold.Begin();
		break;

	case CC_SPINNER_BUTTONUP:
		if (LOWORD(wParam) == IDC_UCOORD_SPIN ||
			LOWORD(wParam) == IDC_VCOORD_SPIN)
			theHold.Accept("Set texture UV");
		break;

	default:
		return FALSE;
	}
	return TRUE;
}

static INT_PTR CALLBACK UVWmachineFaceDlgProc(	HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	UVWEnumProc proc;
	UVWmachine *pInstance = (UVWmachine*)GetWindowLong(hWnd,GWLP_USERDATA);
	UVWmachineUI *pUI;
	if (!pInstance && msg!=WM_INITDIALOG) 
		return FALSE;
	
	if(pInstance)
		pUI = &(pInstance->m_ui);

	switch (msg) {
	case WM_INITDIALOG:
		pInstance = (UVWmachine*)lParam;
		SetWindowLong(hWnd,GWLP_USERDATA,lParam);

		pInstance->m_ui.InitFaceDialog(hWnd);
		break;
		
	case WM_DESTROY:
		pInstance->m_ui.DestroyFaceDialog(hWnd);
		break;
		
	case WM_COMMAND:
		// make sure command mode is set back to the standard select and move
		if(LOWORD(wParam) != IDC_MOUSEROTATE_BUTTON &&
			LOWORD(wParam) != IDC_MOUSESCALE_BUTTON &&
			LOWORD(wParam) != IDC_MOUSETRANSLATE_BUTTON)
			pInstance->EndMouseOperation();

		switch (LOWORD(wParam)) {
		case IDC_SELSINGLE:
			pUI->SetFaceSelection(FACESEL_SINGLE);
			break;
		case IDC_SELPOLY:
			pUI->SetFaceSelection(FACESEL_POLY);
			break;
		case IDC_SELELEMENT:
			pUI->SetFaceSelection(FACESEL_ELEMENT);
			break;
		case IDC_BYVERTEX_CHECK:
			if(IsDlgButtonChecked(hWnd, IDC_BYVERTEX_CHECK))
				pInstance->SetSelectByVertex(true);
			else
				pInstance->SetSelectByVertex(false);
//			pInstance->NotifyDependents(FOREVER, PART_DISPLAY, REFMSG_CHANGE);
//			pUI->GetInterface()->RedrawViews(pUI->GetInterface()->GetTime(),REDRAW_NORMAL);
			break;
		case IDC_ROTATE_BUTTON:
			proc.SetOperation(UVWEnumProc::ROTATE);
			pInstance->ProcessTextureCoords(proc, "Rotate UVs");
			break;
		case IDC_ROTATE90_BUTTON:
			proc.SetOperation(UVWEnumProc::ROTATE90);
			pInstance->ProcessTextureCoords(proc, "Rotate UVs");
			break;
		case IDC_FLIPU_BUTTON:
			proc.SetOperation(UVWEnumProc::FLIPU);
			pInstance->ProcessTextureCoords(proc, "Flip UVs");
			break;
		case IDC_FLIPV_BUTTON:
			proc.SetOperation(UVWEnumProc::FLIPV);
			pInstance->ProcessTextureCoords(proc, "Flip UVs");
			break;
		case IDC_MOUSEROTATE_BUTTON:
			if(pUI->GetRotateButton()->IsChecked())
			{
				pUI->UpdateOrigin();
				pInstance->StartMouseOperation(MOUSE_ROTATE);
			}
			else
				pInstance->EndMouseOperation();

			break;
		case IDC_MOUSESCALE_BUTTON:
			if(pUI->GetScaleButton()->IsChecked())
			{
				pUI->UpdateOrigin();
				pInstance->StartMouseOperation(MOUSE_SCALE);
			}
			else
				pInstance->EndMouseOperation();
			break;
		case IDC_MOUSETRANSLATE_BUTTON:
			if(pUI->GetTranslateButton()->IsChecked())
				pInstance->StartMouseOperation(MOUSE_TRANSLATE);
			else
				pInstance->EndMouseOperation();
			break;
		case IDC_MOUSESCALEU_BUTTON:
			if(pUI->GetScaleButtonU()->IsChecked())
			{
				pUI->UpdateOrigin();
				pInstance->StartMouseOperation(MOUSE_SCALEU);
			}
			else
				pInstance->EndMouseOperation();
			break;
		case IDC_MOUSETRANSLATEU_BUTTON:
			if(pUI->GetTranslateButtonU()->IsChecked())
				pInstance->StartMouseOperation(MOUSE_TRANSLATEU);
			else
				pInstance->EndMouseOperation();
			break;
		case IDC_MOUSESCALEV_BUTTON:
			if(pUI->GetScaleButtonV()->IsChecked())
			{
				pUI->UpdateOrigin();
				pInstance->StartMouseOperation(MOUSE_SCALEV);
			}
			else
				pInstance->EndMouseOperation();
			break;
		case IDC_MOUSETRANSLATEV_BUTTON:
			if(pUI->GetTranslateButtonV()->IsChecked())
				pInstance->StartMouseOperation(MOUSE_TRANSLATEV);
			else
				pInstance->EndMouseOperation();
			break;
		};
		break;

	case CC_SPINNER_CHANGE:
		{
			float values[2];
			proc.SetValuesPtr(&values[0]);
			char *pUndoTitle = "Set texture UV";
			char *pUndo = NULL;

			if(!HIWORD(wParam))
				pUndo = pUndoTitle;

			switch (LOWORD(wParam)) {
			//case IDC_SELECTTHRESHOLD_SPIN:
			//	pInstance->SetSelectThreshold(pUI->GetSelectThresholdValue());
			//	break;
			case IDC_UCOORD_SPIN:
				proc.SetOperation(UVWEnumProc::EXPLICITSETU);
				proc.SetVertexIndex(0);
				values[0] = pUI->GetCoordSpinValue(0);
				pInstance->ProcessTextureCoords(proc, pUndo);
				break;
			case IDC_VCOORD_SPIN:
				proc.SetOperation(UVWEnumProc::EXPLICITSETV);
				proc.SetVertexIndex(0);
				values[1] = pUI->GetCoordSpinValue(4);
				pInstance->ProcessTextureCoords(proc, pUndo);
				break;
			case IDC_UCOORD_SPIN2:
				proc.SetOperation(UVWEnumProc::EXPLICITSETU);
				proc.SetVertexIndex(1);
				values[0] = pUI->GetCoordSpinValue(1);
				pInstance->ProcessTextureCoords(proc, pUndo);
				break;
			case IDC_VCOORD_SPIN2:
				proc.SetOperation(UVWEnumProc::EXPLICITSETV);
				proc.SetVertexIndex(1);
				values[1] = pUI->GetCoordSpinValue(5);
				pInstance->ProcessTextureCoords(proc, pUndo);
				break;
			case IDC_UCOORD_SPIN3:
				proc.SetOperation(UVWEnumProc::EXPLICITSETU);
				proc.SetVertexIndex(2);
				values[0] = pUI->GetCoordSpinValue(2);
				pInstance->ProcessTextureCoords(proc, pUndo);
				break;
			case IDC_VCOORD_SPIN3:
				proc.SetOperation(UVWEnumProc::EXPLICITSETV);
				proc.SetVertexIndex(2);
				values[1] = pUI->GetCoordSpinValue(6);
				pInstance->ProcessTextureCoords(proc, pUndo);
				break;
			case IDC_UCOORD_SPIN4:
				proc.SetOperation(UVWEnumProc::EXPLICITSETU);
				proc.SetVertexIndex(3);
				values[0] = pUI->GetCoordSpinValue(3);
				pInstance->ProcessTextureCoords(proc, pUndo);
				break;
			case IDC_VCOORD_SPIN4:
				proc.SetOperation(UVWEnumProc::EXPLICITSETV);
				proc.SetVertexIndex(3);
				values[1] = pUI->GetCoordSpinValue(7);
				pInstance->ProcessTextureCoords(proc, pUndo);
				break;
			};
		}
		break;

	case CC_SPINNER_BUTTONDOWN:
		if (LOWORD(wParam) == IDC_UCOORD_SPIN ||
			LOWORD(wParam) == IDC_UCOORD_SPIN2 ||
			LOWORD(wParam) == IDC_UCOORD_SPIN3 ||
			LOWORD(wParam) == IDC_UCOORD_SPIN4 ||
			LOWORD(wParam) == IDC_VCOORD_SPIN ||
			LOWORD(wParam) == IDC_VCOORD_SPIN2 ||
			LOWORD(wParam) == IDC_VCOORD_SPIN3 ||
			LOWORD(wParam) == IDC_VCOORD_SPIN4)
			theHold.Begin();
		break;

	case CC_SPINNER_BUTTONUP:
		if (LOWORD(wParam) == IDC_UCOORD_SPIN ||
			LOWORD(wParam) == IDC_UCOORD_SPIN2 ||
			LOWORD(wParam) == IDC_UCOORD_SPIN3 ||
			LOWORD(wParam) == IDC_UCOORD_SPIN4 ||
			LOWORD(wParam) == IDC_VCOORD_SPIN ||
			LOWORD(wParam) == IDC_VCOORD_SPIN2 ||
			LOWORD(wParam) == IDC_VCOORD_SPIN3 ||
			LOWORD(wParam) == IDC_VCOORD_SPIN4)
			theHold.Accept("Set texture UV");
		break;

	default:
		return FALSE;
	}
	return TRUE;
}


