//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Standard.rc
//
#define IDS_LIBDESCRIPTION              1
#define IDS_CATEGORY                    2
#define IDS_CLASS_NAME                  3
#define IDS_PARAMS                      4
#define IDS_SPIN                        5
#define IDS_LENGTH                      6
#define IDS_WIDTH                       7
#define IDS_HEIGHT                      8
#define IDS_DOOR                        9
#define IDS_LEFTSTART                   10
#define IDS_LEFTEND                     11
#define IDS_RIGHTSTART                  12
#define IDS_RIGHTEND                    13
#define IDS_TOPSTART                    14
#define IDS_TOPEND                      15
 #define IDS_TOMILOMESH_ATTR             103
#define IDS_STRING104                   104
#define IDC_CLOSEBUTTON                 1000
#define IDC_DOSTUFF                     1000
#define IDC_LEFTSTART_EDIT              1002
#define IDC_LEFTEND_EDIT                1003
#define IDC_RIGHTSTART_EDIT             1004
#define IDC_RIGHTEND_EDIT               1005
#define IDC_TOPSTART_EDIT               1006
#define IDC_TOPEND_EDIT                 1007
#define IDC_COLOR                       1456
#define IDC_EDIT                        1490
#define IDC_LENGTHEDIT                  1490
#define IDC_WIDTHEDIT                   1491
#define IDC_HEIGHTEDIT                  1492
#define IDC_DOOREDIT                    1493
#define IDC_SPIN                        1496
#define IDC_LENSPINNER                  1496
#define IDC_WIDTHSPINNER                1497
#define IDC_HEIGHTSPINNER               1498
#define IDC_DOORSPINNER                 1499
#define IDC_LEFTSTARTSPINNER            1500
#define IDC_LEFTENDSPINNER              1501
#define IDC_RIGHTSTARTSPINNER           1502
#define IDC_RIGHTENDSPINNER             1503
#define IDC_TOPSTARTSPINNER             1504
#define IDC_TOPENDSPINNER               1505

// Next default values for new objects
//
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1008
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
