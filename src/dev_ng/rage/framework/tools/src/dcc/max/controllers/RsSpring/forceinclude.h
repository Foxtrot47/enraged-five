#ifndef __MAX_FORCEINCLUDE_H__
#define __MAX_FORCEINCLUDE_H__

#pragma warning( disable: 4996 )
#pragma warning( disable: 4482 )
#pragma warning( disable: 4800 )



//we have to include max first because it defines some functions
//that are macros in the rage headers (Assert)

#include <max.h>
#include <bmmlib.h>
#include <guplib.h>
#include <istdplug.h>
#include <iparamb2.h>
#include <iparamm2.h>
// #include <modstack.h>
// #include <iskin.h>
// #include <inode.h>
// #include <actiontable.h>
// #include <iskinpose.h>
// #include <imenuman.h>
// #include <cs/bipexp.h>
// #include <stdmat.h>
// #include <imtl.h>
// #include <windows.h>
// #include <shlobj.h>
// #include <hsv.h>
// #include <lslights.h>

#ifdef UNUSED_PARAM
#undef UNUSED_PARAM
#endif // UNUSED_PARAM

#if defined(_DEBUG) && defined(_M_X64)
#include "forceinclude/win64_tooldebug.h"
#elif defined(_DEBUG)
#include "forceinclude/win32_tooldebug.h"
#elif defined(NDEBUG) && defined(_M_X64)
#include "forceinclude/win64_toolrelease.h"
#elif defined(NDEBUG)
#include "forceinclude/win32_toolrelease.h"
#elif defined(_M_X64)
#include "forceinclude/win64_toolbeta.h"
#else
#include "forceinclude/win32_toolbeta.h"
#endif

enum
{
	NAME_LENGTH = 256
};

#define PLUGIN_VERSION 2 //1 = version 0.01
//#define ROCKSTARNORTH

// These macros collide with grcDevice member functions.
#undef IsMinimized
#undef IsMaximized

#endif //__MAX_FORCEINCLUDE_H__
