//
//
//    Filename: asciiattrib.cpp
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: ASCII version of attribute classes
//
//
#include "asciitypes.h"

// STD headers
#include <cassert>

using namespace ascii;

using std::string;
using std::ifstream;
using std::ofstream;
using std::endl;


// --- AsciiAttributeValue ---------------------------------------------------------------------------------

//
//        name: AsciiAttributeValue::AsciiAttributeValue
// description: Constructor from string
//          in: pValue = pointer to string
//
AsciiAttributeValue::AsciiAttributeValue(const char *pValue) : m_pointer(true)
{
	assert(pValue);

	m_value.pString = new char[strlen(pValue) + 1];
	strcpy(m_value.pString, pValue);
}

//
//        name: AsciiAttributeValue::AsciiAttributeValue
// description: Copy Constructor
//
AsciiAttributeValue::AsciiAttributeValue(const AsciiAttributeValue& copy) : m_pointer(copy.m_pointer)
{
	if(m_pointer)
	{
		m_value.pString = new char[strlen(copy.m_value.pString) + 1];
		strcpy(m_value.pString, copy.m_value.pString);
	}
	else
		m_value = copy.m_value;
}

//
//        name: AsciiAttributeValue::AsciiAttributeValue
// description: Destructor
//
AsciiAttributeValue::~AsciiAttributeValue()
{
	if(m_pointer)
		delete[] m_value.pString;
}

//
//        name: operator=
//
AsciiAttributeValue& AsciiAttributeValue::operator=(const AsciiAttributeValue& copy)
{
	if(m_pointer)
		delete[] m_value.pString;

	m_pointer = copy.m_pointer;
	if(m_pointer)
	{
		m_value.pString = new char[strlen(copy.m_value.pString) + 1];
		strcpy(m_value.pString, copy.m_value.pString);
	}
	else
		m_value = copy.m_value;
	return *this;
}

// --- AsciiAttribute --------------------------------------------------------------------------------------

//
//        name: AsciiAttribute::AsciiAttribute
// description: Constructors
//          in: name = name of attribute
//				value = default value
//
AsciiAttribute::AsciiAttribute(const std::string &name, int32 value) : 
m_type(INT), m_name(name), m_value(value)
{
}

AsciiAttribute::AsciiAttribute(const std::string &name, float value) : 
m_type(FLOAT), m_name(name), m_value(value)
{
}

AsciiAttribute::AsciiAttribute(const std::string &name, bool value) : 
m_type(BOOL), m_name(name), m_value(value)
{
}

AsciiAttribute::AsciiAttribute(const std::string &name, const char *pValue) : 
m_type(STRING), m_name(name), m_value(pValue)
{
}

//
//        name: &GetName,GetType,GetDefault
// description: Access functions
//
const std::string &AsciiAttribute::GetName() const
{
	return m_name;
}

AsciiAttribute::Type AsciiAttribute::GetType() const
{
	return m_type;
}

const AsciiAttributeValue &AsciiAttribute::GetValue() const
{
	return m_value;
}

// --- AsciiAttributeList --------------------------------------------------------------------------------------

const std::string &AsciiAttributeList::GetName() const
{
	return m_name;
}

void AsciiAttributeList::SetName(const std::string &name)
{
	m_name = name;
}

//
//        name: AsciiAttributeList::Read\Write
// description: the read and write functions for the AsciiAttributeList class
//
bool AsciiAttributeList::Read(ifstream& is)
{
	string token;
	string name, s;
	bool b;
	int32 i;
	float f;

	if(!StartRead(is))
		return false;
	while(ReadString(is, token))
	{
		if(token == "ClassName")
		{
			if(!ReadString(is, m_name))
				return false;
			continue;
		}
		if(!ReadString(is, name))
			return false;
		if(token == "int"){
			if(!ReadInteger(is, i))
				return false;
			Add(AsciiAttribute(name, i));
		}
		else if(token == "float"){
			if(!ReadFloat(is, f))
				return false;
			Add(AsciiAttribute(name, f));
		}
		else if(token == "bool"){
			if(!ReadBool(is, b))
				return false;
			Add(AsciiAttribute(name, b));
		}
		else if(token == "string"){
			if(!ReadString(is, s))
				return false;
			Add(AsciiAttribute(name, s.c_str()));
		}
		else
			return false;		// unrecognised type
	}
	return true;
}

bool AsciiAttributeList::Write(ofstream& os) const
{
	StartWrite(os, "Attributes");

	WriteString(os, "ClassName", m_name);

	for(uint32 i=0; i<m_attributes.size(); i++)
	{
		const AsciiAttribute& attr = m_attributes[i];
		
		WriteTab(os);

		switch(attr.GetType())
		{
		case AsciiAttribute::INT:
			os << "int\t";
			WriteString(os, attr.GetName());
			os << "\t" << (int32)attr.GetValue() << endl;
			break;
		case AsciiAttribute::FLOAT:
			os << "float\t";
			WriteString(os, attr.GetName());
			os << "\t" << (float)attr.GetValue() << endl;
			break;
		case AsciiAttribute::BOOL:
			os << "bool\t";
			WriteString(os, attr.GetName());
			os << "\t" << (bool)attr.GetValue() << endl;
			break;
		case AsciiAttribute::STRING:
			os << "string\t";
			WriteString(os, attr.GetName());
			os << "\t";
			WriteString(os, (const char *)attr.GetValue());
			os << endl;
			break;
		}
	}

	EndWrite(os);

	return true;
}

