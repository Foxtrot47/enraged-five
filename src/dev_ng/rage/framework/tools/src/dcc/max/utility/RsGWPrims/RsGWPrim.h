#pragma once
#include "3dsmaxsdk_preinclude.h"
#include "Max.h"
#include "resource.h"
#include "istdplug.h"
#include "iparamb2.h"
#include "iparamm2.h"
//SIMPLE TYPE

#include "utilapi.h"
extern TCHAR *GetString(int id);

#include "utilapi.h"
#include "ifnpub.h"

// solution includes
#include "RsGWPrimsInterface.h"

// helper includes
#include <vector>

using namespace std;

#define MAX_PRIM_POINTS 256

class RsGWPrim :
	public IObject,
	public RsGWPrimsInterface
{
public:
	RsGWPrim(void);
	~RsGWPrim(void);
	// Foo methods
	//...

	// IObject methods
	TCHAR* GetName() { return _T("IRsGWPrim"); }
	int NumInterfaces() const { return 1; }

	BaseInterface* GetInterfaceAt(int i) const
	{
		if (i == 0) 
			return (RsGWPrimsInterface*)this;
		else 
			return NULL;
	}

	BaseInterface* GetInterface(Interface_ID id)
	{
		if (id == RSGWPRIMS_INTERFACE) 
			return (RsGWPrimsInterface*)this; 
		else if (id == Interface_ID(0x60fb7eef, 0x1f6d6dd3)) 
			return (BaseInterface*)this; 
		else { 
			return IObject::GetInterface(id);
		}
		return 0;
	}
	// IFoo1 methods
	static Color GetColour()
	{
		return m_globalColour;
	}

	static void SetGlobalColour(Color colour)
	{
		m_globalColour = colour;
	}
	void SetColour(Color colour)
	{
		m_globalColour = colour;
	}

	// functionality
	struct sGwPolyLine 
	{
		vector<Point3> points;
		vector<Point3> colours;
		vector<int> edgeVis;
		bool closed;
		sGwPolyLine()
			: closed(false)
		{
		}
	};

	void SetOrigin(Matrix3 origin, bool setNow=true)
	{
		m_origin = origin;
		SetInterfacePointer();
		if(setNow)
		{
			ViewExp *vpt = ip->GetActiveViewport();
			GraphicsWindow *gw = vpt->getGW();
			gw->setTransform(m_origin);
		}
	}
	void SetResolution(int resolution)
	{
		m_resolution = resolution;
	}
	void AddCircle(float radius, Matrix3 localTrans);
	void AddSphere(float radius, Matrix3 localTrans);
	void AddCone(float length, float radius, BOOL cap=true, Matrix3 localTrans=NULL);
	void AddCylinder(float length, float radius, Matrix3 localTrans);
	void AddCapsule(float length, float radius, BOOL cap=true, Matrix3 localTrans=NULL);
	void AddLine(Tab<Point3*> points, Matrix3 localTrans);
	void Draw(BOOL flush=true);
	void Clear()
	{
		m_polylines.clear();
	}

private:
	void SetInterfacePointer()
	{
		if(!ip)
			ip = GetCOREInterface();
	}

	// Core
	Interface		*ip;

	// functionalilty
	Matrix3 m_origin;
	static Color m_globalColour;
	int m_resolution;
	vector<sGwPolyLine*> m_polylines;

	// caches
	static Point3	*colourCache;
	static int		*edgeVisCache;
};

typedef vector<RsGWPrim::sGwPolyLine*> PolyLineVec;

// #define RSGWPRIM_CLASS_ID	Class_ID(0x41c23bca, 0x7e573773)
// 
// class RsGWPrimClassDesc : public ClassDesc2 
// {
// public:
// 	virtual int IsPublic() 							{ return TRUE; }
// 	virtual void* Create(BOOL /*loading = FALSE*/) 	{ return new RsGWPrim(); }
// 	virtual const TCHAR *	ClassName() 			{ return GetString(IDS_CLASS_NAME); }
// 	virtual SClass_ID SuperClassID() 				{ return RSGWPRIM_CLASS_ID.PartA(); }
// 	virtual Class_ID ClassID() 						{ return RSGWPRIM_CLASS_ID; }
// 	virtual const TCHAR* Category() 				{ return GetString(IDS_CATEGORY); }
// 
// 	virtual const TCHAR* InternalName() 			{ return _T("RsGWPrim"); }	// returns fixed parsable name (scripter-visible name)
// 	virtual HINSTANCE HInstance() 					{ return hInstance; }					// returns owning module handle
// };

enum 
{
	primMgr_getPrim
};
class RsGWPrimMgrImp : public FPStaticInterface
{
	DECLARE_DESCRIPTOR(RsGWPrimMgrImp)
	BEGIN_FUNCTION_MAP
		FN_0(primMgr_getPrim, TYPE_IOBJECT, GetPrim);
	END_FUNCTION_MAP

	RsGWPrim *GetPrim(void);
};