//
//
//    Filename: PathCreateMgr.h
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Creation manager for Vehicle Paths. Connects previously created connection to new connection
//
//
#ifndef INC_PATH_CREATE_MGR_H_
#define INC_PATH_CREATE_MGR_H_

#include <Max.h>


#define CID_VEHICLE_PATH	CID_USER + 0x383

class VehicleNode;
//class VehiclePath;

//
//   Class Name: VehiclePathCreateMgr
// Base Classes: 
//  Description: 
//    Functions: 
//
//
class VehiclePathCreateMgr : public MouseCallBack, public ReferenceMaker, public PickNodeCallback
{
private:
	INode *m_pThis;
	INode *m_pCanConnectTo;
	VehicleNode *m_pPrev;
	VehiclePath *m_pPath;
	IObjCreate *m_ip;

public:
	VehiclePathCreateMgr();
	~VehiclePathCreateMgr();

	void Begin( IObjCreate *ioc, ClassDesc *desc );
	void End();
	// ReferenceMaker
	int NumRefs();
	RefTargetHandle GetReference(int i);
	void SetReference(int i, RefTargetHandle rtarg);
	RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,  RefMessage message);

	// MouseCallback
	int proc(HWND hwnd, int msg, int point, int flags, IPoint2 m);

	// PickNodeCallback
	BOOL Filter(INode *pNode);
};

//
//   Class Name: VehiclePathCreateMode
// Base Classes: 
//  Description: 
//    Functions: 
//
//
class VehiclePathCreateMode : public CommandMode
{
private:
	VehiclePathCreateMgr m_createMgr;

public:

	void Begin( IObjCreate *ioc, ClassDesc *desc ) {/* m_proc.Begin( ioc, desc ); } /*/m_createMgr.Begin( ioc, desc ); }
	void End() { m_createMgr.End(); }
	// from CommandMode
	int Class() {return CREATE_COMMAND;}
	int ID() {return CID_VEHICLE_PATH;}
	MouseCallBack *MouseProc(int *numPoints) {*numPoints = 10000; return &m_createMgr;}
	ChangeForegroundCallback *ChangeFGProc() { return CHANGE_FG_SELECTED; }
	BOOL ChangeFG( CommandMode *oldMode ) { return (oldMode->ChangeFGProc() != CHANGE_FG_SELECTED); }
	void EnterMode() {}
	void ExitMode() {}

private:
//	CreateVehiclePathMouseCallback m_proc;
};


extern VehiclePathCreateMode theVehiclePathCreateMode;

#endif // INC_PATH_CREATE_MGR_H_