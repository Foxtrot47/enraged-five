//
//
//    Filename: StuntJumpNode.h
//     Creator: Greg Smith
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Single box object within a stunt jump
//
//

#ifndef INC_queueNODE__H
#define INC_queueNODE__H

// Max SDK headers
#include <Max.h>
#include <istdplug.h>
#include <iparamm2.h>
// resources header
//#include "resource.h"
// My headers
#include "Iqueue.h"

extern HINSTANCE hInstance;

TCHAR *GetString(int id);

#define QUEUE_NODE_LEVEL	(1)

//
//   Class Name: queueNode
// Base Classes: HelperObject
//  Description:
//    Functions:
//
//
class queueNode : public HelperObject
{
private:
	IParamBlock2 *m_pBlock2;
	TriObject* m_pTriObject;
	Point3 m_pntDrawCol;

public:
	//Constructor/Destructor
	queueNode();
	~queueNode();

	void setDrawCol(const Point3& pntCol) { m_pntDrawCol = pntCol; }

	// Animatable methods
	int NumParamBlocks() {return 1;}
	IParamBlock2* GetParamBlock(int i) {assert(i == 0); return m_pBlock2;}
	IParamBlock2* GetParamBlockByID(short id) {assert(id == queuenode_params); return m_pBlock2;}
	Class_ID ClassID() {return queueNODE_CLASS_ID;}
	void GetClassName(TSTR& s) {s = _T("queueNode");}
	void BeginEditParams( IObjParam  *ip, ULONG flags,Animatable *prev);
	void EndEditParams( IObjParam *ip, ULONG flags,Animatable *next);
	void DeleteThis() {delete this;}

	// ReferenceTarget methods
#if MAX_VERSION_MAJOR >= 9 
	ReferenceTarget* Clone(RemapDir &remap = DefaultRemapDir());
#else
	ReferenceTarget* Clone(RemapDir &remap = NoRemap());
#endif

	IOResult Load(ILoad *iload);
	IOResult Save(ISave *isave);
	int NumRefs();
	RefTargetHandle GetReference(int i);
	void SetReference(int i, RefTargetHandle pTarg);
	RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,  RefMessage message);

	//From BaseObject
	CreateMouseCallBack* GetCreateMouseCallBack();
	void GetLocalBoundBox(TimeValue t, INode* pNode, ViewExp* pVp, Box3& box);
	void GetWorldBoundBox(TimeValue t, INode* pNode, ViewExp* pVp, Box3& box);
	int Display(TimeValue t, INode* pNode, ViewExp *pVpt, int flags);
	int HitTest(TimeValue t, INode* pNode, int type, int crossing, int flags, IPoint2 *pSp, ViewExp *pVpt);
	TCHAR *GetObjectName() { return _T("queueNode"); }
	int GetSubObjectLevel( ) { return ( QUEUE_NODE_LEVEL ); }

	void BuildMesh(TimeValue t);
	void DrawMesh(GraphicsWindow *gw, TimeValue t, Matrix3& objMat);
	void addBox(Point3 pntCentre,Point3 pntDimension,int& iVertOffset,int& iFaceOffset);

	// From Object
	ObjectState Eval(TimeValue t);
	Interval ObjectValidity(TimeValue t);
	void InitNodeName(TSTR &s) { s = _T("queueNode"); }
	BOOL IsWorldSpaceObject() {return TRUE;}
};


//
//   Class Name: ObjPathNodeClassDesc
// Base Classes: ClassDesc
//  Description:
//    Functions:
//
//
class queueNodeClassDesc : public ClassDesc2
{

public:
	int IsPublic() {return 0;}
	void* Create(BOOL loading = FALSE);
	const TCHAR *ClassName() {return _T("queueNode");}
	SClass_ID SuperClassID() {return HELPER_CLASS_ID;}
	Class_ID ClassID() {return queueNODE_CLASS_ID;}
	const TCHAR* Category() {return _T("Gta Helpers");}
	void ResetClassParams (BOOL fileReset);

	// Hardwired name, used by MAX Script as unique identifier
	const TCHAR*	InternalName() { return _T("queueNode"); }
	HINSTANCE		HInstance()	{ return hInstance; }
};


#endif // INC_queueNODE__H