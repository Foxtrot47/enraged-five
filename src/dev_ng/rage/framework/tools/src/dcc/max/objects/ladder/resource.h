//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ladderNode.rc
//
#define IDS_LIBDESCRIPTION              1
#define IDS_CATEGORY                    2
#define IDS_CLASS_NAME                  3
#define IDS_PARAMS                      4
#define IDS_SPIN                        5
#define IDS_EXTERNAL                    5
#define IDS_TARGET                      6
#define IDS_TYPE                        7
#define IDS_NODEPARAMS                  8
#define IDS_NODE_CLASS_NAME             9
#define IDS_VERSION                     10
#define IDS_LENGTH                      11
#define IDS_LANESIN                     12
#define IDS_LANESOUT                    13
#define IDS_LINKTYPE                    14
#define IDS_WIDTH                       15
#define IDS_LANESIGNORED                16
#define IDS_ISLANDWIDTH                 16
#define IDS_HEIGHT                      16
#define IDS_CARSPEED                    17
#define IDS_DISABLENODE                 18
#define IDS_ROADBLOCK                   19
#define IDS_BETWEENLEVELS               20
#define IDS_DENSITY                     21
#define IDS_UNDERBRIDGE                 22
#define IDS_FXLADDER                    24
#define IDS_LADDER_NODE                 25
#define IDS_LEDGE                       26
#define IDS_RS_LEDGE                    26
#define IDD_PANEL                       101
#define IDD_NODE_PANEL                  101
#define IDD_PATH_PANEL                  102
#define IDC_CONNECT_CURSOR1             104
#define IDB_DMAMAN                      111
#define IDC_CLOSEBUTTON                 1000
#define IDC_DOSTUFF                     1000
#define IDC_EXTERNAL                    1002
#define IDC_PED_RADIO                   1003
#define IDC_VEHICLE_RADIO               1004
#define IDC_DELETE                      1005
#define IDC_WATER_RADIO                 1005
#define IDC_ADD                         1006
#define IDC_SELECT                      1007
#define IDC_ADDLINE                     1008
#define IDC_LENSPINNER                  1009
#define IDC_LANES_STATIC                1010
#define IDC_HGTSPINNER                  1010
#define IDC_IN_STATIC                   1011
#define IDC_OUT_STATIC                  1012
#define IDC_ROADLINK                    1013
#define IDC_LANESIN_SPIN                1014
#define IDC_VERSION                     1015
#define IDC_LANESOUT_SPIN               1016
#define IDC_PAVEMENTLINK                1017
#define IDC_WIDTHSPINNER                1018
#define IDC_LANES_IGNORED_SPIN          1019
#define IDC_ISLANDWIDTH_SPIN            1019
#define IDC_LANES_IGNORED_STATIC        1020
#define IDC_IGNORE_STATIC               1020
#define IDC_ISLANDWIDTH_STATIC          1020
#define IDC_WIDTH                       1021
#define IDC_DISABLE_CHECK               1022
#define IDC_ROADBLOCK_CHECK             1023
#define IDC_AUTHOR_TEXT                 1024
#define IDC_BETWEENLEVELS_CHECK         1025
#define IDC_SPEED1_RADIO                1026
#define IDC_SPEED2_RADIO                1027
#define IDC_SPEED3_RADIO                1028
#define IDC_SPEED_STATIC                1029
#define IDC_DENSITY_SPIN                1030
#define IDC_DENSITY_STATIC              1031
#define IDC_UNDERBRIDGE_CHECK           1033
#define IDC_SKATEABLE_CHECK             1034
#define IDC_COVER_CHECK                 1035
#define IDC_PIGEONS_CHECK               1036
#define IDC_PARALLELP_CHECK             1037
#define IDC_PERPENP_CHECK               1038
#define IDC_VALETP_CHECK                1039
#define IDC_CLUBDROPOFF_CHECK           1040
#define IDC_DONTWANDER_CHECK            1041
#define IDC_CBO_SPECIAL                 1042
#define IDC_EDT_NAME                    1044
#define IDC_COLOR                       1456
#define IDC_EDIT                        1490
#define IDC_SPIN                        1496
#define IDC_LENGTHEDIT                  3009
#define IDC_WIDTHEDIT                   3010
#define IDC_HEIGHTEDIT                  3011
#define IDC_LANESIN_EDIT                3013
#define IDC_LANESOUT_EDIT               3014
#define IDC_LANES_IGNORED_EDIT          3015
#define IDC_ISLANDWIDTH_EDIT            3015
#define IDC_DENSITY_EDIT                3016

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1045
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
