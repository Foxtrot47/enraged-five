#include <max.h>
#include <modstack.h>
// define the new primitives using macros from SDK
#include <maxscript/maxscript.h>
#include <maxscript/maxwrapper/maxclasses.h>
#include <maxscript/foundation/Numbers.h>
#include <maxscript/foundation/strings.h>
#include <maxscript/macros/define_instantiation_functions.h> // LPXO: define me last
#include <maxscript/util/Listener.h>

#include "resource.h"
#include "triobjed.h"

def_visible_primitive( mesh_to_milo,"mesh2milo" );

/////////////////////////////////////////////////////////////////////////////////////////////////
Value* mesh_to_milo_cf(Value** arg_list, int count)
{
	check_arg_count(mesh_to_col, 1, count);
	type_check(arg_list[0], MAXNode, "mesh2milo [node]");

	ConvertToMiloMesh(arg_list[0]->to_node(),MAXScript_interface);

	return &true_value;
}