//
//
//    Filename: queueNode.cpp
//     Creator: Greg Smith
//     $Author: $
//       $Date: $
//   $Revision: $
// Description:
//
//

#include "queueNode.h"
#include "resource.h"

#define NUM_CIRCLE_SEGMENTS	16
#define OBJPATHNODE_CIRCLE_RADIUS	1
#define OBJPATHNODE_SCREEN_RATIO		1.0f/60.0f

// globals
static queueNodeClassDesc thequeueNodeDesc;


ClassDesc* GetqueueNodeDesc() {return &thequeueNodeDesc;}

// --- ObjPathNodeClassDesc ------------------------------------------------------------------------------------

void *queueNodeClassDesc::Create(BOOL loading)
{
	return new queueNode;
}
void queueNodeClassDesc::ResetClassParams (BOOL fileReset)
{

}

ParamBlockDesc2 pathNodeDesc(queuenode_params, _T("queueNode Parameters"), IDS_CLASS_NAME, &thequeueNodeDesc, P_AUTO_CONSTRUCT, queueNODE_PBLOCK_REF_ID,
						 queuenode_length, _T("Length"), TYPE_WORLD, P_ANIMATABLE, IDS_LENGTH,
							p_default,		5.0f,
						 end,
						end
						);

//--- StuntJumpNode -------------------------------------------------------

queueNode::queueNode():
	m_pBlock2(NULL),
	m_pntDrawCol(1.0f,1.0f,1.0f)
{
	TriObject *pNewTri = CreateNewTriObject();

	SetReference(queueNODE_MESH_REF_ID,pNewTri);

	thequeueNodeDesc.MakeAutoParamBlocks(this);
}

queueNode::~queueNode()
{
}


RefTargetHandle queueNode::Clone(RemapDir& remap)
{
	queueNode* pClone = new queueNode();
	pClone->ReplaceReference(queueNODE_PBLOCK_REF_ID, remap.CloneRef(m_pBlock2));
	BaseClone(this, pClone, remap);
	return pClone;
}

Interval queueNode::ObjectValidity(TimeValue t)
{
	Interval ivalid = FOREVER;

	m_pBlock2->GetValidity(t, ivalid);
	return ivalid;
}

void queueNode::BeginEditParams( IObjParam  *ip, ULONG flags,Animatable *prev)
{
	HelperObject::BeginEditParams(ip,flags,prev);
	thequeueNodeDesc.BeginEditParams(ip,this,flags,prev);
}

void queueNode::EndEditParams(IObjParam *ip, ULONG flags,Animatable *next)
{
	thequeueNodeDesc.EndEditParams(ip,this,flags,next);
	HelperObject::EndEditParams(ip,flags,next);
}

IOResult queueNode::Load(ILoad *iload)
{
	return IO_OK;
}

IOResult queueNode::Save(ISave *isave)
{
	return IO_OK;
}

int queueNode::NumRefs()
{
	return 2;
}

RefTargetHandle queueNode::GetReference(int i)
{
	switch(i)
	{
	case queueNODE_PBLOCK_REF_ID:
		return m_pBlock2;
	case queueNODE_MESH_REF_ID:
		return m_pTriObject;
	}
	return NULL;
}

void queueNode::SetReference(int i, RefTargetHandle rtarg)
{
	switch(i)
	{
	case queueNODE_PBLOCK_REF_ID:
		m_pBlock2 = (IParamBlock2 *)rtarg;
		break;
	case queueNODE_MESH_REF_ID:
		m_pTriObject = (TriObject *)rtarg;
		break;
	}
}

RefResult queueNode::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget,
										   PartID& partID,  RefMessage message)
{
	return REF_SUCCEED;
}

CreateMouseCallBack* queueNode::GetCreateMouseCallBack()
{
	return NULL;
}

void queueNode::GetLocalBoundBox(TimeValue t, INode* pNode, ViewExp* pVpt, Box3& box)
{
	if(m_pTriObject)
		box = m_pTriObject->mesh.getBoundingBox();
	else
		box.Init();
}

void queueNode::GetWorldBoundBox(TimeValue t, INode* pNode, ViewExp* pVpt, Box3& box)
{
	Matrix3 local2world = pNode->GetObjectTM(t);
	Box3 localBox;

	GetLocalBoundBox(t, pNode, pVpt, localBox);

	box = localBox * local2world;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
ObjectState queueNode::Eval(TimeValue t)
{
	BuildMesh(t);
	return ObjectState(this);
}

#define POSX 0	// right
#define POSY 1	// back
#define POSZ 2	// top
#define NEGX 3	// left
#define NEGY 4	// front
#define NEGZ 5	// bottom

static int mapDir[6] ={ 3, 5, 0, 2, 4, 1 };

int direction(Point3 *v) {
	Point3 a = v[0]-v[2];
	Point3 b = v[1]-v[0];
	Point3 n = CrossProd(a,b);
	switch(MaxComponent(n)) {
	case 0: return (n.x<0)?NEGX:POSX;
	case 1: return (n.y<0)?NEGY:POSY;
	case 2: return (n.z<0)?NEGZ:POSZ;
	}
	return 0;
}

static void MakeQuad(int nverts, Face *f, int a, int b , int c , int d, int sg, int bias, int flag) {
	int sm = 1<<sg;
	assert(a<nverts);
	assert(b<nverts);
	assert(c<nverts);
	assert(d<nverts);
	if (bias) {
		f[0].setVerts( b, a, c);
		f[0].setSmGroup(sm);
		f[0].setEdgeVisFlags(1,0,1);
		f[1].setVerts( d, c, a);
		f[1].setSmGroup(sm);
		f[1].setEdgeVisFlags(1,0,1);
	} else {
		f[0].setVerts( a, b, c);
		f[0].setSmGroup(sm);
		f[0].setEdgeVisFlags(1,1,0);
		f[1].setVerts( c, d, a);
		f[1].setSmGroup(sm);
		f[1].setEdgeVisFlags(1,1,0);
	}
	if(f)
	{
		f[0].flags |= flag;
		f[1].flags |= flag;
	}
}

#define MAKE_QUAD(na,nb,nc,nd,sm,b, f) {MakeQuad(nverts,&(mesh.faces[nf]),na, nb, nc, nd, sm, b, f);nf+=2;}

/////////////////////////////////////////////////////////////////////////////////////////////////
void queueNode::BuildMesh(TimeValue t)
{
	int ix,iy,iz,nf,kv,mv,nlayer,topStart,midStart;
	int wsegs,lsegs,hsegs,nv,wsp1,nverts,nfaces;
	int workFlag = 0;
	Point3 va,vb,p;
	float l, w, h;
	int genUVs = 1;
	BOOL bias = 0;
	Mesh& mesh = m_pTriObject->mesh;

	l = w = h = 0.1f;

	if (h<0.0f) bias = 1;

	lsegs = 1;
	wsegs = 1;
	hsegs = 1;

	wsp1 = wsegs + 1;
	nlayer  =  2*(lsegs+wsegs);
	topStart = (lsegs+1)*(wsegs+1);
	midStart = 2*topStart;

	nverts = midStart+nlayer*(hsegs-1);
	nfaces = 4*(lsegs*wsegs + hsegs*lsegs + wsegs*hsegs);

	mesh.setNumVerts(nverts);
	mesh.setNumFaces(nfaces);
	mesh.InvalidateTopologyCache();

	nv = 0;

	vb =  Point3(w,l,h)/float(2);
	va = -vb;

	float dx = w/wsegs;
	float dy = l/lsegs;
	float dz = h/hsegs;

	// do bottom vertices.
	p.z = va.z;
	p.y = va.y;
	for(iy=0; iy<=lsegs; iy++) {
		p.x = va.x;
		for (ix=0; ix<=wsegs; ix++) {
			mesh.setVert(nv++, p);
			p.x += dx;
		}
		p.y += dy;
	}

	nf = 0;

//	workFlag = FACE_WORK;
	workFlag = 0;

	// do bottom faces.
	for(iy=0; iy<lsegs; iy++) {
		kv = iy*(wsegs+1);
		for (ix=0; ix<wsegs; ix++) {
			MAKE_QUAD(kv, kv+wsegs+1, kv+wsegs+2, kv+1, 1, bias, workFlag);
			kv++;
		}
	}
	assert(nf==lsegs*wsegs*2);

	// do top vertices.
	p.z = vb.z;
	p.y = va.y;
	for(iy=0; iy<=lsegs; iy++) {
		p.x = va.x;
		for (ix=0; ix<=wsegs; ix++) {
			mesh.setVert(nv++, p);
			p.x += dx;
		}
		p.y += dy;
	}

	// do top faces (lsegs*wsegs);
	for(iy=0; iy<lsegs; iy++) {
		kv = iy*(wsegs+1)+topStart;
		for (ix=0; ix<wsegs; ix++) {
			MAKE_QUAD(kv, kv+1, kv+wsegs+2,kv+wsegs+1, 2, bias, workFlag);
			kv++;
		}
	}
	assert(nf==lsegs*wsegs*4);

	// do middle vertices
	for(iz=1; iz<hsegs; iz++) {

		p.z = va.z + dz * iz;

		// front edge
		p.x = va.x;  p.y = va.y;
		for (ix=0; ix<wsegs; ix++) { mesh.setVert(nv++, p);  p.x += dx;	}

		// right edge
		p.x = vb.x;	  p.y = va.y;
		for (iy=0; iy<lsegs; iy++) { mesh.setVert(nv++, p);  p.y += dy;	}

		// back edge
		p.x =  vb.x;  p.y =  vb.y;
		for (ix=0; ix<wsegs; ix++) { mesh.setVert(nv++, p);	 p.x -= dx;	}

		// left edge
		p.x = va.x;  p.y =  vb.y;
		for (iy=0; iy<lsegs; iy++) { mesh.setVert(nv++, p);	 p.y -= dy;	}
	}

	// do LEFT faces -----------------------
	kv = 0;
	mv = topStart;
	for (ix=0; ix<wsegs; ix++) {
		MAKE_QUAD(kv, kv+1, mv+1, mv, 5, bias, workFlag);
		kv++;
		mv++;
	}

	// do RIGHT faces.-----------------------
	kv = wsegs;
	mv = topStart + kv;
	for (iy=0; iy<lsegs; iy++) {
		MAKE_QUAD(kv, kv+wsp1, mv+wsp1, mv, 4, bias, workFlag);
		kv += wsp1;
		mv += wsp1;
	}

	// do BACK faces.-----------------------
	kv = topStart - 1;
	mv = midStart - 1;
	for (ix=0; ix<wsegs; ix++) {
		MAKE_QUAD(kv, kv-1, mv-1, mv, 5, bias, workFlag);
		kv --;
		mv --;
	}

	// do LEFT faces.----------------------
	kv = lsegs*(wsegs+1);  // index into bottom
	mv = topStart + kv;
	for (iy=0; iy<lsegs; iy++) {
		MAKE_QUAD(kv, kv-wsp1, mv-wsp1, mv, 6, bias, workFlag);
		kv -= wsp1;
		mv -= wsp1;
	}

	mesh.setNumTVerts(0);
	mesh.setNumTVFaces(0);
	for (nf = 0; nf<nfaces; nf++)
	{
		Face& f = mesh.faces[nf];
		DWORD* nv = f.getAllVerts();
		Point3 v[3];
		for (int ix =0; ix<3; ix++)
			v[ix] = mesh.getVert(nv[ix]);
		int dir = direction(v);
		mesh.setFaceMtlIndex(nf,mapDir[dir]);
	}

	mesh.DeleteFlaggedFaces();
	mesh.DeleteIsoVerts();

	mesh.InvalidateGeomCache();
	mesh.BuildStripsAndEdges();
}


/////////////////////////////////////////////////////////////////////////////////////////////////
void queueNode::DrawMesh(GraphicsWindow *gw, TimeValue t, Matrix3& objMat)
{
	if(!m_pTriObject)
	{
		return;
	}
	objMat.NoRot();

	Mesh& mesh = m_pTriObject->mesh;

	DrawLineProc lineProc(gw);
	int i;
	Point3 pt[4];
	Point3 normal;
	Matrix3 invCamMat;
	float camMat[4][4];
	int persp;
	float hither, yon;

	m_pTriObject->mesh.checkNormals(TRUE);

	gw->getCameraMatrix(camMat, &invCamMat, &persp, &hither, &yon);
	invCamMat.NoTrans();
	invCamMat.NoScale();
	objMat.NoTrans();
	objMat.NoScale();

	invCamMat = objMat * Inverse(invCamMat);

	lineProc.SetLineColor(m_pntDrawCol.x,m_pntDrawCol.y,m_pntDrawCol.z);

	for(i=0; i<mesh.numFaces; i++)
	{
		normal = mesh.getFaceNormal(i);
		if((normal * invCamMat)[2] < 0)
			continue;
		pt[0] = mesh.verts[mesh.faces[i].v[0]];
		pt[1] = mesh.verts[mesh.faces[i].v[1]];
		pt[2] = mesh.verts[mesh.faces[i].v[2]];
		pt[3] = mesh.verts[mesh.faces[i].v[0]];
		lineProc.proc(pt,4);
	}

	IParamBlock2 *pNodeBlock2 = GetParamBlockByID(queuenode_params);
	int iType = pNodeBlock2->GetInt(queuenode_length);

	if(iType == 0)
	{
		gw->text(&mesh.verts[0],"Queue");
	}
	else if(iType == 1)
	{
		gw->text(&mesh.verts[0],"Direction");
	}
	else if(iType == 2)
	{
		gw->text(&mesh.verts[0],"Use");
	}
	else if(iType == 3)
	{
		gw->text(&mesh.verts[0],"Forward");
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////
int queueNode::Display(TimeValue t, INode* pNode, ViewExp *pVpt, int flags)
{
	GraphicsWindow *gw = pVpt->getGW();
	Matrix3 mat = pNode->GetObjectTM(t);
	gw->setTransform(mat);

	DrawMesh(gw,t,mat);

	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
int queueNode::HitTest(TimeValue t, INode* pNode, int type, int crossing, int flags, IPoint2 *pSp, ViewExp *pVpt)
{
	HitRegion hitRegion;
	DWORD savedLimits;
	GraphicsWindow *gw = pVpt->getGW();
	Matrix3 objMat = pNode->GetObjectTM(t);
	MakeHitRegion(hitRegion, type, crossing, 4, pSp);

	gw->setTransform(objMat);
	gw->setRndLimits(((savedLimits = gw->getRndLimits()) | GW_PICK) & ~GW_ILLUM);
	gw->setHitRegion(&hitRegion);
	gw->clearHitCode();

	DrawMesh(gw, t, objMat);

	int res = gw->checkHitCode();

	gw->setRndLimits(savedLimits);

	return res;
}
