//
// filename:	DLLEntry.cpp
// author:		David Muir
// date:		23 January 2007
// description:	Scrollbars 3D Studio Max Plugin DLL Entry-Point(s)
//

// --- Include Files ------------------------------------------------------------

// 3D Studio Max SDK headers
#include "Max.h"

// Local headers
#include "Scrollbars.h"
#include "ScrollbarsPoint.h"
#include "Resource.h"
#include "Win32Res.h"

// --- Constants ----------------------------------------------------------------
const int	sC_cnt_NumberOfClasses = 2;

// --- Globals ------------------------------------------------------------------

HINSTANCE	g_hInstance = NULL;
int			g_ControlsInit = FALSE;

// --- Code ---------------------------------------------------------------------

//
// name:		DllMain
// description:	DLL Plugin Entry-Point
//
BOOL WINAPI DllMain( HINSTANCE hInstDLL, ULONG dwReason, LPVOID plReserved )
{
	// Store the DLLs instance handle
	g_hInstance = hInstDLL;

	if ( FALSE == g_ControlsInit )
	{
#if MAX_VERSION_MAJOR < 11
		// Initialise 3DS Max's Custom Controls
		InitCustomControls( g_hInstance );
#endif // MAX_VERSION_MAJOR < 11
		// Initialise platform common controls
		InitCommonControls( );
		g_ControlsInit = TRUE;
	}

	return TRUE;
}

//
// name:		LibDescription
// description:	Return plugin description string (Max API)
//
__declspec( dllexport ) const TCHAR* LibDescription()
{
	return GetStringResource( g_hInstance, IDS_DESCRIPTION );
}

//
// name:		LibNumberClasses
// description:	Return number of classes registered by plugin (Max API)
//
__declspec( dllexport ) int LibNumberClasses( )
{
	return sC_cnt_NumberOfClasses;
}

//
// name:		LibClassDesc
// description:	Return indexed class description class
//
__declspec( dllexport ) ClassDesc* LibClassDesc( int nClassIndex )
{
	assert( nClassIndex < sC_cnt_NumberOfClasses );
	switch( nClassIndex ) 
	{
		case 0: 
			return GetScrollbarsDesc( );
		case 1:
			return GetScrollbarsPointDesc( );
		default: 
			return NULL;
	}
}

//
// name:		LibVersion
// description:	Return version of Max SDK plugin was compiled with
//
__declspec( dllexport ) ULONG LibVersion( )
{
	return VERSION_3DSMAX;
}


//
// name:		CanAutoDefer
// description:	Plugin DLL must not defer loading (required by GTA3)
//
__declspec( dllexport ) ULONG CanAutoDefer( ) 
{ 
	return 0; 
}

// End of file
