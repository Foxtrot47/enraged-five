
#include "Standard.h"

#include "Max.h"
#include "resource.h"
#include "istdplug.h"
#include "iparamm2.h"
#include "Simpobj.h"


/////////////////////////////////////////////////////////////////////////////////////////////////
Standard::Standard(Class_ID& classID,const char* p_cName):
	m_pTriObject(NULL)
{
	m_classID = classID;
	strcpy(m_cClassName,p_cName);

	TriObject *pNewTri = CreateNewTriObject();

	ReplaceReference(STANDARD_MESH_REF,pNewTri);

//	StandardDesc.MakeAutoParamBlocks(this);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
Standard::~Standard()
{
	//delete m_pTriObject;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
RefTargetHandle Standard::GetReference(int i)
{
	switch(i)
	{
	case STANDARD_PBLOCK_REF:
		return NULL;
	case STANDARD_MESH_REF:
		return m_pTriObject;
	}

	return NULL;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void Standard::SetReference(int i, RefTargetHandle rtarg)
{
	switch(i)
	{
	case STANDARD_PBLOCK_REF:
		break;
	case STANDARD_MESH_REF:
		m_pTriObject = (TriObject*)rtarg;
		break;
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////
RefResult Standard::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,RefMessage message)
{
	return REF_SUCCEED;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
ObjectState Standard::Eval(TimeValue t)
{
	BuildMesh(t);
	return ObjectState(this);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void Standard::BeginEditParams( IObjParam *ip, ULONG flags,Animatable *prev)
{
	HelperObject::BeginEditParams(ip,flags,prev);
//	StandardDesc.BeginEditParams(ip,this,flags,prev);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void Standard::EndEditParams( IObjParam *ip, ULONG flags,Animatable *next)
{
	HelperObject::EndEditParams(ip,flags,next);
//	StandardDesc.EndEditParams(ip,this,flags,next);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void Standard::GetWorldBoundBox(TimeValue t, INode* inode, ViewExp* vp, Box3& box )
{
	Matrix3 local2world = inode -> GetObjectTM(t);
	Box3 localBox;

	GetLocalBoundBox(t, inode, vp, localBox);

	box = localBox * local2world;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void Standard::GetLocalBoundBox(TimeValue t, INode* inode, ViewExp* vp, Box3& box )
{
	if(m_pTriObject)
		box = m_pTriObject->mesh.getBoundingBox();
	else
		box.Init();
}

/////////////////////////////////////////////////////////////////////////////////////////////////
int Standard::Display(TimeValue t, INode* inode, ViewExp *vpt, int flags)
{
	GraphicsWindow *gw = vpt->getGW();
	Matrix3 mat = inode->GetObjectTM(t);
	gw->setTransform(mat);

	DrawMesh(gw,t,mat,inode,vpt);

	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
int Standard::HitTest(TimeValue t, INode* inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt)
{
	HitRegion hitRegion;
	DWORD savedLimits;
	GraphicsWindow *gw = vpt->getGW();
	Matrix3 objMat = inode->GetObjectTM(t);
	MakeHitRegion(hitRegion, type, crossing, 4, p);

	gw->setTransform(objMat);
	gw->setRndLimits(((savedLimits = gw->getRndLimits()) | GW_PICK) & ~GW_ILLUM);
	gw->setHitRegion(&hitRegion);
	gw->clearHitCode();

	DrawMesh(gw, t, objMat,inode,vpt);

	int res = gw->checkHitCode();

	gw->setRndLimits(savedLimits);

	return res;
}

void GetMat(TimeValue t, INode* inode, ViewExp* vpt, Matrix3& tm) 
{
	tm = inode->GetObjectTM(t);
	tm.NoScale();
	float scaleFactor = vpt->NonScalingObjectSize()*vpt->GetVPWorldWidth(tm.GetTrans())/(float)360.0;
	tm.Scale(Point3(scaleFactor,scaleFactor,scaleFactor));
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void Standard::DrawMesh(GraphicsWindow *gw, TimeValue t, Matrix3& objMat,INode* pNode,ViewExp *vpt)
{
	if(!m_pTriObject)
	{
		return;
	}

	Matrix3 m;

	GetMat(t,pNode,vpt,m);
	gw->setTransform(m);

	Mesh& mesh = m_pTriObject->mesh;

	DrawLineProc lineProc(gw);
	int i;
	Point3 pt[4];
	Point3 normal;
	Matrix3 invCamMat;
	float camMat[4][4];
	int persp;
	float hither, yon;

	// get face normal array built if not already built
	m_pTriObject->mesh.checkNormals(TRUE);

	gw->getCameraMatrix(camMat, &invCamMat, &persp, &hither, &yon);

	invCamMat.NoTrans();
	invCamMat.NoScale();
	objMat.NoTrans();
	objMat.NoScale();

	invCamMat = objMat * Inverse(invCamMat);

	if(pNode->Selected())
	{
		lineProc.SetLineColor(1.0f,1.0f,1.0f);
	}

	for(i=0; i<mesh.numFaces; i++)
	{
		normal = mesh.getFaceNormal(i);
		if((normal * invCamMat)[2] < 0)
			continue;
		pt[0] = mesh.verts[mesh.faces[i].v[0]];
		pt[1] = mesh.verts[mesh.faces[i].v[1]];
		pt[2] = mesh.verts[mesh.faces[i].v[2]];
		pt[3] = mesh.verts[mesh.faces[i].v[0]];
		lineProc.proc(pt,4);
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////
static void MakeQuad(Face *f, int a, int b , int c , int d, int sg)
{
	int sm = 1<<sg;

	f[0].setVerts( a, b, c);
	f[0].setSmGroup(sm);
	f[0].setEdgeVisFlags(1,1,0);

//	f[1].setVerts( c, b, d);
//	f[1].setSmGroup(sm);
//	f[1].setEdgeVisFlags(1,1,0);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void Standard::addBox(Point3 pntCentre,Point3 pntDimension,int& iVertOffset,int& iFaceOffset)
{
	m_pTriObject->mesh.setVert(iVertOffset + 0, Point3(pntCentre.x - pntDimension.x,pntCentre.y + pntDimension.y,pntCentre.z));
	m_pTriObject->mesh.setVert(iVertOffset + 1, Point3(pntCentre.x + pntDimension.x,pntCentre.y + pntDimension.y,pntCentre.z));
	m_pTriObject->mesh.setVert(iVertOffset + 2, Point3(pntCentre.x - pntDimension.x,pntCentre.y - pntDimension.y,pntCentre.z));
	m_pTriObject->mesh.setVert(iVertOffset + 3, Point3(pntCentre.x + pntDimension.x,pntCentre.y - pntDimension.y,pntCentre.z));

	int sm = 1<<1;

	//m_pTriObject->mesh.faces[0].setVerts( iVertOffset + 2, iVertOffset + 0, iVertOffset + 1);
	m_pTriObject->mesh.faces[0].setVerts( iVertOffset + 2, iVertOffset + 3, iVertOffset + 0);
	m_pTriObject->mesh.faces[0].setSmGroup(sm);
	m_pTriObject->mesh.faces[0].setEdgeVisFlags(1,1,0);

	m_pTriObject->mesh.faces[1].setVerts( iVertOffset + 2, iVertOffset + 3, iVertOffset + 1);
	m_pTriObject->mesh.faces[1].setSmGroup(sm);
	m_pTriObject->mesh.faces[1].setEdgeVisFlags(1,1,0);

//	MakeQuad(&(m_pTriObject->mesh.faces[iFaceOffset + 0]),iVertOffset + 2,iVertOffset + 0,iVertOffset + 3,iVertOffset + 1,2);
//	MakeQuad(&(m_pTriObject->mesh.faces[iFaceOffset + 1]),iVertOffset + 2,iVertOffset + 0,iVertOffset + 3,iVertOffset + 1,2);

	iVertOffset += 4;
	iFaceOffset += 2;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void Standard::BuildMesh(TimeValue t)
{
	int nf;
	int nverts;
	int nfaces;
	float l, w;
	int iVertOffset,iFaceOffset;

	iVertOffset = iFaceOffset = 0;

	nverts = 4;
	nfaces = 2;

	m_pTriObject->mesh.setNumVerts(nverts);
	m_pTriObject->mesh.setNumFaces(nfaces);
	m_pTriObject->mesh.InvalidateTopologyCache();

	nf = 0;

	w = 10.0f;
	l = 10.0f;

	//this is the main box

	addBox(Point3(0.0f,0.0f,0.0f),Point3(w,l,0.0f),iVertOffset,iFaceOffset);

	m_pTriObject->mesh.setNumTVerts(0);
	m_pTriObject->mesh.setNumTVFaces(0);
	m_pTriObject->mesh.DeleteFlaggedFaces();
	m_pTriObject->mesh.DeleteIsoVerts();
	m_pTriObject->mesh.InvalidateGeomCache();
	m_pTriObject->mesh.BuildStripsAndEdges();

	NotifyDependents(FOREVER,PART_ALL,REFMSG_CHANGE);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
class StandardCreateCallBack : public CreateMouseCallBack
/////////////////////////////////////////////////////////////////////////////////////////////////
{
	Standard *ob;
	Point3 p0,p1;
	IPoint2 sp0, sp1;
	BOOL square;
public:
	int proc( ViewExp *vpt,int msg, int point, int flags, IPoint2 m, Matrix3& mat );
	void SetObj(Standard *obj) { ob = obj; }
};

/////////////////////////////////////////////////////////////////////////////////////////////////
int StandardCreateCallBack::proc(ViewExp *vpt,int msg, int point, int flags, IPoint2 m, Matrix3& mat)
{
	Point3 d;
	if(msg == MOUSE_FREEMOVE)
	{
		vpt->SnapPreview(m,m,NULL, SNAP_IN_3D);
	}
	else if(msg==MOUSE_POINT||msg==MOUSE_MOVE)
	{
		switch(point)
		{
		case 0:
			sp0 = m;
			p0 = vpt->SnapPoint(m,m,NULL,SNAP_IN_3D);
			p1 = p0 + Point3(.01,.01,.01);
			mat.SetTrans(float(.5)*(p0+p1));

			{
				Point3 xyz = mat.GetTrans();
				xyz.z = p0.z;
				mat.SetTrans(xyz);
			}

			if(msg==MOUSE_POINT)
			{
				return CREATE_STOP;
			}

			break;
		}
	}
	else if (msg == MOUSE_ABORT)
	{
		return CREATE_ABORT;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
static StandardCreateCallBack standardCreateCB;
/////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////
CreateMouseCallBack* Standard::GetCreateMouseCallBack()
{
	standardCreateCB.SetObj(this);
	return(&standardCreateCB);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
BOOL Standard::OKtoDisplay(TimeValue t)
{
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void Standard::InvalidateUI()
{
}

/////////////////////////////////////////////////////////////////////////////////////////////////
RefTargetHandle Standard::Clone(RemapDir& remap)
{
	Standard* newob = new Standard(m_classID,m_cClassName);

	BaseClone(this, newob, remap);

	return(newob);
}