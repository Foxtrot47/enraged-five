#if !defined(UTILITY_H_INCLUDED_)
#define UTILITY_H_INCLUDED_

#include <Max.h>

#include "PatrolNode.h"
#include "PatrolLink.h"

class CUtility
{
public:
	CUtility(void);
	~CUtility(void);

	void DrawCircle(int nPoints, float radius, IObjParam* pIp, Matrix3& mtx, float r=0.0f, float g=0.0f,float b=0.0f);
	void DrawCircle(int nPoints, float radius, ViewExp *pVpt, Matrix3& mtx, float r=0.0f, float g=0.0f,float b=0.0f);

	void Debug(PatrolNode* pNode, char* strIn);
	void Debug(PatrolLink* pNode, char* strIn);
	void Debug(INode* pNode, char* strIn);
	void Debug(Control* pControl, char* strIn, Interface *ip);
	void Debug(char* strIn);

	void WalkNodes(Interface *ip);

};

extern CUtility g_utility;

#endif // !defined(UTILITY_H_INCLUDED_)