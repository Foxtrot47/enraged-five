#include "mods.h"
#include "MeshDLib.h"
#include "uvwworld.h"
#include "imtl.h"
#include "spline3d.h"
#include "splshape.h"
#include "shape.h"
#include "edmdata.h"
#include "edmrest.h"

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::ClearMeshDataFlag(ModContextList& mcList,DWORD f) 
{
	for(int i = 0;i < mcList.Count();i++) 
	{
		EditMeshData *meshData = (EditMeshData*)mcList[i]->localData;

		if(!meshData) 
		{
			continue;
		}

		meshData->SetFlag(f,FALSE);
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::Transform(	TimeValue t, 
								Matrix3& partm, 
								Matrix3 tmAxis, 
								BOOL localOrigin, 
								Matrix3 xfrm, 
								int type) 
{
	if (!ip) 
	{
		return;
	}

	// Definitely transforming subobject geometry:
	DragMoveRestore();

	// Get modcontexts
	ModContextList mcList;
	INodeTab nodes;
	ip->GetModContexts(mcList,nodes);

	// Get axis type:
	int numAxis = ip->GetNumAxis();
	BOOL normMoveVerts = FALSE;

	// Special case for vertices: Only individual axis when moving in local space
	if ((selLevel==SL_VERTEX) && (numAxis==NUMAXIS_INDIVIDUAL)) 
	{
		if (ip->GetRefCoordSys()!=COORDS_LOCAL || 
			ip->GetCommandMode()->ID()!=CID_SUBOBJMOVE) 
		{
			numAxis = NUMAXIS_ALL;
		} 
		else 
		{
			normMoveVerts = TRUE;
		}
	}

	ClearMeshDataFlag(mcList,EMD_BEENDONE);

	for (int nd=0; nd<mcList.Count(); nd++) 
	{
		EditMeshData *meshData = (EditMeshData*)mcList[nd]->localData;

		if (!meshData) 
		{
			continue;
		}

		if (meshData->GetFlag(EMD_BEENDONE)) 
		{
			continue;
		}

		// If the mesh isn't yet cache, this will cause it to get cached.
		Mesh *mesh = meshData->GetMesh(t);
		MeshTempData *td = meshData->TempData(t);

		// partm is useless -- need this matrix which includes object-offset:
		Matrix3 objTM = nodes[nd]->GetObjectTM(t);

		// Selected vertices - either directly or indirectly through selected faces or edges.
		BitArray sel = mesh->VertexTempSel();

		if (!sel.NumberSet()) 
		{
			continue;
		}

		MeshDelta tmd(*mesh);

		// Setup some of the affect region stuff
		Tab<Point3> *vertNorms=NULL;

		if(normMoveVerts) 
		{
			vertNorms = td->VertexNormals ();
		}

		int i,nv=mesh->numVerts;

		// Compute the transforms
		if ((numAxis==NUMAXIS_INDIVIDUAL) && (selLevel != SL_VERTEX)) 
		{
			DWORD count = (selLevel == SL_EDGE) ? td->EdgeClusters()->count : td->FaceClusters()->count;
			Tab<DWORD> *vclust = td->VertexClusters(meshLevel[selLevel]);
			float *clustDist=NULL, *sss=NULL, *ssss=NULL;
			Tab<float> softSelSum, softSelSquareSum;
			Matrix3 tm, itm;

			for (DWORD j=0; j<count; j++) 
			{
				tmAxis = ip->GetTransformAxis (nodes[nd], j);
				tm  = objTM * Inverse(tmAxis);
				itm = Inverse(tm);
				tm *= xfrm;

				for (i=0; i<nv; i++) 
				{
					if(sel[i]) 
					{
						if ((*vclust)[i]!=j) 
						{
							continue;
						}

						Point3 & old = mesh->verts[i];
						tmd.Move (i, (tm*old)*itm - old);
					}
				}
			}
		}
		else 
		{
			Matrix3 tm  = objTM * Inverse(tmAxis);
			Matrix3 itm = Inverse(tm);
			tm *= xfrm;
			Matrix3 ntm;

			if(numAxis == NUMAXIS_INDIVIDUAL) 
			{
				ntm = nodes[nd]->GetObjectTM(t);
			}

			float *ws=NULL;

			for (i=0; i<nv; i++) 
			{
				if (!sel[i] && (!ws || !ws[i])) 
				{
					continue;
				}

				Point3 & old = mesh->verts[i];
				Point3 delta;

				if (numAxis == NUMAXIS_INDIVIDUAL) 
				{
					MatrixFromNormal ((*vertNorms)[i], tm);
					tm  = objTM * Inverse(tm*ntm);
					itm = Inverse(tm);
					delta = itm*(xfrm*(tm*old)) - old;
				}
				else 
				{
					delta = itm*(tm*old)-old;
				}

				if (sel[i]) 
				{
					tmd.Move (i, delta);
				}
				else 
				{
					tmd.Move (i, delta * ws[i]);
				}
			}
		}

		DragMove (tmd, meshData);
		meshData->SetFlag(EMD_BEENDONE,TRUE);		
	}

	nodes.DisposeTemporary();
	ClearMeshDataFlag(mcList,EMD_BEENDONE);

	ip->RedrawViews(ip->GetTime());
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::Move( TimeValue t, Matrix3& partm, Matrix3& tmAxis, Point3& val, BOOL localOrigin ) 
{
	Transform (t, partm, tmAxis, localOrigin, TransMatrix(val), 0);	
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::Rotate( TimeValue t, Matrix3& partm, Matrix3& tmAxis, Quat& val, BOOL localOrigin ) 
{
	Matrix3 mat;
	val.MakeMatrix(mat);
	Transform(t, partm, tmAxis, localOrigin, mat, 1);
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::Scale( TimeValue t, Matrix3& partm, Matrix3& tmAxis, Point3& val, BOOL localOrigin ) 
{
	Transform (t, partm, tmAxis, localOrigin, ScaleMatrix(val), 2);	
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::TransformStart (TimeValue t) 
{
	if(!ip)
	{
		return;
	}

	ip->LockAxisTripods(TRUE);
	DragMoveInit(t,false);
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::TransformHoldingFinish (TimeValue t) 
{
	if (!ip) 
	{
		return;
	}

	DragMoveAccept();
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::TransformFinish (TimeValue t) 
{
	if(!ip) 
	{
		return;
	}

	ip->LockAxisTripods(FALSE);
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::TransformCancel (TimeValue t) 
{
	if(!ip) 
	{
		return;
	}

	ip->LockAxisTripods(FALSE);
	DragMoveRestore();
}