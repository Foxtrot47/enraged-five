#include "clipImporter.h"

#include <maxscript/maxscript.h>
#include <maxscript/foundation/strings.h>
#include <maxscript/maxwrapper/maxclasses.h>
#include <maxscript/foundation/Numbers.h>
#include <maxscript/macros/define_instantiation_functions.h> // LPXO: define me last

#include "animationLoader.h"

def_visible_primitive(clipEditor,"clipEditor");
def_visible_primitive(clipSetProperty,"clipSetProperty");
def_visible_primitive(clipGetProperty,"clipGetProperty");
def_visible_primitive(clipLoad,"clipLoad");
def_visible_primitive(clipSave,"clipSave");
def_visible_primitive(clipGetIntId,"clipGetIntId");
def_visible_primitive(clipGetFloatId,"clipGetFloatId");
def_visible_primitive(clipGetStringId,"clipGetStringId");
def_visible_primitive(clipCleanup,"clipCleanup");
def_visible_primitive(ApplyAnimation,"ApplyAnimation");


//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* clipEditor_cf(Value** arg_list, int count)
{
	type_check(arg_list[0],String,"file name");
	char* filename = arg_list[0]->to_string();

	ClipObject* p_ClipObject = clipImporter::GetInst().GetClipObject();
	p_ClipObject->Init( filename );
	return_protected(Integer::intern((unsigned int)p_ClipObject));
}

Value* ApplyAnimation_cf(Value** arg_list, int count)
{
	check_arg_count(ApplyAnimation, 2, count);
	type_check(arg_list[0],MAXNode,"node [max node to apply animation to]");
	type_check(arg_list[1],String,"string [animation file path]");

	CAnimationLoader loader;
	loader.Apply(arg_list[0]->to_node(),arg_list[1]->to_string());

	return &ok;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* clipSetProperty_cf(Value** arg_list, int count)
{
	type_check(arg_list[0],String,"property name");
	type_check(arg_list[1],Integer,"attr type");
	type_check(arg_list[2],String,"attr name");

	ClipObject* p_ClipObject = clipImporter::GetInst().GetClipObject();

	char* propName = arg_list[0]->to_string();

	char* p_selSet = NULL;

	if( p_ClipObject->IsInited() )
	{
		p_ClipObject->BuildPropertyFromArgs( propName, arg_list, count );	
	}
	else
		return &false_value;
	return &ok;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* clipGetProperty_cf(Value** arg_list, int count)
{
	type_check(arg_list[1],String,"property type");
	type_check(arg_list[2],String,"property name");

	ClipObject* p_ClipObject = clipImporter::GetInst().GetClipObject();
	char* propType = arg_list[1]->to_string();
	char* propName = arg_list[2]->to_string();

	if( p_ClipObject->IsInited() )
	{
		if( stricmp( propType, "int" ) == 0 )
		{
			int propIntVal;
			p_ClipObject->GetProperty(propName, propIntVal); 
			return_protected( new Integer( propIntVal ) );
		}
		else if( stricmp( propType, "string" ) == 0 )
		{
			const char* p_PropStrVal;
			p_ClipObject->GetProperty(propName, p_PropStrVal); 
			return_protected( new String( p_PropStrVal ) );
		}
		else
			return &false_value;
	}
	return &false_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* clipLoad_cf(Value** arg_list, int count)
{
	type_check(arg_list[0],String,"clipLoad [clip file to load]");

	ClipObject* p_ClipObject = clipImporter::GetInst().GetClipObject();
	char* p_Filename = arg_list[0]->to_string();
	
	p_ClipObject->LoadClip( p_Filename );
	
	return &true_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* clipSave_cf(Value** arg_list, int count)
{
	type_check(arg_list[0],String,"clipSave [clip file to Save]");

	ClipObject* p_ClipObject = clipImporter::GetInst().GetClipObject();
	
	char* p_Filename = arg_list[0]->to_string();
	
	if(!clipImporter::initialised)
		clipImporter::InitClass();

	if( p_ClipObject->IsInited() )
		p_ClipObject->SaveClip(p_Filename);
	
	clipImporter::GetInst().DeleteClipObject();
	return &true_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* clipGetIntId_cf(Value** arg_list, int count)
{
	return_protected( new Integer(ClipObject::PROP_TYPE_INT) );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* clipGetFloatId_cf(Value** arg_list, int count)
{
	return_protected( new Integer(ClipObject::PROP_TYPE_FLOAT) );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* clipGetStringId_cf(Value** arg_list, int count)
{
	return_protected( new Integer(ClipObject::PROP_TYPE_STRING) );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* clipCleanup_cf(Value** arg_list, int count)
{
	clipImporter::GetInst().DeleteClipObject();

	return &true_value;
}