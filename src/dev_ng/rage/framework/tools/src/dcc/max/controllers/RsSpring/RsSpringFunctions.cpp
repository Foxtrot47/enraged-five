/**********************************************************************
 *<
	FILE: jiggleFunctions.cpp

	DESCRIPTION:	A procedural Mass/Spring Position controller 
					Additional functions for the Spring controller

	CREATED BY:		Adam Felt

	HISTORY: 

 *>	Copyright (c) 1999-2000, All Rights Reserved.
 **********************************************************************/

#include "RsSpring.h"
//#include "istdplug.h"
#include "euler.h"

template <class T>
void RsSpring<T>::SetStrength(int axis, float strength, bool update)
{
	int paramID = 4;
	switch(axis)
	{
	case SPRING_PARAM_AXIS_X:
		paramID = jig_strength_x;
		break;
	case SPRING_PARAM_AXIS_Y:
		paramID = jig_strength_y;
		break;
	case SPRING_PARAM_AXIS_Z:
		paramID = jig_strength_z;
		break;
	}
	dyn_pb->SetValue(paramID, 0, strength);
	if (update)
	{
		NotifyDependents(FOREVER, (PartID)PART_ALL, REFMSG_CHANGE);
		if(ip) ip->RedrawViews(ip->GetTime());
	}
}
template <class T>
void RsSpring<T>::SetDampening(int axis, float dampen, bool update)
{
	int paramID = 4;
	switch(axis)
	{
	case SPRING_PARAM_AXIS_X:
		paramID = jig_dampen_x;
		break;
	case SPRING_PARAM_AXIS_Y:
		paramID = jig_dampen_y;
		break;
	case SPRING_PARAM_AXIS_Z:
		paramID = jig_dampen_z;
		break;
	}
	dyn_pb->SetValue(paramID, 0, dampen);
	if (update)
	{
		NotifyDependents(FOREVER, (PartID)PART_ALL, REFMSG_CHANGE);
		if(ip) ip->RedrawViews(ip->GetTime());
	}
}

template <class T>
void RsSpring<T>::SetPreserveXForms(bool doIt)
{
	dyn_pb->SetValue(jig_preserveXForms, 0, doIt);
}


template <class T>
void RsSpring<T>::SetLimit(int axis, int minMax, float limit, bool update, bool linkForward)
{
	int paramID = 4;
	switch(axis)
	{
	case SPRING_PARAM_AXIS_X:
		paramID = jig_xlimitMin;
		break;
	case SPRING_PARAM_AXIS_Y:
		paramID = jig_ylimitMin;
		break;
	case SPRING_PARAM_AXIS_Z:
		paramID = jig_zlimitMin;
		break;
	}
	if(minMax==SPRING_PARAM_MAX)
		paramID += 3;
	dyn_pb->SetValue(paramID, 0, limit);

	if(dlg)
		dlg->Update();
	if (update)
	{
		NotifyDependents(FOREVER, (PartID)PART_ALL, REFMSG_CHANGE);
		if(ip) ip->RedrawViews(ip->GetTime());

	}
}
template <class T>
void RsSpring<T>::SetGravity(int axis, float gravity, bool update)
{
	int paramID = 0;
	switch(axis)
	{
	case SPRING_PARAM_AXIS_X:
		paramID = jig_gravityX;
		break;
	case SPRING_PARAM_AXIS_Y:
		paramID = jig_gravityY;
		break;
	case SPRING_PARAM_AXIS_Z:
		paramID = jig_gravityZ;
		break;
	}

	dyn_pb->SetValue(paramID, 0, gravity);
	if (update)
	{
		NotifyDependents(FOREVER, (PartID)PART_ALL, REFMSG_CHANGE);
		if(ip) ip->RedrawViews(ip->GetTime());
	}
}
template <class T>
void RsSpring<T>::SetGravityAxis(int axis, float gravity, bool update)
{
	int paramID = 0;
	switch(axis)
	{
	case SPRING_PARAM_AXIS_X:
		paramID = jig_gravity_axisX;
		break;
	case SPRING_PARAM_AXIS_Y:
		paramID = jig_gravity_axisY;
		break;
	case SPRING_PARAM_AXIS_Z:
		paramID = jig_gravity_axisZ;
		break;
	}

	dyn_pb->SetValue(paramID, 0, gravity);
	if (update)
	{
		NotifyDependents(FOREVER, (PartID)PART_ALL, REFMSG_CHANGE);
		if(ip) ip->RedrawViews(ip->GetTime());
	}
}
template <class T>
float RsSpring<T>::GetStrength(int axis)
{
	float Strength;
	int paramID = 0;
	switch(axis)
	{
	case SPRING_PARAM_AXIS_X:
		paramID = jig_strength_x;
		break;
	case SPRING_PARAM_AXIS_Y:
		paramID = jig_strength_y;
		break;
	case SPRING_PARAM_AXIS_Z:
		paramID = jig_strength_z;
		break;
	}
	dyn_pb->GetValue(paramID, 0, Strength, FOREVER);
	return Strength;
}
template <class T>
Point3 RsSpring<T>::GetStrengthVec()
{
	Point3 Strength;
	dyn_pb->GetValue(jig_strength_x, 0, Strength.x, FOREVER);
	dyn_pb->GetValue(jig_strength_y, 0, Strength.y, FOREVER);
	dyn_pb->GetValue(jig_strength_z, 0, Strength.z, FOREVER);
	return Strength;
}

template <class T>
float RsSpring<T>::GetGravityAxis(int axis)
{
	float posDamp;
	int paramID = 0;
	switch(axis)
	{
	case SPRING_PARAM_AXIS_X:
		paramID = jig_gravity_axisX;
		break;
	case SPRING_PARAM_AXIS_Y:
		paramID = jig_gravity_axisY;
		break;
	case SPRING_PARAM_AXIS_Z:
		paramID = jig_gravity_axisZ;
		break;
	}
	dyn_pb->GetValue(paramID, 0, posDamp, FOREVER);
	return posDamp;
}

template <class T>
float RsSpring<T>::GetDampening(int axis)
{
	float posDamp;
	int paramID = 0;
	switch(axis)
	{
	case SPRING_PARAM_AXIS_X:
		paramID = jig_dampen_x;
		break;
	case SPRING_PARAM_AXIS_Y:
		paramID = jig_dampen_y;
		break;
	case SPRING_PARAM_AXIS_Z:
		paramID = jig_dampen_z;
		break;
	}
	dyn_pb->GetValue(paramID, 0, posDamp, FOREVER);
	return posDamp;
}
template <class T>
Point3 RsSpring<T>::GetDampeningVec()
{
	Point3 posDamp;
	dyn_pb->GetValue(jig_dampen_x, 0, posDamp.x, FOREVER);
	dyn_pb->GetValue(jig_dampen_y, 0, posDamp.y, FOREVER);
	dyn_pb->GetValue(jig_dampen_z, 0, posDamp.z, FOREVER);
	return posDamp;
}

template <class T>
float RsSpring<T>::GetGravity(int axis)
{
	float gravity = 0;
	int paramID = 0;
	switch(axis)
	{
	case SPRING_PARAM_AXIS_X:
		paramID = jig_gravityX;
		break;
	case SPRING_PARAM_AXIS_Y:
		paramID = jig_gravityY;
		break;
	case SPRING_PARAM_AXIS_Z:
		paramID = jig_gravityZ;
		break;
	}

	dyn_pb->GetValue(paramID, 0, gravity, FOREVER);
	return gravity;
}
template <class T>
BOOL RsSpring<T>::GetPreserveXForms()
{
	BOOL doIt;
	dyn_pb->GetValue(jig_preserveXForms, 0, doIt, FOREVER);
	return doIt;
}
template <class T>
float RsSpring<T>::GetLimit(int axis, int minMax)
{
	float limit = 0;
	int paramID = 4;
	switch(axis)
	{
	case SPRING_PARAM_AXIS_X:
		paramID = jig_xlimitMin;
		break;
	case SPRING_PARAM_AXIS_Y:
		paramID = jig_ylimitMin;
		break;
	case SPRING_PARAM_AXIS_Z:
		paramID = jig_zlimitMin;
		break;
	}
	if(minMax==SPRING_PARAM_MAX)
		paramID += 3;
		
	dyn_pb->GetValue(paramID, 0, limit, FOREVER);
	return limit;
}

template <class T>
BOOL RsSpring<T>::IsParamsLinked()
{
	BOOL linked;
	dyn_pb->GetValue(jig_params_linked, 0, linked, FOREVER);
	return linked;
}
template <class T>
void RsSpring<T>::SetParamsLinked(BOOL linked)
{
	dyn_pb->SetValue(jig_params_linked, 0, linked);
}

// Has to be world space as we're actually calculating based on the world position change of the parent bone.
Point3 PosRsSpring::GetCurrValue(TimeValue t)
{
//	Matrix3 selfTM;
	Point3 pos = Point3(0.0f,0.0f,0.0f); // for ABSOLUTE
	theCtrl->GetValue(t, &pos, FOREVER, CTRL_ABSOLUTE);

	// transformation into WORLD space!
	if( selfNode && selfNode->GetParentNode() != NULL )
	{
 		Matrix3 parentTM = selfNode->GetParentNode()->GetNodeTM(t);
 		pos = parentTM.PointTransform(pos);
	}

	return pos;
}

Quat RotRsSpring::GetCurrValue(TimeValue t)
{
	Quat rot(0.0f,0.0f,0.0f,1.0f);
	theCtrl->GetValue(t, &rot, FOREVER, CTRL_ABSOLUTE);

	INode* parentNode = selfNode->GetParentNode();
	if (parentNode != 0 )
	{
		Quat parentRot(parentNode->GetNodeTM(t));
		rot += parentRot;
	}

	return rot;
}

void RegisterRsSpringWindow(HWND hWnd, HWND hParent, Control *cont)
{
	RsSpringWindow rec(hWnd,hParent,cont);
	jiggleWindows.Append(1,&rec);
}

void UnRegisterRsSpringWindow(HWND hWnd)
{	
	for (int i=0; i<jiggleWindows.Count(); i++) 
	{
		if (hWnd==jiggleWindows[i].hWnd) 
		{
			jiggleWindows.Delete(i,1);
			return;
		}
	}	
}

HWND FindOpenRsSpringWindow(HWND hParent,Control *cont)
{	
	for (int i=0; i<jiggleWindows.Count(); i++) 
	{
		if (hParent == jiggleWindows[i].hParent &&
			cont    == jiggleWindows[i].cont) 
		{
			return jiggleWindows[i].hWnd;
		}
	}
	return NULL;
}