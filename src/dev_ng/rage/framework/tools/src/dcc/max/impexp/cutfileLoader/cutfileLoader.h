#ifndef __CLIP_EDITOR__
#define __CLIP_EDITOR__


#include "3dsmaxsdk_preinclude.h"
#include "Max.h"

#include "resource.h"
#include "istdplug.h"
#include "iparamb2.h"
#include "iparamm2.h"
//SIMPLE TYPE

#include <guplib.h>

#include "atl/array.h"
//#include "crclip/tags.h"

#if HACK_MP3
	#include "crclip/properties.h"
	#include "crclip/propertybasic.h"
#else
	#include "crmetadata/properties.h"
	#include "crmetadata/propertyattributes.h"
#endif

#include "cutfile/cutfile2.h"
#include "LightAnimationExporter.h"

extern TCHAR *GetString(int id);

extern HINSTANCE hInstance;

void PrintToListener(char* pPrefix, char* pMsg);

//using namespace rage;

class cutfileLoader : public GUP {
	public:

		static HWND hParams;

		// GUP Methods
		DWORD		Start			( );
		void		Stop			( );
		DWORD_PTR	Control			( DWORD parameter );
		
		// Loading/Saving
		IOResult Save(ISave *isave);
		IOResult Load(ILoad *iload);
		
		// Singleton access
		static cutfileLoader& GetInst();

		//Constructor/Destructor
		cutfileLoader();
		~cutfileLoader();

		static void InitClass();
		static void ShutdownClass();

		int CreateLightExporter(const float fStartTime, const float fEndTime, const int iFrames, const char* pNodeName, const char* pDirectory, int iLightType);
		CLightAnimationExporter* GetLightExporter(int index);
		rage::cutfCutsceneFile2* GetCutFile();
		rage::cutfCutsceneFile2* GetLightCutFile();
		bool IsCutFileLoaded();
		void SetCutFileLoaded(bool bLoaded);
		bool GetInteractionMode();
		void SetInteractionMode(bool bMode);
		int GetRangeEnd();
		void SetRangeEnd(int iEndRange);
		void Reset();
		
		static bool initialised;

	private:

		atArray<CLightAnimationExporter*> m_lightExporter;
		rage::cutfCutsceneFile2 m_cutFile;
		rage::cutfCutsceneFile2 m_lightCutFile;
		static bool m_bCutFileLoaded;
		static bool m_bInteractionMode;
		static int m_iEndRange;
};



extern cutfileLoader g_cutfileLoader;



#endif //__CLIP_EDITOR__
