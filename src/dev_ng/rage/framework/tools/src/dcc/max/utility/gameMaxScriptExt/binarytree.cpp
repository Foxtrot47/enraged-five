#include "binarytree.h"
#include <assert.h>

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
CBinaryTreeNode::CBinaryTreeNode(const CRect& bb,Int32 Depth):
	m_rect(bb),
	m_Depth(Depth)
{
	for(Int32 i = 0;i<NUM_CHILDREN;i++)
	{
		mp_Children[i] = NULL;
	}
}

//////////////////////////////////////////////////////////////////////////////
CBinaryTreeNode::~CBinaryTreeNode()
{
	for(Int32 i = 0;i<NUM_CHILDREN;i++)
	{
		if(mp_Children[i])
		{
			delete mp_Children[i];
		}
	}
}

//////////////////////////////////////////////////////////////////////////////
void CBinaryTreeNode::AddItem(void* p_vData, const CVector2D& pos)
{
	int32 iSector = FindSector(pos);

	if(iSector == -1)
	{
		m_DataItems.push_back(p_vData);
	}
	else
	{
		if(!mp_Children[iSector])
		{
			CRect newBB = m_rect;
		
			if(m_Depth % 2)
			{
				if(iSector == FIRST)
				{
					newBB.bottom = (newBB.top + newBB.bottom) / 2.0f;
				}
				else
				{
					newBB.top = (newBB.top + newBB.bottom) / 2.0f;
				}
			}
			else
			{
				if(iSector == FIRST)
				{
					newBB.right = (newBB.right + newBB.left) / 2.0f;
				}
				else
				{
					newBB.left = (newBB.right + newBB.left) / 2.0f;
				}
			}
			
			mp_Children[iSector] = new CBinaryTreeNode(newBB, m_Depth - 1);			
		}

		mp_Children[iSector]->AddItem(p_vData, pos);
	}
}

//////////////////////////////////////////////////////////////////////////////
Int32 CBinaryTreeNode::FindSector(const CVector2D& Position)
{
	float centreX = (m_rect.left + m_rect.right) / 2.0f;
	float centreY = (m_rect.bottom + m_rect.top) / 2.0f;
	
	if(m_Depth == 0)
	{
		return -1;
	}
	
	if(m_Depth % 2)
	{
		if((Position.y < centreY))
			return FIRST;
		else
			return SECOND;
	}
	else
	{
		if((Position.x < centreX))
			return FIRST;
		else
			return SECOND;
	}
}

//////////////////////////////////////////////////////////////////////////////
Int32 CBinaryTreeNode::GetTotalNumItems()
{
	Int32 iTotal = 0;

	GetTotalNumItemsRec(iTotal);

	return iTotal;
}

//////////////////////////////////////////////////////////////////////////////
void CBinaryTreeNode::GetTotalNumItemsRec(Int32& r_iTotal)
{
	r_iTotal += (Int32)m_DataItems.size();

	for(int i = 0;i<NUM_CHILDREN;i++)
	{
		if(mp_Children[i])
		{
			mp_Children[i]->GetTotalNumItemsRec(r_iTotal);
		}
	}
}

//////////////////////////////////////////////////////////////////////////////
void CBinaryTreeNode::GetAll(std::list<void*>& r_DataItems)
{
	r_DataItems.insert(r_DataItems.begin(),m_DataItems.begin(),m_DataItems.end());

	for(int i = 0;i<NUM_CHILDREN;i++)
	{
		if(mp_Children[i])
		{
			mp_Children[i]->GetAll(r_DataItems);
		}
	}
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
CDensityBTreeNode::CDensityBTreeNode(const CRect& bb,Int32 Depth)
{
	m_rect = bb;
	m_Depth = Depth;

	for(int32 i=0;i<NUM_CHILDREN;i++)
	{
		mp_Children[i] = NULL;
	}
}

//////////////////////////////////////////////////////////////////////////////
CDensityBTreeNode::~CDensityBTreeNode()
{
	for(int32 i=0;i<NUM_CHILDREN;i++)
	{
		delete mp_Children[i];
	}
}

//////////////////////////////////////////////////////////////////////////////
void CDensityBTreeNode::AddItem(void* p_vData, const CVector2D& pos)
{
	DataItem dataItem;

	assert(m_rect.isInside(pos));

	dataItem.m_vecPos = pos;
	dataItem.mp_DataItem = p_vData;

	m_DataItems.push_back(dataItem);
}

//////////////////////////////////////////////////////////////////////////////
void CDensityBTreeNode::Fixup()
{
	int32 iAxis = 0;

	if(m_Depth == 0)
	{
		return;
	}

	//check we havent already run this
	for(int32 i=0;i<NUM_CHILDREN;i++)
	{
		assert(!mp_Children[i]);
	}

	//figure out which axis to cut this node along
	float fDiffX = m_rect.right - m_rect.left;
	float fDiffY = m_rect.top - m_rect.bottom;

	if(fDiffY > fDiffX)
	{
		iAxis = 1;
	}

	//find the average position along this axis
	std::list<DataItem>::iterator begin = m_DataItems.begin();
	std::list<DataItem>::iterator end = m_DataItems.end();

	float fAverage = 0.0f;
	float fValue;

	while(begin != end)
	{
		if(iAxis == 0)
		{
			fValue = begin->m_vecPos[iAxis] - m_rect.left;
		}
		else
		{
			fValue = begin->m_vecPos[iAxis] - m_rect.bottom;
		}

		assert(fValue >= 0.0f);

		fAverage += fValue;

		begin++;
	}

	if(m_DataItems.size() > 0)
	{
		fAverage /= m_DataItems.size();
	}

	//temporary
#if 0
	if(iAxis == 0)
	{
		fAverage = (m_rect.right - m_rect.left) / 2.0f;
	}
	else
	{
		fAverage = (m_rect.top - m_rect.bottom) / 2.0f;
	}
#endif

	//create the child nodes
	CRect rectA = m_rect;
	CRect rectB = m_rect;

	if(iAxis == 0)
	{
		rectA.right = rectB.left = rectA.right - fAverage;
	}
	else
	{
		rectA.top = rectB.bottom = rectA.top - fAverage;
	}

	int32 iNewDepth = m_Depth - 1;

	mp_Children[FIRST] = new CDensityBTreeNode(rectA,iNewDepth);
	mp_Children[SECOND] = new CDensityBTreeNode(rectB,iNewDepth);

	//add the value into the child nodes
	begin = m_DataItems.begin();
	int32 iAddA = 0;
	int32 iAddB = 0;

	while(begin != end)
	{
		if(iAxis == 0)
		{
			fValue = begin->m_vecPos[iAxis] - m_rect.left;
		}
		else
		{
			fValue = begin->m_vecPos[iAxis] - m_rect.bottom;
		}

		if(fValue < fAverage)
		{
			mp_Children[FIRST]->AddItem(begin->mp_DataItem,begin->m_vecPos);
			iAddA++;
		}
		else
		{
			mp_Children[SECOND]->AddItem(begin->mp_DataItem,begin->m_vecPos);
			iAddB++;
		}

		begin++;
	}

	//clear the items from this node
	m_DataItems.clear();

	//fixup the children
	for(int i=0;i<NUM_CHILDREN;i++)
	{
		if(mp_Children[i])
		{
			mp_Children[i]->Fixup();
		}
	}
}

//////////////////////////////////////////////////////////////////////////////
Int32 CDensityBTreeNode::GetTotalNumItems()
{
	Int32 iTotal = 0;

	GetTotalNumItemsRec(iTotal);

	return iTotal;
}

//////////////////////////////////////////////////////////////////////////////
void CDensityBTreeNode::GetAll(std::list<void*>& r_DataItems)
{
	std::list<DataItem>::iterator begin = m_DataItems.begin();
	std::list<DataItem>::iterator end = m_DataItems.end();

	while(begin != end)
	{
		r_DataItems.push_back(begin->mp_DataItem);

		begin++;
	}

	for(int32 i=0;i<NUM_CHILDREN;i++)
	{
		if(mp_Children[i])
		{
			mp_Children[i]->GetAll(r_DataItems);
		}
	}
}

//////////////////////////////////////////////////////////////////////////////
void CDensityBTreeNode::GetTotalNumItemsRec(Int32& r_iTotal)
{
	r_iTotal += (Int32)m_DataItems.size();

	for(int32 i=0;i<NUM_CHILDREN;i++)
	{
		if(mp_Children[i])
		{
			mp_Children[i]->GetTotalNumItemsRec(r_iTotal);
		}
	}
}