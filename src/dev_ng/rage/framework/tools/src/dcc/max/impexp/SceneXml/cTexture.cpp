//
// filename:	cMaterial.cpp
// description:
//

// --- Include Files ------------------------------------------------------------

// Standard Headers
#include <stdio.h>

// TinyXML headers
#include "tinyxml.h"

// Win32HelperLib headers
#include "Filesystem/Path.h"
#include "Win32HelperLib/stringutil.h"
#include "rexMaxUtility/IRsExportTexture.h"

// SceneXml headers
#include "cTexture.h"
#include "Resource.h"

// RstMaxMtl headers
#include "rstMaxMtl/IRageShaderMaterial.h"
#include "rstMaxMtl/Mtl.h"

// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

// --- Globals ------------------------------------------------------------------

// --- Static Globals -----------------------------------------------------------

// --- Static class members -----------------------------------------------------

// --- Code ---------------------------------------------------------------------

cTexture::cTexture( )
	: m_sFilename( "" )
	, m_sType( "" )
{
}

cTexture::cTexture( std::string filename, std::string type )
	: m_sFilename( filename )
	, m_sType( type )
{
}

cTexture&
cTexture::operator=( const cTexture& rhs )
{
	this->m_sFilename = rhs.RetFilename( );
	this->m_sType = rhs.RetType( );

	return *this;
}

cTexture::~cTexture( )
{
}

TiXmlElement*
cTexture::ToXmlElement( const std::string& export_as ) const
{
	TiXmlElement* pTexture = new TiXmlElement( export_as.c_str() );
	if ( !pTexture )
		return ( NULL );

	pTexture->SetAttribute( "filename", this->m_sFilename.c_str() );
	pTexture->SetAttribute( "type", this->m_sType.c_str() );

	return ( pTexture );
}