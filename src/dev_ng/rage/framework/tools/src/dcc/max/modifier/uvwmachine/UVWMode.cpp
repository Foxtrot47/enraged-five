//
//
//    Filename: UVWMode.cpp
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 3/06/99 12:36 $
//   $Revision: 4 $
// Description: Command mode for UVW machine mouse operations
//
//

#include "UVWMode.h"
#include "UVWmachine.h"
#include "UVWRestore.h"


// global instance of the UVWmachine command mode
UVWCMode theUVWCMode;




//
//        name: UVWMouseOperationCallback::proc
// description: callback procedure for UVW mouse operations
//          in: 
//         out: 
//
int UVWCMode::proc(HWND hwnd, int msg, int point, int flags, IPoint2 m )
{
	switch(msg)
	{
	case MOUSE_PROPCLICK:
		m_ip->SetStdCommandMode(CID_OBJMOVE);
		return FALSE;
	case MOUSE_POINT:
		if(point == 0)
		{
			theHold.Begin();
			m_pt = m;
		}
		else
		{
			switch(m_oper)
			{
			case MOUSE_ROTATE:
				theHold.Accept("Mouse UV Rotate");
				break;
			case MOUSE_SCALE:
				theHold.Accept("Mouse UV Scale");
				break;
			case MOUSE_TRANSLATE:
				theHold.Accept("Mouse UV Translate");
				break;
			case MOUSE_SCALEU:
				theHold.Accept("Mouse U Scale");
				break;
			case MOUSE_TRANSLATEU:
				theHold.Accept("Mouse U Translate");
				break;
			case MOUSE_SCALEV:
				theHold.Accept("Mouse V Scale");
				break;
			case MOUSE_TRANSLATEV:
				theHold.Accept("Mouse V Translate");
				break;
			default:
				theHold.Accept(0);
				break;
			}

		}
		break;
	case MOUSE_MOVE:
		if(flags & MOUSE_LBUTTON)
		{
			UVWEnumProc proc;
			float values[4];

			theHold.Restore();

			if(m_pMod->GetSelTypes()[m_pMod->GetSelLevel()] == IMESHSEL_FACE)
				m_pMod->m_ui.GetOrigin(values[0], values[1]);
			proc.SetValuesPtr(&values[0]);
			
			switch(m_oper)
			{
			case MOUSE_ROTATE:
				values[2] = float(0.02 * (m_pt.y - m.y));
				proc.SetOperation(UVWEnumProc::MOUSEROTATE);
				m_pMod->ProcessTextureCoords(proc, NULL);
				break;
			case MOUSE_SCALE:
				values[2] = float(pow(2.0, 0.02 * (m_pt.x - m.x)));
				values[3] = float(pow(2.0, -0.02 * (m_pt.y - m.y)));
				proc.SetOperation(UVWEnumProc::MOUSESCALE);
				m_pMod->ProcessTextureCoords(proc, NULL);
				break;
			case MOUSE_TRANSLATE:
				values[2] = float(0.01 * (m_pt.x - m.x));
				values[3] = float(-0.01 * (m_pt.y - m.y));
				proc.SetOperation(UVWEnumProc::MOUSETRANSLATE);
				m_pMod->ProcessTextureCoords(proc, NULL);
				break;
			case MOUSE_SCALEU:
				values[2] = float(pow(2.0, 0.02 * (m_pt.x - m.x)));
				proc.SetOperation(UVWEnumProc::MOUSESCALEU);
				m_pMod->ProcessTextureCoords(proc, NULL);
				break;
			case MOUSE_TRANSLATEU:
				values[2] = float(0.01 * (m_pt.x - m.x));
				proc.SetOperation(UVWEnumProc::MOUSETRANSLATEU);
				m_pMod->ProcessTextureCoords(proc, NULL);
				break;
			case MOUSE_SCALEV:
				values[3] = float(pow(2.0, -0.02 * (m_pt.y - m.y)));
				proc.SetOperation(UVWEnumProc::MOUSESCALEV);
				m_pMod->ProcessTextureCoords(proc, NULL);
				break;
			case MOUSE_TRANSLATEV:
				values[3] = float(-0.01 * (m_pt.y - m.y));
				proc.SetOperation(UVWEnumProc::MOUSETRANSLATEV);
				m_pMod->ProcessTextureCoords(proc, NULL);
				break;
			}
		}
		break;
	case MOUSE_ABORT:
		theHold.Cancel();
		m_ip->RedrawViews(m_ip->GetTime(),REDRAW_END);
		break;
	}

	return TRUE;
}


//
//        name: UVWMode::EnterMode
// description: Called when entering the UVW mouse mode
//
void UVWCMode::EnterMode()
{
	m_isMouseDown = false;
}
//
//        name: UVWMode::ExitMode
// description: Called when exiting the UVW mouse mode. Sets the state of the button used to start
//				the mode
//
void UVWCMode::ExitMode()
{
	m_pMod->ClearMouseButtons(m_oper);
}


