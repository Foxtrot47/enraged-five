class IWater;

#define WATER_INTERFACE Interface_ID(0x3674ce30, 0x272cc774)
#define GetWaterInterface(cd) \
	(IWater*)(cd)->GetInterface(WATER_INTERFACE)


class IWater : /*public Control, */public FPMixinInterface 
{
	public:

		enum {	get_mesh, get_alpha, set_alpha, get_alpha_int, set_alpha_int};
		
		// Function Map for Function Publish System 
		//***********************************
		BEGIN_FUNCTION_MAP
		FN_0(get_mesh,				TYPE_MESH_BR, GetMesh);
		FN_1(get_alpha,				TYPE_FLOAT, GetAlphaFloat, TYPE_INT);
		VFN_2(set_alpha,			SetAlphaFloat, TYPE_INT, TYPE_FLOAT);
		FN_1(get_alpha_int,			TYPE_INT, GetAlpha, TYPE_INT);
		VFN_2(set_alpha_int,		SetAlpha, TYPE_INT, TYPE_INT);
		END_FUNCTION_MAP

		FPInterfaceDesc* GetDesc();    // <-- must implement 
		//End of Function Publishing system code 
		//***********************************

		/*! \remarks Returns the mesh of the water helper
		*/
		virtual Mesh &GetMesh()=0;
		virtual float GetAlphaFloat(int index)=0;
		virtual void SetAlphaFloat(int index, float alpha)=0;
		virtual int GetAlpha(int index)=0;
		virtual void SetAlpha(int index, int alpha)=0;
};
