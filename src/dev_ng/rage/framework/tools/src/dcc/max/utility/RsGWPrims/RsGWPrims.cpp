//**************************************************************************/
// Copyright (c) 1998-2007 Autodesk, Inc.
// All rights reserved.
// 
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Autodesk, Inc., and are
// protected by Federal copyright law. They may not be disclosed to third
// parties or copied or duplicated in any form, in whole or in part, without
// the prior written consent of Autodesk, Inc.
//**************************************************************************/
// DESCRIPTION: Appwizard generated plugin
// AUTHOR: 
//***************************************************************************/

#include "RsGWPrims.h"

ClassDesc2* GetRsGWPrimsDesc() { 
	static RsGWPrimsClassDesc RsGWPrimsDesc;
	return &RsGWPrimsDesc; 
}


//--- RsGWPrims -------------------------------------------------------
RsGWPrims::RsGWPrims()
{
	iu = NULL;
	ip = GetCOREInterface();
	hPanel = NULL;
}

RsGWPrims::~RsGWPrims()
{

}

void RsGWPrims::BeginEditParams(Interface* ip,IUtil* iu) 
{
	this->iu = iu;
	this->ip = ip;
	hPanel = ip->AddRollupPage(
		hInstance,
		MAKEINTRESOURCE(IDD_PANEL),
		DlgProc,
		GetString(IDS_PARAMS),
		0);
}
	
void RsGWPrims::EndEditParams(Interface* ip,IUtil* iu) 
{
	this->iu = NULL;
	this->ip = NULL;
	ip->DeleteRollupPage(hPanel);
	hPanel = NULL;
}

void RsGWPrims::Init(HWND hWnd)
{

}

void RsGWPrims::Destroy(HWND hWnd)
{

}

INT_PTR CALLBACK RsGWPrims::DlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg) 
	{
		case WM_INITDIALOG:
			RsGWPrims::GetInstance()->Init(hWnd);
 			RsGWPrims::GetInstance()->GetColourControl() = GetIColorSwatch(GetDlgItem(hWnd, IDC_CUST_COLOUR), RsGWPrim::GetColour(), _M("Mesh colour"));
			break;

		case WM_DESTROY:
			ReleaseIColorSwatch(RsGWPrims::GetInstance()->GetColourControl());
			RsGWPrims::GetInstance()->Destroy(hWnd);
			break;

		case WM_COMMAND:
			{
				switch(msg)
				{
				case CC_COLOR_CHANGE:
				case CC_COLOR_DROP:
					{
						COLORREF color = RsGWPrims::GetInstance()->GetColourControl()->GetColor();
						RsGWPrim::SetGlobalColour(Color(GetRValue(color),GetGValue(color),GetBValue(color)));
					}
				}
			}
			//#pragma message(TODO("React to the user interface commands.  A utility plug-in is controlled by the user from here."))
			break;

		case WM_LBUTTONDOWN:
		case WM_LBUTTONUP:
		case WM_MOUSEMOVE:
			RsGWPrims::GetInstance()->ip->RollupMouseMessage(hWnd,msg,wParam,lParam); 
			break;

		default:
			return 0;
	}
	return 1;
}



//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

// void RsGWPrimsImp::SetOrigin(Matrix3 origin)
// {
// 	RsGWPrims::GetInstance()->SetOrigin(origin);
// }
// void RsGWPrimsImp::SetResolution(int resolution)
// {
// 	RsGWPrims::GetInstance()->SetResolution(resolution);
// }
// void RsGWPrimsImp::AddCircle(float radius)
// {
// 	RsGWPrims::GetInstance()->AddCircle(radius);
// }
// void RsGWPrimsImp::AddSphere(float radius)
// {
// 	RsGWPrims::GetInstance()->AddSphere(radius);
// }
// void RsGWPrimsImp::AddCone(float length, float radius, bool cap=true)
// {
// 	RsGWPrims::GetInstance()->AddCone(length, radius, cap);
// }
// void RsGWPrimsImp::AddCylinder(float length, float radius)
// {
// 	RsGWPrims::GetInstance()->AddCylinder(length, radius);
// }
// void RsGWPrimsImp::Clear()
// {
// 	RsGWPrims::GetInstance()->Clear();
// }
// void RsGWPrimsImp::Draw(BOOL flush)
// {
// 	RsGWPrims::GetInstance()->Draw(flush);
// }
