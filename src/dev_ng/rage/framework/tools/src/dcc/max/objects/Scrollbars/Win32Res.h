//
// filename:	Win32Res.h
// author:		David Muir
// date:		15 January 2007
// description:	Win32 binary resource accessors interface
//

#ifndef INC_WIN32RES_H_
#define INC_WIN32RES_H_

// --- Include Files ------------------------------------------------------------

// Platform SDK headers
#include <Windows.h>

// --- Globals ------------------------------------------------------------------
TCHAR* GetStringResource( HINSTANCE hInst, int nResID );

#endif // !INC_WIN32RES_H_

// End of file
