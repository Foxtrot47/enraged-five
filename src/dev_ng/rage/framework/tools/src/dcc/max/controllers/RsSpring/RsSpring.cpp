/**********************************************************************
 *<
	FILE: jiggle.cpp

	DESCRIPTION:	A procedural Mass/Spring Position controller 
					Main source body

	CREATED BY:		Adam Felt

	HISTORY: 

 *>	Copyright (c) 1999-2000, All Rights Reserved.
 **********************************************************************/

#include "max.h"
#include "IPathConfigMgr.h"
#include "RsSpring.h"
#include "RsSprings.h"
#include "NodeSearchEnumProc.h"
#include "decomp.h"

#include "vectormath/vec3v.h"
#include "vectormath/vec4v.h"
#include "vectormath/mat34v.h"
#include "vectormath/mat44v.h"
#include "vectormath/transformv.h"
#include "vectormath/classfreefuncsv.h"
#include "vectormath/classfreefuncsv.inl"

ParamBlockDesc2 *CreateSpringBasePBlock2(ClassDesc2 *cd, char *paramBlockName)
{
	return new ParamBlockDesc2(
		jig_dynamics_params, _T(paramBlockName), 
		P_VERSION, cd, 
		P_VERSION + P_AUTO_CONSTRUCT + P_AUTO_UI, VERSION_CURRENT, RS_SPRING_PBLOCK_REF1, 
		//rollout
		IDD_DYNAMICS_PANEL, IDS_DYN_PARAMS, BEGIN_EDIT_MOTION, 0, NULL,
		// params
		jig_control_node,    _T(""),  TYPE_INODE_TAB,		0,	0,	IDS_RS_SPRING_CONTROL_NODES,
		end,

		jig_strength_x,	_T("strengthX"),	TYPE_FLOAT,	P_ANIMATABLE,	IDS_POS_STRENGTH,
		p_default,		RS_SPRING_STRENGTH_DEFAULT,
		p_range,		RS_SPRING_PARAM_MIN,	RS_SPRING_STRENGTH_MAX,
		p_ui,			TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_RS_SPRING_STRENGTHX, IDC_RS_SPRING_STRENGTHX_SPIN, RS_SPRING_PARAM_STEP,
		end, 
		jig_dampen_x,	_T("dampenX"),	TYPE_FLOAT,	P_ANIMATABLE,	IDS_RS_SPRING_DAMPENINGX,
		p_default,		RS_SPRING_DAMPEN_DEFAULT,
		p_range,		RS_SPRING_PARAM_MIN,	RS_SPRING_DAMPEN_MAX,
		p_ui,			TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_RS_SPRING_DAMPENINGX, IDC_RS_SPRING_DAMPENINGX_SPIN, RS_SPRING_PARAM_STEP,
		end, 

		jig_params_linked,_T("linkParams"),	TYPE_BOOL,	P_ANIMATABLE,	IDS_POS_LINK,
		p_default,		FALSE,
		end, 

		jig_xfactor,	_T("xInfluence"),	TYPE_FLOAT,	P_OBSOLETE + P_RESET_DEFAULT,	IDS_RS_SPRING_OBSOLETE,
		p_default,		RS_SPRING_STRENGTH_DEFAULT,
		end, 
		jig_yfactor,	_T("yInfluence"),	TYPE_FLOAT,	P_OBSOLETE + P_RESET_DEFAULT,	IDS_RS_SPRING_OBSOLETE,
		p_default,		RS_SPRING_STRENGTH_DEFAULT,
		end, 
		jig_zfactor,	_T("zInfluence"),	TYPE_FLOAT,	P_OBSOLETE + P_RESET_DEFAULT,	IDS_RS_SPRING_OBSOLETE,
		p_default,		RS_SPRING_STRENGTH_DEFAULT,
		end, 

		jig_xlimitMin,	_T("xLimitMin"),	TYPE_FLOAT,	P_ANIMATABLE,	IDS_RS_SPRING_LIMITX,
		p_default,		RS_SPRING_MIN_LIMIT_DEFAULT,
		p_range,		RS_SPRING_MIN_LIMIT_MIN,	RS_SPRING_MAX_LIMIT_MAX,
		p_ui,			TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_RS_SPRING_LIMITX_MIN, IDC_RS_SPRING_LIMITX_SPIN_MIN, RS_SPRING_MIN_LIMIT_STEP,
		end, 
		jig_ylimitMin,	_T("yLimitMin"),	TYPE_FLOAT,	P_ANIMATABLE,	IDS_RS_SPRING_LIMITY,
		p_default,		RS_SPRING_MIN_LIMIT_DEFAULT,
		p_range,		RS_SPRING_MIN_LIMIT_MIN,	RS_SPRING_MAX_LIMIT_MAX,
		p_ui,			TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_RS_SPRING_LIMITY_MIN, IDC_RS_SPRING_LIMITY_SPIN_MIN, RS_SPRING_MIN_LIMIT_STEP,
		end, 
		jig_zlimitMin,	_T("zLimitMin"),	TYPE_FLOAT,	P_ANIMATABLE,	IDS_RS_SPRING_LIMITZ,
		p_default,		RS_SPRING_MIN_LIMIT_DEFAULT,
		p_range,		RS_SPRING_MIN_LIMIT_MIN,	RS_SPRING_MAX_LIMIT_MAX,
		p_ui,			TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_RS_SPRING_LIMITZ_MIN, IDC_RS_SPRING_LIMITZ_SPIN_MIN, RS_SPRING_MIN_LIMIT_STEP,
		end, 
		jig_xlimitMax,	_T("xLimitMax"),	TYPE_FLOAT,	P_ANIMATABLE,	IDS_RS_SPRING_LIMITX,
		p_default,		RS_SPRING_MAX_LIMIT_DEFAULT,
		p_range,		RS_SPRING_MAX_LIMIT_MIN,	RS_SPRING_MAX_LIMIT_MAX,
		p_ui,			TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_RS_SPRING_LIMITX_MAX, IDC_RS_SPRING_LIMITX_SPIN_MAX, RS_SPRING_MAX_LIMIT_STEP,
		end, 
		jig_ylimitMax,	_T("yLimitMax"),	TYPE_FLOAT,	P_ANIMATABLE,	IDS_RS_SPRING_LIMITY,
		p_default,		RS_SPRING_MAX_LIMIT_DEFAULT,
		p_range,		RS_SPRING_MAX_LIMIT_MIN,	RS_SPRING_MAX_LIMIT_MAX,
		p_ui,			TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_RS_SPRING_LIMITY_MAX, IDC_RS_SPRING_LIMITY_SPIN_MAX, RS_SPRING_MAX_LIMIT_STEP,
		end, 
		jig_zlimitMax,	_T("zLimitMax"),	TYPE_FLOAT,	P_ANIMATABLE,	IDS_RS_SPRING_LIMITZ,
		p_default,		RS_SPRING_MAX_LIMIT_DEFAULT,
		p_range,		RS_SPRING_MAX_LIMIT_MIN,	RS_SPRING_MAX_LIMIT_MAX,
		p_ui,			TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_RS_SPRING_LIMITZ_MAX, IDC_RS_SPRING_LIMITZ_SPIN_MAX, RS_SPRING_MAX_LIMIT_STEP,
		end, 

		jig_preserveXForms, _T("preserveXFormsOnFreeze"),	TYPE_BOOL,	P_ANIMATABLE,	IDS_CHCK_PRESRV_XFORMS,
		p_default,		FALSE,
		p_ui,			TYPE_CHECKBUTTON, IDC_CHCK_PRESRV_XFORMS,
		end, 
		jig_gravityX,	_T("gravityX"),	TYPE_FLOAT,	P_ANIMATABLE,	IDS_RS_SPRING_GRAVITYX,
		p_default,		RS_SPRING_GRAVITY_DEFAULT,
		p_range,		RS_SPRING_MIN_LIMIT_MIN,	RS_SPRING_MAX_LIMIT_MAX,
		p_ui,			TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_RS_SPRING_GRAVITYX, IDC_RS_SPRING_GRAVITYX_SPIN, RS_SPRING_PARAM_STEP,
		end, 
		jig_gravityY,	_T("gravityY"),	TYPE_FLOAT,	P_ANIMATABLE,	IDS_RS_SPRING_GRAVITYY,
		p_default,		RS_SPRING_GRAVITY_DEFAULT,
		p_range,		RS_SPRING_MIN_LIMIT_MIN,	RS_SPRING_MAX_LIMIT_MAX,
		p_ui,			TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_RS_SPRING_GRAVITYY, IDC_RS_SPRING_GRAVITYY_SPIN, RS_SPRING_PARAM_STEP,
		end, 
		jig_gravityZ,	_T("gravityZ"),	TYPE_FLOAT,	P_ANIMATABLE,	IDS_RS_SPRING_GRAVITYZ,
		p_default,		RS_SPRING_GRAVITY_DEFAULT,
		p_range,		RS_SPRING_MIN_LIMIT_MIN,	RS_SPRING_MAX_LIMIT_MAX,
		p_ui,			TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_RS_SPRING_GRAVITYZ, IDC_RS_SPRING_GRAVITYZ_SPIN, RS_SPRING_PARAM_STEP,
		end,

		jig_strength_y,	_T("strengthY"),	TYPE_FLOAT,	P_ANIMATABLE,	IDS_POS_STRENGTH,
		p_default,		RS_SPRING_STRENGTH_DEFAULT,
		p_range,		RS_SPRING_PARAM_MIN,	RS_SPRING_STRENGTH_MAX,
		p_ui,			TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_RS_SPRING_STRENGTHY, IDC_RS_SPRING_STRENGTHY_SPIN, RS_SPRING_PARAM_STEP,
		end, 
		jig_strength_z,	_T("strengthZ"),	TYPE_FLOAT,	P_ANIMATABLE,	IDS_POS_STRENGTH,
		p_default,		RS_SPRING_STRENGTH_DEFAULT,
		p_range,		RS_SPRING_PARAM_MIN,	RS_SPRING_STRENGTH_MAX,
		p_ui,			TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_RS_SPRING_STRENGTHZ, IDC_RS_SPRING_STRENGTHZ_SPIN, RS_SPRING_PARAM_STEP,
		end, 

		jig_dampen_y,	_T("dampenY"),	TYPE_FLOAT,	P_ANIMATABLE,	IDS_RS_SPRING_DAMPENINGY,
		p_default,		RS_SPRING_DAMPEN_DEFAULT,
		p_range,		RS_SPRING_PARAM_MIN,	RS_SPRING_DAMPEN_MAX,
		p_ui,			TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_RS_SPRING_DAMPENINGY, IDC_RS_SPRING_DAMPENINGY_SPIN, RS_SPRING_PARAM_STEP,
		end, 
		jig_dampen_z,	_T("dampenZ"),	TYPE_FLOAT,	P_ANIMATABLE,	IDS_RS_SPRING_DAMPENINGZ,
		p_default,		RS_SPRING_DAMPEN_DEFAULT,
		p_range,		RS_SPRING_PARAM_MIN,	RS_SPRING_DAMPEN_MAX,
		p_ui,			TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_RS_SPRING_DAMPENINGZ, IDC_RS_SPRING_DAMPENINGZ_SPIN, RS_SPRING_PARAM_STEP,
		end, 

		jig_gravity_axisX,	_T("gravity_axisX"),	TYPE_FLOAT,	P_ANIMATABLE,	IDS_RS_SPRING_GRAVITY_AXISX,
		p_default,		0,
		p_range,		RS_SPRING_GRAVITY_AXIS_LIMIT_MIN,	RS_SPRING_GRAVITY_AXIS_LIMIT_MAX,
		p_ui,			TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_RS_SPRING_GRAVITY_AXISX, IDC_RS_SPRING_GRAVITY_AXISX_SPIN, RS_SPRING_GRAVITY_AXIS_STEP,
		end, 
		jig_gravity_axisY,	_T("gravity_axisY"),	TYPE_FLOAT,	P_ANIMATABLE,	IDS_RS_SPRING_GRAVITY_AXISY,
		p_default,		0,
		p_range,		RS_SPRING_GRAVITY_AXIS_LIMIT_MIN,	RS_SPRING_GRAVITY_AXIS_LIMIT_MAX,
		p_ui,			TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_RS_SPRING_GRAVITY_AXISY, IDC_RS_SPRING_GRAVITY_AXISY_SPIN, RS_SPRING_GRAVITY_AXIS_STEP,
		end, 
		jig_gravity_axisZ,	_T("gravity_axisZ"),	TYPE_FLOAT,	P_ANIMATABLE,	IDS_RS_SPRING_GRAVITY_AXISZ,
		p_default,		1,
		p_range,		RS_SPRING_GRAVITY_AXIS_LIMIT_MIN,	RS_SPRING_GRAVITY_AXIS_LIMIT_MAX,
		p_ui,			TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_RS_SPRING_GRAVITY_AXISZ, IDC_RS_SPRING_GRAVITY_AXISZ_SPIN, RS_SPRING_GRAVITY_AXIS_STEP,
		end,

		end
		);
}

static ParamBlockDesc2 *jig_param_blk_pos = CreateSpringBasePBlock2(&PosRsSpringDesc, "RsSpringDynamicsParameters");

static ParamBlockDesc2 *jig_param_blk_rot = CreateSpringBasePBlock2(&RotRsSpringDesc, "RsRotSpringDynamicsParameters");

//  Mixin Interface stuff
FPInterfaceDesc* IRsSpring::GetDesc()
{
	if(RS_SPRINGPOS==GetSpringType())
		return &spring_controller_interface_pos;
	else
		return &spring_controller_interface_rot;
}

//-----------------------------------------------------------------------



// CAL-05/24/02: TODO: this should really go to core\decomp.cpp, and it should be optimized.
//		For now, it's defined locally in individual files in the ctrl project.
static void comp_affine( const AffineParts &ap, Matrix3 &mat )
{
	Matrix3 tm;

	mat.IdentityMatrix();
	mat.SetTrans( ap.t );

	if ( ap.f != 1.0f ) {				// has f component
		tm.SetScale( Point3( ap.f, ap.f, ap.f ) );
		mat = tm * mat;
	}

	if ( !ap.q.IsIdentity() ) {			// has q rotation component
		ap.q.MakeMatrix( tm );
		mat = tm * mat;
	}

	if ( ap.k.x != 1.0f || ap.k.y != 1.0f || ap.k.z != 1.0f ) {		// has k scale component
		tm.SetScale( ap.k );
		if ( !ap.u.IsIdentity() ) {			// has u rotation component
			Matrix3 utm;
			ap.u.MakeMatrix( utm );
			mat = Inverse( utm ) * tm * utm * mat;
		} else {
			mat = tm * mat;
		}
	}
}


ClassDesc* GetPosRsSpringDesc() {return &PosRsSpringDesc;}
ClassDesc* GetRotRsSpringDesc() {return &RotRsSpringDesc;}

//TODO: Should implement this method to reset the plugin params when Max is reset
void PosRsSpringClassDesc::ResetClassParams (BOOL /*fileReset*/) 
{
	
}
//TODO: Should implement this method to reset the plugin params when Max is reset
void RotRsSpringClassDesc::ResetClassParams (BOOL /*fileReset*/) 
{

}

IObjParam		*RsSpring<Point3>::ip					= NULL;
IObjParam		*RsSpring<Quat>::ip					= NULL;
IObjParam		*RsSpringDlg::ip				= NULL;
IRollupWindow	*RsSpringDlg::TVRollUp		= NULL;

INT_PTR CALLBACK RsSpringDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

template <class T>
void RsSpring<T>::BeginEditParams(
		IObjParam *ip, ULONG flags,Animatable *prev)
{
	if(GetLocked()==false)
	{
		if (flags&BEGIN_EDIT_MOTION) 
		{
			this->ip = ip;
			theCtrl->BeginEditParams(ip, flags, NULL);
			if(type==RS_SPRINGPOS)
				PosRsSpringDesc.BeginEditParams(ip, this, flags, prev);
			else
				RotRsSpringDesc.BeginEditParams(ip, this, flags, prev);

			if(type==RS_SPRINGPOS)
				jig_param_blk_pos->SetUserDlgProc(new DynMapDlgProc(this));
			else
				jig_param_blk_rot->SetUserDlgProc(new DynMapDlgProc(this));
// 			jig_param_blk.ParamOption(jig_control_node,p_validator,&node_validator);
		}
	}
}

template <class T>
void RsSpring<T>::EndEditParams( IObjParam *ip, ULONG flags,Animatable *next)
{
	if(this->ip!=NULL) //if not true then no params
	{
		theCtrl->EndEditParams(ip,flags,NULL);
		hParams1 = NULL; 
		//PosRsSpringDesc.EndEditParams(ip, this, flags, next);
		if(type==RS_SPRINGPOS)
			PosRsSpringDesc.EndEditParams(ip, this, flags | END_EDIT_REMOVEUI, next);
		else
			RotRsSpringDesc.EndEditParams(ip, this, flags | END_EDIT_REMOVEUI, next);
//		this->ip = NULL;
	}
}

template <class T>
TSTR RsSpring<T>::SvGetRelTip(IGraphObjectManager *gom, IGraphNode *gNodeTarger, int /*id*/, IGraphNode *gNodeMaker)
{
	return SvGetName(gom, gNodeMaker, false) + " -> " + gNodeTarger->GetAnim()->SvGetName(gom, gNodeTarger, false);
}

template <class T>
bool RsSpring<T>::SvHandleRelDoubleClick(IGraphObjectManager *gom, IGraphNode* /*gNodeTarget*/, int /*id*/, IGraphNode *gNodeMaker)
{
	return Control::SvHandleDoubleClick(gom, gNodeMaker);
}

template <class T>
SvGraphNodeReference RsSpring<T>::SvTraverseAnimGraph(IGraphObjectManager *gom, Animatable *owner, int id, DWORD flags)
{
	SvGraphNodeReference nodeRef = Control::SvTraverseAnimGraph( gom, owner, id, flags );

	if( nodeRef.stat == SVT_PROCEED ) {
		INode *pNode = NULL;
		if( dyn_pb ) {
			for( int i=0; i<dyn_pb->Count(jig_control_node); i++ )
			{
				Interval tempTime = FOREVER;
				dyn_pb->GetValue(jig_control_node, 0, pNode, tempTime, i);
				if( pNode )
					gom->AddRelationship( nodeRef.gNode, pNode, i, RELTYPE_CONTROLLER );
			}
		}
	}

	return nodeRef;
}

//#define PARTICLES_CHUCK	100
#define LOCK_CHUNK						0x2535  //the lock value
template <class T>
IOResult RsSpring<T>::Save(ISave *isave)
{ 
	IOResult res;
	ULONG nb;
	int on = (mLocked==true) ? 1 :0;
	isave->BeginChunk(LOCK_CHUNK);
	res = isave->Write(&on,sizeof(on),&nb);
	isave->EndChunk();
	return res;
}

template <class T>
IOResult RsSpring<T>::Load(ILoad *iload)
{ 
	IOResult res=IO_OK;
	ULONG nb;
	validStart = false;
	dlg = NULL;
	ivalid.SetEmpty();
	selfNode = NULL;
	//	parentNode = NULL;
	flags = 0;
	m_lastTime = 0;
	DeletCache();
	m_valid = FALSE;
	while (IO_OK==(res=iload->OpenChunk()))
	{
		ULONG curID = iload->CurChunkID();
		if (curID==LOCK_CHUNK)
		{
			int on;
			res=iload->Read(&on,sizeof(on),&nb);
			if(on)
				mLocked = true;
			else
				mLocked = false;
		}
		iload->CloseChunk();
	};

	return IO_OK;
}

template <class T>
RsSpring<T>::RsSpring(int ctrlType, BOOL loading)
{
	type = ctrlType;
	validStart = false;
	theCtrl = NULL;
	dyn_pb = NULL;
	dlg = NULL;
	pmap = NULL;
	ivalid.SetEmpty();
	selfNode = NULL;
//	parentNode = NULL;
	hParams1 = NULL;
	flags = 0;
	m_lastTime = 0;
	DeletCache();
	m_valid = FALSE;

	if (!loading)
	{
		Control *cont;
		if (type == RS_SPRINGPOS) 
		{
			ClassDesc *desc = GetDefaultController(CTRL_POSITION_CLASS_ID);
			//we don't want it to be another spring controller
			if (desc && desc->ClassID()==RS_SPRING_POS_CLASS_ID) 
				cont = (Control*)CreateInstance(CTRL_POSITION_CLASS_ID, Class_ID(HYBRIDINTERP_POSITION_CLASS_ID,0));			
			else 
				cont = NewDefaultPositionController();
		}
		else //if (type == RS_SPRINGROT) 
		{
			ClassDesc *desc = GetDefaultController(CTRL_ROTATION_CLASS_ID);
			//we don't want it to be another spring controller
			if (desc && desc->ClassID()==RS_SPRING_ROT_CLASS_ID) 
				cont = (Control*)CreateInstance(CTRL_ROTATION_CLASS_ID, Class_ID(HYBRIDINTERP_ROTATION_CLASS_ID,0));			
			else 
				cont = NewDefaultRotationController();
		}
// 		else
// 		{
// 			ClassDesc *desc = GetDefaultController(CTRL_POINT3_CLASS_ID);
// 			if (desc && desc->ClassID()==RS_SPRING_P3_CLASS_ID) 
// 				cont = (Control*)CreateInstance(CTRL_POINT3_CLASS_ID, Class_ID(HYBRIDINTERP_POINT3_CLASS_ID,0));			
// 			else 
// 				cont = NewDefaultPoint3Controller();
//		}
		ReplaceReference(RS_SPRING_CONTROL_REF,cont);
	}
	if (type == RS_SPRINGPOS) 
		GetPosRsSpringDesc()->MakeAutoParamBlocks(this);
	else if (type == RS_SPRINGROT) 
		GetRotRsSpringDesc()->MakeAutoParamBlocks(this);


	GetISceneEventManager()->RegisterCallback(this);

	ivalid.SetEmpty();
}

template <class T>
RsSpring<T>::~RsSpring()
{
	DeleteAllRefsFromMe();
	dyn_pb = NULL;
	hParams1 = NULL;
	dlg = NULL;
	validStart = false;
	theCtrl = NULL;
	dyn_pb = NULL;
	pmap = NULL;
	ivalid.SetEmpty();
	selfNode = NULL;
	flags = 0;
	m_lastTime = 0;
	DeletCache();
	m_valid = FALSE;
}

/*
void RsSpring::EditTimeRange(Interval range,DWORD flags)
{
	if(!(flags&EDITRANGE_LINKTOKEYS)){
		HoldRange();
		this->range = range;
		NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
	}
}

void RsSpring::MapKeys(TimeMap *map,DWORD flags)
	{
	if (flags&TRACK_MAPRANGE) {
		HoldRange();
		TimeValue t0 = map->map(range.Start());
		TimeValue t1 = map->map(range.End());
		range.Set(t0,t1);
		NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
		}
	}
*/


template <class T>
RefTargetHandle RsSpring<T>::GetReference(int i)
	{
	switch (i) {
		case RS_SPRING_CONTROL_REF: return (RefTargetHandle)theCtrl;
		case RS_SPRING_PBLOCK_REF1: return (RefTargetHandle)dyn_pb;
//		case 2: /*somehow still referenced...*/ break;
		default: assert(0);
		}
	return NULL;
	}

template <class T>
void RsSpring<T>::SetReference(int i, RefTargetHandle rtarg)
	{
	switch (i) {
		case RS_SPRING_CONTROL_REF: theCtrl = (Control*)rtarg; break;
		case RS_SPRING_PBLOCK_REF1: dyn_pb = (IParamBlock2*)rtarg; break;
		default: assert(0);
		}
	if (i == RS_SPRING_PBLOCK_REF1 && rtarg == NULL && dlg )
		PostMessage(dlg->hWnd,WM_CLOSE,0,0);
	}

template <class T>
RefResult RsSpring<T>::NotifyRefChanged(
		Interval /*iv*/, 
		RefTargetHandle hTarg, 
		PartID& /*partID*/, 
		RefMessage msg) 
{
	INode* node = NULL;

	switch (msg) {
		//this gets sent when an object is moved, play is pressed, or the time slider is clicked on.
		//so far it seems to work good for triggering when an object might change.  
		//Not called as often as a RedrawViews notification
		case REFMSG_WANT_SHOWPARAMLEVEL:
			if (SuperClassID() == CTRL_POINT3_CLASS_ID) 
			{
				validStart = FALSE;
				ivalid.SetEmpty();
			}
			break;
		//case REFMSG_FLAGDEPENDENTS:
		case REFMSG_CHANGE:
		{
			if (!dyn_pb) break;
			ivalid.SetEmpty();
			validStart = FALSE;
		}
		break;
		case REFMSG_TAB_ELEMENT_NULLED:
		case REFMSG_REF_DELETED:
			break;
		case REFMSG_GET_CONTROL_DIM: 
			break;
		}
	return REF_SUCCEED;
}

template <class T>
BOOL RsSpring<T>::AssignController(Animatable *control,int subAnim)
{
	if(GetLocked()==false)
	{
		if (control->ClassID()==ClassID() || subAnim != RS_SPRING_CONTROL_REF) return FALSE;
		//make sure the jiggle control isn't locked.
		if(theCtrl&&GetLockedTrackInterface(theCtrl)&&GetLockedTrackInterface(theCtrl)->GetLocked()==true)
			return FALSE;
		ReplaceReference(RS_SPRING_CONTROL_REF,(ReferenceTarget*)control);
		NotifyDependents(FOREVER,0,REFMSG_CHANGE);
		NotifyDependents(FOREVER,0,REFMSG_SUBANIM_STRUCTURE_CHANGED);
		return TRUE;
	}
	return FALSE;
}

template <class T>
RsSpring<T>& RsSpring<T>::operator=(const RsSpring<T>& from)
{
	type		= from.type;
	ctrlValid	= from.ctrlValid;
	range		= from.range;
	flags		= 0;

	validStart = FALSE;

	return *this;
}

template <class T>
void RsSpring<T>::Copy(Control *from)
{
	if(GetLocked()==false)
	{
		//you shoulg get a fresh new Spring controller when replacing an existing one
		//no longer copying the existing one  (there was a potential for bugs here anyway because I don't think this was ever tested)
		
		if (from->ClassID()==ClassID()) {
			mLocked = ((RsSpring*)from)->mLocked;
			ReplaceReference(RS_SPRING_CONTROL_REF,CloneRefHierarchy(((RsSpring*)from)->theCtrl));
			if (((RsSpring*)from)->dlg && ((RsSpring*)from)->dlg->hWnd)
				PostMessage(((RsSpring*)from)->dlg->hWnd,WM_CLOSE,0,0);
		} 
		else 
		{ 
			if(from&&GetLockedTrackInterface(from))
			  mLocked = GetLockedTrackInterface(from)->GetLocked();
			if (from->ClassID() == Class_ID(DUMMY_CONTROL_CLASS_ID,0))
				ReplaceReference(RS_SPRING_CONTROL_REF, (ReferenceTarget*)(GetDefaultController(this->SuperClassID())->Create(false)));
			else 
				ReplaceReference(RS_SPRING_CONTROL_REF,(ReferenceTarget*)CloneRefHierarchy(from));
		}
		NotifyDependents(FOREVER,0,REFMSG_CHANGE);
		NotifyDependents(FOREVER,0,REFMSG_SUBANIM_STRUCTURE_CHANGED);
	}
}

RefTargetHandle PosRsSpring::Clone(RemapDir& remap)
{
	RsSpring *cont = new PosRsSpring(true);
	*cont = *this;
	((PosRsSpring*)cont)->mLocked= mLocked;
	cont->ReplaceReference(RS_SPRING_CONTROL_REF, remap.CloneRef(theCtrl));
	cont->ReplaceReference(RS_SPRING_PBLOCK_REF1,remap.CloneRef(dyn_pb));

	CloneControl(cont,remap);
	BaseClone(this, cont, remap);
	return cont;
}	

RefTargetHandle RotRsSpring::Clone(RemapDir& remap)
{
	RsSpring *cont = new RotRsSpring(true);
	*cont = *this;
	((RotRsSpring*)cont)->mLocked= mLocked;
	cont->ReplaceReference(RS_SPRING_CONTROL_REF, remap.CloneRef(theCtrl));
	cont->ReplaceReference(RS_SPRING_PBLOCK_REF1,remap.CloneRef(dyn_pb));

	CloneControl(cont,remap);
	BaseClone(this, cont, remap);
	return cont;
}	

template <class T>
Animatable* RsSpring<T>::SubAnim(int i) 
{
	switch (i) { 
	case RS_SPRING_CONTROL_REF: return theCtrl; break;
	case RS_SPRING_PBLOCK_REF1: return dyn_pb; break;
	default: return NULL;}
}

template <class T>
TSTR RsSpring<T>::SubAnimName(int i) 
{ 
	switch (i) {
	case RS_SPRING_CONTROL_REF: return GetString(IDS_POS_CONTROL);
	case RS_SPRING_PBLOCK_REF1: return GetString(IDS_DYN_PARAMS); 
	default: return _T("");
	}
}
		
template <class T>
int RsSpring<T>::SubNumToRefNum(int subNum) {return subNum;}

//don't check lock for this
template <class T>
BOOL RsSpring<T>::ChangeParents(TimeValue t,const Matrix3& oldP,const Matrix3& newP,const Matrix3& tm)
{
	flags &= ~HAS_NO_PARENT;
	if ( dyn_pb->Count(jig_control_node) )
		dyn_pb->SetValue(jig_control_node, 0, ((INode*)NULL), 0);
	validStart = false;
	theCtrl->ChangeParents(t, oldP, newP, tm);
	return TRUE;
}

template <typename T>
Point3 RsSpring<T>::SolveDisplacement(float dt, Point3 prevDelta, Point3 springOffset)
{
	return prevDelta + dt * (dt * GetStrengthVec() * springOffset - prevDelta * GetDampeningVec());
}

void PosRsSpring::Integrate(TimeValue t, float dt)
{
	// dt: delta time
	// xi: position at frame i
	// v: velocity
	// F: forces
	// a: acceleration
	// 
	// v = (xi - xi-1) / dt
	Point3 prevDelta = GetVectorDifference(currPos, lastPos);
	Point3 springOffset = GetVectorDifference(GetCurrValue(t), currPos);
	// calculate force and integrate
	// F = a = strength * spring offset � damping * v
	// return prevDelta + dt * (dt * descr.m_Strength * springOffset - descr.m_Damping * prevDelta);
	Point3 acceleration = SolveDisplacement(dt, prevDelta, springOffset);

	//xi+1 = xi + v * dt + a * dt * dt
	currPos = currPos + acceleration;

//	currVel = GetCurrVelocity(t, DeltaT)/DeltaT;
}

/*
Quat operator ^(Quat outQuat, float factor)
{
	if(abs(outQuat.w)>=1.0f)
	{
		if(outQuat.Vector().Length()==0)
			return outQuat;
		outQuat /= outQuat.w;
		outQuat.Normalize();
	}

	// alpha = theta/2
	float alpha = acos(outQuat.w);

	float newAlpha = alpha * factor;

	outQuat.w = cos(newAlpha);
 
	//sqrt(outQuat.x*outQuat.x + outQuat.y*outQuat.y + outQuat.z*outQuat.z); // Bollocks? sin(newAlpha) / sin(alpha);
	float multi = newAlpha==0 ? 0.0 : (sin(newAlpha) / sin(alpha));
	outQuat.x *= multi;
	outQuat.y *= multi;
	outQuat.z *= multi;

	return outQuat;
}

void GetAngleFromQuat(Quat inQuat, float &angleOut, Point3 &axisOut)
{
	inQuat.Normalize();
	axisOut = inQuat.Vector();
	axisOut = axisOut.Normalize();
	angleOut = acos(inQuat.w)*2;
}
*/

using namespace rage;

Point3 GetAxisFromQuat(Quat q)
{
	Vec3V axis;
	ScalarV angle;
	QuatV rageQuat(q.x,q.y,q.z,q.w);
	QuatVToAxisAngle(axis, angle, rageQuat);

	Vec3V oppositeAxis = Negate(axis);
	ScalarV oppositeAngle = Add(Negate(angle), ScalarV(V_TWO_PI));
	BoolV isOpposite = IsGreaterThan(angle, ScalarV(V_PI));
	axis = SelectFT(isOpposite, axis, oppositeAxis);
	angle = SelectFT(isOpposite, angle, oppositeAngle);
	Vec3V rageVec = Scale(angle, axis);
	return Point3(rageVec.GetXf(),rageVec.GetYf(),rageVec.GetZf());
}

Quat GetQuatFromAxis(Point3 max_axis)
{
	Vec3V axis(max_axis.x,max_axis.y, max_axis.z);
	ScalarV angleDelta = Mag(axis);
	QuatV axisAngle = QuatVFromAxisAngle(InvScale(axis, angleDelta), angleDelta);
	QuatV rageQuat = SelectFT(IsGreaterThan(angleDelta, ScalarV(V_FLT_EPSILON)), QuatV(V_IDENTITY), axisAngle);
	return Quat(rageQuat.GetXf(),rageQuat.GetYf(),rageQuat.GetZf(),rageQuat.GetWf());
}

/*
void RotRsSpring::Integrate(TimeValue t, float dt)
{
	// dt: delta time
	// xi: position at frame i
	// v: velocity
	// F: forces
	// a: acceleration
	// 
	// v = (xi - xi-1) / dt
	float angleDebug;
	Point3 axisDebug;
	Quat prevDelta = GetVectorDifference(currPos, lastPos);
	GetAngleFromQuat(prevDelta, angleDebug, axisDebug);
// 	prevDelta.Normalize();
	Quat springOffset = GetVectorDifference(GetCurrValue(t), currPos);
	GetAngleFromQuat(springOffset, angleDebug, axisDebug);
//	springOffset.Normalize();
	// calculate force and integrate
	// F = a = strength * spring offset � damping * v
	// return prevDelta + dt * (dt * descr.m_Strength * springOffset - descr.m_Damping * prevDelta);
	Quat springPart = springOffset ^ (dt * GetStrength());
	GetAngleFromQuat(springPart, angleDebug, axisDebug);
	Quat dampeningPart = prevDelta ^ GetDampening();
	GetAngleFromQuat(dampeningPart, angleDebug, axisDebug);
	Quat timedSpringDampen = (springPart / dampeningPart) ^ dt;
	GetAngleFromQuat(timedSpringDampen, angleDebug, axisDebug);
	Quat acceleration = prevDelta * timedSpringDampen;
	GetAngleFromQuat(acceleration, angleDebug, axisDebug);
//	acceleration.Normalize();

	//xi+1 = xi + v * dt + a * dt * dt
	currPos = currPos * acceleration;

	//	currVel = GetCurrVelocity(t, DeltaT)/DeltaT;
}
*/

void RotRsSpring::Integrate(TimeValue t, float dt)
{
	// dt: delta time
	// xi: position at frame i
	// v: velocity
	// F: forces
	// a: acceleration
	// 
	// v = (xi - xi-1) / dt
	Quat prevDelta = GetVectorDifference(currPos, lastPos);
	Quat springOffset = GetVectorDifference(GetCurrValue(t), currPos);
	// calculate force and integrate
	// F = a = strength * spring offset � damping * v
	// return prevDelta + dt * (dt * descr.m_Strength * springOffset - descr.m_Damping * prevDelta);
	Point3 acceleration = SolveDisplacement(dt, GetAxisFromQuat(prevDelta), GetAxisFromQuat(springOffset));

	//xi+1 = xi + v * dt + a * dt * dt
	currPos = currPos + GetQuatFromAxis(acceleration);
}

Quat RotRsSpring::GetVectorDifference(Quat to, Quat from)
{
	Quat diff = to - from;
	// axis swapped direction and angle is bugger than 180 deg
	if(acos(diff.w)>ninetyDegInRad)// && vecDifference<0)
	{
		float oldAngle = acos(diff.w)*2;
		float newAngle = 2.0f * PI - oldAngle;
		Point3 newAxis = -diff.Vector();
		newAngle = cos(newAngle/2);
		newAxis = newAxis.Normalize();
		newAxis *= 1.0f-(newAngle*newAngle);
//		float debugLength = newAxis.Length();
		diff.Set(newAxis, newAngle);
	}
	return diff;
}


template <typename T>
void RsSpring<T>::Solve(int t, float DeltaT)
{
	int referenceTime = 0;
	if (t >= referenceTime)
	{
		int tpf = GetTicksPerFrame();
		int fract =   (t-int(referenceTime)) % tpf;
		int frames =  (t-int(referenceTime)) / tpf;
		int i = pos_cache.Count();
		bool rollingStart = false;

		//lastPos = Point3::Origin;
		if(i>0)
		{
			int cacheindex = i-1;
			if(frames<pos_cache.Count())
				cacheindex = frames;
			lastPos = currPos = pos_cache[cacheindex];
			if(cacheindex>0)
				lastPos = pos_cache[cacheindex-1];
		}
		else
			lastPos = currPos = GetCurrValue(referenceTime);


		for(; i<=frames;i++)
		{
			T prevChild(currPos);
			Integrate(i*tpf, DeltaT);
			lastPos = prevChild;
			if(pos_cache.Count()<=i)
				pos_cache.Resize(i+1);
			pos_cache.Insert(i,1, &currPos);
		}
	}
}


template <typename T>
void RsSpring<T>::ComputeValue(TimeValue t)
{
	Interval for_ever = FOREVER;

	//Set the start time.  This doesn't need to be done here.
	int start = 0; 
	int tpf = 60;//GetTicksPerFrame();
	int ReferenceFrame = (float)start * tpf;
	float DeltaT = 1.0f/tpf;// /(pow(2.0f, steps));
	
	Solve(t, DeltaT);
}

template <typename T>
void RsSpring<T>::ApplyLimits(TimeValue t, T &curval)
{
	float xlimitMin, ylimitMin, zlimitMin, xlimitMax, ylimitMax, zlimitMax;
	dyn_pb->GetValue(jig_xlimitMin,t,xlimitMin,ivalid);
	dyn_pb->GetValue(jig_ylimitMin,t,ylimitMin,ivalid);
	dyn_pb->GetValue(jig_zlimitMin,t,zlimitMin,ivalid);
	dyn_pb->GetValue(jig_xlimitMax,t,xlimitMax,ivalid);
	dyn_pb->GetValue(jig_ylimitMax,t,ylimitMax,ivalid);
	dyn_pb->GetValue(jig_zlimitMax,t,zlimitMax,ivalid);

	// dampen the X, Y, and Z effects
	if (xlimitMax < curval.x || ylimitMax < curval.y || zlimitMax < curval.z)
	{
		curval.x = min(curval.x, xlimitMax);
		curval.y = min(curval.y, ylimitMax);
		curval.z = min(curval.z, zlimitMax);
	}
	if (xlimitMin > curval.x || ylimitMin > curval.y || zlimitMin > curval.z)
	{
		curval.x = max(curval.x, xlimitMin);
		curval.y = max(curval.y, ylimitMin);
		curval.z = max(curval.z, zlimitMin);
	}
}

BOOL TestForSubLoop(Animatable* anim, Control* ctrl)
{
	if (anim == ctrl)
		return TRUE;
	else {
		for (int i = 0;i< anim->NumSubs();i++) {
			if (anim->SubAnim(i) == ctrl)
				return TRUE;
			else if (TestForSubLoop(anim->SubAnim(i), ctrl))
				return TRUE;
		}
	}
	return FALSE;
}

template <typename T>
BOOL RsSpring<T>::SetSelfReference()
{	
	//Find out who you are assigned to
	if ( !(flags & HAS_NO_PARENT) && 
		(!dyn_pb->Count(jig_control_node) || 
		  dyn_pb->GetINode(jig_control_node, 0, 0) == NULL || 
		selfNode == NULL) )
	{
		dyn_pb->EnableNotifications(FALSE);

		NodeSearchEnumProc dep(this);
		DoEnumDependents(&dep);

		selfNode = dep.GetNode();

		MSTR name = selfNode->GetName();

		if (selfNode == NULL) 
		{
			selfNode = GetCOREInterface()->GetRootNode();
		}

// 		if (!selfNode->IsRootNode())// && this->SuperClassID() == CTRL_POSITION_CLASS_ID )
// 		{
// 			parentNode = selfNode->GetParentNode();
// 		}
// 		else
// 			parentNode = NULL;

		name = selfNode->GetName();
		
		//check for an IK controller and iterate through parents until you don't have one
// 		INode* tempNode = selfNode;
// 		while (!tempNode->IsRootNode() && tempNode->GetTMController()->GetInterface(I_IKCONTROL))
// 		{
// 			tempNode = tempNode->GetParentNode();
// 		}
// 
// 		//if this is a Point3 controller you cannot make a reference to it so...
// 		if (this->SuperClassID() == CTRL_POINT3_CLASS_ID )
// 		{
// 			tempNode = GetCOREInterface()->GetRootNode();
// 			while (selfNode && !selfNode->IsRootNode() && TestForSubLoop(selfNode->GetTMController(), this))
// 				selfNode = selfNode->GetParentNode();
// 		}

//		int numNodesToInsert = parentNode ? 2 : 1;// selfnode and parent node lying in memory behind each other!!
		dyn_pb->Resize(jig_control_node, 1);
		dyn_pb->Append(jig_control_node, 1, &selfNode); // selfnode and parent node lying in memory behind each other!!

		if (selfNode->IsRootNode()) 
		{
			dyn_pb->SetValue(jig_control_node, 0, ((INode*)NULL), 0); 
			selfNode = NULL;
			flags |= HAS_NO_PARENT;
		}

		dyn_pb->EnableNotifications(TRUE);

	}
	return TRUE;
}

static bool applyParentSpace = true;
static bool invertParentSpace = true;
static bool applyLimits = true;

void PosRsSpring::GetValue(TimeValue t, void* val, Interval& valid, GetSetMethod method)
{
/************************************************************************
	Position
		If method == CTRL_ABSOLUTE, *val points to a Point3. The controller should simply store the value.
		If method == CTRL_RELATIVE, *val points to a Matrix3. The controller should apply its value to the matrix by pre-multiplying its position. Matrix3 *mat = (Matrix3*)val;

		Point3 v = the computed value of the controller...
		mat->PreTranslate(v);
************************************************************************/

	int preserveXFormsOnFreeze;
	dyn_pb->GetValue(jig_preserveXForms, t, preserveXFormsOnFreeze, FOREVER);
	if(preserveXFormsOnFreeze)
		t = GetCOREInterface()->GetTime();

	ivalid.SetInfinite();
	SetSelfReference();

	// Has to be world space as we're actually calculating based on the world position change of the parent bone.
	ComputeValue(t);
	Point3 curval = currPos;
	Point3 baseValue = GetCurrValue(t);

	// tansform to local space
	if(applyParentSpace && selfNode && selfNode->GetParentNode())
	{
		Matrix3 localMat = selfNode->GetParentNode()->GetNodeTM(t);
		if(invertParentSpace)
			localMat.Invert();
		curval = localMat.PointTransform(curval);
		baseValue = localMat.PointTransform(baseValue);
	}

	if(applyLimits)
	{
		curval -= baseValue;
		ApplyLimits(t, curval);
		curval += baseValue;
	}

	ivalid.SetInstant(t);
	valid &= ivalid;
	if (method==CTRL_RELATIVE) 
	{
		// write into parent matrix
		Matrix3 *mat = (Matrix3*)val;
		mat->PreTranslate(curval);
	}
	else 
	{
		*((Point3*)val) = curval;
	}
}

void RotRsSpring::GetValue(TimeValue t, void* val, Interval& valid, GetSetMethod method)
{

/***********************************************************************
Rotation
	If method == CTRL_ABSOLUTE, *val points to a Quat. The controller should simply store the value.
	If method == CTRL_RELATIVE, *val points to a Matrix3. The controller should apply its value to the matrix by pre-multiplying its rotation.

	Matrix3 *mat = (Matrix3*)val;
	Quat q = the computed value of the controller...
************************************************************************/	

	int preserveXFormsOnFreeze;
	dyn_pb->GetValue(jig_preserveXForms, t, preserveXFormsOnFreeze, FOREVER);
	if(preserveXFormsOnFreeze)
		t = GetCOREInterface()->GetTime();

	ivalid.SetInfinite();
	SetSelfReference();

	// Has to be world space as we're actually calculating based on the world position change of the parent bone.
	ComputeValue(t);
	Quat curval = currPos;

	// tansform to local space
	if(selfNode && selfNode->GetParentNode())
	{
		Quat parentRot(selfNode->GetParentNode()->GetNodeTM(t));
		curval -= parentRot;
	}

	ivalid.SetInstant(t);
	valid &= ivalid;
	if (method==CTRL_RELATIVE) 
	{
		Matrix3 *mat = (Matrix3*)val;

		PreRotateMatrix(*mat, curval);
// 		AffineParts ap;
// 		decomp_affine( *mat, &ap );
// 		ap.q = curval;				// target world rotation
// 		comp_affine( ap, *mat );
	}
	else 
	{
		*((Quat*)val) = curval;
	}
}

template <typename T>
void RsSpring<T>::SetValue(TimeValue t, void *val, int commit, GetSetMethod method)
{
	if(GetLocked()==false)
	{
		validStart = false;
		theCtrl->SetValue(t, val, commit, method); 
	}
// 	Point3 newVal;
// 	theCtrl->GetValue(t, newVal, FOREVER, CTRL_ABSOLUTE);
}
