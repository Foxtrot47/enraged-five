//
//
//    Filename: VehicleNode.cpp
//     Creator: SDKAPWZ
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: 
//
//
#include "VehicleNode.h"
#include "NodeCreateMgr.h"
#include "VehLink.h"
#include "PickVehConnection.h"
#include "IMaxDataStore.h"

#include "DataInstance.h"
//#include "Attribute.h"

#define LOAD_SPECIAL_STRINGS_MANUALY
#define USE_COMBO_FOR_SPECIAL
#if defined (USE_COMBO_FOR_SPECIAL)
#if !defined(LOAD_SPECIAL_STRINGS_MANUALY)
#error Need to define LOAD_SPECIAL_STRINGS_MANUALY
#endif //!defined(LOAD_SPECIAL_STRINGS_MANUALY)
#endif	// defined (USE_COMBO_FOR_SPECIAL)

#define NUM_CIRCLE_SEGMENTS	16
#define VEH_NODE_CIRCLE_RADIUS	1
#define VEH_NODE_SCREEN_RATIO		1.0f/80.0f

// load/save chunk ids
#define VEH_CONNECTION_NUMREFS_CHUNK	0x762

static VehicleNodeClassDesc2 VehicleNodeDesc2;
static PickVehConnection theConnectVehPickMode;
static PickVehConnection theMergeVehPickMode;

IObjParam *VehicleNode::m_ip = NULL;
HWND VehicleNode::m_hObjWnd = NULL;
HWND VehicleNode::m_hLinkWnd = NULL;
ICustButton *VehicleNode::m_pConnectButton = NULL;
ICustButton *VehicleNode::m_pMergeButton = NULL;
SelectModBoxCMode* VehicleNode::m_selectMode = NULL;
bool VehicleNode::m_creating = false;
VehicleLink* VehicleNode::m_pLinkEdited = NULL;

#if !defined( _WIN64 )
BOOL CALLBACK EditVehicleNodeProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
#else
INT_PTR CALLBACK EditVehicleNodeProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
#endif

ClassDesc* GetVehicleNodeDesc() {return &VehicleNodeDesc2;}

ObjectValidator theObjectValidator;

//
// Max3 paramblock2 version
//

ParamBlockDesc2 vehicleNodeDesc(vehicle_node_params, _T("VehicleNode parameters"), IDS_CLASS_NAME, &VehicleNodeDesc2, P_AUTO_CONSTRUCT | P_AUTO_UI, 0,
						IDD_VEH_NODE_PARAMS_PANEL, IDS_PARAMS, 0, 0, NULL,
/*						vehicle_node_disable, _T("Disabled"), TYPE_BOOL, 0, IDS_DISABLED_NODE,
							p_default,		FALSE,
							p_ui,			TYPE_SINGLECHEKBOX, IDC_DISABLED_CHECK,
							end,
						vehicle_node_roadblock, _T("Roadblock"), TYPE_BOOL, 0, IDS_ROADBLOCK_NODE,
							p_default,		FALSE,
							p_ui,			TYPE_SINGLECHEKBOX, IDC_ROADBLOCK_CHECK,
							end,
						vehicle_node_water, _T("Water"), TYPE_BOOL, 0, IDS_WATER_NODE,
							p_default,		FALSE,
							p_ui,			TYPE_SINGLECHEKBOX, IDC_WATER_CHECK,
							end,
						vehicle_node_under_bridge, _T("UnderBridge"), TYPE_BOOL, 0, IDS_UNDER_BRIDGE_NODE,
							p_default,		FALSE,
							p_ui,			TYPE_SINGLECHEKBOX, IDC_BRIDGE_CHECK,
							end,
						vehicle_node_car_speed, _T("CarSpeed"), TYPE_INT, 0, IDS_CAR_SPEED_NODE,
							p_default,		0,
							p_ui,			TYPE_RADIO, 3, IDC_SLOW_SPEED_RADIO, IDC_MEDIUM_SPEED_RADIO, IDC_FAST_SPEED_RADIO,
							end,
						vehicle_node_special, _T("SpecialList"), TYPE_INT, 0, IDC_CBO_SPECIAL, 
							p_default,	0, 
							p_range, 0, 5, 
							p_ui,			TYPE_INTLISTBOX, IDC_NODE_SPECIAL_LIST,	0, 
							end,
*/						end
						);

// --- VehConnectionClassDesc ------------------------------------------------------------------------------------

void* VehicleNodeClassDesc2::Create(BOOL loading) 
{
	return new VehicleNode;
}
//TODO: Should implement this method to reset the plugin params when Max is reset
void VehicleNodeClassDesc2::ResetClassParams (BOOL fileReset) 
{

}

int VehicleNodeClassDesc2::BeginCreate (Interface *ip) 
{
	IObjCreate *pIob = ip->GetIObjCreate();
	
	theVehicleNodeCreateMode.Begin( pIob, this );
	pIob->PushCommandMode( &theVehicleNodeCreateMode);
	
	return TRUE;
}

int VehicleNodeClassDesc2::EndCreate (Interface *ip) 
{
	theVehicleNodeCreateMode.End();
	ip->RemoveMode( &theVehicleNodeCreateMode );
	return TRUE;
}


//
//        name: grow_vector
// description: Template function that grows a vector and places a default value 
//          in: vec = reference to vector
//				size = size want vector to be
//				value = value to place in new entries
//         out: 
//
template<class T> void grow_vector(std::vector<T>& vec, s32 size, T value)
{
	while(vec.size() <= size)
		vec.push_back(value);
}


//--- VehicleNode -------------------------------------------------------

VehicleNode::VehicleNode() 
	: m_level(0)
	, m_pBlock2(NULL)
	, m_sAttrWnd(NULL)
{
	VehicleNodeDesc2.MakeAutoParamBlocks(this);
}

VehicleNode::~VehicleNode()
{
	s32 i=0;
}


void VehicleNode::BeginEditParams(IObjParam *ip,ULONG flags,Animatable *prev)
{
	m_ip = ip;

	m_level = 0;	
	if (flags&BEGIN_EDIT_CREATE) 
	{
		m_creating = true;
	} 
	else 
	{
		InitObjUI();
		m_creating = false;
	}

	if(!m_sAttrWnd)
	{
		INodePtr pNode = this->GetWorldSpaceObjectNode();
		if(pNode)
		{
			if(!theIMaxDataStore.GetData(pNode))
				theIMaxDataStore.AddData(pNode);
			m_sAttrWnd = theIMaxDataStore.AddRollupPage(pNode, ip);
		}
	}
}

void VehicleNode::EndEditParams( IObjParam *ip, ULONG flags,Animatable *next )
{
	if(m_sAttrWnd)
		theIMaxDataStore.DeleteRollupPage(m_sAttrWnd, ip);

	m_sAttrWnd = NULL;

	if(!m_creating)
	{
		CloseObjUI();
	}

	m_ip = NULL;
	m_creating = false;
}

RefTargetHandle VehicleNode::Clone(RemapDir& remap) 
{
	VehicleNode* pNewObj = new VehicleNode();
	BaseClone(this, pNewObj, remap);

	grow_vector(pNewObj->m_refList, NumRefs()-1, (RefTargetHandle)NULL);
	for(s32 i=0; i<NumRefs(); i++)
		pNewObj->ReplaceReference(i, remap.CloneRef(GetReference(i)));

/*	for(i=0; i<pNewObj->m_refList.size(); i++)
	{
		VehicleLink* pLink = (VehicleLink*)pNewObj->m_refList[i];
		s32 count = 0;

		if(pLink)
		{
			// Get the two nodes referencing the two Vehicle Connections referencing this link
			RefListItem *pItem = pLink->GetRefList().FirstItem();

			while(pItem)
			{
				if(pItem->maker->ClassID() == VEHICLE_CONNECTION_CLASS_ID)
					count++;
				pItem = pItem->next;
			}
			if(count != 2)
			{
				pLink->DeleteMe();
			}
		}
	}*/

	return pNewObj;
}

//
//        name: VehicleNode::Load/Save
// description: Load and Save number of links VehicleNode references. Have to do this or none of the links
//				are loaded
//
IOResult VehicleNode::Load(ILoad *iload)
{
	ULONG length;
	IOResult res;
	s32 numRefs;

	while (IO_OK == (res=iload->OpenChunk()))
	{
		switch(iload->CurChunkID())  
		{
		case VEH_CONNECTION_NUMREFS_CHUNK:
			res = iload->Read(&numRefs, sizeof(numRefs), &length);
			if(res == IO_OK)
				m_refList.assign(numRefs, NULL);
			break;
		}
		iload->CloseChunk();
		if (res!=IO_OK) 
			return res;
	}

	return IO_OK;
}

IOResult VehicleNode::Save(ISave *isave)
{
	IOResult res=IO_OK;
	ULONG length;
	s32 numRefs = m_refList.size();

	isave->BeginChunk(VEH_CONNECTION_NUMREFS_CHUNK);
	res = isave->Write(&numRefs, sizeof(numRefs), &length);
	isave->EndChunk();

	return res;
}

//
//        name: VehicleNode::AddReference,RemoveReference
// description: Extra reference handling functions. To allow the user to add a reference without indicating a position
//
s32 VehicleNode::AddReference(RefTargetHandle rtarg)
{
	VehicleLink* pLink=(VehicleLink*)rtarg;

	s32 i=0;

	for(i=0; i<m_refList.size(); i++)
	{
		if(m_refList[i] == NULL)
			break;
	}
	grow_vector(m_refList, i, (RefTargetHandle)NULL);

	ReplaceReference(i+1, rtarg);
	return i;
}

void VehicleNode::RemoveReference(RefTargetHandle rtarg)
{
	VehicleLink* pLink=(VehicleLink*)rtarg;

	for(s32 i=0; i<m_refList.size(); i++)
	{
		if(m_refList[i] == rtarg)
		{
			ReplaceReference(i+1, NULL);
			return;
		}
	}
	assert(0);
}

void VehicleNode::RefDeleted()
{
	INode* pINode = GetINode();
	VehicleLink *pLink;

	if(NULL == pINode)
	{
		// go thru list of links attached to this connection
		for(int i=0; i<m_refList.size(); i++)
		{
			if(m_refList[i] && m_refList[i]->ClassID() == VEHICLE_LINK_CLASS_ID)
			{
				pLink = (VehicleLink *)m_refList[i];
				RemoveReference(pLink);

			}
		}
	}
}

//
//        name: VehicleNode::NumRefs, GetReference, SetReference
// description: Standard reference handling functions
//
int VehicleNode::NumRefs() 
{
	return m_refList.size()+1;
}

RefTargetHandle VehicleNode::GetReference(int i) 
{
	if(i == 0)
	{
		return m_pBlock2;
	}
	else
	{
		i--;
		assert(i < m_refList.size());
		return m_refList[i];
	}
}

void VehicleNode::SetReference(int i, RefTargetHandle rtarg) 
{	

	if(i == 0)
	{
		m_pBlock2 = (IParamBlock2 *)rtarg;
	}
	else
	{
		i--;
		grow_vector(m_refList, i, (RefTargetHandle)NULL);
		m_refList[i] = rtarg;
	}
}

RefResult VehicleNode::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget,
										   PartID& partID,  RefMessage message) 
{
	switch (message)
	{
	case REFMSG_TARGET_DELETED:
		for(s32 i=0; i<m_refList.size(); i++)
		{
			if(m_refList[i] == hTarget)
			{
				assert(m_refList[i]->ClassID() == VEHICLE_LINK_CLASS_ID);
				VehicleLink* pLink = (VehicleLink* )m_refList[i];
				m_refList[i] = NULL;
			}
		}
	break;

	}
	return REF_SUCCEED;
}



//From BaseObject
CreateMouseCallBack* VehicleNode::GetCreateMouseCallBack() 
{
	return NULL;
}

//
//        name: VehicleNode::GetLocalBoundBox,GetWorldBoundBox
// description: Return bounding box in local coords or world coords
//
void VehicleNode::GetLocalBoundBox(TimeValue t, INode* pNode, ViewExp* pVpt, Box3& box)
{
	box.Init();
}

void VehicleNode::GetWorldBoundBox(TimeValue t, INode* pNode, ViewExp* pVpt, Box3& box)
{
	Matrix3 objMat = pNode -> GetObjectTM(t);
	float scaleFactor = GetDrawingScaleFactor(t, pNode, pVpt) * VEH_NODE_CIRCLE_RADIUS;
	float radius = 1.0f; //m_pBlock2->GetFloat(ai_radius);
	Box3 box2, linkBox;

	// get box surrounding radius circle
	box = Box3(Point3(-1.0f, -1.0f, -1.0f)*radius, Point3(1.0f, 1.0f, 1.0f)*radius);
	box = box * objMat;

	// add box surrounding static size circle
    box2 = Box3(Point3(-1.0f, -1.0f, -1.0f)*scaleFactor, Point3(1.0f, 1.0f, 1.0f)*scaleFactor);

	objMat.NoScale();
	objMat.NoRot();
	box += box2 * objMat;


	for(s32 i=0; i<m_refList.size(); i++)
	{
		if(m_refList[i])
		{
			((VehicleLink *)m_refList[i])->GetWorldBoundBox(t, pNode, pVpt, linkBox);
			box += linkBox;
		}
	}
}

//
//        name: VehicleNode::Display
// description: Display the object
//
int VehicleNode::Display(TimeValue t, INode* pNode, ViewExp *pVpt, int flags)
{
	GraphicsWindow *gw = pVpt->getGW();

	if(true == VehicleNodeCreateMgr::m_bViewDifferent)
	{
		if(true == IsDifferentFrom(VehicleNodeCreateMgr::GetDefaultVehicleNode()))
		{
			gw->setColor(LINE_COLOR, 0.0f, 0.0f, 5.0f);
			Draw(t, pVpt, pNode);
			return 0;
		}
	}

	if (pNode->Selected()) 
	{
		gw->setColor(LINE_COLOR, 1.0f, 0.0f, 0.0f);
	}
	else if(0 == GetNumberOfLinks()) 
	{
		gw->setColor(LINE_COLOR, 1.0f, 0.5f, 0.5f);
	}
	else if(!pNode->IsFrozen()) 
	{
		gw->setColor(LINE_COLOR, 1.0f, 1.0f, 0.0f);
	}
	Draw(t, pVpt, pNode);

	return 0;
}

//
//        name: VehicleNode::HitTest
// description: Test if object is under point
//
int VehicleNode::HitTest(TimeValue t, INode* pNode, int type, int crossing, int flags, IPoint2 *pSp, ViewExp *pVpt)
{
	HitRegion hitRegion;
	DWORD savedLimits;
	GraphicsWindow *gw = pVpt->getGW();

	MakeHitRegion(hitRegion, type, crossing, 4, pSp);

	gw->setRndLimits(((savedLimits = gw->getRndLimits()) | GW_PICK) & ~GW_ILLUM);
	gw->setHitRegion(&hitRegion);
	gw->clearHitCode();	

	Draw(t, pVpt, pNode);

	int res = gw->checkHitCode();
	if(1 == res)
	{
		int i = 0;
		i++;
	}

	gw->setRndLimits(savedLimits);

	return res;
}

//
//        name: VehicleNode::GetDrawingScaleFactor
// description: Return how much to scale VehicleNode to keep it the same no matter how far or near it is to
//				the camera
//          in: pNode = pointer to VehicleNode's INode
//				pVpt = pointer to viewport
//         out: scale factor
//
float VehicleNode::GetDrawingScaleFactor(TimeValue t, INode* pNode, ViewExp* pVpt)
{
	Matrix3 mtx = pNode->GetObjectTM(t);
	float nonScale = pVpt->NonScalingObjectSize();
	float worldWidth = pVpt->GetVPWorldWidth(mtx.GetTrans());

	return (nonScale*worldWidth) * VEH_NODE_SCREEN_RATIO;
}

//
//        name: VehicleNode::Draw
// description: Code to draw the Vehicle Connection. This is used by both the Display and HitTest functions
//
void VehicleNode::Draw(TimeValue t, ViewExp* pVpt, INode *pNode)
{
	GraphicsWindow *gw = pVpt->getGW();
	DrawLineProc lineProc(gw);

	float scaleFactor = GetDrawingScaleFactor(t, pNode, pVpt);
	Point3 pt[NUM_CIRCLE_SEGMENTS+1];
	float u;
	int i;
	float radius = 0.5f; // m_pBlock2->GetFloat(ai_radius);

	//dmat::AttributeInst *pInst = GetAttributes(pNode);

	Matrix3 mtx = pNode->GetObjectTM(t);
	mtx.NoRot();
	gw->setTransform(mtx);

	// Camera facing
	float multiplier = float(TWOPI) / float(NUM_CIRCLE_SEGMENTS);
	for (i = 0; i <= NUM_CIRCLE_SEGMENTS; i++)
	{
		u = float(i) * multiplier;
		pt[i].x = (float)cos(u) * radius;
		pt[i].y = (float)sin(u) * radius;
		pt[i].z = 0.0f;
	}
	lineProc.proc(pt,NUM_CIRCLE_SEGMENTS+1);

	mtx.NoRot();
	mtx.NoScale();
	mtx.Scale(Point3(scaleFactor, scaleFactor, scaleFactor));
	gw->setTransform(mtx);

//	gw->setColor(LINE_COLOR, 0, 0, 0);
	for (i = 0; i <= NUM_CIRCLE_SEGMENTS; i++)
	{
		u = float(i) * multiplier;
		pt[i].x = (float)cos(u) * VEH_NODE_CIRCLE_RADIUS;
		pt[i].y = (float)sin(u) * VEH_NODE_CIRCLE_RADIUS;
		pt[i].z = 0.0f;

	//	pt[i]=pt[i]*invCamMat;
	}
	lineProc.proc(pt,NUM_CIRCLE_SEGMENTS+1);
}

//
//        name: VehicleNode::PickConnectionTarget
// description: Start the pick mode for picking another VehicleNode to connect to
//
void VehicleNode::PickConnectionTarget()
{
	theConnectVehPickMode.SetButton(m_pConnectButton);
	theConnectVehPickMode.SetPickOperation(PickVehConnection::CONNECT);
	theConnectVehPickMode.SetVehicleNode(this);
	m_ip->SetPickMode(&theConnectVehPickMode);
}

//
//        name: VehicleNode::PickConnectionTarget
// description: Start the pick mode for picking another VehicleNode to connect to
//
/*
void VehicleNode::PickConnectionMerge()
{
	theMergeVehPickMode.SetButton(m_pMergeButton);
	theMergeVehPickMode.SetPickOperation(PickVehConnection::MERGE);
	theMergeVehPickMode.SetVehicleNode(this);
	m_ip->SetPickMode(&theMergeVehPickMode);
}
*/
//        name: VehicleNode::GetLinkBetween
// description: Returns the link between the two nodes, if one exists
//          in: pOther = the other VehicleNode
//
VehicleLink* VehicleNode::GetLinkBetween(VehicleNode *pOther)
{
	s32 i;
	VehicleLink *pLink;

	// go thru list of links attached to this connection
	for(i=0; i<m_refList.size(); i++)
	{
		if(m_refList[i])
		{
			pLink = (VehicleLink *)m_refList[i];

			// parse dependents to find the other connection
#if (MAX_RELEASE >= 12000)
			DependentIterator pItem(pLink);
			ReferenceMaker* maker = NULL;
			while(NULL != (maker = pItem.Next()))
			{
				if(maker == pOther)
				{
					return pLink;
				}
			}
#else
			RefList &refList = pLink->GetRefList();
			RefListItem *pItem;

			pItem = refList.FirstItem();
			while(pItem)
			{
				if(pItem->maker == pOther)
				{
					return pLink;
				}
				pItem = pItem->next;
			}
#endif  //MAX_RELEASE >= 12000
		}
	}
	return NULL;
}


//        name: VehicleNode::GetOtherLink
// description: Returns the link that isnt the one passed, if one exists
//          in: pLinkThis = the link not to get
//
VehicleLink* VehicleNode::GetOtherLink(VehicleLink* pLinkIgnore)
{
	s32 i;
	VehicleLink *pLink;

	// go thru list of links attached to this Node
	for(i=0; i<m_refList.size(); i++)
	{
		if(m_refList[i])
		{
			pLink = (VehicleLink *)m_refList[i];
			if(pLink != pLinkIgnore)
			{
				return pLink;
			}
		}
	}
	return NULL;
}


//
//        name: *VehicleNode::GetINode
// description: Get the INode that references this object. Assumes only one INode ie object hasn't been instanced
//
INode* VehicleNode::GetINode()
{
	INode *pThisNode = NULL;

	// parse dependents to find the INode
#if (MAX_RELEASE >= 12000)
	//ReferenceTarget *rt = NULL;
	ReferenceTarget *rt = (ReferenceTarget*)this;
	DependentIterator pItem(rt);
	ReferenceMaker* maker = NULL;

	while(NULL != (maker = pItem.Next()))
	{
		if((maker) && (maker->SuperClassID() == BASENODE_CLASS_ID))
		{
			pThisNode = (INode *)maker;
			break;
		}
	}
#else
	RefList &refList = GetRefList();
	RefListItem *pItem;

	pItem = refList.FirstItem();
	while(pItem)
	{
		if((pItem->maker) && (pItem->maker->SuperClassID() == BASENODE_CLASS_ID))
		{
			pThisNode = (INode *)pItem->maker;
			break;
		}
		pItem = pItem->next;
	}
#endif  //MAX_RELEASE >= 12000

	//assert(pThisNode);

	return pThisNode;
}

//
//        name: ConnectTo
// description: Connect this VehicleNode to the other node
//

VehicleLink* VehicleNode::ConnectTo(INode *pNode)
{
	VehicleNode *pOther = (VehicleNode *)pNode->EvalWorldState(0).obj;

	assert(pOther->ClassID() == VEHICLE_NODE_CLASS_ID);

	if(GetLinkBetween(pOther) != NULL)
		return NULL;

	INode *pThisNode = GetINode();

	theHold.Begin();

	VehicleLink *pLink = NULL; //(VehicleLink *)m_ip->CreateInstance(HELPER_CLASS_ID, VEHICLE_LINK_CLASS_ID);
	INode *pLinkNode = NULL; //m_ip->CreateObjectNode(pLink);
	VehicleNodeCreateMgr::CreateVehicleLinkDefault((IObjCreate*)m_ip, pLink, pLinkNode);

	pLink->ReplaceReference(0, pThisNode->GetTMController());
	pLink->ReplaceReference(1, pNode->GetTMController());
	AddReference(pLink);
	pOther->AddReference(pLink);

	NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
	pOther->NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);

	theHold.Accept("Link VehicleNode");

	m_ip->RedrawViews(m_ip->GetTime());

	return pLink;
}

//
//        name: VehicleNode::SelectConnectingObjects
// description: Select objects in connected list
//
/*
void VehicleNode::SelectConnectingObjects(IParamBlock2 *pBlock2, s32 id)
{
	INodeTab nodeTab;
	INode *pNode;
	s32 i;
	Interface *ip = m_ip;


	for(i=0; i<pBlock2->Count(id); i++)
	{
		pNode = pBlock2->GetINode(id, 0, i);
		if(pNode)
			nodeTab.Insert(i, 1, &pNode);
	}

	// have to copy m_ip because the VehicleNode is deselected and EndEditParams is called
	ip->ClearNodeSelection();
	ip->SelectNodeTab(nodeTab, TRUE);
}
*/
//
//        name: VehicleNode::GetNumberOfLinks
// description: Return number of connections linked to this one
//
s32 VehicleNode::GetNumberOfLinks()
{
	s32 i, count=0;
	// go thru list of links attached to this connection
	for(i=0; i<m_refList.size(); i++)
	{
		if(m_refList[i] && m_refList[i]->ClassID() == VEHICLE_LINK_CLASS_ID)
		{
			count++;
		}
	}
	return count;
}

//
//        name: InitObjUI, CloseObjUI
//
void VehicleNode::InitObjUI()
{
	if(m_hObjWnd == NULL)
	{
		m_hObjWnd = m_ip->AddRollupPage(
			hInstance, 
			MAKEINTRESOURCE(IDD_VEH_NODE_PANEL),
			EditVehicleNodeProc,
			_T("Vehicle Node"),
			(LPARAM)this);
		m_pConnectButton = GetICustButton(GetDlgItem(m_hObjWnd, IDC_CONNECT_BUTTON));
		m_pConnectButton->SetType(CBT_CHECK);
		m_pConnectButton->SetHighlightColor(GREEN_WASH);
		EnableWindow(GetDlgItem(m_hObjWnd, IDC_MERGE_BUTTON), FALSE);
		EnableWindow(GetDlgItem(m_hObjWnd, IDC_SELECTCONNECT_BUTTON), FALSE);
		SetWindowText(GetDlgItem(m_hObjWnd, IDC_MERGE_BUTTON), " ");
		SetWindowText(GetDlgItem(m_hObjWnd, IDC_SELECTCONNECT_BUTTON), " ");
	}
}

void VehicleNode::CloseObjUI()
{
	if(m_hObjWnd)
	{
		m_ip->ClearPickMode();
		ReleaseICustButton(m_pConnectButton);
		ReleaseICustButton(m_pMergeButton);
		m_ip->DeleteRollupPage(m_hObjWnd);
		m_hObjWnd = NULL;
	}
}

//
//        name: VehicleNode::HitTest
// description: Testing for selection of VehicleLink's 
//
int VehicleNode::HitTest(TimeValue t, INode* pNode, int type, int crossing, 
						int flags, IPoint2 *p, ViewExp *pVpt, ModContext* mc)
{	
	VehicleLink* pLink;
	s32 i;

	int res = 0;
	// go thru list of links attached to this connection

	for(i=0; i<m_refList.size(); i++)
	{
		if(m_refList[i])
		{
			pLink = (VehicleLink* )m_refList[i];
			
/*			if(!(flags&HIT_SELONLY && !pLink->IsSelected()) &&
				!(flags&HIT_UNSELONLY && pLink->IsSelected())) 
*/			{
				if(pLink->HitTest(t, pNode,type,crossing,flags,p,pVpt))
				{
					pVpt->LogHit(pNode, mc, 0, i, NULL);
					res = TRUE;
					if(flags & HIT_ABORTONHIT) 
						return TRUE;
				}
			}
		}
	}
	return res;
}

// --- Windows callback ------------------------------------------------------------------------------------
#if !defined( _WIN64 )
BOOL CALLBACK EditVehicleNodeProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
#else
INT_PTR CALLBACK EditVehicleNodeProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
#endif
{
	static VehicleNode *pObject;
	if(pObject == NULL && message != WM_INITDIALOG)
		return TRUE;

	switch(message)
	{
	case WM_INITDIALOG:
		pObject = (VehicleNode *)lParam;
		break;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDC_CONNECT_BUTTON:
			pObject->PickConnectionTarget();
			break;
//		case IDC_SELECTCONNECT_BUTTON:
//			pObject->SelectConnectingObjects(pObject->GetParamBlockByID(vehicle_node_params), aic_objectlist);
//			break;
		default:
			break;
		}
		break;

	case WM_CLOSE:
		break;

	default:
		return FALSE;
	}
	return TRUE;
}

void VehicleNode::DeleteThis()
{
//	DeleteAllLinks();
	delete this;
}

//
//        name: VehicleNode::DeleteAllLinks
// description: Deletes all the links referenced ny this node
//          in: 
//
void VehicleNode::DeleteAllLinks()
{
	s32 i;
	VehicleLink *pLink;

	// go thru list of links attached to this connection
	for(i=0; i<m_refList.size(); i++)
	{
		if(m_refList[i] && m_refList[i]->ClassID() == VEHICLE_LINK_CLASS_ID)
		{
			pLink = (VehicleLink *)m_refList[i];
			RemoveReference(pLink);
			pLink->Delete(this);

		}
	}
}

//
//        name: VehicleNode::UpdateAllLinks
// description: Updates all the links belonging to this Node
//				Only the position just now
//          in: 
//
void VehicleNode::UpdateAllLinks(Interface *ip)
{
	s32 i;
	VehicleLink *pLink;

	// go thru list of links attached to this connection
	for(i=0; i<m_refList.size(); i++)
	{
		if(m_refList[i] && m_refList[i]->ClassID() == VEHICLE_LINK_CLASS_ID)
		{
			pLink = (VehicleLink *)m_refList[i];
			pLink->SetPos(ip);

		}
	}
}

//
//        name: VehicleNode::GetBothLinks
// description: Fills array of 2 pointers to links
//				Useed by the Autocreate system so Node should have 2 links
//				If node doesnt have 2 links returns false				
//          in: array of pointers to store the links
//
bool VehicleNode::GetBothLinks(VehicleLink* pLinks[2])
{
	s32 i;
	s32 index = 0;
	// go thru list of links attached to this connection
	for(i=0; i<m_refList.size(); i++)
	{
		if(m_refList[i] && m_refList[i]->ClassID() == VEHICLE_LINK_CLASS_ID)
		{
			pLinks[index] = (VehicleLink *)m_refList[i];
			index++;
			if(index > 2)
			{
				return false;
			}
		}
	}
	return (index == 2);
}

//        name: VehicleNode::IsDifferentFrom
// description: If this node is different from the Node passed 
//				then return true.
//          in: Node to check differences with
//
bool VehicleNode::IsDifferentFrom(VehicleNode* pNodeToCheck)
{
	DataInstance* pThisData = theIMaxDataStore.GetData(this);
	DataInstance* pDefData = theIMaxDataStore.GetData(pNodeToCheck);
	if( (NULL ==pThisData) || (NULL == pDefData) )
	{
		return false;
	}
	dmat::AttributeInst* pThisAttr = pThisData->GetAttributes();
	dmat::AttributeInst* pDefAttr = pDefData->GetAttributes();

	dmat::AttributeClass defClass = pDefAttr->GetClass();
	dmat::AttributeClass thisClass = pThisAttr->GetClass();

	s32 sizeDef = defClass.GetSize();
	s32 sizeThis = defClass.GetSize();
	if(sizeThis != sizeDef)
	{
		return true;
	}
	s32 matching=0;
	for(s32 i=0; i<sizeDef; i++)
	{
		dmat::Attribute defAttr = defClass.GetItem(i);
		switch(defAttr.GetType())
		{
			case dmat::Attribute::INT :
			{
				s32 val1 = pDefAttr->GetAttributeValue(i);
				s32 val2 = pThisAttr->GetAttributeValue(i);
				{
					if(val1 == val2)
					{
						matching++;
					}
				}
			}
			break;
			case dmat::Attribute::FLOAT :
			{
				float val1 = pDefAttr->GetAttributeValue(i);
				float val2 = pThisAttr->GetAttributeValue(i);
				{
					if(val1 == val2)
					{
						matching++;
					}
				}
			}
			break;
			case dmat::Attribute::BOOL	:
			{
				bool val1 = pDefAttr->GetAttributeValue(i);
				bool val2 = pThisAttr->GetAttributeValue(i);
				{
					if(val1 == val2)
					{
						matching++;
					}
				}
			}
			break;
			case dmat::Attribute::STRING :
			{	
				char* val1 = pDefAttr->GetAttributeValue(i);
				char* val2 = pThisAttr->GetAttributeValue(i);
				{
					if(0 == strcmp(val1, val2))
					{
						matching++;
					}
				}
			}
			break;
		}
	}

	if(matching == sizeDef)
	{
		return false;
	}
	return true;
}
