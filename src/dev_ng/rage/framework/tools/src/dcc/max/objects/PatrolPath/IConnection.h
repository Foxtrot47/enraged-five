//
//
//    Filename: IConnection.h
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Interface header for PatrolPath
//
//
#ifndef INC_ICONNECTION_H_
#define INC_ICONNECTION_H_

// Class IDs
#define PATROL_NODE_CLASS_ID		Class_ID(0x6eb17bd4, 0x666250c4)
#define PATROL_LINK_CLASS_ID		Class_ID(0x4b1a066c, 0x7f5058c)

// PatrolPath parameters
// Paramblock2 name
enum { patrol_node_params, patrol_link_params, patrol_path_params}; 

// PatrolPath Paramblock2 parameter list
/*
enum { 
	patrol_auto_create,
	patrol_auto_create_min_length,
	patrol_node_disable,
	patrol_node_roadblock,
	patrol_node_water,
	patrol_node_car_speed,
	patrol_node_special,
	patrol_link_lanes_in,
	patrol_link_lanes_out,
	patrol_link_width,
};
*/

// PatrolLink References
enum {
	ail_control1 = 0,
	ail_control2,
	ail_paramblock,
	ail_numrefs
};

#define	MIN_LINK_LEN	(100)
#define MAX_LINK_LEN	(1000)
#define DEF_LINK_LEN	(200)

#endif // INC_ICONNECTION_H_