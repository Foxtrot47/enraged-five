#include "Water.h"

extern ClassDesc2* GetWaterDesc();
extern ClassDesc2* GetWaterCalmingDesc();
extern ClassDesc2* GetWaterWaveDesc();
extern ClassDesc2* GetRiverHelperDesc();

HINSTANCE hInstance;
int controlsInit = FALSE;

/////////////////////////////////////////////////////////////////////////////////////////////////
BOOL WINAPI DllMain(HINSTANCE hinstDLL,ULONG fdwReason,LPVOID lpvReserved)
{
	hInstance = hinstDLL;

	if(!controlsInit) 
	{
		controlsInit = TRUE;
		InitCommonControls();
	}
			
	return (TRUE);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
__declspec( dllexport ) const TCHAR* LibDescription()
{
	return GetString(IDS_LIBDESCRIPTION);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
__declspec( dllexport ) int LibNumberClasses()
{
	return 4;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
__declspec( dllexport ) ClassDesc* LibClassDesc(int i)
{
	switch(i) 
	{
		case 0: return GetWaterDesc();
		case 1: return GetWaterCalmingDesc();
		case 2: return GetWaterWaveDesc();
		case 3: return GetRiverHelperDesc();
		default: return 0;
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////
__declspec( dllexport ) ULONG LibVersion()
{
	return VERSION_3DSMAX;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
TCHAR *GetString(int id)
{
	static TCHAR buf[256];

	if(hInstance)
	{
		return LoadString(hInstance, id, buf, sizeof(buf)) ? buf : NULL;
	}

	return NULL;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
__declspec( dllexport ) ULONG CanAutoDefer()
{
	return 0;
}
