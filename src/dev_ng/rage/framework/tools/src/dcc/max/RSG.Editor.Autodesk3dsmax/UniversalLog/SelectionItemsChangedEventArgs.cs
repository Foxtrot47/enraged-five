﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Editor.Autodesk3dsmax.UniversalLog
{
    /// <summary>
    /// Event arguments to notify that the selected items have changed.
    /// </summary>
    public class SelectionItemsChangedEventArgs : EventArgs
    {
        #region Member Data
        /// <summary>
        /// Private field for the <see cref="SelectedItems"/> property.
        /// </summary>
        private IList _selectedItems;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="selectedItems"></param>
        public SelectionItemsChangedEventArgs(IList selectedItems)
        {
            _selectedItems = selectedItems;
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// The new list of selected items.
        /// </summary>
        public IList SelectedItems
        {
            get { return _selectedItems; }
        }
        #endregion // Properties
    } // SelectedItemsChangedEventArgs
}
