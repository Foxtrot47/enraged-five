Project cutfileloader
Template max_configurations.xml

OutputFileName cutfileloader.gup
ConfigurationType dll

WarningsAsErrors false
WarningLevel 3

IncludePath .\midl\Win32;$(RAGE_DIR)/base/src;$(RAGE_DIR)\suite\src;$(RAGE_DIR)\base\tools;$(RAGE_DIR)\base\tools\dcc\max;$(RAGE_DIR)\base\tools\dcc\libs

ForceInclude forceinclude.h

EmbeddedLibAll odbc32.lib odbccp32.lib comctl32.lib bmm.lib core.lib geom.lib gfx.lib mesh.lib maxutil.lib maxscrpt.lib gup.lib paramblk2.lib

Files {
	Folder "Header Files" { 
		3dsmaxsdk_preinclude.h
		cutfileloader.h
		forceinclude.h
		LightAnimationExporter.h
		resource.h
	}
	Folder "Resource Files" {	
		cutfileloader.rc
	}
	Folder "Source Files" {
		cutfileloader.cpp
		LightAnimationExporter.cpp
		cutfileloader.def
		DllEntry.cpp
		script.cpp
	}	
}