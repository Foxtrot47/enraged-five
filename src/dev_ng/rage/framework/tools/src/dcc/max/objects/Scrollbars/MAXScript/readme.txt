gtaScrollbars.ms

Attempt at constructing the Scrollbar object using a MAX Script plugin rather than a C++ plugin.
Changed to the C++ plugin as could not find a method of constructing sub-objects in MAXScript.