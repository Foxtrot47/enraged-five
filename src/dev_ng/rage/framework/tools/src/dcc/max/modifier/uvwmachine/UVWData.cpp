//
//
//    Filename: UVWData.cpp
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 7/06/99 17:17 $
//   $Revision: 6 $
// Description: LocalModData for UVWmachine modifier
//
//

// std headers
#include <stack>
// my headers
#include "UVWmachine.h"
#include "UVWData.h"
#include "UVWRestore.h"

//
//        name: UVWData::UVWData
// description: default constructor
//
UVWData::UVWData() : StdMeshSelectData(), m_faceHeld(false), m_vertexHeld(false)
{
}

//
//        name: UVWData::UVWData
// description: copy constructor. Only copies selection information. Waits for SetCache to do the
//				rest of the copying
//
UVWData::UVWData(const UVWData &data) : StdMeshSelectData(data), m_faceHeld(false), m_vertexHeld(false)
{
	m_uvw = data.m_uvw;
	m_uvwFaces = data.m_uvwFaces;
}

UVWData::UVWData(Mesh &mesh) : StdMeshSelectData(mesh), m_faceHeld(false), m_vertexHeld(false)
{
}

//
//        name: UVWData::Clone
// description: Produce a clone of the mod context
//          in: 
//         out: 
//
LocalModData* UVWData::Clone(void) 
{
	return new UVWData(*this);
}

//
//        name: UVWData::SetCache
// description: 
//          in: 
//         out: 
//
void UVWData::SetCache(Mesh &mesh)
{
	StdMeshSelectData::SetCache(mesh);

	//
	// copy UV coordinates
	//
	std::vector<UVVert> uvw;
	int32 noOfUVW = mesh.numTVerts;
	int32 uvwToCopy;
	int32 i;

	// reserve space for uv coords
	uvw.reserve(noOfUVW);

	// how many uv coords to copy from previous version of mod context
	uvwToCopy = m_uvw.size();
	if(uvwToCopy)
		uvw.assign(m_uvw.begin(), m_uvw.end());

	// copy uv coords from mesh
	if(noOfUVW - uvwToCopy > 0)
	{
		for(i=0; i<(noOfUVW - uvwToCopy); i++)
			uvw.push_back(*(mesh.tVerts+uvwToCopy+i));
	}

	// copy uv coords vector
	m_uvw = uvw;


	//
	// copy UV coordinate faces
	//
	std::vector<TVFace> uvwFaces;
	int32 noOfUVWFaces = mesh.numFaces;
	int32 uvwFacesToCopy;

	// reserve space for colours
	uvwFaces.reserve(noOfUVWFaces);

	// how many uv coords to copy from previous version of mod context
	uvwFacesToCopy = m_uvwFaces.size();
	if(uvwFacesToCopy)
		uvwFaces.assign(m_uvwFaces.begin(), m_uvwFaces.end());

	// copy uv coords from mesh
	if(noOfUVWFaces - uvwFacesToCopy > 0)
	{
		for(i=0; i<(noOfUVWFaces - uvwFacesToCopy); i++)
			uvwFaces.push_back(*(mesh.tvFace + uvwFacesToCopy+i));
	}

	// copy colour vector
	m_uvwFaces = uvwFaces;


	DestroyExtractedArrays();
}


//
//        name: UVWData::SetUVW
// description: set uv coord array
//
void UVWData::SetUVW(std::vector<UVVert> &uvw) 
{
	m_uvw = uvw; 
	if(m_uvw.size() != m_vtxExtracted.size())
		m_vtxExtracted.clear();
}

//
//        name: UVWData::SetUVWFaces
// description: set uv faces array
//
void UVWData::SetUVWFaces(std::vector<TVFace> &uvwFaces)
{
	m_uvwFaces = uvwFaces; 
	if(m_uvwFaces.size() != m_faceExtracted.size())
		m_faceExtracted.clear();
}

//
//        name: UVWData::DestroyExtractedArrays
// description: Clear the extracted arrays
//
void UVWData::DestroyExtractedArrays(void)
{
	m_vtxExtracted.clear();
	m_faceExtracted.clear();
}

//
//        name: UVWData::ExtractVertex
// description: Set UV coord so it is only used by one mesh vertex
//          in: index = index of UV coord
//
bool UVWData::ExtractVertex(int32 face, int32 index)
{
	int32 vtxIndex = m_pMesh->faces[face].v[index];
	int32 uvIndex = m_uvwFaces[face].t[index];
	int32 i,j;

	// construct array if it is not already available
	if(m_vtxExtracted.empty())
		m_vtxExtracted.assign(m_uvw.size(), false);

	// check if already extracted
	if(m_vtxExtracted[uvIndex])
		return false;

	//
	// Go through all the vertices on all the faces if they have the same uv coord index as the
	// vertex to be extracted and do not have the same vertex position then extract that uv coord
	// by adding a new uv coordinate at the end of the list and pointing to it
	//
	for(i=0; i<m_pMesh->numFaces; i++)
	{
		// make assumption that one face cannot have the same UV coordinate on 2 of its vertices
		if(i == face)
			continue;
		for(j=0; j<3; j++)
		{
			if(m_uvwFaces[i].t[j] == uvIndex && m_pMesh->faces[i].v[j] != vtxIndex)
			{
				if(theHold.Holding())
				{
					if(!IsVertexHeld())
						theHold.Put(new UVWVertexRestore(UVWmachine::m_pCurrentModifier, this));
					if(!IsFaceHeld())
						theHold.Put(new UVWFaceRestore(UVWmachine::m_pCurrentModifier, this));
				}
				m_uvw.push_back(m_uvw[uvIndex]);
				m_vtxExtracted.push_back(true);
				uvIndex = m_uvw.size() - 1;
				m_uvwFaces[face].t[index] = uvIndex;
				return true;
			}
		}
	}
	m_vtxExtracted[uvIndex] = true;
	return false;
}

//
//        name: UVWData::ExtractFace
// description: Make face use unique vertices
//          in: index = index of face
//
void UVWData::ExtractFace(int32 index)
{
	int32 uvIndex;
	int32 i,j,k;

	if(m_pMesh == NULL)
		return;

	// construct array if it is not already available
	if(m_faceExtracted.empty())
		m_faceExtracted.assign(m_uvwFaces.size(), false);

	if(m_faceExtracted[index])
		return;

	// go through the three vertices of this face
	for(i=0; i<3; i++)
	{
		uvIndex = m_uvwFaces[index].t[i];

		// for all the other faces in this mesh check to see if they are using the same uv coordinate
		// index as this vertex. If one is then extract this vertex, by adding a new uv coord at the
		// end of the list and pointing to it
		for(j=0; j<m_uvwFaces.size(); j++)
		{
			if(j == index)
				continue;
			for(k=0; k<3; k++)
			{
				if(m_uvwFaces[j].t[k] == uvIndex)
					break;
			}
			if(k != 3)
			{
				if(theHold.Holding())
				{
					if(!IsVertexHeld())
						theHold.Put(new UVWVertexRestore(UVWmachine::m_pCurrentModifier, this));
					if(!IsFaceHeld())
						theHold.Put(new UVWFaceRestore(UVWmachine::m_pCurrentModifier, this));
				}
				m_uvw.push_back(m_uvw[uvIndex]);
				if(!m_vtxExtracted.empty())
					m_vtxExtracted.push_back(true);
				uvIndex = m_uvw.size() - 1;
				m_uvwFaces[index].t[i] = uvIndex;
				break;
			}
		}

	}
	m_faceExtracted[index] = true;
}

//
//        name: UVWData::ExtractFaces
// description: Make faces in array use unique vertices from the rest of the mesh
//          in: index = index of face
//
void UVWData::ExtractFaces(BitArray &faces)
{
	int32 uvIndex;
	int32 i,j,k,m;

	if(m_pMesh == NULL)
		return;

	// construct array if it is not already available
	if(m_faceExtracted.empty())
		m_faceExtracted.assign(m_uvwFaces.size(), false);

	// 
	// for all the selected faces check uv coordinate indices of 3 corners against the uv coordinate
	// indices of all the unselected faces. If an unselected face uses a uv coord index used by a 
	// selected face's corner then create a new version of that uv coord and get the selected face
	// to use that version
	// 
	for(m=0; m<m_uvwFaces.size(); m++)
	{
		if(!faces[m] || m_faceExtracted[m])
			continue;
		for(i=0; i<3; i++)
		{
			uvIndex = m_uvwFaces[m].t[i];

			for(j=0; j<m_uvwFaces.size(); j++)
			{
				if(faces[j])
					continue;
				for(k=0; k<3; k++)
				{
					if(m_uvwFaces[j].t[k] == uvIndex)
						break;
				}
				if(k != 3)
				{
					if(theHold.Holding())
					{
						if(!IsVertexHeld())
							theHold.Put(new UVWVertexRestore(UVWmachine::m_pCurrentModifier, this));
						if(!IsFaceHeld())
							theHold.Put(new UVWFaceRestore(UVWmachine::m_pCurrentModifier, this));
					}
					m_uvw.push_back(m_uvw[uvIndex]);
					if(!m_vtxExtracted.empty())
						m_vtxExtracted.push_back(false);
					m_uvwFaces[m].t[i] = m_uvw.size() - 1;
					break;
				}
			}
		}
		m_faceExtracted[m] = true;
	}
}

//
//        name: ConstructSurroundingVertexList
// description: 
//          in: 
//         out: 
//
void UVWData::ConstructSurroundingVertexList(AdjFaceList &adjFaceList, int32 face, BitArray &faceSet,
									std::vector<std::pair<int32, int32> > &fvList)
{
	std::vector<int32> firstFace(m_pMesh->getNumVerts(), -1);
	int32 edge;
	int32 vertex;
	int32 otherFace;
	int32 i;

	// get start edge. Cannot start on an edge with an adjacent face
	for(i=0; i<3; i++)
	{
		vertex = m_pMesh->faces[face].v[i];
		otherFace = adjFaceList[face].f[i];
		if(otherFace == UNDEFINED ||
			m_pMesh->faces[face].getEdgeVis(i))
		{
			break;
		}
	}

	// if did not find an outside edge then quit
	if(i == 3)
		return;

	edge = i;
	vertex = m_pMesh->faces[face].v[edge];


	do
	{
		// set I have visited this vertex
		if(firstFace[vertex] == -1)
			firstFace[vertex] = face;

		// add the face and vertex index pair to the list
		fvList.push_back(std::pair<int32, int32>(face, edge));
		faceSet.Set(face);

		// if there is an adjacent face and it has it's edge invisible assume they are part of 
		// the same polygon. Set the values to look at the new face and work out it's edge
		otherFace = adjFaceList[face].f[edge];
		if(otherFace != UNDEFINED && 
			!m_pMesh->faces[face].getEdgeVis(edge))
		{
			// get the other face's edge
			for(i=0; i<3; i++)
			{
				if(adjFaceList[otherFace].f[i] == face)
				{
					edge = i;
					break;
				}
			}
			assert(i != 3);
			face = otherFace;

		}

		edge++;
		if(edge == 3)
			edge = 0;
		vertex = m_pMesh->faces[face].v[edge];

	} while(firstFace[vertex] != face);

}

//
//        name: UVWData::RotatePolygonTextureCoords
// description: 
//          in: 
//         out: 
//
void UVWData::RotatePolygonTextureCoords(AdjFaceList &faceList, int32 face, BitArray &faceSet)
{
	std::vector<std::pair<int32, int32> > fvList;
	std::vector<std::pair<int32, int32> >::iterator iFaceVertex;
	std::pair<int32, int32> prev;
	int32 uv,uv2;

	ConstructSurroundingVertexList(faceList, face, faceSet, fvList);

	if(fvList.empty())
		return;

	// get last uv
	prev = *fvList.begin();
	uv = m_uvwFaces[(*(fvList.end()-1)).first].t[(*(fvList.end()-1)).second];
	uv2 = m_uvwFaces[prev.first].t[prev.second];

	for(iFaceVertex = fvList.begin() + 1; iFaceVertex != fvList.end(); iFaceVertex++)
	{
		// if the previous vertex was in the same face then use it's old uv index. Otherwise keep
		// the old uv
		if(prev.first == (*iFaceVertex).first)
			uv = uv2;
		// store the old uv
		uv2 = m_uvwFaces[(*iFaceVertex).first].t[(*iFaceVertex).second];
		m_uvwFaces[(*iFaceVertex).first].t[(*iFaceVertex).second] = uv;
		// store previous face/vertex pair
		prev = *iFaceVertex;
	}

	// set the first vertex uv
	if(prev.first == (*fvList.begin()).first)
		uv = uv2;
	m_uvwFaces[(*fvList.begin()).first].t[(*fvList.begin()).second] = uv;
}
//
//        name: UVWData::RotateTextureCoords
// description: Rotate the texture coords on a selection of faces
//          in: sel = what kind of face selection is used
//         out: 
//
void UVWData::RotateTextureCoords(int32 sel)
{
	int32 i;
	int32 uv, uv2;
	
	if (theHold.Holding() && !IsFaceHeld())
		theHold.Put(new UVWFaceRestore(UVWmachine::m_pCurrentModifier, this));

	if(sel == FACESEL_SINGLE)
	{

		for(i=0; i<m_uvwFaces.size(); i++)
		{
			if(m_faceSel[i])
			{
				uv = m_uvwFaces[i].t[0];
				m_uvwFaces[i].t[0] = m_uvwFaces[i].t[2];
				uv2 = m_uvwFaces[i].t[1];
				m_uvwFaces[i].t[1] = uv;
				m_uvwFaces[i].t[2] = uv2;
			}
		}
	}
	else if(m_pMesh)
	{
		AdjEdgeList edgeList(*m_pMesh);
		AdjFaceList faceList(*m_pMesh, edgeList);
		BitArray facesInPoly(m_faceSel.GetSize());
		BitArray faceSel = m_faceSel;

		facesInPoly.ClearAll();

		for(i=0; i<m_pMesh->getNumFaces(); i++)
		{
			if(faceSel[i])
			{
				RotatePolygonTextureCoords(faceList, i, facesInPoly);
				faceSel &= ~facesInPoly;
			}
		}
	}
}


//
//        name: UVWData::ProcessFaces
// description: Process texture coordinates of selected faces with a member function pointer
//
void UVWData::ProcessFaces(void (UVWData::*func)(int32 index))
{
	BitArray vertProcessed;
	int32 index;
	int32 i,j;
	
	if (theHold.Holding() && !IsVertexHeld())
		theHold.Put(new UVWVertexRestore(UVWmachine::m_pCurrentModifier, this));

	ExtractFaces(m_faceSel);
	
	vertProcessed.SetSize(m_uvw.size());
	vertProcessed.ClearAll();
	
	for(i=0; i<m_uvwFaces.size(); i++)
	{
		if(!m_faceSel[i])
			continue;
		for(j=0; j<3; j++)
		{	
			index = m_uvwFaces[i].t[j];
			if(vertProcessed[index])
				continue;
			vertProcessed.Set(index);
			
			(this->*func)(index);
		}
	}
}


//
//        name: UVWData::ProcessVertices
// description: Process selected texture coordinates with a member function pointer
//
void UVWData::ProcessVertices(void (UVWData::*func)(int32 index))
{
	BitArray vertProcessed(m_uvwFaces.size()*3);
	int32 index;
	int32 i,j;
	bool isExtracted;
	
	if (theHold.Holding() && !IsVertexHeld())
		theHold.Put(new UVWVertexRestore(UVWmachine::m_pCurrentModifier, this));
	
	vertProcessed.ClearAll();
	
	for(i=0; i<m_pMesh->getNumFaces(); i++)
	{
		for(j=0; j<3; j++)
		{
			if(m_vertSel[m_pMesh->faces[i].v[j]])
			{
				isExtracted = ExtractVertex(i,j);

				index = m_uvwFaces[i].t[j];
				// check if face has either been extracted or it has already been processed. Cannot
				// just check if it has been processed because the vertProcessed array does not
				// take into account extracted vertices
				if(isExtracted || !vertProcessed[index])
				{
					if(!isExtracted)
						vertProcessed.Set(index);
					(this->*func)(index);
				}
			}
		}
	}
	
}


void UVWData::Rotate90(int32 index)
{
	float u = m_uvw[index].x;
	m_uvw[index].x = m_uvw[index].y;
	m_uvw[index].y = 1 - u;
}
void UVWData::FlipU(int32 index)
{
	m_uvw[index].x = 1 - m_uvw[index].x;
}
void UVWData::FlipV(int32 index)
{
	m_uvw[index].y = 1 - m_uvw[index].y;
}

void UVWData::Rotate(int32 index)
{
	float u = m_uvw[index].x;
	float v = m_uvw[index].y;
	float u2;

	// values 0 and 1 are the origin of the rotate. value 2 is the angle to rotate
	u -= *m_pValues;
	v -= *(m_pValues+1);

	u2 = u * cos(*(m_pValues+2)) - v * sin(*(m_pValues+2));
	v = u * sin(*(m_pValues+2)) + v * cos(*(m_pValues+2));

	m_uvw[index].x = u2 + *m_pValues;
	m_uvw[index].y = v + *(m_pValues+1);
}

void UVWData::Scale(int32 index)
{
	float u = m_uvw[index].x;
	float v = m_uvw[index].y;

	// values 0 and 1 are the origin of the rotate. value 2 and 3 are the amounts to scale
	u -= *m_pValues;
	v -= *(m_pValues+1);

	u = u * (*(m_pValues+2));
	v = v * (*(m_pValues+3));

	m_uvw[index].x = u + *m_pValues;
	m_uvw[index].y = v + *(m_pValues+1);
}

void UVWData::Translate(int32 index)
{
	m_uvw[index].x += *(m_pValues+2);
	m_uvw[index].y += *(m_pValues+3);
}

void UVWData::ScaleU(int32 index)
{
	float u = m_uvw[index].x;

	u -= *m_pValues;

	u = u * (*(m_pValues+2));

	m_uvw[index].x = u + *m_pValues;
}

void UVWData::TranslateU(int32 index)
{
	m_uvw[index].x += *(m_pValues+2);
}

void UVWData::ScaleV(int32 index)
{
	float v = m_uvw[index].y;

	v -= *(m_pValues+1);

	v = v * (*(m_pValues+3));

	m_uvw[index].y = v + *(m_pValues+1);
}

void UVWData::TranslateV(int32 index)
{
	m_uvw[index].y += *(m_pValues+3);
}

void UVWData::SetU(int32 index)
{
	m_uvw[index].x = *(m_pValues);
}

void UVWData::SetV(int32 index)
{
	m_uvw[index].y = *(m_pValues+1);
}

//
//        name: UVWData::ExplicitSet
// description: Set the vertex texture coordinates explicitly
//          in: index = index of vertex into face
//				coord = 0 or 1. either u or v
//         out: 
//
void UVWData::ExplicitSet(int32 sel, int32 index, int32 coord)
{
	int32 vtxIndex;
	int32 i;


	if (theHold.Holding() && !IsVertexHeld())
		theHold.Put(new UVWVertexRestore(UVWmachine::m_pCurrentModifier, this));

	if(sel == FACESEL_SINGLE)
	{
		if(index == 3)
			return;

		ExtractFaces(m_faceSel);
		
		for(i=0; i<m_uvwFaces.size(); i++)
		{
			if(!m_faceSel[i])
				continue;
			vtxIndex = m_uvwFaces[i].t[index];
			if(coord == 0)
				m_uvw[vtxIndex].x = *(m_pValues);
			else
				m_uvw[vtxIndex].y = *(m_pValues+1);
		}
	}
	else
	{
		AdjEdgeList adjEdgeList(*m_pMesh);
		AdjFaceList adjFaceList(*m_pMesh, adjEdgeList);
		std::vector<std::pair<int32, int32> > fvList;
		std::vector<std::pair<int32, int32> >::iterator iFaceVertex;
		std::vector<std::pair<int32, int32> >::iterator iNext;
		BitArray faceSel(m_faceSel);
		BitArray facesInPoly(m_faceSel.GetSize());
		int32 j, size;
		
		ExtractFaces(m_faceSel);
		
		for(i=0; i<m_uvwFaces.size(); i++)
		{
			if(!faceSel[i])
				continue;
			
			ConstructSurroundingVertexList(adjFaceList, i, facesInPoly, fvList);
			
			// clear other faces in polygon from face selected array
			faceSel &= ~facesInPoly;

			// there are two cases covered 3 entries (ie one polygon) and 6 entries (ie 2 polygons)
			// if neither of these cases occur then ignore
			size = fvList.size();
			if(size != 3 && size != 6)
				continue;
			
			j = index;
			for(iFaceVertex = fvList.begin(); iFaceVertex != fvList.end(); iFaceVertex++)
			{
				if(j == 0)
				{
					vtxIndex = m_uvwFaces[(*iFaceVertex).first].t[(*iFaceVertex).second];
					if(coord == 0)
						m_uvw[vtxIndex].x = *(m_pValues);
					else
						m_uvw[vtxIndex].y = *(m_pValues+1);
				}
				iNext = iFaceVertex + 1;
				if(iNext != fvList.end())
				{
					if((*iFaceVertex).first == (*iNext).first)
					{
						j--;
						if(j == -3 && size == 3)
							j = 0;
						if(j == -4 && size == 6)
							j = 0;
					}
				}
			}
		}
		
	}
}

