﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Editor;
using RSG.Editor.Controls;
using RSG.Editor.Controls.Commands;
using RSG.Editor.SharedCommands;
using RSG.UniversalLog.Commands;
using RSG.UniversalLog.View;
using RSG.UniversalLog.ViewModel;

namespace RSG.Editor.Autodesk3dsmax.UniversalLog
{
    /// <summary>
    /// Event handler for when selected items changed.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void SelectionItemsChangedEventHandler(object sender, SelectionItemsChangedEventArgs e);

    /// <summary>
    /// Interaction logic for UniversalLogWindow.xaml
    /// </summary>
    public partial class UniversalLogWindow : RsMainWindow
    {
        #region Static Fields
        /// <summary>
        /// The global identifier used for the filter toolbar.
        /// </summary>
        private static readonly Guid _filterToolBar = new Guid("CC372B15-1EC5-4726-88F7-362B608F95DA");

        /// <summary>
        /// The global identifier used for our custom help toolbar.
        /// </summary>
        private static readonly Guid _customHelpToolBar = new Guid("8341B5BB-EE8E-4D24-BA03-7B0ED6223BDA");
        #endregion Static Fields

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="filenames"></param>
        public UniversalLogWindow(IEnumerable<String> filenames, bool showCancel = false, bool showContinue = false)
        {
            _showCancel = showCancel || showContinue;
            _showContinue = showContinue;

            InitializeComponent();
            AttachCommandBindings();

            UniversalLogDataContext dc = new UniversalLogDataContext();
            dc.OpenFiles(filenames, false);
            DataContext = dc;

            PrimaryViewer.FilterByLogLevelAction.ReInitialise(PrimaryViewer, UniversalLogCommands.FilterByLogLevel);
            PrimaryViewer.FilterByFileAction.ReInitialise(PrimaryViewer);

            PrimaryViewer.DataGrid.SelectionChanged += OnDataGridSelectionChanged;
        }

        /// <summary>
        /// 
        /// </summary>
        static UniversalLogWindow()
        {
            AddCommandInstances();
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Gets the url string that is shown when the user chooses to view help.
        /// </summary>
        protected override String HelpUrl
        {
            get { return "https://devstar.rockstargames.com/wiki/index.php/Universal_Log"; }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool ShowCancel
        {
            get { return _showCancel; }
        }
        private bool _showCancel;

        /// <summary>
        /// 
        /// </summary>
        public bool AllowMinimise
        {
            get { return !_showCancel; }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool ShowContinue
        {
            get { return _showContinue; }
        }
        private bool _showContinue;
        #endregion // Properties

        #region Events
        /// <summary>
        /// Wraps the DataGrid's selection changed event.
        /// </summary>
        public event SelectionItemsChangedEventHandler SelectedItemsChanged;
        #endregion // Events

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        public void DisableCountdown()
        {
            CountdownButton.Active = false;
        }

        /// <summary>
        /// Adds the commands/menus to the command manager.
        /// </summary>
        internal static void AddCommandInstances()
        {
            // Filter toolbar.
            RockstarCommandManager.AddToolbar(0, 0, "Filter", _filterToolBar);

            RockstarCommandManager.AddCommandInstance(
                UniversalLogCommands.FilterByLogLevel,
                0,
                true,
                "Filter by Log Level",
                new Guid("CD2F05E7-D5BD-4042-8B63-51DD99E69AA7"),
                _filterToolBar,
                new CommandInstanceCreationArgs()
                {
                    AreMultiItemsShown = true,
                    EachItemStartsGroup = false,
                });

            RockstarCommandManager.AddCommandInstance(
                UniversalLogCommands.FilterByFile,
                1,
                true,
                "Filter by File",
                new Guid("CB2091A4-99E7-4268-8158-FDD03680B16D"),
                _filterToolBar,
                new CommandInstanceCreationArgs()
                {
                    AreMultiItemsShown = false,
                    EachItemStartsGroup = true,
                });

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Find,
                2,
                true,
                "Find Text...",
                new Guid("77C0109E-D225-43D6-9F6C-58C9A47F3D2B"),
                _filterToolBar);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.FindPrevious,
                2,
                false,
                "Find Previous",
                new Guid("88EE9376-C6FA-4693-8561-04D8FD2867D5"),
                _filterToolBar);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.FindNext,
                2,
                false,
                "Find Next",
                new Guid("157D02AC-08D3-4036-88D0-5FB734BDC5DC"),
                _filterToolBar);

            // Remove the existing help toolbar.
            CommandBarItem helpItem =
                RockstarCommandManager.AllCommandBarItems.FirstOrDefault(item => item.Id == RockstarCommandManager.HelpToolBarId);
            if (helpItem != null)
            {
                RockstarCommandManager.RemoveCommandBarItem(helpItem);
            }

            // Add a custom help toolbar.
            RockstarCommandManager.AddToolbar(1, 0, "Help", _customHelpToolBar);

            RockstarCommandManager.AddCommandInstance(
                UniversalLogCommands.Email,
                0,
                false,
                "Email",
                new Guid("1C6BE5DF-887F-422B-90EC-8821166745DD"),
                _customHelpToolBar,
                new CommandInstanceCreationArgs()
                {
                    DisplayStyle = CommandItemDisplayStyle.ImageAndText
                });
            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.SaveAs,
                1,
                false,
                "Save As",
                new Guid("E2E62FA7-D4C1-4B92-965B-DEACC8E2EA7F"),
                _customHelpToolBar,
                new CommandInstanceCreationArgs()
                {
                    DisplayStyle = CommandItemDisplayStyle.ImageAndText
                });

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.ReportBug,
                2,
                false,
                "Report Bug",
                new Guid("08AA8F0A-5C8A-42A0-A6E8-F4DAE4FBC5AB"),
                _customHelpToolBar);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.ViewHelp,
                3,
                false,
                "Help",
                new Guid("81B35B08-7C76-4BF2-8082-77FB4129C31E"),
                _customHelpToolBar);

            // Copy context menu
            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Copy,
                0,
                false,
                "Copy",
                new Guid("25EF134F-FE34-4BE4-BEB0-9D97E35D142A"),
                UniversalLogCommandIds.ContextMenu);
        }
        
        /// <summary>
        /// Attaches all of the necessary command bindings to this instance.
        /// </summary>
        private void AttachCommandBindings()
        {
            new EmailAction(this.PrimaryViewer.DataContextResolver)
                .AddBinding(UniversalLogCommands.Email, this);
            new SaveAsAction(this.PrimaryViewer.DataContextResolver)
                .AddBinding(RockstarCommands.SaveAs, this);
            
            // Universal Log Control Commands
            this.PrimaryViewer.CopyAction.AddBinding(RockstarCommands.Copy, this);
            this.PrimaryViewer.FilterByLogLevelAction.AddBinding(UniversalLogCommands.FilterByLogLevel, this);
            this.PrimaryViewer.FilterByFileAction.AddBinding(UniversalLogCommands.FilterByFile, this);
            this.PrimaryViewer.SearchAction.AddBinding(this);
        }

        /// <summary>
        /// Called whenever the cancel button is pressed.
        /// </summary>
        /// <param name="sender">The button that fired this event.</param>
        /// <param name="e">The System.Windows.RoutedEventArgs data used for this event.</param>
        private void OnCancelPressed(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        /// <summary>
        /// Called whenever the continue button is pressed.
        /// </summary>
        /// <param name="sender">The button that fired this event.</param>
        /// <param name="e">The System.Windows.RoutedEventArgs data used for this event.</param>
        private void OnContinuePressed(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        /// <summary>
        /// Event handler for whenever the universal log control's data grid selection changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnDataGridSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (SelectedItemsChanged != null)
            {
                SelectedItemsChanged(sender, new SelectionItemsChangedEventArgs(PrimaryViewer.DataGrid.SelectedItems.Cast<UniversalLogMessageViewModel>().ToArray()));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void OnContentRendered(EventArgs e)
        {
            base.OnContentRendered(e);
            this.PrimaryViewer.ScrollToBottom();
        }
        #endregion // Methods
    } // UniversalLogWindow
}
