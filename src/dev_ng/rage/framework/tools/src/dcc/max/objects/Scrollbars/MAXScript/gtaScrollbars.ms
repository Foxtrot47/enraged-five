-- Gta Scrollbars 2D Effect Scripted Plugin
-- Provides a representation for Scrollbar (aka Times Sq., Vegas neon signs) to
-- place in maps.
--
-- by David Muir, Rockstar North
--

plugin Helper gtaScrollbars
name:"Gta Scrollbars" 
classID:#(0x380f3416, 0x220e0bed)
category:"Gta" 
extends:dummy
version:1
(
	local scrollbarTypes = #("Financial", "Theatre")
	local lastType
	
	parameters pblock rollout:params
	(
		-- Note: ui parameters specify rollout UI element that controls
		-- that parameter.  Saves adding trivial event handlers for UI controls.
		
		-- Scrollbar Type (e.g. Financial, Theatre etc.)
		scrollbarType type:#integer animatable:false default:1 ui:lstType
		
		-- Scrollbar Number of Points
		scrollbarNumPoints type:#integer animatable:false default:8 ui:spnNumPoints
		
		-- Scrollbar Height (downwards)
		scrollbarHeight type:#worldUnits animatable:false default:1.0 ui:spnHeight
		
		-- Scrollbar Point Node Subobjects (array of nodes)
		scrollbarPoints type:#nodeTab animatable:false tabSizeVariable:true
	)

	rollout params "Gta Scrollbar Parameters"
	(
		------------------------------------------------------------------------
		-- Rollout UI
		------------------------------------------------------------------------
		
		dropdownlist lstType "Type:" items:scrollbarTypes
		spinner spnNumPoints "# of Points:" range:[2,100,8] type:#integer
		spinner spnHeight "Height:" range:[0.0,1000.0,1.0] type:#worldUnits scale:0.1
		
		------------------------------------------------------------------------
		-- Event Handlers
		------------------------------------------------------------------------
		
		-- Rollout Startup Event
		on params open do 
		(
			lstType.selection = scrollbarType
			spnNumPoints.value = scrollbarNumPoints
			spnHeight.value = scrollbarHeight
		)
	)

	-- Post-creation of a Scrollbar object
	--   Here we construct our sub-objects, as our parameter block has now been
	--   initialised we shall have a valid number of points.  Create the dummy 
	--   nodes for those points and calculate their initial positions along a
	--   line, to define a vertical plane.

	
	
		
	-- Gta Scrollbar Rendering
	on getDisplayMesh do 
	(
		meshObj = createInstance box [0.1,0.1,0.1] mapCoords:false 
	
		meshObj.mesh
	)
	
	-- Mouse Creation Tool
	--   Laying out the initial 8 points using mouse clicks in the XY-plane.
	tool create
	(
		on mousePoint numClick do
		(
			local bCreateSubObjs = false
			
			-- First click defines the initial Gta Scrollbars object
			if ( ( not bCreateSubObjs ) and ( 1 == numClick ) ) then
			(
				nodeTM.translation = gridPoint
				bCreateSubObjs = true
			)
			else
			(
				-- Further mouse clicks should create new objects and
				-- add them as a child.
				subObj = dummy()
				move subObj ( nodeTM.translation + [2,0,0] )
			)
		)
		
		on mouseAbort numClick do
		(
			#stop
		)
	)
)

plugin Helper gtaScrollbarPoint
name:"Gta Scrollbar Point" 
classID:#(0x63f06da5, 0x71051259)
extends:dummy
(
	
)
	
-- End of script
