#include "mods.h"
#include "MeshDLib.h"
#include "uvwworld.h"
#include "iColorMan.h"
#include "MaxIcon.h"
#include "modsres.h"
#include "edmdata.h"
#include "trig.h"

////////////////////////////////////////////////////////////////////////////////////////////
static INT_PTR CALLBACK SelectDlgProc (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

static float planarFaceThresh = 45.0f;
static const int SOFTSEL_EDGEDIST_MIN = 1;
static const int SOFTSEL_EDGEDIST_MAX = 999;

static BOOL oldShowEnd;
	
static GenSubObjType SOT_Vertex(1);
static GenSubObjType SOT_Edge(2);
static GenSubObjType SOT_Face(3);
static GenSubObjType SOT_Poly(4);
static GenSubObjType SOT_Element(5);

static BOOL updateNumSel = TRUE;

static int butIDs[] = {		0, 
							IDC_SELVERTEX, 
							IDC_SELEDGE, 
							IDC_SELFACE, 
							IDC_SELPOLY, 
							IDC_SELELEMENT };

////////////////////////////////////////////////////////////////////////////////////////////
class MeshSelImageHandler 
////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	HIMAGELIST images;
	MeshSelImageHandler () { images = NULL; }
	~MeshSelImageHandler () { if (images) ImageList_Destroy (images); }
	HIMAGELIST LoadImages ();
};

static MeshSelImageHandler theMeshSelImageHandler;

////////////////////////////////////////////////////////////////////////////////////////////
void ResetEditMeshUI() 
{
	planarFaceThresh = 45.0f;
}

////////////////////////////////////////////////////////////////////////////////////////////
HIMAGELIST MeshSelImageHandler::LoadImages() 
{
	if(images) 
	{
		return images;
	}

	HBITMAP hBitmap, hMask;
	images = ImageList_Create(24, 23, ILC_COLOR|ILC_MASK, 10, 0);
	hBitmap = LoadBitmap (hInstance, MAKEINTRESOURCE(IDB_EM_SELTYPES));
	hMask = LoadBitmap (hInstance, MAKEINTRESOURCE(IDB_EM_SELMASK));
	ImageList_Add (images, hBitmap, hMask);
	DeleteObject(hBitmap);
	DeleteObject(hMask);

	return images;
}

////////////////////////////////////////////////////////////////////////////////////////////
int EditMeshMod::NumSubObjTypes() 
{ 
	return 5;
}

////////////////////////////////////////////////////////////////////////////////////////////
ISubObjType *EditMeshMod::GetSubObjType(int i) 
{
	static bool initialized = false;

	if(!initialized)
	{
		initialized = true;
		SOT_Vertex.SetName(GetString(IDS_RB_VERTEX));
		SOT_Edge.SetName(GetString(IDS_RB_EDGE));
		SOT_Face.SetName(GetString(IDS_RB_FACE));
		SOT_Poly.SetName(GetString(IDS_EM_POLY));
		SOT_Element.SetName(GetString(IDS_EM_ELEMENT));
	}

	switch(i)
	{
	case -1:
		if(GetSubObjectLevel() > 0)
		{
			return GetSubObjType(GetSubObjectLevel()-1);
		}
		break;
	case 0:
		return &SOT_Vertex;
	case 1:
		return &SOT_Edge;
	case 2:
		return &SOT_Face;
	case 3:
		return &SOT_Poly;
	case 4:
		return &SOT_Element;
	}
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::UpdateSurfType() 
{
	return;
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::RefreshSelType () 
{
	ICustToolbar *iToolbar = GetICustToolbar(GetDlgItem(hSel,IDC_EM_SELTYPE));
	ICustButton *but;

	for (int i=1; i<6; i++) 
	{
		but = iToolbar->GetICustButton (butIDs[i]);
		but->SetCheck (selLevel==i);
		ReleaseICustButton (but);
	}

	ReleaseICustToolbar(iToolbar);
	SetSelDlgEnables ();
	UpdateSurfType ();
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::InvalidateNumberSelected () 
{
	if(!hSel) 
	{
		return;
	}

	InvalidateRect (hSel, NULL, FALSE);
	updateNumSel = TRUE;
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::SetNumSelLabel() 
{
	static TSTR buf;

	if(!hSel) 
	{
		return;
	}

	if(!updateNumSel) 
	{
		SetDlgItemText(hSel,IDC_EM_NUMBER_SEL,buf);
		return;
	}

	updateNumSel = FALSE;

	if(selLevel == SL_OBJECT) 
	{
		buf.printf (GetString (IDS_EM_OBJECT_SEL));
		SetDlgItemText(hSel,IDC_EM_NUMBER_SEL,buf);
		return;
	}

	ModContextList mcList;	
	INodeTab nodes;	
	ip->GetModContexts(mcList,nodes);
	int i, num=0, where=0, thissel;

	for (i=0; i<mcList.Count(); i++) 
	{
		EditMeshData *meshData = (EditMeshData*)mcList[i]->localData;

		if(!meshData) 
		{
			continue;
		}

		switch (selLevel) 
		{
		case SL_VERTEX:
			thissel = meshData->mdelta.vsel.NumberSet();
			break;
		case SL_FACE:
		case SL_POLY:
		case SL_ELEMENT:
			thissel = meshData->mdelta.fsel.NumberSet();
			break;
		case SL_EDGE:
			thissel= meshData->mdelta.esel.NumberSet();
			break;
		}

		if(thissel) 
		{
			where = i;
			num += thissel;
		}
	}

	// If we have only one element selected, which one is it?
	int which;

	if (num==1) 
	{
		EditMeshData *meshData = (EditMeshData*) mcList[where]->localData;

		switch (selLevel) 
		{
		case SL_VERTEX:
			for (which=0; which<meshData->mdelta.vsel.GetSize(); which++) 
			{
				if (meshData->mdelta.vsel[which]) 
				{
					break;
				}
			}
			break;
		case SL_FACE:
		case SL_POLY:
		case SL_ELEMENT:
			for(which=0; which<meshData->mdelta.fsel.GetSize(); which++) 
			{
				if (meshData->mdelta.fsel[which]) 
				{
					break;
				}
			}
			break;
		case SL_EDGE:
			for(which=0; which<meshData->mdelta.esel.GetSize(); which++)
			{
				if (meshData->mdelta.esel[which]) 
				{
					break;
				}
			}
			break;
		}
	}

	switch (selLevel) 
	{
	case SL_VERTEX:			
		if(num==1) 
		{
			buf.printf (GetString(IDS_EM_WHICHVERTSEL),which + 1);
		}
		else 
		{
			buf.printf(GetString(IDS_RB_NUMVERTSELP),num);
		}
		break;

	case SL_FACE:
	case SL_POLY:
	case SL_ELEMENT:
		if (num==1) 
		{
			buf.printf (GetString(IDS_EM_WHICHFACESEL), which+1);
		}
		else 
		{
			buf.printf(GetString(IDS_RB_NUMFACESELP),num);
		}
		break;

	case SL_EDGE:
		if (num==1) 
		{
			buf.printf (GetString(IDS_EM_WHICHEDGESEL), which+1);
		}
		else 
		{
			buf.printf(GetString(IDS_RB_NUMEDGESELP),num);
		}
		break;
	}

	SetDlgItemText(hSel, IDC_EM_NUMBER_SEL, buf);
}

////////////////////////////////////////////////////////////////////////////////////////////
float EditMeshMod::GetPolyFaceThresh() 
{
	return DegToRad(planarFaceThresh);
}

////////////////////////////////////////////////////////////////////////////////////////////
// --- Begin/End Edit Params ---------------------------------
////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::BeginEditParams( IObjParam *ip, ULONG flags,Animatable *prev ) 
{
	if(!GetSystemSetting(SYSSET_ENABLE_EDITMESHMOD)) 
	{
		return;
	}

	this->ip = ip;
	CreateMeshDataTempData();

	if(!hSel) 
	{
		hSel = ip->AddRollupPage (	hInstance, 
									MAKEINTRESOURCE(IDD_EM_SELECT),
									SelectDlgProc, 
									GetString (IDS_EM_SELECTION), 
									(LPARAM)this, 
									rsSel ? 0 : APPENDROLL_CLOSED);

		InvalidateNumberSelected();
	} 
	else 
	{
		SetWindowLongPtr(hSel,GWLP_USERDATA,(LONG_PTR)this);
	}

	SetFlag(EM_EDITING);

	// Create sub object editing modes.
	moveMode       = new MoveModBoxCMode(this,ip);
	rotMode        = new RotateModBoxCMode(this,ip);
	uscaleMode     = new UScaleModBoxCMode(this,ip);
	nuscaleMode    = new NUScaleModBoxCMode(this,ip);
	squashMode     = new SquashModBoxCMode(this,ip);
	selectMode     = new SelectModBoxCMode(this,ip);

	// Restore the selection level.
	ip->SetSubObjectLevel(selLevel);

	// Set show end result.
	oldShowEnd = ip->GetShowEndResult();
	ip->SetShowEndResult (GetFlag (EM_DISP_RESULT));

	TimeValue t = ip->GetTime();
	NotifyDependents(Interval(t,t), PART_ALL, REFMSG_BEGIN_EDIT);
	NotifyDependents(Interval(t,t), PART_ALL, REFMSG_MOD_DISPLAY_ON);
	SetAFlag(A_MOD_BEING_EDITED);	
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::EndEditParams( IObjParam *ip, ULONG flags,Animatable *next ) 
{
	if(!GetSystemSetting(SYSSET_ENABLE_EDITMESHMOD)) 
	{
		return;
	}

	if (flags&END_EDIT_REMOVEUI ) 
	{
		if (hSel) 
		{
			rsSel = IsRollupPanelOpen (hSel);
			ip->DeleteRollupPage (hSel);
			hSel = NULL;
		}
	} 
	else 
	{
		SetWindowLongPtr (hSel, GWLP_USERDATA, 0);
	}

	TimeValue t = ip->GetTime();
	NotifyDependents(Interval(t,t), PART_ALL, REFMSG_END_EDIT);
	NotifyDependents(Interval(t,t), PART_ALL, REFMSG_MOD_DISPLAY_OFF);
	ClearAFlag(A_MOD_BEING_EDITED);
	
	DeleteMeshDataTempData();

	ExitAllCommandModes();

	if( moveMode ) 
	{
		delete moveMode;
		moveMode = NULL;
	}
	if( rotMode ) 
	{
		delete rotMode;
		rotMode = NULL;
	}
	if( uscaleMode ) 
	{
		delete uscaleMode;
		uscaleMode = NULL;
	}
	if( nuscaleMode ) 
	{
		delete nuscaleMode;
		nuscaleMode = NULL;
	}
	if( squashMode ) 
	{
		delete squashMode;
		squashMode = NULL;
	}
	if( selectMode ) 
	{
		delete selectMode;
		selectMode = NULL;
	}

	this->ip = NULL;
	ClearFlag (EM_EDITING);

	// Reset show end result
	SetFlag (EM_DISP_RESULT, ip->GetShowEndResult());
	ip->SetShowEndResult(oldShowEnd);
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::ExitAllCommandModes (bool exSlice, bool exStandardModes) 
{
	if (exStandardModes) 
	{
		ip->DeleteMode(moveMode);
		ip->DeleteMode(rotMode);
		ip->DeleteMode(uscaleMode);
		ip->DeleteMode(nuscaleMode);
		ip->DeleteMode(squashMode);
		ip->DeleteMode(selectMode);
	}

	ip->ClearPickMode();
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::SetSelDlgEnables() 
{
	if(!hSel)
	{
		return;
	}

	BOOL fac = (selLevel >= SL_FACE);
	BOOL poly = (selLevel == SL_POLY);
	BOOL edg = (selLevel == SL_EDGE);
	BOOL obj = (selLevel == SL_OBJECT);
	BOOL vtx = (selLevel == SL_VERTEX);

	EnableWindow (GetDlgItem (hSel, IDC_EM_SEL_BYVERT), fac||edg);
	EnableWindow (GetDlgItem (hSel, IDC_EM_IGNORE_BACKFACES), !obj);
	EnableWindow (GetDlgItem (hSel,IDC_EM_IGNORE_VISEDGE), poly);
	EnableWindow (GetDlgItem (hSel, IDC_EM_SEL_PT_TEXT), poly);
	ISpinnerControl *spin = GetISpinner(GetDlgItem(hSel,IDC_EM_PLANARSPINNER));
	spin->Enable(poly);
	ReleaseISpinner(spin);

	EnableWindow (GetDlgItem (hSel, IDC_EM_NORMAL_SHOW), vtx||fac);
}

////////////////////////////////////////////////////////////////////////////////////////////
static INT_PTR CALLBACK SelectDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) 
{
	EditMeshMod *em = (EditMeshMod *) GetWindowLongPtr (hWnd, GWLP_USERDATA);
	ICustToolbar *iToolbar;
	ISpinnerControl *spin;

	switch(msg) 
	{
	case WM_INITDIALOG:
		em = (EditMeshMod*) lParam;
		em->hSel = hWnd;
		SetWindowLongPtr(hWnd,GWLP_USERDATA,lParam);			

		spin = GetISpinner(GetDlgItem(hWnd,IDC_EM_PLANARSPINNER));
		spin->SetLimits(0, 180, FALSE);
		spin->SetScale(0.1f);
		spin->LinkToEdit(GetDlgItem(hWnd,IDC_EM_PLANAR), EDITTYPE_FLOAT);
		spin->SetValue (planarFaceThresh, FALSE);
		ReleaseISpinner(spin);

		iToolbar = GetICustToolbar(GetDlgItem(hWnd,IDC_EM_SELTYPE));
		iToolbar->SetImage (theMeshSelImageHandler.LoadImages());
		iToolbar->AddTool(ToolButtonItem(CTB_CHECKBUTTON,0,5,0,5,24,23,24,23,IDC_SELVERTEX));
		iToolbar->AddTool(ToolButtonItem(CTB_CHECKBUTTON,1,6,1,6,24,23,24,23,IDC_SELEDGE));
		iToolbar->AddTool(ToolButtonItem(CTB_CHECKBUTTON,2,7,2,7,24,23,24,23,IDC_SELFACE));
		iToolbar->AddTool(ToolButtonItem(CTB_CHECKBUTTON,3,8,3,8,24,23,24,23,IDC_SELPOLY));
		iToolbar->AddTool(ToolButtonItem(CTB_CHECKBUTTON,4,9,4,9,24,23,24,23,IDC_SELELEMENT));
		ReleaseICustToolbar(iToolbar);
		em->RefreshSelType();

		CheckDlgButton (hWnd, IDC_EM_SEL_BYVERT, em->selByVert);
		CheckDlgButton (hWnd, IDC_EM_IGNORE_BACKFACES, em->ignoreBackfaces);
		CheckDlgButton (hWnd, IDC_EM_IGNORE_VISEDGE, em->ignoreVisEdge);
		break;

	case WM_DESTROY:
		break;

	case WM_COMMAND:
		switch (LOWORD(wParam)) 
		{
		case IDC_SELVERTEX:
			if (em->selLevel == SL_VERTEX) 
			{
				em->ip->SetSubObjectLevel (SL_OBJECT);
			}
			else 
			{
				em->ip->SetSubObjectLevel (SL_VERTEX);
			}
			break;

		case IDC_SELEDGE:
			if (em->selLevel == SL_EDGE) 
			{
				em->ip->SetSubObjectLevel (SL_OBJECT);
			}
			else 
			{
				em->ip->SetSubObjectLevel (SL_EDGE);
			}
			break;

		case IDC_SELFACE:
			if (em->selLevel == SL_FACE) 
			{
				em->ip->SetSubObjectLevel (SL_OBJECT);
			}
			else 
			{
				em->ip->SetSubObjectLevel (SL_FACE);
			}
			break;

		case IDC_SELPOLY:
			if (em->selLevel == SL_POLY) 
			{
				em->ip->SetSubObjectLevel (SL_OBJECT);
			}
			else 
			{
				em->ip->SetSubObjectLevel (SL_POLY);
			}
			break;

		case IDC_SELELEMENT:
			if (em->selLevel == SL_ELEMENT) 
			{
				em->ip->SetSubObjectLevel (SL_OBJECT);
			}
			else 
			{
				em->ip->SetSubObjectLevel (SL_ELEMENT);
			}
			break;

		case IDC_EM_SEL_BYVERT:
			em->selByVert = IsDlgButtonChecked(hWnd,IDC_EM_SEL_BYVERT);
			break;

		case IDC_EM_IGNORE_BACKFACES:
			em->ignoreBackfaces = IsDlgButtonChecked(hWnd,IDC_EM_IGNORE_BACKFACES);
			break;

		case IDC_EM_IGNORE_VISEDGE:
			em->ignoreVisEdge = IsDlgButtonChecked(hWnd,IDC_EM_IGNORE_VISEDGE);
			break;
		}
		break;

	case WM_PAINT:
		if (updateNumSel) 
		{
			em->SetNumSelLabel();
		}
		return FALSE;

	case WM_NOTIFY:
		if (((LPNMHDR)lParam)->code != TTN_NEEDTEXT) break;
		LPTOOLTIPTEXT lpttt;
		lpttt = (LPTOOLTIPTEXT)lParam;				
		switch (lpttt->hdr.idFrom)
		{
		case IDC_SELVERTEX:
			lpttt->lpszText = GetString (IDS_RB_VERTEX);
			break;
		case IDC_SELEDGE:
			lpttt->lpszText = GetString (IDS_RB_EDGE);
			break;
		case IDC_SELFACE:
			lpttt->lpszText = GetString(IDS_RB_FACE);
			break;
		case IDC_SELPOLY:
			lpttt->lpszText = GetString(IDS_EM_POLY);
			break;
		case IDC_SELELEMENT:
			lpttt->lpszText = GetString(IDS_EM_ELEMENT);
			break;
		}
		break;

	default:
		return FALSE;
	}

	return TRUE;
}
