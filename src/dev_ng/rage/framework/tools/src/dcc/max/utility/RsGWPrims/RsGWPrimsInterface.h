// interface ID
#define RSGWPRIMS_INTERFACE Interface_ID(0x544a9b3a, 0x27403d59)
#define GetRsGWPrimsInterface(obj) \
	((RsGWPrimsInterface*)obj->GetPInterface(RSGWPRIMS_INTERFACE))

// function IDs
enum {	
	prims_setOrigin, 
	prims_setResolution,
	prims_setColour,
	prims_circle, 
	prims_sphere,
	prims_cone,
	prims_cylinder,
	prims_capsule,
	prims_line,
	prims_clear,
	prims_draw
};

// mixin interface
class RsGWPrimsInterface : public FPMixinInterface
{
	BEGIN_FUNCTION_MAP
		VFN_1(prims_setOrigin, SetOrigin, TYPE_MATRIX3);
		VFN_1(prims_setResolution, SetResolution, TYPE_INT);
		VFN_1(prims_setColour, SetColour, TYPE_COLOR_BR);
		VFN_2(prims_circle, AddCircle, TYPE_FLOAT, TYPE_MATRIX3);
		VFN_2(prims_sphere, AddSphere, TYPE_FLOAT, TYPE_MATRIX3);
		VFN_4(prims_cone, AddCone, TYPE_FLOAT, TYPE_FLOAT, TYPE_BOOL, TYPE_MATRIX3);
		VFN_3(prims_cylinder, AddCylinder, TYPE_FLOAT, TYPE_FLOAT, TYPE_MATRIX3);
		VFN_4(prims_capsule, AddCapsule, TYPE_FLOAT, TYPE_FLOAT, TYPE_BOOL, TYPE_MATRIX3);
		VFN_2(prims_line, AddLine, TYPE_POINT3_TAB_BR, TYPE_MATRIX3);
		VFN_0(prims_clear, Clear);
		VFN_1(prims_draw, Draw, TYPE_BOOL);
	END_FUNCTION_MAP

	virtual void SetOrigin(Matrix3 origin, bool setNow=true)=0;
	virtual void SetResolution(int resolution)=0;
	virtual void SetColour(Color colour)=0;
	virtual void AddCircle(float radius, Matrix3 localTrans)=0;
	virtual void AddSphere(float radius, Matrix3 localTrans)=0;
	virtual void AddCone(float length, float radius, BOOL cap, Matrix3 localTrans)=0;
	virtual void AddCylinder(float length, float radius, Matrix3 localTrans)=0;
	virtual void AddCapsule(float length, float radius, BOOL cap, Matrix3 localTrans)=0;
	virtual void AddLine(Tab<Point3*> points, Matrix3 localTrans)=0;
	virtual void Clear()=0;
	virtual void Draw(BOOL flush=true)=0;

	FPInterfaceDesc* GetDesc();
};

// Static interface
// class RsGWPrimsImp : public FPStaticInterface
// {
// public:
// 	DECLARE_DESCRIPTOR(RsGWPrimsImp)
// 	BEGIN_FUNCTION_MAP
// 		VFN_1(prims_setOrigin, SetOrigin, TYPE_MATRIX3);
// 		VFN_1(prims_setResolution, SetResolution, TYPE_INT);
// 		VFN_1(prims_circle, AddCircle, TYPE_FLOAT);
// 		VFN_1(prims_sphere, AddSphere, TYPE_FLOAT);
// 		VFN_3(prims_cone, AddCone, TYPE_FLOAT, TYPE_FLOAT, TYPE_BOOL);
// 		VFN_2(prims_cylinder, AddCylinder, TYPE_FLOAT, TYPE_FLOAT);
// 		VFN_0(prims_clear, Clears);
// 		VFN_1(prims_draw, Draw, TYPE_BOOL);
// 	END_FUNCTION_MAP
// 
// 	void SetOrigin(Matrix3 origin);
// 	void SetResolution(int resolution);
// 	void AddCircle(float radius);
// 	void AddSphere(float radius);
// 	void AddCone(float length, float radius, bool cap);
// 	void AddCylinder(float length, float radius);
// 	void Clear();
// 	void Draw(BOOL flush=true);
// };
