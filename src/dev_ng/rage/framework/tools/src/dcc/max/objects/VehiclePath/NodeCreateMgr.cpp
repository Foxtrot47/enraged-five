//
//
//    Filename: ConnectionCreateMgr.cpp
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: 
//
//
#pragma warning (disable : 4251)

#include "IMaxDataStore.h"
#include "VehicleNode.h"
#include "VehLink.h"
#include "NodeCreateMgr.h"
#include "DataInstance.h"

#define MAX_NUM_NODES_AUTO_CREATE	(100)

VehicleNodeCreateMode theVehicleNodeCreateMode;

HWND VehicleNodeCreateMgr::m_hWndAutoCreate=NULL;

VehicleNode*		VehicleNodeCreateMgr::m_pDefaultVehicleNode;
VehicleLink*	VehicleNodeCreateMgr::m_pDefaultVehicleLink=NULL;
bool			VehicleNodeCreateMgr::m_bAutoCreate=false;
float			VehicleNodeCreateMgr::m_fAutoCreateDistance=200.0f;

bool			VehicleNodeCreateMgr::m_bViewDifferent=false;

#if !defined( _WIN64 )
BOOL CALLBACK EditAutoCreateProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
#else
INT_PTR CALLBACK EditAutoCreateProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////
//
//        name: VehicleNodeCreateMgr::VehicleNodeCreateMgr
// description: Constructor
//
VehicleNodeCreateMgr::VehicleNodeCreateMgr() : 
	m_pThis(NULL),
	m_pCanConnectTo(NULL),
	m_pPrev(NULL),
	m_pVehicleNode(NULL),
	m_ip(NULL),
	m_creating(false)
{
}

//
//        name: VehicleNodeCreateMgr::~VehicleNodeCreateMgr
// description: Destructor
//
VehicleNodeCreateMgr::~VehicleNodeCreateMgr()
{
}

//
//        name: VehicleNodeCreateMgr::Begin
// description: Called at beginning of creation
//
void VehicleNodeCreateMgr::Begin( IObjCreate *ioc, ClassDesc *desc )
{
	m_ip = ioc;
	m_pPrev = NULL;
	m_pCanConnectTo = NULL;
	// make sure VehicleNode create manager isn't pointing to anything
	DeleteReference(0);

	if(m_hWndAutoCreate == NULL)
	{
		InitUI();
	}

	CreateVehicleNode(m_ip, m_pDefaultVehicleNode);
	CreateVehicleLink(m_ip, m_pDefaultVehicleLink);
}

void VehicleNodeCreateMgr::End()
{
	m_pPrev = NULL;
	m_pCanConnectTo = NULL;

	CloseUI();
}

void VehicleNodeCreateMgr::InitUI()
{
	if(m_hWndAutoCreate == NULL)
	{
		LPTSTR lpStr = MAKEINTRESOURCE(IDD_AUTO_CREATE_PARAMS_PANEL);

		m_hWndAutoCreate = m_ip->AddRollupPage(
			hInstance, 
			MAKEINTRESOURCE(IDD_AUTO_CREATE_PARAMS_PANEL),
			EditAutoCreateProc,
			_T("AutoCreate"),
			(LPARAM)this);

	m_pSpinLinkLen	= SetupIntSpinner(m_hWndAutoCreate, IDC_CREATE_DIST_SPIN, IDC_CREATE_DIST_EDIT, MIN_LINK_LEN, MAX_LINK_LEN, DEF_LINK_LEN);
	m_pSpinLinkLen->SetValue(m_fAutoCreateDistance, FALSE);
	m_pSpinLinkLen->SetScale(20.0f);
	
	SendMessage(GetDlgItem(VehicleNodeCreateMgr::m_hWndAutoCreate, IDC_CHECK_VIEW), 
				(UINT) BM_SETCHECK,
				VehicleNodeCreateMgr::m_bViewDifferent,
				0
				);  
	}

}


void VehicleNodeCreateMgr::CloseUI()
{
	if(m_hWndAutoCreate )
	{
		ReleaseISpinner(m_pSpinLinkLen);
		m_ip->DeleteRollupPage(m_hWndAutoCreate);
		m_hWndAutoCreate = NULL;
	}
}

//
//        name: VehicleNodeCreateMgr::Filter
// description: Return if previous node can connect to this node
//
BOOL VehicleNodeCreateMgr::Filter(INode *pNode)
{
	if(m_pPrev == NULL)
		return FALSE;

	if(pNode && pNode->GetObjectRef() != m_pPrev)
	{
		Object* pObj = pNode -> EvalWorldState(0).obj;
		if (pObj && pObj->ClassID() == VEHICLE_NODE_CLASS_ID)
		{
#ifdef ONLY_LINEAR_PATHS	
			if(((VehicleConnection *)pObj)->GetNumberOfLinks() < 2 &&
				m_pPrev->GetLinkBetween((VehicleNode* )pObj) == NULL)
				return TRUE;
#else
			if(m_pPrev->GetLinkBetween((VehicleNode* ) pObj) == NULL)
				return TRUE;
#endif
		}
	}
	return FALSE;

}
	
// ReferenceMaker
int VehicleNodeCreateMgr::NumRefs()
{
	return 1;
}
RefTargetHandle VehicleNodeCreateMgr::GetReference(int i)
{
	switch(i)
	{
	case 0:
		return m_pThis;
	}
	assert(0);
	return NULL;
}
void VehicleNodeCreateMgr::SetReference(int i, RefTargetHandle rtarg)
{
	switch(i)
	{
	case 0:
		m_pThis = (INode *)rtarg;
		if(m_pThis)
			m_pVehicleNode= (VehicleNode* )m_pThis->GetObjectRef();
		else
			m_pVehicleNode= NULL;
		break;
	default:
		assert(0);
	}
}

RefResult VehicleNodeCreateMgr::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,  RefMessage message)
{
	switch(message)
	{
	case REFMSG_TARGET_DELETED:
		if(m_pVehicleNode)
		{
			m_pVehicleNode->EndEditParams(m_ip, END_EDIT_REMOVEUI, NULL);
			m_pVehicleNode= NULL;
		}
		m_pThis = NULL;
		break;
	}
	return REF_SUCCEED;
}

// MouseCallback
int VehicleNodeCreateMgr::proc(HWND hWnd, int msg, int point, int flags, IPoint2 sp)
{
	TimeValue t = m_ip->GetTime();
	int res = TRUE;
	ViewExp *pVpt = m_ip->GetViewport(hWnd);

	switch(msg)
	{
	case MOUSE_FREEMOVE:
		pVpt->TrackImplicitGrid(sp);
		break;

	case MOUSE_MOVE:
	{
		if(m_pThis != NULL)
		{
			Matrix3 mat(1);
			mat.SetTrans(pVpt->SnapPoint(sp,sp,NULL,SNAP_IN_3D));
			m_ip->SetNodeTMRelConstPlane(m_pThis, mat);
		}
		m_ip->RedrawViews(t,REDRAW_NORMAL,m_pThis);  
	}
	break;
	case MOUSE_POINT:
	{	// if there isn't any node created already then create one
		if(m_pThis == NULL)
		{
			if ( m_ip->SetActiveViewport(hWnd) )
			{
				res = FALSE;
				break;
			}
			// check valid construction plane
			if (m_ip->IsCPEdgeOnInView())
			{ 
				res = FALSE;
				break;
			}

			pVpt->CommitImplicitGrid(sp, flags);

			// Create matrix for node position
			Matrix3 mat(1);

			mat.SetTrans(pVpt->SnapPoint(sp,sp,NULL,SNAP_IN_3D));

			theHold.Begin();

			INode* pINode = NULL;
			CreateVehicleNodeDefault(m_ip, m_pVehicleNode, pINode, &mat);

			// reference node, so that we are informed when it is destroyed
			ReplaceReference(0, pINode);


			// begin edit parameters
			m_pVehicleNode->BeginEditParams(m_ip, BEGIN_EDIT_CREATE, NULL);

			m_ip->RedrawViews(t,REDRAW_BEGIN,m_pThis); 
		}
		else
		{
			// move node about with the mouse
			Matrix3 mat(1);
			mat.SetTrans(pVpt->SnapPoint(sp,sp,NULL,SNAP_IN_3D));
			m_ip->SetNodeTMRelConstPlane(m_pThis, mat);
			// translate up 1 metre
//			mat = m_pThis->GetNodeTM(0);
//			mat.Translate(Point3(0.0f,0.0f,1.0f));
//			m_pThis->SetNodeTM(0, mat);

			// start creating a new node
			// if connecting to a VehicleNode already placed down
			if(m_pCanConnectTo)
			{
				// remove link between current node and previous node and replace it with a link
				// between the previous node and the node the mouse has moved over 'm_pCanConnectTo' and
				// a link between the current node and 'm_pCanConnectTo'
				// here  it is SHAUN
				RemoveLink();
//				m_pPrev->ConnectTo(m_pCanConnectTo);
				ConnectTo(m_pPrev, m_pCanConnectTo);
				AutoCreate(m_ip, m_pPrev, (VehicleNode *)m_pCanConnectTo->GetObjectRef());
				m_pPrev->UpdateAllLinks(m_ip);
				m_pPrev = (VehicleNode *)m_pCanConnectTo->GetObjectRef();


				res = FALSE;
				StopCreating(pVpt);

			}
			// otherwise create a new VehicleNode and link it to the current node
			else
			{
				// store old Vehicle Node
				if(NULL != m_pPrev)
				{
					m_pPrev->UpdateAllLinks(m_ip);
					AutoCreate(m_ip, m_pPrev, m_pVehicleNode);
				}
				m_pPrev = m_pVehicleNode;

				VehicleNode*	pVehicleNode	= NULL;
				INode*			pINode			= NULL;
				CreateVehicleNodeDefault(m_ip, pVehicleNode, pINode, &mat);
				// connect old connection to the new connection
//				m_pVehicleNode->ConnectTo(pINode);
				ConnectTo(m_pVehicleNode, pINode);
				m_pVehicleNode->EndEditParams(m_ip, END_EDIT_REMOVEUI, NULL);

				// reference new connection
				ReplaceReference(0, pINode);
				m_pVehicleNode = pVehicleNode;
				m_pVehicleNode->BeginEditParams(m_ip, BEGIN_EDIT_CREATE, NULL);
			}
		}
		m_ip->RedrawViews(t,REDRAW_NORMAL,m_pThis);  
	}
	break;
	case MOUSE_ABORT:
		res = FALSE;
		StopCreating(pVpt);
		break;

    case MOUSE_PROPCLICK:
		// right click while between creations
		m_ip->RemoveMode(NULL);
		break;
	}


	// if have moved over a Vehicle Connection that current connection can connect to 
	m_pCanConnectTo = m_ip->PickNode(hWnd, sp, this);
	if(m_pCanConnectTo)
		SetCursor(LoadCursor(hInstance, MAKEINTRESOURCE(IDC_CONNECT_CURSOR1)));
	else
		SetCursor(m_ip->GetSysCursor(SYSCUR_DEFARROW));

	m_ip->ReleaseViewport(pVpt);

	return res;
}

void VehicleNodeCreateMgr::CreateVehicleNode(IObjCreate* ip, VehicleNode*& pVehicleNode)
{
	pVehicleNode = (VehicleNode *)ip->CreateInstance(HELPER_CLASS_ID, VEHICLE_NODE_CLASS_ID);
}

void VehicleNodeCreateMgr::CreateVehicleNodeDefault(IObjCreate* ip, VehicleNode*& pVehicleNode, INode*& pINode, Matrix3* pMat, bool bCopyDefaultParams/*=true*/)
{
	pVehicleNode = (VehicleNode *)ip->CreateInstance(HELPER_CLASS_ID, VEHICLE_NODE_CLASS_ID);
	pINode = ip->CreateObjectNode(pVehicleNode);

	// set node position
	if(NULL != pMat)
	{
		ip->SetNodeTMRelConstPlane(pINode, *pMat);
	}
}

void VehicleNodeCreateMgr::CreateVehicleLink(IObjCreate* ip, VehicleLink*& pVehicleLink)
{
	pVehicleLink = (VehicleLink *)ip->CreateInstance(HELPER_CLASS_ID, VEHICLE_LINK_CLASS_ID);
}

void VehicleNodeCreateMgr::CreateVehicleLinkDefault(IObjCreate* ip, VehicleLink*& pVehicleLink, INode*& pINode, bool bCopyDefaultParams/*=true*/)
{
	pVehicleLink = (VehicleLink *)ip->CreateInstance(HELPER_CLASS_ID, VEHICLE_LINK_CLASS_ID);
	pINode = ip->CreateObjectNode(pVehicleLink);
}

void VehicleNodeCreateMgr::RemoveLink()
{
	VehicleLink* pLink = m_pVehicleNode->GetLinkBetween(m_pPrev);
	assert(pLink);

	m_pVehicleNode->RemoveReference(pLink);
}

void VehicleNodeCreateMgr::RemoveVehicleNode(INode* pNode)
{
	pNode->Delete(m_ip->GetTime(), TRUE);
}

void VehicleNodeCreateMgr::AutoCreate(Interface* ip, VehicleNode* pNode1, VehicleNode* pNode2)
{
	if(false == m_bAutoCreate)
		return;

	VehicleLink* pLink = pNode1->GetLinkBetween(pNode2);
	assert(pLink);
	m_fAutoCreateDistance = m_pSpinLinkLen->GetFVal();
	float fAutoCreateDist = m_fAutoCreateDistance;
	float fLinkLen = pLink->GetLength(ip);
	if((fLinkLen / m_fAutoCreateDistance) > MAX_NUM_NODES_AUTO_CREATE)
	{
		fAutoCreateDist =  fLinkLen / MAX_NUM_NODES_AUTO_CREATE;
	}
	VehicleNode* pNode = pLink->AutoCreate(ip, fAutoCreateDist);
	if(NULL != pNode)
	{
		m_pVehicleNode = pNode;
	}
}

void VehicleNodeCreateMgr::StopCreating(ViewExp *pVpt)
{
	// if node created stop editing connection and delete the node
	pVpt->ReleaseImplicitGrid();
	if(m_pThis)
	{
		// If we have a previous node
		assert(m_pVehicleNode);

		VehicleLink* pLink = m_pVehicleNode->GetLinkBetween(m_pPrev);
		if(pLink)
		{
			m_pVehicleNode->RemoveReference(pLink);
		}
		if(m_pThis)
		{
			RemoveVehicleNode(m_pThis);
		}
	}
	if(m_pPrev)
	{
		theHold.Begin();
		m_ip->SelectNode(m_pPrev->GetINode());
		theHold.Accept("Select");
	}
	m_pPrev = NULL;
	
	theHold.Accept("Create VehicleConnection");
	m_ip->RedrawViews(m_ip->GetTime(), REDRAW_END,m_pThis);  
}

//
//        name: ConnectTo
// description: Connect a VehicleNode to another INode
//

VehicleLink* VehicleNodeCreateMgr::ConnectTo(VehicleNode* pNodeToConnect, INode *pINodeTarget)
{
	VehicleNode *pNodeTarget = (VehicleNode *)pINodeTarget->EvalWorldState(0).obj;

	assert(pNodeTarget->ClassID() == VEHICLE_NODE_CLASS_ID);

	if(pNodeToConnect->GetLinkBetween(pNodeTarget) != NULL)
		return NULL;

	INode *pINodeToConnect = pNodeToConnect->GetINode();

	theHold.Begin();

	VehicleLink *pLink = NULL; //(VehicleLink *)m_ip->CreateInstance(HELPER_CLASS_ID, VEHICLE_LINK_CLASS_ID);
	INode *pLinkNode = NULL; //m_ip->CreateObjectNode(pLink);
	CreateVehicleLinkDefault(m_ip, pLink, pLinkNode);

	pLink->ReplaceReference(0, pINodeToConnect->GetTMController());
	pLink->ReplaceReference(1, pINodeTarget->GetTMController());
	pNodeToConnect->AddReference(pLink);
	pNodeTarget->AddReference(pLink);

	NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
	pNodeTarget->NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);

	theHold.Accept("Link VehicleNode");

	m_ip->RedrawViews(m_ip->GetTime());


	return pLink;
}


// --- Windows callback ------------------------------------------------------------------------------------
#if !defined( _WIN64 )
BOOL CALLBACK EditAutoCreateProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
#else
INT_PTR CALLBACK EditAutoCreateProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
#endif
{
	switch(message)
	{
	case WM_INITDIALOG:
		break;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDC_CHECK_AUTOCREATE:
			VehicleNodeCreateMgr::m_bAutoCreate = (bool) SendMessage(
				GetDlgItem(VehicleNodeCreateMgr::m_hWndAutoCreate, IDC_CHECK_AUTOCREATE), // handle to destination control     
				(UINT) BM_GETCHECK,      // message ID     
				0,      // = 0; not used, must be zero    
				0		// = 0; not used, must be zero 
				);  
			break;
		case IDC_CHECK_VIEW:
			VehicleNodeCreateMgr::m_bViewDifferent = (bool) SendMessage(
				GetDlgItem(VehicleNodeCreateMgr::m_hWndAutoCreate, IDC_CHECK_VIEW), // handle to destination control     
				(UINT) BM_GETCHECK,      // message ID     
				0,      // = 0; not used, must be zero    
				0		// = 0; not used, must be zero 
				);  
			GetCOREInterface()->RedrawViews(GetCOREInterface()->GetTime());
			break;
		default:
			break;
		}
		break;

	case WM_CLOSE:
		break;

	default:
		return FALSE;
	}
	return TRUE;

}

