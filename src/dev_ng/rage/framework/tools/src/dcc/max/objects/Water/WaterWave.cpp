#include "WaterWave.h"

/////////////////////////////////////////////////////////////////////////////////////////////////
class WaterWaveClassDesc : public ClassDesc2 
	/////////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	int 			IsPublic() { return TRUE; }
	void *			Create(BOOL loading = FALSE) { return new WaterWave(); }
	const TCHAR *	ClassName() { return GetString(IDS_WATER_WAVE_NAME); }
	SClass_ID		SuperClassID() { return HELPER_CLASS_ID; }
	Class_ID		ClassID() { return WATER_WAVE_CLASS_ID; }
	const TCHAR* 	Category() { return GetString(IDS_CATEGORY); }

	const TCHAR*	InternalName() { return _T("RSWaterWave"); }
	HINSTANCE		HInstance() { return hInstance; }
};

/////////////////////////////////////////////////////////////////////////////////////////////////
static WaterWaveClassDesc WaterWaveDesc;

static ParamBlockDesc2* waterBaseBlockWave = CreateWaterBasePBlock2(&WaterWaveDesc, "Wave Parameters");

ParamBlockDesc2 waterWavePBlockDesc (
									 water_wave_params, 
									 _T("Water wave Parameters"),
									 IDS_WATER_NAME, 
									 &WaterWaveDesc, 
									 P_AUTO_CONSTRUCT | P_AUTO_UI | P_VERSION, 
									 WATER_WAVE_CURRENT_VERSION,
									 WATER_EXTEND_PBLOCK_REF,
									 IDD_PANEL_WAVE, IDS_EXTENDED_PARAMS, 0, 0, NULL,

										 water_amplitude, _T("Wave Amplitude"), TYPE_WORLD, P_ANIMATABLE, IDS_AMPLITUDE,
										 p_default,		1.0f,
										 p_range, 		0.0f, 1000.0f, 
										 p_ui,			TYPE_SPINNER, EDITTYPE_FLOAT, IDC_AMPLITUDE, IDC_SPIN_AMPLITUDE, 0.1f,
										 end, 

										 water_wave_direction, _T("Wave Dir"), TYPE_WORLD, P_ANIMATABLE, IDS_WAVE_DIRECTION,
										 p_default,		0.0f,
										 p_range, 		-180.0f, 180.0f, 
										 p_ui,			TYPE_SPINNER, EDITTYPE_FLOAT, IDC_WAVE_DIRECTION, IDC_SPIN_WAVE_DIRECTION, 0.1f,
										 end, 
									 end
										);


/////////////////////////////////////////////////////////////////////////////////////////////////
ClassDesc2* GetWaterWaveDesc()
{
	return &WaterWaveDesc;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void WaterWave::BeginEditParams( IObjParam *ip, ULONG flags,Animatable *prev)
{
	Water::BeginEditParams(ip,flags,prev);
	WaterWaveDesc.BeginEditParams(ip,this,flags,prev);
	WaterWaveDesc.SetUserDlgProc(&waterWavePBlockDesc, new WaterDlgProc(this));
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void WaterWave::EndEditParams( IObjParam *ip, ULONG flags,Animatable *next)
{
	WaterWaveDesc.EndEditParams(ip,this,flags,next);
	Water::EndEditParams(ip,flags,next);
}


WaterWave::WaterWave(void)
: Water(&WaterWaveDesc)
{
	WaterWaveDesc.MakeAutoParamBlocks(this);
	mLineColour = Point3(1.0f,0.0f,0.0f);
}

int WaterWave::GetType()
{
	return 0;
}

void WaterWave::BuildSpecificMesh(TimeValue t, Mesh &mesh, Point3 centre, Point3 dim)
{
	float dir, amp;
	pExtendPblock2->GetValue(water_wave_direction,t,dir,FOREVER);
	pExtendPblock2->GetValue(water_amplitude,t,amp,FOREVER);

	// Use the amplitude for the height of the arrow at the centre
	if (amp < 1.0){ amp = 1.0f; }
	
	float smallestDim = dim.x > dim.y ? dim.y : dim.x;
	Point3 centreTopPoint = Point3(centre.x, centre.y, amp);
	// Point along X as 0 degrees
	Point3 arrowPoint = Point3(centre.x + smallestDim, centre.y, 0.0f);

	// Rotate the arrow point around the Z axis depending on the degrees that are selected
	float dirRad = DegToRad(dir);
	Matrix3 pointMat = Matrix3();
	pointMat.SetRotateZ(dirRad);
	Point3 rotatedPoint = pointMat * arrowPoint;

	// Add verts and a face for the arrow to point in which direction the waves will go
	int vertIdx = mesh.getNumVerts();
	int faceIdx = mesh.getNumFaces();
	mesh.setNumVerts( vertIdx + 3, TRUE);
	mesh.setNumFaces( faceIdx + 1, TRUE);

	mesh.setVert(vertIdx, centre);
	mesh.setVert(vertIdx + 1, centreTopPoint);
	mesh.setVert(vertIdx + 2, rotatedPoint);

	mesh.faces[faceIdx].setVerts( 4, 5, 6 );
	mesh.faces[faceIdx].setSmGroup(2);
	mesh.faces[faceIdx].setEdgeVisFlags(EDGE_VIS, EDGE_VIS, EDGE_VIS);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
RefTargetHandle WaterWave::Clone(RemapDir& remap) 
{
	WaterWave* newob = new WaterWave();
	newob->ReplaceReference(0,pBasePblock2->Clone(remap));
	newob->ReplaceReference(1,pExtendPblock2->Clone(remap));

	BaseClone(this, newob, remap);

	return(newob);
}