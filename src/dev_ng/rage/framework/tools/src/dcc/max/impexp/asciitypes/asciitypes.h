//
//
//    Filename: asciitypes.h
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Header file describing types for ASCII output
//
//
//
// These classes are used to load/save ASCII types files output by AsciiFile plugin for 3d Studio Max. To 
//	load the file create an instance of the class Scene and call the function Read. The Scene class is defined 
//	at the bottom of this file.
//
//	std::ifstream fs("MyFile.a3d");
//	Scene scene;
//
//	if(scene.Read(fs) == false)
//	{
//		failed;
//	}
//	else
//	{
//		successful;
//	}
//
// Scene has a collection of access functions to get information about the scene contents: GetRootComponent(), 
//	GetAIConnection() etc	
//
// The Component class is used to describe the model hierarchy and its contents. You can process the hierarchy 
//	using a very simple recursive function. The Component class is defined just above the Scene class
//
//	void ProcessHierarchy(Component* pComp)
//	{
//		Component* pChild = pComp->GetChild(0);		// get first child component
//
//		DoStuff();
//		while(pChild)
//		{
//			ProcessHierarchy(pChild);
//			pChild = pChild->GetSibling();
//		}
//	}
//
// Component also has a collection of access functions to get information about its contents: 
// GetCollisionVolume(), GetCullingSphere() etc.
//
// NB everything is in the namespace 'ascii' to stop name clashes with other code.
//
#ifndef INC_ASCII_TYPES_H_
#define INC_ASCII_TYPES_H_

#include <dma.h>
// STD headers
#pragma warning (disable : 4786)
#include <fstream>
#include <string>
#include <vector>


namespace ascii {


// class prototypes
class Point2d;
class Point3d;
class Point4d;
class BaseObject;
class Component;
class BoundingSphere;
class BoundingBox;
class CullingSphere;
class CollisionVolume;
class CollisionSphere;
class CollisionBox;
class CollisionLine;
class CollisionPlane;
class CollisionMesh;
class AIConnection;
class AILink;
class XRefObject;

//
// --- Base Classes ------------------------------------------------------------------------------------------
//

//
//   Class Name: PtrArray
//  Description: Class holding an array of objects inheriting from BaseObject
//
template<class T>
class PtrArray
{
public:
	~PtrArray() {Clear();}

	int32 GetSize() const {return m_ptrArray.size();}
	void Add(T *pPtr) {m_ptrArray.push_back(pPtr);}
	void Clear()
	{
		for(uint32 i=0; i<m_ptrArray.size(); i++)
			delete m_ptrArray[i];
		m_ptrArray.clear();
	}
	
	// operators
	T* operator[](int32 i) {return m_ptrArray[i];}
	const T* operator[](int32 i) const {return m_ptrArray[i];}

private:
	// arrays of objects
	std::vector<T *> m_ptrArray;
};

//
//   Class Name: BaseObject
//  Description: Base class for everything in the scene
//
class BaseObject
{
public:
	BaseObject(const std::string& name);
	virtual ~BaseObject() {}

	const char *GetName() const;
	void SetName(std::string name);

	// read and write
	virtual bool Read(std::ifstream& is) {return false;}
	virtual bool Write(std::ofstream& os) const {return false;}

	static void WriteString(std::ofstream& os, const char* pToken, const std::string& s);
	static void WriteInteger(std::ofstream& os, const char* pToken, int32 i);
	static void WriteFloat(std::ofstream& os, const char* pToken, float f);
	static void WriteBool(std::ofstream& os, const char* pToken, bool b);
	static void WritePointer(std::ofstream& os, const char* pToken, const BaseObject *pObj);
	static void WritePoint4d(std::ofstream& os, const char* pToken, const Point4d& pt);
	static void WritePoint3d(std::ofstream& os, const char* pToken, const Point3d& pt);
	static void WritePoint2d(std::ofstream& os, const char* pToken, const Point2d& pt);
	static void WriteIntegerArray(std::ofstream& os, const char* pToken, const int32 *pI, int32 size);
	static void WriteFloatArray(std::ofstream& os, const char* pToken, const float *pF, int32 size);
	static void WriteString(std::ofstream& os, const std::string& s);
	static void WriteTab(std::ofstream& os);

	static bool ReadString(std::ifstream& is, std::string& s);
	static bool ReadInteger(std::ifstream& is, int32& i);
	static bool ReadFloat(std::ifstream& is, float& f);
	static bool ReadBool(std::ifstream& is, bool& b);
	static bool ReadPointer(std::ifstream& is, void** pObj);
	static bool ReadPoint4d(std::ifstream& is, Point4d& pt);
	static bool ReadPoint3d(std::ifstream& is, Point3d& pt);
	static bool ReadPoint2d(std::ifstream& is, Point2d& pt);
	static bool ReadIntegerArray(std::ifstream& is, int32 *pI, int32 size);
	static bool ReadFloatArray(std::ifstream& is, float *pF, int32 size);
	static void SkipLineOrBlock(std::ifstream& is);

protected:
	bool StartRead(std::ifstream& is);
	void StartWrite(std::ofstream& os, const char *pClassName) const;
	void EndWrite(std::ofstream& os) const;
	bool StartReadWithName(std::ifstream& is);
	void StartWriteWithName(std::ofstream& os, const char *pClassName) const;

	std::string m_name;
};
//
//   Class Name: Point2d
//  Description: Class describing a 2d point
//
class Point2d
{
public:
	Point2d() {}
	Point2d(float x, float y) {p[0] = x; p[1] = y;}

	float& operator[](int32 i) {return p[i];}
	const float& operator[](int32 i) const {return p[i];}

private:
	float p[2];
};

//
//   Class Name: Point3d
//  Description: Class describing a 3d point
//
class Point3d
{
public:
	Point3d() {}
	Point3d(float x, float y, float z) {p[0] = x; p[1] = y; p[2] = z;}

	float& operator[](int32 i) {return p[i];}
	const float& operator[](int32 i) const {return p[i];}

private:
	float p[3];
};

//
//   Class Name: Point4d
//  Description: Class describing a 4d point
//
class Point4d
{
public:
	Point4d() {}
	Point4d(float w, float x, float y, float z) {p[0] = w; p[1] = x; p[2] = y; p[3] = z;}

	float& operator[](int32 i) {return p[i];}
	const float& operator[](int32 i) const {return p[i];}

private:
	float p[4];
};

//
//   Class Name: Line
//  Description: Class describing a line
//
class Line
{
public:
	virtual bool ReadToken(std::ifstream& is, std::string& token);
	virtual bool WriteTokens(std::ofstream& os) const;

	const Point3d& GetStart() const;
	void SetStart(const Point3d& p);
	const Point3d& GetEnd() const;
	void SetEnd(const Point3d& s);

private:
	Point3d m_start;
	Point3d m_end;
};

//
//   Class Name: Sphere
//  Description: Class describing a sphere with a position
//
class Sphere
{
public:
	virtual bool ReadToken(std::ifstream& is, std::string& token);
	virtual bool WriteTokens(std::ofstream& os) const;

	float GetRadius() const;
	void SetRadius(float r);
	const Point3d& GetPosition() const;
	void SetPosition(const Point3d& p);

protected:
	float m_radius;
	Point3d m_posn;
};

//
//   Class Name: Box
//  Description: Class describing a box
//
class Box
{
public:
	virtual bool ReadToken(std::ifstream& is, std::string& token);
	virtual bool WriteTokens(std::ofstream& os) const;

	const Point3d& GetPosition() const;
	void SetPosition(const Point3d& p);
	const Point3d& GetSize() const;
	void SetSize(const Point3d& s);

private:
	Point3d m_size;
	Point3d m_posn;
};

//
//   Class Name: ObjectOwner
//  Description: Class holding an array of pointers to objects. When this class is destroyed 
//					all the objects are destroyed
//
class ObjectOwner 
{
public:
	virtual bool WriteTokens(std::ofstream& os) const;
	virtual void Clear() {m_objectArray.Clear();}

	void AddObject(BaseObject *pObj) {m_objectArray.Add(pObj);}

private:
	PtrArray<BaseObject> m_objectArray;
};

//
// --- End of Base Classes ------------------------------------------------------------------------------------------
//

//
//   Class Name: ConnectedComponentList
//  Description: Class holding a list of components. This class does not own these components so does not
//					delete them when destroyed
//
class ConnectedComponentList : public BaseObject
{
public:
	ConnectedComponentList() : BaseObject("ConnectedComps") {}

	virtual bool Read(std::ifstream& is);
	virtual bool Write(std::ofstream& os) const;

	void SetSize(int32 size);
	void AddConnectedComponent(Component *pComp);
	void AddConnectedComponent(std::string& name);
	Component* GetConnectedComponent(int32 i);
	int32 GetNumConnectedComponents() const;

private:
	std::vector<Component*> m_connectedComponents;
};

//
//   Class Name: XForm
//  Description: Stores transformation info
//
class XForm : public BaseObject
{
public:
	XForm() : BaseObject("XForm"), m_posn(0.0f,0.0f,0.0f), m_quat(0.0f,0.0f,0.0f,1.0f), m_scale(1.0f,1.0f,1.0f) {}

	virtual bool Read(std::ifstream& is);
	virtual bool Write(std::ofstream& os) const;

	const Point3d& GetPosition() const {return m_posn;}
	void SetPosition(const Point3d& posn) {m_posn = posn;}
	const Point4d& GetQuaternion() const {return m_quat;}
	void SetQuaternion(const Point4d& quat) {m_quat = quat;}
	const Point3d& GetScale() const {return m_scale;}
	void SetScale(const Point3d& scale) {m_scale = scale;}

private:
	Point3d m_posn;
	Point4d m_quat;
	Point3d m_scale;
};

//
//   Class Name: AsciiAttributeValue
//  Description: Class holding an attribute of undefined type. It holds it inside a union
//    Functions: AttributeValue(): constructors from int,float,bool and char *. These are all implicit
//					and a copy constructor
//				~AttributeValue(): destructor
//
//
class AsciiAttributeValue
{
	typedef union {
		int32 i;
		float f;
		bool b;
		char *pString;
	} valueType;
	
	valueType m_value;
	bool m_pointer;
	
public:
	AsciiAttributeValue(int32 value) : m_pointer(false) {m_value.i = value;}
	AsciiAttributeValue(float value) : m_pointer(false)  {m_value.f = value;}
	AsciiAttributeValue(bool value) : m_pointer(false)  {m_value.b = value;}
	AsciiAttributeValue(const char *pValue);
	AsciiAttributeValue(const AsciiAttributeValue &copy);
	~AsciiAttributeValue();
	
	AsciiAttributeValue &operator=(const AsciiAttributeValue& copy);
	// conversion operators
	operator int32() const {return m_value.i;}
	operator float() const {return m_value.f;}
	operator bool() const {return m_value.b;}
	operator char*() const {return m_value.pString;}
};

//
//   Class Name: Attribute
//  Description: Class defining an attribute: its type, name and default value
//    Functions: Attribute(): constructor from int, float, bool and char *
//				GetName(): returns name of attribute
//				GetType(): returns type of attribute
//				GetDefault(): returns default value
//
//
class AsciiAttribute
{
public:
	enum Type {INT, FLOAT, BOOL, STRING};
	
	AsciiAttribute(const std::string &name, int32 value);
	AsciiAttribute(const std::string &name, float value);
	AsciiAttribute(const std::string &name, bool value);
	AsciiAttribute(const std::string &name, const char *pValue);
	
	const std::string &GetName() const;
	Type GetType() const;
	const AsciiAttributeValue &GetValue() const;
	
private:
	Type m_type;
	std::string m_name;
	AsciiAttributeValue m_value;
};

//
//   Class Name: AsciiAttributeList
//  Description: Class holding a list of attributes
//
class AsciiAttributeList : public BaseObject
{
public:
	AsciiAttributeList() : BaseObject("Attributes"), m_name("Undefined") {}

	virtual bool Read(std::ifstream& is);
	virtual bool Write(std::ofstream& os) const;

	int32 GetSize() const {return m_attributes.size();}
	void Add(const AsciiAttribute& attr) {m_attributes.push_back(attr);}
	void Clear() { m_attributes.clear();}
	const std::string &GetName() const;
	void SetName(const std::string &name);
	
	// operators
	AsciiAttribute& operator[](int32 i) {return m_attributes[i];}
	const AsciiAttribute& operator[](int32 i) const {return m_attributes[i];}


private:
	std::string m_name;
	std::vector<AsciiAttribute> m_attributes;
};


//
//   Class Name: CullingSphere
// Base Classes: BaseObject
//  Description: Class describing a culling sphere
//
class CullingSphere : public BaseObject
{
public:
	// default constructor
	CullingSphere();

	virtual bool Read(std::ifstream& is);
	virtual bool Write(std::ofstream& os) const;

	float GetRadius() const;
	void SetRadius(float r);

private:
	float m_radius;
};

//
//   Class Name: CollisionVolume
// Base Classes: BaseObject
//  Description: Base class for collision volumes
//
class CollisionVolume : public BaseObject, public ObjectOwner
{
public:
	CollisionVolume(const std::string& name) : BaseObject(name), m_pAttributeList(NULL) {}
	virtual ~CollisionVolume();

	virtual CollisionSphere* AsCollisionSpherePtr() {return NULL;}
	virtual CollisionBox* AsCollisionBoxPtr() {return NULL;}
	virtual CollisionLine* AsCollisionLinePtr() {return NULL;}
	virtual CollisionPlane* AsCollisionPlanePtr() {return NULL;}
	virtual CollisionMesh* AsCollisionMeshPtr() {return NULL;}

	virtual bool ReadToken(std::ifstream& is, std::string& token);

	AsciiAttributeList* GetAttributeList() {return m_pAttributeList;}
	void SetAttributeList(AsciiAttributeList* pAttributeList) {m_pAttributeList = pAttributeList; AddObject(pAttributeList);}

private:
	AsciiAttributeList* m_pAttributeList;
};

//
//   Class Name: CollisionSphere
// Base Classes: CollisionVolume
//  Description: Class holding collision sphere information
//
class CollisionSphere : public CollisionVolume, public Sphere
{
public:
	CollisionSphere();

	virtual bool Read(std::ifstream& is);
	virtual bool Write(std::ofstream& os) const;

	virtual CollisionSphere *AsCollisionSpherePtr() {return this;}

	void SetLightColours(uint8 lightColour) { m_lightColours = lightColour; }
	uint8 GetLightColours() { return m_lightColours; }

	uint8 m_lightColours;
};


//
//   Class Name: CollisionBox
// Base Classes: CollisionVolume
//  Description: Class holding collision box information
//
class CollisionBox : public CollisionVolume, public Box
{
public:
	CollisionBox();

	virtual bool Read(std::ifstream& is);
	virtual bool Write(std::ofstream& os) const;

	virtual CollisionBox *AsCollisionBoxPtr() {return this;}

	void SetLightColours(uint8 lightColour) { m_lightColours = lightColour; }
	uint8 GetLightColours() { return m_lightColours; }

	uint8 m_lightColours;
};


//
//   Class Name: CollisionLine
// Base Classes: CollisionVolume
//  Description: Class holding collision line information
//
class CollisionLine : public CollisionVolume, public Line
{
public:
	CollisionLine();

	virtual bool Read(std::ifstream& is);
	virtual bool Write(std::ofstream& os) const;

	virtual CollisionLine *AsCollisionLinePtr() {return this;}
};


//
//   Class Name: CollisionPlane
// Base Classes: CollisionVolume
//  Description: Class holding collision plane information
//
class CollisionPlane : public CollisionVolume
{
public:
	enum Plane {XY=0, XZ, YZ};

	CollisionPlane();

	virtual bool Read(std::ifstream& is);
	virtual bool Write(std::ofstream& os) const;

	virtual CollisionPlane *AsCollisionPlanePtr() {return this;}

	const Point3d& GetPosition() const;
	void SetPosition(const Point3d& p);
	const Point2d& GetSize() const;
	void SetSize(const Point2d& s);
	Plane GetPlane() const;
	void SetPlane(Plane p);

private:
	Point3d m_posn;
	Point2d m_size;
	Plane m_plane;
};



//
//   Class Name: CollisionMesh
// Base Classes: CollisionVolume
//  Description: Class holding collision mesh information
//
class CollisionMesh : public CollisionVolume
{
public:
	// Face class used in this class. Basically an array of 3 indices
	class Face
	{
	public:
		Face() {}
		Face(int32 x, int32 y, int32 z, uint8 lightCols = 0xff):lightColours(lightCols) {v[0] = x; v[1] = y; v[2] = z;}

		int32& operator[](int32 i) {return v[i];}
		const int32& operator[](int32 i) const {return v[i];}

		uint8 getLightColours() { return lightColours; }
		void setLightColours(uint8 newLColours) { lightColours = newLColours; }
		void setLightColourDay(uint8 newLColourDay) { lightColours = (newLColourDay & 0xf) + ((lightColours << 8) & 0xf0); }
		void setLightColourNight(uint8 newLColourNight) { lightColours = (lightColours & 0xf) + ((newLColourNight << 8) & 0xf0); }

	private:
		int32 v[3];
		uint8 lightColours;
	};
	
	CollisionMesh();

	virtual bool Read(std::ifstream& is);
	virtual bool Write(std::ofstream& os) const;

	virtual CollisionMesh *AsCollisionMeshPtr() {return this;}

	const Point3d& GetPosition() const;
	void SetPosition(const Point3d& p);

	void AddVertex(Point3d& vert);
	Point3d& GetVertex(int32 i);
	int32 GetNumVertices();
	void AddFace(Face& face);
	Face& GetFace(int32 i);
	int32 GetNumFaces();

private:
	Point3d m_posn;
	std::vector<Point3d> m_vertArray;
	std::vector<Face> m_faceArray;

	// methods
	bool ReadVertices(std::ifstream& is);
	bool ReadFaces(std::ifstream& is);
	bool WriteVertices(std::ofstream& os) const;
	bool WriteFaces(std::ofstream& os) const;
};

//
//   Class Name: BoundingSphere
//  Description: Class holding component bounding sphere information
//
class BoundingSphere : public BaseObject, public Sphere
{
public:
	BoundingSphere();

	virtual bool Read(std::ifstream& is);
	virtual bool Write(std::ofstream& os) const;
};


//
//   Class Name: BoundingBox
// Base Classes: CollisionVolume
//  Description: Class holding collision box information
//
class BoundingBox : public BaseObject, public Box
{
public:
	BoundingBox();

	virtual bool Read(std::ifstream& is);
	virtual bool Write(std::ofstream& os) const;
};

// --- Objects -------------------------------------------------------------------------------------------------

//
//   Class Name: Object
// Base Classes: BaseObject
//  Description: Base Class for all objects attached to components
//
class Object : public BaseObject
{
public:
	Object(const std::string& name) : BaseObject(name) {}

	virtual XRefObject* AsXRefObjectPtr() {return NULL;}
};

//
//   Class Name: XRefObject
// Base Classes: Baseobject
//  Description: Class holding XRef information
//
class XRefObject : public Object
{
public:
	XRefObject();

	virtual XRefObject* AsXRefObjectPtr() {return this;}

	virtual bool Read(std::ifstream& is);
	virtual bool Write(std::ofstream& os) const;

	void SetFilename(const std::string name) {m_filename = name;}
	const std::string& GetFilename() {return m_filename;}
	void SetObjectName(const std::string name) {m_objectName = name;}
	const std::string& GetObjectName() {return m_objectName;}

private:
	std::string m_filename;
	std::string m_objectName;
};



//
//   Class Name: AILink
// Base Classes: BaseObject
//  Description: Class holding information about the links from one AIConnection to another
//
class AILink : public BaseObject, public ObjectOwner
{
public:
	AILink();

	virtual bool Read(std::ifstream& is);
	virtual bool Write(std::ofstream&) const;

	AIConnection* GetConnection();
	void SetConnection(AIConnection* pConnection);
	void SetConnection(std::string& name);

	AsciiAttributeList* GetAttributeList() {return m_pAttributeList;}
	void SetAttributeList(AsciiAttributeList* pAttributeList) {m_pAttributeList = pAttributeList; AddObject(pAttributeList);}
	ConnectedComponentList* GetConnectedComponentList() {return m_pConnectedComps;}
	void SetConnectedComponentList(ConnectedComponentList* pList) {m_pConnectedComps = pList; AddObject(pList);}

private:
	AIConnection* m_pConnection;
	AsciiAttributeList* m_pAttributeList;
	ConnectedComponentList* m_pConnectedComps;
};


//
//   Class Name: AIConnection
// Base Classes: BaseObject
//  Description: Class holding information about the AIConnection plugin
//
class AIConnection : public BaseObject, public Sphere, public ObjectOwner
{
public:
	AIConnection();

	virtual bool Read(std::ifstream& is);
	virtual bool Write(std::ofstream& os) const;

	void AddLink(AILink *pLink);
	AILink *GetLink(int32 i);
	int32 GetNumLinks() const;

	AsciiAttributeList* GetAttributeList() {return m_pAttributeList;}
	void SetAttributeList(AsciiAttributeList* pAttributeList) {m_pAttributeList = pAttributeList; AddObject(pAttributeList);}
	ConnectedComponentList* GetConnectedComponentList() {return m_pConnectedComps;}
	void SetConnectedComponentList(ConnectedComponentList* pList) {m_pConnectedComps = pList; AddObject(pList);}

private:
	PtrArray<AILink> m_linkArray;
	AsciiAttributeList* m_pAttributeList;
	ConnectedComponentList* m_pConnectedComps;
};

//
//   Class Name: AIPath
//  Description: Class holding a list of AIConnections that compose a path
//
class AIPath : public BaseObject, public ObjectOwner
{
public:
	AIPath();

	virtual bool Read(std::ifstream& is);
	virtual bool Write(std::ofstream& os) const;

	void SetSize(int32 size);
	void AddAIConnection(AIConnection *pConnection);
	void AddAIConnection(std::string& name);
	AIConnection* GetAIConnection(int32 i);
	int32 GetNumAIConnections() const;

	AsciiAttributeList* GetAttributeList() {return m_pAttributeList;}
	void SetAttributeList(AsciiAttributeList* pAttributeList) {m_pAttributeList = pAttributeList; AddObject(pAttributeList);}

private:
	std::vector<AIConnection*> m_aiConnections;
	AsciiAttributeList* m_pAttributeList;
};


//
//   Class Name: Component
// Base Classes: BaseObject
//  Description: Class describing a component in the scene
//
class Component : public BaseObject, protected ObjectOwner
{
public:
	Component();
	
	virtual bool Read(std::ifstream& is);
	virtual bool Write(std::ofstream& os) const;
	
	Component* GetSibling();
	Component* GetPreviousSibling();
	Component* GetParent();
	Component* GetChild(int32 i);
	void AddChild(Component *pChild);
	void DisInherit();

	XForm* GetXForm() {return m_pXForm;}
	void SetXForm(XForm* pXForm);

	void SetCullingSphere(CullingSphere *pCullingSphere);
	CullingSphere *GetCullingSphere();
	void SetBoundingSphere(BoundingSphere *pBoundingSphere);
	BoundingSphere *GetBoundingSphere();
	void SetBoundingBox(BoundingBox *pBoundingBox);
	BoundingBox *GetBoundingBox();
	void SetAttributeList(AsciiAttributeList *pAttributeList);
	AsciiAttributeList *GetAttributeList() {return m_pAttributeList;}
	void SetReferenceObject(Object *pObject);
	Object *GetReferenceObject() {return m_pObject;}

	void AddCollisionVolume(CollisionVolume *pVolume);
	CollisionVolume *GetCollisionVolume(int32 i);
	int32 GetNumCollisionVolumes() const;

	void AddShadowVolume(CollisionVolume *pVolume);
	CollisionVolume *GetShadowVolume(int32 i);
	int32 GetNumShadowVolumes() const;

private:
	Component* m_pParent;
	Component* m_pSibling;
	Component* m_pChild;
	Component* m_pLastChild;
	XForm *m_pXForm;
	CullingSphere* m_pCullingSphere;
	BoundingSphere* m_pBoundingSphere;
	BoundingBox* m_pBoundingBox;
	AsciiAttributeList* m_pAttributeList;
	Object* m_pObject;
	std::vector<CollisionVolume*> m_collisionVolumeArray;
	std::vector<CollisionVolume*> m_shadowVolumeArray;
};

//
//   Class Name: SceneData
// Base Classes: ObjectOwner
//  Description: Class holding all the scene data
//    Functions: 
//
//
class SceneData : protected ObjectOwner
{
public:
	// default constructor
	SceneData();

	void Clear();
	bool Read(std::ifstream& is);
	bool Write(std::ofstream& os) const;

	// components
	void AddComponent(Component *pComp);
	Component* GetComponent(int32 i);
	const Component* GetComponent(int32 i) const;
	int32 GetNumComponents() const;
	Component* GetRootComponent();

	// AI Connections
	void AddAIConnection(AIConnection *pConnection);
	AIConnection* GetAIConnection(int32 i);
	int32 GetNumAIConnections() const;

	// AI Paths
	void AddAIPath(AIPath *pPath);
	AIPath* GetAIPath(int32 i);
	int32 GetNumAIPaths() const;

private:
	// arrays of scene objects
	Component m_root;
	std::vector<Component *> m_compArray;
	std::vector<AIConnection *> m_aiConnectionArray;
	std::vector<AIPath *> m_aiPathArray;
};

//
//   Class Name: Scene
// Base Classes: ObjectOwner: class containing a list of objects which are deleted when the class is deleted
//  Description: Interface class to the scene
//    Functions: Scene(): default constructor.
//				Clear(): Empty the scene.
//				Read(): Read scene from file stream.
//				Write(): Write scene to file stream.
//				AddComponent(): Add a component to the scene component list.
//				GetComponent(): Access a component from the scene component list.
//				GetNumComponents(): Return the number of components in the scene.
//				GetRootComponent(): Return the root component of the hierarchy tree.
//				AddAIConnection(): Add an AI Connection to the scene.
//				GetAIConnection(): Access an AI Connection from the scene.
//				GetNumAIConnections(): Return number of AI Connections in scene
//
class Scene 
{
public:
	// default constructor
	Scene();
	~Scene();

	void Clear();
	bool Read(std::ifstream& is);

	// components
	Component* GetComponent(int32 i);
	int32 GetNumComponents() const;
	Component* GetRootComponent();
	// AI Connections
	AIConnection* GetAIConnection(int32 i);
	int32 GetNumAIConnections() const;
	// AI Paths
	AIPath* GetAIPath(int32 i);
	int32 GetNumAIPaths() const;

private:
	class SceneData* m_pScene;
};



} // namespace ascii

#endif