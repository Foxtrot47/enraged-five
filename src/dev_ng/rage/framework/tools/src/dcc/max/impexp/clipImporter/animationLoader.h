#pragma once

#include <atl/array.h>
#include <atl/map.h>

#include "cranimation/animation.h"
#include "crclip/clipanimation.h"
#include "cranimation/animtrack.h"
#include "istdplug.h"

#include "math/amath.h"
#include "vector/matrix34.h"

using namespace rage;

class CAnimationLoader
{
public:
	CAnimationLoader();
	~CAnimationLoader();

	void Apply(INode* pNode, const char* pAnimationFile);

private:
	void GetLocalMatrices(atMap<unsigned short, atString>& mapBoneHashs , atMap<atString, Matrix34>& mapBoneNameToLocal);
	bool GetBoneID(INode* pNode, std::string& outId);
	void PlotOnObject(INode* pNode, crAnimation* pAnim, unsigned short usHash, atString& strName, atMap<atString, Matrix34> mapBoneNameToLocal);
	ISkin* GetSkin(INode* p_Node);
	INode* GetSkinRootBone(ISkin* p_Skin);
	s32 GetNumLevels(INode* p_Node);
	bool GetBoneHashsFromNode(INode* pNode, atMap<unsigned short, atString>& mapBoneHashs);
	
};