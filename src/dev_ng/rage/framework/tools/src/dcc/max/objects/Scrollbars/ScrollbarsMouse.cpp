//
// filename:	ScrollbarsMouse.cpp
// author:		David Muir
// date:		23 January 2007
// description:	Mouse handling for creating/updating Scrollbars Helper Objects
//

// --- Include Files ------------------------------------------------------------
#include "ScrollbarsMouse.h"

// 3D Studio Max SDK headers
#include "iparamb2.h"

// Local headers
#include "Scrollbars.h"
#include "IScrollbars.h"
#include "ScrollbarsPoint.h"

// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

// --- Globals ------------------------------------------------------------------

// --- Static Globals -----------------------------------------------------------

// --- Static class members -----------------------------------------------------

// --- Code ---------------------------------------------------------------------

CScrollbarsMouse::CScrollbarsMouse( )
	: m_bIgnoreSelectionChange( false ),
	m_bIsAttachedToNode( false ),
	m_LastPutCount( -1 ),
	m_NumPointNodes( -1 ),
	m_CurrentPointNode( -1 ),
	m_PreviousPointNode( -1 ),
	m_pt0( 0.0f, 0.0f, 0.0f ),
	m_pt1( 0.0f, 0.0f, 0.0f ),
	m_pInterface( NULL ),
	m_pScrollbars( NULL ),
	m_pNode( NULL )
{

}

CScrollbarsMouse::~CScrollbarsMouse( )
{
	m_pInterface = NULL;
	m_pScrollbars = NULL;
	m_pNode = NULL;
}

//
//        name: CScrollbarsMouse::Begin
// description: Called at the beginning of creation
//          in:
//
void CScrollbarsMouse::Begin( IObjCreate* pObjCreate, ClassDesc* pDescriptor )
{
	assert( pObjCreate );

	m_pInterface = pObjCreate;
	m_pNode = NULL;
	m_pScrollbars = NULL;
	m_bIsAttachedToNode = false;

	CreateNewScrollbars( );
}

//
//        name: ObjectArrayCreateManager::End
// description: Called once creation has finished
//
void CScrollbarsMouse::End( )
{
	if ( m_pScrollbars ) 
	{
		m_pScrollbars->EndEditParams( static_cast<IObjParam*>( m_pInterface ), TRUE, NULL );
		if ( !m_bIsAttachedToNode )
		{
			delete m_pScrollbars;
			m_pScrollbars = NULL;

			// DS 8/21/97: If something has been put on the undo stack since this object was created,
			// we have to flush the undo stack.
			if ( theHold.GetGlobalPutCount() != m_LastPutCount )
			{
				GetSystemSetting( SYSSET_CLEAR_UNDO );
			}
		} 
		else if ( m_pNode ) 
		{
			// Get rid of the references.
			DeleteAllRefsFromMe();
			m_pScrollbars = NULL;
		}
	}
}

//
//        name: ObjectArrayCreateManager::CreateNewArray
// description: Creates a new object array
//
void CScrollbarsMouse::CreateNewScrollbars( )
{
	m_pScrollbars = new CScrollbars;
	m_CurrentPointNode = -1;
	m_PreviousPointNode = -1;
	m_NumPointNodes = 1;
	m_bIsAttachedToNode = false;
	m_pScrollbars->BeginEditParams( static_cast<IObjParam*>(m_pInterface), BEGIN_EDIT_CREATE, NULL );
	m_LastPutCount = theHold.GetGlobalPutCount();
}

//
//        name: CScrollbarsMouse::GetReference
// description: Reference functions for creation manager
//
RefTargetHandle CScrollbarsMouse::GetReference(int i)
{
	if(i == 0)
		return m_pNode;
	return NULL;
}

//
// name:		CScrollbarsMouse::SetReference
// description:	
//
void CScrollbarsMouse::SetReference(int i, RefTargetHandle rtarg)
{
	if( 0 == i )
	{
		m_pNode = static_cast<INode*>( rtarg );
	}
}

//
// name:		CScrollbarsMouse::NotifyRefChanged
// description:	
//
RefResult CScrollbarsMouse::NotifyRefChanged( Interval changeInt, RefTargetHandle hTarget, 
											  PartID& partID, RefMessage message )
{
	assert( m_pInterface );

	switch ( message ) 
	{
	case REFMSG_TARGET_SELECTIONCHANGE:
		if ( m_bIgnoreSelectionChange ) 
		{
			break;
		}
		if ( m_pScrollbars ) 
		{
			m_pScrollbars->EndEditParams( static_cast<IObjParam*>( m_pInterface ), FALSE, NULL );
			m_pScrollbars = NULL;
			// this will set m_pNode = NULL;
			DeleteAllRefsFromMe();
			CreateNewScrollbars();
		}
		break;

	case REFMSG_TARGET_DELETED:
		if ( m_pScrollbars ) 
		{
			m_pScrollbars->EndEditParams( static_cast<IObjParam*>( m_pInterface ), FALSE, NULL );
			m_pScrollbars = NULL;
			CreateNewScrollbars();
		}
		break;
	}
	return REF_SUCCEED;
}


//
//        name: CScrollbarsMouse::proc
// description: Mouse event-handler function
//				It should be possible to override this behaviour using MAXScript which
//				would allow for easily updating the user-interaction behaviour of
//				constructing Scrollbar 2D effects.
//
int CScrollbarsMouse::proc( HWND hwnd, int msg, int point, int flag, IPoint2 m )
{
	assert( m_pInterface );
	TimeValue t = m_pInterface->GetTime();
	s32 res = TRUE;
	ViewExp* pVpt = m_pInterface->GetViewport( hwnd );
	assert( pVpt );

	switch ( msg ) 
	{
	case MOUSE_FREEMOVE:
		// The MOUSE_FREEMOVE mode isn't really used but we mimic other Max
		// tools by snapping the cursor.

		SetCursor( m_pInterface->GetSysCursor( SYSCUR_DEFARROW ) );
		pVpt->SnapPreview( m, m, NULL, SNAP_IN_3D );
		break;
	case MOUSE_MOVE:
	case MOUSE_POINT:
		// The MOUSE_POINT (left-click) is used for adding new points to the
		// scrollbars object.  As the points are added the viewport is refreshed
		// in order to render the intermediate state of the scrollbars object.
		if ( ( 0 == point ) && ( MOUSE_LBUTTON & flag ) && 
			 ( 0 == m_pScrollbars->GetNumPoints() ) )
		{
			// First Point
			assert( m_pScrollbars );
			assert( 0 == m_pScrollbars->GetNumPoints() );
			assert( !m_bIsAttachedToNode );
			theHold.Begin( ); // Start undo context

			INode* pNode = m_pInterface->CreateObjectNode( m_pScrollbars );
			m_bIsAttachedToNode = true;

			// Reference the first node so we'll get notifications
			ReplaceReference( 0, pNode );

			m_bIgnoreSelectionChange = true;
			m_pInterface->SelectNode( m_pNode );
			m_bIgnoreSelectionChange = false;

			// Get mouse position
			Matrix3 mat(1);
			m_pt0 = pVpt->SnapPoint( m, m, NULL, SNAP_IN_3D );
			mat.SetTrans( m_pt0 );

			m_pInterface->SetNodeTMRelConstPlane( m_pNode, mat );
			res = TRUE;

			s32 newPathNode = m_pScrollbars->AddPoint( );

			CScrollbarsPoint* pPoint = m_pScrollbars->GetScrollbarPoint( 0 );
			assert( pPoint );
			pPoint->SetDrawCol( Point3( 1.0f, 1.0f, 1.0f ) );
		}
		else if ( ( 1 < point ) && ( MOUSE_POINT == msg ) )
		{
			// Additional Points
			s32 nNewPointIndex = m_pScrollbars->AddPoint( );
			assert( point == m_pScrollbars->GetNumPoints() );

			CScrollbarsPoint* pNewPoint = m_pScrollbars->GetScrollbarPoint( nNewPointIndex - 1 );
			assert( pNewPoint );

			m_pt1 = pVpt->SnapPoint( m, m, NULL, SNAP_IN_3D );

			pNewPoint->SetDrawCol( Point3( 1.0f, 1.0f, 1.0f ) );
			
			m_PreviousPointNode = m_CurrentPointNode;
			m_CurrentPointNode = nNewPointIndex;
			m_NumPointNodes++;

			m_pt1 = pVpt->SnapPoint( m, m, NULL, SNAP_IN_3D );
			m_pt1 -= m_pt0;

			m_pScrollbars->SetPointPosition( (point - 1), m_pt1 );

			// After construction of a new point we refresh the viewport
			// so the new scrollbar lines are rendered.
			m_pInterface->RedrawViews( t );
		}
		break;
	case MOUSE_ABORT:
		// The MOUSE_ABORT (right-click during scrollbars creation) is used to
		// stop scrollbar creation using the last left-click point as the last
		// point for the scrollbar object.
		if ( m_pScrollbars->GetNumPoints() > 1 )
		{
			m_pScrollbars->SetVisibleNumPoints( m_pScrollbars->GetNumPoints() );
			m_pScrollbars->EndEditParams( static_cast<IObjParam*>( m_pInterface ), 0, NULL );
			m_pScrollbars = NULL;
			theHold.Accept( _T("Create Scrollbars") );

			CreateNewScrollbars( );
		}
		break;
	case MOUSE_PROPCLICK:
		// right click while between creations
		m_pInterface->RemoveMode( NULL );
		break;
	}

	m_pInterface->ReleaseViewport( pVpt );
	return res;
}

// End of file
