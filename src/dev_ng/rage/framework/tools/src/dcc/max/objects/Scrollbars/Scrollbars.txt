Project Scrollbars
Template max_configurations.xml

OutputFileName Scrollbars.dlo
ConfigurationType dll

WarningsAsErrors false
WarningLevel 3

Define GTA_PC;SCROLLBARS_EXPORTS;MAX8

IncludePath $(RAGE_DIR)\base\src
IncludePath $(RAGE_DIR)\base\tools\dcc\max\rageMaxDataStore
IncludePath $(RS_TOOLSROOT)\src\GTA_tools\include

EmbeddedLibAll comctl32.lib core.lib geom.lib gfx.lib mesh.lib maxutil.lib paramblk2.lib maxscrpt.lib

ForceInclude forceinclude.h

Files {
	Folder "Header Files" { 
		forceinclude.h
		IScrollbars.h
		resource.h
		Scrollbars.h
		ScrollbarsCommandMode.h
		ScrollbarsMouse.h
		ScrollbarsPoint.h
		Win32Res.h
	}
	Folder "Resource Files" {	
		Resources.rc
		rockstar.bmp
		Scrollbars.def
	}
	Folder "Source Files" {
		DLLEntry.cpp
		Scrollbars.cpp
		ScrollbarsCommandMode.cpp
		ScrollbarsMouse.cpp
		ScrollbarsPoint.cpp
		Win32Res.cpp
	}	
}