#ifndef __CLIP_EDITOR__
#define __CLIP_EDITOR__


#include "3dsmaxsdk_preinclude.h"
#include "Max.h"

#include "resource.h"
#include "istdplug.h"
#include "iparamb2.h"
#include "iparamm2.h"
//SIMPLE TYPE

#include <guplib.h>

#include "atl/array.h"
//#include "crclip/tags.h"
#include "crmetadata/properties.h"
#include "crmetadata/propertyattributes.h"

#include "clipObject.h"

extern TCHAR *GetString(int id);

extern HINSTANCE hInstance;

using namespace rage;

class clipImporter : public GUP {
	public:

		static HWND hParams;

		// GUP Methods
		DWORD		Start			( );
		void		Stop			( );
		DWORD_PTR	Control			( DWORD parameter );
		
		// Loading/Saving
		IOResult Save(ISave *isave);
		IOResult Load(ILoad *iload);
		
		// Singleton access
		static clipImporter& GetInst();

		//Constructor/Destructor
		clipImporter();
		~clipImporter();

		static void InitClass();
		static void ShutdownClass();
		
		static bool initialised;

		ClipObject* GetClipObject();
		void DeleteClipObject();

	private:
		ClipObject m_clipObject;
};



extern clipImporter g_clipImporter;



#endif //__CLIP_EDITOR__
