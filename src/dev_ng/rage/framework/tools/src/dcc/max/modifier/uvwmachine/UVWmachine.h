/**********************************************************************
 *<
	FILE: UVWmachine.h

	DESCRIPTION:	Template Utility

	CREATED BY:

	HISTORY:

 *>	Copyright (c) 1997, All Rights Reserved.
 **********************************************************************/

#ifndef __UVWMACHINE__H
#define __UVWMACHINE__H

#include <Max.h>
#include "resource.h"
// my headers
//#include <dma.h>
#include "MeshSelect.h"
#include "uvwui.h"
#include "UVWMode.h"

extern TCHAR *GetString(int id);
extern HINSTANCE hInstance;

#define UVWMACHINE_CLASS_ID	Class_ID(0xbeada056, 0xd3cd3ab2)


//
//   Class Name: UVWEnumProc
// Base Classes: 
//  Description: 
//    Functions: 
//
//
class UVWEnumProc : public ModContextEnumProc
{
private:
	int32 m_operation;
	int32 m_select;
	int32 m_faceSel;
	int32 m_index;
	float *m_pValues;

public:
	enum {NONE=0, 
		ROTATE, 
		ROTATE90, 
		FLIPU, 
		FLIPV, 
		EXPLICITSETU, 
		EXPLICITSETV, 
		MOUSEROTATE, 
		MOUSESCALE, 
		MOUSETRANSLATE,
		MOUSESCALEU, 
		MOUSETRANSLATEU,
		MOUSESCALEV, 
		MOUSETRANSLATEV,
		DESTROYEXTRACTED};

	UVWEnumProc() {m_operation = NONE;}
	UVWEnumProc(int32 operation) {m_operation = operation;}

	void SetOperation(int32 operation) {m_operation = operation;}
	void SetSelection(int32 select) {m_select = select;}
	void SetFaceSelection(int32 faceSel) {m_faceSel = faceSel;}
	void SetVertexIndex(int32 v) {m_index = v;}
	void SetValuesPtr(float *pV) {m_pValues = pV;}

	BOOL proc(ModContext *mc);
};



//
//   Class Name: UVWmachine
// Base Classes: 
//  Description: 
//    Functions: 
//
//
class UVWmachine : public StdMeshSelectModifier {

public:
	static UVWmachine *m_pCurrentModifier;
	UVWmachineUI m_ui;

	//Constructor/Destructor
	UVWmachine();
	~UVWmachine();

	virtual const char **GetSelTypeNames();
	virtual const int32 *GetSelTypes();
	virtual int32 GetNumSelTypes();

	void DestroyExtractedArrays(void);
	void ProcessTextureCoords(UVWEnumProc &proc, char *pName);
	void SetTextureCoord(int32 index, float u, float v);
	void StartMouseOperation(MouseOper oper);
	void EndMouseOperation(void);
	void ClearMouseButtons(MouseOper oper);

	// from StdMeshSelectModifier
	void SetFaceSelLevel(uint32 level);
	void SetSelLevel(DWORD level);
	void LocalDataChanged();

	// From Animatable
	void DeleteThis() { delete this; }
	void GetClassName(TSTR& s) { s= TSTR(GetString(IDS_CLASS_NAME)); }  
	virtual Class_ID ClassID() { return UVWMACHINE_CLASS_ID;}		
#if MAX_VERSION_MAJOR >= 9 
	RefTargetHandle Clone(RemapDir& remap = DefaultRemapDir());
#else
	RefTargetHandle Clone(RemapDir& remap = NoRemap());
#endif
	TCHAR *GetObjectName() { return GetString(IDS_CLASS_NAME); }
	IOResult Load(ILoad *iload);
	IOResult Save(ISave *isave);
	IOResult LoadLocalData(ILoad *iload, LocalModData **ppModData);
	IOResult SaveLocalData(ISave *isave, LocalModData *pModData);

	//From Modifier
	ChannelMask ChannelsUsed()  { return TEXMAP_CHANNEL|SELECT_CHANNEL|SUBSEL_TYPE_CHANNEL|TOPO_CHANNEL|DISP_ATTRIB_CHANNEL|GEOM_CHANNEL; }
	ChannelMask ChannelsChanged() { return TEXMAP_CHANNEL|SELECT_CHANNEL|SUBSEL_TYPE_CHANNEL|TOPO_CHANNEL|DISP_ATTRIB_CHANNEL; }
	void ModifyObject(TimeValue t, ModContext &mc, ObjectState *os, INode *node);
	Interval LocalValidity(TimeValue t);

	// From BaseObject
	BOOL ChangeTopology() {return TRUE;}
	void ActivateSubobjSel(int level, XFormModes& modes);
	int NumSubObjTypes();
	ISubObjType *GetSubObjType(int i);

	RefResult NotifyRefChanged( Interval changeInt,RefTargetHandle hTarget, 
	   PartID& partID, RefMessage message);

	CreateMouseCallBack* GetCreateMouseCallBack() {return NULL;}

	void BeginEditParams(IObjParam *ip, ULONG flags,Animatable *prev);
	void EndEditParams(IObjParam *ip, ULONG flags,Animatable *next);
};



//
//   Class Name: UVWmachineClassDesc
// Base Classes: ClassDesc
//  Description: Class description
//    Functions: 
//
//
class UVWmachineClassDesc : public ClassDesc
{
public:
	int 			IsPublic() {return 1;}
	void *			Create(BOOL loading = FALSE) {return new UVWmachine();}
	const TCHAR *	ClassName() {return GetString(IDS_CLASS_NAME);}
	SClass_ID		SuperClassID() {return OSM_CLASS_ID;}
	Class_ID		ClassID() {return UVWMACHINE_CLASS_ID;}
	const TCHAR* 	Category() {return GetString(IDS_CATEGORY);}
	void			ResetClassParams (BOOL fileReset);

	// Hardwired name, used by MAX Script as unique identifier
	const TCHAR*	InternalName()				{ return _T("UVWmachine"); }
	HINSTANCE		HInstance()					{ return hInstance; }
};



#endif // __UVWMACHINE__H
