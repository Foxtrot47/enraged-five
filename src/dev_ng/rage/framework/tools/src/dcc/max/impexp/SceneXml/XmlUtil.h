//
// filename:	XmlUtil.h
// description:	
//

#ifndef INC_XMLUTIL_H_
#define INC_XMLUTIL_H_

// --- Include Files ------------------------------------------------------------

// Max headers
#include "max.h"

// STL headers
#include <string>

// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

// Forward-declare some TinyXml classes.
class TiXmlDocument;
class TiXmlElement;

// --- Globals ------------------------------------------------------------------

namespace XmlUtil
{

/**
 * @brief Create transform element from a position and quaternion.
 */
TiXmlElement* CreateTransformNode( const char* name, const Point3& pos, const Quat& rot, const Point3& scale );
TiXmlElement* CreateTransformNode( const char* name, const Matrix3& mat, const Matrix3& matNode );

/**
 * @brief Create colour element from a Point3 RGB.
 */
TiXmlElement* CreateRGBAColour( const Point3& rgb, float alpha = 0.0f );
TiXmlElement* CreatePoint3Node( const Point3& rgb );
TiXmlElement* CreatePoint4Node( const Point4& rgb );

void SetAttr( TiXmlElement* pElement, const std::string& attr, const float val );
void SetAttr( TiXmlElement* pElement, const std::string& attr, const int val );
void SetAttr( TiXmlElement* pElement, const std::string& attr, const std::string& val );

/**
 * @brief Escape a string to be suitable for XML output.
 */
std::string EscapeStr( const std::string& input );

} 

#endif // !INC_XMLUTIL_H_
