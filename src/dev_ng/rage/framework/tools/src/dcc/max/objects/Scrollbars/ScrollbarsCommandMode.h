//
// filename:	ScrollbarsCommandMode.h
// author:		David Muir
// date:		24 January 2007
// description:	Command Mode interface for creating Scrollbars objects
//

#ifndef INC_SCROLLBARSCOMMANDMODE_H_
#define INC_SCROLLBARSCOMMANDMODE_H_

// --- Include Files ------------------------------------------------------------

// 3D Studio Max SDK headers
#include "Max.h"
#include "cmdmode.h"

// Local headers
#include "ScrollbarsMouse.h"

// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

class CScrollbarsCommandMode : public CommandMode
{
public:
	int							Class( );
	int							ID( );
	void						Begin( IObjCreate* pObjCreate, ClassDesc* pDescriptor );
	void						End( );
	MouseCallBack*				MouseProc(int *pNumPoints);
	ChangeForegroundCallback*	ChangeFGProc( );
	BOOL						ChangeFG( CommandMode *oldMode );
	void						EnterMode( );
	void						ExitMode( );

private:
	CScrollbarsMouse			m_proc;
};

// --- Globals ------------------------------------------------------------------

extern CScrollbarsCommandMode g_ScrollbarsCommandMode;

#endif // !INC_SCROLLBARSCOMMANDMODE_H_
