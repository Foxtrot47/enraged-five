#ifndef __INTERIOR__H
#define __INTERIOR__H

#include "Max.h"
#include "resource.h"
#include "istdplug.h"
#include "iparamb2.h"
#include "iparamm2.h"

extern TCHAR *GetString(int id);

extern HINSTANCE hInstance;

#define STANDARD_PBLOCK_REF 0
#define STANDARD_MESH_REF 1

#ifdef STANDARD_EXPORTS
#define STANDARD_API __declspec(dllexport)
#else
#define STANDARD_API __declspec(dllimport)
#endif

/////////////////////////////////////////////////////////////////////////////////////////////////
enum 
{
	standard_params 
};

/////////////////////////////////////////////////////////////////////////////////////////////////
class Standard : public HelperObject
/////////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	Standard(Class_ID& classID,const char* p_cName);
	~Standard();

	// From BaseObject
	CreateMouseCallBack* GetCreateMouseCallBack();
	void BeginEditParams( IObjParam *ip, ULONG flags,Animatable *prev);
	void EndEditParams( IObjParam *ip, ULONG flags,Animatable *next);
	TCHAR *GetObjectName() { return m_cClassName; }
	int Display(TimeValue t, INode* inode, ViewExp *vpt, int flags);
	void GetWorldBoundBox(TimeValue t, INode* inode, ViewExp* vp, Box3& box );
	void GetLocalBoundBox(TimeValue t, INode* inode, ViewExp* vp, Box3& box );
	int HitTest(TimeValue t, INode* inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt);

	// Animatable methods
	int NumParamBlocks() {return 0;}
	IParamBlock2* GetParamBlock(int i) {return NULL;}
	IParamBlock2* GetParamBlockByID(short id) {return NULL;}
	void DeleteThis() { delete this; }
	Class_ID ClassID() { return m_classID; }  
	void GetClassName(TSTR& s){	s = m_cClassName;}

	int NumRefs() { return 2; }
	RefTargetHandle GetReference(int i);
	void SetReference(int i, RefTargetHandle rtarg);
	RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,RefMessage message);
	ObjectState Eval(TimeValue t);

	// From ref
#if MAX_VERSION_MAJOR >= 9 
	ReferenceTarget* Clone(RemapDir &remap = DefaultRemapDir());
#else
	ReferenceTarget* Clone(RemapDir &remap = NoRemap());
#endif
	
	// From object
	void InitNodeName(TSTR& s){	s = m_cClassName;}

	// From SimpleObject
	void BuildMesh(TimeValue t);
	BOOL OKtoDisplay(TimeValue t);
	void InvalidateUI();

	void DrawMesh(GraphicsWindow *gw, TimeValue t, Matrix3& objMat,INode* pNode,ViewExp *vpt);

	TriObject* m_pTriObject;

	void addBox(Point3 pntCentre,Point3 pntDimension,int& iVertOffset,int& iFaceOffset);

	Class_ID m_classID;
	TCHAR m_cClassName[64];
};	

#endif // __INTERIOR__H
