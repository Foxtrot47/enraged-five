//
//
//    Filename: IConnection.h
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Interface header for VehiclePath
//
//
#ifndef INC_ICONNECTION_H_
#define INC_ICONNECTION_H_

// Class IDs
#define VEHICLE_NODE_CLASS_ID		Class_ID(0x52ee77f9, 0x7cb85a7d)
#define VEHICLE_LINK_CLASS_ID		Class_ID(0x6a8d55e4, 0x23c91849)
//#define CREATE_MGR_CLASS_ID			Class_ID(0x6c1a7f53, 0x4f72510)
//#define VEHICLE_PATH_CLASS_ID		Class_ID(0x6c1a7f53, 0x4f72510)

// VehiclePath parameters
// Paramblock2 name
enum { vehicle_node_params, vehicle_link_params, vehicle_path_params}; 

// VehiclePath Paramblock2 parameter list
enum { 
	vehicle_auto_create,
	vehicle_auto_create_min_length,
	vehicle_node_disable,
	vehicle_node_roadblock,
	vehicle_node_water,
	vehicle_node_car_speed,
	vehicle_node_special,
	vehicle_link_lanes_in,
	vehicle_link_lanes_out,
	vehicle_link_width,
};

// VehiclePath Paramblock2 parameter list
enum { 
	vehicle_connectionlist,
	vehicle_valid
};

// VehicleLink References
enum {
	ail_control1 = 0,
	ail_control2,
	ail_paramblock,
	ail_numrefs
};

#define MIN_LANES	(1)
#define MAX_LANES	(4)
#define DEF_LANES	(1)

#define MIN_WIDTH	(0)
#define MAX_WIDTH	(10)
#define DEF_WIDTH	(0)

#define	MIN_LINK_LEN	(100)
#define MAX_LINK_LEN	(1000)
#define DEF_LINK_LEN	(200)

#endif // INC_ICONNECTION_H_