#pragma once
#include "water.h"
#include "FPInterface.h"

#define WATER_CLASS_ID	Class_ID(0x7cbc6843, 0x4f1377a7)

class WaterHelper :
	public Water,
	public IWater
{
public:
	WaterHelper(void);

	TCHAR *GetObjectName() { return GetString(IDS_WATER_NAME); }

	RefTargetHandle Clone(RemapDir& remap);

	// Animatable methods
// 	int NumParamBlocks() {return 1;}
// 	IParamBlock2* GetParamBlock(int i);
// 	IParamBlock2* GetParamBlockByID(short id);
	Class_ID ClassID() { return WATER_CLASS_ID; }  
	void GetClassName(TSTR& s){	s = GetString(IDS_WATER_NAME);}

	void BeginEditParams( IObjParam *ip, ULONG flags,Animatable *prev);
	void EndEditParams( IObjParam *ip, ULONG flags,Animatable *next);

// 	int NumRefs() { return 2; }
// 	RefTargetHandle GetReference(int i);
// 	void SetReference(int i, RefTargetHandle rtarg);

	// From object
	void InitNodeName(TSTR& s){	s = GetString(IDS_WATER_NAME);}
	int		GetType();
	bool	ShowTypeButton()
	{
		return true;
	}
	float	GetAlphaFloat(int index);
	void	SetAlphaFloat(int index, float alpha);
	int		GetAlpha(int index);
	void	SetAlpha(int index, int alpha);

	void LoadAlphaFromFloats();
	void PostLoadHook(ILoad *iload);
	void BuildSpecificMesh(TimeValue t, Mesh &mesh, Point3 centre, Point3 dim);

	// IWater functions
	//Function Publishing method (Mixin Interface)
	//Added by Adam Felt (5-16-00)
	//******************************
	BaseInterface* GetInterface(Interface_ID id)  
	{ 
		if (id == WATER_INTERFACE) 
			return (WaterHelper*)this; 
		else 
			return FPMixinInterface::GetInterface(id);
	} 
	//******************************
	Mesh &GetMesh()
	{
		return m_pTriObject->GetMesh();
	}

// 
// 	IParamBlock2* m_pblockWater;
};
