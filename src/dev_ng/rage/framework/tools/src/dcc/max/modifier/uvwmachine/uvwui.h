//
//
//    Filename: uvwui.h
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 18/05/99 15:50 $
//   $Revision: 3 $
// Description: User interface to UVWmachine modifier
//
//
#ifndef _UVW_UI_H_
#define _UVW_UI_H_


#include <Max.h>
#include "resource.h"
// my headers
#include <dma.h>

#define NUM_COORD_SPINNERS	8

// class prototype
class UVWmachine;



class UVWmachineUI
{
private:
	static HWND m_hVtxParams;
	static HWND m_hFaceParams;
	static HIMAGELIST m_hImageList;

	UVWmachine *m_pMod;
	Interface *m_ip;
	ICustToolbar *m_pToolbar;
	ICustButton *m_pSingleButton;
	ICustButton *m_pPolyButton;
	ICustButton *m_pElementButton;
	ICustButton *m_pRotateButton;
	ICustButton *m_pScaleButton;
	ICustButton *m_pScaleButtonU;
	ICustButton *m_pScaleButtonV;
	ICustButton *m_pTranslateButton;
	ICustButton *m_pTranslateButtonU;
	ICustButton *m_pTranslateButtonV;
	ISpinnerControl *m_pThresholdSpin;
	ISpinnerControl *m_pUOriginSpin;
	ISpinnerControl *m_pVOriginSpin;
	ISpinnerControl *m_pCoordSpin[NUM_COORD_SPINNERS];

	float m_uOrigin, m_vOrigin;

public:
	UVWmachineUI() : m_uOrigin(0.0), m_vOrigin(0.0) {}

	void SetInterface(Interface *ip) {m_ip = ip;}
	Interface *GetInterface(void) {return m_ip;}
	void SetModifier(UVWmachine *pMod) {m_pMod = pMod;}

	void InitVertexDialog(HWND hWnd);
	void DestroyVertexDialog(HWND hWnd);
	void InitFaceDialog(HWND hWnd);
	void DestroyFaceDialog(HWND hWnd);
	void AddRollupPage(void); 
	void DeleteRollupPage(void);
	void LoadFaceToolbar(void);
	void SetFaceSelection(int32 selection);
	float GetSelectThresholdValue(void) {return m_pThresholdSpin->GetFVal();}
	float GetCoordSpinValue(int32 i) {return m_pCoordSpin[i]->GetFVal();}
	void UpdateOrigin(void) {m_uOrigin = m_pUOriginSpin->GetFVal(); m_vOrigin = m_pVOriginSpin->GetFVal(); }
	void GetOrigin(float &u, float &v) {UpdateOrigin(); u = m_uOrigin; v = m_vOrigin;}
	ICustButton *GetRotateButton() {return m_pRotateButton;}
	ICustButton *GetScaleButton() {return m_pScaleButton;}
	ICustButton *GetTranslateButton() {return m_pTranslateButton;}
	ICustButton *GetScaleButtonU() {return m_pScaleButtonU;}
	ICustButton *GetTranslateButtonU() {return m_pTranslateButtonU;}
	ICustButton *GetScaleButtonV() {return m_pScaleButtonV;}
	ICustButton *GetTranslateButtonV() {return m_pTranslateButtonV;}
};


#endif // _UVW_UI_H_