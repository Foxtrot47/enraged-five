//
//
//    Filename: VehiclePath.h
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: 
//
//
#ifndef INC_VEHICLE_PATH__H
#define INC_VEHICLE_PATH__H

// Max SDK headers
#include <Max.h>
#include <istdplug.h>
#include <iparamm2.h>
// resources header
#include "resource.h"
// My headers
#include <dma.h>
#include "IConnection.h"

// STD headers
#pragma warning (disable : 4786)
#include <vector>

//#define ONLY_LINEAR_PATHS

extern HINSTANCE hInstance;

TCHAR *GetString(int id);
ClassDesc* GetVehiclePathDesc();

class VehicleNode;

//
//   Class Name: VehiclePath
// Base Classes: HelperObject
//  Description: 
//    Functions: 
//
//
class VehiclePath : public HelperObject 
{
private:
	static IObjParam *m_ip;			//Access to the interface
	static HWND m_hDefaultPanel;
	static bool m_creating;

	IParamBlock2 *m_pBlock2;

	static ICustButton*		m_pAutoCreateButton;
	static ISpinnerControl* m_pSpinLinkLen;

	static ICustButton*		m_pDisabledButton;

	static ISpinnerControl* m_pSpinLanesIn;
	static ISpinnerControl* m_pSpinLanesOut;
	static ISpinnerControl* m_pSpinWidth;

public:


	//Constructor/Destructor
	VehiclePath();
	~VehiclePath();

/*	// VehiclePath methods
	void RemoveDeletedNodes();
	void SetIsValid();
	// drawing
	float GetDrawingScaleFactor(TimeValue t, INode *pNode, ViewExp* pVpt);
	void Draw(TimeValue t, ViewExp *pVpt, INode *pNode, float r=1.0f, float g=1.0f, float b=1.0f);
*/
	// UI
	void InitDefaultUI();
	void CloseDefaultUI();
	void UpdateDefaultUI();

	// Animatable methods
	int NumParamBlocks() {return 1;}
	IParamBlock2* GetParamBlock(int i) {assert(i == 0); return m_pBlock2;}
	IParamBlock2* GetParamBlockByID(short id) {assert(id == vehicle_path_params); return m_pBlock2;}
	Class_ID ClassID() {return VEHICLE_PATH_CLASS_ID;}
	void GetClassName(TSTR& s) {s = _T("VehiclePath");}
	void BeginEditParams( IObjParam  *ip, ULONG flags,Animatable *prev);
	void EndEditParams( IObjParam *ip, ULONG flags,Animatable *next);
	void DeleteThis() {delete this;}

	// ReferenceTarget methods
	RefTargetHandle Clone(RemapDir& remap = NoRemap());
	IOResult Load(ILoad *iload);
	IOResult Save(ISave *isave);
	int NumRefs();
	RefTargetHandle GetReference(int i);
	void SetReference(int i, RefTargetHandle pTarg);
	RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,  RefMessage message);

		
	//From BaseObject
	CreateMouseCallBack* GetCreateMouseCallBack();
	TCHAR *GetObjectName() { return _T("VehiclePath"); }
	
	// From Object
	ObjectState Eval(TimeValue t) {return ObjectState(this);}
//	void InitNodeName(TSTR &s) { s = _T("VehiclePath"); }
//	BOOL IsWorldSpaceObject() {return TRUE;}

	bool UseAutoCreate();
	float GetLinkMinLength();

	void SetNodeDefaults(VehicleNode* pNode);


};


//
//   Class Name: VehiclePathClassDesc2
// Base Classes: ClassDesc
//  Description: 
//    Functions: 
//
//
class VehiclePathClassDesc2 : public ClassDesc2
{

public:
	int IsPublic() {return 1;}
	void* Create(BOOL loading = FALSE);
	const TCHAR *ClassName() {return _T("VehiclePath");}
	SClass_ID SuperClassID() {return HELPER_CLASS_ID;}
	Class_ID ClassID() {return VEHICLE_PATH_CLASS_ID;}
	const TCHAR* Category() {return _T("Standard");}
	void ResetClassParams (BOOL fileReset);

	int BeginCreate(Interface *ip);
	int EndCreate(Interface *ip);

	// Hardwired name, used by MAX Script as unique identifier
	const TCHAR*	InternalName() { return _T("VehiclePath"); }
	HINSTANCE		HInstance()	{ return hInstance; }
};

extern VehiclePath  g_vehiclePath;

#endif // INC_VEHICLE_PATH__H
