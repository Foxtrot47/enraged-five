#pragma warning (disable : 4786)

// max headers
#include <max.h>
// define the new primitives using macros from SDK
#include <maxscript/maxscript.h>
#include <maxscript/maxwrapper/maxclasses.h>
#include <maxscript/foundation/Numbers.h>
#include <maxscript/foundation/strings.h>
#include <maxscript/compiler/parser.h>
#include <maxscript/macros/define_instantiation_functions.h> // LPXO: define me last

#include <string>

#include "PatrolNode.h"

def_visible_primitive( patrolnode_numconnections, "PatrolNodeNumConn" );
def_visible_primitive( patrolnode_getconnection, "PatrolNodeGetConn" );

def_visible_primitive( patrollink_getnodea, "PatrolLinkGetNodeA" );
def_visible_primitive( patrollink_getnodeb, "PatrolLinkGetNodeB" );

/////////////////////////////////////////////////////////////////////////////////////
Value *patrolnode_numconnections_cf(Value** arg_list, int count)
{
	//check we have the right number of parameters
	check_arg_count(patrolnode_numconnections, 1, count);

	//need to make sure that the object passed in is an aiconnection
	type_check(arg_list[0], MAXNode, "PatrolNodeNumConn <patrolnode>");

	INode *pNode = arg_list[0]->to_node();
	Object *pObj = pNode->EvalWorldState(0).obj;

	if(pObj->ClassID() != PATROL_NODE_CLASS_ID)
		throw RuntimeError ("PatrolNodeNumConn requires a PatrolNode : ", arg_list[0]);

	PatrolNode* pPatrolNode = (PatrolNode *)pObj;

	//if it is, then return the number of other nodes it is linked to

	int iNumRefs = pPatrolNode->NumRefs();
	int iNumConnRefs = 0;
	
	for(int i=0;i<iNumRefs;i++)
	{
		RefTargetHandle handle = pPatrolNode->GetReference(i);

		if(!handle)
		{
			continue;
		}

		Object* pObject = static_cast<Object*>(handle);

		if(pObject->ClassID() == PATROL_LINK_CLASS_ID)
		{
			iNumConnRefs++;
		}
	}

	return_protected(Integer::intern(iNumConnRefs));
}

/////////////////////////////////////////////////////////////////////////////////////
INode* getINode(PatrolNode* pFrom)
{
	INode *pThisNode = NULL;
#if (MAX_RELEASE >= 12000)
	DependentIterator pItem(pFrom);
	ReferenceMaker* maker = NULL;

	while(NULL != (maker = pItem.Next()))
	{
		if(maker->SuperClassID() == BASENODE_CLASS_ID)
		{
			pThisNode = (INode *)maker;
			break;
		}
	}
#else
	RefListItem *pItem = pFrom->GetRefList().FirstItem();

	while(pItem)
	{
		if(pItem->maker->SuperClassID() == BASENODE_CLASS_ID)
		{
			pThisNode = (INode *)pItem->maker;
			break;
		}
		pItem = pItem->next;
	}
#endif //MAX_RELEASE >= 12000

	return pThisNode;
}

/////////////////////////////////////////////////////////////////////////////////////
Value *patrolnode_getconnection_cf(Value** arg_list, int count)
{
	//check we have the right number of parameters
	check_arg_count(patrolnode_getconnection, 2, count);

	//need to make sure that we have the correct parameters passed in
	type_check(arg_list[0], MAXNode, "PatrolNodeGetConn <patrolnode> <index>");
	type_check(arg_list[1], Integer, "PatrolNodeGetConn <patrolnode> <index>");

	//get the parameters
	INode *pNode = arg_list[0]->to_node();
	int iIndex = arg_list[1]->to_int();
	Object *pObj = pNode->EvalWorldState(0).obj;

	if(pObj->ClassID() != PATROL_NODE_CLASS_ID)
		throw RuntimeError( "PatrolNodeGetConn requires an PatrolNode: ", arg_list[0] );

	PatrolNode* pPatrolNode = (PatrolNode*) pObj;
	PatrolNode* pPatrolLinkedNode = NULL;

	//check the index isnt out of range
	int iNumRefs = pPatrolNode->NumRefs();

	if(iIndex > iNumRefs)
		throw RuntimeError ("index is out of range in PatrolNodeGetConn: ", arg_list[1]);

	//get the link

	Object* pLink = NULL;
	int iCurrentIndex = 0;

	for(int i=0;i<iNumRefs;i++)
	{ 
		RefTargetHandle handle = pPatrolNode->GetReference(i);

		if(!handle)
		{
			continue;
		}

		Object* pObject = static_cast<Object*>(handle);

		if(pObject->ClassID() == PATROL_LINK_CLASS_ID)
		{
			iCurrentIndex++;
		}

		if(iCurrentIndex == iIndex)
		{
			pLink = pObject;
			break;
		}
	}

	if(!pLink)
		throw RuntimeError ("Couldnt find link for index: ", arg_list[1]);	
	

	//get the linked connection
#if (MAX_RELEASE >= 12000)
	DependentIterator pItem(pLink);
	ReferenceMaker* maker = NULL;

	while(NULL != (maker = pItem.Next()))
	{
		if(maker->ClassID() == PATROL_NODE_CLASS_ID && maker != pPatrolNode)
		{
			pPatrolLinkedNode = static_cast<PatrolNode*>(maker);
			break;
		}
	}
#else
	RefListItem *pItem = pLink->GetRefList().FirstItem();

	while(pItem)
	{
		if(pItem->maker->ClassID() == PATROL_NODE_CLASS_ID && pItem->maker != pPatrolNode)
		{
			pPatrolLinkedNode = static_cast<PatrolNode*>(pItem->maker);
			break;
		}
		pItem = pItem->next;
	}
#endif  //MAX_RELEASE >= 12000

	//we now have the two aiconnections, we now need to find the distance between them
	//so we need to get the INodes for each object
	INode* p_NodeA = getINode(pPatrolNode);
	INode* p_NodeB = getINode(pPatrolLinkedNode);

	if(!p_NodeA || !p_NodeB)
		throw RuntimeError ("problem getting INode from PatrolNode: ", arg_list[0]);		

	return_protected(new MAXNode(p_NodeB));
}

PatrolNode* getNthPatrolNode(Object* p_Obj,int Index)
{
	int Count = 0;

	PatrolNode* pVehicleLinkedNode = NULL;

#if (MAX_RELEASE >= 12000)
	DependentIterator pItem(p_Obj);
	ReferenceMaker* maker = NULL;

	while(NULL != (maker = pItem.Next()))
	{
		if(maker->ClassID() == PATROL_NODE_CLASS_ID)
		{
			if(Count == Index)
				pVehicleLinkedNode = static_cast<PatrolNode*>(maker);

			Count++;
		}
	}
#else
	RefListItem *pItem = p_Obj->GetRefList().FirstItem();

	while(pItem)
	{
		if(pItem->maker->ClassID() == PATROL_NODE_CLASS_ID)
		{
			if(Count == Index)
				pVehicleLinkedNode = static_cast<PatrolNode*>(pItem->maker);

			Count++;
		}
		pItem = pItem->next;
	}
#endif  //MAX_RELEASE >= 12000

	return pVehicleLinkedNode;
}

/////////////////////////////////////////////////////////////////////////////////////
Value *patrollink_getnodea_cf(Value** arg_list, int count)
{
	type_check(arg_list[0], MAXNode, "PatrolLinkGetNodeA <patrollink>");
	INode *pNode = arg_list[0]->to_node();
	Object *pObj = pNode->EvalWorldState(0).obj;

	if(pObj->ClassID() != PATROL_LINK_CLASS_ID)
		throw RuntimeError ("PatrolLinkGetNodeA requires an PatrolLink: ", arg_list[0]);

	PatrolNode* pPatrolLinkedNode = getNthPatrolNode(pObj,0);

	if(!pPatrolLinkedNode)
		return &undefined;

	INode* p_NodeA = getINode(pPatrolLinkedNode);

	return_protected(new MAXNode(p_NodeA));
}

/////////////////////////////////////////////////////////////////////////////////////
Value *patrollink_getnodeb_cf(Value** arg_list, int count)
{
	type_check(arg_list[0], MAXNode, "PatrolLinkGetNodeA <patrollink>");
	INode *pNode = arg_list[0]->to_node();
	Object *pObj = pNode->EvalWorldState(0).obj;

	if(pObj->ClassID() != PATROL_LINK_CLASS_ID)
		throw RuntimeError ("PatrolLinkGetNodeA requires an PatrolLink: ", arg_list[0]);

	PatrolNode* pPatrolLinkedNode = getNthPatrolNode(pObj,1);

	if(!pPatrolLinkedNode)
		return &undefined;

	INode* p_NodeA = getINode(pPatrolLinkedNode);

	return_protected(new MAXNode(p_NodeA));
}