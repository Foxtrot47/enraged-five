//
//
//    Filename: BoxObj2.cpp
//     Creator: 
//     $Author: Adam $
//       $Date: 3/06/99 14:36 $
//   $Revision: 2 $
// Description: A box object plugin. This is the code from the standard box object with a few of my
//				extras added
//
//

#include "BoxObj2.h"
//#include "surf_api.h"



#define BOTTOMPIV

#define MIN_SEGMENTS	1
#define MAX_SEGMENTS	200

// --- BoxObj2ClassDesc -----------------------------------------------------------------------------

//static BoxObj2ClassDesc boxObj2Desc;
static BoxObj2ClassDesc2 boxObj2Desc2;
//ClassDesc* GetBoxObj2Desc() { return &boxObj2Desc; }
ClassDesc* GetBoxObj2Desc() { return &boxObj2Desc2; }

void BoxObj2ClassDesc2::ResetClassParams(BOOL fileReset)
{
	BoxObject2::crtWidth   = 0.0f; 
	BoxObject2::crtHeight  = 0.0f;
	BoxObject2::crtLength  = 0.0f;
	BoxObject2::createMeth = 0;
	BoxObject2::crtPos     = Point3(0,0,0);
}

// static class variable for BoxObject2 class.
IObjParam *BoxObject2::ip       = NULL;
/*IParamMap *BoxObject2::pmapCreate = NULL;
IParamMap *BoxObject2::pmapTypeIn = NULL;
IParamMap *BoxObject2::pmapParam  = NULL;	*/
Point3 BoxObject2::crtPos         = Point3(0,0,0);		
float BoxObject2::crtWidth        = 0.0f; 
float BoxObject2::crtHeight       = 0.0f;
float BoxObject2::crtLength       = 0.0f;
int BoxObject2::createMeth        = 0;

//--- Parameter map/block descriptors -------------------------------

// Non-parameter block indices
#define PB_CREATEMETHOD		0
#define PB_TI_POS			1
#define PB_TI_LENGTH		2
#define PB_TI_WIDTH			3
#define PB_TI_HEIGHT		4


//
//
//	Creation method

static int createMethIDs[] = {IDC_CREATEBOX,IDC_CREATECUBE};

static ParamUIDesc descCreate[] = {
	// Diameter/radius
	ParamUIDesc(PB_CREATEMETHOD,TYPE_RADIO,createMethIDs,2)
};
#define CREATEDESC_LENGH 1



//
//
// Type in
/*static ParamUIDesc descTypeIn[] = {
	
	// Position
	ParamUIDesc(
		PB_TI_POS,
		EDITTYPE_UNIVERSE,
		IDC_TI_POSX,IDC_TI_POSXSPIN,
		IDC_TI_POSY,IDC_TI_POSYSPIN,
		IDC_TI_POSZ,IDC_TI_POSZSPIN,
		float(-1.0E30),float(1.0E30),
		SPIN_AUTOSCALE),
		
		// Length
		ParamUIDesc(
		PB_TI_LENGTH,
		EDITTYPE_UNIVERSE,
		IDC_LENGTHEDIT,IDC_LENSPINNER,
		BMIN_LENGTH,BMAX_LENGTH,
		SPIN_AUTOSCALE),
		
		// Width
		ParamUIDesc(
		PB_TI_WIDTH,
		EDITTYPE_UNIVERSE,
		IDC_WIDTHEDIT,IDC_WIDTHSPINNER,
		BMIN_WIDTH,BMAX_WIDTH,
		SPIN_AUTOSCALE),	
		
		// Height
		ParamUIDesc(
		PB_TI_HEIGHT,
		EDITTYPE_UNIVERSE,
		IDC_HEIGHTEDIT,IDC_HEIGHTSPINNER,
		BMIN_HEIGHT,BMAX_HEIGHT,
		SPIN_AUTOSCALE),	
};

#define TYPEINDESC_LENGH 4
*/

// old parameter block
ParamBlockDescID descVer0[] = {
	{ TYPE_FLOAT, NULL, TRUE, 0 },	// length
	{ TYPE_FLOAT, NULL, TRUE, 1 },	// width 
	{ TYPE_FLOAT, NULL, TRUE, 2 },	// height
	{ TYPE_INT, NULL, TRUE, 3 },	// width segments	
	{ TYPE_INT, NULL, TRUE, 4 },	// length segments
	{ TYPE_INT, NULL, TRUE, 5 },	// height segments
	{ TYPE_INT, NULL, FALSE, 6 },	// generate UVs
	{ TYPE_BOOL, NULL, FALSE, 7 },	// display top
	{ TYPE_BOOL, NULL, FALSE, 8 },	// display bottom
	{ TYPE_BOOL, NULL, FALSE, 9 },	// display front
	{ TYPE_BOOL, NULL, FALSE, 10 },	// display back
	{ TYPE_BOOL, NULL, FALSE, 11 },	// display left
	{ TYPE_BOOL, NULL, FALSE, 12 }	// display right
};

#define PBLOCK_LENGTH	13
#define CURRENT_VERSION	1

static ParamVersionDesc oldPBVersion(descVer0,PBLOCK_LENGTH,CURRENT_VERSION);

//
// Max3 paramblock2 version
//
ParamBlockDesc2 boxDesc (box2_params, _T("BoxObj2 parameters"), IDS_CLASS_NAME, &boxObj2Desc2, P_AUTO_CONSTRUCT | P_AUTO_UI, 0,
						 IDD_BOXPARAM2, IDS_PARAMS, 0, 0, NULL,
						 box2_length, _T("Length"), TYPE_WORLD, P_ANIMATABLE, IDS_LENGTH,
							p_default,		10.0f,
							p_range, 		0.0f, float(1.0E30), 
							p_ui,			TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_LENGTHEDIT, IDC_LENSPINNER, 0.1f,
							end,
						 box2_width, _T("Width"), TYPE_WORLD, P_ANIMATABLE, IDS_WIDTH,
							p_default,		10.0f,
							p_range, 		0.0f, float(1.0E30), 
							p_ui,			TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_WIDTHEDIT, IDC_WIDTHSPINNER, 0.1f,
							end, 
						 box2_height, _T("Height"), TYPE_WORLD, P_ANIMATABLE, IDS_HEIGHT,
							p_default,		10.0f,
							p_range, 		float(-1.0E30), float(1.0E30), 
							p_ui,			TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_HEIGHTEDIT, IDC_HEIGHTSPINNER, 0.1f,
							end,
						 box2_widthSegs, _T("WidthSegments"), TYPE_INT, P_ANIMATABLE, IDS_WSEGS,
							p_default,		MIN_SEGMENTS,
							p_range, 		MIN_SEGMENTS, MAX_SEGMENTS, 
							p_dim,			defaultDim,
							p_ui,			TYPE_SPINNER, EDITTYPE_POS_INT, IDC_WSEGS, IDC_WSEGSPIN, 1.0f,
							end,
						 box2_lengthSegs, _T("LengthSegments"), TYPE_INT, P_ANIMATABLE, IDS_LSEGS,
							p_default,		MIN_SEGMENTS,
							p_range, 		MIN_SEGMENTS, MAX_SEGMENTS, 
							p_dim,			defaultDim,
							p_ui,			TYPE_SPINNER, EDITTYPE_POS_INT, IDC_LSEGS, IDC_LSEGSPIN, 1.0f,
							end,
						 box2_heightSegs, _T("HeightSegments"), TYPE_INT, P_ANIMATABLE, IDS_HSEGS,
							p_default,		MIN_SEGMENTS,
							p_range, 		MIN_SEGMENTS, MAX_SEGMENTS, 
							p_dim,			defaultDim,
							p_ui,			TYPE_SPINNER, EDITTYPE_POS_INT, IDC_HSEGS, IDC_HSEGSPIN, 1.0f,
							end,
						 box2_genUVs, _T("GenerateUVs"), TYPE_BOOL, 0, IDS_GENUVS,
							p_default,		0,
							p_ui,			TYPE_SINGLECHEKBOX, IDC_GENTEXTURE,
							end,
						 box2_top, _T("Top"), TYPE_BOOL, P_ANIMATABLE, IDS_TOP,
							p_default,		TRUE,
							p_ui,			TYPE_SINGLECHEKBOX, IDC_TOP_CHECK,
							end,
						 box2_bottom, _T("Bottom"), TYPE_BOOL, P_ANIMATABLE, IDS_BOTTOM,
							p_default,		TRUE,
							p_ui,			TYPE_SINGLECHEKBOX, IDC_BOTTOM_CHECK,
							end,
						 box2_front, _T("Front"), TYPE_BOOL, P_ANIMATABLE, IDS_FRONT,
							p_default,		TRUE,
							p_ui,			TYPE_SINGLECHEKBOX, IDC_FRONT_CHECK,
							end,
						 box2_back, _T("Back"), TYPE_BOOL, P_ANIMATABLE, IDS_BACK,
							p_default,		TRUE,
							p_ui,			TYPE_SINGLECHEKBOX, IDC_BACK_CHECK,
							end,
						 box2_left, _T("Left"), TYPE_BOOL, P_ANIMATABLE, IDS_LEFT,
							p_default,		TRUE,
							p_ui,			TYPE_SINGLECHEKBOX, IDC_LEFT_CHECK,
							end,
						 box2_right, _T("Right"), TYPE_BOOL, P_ANIMATABLE, IDS_RIGHT,
							p_default,		TRUE,
							p_ui,			TYPE_SINGLECHEKBOX, IDC_RIGHT_CHECK,
							end,
						 end
						 );


//--- TypeInDlgProc --------------------------------

/*class BoxTypeInDlgProc : public ParamMapUserDlgProc {
public:
	BoxObject2 *ob;
	
	BoxTypeInDlgProc(BoxObject2 *o) {ob=o;}
	BOOL DlgProc(TimeValue t,IParamMap *map,HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam);
	void DeleteThis() {delete this;}
};

BOOL BoxTypeInDlgProc::DlgProc(TimeValue t,IParamMap *map,HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
	switch (msg) {
	case CC_SPINNER_CHANGE:
		switch (LOWORD(wParam)) {
		case IDC_LENSPINNER:
		case IDC_WIDTHSPINNER:
		case IDC_HEIGHTSPINNER:
			if (ob->createMeth) {
				ISpinnerControl *spin = (ISpinnerControl*)lParam;
				ob->crtLength = ob->crtWidth = ob->crtHeight =
					spin->GetFVal();
				map->Invalidate();
			}
			break;
		}
		break;
		
		case WM_COMMAND:
			switch (LOWORD(wParam)) {
			case IDC_TI_CREATE: {					
				// We only want to set the value if the object is 
				// not in the scene.
				if (ob->TestAFlag(A_OBJ_CREATING)) {
					ob->pblock2->SetValue(box2_length,0,ob->crtLength);
					ob->pblock2->SetValue(box2_width,0,ob->crtWidth);
					ob->pblock2->SetValue(box2_height,0,ob->crtHeight);
				}
				
				Matrix3 tm(1);
				tm.SetTrans(ob->crtPos);					
				ob->suspendSnap = FALSE;
				ob->ip->NonMouseCreate(tm);					
				// NOTE that calling NonMouseCreate will cause this
				// object to be deleted. DO NOT DO ANYTHING BUT RETURN.
				return TRUE;	
								}
			}
			break;	
	}
	return FALSE;
}

*/
//--- Box methods -------------------------------


BoxObject2::BoxObject2()
{
	boxObj2Desc2.MakeAutoParamBlocks(this);
}

IOResult BoxObject2::Load(ILoad *iload)
{
	iload->RegisterPostLoadCallback(new ParamBlock2PLCB(&oldPBVersion, 1, &boxDesc, this, 0));
	return IO_OK;
}

void BoxObject2::BeginEditParams( IObjParam *ip, ULONG flags,Animatable *prev )
{
	SimpleObject2::BeginEditParams(ip,flags,prev);
	boxObj2Desc2.BeginEditParams(ip, this, flags, prev);
	this->ip = ip;
	
/*	if (pmapCreate && pmapParam) {
		
		// Left over from last Box created
		pmapCreate->SetParamBlock(this);
		pmapTypeIn->SetParamBlock(this);
		pmapParam->SetParamBlock(pblock);
	} else {
		
		// Gotta make a new one.
		if (flags&BEGIN_EDIT_CREATE) {
			pmapCreate = CreateCPParamMap(
				descCreate,CREATEDESC_LENGH,
				this,
				ip,
				hInstance,
				MAKEINTRESOURCE(IDD_BOXPARAM1),
				GetString(IDS_CREATIONMETHOD),
				0);
			
			pmapTypeIn = CreateCPParamMap(
				descTypeIn,TYPEINDESC_LENGH,
				this,
				ip,
				hInstance,
				MAKEINTRESOURCE(IDD_BOXPARAM3),
				GetString(IDS_KEYBOARDENTRY),
				APPENDROLL_CLOSED);			
		}
		
		pmapParam = CreateCPParamMap(
			descParam,PARAMDESC_LENGH,
			pblock,
			ip,
			hInstance,
			MAKEINTRESOURCE(IDD_BOXPARAM2),
			GetString(IDS_PARAMS),
			0);
	}
	
	if(pmapTypeIn) {
		// A callback for the type in.
		pmapTypeIn->SetUserDlgProc(new BoxTypeInDlgProc(this));
	}*/
}

void BoxObject2::EndEditParams( IObjParam *ip, ULONG flags,Animatable *next )
{
	SimpleObject2::EndEditParams(ip,flags,next);
	boxObj2Desc2.EndEditParams(ip, this, flags, next);
	this->ip = NULL;
	
/*	if (flags&END_EDIT_REMOVEUI ) {
		if (pmapCreate) DestroyCPParamMap(pmapCreate);
		if (pmapTypeIn) DestroyCPParamMap(pmapTypeIn);
		DestroyCPParamMap(pmapParam);
		pmapParam  = NULL;
		pmapTypeIn = NULL;
		pmapCreate = NULL;
	}
*/	
}

// vertices ( a b c d ) are in counter clockwise order when viewd from 
// outside the surface unless bias!=0 in which case they are clockwise
static void MakeQuad(int nverts, Face *f, int a, int b , int c , int d, int sg, int bias, int flag) {
	int sm = 1<<sg;
	assert(a<nverts);
	assert(b<nverts);
	assert(c<nverts);
	assert(d<nverts);
	if (bias) {
		f[0].setVerts( b, a, c);
		f[0].setSmGroup(sm);
		f[0].setEdgeVisFlags(1,0,1);
		f[1].setVerts( d, c, a);
		f[1].setSmGroup(sm);
		f[1].setEdgeVisFlags(1,0,1);
	} else {
		f[0].setVerts( a, b, c);
		f[0].setSmGroup(sm);
		f[0].setEdgeVisFlags(1,1,0);
		f[1].setVerts( c, d, a);
		f[1].setSmGroup(sm);
		f[1].setEdgeVisFlags(1,1,0);
	}
	if(f)
	{
		f[0].flags |= flag;
		f[1].flags |= flag;
	}
}


#define POSX 0	// right
#define POSY 1	// back
#define POSZ 2	// top
#define NEGX 3	// left
#define NEGY 4	// front
#define NEGZ 5	// bottom

int direction(Point3 *v) {
	Point3 a = v[0]-v[2];
	Point3 b = v[1]-v[0];
	Point3 n = CrossProd(a,b);
	switch(MaxComponent(n)) {
	case 0: return (n.x<0)?NEGX:POSX;
	case 1: return (n.y<0)?NEGY:POSY;
	case 2: return (n.z<0)?NEGZ:POSZ;
	}
	return 0;
}

// Remap the sub-object material numbers so that the top face is the first one
// The order now is:
// Top / Bottom /  Left/ Right / Front / Back
static int mapDir[6] ={ 3, 5, 0, 2, 4, 1 };

#define MAKE_QUAD(na,nb,nc,nd,sm,b, f) {MakeQuad(nverts,&(mesh.faces[nf]),na, nb, nc, nd, sm, b, f);nf+=2;}

BOOL BoxObject2::HasUVW() { 
	BOOL genUVs;
	Interval v;
	pblock2->GetValue(box2_genUVs, 0, genUVs, v);
	return genUVs; 
}

void BoxObject2::SetGenUVW(BOOL sw) {  
	if (sw==HasUVW()) return;
	pblock2->SetValue(box2_genUVs,0, sw);				
}


void BoxObject2::BuildMesh(TimeValue t)
{
	int ix,iy,iz,nf,kv,mv,nlayer,topStart,midStart;
	int nverts,wsegs,lsegs,hsegs,nv,nextk,nextm,wsp1;
	BOOL top, bottom, front, back, left, right;
	int workFlag = 0;
	int nfaces;
	Point3 va,vb,p;
	float l, w, h;
	int genUVs = 1;
	BOOL bias = 0;
	
	// Start the validity interval at forever and widdle it down.
	ivalid = FOREVER;	
	pblock2->GetValue(box2_length,t,l,ivalid);
	pblock2->GetValue(box2_width,t,w,ivalid);
	pblock2->GetValue(box2_height,t,h,ivalid);
	pblock2->GetValue(box2_lengthSegs,t,lsegs,ivalid);
	pblock2->GetValue(box2_widthSegs,t,wsegs,ivalid);
	pblock2->GetValue(box2_heightSegs,t,hsegs,ivalid);
	pblock2->GetValue(box2_genUVs,t,genUVs,ivalid);

	pblock2->GetValue(box2_top,t,top,ivalid);
	pblock2->GetValue(box2_bottom,t,bottom,ivalid);
	pblock2->GetValue(box2_front,t,front,ivalid);
	pblock2->GetValue(box2_back,t,back,ivalid);
	pblock2->GetValue(box2_left,t,left,ivalid);
	pblock2->GetValue(box2_right,t,right,ivalid);

	if (h<0.0f) bias = 1;
	
	LimitValue(lsegs, MIN_SEGMENTS, MAX_SEGMENTS);
	LimitValue(wsegs, MIN_SEGMENTS, MAX_SEGMENTS);
	LimitValue(hsegs, MIN_SEGMENTS, MAX_SEGMENTS);
	
	// Number of verts
	// bottom : (lsegs+1)*(wsegs+1)
	// top    : (lsegs+1)*(wsegs+1)
	// sides  : (2*lsegs+2*wsegs)*(hsegs-1)
	
	// Number of rectangular faces.
	// bottom : (lsegs)*(wsegs)
	// top    : (lsegs)*(wsegs)
	// sides  : 2*(hsegs*lsegs)+2*(wsegs*lsegs)
	
	wsp1 = wsegs + 1;
	nlayer  =  2*(lsegs+wsegs);
	topStart = (lsegs+1)*(wsegs+1);
	midStart = 2*topStart;
	
	nverts = midStart+nlayer*(hsegs-1);
	nfaces = 4*(lsegs*wsegs + hsegs*lsegs + wsegs*hsegs);
	
	mesh.setNumVerts(nverts);
	mesh.setNumFaces(nfaces);
	mesh.InvalidateTopologyCache();
	
	nv = 0;
	
	vb =  Point3(w,l,h)/float(2);   
	va = -vb;
	
#ifdef BOTTOMPIV
	va.z = float(0);
	vb.z = h;
#endif
	
	float dx = w/wsegs;
	float dy = l/lsegs;
	float dz = h/hsegs;
	
	// do bottom vertices.
	p.z = va.z;
	p.y = va.y;
	for(iy=0; iy<=lsegs; iy++) {
		p.x = va.x;
		for (ix=0; ix<=wsegs; ix++) {
			mesh.setVert(nv++, p);
			p.x += dx;
		}
		p.y += dy;
	}
	
	nf = 0;

	if(!bottom)
		workFlag = FACE_WORK;
	else
		workFlag = 0;

	// do bottom faces.
	for(iy=0; iy<lsegs; iy++) {
		kv = iy*(wsegs+1);
		for (ix=0; ix<wsegs; ix++) {
			MAKE_QUAD(kv, kv+wsegs+1, kv+wsegs+2, kv+1, 1, bias, workFlag);
			kv++;
		}
	}
	assert(nf==lsegs*wsegs*2);
	
	// do top vertices.
	p.z = vb.z;
	p.y = va.y;
	for(iy=0; iy<=lsegs; iy++) {
		p.x = va.x;
		for (ix=0; ix<=wsegs; ix++) {
			mesh.setVert(nv++, p);
			p.x += dx;
		}
		p.y += dy;
	}
	
	if(!top)
		workFlag = FACE_WORK;
	else
		workFlag = 0;

	// do top faces (lsegs*wsegs);
	for(iy=0; iy<lsegs; iy++) {
		kv = iy*(wsegs+1)+topStart;
		for (ix=0; ix<wsegs; ix++) {
			MAKE_QUAD(kv, kv+1, kv+wsegs+2,kv+wsegs+1, 2, bias, workFlag);
			kv++;
		}
	}
	assert(nf==lsegs*wsegs*4);
	
	// do middle vertices 
	for(iz=1; iz<hsegs; iz++) {
		
		p.z = va.z + dz * iz;
		
		// front edge
		p.x = va.x;  p.y = va.y;
		for (ix=0; ix<wsegs; ix++) { mesh.setVert(nv++, p);  p.x += dx;	}
		
		// right edge
		p.x = vb.x;	  p.y = va.y;
		for (iy=0; iy<lsegs; iy++) { mesh.setVert(nv++, p);  p.y += dy;	}
		
		// back edge
		p.x =  vb.x;  p.y =  vb.y;
		for (ix=0; ix<wsegs; ix++) { mesh.setVert(nv++, p);	 p.x -= dx;	}
		
		// left edge
		p.x = va.x;  p.y =  vb.y;
		for (iy=0; iy<lsegs; iy++) { mesh.setVert(nv++, p);	 p.y -= dy;	}
	}
	
	if (hsegs==1) {

		if(!front)
			workFlag = FACE_WORK;
		else
			workFlag = 0;


		// do LEFT faces -----------------------
		kv = 0;
		mv = topStart;
		for (ix=0; ix<wsegs; ix++) {
			MAKE_QUAD(kv, kv+1, mv+1, mv, 5, bias, workFlag);
			kv++;
			mv++;
		}
		
		if(!right)
			workFlag = FACE_WORK;
		else
			workFlag = 0;

		// do RIGHT faces.-----------------------
		kv = wsegs;  
		mv = topStart + kv;
		for (iy=0; iy<lsegs; iy++) {
			MAKE_QUAD(kv, kv+wsp1, mv+wsp1, mv, 4, bias, workFlag);
			kv += wsp1;
			mv += wsp1;
		}	
		
		if(!back)
			workFlag = FACE_WORK;
		else
			workFlag = 0;

		// do BACK faces.-----------------------
		kv = topStart - 1;
		mv = midStart - 1;
		for (ix=0; ix<wsegs; ix++) {
			MAKE_QUAD(kv, kv-1, mv-1, mv, 5, bias, workFlag);
			kv --;
			mv --;
		}
		
		if(!left)
			workFlag = FACE_WORK;
		else
			workFlag = 0;

		// do LEFT faces.----------------------
		kv = lsegs*(wsegs+1);  // index into bottom
		mv = topStart + kv;
		for (iy=0; iy<lsegs; iy++) {
			MAKE_QUAD(kv, kv-wsp1, mv-wsp1, mv, 6, bias, workFlag);
			kv -= wsp1;
			mv -= wsp1;
		}
	}
	else 
	{
		if(!front)
			workFlag = FACE_WORK;
		else
			workFlag = 0;

		// do front faces.
		kv = 0;
		mv = midStart;
		for(iz=0; iz<hsegs; iz++) {
			if (iz==hsegs-1) mv = topStart;
			for (ix=0; ix<wsegs; ix++) 
				MAKE_QUAD(kv+ix, kv+ix+1, mv+ix+1, mv+ix, 3, bias, workFlag);
			kv = mv;
			mv += nlayer;
		}
		
		assert(nf==lsegs*wsegs*4 + wsegs*hsegs*2);
		
		if(!right)
			workFlag = FACE_WORK;
		else
			workFlag = 0;

		// do RIGHT faces.-------------------------
		// RIGHT bottom row:
		kv = wsegs; // into bottom layer. 
		mv = midStart + wsegs; // first layer of mid verts
		
		
		for (iy=0; iy<lsegs; iy++) {
			MAKE_QUAD(kv, kv+wsp1, mv+1, mv, 4, bias, workFlag);
			kv += wsp1;
			mv ++;
		}
		
		// RIGHT middle part:
		kv = midStart + wsegs; 
		for(iz=0; iz<hsegs-2; iz++) {
			mv = kv + nlayer;
			for (iy=0; iy<lsegs; iy++) {
				MAKE_QUAD(kv+iy, kv+iy+1, mv+iy+1, mv+iy, 4, bias, workFlag);
			}
			kv += nlayer;
		}
		
		// RIGHT top row:
		kv = midStart + wsegs + (hsegs-2)*nlayer; 
		mv = topStart + wsegs;
		for (iy=0; iy<lsegs; iy++) {
			MAKE_QUAD(kv, kv+1, mv+wsp1, mv, 4, bias, workFlag);
			mv += wsp1;
			kv++;
		}
		
		assert(nf==lsegs*wsegs*4 + wsegs*hsegs*2 + lsegs*hsegs*2);
		
		if(!back)
			workFlag = FACE_WORK;
		else
			workFlag = 0;

		// do BACK faces. ---------------------
		// BACK bottom row:
		kv = topStart - 1;
		mv = midStart + wsegs + lsegs;
		for (ix=0; ix<wsegs; ix++) {
			MAKE_QUAD(kv, kv-1, mv+1, mv, 5, bias, workFlag);
			kv --;
			mv ++;
		}
		
		// BACK middle part:
		kv = midStart + wsegs + lsegs; 
		for(iz=0; iz<hsegs-2; iz++) {
			mv = kv + nlayer;
			for (ix=0; ix<wsegs; ix++) {
				MAKE_QUAD(kv+ix, kv+ix+1, mv+ix+1, mv+ix, 5, bias, workFlag);
			}
			kv += nlayer;
		}
		
		// BACK top row:
		kv = midStart + wsegs + lsegs + (hsegs-2)*nlayer; 
		mv = topStart + lsegs*(wsegs+1)+wsegs;
		for (ix=0; ix<wsegs; ix++) {
			MAKE_QUAD(kv, kv+1, mv-1, mv, 5, bias, workFlag);
			mv --;
			kv ++;
		}
		
		assert(nf==lsegs*wsegs*4 + wsegs*hsegs*4 + lsegs*hsegs*2);
		
		if(!left)
			workFlag = FACE_WORK;
		else
			workFlag = 0;

		// do LEFT faces. -----------------
		// LEFT bottom row:
		kv = lsegs*(wsegs+1);  // index into bottom
		mv = midStart + 2*wsegs +lsegs;
		for (iy=0; iy<lsegs; iy++) {
			nextm = mv+1;
			if (iy==lsegs-1) 
				nextm -= nlayer;
			MAKE_QUAD(kv, kv-wsp1, nextm, mv, 6, bias, workFlag);
			kv -=wsp1;
			mv ++;
		}
		
		// LEFT middle part:
		kv = midStart + 2*wsegs + lsegs; 
		for(iz=0; iz<hsegs-2; iz++) {
			mv = kv + nlayer;
			for (iy=0; iy<lsegs; iy++) {
				nextm = mv+1;
				nextk = kv+iy+1;
				if (iy==lsegs-1) { 
					nextm -= nlayer;
					nextk -= nlayer;
				}
				MAKE_QUAD(kv+iy, nextk, nextm, mv, 6, bias, workFlag);
				mv++;
			}
			kv += nlayer;
		}
		
		// LEFT top row:
		kv = midStart + 2*wsegs + lsegs+ (hsegs-2)*nlayer; 
		mv = topStart + lsegs*(wsegs+1);
		for (iy=0; iy<lsegs; iy++) {
			nextk = kv+1;
			if (iy==lsegs-1) 
				nextk -= nlayer;
			MAKE_QUAD(kv, nextk, mv-wsp1, mv, 6, bias, workFlag);
			mv -= wsp1;
			kv++;
		}
	}
	
	if (genUVs) {
		int ls = lsegs+1;
		int ws = wsegs+1;
		int hs = hsegs+1;
		int ntverts = ls*hs + hs*ws + ws*ls ;
		mesh.setNumTVerts( ntverts ) ;
		mesh.setNumTVFaces(nfaces);		
		
		int xbase = 0;
		int ybase = ls*hs;
		int zbase = ls*hs + hs*ws;
		
		float dw = 1.0f/float(wsegs);
		float dl = 1.0f/float(lsegs);
		float dh = 1.0f/float(hsegs);
		
		if (w==0.0f) w = .0001f;
		if (l==0.0f) l = .0001f;
		if (h==0.0f) h = .0001f;
		float u,v;
		
		nv = 0;
		v = 0.0f;
		// X axis face
		for (iz =0; iz<hs; iz++) {
			u = 0.0f; 
			for (iy =0; iy<ls; iy++) {
				mesh.setTVert(nv, u, v, 0.0f);
				nv++; u+=dl;
			}
			v += dh;
		}
		
		v = 0.0f; 
		//Y Axis face
		for (iz =0; iz<hs; iz++) {
			u = 0.0f;
			for (ix =0; ix<ws; ix++) {
				mesh.setTVert(nv, u, v, 0.0f);
				nv++; u+=dw;
			}
			v += dh;
		}
		
		v = 0.0f; 
		for (iy =0; iy<ls; iy++) {
			u = 0.0f; 
			for (ix =0; ix<ws; ix++) {
				mesh.setTVert(nv, u, v, 0.0f);
				nv++; u+=dw;
			}
			v += dl;
		}
		
		assert(nv==ntverts);
		
		for (nf = 0; nf<nfaces; nf++) {
			Face& f = mesh.faces[nf];
			DWORD* nv = f.getAllVerts();
			Point3 v[3];
			for (int ix =0; ix<3; ix++)
				v[ix] = mesh.getVert(nv[ix]);
			int dir = direction(v);
			int ntv[3];
			for (ix=0; ix<3; ix++) {
				int iu,iv;
				switch(dir) {
				case POSX: case NEGX:
					iu = int(((float)lsegs*(v[ix].y-va.y)/l)+.5f); 
					iv = int(((float)hsegs*(v[ix].z-va.z)/h)+.5f);  
					if (dir==NEGX) iu = lsegs-iu;
					ntv[ix] = (xbase + iv*ls + iu);
					break;
				case POSY: case NEGY:
					iu = int(((float)wsegs*(v[ix].x-va.x)/w)+.5f);  
					iv = int(((float)hsegs*(v[ix].z-va.z)/h)+.5f); 
					if (dir==POSY) iu = wsegs-iu;
					ntv[ix] = (ybase + iv*ws + iu);
					break;
				case POSZ: case NEGZ:
					iu = int(((float)wsegs*(v[ix].x-va.x)/w)+.5f);  
					iv = int(((float)lsegs*(v[ix].y-va.y)/l)+.5f); 
					if (dir==NEGZ) iu = wsegs-iu;
					ntv[ix] = (zbase + iv*ws + iu);
					break;
				}
			}
			assert(ntv[0]<ntverts);
			assert(ntv[1]<ntverts);
			assert(ntv[2]<ntverts);
			
			mesh.tvFace[nf].setTVerts(ntv[0],ntv[1],ntv[2]);
			mesh.setFaceMtlIndex(nf,mapDir[dir]);
		}
	}
	else 
	{
		mesh.setNumTVerts(0);
		mesh.setNumTVFaces(0);
		for (nf = 0; nf<nfaces; nf++) {
			Face& f = mesh.faces[nf];
			DWORD* nv = f.getAllVerts();
			Point3 v[3];
			for (int ix =0; ix<3; ix++)
				v[ix] = mesh.getVert(nv[ix]);
			int dir = direction(v);
			mesh.setFaceMtlIndex(nf,mapDir[dir]);
		}
	}

	mesh.DeleteFlaggedFaces();
	mesh.DeleteIsoVerts();

	mesh.InvalidateGeomCache();
	mesh.BuildStripsAndEdges();
}


/*Object* BuildNURBSBox(float width, float length, float height, int genUVs)
{
	int cube_faces[6][4] = {{0, 1, 2, 3}, // bottom
	{5, 4, 7, 6}, // top
	{2, 3, 6, 7}, // back
	{1, 0, 5, 4}, // front
	{3, 1, 7, 5}, // left
	{0, 2, 4, 6}};// right
	Point3 cube_verts[8] = {Point3(-0.5, -0.5, 0.0),
		Point3( 0.5, -0.5, 0.0),
		Point3(-0.5,  0.5, 0.0),
		Point3( 0.5,  0.5, 0.0),
		Point3(-0.5, -0.5, 1.0),
		Point3( 0.5, -0.5, 1.0),
		Point3(-0.5,  0.5, 1.0),
		Point3( 0.5,  0.5, 1.0)};
	
	NURBSSet nset;
	
	for (int face = 0; face < 6; face++) {
		Point3 bl = cube_verts[cube_faces[face][0]];
		Point3 br = cube_verts[cube_faces[face][1]];
		Point3 tl = cube_verts[cube_faces[face][2]];
		Point3 tr = cube_verts[cube_faces[face][3]];
		
		Matrix3 size;
		size.IdentityMatrix();
		Point3 lwh(width, length, height);
		size.Scale(lwh);
		
		bl = bl * size;
		br = br * size;
		tl = tl * size;
		tr = tr * size;
		
		NURBSCVSurface *surf = new NURBSCVSurface();
		nset.AppendObject(surf);
		surf->SetUOrder(4);
		surf->SetVOrder(4);
		surf->SetNumCVs(4, 4);
		surf->SetNumUKnots(8);
		surf->SetNumVKnots(8);
		
		Point3 top, bot;
		for (int r = 0; r < 4; r++) {
			top = tl + (((float)r/3.0f) * (tr - tl));
			bot = bl + (((float)r/3.0f) * (br - bl));
			for (int c = 0; c < 4; c++) {
				NURBSControlVertex ncv;
				ncv.SetPosition(0, bot + (((float)c/3.0f) * (top - bot)));
				ncv.SetWeight(0, 1.0f);
				char bname[40];
				char sname[40];
				strcpy(bname, GetString(IDS_OBJ_NAME));
				sprintf(sname, "%s%s%02dCV%02d", bname, GetString(IDS_SURFACE), face, r * 4 + c);
				ncv.SetName(sname);
				surf->SetCV(r, c, ncv);
			}
		}
		
		for (int k = 0; k < 4; k++) {
			surf->SetUKnot(k, 0.0);
			surf->SetVKnot(k, 0.0);
			surf->SetUKnot(k + 4, 1.0);
			surf->SetVKnot(k + 4, 1.0);
		}
		
		surf->Renderable(TRUE);
		surf->SetGenerateUVs(genUVs);
		if (height > 0.0f)
			surf->FlipNormals(TRUE);
		else
			surf->FlipNormals(FALSE);
		
		surf->SetTextureUVs(0, 0, Point2(1.0f, 0.0f));
		surf->SetTextureUVs(0, 1, Point2(0.0f, 0.0f));
		surf->SetTextureUVs(0, 2, Point2(1.0f, 1.0f));
		surf->SetTextureUVs(0, 3, Point2(0.0f, 1.0f));
		
		char bname[40];
		char sname[40];
		strcpy(bname, GetString(IDS_OBJ_NAME));
		sprintf(sname, "%s%s%02d", bname, GetString(IDS_SURFACE), face);
		surf->SetName(sname);
	}
	
#define F(s1, s2, s1r, s1c, s2r, s2c) \
	fuse.mSurf1 = (s1); \
	fuse.mSurf2 = (s2); \
	fuse.mRow1 = (s1r); \
	fuse.mCol1 = (s1c); \
	fuse.mRow2 = (s2r); \
	fuse.mCol2 = (s2c); \
	nset.mSurfFuse.Append(1, &fuse);
	
	NURBSFuseSurfaceCV fuse;
	
	// Bottom(0) to Back (2)
	F(0, 2, 3, 3, 3, 0);
	F(0, 2, 2, 3, 2, 0);
	F(0, 2, 1, 3, 1, 0);
	F(0, 2, 0, 3, 0, 0);
	
	// Top(1) to Back (2)
	F(1, 2, 0, 3, 3, 3);
	F(1, 2, 1, 3, 2, 3);
	F(1, 2, 2, 3, 1, 3);
	F(1, 2, 3, 3, 0, 3);
	
	// Bottom(0) to Front (3)
	F(0, 3, 0, 0, 3, 0);
	F(0, 3, 1, 0, 2, 0);
	F(0, 3, 2, 0, 1, 0);
	F(0, 3, 3, 0, 0, 0);
	
	// Top(1) to Front (3)
	F(1, 3, 3, 0, 3, 3);
	F(1, 3, 2, 0, 2, 3);
	F(1, 3, 1, 0, 1, 3);
	F(1, 3, 0, 0, 0, 3);
	
	// Bottom(0) to Left (4)
	F(0, 4, 3, 0, 3, 0);
	F(0, 4, 3, 1, 2, 0);
	F(0, 4, 3, 2, 1, 0);
	F(0, 4, 3, 3, 0, 0);
	
	// Top(1) to Left (4)
	F(1, 4, 0, 0, 3, 3);
	F(1, 4, 0, 1, 2, 3);
	F(1, 4, 0, 2, 1, 3);
	F(1, 4, 0, 3, 0, 3);
	
	// Bottom(0) to Right (5)
	F(0, 5, 0, 0, 0, 0);
	F(0, 5, 0, 1, 1, 0);
	F(0, 5, 0, 2, 2, 0);
	F(0, 5, 0, 3, 3, 0);
	
	// Top(1) to Right (5)
	F(1, 5, 3, 0, 0, 3);
	F(1, 5, 3, 1, 1, 3);
	F(1, 5, 3, 2, 2, 3);
	F(1, 5, 3, 3, 3, 3);
	
	// Front (3)  to Right (5)
	F(3, 5, 3, 1, 0, 1);
	F(3, 5, 3, 2, 0, 2);
	
	// Right (5) to Back (2)
	F(5, 2, 3, 1, 0, 1);
	F(5, 2, 3, 2, 0, 2);
	
	// Back (2) to Left (4)
	F(2, 4, 3, 1, 0, 1);
	F(2, 4, 3, 2, 0, 2);
	
	// Left (4) to Front (3)
	F(4, 3, 3, 1, 0, 1);
	F(4, 3, 3, 2, 0, 2);
	
	
	Matrix3 mat;
	mat.IdentityMatrix();
	Object *obj = CreateNURBSObject(NULL, &nset, mat);
	for (int i = 0; i < 6; i++)
		delete (NURBSCVSurface*)nset.GetNURBSObject(i);
	return obj;
}

*/

Object* BoxObject2::ConvertToType(TimeValue t, Class_ID obtype)
{
/*	if (obtype == EDITABLE_SURF_CLASS_ID) 
	{
		Interval valid = FOREVER;
		float length, width, height;
		int genUVs;
		pblock->GetValue(box2_length,t,length,valid);
		pblock->GetValue(box2_width,t,width,valid);
		pblock->GetValue(box2_height,t,height,valid);
		pblock->GetValue(box2_genUVs,t,genUVs,valid);
		Object *ob = BuildNURBSBox(width, length, height, genUVs);
		ob->SetChannelValidity(TOPO_CHAN_NUM,valid);
		ob->SetChannelValidity(GEOM_CHAN_NUM,valid);
		ob->UnlockObject();
		return ob;
	}
	else 
	{*/
		return SimpleObject2::ConvertToType(t,obtype);
	//}
}

int BoxObject2::CanConvertToType(Class_ID obtype)
{
//	if (obtype==EDITABLE_SURF_CLASS_ID) {
//		return 1;
//	} else {
		return SimpleObject2::CanConvertToType(obtype);
//	}
}

void BoxObject2::GetCollapseTypes(Tab<Class_ID> &clist, Tab<TSTR*> &nlist)
{
    Object::GetCollapseTypes(clist, nlist);
/*    Class_ID id = EDITABLE_SURF_CLASS_ID;
	TSTR *name = new TSTR(GetString(IDS_NURBS_SURFACE));
    clist.Append(1, &id, 1);
    nlist.Append(1, &name, 1);*/
}

class BoxObjCreateCallBack: public CreateMouseCallBack {
	BoxObject2 *ob;
	Point3 p0,p1;
	IPoint2 sp0, sp1;
	BOOL square;
public:
	int proc( ViewExp *vpt,int msg, int point, int flags, IPoint2 m, Matrix3& mat );
	void SetObj(BoxObject2 *obj) { ob = obj; }
};

int BoxObjCreateCallBack::proc(ViewExp *vpt,int msg, int point, int flags, IPoint2 m, Matrix3& mat ) {
	Point3 d;
	if (msg == MOUSE_FREEMOVE)
	{
		vpt->SnapPreview(m,m,NULL, SNAP_IN_3D);
	}
	else if (msg==MOUSE_POINT||msg==MOUSE_MOVE) 
	{
		switch(point) {
		case 0:
			sp0 = m;
			ob->pblock2->SetValue(box2_width,0,0.0f);
			ob->pblock2->SetValue(box2_length,0,0.0f);
			ob->pblock2->SetValue(box2_height,0,0.0f);
			ob->suspendSnap = TRUE;								
			p0 = vpt->SnapPoint(m,m,NULL,SNAP_IN_3D);
			p1 = p0 + Point3(.01,.01,.01);
			mat.SetTrans(float(.5)*(p0+p1));				
#ifdef BOTTOMPIV
			{
				Point3 xyz = mat.GetTrans();
				xyz.z = p0.z;
				mat.SetTrans(xyz);
			}
#endif
			break;
		case 1:
			sp1 = m;
			p1 = vpt->SnapPoint(m,m,NULL,SNAP_IN_3D);
			p1.z = p0.z +(float).01; 
			if (ob->createMeth || (flags&MOUSE_CTRL)) {
				mat.SetTrans(p0);
			} else {
				mat.SetTrans(float(.5)*(p0+p1));
#ifdef BOTTOMPIV 					
				Point3 xyz = mat.GetTrans();
				xyz.z = p0.z;
				mat.SetTrans(xyz);					
			}
#endif
			d = p1-p0;
			
			square = FALSE;
			if (ob->createMeth) {
				// Constrain to cube
				d.x = d.y = d.z = Length(d)*2.0f;
			} else 
				if (flags&MOUSE_CTRL) {
					// Constrain to square base
					float len;
					if (fabs(d.x) > fabs(d.y)) len = d.x;
					else len = d.y;
					d.x = d.y = 2.0f * len;
					square = TRUE;
				}
				
				ob->pblock2->SetValue(box2_width,0,float(fabs(d.x)));
				ob->pblock2->SetValue(box2_length,0,float(fabs(d.y)));
				ob->pblock2->SetValue(box2_height,0,float(fabs(d.z)));
				boxDesc.InvalidateUI();
				//ob->pmapParam->Invalidate();										
				
				if (msg==MOUSE_POINT && ob->createMeth) {
					ob->suspendSnap = FALSE;
					return (Length(sp1-sp0)<3)?CREATE_ABORT:CREATE_STOP;					
				} else if (msg==MOUSE_POINT && 
					(Length(sp1-sp0)<3 || Length(d)<0.1f)) {
					return CREATE_ABORT;
				}
				break;
		case 2:
#ifdef _OSNAP
			p1.z = p0.z + vpt->SnapLength(vpt->GetCPDisp(p0,Point3(0,0,1),sp1,m,TRUE));
#else
			p1.z = p0.z + vpt->SnapLength(vpt->GetCPDisp(p1,Point3(0,0,1),sp1,m));
#endif				
			if (!square) {
				mat.SetTrans(float(.5)*(p0+p1));
#ifdef BOTTOMPIV
				mat.SetTrans(2,p0.z); // set the Z component of translation
#endif
			}
			
			d = p1-p0;
			if (square) {
				// Constrain to square base
				float len;
				if (fabs(d.x) > fabs(d.y)) len = d.x;
				else len = d.y;
				d.x = d.y = 2.0f * len;					
			}
			
			ob->pblock2->SetValue(box2_width,0,float(fabs(d.x)));
			ob->pblock2->SetValue(box2_length,0,float(fabs(d.y)));
			ob->pblock2->SetValue(box2_height,0,float(d.z));
			boxDesc.InvalidateUI();
			
			if (msg==MOUSE_POINT) {
				ob->suspendSnap = FALSE;					
				return CREATE_STOP;
			}
			break;
		}
	}
	else if (msg == MOUSE_ABORT) {		
		return CREATE_ABORT;
	}
	
	return TRUE;
}

static BoxObjCreateCallBack boxCreateCB;

CreateMouseCallBack* BoxObject2::GetCreateMouseCallBack() {
	boxCreateCB.SetObj(this);
	return(&boxCreateCB);
}


BOOL BoxObject2::OKtoDisplay(TimeValue t) 
{
	return TRUE;
}


void BoxObject2::InvalidateUI() 
{
	IParamMap2 *pMapParam = pblock2->GetMap();
	if (pMapParam) 
		pMapParam->Invalidate();
}

RefTargetHandle BoxObject2::Clone(RemapDir& remap) 
{
	BoxObject2* newob = new BoxObject2();
	newob->ReplaceReference(0,pblock2->Clone(remap));
	newob->ivalid.SetEmpty();

	BaseClone(this, newob, remap);

	return(newob);
}

