//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Water.rc
//
#define IDS_LIBDESCRIPTION              1
#define IDS_CATEGORY                    2
#define IDS_WATER_NAME                  3
#define IDS_PARAMS                      4
#define IDS_SPIN                        5
#define IDS_LENGTH                      6
#define IDS_WIDTH                       7
#define IDS_HEIGHT                      8
#define IDS_TYPE                        8
#define IDS_BUTTSHAPE                   8
#define IDS_DOOR                        9
#define IDS_POINTHEIGHT1                9
#define IDS_LEFTSTART                   10
#define IDS_POINTHEIGHT2                10
#define IDS_LEFTEND                     11
#define IDS_POINTHEIGHT3                11
#define IDS_RIGHTSTART                  12
#define IDS_POINTHEIGHT4                12
#define IDS_RIGHTEND                    13
#define IDS_MESHPARAM                   13
#define IDS_TOPSTART                    14
#define IDS_SNAPGRID                    14
#define IDS_TOPEND                      15
#define IDS_BASE_CLASS_NAME             15
#define IDS_WATER_CALM_NAME             16
#define IDS_CALMING                     17
#define IDS_EXTENDED_PARAMS             18
#define IDS_POINTS                      19
#define IDS_WATER_WAVE_NAME             20
#define IDS_AMPLITUDE                   21
#define IDS_WAVE_X_DIRECTION            22
#define IDS_WAVE_Y_DIRECTION            23
#define IDS_WATER_CALM2_NAME            24
#define IDS_WAVE_DIRECTION              24
#define IDS_RIVER_HELPER_NAME           25
#define IDD_PANEL                       101
#define IDB_WATER_SHAPES                101
#define IDB_BITMAP1                     102
#define IDB_WATER_SHAPES_A              102
#define IDD_PANEL_CALMER                102
#define IDD_PANEL_RIVER                 103
#define IDD_PANEL_BASE                  104
#define IDD_Wave                        105
#define IDD_PANEL_WAVE                  105
#define IDD_DIALOG1                     107
#define IDC_CLOSEBUTTON                 1000
#define IDC_DOSTUFF                     1000
#define IDC_LEFTSTART_EDIT              1002
#define IDC_LEFTEND_EDIT                1003
#define IDC_RIGHTSTART_EDIT             1004
#define IDC_RIGHTEND_EDIT               1005
#define IDC_TOPSTART_EDIT               1006
#define IDC_TOPEND_EDIT                 1007
#define IDC_CHK_TRIANGLE                1008
#define IDC_BUTTSHAPE                   1009
#define IDC_AMPLITUDE                   1011
#define IDC_SPIN_X_DIRECTION            1014
#define IDC_Y_DIRECTION                 1015
#define IDC_SPIN_AMPLITUDE              1018
#define IDC_X_DIRECTION                 1019
#define IDC_CUSTOM3                     1020
#define IDC_SPIN_Y_DIRECTION            1020
#define IDC_WAVE_DIRECTION              1021
#define IDC_CUSTOM2                     1022
#define IDC_SPIN_WAVE_DIRECTION         1022
#define IDC_COLOR                       1456
#define IDC_EDIT                        1490
#define IDC_LENGTHEDIT                  1490
#define IDC_WIDTHEDIT                   1491
#define IDC_ALPHAEDIT1                  1491
#define IDC_HEIGHTEDIT                  1492
#define IDC_POINTHEIGHT1                1492
#define IDC_ALPHAEDIT2                  1492
#define IDC_DOOREDIT                    1493
#define IDC_POINTHEIGHT2                1493
#define IDC_ALPHAEDIT3                  1493
#define IDC_POINTHEIGHT3                1494
#define IDC_ALPHAEDIT4                  1494
#define IDC_POINTHEIGHT4                1495
#define IDC_SPIN                        1496
#define IDC_LENSPINNER                  1496
#define IDC_WIDTHSPINNER                1497
#define IDC_ALPHASPINNER1               1497
#define IDC_HEIGHTSPINNER               1498
#define IDC_POINTSPIN1                  1498
#define IDC_ALPHASPINNER2               1498
#define IDC_DOORSPINNER                 1499
#define IDC_POINTSPIN2                  1499
#define IDC_ALPHASPINNER3               1499
#define IDC_LEFTSTARTSPINNER            1500
#define IDC_POINTSPIN3                  1500
#define IDC_ALPHASPINNER4               1500
#define IDC_LEFTENDSPINNER              1501
#define IDC_POINTSPIN4                  1501
#define IDC_RIGHTSTARTSPINNER           1502
#define IDC_SNAPGRID                    1502
#define IDC_RIGHTENDSPINNER             1503
#define IDC_SPIN_SNAPGRID               1503
#define IDC_TOPSTARTSPINNER             1504
#define IDC_CALMING                     1504
#define IDC_TOPENDSPINNER               1505
#define IDC_SPIN_CALMING                1505

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        108
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1023
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
