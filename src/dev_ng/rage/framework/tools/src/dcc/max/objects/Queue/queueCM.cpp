//
//
//    Filename: queueCM.cpp
//     Creator: Greg Smith
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Methods from all the command modes used in creating a stunt jump
//
//

#include "queueCM.h"
#include "queueNode.h"
#include "queue.h"
#include "resource.h"

CreatePathCM theCreatePathCM;

//
//        name: ObjectArrayCreateManager::Begin
// description: Called at the beginning of creation
//          in:
//
void CreatePathMouseCallback::Begin( IObjCreate *ioc, ClassDesc *desc )
{
	m_ip = ioc;
	m_pNode = NULL;
	m_pqueue = NULL;
	m_isAttachedToNode = false;
	CreateNewPath();
}

//
//        name: ObjectArrayCreateManager::End
// description: Called once creation has finished
//
void CreatePathMouseCallback::End()
{
	if (m_pqueue) {
		m_pqueue->EndEditParams( (IObjParam*)m_ip, TRUE, NULL );
		if ( !m_isAttachedToNode )
		{
			delete m_pqueue;
			m_pqueue = NULL;
			// DS 8/21/97: If something has been put on the undo stack since this object was created,
			// we have to flush the undo stack.
			if (theHold.GetGlobalPutCount() != m_lastPutCount)
			{
				GetSystemSetting(SYSSET_CLEAR_UNDO);
			}
		} else if ( m_pNode ) {
			// Get rid of the references.
			DeleteAllRefsFromMe();
			m_pqueue = NULL;
		}
	}
}

//
//        name: ObjectArrayCreateManager::CreateNewArray
// description: Creates a new object array
//
void CreatePathMouseCallback::CreateNewPath()
{
	m_pqueue = new queue();
	//m_currentPathNode = m_pqueue->AddPathNode();
	m_currentPathNode = -1;
	m_previousPathNode = -1;
	m_numPathNodes = 1;
	m_isAttachedToNode = false;
	m_pqueue->BeginEditParams( (IObjParam*)m_ip, BEGIN_EDIT_CREATE,NULL );
	m_lastPutCount = theHold.GetGlobalPutCount();
}

//
//        name: GetReference,SetReference,NotifyRefChanged
// description: Reference functions for creation manager
//
RefTargetHandle CreatePathMouseCallback::GetReference(int i)
{
	if(i == 0)
		return m_pNode;
	return NULL;
}
void CreatePathMouseCallback::SetReference(int i, RefTargetHandle rtarg)
{
	if(i == 0)
	{
		m_pNode = (INode *)rtarg;
	}
}
RefResult CreatePathMouseCallback::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,  RefMessage message)
{
	switch (message) {
	case REFMSG_TARGET_SELECTIONCHANGE:
		if ( m_ignoreSelectionChange) {
			break;
		}
		if (m_pqueue) {
			m_pqueue->EndEditParams( (IObjParam*)m_ip, FALSE, NULL );
			m_pqueue = NULL;
			// this will set m_pNode = NULL;
			DeleteAllRefsFromMe();
			CreateNewPath();
		}
		break;

	case REFMSG_TARGET_DELETED:
		if (m_pqueue) {
			m_pqueue->EndEditParams( (IObjParam*)m_ip, FALSE, NULL );
			m_pqueue = NULL;
			CreateNewPath();
		}
		break;
	}
	return REF_SUCCEED;
}


//
//        name: proc
// description: Mouse procedure
//          in:
//
int CreatePathMouseCallback::proc( HWND hwnd, int msg, int point, int flag, IPoint2 m )
{
	TimeValue t = m_ip->GetTime();
	int res = TRUE;
	ViewExp *pVpt = m_ip->GetViewport(hwnd);

	assert( pVpt );

	switch ( msg ) {
	case MOUSE_FREEMOVE:
		pVpt->SnapPreview(m,m,NULL, SNAP_IN_3D);
		break;
	case MOUSE_POINT:
	case MOUSE_MOVE:
		{
			int hitTest = -1;

			if (point == 0) {

				assert(m_pqueue);

				if (m_ip->SetActiveViewport(hwnd) ||
					m_ip->IsCPEdgeOnInView()) {
					res = FALSE;
					break;
				}

				if ( m_isAttachedToNode ) {
					// send this one on its way
					m_pqueue->EndEditParams( (IObjParam*)m_ip,0,NULL );
					// Get rid of the references.
					DeleteAllRefsFromMe();
					// new object
					CreateNewPath();   // creates theMaster
				}

				theHold.Begin();	 // begin hold for undo

				//
				// Create a node
				//
				INode *pNode = m_ip->CreateObjectNode(m_pqueue);
				m_isAttachedToNode = TRUE;

				// Reference the first node so we'll get notifications.
				ReplaceReference(0, pNode);

				// select a node so if go into modify branch, see params
				m_ignoreSelectionChange = TRUE;
				m_ip->SelectNode(m_pNode);
				m_ignoreSelectionChange = FALSE;

				// get mouse position
				Matrix3 mat(1);
				m_pt0 = pVpt->SnapPoint(m,m,NULL,SNAP_IN_3D);
				mat.SetTrans(m_pt0);

				m_ip->SetNodeTMRelConstPlane(m_pNode, mat);
				res = TRUE;

				int newPathNode = m_pqueue->AddPathNode();
				m_pqueue->m_pPathNodes[0]->setDrawCol(Point3(1.0f,1.0f,1.0f));
				m_pqueue->m_pPathNodes[0]->GetParamBlock(0)->SetValue(queuenode_length,0,0);
				//m_pqueue->SetPathNodePosn(0, m_pt0);
			}
			else if(point > 1)
			{
				if(msg == MOUSE_POINT)
				{
					int newPathNode = m_pqueue->AddPathNode();

					if(point == 2)
					{
						m_pqueue->m_pPathNodes[1]->setDrawCol(Point3(1.0f,0.0f,0.0f));
						m_pqueue->m_pPathNodes[1]->GetParamBlock(0)->SetValue(queuenode_length,0,1);
					}
					else if(point == 3)
					{
						m_pqueue->m_pPathNodes[2]->setDrawCol(Point3(0.0f,1.0f,0.0f));
						m_pqueue->m_pPathNodes[2]->GetParamBlock(0)->SetValue(queuenode_length,0,2);
					}
					else if(point == 4)
					{
						m_pqueue->m_pPathNodes[3]->setDrawCol(Point3(0.0f,0.0f,1.0f));
						m_pqueue->m_pPathNodes[3]->GetParamBlock(0)->SetValue(queuenode_length,0,3);
					}

					m_previousPathNode = m_currentPathNode;
					m_currentPathNode = newPathNode;
					m_numPathNodes++;

					m_pt1 = pVpt->SnapPoint(m,m,NULL,SNAP_IN_3D);
					m_pt1 -= m_pt0;

					m_pqueue->SetPathNodePosn((point - 1), m_pt1);

					if(point == (MAX_NUM_queueNODE))
					{
						m_ip->RedrawViews(m_ip->GetTime(),REDRAW_END,m_pqueue);

						theHold.Accept(_T("Create StuntJump"));
						res = FALSE;
						break;
					}
				}
			}

			m_ip->RedrawViews(m_ip->GetTime(),REDRAW_NORMAL,m_pqueue);

			if(hitTest != -1)
				SetCursor(LoadCursor(hInstance, MAKEINTRESOURCE(IDC_CONNECT_CURSOR1)));
			else
				SetCursor(m_ip->GetSysCursor(SYSCUR_DEFARROW));
		}

		break;

	case MOUSE_ABORT:
		assert(m_pqueue);
		if(m_numPathNodes == 1)
		{
			m_pqueue->EndEditParams( (IObjParam*)m_ip, 0,NULL );
			m_pqueue = NULL;
			theHold.Cancel();  // undo the changes
			DeleteAllRefsFromMe();
			// DS 8/21/97: If something has been put on the undo stack since this object was created, we have to flush the undo stack.
			if (theHold.GetGlobalPutCount() != m_lastPutCount) {
				GetSystemSetting(SYSSET_CLEAR_UNDO);
			}
			CreateNewPath();
		}
		else
		{
			// delete last path node
			m_pqueue->DeletePathNode(m_currentPathNode);

			// set previous to external
			IParamBlock2* pBlock = m_pqueue->GetPathNode(m_previousPathNode)->GetParamBlockByID(path_params);

			m_ip->RedrawViews(m_ip->GetTime(),REDRAW_END,m_pqueue);
			theHold.Accept(_T("Create StuntJump"));
		}
		res = FALSE;
		break;

    case MOUSE_PROPCLICK:
		// right click while between creations
		m_ip->RemoveMode(NULL);
		break;
	}

	m_ip->ReleaseViewport(pVpt);
	return res;
}