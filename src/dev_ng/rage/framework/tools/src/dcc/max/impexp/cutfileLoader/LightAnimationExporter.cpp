#include "LightAnimationExporter.h"
#include "cranimation/animation.h"
#include "crclip/clip.h"
#include "crclip/clipanimation.h"
#include "cutfileLoader.h"
#include "cutfile/cutfile2.h"
#include "cutfile/cutfevent.h"
#include "cutfile/cutfeventargs.h"

#include "crmetadata/tag.h"
#include "crmetadata/tags.h"
#include "crmetadata/properties.h"
#include "crmetadata/propertyattributes.h"

using namespace rage;

#define COMPRESSION_FILE_PROPERTY_NAME "Compressionfile_DO_NOT_RESOURCE"

void CopyMaxMatrixToRageMatrix(const Matrix3& r_SrcMat,Matrix34& r_DstMat)
{
	Point4 SrcRow;

	SrcRow = r_SrcMat.GetRow(0);
	r_DstMat.a = Vector3(SrcRow[0],SrcRow[1],SrcRow[2]);
	SrcRow = r_SrcMat.GetRow(1);
	r_DstMat.b = Vector3(SrcRow[0],SrcRow[1],SrcRow[2]);
	SrcRow = r_SrcMat.GetRow(2);
	r_DstMat.c = Vector3(SrcRow[0],SrcRow[1],SrcRow[2]);
	SrcRow = r_SrcMat.GetRow(3);
	r_DstMat.d = Vector3(SrcRow[0],SrcRow[1],SrcRow[2]);
}

CLightAnimationExporter::CLightAnimationExporter()
{
	m_iGameScene = GetIGameInterface();
	m_iGameScene->InitialiseIGame();
}

void CLightAnimationExporter::Reset()
{
	m_trackNameList.Reset();
	m_trackIDList.Reset();
}

void CLightAnimationExporter::Create(const float fStartTime, const float fEndTime, const int iFrames, const char* pNodeName, const char* pDirectory, int iLightType)
{
	crAnimation::InitClass();
	crClip::InitClass();
	Reset();

	m_fStartTime = fStartTime;
	m_fEndTime = fEndTime;

	m_cAnimFilename[0] = 0;
	sprintf( m_cAnimFilename, "%s\\%s.anim", pDirectory, pNodeName );

	// make sure the directory exists.
	ASSET.CreateLeadingPath( m_cAnimFilename );

	m_pAnimationSegment = rage_new rexObjectGenericAnimationSegment;
	m_pAnimationSegment->SetName( m_cAnimFilename );

	m_pAnimationSegment->m_StartTime = m_fStartTime;
	m_pAnimationSegment->m_EndTime = m_fEndTime;
	m_pAnimationSegment->m_Autoplay = false;
	m_pAnimationSegment->m_Looping = false;
	m_pAnimationSegment->m_IgnoreChildBones = false;

	m_pAnimation = rage_new rexObjectGenericAnimation;
	m_pAnimation->m_StartTime = m_pAnimationSegment->m_StartTime;
	m_pAnimation->m_EndTime = m_pAnimationSegment->m_EndTime;
	m_pAnimation->m_FramesPerSecond = 30;
	m_pAnimation->m_ParentMatrix.Identity();
	m_pAnimation->m_ContainedObjects.PushAndGrow( m_pAnimationSegment, 1 );
	m_pAnimation->m_AuthoredOrientation = true;

	m_pChunk = rage_new rexObjectGenericAnimation::ChunkInfo;
	m_pChunk->m_Name = "root";
	m_pChunk->m_IsJoint = false;
	m_pChunk->m_BoneIndex = 0;
	m_pChunk->m_Scale = Vector3(1.0f,1.0f,1.0f);
	m_pChunk->m_BoneID = "root";

	m_iLightType = iLightType;

	CreateTracks();

	InitTracks( m_fStartTime, m_fEndTime, iFrames, m_pChunk->m_Tracks );
}

void CLightAnimationExporter::AddFrameData(INode* pNode, TimeValue t, rage::cutfCutsceneFile2* pCutFile)
{
	CreateAnimData(pNode, t, m_pChunk->m_Tracks, pCutFile );
}

bool CLightAnimationExporter::SaveAnim()
{
	m_pAnimationSegment->m_ChunkNames.PushAndGrow( m_pChunk->m_Name, 1 );
	m_pAnimation->m_RootChunks.PushAndGrow( m_pChunk, 1 );

	rexSerializerRAGEAnimation* pRexSer = CreateAnimationSerializer();

	rexResult res = rexResult::PERFECT;
	res.Combine(pRexSer->Serialize( *m_pAnimation ));

	if(res.Errors())
	{
		char cMsg[RAGE_MAX_PATH];
		sprintf(cMsg,"'%s' save fail.\n", ASSET.FileName(m_cAnimFilename));

		if(cutfileLoader::GetInst().GetInteractionMode())
		{
			MessageBox(NULL,cMsg,"Error", MB_ICONERROR);
		}

		PrintToListener("Error", cMsg); // print to listener
		
		delete pRexSer;
		delete m_pAnimation;

		return false;
	}

	char cMsg[RAGE_MAX_PATH];
	sprintf(cMsg,"'%s' save success.", ASSET.FileName(m_cAnimFilename));
	PrintToListener(NULL, cMsg); // print to listener

	if(!SaveClip())
	{
		delete pRexSer;
		delete m_pAnimation;

		return false;
	}

	delete pRexSer;
	delete m_pAnimation;

	return true;
}

bool CLightAnimationExporter::SaveClip()
{
	char cDirectory[RAGE_MAX_PATH];
	ASSET.RemoveExtensionFromPath( cDirectory, sizeof(cDirectory), m_cAnimFilename );	
	
	char cClipFilename[RAGE_MAX_PATH];
	sprintf( cClipFilename, "%s.clip", cDirectory );

	bool bResult = true;

	crAnimation *pAnim = crAnimation::AllocateAndLoad( m_cAnimFilename );
	if(pAnim)
	{
		crClipAnimation *pClipAnim = crClipAnimation::AllocateAndCreate( *pAnim );
		if(pClipAnim)
		{
			pAnim->Release();

			const atArray<cutfEvent *>& eventList = cutfileLoader::GetInst().GetCutFile()->GetEventList();

			for(int i=0; i < eventList.GetCount(); ++i)
			{
				cutfEvent* pEvent = eventList[i];

				if(pEvent->GetEventId() == CUTSCENE_CAMERA_CUT_EVENT)
				{
					if(pEvent->GetTime() != 0)
					{
						// add a block tag for each camera cut
						float thirtiethSecond = 1.0f / CUTSCENE_FPS;
						float startTime = pClipAnim->ConvertTimeToPhase( pEvent->GetTime() - thirtiethSecond - SMALL_FLOAT );
						float endTime = pClipAnim->ConvertTimeToPhase( pEvent->GetTime() + SMALL_FLOAT);

						crTag pBlockTag;
						pBlockTag.GetProperty().SetName( "Block" );
						pBlockTag.SetStart( startTime );
						pBlockTag.SetEnd( endTime );
						pClipAnim->GetTags()->AddUniqueTag( pBlockTag );
					}
				}
			}

			atString compFilePath("Cutscene_Default_Compress.txt");

			crProperties* pProperties = pClipAnim->GetProperties();

			crPropertyAttributeString compressionAttribute;
			compressionAttribute.SetName(COMPRESSION_FILE_PROPERTY_NAME);
			atString& attributeString = compressionAttribute.GetString();
			attributeString = compFilePath;

			crProperty pCompressionFile;
			pCompressionFile.SetName(COMPRESSION_FILE_PROPERTY_NAME);
			pCompressionFile.AddAttribute(compressionAttribute);
			pProperties->AddProperty( pCompressionFile );

			const char *pExtension = ASSET.FindExtensionInPath( cClipFilename );
			if ( pExtension != NULL )
			{
				*(const_cast<char *>( pExtension )) = 0;
			}

			bResult = pClipAnim->Save( cClipFilename );

			delete pClipAnim;
		}
	}

	if(!bResult)
	{
		char cMsg[RAGE_MAX_PATH];
		sprintf(cMsg,"Unable to save clip file '%s.clip'\n", ASSET.FileName(cClipFilename));

		if(cutfileLoader::GetInst().GetInteractionMode())
		{
			MessageBox(NULL,cMsg,"Error", MB_ICONERROR);
			
		}
		
		PrintToListener("Error", cMsg); // print to listener
	}

	char cMsg[RAGE_MAX_PATH];
	sprintf(cMsg,"'%s.clip' save success.", ASSET.FileName(cClipFilename));
	PrintToListener(NULL, cMsg); // print to listener

	return bResult;
}

void CLightAnimationExporter::CreateAnimData(INode* pNode, TimeValue t, atArray<rexObjectGenericAnimation::TrackInfo*> &trackList, rage::cutfCutsceneFile2* pCutFile)
{
	IGameNode* pIGameNode = m_iGameScene->GetIGameNode (pNode) ;
	IGameObject *pIGObject = pIGameNode->GetIGameObject();
	IPropertyContainer *pLightProps = pIGObject->GetIPropertyContainer();

	Point3 p3Colour;
	float fIntensity, fFalloff, fExpfalloff, fConeangle;
	Matrix3 tm = pNode->GetNodeTM(t);

	IGameProperty *pProp = pLightProps->QueryProperty("lightIntensity");
	if(pProp)
		pProp->GetPropertyValue(fIntensity, t);

	pProp = pLightProps->QueryProperty("lightAttenEnd");
	if(pProp)
		pProp->GetPropertyValue(fFalloff, t);

	pProp = pLightProps->QueryProperty("lightFalloff");
	if(pProp)
		pProp->GetPropertyValue(fConeangle, t);

	pProp = pLightProps->QueryProperty("lightExpFalloff");
	if(pProp)
		pProp->GetPropertyValue(fExpfalloff, t);

	pProp = pLightProps->QueryProperty("lightColour");
	if(pProp)
		pProp->GetPropertyValue(p3Colour, t);

	AddAnimDataToTracks(tm, p3Colour, fIntensity, fFalloff, fExpfalloff, fConeangle, trackList, pCutFile );
}

void CLightAnimationExporter::AddAnimDataToTracks(Matrix3 mTransform, Point3 aColour, float fIntensity, float fFallOff, float fExpFallOff, float fConeAngle, atArray<rexObjectGenericAnimation::TrackInfo*> &trackList, rage::cutfCutsceneFile2* pCutFile )
{
	Matrix34 mRageTransform;
	CopyMaxMatrixToRageMatrix(mTransform, mRageTransform);

	// Go through each of our data collectors and send the data to the track
	for ( int i = 0; i < trackList.GetCount(); ++i )
	{
		rexObjectGenericAnimation::TrackInfo *pTrackInfo = trackList[i];

		switch( pTrackInfo->m_TrackID )
		{
		case rexObjectGenericAnimation::TRACK_TRANSLATE:
			{
				Vector3 vec(0,0,pCutFile->GetRotation()*DtoR); 

				Matrix34 lightTransform = mRageTransform;

				Matrix34 sceneTransform;
				sceneTransform.Identity();
				sceneTransform.FromEulersXYZ(vec);
				sceneTransform.d = pCutFile->GetOffset();

				lightTransform.DotTranspose(sceneTransform);

				pTrackInfo->AppendVector3Key( lightTransform.d );
			}
			break;
		case rexObjectGenericAnimation::TRACK_LIGHT_DIRECTION:
			{
				Vector3 vec(0,0,pCutFile->GetRotation()*DtoR); 

				Matrix34 lightTransform = mRageTransform;

				Matrix34 sceneTransform;
				sceneTransform.Identity();
				sceneTransform.FromEulersXYZ(vec);
				sceneTransform.d = pCutFile->GetOffset();

				lightTransform.DotTranspose(sceneTransform);

				Vector3 vDirection(VEC3_ZERO);
				lightTransform.Transform3x3(Vector3(0,0,-1), vDirection);

				//char cMsg2[RAGE_MAX_PATH];
				//sprintf( cMsg2, "Direction : %f, %f , %f", vDirection.GetX(), vDirection.GetY(), vDirection.GetZ() );
				//PrintToListener("", cMsg2);

				pTrackInfo->AppendVector3Key( vDirection );
			}
			break;
		case rexObjectGenericAnimation::TRACK_COLOUR:
			{
				Vector3 v(aColour.x, aColour.y, aColour.z);
				pTrackInfo->AppendVector3Key(v);
			}
			break;
		case rexObjectGenericAnimation::TRACK_UNKNOWN:
			{
				if(pTrackInfo->m_Name == atString("lightIntensity"))
				{
					pTrackInfo->AppendFloatKey(fIntensity);
				}
				else if (pTrackInfo->m_Name == atString("lightFallOff"))
				{
					pTrackInfo->AppendFloatKey(fFallOff);
				}
				else if (pTrackInfo->m_Name == atString("lightConeAngle"))
				{
					pTrackInfo->AppendFloatKey(fConeAngle);
				}
				else if (pTrackInfo->m_Name == atString("lightExpFallOff"))
				{
					pTrackInfo->AppendFloatKey(fExpFallOff);
				}
			}
			break;
		}
	}
}

void CLightAnimationExporter::InitTracks( float fStartTime, float fEndTime, int nFrames, atArray<rexObjectGenericAnimation::TrackInfo*>& trackList )
{
	int iTrackCount = m_trackNameList.GetCount();
	trackList.Reset();
	trackList.Reserve( iTrackCount );

	for ( int i = 0; i < iTrackCount; ++i )
	{
		rexObjectGenericAnimation::TrackInfo* pTrack = NULL;

		switch( m_trackIDList[i] )
		{
		case rexObjectGenericAnimation::TRACK_TRANSLATE:
		case rexObjectGenericAnimation::TRACK_SCALE:
		case rexObjectGenericAnimation::TRACK_COLOUR:
		case rexObjectGenericAnimation::TRACK_LIGHT_DIRECTION:
			pTrack = rage_new rexObjectGenericAnimation::TrackInfoVector3();
			break;
		case rexObjectGenericAnimation::TRACK_ROTATE:
			pTrack = rage_new rexObjectGenericAnimation::TrackInfoQuaternion();
			break;
		case rexObjectGenericAnimation::TRACK_VISEMES:
		case rexObjectGenericAnimation::TRACK_UNKNOWN:
			pTrack = rage_new rexObjectGenericAnimation::TrackInfoFloat();
			break;
		}

		if ( !pTrack )
		{
			char msg[256];
			sprintf( msg, "WARNING: Unknown track id %d for a track named '%s'.", m_trackIDList[i], m_trackNameList[i].c_str() );
			//RexMbCutsceneExportModel::SetProgressMessage( msg );

			continue;
		}

		pTrack->m_Name = m_trackNameList[i];
		pTrack->m_TrackID = m_trackIDList[i];

		if ( InitTrack( fStartTime, fEndTime, nFrames, *pTrack ) )
		{
			trackList.Append() = pTrack;
		}
		else
		{
			//Couldn't find the appropriate data for the track, so don't add it
			//TODO : Produce some sort of useful warning/error here...
			delete pTrack;
		}
	}
}

bool CLightAnimationExporter::InitTrack( float fStartTime, float fEndTime, int nFrames, rexObjectGenericAnimation::TrackInfo& track )
{
	track.m_StartTime = fStartTime;
	track.m_EndTime = fEndTime;

	//TODO : Maya exporter supports determining if a track is locked by the following logic
	// track.m_IsLocked = (plug.isLocked() || !plug.isKeyable() || (track.m_EndTime <= track.m_StartTime));
	// we probably should look to support some similar functionality here.
	track.m_IsLocked = false;

	track.ResetKeys( nFrames + 1 );

	return true;
}

rexSerializerRAGEAnimation* CLightAnimationExporter::CreateAnimationSerializer()
{
	rexSerializerRAGEAnimation* pRexSer = rage_new rexSerializerRAGEAnimation;

	pRexSer->AddChannelToWrite( atString("translateX"), atString("translateX"), rexObjectGenericAnimation::TRACK_TRANSLATE, true );
	pRexSer->AddChannelToWrite( atString("translateY"), atString("translateY"), rexObjectGenericAnimation::TRACK_TRANSLATE, true );
	pRexSer->AddChannelToWrite( atString("translateZ"), atString("translateZ"), rexObjectGenericAnimation::TRACK_TRANSLATE, true );
	pRexSer->AddChannelToWrite( atString("lightDirection"), atString("lightDirection"), rexObjectGenericAnimation::TRACK_LIGHT_DIRECTION, true );
	pRexSer->AddChannelToWrite( atString("color"), atString("color"), rexObjectGenericAnimation::TRACK_COLOUR, true );
	pRexSer->AddChannelToWrite( atString("lightIntensity"), atString("lightIntensity"), rexObjectGenericAnimation::TRACK_UNKNOWN, true );
	pRexSer->AddChannelToWrite( atString("lightFallOff"), atString("lightFallOff"), rexObjectGenericAnimation::TRACK_UNKNOWN, true );
	pRexSer->AddChannelToWrite( atString("lightExpFallOff"), atString("lightExpFallOff"), rexObjectGenericAnimation::TRACK_UNKNOWN, true );

	switch(m_iLightType)
	{
	case FREESPOT:
		{
			pRexSer->AddChannelToWrite( atString("lightConeAngle"), atString("lightConeAngle"), rexObjectGenericAnimation::TRACK_UNKNOWN, true );
		}
		break;
	}

	return pRexSer;
}

bool CLightAnimationExporter::CreateTracks()
{
	// Translate and Rotate
	m_trackNameList.PushAndGrow( atString("translate") );
	m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_TRANSLATE );

	m_trackNameList.PushAndGrow( atString("lightDirection") );
	m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_LIGHT_DIRECTION );

	m_trackNameList.PushAndGrow( atString("color") );
	m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_COLOUR );

	m_trackNameList.PushAndGrow( atString("lightIntensity") );
	m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_UNKNOWN );

	m_trackNameList.PushAndGrow( atString("lightFallOff") );
	m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_UNKNOWN );

	m_trackNameList.PushAndGrow( atString("lightExpFallOff") );
	m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_UNKNOWN );

	switch(m_iLightType)
	{
	case FREESPOT:
		{
			m_trackNameList.PushAndGrow( atString("lightConeAngle") );
			m_trackIDList.PushAndGrow( rexObjectGenericAnimation::TRACK_UNKNOWN );
		}
		break;
	}

	return true;
}