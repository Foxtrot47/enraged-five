/**********************************************************************
 *<
	FILE: jiggleAPI.h

	DESCRIPTION:	Public header file for Spring controller

	CREATED BY:		Adam Felt

	HISTORY: 

 *>	Copyright (c) 1999-2000, All Rights Reserved.
 **********************************************************************/
#pragma once

#include "springsys.h"
#include "ifnpub.h"


#define RS_SPRINGPOS 0x6bff0328
#define RS_SPRINGROT 0x604f24bc
#define RS_SPRINGP3 0x1c2067e7

#define RS_SPRING_POS_CLASS_ID	Class_ID(RS_SPRINGPOS, 0x62043b15)
#define RS_SPRING_ROT_CLASS_ID	Class_ID(RS_SPRINGROT, 0x65764cf7)
#define RS_SPRING_P3_CLASS_ID	Class_ID(RS_SPRINGP3, 0x3ae1878)

#define RS_SPRING_CONTROL_REF  0
#define RS_SPRING_PBLOCK_REF1	1
//#define RS_SPRING_PBLOCK_REF2  2

//parameter defaults
#define RS_SPRING_CONTROL_DEFAULT 1.0f

#define RS_SPRING_CONTROLLER_INTERFACE Interface_ID(0x79437521, 0x3a1576fe)

enum 
{
	SPRING_PARAM_AXIS_X,
	SPRING_PARAM_AXIS_Y,
	SPRING_PARAM_AXIS_Z
};
enum 
{
	SPRING_PARAM_MIN,
	SPRING_PARAM_MAX
};

class RsSpringDlg;
class DynMapDlgProc;

/*! \sa  Class FPMixinInterface,  Class SpringSys , Class INode\n\n
\par Description:
This class is available in release 4.0 and later only.\n\n
This class represents the interface to the Spring Controller and comes in two
different flavors, the <b>RS_SPRING_POS_CLASS_ID</b> for the position spring
controller and <b>RS_SPRING_P3_CLASS_ID</b> for the Point3 spring controller.\n\n
All methods of this class are Implemented by the System.\n\n

\par Data Members:
<b>SpringSys* partsys;</b>\n\n
A pointer to the spring system class.  */
class IRsSpring : public FPMixinInterface
{
	public:

		// shared 
		DynMapDlgProc* pmap;
		HWND	hParams1;  //dynamics dialog handle
		IParamBlock2 *dyn_pb;
		float LimitButtDownVal[3];
		BOOL			m_valid;
		RsSpringDlg* dlg;

		virtual RefResult NotifyDependents  (  Interval  changeInt,  
			PartID  partID,  
			RefMessage  message,  
			SClass_ID  sclass = NOTIFY_ALL,  
			BOOL  propagate = TRUE,  
			RefTargetHandle  hTarg = NULL 
			)=0;
		virtual void	SetParamsLinked(BOOL linked)=0;
		virtual BOOL	IsParamsLinked()=0;
		virtual BOOL SetSelfReference()=0;
		virtual void	SetPreserveXForms(bool doIt)=0;
		virtual BOOL	GetPreserveXForms()=0;

		//function publishing
		enum { 
			get_strength,
			set_strength,
			get_dampening,
			set_dampening,
			get_influence,
			set_influence,
			get_limit,
			set_limit,
			get_spring_type,
//			get_spring_system, 
		};

		BEGIN_FUNCTION_MAP
			FN_1	(get_strength, TYPE_FLOAT, GetStrength, TYPE_INT);
			VFN_2	(set_strength, SetStrength, TYPE_INT, TYPE_FLOAT);
			FN_1	(get_dampening, TYPE_FLOAT, GetDampening, TYPE_INT);
			VFN_2	(set_dampening, SetDampening, TYPE_INT, TYPE_FLOAT);
			FN_2	(get_limit, TYPE_FLOAT, GetLimit, TYPE_INT, TYPE_INT);
			VFN_3	(set_limit, SetLimit, TYPE_INT, TYPE_INT, TYPE_FLOAT);
			
			FN_0	(get_spring_type, TYPE_INT, GetSpringType);
		END_FUNCTION_MAP

		FPInterfaceDesc* GetDesc();    // <-- must implement 
		
//		SpringSys* partsys;
		/*! \remarks This method returns the pointer to the associated spring
		system object. */
//		virtual SpringSys* GetSpringSystem()=0;
		
		virtual float	GetStrength(int axis)=0;
		virtual void	SetStrength(int axis, float strength, bool update=true)=0;
		virtual float	GetDampening(int axis)=0;
		virtual void	SetDampening(int axis, float dampening, bool update=true)=0;
		virtual float	GetLimit(int axis, int minMax)=0;
		virtual void	SetLimit(int axis, int minMax, float limit, bool update=true, bool linkForward=false)=0;
		virtual float	GetGravity(int axis)=0;
		virtual void	SetGravity(int axis, float gravity, bool update=true)=0;
		virtual float	GetGravityAxis(int axis)=0;
		virtual void	SetGravityAxis(int axis, float gravity, bool update=true)=0;
		virtual int		GetSpringType()=0;
};

