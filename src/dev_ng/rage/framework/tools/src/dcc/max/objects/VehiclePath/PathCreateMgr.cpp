//
//
//    Filename: PathCreateMgr.cpp
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: 
//
//
#include "PathCreateMgr.h"
#include "VehiclePath.h"
#include "VehicleNode.h"

VehiclePathCreateMode theVehiclePathCreateMode;

//
//        name: VehiclePathCreateMgr::VehiclePathCreateMgr
// description: Constructor
//
VehiclePathCreateMgr::VehiclePathCreateMgr() : m_pThis(NULL), m_pPrev(NULL)
{
}

//
//        name: VehiclePathCreateMgr::~VehiclePathCreateMgr
// description: Destructor
//
VehiclePathCreateMgr::~VehiclePathCreateMgr()
{
}

//
//        name: VehiclePathCreateMgr::Begin
// description: Called at beginning of creation
//
void VehiclePathCreateMgr::Begin( IObjCreate *ioc, ClassDesc *desc )
{
	m_ip = ioc;
	m_pPrev = NULL;
	m_pCanConnectTo = NULL;
	// make sure VehiclePath create manager isn't pointing to anything
	DeleteReference(0);

	m_pPath = new VehiclePath();
	m_pPath->BeginEditParams( (IObjParam*)m_ip, BEGIN_EDIT_CREATE,NULL );
}

void VehiclePathCreateMgr::End()
{
	m_pPrev = NULL;
	m_pCanConnectTo = NULL;

	m_pPath->EndEditParams( (IObjParam*)m_ip, FALSE, NULL );
	m_pPath = NULL;

}


//
//        name: VehiclePathCreateMgr::Filter
// description: Return if pointing to valid connection
//
BOOL VehiclePathCreateMgr::Filter(INode *pNode)
{
	if(pNode && pNode->GetObjectRef() != m_pPrev)
	{
		Object* pObj = pNode -> EvalWorldState(0).obj;
		if (pObj && pObj->ClassID() == VEHICLE_NODE_CLASS_ID)
		{
			if(m_pPrev == NULL)
				return TRUE;
			if(m_pPrev->GetLinkBetween((VehicleNode *)pObj))
				return TRUE;
		}
	}
	return FALSE;

}
	
// ReferenceMaker
int VehiclePathCreateMgr::NumRefs()
{
	return 1;
}
RefTargetHandle VehiclePathCreateMgr::GetReference(int i)
{
	switch(i)
	{
	case 0:
		return m_pThis;
	}
	assert(0);
	return NULL;
}
void VehiclePathCreateMgr::SetReference(int i, RefTargetHandle rtarg)
{
	switch(i)
	{
	case 0:
		m_pThis = (INode *)rtarg;
		if(m_pThis)
			m_pPath = (VehiclePath *)m_pThis->GetObjectRef();
		else
			m_pPath = NULL;
		break;
	default:
		assert(0);
	}
}

RefResult VehiclePathCreateMgr::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,  RefMessage message)
{
	switch(message)
	{
	case REFMSG_TARGET_DELETED:
		if(m_pPath)
		{
			m_pPath->EndEditParams(m_ip, END_EDIT_REMOVEUI, NULL);
			m_pPath = NULL;
		}
		m_pThis = NULL;
		break;
	}
	return REF_SUCCEED;
}

// MouseCallback
int VehiclePathCreateMgr::proc(HWND hWnd, int msg, int point, int flags, IPoint2 sp)
{
	TimeValue t = m_ip->GetTime();
	int res = TRUE;
	ViewExp *pVpt = m_ip->GetViewport(hWnd);

	switch(msg)
	{
	case MOUSE_MOVE:
	{
		if(m_pThis != NULL)
		{
			// move node about with the mouse
			Matrix3 mat(1);
			mat.SetTrans(pVpt->SnapPoint(sp,sp,NULL,SNAP_IN_PLANE));
			m_ip->SetNodeTMRelConstPlane(m_pThis, mat);
		}	
		m_ip->RedrawViews(t, REDRAW_NORMAL, m_pThis);  
	}
	break;
	case MOUSE_POINT:
	{
		// if there isn't any path created already then create one
		if(m_pThis == NULL)
		{
			if ( m_ip->SetActiveViewport(hWnd) )
			{
				return FALSE;
			}
			// check valid construction plane
			if (m_ip->IsCPEdgeOnInView())
			{ 
				return FALSE;
			}
			theHold.Begin();

			// create VehiclePath and node
			m_pPath = (VehiclePath *)m_ip->CreateInstance(HELPER_CLASS_ID, VEHICLE_PATH_CLASS_ID);
			INode *pNode = m_ip->CreateObjectNode(m_pPath);

			// set node position
			// Create matrix for node position
			Matrix3 mat(1);
			mat.SetTrans(pVpt->SnapPoint(sp,sp,NULL,SNAP_IN_PLANE));
			m_ip->SetNodeTMRelConstPlane(pNode, mat);
			m_ip->SelectNode(pNode);

			// reference node, so that we are informed when it is destroyed
			ReplaceReference(0, pNode);

			// begin edit parameters
			m_pPath->BeginEditParams(m_ip, BEGIN_EDIT_CREATE, NULL);
			m_ip->RedrawViews(t,REDRAW_BEGIN,m_pThis); 
		}
		else
		{
			// move node about with the mouse
			Matrix3 mat(1);
			mat.SetTrans(pVpt->SnapPoint(sp,sp,NULL,SNAP_IN_PLANE));
			m_ip->SetNodeTMRelConstPlane(m_pThis, mat);
		}

		// start creating a new node
		IParamBlock2* pBlock = m_pPath->GetParamBlockByID(vehicle_path_params);
		assert(pBlock);

		// if connecting to a VehiclePath already placed down
		if(m_pCanConnectTo)
		{
			m_pPrev = (VehicleNode* )m_pCanConnectTo->EvalWorldState(0).obj;
			
			pBlock->Append(vehicle_connectionlist, 1, &m_pCanConnectTo);

			// set path position 
			Point3 posn(0.0f,0.0f,0.0f);
			int32 size = pBlock->Count(vehicle_connectionlist);
			for(int32 i=0; i<size; i++)
			{
				INode* pNode = pBlock->GetINode(vehicle_connectionlist, 0, i);
				assert(pNode);
				posn += pNode->GetNodeTM(0).GetTrans() / (float)size;
			}


		}
		else if(point != 1)
		{
			// Create path if there are connection in it. Otherwise delete it
			if(pBlock->Count(vehicle_connectionlist) > 0)
			{
				DeleteReference(0);
				theHold.Accept("Create VehiclePath");
			}
			else
			{
				theHold.Cancel();
			}
			res = FALSE;
			m_pPrev = NULL;
			m_ip->RedrawViews(t,REDRAW_END,m_pThis);  
		}
		if(res)
			m_ip->RedrawViews(t,REDRAW_NORMAL,m_pThis);  
	}
	break;
	case MOUSE_ABORT:
		res = FALSE;
		m_pPrev = NULL;
		theHold.Cancel();
		m_ip->RedrawViews(t,REDRAW_END,m_pThis);  
		break;

    case MOUSE_PROPCLICK:
		// right click while between creations
		m_ip->RemoveMode(NULL);
		break;
	}


	// if have moved over a Vehicle Path that current connection can connect to 
	m_pCanConnectTo = m_ip->PickNode(hWnd, sp, this);
	if(m_pCanConnectTo)
		SetCursor(LoadCursor(hInstance, MAKEINTRESOURCE(IDC_CONNECT_CURSOR1)));
	else
		SetCursor(m_ip->GetSysCursor(SYSCUR_DEFARROW));

	m_ip->ReleaseViewport(pVpt);

	return res;
}