﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using RSG.Editor.Autodesk3dsmax.UniversalLog;
using RSG.Editor.Controls;
using RSG.Editor.Controls.Exceptions;

namespace RSG.Editor.Autodesk3dsmax
{
    /// <summary>
    /// 
    /// </summary>
    public class Autodesk3dsmaxApplication : RsApplication
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Autodesk3dsmaxApplication"/> class.
        /// </summary>
        public Autodesk3dsmaxApplication()
        {
            ShutdownMode = ShutdownMode.OnExplicitShutdown;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the unique name for this application that is used for mutex creation, and
        /// folder organisation inside the local app data folder and the registry.
        /// </summary>
        public override string UniqueName
        {
            get { return "3dsMaxWindowHost"; }
        }

        /// <summary>
        /// Gets a value indicating whether the layout for the main window get serialised on
        /// exit and deserialised during loading.
        /// </summary>
        protected override bool MakeLayoutPersistent
        {
            get { return true; }
        }

        /// <summary>
        /// Buttons to display in the unhandled exception window dialog.
        /// </summary>
        protected override ExceptionWindowButtons ExceptionWindowButtons
        {
            get { return ExceptionWindowButtons.All; }
        }

        /// <summary>
        /// Gets a semi-colon separate list of the application authors email addresses. These
        /// are the addresses which are used by the unhandled exception dialog to determine
        /// where the mail should be sent by default.
        /// e.g.
        /// *tools@rockstarnorth.com
        /// michael.taschler@rockstarnorth.com;dave.evans@rockstarnorth.com
        /// </summary>
        public override string AuthorEmailAddress
        {
            get { return String.Format("{0};*Default_Tech_Art@rockstarnorth.com;", base.AuthorEmailAddress); }
        }

        /// <summary>
        /// Gets a value indicating whether we should throw an exception when an unregistered
        /// command line argument is found while parsing the arguments.
        /// </summary>
        public override bool ThrowOnUnsupportedCommandLineArgs
        {
            get { return false; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Call this method as soon as the application enters the Main method.
        /// </summary>
        public void OnEntry()
        {
            InitialisationResult result =
                ApplicationManager<RsApplication>.InitialiseApplication(this, this.UniqueName);
            if (result == InitialisationResult.Success)
            {
                ApplicationInitialised = true;
            }
        }

        /// <summary>
        /// Call this method when shutting down the application.
        /// </summary>
        public void OnShutdown()
        {
            Shutdown(0);
            ApplicationManager<RsApplication>.Cleanup();
            RSG.Base.Logging.LogFactory.ApplicationShutdown();
        }

        /// <summary>
        /// Override to create the main window for the application.
        /// </summary>
        /// <returns>
        /// The System.Windows.Window that will be the main window for this application.
        /// </returns>
        protected override Window CreateMainWindow()
        {
            return null;
        }

        /// <summary>
        /// Gets called when an exception is fired from either the application or the
        /// dispatcher thread and it goes unhandled. By default this shows the exception
        /// window showing the thrown exception.
        /// </summary>
        /// <param name="exception">
        /// The exception that was thrown and not handled.
        /// </param>
        protected override void OnUnhandledException(Exception exception)
        {
            // Check to see if this is an exception coming from max and rethrow to let max "handle" it.
            if (String.Equals(exception.Source, "3dsmax", StringComparison.InvariantCultureIgnoreCase))
            {
                throw exception;
            }

            base.OnUnhandledException(exception);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filenames"></param>
        /// <param name="showCancel"></param>
        /// <param name="showContinue"></param>
        /// <returns></returns>
        public UniversalLogWindow CreateUniversalLogWindow(IEnumerable<String> filenames, bool showCancel = false, bool showContinue = false)
        {
            return new UniversalLogWindow(filenames, showCancel, showContinue);
        }
        #endregion // Methods
    } // Autodesk3dsmaxApplication
}
