//
// filename:	DLLEntry.cpp
// author:		David Muir
// date:		23 January 2007
// description:	Scrollbars 3D Studio Max Plugin DLL Entry-Point(s)
//

// --- Include Files ------------------------------------------------------------

// 3D Studio Max SDK headers
#include "Max.h"
#include "IPathConfigMgr.h"

// Local headers
#include "SceneXml.h"
#include "cSceneXmlConfig.h"
#include "Resource.h"
#include "Win32HelperLib/Win32Res.h"

// --- Constants ----------------------------------------------------------------
const int	sC_cnt_NumberOfClasses = 0;

// --- Globals ------------------------------------------------------------------

HINSTANCE	g_hInstance = NULL;
int			g_ControlsInit = FALSE;

// --- Code ---------------------------------------------------------------------

//
// name:		DllMain
// description:	DLL Plugin Entry-Point
//
BOOL WINAPI 
DllMain( HINSTANCE hInstDLL, ULONG dwReason, LPVOID plReserved )
{
	// Store the DLLs instance handle
	g_hInstance = hInstDLL;

	if ( FALSE == g_ControlsInit )
	{
		// Initialise platform common controls
		InitCommonControls( );
		g_ControlsInit = TRUE;
	}

	return TRUE;
}


SCENEXML_API int 
LibInitialize( void )
{
	// Load SceneXml configuration
	if ( (!g_SceneXmlConfig.HasLoaded()) && (!g_SceneXmlConfig.LoadFailed()) )
	{
		IPathConfigMgr* pConfigMgr = IPathConfigMgr::GetPathConfigMgr();
		const TCHAR* pPlugCfgDir = pConfigMgr->GetDir( APP_PLUGCFG_DIR );

		MaxSDK::Util::Path configPath( pPlugCfgDir );
		configPath.Append( CONFIG_FILE_DEFAULT );

		g_SceneXmlConfig.Load( configPath.GetString() );
	}

	return ( 1 );
}


SCENEXML_API int
LibShutdown( void )
{
	return ( 1 );
}


//
// name:		LibDescription
// description:	Return plugin description string (Max API)
//
SCENEXML_API const TCHAR* 
LibDescription()
{
	return GetStringResource( g_hInstance, IDS_DESCRIPTION );
}


//
// name:		LibNumberClasses
// description:	Return number of classes registered by plugin (Max API)
//
SCENEXML_API int 
LibNumberClasses( )
{
	return sC_cnt_NumberOfClasses;
}


//
// name:		LibClassDesc
// description:	Return indexed class description class
//
SCENEXML_API ClassDesc* 
LibClassDesc( int nClassIndex )
{
	assert( sC_cnt_NumberOfClasses==0 || nClassIndex < sC_cnt_NumberOfClasses );
	return ( NULL );
}


//
// name:		LibVersion
// description:	Return version of Max SDK plugin was compiled with
//
SCENEXML_API ULONG 
LibVersion( )
{
	return VERSION_3DSMAX;
}


//
// name:		CanAutoDefer
// description:
//
SCENEXML_API ULONG 
CanAutoDefer( ) 
{ 
	return ( 0 ); 
}

// End of file
