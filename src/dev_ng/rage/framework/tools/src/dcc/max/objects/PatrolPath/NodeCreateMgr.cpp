//
//
//    Filename: ConnectionCreateMgr.cpp
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: 
//
//
#pragma warning (disable : 4251)

#include "IMaxDataStore.h"
#include "PatrolNode.h"
#include "PatrolLink.h"
#include "NodeCreateMgr.h"
#include "DataInstance.h"

#define MAX_NUM_NODES_AUTO_CREATE	(100)

PatrolNodeCreateMode thePatrolNodeCreateMode;

HWND PatrolNodeCreateMgr::m_hWndIMaxNode=NULL;
HWND PatrolNodeCreateMgr::m_hWndIMaxLink=NULL;
HWND PatrolNodeCreateMgr::m_hWndAutoCreate=NULL;

PatrolNode*		PatrolNodeCreateMgr::m_pDefaultPatrolNode;
PatrolLink*		PatrolNodeCreateMgr::m_pDefaultPatrolLink=NULL;
bool			PatrolNodeCreateMgr::m_bAutoCreate=false;
float			PatrolNodeCreateMgr::m_fAutoCreateDistance=200.0f;
bool			PatrolNodeCreateMgr::m_bViewDifferent=false;

#if !defined( _WIN64 )
BOOL CALLBACK EditAutoCreateProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
#else
INT_PTR CALLBACK EditAutoCreateProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////
//
//        name: PatrolNodeCreateMgr::PatrolNodeCreateMgr
// description: Constructor
//
PatrolNodeCreateMgr::PatrolNodeCreateMgr() : 
	m_pThis(NULL),
	m_pCanConnectTo(NULL),
	m_pPrev(NULL),
	m_pVehicleNode(NULL),
	m_ip(NULL),
	m_creating(false)
{
}

//
//        name: PatrolNodeCreateMgr::~PatrolNodeCreateMgr
// description: Destructor
//
PatrolNodeCreateMgr::~PatrolNodeCreateMgr()
{
}

//
//        name: PatrolNodeCreateMgr::Begin
// description: Called at beginning of creation
//
void PatrolNodeCreateMgr::Begin( IObjCreate *ioc, ClassDesc *desc )
{
	m_ip = ioc;
	m_pPrev = NULL;
	m_pCanConnectTo = NULL;
	// make sure PatrolNode create manager isn't pointing to anything
	DeleteReference(0);

	if(m_hWndAutoCreate == NULL)
	{
		InitUI();
	}

	if(NULL == m_hWndIMaxNode)
	{
		CreateVehicleNode(m_ip, m_pDefaultPatrolNode);
		m_hWndIMaxNode = theIMaxDataStore.AddRollupPage(m_pDefaultPatrolNode, m_ip);
	}
	if(NULL == m_hWndIMaxLink)
	{
		CreateVehicleLink(m_ip, m_pDefaultPatrolLink);
		m_hWndIMaxLink = theIMaxDataStore.AddRollupPage(m_pDefaultPatrolLink, m_ip);
	}
}

void PatrolNodeCreateMgr::End()
{
	m_pPrev = NULL;
	m_pCanConnectTo = NULL;

	theIMaxDataStore.DeleteRollupPage(m_hWndIMaxNode, m_ip);
	theIMaxDataStore.DeleteRollupPage(m_hWndIMaxLink, m_ip);
	m_hWndIMaxNode = NULL;
	m_hWndIMaxLink = NULL;

	CloseUI();
}

void PatrolNodeCreateMgr::InitUI()
{
	if(m_hWndAutoCreate == NULL)
	{
		LPTSTR lpStr = MAKEINTRESOURCE(IDD_AUTO_CREATE_PARAMS_PANEL);

		m_hWndAutoCreate = m_ip->AddRollupPage(
			hInstance, 
			MAKEINTRESOURCE(IDD_AUTO_CREATE_PARAMS_PANEL),
			EditAutoCreateProc,
			_T("AutoCreate"),
			(LPARAM)this);

	m_pSpinLinkLen	= SetupIntSpinner(m_hWndAutoCreate, IDC_CREATE_DIST_SPIN, IDC_CREATE_DIST_EDIT, MIN_LINK_LEN, MAX_LINK_LEN, DEF_LINK_LEN);
	m_pSpinLinkLen->SetValue(m_fAutoCreateDistance, FALSE);
	m_pSpinLinkLen->SetScale(20.0f);
	
	SendMessage(GetDlgItem(PatrolNodeCreateMgr::m_hWndAutoCreate, IDC_CHECK_VIEW), 
				(UINT) BM_SETCHECK,
				PatrolNodeCreateMgr::m_bViewDifferent,
				0
				);  
	}

}


void PatrolNodeCreateMgr::CloseUI()
{
	if(m_hWndAutoCreate )
	{
		ReleaseISpinner(m_pSpinLinkLen);
		m_ip->DeleteRollupPage(m_hWndAutoCreate);
		m_hWndAutoCreate = NULL;
	}
}

//
//        name: VehicleNodeCreateMgr::Filter
// description: Return if previous node can connect to this node
//
BOOL PatrolNodeCreateMgr::Filter(INode *pNode)
{
	if(m_pPrev == NULL)
		return FALSE;

	if(pNode && pNode->GetObjectRef() != m_pPrev)
	{
		Object* pObj = pNode -> EvalWorldState(0).obj;
		if (pObj && pObj->ClassID() == PATROL_NODE_CLASS_ID)
		{
#ifdef ONLY_LINEAR_PATHS	
			if(((PatrolConnection *)pObj)->GetNumberOfLinks() < 2 &&
				m_pPrev->GetLinkBetween((PatrolNode* )pObj) == NULL)
				return TRUE;
#else
			if(m_pPrev->GetLinkBetween((PatrolNode* ) pObj) == NULL)
				return TRUE;
#endif
		}
	}
	return FALSE;

}
	
// ReferenceMaker
int PatrolNodeCreateMgr::NumRefs()
{
	return 1;
}
RefTargetHandle PatrolNodeCreateMgr::GetReference(int i)
{
	switch(i)
	{
	case 0:
		return m_pThis;
	}
	assert(0);
	return NULL;
}
void PatrolNodeCreateMgr::SetReference(int i, RefTargetHandle rtarg)
{
	switch(i)
	{
	case 0:
		m_pThis = (INode *)rtarg;
		if(m_pThis)
			m_pVehicleNode= (PatrolNode* )m_pThis->GetObjectRef();
		else
			m_pVehicleNode= NULL;
		break;
	default:
		assert(0);
	}
}

RefResult PatrolNodeCreateMgr::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,  RefMessage message)
{
	switch(message)
	{
	case REFMSG_TARGET_DELETED:
		if(m_pVehicleNode)
		{
			m_pVehicleNode->EndEditParams(m_ip, END_EDIT_REMOVEUI, NULL);
			m_pVehicleNode= NULL;
		}
		m_pThis = NULL;
		break;
	}
	return REF_SUCCEED;
}

// MouseCallback
int PatrolNodeCreateMgr::proc(HWND hWnd, int msg, int point, int flags, IPoint2 sp)
{
	TimeValue t = m_ip->GetTime();
	int res = TRUE;
	ViewExp *pVpt = m_ip->GetViewport(hWnd);

	switch(msg)
	{
	case MOUSE_FREEMOVE:
		pVpt->TrackImplicitGrid(sp);
		break;

	case MOUSE_MOVE:
	{
		if(m_pThis != NULL)
		{
			Matrix3 mat(1);
			mat.SetTrans(pVpt->SnapPoint(sp,sp,NULL,SNAP_IN_3D));
			m_ip->SetNodeTMRelConstPlane(m_pThis, mat);
		}
		m_ip->RedrawViews(t,REDRAW_NORMAL,m_pThis);  
	}
	break;
	case MOUSE_POINT:
	{	// if there isn't any node created already then create one
		if(m_pThis == NULL)
		{
			if ( m_ip->SetActiveViewport(hWnd) )
			{
				res = FALSE;
				break;
			}
			// check valid construction plane
			if (m_ip->IsCPEdgeOnInView())
			{ 
				res = FALSE;
				break;
			}

			pVpt->CommitImplicitGrid(sp, flags);

			// Create matrix for node position
			Matrix3 mat(1);

			mat.SetTrans(pVpt->SnapPoint(sp,sp,NULL,SNAP_IN_3D));

			theHold.Begin();

			INode* pINode = NULL;
			CreateVehicleNodeDefault(m_ip, m_pVehicleNode, pINode, &mat);

			// reference node, so that we are informed when it is destroyed
			ReplaceReference(0, pINode);


			// begin edit parameters
			m_pVehicleNode->BeginEditParams(m_ip, BEGIN_EDIT_CREATE, NULL);

			m_ip->RedrawViews(t,REDRAW_BEGIN,m_pThis); 
		}
		else
		{
			// move node about with the mouse
			Matrix3 mat(1);
			mat.SetTrans(pVpt->SnapPoint(sp,sp,NULL,SNAP_IN_3D));
			m_ip->SetNodeTMRelConstPlane(m_pThis, mat);
			// translate up 1 metre
//			mat = m_pThis->GetNodeTM(0);
//			mat.Translate(Point3(0.0f,0.0f,1.0f));
//			m_pThis->SetNodeTM(0, mat);

			// start creating a new node
			// if connecting to a PatrolNode already placed down
			if(m_pCanConnectTo)
			{
				// remove link between current node and previous node and replace it with a link
				// between the previous node and the node the mouse has moved over 'm_pCanConnectTo' and
				// a link between the current node and 'm_pCanConnectTo'
				// here  it is SHAUN
				RemoveLink();
//				m_pPrev->ConnectTo(m_pCanConnectTo);
				ConnectTo(m_pPrev, m_pCanConnectTo);
				AutoCreate(m_ip, m_pPrev, (PatrolNode *)m_pCanConnectTo->GetObjectRef());
				m_pPrev->UpdateAllLinks(m_ip);
				m_pPrev = (PatrolNode *)m_pCanConnectTo->GetObjectRef();


				res = FALSE;
				StopCreating(pVpt);

			}
			// otherwise create a new PatrolNode and link it to the current node
			else
			{
				// store old Vehicle Node
				if(NULL != m_pPrev)
				{
					m_pPrev->UpdateAllLinks(m_ip);
					AutoCreate(m_ip, m_pPrev, m_pVehicleNode);
				}
				m_pPrev = m_pVehicleNode;

				PatrolNode*	pVehicleNode	= NULL;
				INode*			pINode			= NULL;
				CreateVehicleNodeDefault(m_ip, pVehicleNode, pINode, &mat);
				// connect old connection to the new connection
//				m_pVehicleNode->ConnectTo(pINode);
				ConnectTo(m_pVehicleNode, pINode);
				m_pVehicleNode->EndEditParams(m_ip, END_EDIT_REMOVEUI, NULL);

				// reference new connection
				ReplaceReference(0, pINode);
				m_pVehicleNode = pVehicleNode;
				m_pVehicleNode->BeginEditParams(m_ip, BEGIN_EDIT_CREATE, NULL);
			}
		}
		m_ip->RedrawViews(t,REDRAW_NORMAL,m_pThis);  
	}
	break;
	case MOUSE_ABORT:
		res = FALSE;
		StopCreating(pVpt);
		break;

    case MOUSE_PROPCLICK:
		// right click while between creations
		m_ip->RemoveMode(NULL);
		break;
	}


	// if have moved over a Vehicle Connection that current connection can connect to 
	m_pCanConnectTo = m_ip->PickNode(hWnd, sp, this);
	if(m_pCanConnectTo)
		SetCursor(LoadCursor(hInstance, MAKEINTRESOURCE(IDC_CONNECT_CURSOR1)));
	else
		SetCursor(m_ip->GetSysCursor(SYSCUR_DEFARROW));

	m_ip->ReleaseViewport(pVpt);

	return res;
}

void PatrolNodeCreateMgr::CreateVehicleNode(IObjCreate* ip, PatrolNode*& pVehicleNode)
{
	pVehicleNode = (PatrolNode *)ip->CreateInstance(HELPER_CLASS_ID, PATROL_NODE_CLASS_ID);
	theIMaxDataStore.AddData(pVehicleNode);
}

void PatrolNodeCreateMgr::CreateVehicleNodeDefault(IObjCreate* ip, PatrolNode*& pVehicleNode, INode*& pINode, Matrix3* pMat, bool bCopyDefaultParams/*=true*/)
{
	pVehicleNode = (PatrolNode *)ip->CreateInstance(HELPER_CLASS_ID, PATROL_NODE_CLASS_ID);
	pINode = ip->CreateObjectNode(pVehicleNode);

	// set node position
	if(NULL != pMat)
	{
		ip->SetNodeTMRelConstPlane(pINode, *pMat);
	}

	if(bCopyDefaultParams)
	{
#if !defined( _WIN64 )
		dmat::AttributeDialog* pDialog = (dmat::AttributeDialog *)GetWindowLong(m_hWndIMaxNode, GWL_USERDATA);
#else
		dmat::AttributeDialog* pDialog = (dmat::AttributeDialog *)GetWindowLong(m_hWndIMaxNode, GWLP_USERDATA);
#endif
		// Dialog only valid if creating not editing
		if(NULL != pDialog)
		{
			pDialog->SyncWithControls();
		}
//		theIMaxDataStore.CopyData(pVehicleNode, &m_DefaultVehicleNode);
		theIMaxDataStore.CopyData(pVehicleNode, m_pDefaultPatrolNode);
	}
}

void PatrolNodeCreateMgr::CreateVehicleLink(IObjCreate* ip, PatrolLink*& pVehicleLink)
{
	pVehicleLink = (PatrolLink *)ip->CreateInstance(HELPER_CLASS_ID, PATROL_LINK_CLASS_ID);
	theIMaxDataStore.AddData(pVehicleLink);
}

void PatrolNodeCreateMgr::CreateVehicleLinkDefault(IObjCreate* ip, PatrolLink*& pVehicleLink, INode*& pINode, bool bCopyDefaultParams/*=true*/)
{
	pVehicleLink = (PatrolLink *)ip->CreateInstance(HELPER_CLASS_ID, PATROL_LINK_CLASS_ID);
	pINode = ip->CreateObjectNode(pVehicleLink);

	if(bCopyDefaultParams)
	{
#if !defined( _WIN64 )
		dmat::AttributeDialog* pDialog = (dmat::AttributeDialog *)GetWindowLong(m_hWndIMaxLink, GWL_USERDATA);
#else
		dmat::AttributeDialog* pDialog = (dmat::AttributeDialog *)GetWindowLong(m_hWndIMaxLink, GWLP_USERDATA);
#endif
		// The dialog is only valid if were connecting Not editing
		if(NULL != pDialog)
		{
			pDialog->SyncWithControls();
		}
		theIMaxDataStore.CopyData(pVehicleLink, m_pDefaultPatrolLink);
//		theIMaxDataStore.CopyData(pVehicleLink, &m_DefaultVehicleLink);
	}
}

void PatrolNodeCreateMgr::RemoveLink()
{
	PatrolLink* pLink = m_pVehicleNode->GetLinkBetween(m_pPrev);
	assert(pLink);

	m_pVehicleNode->RemoveReference(pLink);
}

void PatrolNodeCreateMgr::RemovePatrolNode(INode* pNode)
{
	pNode->Delete(m_ip->GetTime(), TRUE);
}

void PatrolNodeCreateMgr::AutoCreate(Interface* ip, PatrolNode* pNode1, PatrolNode* pNode2)
{
	if(false == m_bAutoCreate)
		return;

	PatrolLink* pLink = pNode1->GetLinkBetween(pNode2);
	assert(pLink);
	m_fAutoCreateDistance = m_pSpinLinkLen->GetFVal();
	float fAutoCreateDist = m_fAutoCreateDistance;
	float fLinkLen = pLink->GetLength(ip);
	if((fLinkLen / m_fAutoCreateDistance) > MAX_NUM_NODES_AUTO_CREATE)
	{
		fAutoCreateDist =  fLinkLen / MAX_NUM_NODES_AUTO_CREATE;
	}
	PatrolNode* pNode = pLink->AutoCreate(ip, fAutoCreateDist);
	if(NULL != pNode)
	{
		m_pVehicleNode = pNode;
	}
}

void PatrolNodeCreateMgr::StopCreating(ViewExp *pVpt)
{
	// if node created stop editing connection and delete the node
	pVpt->ReleaseImplicitGrid();
	if(m_pThis)
	{
		// If we have a previous node
		assert(m_pVehicleNode);

		PatrolLink* pLink = m_pVehicleNode->GetLinkBetween(m_pPrev);
		if(pLink)
		{
			m_pVehicleNode->RemoveReference(pLink);
		}
		if(m_pThis)
		{
			RemovePatrolNode(m_pThis);
		}
	}
	if(m_pPrev)
	{
		theHold.Begin();
		m_ip->SelectNode(m_pPrev->GetINode());
		theHold.Accept("Select");
	}
	m_pPrev = NULL;
	
	theHold.Accept("Create PatrolConnection");
	m_ip->RedrawViews(m_ip->GetTime(), REDRAW_END,m_pThis);  
}

//
//        name: ConnectTo
// description: Connect a PatrolNode to another INode
//

PatrolLink* PatrolNodeCreateMgr::ConnectTo(PatrolNode* pNodeToConnect, INode *pINodeTarget)
{
	PatrolNode *pNodeTarget = (PatrolNode *)pINodeTarget->EvalWorldState(0).obj;

	assert(pNodeTarget->ClassID() == PATROL_NODE_CLASS_ID);

	if(pNodeToConnect->GetLinkBetween(pNodeTarget) != NULL)
		return NULL;

	INode *pINodeToConnect = pNodeToConnect->GetINode();

	theHold.Begin();

	PatrolLink *pLink = NULL; //(PatrolLink *)m_ip->CreateInstance(HELPER_CLASS_ID, PATROL_LINK_CLASS_ID);
	INode *pLinkNode = NULL; //m_ip->CreateObjectNode(pLink);
	CreateVehicleLinkDefault(m_ip, pLink, pLinkNode);

	pLink->ReplaceReference(0, pINodeToConnect->GetTMController());
	pLink->ReplaceReference(1, pINodeTarget->GetTMController());
	pNodeToConnect->AddReference(pLink);
	pNodeTarget->AddReference(pLink);

	NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
	pNodeTarget->NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);

	theHold.Accept("Link PatrolNode");

	m_ip->RedrawViews(m_ip->GetTime());

	return pLink;
}


// --- Windows callback ------------------------------------------------------------------------------------
#if !defined( _WIN64 )
BOOL CALLBACK EditAutoCreateProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
#else
INT_PTR CALLBACK EditAutoCreateProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
#endif
{
	switch(message)
	{
	case WM_INITDIALOG:
		break;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDC_CHECK_AUTOCREATE:
			PatrolNodeCreateMgr::m_bAutoCreate = (bool) SendMessage(
				GetDlgItem(PatrolNodeCreateMgr::m_hWndAutoCreate, IDC_CHECK_AUTOCREATE), // handle to destination control     
				(UINT) BM_GETCHECK,      // message ID     
				0,      // = 0; not used, must be zero    
				0		// = 0; not used, must be zero 
				);  
			break;
		case IDC_CHECK_VIEW:
			PatrolNodeCreateMgr::m_bViewDifferent = (bool) SendMessage(
				GetDlgItem(PatrolNodeCreateMgr::m_hWndAutoCreate, IDC_CHECK_VIEW), // handle to destination control     
				(UINT) BM_GETCHECK,      // message ID     
				0,      // = 0; not used, must be zero    
				0		// = 0; not used, must be zero 
				);  
			GetCOREInterface()->RedrawViews(GetCOREInterface()->GetTime());
			break;
		default:
			break;
		}
		break;

	case WM_CLOSE:
		break;

	default:
		return FALSE;
	}
	return TRUE;

}

