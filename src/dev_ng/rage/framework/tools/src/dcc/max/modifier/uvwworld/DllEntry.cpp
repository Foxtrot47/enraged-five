#include <max.h>
#include "mods.h"
#include "UVWworld.h"

////////////////////////////////////////////////////////////////////////////////////////////
HINSTANCE hInstance;
int controlsInit = FALSE;
extern ClassDesc* GetEditMeshModDesc();

////////////////////////////////////////////////////////////////////////////////////////////
BOOL WINAPI DllMain(HINSTANCE hinstDLL,ULONG fdwReason,LPVOID lpvReserved)
{
	hInstance = hinstDLL;				// Hang on to this DLL's instance handle.

	if (!controlsInit)
	{
		controlsInit = TRUE;
		InitCommonControls();			// Initialize Win95 controls
	}
			
	return (TRUE);
}

////////////////////////////////////////////////////////////////////////////////////////////
__declspec( dllexport ) const TCHAR* LibDescription()
{
	return GetString(IDS_LIBDESCRIPTION);
}

////////////////////////////////////////////////////////////////////////////////////////////
__declspec( dllexport ) int LibNumberClasses()
{
	return 1;
}

////////////////////////////////////////////////////////////////////////////////////////////
__declspec( dllexport ) ClassDesc* LibClassDesc(int i)
{
	switch(i) 
	{
	case 0: 
		return (ClassDesc*)GetEditMeshModDesc();
	default: 
		return (ClassDesc*)0;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
__declspec( dllexport ) ULONG LibVersion()
{
	return VERSION_3DSMAX;
}

////////////////////////////////////////////////////////////////////////////////////////////
TCHAR *GetString(int id)
{
	static TCHAR buf[256];

	if (hInstance)
	{
		return LoadString(hInstance, id, buf, sizeof(buf)) ? buf : NULL;
	}

	return NULL;
}
