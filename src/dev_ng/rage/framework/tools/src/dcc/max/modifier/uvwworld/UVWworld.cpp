#include "mods.h"
#include "MeshDLib.h"
#include "uvwworld.h"
#include "edmdata.h"
#include "edmrest.h"

////////////////////////////////////////////////////////////////////////////////////////////
#define NAMEDVSEL_NAMES_CHUNK		0x2805
#define NAMEDFSEL_NAMES_CHUNK		0x2806
#define NAMEDESEL_NAMES_CHUNK		0x2807
#define FLAGS_CHUNK 				0x2740
#define VSELSET_CHUNK				0x2845
#define FSELSET_CHUNK				0x2846
#define ESELSET_CHUNK				0x2847
#define MDELTA_CHUNK				0x4000
#define MUV_CHUNK					0x4001

////////////////////////////////////////////////////////////////////////////////////////////
HWND                EditMeshMod::hSel = NULL;
IObjParam*          EditMeshMod::ip      = NULL;
MoveModBoxCMode*    EditMeshMod::moveMode        = NULL;
RotateModBoxCMode*  EditMeshMod::rotMode 	     = NULL;
UScaleModBoxCMode*  EditMeshMod::uscaleMode      = NULL;
NUScaleModBoxCMode* EditMeshMod::nuscaleMode     = NULL;
SquashModBoxCMode*  EditMeshMod::squashMode      = NULL;
SelectModBoxCMode*  EditMeshMod::selectMode      = NULL;
BOOL				EditMeshMod::ignoreBackfaces = FALSE;
BOOL				EditMeshMod::ignoreVisEdge = FALSE;
BOOL				EditMeshMod::selByVert = FALSE;
int					EditMeshMod::pickBoxSize = DEF_PICKBOX_SIZE;
BOOL				EditMeshMod::rsSel = TRUE;

static EditMeshClassDesc editMeshDesc;
static TimeValue dragTime;
static bool dragRestored;
static bool inDragMove=FALSE;
static int namedSelID[] = {NAMEDVSEL_NAMES_CHUNK,NAMEDFSEL_NAMES_CHUNK,NAMEDESEL_NAMES_CHUNK};

////////////////////////////////////////////////////////////////////////////////////////////
extern ClassDesc* GetEditMeshModDesc() 
{ 
	return &editMeshDesc; 
}

////////////////////////////////////////////////////////////////////////////////////////////
// EditMeshClassDesc
////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshClassDesc::ResetClassParams(BOOL fileReset) 
{
	EditMeshMod::selByVert = FALSE;
	EditMeshMod::rsSel = TRUE;
	ResetEditMeshUI();
}

////////////////////////////////////////////////////////////////////////////////////////////
// NamedSetDelete
////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////
NamedSetDelete::NamedSetDelete (EditMeshData *em, int n, BitArray &d) 
{
	emd = em;
	nsl = n;
	del = d;
	oldSets.SetCount (emd->selSet[nsl].Count());

	for (int i=0; i<emd->selSet[nsl].Count(); i++) 
	{
		oldSets[i] = new BitArray;
		(*oldSets[i]) = (*(emd->selSet[nsl].sets[i]));
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
NamedSetDelete::~NamedSetDelete () 
{
	for (int i=0; i<oldSets.Count(); i++) 
	{
		delete oldSets[i];
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
void NamedSetDelete::Restore (int isUndo) 
{
	int i, max = oldSets.Count();

	if (emd->selSet[nsl].Count() < max) 
	{
		max = emd->selSet[nsl].Count();
	}

	for (i=0; i<max; i++) 
	{
		*(emd->selSet[nsl].sets[i]) = *(oldSets[i]);
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
// ScaleVertDeltas
////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////
BOOL ScaleVertDeltas::proc(ModContext *mc) 
{
	EditMeshData *emdata = (EditMeshData*)mc->localData;
	int i;
	if (!emdata) 
	{
		return TRUE;
	}

	if (theHold.Holding()) 
	{
		theHold.Put(new VertexEditRestore(emdata, mod));
	}

	MeshDelta & md = emdata->mdelta;

	for (i=0; i<md.vMove.Count(); i++) 
	{
		md.vMove[i].dv *= f;
	}

	for (i=0; i<md.vClone.Count(); i++) 
	{
		md.vClone[i].dv *= f;
	}

	return TRUE;
}

////////////////////////////////////////////////////////////////////////////////////////////
// EditMeshMod
////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////
EditMeshMod::EditMeshMod() 
{
	selLevel = SL_OBJECT;
	useEdgeDist = 0;
	edgeIts = 1;
	arIgBack = 0;
	emFlags = 0;
}

////////////////////////////////////////////////////////////////////////////////////////////
EditMeshMod::~EditMeshMod() 
{
//	ClearSetNames();
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::RescaleWorldUnits(float f) 
{
	if(TestAFlag(A_WORK1)) 
	{
		return;
	}

	SetAFlag(A_WORK1);
	ScaleVertDeltas svd(f,this);
	EnumModContexts(&svd);
}

////////////////////////////////////////////////////////////////////////////////////////////
Interval EditMeshMod::LocalValidity(TimeValue t) 
{
	if(TestAFlag(A_MOD_BEING_EDITED))
	{
	 	return NEVER;
	}

	return FOREVER;
}

////////////////////////////////////////////////////////////////////////////////////////////
RefTargetHandle EditMeshMod::Clone(RemapDir& remap) 
{
	EditMeshMod* newmod = new EditMeshMod();
	newmod->selLevel = selLevel;
	newmod->useEdgeDist = useEdgeDist;
	newmod->edgeIts = edgeIts;
	newmod->arIgBack = arIgBack;
	BaseClone(this, newmod, remap);

	return(newmod);
}

////////////////////////////////////////////////////////////////////////////////////////////
BOOL EditMeshMod::DependOnTopology(ModContext &mc) 
{
	EditMeshData *meshData = (EditMeshData*)mc.localData;

	if (meshData) 
	{
		if (meshData->GetFlag(EMD_HASDATA)) 
		{
			return TRUE;
		}
	}

	return FALSE;
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::ModifyObject(TimeValue t, ModContext &mc, ObjectState * os, INode *node) 
{
	DbgAssert(os->obj->IsSubClassOf(triObjectClassID));

	TriObject *triOb = (TriObject *)os->obj;
	Mesh &mesh = triOb->mesh;

	EditMeshData *meshData;

	if(!mc.localData)
	{
		mc.localData = new EditMeshData();
		meshData = (EditMeshData*)mc.localData;
	}
	else 
	{
		meshData = (EditMeshData*)mc.localData;
	}

	meshData->SetModifier(this);

	meshData->Apply(t,triOb,this);
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::NotifyInputChanged(Interval changeInt, PartID partID, RefMessage message, ModContext *mc) 
{
	if(mc->localData) 
	{
		EditMeshData *meshData = (EditMeshData*)mc->localData;

		if(meshData) 
		{
			meshData->Invalidate(partID,FALSE);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::SelectSubComponent (HitRecord *hitRec, BOOL selected, BOOL all, BOOL invert) 
{
	if(selLevel == SL_OBJECT) 
	{
		return;
	}

	if(!ip) 
	{
		return;
	}

	TimeValue t = ip->GetTime();
	ip->ClearCurNamedSelSet();

	AdjFaceList *af=NULL;
	AdjEdgeList *ae=NULL;
	BitArray nsel;
	HitRecord *hr;
	EditMeshData *meshData;
	Mesh *mesh;

	ModContextList mcList;
	INodeTab nodes;
	ip->GetModContexts(mcList,nodes);
	int nd;
	int localSelByVert = selByVert;

	for (nd=0; nd<mcList.Count(); nd++) 
	{
		meshData = (EditMeshData*) mcList[nd]->localData;
		if (meshData==NULL) 
		{
			continue;
		}

		if (!all && (hitRec->modContext->localData != meshData)) 
		{
			continue;
		}

		for (hr=hitRec; hr!=NULL; hr=hr->Next()) 
		{
			if (hr->modContext->localData == meshData) 
			{
				break;
			}
		}

		if (hr==NULL) 
		{
			continue;
		}

		mesh = meshData->GetMesh (t);

		if (selLevel>=SL_POLY) 
		{
			af = meshData->TempData(t)->AdjFList();
		}

		if (localSelByVert) 
		{
			ae = meshData->TempData(t)->AdjEList();
		}

		switch (selLevel) 
		{
		case SL_VERTEX:
			nsel = mesh->vertSel;
			for (; hr != NULL; hr=hr->Next()) 
			{
				if (hr->modContext->localData != meshData) 
				{
					continue;
				}

				nsel.Set (hr->hitInfo, invert ? !nsel[hr->hitInfo] : selected);

				if (!all) 
				{
					break;
				}
			}
			meshData->SetVertSel (nsel, this, t);
			break;

		case SL_EDGE:
			nsel = mesh->edgeSel;
			for (; hr != NULL; hr=hr->Next()) 
			{
				if (hr->modContext->localData != meshData) 
				{
					continue;
				}

				if (localSelByVert) 
				{
					DWORDTab & list = ae->list[hr->hitInfo];
					for (int i=0; i<list.Count(); i++) 
					{
						MEdge & me = ae->edges[list[i]];
						for (int j=0; j<2; j++) 
						{
							if (me.f[j] == UNDEFINED) 
							{
								continue;
							}

							DWORD ei = mesh->faces[me.f[j]].GetEdgeIndex (me.v[0], me.v[1]);

							if (ei>2) 
							{
								continue;
							}

							ei += me.f[j]*3;
							nsel.Set (ei, invert ? !mesh->edgeSel[ei] : selected);
						}
					}
				} 
				else 
				{
					nsel.Set (hr->hitInfo, invert ? !nsel[hr->hitInfo] : selected);
				}

				if (!all) 
				{
					break;
				}
			}

			meshData->SetEdgeSel (nsel, this, t);
			break;

		case SL_FACE:
			nsel = mesh->faceSel;
			for (; hr != NULL; hr=hr->Next())
			{
				if (hr->modContext->localData != meshData) 
				{
					continue;
				}

				if (localSelByVert) 
				{
					DWORDTab & list = ae->list[hr->hitInfo];
					for (int i=0; i<list.Count(); i++) 
					{
						MEdge & me = ae->edges[list[i]];
						for (int j=0; j<2; j++) 
						{
							if (me.f[j] == UNDEFINED) 
							{
								continue;
							}

							nsel.Set (me.f[j], invert ? !mesh->faceSel[me.f[j]] : selected);
						}
					}
				} 
				else 
				{
					nsel.Set (hr->hitInfo, invert ? !mesh->faceSel[hr->hitInfo] : selected);
				}

				if (!all)
				{
					break;
				}
			}

			meshData->SetFaceSel (nsel, this, t);
			break;

		case SL_POLY:
		case SL_ELEMENT:
			nsel.SetSize (mesh->getNumFaces());
			nsel.ClearAll ();
			for (; hr != NULL; hr=hr->Next())
			{
				if (hr->modContext->localData != meshData) 
				{
					continue;
				}

				if (!localSelByVert) 
				{
					if (selLevel==SL_ELEMENT) 
					{
						mesh->ElementFromFace (hr->hitInfo, nsel, af);
					}
					else 
					{
						mesh->PolyFromFace (hr->hitInfo, nsel, GetPolyFaceThresh(), ignoreVisEdge, af);
					}
				} 
				else 
				{
					DWORDTab & list = ae->list[hr->hitInfo];

					for (int i=0; i<list.Count(); i++) 
					{
						MEdge & me = ae->edges[list[i]];

						for (int j=0; j<2; j++) 
						{
							if (me.f[j] == UNDEFINED) 
							{
								continue;
							}

							if (selLevel==SL_ELEMENT) 
							{
								mesh->ElementFromFace (me.f[j], nsel, af);
							}
							else 
							{
								mesh->PolyFromFace (me.f[j], nsel, GetPolyFaceThresh(), ignoreVisEdge, af);
							}
						}
					}
				}

				if (!all) 
				{
					break;
				}
			}

			if (invert) 
			{
				nsel ^= mesh->faceSel;
			}
			else 
			{
				if (selected) 
				{
					nsel |= mesh->faceSel;
				}
				else 
				{
					nsel = mesh->faceSel & ~nsel;
				}
			}

			meshData->SetFaceSel (nsel, this, t);
		}
	}

	LocalDataChanged ();
	nodes.DisposeTemporary ();
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::ClearSelection(int selLevel) 
{
	if (selLevel==SL_OBJECT)
	{
		return;
	}

	if (!ip)
	{
		return;	
	}

	ModContextList mcList;
	INodeTab nodes;
	ip->GetModContexts(mcList,nodes);
	ip->ClearCurNamedSelSet();
	BitArray nsel;

	for (int i=0; i<mcList.Count(); i++)
	{
		EditMeshData *meshData = (EditMeshData*)mcList[i]->localData;

		if (!meshData) 
		{
			continue;		
		}

		Mesh *mesh = meshData->GetMesh (ip->GetTime());

		switch (selLevel) 
		{
		case SL_VERTEX:
			nsel.SetSize (mesh->getNumVerts());
			nsel.ClearAll ();
			meshData->SetVertSel (nsel, this, ip->GetTime());
			break;
		case SL_FACE:
		case SL_POLY:
		case SL_ELEMENT:
			nsel.SetSize (mesh->getNumFaces());
			nsel.ClearAll ();
			meshData->SetFaceSel (nsel, this, ip->GetTime());
			break;
		case SL_EDGE:
			nsel.SetSize (mesh->getNumFaces()*3);
			nsel.ClearAll ();
			meshData->SetEdgeSel (nsel, this, ip->GetTime());
			break;
		}
	}
	
	nodes.DisposeTemporary();
	LocalDataChanged ();
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::SelectAll(int selLevel) 
{
	if (selLevel==SL_OBJECT) 
	{
		return;
	}

	if (!ip)
	{
		return;	
	}

	ModContextList mcList;
	INodeTab nodes;
	ip->GetModContexts(mcList,nodes);
	ip->ClearCurNamedSelSet();

	for (int i=0; i<mcList.Count(); i++) 
	{
		EditMeshData *meshData = (EditMeshData*)mcList[i]->localData;

		if(!meshData)
		{
			continue;
		}

		Mesh *mesh = meshData->GetMesh (ip->GetTime());
		BitArray sel;

		switch ( selLevel ) 
		{
		case SL_VERTEX:	
			sel.SetSize (mesh->numVerts);
			sel.SetAll ();
			meshData->SetVertSel (sel, this, ip->GetTime());
			break;

		case SL_EDGE:
			sel.SetSize (mesh->numFaces*3);
			sel.SetAll ();
			meshData->SetEdgeSel (sel, this, ip->GetTime());
			break;

		default:
			sel.SetSize (mesh->numFaces);
			sel.SetAll ();
			meshData->SetFaceSel (sel, this, ip->GetTime());
			break;
		}
	}

	nodes.DisposeTemporary();
	LocalDataChanged ();
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::InvertSelection(int selLevel) 
{
	if(!ip)
	{
		return;
	}

	if(selLevel == SL_OBJECT)
	{
		return;
	}

	ModContextList mcList;
	INodeTab nodes;
	ip->GetModContexts(mcList,nodes);
	ip->ClearCurNamedSelSet();

	for (int i = 0; i < mcList.Count(); i++) 
	{
		EditMeshData *meshData = (EditMeshData*)mcList[i]->localData;

		if(!meshData)
		{
			continue;		
		}

		Mesh *mesh = meshData->GetMesh (ip->GetTime());
		BitArray sel;

		switch ( selLevel ) 
		{
		case SL_VERTEX:	
			sel = ~mesh->vertSel;
			meshData->SetVertSel (sel, this, ip->GetTime ());
			break;
	
		case SL_EDGE:
			sel = ~mesh->edgeSel;
			meshData->SetEdgeSel (sel, this, ip->GetTime());
			break;

		default:
			sel = ~mesh->faceSel;
			meshData->SetFaceSel (sel, this, ip->GetTime());
			break;
		}
	}
	
	nodes.DisposeTemporary();
	LocalDataChanged ();
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::InvalidateDistances() 
{
	if(!ip)
	{
		return;
	}

	if(selLevel == SL_OBJECT) 
	{
		return;
	}

	ModContextList mcList;
	INodeTab nodes;
	ip->GetModContexts(mcList,nodes);

	for(int i = 0;i<mcList.Count();i++) 
	{
		EditMeshData *meshData = (EditMeshData*)mcList[i]->localData;

		if(!meshData)
		{
			continue;
		}

		MeshTempData *emt = meshData->tempData;

		if(!emt)
		{
			continue;
		}

		emt->InvalidateDistances();
	}

	nodes.DisposeTemporary();
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::InvalidateAffectRegion () 
{
	if(!ip)
	{
		return;
	}

	if(selLevel == SL_OBJECT) 
	{
		return;
	}

	ModContextList mcList;
	INodeTab nodes;
	ip->GetModContexts(mcList,nodes);

	for(int i = 0; i < mcList.Count(); i++) 
	{
		EditMeshData *meshData = (EditMeshData*)mcList[i]->localData;

		if(!meshData)
		{
			continue;
		}

		MeshTempData *emt = meshData->tempData;

		if(!emt) 
		{
			continue;
		}

		emt->InvalidateAffectRegion ();
	}
	nodes.DisposeTemporary();
}

////////////////////////////////////////////////////////////////////////////////////////////
DWORD EditMeshMod::GetSelLevel () 
{
	switch (selLevel) 
	{
	case SL_OBJECT: return IMESHSEL_OBJECT;
	case SL_VERTEX: return IMESHSEL_VERTEX;
	case SL_EDGE: return IMESHSEL_EDGE;
	}

	return IMESHSEL_FACE;
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::SetSelLevel (DWORD sl) 
{
	switch (sl) 
	{
	case IMESHSEL_OBJECT:
		selLevel = SL_OBJECT;
		break;
	case IMESHSEL_VERTEX:
		selLevel = SL_VERTEX;
		break;
	case IMESHSEL_EDGE:
		selLevel = SL_EDGE;
		break;
	case IMESHSEL_FACE:
		if (selLevel < SL_FACE) 
		{
			selLevel = SL_POLY;	// don't change if we're already in a face mode.
		}
		break;
	}

	if(ip) 
	{
		ip->SetSubObjectLevel(selLevel);
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::LocalDataChanged () 
{
	NotifyDependents(FOREVER, PART_SELECT, REFMSG_CHANGE);

	if(!ip) 
	{
		return;	
	}

	ip->ClearCurNamedSelSet();
	InvalidateNumberSelected ();
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::LocalDataChanged (DWORD parts) 
{
	NotifyDependents(FOREVER, parts, REFMSG_CHANGE);

	if(!ip) 
	{
		return;
	}

	ip->ClearCurNamedSelSet();

	if(parts & SELECT_CHANNEL) 
	{
		InvalidateNumberSelected ();
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
// from MeshDeltaUser
////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::GetUIParam(meshUIParam uiCode, float & ret) 
{
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::SetUIParam(meshUIParam uiCode, float val) 
{
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::GetUIParam(meshUIParam uiCode, int & ret) 
{
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::SetUIParam(meshUIParam uiCode, int val) 
{
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::ToggleCommandMode(meshCommandMode mode) 
{
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::ButtonOp (meshButtonOp opcode) 
{
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::ActivateSubobjSel(int level, XFormModes& modes ) 
{	
	ModContextList mcList;
	INodeTab nodes;
	int old = selLevel;

	if (!ip) 
	{
		return;
	}

	ip->GetModContexts(mcList,nodes);

	if ((selLevel != level) && ((level<SL_FACE) || (selLevel<SL_FACE))) 
	{
		ExitAllCommandModes (level == SL_OBJECT, level==SL_OBJECT);
	}

	selLevel = level;
	
	if (level!=SL_OBJECT) 
	{
		modes = XFormModes(moveMode,rotMode,nuscaleMode,uscaleMode,squashMode,selectMode);
	}

	if (selLevel != old) 
	{
		// Modify the caches to reflect the new sel level.
		for ( int i = 0; i < mcList.Count(); i++ ) 
		{
			EditMeshData *meshData = (EditMeshData*)mcList[i]->localData;
			if ( !meshData ) 
			{
				continue;		
			}
		
			if ( meshData->MeshCached(ip->GetTime()) ) 
			{
				Mesh *mesh = meshData->GetMesh (ip->GetTime());
				mesh->dispFlags = 0;
				mesh->SetDispFlag(levelDispFlags[selLevel]);
				mesh->selLevel = meshLevel[selLevel];

			}

			meshData->Invalidate(PART_SELECT);
		}

		NotifyDependents (FOREVER, PART_SELECT|PART_SUBSEL_TYPE|PART_DISPLAY, REFMSG_CHANGE);	
		ip->PipeSelLevelChanged();
	}
	
	nodes.DisposeTemporary();

	if(hSel) 
	{
		RefreshSelType ();
	}

	ip->RedrawViews (ip->GetTime());
}

////////////////////////////////////////////////////////////////////////////////////////////
int EditMeshMod::SubObjectIndex(HitRecord *hitRec) 
{
	EditMeshData *meshData = (EditMeshData*)hitRec->modContext->localData;

	if(!meshData) 
	{
		return 0;
	}

	if( !ip ) 
	{
		return 0;
	}

	TimeValue t = ip->GetTime();
	FaceClusterList *fc;
	EdgeClusterList *ec;
	DWORD id;

	switch ( selLevel ) 
	{
	case SL_VERTEX:
		// Changed SubObjectIndex to mean the actual index of the vertex,
		// not the nth selected.
		return hitRec->hitInfo;
	
	case SL_FACE:
	case SL_POLY:
	case SL_ELEMENT:
		fc = meshData->TempData(t)->FaceClusters();
		id = (*fc)[hitRec->hitInfo];
		if (id!=UNDEFINED) 
		{
			return id;
		}
		else 
		{
			return 0;
		}

	case SL_EDGE:
		ec = meshData->TempData(t)->EdgeClusters();
		id = (*ec)[hitRec->hitInfo];

		if (id!=UNDEFINED) 
		{
			return id;
		}
		else 
		{
			return 0;
		}

	default:
		return 0;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::GetSubObjectCenters (SubObjAxisCallback *cb,TimeValue t,INode *node,ModContext *mc) 
{
	Matrix3 tm = node->GetObjectTM(t);	
	
	DbgAssert(ip);

	if (!mc->localData)
	{
		return;
	}

	EditMeshData *meshData = (EditMeshData*)mc->localData;		
	Mesh *mesh = meshData->GetMesh (t);
	int i;
	Point3 cent = Point3(0,0,0);
	
	switch (selLevel) 
	{
	case SL_VERTEX:
		int ct;
		for (i=0, ct=0; i<mesh->getNumVerts(); i++) 
		{
			if (!mesh->vertSel[i]) 
			{
				continue;
			}

			cent += mesh->verts[i];
			ct++;
		}

		if (ct) 
		{
			cent /= float(ct);	
			cb->Center(cent*tm,0);
		}

		break;

	case SL_EDGE:
	case SL_FACE:
	case SL_POLY:
	case SL_ELEMENT:
		Tab<Point3> *centers;
		centers = meshData->TempData(t)->ClusterCenters(meshLevel[selLevel]);
		for (i=0; i<centers->Count(); i++)
		{
			cb->Center((*centers)[i]*tm,i);
		}
		break;

	default:
		cb->Center(tm.GetTrans(),0);
		break;
	}		
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::GetSubObjectTMs (SubObjAxisCallback *cb,TimeValue t,INode *node,ModContext *mc) 
{
	Matrix3 tm;	
	Matrix3 otm = node->GetObjectTM(t);

	if (!mc->localData ) 
	{
		return;
	}

	EditMeshData *meshData = (EditMeshData*)mc->localData;
	Mesh *mesh = meshData->GetMesh (t);
	int i,j;

	switch (selLevel) 
	{
	case SL_VERTEX:
		if (mesh->vertSel.NumberSet()==0) 
		{
			return;
		}

		if (ip->GetCommandMode()->ID()==CID_SUBOBJMOVE) 
		{
			Tab<Point3> *vnorms;
			vnorms = meshData->TempData(t)->VertexNormals();

			for (i=0,j=0; i<vnorms->Count(); i++) 
			{
				if (!mesh->vertSel[i]) 
				{
					continue;
				}

				Point3 n = VectorTransform(otm, (*vnorms)[i]);
				n = Normalize(n);
				MatrixFromNormal(n,tm);
				tm.SetTrans(mesh->verts[i]*otm);
				cb->TM(tm, j++);
			}
		} 
		else 
		{
			Point3 norm;
			Point3 cent;
			int ct;
			cent = Point3(0,0,0);
			ct=0;

			// Comute average face normal
			norm = AverageSelVertNormal(*mesh);

			// Compute center of selection
			for (i=0; i<mesh->getNumVerts(); i++) 
			{
				if (!mesh->vertSel[i]) 
				{
					continue;
				}

				cent += mesh->verts[i];
				ct++;
			}

			if (ct) 
			{
				cent /= float(ct);
			}

			cent = cent * otm;
			norm = Normalize(VectorTransform(otm,norm));
			Matrix3 mat;
			MatrixFromNormal(norm,mat);
			mat.SetTrans(cent);
			cb->TM(mat,0);
		}
		break;

	case SL_EDGE:
	case SL_FACE:
	case SL_POLY:
	case SL_ELEMENT:
		Tab<Point3> *norms;
		norms = meshData->TempData(t)->ClusterNormals(meshLevel[selLevel]);
		Tab<Point3> *centers;
		centers = meshData->tempData->ClusterCenters(meshLevel[selLevel]);

		for (int i=0; i<norms->Count(); i++) 
		{
			Point3 n = VectorTransform(otm,(*norms)[i]);
			n = Normalize(n);
			MatrixFromNormal(n,tm);
			tm.SetTrans((*centers)[i]*otm);
			cb->TM(tm,i);
		}

		break;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::DeleteMeshDataTempData()
	{
	ModContextList mcList;
	INodeTab nodes;

	if ( !ip ) 
	{
		return;		
	}

	ip->GetModContexts(mcList,nodes);

	for ( int i = 0; i < mcList.Count(); i++ ) 
	{
		EditMeshData *meshData = (EditMeshData*)mcList[i]->localData;

		if ( !meshData ) 
		{
			continue;				
		}

		if ( meshData->tempData ) 
		{
			delete meshData->tempData;
		}

		meshData->tempData = NULL;
	}

	nodes.DisposeTemporary();
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::CreateMeshDataTempData()
{
	ModContextList mcList;
	INodeTab nodes;

	if ( !ip ) 
	{
		return;		
	}

	ip->GetModContexts(mcList,nodes);

	for ( int i = 0; i < mcList.Count(); i++ ) 
	{
		EditMeshData *meshData = (EditMeshData*)mcList[i]->localData;

		if ( !meshData ) 
		{
			continue;
		}

		meshData->mod = this;

		if ( meshData->tempData ) 
		{
			continue;
		}

		meshData->tempData = new MeshTempData ();
	}
	
	nodes.DisposeTemporary();
}

////////////////////////////////////////////////////////////////////////////////////////////
bool CheckNodeSelection(Interface *ip,INode *inode) 
{
	if (!ip) 
	{
		return FALSE;
	}

	if (!inode) 
	{
		return FALSE;
	}

	int i, nct = ip->GetSelNodeCount();

	for (i=0; i<nct; i++) 
	{
		if (ip->GetSelNode (i) == inode) 
		{
			return TRUE;
		}
	}

	return FALSE;
}

////////////////////////////////////////////////////////////////////////////////////////////
int EditMeshMod::HitTest(	TimeValue t, 
							INode* inode, 
							int type, 
							int crossing, 
							int flags, 
							IPoint2 *p,
							ViewExp *vpt, 
							ModContext* mc) 
{
	Interval valid;
	int savedLimits,res = 0;
	GraphicsWindow *gw = vpt->getGW();
	HitRegion hr;
	int i,j;

	// Setup GW
	MakeHitRegion(hr,type, crossing, pickBoxSize, p);
	gw->setHitRegion(&hr);
	Matrix3 mat = inode->GetObjectTM(t);
	gw->setTransform(mat);	
	gw->setRndLimits(((savedLimits = gw->getRndLimits()) | GW_PICK) & ~GW_ILLUM);

	if (ignoreBackfaces) 
	{
		gw->setRndLimits(gw->getRndLimits() |  GW_BACKCULL);
	}
	else 
	{
		gw->setRndLimits(gw->getRndLimits() & ~GW_BACKCULL);
	}

	gw->clearHitCode();

	int localSelByVert = selByVert;

	if(mc->localData) 
	{
		EditMeshData *meshData = (EditMeshData*)mc->localData;
		Mesh *mesh = meshData->GetMesh (ip->GetTime());
		SubObjHitList hitList;
		MeshSubHitRec *rec;
		
		DWORD hitLev = hitLevel[selLevel];

		if (localSelByVert) 
		{
			hitLev = SUBHIT_VERTS;
		}

		if (hitLev == SUBHIT_VERTS) 
		{
			BitArray oldHide;

			if (ignoreBackfaces) 
			{
				BOOL flip = mat.Parity();
				oldHide = mesh->vertHide;
				BitArray faceBack;
				faceBack.SetSize (mesh->getNumFaces());
				faceBack.ClearAll ();

				for (i=0; i<mesh->getNumFaces(); i++) 
				{
					DWORD *vv = mesh->faces[i].v;
					IPoint3 A[3];

					for (j=0; j<3; j++) 
					{
						gw->wTransPoint (&(mesh->verts[vv[j]]), &(A[j]));
					}

					IPoint3 d1 = A[1] - A[0];
					IPoint3 d2 = A[2] - A[0];

					if (flip) 
					{
						if ((d1^d2).z > 0) 
						{
							continue;
						}
					} 
					else 
					{
						if ((d1^d2).z < 0) 
						{
							continue;
						}
					}

					for (j=0; j<3; j++) 
					{
						mesh->vertHide.Set (vv[j]);
					}

					faceBack.Set (i);
				}

				for (i=0; i<mesh->getNumFaces(); i++) 
				{
					if (faceBack[i]) 
					{
						continue;
					}

					DWORD *vv = mesh->faces[i].v;

					for (int j=0; j<3; j++) 
					{
						mesh->vertHide.Clear (vv[j]);
					}
				}

				mesh->vertHide |= oldHide;
			}

			DWORD thisFlags = flags | hitLev;

			if ((selLevel != SL_VERTEX) && localSelByVert) 
			{
				thisFlags |= SUBHIT_USEFACESEL;
			}

			res = mesh->SubObjectHitTest(gw, gw->getMaterial(), &hr, thisFlags, hitList);

			if (ignoreBackfaces) 
			{
				mesh->vertHide = oldHide;
			}
		} 
		else 
		{
			res = mesh->SubObjectHitTest(gw, gw->getMaterial(), &hr, flags|hitLev, hitList);
		}

		rec = hitList.First();

		while (rec) 
		{
			vpt->LogHit(inode,mc,rec->dist,rec->index,NULL);
			rec = rec->Next();
		}
	}

	gw->setRndLimits(savedLimits);	

	return res;
}

////////////////////////////////////////////////////////////////////////////////////////////
int EditMeshMod::Display(TimeValue t, INode* inode, ViewExp *vpt, int flags, ModContext *mc) 
{	
	if (!ip)
	{
		return 0;
	}

	if (!GetFlag (EM_EDITING)) 
	{
		return 0;
	}
	// Set up GW
	GraphicsWindow *gw = vpt->getGW();
	int savedLimits = gw->getRndLimits();
	gw->setRndLimits ((savedLimits & ~GW_ILLUM) | GW_ALL_EDGES);
	Matrix3 tm = inode->GetObjectTM(t) * (mc->tm?Inverse(*mc->tm):Matrix3(1));
	gw->setTransform(tm);

	if (ip->GetShowEndResult() && mc->localData && selLevel) 
	{
		tm = inode->GetObjectTM(t);
		gw->setTransform(tm);
		// We need to draw a "gizmo" version of the mesh:
		Point3 colSel=GetSubSelColor();
		Point3 colTicks=GetUIColor (COLOR_VERT_TICKS);
		Point3 colGiz=GetUIColor(COLOR_GIZMOS);
		Point3 colGizSel=GetUIColor(COLOR_SEL_GIZMOS);
		gw->setColor (LINE_COLOR, colGiz);
		EditMeshData *meshData = (EditMeshData*)mc->localData;
		Mesh *mesh = meshData->GetMesh (ip->GetTime());
		AdjEdgeList *ae = meshData->TempData(ip->GetTime())->AdjEList();
		Point3 rp[3];
		int i, ect = ae->edges.Count();
		int es[3];

		for (i=0; i<ect; i++) 
		{
			MEdge & me = ae->edges[i];

			if (me.Hidden (mesh->faces)) 
			{
				continue;
			}

			if (me.Visible (mesh->faces)) 
			{
				es[0] = GW_EDGE_VIS;
			} 
			else 
			{
				if (selLevel < SL_EDGE) 
				{
					continue;
				}

				if (selLevel > SL_FACE) 
				{
					continue;
				}

				es[0] = GW_EDGE_INVIS;
			}

			if (selLevel == SL_EDGE) 
			{
				if (ae->edges[i].Selected (mesh->faces, meshData->GetEdgeSel())) 
				{
					gw->setColor (LINE_COLOR, colGizSel);
				}
				else 
				{
					gw->setColor (LINE_COLOR, colGiz);
				}
			}

			if (selLevel >= SL_FACE) 
			{
				if (ae->edges[i].AFaceSelected (meshData->GetFaceSel())) 
				{
					gw->setColor (LINE_COLOR, colGizSel);
				}
				else 
				{
					gw->setColor (LINE_COLOR, colGiz);
				}
			}

			rp[0] = mesh->verts[me.v[0]];
			rp[1] = mesh->verts[me.v[1]];
			gw->polyline (2, rp, NULL, NULL, FALSE, es);
		}
	}

	gw->setRndLimits(savedLimits);

	return 0;	
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::GetWorldBoundBox(TimeValue t,INode* inode, ViewExp *vpt, Box3& box, ModContext *mc) 
{
	if (!ip)
	{
		return;
	}

	box.Init();
	Matrix3 tm = inode->GetObjectTM(t) * (mc->tm?Inverse(*mc->tm):Matrix3(1));

	if (ip->GetShowEndResult() && mc->localData && selLevel) 
	{
		// We need to draw a "gizmo" version of the mesh:
		Matrix3 tm = inode->GetObjectTM(t);
		EditMeshData *meshData = (EditMeshData*)mc->localData;

		if (meshData->MeshCached (ip->GetTime())) 
		{
			Mesh *mesh = meshData->mesh;
			AdjEdgeList *ae = meshData->TempData(ip->GetTime())->AdjEList();

			int i, ect = ae->edges.Count();

			for (i=0; i<ect; i++) 
			{
				box += tm*mesh->verts[ae->edges[i].v[0]];
				box += tm*mesh->verts[ae->edges[i].v[1]];
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
// IO
////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////
IOResult EditMeshMod::Save(ISave *isave) 
{
	Modifier::Save(isave);

	return IO_OK;
}

////////////////////////////////////////////////////////////////////////////////////////////
IOResult EditMeshMod::Load(ILoad *iload) 
{
	Modifier::Load(iload);

	return IO_OK;
}

////////////////////////////////////////////////////////////////////////////////////////////
IOResult EditMeshMod::SaveLocalData(ISave *isave, LocalModData *ld) 
{
	ULONG nb;
	EditMeshData *em = (EditMeshData *)ld;

	isave->BeginChunk(FLAGS_CHUNK);
	isave->Write(&em->flags,sizeof(DWORD), &nb);
	isave->EndChunk();

	if(em->selSet[NS_VERTEX].Count())
	{
		isave->BeginChunk(VSELSET_CHUNK);
		em->selSet[NS_VERTEX].Save(isave);
		isave->EndChunk();
	}
	if(em->selSet[NS_EDGE].Count()) 
	{
		isave->BeginChunk(ESELSET_CHUNK);
		em->selSet[NS_EDGE].Save(isave);
		isave->EndChunk();
	}
	if(em->selSet[NS_FACE].Count()) 
	{
		isave->BeginChunk(FSELSET_CHUNK);
		em->selSet[NS_FACE].Save(isave);
		isave->EndChunk();
	}

	isave->BeginChunk (MDELTA_CHUNK);
	IOResult res = em->mdelta.Save (isave);
	isave->EndChunk ();

	isave->BeginChunk(MUV_CHUNK);
	isave->Write(&em->m_iNoTVerts,sizeof(DWORD),&nb);
	isave->Write(em->mp_tVerts,sizeof(Point3) * em->m_iNoTVerts,&nb);
	isave->EndChunk();

	return res;
}

////////////////////////////////////////////////////////////////////////////////////////////
IOResult EditMeshMod::LoadLocalData(ILoad *iload, LocalModData **pld) 
{
	ULONG nb;
	IOResult res;
	EditMeshData *em;

	if (*pld==NULL) 
	{
		*pld =(LocalModData *) new EditMeshData();
	}

	em = (EditMeshData *)*pld;
	em->SetModifier (this);

	while (IO_OK==(res=iload->OpenChunk())) 
	{
		switch(iload->CurChunkID())  
		{
			case FLAGS_CHUNK:
				res = iload->Read(&em->flags,sizeof(DWORD), &nb);
				break;
				case VSELSET_CHUNK:
				res = em->selSet[NS_VERTEX].Load(iload);
				break;
			case FSELSET_CHUNK:
				res = em->selSet[NS_FACE].Load(iload);
				break;
			case ESELSET_CHUNK:
				res = em->selSet[NS_EDGE].Load(iload);
				break;
			case MDELTA_CHUNK:
				res = em->mdelta.Load (iload);
				break;
			case MUV_CHUNK:
				res = iload->Read(&em->m_iNoTVerts,sizeof(DWORD),&nb);

				delete[] em->mp_tVerts;
				em->mp_tVerts = new Point3[em->m_iNoTVerts];

				res = iload->Read(em->mp_tVerts,sizeof(Point3) * em->m_iNoTVerts,&nb);
				break;
			}
		iload->CloseChunk();

		if (res!=IO_OK) 
		{
			return res;
		}
	}

	BOOL ret = em->mdelta.CheckOrder();
	ret = em->mdelta.CheckMapFaces();

	return IO_OK;
}

////////////////////////////////////////////////////////////////////////////////////////////
void* EditMeshMod::GetInterface(ULONG id) 
{
	if(id==I_SUBMTLAPI) 
	{
		return (ISubMtlAPI*)this;
	}
	else if (id==I_MESHDELTAUSER) 
	{
		return (MeshDeltaUser*)this;
	}
	else if (id==I_MESHSELECT) 
	{
		return (IMeshSelect*)this;
	}
	else 
	{
		return Modifier::GetInterface(id);
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
bool EditMeshMod::HasActiveSelection () 
{
	if (selLevel == SL_OBJECT) 
	{
		return true;
	}

	ModContextList mcList;
	INodeTab nodes;	
	ip->GetModContexts(mcList,nodes);
	int i;

	for (i=0; i<mcList.Count(); i++) 
	{
		EditMeshData *emd = (EditMeshData *) mcList[i]->localData;
		if (!emd) 
		{
			continue;
		}

		if (emd->GetSel (namedSetLevel[selLevel]).NumberSet() > 0) 
		{
			return true;
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::ShowEndResultChanged(BOOL showEndResult) 
{
	if (!ip) 
	{
		return;
	}

	if (!GetFlag (EM_EDITING)) 
	{
		return;
	}

	NotifyDependents (FOREVER, PART_DISPLAY, REFMSG_CHANGE);
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::DragMoveInit (TimeValue t, bool doMaps) 
{
	if (!ip) 
	{
		return;
	}

	dragTime = t;
	inDragMove = TRUE;
	ModContextList mcList;
	INodeTab nodes;
	ip->GetModContexts(mcList,nodes);

	for (int nd = 0; nd<mcList.Count(); nd++) 
	{
		if (!mcList[nd]->localData) 
		{
			continue;
		}

		EditMeshData *emd = (EditMeshData *) mcList[nd]->localData;
		Mesh *mesh = emd->GetMesh(t);
		emd->tempMove.InitToMesh (*mesh);

		//create copy of uvw list to amend
		emd->createBackupUVW();

		if (emd->tmr) 
		{
			delete emd->tmr;
		}

		emd->tmr = new TempMoveRestore (mesh);
	}

	dragRestored = TRUE;
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::DragMoveRestore () 
{
	if(!ip) 
	{
		return;
	}

	if(dragRestored) 
	{
		return;
	}

	ModContextList mcList;
	INodeTab nodes;
	ip->GetModContexts(mcList,nodes);

	for (int nd = 0; nd<mcList.Count(); nd++) 
	{
		if(!mcList[nd]->localData) 
		{
			continue;
		}

		EditMeshData *emd = (EditMeshData *) mcList[nd]->localData;
		emd->tempMove.vMove.ZeroCount();

		if(!emd->tmr)
		{
			continue;
		}

		emd->tmr->Restore(emd->GetMesh(dragTime));
	}

	LocalDataChanged(PART_GEOM|PART_TEXMAP);

	if(theHold.Holding()) 
	{
		theHold.Put(new CueLocalRestore(this));
	}

	dragRestored = TRUE;
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::DragMove(MeshDelta & md, MeshDeltaUserData *mdu) 
{
	EditMeshData *emd = (EditMeshData *) mdu;
	Mesh *mesh = emd->GetMesh (dragTime);

	if(!ip) 
	{
		return;
	}

	emd->applyUVChange(md,mesh);

	for (int i=0; i<md.vMove.Count(); i++)
	{
		DWORD j = md.vMove[i].vid;
		mesh->verts[j] += md.vMove[i].dv;
	}

	emd->tempMove = md;
	LocalDataChanged(PART_GEOM|PART_TEXMAP);
	dragRestored = FALSE;
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshMod::DragMoveAccept() 
{
	if(!ip) 
	{
		return;
	}

	ModContextList mcList;
	INodeTab nodes;
	ip->GetModContexts(mcList,nodes);

	for(int nd = 0; nd<mcList.Count(); nd++) 
	{
		if(!mcList[nd]->localData) 
		{
			continue;
		}

		EditMeshData *emd = (EditMeshData *) mcList[nd]->localData;

		if(!emd->tmr) 
		{
			continue;
		}

		emd->Invalidate(emd->tmr->ChannelsChanged(), FALSE);
		emd->ApplyMeshDelta(emd->tempMove, this, dragTime);

		emd->saveUVW();

		emd->tempMove.ClearAllOps();
		delete emd->tmr;
		emd->tmr = NULL;
	}

	inDragMove = FALSE;
}
