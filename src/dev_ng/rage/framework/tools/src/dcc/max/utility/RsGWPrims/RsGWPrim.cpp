#include "RsGWPrim.h"

// ClassDesc2* GetRsGWPrimDesc() { 
// 	static RsGWPrimClassDesc RsGWPrimDesc;
// 	return &RsGWPrimDesc; 
// }
Color	RsGWPrim::m_globalColour = Color(0.0f, 1.0f, 1.0f);
Point3	*RsGWPrim::colourCache;
int		*RsGWPrim::edgeVisCache;
// Function publichsing

static Matrix3 identityMatrix(TRUE);

#define NUM_EXTRA_GW_POINTS 3
#define ENABLE_MULTILINE_RENDERING 1

static FPInterfaceDesc theRsGWPrimsImp(
									   RSGWPRIMS_INTERFACE,
									   _T("IRsGWPrim"),
									   0,
									   NULL, //GetRsGWPrimsDesc(),
									   FP_MIXIN,

									   prims_setOrigin, _T("SetOrigin"), 0, TYPE_VOID, 0, 1,
									   _T("origin"), 0, TYPE_MATRIX3,
									   prims_setResolution, _T("SetResolution"), 0, TYPE_VOID, 0, 1,
									   _T("resolution"), 0, TYPE_INT,
									   prims_setColour, _T("SetColour"), 0, TYPE_VOID, 0, 1,
									   _T("colour"), 0, TYPE_COLOR_BR,
									   prims_circle, _T("AddCircle"), 0, TYPE_VOID, 0, 2,
									   _T("radius"), 0, TYPE_FLOAT,
									   _T("localTrans"), 0, TYPE_MATRIX3, f_keyArgDefault, &identityMatrix,
									   prims_sphere, _T("AddSphere"), 0, TYPE_VOID, 0, 2,
									   _T("radius"), 0, TYPE_FLOAT,
									   _T("localTrans"), 0, TYPE_MATRIX3, f_keyArgDefault, &identityMatrix,
									   prims_cone, _T("AddCone"), 0, TYPE_VOID, 0, 4,
									   _T("length"), 0, TYPE_FLOAT,
									   _T("radius"), 0, TYPE_FLOAT,
									   _T("hasCap"), 0, TYPE_BOOL,
									   _T("localTrans"), 0, TYPE_MATRIX3, f_keyArgDefault, &identityMatrix,
									   prims_cylinder, _T("AddCylinder"), 0, TYPE_VOID, 0, 3,
									   _T("length"), 0, TYPE_FLOAT,
									   _T("radius"), 0, TYPE_FLOAT,
									   _T("localTrans"), 0, TYPE_MATRIX3, f_keyArgDefault, &identityMatrix,
									   prims_capsule, _T("AddCapsule"), 0, TYPE_VOID, 0, 4,
									   _T("length"), 0, TYPE_FLOAT,
									   _T("radius"), 0, TYPE_FLOAT,
									   _T("hasCap"), 0, TYPE_BOOL,
									   _T("localTrans"), 0, TYPE_MATRIX3, f_keyArgDefault, &identityMatrix,
									   prims_line, _T("AddLine"), 0, TYPE_VOID, 0, 2,
									   _T("points"), 0, TYPE_POINT3_TAB_BR,
									   _T("localTrans"), 0, TYPE_MATRIX3, f_keyArgDefault, &identityMatrix,
									   prims_clear, _T("Clear"), 0, TYPE_VOID, 0, 0,
									   prims_draw, _T("Draw"), 0, TYPE_VOID, 0, 1,
									   _T("flush"), 0, TYPE_BOOL,
									   end
									   );

FPInterfaceDesc* RsGWPrimsInterface::GetDesc()
{
	return &theRsGWPrimsImp;
}

static RsGWPrimMgrImp theRsGWPrimMgr(
									RSGWPRIMS_INTERFACE,
									_T("RsGWPrimMgr"),
									-1,
									NULL, //GetRsGWPrimsDesc(),
									FP_CORE,//FP_MIXIN

									primMgr_getPrim, _T("getPrim"), 0, TYPE_IOBJECT, 0, 0,
									end
);

RsGWPrim *RsGWPrimMgrImp::GetPrim()
{
	RsGWPrim *x = new RsGWPrim();
	return x;
}
//////////////////////////////////////////////////////////////////////////
// implemantations

RsGWPrim::RsGWPrim(void)
{
	m_resolution = 10;
	m_origin = Matrix3(1);
	ip = NULL;

	edgeVisCache = new int[MAX_PRIM_POINTS];
	colourCache = new Point3[MAX_PRIM_POINTS];
	for (int f=0;f<MAX_PRIM_POINTS;f++)
	{
		edgeVisCache[f]=GW_EDGE_VIS;
		colourCache[f]=m_globalColour;
	}
}

RsGWPrim::~RsGWPrim(void)
{
}

/**
	No fucking comment.
**/
void PushExtraPointsForMaxSDK(RsGWPrim::sGwPolyLine *prim)
{
	for(int k=0;k<NUM_EXTRA_GW_POINTS;k++)
		prim->points.push_back(Point3());
}

void RsGWPrim::AddCircle(float radius, Matrix3 localTrans)
{
	bool applyMatrix = !localTrans.IsIdentity();
	sGwPolyLine *newPrim1 = new sGwPolyLine();
	float step = DegToRad(360.0f/m_resolution);
	for(int k=0;k<m_resolution;k++)
	{
		float ak = (cos (step*k)) * radius;
		float gk = (sin (step*k)) * radius;
		Point3 p1(ak, gk, 0.0f);
		if(applyMatrix)
		{
			p1 = localTrans.PointTransform(p1);
		}
		newPrim1->points.push_back(p1);
		newPrim1->colours.push_back(m_globalColour);
		newPrim1->edgeVis.push_back(GW_EDGE_VIS);
	}
	PushExtraPointsForMaxSDK(newPrim1);
	m_polylines.push_back(newPrim1);
}
void RsGWPrim::AddSphere(float radius, Matrix3 localTrans)
{
	bool applyMatrix = !localTrans.IsIdentity();
	sGwPolyLine *newPrim1 = new sGwPolyLine();
	sGwPolyLine *newPrim2 = new sGwPolyLine();
	sGwPolyLine *newPrim3 = new sGwPolyLine();
	float step = DegToRad(360.0f/m_resolution);
	for(int k=0;k<m_resolution+1;k++)
	{
		float ak = (cos (step*k)) * radius;
		float gk = (sin (step*k)) * radius;
		Point3 p1(ak, gk, 0.0f);
		Point3 p2(ak, 0.0f, gk);
		Point3 p3(0.0f, ak, gk);
		if(applyMatrix)
		{
			p1 = localTrans.PointTransform(p1);
			p2 = localTrans.PointTransform(p2);
			p3 = localTrans.PointTransform(p3);
		}
		newPrim1->points.push_back(p1);
		newPrim2->points.push_back(p2);
		newPrim3->points.push_back(p3);
	}
	PushExtraPointsForMaxSDK(newPrim1);
	PushExtraPointsForMaxSDK(newPrim2);
	PushExtraPointsForMaxSDK(newPrim3);
	m_polylines.push_back(newPrim1);
	m_polylines.push_back(newPrim2);
	m_polylines.push_back(newPrim3);
}
void RsGWPrim::AddCone(float length, float radius, BOOL cap, Matrix3 localTrans)
{
	bool applyMatrix = !localTrans.IsIdentity();
	sGwPolyLine *newPrim1 = new sGwPolyLine();

	float degRadius = DegToRad(radius);
	float akSpot = (cos (degRadius/2)) * length;
	float gkSpot = (sin (degRadius/2)) * length;
	float step = DegToRad(360.0f/m_resolution);
	int arcResolution = m_resolution/3;
	float arcStep = (degRadius/2)/arcResolution;
	for(int k=0;k<m_resolution+1;k++)
	{
		float currStep = (step*k);
		float ak = (cos(currStep)) * gkSpot;
		float gk = (sin(currStep)) * gkSpot;
		Point3 currPoint(ak,gk,0.0f);
		Point3 outerDir = currPoint.Normalize();

		if(applyMatrix)
		{
			currPoint = localTrans.PointTransform(currPoint);
		}

		newPrim1->points.push_back(Point3(currPoint.x,currPoint.y,-akSpot));
		if(cap)
		{
			sGwPolyLine *arcPrim = new sGwPolyLine();
			// 			local vertsArc = #()
			for(int i=0;i<arcResolution+1;i++)
			{
				float akArc = (cos (arcStep*i)) * length;
				float gkArc = (sin (arcStep*i)) * length;
				Point3 currArcPoint = outerDir * gkArc;
				currArcPoint[2] = -akArc;
				if(applyMatrix)
				{
					currArcPoint = localTrans.PointTransform(currArcPoint);
				}
				arcPrim->points.push_back(currArcPoint);
			}
			arcPrim->closed = false;
			PushExtraPointsForMaxSDK(arcPrim);
			m_polylines.push_back(arcPrim);
		}
	}
	Point3 topVert(0,0,0);
	if(applyMatrix)
	{
		topVert = localTrans.PointTransform(topVert);
	}
	for(vector<Point3>::const_iterator it=newPrim1->points.begin();
		it!=newPrim1->points.end();
		it++)
	{
		sGwPolyLine *sideLine = new sGwPolyLine();
		sideLine->points.push_back(topVert);
		sideLine->points.push_back(*it);
		PushExtraPointsForMaxSDK(sideLine);
		m_polylines.push_back(sideLine);
	}
	PushExtraPointsForMaxSDK(newPrim1);
	m_polylines.push_back(newPrim1);
}

void RsGWPrim::AddCylinder(float length, float radius, Matrix3 localTrans)
{
	bool applyMatrix = !localTrans.IsIdentity();

	sGwPolyLine *newPrim1 = new sGwPolyLine();
	sGwPolyLine *newPrim2 = new sGwPolyLine();

	float step = DegToRad(360.0f/m_resolution);
	for(int k=0;k<m_resolution+1;k++)
	{
		float currStep = (step*k);
		float ak = (cos(currStep)) * radius;
		float gk = (sin(currStep)) * radius;
		Point3 currPoint1(ak,gk,0.0f);
		Point3 currPoint2(ak,gk,-length);
		if(applyMatrix)
		{
			currPoint1 = localTrans.PointTransform(currPoint1);
			currPoint2 = localTrans.PointTransform(currPoint2);
		}
		newPrim1->points.push_back(currPoint1);
		newPrim2->points.push_back(currPoint2);
	}
	for(vector<Point3>::iterator it1=newPrim1->points.begin(), it2=newPrim2->points.begin();
		it1!=newPrim1->points.end() && it2!=newPrim2->points.end();
		it1++, it2++)
	{
		sGwPolyLine *sideLine = new sGwPolyLine();
		sideLine->points.push_back(*it2);
		sideLine->points.push_back(*it1);
		PushExtraPointsForMaxSDK(sideLine);
		m_polylines.push_back(sideLine);
	}
	PushExtraPointsForMaxSDK(newPrim1);
	PushExtraPointsForMaxSDK(newPrim2);
	m_polylines.push_back(newPrim1);
	m_polylines.push_back(newPrim2);
}

void RsGWPrim::AddCapsule(float length, float radius, BOOL cap, Matrix3 localTrans)
{
	sGwPolyLine *newPrim1 = new sGwPolyLine();
	sGwPolyLine *newPrim2 = new sGwPolyLine();

	bool applyMatrix = !localTrans.IsIdentity();
	int arcResolution = m_resolution/3;

	float step = DegToRad(360.0f/m_resolution);
	float arcStep = DegToRad(90.0f/arcResolution);
	for(int k=0;k<m_resolution+1;k++)
	{
		float currStep = (step*k);
		float ak = (cos(currStep)) * radius;
		float gk = (sin(currStep)) * radius;
		Point3 currPoint1(ak,gk,0.0f);
		Point3 currPoint2(ak,gk,-length);
		newPrim1->points.push_back(currPoint1);
		newPrim2->points.push_back(currPoint2);
	}
	for(vector<Point3>::iterator it1=newPrim1->points.begin(), it2=newPrim2->points.begin();
		it1!=newPrim1->points.end() && it2!=newPrim2->points.end();
		it1++, it2++)
	{
		Point3 outerDir = (*it1).Normalize();
		if(applyMatrix)
		{
			(*it1) = localTrans.PointTransform((*it1));
			(*it2) = localTrans.PointTransform((*it2));
		}

		sGwPolyLine *sideLine = new sGwPolyLine();
		sideLine->points.push_back(*it2);
		sideLine->points.push_back(*it1);
		PushExtraPointsForMaxSDK(sideLine);
		m_polylines.push_back(sideLine);

		if(cap)
		{
			sGwPolyLine *arcPrim = new sGwPolyLine();
			for(int i=0;i<arcResolution+1;i++)
			{
				float akArc = (cos (arcStep*i)) * radius;
				float gkArc = (sin (arcStep*i)) * radius;
				Point3 currArcPoint = outerDir * gkArc;
				currArcPoint[2] = akArc;
				if(applyMatrix)
					currArcPoint = localTrans.PointTransform(currArcPoint);
				arcPrim->points.push_back(currArcPoint);
			}
			arcPrim->closed = false;
			PushExtraPointsForMaxSDK(arcPrim);
			m_polylines.push_back(arcPrim);

			sGwPolyLine *arcPrim2 = new sGwPolyLine();
			for(int i=0;i<arcResolution+1;i++)
			{
				float akArc = (cos (arcStep*i)) * radius;
				float gkArc = (sin (arcStep*i)) * radius;
				Point3 currArcPoint = outerDir * gkArc;
				currArcPoint[2] = -akArc;
				currArcPoint[2] -= length;
				if(applyMatrix)
					currArcPoint = localTrans.PointTransform(currArcPoint);
				arcPrim2->points.push_back(currArcPoint);
			}
			arcPrim2->closed = false;
			PushExtraPointsForMaxSDK(arcPrim2);
			m_polylines.push_back(arcPrim2);
		}
	}
	PushExtraPointsForMaxSDK(newPrim1);
	PushExtraPointsForMaxSDK(newPrim2);
	m_polylines.push_back(newPrim1);
	m_polylines.push_back(newPrim2);
}

void RsGWPrim::AddLine(Tab<Point3*> points, Matrix3 localTrans)
{
	bool applyMatrix = !localTrans.IsIdentity();

	sGwPolyLine *newPrim1 = new sGwPolyLine();
	for(int k=0;k<points.Count();k++)
	{
		Point3 currPoint1(*points[k]);
		if(applyMatrix)
		{
			currPoint1 = localTrans.PointTransform(currPoint1);
		}
		newPrim1->points.push_back(*points[k]);
	}	
	PushExtraPointsForMaxSDK(newPrim1);
	m_polylines.push_back(newPrim1);
}

void RsGWPrim::Draw(BOOL flush)
{
	SetInterfacePointer();

	ViewExp *vpt = ip->GetActiveViewport();
	GraphicsWindow *gw = vpt->getGW();
	gw->setTransform(m_origin);
	gw->setColor(LINE_COLOR, m_globalColour);

	for(PolyLineVec::const_iterator it = m_polylines.begin();
		it!=m_polylines.end();
		it++)
	{
		sGwPolyLine *line = *it;
		if(line && line->points.size()>NUM_EXTRA_GW_POINTS)
		{
			int lineNumPoints = (int)line->points.size();
			int lineNumColours = (int)line->colours.size();
			int lineNumEdgeVis = (int)line->edgeVis.size();
#if ENABLE_MULTILINE_RENDERING
			const int SplitLineAt = 31;

			int numIterations = ceil((float)(lineNumPoints - NUM_EXTRA_GW_POINTS)/SplitLineAt);
			for(int k=0;k<numIterations;k++)
			{
				int offset = k*SplitLineAt;
				int numRemaining = lineNumPoints - offset;
				int currNumToRender = numRemaining > SplitLineAt ? SplitLineAt : numRemaining;
				if(k==numIterations-1)
				{
					int numToSubtract = NUM_EXTRA_GW_POINTS;
					int notRendered = numRemaining - SplitLineAt;
					if(notRendered>0)
						numToSubtract -= notRendered;
					currNumToRender -= numToSubtract;
				}
				if(k>0)
				{
					offset -= 1;
					currNumToRender += 1;
				}

// 				Point3 *rgb;
// 				if(lineNumColours-offset<currNumToRender)
// 					rgb = colourCache;
// 				else
// 					rgb = &line->colours[offset];
// 				int *es;
// 				if(lineNumEdgeVis-offset<currNumToRender)
// 					es = edgeVisCache;
// 				else
// 					es = &line->edgeVis[offset];

				gw->polyline(currNumToRender, &line->points[offset], NULL, line->closed, NULL);
			}
#else
// 			Point3 *rgb;
// 			if(lineNumColours<lineNumPoints)
// 				rgb = colourCache;
// 			else
// 				rgb = &line->colours[0];
// 			int *es;
// 			if(lineNumEdgeVis<lineNumPoints)
// 				es = edgeVisCache;
// 			else
// 				es = &line->edgeVis[0];

			gw->polyline(lineNumPoints-NUM_EXTRA_GW_POINTS, &line->points[0], NULL, line->closed, NULL);

#endif
		}
	}

	if (flush)
		m_polylines.clear();

	ip->ReleaseViewport(vpt);
}