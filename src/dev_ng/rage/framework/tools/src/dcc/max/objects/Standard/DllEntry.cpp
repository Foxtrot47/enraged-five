#include "Standard.h"

HINSTANCE hInstance;
int controlsInit = FALSE;

#define MILO_CLASS_ID	Class_ID(0x5591738d, 0x54e3342e)

/////////////////////////////////////////////////////////////////////////////////////////////////
class MILOClassDesc : public ClassDesc2
/////////////////////////////////////////////////////////////////////////////////////////////////
{
	public:
	int 			IsPublic() { return TRUE; }
	void *			Create(BOOL loading = FALSE) { return new Standard(MILO_CLASS_ID,"Gta MILO"); }
	const TCHAR *	ClassName() { return "Gta MILO"; }
	SClass_ID		SuperClassID() { return HELPER_CLASS_ID; }
	Class_ID		ClassID() { return MILO_CLASS_ID; }
	const TCHAR* 	Category() { return GetString(IDS_CATEGORY); }

	const TCHAR*	InternalName() { return _T("Gta MILO"); }
	HINSTANCE		HInstance() { return hInstance; }
};

/////////////////////////////////////////////////////////////////////////////////////////////////
static MILOClassDesc MILODesc;

// MWW - TOOLS CLEANUP 09/2009
/*
#define LEVELPIVOT_CLASS_ID	Class_ID(0x3be80036, 0x201d6e2a)

/////////////////////////////////////////////////////////////////////////////////////////////////
class LevelPivotClassDesc : public ClassDesc2
/////////////////////////////////////////////////////////////////////////////////////////////////
{
	public:
	int 			IsPublic() { return TRUE; }
	void *			Create(BOOL loading = FALSE) { return new Standard(LEVELPIVOT_CLASS_ID,"Gta LevelPivot"); }
	const TCHAR *	ClassName() { return "Gta LevelPivot"; }
	SClass_ID		SuperClassID() { return HELPER_CLASS_ID; }
	Class_ID		ClassID() { return LEVELPIVOT_CLASS_ID; }
	const TCHAR* 	Category() { return GetString(IDS_CATEGORY); }

	const TCHAR*	InternalName() { return _T("Gta LevelPivot"); }
	HINSTANCE		HInstance() { return hInstance; }
};

/////////////////////////////////////////////////////////////////////////////////////////////////
static LevelPivotClassDesc LevelPivotDesc;
*/

#define CARGENERATOR_CLASS_ID Class_ID(0x308e4739, 0x4baf1b16)

/////////////////////////////////////////////////////////////////////////////////////////////////
class CarGeneratorClassDesc : public ClassDesc2
/////////////////////////////////////////////////////////////////////////////////////////////////
{
	public:
	int 			IsPublic() { return TRUE; }
	void *			Create(BOOL loading = FALSE) { return new Standard(CARGENERATOR_CLASS_ID,"Gta CarGenerator"); }
	const TCHAR *	ClassName() { return "Gta CarGenerator"; }
	SClass_ID		SuperClassID() { return HELPER_CLASS_ID; }
	Class_ID		ClassID() { return CARGENERATOR_CLASS_ID; }
	const TCHAR* 	Category() { return GetString(IDS_CATEGORY); }

	const TCHAR*	InternalName() { return _T("Gta CarGenerator"); }
	HINSTANCE		HInstance() { return hInstance; }
};

static CarGeneratorClassDesc CarGeneratorDesc;

#define PICKUP_CLASS_ID Class_ID(0x608766b2, 0x20a03dbb)

/////////////////////////////////////////////////////////////////////////////////////////////////
class PickupClassDesc : public ClassDesc2
/////////////////////////////////////////////////////////////////////////////////////////////////
{
	public:
	int 			IsPublic() { return TRUE; }
	void *			Create(BOOL loading = FALSE) { return new Standard(PICKUP_CLASS_ID,"Gta Pickup"); }
	const TCHAR *	ClassName() { return "Gta Pickup"; }
	SClass_ID		SuperClassID() { return HELPER_CLASS_ID; }
	Class_ID		ClassID() { return PICKUP_CLASS_ID; }
	const TCHAR* 	Category() { return GetString(IDS_CATEGORY); }

	const TCHAR*	InternalName() { return _T("Gta Pickup"); }
	HINSTANCE		HInstance() { return hInstance; }
};

static PickupClassDesc PickupDesc;

// MWW - TOOLS CLEANUP 09/2009
/*
#define COVER_CLASS_ID Class_ID(0x36393464, 0x5b8f6fb9)

/////////////////////////////////////////////////////////////////////////////////////////////////
class CoverClassDesc : public ClassDesc2
/////////////////////////////////////////////////////////////////////////////////////////////////
{
	public:
	int 			IsPublic() { return TRUE; }
	void *			Create(BOOL loading = FALSE) { return new Standard(COVER_CLASS_ID,"Gta Cover"); }
	const TCHAR *	ClassName() { return "Gta Cover"; }
	SClass_ID		SuperClassID() { return HELPER_CLASS_ID; }
	Class_ID		ClassID() { return COVER_CLASS_ID; }
	const TCHAR* 	Category() { return GetString(IDS_CATEGORY); }

	const TCHAR*	InternalName() { return _T("Gta Cover"); }
	HINSTANCE		HInstance() { return hInstance; }
};

/////////////////////////////////////////////////////////////////////////////////////////////////
static CoverClassDesc CoverDesc;
*/

#define SCRIPT_CLASS_ID Class_ID(0x57995e2c, 0x44ea0e3d)

/////////////////////////////////////////////////////////////////////////////////////////////////
class ScriptClassDesc : public ClassDesc2
	/////////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	int 			IsPublic() { return TRUE; }
	void *			Create(BOOL loading = FALSE) { return new Standard(SCRIPT_CLASS_ID,"Gta Script"); }
	const TCHAR *	ClassName() { return "Gta Script"; }
	SClass_ID		SuperClassID() { return HELPER_CLASS_ID; }
	Class_ID		ClassID() { return SCRIPT_CLASS_ID; }
	const TCHAR* 	Category() { return GetString(IDS_CATEGORY); }

	const TCHAR*	InternalName() { return _T("Gta Script"); }
	HINSTANCE		HInstance() { return hInstance; }
};

/////////////////////////////////////////////////////////////////////////////////////////////////
static ScriptClassDesc ScriptDesc;

/////////////////////////////////////////////////////////////////////////////////////////////////
BOOL WINAPI DllMain(HINSTANCE hinstDLL,ULONG fdwReason,LPVOID lpvReserved)
{
	hInstance = hinstDLL;

	if(!controlsInit) 
	{
		controlsInit = TRUE;
#if MAX_VERSION_MAJOR < 11
		// MAXontrols
		InitCustomControls(hInstance);
#endif // MAX_VERSION_MAJOR < 11
		InitCommonControls();
	}
			
	return (TRUE);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
__declspec( dllexport ) const TCHAR* LibDescription()
{
	return GetString(IDS_LIBDESCRIPTION);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
__declspec( dllexport ) int LibNumberClasses()
{
	return 5;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
__declspec( dllexport ) ClassDesc* LibClassDesc(int i)
{
	switch(i) 
	{
		//case 0: return &LevelPivotDesc;		// MWW - TOOLS CLEANUP 09/2009
		case 1: return &PickupDesc;
		//case 2: return &CoverDesc;			// MWW - TOOLS CLEANUP 09/2009
		case 3: return &MILODesc;
		case 4: return &ScriptDesc;
		default: return 0;
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////
__declspec( dllexport ) ULONG LibVersion()
{
	return VERSION_3DSMAX;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
TCHAR *GetString(int id)
{
	static TCHAR buf[256];

	if(hInstance)
	{
		return LoadString(hInstance, id, buf, sizeof(buf)) ? buf : NULL;
	}

	return NULL;
}

