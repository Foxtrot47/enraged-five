//
// filename:	MaxScript.cpp
// description:	SceneXml MaxScript Functions
//

// --- Include Files ------------------------------------------------------------

// Max headers
#include "max.h"
#include "IPathConfigMgr.h"
#if MAX_RELEASE > MAX_RELEASE_R13
#include <maxscript\MaxScript.h>
#include <maxscript\maxwrapper\maxclasses.h>
#include <maxscript\foundation\Numbers.h>
#include <maxscript\foundation\Strings.h>
#include <maxscript\util\listener.h>
#include <maxscript\foundation\3dmath.h>
#include <maxscript\macros\define_instantiation_functions.h>
#else
#include "maxscrpt\MaxScrpt.h"
#include "maxscrpt\MAXclses.h"
#include "maxscrpt\Numbers.h"
#include "maxscrpt\3dmath.h"
#include "maxscrpt\Strings.h"
#include "maxscrpt\definsfn.h"
#include "maxscrpt\Listener.h"
#endif
// SceneXml headers
#include "cSceneXmlConfig.h"
#include "cSceneXmlExporter.h"

// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

// --- Globals ------------------------------------------------------------------

// --- Static Globals -----------------------------------------------------------

// --- Static class members -----------------------------------------------------

// --- Code ---------------------------------------------------------------------

// ------------------------------------------------------------------------------
// 	MaxScript Function Declarations
// ------------------------------------------------------------------------------
def_visible_primitive( scenexml_export,		"SceneXmlExport" );
def_visible_primitive( scenexml_export_from, "SceneXmlExportFrom" );
def_visible_primitive( scenexml_reload_cfg,	"SceneXmlReloadCfg" );

// ------------------------------------------------------------------------------
// 	MaxScript Function Definitions
// ------------------------------------------------------------------------------

Value* 
scenexml_export_cf( Value** arg_list, int arg_count ) 
{
	Value* pFilename = arg_list[0];
	type_check( pFilename, String, "Export filename." );

	MCHAR* pExportFilename = pFilename->to_string();
	std::string filename( pExportFilename );

	Interface* pInterface = GetCOREInterface( );
	cSceneXmlExporter exporter;

	if(arg_count>1)
	{
		type_check( arg_list[1], String, "ULog filename." );
		exporter.SetULogFilename(arg_list[1]->to_string());
	}

	std::vector<INode*> vNodes;
	for ( int n = 0; n < pInterface->GetRootNode()->NumberOfChildren(); ++n )
	{
		INode* pChildNode = pInterface->GetRootNode()->GetChildNode( n );
		vNodes.push_back( pChildNode );
	}

	int nResult = exporter.DoExport( filename, pInterface, vNodes );
	if ( IMPEXP_FAIL == nResult )
		return ( &false_value );

	return ( &true_value );
}

Value*
scenexml_export_from_cf( Value** arg_list, int arg_count )
{
	type_check( arg_list[0], Array, "Nodes" );
	type_check( arg_list[1], String, "Export filename" );

	if(arg_count > 3)
	{
		mprintf("scenexml_export_from <nodes> <export filename> [<universal log file name>]");
		return &false_value;
	}

	Array* pNodeArray = static_cast<Array*>( arg_list[0] );
	if ( pNodeArray->size < 1 )
		return ( &false_value );

	std::vector<INode*> vNodes;
	for ( int n = 1; n <= pNodeArray->size; ++n )
	{
		type_check( pNodeArray->get(n), MAXNode, "Node" );
		vNodes.push_back( pNodeArray->get(n)->to_node() );
	}
	
	MCHAR* pFilename = arg_list[1]->to_string( );
	std::string filename( pFilename );

	Interface* pInterface = GetCOREInterface();
	cSceneXmlExporter exporter;
	if(arg_count>2)
	{
		type_check( arg_list[2], String, "ULog filename." );
		exporter.SetULogFilename(arg_list[2]->to_string());
	}
	int result = exporter.DoExport( filename, pInterface, vNodes );
	if ( IMPEXP_FAIL == result )
		return ( &false_value );

	return ( &true_value );
}

Value* 
scenexml_reload_cfg_cf( Value** arg_list, int arg_count )
{
	if ( 0 == arg_count )
	{
		// Reload default configuration file
		IPathConfigMgr* pConfigMgr = IPathConfigMgr::GetPathConfigMgr();
		const TCHAR* pPlugCfgDir = pConfigMgr->GetDir( APP_PLUGCFG_DIR );

		MaxSDK::Util::Path configPath( pPlugCfgDir );
		configPath.Append( CONFIG_FILE_DEFAULT );

		g_SceneXmlConfig.Load( configPath.GetString() );

		return ( &true_value );
	}
	else if ( 1 == arg_count )
	{
		Value* pConfigFilenameValue = arg_list[0];
		type_check( pConfigFilenameValue, String, "Config filename must be string filename." );

		MCHAR* pConfigFilename = pConfigFilenameValue->to_string();
		MaxSDK::Util::Path configPath( pConfigFilename );

		g_SceneXmlConfig.Load( configPath.GetString() );

		return ( &true_value );
	}
	else
	{
		return ( &false_value );
	}
}
