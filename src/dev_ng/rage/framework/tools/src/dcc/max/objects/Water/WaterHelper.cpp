#include "WaterHelper.h"

#include "maxscript/maxscript.h"

#define NUM_ALPHAS 4
#define ALPHA_DEFAULT 0.1f
#define ALPHA_DEFAULT_INT 26

/////////////////////////////////////////////////////////////////////////////////////////////////
class WaterClassDesc : public ClassDesc2 
/////////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	int 			IsPublic() { return TRUE; }
	void *			Create(BOOL loading = FALSE) { return new WaterHelper(); }
	const TCHAR *	ClassName() { return GetString(IDS_WATER_NAME); }
	SClass_ID		SuperClassID() { return HELPER_CLASS_ID; }
	Class_ID		ClassID() { return WATER_CLASS_ID; }
	const TCHAR* 	Category() { return GetString(IDS_CATEGORY); }

	const TCHAR*	InternalName() { return _T("Gta_Water"); }
	HINSTANCE		HInstance() { return hInstance; }
};

/////////////////////////////////////////////////////////////////////////////////////////////////
static WaterClassDesc WaterDesc;

/////////////////////////////////////////////////////////////////////////////////////////////////

static ParamBlockDesc2* waterBaseBlock = CreateWaterBasePBlock2(&WaterDesc, "Water Parameters");

ParamBlockDesc2 waterPBlockDesc (
								 water_prim_params, 
								 _T("Extended water Parameters"), 
								 IDS_WATER_NAME, 
								 &WaterDesc, 
								 P_AUTO_CONSTRUCT | P_AUTO_UI | P_VERSION, 
								 WATER_HELPER_CURRENT_VERSION,
								 WATER_EXTEND_PBLOCK_REF,
								 IDD_PANEL, IDS_EXTENDED_PARAMS, 0, 0, NULL,

								 water_type, _T("Type"), TYPE_INT, P_ANIMATABLE, IDS_BUTTSHAPE,
									 p_default,	1,
									 p_range,	0,	4,
								 end,
								 water_alpha0,_T("alphaFloat0"),	TYPE_FLOAT, P_ANIMATABLE, IDS_POINTS,
//									 p_ui,		TYPE_SPINNER,	EDITTYPE_FLOAT, IDC_ALPHAEDIT1, IDC_ALPHASPINNER1, ALPHA_DEFAULT,
									 p_range,	0.0f,	1.0f,
									 p_default,	ALPHA_DEFAULT,
								 end,
								 water_alpha1,_T("alphaFloat1"),	TYPE_FLOAT, P_ANIMATABLE, IDS_POINTS,
//									 p_ui,		TYPE_SPINNER,	EDITTYPE_FLOAT, IDC_ALPHAEDIT2, IDC_ALPHASPINNER2, ALPHA_DEFAULT,
									 p_range,	0.0f,	1.0f,
									 p_default,	ALPHA_DEFAULT,
								 end,
								 water_alpha2,_T("alphaFloat2"),	TYPE_FLOAT, P_ANIMATABLE, IDS_POINTS,
//									 p_ui,		TYPE_SPINNER,	EDITTYPE_FLOAT, IDC_ALPHAEDIT3, IDC_ALPHASPINNER3, ALPHA_DEFAULT,
									 p_range,	0.0f,	1.0f,
									 p_default,	ALPHA_DEFAULT,
								 end,
								 water_alpha3,_T("alphaFloat3"),	TYPE_FLOAT, P_ANIMATABLE, IDS_POINTS,
//									 p_ui,		TYPE_SPINNER,	EDITTYPE_FLOAT, IDC_ALPHAEDIT4, IDC_ALPHASPINNER4, ALPHA_DEFAULT,
									 p_range,	0.0f,	1.0f,
									 p_default,	ALPHA_DEFAULT,
								 end,
								 water_alpha_int0,_T("alpha0"),	TYPE_INT, P_ANIMATABLE, IDS_POINTS,
									 p_ui,		TYPE_SPINNER,	EDITTYPE_INT, IDC_ALPHAEDIT1, IDC_ALPHASPINNER1, ALPHA_DEFAULT_INT,
									 p_range,	0, 255,
									 p_default,	ALPHA_DEFAULT_INT,
								 end,
								 water_alpha_int1,_T("alpha1"),	TYPE_INT, P_ANIMATABLE, IDS_POINTS,
									 p_ui,		TYPE_SPINNER,	EDITTYPE_INT, IDC_ALPHAEDIT2, IDC_ALPHASPINNER2, ALPHA_DEFAULT_INT,
									 p_range,	0, 255,
									 p_default,	ALPHA_DEFAULT_INT,
									 end,
								 water_alpha_int2,_T("alpha2"),	TYPE_INT, P_ANIMATABLE, IDS_POINTS,
									 p_ui,		TYPE_SPINNER,	EDITTYPE_INT, IDC_ALPHAEDIT3, IDC_ALPHASPINNER3, ALPHA_DEFAULT_INT,
									 p_range,	0, 255,
									 p_default,	ALPHA_DEFAULT_INT,
								 end,
								 water_alpha_int3,_T("alpha3"),	TYPE_INT, P_ANIMATABLE, IDS_POINTS,
									 p_ui,		TYPE_SPINNER,	EDITTYPE_INT, IDC_ALPHAEDIT4, IDC_ALPHASPINNER4, ALPHA_DEFAULT_INT,
									 p_range,	0, 255,
									 p_default,	ALPHA_DEFAULT_INT,
								 end,
								 end
								 );

/////////////////////////////////////////////////////////////////////////////////////////////////
ClassDesc2* GetWaterDesc()
{
	return &WaterDesc;
}

void WaterHelper::LoadAlphaFromFloats()
{
	for(int index=0;index<NUM_ALPHAS;index++)
	{
		int ai = Water::pExtendPblock2->GetInt(water_alpha_int0+index, GetCOREInterface()->GetTime());
		float af = Water::pExtendPblock2->GetFloat(water_alpha0+index, GetCOREInterface()->GetTime());
		if(ai == ALPHA_DEFAULT_INT && af != ALPHA_DEFAULT)
		{
			MSTR nodeName = "";
			if(IsWorldSpaceObject())
			{
				INodePtr pNode = this->GetWorldSpaceObjectNode();
				if(pNode)
					nodeName = MSTR(" ") + pNode->GetName();
			}
			mprintf("Found old alpha value %5.2f in Waterhelper%s. Putting into integer field as %d.\n", af, nodeName.data(), (int)(af*255));
			Water::pExtendPblock2->SetValue(water_alpha_int0+index, GetCOREInterface()->GetTime(), af*255);
		}
		Water::pExtendPblock2->SetValue(water_alpha0+index, GetCOREInterface()->GetTime(), ALPHA_DEFAULT);
	}
}

void WaterHelper::PostLoadHook(ILoad *iload)
{
	if(pExtendPblock2->GetVersion()<WATER_HELPER_CURRENT_VERSION)
		mprintf("Loading Water helper with older version %d.\n", pExtendPblock2->GetVersion());
	LoadAlphaFromFloats();
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void WaterHelper::BeginEditParams( IObjParam *ip, ULONG flags,Animatable *prev)
{
	Water::BeginEditParams(ip,flags,prev);
	WaterDesc.BeginEditParams(ip,this,flags,prev);
	WaterDesc.SetUserDlgProc(&waterPBlockDesc, new WaterDlgProc(this));
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void WaterHelper::EndEditParams( IObjParam *ip, ULONG flags,Animatable *next)
{
	WaterDesc.EndEditParams(ip,this,flags,next);
	Water::EndEditParams(ip,flags,next);
}


WaterHelper::WaterHelper(void)
	: Water(&WaterDesc)
{
//	CreateWaterBasePBlock2(&WaterDesc, IDD_PANEL);
//	WaterDesc.AddParamBlockDesc(&waterPBlockDesc);
	WaterDesc.MakeAutoParamBlocks(this);
	mLineColour = Point3(0.0f,1.0f,1.0f);
}

int WaterHelper::GetType()
{
	return Water::pExtendPblock2->GetInt(water_type, GetCOREInterface()->GetTime());
}
float WaterHelper::GetAlphaFloat(int index)
{
	if(NUM_ALPHAS<index)
		return 1.0f;
	return  Water::pExtendPblock2->GetFloat(water_alpha0+index, GetCOREInterface()->GetTime());
}
void WaterHelper::SetAlphaFloat(int index, float alpha)
{
	if(NUM_ALPHAS<index)
		return;
	alpha = alpha > 1.0f ? 1.0f : (alpha < 0.0f ? 0.0f : alpha);
	Water::pExtendPblock2->SetValue(water_alpha0+index, GetCOREInterface()->GetTime(), alpha);
	int alphaInt = alpha*255;
	alphaInt = alphaInt > 255 ? 255 : (alphaInt < 0 ? 0 : alphaInt);
	Water::pExtendPblock2->SetValue(water_alpha_int0+index, GetCOREInterface()->GetTime(), alphaInt);
}

int WaterHelper::GetAlpha(int index)
{
	if(NUM_ALPHAS<index)
		return 255;
	return  Water::pExtendPblock2->GetInt(water_alpha_int0+index, GetCOREInterface()->GetTime());
}
void WaterHelper::SetAlpha(int index, int alpha)
{
	if(NUM_ALPHAS<index)
		return;
	alpha = alpha > 255 ? 255 : (alpha < 0 ? 0 : alpha);
	Water::pExtendPblock2->SetValue(water_alpha_int0+index, GetCOREInterface()->GetTime(), alpha);
	float alphaFloat = (float)alpha / 255.0f;
	alphaFloat = alphaFloat > 1.0f ? 1.0f : (alphaFloat < 0.0f ? 0.0f : alphaFloat);
	Water::pExtendPblock2->SetValue(water_alpha0+index, GetCOREInterface()->GetTime(), alphaFloat);
}

void WaterHelper::BuildSpecificMesh(TimeValue t, Mesh &mesh, Point3 centre, Point3 dim)
{

}
/////////////////////////////////////////////////////////////////////////////////////////////////
RefTargetHandle WaterHelper::Clone(RemapDir& remap) 
{
	WaterHelper* newob = new WaterHelper();
	newob->ReplaceReference(0,pBasePblock2->Clone(remap));
	newob->ReplaceReference(1,pExtendPblock2->Clone(remap));

	BaseClone(this, newob, remap);

	return(newob);
}

//Function Publishing descriptor for Mixin interface
//*****************************************************

static FPInterfaceDesc water_interface(
    WATER_INTERFACE, _T("IWater"), 0, &WaterDesc, 0,
		IWater::get_mesh,		_T("GetMesh"), 0, TYPE_MESH_BR, 0, 0,
		IWater::get_alpha,		_T("GetAlphaFloat"), 0, TYPE_FLOAT, 0, 1,
			_T("index"), 0, TYPE_INT,
		IWater::set_alpha,		_T("SetAlphaFloat"), 0, TYPE_VOID, 0, 2,
			_T("index"), 0, TYPE_INT,
			_T("index"), 0, TYPE_FLOAT,
		IWater::get_alpha_int,		_T("GetAlpha"), 0, TYPE_INT, 0, 1,
			_T("index"), 0, TYPE_INT,
		IWater::set_alpha_int,		_T("SetAlpha"), 0, TYPE_VOID, 0, 2,
			_T("index"), 0, TYPE_INT,
			_T("index"), 0, TYPE_INT,
		end
		);

//  Get Descriptor method for Mixin Interface
//  *****************************************
FPInterfaceDesc* IWater::GetDesc()
{
	return &water_interface;
}

//*********************************************
// End of Function Publishing Code
