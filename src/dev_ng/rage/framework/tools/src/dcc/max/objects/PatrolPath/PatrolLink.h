//
//
//    Filename: PatrolLink.h
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Sub object class describing a link between two PatrolConnections
//
//
#ifndef PATROL_LINK_H_INCLUDED
#define PATROL_LINK_H_INCLUDED

#include "PatrolNode.h"

#define PATROL_L_DISPLAY_EXTENTS	0x10000

class PatrolLink : public HelperObject
{
private:
	IParamBlock2 *m_pBlock2;
	Control *m_pControl[2];
	PatrolLink* m_pTheOtherDir;

	static Interface*		m_ip;			//Access to the interface
	static HWND				m_hObjWnd;
	static ICustButton*		m_pFlipButton;
	static ICustButton*		m_pMergeButton;
	static ICustButton*		m_pInsertButton;
public:
	PatrolLink();
	~PatrolLink();

	s32 GetNumConnections();
	bool GetEndPoints(TimeValue t, Point3 pt[2], float radii[2]);
	bool GetEndPointsNew(TimeValue t, Point3 pt[2]);
	void Delete(PatrolNode* pConnection);

	// Animatable methods
	int NumParamBlocks() {return 1;}
	IParamBlock2* GetParamBlock(int i) {assert(i == 0); return m_pBlock2;}
	IParamBlock2* GetParamBlockByID(short id) {assert(id == patrol_link_params); return m_pBlock2;}
	Class_ID ClassID();// {return PATROL_LINK_CLASS_ID;}
	void GetClassName(TSTR& s) {s = _T("PatrolLink");}
	void BeginEditParams( IObjParam  *ip, ULONG flags,Animatable *prev);
	void EndEditParams( IObjParam *ip, ULONG flags,Animatable *next);
	void DeleteThis();// {delete this;}

	// ReferenceTarget methods
#if MAX_VERSION_MAJOR >= 9 
	ReferenceTarget* Clone(RemapDir &remap = DefaultRemapDir());
#else
	ReferenceTarget* Clone(RemapDir &remap = NoRemap());
#endif

	IOResult Load(ILoad *iload);
	IOResult Save(ISave *isave);
	int NumRefs();
	RefTargetHandle GetReference(int i);
	void SetReference(int i, RefTargetHandle pTarg);
	RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,  RefMessage message);
	void RefDeleted();
		
	//From BaseObject
	CreateMouseCallBack* GetCreateMouseCallBack() {return NULL;}
	void GetLocalBoundBox(TimeValue t, INode* pNode, ViewExp* pVp, Box3& box);
	void GetWorldBoundBox(TimeValue t, INode* pNode, ViewExp* pVp, Box3& box);
	int Display(TimeValue t, INode* pNode, ViewExp *pVpt, int flags);
	int HitTest(TimeValue t, INode* pNode, int type, int crossing, int flags, IPoint2 *pSp, ViewExp *pVpt);

	ObjectState Eval(TimeValue t) {return ObjectState(this);}
	void InitNodeName(TSTR &s) { s = _T("plink"); }
	TCHAR *GetObjectName() { return _T("PatrolLink"); }

	Interface*	GetInterface() { return m_ip; }

	INode*	GetINode();
	void	SetPos(Interface *ip);
	void	GetVehicleNodes(PatrolNode* pNodes[2]);
	bool	ReplaceController(Control* pControlDest, Control* pControlSource);
	PatrolNode*	InsertNode(Interface* ip);
	PatrolNode*	InsertNodeAuto(Interface* ip, float fMinLength);
	PatrolNode*	AutoCreate(Interface* ip, float fMinLength);
	bool			IsLongEnoughForAutoCreate(Interface* ip, float fMinLength);
	float			GetLength(Interface* ip);

	// UI
	void InitObjUI();
	void CloseObjUI();
	void Flip();
	void Merge();

	PatrolNode* GetOtherNode(PatrolNode* pCaller);
	void CleanMeUp();

	bool IsDifferentFrom(PatrolLink* pLinkToCheck);

};


//
//   Class Name: PatrolLinkClassDesc
// Base Classes: ClassDesc
//  Description: 
//    Functions: 
//
//
class PatrolLinkClassDesc : public ClassDesc2
{

public:
	int IsPublic() {return /*1; } */0;}
	void* Create(BOOL loading = FALSE);// {return new PatrolLink;}
	const TCHAR *ClassName() {return _T("PatrolLink");}
	SClass_ID SuperClassID() {return HELPER_CLASS_ID;}
	Class_ID ClassID() {return PATROL_LINK_CLASS_ID;}
	const TCHAR* Category() {return _T("Standard");}
	void ResetClassParams (BOOL fileReset) {}

	// Hardwired name, used by MAX Script as unique identifier
	const TCHAR*	InternalName() { return _T("PatrolLink"); }
	HINSTANCE		HInstance()	{ return hInstance; }
};

#endif	// PATROL_LINK_H_INCLUDED
