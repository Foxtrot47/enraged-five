//
//
//    Filename: UVWData.h
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 3/06/99 12:32 $
//   $Revision: 5 $
// Description: LocalModData for UVWmachine modifier
//
//
#ifndef _UVW_DATA_H_
#define _UVW_DATA_H_

// STL header
#include <functional>
#include <vector>
// Max header
#include <Max.h>
// my header
#include <dma.h>
#include "MeshSelect.h"


//
//   Class Name: UVWData
// Base Classes: 
//  Description: 
//    Functions: 
//
//
class UVWData : public StdMeshSelectData
{
private:
	std::vector<TVFace> m_uvwFaces;
	std::vector<UVVert> m_uvw;
	std::vector<bool> m_vtxExtracted;
	std::vector<bool> m_faceExtracted;
	float *m_pValues;
	bool m_faceHeld, m_vertexHeld;

public:
	// constructors
	UVWData();
	UVWData(const UVWData &data);
	UVWData(Mesh &mesh);
	// clone
	virtual LocalModData *Clone(void);

	// return current cached mesh
	void SetCache(Mesh &mesh);

	// access functions
	void SetEdgeSel(BitArray &set) {}

	std::vector<UVVert> &GetUVW(void) {return m_uvw;}
	void SetUVW(std::vector<UVVert> &uvw);
	std::vector<TVFace> &GetUVWFaces(void) {return m_uvwFaces;}
	void SetUVWFaces(std::vector<TVFace> &uvwFaces);

	// undo/redo stuff
	void SetFaceHeld(bool held) {m_faceHeld = held;}
	bool IsFaceHeld() {return m_faceHeld;}
	void SetVertexHeld(bool held) {m_vertexHeld = held;}
	bool IsVertexHeld() {return m_vertexHeld;}

	// operations
	void DestroyExtractedArrays(void);
	bool ExtractVertex(int32 face, int32 index);
	void ExtractFace(int32 index);
	void ExtractFaces(BitArray &faces);
	void ConstructSurroundingVertexList(AdjFaceList &faceList, int32 face, BitArray &faceSet, std::vector<std::pair<int32, int32> > &list);
	void RotatePolygonTextureCoords(AdjFaceList &faceList, int32 face, BitArray &faceSet);

	void RotateTextureCoords(int32 sel);
	void Rotate90(int32 index);
	void FlipU(int32 index);
	void FlipV(int32 index);
	void Rotate(int32 index);
	void Scale(int32 index);
	void Translate(int32 index);
	void ScaleU(int32 index);
	void TranslateU(int32 index);
	void ScaleV(int32 index);
	void TranslateV(int32 index);
	void SetU(int32 index);
	void SetV(int32 index);

	void SetValuesPtr(float *pV) {m_pValues = pV;}

	void ProcessVertices(void (UVWData::*func)(int32 index));
	void ProcessFaces(void (UVWData::*func)(int32 index));
	void ExplicitSet(int32 sel, int32 index, int32 coord);
};


#endif	// _UVW_DATA_H_