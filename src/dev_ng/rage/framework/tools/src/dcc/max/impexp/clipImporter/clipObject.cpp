#include "clipObject.h"

// max headers
#include "notetrck.h"

// rage headers
#include "crclip/clip.h"
//#include "crmetadata/propertybasic.h"
#include "crskeleton/skeletondata.h"
#include "crskeleton/skeleton.h"

// Maxscript includes
#include <maxscript/maxscript.h>
#include <maxscript/foundation/strings.h>
#include <maxscript/compiler/parser.h>

#include <string>

ClipObject::ClipObject()
: mb_Inited(false)
{
	//rage::crAnimation::InitClass();
    //rage::crClip::InitClass();
}

void ClipObject::Reset()
{
	if(m_pClipAnim)
	{
		delete m_pClipAnim;
		m_pClipAnim = NULL;
	}
	//rage::crClip::ShutdownClass();
	//rage::crAnimation::ShutdownClass();
	mb_Inited = false;
}

ClipObject::~ClipObject()
{
	Reset();
}

void ClipObject::Init(char* p_Filename)
{
	rage::crAnimation::InitClass();
    rage::crClip::InitClass();

	char animFilename[512];

	sprintf(animFilename, "%s.anim", p_Filename);
	m_pAnim = crAnimation::AllocateAndLoad( animFilename );
	if( m_pAnim )
		m_pClipAnim = crClipAnimation::AllocateAndCreate( *m_pAnim );
	else
		m_pClipAnim = crClipAnimation::Allocate( );

	m_pProperties = m_pClipAnim->GetProperties();

	mb_Inited = true;
}




void ClipObject::LoadClip( char* p_Filename )
{
	rage::crAnimation::InitClass();
    rage::crClip::InitClass();

	if(m_pClipAnim) 
		delete m_pClipAnim;

	m_pClipAnim = static_cast<crClipAnimation*>(crClip::AllocateAndLoad( p_Filename ));
	
	if(!m_pClipAnim)
	{
		Init( p_Filename );
	}
	else
	{
		if(m_pAnim)
			m_pClipAnim->SetAnimation(*m_pAnim);

		m_pProperties = m_pClipAnim->GetProperties();
	}
	
	mb_Inited = true;
}

void ClipObject::SaveClip( char* p_Filename )
{
	rage::crAnimation::InitClass();
    rage::crClip::InitClass();

	if(m_pClipAnim == NULL)
		Init(p_Filename);
	
	m_pClipAnim->Save( p_Filename );
}

/*
void ClipObject::AddProperty(const char* propName, const int propValue)
{
	assert(m_pProperties);
	if(!m_pProperties)
		return;

	crPropertyAttributeInt pPropInt;
	pPropInt.SetName( propName );
	int& val = pPropInt.GetInt();
	val = propValue;

	crProperty pProperty;
	pProperty.SetName( propName );
	pProperty.AddAttribute( pPropInt );
	m_pProperties->AddProperty( pProperty );
}

void ClipObject::AddProperty(const char* propName, const char* propValue)
{
	assert(m_pProperties);
	if(!m_pProperties)
		return;

	crPropertyAttributeString pPropString;
	pPropString.SetName( propName );
	atString& val = pPropString.GetString();
	val = propValue;

	crProperty pProperty;
	pProperty.SetName( propName );
	pProperty.AddAttribute( pPropString );
	m_pProperties->AddProperty( pProperty );
}
*/

void ClipObject::GetProperty( const char* propName, int &propValue )
{
	propValue = 0;
	if(m_pProperties)
	{
		crPropertyAttributeInt* p_PropInt = ( crPropertyAttributeInt* )m_pProperties->FindProperty( propName );
		if(p_PropInt)
			propValue = p_PropInt->GetInt();
	}

}

void ClipObject::GetProperty( const char* propName, const char* &propValue )
{
	propValue = "";
	if(m_pProperties)
	{
		crPropertyAttributeString* p_PropString = ( crPropertyAttributeString* )m_pProperties->FindProperty( propName );
		if( p_PropString )
			propValue = p_PropString->GetString();
	}
}

void ClipObject::BuildPropertyFromArgs( const char* propName, Value** args, int count )
{
	// from 2 onwards
	crProperty pProperty;
	pProperty.SetName( propName );
	atString attrName;

	for( int i=1; i<count; i++ )
	{
		switch (args[i]->to_int())
		{
		case PROP_TYPE_FLOAT:
			{
			crPropertyAttributeFloat pAttrFloat;
			float& floatval = pAttrFloat.GetFloat();
			
			attrName = args[++i]->to_string();
			floatval = args[++i]->to_float();
			pAttrFloat.SetName( attrName );
			
			pProperty.AddAttribute( pAttrFloat );
			} break;
		case PROP_TYPE_INT:
			{
				crPropertyAttributeInt pAttrInt;
				int& val = pAttrInt.GetInt();

				attrName = args[++i]->to_string();
				val = args[++i]->to_int();
				pAttrInt.SetName( attrName );

				pProperty.AddAttribute( pAttrInt );
			} break;
		case PROP_TYPE_STRING:
			{
				crPropertyAttributeString pAttrString;
				atString& val = pAttrString.GetString();

				attrName = atString(args[++i]->to_string());
				val = args[++i]->to_string();
				pAttrString.SetName( attrName );

				pProperty.AddAttribute( pAttrString );
			} break;
		}
		
	}
	m_pProperties->AddProperty( pProperty );
}
