//
// filename:	cSceneXmlExporter.h
// description:	
//

#ifndef INC_CSCENEXMLEXPORTER_H_
#define INC_CSCENEXMLEXPORTER_H_

// MWW: Disable for VS 2008
#pragma warning(disable: 4275)

// --- Include Files ------------------------------------------------------------

// STL headers
#include <string>
#include <vector>

// Max headers
#include "max.h"


#include "Attribute.h"
#include "IFpMaxDataStore.h"

// SceneXml headers
#include "cSceneXmlConfig.h"
#include "cMaterial.h"
#include "IGame/IGame.h"
#include "IGame/IGameError.h"

#include "IFPULog.h"
#include "RsGuid/RsGuidMgr.h"

// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------

// --- Forward Declarations -----------------------------------------------------

class INode;
class TiXmlDocument;
class TiXmlElement;

// --- Structure/Class Definitions ----------------------------------------------

class ObjectStats
{
public:
	ObjectStats()
		: Name( "" )
		, PolyCount( 0 )
		, PolyDensity( 0.0f )
		, IsCollision( false )
	{
	}

	std::string Name;
	Box3 Bounds;
	int PolyCount;
	float PolyDensity;
	bool IsCollision;
};

/**
 * @brief 3dsmax Exporter object, triggered from MaxScript.
 */
class cSceneXmlExporter
{
public:
	cSceneXmlExporter();
	~cSceneXmlExporter();

	unsigned int Version( ) const;
	int DoExport( const std::string& filename, Interface* i, const std::vector<INode*>& vNodes );
	void SetULogFilename(const char* filename)
	{
		m_pULog = RsULogInst();
		if(m_pULog)
			m_pULog->InitPath(filename, "Scene XML");
		else
			mprintf("ERROR: Couldn't initialise Universal log for SceneXML and filename %s.\n", filename);
	}
	IFPULog *GetULog() const
	{
		return m_pULog;
	}

private:
	bool Validate(INodePtr pNode);
	void ExportStats(TiXmlElement* pElement);

	TiXmlElement* ExportRoot( TiXmlDocument* pDocument, Interface* pInterface );
	TiXmlElement* ExportNode( FILE* fpLog, INode* pNode, Interface* pInterface );
	TiXmlElement* ExportSubObjects( INode* pNode );
	void ExportProperties( INode* pNode, const tNodeDescription* pNodeDesc, 
		TiXmlElement* pElement, Interface* pInterface );
	void ExportAnimationProps( INode* pNode, const tNodeProperty* pNodeDesc, TiXmlElement* pElement );
	void ExportLightProperties( INode* pNode, TiXmlElement* pElement );
	void ExportUserProperty( INode* pNode, const tNodeDescription* pNodeDesc, 
		const tNodeProperty* pNodeProp, TiXmlElement* pElement );
	void ExportUserProperties( INode* pNode, TiXmlElement* pElement );
	void ExportNameProperty( INode* pNode, TiXmlElement* pElement );
	void ExportBoundingBoxProperty( INode* pNode, TiXmlElement* pElement );
	void ExportSkeletonRootObject( INode* pNode, TiXmlElement* pElement );
	void ExportPlatformAttributesProperty( INode* pNode, TiXmlElement* pElement);
	void ExportAttributesProperty( INode* pNode, TiXmlElement* pElement );
	void ExportParamBlocksProperty( INode* pNode, TiXmlElement* pElement );
	void ExportRefTexturesProperty( INode* pNode, TiXmlElement* pElement );

	void ExportParamBlock( IParamBlock2* pParamBlock, TiXmlElement* pElement );
	void ExportParamBlock( IParamBlock* pParamBlock, TiXmlElement* pElement );
	void ExportTransformProperty( INode* pNode, TiXmlElement* pElement );
	void ExportLocalTransformProperty( INode* pNode, TiXmlElement* pElement );
	void ExportLodHierarchy( INode* pNode, TiXmlElement* pElement );
	void ExportSceneLodHierarchy( INode* pNode, TiXmlElement* pElement );
	void ExportDrawableLodHierarchy( INode* pNode, TiXmlElement* pElement );
	void GetNodeWithGuids( INode* pNode, char *name, TiXmlElement* pParentElement );
	void ExportSceneLinkHierarchy( INode* pNode, TiXmlElement* pElement );
	void ExportReferences( INode* pNode, TiXmlElement* pElement );
	void ExportPolycount( INode* pNode, TiXmlElement* pElement );
	void ExportLedgeProperty( INode* pNode, TiXmlElement* pElement, const char* ledge_property );
	void ExportMaterialList( TiXmlElement* pElement );

	// Generic utility methods
	std::string MakeClassNameFriendly( const std::string& classname );
	void PopulateMaterialsList( INode* pNode, TiXmlElement* pElement, Interface* pInterface );
	void PrecacheSelectionSets( );
	
	// Attributes
	bool					HasAttributeClass( INode* pNode, const std::string& classname );
	dmat::AttributeInst*	GetAttributes( INode* pNode );
	void					SetAttributeIds( const dmat::AttributeClass* pClass );

	bool IsDontExport( INode *pNode );
	bool IsXref( INode* pNode );
	bool IsRSref( INode* pNode );
	bool IsRageLight( INode* pNode );
	bool IsInternalRef( INode* pNode );
	bool IsVehicleLink( INode* pNode );
	bool IsPatrolLink( INode* pNode );
	bool IsPatrolNode( INode* pNode );

	const TCHAR *GetNodeGuidString(INodePtr pNode);

	typedef std::map<ULONG, cGuid>		tObjectList;
	typedef tObjectList::iterator		tObjectListIterator;
	typedef tObjectList::const_iterator	tObjectListConstIterator;
	
	typedef std::map<ULONG, ObjectStats> tObjectStatsList;
	typedef tObjectStatsList::iterator		tObjectStatsListIterator;

	IGameScene		*m_iGameScene;
	tObjectList		m_mObjects;
	tMaterialList	m_mMaterials;		//!< Materials list

	tObjectStatsList m_mObjectStatsList;

	IFPULog *m_pULog;
};

// --- Globals ------------------------------------------------------------------


class MyProc : public PropertyEnum
{
//	DumpProperties * dp;

public:

//	MyProc(DumpProperties * d) {dp = d;}

	bool Proc(IGameProperty * prop)
	{
		TCHAR Buf[256];
		_tcscpy(Buf,prop->GetName());

		// if it is null, this is usually because its a IParamBlock and it is // not animated, thus No name
		if(_tcscmp(Buf, _T(""))!=0){
			DebugPrint("Property: %s\n", Buf);
		}
		return false;	//lets keep going
	}
};

class MyErrorProc : public IGameErrorCallBack
{
	cSceneXmlExporter *pSceneExporter;
public:
	MyErrorProc(cSceneXmlExporter *pse)
	{
		pSceneExporter = pse;
	}

	void ErrorProc(IGameError error)
	{
		std::string lastError = GetLastIGameErrorText();
		if(lastError.empty())
			return;
		char buf[4096]; 
		sprintf_s(buf, "Error occured related to IGame library:%s", lastError.c_str());
		if(pSceneExporter && pSceneExporter->GetULog())
			pSceneExporter->GetULog()->LogError(buf);
	}
};



#endif // !INC_CSCENEXMLEXPORTER_H_
