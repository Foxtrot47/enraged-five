//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Resources.rc
//
#define IDS_DESCRIPTION                 101
#define IDS_AUTHOR                      102
#define IDS_SCROLLBARS_CLASSNAME        103
#define IDS_CATEGORY                    104
#define IDB_BITMAP1                     104
#define IDS_SCROLLBARSPOINT_CLASSNAME   105
#define IDD_SCROLLBARS_PANEL            105
#define IDS_PARAMS                      106
#define IDD_POINTS_PANEL                106
#define IDS_VERSION                     107
#define IDS_SCROLLBARS_INTERNALNAME     108
#define IDS_HEIGHT                      109
#define IDS_TYPE                        110
#define IDS_POSITION                    111
#define IDS_TYPE_FINANCIAL              112
#define IDS_TYPE_THEATRE                113
#define IDS_TYPE_ADVERT                 114
#define IDS_TYPE_CLOCK                  115
#define IDS_NUMPOINTS                   116
#define IDS_TYPE_URLS                   117
#define IDS_TYPE_COMEDYCLUB             118
#define IDS_TYPE_TRAFFIC                119
#define IDS_TYPE_NEWS                   120
#define IDC_VERSION                     1002
#define IDC_SCROLLBARS_HEIGHT_EDIT      1003
#define IDC_SCROLLBARS_HEIGHT_SPIN      1004
#define IDC_SCROLLBARS_NUMPOINTS_EDIT   1005
#define IDC_SCROLLBARS_TYPE_COMBO       1006
#define IDC_SCROLLBARS_NUMPOINTS_SPIN   1007
#define IDC_EDT_NAME                    1044

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        121
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1009
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
