//
// filename:	cSceneXmlUtil.h
// description:	
//

#ifndef INC_CSCENEXMLUTIL_H_
#define INC_CSCENEXMLUTIL_H_

// --- Include Files ------------------------------------------------------------

// Max headers
#include "max.h"

// STL headers
#include <list>
#include <vector>

// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

/**
 * @brief Callback class for BaseObject->GetSubObjectTMs / GetSubObjectCentres.
 *
 * The class provides public access to its current centres and transformation
 * matrices collected since construction or clearing.
 */
class cSubObjectCallback : public SubObjAxisCallback
{
public:
	virtual void Center( Point3 c, int id );
	virtual void TM( Matrix3 tm, int id );
	virtual int Type( );

	typedef std::vector<Point3> Point3Collection;
	typedef Point3Collection::iterator Point3CollectionIter;
	typedef Point3Collection::const_iterator Point3CollectionConstIter;
	typedef std::vector<Matrix3> Matrix3Collection;
	typedef Matrix3Collection::iterator Matrix3CollectionIter;
	typedef Matrix3Collection::const_iterator Matrix3CollectionConstIter;

	void Clear( );

	const Point3Collection& Centers( ) const { return m_vCenters; }
	const Matrix3Collection& TMs( ) const { return m_matTMs; }

	Point3CollectionIter BeginCenters( ) { return m_vCenters.begin(); }
	Point3CollectionIter EndCenters( ) { return m_vCenters.end(); }
	Point3CollectionConstIter BeginCenters( ) const { return m_vCenters.begin(); }
	Point3CollectionConstIter EndCenters( ) const { return m_vCenters.end(); }

	Matrix3CollectionIter BeginTMs( ) { return m_matTMs.begin(); }
	Matrix3CollectionIter EndTMs( ) { return m_matTMs.end(); }
	Matrix3CollectionConstIter BeginTMs( ) const { return m_matTMs.begin(); }
	Matrix3CollectionConstIter EndTMs( ) const { return m_matTMs.end(); }

private:
	Point3Collection m_vCenters;
	Matrix3Collection m_matTMs;
};

// --- Globals ------------------------------------------------------------------

class LightObject;
class LightscapeLight;

LightObject* IsLight( Object* obj );
LightscapeLight* IsPhotometric( Object* obj );

#endif // !INC_CSCENEXMLUTIL_H_
