//
//
//    Filename: PickPatrolConnection.cpp
//     Creator: 
//     $Author: Adam $
//       $Date: 29/04/99 9:44 $
//   $Revision: 1 $
// Description: 
//
//

#include "PickPatrolConnection.h"
#include "PatrolNode.h"


//
//        name: PickPatrolConnection::Filter
// description: Return if Node is valid to be picked
//          in: pNode = pointer to node
//         out: valid? (TRUE/FALSE)
//
BOOL PickPatrolConnection::Filter(INode *pNode)
{
	if(pNode && pNode->GetObjectRef() != m_pPatrolNode)
	{
		Object* pObj = pNode -> EvalWorldState(0).obj;
		if (pObj && pObj->ClassID() == PATROL_NODE_CLASS_ID)
		{
#ifdef ONLY_LINEAR_PATHS
			if(m_oper == MERGE && 
				m_pPatrolNode->GetLinkBetween((PatrolNode *)pObj) != NULL)
				return TRUE;
			if(((m_pPatrolNode* )pObj)->GetNumberOfLinks() < 2 &&
				m_pPatrolNode->GetNumberOfLinks() < 2 &&
				m_pPatrolNode->GetLinkBetween((m_pPatrolNode* )pObj) == NULL)
				return TRUE;
#else
//			if(m_oper == MERGE || m_pPatrolNode->GetLinkBetween((VehiclePath *)pObj) == NULL)
				return TRUE;
#endif
		}
	}
	return FALSE;
}

//
//        name: PickPatrolConnection::HitTest
// description: Called to check what has been hit by the mouse cursor
//
BOOL PickPatrolConnection::HitTest(IObjParam *ip, HWND hWnd, ViewExp *vpt, IPoint2 m, int flags)
{
	INode *pNode = ip->PickNode(hWnd, m, this);
	
	if(pNode)
	{
		return TRUE;
	}
	return FALSE;
}


//
//        name: PickPatrolConnection::Pick
// description: Called when the mouse button is pressed
//
BOOL PickPatrolConnection::Pick(IObjParam *ip, ViewExp *vpt)
{
	INode *pNode = vpt->GetClosestHit();
	
	if (pNode)
	{
		switch(m_oper)
		{
		case CONNECT:
			m_pPatrolNode->ConnectTo(pNode);
			m_pPatrolNode->UpdateAllLinks(ip);
			break;
		case MERGE:
//			assert(false);
//			m_pPatrolNode->MergeWith(pNode);
			break;
		}
	}
	return FALSE;
}

//
//        name: PickPatrolConnection::EnterMode
// description: Called when mode is entered
//
void PickPatrolConnection::EnterMode(IObjParam *ip)
{
	m_pButton->SetCheck(TRUE);
}

//
//        name: PickVehConnection::ExitMode
// description: Called when mode is exited
//
void PickPatrolConnection::ExitMode(IObjParam *ip)
{
	m_pButton->SetCheck(FALSE);
}

