/**********************************************************************
 *<
   FILE:       DlgProcs.cpp

   DESCRIPTION:   Dialog Handlers and UI functions for the Spring Controller

   CREATED BY:    Adam Felt

   HISTORY: 

 *>   Copyright (c) 1999-2000, All Rights Reserved.
 **********************************************************************/

#include "rsspring.h"

#include "3dsmaxport.h"
#include <maxicon.h> 

//Function to invoke the UI of the modeless trackview dialog
//***********************************************************************
template <class T>
void RsSpring<T>::EditTrackParams(TimeValue /*t*/,ParamDimensionBase* /*dim*/,
            TCHAR* /*pname*/,HWND hParent,IObjParam *ip,DWORD /*flags*/)
{
   //many, many bugs might occur if you end up with two Spring dialogs open!
   //changed it so it actives the existing open window.
	HWND openWindow = FindOpenRsSpringWindow(hParent, this);
	if (NULL==openWindow) 
	{
		dlg = new RsSpringTrackDlg(ip,this);
		dlg->Init(hParent);
		RegisterRsSpringWindow(dlg->hWnd,hParent,this);
	} 
	else 
	{
		SetActiveWindow(openWindow);
	}
	this->ip = ip;
}

//ParamMap2 dialog proc for the dynamics rollout in the Motion Panel
//********************************************************************
INT_PTR DynMapDlgProc::DlgProc(
      TimeValue ,IParamMap2* map,HWND hWnd,UINT msg,WPARAM wParam,LPARAM )
{
   TSTR undoStr = GetString(IDS_UNDO_SPINNER_CHANGE);

   switch (msg) 
   {
      case WM_INITDIALOG: 
         cont->hParams1 = hWnd;
         cont->pmap = this;
         paramMap = map;
         InitParams();
         break;

      case CC_SPINNER_BUTTONDOWN:
		  if (!theHold.Holding())
			  theHold.Begin();
		  break;
      case CC_SPINNER_CHANGE:
		  {
			  cont->NotifyDependents(FOREVER,(PartID)PART_ALL,REFMSG_CHANGE);
			  Interface *ip = GetCOREInterface();
			  ip->RedrawViews(ip->GetTime());
			  break;
		  }
      case WM_CUSTEDIT_ENTER:
         break;

      case WM_COMMAND:
		  {
			  BOOL isDown;
			  switch (LOWORD(wParam)) 
			  {
			  case IDC_CHCK_PRESRV_XFORMS: 
				  isDown = IsDlgButtonChecked(hWnd, IDC_CHCK_PRESRV_XFORMS);//dlg->iButtLink->IsChecked();
				  cont->SetPreserveXForms(isDown);
				  break;
			  }
		  }
		  break;
      case WM_DESTROY:
         DestroyParams();
         cont->pmap = NULL;
         return FALSE;

   }
   return FALSE;
}

//DynMapDlgProc : UI functions for Dynamics rollout in the Motion Panel
//********************************************************************
void DynMapDlgProc::InitParams()
{
	iStrengthX = GetISpinner(GetDlgItem(cont->hParams1,IDC_RS_SPRING_STRENGTHX_SPIN));
	iStrengthX->SetLimits(RS_SPRING_PARAM_MIN,RS_SPRING_STRENGTH_MAX,FALSE);
	iStrengthX->SetScale(RS_SPRING_PARAM_STEP);
	iStrengthX->LinkToEdit(GetDlgItem(cont->hParams1,IDC_RS_SPRING_STRENGTHX),EDITTYPE_FLOAT);
	iStrengthX->SetValue(cont->GetStrength(SPRING_PARAM_AXIS_X), 0);

	iStrengthY = GetISpinner(GetDlgItem(cont->hParams1,IDC_RS_SPRING_STRENGTHY_SPIN));
	iStrengthY->SetLimits(RS_SPRING_PARAM_MIN,RS_SPRING_STRENGTH_MAX,FALSE);
	iStrengthY->SetScale(RS_SPRING_PARAM_STEP);
	iStrengthY->LinkToEdit(GetDlgItem(cont->hParams1,IDC_RS_SPRING_STRENGTHY),EDITTYPE_FLOAT);
	iStrengthY->SetValue(cont->GetStrength(SPRING_PARAM_AXIS_Y), 0);

	iStrengthZ = GetISpinner(GetDlgItem(cont->hParams1,IDC_RS_SPRING_STRENGTHZ_SPIN));
	iStrengthZ->SetLimits(RS_SPRING_PARAM_MIN,RS_SPRING_STRENGTH_MAX,FALSE);
	iStrengthZ->SetScale(RS_SPRING_PARAM_STEP);
	iStrengthZ->LinkToEdit(GetDlgItem(cont->hParams1,IDC_RS_SPRING_STRENGTHZ),EDITTYPE_FLOAT);
	iStrengthZ->SetValue(cont->GetStrength(SPRING_PARAM_AXIS_Z), 0);

	iDampeningX = GetISpinner(GetDlgItem(cont->hParams1,IDC_RS_SPRING_DAMPENINGX_SPIN));
	iDampeningX->SetLimits(RS_SPRING_PARAM_MIN,RS_SPRING_DAMPEN_MAX,FALSE);
	iDampeningX->SetScale(RS_SPRING_PARAM_STEP);
	iDampeningX->LinkToEdit(GetDlgItem(cont->hParams1,IDC_RS_SPRING_DAMPENINGX),EDITTYPE_FLOAT);
	iDampeningX->SetValue(cont->GetDampening(SPRING_PARAM_AXIS_X), 0);

	iDampeningY = GetISpinner(GetDlgItem(cont->hParams1,IDC_RS_SPRING_DAMPENINGY_SPIN));
	iDampeningY->SetLimits(RS_SPRING_PARAM_MIN,RS_SPRING_DAMPEN_MAX,FALSE);
	iDampeningY->SetScale(RS_SPRING_PARAM_STEP);
	iDampeningY->LinkToEdit(GetDlgItem(cont->hParams1,IDC_RS_SPRING_DAMPENINGY),EDITTYPE_FLOAT);
	iDampeningY->SetValue(cont->GetDampening(SPRING_PARAM_AXIS_Y), 0);

	iDampeningZ = GetISpinner(GetDlgItem(cont->hParams1,IDC_RS_SPRING_DAMPENINGZ_SPIN));
	iDampeningZ->SetLimits(RS_SPRING_PARAM_MIN,RS_SPRING_DAMPEN_MAX,FALSE);
	iDampeningZ->SetScale(RS_SPRING_PARAM_STEP);
	iDampeningZ->LinkToEdit(GetDlgItem(cont->hParams1,IDC_RS_SPRING_DAMPENINGZ),EDITTYPE_FLOAT);
	iDampeningZ->SetValue(cont->GetDampening(SPRING_PARAM_AXIS_Z), 0);

	xLimitSpinMin = GetISpinner(GetDlgItem(cont->hParams1,IDC_RS_SPRING_LIMITX_SPIN_MIN));
	xLimitSpinMin->SetLimits(RS_SPRING_MIN_LIMIT_MIN,RS_SPRING_MIN_LIMIT_MAX,FALSE);
	xLimitSpinMin->SetScale(RS_SPRING_MIN_LIMIT_STEP);
	xLimitSpinMin->LinkToEdit(GetDlgItem(cont->hParams1,IDC_RS_SPRING_LIMITX_MIN),EDITTYPE_FLOAT);
	xLimitSpinMin->SetValue(cont->GetLimit(SPRING_PARAM_AXIS_X, SPRING_PARAM_MIN), 0);

	yLimitSpinMin = GetISpinner(GetDlgItem(cont->hParams1,IDC_RS_SPRING_LIMITY_SPIN_MIN));
	yLimitSpinMin->SetLimits(RS_SPRING_MIN_LIMIT_MIN,RS_SPRING_MIN_LIMIT_MAX,FALSE);
	yLimitSpinMin->SetScale(RS_SPRING_MIN_LIMIT_STEP);
	yLimitSpinMin->LinkToEdit(GetDlgItem(cont->hParams1,IDC_RS_SPRING_LIMITY_MIN),EDITTYPE_FLOAT);
	yLimitSpinMin->SetValue(cont->GetLimit(SPRING_PARAM_AXIS_Y, SPRING_PARAM_MIN), 0);

	zLimitSpinMin = GetISpinner(GetDlgItem(cont->hParams1,IDC_RS_SPRING_LIMITZ_SPIN_MIN));
	zLimitSpinMin->SetLimits(RS_SPRING_MIN_LIMIT_MIN,RS_SPRING_MIN_LIMIT_MAX,FALSE);
	zLimitSpinMin->SetScale(RS_SPRING_MIN_LIMIT_STEP);
	zLimitSpinMin->LinkToEdit(GetDlgItem(cont->hParams1,IDC_RS_SPRING_LIMITZ_MIN),EDITTYPE_FLOAT);
	zLimitSpinMin->SetValue(cont->GetLimit(SPRING_PARAM_AXIS_Z, SPRING_PARAM_MIN), 0);

	xLimitSpinMax = GetISpinner(GetDlgItem(cont->hParams1,IDC_RS_SPRING_LIMITX_SPIN_MAX));
	xLimitSpinMax->SetLimits(RS_SPRING_MAX_LIMIT_MIN,RS_SPRING_MAX_LIMIT_MAX,FALSE);
	xLimitSpinMax->SetScale(RS_SPRING_MAX_LIMIT_STEP);
	xLimitSpinMax->LinkToEdit(GetDlgItem(cont->hParams1,IDC_RS_SPRING_LIMITX_MAX),EDITTYPE_FLOAT);
	xLimitSpinMax->SetValue(cont->GetLimit(SPRING_PARAM_AXIS_X, SPRING_PARAM_MAX), 0);

	yLimitSpinMax = GetISpinner(GetDlgItem(cont->hParams1,IDC_RS_SPRING_LIMITY_SPIN_MAX));
	yLimitSpinMax->SetLimits(RS_SPRING_MAX_LIMIT_MIN,RS_SPRING_MAX_LIMIT_MAX,FALSE);
	yLimitSpinMax->SetScale(RS_SPRING_MAX_LIMIT_STEP);
	yLimitSpinMax->LinkToEdit(GetDlgItem(cont->hParams1,IDC_RS_SPRING_LIMITY_MAX),EDITTYPE_FLOAT);
	yLimitSpinMax->SetValue(cont->GetLimit(SPRING_PARAM_AXIS_Y, SPRING_PARAM_MAX), 0);

	zLimitSpinMax = GetISpinner(GetDlgItem(cont->hParams1,IDC_RS_SPRING_LIMITZ_SPIN_MAX));
	zLimitSpinMax->SetLimits(RS_SPRING_MAX_LIMIT_MIN,RS_SPRING_MAX_LIMIT_MAX,FALSE);
	zLimitSpinMax->SetScale(RS_SPRING_MAX_LIMIT_STEP);
	zLimitSpinMax->LinkToEdit(GetDlgItem(cont->hParams1,IDC_RS_SPRING_LIMITZ_MAX),EDITTYPE_FLOAT);
	zLimitSpinMax->SetValue(cont->GetLimit(SPRING_PARAM_AXIS_Z, SPRING_PARAM_MAX), 0);

	xGravitySpin = GetISpinner(GetDlgItem(cont->hParams1,IDC_RS_SPRING_GRAVITYX_SPIN));
	xGravitySpin->SetLimits(RS_SPRING_MIN_LIMIT_MIN,RS_SPRING_MAX_LIMIT_MAX,FALSE);
	xGravitySpin->SetScale(RS_SPRING_PARAM_STEP);
	xGravitySpin->LinkToEdit(GetDlgItem(cont->hParams1,IDC_RS_SPRING_GRAVITYX),EDITTYPE_FLOAT);
	xGravitySpin->SetValue(cont->GetGravity(SPRING_PARAM_AXIS_X), 0);

	yGravitySpin = GetISpinner(GetDlgItem(cont->hParams1,IDC_RS_SPRING_GRAVITYY_SPIN));
	yGravitySpin->SetLimits(RS_SPRING_MIN_LIMIT_MIN,RS_SPRING_MAX_LIMIT_MAX,FALSE);
	yGravitySpin->SetScale(RS_SPRING_PARAM_STEP);
	yGravitySpin->LinkToEdit(GetDlgItem(cont->hParams1,IDC_RS_SPRING_GRAVITYY),EDITTYPE_FLOAT);
	yGravitySpin->SetValue(cont->GetGravity(SPRING_PARAM_AXIS_Y), 0);

	zGravitySpin = GetISpinner(GetDlgItem(cont->hParams1,IDC_RS_SPRING_GRAVITYZ_SPIN));
	zGravitySpin->SetLimits(RS_SPRING_MIN_LIMIT_MIN, RS_SPRING_MAX_LIMIT_MAX,FALSE);
	zGravitySpin->SetScale(RS_SPRING_PARAM_STEP);
	zGravitySpin->LinkToEdit(GetDlgItem(cont->hParams1,IDC_RS_SPRING_GRAVITYZ),EDITTYPE_FLOAT);
	zGravitySpin->SetValue(cont->GetGravity(SPRING_PARAM_AXIS_Z), 0);


	xGravityAxisSpin = GetISpinner(GetDlgItem(cont->hParams1,IDC_RS_SPRING_GRAVITY_AXISX_SPIN));
	xGravityAxisSpin->SetLimits(RS_SPRING_GRAVITY_AXIS_LIMIT_MIN,RS_SPRING_GRAVITY_AXIS_LIMIT_MAX,FALSE);
	xGravityAxisSpin->SetScale(RS_SPRING_GRAVITY_AXIS_STEP);
	xGravityAxisSpin->LinkToEdit(GetDlgItem(cont->hParams1,IDC_RS_SPRING_GRAVITY_AXISX),EDITTYPE_FLOAT);
	xGravityAxisSpin->SetValue(cont->GetGravityAxis(SPRING_PARAM_AXIS_X), 0);

	yGravityAxisSpin = GetISpinner(GetDlgItem(cont->hParams1,IDC_RS_SPRING_GRAVITY_AXISY_SPIN));
	yGravityAxisSpin->SetLimits(RS_SPRING_GRAVITY_AXIS_LIMIT_MIN,RS_SPRING_GRAVITY_AXIS_LIMIT_MAX,FALSE);
	yGravityAxisSpin->SetScale(RS_SPRING_GRAVITY_AXIS_STEP);
	yGravityAxisSpin->LinkToEdit(GetDlgItem(cont->hParams1,IDC_RS_SPRING_GRAVITY_AXISY),EDITTYPE_FLOAT);
	yGravityAxisSpin->SetValue(cont->GetGravityAxis(SPRING_PARAM_AXIS_Y), 0);

	zGravityAxisSpin = GetISpinner(GetDlgItem(cont->hParams1,IDC_RS_SPRING_GRAVITY_AXISZ_SPIN));
	zGravityAxisSpin->SetLimits(RS_SPRING_GRAVITY_AXIS_LIMIT_MIN,RS_SPRING_GRAVITY_AXIS_LIMIT_MAX,FALSE);
	zGravityAxisSpin->SetScale(RS_SPRING_GRAVITY_AXIS_STEP);
	zGravityAxisSpin->LinkToEdit(GetDlgItem(cont->hParams1,IDC_RS_SPRING_GRAVITY_AXISZ),EDITTYPE_FLOAT);
	zGravityAxisSpin->SetValue(cont->GetGravityAxis(SPRING_PARAM_AXIS_Z), 0);
}

void DynMapDlgProc::DestroyParams()
{
	ReleaseISpinner(iStrengthX);   iStrengthX = NULL;
	ReleaseISpinner(iStrengthY);   iStrengthY = NULL;
	ReleaseISpinner(iStrengthZ);   iStrengthZ = NULL;
	ReleaseISpinner(iDampeningX);  iDampeningX = NULL;
	ReleaseISpinner(iDampeningY);  iDampeningY = NULL;
	ReleaseISpinner(iDampeningZ);  iDampeningZ = NULL;
	ReleaseISpinner(xLimitSpinMin);  xLimitSpinMin = NULL;
	ReleaseISpinner(yLimitSpinMin);  yLimitSpinMin = NULL;
	ReleaseISpinner(zLimitSpinMin);  zLimitSpinMin = NULL;
	ReleaseISpinner(xLimitSpinMax);  xLimitSpinMax = NULL;
	ReleaseISpinner(yLimitSpinMax);  yLimitSpinMax = NULL;
	ReleaseISpinner(zLimitSpinMax);  zLimitSpinMax = NULL;
	ReleaseISpinner(xGravitySpin);  xGravitySpin = NULL;
	ReleaseISpinner(yGravitySpin);  yGravitySpin = NULL;
	ReleaseISpinner(zGravitySpin);  zGravitySpin = NULL;
	ReleaseISpinner(xGravityAxisSpin);  xGravityAxisSpin = NULL;
	ReleaseISpinner(yGravityAxisSpin);  yGravityAxisSpin = NULL;
	ReleaseISpinner(zGravityAxisSpin);  zGravityAxisSpin = NULL;
//	ReleaseICustButton(iButtLink);	iButtLink = NULL;
}


//The Window handler for the Dynamics rollout in the modeless propertis dialog
//******************************************************************************
INT_PTR CALLBACK DynamicsRollupDialogProc( HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam )
{
	RsSpringTrackDlg *dlg = DLGetWindowLongPtr<RsSpringTrackDlg*>(hDlg);
	if ( !dlg && message != WM_INITDIALOG ) return FALSE;
	// TimeValue t = 0;
	//int sel;
	//int state;
	TSTR undoStr = GetString(IDS_UNDO_SPINNER_CHANGE);

	switch ( message ) 
	{
	case WM_INITDIALOG:
		DLSetWindowLongPtr(hDlg, lParam);
		dlg = (RsSpringTrackDlg*)lParam;
		dlg->dynDlg = hDlg;
		dlg->InitDynamicsParams();
		return TRUE;

	case WM_DESTROY:
		//dlg->DestroyDynamicsParams();
		return FALSE;

	case WM_MOUSEACTIVATE:
		return FALSE;

	case WM_LBUTTONDOWN:
	case WM_LBUTTONUP:
	case WM_MOUSEMOVE:            
		//          cont->ip->RollupMouseMessage(hDlg,message,wParam,lParam);
		return FALSE;


	case CC_SPINNER_BUTTONDOWN:
		if (!theHold.Holding())
			theHold.Begin();
		break;

	case CC_SPINNER_CHANGE:
		{
			switch (LOWORD(wParam)) 
			{
			  case IDC_RS_SPRING_STRENGTHX_SPIN:
				  dlg->cont->SetStrength(SPRING_PARAM_AXIS_X, (float)dlg->iStrengthX->GetFVal(), false);
				  break;
			  case IDC_RS_SPRING_STRENGTHY_SPIN:
				  dlg->cont->SetStrength(SPRING_PARAM_AXIS_Y, (float)dlg->iStrengthY->GetFVal(), false);
				  break;
			  case IDC_RS_SPRING_STRENGTHZ_SPIN:
				  dlg->cont->SetStrength(SPRING_PARAM_AXIS_Z, (float)dlg->iStrengthZ->GetFVal(), false);
				  break;

			  case IDC_RS_SPRING_DAMPENINGX_SPIN:
				  dlg->cont->SetDampening(SPRING_PARAM_AXIS_X, (float)dlg->iDampeningX->GetFVal(), false);
				  break;
			  case IDC_RS_SPRING_DAMPENINGY_SPIN:
				  dlg->cont->SetDampening(SPRING_PARAM_AXIS_Y, (float)dlg->iDampeningY->GetFVal(), false);
				  break;
			  case IDC_RS_SPRING_DAMPENINGZ_SPIN:
				  dlg->cont->SetDampening(SPRING_PARAM_AXIS_Z, (float)dlg->iDampeningZ->GetFVal(), false);
				  break;
			case IDC_RS_SPRING_LIMITX_SPIN_MIN:
				dlg->cont->SetLimit(SPRING_PARAM_AXIS_X, SPRING_PARAM_MIN, (float)dlg->xLimitSpinMin->GetFVal(), false);
				break;
			case IDC_RS_SPRING_LIMITY_SPIN_MIN:
				dlg->cont->SetLimit(SPRING_PARAM_AXIS_Y, SPRING_PARAM_MIN, (float)dlg->yLimitSpinMin->GetFVal(), false);
				break;
			case IDC_RS_SPRING_LIMITZ_SPIN_MIN:
				dlg->cont->SetLimit(SPRING_PARAM_AXIS_Z, SPRING_PARAM_MIN, (float)dlg->zLimitSpinMin->GetFVal(), false);
				break;
			case IDC_RS_SPRING_LIMITX_SPIN_MAX:
				dlg->cont->SetLimit(SPRING_PARAM_AXIS_X, SPRING_PARAM_MAX, (float)dlg->xLimitSpinMax->GetFVal(), false);
				break;
			case IDC_RS_SPRING_LIMITY_SPIN_MAX:
				dlg->cont->SetLimit(SPRING_PARAM_AXIS_Y, SPRING_PARAM_MAX, (float)dlg->yLimitSpinMax->GetFVal(), false);
				break;
			case IDC_RS_SPRING_LIMITZ_SPIN_MAX:
				dlg->cont->SetLimit(SPRING_PARAM_AXIS_Z, SPRING_PARAM_MAX, (float)dlg->zLimitSpinMax->GetFVal(), false);
				break;
			case IDC_RS_SPRING_GRAVITYX_SPIN:
				dlg->cont->SetGravity(SPRING_PARAM_AXIS_X, (float)dlg->xGravitySpin->GetFVal(), false);
				break;
			case IDC_RS_SPRING_GRAVITYY_SPIN:
				dlg->cont->SetGravity(SPRING_PARAM_AXIS_Y, (float)dlg->yGravitySpin->GetFVal(), false);
				break;
			case IDC_RS_SPRING_GRAVITYZ_SPIN:
				dlg->cont->SetGravity(SPRING_PARAM_AXIS_Z, (float)dlg->zGravitySpin->GetFVal(), false);
				break;
			case IDC_RS_SPRING_GRAVITY_AXISX_SPIN:
				dlg->cont->SetGravityAxis(SPRING_PARAM_AXIS_X, (float)dlg->xGravityAxisSpin->GetFVal(), false);
				break;
			case IDC_RS_SPRING_GRAVITY_AXISY_SPIN:
				dlg->cont->SetGravityAxis(SPRING_PARAM_AXIS_Y, (float)dlg->yGravityAxisSpin->GetFVal(), false);
				break;
			case IDC_RS_SPRING_GRAVITY_AXISZ_SPIN:
				dlg->cont->SetGravityAxis(SPRING_PARAM_AXIS_Z, (float)dlg->zGravityAxisSpin->GetFVal(), false);
				break;
			default: break;
			}
			dlg->cont->NotifyDependents(FOREVER,(PartID)PART_ALL,REFMSG_CHANGE);
			dlg->ip->RedrawViews(dlg->ip->GetTime());
			break;
		}
	case WM_CUSTEDIT_ENTER:
	case CC_SPINNER_BUTTONUP:
 		break;

	case WM_COMMAND:
		{
			BOOL isDown;
			switch (LOWORD(wParam)) 
			{
			case IDC_CHCK_PRESRV_XFORMS: 
				isDown = IsDlgButtonChecked(hDlg, IDC_CHCK_PRESRV_XFORMS);//dlg->iButtLink->IsChecked();
				dlg->cont->SetPreserveXForms(isDown);
				break;
			}
		}
		break;
	case WM_PAINT:
		if (!dlg->cont->m_valid) dlg->Update();
		return FALSE;

	}
	return FALSE;

}

//Dialog handler for the About rollout
//*********************************************
INT_PTR CALLBACK AboutRollupDialogProc( HWND , UINT message, WPARAM , LPARAM  )
{
   switch ( message ) 
   {
      case WM_INITDIALOG:
         break;
      break;
   }
   return false;
}

//Dialog Handler for the modeless properties dialog
//********************************************************************
static INT_PTR CALLBACK RsSpringDlgProc(HWND hWnd,UINT message,WPARAM wParam,LPARAM lParam)
{
   RsSpringDlg *dlg = DLGetWindowLongPtr<RsSpringDlg*>(hWnd);

   switch (message) {
      case WM_INITDIALOG: {         
         DLSetWindowLongPtr(hWnd, lParam);
		 SendMessage(hWnd, WM_SETICON, ICON_SMALL, 
			 DLGetClassLongPtr<LPARAM>(GetCOREInterface()->GetMAXHWnd(), GCLP_HICONSM));
         dlg = (RsSpringDlg*)lParam;
         
         if (!dlg) return FALSE;
         dlg->TVRollUp = GetIRollup(GetDlgItem(hWnd,IDC_RS_SPRING_ROLLOUT));

         static TCHAR buf[80];
         int bufLen = 80;        

         LoadString(hInstance, IDS_DYN_PARAMS, buf, bufLen);
         dlg->TVRollUp->AppendRollup(hInstance, 
            MAKEINTRESOURCE(IDD_DYNAMICS_PANEL), 
            DynamicsRollupDialogProc, 
            buf, (LPARAM)dlg);

         dlg->TVRollUp->Show(0);

         break;
         }
      case WM_COMMAND:
      case WM_PAINT:
         if (!dlg->cont->m_valid) dlg->Update();
         return 0;
         
      case WM_SIZING:
         break;

      case WM_CLOSE:
         DestroyWindow(hWnd);       
         break;

      case WM_DESTROY:     
          dlg->DestroyDynamicsParams();
         ReleaseIRollup(dlg->TVRollUp);   dlg->TVRollUp = NULL;
         delete dlg;
         break;

      case WM_LBUTTONDOWN:
      case WM_LBUTTONUP:
      case WM_MOUSEMOVE:         
         dlg->MouseMessage(message,wParam,lParam);
         return FALSE;

      default:
         return 0;
      }
   return 1;
}

//TimeChangeCallback method:
void RsSpringDlgTimeChangeCallback::TimeChanged(TimeValue )
{
}

// Functions for the modeless trackview dialog
//***********************************************************
RsSpringDlg::RsSpringDlg(IObjParam *i,IRsSpring *c)
	: iButtLink(NULL)
	, iStrengthX(NULL)
	, iStrengthY(NULL)
	, iStrengthZ(NULL)
	, iDampeningX(NULL)
	, iDampeningY(NULL)
	, iDampeningZ(NULL)
	, xLimitSpinMin(NULL)
	, yLimitSpinMin(NULL)
	, zLimitSpinMin(NULL)
	, xLimitSpinMax(NULL)
	, yLimitSpinMax(NULL)
	, zLimitSpinMax(NULL)
	, xGravitySpin(NULL)
	, yGravitySpin(NULL)
	, zGravitySpin(NULL)
	, xGravityAxisSpin(NULL)
	, yGravityAxisSpin(NULL)
	, zGravityAxisSpin(NULL)
   {
   cont = NULL;
   theHold.Suspend();
   ReplaceReference(0,dynamic_cast<RefTargetHandle>(c));
   theHold.Resume();
   ip = i;
   blockRedraw = FALSE;
   hWnd  = NULL;
   dynDlg = forceDlg = NULL;
   cont->dlg = this;
   timeChangeCallback = new RsSpringDlgTimeChangeCallback(this);
   ip->RegisterTimeChangeCallback(timeChangeCallback);
   }

RsSpringDlg::~RsSpringDlg()
   {
   cont->dlg = NULL;
   ip->UnRegisterTimeChangeCallback(timeChangeCallback);
   delete timeChangeCallback;
   UnRegisterRsSpringWindow(hWnd);

   theHold.Suspend();
   DeleteAllRefsFromMe();
   theHold.Resume();
   }

void RsSpringDlg::Init(HWND hParent)
{
   hWnd = CreateWin(hParent);
   //Update();
}

void RsSpringDlg::InitDynamicsParams()
   {
//   if (!cont->hParams1) cont->pickNodeMode = new PickNodeMode(cont);

	iStrengthX = GetISpinner(GetDlgItem(dynDlg,IDC_RS_SPRING_STRENGTHX_SPIN));
	iStrengthX->SetLimits(RS_SPRING_PARAM_MIN,RS_SPRING_STRENGTH_MAX,FALSE);
	iStrengthX->SetScale(RS_SPRING_PARAM_STEP);
	iStrengthX->LinkToEdit(GetDlgItem(dynDlg,IDC_RS_SPRING_STRENGTHX),EDITTYPE_FLOAT);
	iStrengthX->SetValue(cont->GetStrength(SPRING_PARAM_AXIS_X), 0);

	iStrengthY = GetISpinner(GetDlgItem(dynDlg,IDC_RS_SPRING_STRENGTHY_SPIN));
	iStrengthY->SetLimits(RS_SPRING_PARAM_MIN,RS_SPRING_STRENGTH_MAX,FALSE);
	iStrengthY->SetScale(RS_SPRING_PARAM_STEP);
	iStrengthY->LinkToEdit(GetDlgItem(dynDlg,IDC_RS_SPRING_STRENGTHY),EDITTYPE_FLOAT);
	iStrengthY->SetValue(cont->GetStrength(SPRING_PARAM_AXIS_Y), 0);

	iStrengthZ = GetISpinner(GetDlgItem(dynDlg,IDC_RS_SPRING_STRENGTHZ_SPIN));
	iStrengthZ->SetLimits(RS_SPRING_PARAM_MIN,RS_SPRING_STRENGTH_MAX,FALSE);
	iStrengthZ->SetScale(RS_SPRING_PARAM_STEP);
	iStrengthZ->LinkToEdit(GetDlgItem(dynDlg,IDC_RS_SPRING_STRENGTHZ),EDITTYPE_FLOAT);
	iStrengthZ->SetValue(cont->GetStrength(SPRING_PARAM_AXIS_Z), 0);

	iDampeningX = GetISpinner(GetDlgItem(dynDlg,IDC_RS_SPRING_DAMPENINGX_SPIN));
	iDampeningX->SetLimits(RS_SPRING_PARAM_MIN,RS_SPRING_DAMPEN_MAX,FALSE);
	iDampeningX->SetScale(RS_SPRING_PARAM_STEP);
	iDampeningX->LinkToEdit(GetDlgItem(dynDlg,IDC_RS_SPRING_DAMPENINGX),EDITTYPE_FLOAT);
	iDampeningX->SetValue(cont->GetDampening(SPRING_PARAM_AXIS_X), 0);

	iDampeningY = GetISpinner(GetDlgItem(dynDlg,IDC_RS_SPRING_DAMPENINGY_SPIN));
	iDampeningY->SetLimits(RS_SPRING_PARAM_MIN,RS_SPRING_DAMPEN_MAX,FALSE);
	iDampeningY->SetScale(RS_SPRING_PARAM_STEP);
	iDampeningY->LinkToEdit(GetDlgItem(dynDlg,IDC_RS_SPRING_DAMPENINGY),EDITTYPE_FLOAT);
	iDampeningY->SetValue(cont->GetDampening(SPRING_PARAM_AXIS_Y), 0);

	iDampeningZ = GetISpinner(GetDlgItem(dynDlg,IDC_RS_SPRING_DAMPENINGZ_SPIN));
	iDampeningZ->SetLimits(RS_SPRING_PARAM_MIN,RS_SPRING_DAMPEN_MAX,FALSE);
	iDampeningZ->SetScale(RS_SPRING_PARAM_STEP);
	iDampeningZ->LinkToEdit(GetDlgItem(dynDlg,IDC_RS_SPRING_DAMPENINGZ),EDITTYPE_FLOAT);
	iDampeningZ->SetValue(cont->GetDampening(SPRING_PARAM_AXIS_Z), 0);

	xLimitSpinMin = GetISpinner(GetDlgItem(dynDlg,IDC_RS_SPRING_LIMITX_SPIN_MIN));
	xLimitSpinMin->SetLimits(RS_SPRING_MIN_LIMIT_MIN,RS_SPRING_MIN_LIMIT_MAX,FALSE);
	xLimitSpinMin->SetScale(RS_SPRING_MIN_LIMIT_STEP);
	xLimitSpinMin->LinkToEdit(GetDlgItem(dynDlg,IDC_RS_SPRING_LIMITX_MIN),EDITTYPE_FLOAT);
	xLimitSpinMin->SetValue(cont->GetLimit(SPRING_PARAM_AXIS_X, SPRING_PARAM_MIN), 0);

	yLimitSpinMin = GetISpinner(GetDlgItem(dynDlg,IDC_RS_SPRING_LIMITY_SPIN_MIN));
	yLimitSpinMin->SetLimits(RS_SPRING_MIN_LIMIT_MIN,RS_SPRING_MIN_LIMIT_MAX,FALSE);
	yLimitSpinMin->SetScale(RS_SPRING_MIN_LIMIT_STEP);
	yLimitSpinMin->LinkToEdit(GetDlgItem(dynDlg,IDC_RS_SPRING_LIMITY_MIN),EDITTYPE_FLOAT);
	yLimitSpinMin->SetValue(cont->GetLimit(SPRING_PARAM_AXIS_Y, SPRING_PARAM_MIN), 0);

	zLimitSpinMin = GetISpinner(GetDlgItem(dynDlg,IDC_RS_SPRING_LIMITZ_SPIN_MIN));
	zLimitSpinMin->SetLimits(RS_SPRING_MIN_LIMIT_MIN,RS_SPRING_MIN_LIMIT_MAX,FALSE);
	zLimitSpinMin->SetScale(RS_SPRING_MIN_LIMIT_STEP);
	zLimitSpinMin->LinkToEdit(GetDlgItem(dynDlg,IDC_RS_SPRING_LIMITZ_MIN),EDITTYPE_FLOAT);
	zLimitSpinMin->SetValue(cont->GetLimit(SPRING_PARAM_AXIS_Z, SPRING_PARAM_MIN), 0);

	xLimitSpinMax = GetISpinner(GetDlgItem(dynDlg,IDC_RS_SPRING_LIMITX_SPIN_MAX));
	xLimitSpinMax->SetLimits(RS_SPRING_MAX_LIMIT_MIN,RS_SPRING_MAX_LIMIT_MAX,FALSE);
	xLimitSpinMax->SetScale(RS_SPRING_MAX_LIMIT_STEP);
	xLimitSpinMax->LinkToEdit(GetDlgItem(dynDlg,IDC_RS_SPRING_LIMITX_MAX),EDITTYPE_FLOAT);
	xLimitSpinMax->SetValue(cont->GetLimit(SPRING_PARAM_AXIS_X, SPRING_PARAM_MAX), 0);

	yLimitSpinMax = GetISpinner(GetDlgItem(dynDlg,IDC_RS_SPRING_LIMITY_SPIN_MAX));
	yLimitSpinMax->SetLimits(RS_SPRING_MAX_LIMIT_MIN,RS_SPRING_MAX_LIMIT_MAX,FALSE);
	yLimitSpinMax->SetScale(RS_SPRING_MAX_LIMIT_STEP);
	yLimitSpinMax->LinkToEdit(GetDlgItem(dynDlg,IDC_RS_SPRING_LIMITY_MAX),EDITTYPE_FLOAT);
	yLimitSpinMax->SetValue(cont->GetLimit(SPRING_PARAM_AXIS_Y, SPRING_PARAM_MAX), 0);

	zLimitSpinMax = GetISpinner(GetDlgItem(dynDlg,IDC_RS_SPRING_LIMITZ_SPIN_MAX));
	zLimitSpinMax->SetLimits(RS_SPRING_MAX_LIMIT_MIN,RS_SPRING_MAX_LIMIT_MAX,FALSE);
	zLimitSpinMax->SetScale(RS_SPRING_MAX_LIMIT_STEP);
	zLimitSpinMax->LinkToEdit(GetDlgItem(dynDlg,IDC_RS_SPRING_LIMITZ_MAX),EDITTYPE_FLOAT);
	zLimitSpinMax->SetValue(cont->GetLimit(SPRING_PARAM_AXIS_Z, SPRING_PARAM_MAX), 0);

	xGravitySpin = GetISpinner(GetDlgItem(dynDlg,IDC_RS_SPRING_GRAVITYX_SPIN));
	xGravitySpin->SetLimits(RS_SPRING_MIN_LIMIT_MIN,RS_SPRING_MAX_LIMIT_MAX,FALSE);
	xGravitySpin->SetScale(RS_SPRING_PARAM_STEP);
	xGravitySpin->LinkToEdit(GetDlgItem(dynDlg,IDC_RS_SPRING_GRAVITYX),EDITTYPE_FLOAT);
	xGravitySpin->SetValue(cont->GetGravity(SPRING_PARAM_AXIS_X), 0);

	yGravitySpin = GetISpinner(GetDlgItem(dynDlg,IDC_RS_SPRING_GRAVITYY_SPIN));
	yGravitySpin->SetLimits(RS_SPRING_MIN_LIMIT_MIN,RS_SPRING_MAX_LIMIT_MAX,FALSE);
	yGravitySpin->SetScale(RS_SPRING_PARAM_STEP);
	yGravitySpin->LinkToEdit(GetDlgItem(dynDlg,IDC_RS_SPRING_GRAVITYY),EDITTYPE_FLOAT);
	yGravitySpin->SetValue(cont->GetGravity(SPRING_PARAM_AXIS_Y), 0);

	zGravitySpin = GetISpinner(GetDlgItem(dynDlg,IDC_RS_SPRING_GRAVITYZ_SPIN));
	zGravitySpin->SetLimits(RS_SPRING_MIN_LIMIT_MIN,RS_SPRING_MAX_LIMIT_MAX,FALSE);
	zGravitySpin->SetScale(RS_SPRING_PARAM_STEP);
	zGravitySpin->LinkToEdit(GetDlgItem(dynDlg,IDC_RS_SPRING_GRAVITYZ),EDITTYPE_FLOAT);
	zGravitySpin->SetValue(cont->GetGravity(SPRING_PARAM_AXIS_Z), 0);


	xGravityAxisSpin = GetISpinner(GetDlgItem(dynDlg,IDC_RS_SPRING_GRAVITY_AXISX_SPIN));
	xGravityAxisSpin->SetLimits(RS_SPRING_GRAVITY_AXIS_LIMIT_MIN,RS_SPRING_GRAVITY_AXIS_LIMIT_MAX,FALSE);
	xGravityAxisSpin->SetScale(RS_SPRING_GRAVITY_AXIS_STEP);
	xGravityAxisSpin->LinkToEdit(GetDlgItem(dynDlg,IDC_RS_SPRING_GRAVITY_AXISX),EDITTYPE_FLOAT);
	xGravityAxisSpin->SetValue(cont->GetGravityAxis(SPRING_PARAM_AXIS_X), 0);

	yGravityAxisSpin = GetISpinner(GetDlgItem(dynDlg,IDC_RS_SPRING_GRAVITY_AXISY_SPIN));
	yGravityAxisSpin->SetLimits(RS_SPRING_GRAVITY_AXIS_LIMIT_MIN,RS_SPRING_GRAVITY_AXIS_LIMIT_MAX,FALSE);
	yGravityAxisSpin->SetScale(RS_SPRING_GRAVITY_AXIS_STEP);
	yGravityAxisSpin->LinkToEdit(GetDlgItem(dynDlg,IDC_RS_SPRING_GRAVITY_AXISY),EDITTYPE_FLOAT);
	yGravityAxisSpin->SetValue(cont->GetGravityAxis(SPRING_PARAM_AXIS_Y), 0);

	zGravityAxisSpin = GetISpinner(GetDlgItem(dynDlg,IDC_RS_SPRING_GRAVITY_AXISZ_SPIN));
	zGravityAxisSpin->SetLimits(RS_SPRING_GRAVITY_AXIS_LIMIT_MIN,RS_SPRING_GRAVITY_AXIS_LIMIT_MAX,FALSE);
	zGravityAxisSpin->SetScale(RS_SPRING_GRAVITY_AXIS_STEP);
	zGravityAxisSpin->LinkToEdit(GetDlgItem(dynDlg,IDC_RS_SPRING_GRAVITY_AXISZ),EDITTYPE_FLOAT);
	zGravityAxisSpin->SetValue(cont->GetGravityAxis(SPRING_PARAM_AXIS_Z), 0);
	//   cont->UpdateNodeList();
}

void RsSpringDlg::DestroyDynamicsParams()
{
	ReleaseISpinner(iStrengthX);   iStrengthX = NULL;
	ReleaseISpinner(iStrengthY);   iStrengthY = NULL;
	ReleaseISpinner(iStrengthZ);   iStrengthZ = NULL;
	ReleaseISpinner(iDampeningX);  iDampeningX = NULL;
	ReleaseISpinner(iDampeningY);  iDampeningY = NULL;
	ReleaseISpinner(iDampeningZ);  iDampeningZ = NULL;
   ReleaseISpinner(xLimitSpinMin);  xLimitSpinMin = NULL;
   ReleaseISpinner(yLimitSpinMin);  yLimitSpinMin = NULL;
   ReleaseISpinner(zLimitSpinMin);  zLimitSpinMin = NULL;
   ReleaseISpinner(xLimitSpinMax);  xLimitSpinMax = NULL;
   ReleaseISpinner(yLimitSpinMax);  yLimitSpinMax = NULL;
   ReleaseISpinner(zLimitSpinMax);  zLimitSpinMax = NULL;
   ReleaseISpinner(xGravitySpin);  xGravitySpin = NULL;
   ReleaseISpinner(yGravitySpin);  yGravitySpin = NULL;
   ReleaseISpinner(zGravitySpin);  zGravitySpin = NULL;
   ReleaseISpinner(xGravityAxisSpin);  xGravityAxisSpin = NULL;
   ReleaseISpinner(yGravityAxisSpin);  yGravityAxisSpin = NULL;
   ReleaseISpinner(zGravityAxisSpin);  zGravityAxisSpin = NULL;
//   ReleaseICustButton(iButtLink);	iButtLink = NULL;
}

void RsSpringDlg::Invalidate(BOOL force)
{
   if (!blockRedraw || force)
   {
      cont->m_valid = FALSE;
      if (hWnd) 
         InvalidateRect(hWnd,NULL,FALSE);
	  blockRedraw = TRUE;
   }
}

void RsSpringDlg::Update()
{
   if (dynDlg && cont && !cont->m_valid && cont->dyn_pb) 
   {     
      cont->SetSelfReference();
      
	  iStrengthX->SetValue(cont->GetStrength(SPRING_PARAM_AXIS_X), 0);
	  iStrengthY->SetValue(cont->GetStrength(SPRING_PARAM_AXIS_Y), 0);
	  iStrengthZ->SetValue(cont->GetStrength(SPRING_PARAM_AXIS_Z), 0);
	  iDampeningX->SetValue(cont->GetDampening(SPRING_PARAM_AXIS_X), 0);
	  iDampeningY->SetValue(cont->GetDampening(SPRING_PARAM_AXIS_Y), 0);
	  iDampeningZ->SetValue(cont->GetDampening(SPRING_PARAM_AXIS_Z), 0);
	  xLimitSpinMin->SetValue(cont->GetLimit(SPRING_PARAM_AXIS_X, SPRING_PARAM_MIN), 0);
	  yLimitSpinMin->SetValue(cont->GetLimit(SPRING_PARAM_AXIS_Y, SPRING_PARAM_MIN), 0);
	  zLimitSpinMin->SetValue(cont->GetLimit(SPRING_PARAM_AXIS_Z, SPRING_PARAM_MIN), 0);
	  xLimitSpinMax->SetValue(cont->GetLimit(SPRING_PARAM_AXIS_X, SPRING_PARAM_MAX), 0);
	  yLimitSpinMax->SetValue(cont->GetLimit(SPRING_PARAM_AXIS_Y, SPRING_PARAM_MAX), 0);
	  zLimitSpinMax->SetValue(cont->GetLimit(SPRING_PARAM_AXIS_Z, SPRING_PARAM_MAX), 0);
	  xGravitySpin->SetValue(cont->GetGravity(SPRING_PARAM_AXIS_X), 0);
	  yGravitySpin->SetValue(cont->GetGravity(SPRING_PARAM_AXIS_Y), 0);
	  zGravitySpin->SetValue(cont->GetGravity(SPRING_PARAM_AXIS_Z), 0);
	  xGravityAxisSpin->SetValue(cont->GetGravityAxis(SPRING_PARAM_AXIS_X), 0);
	  yGravityAxisSpin->SetValue(cont->GetGravityAxis(SPRING_PARAM_AXIS_Y), 0);
	  zGravityAxisSpin->SetValue(cont->GetGravityAxis(SPRING_PARAM_AXIS_Z), 0);

      cont->m_valid = TRUE;
   }
}


void RsSpringDlg::SetupUI(){ }


int RsSpringDlg::NumRefs() { return 1; }


RefTargetHandle RsSpringDlg::GetReference(int i)
{
	if (i == 0)
	{
		return dynamic_cast<RefTargetHandle>(cont);	
	}
	return NULL;
}

void RsSpringDlg::SetReference(int i, RefTargetHandle rtarg)
{
	if (i == 0)
	{
		cont = dynamic_cast<IRsSpring*>(rtarg);
	}
}

RefResult RsSpringDlg::NotifyRefChanged(
      Interval , 
      RefTargetHandle , 
      PartID& , 
      RefMessage message)
{
   switch (message) {
      case REFMSG_CHANGE:
         if (!blockRedraw) 
			 Invalidate();
         break;
            
      case REFMSG_REF_DELETED:
         MaybeCloseWindow();
         break;
      }
   return REF_SUCCEED;
}


//Functions for subclass of RsSpring Dialog
//***************************************************
HWND RsSpringTrackDlg::CreateWin(HWND hParent)
   {
   return CreateDialogParam(
      hInstance,
      MAKEINTRESOURCE(IDD_RS_SPRING_TRACK),
      hParent,
      RsSpringDlgProc,
      (LPARAM)this);
   }

void RsSpringTrackDlg::MaybeCloseWindow()
{
	PosRsSpring *pPosSpring = dynamic_cast<PosRsSpring*>(cont);
	RotRsSpring *pRotSpring = dynamic_cast<RotRsSpring*>(cont);
	if(pPosSpring)
	{
		CheckForNonRsSpringDlg check(pPosSpring );
		pPosSpring->DoEnumDependents(&check);
		if (!check.non) 
			PostMessage(hWnd,WM_CLOSE,0,0);
	}
	else if(pRotSpring )
	{
		CheckForNonRsSpringDlg check(pRotSpring );
		pRotSpring->DoEnumDependents(&check);
		if (!check.non) 
			PostMessage(hWnd,WM_CLOSE,0,0);
	}
}
