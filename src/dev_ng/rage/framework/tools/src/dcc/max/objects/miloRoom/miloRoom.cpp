
 
#include "MiloRoom.h"
#include <decomp.h>

// Parameter block indices
#define PB_LENGTH	0

//------------------------------------------------------

class MiloRoomClassDesc:public ClassDesc {
public:
	int 			IsPublic() { return 1; }
	void *			Create(BOOL loading = FALSE) { return new MiloRoomObject; }
    const TCHAR * ClassName() {return _T(GetString(IDS_CLASS_NAME));}
	SClass_ID		SuperClassID() { return HELPER_CLASS_ID; }
	Class_ID 		ClassID() { return MILOROOM_CLASS_ID; }
    const TCHAR* 	Category() { return GetString(IDS_CATEGORY);  }
};

static MiloRoomClassDesc miloRoomObjDesc;

ClassDesc* GetMiloRoomObjDesc() { return &miloRoomObjDesc; }

extern HINSTANCE hInstance;

//ICustButton *MiloMeshObject::intRefPickButton = NULL;

HWND MiloRoomObject::hRollup = NULL;
int MiloRoomObject::dlgPrevSel = -1;

class MiloRoomObjPick : public PickModeCallback {
	MiloRoomObject *miloRoomObj;
public:		

	BOOL HitTest(IObjParam *ip,HWND hWnd,ViewExp *vpt,IPoint2 m,int flags);
	BOOL Pick(IObjParam *ip,ViewExp *vpt);	

	void EnterMode(IObjParam *ip);
	void ExitMode(IObjParam *ip);

	HCURSOR GetHitCursor(IObjParam *ip);
	void SetRoomObj(MiloRoomObject *l) { miloRoomObj = l; }
};

static MiloRoomObjPick theSelRoom;

BOOL
MiloRoomObjPick::HitTest(IObjParam *ip,HWND hWnd,ViewExp *vpt,IPoint2 m,int flags)
{
	INode *node = ip->PickNode(hWnd, m);
	if (node == NULL)
		return FALSE;
    Object* obj = node->EvalWorldState(0).obj;
	if ((obj->SuperClassID() == HELPER_CLASS_ID &&
         obj->ClassID() == MILOROOM_CLASS_ID))
		return FALSE;
	return TRUE;
}

void
MiloRoomObjPick::EnterMode(IObjParam *ip)
{

}

void
MiloRoomObjPick::ExitMode(IObjParam *ip)
{
	ip->PopPrompt();
}

BOOL
MiloRoomObjPick::Pick(IObjParam *ip,ViewExp *vpt)
{
	if (vpt->HitCount() == 0)
		return FALSE;
	
	miloRoomObj->iObjParams->RedrawViews(0,REDRAW_NORMAL,miloRoomObj);
	miloRoomObj->iObjParams->ClearPickMode();
	return FALSE;
}


HCURSOR MiloRoomObjPick::GetHitCursor(IObjParam *ip)
{
	return 0;
}


INT_PTR CALLBACK
RollupDialogProc( HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam )
{
	MiloRoomObject *th = (MiloRoomObject *)GetWindowLongPtr( hDlg, GWLP_USERDATA );	
	if ( !th && message != WM_INITDIALOG ) return FALSE;

	switch ( message ) {
	case WM_INITDIALOG: {
		th = (MiloRoomObject *)lParam;
		SetWindowLongPtr( hDlg, GWLP_USERDATA, (LONG)th );
		SetDlgFont( hDlg, th->iObjParams->GetAppHFont() );

		th->hRollup = hDlg;
		th->dlgPrevSel = -1;
		}
		return TRUE;			

	case WM_DESTROY:
		th->iObjParams->ClearPickMode();
		th->previousMode = NULL;
		return FALSE;


	case WM_MOUSEACTIVATE:
		th->iObjParams->RealizeParamPanel();
		return FALSE;

	case WM_LBUTTONDOWN:
	case WM_LBUTTONUP:
	case WM_MOUSEMOVE:
		th->iObjParams->RollupMouseMessage(hDlg,message,wParam,lParam);
		return FALSE;
	
	default:
		return FALSE;
	}
}


void MiloRoomObject::BeginEditParams( IObjParam *ip, ULONG flags,Animatable *prev )
{
	
}
		
void MiloRoomObject::EndEditParams( IObjParam *ip, ULONG flags,Animatable *prev)
{
	
}

MiloRoomObject::MiloRoomObject() : HelperObject() 
{
	dlgCurSelDist = -1.0f;
	previousMode = NULL;
}

MiloRoomObject::~MiloRoomObject()
{
	
	DeleteAllRefsFromMe();
}


IObjParam *MiloRoomObject::iObjParams;

// This is only called if the object MAKES references to other things.
RefResult
MiloRoomObject::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, 
     PartID& partID, RefMessage message ) 
{
	
	return REF_SUCCEED;
}


ObjectState
MiloRoomObject::Eval(TimeValue time){
	return ObjectState(this);
}

Interval
MiloRoomObject::ObjectValidity(TimeValue time)
{
	Interval ivalid;
	ivalid.SetInfinite();
	return ivalid;	
}

void
MiloRoomObject::GetMat(TimeValue t, INode* inode, ViewExp* vpt, Matrix3& tm)
{
	tm = inode->GetObjectTM(t);
}

void
MiloRoomObject::GetLocalBoundBox(TimeValue t, INode* inode, ViewExp* vpt, Box3& box )
{
}

void MiloRoomObject::GetWorldBoundBox(TimeValue t, INode* inode, ViewExp* vpt, Box3& box )
{
	
	Matrix3 tm;
    BuildMesh();             
	GetMat(t,inode,vpt,tm);

	
	int nv = mesh.getNumVerts();
	box.Init();
	for (int i=0; i<nv; i++) 
		box += tm*mesh.getVert(i);
	if (dlgCurSelDist >= 0.0f) {
		Point3 x[32], y[32], z[32];
		GetDistPoints(dlgCurSelDist, x, y, z);
		for (int i = 0; i < 32; i++) {
			box += tm*x[i];
			box += tm*y[i];
			box += tm*z[i];
		}
	}
	
	
}


void MiloRoomObject::BuildMesh()
{

	//Build room object mesh
	mesh.setNumVerts(5);
	mesh.setNumFaces(4);

	mesh.setVert(0, Point3(-0.5,-0.5,0.000000));
	mesh.setVert(1, Point3(0.5,-0.5,0.000000));
	mesh.setVert(2, Point3(-0.5,0.5,0.000000));
	mesh.setVert(3, Point3(0.5,0.5,0.000000));
	mesh.setVert(4, Point3(0.0,0.0,0.500000));

	mesh.faces[0].setVerts(0, 1, 4);
	mesh.faces[0].setEdgeVisFlags(EDGE_VIS ,EDGE_VIS ,EDGE_VIS);
	mesh.faces[0].setSmGroup(2);
	mesh.faces[1].setVerts(0, 2, 4);
	mesh.faces[1].setEdgeVisFlags(EDGE_VIS,EDGE_VIS,EDGE_VIS);
	mesh.faces[1].setSmGroup(2);
	mesh.faces[2].setVerts(2, 4, 3);
	mesh.faces[2].setEdgeVisFlags(EDGE_VIS,EDGE_VIS,EDGE_VIS);
	mesh.faces[2].setSmGroup(2);
	mesh.faces[3].setVerts(3, 4, 1);
	mesh.faces[3].setEdgeVisFlags(EDGE_VIS,EDGE_VIS,EDGE_VIS);
	mesh.faces[3].setSmGroup(2);


	mesh.InvalidateGeomCache();
	mesh.EnableEdgeList(1);

	
}

void MiloRoomObject::DrawBoundBox(TimeValue t, INode* inode, GraphicsWindow *gw, Box3 &roomBounds)
{
	// Draw the bounding box for child objects of the rooom object.
	Point3 vertArray1[5], vertArray2[5], vertArray3[5], vertArray4[5], vertArray5[5], vertArray6[5];
	BuildBoundBox(vertArray1, vertArray2, vertArray3, vertArray4, vertArray5, vertArray6, roomBounds);
	
	if(!inode->IsFrozen())
		gw->setColor( LINE_COLOR, 1.0f, 1.0f, 1.0f);

	gw->polyline(5, vertArray1, NULL, NULL, TRUE, NULL);
	gw->polyline(5, vertArray2, NULL, NULL, TRUE, NULL);
	gw->polyline(5, vertArray3, NULL, NULL, TRUE, NULL);
	gw->polyline(5, vertArray4, NULL, NULL, TRUE, NULL);
	gw->polyline(5, vertArray5, NULL, NULL, TRUE, NULL);
}

void MiloRoomObject::BuildBoundBox(Point3* vertArray1, Point3* vertArray2, Point3* vertArray3, Point3* vertArray4, Point3* vertArray5, Point3* vertArray6, Box3 &roomBounds)
{
	// Build the arrays for the bounding box
	Point3 roomBBMax = roomBounds.pmax;
	Point3 roomBBMin = roomBounds.pmin;

	Point3 point1 = roomBBMax;
	Point3 point2 = roomBBMin;
	Point3 point3 = Point3(roomBBMax.x,roomBBMax.y,roomBBMin.z);
	Point3 point4 = Point3(roomBBMax.x,roomBBMin.y,roomBBMin.z);
	Point3 point5 = Point3(roomBBMin.x,roomBBMax.y,roomBBMin.z);
	Point3 point6 = Point3(roomBBMin.x,roomBBMin.y,roomBBMax.z);
	Point3 point7 = Point3(roomBBMax.x,roomBBMin.y,roomBBMax.z);
	Point3 point8 = Point3(roomBBMin.x,roomBBMax.y,roomBBMax.z);

	vertArray1[0] = point7;
	vertArray1[1] = point4;
	vertArray1[2] = point2;
	vertArray1[3] = point6;
	vertArray1[4] = point7;

	vertArray2[0] = point1;
	vertArray2[1] = point3;
	vertArray2[2] = point5;
	vertArray2[3] = point8;
	vertArray2[4] = point1;
	
	vertArray3[0] = point8;
	vertArray3[1] = point5;
	vertArray3[2] = point2;
	vertArray3[3] = point6;
	vertArray3[4] = point8;

	vertArray4[0] = point1;
	vertArray4[1] = point3;
	vertArray4[2] = point4;
	vertArray4[3] = point7;
	vertArray4[4] = point1;

	vertArray5[0] = point3;
	vertArray5[1] = point5;
	vertArray5[2] = point2;
	vertArray5[3] = point4;
	vertArray5[4] = point3;

	vertArray5[0] = point1;
	vertArray5[1] = point8;
	vertArray5[2] = point6;
	vertArray5[3] = point7;
	vertArray5[4] = point1;

	
}



void MiloRoomObject::GetDistPoints(float radius, Point3 *x, Point3 *y, Point3 *z)
{
	
	float dang = PI / ( 2.0f * 8.0f);
	float ang = 0.0f;
	for (int i = 0; i < 32; i++ ) {
		z[i].x = x[i].y = y[i].x = radius * (float) cos(ang);
		z[i].y = x[i].z = y[i].z = radius * (float) sin(ang);
		z[i].z = x[i].x = y[i].y = 0.0f;
		ang += dang;
	}
	
}





void MiloRoomObject::CalculateTotalRoomBounds(INode* inode, Box3 &roomBounds)
{
	
	// Loop through all room objects and calculate the min and max bound values.
	for (int i=0; i<inode->NumberOfChildren(); ++i)
	{
		 
		int cCount = inode->NumberOfChildren();
		INode *pChild = inode->GetChildNode(i);

		Matrix3 nodeParentMtx = pChild->GetParentTM(0);
		Matrix3 childTM = pChild->GetNodeTM(0);
					
		childTM *= Inverse(nodeParentMtx);

		AffineParts affine;
		decomp_affine(childTM, &affine);

		Point3 objTrans(affine.t.x, affine.t.y, affine.t.z);
		roomBounds += objTrans;
		
	}
}


int MiloRoomObject::Display(TimeValue t, INode* inode, ViewExp *vpt, int flags)
{

	BuildMesh();
	Matrix3 m;
	GraphicsWindow *gw = vpt->getGW();
	Material *mtl = gw->getMaterial();

	DWORD rlim = gw->getRndLimits();
	
	gw->setRndLimits(GW_WIREFRAME|GW_EDGES_ONLY|GW_BACKCULL);
	
	GetMat(t,inode,vpt,m);
	gw->setTransform(m);
	if (inode->Selected()) 
		gw->setColor( LINE_COLOR, 1.0f, 1.0f, 1.0f);
	else if(!inode->IsFrozen())
		gw->setColor( LINE_COLOR, 0.0f, 1.0f, 1.0f);
	
	mesh.render( gw, mtl, NULL, COMP_ALL);
	Box3 roomBounds;
	if (inode->Selected()) 
	{
		CalculateTotalRoomBounds(inode, roomBounds);
		DrawBoundBox(t, inode, gw, roomBounds);
	}

	gw->setRndLimits(rlim);
	return(0);
}

int
MiloRoomObject::HitTest(TimeValue t, INode *inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt)
{
	HitRegion hitRegion;
	DWORD	savedLimits;
	int res = FALSE;
	Matrix3 m;
	GraphicsWindow *gw = vpt->getGW();	
	Material *mtl = gw->getMaterial();
	MakeHitRegion(hitRegion,type,crossing,4,p);	
	gw->setRndLimits(((savedLimits = gw->getRndLimits()) | GW_PICK) & ~GW_ILLUM);
	GetMat(t,inode,vpt,m);
	gw->setTransform(m);
	gw->clearHitCode();
	if (mesh.select( gw, mtl, &hitRegion, flags & HIT_ABORTONHIT )) 
		return TRUE;
	gw->setRndLimits(savedLimits);
	return res;
}

class MiloRoomCreateCallBack : public CreateMouseCallBack {
	private:
		IPoint2 sp0;
		Point3 p0;
		MiloRoomObject *miloRoomObject;

	public:
		int proc( ViewExp *vpt,int msg, int point, int flags, IPoint2 m, 
			Matrix3& mat);
		void SetObj(MiloRoomObject *obj) {miloRoomObject = obj;}

};

			
int MiloRoomCreateCallBack::proc(ViewExp *vpt,int msg, int point, int flags, IPoint2 m, Matrix3& mat)
{	
	Point3 p1,center;

	
	if (msg==MOUSE_POINT || MOUSE_MOVE) 
	{
	//case MOUSE_MOVE:
		
		switch ( point ) {
		case 0:  // only happens with MOUSE_POINT msg				

			sp0 = m;
			p0 = vpt->SnapPoint(m,m,NULL,SNAP_IN_PLANE);
			mat.SetTrans(p0);
			
			return CREATE_STOP;
			break;
		
		case 1:
			mat.IdentityMatrix();
			p1 = vpt->SnapPoint(m,m,NULL,SNAP_IN_PLANE);
			mat.SetTrans(p1);
			//miloRoomObject->radius = Length(p1-p0);
			
			if (flags&MOUSE_CTRL) {
				float ang = (float)atan2(p1.y-p0.y,p1.x-p0.x);					
				mat.PreRotateZ(miloRoomObject->iObjParams->SnapAngle(ang));
			}

			if (msg==MOUSE_POINT) {										
				return (Length(m-sp0)<3)?CREATE_ABORT:CREATE_STOP;
			}
			
			break;					   
		}			
		//break;
	}
	
	return TRUE;
}

// A single instance of the callback object.
static MiloRoomCreateCallBack miloRoomCreateCB;

// This method allows MAX to access and call our proc method to 
// handle the user input.
CreateMouseCallBack*
MiloRoomObject::GetCreateMouseCallBack() 
{
	miloRoomCreateCB.SetObj(this);
	return(&miloRoomCreateCB);
}

// IO
#define INST_REF_SIZE_CHUNK		0xaca0



IOResult MiloRoomObject::Save(ISave *isave)
{
	
	return IO_OK;
}

IOResult MiloRoomObject::Load(ILoad *iload)
{
	
	return IO_OK;
}


RefTargetHandle
MiloRoomObject::Clone(RemapDir& remap)
{
	
	MiloRoomObject* ts = new MiloRoomObject();
   
    
     
   
	BaseClone(this, ts, remap);
    return ts;
	

}
