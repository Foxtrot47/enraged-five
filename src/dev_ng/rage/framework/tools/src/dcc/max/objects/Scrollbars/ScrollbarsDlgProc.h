//
// filename:	ScrollbarsDlgProc.h
// author:		David Muir
// date:		24 January 2007
// description:	
//

#ifndef INC_SCROLLBARDLGPROC_H_
#define INC_SCROLLBARDLGPROC_H_

// --- Include Files ------------------------------------------------------------

// 3D Studio Max SDK headers
#include "Max.h"
#include "iparamm2.h"

// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

class CScrollbarsDlgProc : public ParamMap2UserDlgProc
{
public:
	CScrollbarsDlgProc( ) { }

	void DeleteThis( );
#if !defined( _WIN64 )
	BOOL DlgProc( TimeValue t, IParamMap2* map, HWND hWnd,
				  UINT msg, WPARAM wParam, LPARAM lParam );
#else
	INT_PTR DlgProc( TimeValue t, IParamMap2* map, HWND hWnd,
				  UINT msg, WPARAM wParam, LPARAM lParam );
#endif
	void Update( TimeValue t );
};

// --- Globals ------------------------------------------------------------------
extern CScrollbarsDlgProc g_ScrollbarsDlgProc;

#endif // !INC_SCROLLBARDLGPROC_H_
