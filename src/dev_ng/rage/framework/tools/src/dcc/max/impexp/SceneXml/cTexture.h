//
// filename:	cMaterial.h
// description:
//

#ifndef INC_CTEXTURE_H_
#define INC_CTEXTURE_H_

// --- Include Files ------------------------------------------------------------
#include <Windows.h>

// STL headers
#include <map>
#include <string>
#include <vector>

// Win32HelperLib headers
#include "Win32HelperLib/Guid.h"

// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------

// --- Forward Declarations -----------------------------------------------------

class TiXmlElement;

// --- Structure/Class Definitions ----------------------------------------------

class cTexture;

/**
 * @brief Material information.
 */
class cTexture
{
public:
	cTexture( );
	cTexture( std::string filename, std::string type );
	cTexture& operator=( const cTexture& rhs );
	virtual ~cTexture( );

	TiXmlElement* ToXmlElement( const std::string& export_as ) const;

	const std::string& RetFilename( ) const { return m_sFilename; }
	const std::string& RetType( ) const { return m_sType; }

protected:
	std::string					m_sFilename;		//!< The file name for the texture
	std::string					m_sType;			//!< The type of the texture
};

// --- Globals ------------------------------------------------------------------

#endif // !INC_CMATERIAL_H_