//
//
//    Filename: UVWRestore.h
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 18/05/99 15:45 $
//   $Revision: 2 $
// Description: Backup classes for UVWmachine
//
//
#ifndef _UVW_RESTORE_H_
#define _UVW_RESTORE_H_

// std headers
#include <vector>
// max headers
#include <Max.h>


// class prototypes
class UVWData;


class UVWFaceRestore : public RestoreObj
{
private:
	std::vector<TVFace> m_undoFaces, m_redoFaces;
	Modifier *m_pMod;
	UVWData *m_pUVWData;
	
public:
	UVWFaceRestore(Modifier *pM, UVWData *pD);
	void Restore(int isUndo);
	void Redo();
	int Size() { return m_undoFaces.size() * sizeof(TVFace) + m_redoFaces.size() * sizeof(TVFace); }
	void EndHold(); 
	TSTR Description() { return TSTR(_T("UV Face Restore")); }

};

class UVWVertexRestore : public RestoreObj
{
private:
	std::vector<UVVert> m_undoVerts, m_redoVerts;
	Modifier *m_pMod;
	UVWData *m_pUVWData;
	
public:
	UVWVertexRestore(Modifier *pM, UVWData *pD);
	void Restore(int isUndo);
	void Redo();
	int Size() { return m_undoVerts.size() * sizeof(UVVert) + m_redoVerts.size() * sizeof(UVVert); }
	void EndHold(); 
	TSTR Description() { return TSTR(_T("UV Vertex Restore")); }

};

#endif // _UVW_RESTORE_H_