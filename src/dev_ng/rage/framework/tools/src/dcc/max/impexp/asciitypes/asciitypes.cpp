//
//
//    Filename: asciitypes.cpp
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Read/Write operators for the ascii types
//
//
#include "asciitypes.h"
#include "asciitools.h"
#include <script.h>

// STD headers
#pragma warning (disable : 4786)
#include <cassert>
#include <iomanip>

// using directives
using namespace ascii;

using std::string;
using std::ifstream;
using std::ofstream;
using std::endl;

TabSpaces tab;

// --- Scene ------------------------------------------------------------------------------------------------------

//
//        name: Scene::Scene
// description: Constructor
//
Scene::Scene()
{
	m_pScene = new SceneData;
}

//
//        name: Scene::~Scene
// description: Destructor
//
Scene::~Scene()
{
	delete m_pScene;
}

//
//        name: Scene::Clear
// description: Clear contents of scene
//
void Scene::Clear()
{
	m_pScene->Clear();
}

//
//        name: SceneData::Read
// description: Read scene from file stream
//
bool Scene::Read(ifstream& is)
{
	return m_pScene->Read(is);
}

//
//        name: GetComponent etc
// description: Access functions
//
Component* Scene::GetComponent(int32 i)
{
	return m_pScene->GetComponent(i);
}
int32 Scene::GetNumComponents() const
{
	return m_pScene->GetNumComponents();
}
Component* Scene::GetRootComponent()
{
	return m_pScene->GetRootComponent();
}
AIConnection* Scene::GetAIConnection(int32 i)
{
	return m_pScene->GetAIConnection(i);
}
int32 Scene::GetNumAIConnections() const
{
	return m_pScene->GetNumAIConnections();
}

AIPath* Scene::GetAIPath(int32 i)
{
	return m_pScene->GetAIPath(i);
}
int32 Scene::GetNumAIPaths() const
{
	return m_pScene->GetNumAIPaths();
}

// --- SceneData --------------------------------------------------------------------------------------------------

//
//        name: SceneData::SceneData
// description: Constructor
//
SceneData::SceneData() 
{
	m_root.SetName("ROOT");
}

//
//        name: SceneData::Clear
// description: Clear contents of scene
//          in: 
//         out: 
//
void SceneData::Clear()
{
	ObjectOwner::Clear();
	m_compArray.clear();
	m_aiConnectionArray.clear();
	m_aiPathArray.clear();
}

//
//        name: SceneData::Read\Write
// description: the read and write functions for the scene class
//
bool SceneData::Read(ifstream& is)
{
	string token;

	Clear();
	thePointerRedirector.Clear();
	thePointerRedirector.AddRedirection("NULL", NULL);
	thePointerRedirector.AddRedirection("ROOT", NULL);

	while(GetValidString(is, token))
	{
		if(token == "Component")
		{
			Component *pComp = new Component;

			if(!pComp->Read(is))
			{
				delete pComp;
				return false;
			}
			AddComponent(pComp);
			if(!thePointerRedirector.AddRedirection(pComp->GetName(), pComp) )
			{
				return false;
			}
		}
		else if(token == "AIConnection")
		{
			AIConnection *pConnection = new AIConnection;

			if(!pConnection->Read(is))
			{
				delete pConnection;
				return false;
			}
			AddAIConnection(pConnection);
			if(!thePointerRedirector.AddRedirection(pConnection->GetName(), pConnection) )
			{
				return false;
			}
		}
		else if(token == "AIPath")
		{
			AIPath *pPath = new AIPath;

			if(!pPath->Read(is))
			{
				delete pPath;
				return false;
			}
			AddAIPath(pPath);
			if(!thePointerRedirector.AddRedirection(pPath->GetName(), pPath) )
			{
				return false;
			}
		}
	}

	if(!thePointerRedirector.Process())
		return false;

	// Add components with NULL parent to the ROOT component
	for(uint32 i=0; i<m_compArray.size(); i++)
	{
		if(m_compArray[i]->GetParent() == NULL)
			m_root.AddChild(m_compArray[i]);
	}

	return true;
}

bool SceneData::Write(ofstream& os) const
{
	os << std::fixed << std::setprecision(6);
	ObjectOwner::WriteTokens(os);

	return true;
}

//
//        name: SceneData::AddComponent etc
// description: Component list access functions
//
void SceneData::AddComponent(Component *pComp) 
{
	m_compArray.push_back(pComp); 
	AddObject(pComp);
}
Component* SceneData::GetComponent(int32 i) 
{
	return m_compArray[i];
}
const Component* SceneData::GetComponent(int32 i) const 
{
	return m_compArray[i];
}
int32 SceneData::GetNumComponents() const 
{
	return m_compArray.size();
}
Component* SceneData::GetRootComponent() 
{
	return &m_root;
}

//
//        name: SceneData::AddAIConnection
// description: AIConnection list access functions
//
void SceneData::AddAIConnection(AIConnection *pConnection) 
{
	m_aiConnectionArray.push_back(pConnection); 
	AddObject(pConnection);
}
AIConnection* SceneData::GetAIConnection(int32 i) 
{
	return m_aiConnectionArray[i];
}
int32 SceneData::GetNumAIConnections() const 
{
	return m_aiConnectionArray.size();
}

//
//        name: SceneData::AddAIPath
// description: AIPath list access functions
//
void SceneData::AddAIPath(AIPath *pPath) 
{
	m_aiPathArray.push_back(pPath); 
	AddObject(pPath);
}
AIPath* SceneData::GetAIPath(int32 i) 
{
	return m_aiPathArray[i];
}
int32 SceneData::GetNumAIPaths() const 
{
	return m_aiPathArray.size();
}

// --- BaseObject --------------------------------------------------------------------------------------------------

//
//        name: BaseObject::BaseObject
// description: Constructor
//
BaseObject::BaseObject(const std::string& name) : m_name(name) 
{
}

//
//        name: *BaseObject::GetName
// description: Name access functions
//
const char *BaseObject::GetName() const 
{
	return m_name.c_str();
}
void BaseObject::SetName(std::string name) 
{
	m_name = name;
}
//
//        name: BaseObject::StartRead
// description: Start reading object data. Read { bracket and name
//          in: is = input stream
//
bool BaseObject::StartRead(std::ifstream& is)
{
	std::string token;
	if(!(is >> Check<char>('{')))
		return false;
	return true;
}

//
//        name: BaseObject::StartWrite
// description: Start writing object data
//          in: os = output stream
//				pClassName = name of class to output
//
void BaseObject::StartWrite(std::ofstream& os, const char *pClassName) const
{
	os << tab << pClassName << " {" << endl;
	tab.Add();
}

//
//        name: BaseObject::StartReadWithName
// description: Start reading object data. Read { bracket and name
//          in: is = input stream
//
bool BaseObject::StartReadWithName(std::ifstream& is)
{
	std::string token;

	if(!StartRead(is))
		return false;
	if(!(is >> Check<string>("Name")))
		return false;
	if(!GetValidString(is, m_name))
		return false;
	return true;
}

//
//        name: BaseObject::StartWriteWithName
// description: Start writing object data
//          in: os = output stream
//				pClassName = name of class to output
//
void BaseObject::StartWriteWithName(std::ofstream& os, const char *pClassName) const
{
	StartWrite(os, pClassName);
	WriteString(os, "Name", m_name);
}

//
//        name: BaseObject::EndWrite
// description: End writing object data
//
void BaseObject::EndWrite(std::ofstream& os) const
{
	tab.Delete();
	os << tab << "}" << std::endl;
}

//
//        name: BaseObject::WriteString
// description: Write functions for an ASCII object, writes string, integer, floating point and pointer name
//          in: os = output stream
//				pToken = name of item
//				s = string
//				i = integer
//				f = floating point
//				pObj = pointer to object
//
void BaseObject::WriteString(ofstream& os, const char* pToken, const string& s) 
{
	// if string contains a space then put speech marks round it
	if(s.find(' ') != s.npos)
		os << tab << pToken << "\t\"" << s << "\"" << endl;
	else
		os << tab << pToken << "\t" << s << endl;
}
void BaseObject::WriteInteger(ofstream& os, const char* pToken, int32 i) 
{
	os << tab << pToken << "\t" << i << endl;
}
void BaseObject::WriteFloat(ofstream& os, const char* pToken, float f) 
{
	os << tab << pToken << "\t" << f << endl;
}
void BaseObject::WriteBool(ofstream& os, const char* pToken, bool b) 
{
	os << tab << pToken << "\t" << b << endl;
}
void BaseObject::WritePointer(ofstream& os, const char* pToken, const BaseObject* pObj) 
{
	if(pObj)
		WriteString(os, pToken, pObj->m_name);
	else
		WriteString(os, pToken, "NULL");
}
void BaseObject::WritePoint4d(ofstream& os, const char* pToken, const Point4d& pt)
{
	os << tab << pToken << "\t" << pt[0] << ", " << pt[1] << ", " << pt[2] << ", " << pt[3] << endl;
}
void BaseObject::WritePoint3d(ofstream& os, const char* pToken, const Point3d& pt)
{
	os << tab << pToken << "\t" << pt[0] << ", " << pt[1] << ", " << pt[2] << endl;
}
void BaseObject::WritePoint2d(ofstream& os, const char* pToken, const Point2d& pt)
{
	os << tab << pToken << "\t" << pt[0] << ", " << pt[1] << endl;
}
void BaseObject::WriteIntegerArray(ofstream &os, const char *pToken, const int32* pI, int32 size)
{
	size--;
	os << tab << pToken << "\t" << *pI++;

	while(size--)
		os << ", " << *pI++;
	os << endl;
}
void BaseObject::WriteFloatArray(ofstream &os, const char *pToken, const float* pF, int32 size)
{
	size--;
	os << tab << pToken << "\t" << *pF++;

	while(size--)
		os << ", " << *pF++;
	os << endl;
}
void BaseObject::WriteString(ofstream& os, const string& s) 
{
	// if string contains a space then put speech marks round it
	if(s.find(' ') != s.npos)
		os << "\"" << s << "\"";
	else
		os << s;
}
void BaseObject::WriteTab(ofstream& os)
{
	os << tab;
}

//
//        name: BaseObject::ReadString
// description: Read a token and Read a string, an integer, a floating point or an object name
//          in: os = output stream
//				pToken = name of item
//				s = string
//				i = integer
//				f = floating point
//				ppObj = pointer to a pointer to an object
//         out: success
//
bool BaseObject::ReadString(ifstream& is, string& s)
{
	if(GetValidString(is, s))
	{
		if(s[0] == '}')
			return false;
		return true;
	}
	return false;
}
bool BaseObject::ReadInteger(ifstream& is, int32& i)
{
	if(is >> i)
		return true;
	return false;
}
bool BaseObject::ReadFloat(ifstream& is, float& f)
{
	if(is >> f)
		return true;
	return false;
}
bool BaseObject::ReadBool(ifstream& is, bool& b)
{
	if(is >> b)
		return true;
	return false;
}
bool BaseObject::ReadPointer(ifstream& is, void** ppObj)
{
	string name;
	if(GetValidString(is, name))
	{
		if(name[0] == '}')
			return false;
		thePointerRedirector.AddRequest(name, ppObj);
		return true;
	}
	return false;
}
bool BaseObject::ReadPoint4d(ifstream& is, Point4d& pt)
{
	if(is >> pt[0] >> Check<char>(',') >> pt[1] >> Check<char>(',') >> pt[2] >> Check<char>(',') >> pt[3])
		return true;
	else
		return false;
}
bool BaseObject::ReadPoint3d(ifstream& is, Point3d& pt)
{
	if(is >> pt[0] >> Check<char>(',') >> pt[1] >> Check<char>(',') >> pt[2])
		return true;
	else
		return false;
}
bool BaseObject::ReadPoint2d(ifstream& is, Point2d& pt)
{
	if(is >> pt[0] >> Check<char>(',') >> pt[1])
		return true;
	else
		return false;
}
bool BaseObject::ReadIntegerArray(ifstream& is, int32* pI, int32 size)
{
	size--;
	if(!(is >> *pI++))
		return false;
	while(size--)
	{
		if(!(is >> Check<char>(',') >> *pI++))
			return false;
	}
	return true;
}
bool BaseObject::ReadFloatArray(ifstream& is, float* pF, int32 size)
{
	size--;
	if(!(is >> *pF++))
		return false;
	while(size--)
	{
		if(!(is >> Check<char>(',') >> *pF++))
			return false;
	}
	return true;
}

//
//        name: BaseObject::SkipLineOrBlock
// description: Skip to end of line or end of block
//
void BaseObject::SkipLineOrBlock(ifstream& is)
{
	char c;
	int32 brackets=0;

	do {
		is.get(c);
		if(c == '{')
			brackets++;
		if(c == '}')
			brackets--;
	} while(c != '\n' || brackets > 0);
}

// --- Component -------------------------------------------------------------------------------------------------------

//
//        name: Component::Component
// description: Constructor
//
Component::Component() : BaseObject("component"), m_pSibling(NULL), m_pChild(NULL), m_pLastChild(NULL), m_pParent(NULL),
		m_pCullingSphere(NULL), m_pBoundingSphere(NULL), m_pBoundingBox(NULL), m_pAttributeList(NULL), m_pXForm(NULL),
		m_pObject(NULL)
{
}

//
//        name: Component::Read\Write
// description: the read and write functions for the Component class
//
bool Component::Read(ifstream& is)
{
	string token;

	if(!StartReadWithName(is))
		return false;
	while(ReadString(is, token))
	{
		if(token == "Parent")
		{
			if(!ReadPointer(is, reinterpret_cast<void **>(&m_pParent)))
				return false;
		}
		else if(token == "Sibling")
		{
			if(!ReadPointer(is, reinterpret_cast<void **>(&m_pSibling)))
				return false;
		}
		else if(token == "Child")
		{
			if(!ReadPointer(is, reinterpret_cast<void **>(&m_pChild)))
				return false;
		}
		else if(token == "BoundSphere")
		{
			BoundingSphere *pBoundingSphere = new BoundingSphere;
			if(!pBoundingSphere->Read(is))
				return false;
			SetBoundingSphere(pBoundingSphere);
		}
		else if(token == "BoundBox")
		{
			BoundingBox *pBoundingBox = new BoundingBox;
			if(!pBoundingBox->Read(is))
				return false;
			SetBoundingBox(pBoundingBox);
		}
		else if(token == "CullSphere")
		{
			m_pCullingSphere = new CullingSphere;
			if(!m_pCullingSphere->Read(is))
				return false;
		}
		else if(token == "ColSphere")
		{
			CollisionSphere *pCollisionSphere = new CollisionSphere;
			if(!pCollisionSphere->Read(is))
				return false;
			AddCollisionVolume(pCollisionSphere);
		}
		else if(token == "ColBox")
		{
			CollisionBox *pCollisionBox = new CollisionBox;
			if(!pCollisionBox->Read(is))
				return false;
			AddCollisionVolume(pCollisionBox);
		}
		else if(token == "ColLine")
		{
			CollisionLine *pCollisionLine = new CollisionLine;
			if(!pCollisionLine->Read(is))
				return false;
			AddCollisionVolume(pCollisionLine);
		}
		else if(token == "ColPlane")
		{
			CollisionPlane *pCollisionPlane = new CollisionPlane;
			if(!pCollisionPlane->Read(is))
				return false;
			AddCollisionVolume(pCollisionPlane);
		}
		else if(token == "ColMesh")
		{
			CollisionMesh *pCollisionMesh = new CollisionMesh;
			if(!pCollisionMesh->Read(is))
				return false;
			AddCollisionVolume(pCollisionMesh);
		}
		else if(token == "Attributes")
		{
			AsciiAttributeList *pAttributeList = new AsciiAttributeList;
			if(!pAttributeList->Read(is))
				return false;
			SetAttributeList(pAttributeList);
		}
		else if(token == "XRefObject")
		{
			XRefObject *pXRefObject = new XRefObject;
			if(!pXRefObject->Read(is))
				return false;
			SetReferenceObject(pXRefObject);
		}
		else if(token == "XForm")
		{
			XForm *pXForm = new XForm;
			if(!pXForm->Read(is))
				return false;
			SetXForm(pXForm);
		}
		else
			SkipLineOrBlock(is);
	}
	return true;
}

bool Component::Write(ofstream& os) const
{
	StartWriteWithName(os, "Component");
	WritePointer(os, "Parent", m_pParent);
	WritePointer(os, "Sibling", m_pSibling);
	WritePointer(os, "Child", m_pChild);
	ObjectOwner::WriteTokens(os);
	EndWrite(os);

	return true;
}

Component* Component::GetSibling() {return m_pSibling;}
Component* Component::GetParent() {return m_pParent;}

CullingSphere *Component::GetCullingSphere() {return m_pCullingSphere;}
BoundingSphere *Component::GetBoundingSphere() {return m_pBoundingSphere;}
BoundingBox *Component::GetBoundingBox() {return m_pBoundingBox;}

void Component::AddCollisionVolume(CollisionVolume *pVolume) {m_collisionVolumeArray.push_back(pVolume); AddObject(pVolume);}
CollisionVolume *Component::GetCollisionVolume(int32 i) {return m_collisionVolumeArray[i];}
int32 Component::GetNumCollisionVolumes() const {return m_collisionVolumeArray.size();}

void Component::AddShadowVolume(CollisionVolume *pVolume) {m_shadowVolumeArray.push_back(pVolume); AddObject(pVolume);}
CollisionVolume *Component::GetShadowVolume(int32 i) {return m_shadowVolumeArray[i];}
int32 Component::GetNumShadowVolumes() const {return m_shadowVolumeArray.size();}

//
//        name: Component::SetCullingSphere/SetBoundingSphere/SetBoundingBox
// description: Set the culling sphere, bounding sphere, bounding box for a component
//          in: pCullingSphere = pointer to culling sphere
//
void Component::SetCullingSphere(CullingSphere *pCullingSphere) 
{
	m_pCullingSphere = pCullingSphere;
	AddObject(pCullingSphere);
}
void Component::SetBoundingSphere(BoundingSphere *pBoundingSphere) 
{
	m_pBoundingSphere = pBoundingSphere;
	AddObject(pBoundingSphere);
}
void Component::SetBoundingBox(BoundingBox *pBoundingBox) 
{
	m_pBoundingBox = pBoundingBox;
	AddObject(pBoundingBox);
}

void Component::SetAttributeList(AsciiAttributeList *pAttributeList)
{
	m_pAttributeList = pAttributeList;
	AddObject(pAttributeList);
}

void Component::SetReferenceObject(Object *pObject)
{
	m_pObject = pObject;
	AddObject(pObject);
}

void Component::SetXForm(XForm *pXForm)
{
	m_pXForm = pXForm;
	AddObject(pXForm);
}

//
//        name: Component::GetChild
// description: Return child component
//          in: i = number of child component
//         out: pointer to child component
//
Component* Component::GetChild(int32 i) 
{
	Component *pChild = m_pChild;

	while(i-- && pChild)
		pChild = pChild->m_pSibling;
	return pChild;
}

//
//        name: Component::GetPreviousSibling
// description: Return the previous sibling component
//
Component* Component::GetPreviousSibling() 
{
	assert(m_pParent);

	Component *pChild = m_pParent->m_pChild;

	// if first child there is no previous sibling
	if(pChild == this)
		return NULL;

	while(pChild->m_pSibling)
	{
		if(pChild->m_pSibling == this)
			return pChild;
		pChild = pChild->m_pSibling;
	}
	// this should not happen
	assert(0);
	return NULL;
}

//
//        name: Component::AddChild
// description: Add a child to a parent component
//          in: pChild = pointer to the soon to be child component
//
void Component::AddChild(Component *pChild)
{
	if(pChild->m_pParent)
		DisInherit();

	pChild->m_pParent = this;
	pChild->m_pSibling = NULL;
	if(m_pChild == NULL)
		m_pChild = pChild;
	if(m_pLastChild)
		m_pLastChild->m_pSibling = pChild;
	m_pLastChild = pChild;

}

//
//        name: Component::DisInherit
// description: Extract component from parent child array
//
void Component::DisInherit()
{
	assert(m_pParent);

	Component *pPrev = GetPreviousSibling();
	if(pPrev)
	{
		pPrev->m_pSibling = m_pSibling;
	}
	else
		m_pParent->m_pChild = m_pSibling;

	if(m_pParent->m_pLastChild == this)
		m_pParent->m_pLastChild = pPrev;

	m_pSibling = NULL;
	m_pParent = NULL;
}

// --- Sphere -------------------------------------------------------------------------------------------------------
//
//        name: Sphere::Read\Write
// description: the read and write functions for the collision sphere class
//
bool Sphere::ReadToken(ifstream& is, string& token)
{
	if(token == "Radius")
	{
		if(!BaseObject::ReadFloat(is, m_radius))
			return false;
	}
	else if(token == "Position")
	{
		if(!BaseObject::ReadPoint3d(is, m_posn))
			return false;
	}
	return true;
}

bool Sphere::WriteTokens(ofstream& os) const
{
	BaseObject::WritePoint3d(os, "Position", m_posn);
	BaseObject::WriteFloat(os, "Radius", m_radius);

	return true;
}

//
//        name: Sphere::GetRadius
// description: Sphere data access functions
//
float Sphere::GetRadius() const 
{
	return m_radius;
}
void Sphere::SetRadius(float r) 
{
	m_radius = r;
}
const Point3d& Sphere::GetPosition() const 
{
	return m_posn;
}
void Sphere::SetPosition(const Point3d& p) 
{
	m_posn = p;
}

// --- Box -------------------------------------------------------------------------------------------------

//
//        name: CollisionBox::Read\Write
// description: the read and write functions for the collision box class
//
bool Box::ReadToken(ifstream& is, string& token)
{
	if(token == "Size")
	{
		if(!BaseObject::ReadPoint3d(is, m_size))
			return false;
	}
	else if(token == "Position")
	{
		if(!BaseObject::ReadPoint3d(is, m_posn))
			return false;
	}
	return true;
}

bool Box::WriteTokens(ofstream& os) const
{
	BaseObject::WritePoint3d(os, "Position", m_posn);
	BaseObject::WritePoint3d(os, "Size", m_size);

	return true;
}

//
//        name: Box::GetPosition
// description: Box data access functions
//
const Point3d& Box::GetPosition() const 
{
	return m_posn;
}
void Box::SetPosition(const Point3d& p) 
{
	m_posn = p;
}
const Point3d& Box::GetSize() const 
{
	return m_size;
}
void Box::SetSize(const Point3d& s) 
{
	m_size = s;
}

// --- Line -------------------------------------------------------------------------------------------------

//
//        name: Line::Read\Write
// description: the read and write functions for the line class
//
bool Line::ReadToken(ifstream& is, string& token)
{
	if(token == "StartPoint")
	{
		if(!BaseObject::ReadPoint3d(is, m_start))
			return false;
	}
	else if(token == "EndPoint")
	{
		if(!BaseObject::ReadPoint3d(is, m_end))
			return false;
	}
	return true;
}

bool Line::WriteTokens(ofstream& os) const
{
	BaseObject::WritePoint3d(os, "StartPoint", m_start);
	BaseObject::WritePoint3d(os, "EndPoint", m_end);

	return true;
}

//
//        name: Box::GetPosition
// description: Box data access functions
//
const Point3d& Line::GetStart() const 
{
	return m_start;
}
void Line::SetStart(const Point3d& p) 
{
	m_start = p;
}
const Point3d& Line::GetEnd() const 
{
	return m_end;
}
void Line::SetEnd(const Point3d& p) 
{
	m_end = p;
}


// --- ConnectedComponentList --------------------------------------------------------------------------------------

//
//        name: ConnectedComponentList::Read\Write
// description: the read and write functions for the ConnectedComponentList class
//
bool ConnectedComponentList::Read(ifstream& is)
{
	string token;

	if(!StartRead(is))
		return false;
	while(ReadString(is, token))
	{
		if(token == "Component")
		{
			if(!ReadString(is, token))
				return false;
			AddConnectedComponent(token);
		}
		else if(token == "Number")
		{
			int32 number;
			if(!ReadInteger(is, number))
				return false;
			m_connectedComponents.reserve(number);
		}
	}
	return true;
}

bool ConnectedComponentList::Write(ofstream& os) const
{
	if(m_connectedComponents.size() != 0)
	{
		StartWrite(os, "ConnectedComponents");
		WriteInteger(os, "Number", m_connectedComponents.size());
		for(uint32 i=0; i<m_connectedComponents.size(); i++)
		{
			WritePointer(os, "Component", m_connectedComponents[i]);
		}
		EndWrite(os);
	}

	return true;
}

//
//        name: ConnectedComponentList::AddConnectedComponent,GetConnectedComponent, GetNumConnectedComponents
// description: Access functions to list of components connected to this AIConnection
//
void ConnectedComponentList::AddConnectedComponent(Component *pComp)
{
	m_connectedComponents.push_back(pComp);
}
void ConnectedComponentList::AddConnectedComponent(std::string& name)
{
	m_connectedComponents.push_back(NULL);
	thePointerRedirector.AddRequest(name, reinterpret_cast<void**>(&(m_connectedComponents.back())));
}
Component* ConnectedComponentList::GetConnectedComponent(int32 i)
{
	return m_connectedComponents[i];
}
int32 ConnectedComponentList::GetNumConnectedComponents() const
{
	return m_connectedComponents.size();
}

//
//        name: ConnectedComponentList::SetSize
// description: If you are using the AddConnectedComponent function with a string as a parameter. This function
//				has to be called first to avoid reallocation of the vector storing the component pointers
//          in: size = size of component vector required
//
void ConnectedComponentList::SetSize(int32 size)
{
	m_connectedComponents.reserve(size);
}


// --- XForm -------------------------------------------------------------------------------------------------------

//
//        name: XForm::Read\Write
// description: the read and write functions for the Component class
//
bool XForm::Read(ifstream& is)
{
	string token;

	if(!StartRead(is))
		return false;
	while(ReadString(is, token))
	{
		if(token == "Position")
		{
			if(!ReadPoint3d(is, m_posn))
				return false;
		}
		else if(token == "Quaternion")
		{
			if(!ReadPoint4d(is, m_quat))
				return false;
		}
		if(token == "Scale")
		{
			if(!ReadPoint3d(is, m_scale))
				return false;
		}
		else
			SkipLineOrBlock(is);
	}
	return true;
}


bool XForm::Write(ofstream& os) const
{
	StartWrite(os, "XForm");
	WritePoint3d(os, "Position", m_posn);
	WritePoint4d(os, "Quaternion", m_quat);
	WritePoint3d(os, "Scale", m_scale);
	EndWrite(os);

	return true;
}

// --- ObjectOwner -------------------------------------------------------------------------------------------------

//
//        name: ObjectOwner::WriteTokens
// description: Write objects in object array
//
bool ObjectOwner::WriteTokens(std::ofstream& os) const
{
	for(int32 i=0; i<m_objectArray.GetSize(); i++)
	{
		m_objectArray[i]->Write(os);
	}
	return true;
}

// --- CullingSphere -------------------------------------------------------------------------------------------------

//
//        name: CullingSphere::CullingSphere
// description: Constructor
//
CullingSphere::CullingSphere() : BaseObject("CullSphere") 
{
}

//
//        name: CullingSphere::Read\Write
// description: the read and write functions for the CullingSphere class
//
bool CullingSphere::Read(ifstream& is)
{
	string token;

	if(!StartReadWithName(is))
		return false;
	while(ReadString(is, token))
	{
		if(token == "Radius")
		{
			if(!ReadFloat(is, m_radius))
				return false;
		}
		else
			SkipLineOrBlock(is);
	}
	return true;
}

bool CullingSphere::Write(ofstream& os) const
{
	StartWriteWithName(os, "CullSphere");
	WriteFloat(os, "Radius", m_radius);
	EndWrite(os);

	return true;
}

//
//        name: CullingSphere::GetRadius
// description: Radius access functions
//
float CullingSphere::GetRadius() const 
{
	return m_radius;
}
void CullingSphere::SetRadius(float r) 
{
	m_radius = r;
}

// --- CollisionVolume ----------------------------------------------------------------------------------------------

//
//        name: CollisionVolume::Read\Write
// description: the read and write functions for the collision volume class
//
bool CollisionVolume::ReadToken(ifstream& is, string& token)
{
	if(token == "Attributes")
	{
		AsciiAttributeList *pAttributeList = new AsciiAttributeList;
		if(!pAttributeList->Read(is))
			return false;
		SetAttributeList(pAttributeList);
	}
	return true;
}

CollisionVolume::~CollisionVolume()
{
}

// --- CollisionSphere -------------------------------------------------------------------------------------------------

//
//        name: CollisionSphere::CollisionSphere
// description: Constructor
//
CollisionSphere::CollisionSphere() : CollisionVolume("ColSphere") 
{
}

//
//        name: CollisionSphere::Read\Write
// description: the read and write functions for the collision sphere class
//
bool CollisionSphere::Read(ifstream& is)
{
	string token;

	if(!StartReadWithName(is))
		return false;
	while(ReadString(is, token))
	{
		if(!Sphere::ReadToken(is, token))
			return false;
		if(!CollisionVolume::ReadToken(is, token))
			return false;
	}
	return true;
}

bool CollisionSphere::Write(ofstream& os) const
{
	StartWriteWithName(os, "ColSphere");
	Sphere::WriteTokens(os);
	CollisionVolume::WriteTokens(os);
	EndWrite(os);

	return true;
}

// --- CollisionBox -------------------------------------------------------------------------------------------------

//
//        name: CollisionBox::CollisionBox
// description: Constructor
//
CollisionBox::CollisionBox() : CollisionVolume("ColBox") 
{
}

//
//        name: CollisionBox::Read\Write
// description: the read and write functions for the collision box class
//
bool CollisionBox::Read(ifstream& is)
{
	string token;

	if(!StartReadWithName(is))
		return false;
	while(ReadString(is, token))
	{
		if(!Box::ReadToken(is, token))
			return false;
		if(!CollisionVolume::ReadToken(is, token))
			return false;
	}
	return true;
}

bool CollisionBox::Write(ofstream& os) const
{
	StartWriteWithName(os, "ColBox");
	Box::WriteTokens(os);
	CollisionVolume::WriteTokens(os);
	EndWrite(os);

	return true;
}

// --- CollisionLine -------------------------------------------------------------------------------------------------

//
//        name: CollisionLine::CollisionLine
// description: Constructor
//
CollisionLine::CollisionLine() : CollisionVolume("ColLine") 
{
}

//
//        name: CollisionLine::Read\Write
// description: the read and write functions for the collision box class
//
bool CollisionLine::Read(ifstream& is)
{
	string token;

	if(!StartReadWithName(is))
		return false;
	while(ReadString(is, token))
	{
		if(!Line::ReadToken(is, token))
			return false;
		if(!CollisionVolume::ReadToken(is, token))
			return false;
	}
	return true;
}

bool CollisionLine::Write(ofstream& os) const
{
	StartWriteWithName(os, "ColLine");
	Line::WriteTokens(os);
	CollisionVolume::WriteTokens(os);
	EndWrite(os);

	return true;
}

// --- CollisionPlane -------------------------------------------------------------------------------------------------

//
//        name: CollisionPlane::CollisionPlane
// description: Constructor
//
CollisionPlane::CollisionPlane() : CollisionVolume("ColPlane") 
{
}

//
//        name: CollisionPlane::Read\Write
// description: the read and write functions for the collision box class
//
bool CollisionPlane::Read(ifstream& is)
{
	string token;

	if(!StartReadWithName(is))
		return false;
	while(ReadString(is, token))
	{
		if(token == "Position")
		{
			if(!ReadPoint3d(is, m_posn))
				return false;
		}
		else if(token == "Plane")
		{
			if(!ReadPoint2d(is, m_size))
				return false;
		}
		else if(token == "Plane")
		{
			if(!ReadString(is, token))
				return false;
			if(token == "XY")
				m_plane = XY;
			else if(token == "XZ")
				m_plane = XZ;
			else
				m_plane = YZ;
		}
		else
		{
			if(!CollisionVolume::ReadToken(is, token))
				return false;
		}
	}
	return true;
}

bool CollisionPlane::Write(ofstream& os) const
{
	StartWriteWithName(os, "ColPlane");

	WritePoint3d(os, "Position", m_posn);
	WritePoint2d(os, "Size", m_size);
	if(m_plane == XY)
		WriteString(os, "Plane", "XY");
	else if (m_plane == XZ)
		WriteString(os, "Plane", "XZ");
	else
		WriteString(os, "Plane", "YZ");
	CollisionVolume::WriteTokens(os);

	EndWrite(os);

	return true;
}

//
//        name: CollisionPlane::GetPosition etc
// description: Collsion plane data access functions
//
const Point3d& CollisionPlane::GetPosition() const 
{
	return m_posn;
}
void CollisionPlane::SetPosition(const Point3d& p) 
{
	m_posn = p;
}
const Point2d& CollisionPlane::GetSize() const 
{
	return m_size;
}
void CollisionPlane::SetSize(const Point2d& s) 
{
	m_size = s;
}
CollisionPlane::Plane CollisionPlane::GetPlane() const 
{
	return m_plane;
}
void CollisionPlane::SetPlane(CollisionPlane::Plane p) 
{
	m_plane = p;
}

// --- CollisionMesh -------------------------------------------------------------------------------------------------

//
//        name: CollisionMesh::CollisionMesh
// description: Constructor
//
CollisionMesh::CollisionMesh() : CollisionVolume("ColMesh") 
{
}

//
//        name: CollisionMesh::Read\Write
// description: the read and write functions for the collision box class
//
bool CollisionMesh::Read(ifstream& is)
{
	string token;

	if(!StartReadWithName(is))
		return false;
	while(ReadString(is, token))
	{
		if(token == "VertexArray")
		{
			if(!ReadVertices(is))
				return false;
		}
		else if(token == "FaceArray")
		{
			if(!ReadFaces(is))
				return false;
		}
		else if(token == "Position")
		{
			if(!ReadPoint3d(is, m_posn))
				return false;
		}
		else
		{
			CollisionVolume::ReadToken(is, token);
		}
	}
	return true;
}

bool CollisionMesh::Write(ofstream& os) const
{
	StartWriteWithName(os, "ColMesh");

	WritePoint3d(os, "Position", m_posn);
	WriteVertices(os);
	WriteFaces(os);
	CollisionVolume::WriteTokens(os);

	EndWrite(os);

	return true;
}

//
//        name: CollisionMesh::ReadFaces/ReadVertices/WriteFaces/WriteVertices
// description: Functions to read or write face and vertex arrays
//
bool CollisionMesh::ReadFaces(ifstream& is)
{
	string token;

	if(!StartRead(is))
		return false;
	while(ReadString(is, token))
	{
		if(token == "Face")
		{
			Face face;
			if(!ReadIntegerArray(is, &face[0], 3))
				return false;
			AddFace(face);
		}
		else if(token == "Number")
		{
			int32 number;
			if(!ReadInteger(is, number))
				return false;
			m_faceArray.reserve(number);
		}
	}
	return true;
}

bool CollisionMesh::ReadVertices(ifstream& is)
{	
	string token;

	if(!StartRead(is))
		return false;
	while(ReadString(is, token))
	{
		if(token == "Vertex")
		{
			Point3d vert;
			if(!ReadPoint3d(is, vert))
				return false;
			AddVertex(vert);
		}
		else if(token == "Number")
		{
			int32 number;
			if(!ReadInteger(is, number))
				return false;
			m_vertArray.reserve(number);
		}
	}
	return true;

}

bool CollisionMesh::WriteFaces(ofstream& os) const
{
	StartWrite(os, "FaceArray");

	WriteInteger(os, "Number", m_faceArray.size());
	for(uint32 i=0; i<m_faceArray.size(); i++)
		WriteIntegerArray(os, "Face", &m_faceArray[i][0], 3);

	EndWrite(os);
	return true;
}

bool CollisionMesh::WriteVertices(ofstream& os) const
{
	StartWrite(os, "VertexArray");

	WriteInteger(os, "Number", m_vertArray.size());
	for(uint32 i=0; i<m_vertArray.size(); i++)
		WritePoint3d(os, "Vertex", m_vertArray[i]);

	EndWrite(os);
	return true;
}

//
//        name: CollisionMesh::GetPosition
// description: Position access functions
//
const Point3d& CollisionMesh::GetPosition() const 
{
	return m_posn;
}
void CollisionMesh::SetPosition(const Point3d& p) 
{
	m_posn = p;
}

//
//        name: CollisionMesh::AddVertex
// description: Mesh access functions
//
void CollisionMesh::AddVertex(Point3d& vert) 
{
	m_vertArray.push_back(vert);
}
Point3d& CollisionMesh::GetVertex(int32 i) 
{
	return m_vertArray[i];
}
int32 CollisionMesh::GetNumVertices() 
{
	return m_vertArray.size();
}
void CollisionMesh::AddFace(CollisionMesh::Face& face) 
{
	m_faceArray.push_back(face);
}
CollisionMesh::Face& CollisionMesh::GetFace(int32 i) 
{
	return m_faceArray[i];
}
int32 CollisionMesh::GetNumFaces() 
{
	return m_faceArray.size();
}


// --- BoundingSphere -------------------------------------------------------------------------------------------------

//
//        name: BoundingSphere::BoundingSphere
// description: Constructor
//
BoundingSphere::BoundingSphere() : BaseObject("BoundSphere") 
{
}

//
//        name: BoundingSphere::Read\Write
// description: the read and write functions for the component bounding sphere class
//
bool BoundingSphere::Read(ifstream& is)
{
	string token;

	if(!StartRead(is))
		return false;
	while(ReadString(is, token))
	{
		if(!Sphere::ReadToken(is, token))
			return false;
	}
	return true;
}

bool BoundingSphere::Write(ofstream& os) const
{
	StartWrite(os, "BoundSphere");
	Sphere::WriteTokens(os);
	EndWrite(os);

	return true;
}

// --- BoundingBox -------------------------------------------------------------------------------------------------

//
//        name: BoundingBox::BoundingBox
// description: Constructor
//
BoundingBox::BoundingBox() : BaseObject("BoundBox") 
{
}

//
//        name: BoundingBox::Read\Write
// description: the read and write functions for the component bounding box class
//
bool BoundingBox::Read(ifstream& is)
{
	string token;

	if(!StartRead(is))
		return false;
	while(ReadString(is, token))
	{
		if(!Box::ReadToken(is, token))
			return false;
	}
	return true;
}

bool BoundingBox::Write(ofstream& os) const
{
	StartWrite(os, "BoundBox");
	Box::WriteTokens(os);
	EndWrite(os);

	return true;
}

// --- XRefObject ----------------------------------------------------------------------------------------------------

//
//        name: XRefObject::XRefObject
// description: Constructor
//
XRefObject::XRefObject() : Object("XRefObject"), m_filename("<NULL>"), m_objectName("<NULL>")
{
}

//
//        name: XRefObject::Read\Write
// description: the read and write functions for the xref object class
//
bool XRefObject::Read(ifstream& is)
{
	string token;

	if(!StartRead(is))
		return false;
	while(ReadString(is, token))
	{
		if(token == "Filename")
		{
			if(!ReadString(is, m_filename))
				return false;
		}
		else if(token == "ObjectName")
		{
			if(!ReadString(is, m_objectName))
				return false;
		}
		else
			SkipLineOrBlock(is);
	}
	return true;
}


bool XRefObject::Write(ofstream& os) const
{
	StartWrite(os, "XRefObject");
	WriteString(os, "Filename", m_filename);
	WriteString(os, "ObjectName", m_objectName);
	EndWrite(os);

	return true;
}


// --- AIConnection -----------------------------------------------------------------------------------------------

//
//        name: AIConnection::AIConnection
// description: Constructor
//
AIConnection::AIConnection() : BaseObject("AIConnection"), m_pConnectedComps(NULL), m_pAttributeList(NULL)
{
}

//
//        name: AIConnection::Read\Write
// description: the read and write functions for the AIConnection class
//
bool AIConnection::Read(ifstream& is)
{
	string token;

	if(!StartReadWithName(is))
		return false;
	while(ReadString(is, token))
	{
		if(token == "AILink")
		{
			AILink *pAILink = new AILink;
			if(!pAILink->Read(is))
				return false;
			AddLink(pAILink);
		}
		else if(token == "Attributes")
		{
			AsciiAttributeList *pAttributeList = new AsciiAttributeList;
			if(!pAttributeList->Read(is))
				return false;
			SetAttributeList(pAttributeList);
		}
		else if(token == "ConnectedComponents")
		{
			ConnectedComponentList *pComponentList = new ConnectedComponentList;
			if(!pComponentList->Read(is))
				return false;
			SetConnectedComponentList(pComponentList);
		}
		else
		{
			if(!Sphere::ReadToken(is, token))
				return false;
		}
	}
	return true;
}

bool AIConnection::Write(ofstream& os) const
{
	StartWriteWithName(os, "AIConnection");

	Sphere::WriteTokens(os);
	ObjectOwner::WriteTokens(os);

	for(int32 i=0; i<m_linkArray.GetSize(); i++)
		m_linkArray[i]->Write(os);

	EndWrite(os);

	return true;
}

//
//        name: AIConnection::AddLink
// description: Link list access functions
//
void AIConnection::AddLink(AILink *pLink) 
{
	m_linkArray.Add(pLink);
}
AILink *AIConnection::GetLink(int32 i) 
{
	return m_linkArray[i];
}
int32 AIConnection::GetNumLinks() const 
{
	return m_linkArray.GetSize();
}

// --- AILink -----------------------------------------------------------------------------------------------

//
//        name: AILink::AILink
// description: Constructor
//
AILink::AILink() : BaseObject("AILink"), m_pConnection(NULL), m_pConnectedComps(NULL)
{
}

//
//        name: AILink::Read\Write
// description: the read and write functions for the AILink class
//
bool AILink::Read(ifstream& is)
{
	string token;

	if(!StartRead(is))
		return false;
	while(ReadString(is, token))
	{
		if(token == "AIConnection")
		{
			if(!ReadPointer(is, reinterpret_cast<void**>(&m_pConnection)))
				return false;
		}
		else if(token == "Attributes")
		{
			AsciiAttributeList *pAttributeList = new AsciiAttributeList;
			if(!pAttributeList->Read(is))
				return false;
			SetAttributeList(pAttributeList);
		}
		else if(token == "ConnectedComponents")
		{
			ConnectedComponentList *pComponentList = new ConnectedComponentList;
			if(!pComponentList->Read(is))
				return false;
			SetConnectedComponentList(pComponentList);
		}
		else
			SkipLineOrBlock(is);
	}
	return true;
}

bool AILink::Write(ofstream& os) const
{
	StartWrite(os, "AILink");
	WritePointer(os, "AIConnection", m_pConnection);
	ObjectOwner::WriteTokens(os);
	EndWrite(os);

	return true;
}

//
//        name: AILink::GetConnection, SetConnection
// description: AILink connection access functions
//
AIConnection* AILink::GetConnection() 
{
	return m_pConnection;
}
void AILink::SetConnection(AIConnection* pConnection) 
{
	m_pConnection = pConnection;
}

//
//        name: AILink::SetConnection
// description: Request a pointer from a AIConnection name
//          in: pName = name ofthe AIConnection
//
void AILink::SetConnection(std::string& name)
{
	thePointerRedirector.AddRequest(name, reinterpret_cast<void**>(&m_pConnection));
}


// --- AIPath --------------------------------------------------------------------------------------

//
//        name: AIPath::AIPath
// description: Constructor
//
AIPath::AIPath() : BaseObject("AIPath"), m_pAttributeList(NULL)
{
}

//
//        name: AIPath::Read\Write
// description: the read and write functions for the AIPath class
//
bool AIPath::Read(ifstream& is)
{
	string token;

	if(!StartReadWithName(is))
		return false;
	while(ReadString(is, token))
	{
		if(token == "AIConnection")
		{
			if(!ReadString(is, token))
				return false;
			AddAIConnection(token);
		}
		else if(token == "Number")
		{
			int32 number;
			if(!ReadInteger(is, number))
				return false;
			m_aiConnections.reserve(number);
		}
		else if(token == "Attributes")
		{
			AsciiAttributeList *pAttributeList = new AsciiAttributeList;
			if(!pAttributeList->Read(is))
				return false;
			SetAttributeList(pAttributeList);
		}
	}
	return true;
}

bool AIPath::Write(ofstream& os) const
{
	if(m_aiConnections.size() != 0)
	{
		StartWriteWithName(os, "AIPath");
		WriteInteger(os, "Number", m_aiConnections.size());
		for(uint32 i=0; i<m_aiConnections.size(); i++)
		{
			WritePointer(os, "AIConnection", m_aiConnections[i]);
		}
		ObjectOwner::WriteTokens(os);
		EndWrite(os);
	}

	return true;
}

//
//        name: AIConnection::AddConnectedComponent,GetConnectedComponent, GetNumConnectedComponents
// description: Access functions to list of components connected to this AIConnection
//
void AIPath::AddAIConnection(AIConnection *pConnection)
{
	m_aiConnections.push_back(pConnection);
}
void AIPath::AddAIConnection(std::string& name)
{
	m_aiConnections.push_back(NULL);
	thePointerRedirector.AddRequest(name, reinterpret_cast<void**>(&(m_aiConnections.back())));
}
AIConnection* AIPath::GetAIConnection(int32 i)
{
	return m_aiConnections[i];
}
int32 AIPath::GetNumAIConnections() const
{
	return m_aiConnections.size();
}

//
//        name: AIPath::SetSize
// description: If you are using the AddConnectedComponent function with a string as a parameter. This function
//				has to be called first to avoid reallocation of the vector storing the component pointers
//          in: size = size of component vector required
//
void AIPath::SetSize(int32 size)
{
	m_aiConnections.reserve(size);
}


