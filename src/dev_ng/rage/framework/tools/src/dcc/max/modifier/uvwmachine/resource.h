//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by UVWmachine.rc
//
#define IDS_LIBDESCRIPTION              1
#define IDS_CATEGORY                    2
#define IDS_CLASS_NAME                  3
#define IDS_PARAMS                      4
#define IDS_VERSION                     5
#define IDD_PANEL                       101
#define IDD_VTX_PANEL                   101
#define IDD_FACE_PANEL                  102
#define IDB_FACESEL                     108
#define IDB_FACESEL_MASK                109
#define IDB_DMAMAN                      111
#define IDC_CLOSEBUTTON                 1000
#define IDC_DOSTUFF                     1000
#define IDC_FACE_TOOLBAR                1001
#define IDC_SELECTION_STATIC            1002
#define IDC_BYVERTEX_CHECK              1003
#define IDC_EDIT_STATIC                 1007
#define IDC_ROTATE_BUTTON               1008
#define IDC_ROTATE90_BUTTON             1009
#define IDC_FLIPU_BUTTON                1010
#define IDC_FLIPV_BUTTON                1011
#define IDC_EXPORT_TEXT_1               1012
#define IDC_UCOORD_EDIT                 1013
#define IDC_UCOORD_SPIN                 1014
#define IDC_UCOORD_LABEL                1015
#define IDC_VCOORD_EDIT                 1016
#define IDC_VCOORD_SPIN                 1017
#define IDC_VCOORD_LABEL                1018
#define IDC_MOUSE_STATIC                1019
#define IDC_VERSION                     1021
#define IDC_UCOORD_EDIT2                1023
#define IDC_UCOORD_SPIN2                1024
#define IDC_UCOORD_LABEL2               1025
#define IDC_VCOORD_EDIT2                1026
#define IDC_VCOORD_SPIN2                1027
#define IDC_VCOORD_LABEL2               1028
#define IDC_UCOORD_EDIT3                1029
#define IDC_UCOORD_SPIN3                1030
#define IDC_UCOORD_LABEL3               1031
#define IDC_VCOORD_EDIT3                1032
#define IDC_VCOORD_SPIN3                1033
#define IDC_VCOORD_LABEL3               1034
#define IDC_UCOORD_EDIT4                1035
#define IDC_UCOORD_SPIN4                1036
#define IDC_UCOORD_LABEL4               1037
#define IDC_VCOORD_EDIT4                1038
#define IDC_VCOORD_SPIN4                1039
#define IDC_VCOORD_LABEL4               1040
#define IDC_UORIGIN_EDIT                1041
#define IDC_UORIGIN_SPIN                1042
#define IDC_UORIGIN_LABEL               1043
#define IDC_VORIGIN_EDIT                1044
#define IDC_VORIGIN_SPIN                1045
#define IDC_VORIGIN_LABEL               1046
#define IDC_ORIGIN_STATIC               1047
#define IDC_MOUSEROTATE_BUTTON          1048
#define IDC_MOUSESCALE_BUTTON           1049
#define IDC_EXPLICIT_STATIC             1049
#define IDC_MOUSETRANSLATE_BUTTON       1050
#define IDC_MOUSETRANSLATEU_BUTTON      1051
#define IDC_MOUSETRANSLATEV_BUTTON      1052
#define IDC_DMAMAN_STATIC               1053
#define IDC_MOUSESCALEU_BUTTON          1054
#define IDC_MOUSESCALEV_BUTTON          1055
#define IDC_COLOR                       1456
#define IDC_EDIT                        1490
#define IDC_SPIN                        1496

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        110
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1050
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
