//
// filename:	XmlUtil.cpp
// description:	
//

// --- Include Files ------------------------------------------------------------

#include "XmlUtil.h"
#include "decomp.h"
// TinyXML headers
#include "tinyxml.h"

// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

// --- Globals ------------------------------------------------------------------

// --- Static Globals -----------------------------------------------------------

// --- Static class members -----------------------------------------------------

// --- Code ---------------------------------------------------------------------

namespace XmlUtil
{

TiXmlElement* 
CreateTransformNode( const char* name, const Point3& pos, const Quat& rot, const Point3& scale )
{
	Quat rot2 = rot;
	rot2.Normalize();
	TiXmlElement* pTransformElem = new TiXmlElement( name );
	TiXmlElement* pPositionElem = new TiXmlElement( "position" );
	pTransformElem->LinkEndChild( pPositionElem );

	char* dim[] = { "x", "y", "z", "w" };
	char di[64];
	for ( int d = 0; d < 3; ++d )
	{
		sprintf_s( di, 64, "%f", pos[d] );
		pPositionElem->SetAttribute( dim[d], di );
	}

	if ( !rot2.IsIdentity() )
	{
		TiXmlElement* pRotationElem = new TiXmlElement( "rotation" );
		pTransformElem->LinkEndChild( pRotationElem );

		for ( int d = 0; d < 4; ++d )
		{
			sprintf_s( di, 64, "%f", rot2[d] );
			pRotationElem->SetAttribute( dim[d], di );
		}
	}
	if ( !scale.Equals( Point3( 1.0f, 1.0f, 1.0f ), 0.001f ) )
	{
		TiXmlElement* pScaleElem = new TiXmlElement("scale");
		pTransformElem->LinkEndChild( pScaleElem );

		for ( int d = 0; d < 3; ++d )
		{
			sprintf_s( di, 64, "%f", rot2[d] );
			pScaleElem->SetAttribute( dim[d], di );
		}
	}

	return ( pTransformElem );
}

TiXmlElement* 
CreateTransformNode( const char* name, const Matrix3& matObject, const Matrix3& matNode )
{
	TiXmlElement* pTransformElem = new TiXmlElement( name );
	char* dim[] = { "x", "y", "z", "w" };
	char di[64];

	// Handle Object transformation
	if ( !matObject.IsIdentity() )
	{
		// Do position (and optionally rotation) export.
		// Which seems to be far quicker to load in Ruby.
		const Point3& posObject = matObject.GetTrans();
		// new: get scale off the matrix.
		Matrix3 noScaleMat(matObject);
		noScaleMat.NoScale();
		Quat rotObject( noScaleMat );
		rotObject.Normalize();
		Point3 scale( 1.0f, 1.0f, 1.0f );
		scale.x = matObject.GetRow( 0 ).Length( );
		scale.y = matObject.GetRow( 1 ).Length( );
		scale.z = matObject.GetRow( 2 ).Length( );

		TiXmlElement* pObjectElem = new TiXmlElement( "object" );
		pTransformElem->LinkEndChild( pObjectElem );
		TiXmlElement* pPositionElem = new TiXmlElement( "position" );
		pObjectElem->LinkEndChild( pPositionElem );

		for ( int d = 0; d < 3; ++d )
		{
			sprintf_s( di, 64, "%f", posObject[d] );
			pPositionElem->SetAttribute( dim[d], di );
		}

		if ( !rotObject.IsIdentity() )
		{
			TiXmlElement* pRotationElem = new TiXmlElement( "rotation" );
			pObjectElem->LinkEndChild( pRotationElem );

			for ( int d = 0; d < 4; ++d )
			{
				sprintf_s( di, 64, "%f", rotObject[d] );
				pRotationElem->SetAttribute( dim[d], di );
			}
		}
		if ( !scale.Equals( Point3( 1.0f, 1.0f, 1.0f ), 0.001f ) )
		{
			TiXmlElement* pScaleElem = new TiXmlElement( "scale" );
			pObjectElem->LinkEndChild( pScaleElem );

			for ( int d = 0; d < 3; ++d )
			{
				sprintf_s( di, 64, "%f", scale[d] );
				pScaleElem->SetAttribute( dim[d], di );
			}
		}
	}

	// Handle node transformation.
	if ( !matNode.IsIdentity() )
	{
		const Point3& posNode = matNode.GetTrans();
		// new: get scale off the matrix.
		Matrix3 noScaleMat(matNode);
		noScaleMat.NoScale();
		Quat rotNode( noScaleMat );
		rotNode.Normalize();
		Point3 scale( 1.0f, 1.0f, 1.0f );
		scale.x = matNode.GetRow( 0 ).Length( );
		scale.y = matNode.GetRow( 1 ).Length( );
		scale.z = matNode.GetRow( 2 ).Length( );

		TiXmlElement* pNodeElem = new TiXmlElement( "node" );
		pTransformElem->LinkEndChild( pNodeElem );
		TiXmlElement* pNodePositionElem = new TiXmlElement( "position" );
		pNodeElem->LinkEndChild( pNodePositionElem );
		for ( int d = 0; d < 3; ++d )
		{
			sprintf_s( di, 64, "%f", posNode[d] );
			pNodePositionElem->SetAttribute( dim[d], di );
		}

		if ( !rotNode.IsIdentity() )
		{
			TiXmlElement* pNodeRotationElem = new TiXmlElement( "rotation" );
			pNodeElem->LinkEndChild( pNodeRotationElem );

			for ( int d = 0; d < 4; ++d )
			{
				sprintf_s( di, 64, "%f", rotNode[d] );
				pNodeRotationElem->SetAttribute( dim[d], di );
			}
		}
		if ( !scale.Equals( Point3( 1.0f, 1.0f, 1.0f ), 0.001f ) )
		{
			TiXmlElement* pScaleElem = new TiXmlElement( "scale" );
			pNodeElem->LinkEndChild( pScaleElem );

			for ( int d = 0; d < 3; ++d )
			{
				sprintf_s( di, 64, "%f", scale[d] );
				pScaleElem->SetAttribute( dim[d], di );
			}
		}
	}

	return ( pTransformElem );
}

TiXmlElement* 
CreateRGBAColour( const Point3& rgb, float alpha )
{
	TiXmlElement* pColourElem = new TiXmlElement( "colour" );

	char* dim[] = { "r", "g", "b" };
	char di[64];
	for ( int d = 0; d < 3; ++d )
	{
		sprintf_s( di, 64, "%f", rgb[d] );
		pColourElem->SetAttribute( dim[d], di );
	}
	XmlUtil::SetAttr( pColourElem, "a", alpha );

	return ( pColourElem );
}
TiXmlElement* 
CreatePoint3Node( const Point3& point )
{
	TiXmlElement* pColourElem = new TiXmlElement( "point3" );

	char* dim[] = { "x", "y", "z" };
	char di[64];
	for ( int d = 0; d < 3; ++d )
	{
		sprintf_s( di, 64, "%f", point[d] );
		pColourElem->SetAttribute( dim[d], di );
	}

	return ( pColourElem );
}
TiXmlElement* 
CreatePoint4Node( const Point4& point )
{
	TiXmlElement* pColourElem = new TiXmlElement( "point4" );

	char* dim[] = { "x", "y", "z", "w" };
	char di[64];
	for ( int d = 0; d < 4; ++d )
	{
		sprintf_s( di, 64, "%f", point[d] );
		pColourElem->SetAttribute( dim[d], di );
	}

	return ( pColourElem );
}

void 
SetAttr( TiXmlElement* pElement, const std::string& attr, const float val )
{
	char valstr[512];
	sprintf_s( static_cast<char*>(valstr), 512, "%f", val );
	pElement->SetAttribute( attr.c_str(), valstr );
}

void 
SetAttr( TiXmlElement* pElement, const std::string& attr, const int val )
{
	pElement->SetAttribute( attr.c_str(), val );
}

void 
SetAttr( TiXmlElement* pElement, const std::string& attr, const std::string& val )
{
	pElement->SetAttribute( attr.c_str(), val.c_str() );
}

std::string
EscapeStr( const std::string& input )
{
	std::string output;
	for ( std::string::const_iterator itInput = input.begin();
		  itInput != input.end();
		  ++itInput )
	{
		char c = (*itInput);
		switch ( c )
		{
		case '<':
			output.append( "&lt;" ); 
			break;
		case '>':
			output.append( "&gt;" ); 
			break;
		case '&':
			output.append( "&amp;" ); 
			break;
		case '"':
			output.append( "&quot;" ); 
			break;
		case '\'':
			output.append( "&apos;" ); 
			break;
		default:
			// Only include if its in the normal printable range.
			if ( c >= 32 && c <= 126 )
				output.append( 1, c );
			break;
		}
	}

	return ( output );
}

} // XmlUtil namespace
