//
//
//    Filename: StuntJumpNode.h
//     Creator: Greg Smith
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Single box object within a stunt jump
//
//

#ifndef INC_LADDERNODE__H
#define INC_LADDERNODE__H

// Max SDK headers
#include <Max.h>
#include <istdplug.h>
#include <iparamm2.h>
// resources header
//#include "resource.h"
// My headers
#include "Iladder.h"

extern HINSTANCE hInstance;

TCHAR *GetString(int id);

// --- Defines ------------------------------------------------------------------
#define LADDER_NODE_LEVEL	(1)

// --- Structure/Class Definitions ----------------------------------------------

//
//   Class Name: ladderNode
// Base Classes: HelperObject
//  Description:
//    Functions:
//
//
class ladderNode : public HelperObject
{
private:
	IParamBlock2 *m_pBlock2;
	TriObject* m_pTriObject;
	Point3 m_pntDrawCol;

public:
	//Constructor/Destructor
	ladderNode();
	~ladderNode();

	void setDrawCol(const Point3& pntCol) { m_pntDrawCol = pntCol; }

	// Animatable methods
	int NumParamBlocks() {return 1;}
	IParamBlock2* GetParamBlock(int i) {assert(i == 0); return m_pBlock2;}
	IParamBlock2* GetParamBlockByID(short id) {assert(id == laddernode_params); return m_pBlock2;}
	Class_ID ClassID() {return LADDERNODE_CLASS_ID;}
	void GetClassName(TSTR& s) {s = _T("ladderNode");}
	void BeginEditParams( IObjParam  *ip, ULONG flags,Animatable *prev);
	void EndEditParams( IObjParam *ip, ULONG flags,Animatable *next);
	void DeleteThis() {delete this;}

	// ReferenceTarget methods
#if MAX_VERSION_MAJOR >= 9 
	ReferenceTarget* Clone(RemapDir &remap = DefaultRemapDir());
#else
	ReferenceTarget* Clone(RemapDir &remap = NoRemap());
#endif

	IOResult Load(ILoad *iload);
	IOResult Save(ISave *isave);
	int NumRefs();
	RefTargetHandle GetReference(int i);
	void SetReference(int i, RefTargetHandle pTarg);
	RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,  RefMessage message);

	//From BaseObject
	CreateMouseCallBack* GetCreateMouseCallBack();
	void GetLocalBoundBox(TimeValue t, INode* pNode, ViewExp* pVp, Box3& box);
	void GetWorldBoundBox(TimeValue t, INode* pNode, ViewExp* pVp, Box3& box);
	int Display(TimeValue t, INode* pNode, ViewExp *pVpt, int flags);
	int HitTest(TimeValue t, INode* pNode, int type, int crossing, int flags, IPoint2 *pSp, ViewExp *pVpt);
	TCHAR *GetObjectName() { return GetString(IDS_LADDER_NODE); }
	int GetSubObjectLevel( ) { return ( LADDER_NODE_LEVEL ); } 

	void BuildMesh(TimeValue t);
	void DrawMesh(GraphicsWindow *gw, TimeValue t, Matrix3& objMat);
	void addBox(Point3 pntCentre,Point3 pntDimension,int& iVertOffset,int& iFaceOffset);

	// From Object
	ObjectState Eval(TimeValue t);
	Interval ObjectValidity(TimeValue t);
	void InitNodeName(TSTR &s) { s = TSTR(GetString(IDS_LADDER_NODE)); }
	BOOL IsWorldSpaceObject() {return TRUE;}
};


//
//   Class Name: ObjPathNodeClassDesc
// Base Classes: ClassDesc
//  Description:
//    Functions:
//
//
class ladderNodeClassDesc : public ClassDesc2
{

public:
	int IsPublic() {return (FALSE);}
	void* Create(BOOL loading = FALSE);
	const TCHAR *ClassName() {return GetString(IDS_LADDER_NODE);}
	SClass_ID SuperClassID() {return HELPER_CLASS_ID;}
	Class_ID ClassID() {return LADDERNODE_CLASS_ID;}
	const TCHAR* Category() {return _T("Gta Helpers");}
	void ResetClassParams (BOOL fileReset);

	// Hardwired name, used by MAX Script as unique identifier
	const TCHAR*	InternalName() { return GetString(IDS_LADDER_NODE); }
	HINSTANCE		HInstance()	{ return hInstance; }
};


#endif // INC_LADDERNODE__H