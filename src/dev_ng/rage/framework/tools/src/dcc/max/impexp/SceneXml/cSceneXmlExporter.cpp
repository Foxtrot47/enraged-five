//
// filename:	cSceneXmlExporter.cpp
// description:
//

// --- Include Files ------------------------------------------------------------

// STL headers
#include <algorithm>
#include <vector>

// Max headers
#include "max.h"
#include "stdmat.h"
#include "iparamb2.h"

#if MAX_RELEASE > MAX_RELEASE_R13
#include "maxscript/maxscript.h"
#include <INamedSelectionSetManager.h>
#include "maxscript/mxsplugin//mxsplugin.h"
#else
#include "maxscrpt/maxscrpt.h"
#include "maxscrpt/msplugin.h"
#endif

#include "CS/delegexp.h"
#include "XRef/iXrefObj.h"
#include "IGame/IGameModifier.h"
#include <IGame/IGameProperty.h>

// TinyXML headers
#include "tinyxml.h"

// Other plugin headers
#include "Attribute.h"
#include "IMaxDataStore.h"
#include "IFpMaxDataStore.h"
#include "rageMaxCollisionVolumes/IFpCollisionVolumes.h"
#include "DataInstance.h"
#include "objects/Scrollbars/Scrollbars.h"
#include "objects/InternalRef/InternalRef.h"
#include "HierarchyInterface.h"
#include "INamedSelectionSetManager.h"
#include"ISTDPLUG.H"
#include "animtbl.h"
#include "rageMaxPlatformAttributes/IFpMaxPlatformAttributes.h"

// Utility lib headers
#include "Filesystem/Path.h"
#include "Win32HelperLib/stringutil.h"
#include "Win32HelperLib/Win32Res.h"
#include "MaxUtil/MaxUtil.h"
#include "rexMaxUtility/IRsExportTexture.h"

// SceneXml headers
#include "cMaterial.h"
#include "cSceneXmlExporter.h"
#include "cSceneXmlConfig.h"
#include "cSceneXmlUtil.h"
#include "XmlUtil.h"
#include "resource.h"

// --- Defines ------------------------------------------------------------------

#define VERSION  ( 130 )
#define fEPSILON (0.001f)
#define STACK_BUFFER_SIZE (512)

#ifdef MAX8
#include <xref/iXrefObj.h>
#define XREFOBJ_CLASS_ID_PART 0x92aab38c
#else
#define XREFOBJ_CLASS_ID_PART XREFOBJ_CLASS_ID
#endif

#define RAGE_LGHT_CLASSID Class_ID(0x4ff87de6, 0x1b693928)
typedef std::map<std::string, std::string> LightPropMap;

const LightPropMap::value_type rawData[] = {
// 	LightPropMap::value_type("lightColour", "Colour"),
	LightPropMap::value_type("lightType", "Type"),
// 	LightPropMap::value_type("lightIntensity", "Intensity"),
// 	LightPropMap::value_type("lightAttenEnd", "Attenuation End"),
	LightPropMap::value_type("lightWidthAttenEnd", "Width Attenuation End"),
// 	LightPropMap::value_type("lightHotspot", "Hotspot"),
// 	LightPropMap::value_type("lightFalloff", "Fallsize"),
	LightPropMap::value_type("lightMeshResolution", "Viewport Resolution"),
	LightPropMap::value_type("lightMeshSize", "Viewport Mesh Size"),
	LightPropMap::value_type("lightMeshColour", "Viewport Mesh Colour"),
	LightPropMap::value_type("lightIsSquare", "Is Square"),
	LightPropMap::value_type("lightExpFalloff", "Exponential Falloff"),
	LightPropMap::value_type("lightUseCullPlane", "Use Cullplane"),
	LightPropMap::value_type("lightCullPlane", "Cullplane"),
	LightPropMap::value_type("lightUseOuterColour", "Use Volume Outer Colour"),
	LightPropMap::value_type("lightOuterColour", "Volume Outer Colour")
};
const int numElems = sizeof rawData / sizeof rawData[0];
LightPropMap lightPropSceneXMLMap = LightPropMap(rawData, rawData + numElems);

#define VISIBILITY_TRACK_REFNUM 4

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

// --- Globals ------------------------------------------------------------------

DWORD WINAPI progress_fn( LPVOID arg );
extern HINSTANCE g_hInstance;

MyErrorProc *pErrorProc;

// --- Static Globals -----------------------------------------------------------

// --- Static class members -----------------------------------------------------

// --- Code ---------------------------------------------------------------------

DWORD WINAPI
progress_fn( LPVOID arg )
{
	return ( 0 );
}

cSceneXmlExporter::cSceneXmlExporter( ) : m_pULog(NULL)
{
	m_iGameScene = GetIGameInterface();
	m_iGameScene->InitialiseIGame();

	pErrorProc = new MyErrorProc(this);
	SetErrorCallBack(pErrorProc);
}

cSceneXmlExporter::~cSceneXmlExporter( )
{
}

unsigned int
cSceneXmlExporter::Version( ) const
{
	return ( VERSION );
}

const TCHAR *
cSceneXmlExporter::GetNodeGuidString(INodePtr pNode)
{
	IRsGuidMgrInterface* pGuidMgr = GetRsGuidInterface();
	if(pGuidMgr)
	{
		return pGuidMgr->GetGuid(pNode);
	}

	// Create a GUID for this INode if we haven't already created one.
	if ( m_mObjects.end() == m_mObjects.find( pNode->GetHandle() ) )
	{
		m_mObjects[pNode->GetHandle()] = cGuid::Create( );
	}
	return m_mObjects[pNode->GetHandle()].ToString().c_str();
}

void cSceneXmlExporter::ExportStats(TiXmlElement* pElement)
{
	TiXmlElement* pStatsElem = new TiXmlElement( "stats" );
	pElement->LinkEndChild( pStatsElem );

	TiXmlElement* pGeosElem = new TiXmlElement( "geometries" );
	pStatsElem->LinkEndChild( pGeosElem );

	TiXmlElement* pCollisionElements = new TiXmlElement( "collision_geometry" );
	pStatsElem->LinkEndChild( pCollisionElements );

	tObjectStatsListIterator begin = m_mObjectStatsList.begin();
	tObjectStatsListIterator end = m_mObjectStatsList.end();

	while(begin != end)
	{
		INode *pNode = GetCOREInterface()->GetINodeByHandle(begin->first);

		if (begin->second.IsCollision == true)
		{
			TiXmlElement* pCollisionElement = new TiXmlElement( "geometry" );

			Point3 width = begin->second.Bounds.Width();
			float cubicDim = (width.x * width.y * width.z);
			float PolyDensity = 0;
			if(cubicDim>0.1)
				PolyDensity = (float)begin->second.PolyCount / cubicDim;

			char buffer[1024];

			pCollisionElement->SetAttribute( "guid", GetNodeGuidString(pNode) );
			pCollisionElement->SetAttribute( "name", begin->second.Name.c_str() );

			sprintf(buffer,"%d",begin->second.PolyCount);
			pCollisionElement->SetAttribute( "polycount", buffer );

			sprintf(buffer,"%f",PolyDensity);
			pCollisionElement->SetAttribute( "polydensity", buffer );

			pCollisionElements->LinkEndChild( pCollisionElement );
		}
		else
		{
			TiXmlElement* pGeoElem = new TiXmlElement( "geometry" );

			Point3 width = begin->second.Bounds.Width();
			float cubicDim = (width.x * width.y * width.z);
			float PolyDensity = 0;
			if(cubicDim>0.1)
				PolyDensity = (float)begin->second.PolyCount / cubicDim;

			char buffer[1024];

			pGeoElem->SetAttribute( "guid", GetNodeGuidString(pNode) );
			pGeoElem->SetAttribute( "name", begin->second.Name.c_str() );

			sprintf(buffer,"%d",begin->second.PolyCount);
			pGeoElem->SetAttribute( "polycount", buffer );

			sprintf(buffer,"%f",PolyDensity);
			pGeoElem->SetAttribute( "polydensity", buffer );

			pGeosElem->LinkEndChild( pGeoElem );
		}

		begin++;
	}
}

int
cSceneXmlExporter::DoExport( const std::string& filename, Interface* i, const std::vector<INode*>& vNodes )
{
	if(m_pULog)
		m_pULog->LogMessage(filename.data());

	if ( (!g_SceneXmlConfig.HasLoaded()) || g_SceneXmlConfig.LoadFailed() )
	{
		const char *msg = "SceneXml Configuration is not loaded.  Contact tools.";
		if(m_pULog)
			m_pULog->LogError(msg);
		MessageBox( NULL, msg, "SceneXml Error", SW_NORMAL );
		return (IMPEXP_FAIL);
	}

	try
	{
		PrecacheSelectionSets();

		TiXmlDocument doc;
		TiXmlElement* pRootElem = ExportRoot( &doc, i );
		TiXmlElement* pObjectsElem = new TiXmlElement( "objects" );
		pRootElem->LinkEndChild( pObjectsElem );

		// Start progress bar
		LPVOID arg = NULL;
		i->ProgressStart( _T("SceneXml Export: Objects"), TRUE, progress_fn, arg );

		// Iterate through all scene nodes
		char log_file[MAX_PATH];
		MaxSDK::Util::Path output_filename( filename.c_str() );
		MaxSDK::Util::Path& log_file_dir = output_filename.RemoveLeaf( );
		sprintf_s( log_file, MAX_PATH, "%s/scenexml.log", log_file_dir.GetCStr() );
		FILE* fp = fopen( log_file, "w+" );
		if ( !fp )
			mprintf( "Failed to create SceneXml log file: %s.  Logging disabled.\n", log_file );

		tObjectStatsListIterator begin = m_mObjectStatsList.begin();

		for ( std::vector<INode*>::const_iterator itNode = vNodes.begin();
			  itNode != vNodes.end();
			  ++itNode )
		{
			TiXmlElement* pNodeElem = NULL;
			if ( (*itNode) == i->GetRootNode() )
			{
				int nChildren = (*itNode)->NumberOfChildren();
				for ( int idx = 0; idx < nChildren; ++idx )
				{
					int percent = static_cast<int>( 100.0f * ( idx / static_cast<float>( nChildren ) ) );
					i->ProgressUpdate( percent );

					INode* pNode = (*itNode)->GetChildNode( idx );
					if ( !pNode )
						continue;

					MCHAR* pName = pNode->GetName( );
					char msg[4096];
					sprintf(msg, "Exporting object %s (%d of %d)\n", const_cast<char*>( pName ), idx, nChildren );
					if(m_pULog)
						m_pULog->LogMessage(msg, pName);
					else
						mprintf(msg);

					TiXmlElement* pChildNodeElem = ExportNode( fp, pNode, i );

					if ( NULL != pChildNodeElem )
						pNodeElem->LinkEndChild( pChildNodeElem );
				}
			}
			else
			{
				pNodeElem = ExportNode( fp, *itNode, i );
				if(pNodeElem){
					pObjectsElem->LinkEndChild( pNodeElem );
				}
			}
		}

		ExportStats(pRootElem);

		// End progress bar
		i->ProgressEnd( );

		if ( fp )
			fclose( fp );

		// As we iterated over all scene nodes above we collected a material list.
		// Now we should export this material list.
		ExportMaterialList( pRootElem );

		bool saveSuccess = doc.SaveFile( filename.c_str() );
		if(m_pULog)
		{
			if(!saveSuccess)
			{
				char msg[4096];
				sprintf_s(msg, "Scene XML could not be saved to %s! Check read-only flag.", filename.data());
				m_pULog->LogError(msg);
			}
			else
				m_pULog->LogMessage("Scene XML export successfully.");
		}
		doc.Clear( );
	}
	catch ( ... )
	{
		return ( IMPEXP_FAIL );
	}

	return ( IMPEXP_SUCCESS );
}

// --- Private Methods ----------------------------------------------------------

/**
 *
 */
TiXmlElement*
cSceneXmlExporter::ExportRoot( TiXmlDocument* pDocument, Interface* pInterface )
{
	assert( pInterface );
	assert( pDocument );

	char version[STACK_BUFFER_SIZE];
	char user[STACK_BUFFER_SIZE];
	char machine[STACK_BUFFER_SIZE];
	char timestamp[STACK_BUFFER_SIZE];
	time_t ltime;
	struct tm today;

	ZeroMemory( version, STACK_BUFFER_SIZE );
	ZeroMemory( user, STACK_BUFFER_SIZE );
	ZeroMemory( machine, STACK_BUFFER_SIZE );
	ZeroMemory( timestamp, STACK_BUFFER_SIZE );

	MSTR& maxfile = pInterface->GetCurFilePath( );
	std::string filename( maxfile.data() );
	Filesystem::cPath::normalise( filename );

	sprintf_s( version, "%.2f", this->Version() / 100.0f );
	DWORD user_size = STACK_BUFFER_SIZE;
	GetUserName( user, &user_size );
	GetComputerName( machine, &user_size );
	time( &ltime );
	_localtime64_s( &today, &ltime );
	strftime( timestamp, STACK_BUFFER_SIZE, "%A, %d %B %Y %H:%M:%S", &today );

	TiXmlDeclaration* pDecl = new TiXmlDeclaration( "1.0", "iso-8859-1", "" );
	TiXmlElement* pRootElem = new TiXmlElement( "scene" );
	pRootElem->SetAttribute( "version", version );
	pRootElem->SetAttribute( "filename", filename.c_str() );
	pRootElem->SetAttribute( "timestamp", timestamp );
	if ( is_printable( user ) )
		pRootElem->SetAttribute( "user", user );
	else
		pRootElem->SetAttribute( "user", "" );

	pDocument->LinkEndChild( pDecl );
	pDocument->LinkEndChild( pRootElem );

	return ( pRootElem );
}

bool cSceneXmlExporter::Validate(INodePtr pNode)
{
	Modifier *pMod = MaxUtil::GetModifier(pNode, SKIN_CLASSID);
	if(pMod)
	{
		ISkin *pSkin = (ISkin*)pMod->GetInterface(I_SKIN);
		if(pSkin && pSkin->GetNumBones()==0)
		{
			char msg[MAX_PATH];
			sprintf_s(msg, MAX_PATH, "%s has a skin modifier with 0 bones in it. Exported SceneXML node will be missing animation attrubutes.", pNode->GetName());
			if(m_pULog)
				m_pULog->LogWarning(msg, pNode->GetName());
			return false;
		}
	}
	return true;
}

/**
 * @brief Export main node properties.
 */
TiXmlElement*
cSceneXmlExporter::ExportNode( FILE* fpLog, INode* pNode, Interface* pInterface )
{
	MCHAR* pName = pNode->GetName( );
	if ( fpLog )
	{
		fprintf( fpLog, "Exporting Object: %s\n", pName );
		fflush( fpLog );
	}

	Object* pObject = pNode->GetObjectRef( );
	if ( !pObject )
		return ( NULL );
	// In order to avoid modifiers in GetObjectRef...
	if ( HasAttributeClass( pNode, "Gta Object" ) && !IsXref( pNode ) )
		pObject = MaxUtil::GetBaseObject( pNode );
	if ( !pObject )
		return ( NULL );

	Interface* ip = GetCOREInterface( );

	// Get classname
	MCHAR* classname = pObject->GetObjectName();
	if(pNode->IsGroupHead())
		classname = _T("Max_Group_Head");
	std::string sClassName = MakeClassNameFriendly( classname );

	// Get superclass name
	SClass_ID sid = pObject->SuperClassID( );
	const std::string* pSuperClass = g_SceneXmlConfig.RetSuperclassName( sid );
	std::string sSuperClass = std::string( pSuperClass ? (*pSuperClass) : "" );

	// See if we have an exporter definition for this class or superclass,
	// if so adhere to its XML export specification.
	const tNodeDescription* pClassNodeDesc = g_SceneXmlConfig.RetNodeDescForClass( sClassName );
	const tNodeDescription* pSuperNodeDesc = g_SceneXmlConfig.RetNodeDescForSuperClass( sSuperClass );

	FPInterfaceDesc* pDesc = pNode->GetDesc();
	TiXmlElement* pNodeElem = NULL;
	if ( pClassNodeDesc )
	{
		pNodeElem = new TiXmlElement( g_SceneXmlConfig.RetNodeExportAs().c_str() );
		pNodeElem->SetAttribute( "guid", GetNodeGuidString(pNode) );
		ExportProperties( pNode, pClassNodeDesc, pNodeElem, pInterface );

		if ( is_printable( sClassName ) )
			pNodeElem->SetAttribute( "class", sClassName.c_str() );
		else
			pNodeElem->SetAttribute( "class", pClassNodeDesc->sClassName.c_str() );

		if ( is_printable( sSuperClass ) )
			pNodeElem->SetAttribute( "superclass", sSuperClass.c_str() );
	}
	else if ( pSuperNodeDesc )
	{
		pNodeElem = new TiXmlElement( g_SceneXmlConfig.RetNodeExportAs().c_str() );
		pNodeElem->SetAttribute( "guid", GetNodeGuidString(pNode) );
		ExportProperties( pNode, pSuperNodeDesc, pNodeElem, pInterface );

		if ( is_printable( sClassName ) )
			pNodeElem->SetAttribute( "class", sClassName.c_str() );
		else
			pNodeElem->SetAttribute( "class", pSuperNodeDesc->sClassName.c_str() );

		if ( is_printable( sSuperClass ) )
			pNodeElem->SetAttribute( "superclass", sSuperClass.c_str() );
	}
	else
	{
		if(m_pULog)
			m_pULog->LogMessage("No class or superclass configured in SceenXML.xml for object.", pName);
		pNodeElem = new TiXmlElement( "unknown" );
		pNodeElem->SetAttribute( "guid", GetNodeGuidString(pNode) );
		ExportNameProperty( pNode, pNodeElem );
		ExportAttributesProperty( pNode, pNodeElem );
		ExportParamBlocksProperty( pNode, pNodeElem );
		ExportTransformProperty( pNode, pNodeElem );
		if ( is_printable( sClassName ) )
			pNodeElem->SetAttribute( "class", sClassName.c_str() );
		if ( is_printable( sSuperClass ) )
			pNodeElem->SetAttribute( "superclass", sSuperClass.c_str() );
	}

	// Recurse through children link new XML element (if available)
	if ( NULL != pNodeElem )
	{
		// Do children next
		TiXmlElement* pNodeChildrenElem = NULL;
		int nChildren = pNode->NumberOfChildren();
		if ( nChildren > 0 )
			pNodeChildrenElem = new TiXmlElement( "children" );

		for ( int idx = 0; idx < nChildren; ++idx )
		{
			INode* pChildNode = pNode->GetChildNode( idx );
			TiXmlElement* pNodeChildElem = ExportNode( fpLog, pChildNode, pInterface );

			if ( NULL != pNodeChildElem )
				pNodeChildrenElem->LinkEndChild( pNodeChildElem );
		}

		if ( NULL != pNodeChildrenElem )
			pNodeElem->LinkEndChild( pNodeChildrenElem );
	}

	return ( pNodeElem );
}

/**
 * @brief Exports details of the subobjects of the specified object.
 */
TiXmlElement*
cSceneXmlExporter::ExportSubObjects( INode* pNode )
{
	MCHAR* pName = pNode->GetName( );
	BaseObject* pBaseObject = pNode->GetObjectRef( );
	if ( !pBaseObject )
		return ( NULL );
	int nNumSubsForReal = pBaseObject->NumSubs( );
	int nNumSubs = pBaseObject->NumSubObjTypes( );
	if ( 0 == nNumSubs )
		return ( NULL );

	// Select all sub-objects first
	Quat quat_identity;
	quat_identity.Identity( );

	try
	{
		TiXmlElement* pSubObjectsElem = new TiXmlElement( "subobjects" );
		cSubObjectCallback callback;
		for ( int idx = 1; idx <= nNumSubs; ++idx )
		{
			// Activate sub-objects and select them all.
			XFormModes m;
			//pBaseObject->ActivateSubobjSel( idx, m );
			pBaseObject->SelectAll( idx );

			pBaseObject->GetSubObjectTMs( &callback, 0, pNode, NULL );

			// Iterate through our collected points writing them.
			cSubObjectCallback::Matrix3CollectionConstIter it = callback.BeginTMs();
			int nSubObjIdx = 0;
			for ( ; it != callback.EndTMs(); ++it )
			{
				char sName[512];
				sprintf_s( sName, "subobject%d", nSubObjIdx );
				Matrix3 identity( 1 );

				TiXmlElement* pSubObjElem = new TiXmlElement( "object" );
				pSubObjElem->SetAttribute( "name", sName );
				pSubObjElem->SetAttribute( "type", callback.Type() );
				pSubObjElem->LinkEndChild( XmlUtil::CreateTransformNode( "transform", *it, identity ) );
				pSubObjectsElem->LinkEndChild( pSubObjElem );
				++nSubObjIdx;
			}
		}

		return ( pSubObjectsElem );
	}
	catch ( ... )
	{
		return ( NULL );
	}
}

/**
 *
 */
void
cSceneXmlExporter::ExportProperties( INode* pNode, const tNodeDescription* pNodeDesc, TiXmlElement* pElement, Interface* pInterface )
{
	assert( pNode );
	assert( pNodeDesc );
	assert( pElement );
	assert( pInterface );

	TiXmlElement* pCustomElem = new TiXmlElement( "custom_attrs" );

	// Loop through our defined properties adding them to our
	// XML element as attributes, except any attributes.
	std::vector<tNodeProperty>::const_iterator itProperty = pNodeDesc->vProperties.begin();
	for ( itProperty; itProperty != pNodeDesc->vProperties.end(); ++itProperty )
	{
		// Process Special Properties
		if ( "transform" == itProperty->sName )
		{
			ExportTransformProperty( pNode, pElement );
		}
		else if ( "localtransform" == itProperty->sName )
		{
			ExportLocalTransformProperty( pNode, pElement );
		}
		else if ( "attributes" == itProperty->sName )
		{
			ExportAttributesProperty( pNode, pElement );
		}
		else if ("platformAttributes" == itProperty->sName)
		{
			ExportPlatformAttributesProperty(pNode, pElement);
		}
		else if ( "paramblocks" == itProperty->sName )
		{
			ExportParamBlocksProperty( pNode, pElement );
		}
		else if ( "refTextures" == itProperty->sName )
		{
			ExportRefTexturesProperty( pNode, pElement );
		}
		else if ( "name" == itProperty->sName )
		{
			ExportNameProperty( pNode, pElement );
		}
		else if ( "materials" == itProperty->sName )
		{
			PopulateMaterialsList( pNode, pElement, pInterface );
		}
		else if ( "boundingbox" == itProperty->sName )
		{
			ExportBoundingBoxProperty( pNode, pElement );
		}
		else if ( "subobjects" == itProperty->sName )
		{
			// Subobjects for helpers
			TiXmlElement* pSubObjectsElem = ExportSubObjects( pNode );
			if ( NULL != pSubObjectsElem )
				pElement->LinkEndChild( pSubObjectsElem );
		}
		else if ( "lodhierarchy" == itProperty->sName)
		{
			ExportLodHierarchy( pNode, pElement );
		}
		else if ( "references" == itProperty->sName )
		{
			ExportReferences( pNode, pElement );
		}
		else if ( "polycount" == itProperty->sName )
		{
			ExportPolycount( pNode, pElement );
		}
		// new animation attributes
		else if ( "animation_props" == itProperty->sName )
		{
			ExportAnimationProps( pNode, &(*itProperty), pElement );
		}
		else if ( "user_properties" == itProperty->sName )
		{
			ExportUserProperties( pNode, pElement );
		}
		else
		{
			ExportUserProperty( pNode, pNodeDesc, &(*itProperty), pElement );
		}
	}
	pElement->LinkEndChild( pCustomElem );
}

/************************************************************************/
// Hierachichally loook through children to find all keyframes
/************************************************************************/

void CheckLastKeyFrameTime(Control *c, int *pLastFrame)
{
	int &lastFrame = *pLastFrame;
	IKeyControl *ikeys = GetKeyControlInterface(c);
	//		With this controller interface you can use its methods to get information about the keys.
	int num =0;
	if(ikeys && (num = ikeys->GetNumKeys())>0)
	{
		int frame = MaxUtil::GetKeyFrameTime(c, num-1);
		if(frame>lastFrame)
			lastFrame = frame;
	}
	for(int i=0; i<c->NumSubs(); i++)
	{
		Control *subControl = dynamic_cast<Control*>(c->SubAnim(i));
		if(subControl)
			CheckLastKeyFrameTime(subControl, pLastFrame);
	}
}

void LookForLongestAnimationInHierarchy(IGameNode *root, int *pLastFrame)
{
	DebugPrint("Node name: %s\n", root->GetName());
	IGameKeyTab iGameKeyTab;
	Control *controller = root->GetMaxNode()->GetTMController();
	if(controller)
	{
		CheckLastKeyFrameTime(controller->GetPositionController(), pLastFrame);
		CheckLastKeyFrameTime(controller->GetRotationController(), pLastFrame);
	}
	for(int i=0; i<root->GetChildCount(); i++)
	{
		LookForLongestAnimationInHierarchy(root->GetNodeChild(i), pLastFrame);
	}
}

/**
* Exports the user defined properties for the given node
*/
void
cSceneXmlExporter::ExportUserProperties( INode* pNode, TiXmlElement* pElement )
{
	if ( pNode == NULL || pElement == NULL )
		return;

	TiXmlElement* pPropertiesElem = new TiXmlElement( "user_properties" );
	pElement->LinkEndChild( pPropertiesElem );

	MSTR userPropertiesBuffer;
	pNode->GetUserPropBuffer( userPropertiesBuffer );

	if ( userPropertiesBuffer.data() == NULL )
		return;

	std::vector<char*> userProperties;
	char* pch;
	pch = strtok( userPropertiesBuffer.data(), "\r\n" );
	while (pch != NULL)
	{
		userProperties.push_back( pch );
		pch = strtok( NULL, "\r\n" );
	}

	std::vector<char*>::iterator it;
	for ( it = userProperties.begin() ; it < userProperties.end(); it++ )
	{
		if ( *it != NULL)
		{
			std::vector<std::string> propertyComponents;
			char* propertyPch;
			propertyPch = strtok( *it, "=" );
			while (propertyPch != NULL)
			{
				propertyComponents.push_back( std::string( propertyPch ) );
				propertyPch = strtok( NULL, "=" );
			}

			if ( ( int )propertyComponents.size() >= 2 )
			{
				TiXmlElement* pPropertyElem = new TiXmlElement( "property" );
				pPropertiesElem->LinkEndChild( pPropertyElem );
				XmlUtil::SetAttr( pPropertyElem, "name", propertyComponents[0] );
				XmlUtil::SetAttr( pPropertyElem, "value", propertyComponents[1] );
				XmlUtil::SetAttr( pPropertyElem, "type", "string" );
			}
		}
	}
}


/**
 *
 */
void
cSceneXmlExporter::ExportAnimationProps( INode* pNode, const tNodeProperty* pNodeDesc, TiXmlElement* pElement )
{
	if	(!IsXref( pNode ) &&
		 (HasAttributeClass( pNode, "Gta Object" ) || HasAttributeClass( pNode, "RAGE Particle" )) &&
		 Validate(pNode)		
		)
	{
		TiXmlElement* pAnimElem = new TiXmlElement( "anim_attrs" );
		pElement->LinkEndChild( pAnimElem );

		IGameNode * pIGameNode =  m_iGameScene->GetIGameNode (pNode) ;
		IGameNode *rootBone = pIGameNode;

		IFpMaxDataStore *pDataStore = GetMaxDataStoreInterface();
		if(pIGameNode)
		{
			IGameObject *pIGameObj = pIGameNode->GetIGameObject();

			if(0==strcmp(pDataStore->GetAttrClass(pNode).data(),"Gta Object"))
			{
				// Skeleton info
				IGameSkin *pSkin = pIGameObj->GetIGameSkin();
				if(pSkin)
				{
					INode *pRootNode = MaxUtil::GetSkinRootBone(MaxUtil::GetSkin(pNode));
					if(pRootNode)
					{
						rootBone = m_iGameScene->GetIGameNode (pRootNode);
						TiXmlElement* pPolycountElem = new TiXmlElement( "skeleton_root_obj" );
						pAnimElem->LinkEndChild( pPolycountElem );
						XmlUtil::SetAttr( pPolycountElem, "guid", GetNodeGuidString(pRootNode) );
					}
					else
						rootBone = pIGameNode;
				}

				int idxHasAnim = pDataStore->GetAttrIndex("Gta Object", "Has Anim");
				bool hasAnim = pDataStore->GetAttrBool(pNode, idxHasAnim);
				if(hasAnim)
				{
					// Animation length
					int lastKeyFrame = 0;
					LookForLongestAnimationInHierarchy(rootBone, &lastKeyFrame);
					if(lastKeyFrame>0)
					{
						TiXmlElement* pLengthElem = new TiXmlElement( "animation_length" );
						pAnimElem->LinkEndChild( pLengthElem );
						XmlUtil::SetAttr( pLengthElem, "value", lastKeyFrame );
					}
				} // if hasAnim
			} // if Gta Object

			// Loop through our defined properties adding them to our
			// XML element as attributes, except any attributes.
			TiXmlElement* pKeysElem = new TiXmlElement( "keys" );
			std::vector<tNodeProperty>::const_iterator itProperty = pNodeDesc->vKeyedProperties.begin();

			DebugPrint("IExportEntity::IsEntitySupported()? %s %s\n", pIGameNode->GetName(), pIGameObj->IsEntitySupported()?"true":"false");
			IPropertyContainer * pc = pIGameObj->GetIPropertyContainer ();
			for ( itProperty; itProperty != pNodeDesc->vKeyedProperties.end(); ++itProperty )
			{
				tNodeProperty propDefNode = (*itProperty);
				// "active" property keyframes
				IGameProperty * prop = pc->QueryProperty(_T(propDefNode.sName.c_str()));
				Control *pMaxControl = NULL;
				if(prop)
				{
					IGameControl *pPropControl = prop->GetIGameControl();
					if(pPropControl)
					{
						pMaxControl =pPropControl->GetMaxControl(IGAME_FLOAT);
					}
				}//if pPropControl
				if(propDefNode.sName=="visibility")
				{
					RefTargetHandle pRef = pNode->GetReference(VISIBILITY_TRACK_REFNUM);
					pMaxControl = dynamic_cast<Control *>(pRef);
				}
				if(!pMaxControl)
					continue;
				IKeyControl *pikeys = GetKeyControlInterface(pMaxControl);
				if(!pikeys)
					continue;

				TiXmlElement* pKeysPropElem = new TiXmlElement( propDefNode.sName.c_str() );
				pKeysElem->LinkEndChild( pKeysPropElem );

				for(int k=0;k<pikeys->GetNumKeys();k++)
				{
					TimeValue time = 0;
					float val = -1.0f;
					ITCBFloatKey pTcpKey;
					IBezFloatKey pBezKey;
					ILinFloatKey pLinKey;
					IBoolFloatKey pBoolKey;
					if(dynamic_cast<IBoolCntrl *>(pikeys))
					{
						pikeys->GetKey(k,&pBoolKey);
						val = pBoolKey.val;
						time = pBoolKey.time;
					}
					else if(pMaxControl->ClassID().PartA() == TCBINTERP_FLOAT_CLASS_ID)
					{
						pikeys->GetKey(k,&pTcpKey);
						val = pTcpKey.val;
						time = pTcpKey.time;
					}
					else if(pMaxControl->ClassID().PartA() == HYBRIDINTERP_FLOAT_CLASS_ID)
					{
						pikeys->GetKey(k,&pBezKey);
						val = pBezKey.val;
						time = pBezKey.time;
					}
					else if(pMaxControl->ClassID().PartA() ==LININTERP_FLOAT_CLASS_ID)
					{
						pikeys->GetKey(k,&pLinKey);
						val = pLinKey.val;
						time = pLinKey.time;
					}
					else
					{
						MSTR classname;
						pMaxControl->GetClassName(classname);
						char buffer[4096];
						sprintf(buffer, "\"%s\" property control type not supported\n", propDefNode.sName.c_str(), classname.data());
						if(m_pULog)
							m_pULog->LogWarning(buffer, pNode->GetName());
						continue;
					}

					char buffer[4096];
					sprintf(buffer, "\"%s\" property key value %s at time %d\n", propDefNode.sName.c_str(), val==1.0?"ON":"OFF", time);
					if(m_pULog)
						m_pULog->LogMessage(buffer, pNode->GetName());
					else
						mprintf(buffer);
					TiXmlElement* pKeyElem = new TiXmlElement( "key" );
					pKeysPropElem->LinkEndChild( pKeyElem );
					XmlUtil::SetAttr( pKeyElem, "value", val==1.0?1:0 );
					XmlUtil::SetAttr( pKeyElem, "frame", MaxUtil::TimeValueToFrame(time) );
				}
			}
			if(!pKeysElem->NoChildren())
				pAnimElem->LinkEndChild( pKeysElem );

			pIGameNode->ReleaseIGameObject();
		}// if pIGamenode
	}
}

/**
 *
 */
void
cSceneXmlExporter::ExportUserProperty( INode* pNode, const tNodeDescription* pNodeDesc,
								   const tNodeProperty* pNodeProp, TiXmlElement* pElement )
{
//	assert( pNodeProp->sType.length > 0 );
	CStr userPropName( pNodeProp->sName.c_str() );
	const char* pUserPropExportAs = pNodeProp->sExportAs.c_str();
	if(0==strcmp(pUserPropExportAs, "unknownExportAs"))
	{
		if(m_pULog)
		{
			char buffer[4098];
			sprintf_s(buffer, 4098, "Unknown export type for user property %s. Possible desynchronised SceneXML.xml/.gup. Please inform Tools.", userPropName);
			m_pULog->LogWarning(buffer, pNode->GetName());
		}
	}

	bool bPropertyFound = false;

	if ( pNode->UserPropExists( userPropName ) )
	{
		if ( "string" == pNodeProp->sType )
		{
			CStr userPropVal;
			if ( pNode->GetUserPropString( userPropName, userPropVal ) )
				pElement->SetAttribute( pUserPropExportAs, userPropVal.data() );
		}
		else if ( "filename" == pNodeProp->sType )
		{
			CStr userPropVal;
			if (pNode->GetUserPropString( userPropName, userPropVal ) )
			{
				std::string filename = userPropVal.data();
				Filesystem::cPath::normalise( filename );
				pElement->SetAttribute( pUserPropExportAs, filename.c_str() );
			}
		}
		else if ( "int" == pNodeProp->sType )
		{
			int userPropVal;
			if ( pNode->GetUserPropInt( userPropName, userPropVal ) )
			{
				char buffer[1024];
				sprintf_s( buffer, 1024, "%d", userPropVal );
				pElement->SetAttribute( pUserPropExportAs, buffer );
			}
		}
		else if ( "bool" == pNodeProp->sType )
		{
			BOOL userPropVal;
			if ( pNode->GetUserPropBool( userPropName, userPropVal ) )
				pElement->SetAttribute( pUserPropExportAs, userPropVal ? "true" : "false" );
		}
		else if ( "float" == pNodeProp->sType )
		{
			float userPropVal;
			if ( pNode->GetUserPropFloat( userPropName, userPropVal ) )
			{
				char buffer[1024];
				sprintf_s( buffer, 1024, "%f", userPropVal );
				pElement->SetAttribute( pUserPropExportAs, buffer );
			}
		}

		bPropertyFound = true;
	}
	else if ( !(pNodeDesc->idInterface == Interface_ID(0L, 0L)) )
	{
		Object* pObject = pNode->GetObjectRef( );
		FPMixinInterface* pFPMixin = dynamic_cast<FPMixinInterface*>(
			pObject->GetInterface( pNodeDesc->idInterface ) );
		FPInterfaceDesc* pFPDesc = pFPMixin->GetDesc( );

		if ( ( NULL != pFPDesc ) && ( pFPDesc->props.Count() > 0 ) )
		{
			for ( int nProp = 0; nProp < pFPDesc->props.Count(); ++nProp )
			{
				FPPropDef* pPropDef = pFPDesc->props[nProp];
				assert( pPropDef );
				if ( !pPropDef )
					continue;
				if ( 0 != strcmp( pPropDef->internal_name, userPropName.data() ) )
					continue;

				FunctionID funcID = pPropDef->getter_ID;
				if ( FP_NO_FUNCTION == funcID )
				{
					pElement->SetAttribute( pUserPropExportAs, "unknown_prop" );
				}
				else
				{
					FPValue result;
					FPStatus status = pFPMixin->Invoke( funcID, result, NULL );
					if ( FPS_OK == status )
					{
						switch (result.type)
						{
						case TYPE_FILENAME:
							{
								std::string filename( result.s );
								Filesystem::cPath::normalise( filename );
								pElement->SetAttribute( pUserPropExportAs, filename.c_str() );
							}
							break;
						case TYPE_STRING:
							pElement->SetAttribute( pUserPropExportAs, result.s );
							break;
						case TYPE_bool:
							pElement->SetAttribute( pUserPropExportAs, result.b ? "true" : "false" );
							break;
						default:
							assert( false );
						}
					}
					else
						pElement->SetAttribute( pUserPropExportAs, "unknown_fn" );
				}

				bPropertyFound = true;
				break;
			}
		}
	}

	// Handle properties that haven't been found.
	if ( !bPropertyFound )
	{
		pElement->SetAttribute( pUserPropExportAs, "unknown" );
	}
}

/**
 * @brief Export node name.
 */
void
cSceneXmlExporter::ExportNameProperty( INode* pNode, TiXmlElement* pElement )
{
	MCHAR* pName = pNode->GetName( );
	std::string sName = XmlUtil::EscapeStr( pName );

	pElement->SetAttribute( "name", sName.c_str() );
	// If the object is an XRef then we export the refname attribute.
	if ( IsXref( pNode ) )
	{
		Object* pObject = pNode->GetObjectRef( );
		MCHAR* pRefName = ((IXRefObject *)pObject)->GetObjName();
		std::string refName = XmlUtil::EscapeStr( pRefName );
		pElement->SetAttribute( "refname", refName.c_str() );
	}
	// If the object is an internalref then we export the refname and ref attributes.
	else if ( IsInternalRef( pNode ) )
	{
		Object *pObjL = pNode->GetObjectRef( );
		InternalRefObject* pRefObject = (InternalRefObject*)pObjL;
		INode* pNodeRef = (INode*)pRefObject->GetReference(0);

		if ( !pNodeRef )
			return;

		pElement->SetAttribute( "refname", pNodeRef->GetName() );
		pElement->SetAttribute( "ref", GetNodeGuidString(pNodeRef) );
	}
	else if ( HasAttributeClass( pNode, "Gta Object" ) )
	{
		if ( !IsRSref( pNode ) && !IsInternalRef( pNode ) )
		{
			m_mObjectStatsList[ pNode->GetHandle( ) ].Name = sName;
			m_mObjectStatsList[ pNode->GetHandle( ) ].IsCollision = false;
			//bool dontexport = true;
			//IFpMaxDataStore *pMaxDataStore = GetMaxDataStoreInterface();
			//if(pMaxDataStore)
			//{
			//	int idxDontExport = pMaxDataStore->GetAttrIndex("Gta Object","Dont Export");
			//	if(idxDontExport != -1)
			//		dontexport = pMaxDataStore->GetAttrBool(pNode,idxDontExport);

			//	if(dontexport == false)
			//
			//}
		}
	}
	else if ( HasAttributeClass( pNode, "Gta Collision" ) )
	{
		m_mObjectStatsList[ pNode->GetHandle( ) ].Name = sName;
		m_mObjectStatsList[ pNode->GetHandle( ) ].IsCollision = true;
	}
}

/**
 * @brief Export Axis-Aligned Bounding Box information.
 *
 * @param pNode - 3dsmax INode pointer.
 * @param pElement - TinyXml element pointer.
 */
void
cSceneXmlExporter::ExportBoundingBoxProperty( INode* pNode, TiXmlElement* pElement )
{
	assert( pNode );
	if ( !pNode )
		return;
	Object* pObj = pNode->EvalWorldState(0).obj;
	assert( pObj );
	if ( !pObj )
		return;

	// Skip unresolved xref objects.
	if ( IsXref( pNode ) )
	{
		if ( !HasAttributeClass( pNode, "Gta Object" ) )
		{
			if(m_pULog)
				m_pULog->LogWarning("unresolved Xref", pNode->GetName());
			return;
		}
	}
	// skip all rsrefs for now
	if( IsRSref(pNode) )
	{
		if(m_pULog)
			m_pULog->LogWarning("RSRef", pNode->GetName());
		return;
	}

	Matrix3 parmat = pNode->GetNodeTM(0);
	Matrix3 matChild = pNode->GetObjectTM(0);
	Matrix3 matOffset;
	parmat.Invert();
	matChild *= parmat;

	Box3 localbb;
	pObj->GetLocalBoundBox( 0, pNode, NULL, localbb );
	localbb = localbb * matChild;

	// Local Bounding Box
	TiXmlElement* pBoundElement = new TiXmlElement( "boundingbox3" );
	pElement->LinkEndChild( pBoundElement );
	TiXmlElement* pLocalElement = new TiXmlElement( "local" );
	TiXmlElement* pLocalMinElem = new TiXmlElement( "min" );
	TiXmlElement* pLocalMaxElem = new TiXmlElement( "max" );
	pBoundElement->LinkEndChild( pLocalElement );
	pLocalElement->LinkEndChild( pLocalMinElem );
	pLocalElement->LinkEndChild( pLocalMaxElem );

	if ( !IsXref( pNode ) )
	{
		if( m_mObjectStatsList.find( pNode->GetHandle( ) ) != m_mObjectStatsList.end( ) )
		{
			m_mObjectStatsList[ pNode->GetHandle( ) ].Bounds = localbb;
		}
	}

	char val[100];
	ZeroMemory( val, 100 );
	sprintf( val, "%f", localbb.Min().x );
	pLocalMinElem->SetAttribute( "x", val );
	sprintf( val, "%f", localbb.Min().y );
	pLocalMinElem->SetAttribute( "y", val );
	sprintf( val, "%f", localbb.Min().z );
	pLocalMinElem->SetAttribute( "z", val );
	sprintf( val, "%f", localbb.Max().x );
	pLocalMaxElem->SetAttribute( "x", val );
	sprintf( val, "%f", localbb.Max().y );
	pLocalMaxElem->SetAttribute( "y", val );
	sprintf( val, "%f", localbb.Max().z );
	pLocalMaxElem->SetAttribute( "z", val );

	// World Bounding Box
	Box3 worldbb;
	pObj->GetWorldBoundBox( 0, pNode, NULL, worldbb );

	TiXmlElement* pWorldElement = new TiXmlElement( "world" );
	TiXmlElement* pWorldMinElem = new TiXmlElement( "min" );
	TiXmlElement* pWorldMaxElem = new TiXmlElement( "max" );
	pBoundElement->LinkEndChild( pWorldElement );
	pWorldElement->LinkEndChild( pWorldMinElem );
	pWorldElement->LinkEndChild( pWorldMaxElem );

	ZeroMemory( val, 100 );
	sprintf( val, "%f", worldbb.Min().x );
	pWorldMinElem->SetAttribute( "x", val );
	sprintf( val, "%f", worldbb.Min().y );
	pWorldMinElem->SetAttribute( "y", val );
	sprintf( val, "%f", worldbb.Min().z );
	pWorldMinElem->SetAttribute( "z", val );
	sprintf( val, "%f", worldbb.Max().x );
	pWorldMaxElem->SetAttribute( "x", val );
	sprintf( val, "%f", worldbb.Max().y );
	pWorldMaxElem->SetAttribute( "y", val );
	sprintf( val, "%f", worldbb.Max().z );
	pWorldMaxElem->SetAttribute( "z", val );
}

void
cSceneXmlExporter::ExportPlatformAttributesProperty( INode* pNode, TiXmlElement* pElement)
{
	try
	{
		dmat::AttributeInst* pInst = GetAttributes( pNode );

		if (!pInst)
			return;

		const dmat::AttributeClass& attrClass = pInst->GetClass( );
		IFpMaxPlatformAttributes* pPlatformAttributesInterface = GetPlatformAttributesInterface();
		Array* platformArray = (Array*)pPlatformAttributesInterface->GetPlatformAttrPlatforms(pNode);

		if (!platformArray)
			return;

		if (platformArray->size == 0)
			return;

		TiXmlElement* pPlatformAttrElem = new TiXmlElement( "platformAttributes" );
		pElement->LinkEndChild( pPlatformAttrElem );

		for (int iPlatform = 1; iPlatform < platformArray->size + 1; iPlatform++)
		{
			Value* platformValue = platformArray->get(iPlatform);
			if (!platformValue)
				continue;

			char* platformString = platformValue->to_string();
			if (!platformString)
				continue;

			int attrCount = pPlatformAttributesInterface->GetPlatformAttrCount(pNode, CStr(platformString));

			// No attributes for this platform so just move on.
			if (attrCount == 0)
				continue;

			TiXmlElement* pPlatformAttr = new TiXmlElement( "platform" );
			pPlatformAttr->SetAttribute( "name", platformString );
			pPlatformAttrElem->LinkEndChild(pPlatformAttr);

			for (int i = 0; i < attrCount; i++)
			{
				s32 index = pPlatformAttributesInterface->GetPlatformAttrIndexAt(pNode, i, CStr(platformString));
				s32 globalIndex = attrClass.GetId(index);
				const dmat::Attribute& attr = attrClass.GetItem(index);
				const dmat::AttributeValue& attrDefault = attr.GetDefault();

				switch (attr.GetType())
				{
				case dmat::Attribute::STRING:
					{
						CStr attrValue = pPlatformAttributesInterface->GetPlatformAttrStr(pNode, globalIndex, CStr(platformString));

						TiXmlElement* pAttr = new TiXmlElement( "attribute" );
						pAttr->SetAttribute( "name", attr.GetName().c_str() );
						pAttr->SetAttribute( "type", "string" );
						pAttr->SetAttribute( "value", attrValue.data() );
						pPlatformAttr->LinkEndChild( pAttr );
					}
					break;
				case dmat::Attribute::BOOL:
					{
						bool attrValue = pPlatformAttributesInterface->GetPlatformAttrBool(pNode, globalIndex, CStr(platformString));

						TiXmlElement* pAttr = new TiXmlElement( "attribute" );
						pAttr->SetAttribute( "name", attr.GetName().c_str() );
						pAttr->SetAttribute( "type", "bool" );
						pAttr->SetAttribute( "value", attrValue ? "true" : "false" );
						pPlatformAttr->LinkEndChild( pAttr );
					}
					break;
				case dmat::Attribute::INT:
					{
						s32 attrValue = pPlatformAttributesInterface->GetPlatformAttrInt(pNode, globalIndex, CStr(platformString));

						char value[100];
						sprintf_s( value, 100, "%d", attrValue );

						TiXmlElement* pAttr = new TiXmlElement( "attribute" );
						pAttr->SetAttribute( "name", attr.GetName().c_str() );
						pAttr->SetAttribute( "type", "int" );
						pAttr->SetAttribute( "value", value );
						pPlatformAttr->LinkEndChild( pAttr );
					}
					break;
				case dmat::Attribute::FLOAT:
					{
						float attrValue = pPlatformAttributesInterface->GetPlatformAttrFloat(pNode, globalIndex, CStr(platformString));

						char value[100];
						sprintf_s( value, 100, "%f", attrValue );

						TiXmlElement* pAttr = new TiXmlElement( "attribute" );
						pAttr->SetAttribute( "name", attr.GetName().c_str() );
						pAttr->SetAttribute( "type", "float" );
						pAttr->SetAttribute( "value", value );
						pPlatformAttr->LinkEndChild( pAttr );
					}
					break;
				default:
					break;
				}
			}
		}
	}
	catch (...)
	{

	}
}

/**
 * @brief Export our custom attributes for the node.
 *
 * @param pNode - 3dsmax INode pointer.
 * @param pElement - TinyXml element pointer.
 */
void
cSceneXmlExporter::ExportAttributesProperty( INode* pNode, TiXmlElement* pElement )
{
	std::string attrClassName;
	theIMaxDataStore.GetClassFromRefTarget( pNode, attrClassName );

	try
	{
		dmat::AttributeInst* pInst = GetAttributes( pNode );
		if ( !pInst )
			return;

		TiXmlElement* pAttrElem = new TiXmlElement( "attributes" );
		pAttrElem->SetAttribute( "class", attrClassName.c_str() );
		pAttrElem->SetAttribute( "guid", GetNodeGuidString(pNode));
		pElement->LinkEndChild( pAttrElem );

		const dmat::AttributeClass& attrClass = pInst->GetClass( );

		for ( int nAttr = 0; nAttr < attrClass.GetSize(); ++nAttr )
		{
			const dmat::Attribute& attr = attrClass.GetItem( nAttr );
			const dmat::AttributeValue& attrDefault = attr.GetDefault( );

			switch ( attr.GetType() )
			{
			case dmat::Attribute::STRING:
				{
					const char* pBuffer = NULL;
					const char* pDefault = attrDefault;
					pInst->GetValue( nAttr, pBuffer );

					if ( 0 != strcmp( pBuffer, pDefault ) )
					{
						TiXmlElement* pAttr = new TiXmlElement( "attribute" );
						pAttr->SetAttribute( "name", attr.GetName().c_str() );
						pAttr->SetAttribute( "type", "string" );
						pAttr->SetAttribute( "value", pBuffer );
						pAttrElem->LinkEndChild( pAttr );
					}
				}
				break;
			case dmat::Attribute::INT:
				{
					s32 val;
					pInst->GetValue( nAttr, val );
					if ( val != (s32)attrDefault )
					{
						char value[100];
						sprintf_s( value, 100, "%d", val );

						TiXmlElement* pAttr = new TiXmlElement( "attribute" );
						pAttr->SetAttribute( "name", attr.GetName().c_str() );
						pAttr->SetAttribute( "type", "int" );
						pAttr->SetAttribute( "value", value );
						pAttrElem->LinkEndChild( pAttr );
					}
				}
				break;
			case dmat::Attribute::BOOL:
				{
					bool val;
					pInst->GetValue( nAttr, val );
					if ( val != (bool)attrDefault )
					{
						TiXmlElement* pAttr = new TiXmlElement( "attribute" );
						pAttr->SetAttribute( "name", attr.GetName().c_str() );
						pAttr->SetAttribute( "type", "bool" );
						pAttr->SetAttribute( "value", val ? "true" : "false" );
						pAttrElem->LinkEndChild( pAttr );
					}
				}
				break;
			case dmat::Attribute::FLOAT:
				{
					float val;
					float defaultVal = static_cast<float>( attrDefault );
					pInst->GetValue( nAttr, val );
					if ( ( val < ( defaultVal - fEPSILON ) || val > ( defaultVal + fEPSILON ) ) )
					{
						char value[100];
						sprintf_s( value, 100, "%f", val );

						TiXmlElement* pAttr = new TiXmlElement( "attribute" );
						pAttr->SetAttribute( "name", attr.GetName().c_str() );
						pAttr->SetAttribute( "type", "float" );
						pAttr->SetAttribute( "value", value );
						pAttrElem->LinkEndChild( pAttr );
					}
				}
				break;
			default:
				{
					TiXmlElement* pAttr = new TiXmlElement( "attribute" );
					pAttr->SetAttribute( "name", attr.GetName().c_str() );
					pAttr->SetAttribute( "type", "unknown" );
					pAttrElem->LinkEndChild( pAttr );
				}
			}
		}
	}
	catch (...)
	{
		if(m_pULog)
			m_pULog->LogWarning("Attribute export error", pNode->GetName());
		DebugPrint("Eror\n");
	}
	// 	catch (CException* e)
	// 	{
	// 	}
}

using namespace std;

void GetParamBlock(ReferenceMaker* obj, vector<ReferenceTarget*>& pBlocks )
{
	int nRefs = obj->NumRefs();
	for ( int i = 0; i < nRefs; ++i )
	{
		ReferenceTarget* pRef = obj->GetReference(i);
		if ( pRef )
		{
			if(pRef->SuperClassID() == PARAMETER_BLOCK2_CLASS_ID || pRef->SuperClassID() == PARAMETER_BLOCK_CLASS_ID)
				pBlocks.push_back(dynamic_cast<ReferenceTarget*>( pRef ));
		}
	}
}

void GetRefdTextures(ReferenceMaker* obj, vector<Texmap*>& texmaps )
{
	int nRefs = obj->NumRefs();
	for ( int i = 0; i < nRefs; ++i )
	{
		ReferenceTarget* pRef = obj->GetReference(i);
		Texmap *pMap = NULL;
		if ( pRef )
		{
			pMap = dynamic_cast<Texmap *>(pRef);
		}
		texmaps.push_back(pMap);
	}
}

/**
 * @brief Export node parameter blocks (if any).
 *
 * There are a few of our custom objects which still utilise 3dsmax ParamBlock
 * structures to store their attributes.  This is used to ensure that they
 * also get exported any IDE/IPL export would require this information.
 */
void
cSceneXmlExporter::ExportParamBlocksProperty( INode* pNode, TiXmlElement* pElement )
{
	Animatable* pObject = (Animatable*)( pNode->GetObjectRef( ) );
	assert( pObject );
	if ( !pObject )
		return;
	assert( pElement );
	if ( !pElement )
		return;

	TiXmlElement* pParamBlocksElem = new TiXmlElement( "paramblocks" );
	vector<ReferenceTarget*> unexposedPBlocks;

	// Regular object parameter block(s)
	GetParamBlock(pNode->GetObjectRef(), unexposedPBlocks );

	// Scripted-object parameter block(s)
	MSPlugin* pScriptObj = (MSPlugin*)pObject->GetInterface( I_MAXSCRIPTPLUGIN );
	if ( pScriptObj )
	{
		ReferenceTarget* pDelegate = pScriptObj->get_delegate();
		if ( pDelegate )
		{
			GetParamBlock(pDelegate, unexposedPBlocks );
		}
	}

	for(vector<ReferenceTarget*>::const_iterator it = unexposedPBlocks.begin();
		it!=unexposedPBlocks.end();
		it++)
	{
		IParamBlock2 *pBlock2 = dynamic_cast<IParamBlock2 *>( (*it) );
		if(pBlock2)
			ExportParamBlock( pBlock2, pParamBlocksElem );
		IParamBlock *pBlock = dynamic_cast<IParamBlock *>( (*it) );
		if(pBlock)
			ExportParamBlock( pBlock, pParamBlocksElem );
	}

	if ( pParamBlocksElem )
		pElement->LinkEndChild( pParamBlocksElem );
}

/**
 * @brief Export referenced textures.
 */
void
cSceneXmlExporter::ExportRefTexturesProperty( INode* pNode, TiXmlElement* pElement )
{
	Animatable* pObject = (Animatable*)( pNode->GetObjectRef( ) );
	assert( pElement );
	if ( !pElement )
		return;

	TiXmlElement* pRefTexsElem = new TiXmlElement( "refdTextures" );
	vector<Texmap*> texmapRefs;

	// Regular object parameter block(s)
	GetRefdTextures(pNode->GetObjectRef(), texmapRefs );

	// Scripted-object parameter block(s)
	MSPlugin* pScriptObj = (MSPlugin*)pObject->GetInterface( I_MAXSCRIPTPLUGIN );
	if ( pScriptObj )
	{
		ReferenceTarget* pDelegate = pScriptObj->get_delegate();
		if ( pDelegate )
		{
			GetRefdTextures(pDelegate, texmapRefs );
		}
	}

	for(int refTexIndex = 0; refTexIndex<texmapRefs.size();refTexIndex++)
	{
		Texmap *pMap = texmapRefs[refTexIndex];
		if(pMap)
		{
			TiXmlElement* pRefTexElem = new TiXmlElement( "texture" );
			assert( pRefTexElem );

			BitmapTex *pBitmapTex = dynamic_cast<BitmapTex *>(pMap);

			pRefTexElem->SetAttribute("name", pMap->GetName());
			pRefTexElem->SetAttribute("refNum", refTexIndex);
			if(pBitmapTex)
			{
				std::string valnorm( pBitmapTex->GetMapName() );
				Filesystem::cPath::normalise( valnorm );
				pRefTexElem->SetAttribute("filename", valnorm.c_str() );
			}
			pRefTexsElem->LinkEndChild( pRefTexElem );
		}
	}

	if ( pRefTexsElem )
		pElement->LinkEndChild( pRefTexsElem );
};

/**
 * @brief Export single IParamBlock2.
 */
void
cSceneXmlExporter::ExportParamBlock( IParamBlock2* pParamBlock, TiXmlElement* pElement )
{
	assert( pParamBlock );
	if ( !pParamBlock )
		return;

	int nParams = pParamBlock->NumParams( );
	if ( 0 == nParams )
		return;

	TiXmlElement* pParamBlockElem = new TiXmlElement( "paramblock" );
	pElement->LinkEndChild( pParamBlockElem );

	for ( int nParam = 0; nParam < nParams; ++nParam )
	{
		ParamID id = pParamBlock->IndextoID(nParam);
		const ParamDef& paramDef = pParamBlock->GetParamDef( id );
		const ParamType2 paramType = paramDef.type;

		// Some proprietary plugins that aren't installed on your machine but are referenced in the maxfile
		// have an empty name, so just set the attribute name as "undefined" otherwise this will crap out.
		char* paramDefName = paramDef.int_name != NULL ? paramDef.int_name : "undefined";

		switch ( paramType )
		{
		case TYPE_BOOL:
			{
				int val = pParamBlock->GetInt( paramDef.ID );
				int defval = paramDef.def.i;

				if ( val != defval )
				{
					TiXmlElement* pAttr = new TiXmlElement( "parameter" );
					pAttr->SetAttribute( "name", paramDefName);
					pAttr->SetAttribute( "type", "bool" );
					pAttr->SetAttribute( "value", val ? "true" : "false" );
					pParamBlockElem->LinkEndChild( pAttr );
				}
			}
			break;
		case TYPE_FILENAME:
			{
				const char* val = pParamBlock->GetStr( paramDef.ID );
				const char* defval = paramDef.def.s;

				if(!val)
				{
					if(m_pULog)
					{
						char msg[256];
						sprintf_s(msg, 256, "Object's filename paramblock entry %s returned NULL.", pParamBlock->GetLocalName(id));
						m_pULog->LogWarning(msg, pParamBlock->NodeName());
					}
					break;
				}

				if ( ( val && !defval ) ||
					(	val && defval && 
						is_printable( val ) && is_printable( defval ) && 
						( 0 != strcmp( val, defval ) ) ) )
				{
					std::string valnorm( val );
					Filesystem::cPath::normalise( valnorm );

					TiXmlElement* pAttr = new TiXmlElement( "parameter" );
					pAttr->SetAttribute( "name", paramDefName);
					pAttr->SetAttribute( "type", "string" );
					pAttr->SetAttribute( "value", valnorm.c_str() );
					pParamBlockElem->LinkEndChild( pAttr );
				}
			}
			break;
		case TYPE_TEXMAP:
			{
				Texmap* val = pParamBlock->GetTexmap( paramDef.ID );
				BitmapTex* pBitmap = dynamic_cast<BitmapTex *>(val);
				Texmap* defval = (Texmap*)paramDef.def.r;

				if(!pBitmap)
				{
					if (m_pULog)
					{
						char msg[256];
						sprintf_s(msg, 256, "Object's texmap paramblock entry %s returned NULL.", pParamBlock->GetLocalName(id));
						m_pULog->LogWarning(msg, pParamBlock->NodeName());
					}

					break;
				}

				if ( ( val && !defval ) ||  (  val != defval ) )
				{
					std::string valnorm( pBitmap->GetMapName() );
					Filesystem::cPath::normalise( valnorm );

					TiXmlElement* pAttr = new TiXmlElement( "parameter" );
					pAttr->SetAttribute( "name", paramDefName);
					pAttr->SetAttribute( "type", "string" );
					pAttr->SetAttribute( "value", valnorm.c_str() );
					pParamBlockElem->LinkEndChild( pAttr );
				}
			}
			break;
		case TYPE_STRING:
			{
				const char* val = pParamBlock->GetStr( paramDef.ID );
				const char* defval = paramDef.def.s;

				if(!val)
				{
					if(m_pULog)
					{
						char msg[256];
						sprintf_s(msg, 256, "Object's string paramblock entry %s returned NULL.", pParamBlock->GetLocalName(id));
						m_pULog->LogWarning(msg, pParamBlock->NodeName());
					}
					break;
				}

				if ( !defval || 0 != strcmp( val, defval ) )
				{
					TiXmlElement* pAttr = new TiXmlElement( "parameter" );
					pAttr->SetAttribute( "name", paramDefName);
					pAttr->SetAttribute( "type", "string" );
					pAttr->SetAttribute( "value", val );
					pParamBlockElem->LinkEndChild( pAttr );
				}
			}
			break;
		case TYPE_INT:
			{
				int val = pParamBlock->GetInt( paramDef.ID );
				int defval = paramDef.def.i;

				if ( val != defval )
				{
					TiXmlElement* pAttr = new TiXmlElement( "parameter" );
					pAttr->SetAttribute( "name", paramDefName);
					pAttr->SetAttribute( "type", "int" );
					pAttr->SetAttribute( "value", val );
					pParamBlockElem->LinkEndChild( pAttr );
				}
			}
			break;
		case TYPE_WORLD:
		case TYPE_DOUBLE:
		case TYPE_FLOAT:
			{
				float val = pParamBlock->GetFloat( paramDef.ID );
				float defval = paramDef.def.f;

				if ( val < ( defval - fEPSILON ) ||
					val > ( defval + fEPSILON ) )
				{
					char value[100];
					sprintf_s( value, 100, "%f", val );

					TiXmlElement* pAttr = new TiXmlElement( "parameter" );
					pAttr->SetAttribute( "name", paramDefName);
					pAttr->SetAttribute( "type", "float" );
					pAttr->SetAttribute( "value", value );
					pParamBlockElem->LinkEndChild( pAttr );
				}
			}
			break;
		case TYPE_INODE:
			{
				INode* pNodeRef = pParamBlock->GetINode( paramDef.ID );
				if ( !pNodeRef )
					break;
				Object* pObjRef = pNodeRef->GetObjectRef( );
				if ( !pObjRef )
					break;

				TiXmlElement* pAttr = new TiXmlElement( "parameter" );
				pAttr->SetAttribute( "name", paramDefName);
				pAttr->SetAttribute( "type", "object" );
				pAttr->SetAttribute( "value", GetNodeGuidString(pNodeRef) );
				pParamBlockElem->LinkEndChild( pAttr );
			}
			break;
		case TYPE_INODE_TAB:
			{
				int cnt = pParamBlock->Count( paramDef.ID );
				TiXmlElement* pAttr = new TiXmlElement( "parameter" );
				pAttr->SetAttribute( "name", paramDefName);
				pAttr->SetAttribute( "type", "array" );
				pAttr->SetAttribute( "value", cnt );
				pParamBlockElem->LinkEndChild( pAttr );

				for ( int n = 0; n < cnt; n++ )
				{
					INode* pNodeRef = pParamBlock->GetINode( paramDef.ID, 0, n );
					if ( !pNodeRef )
						continue;
					Object* pObjRef = MaxUtil::GetBaseObject( pNodeRef );
					if ( !pObjRef )
						continue;

					TiXmlElement* pValAttr = new TiXmlElement( "parameter" );
					pValAttr->SetAttribute( "name", n );
					pValAttr->SetAttribute( "type", "object" );
					pValAttr->SetAttribute( "value",GetNodeGuidString(pNodeRef) );
					pAttr->LinkEndChild( pValAttr );
				}
			}
			break;
		case TYPE_STRING_TAB:
			{
				int cnt = pParamBlock->Count( paramDef.ID );
				TiXmlElement* pAttr = new TiXmlElement( "parameter" );
				pAttr->SetAttribute( "name", paramDefName);
				pAttr->SetAttribute( "type", "array" );
				pAttr->SetAttribute( "value", cnt );
				pParamBlockElem->LinkEndChild( pAttr );

				for ( int n = 0; n < cnt; n++ )
				{
					const MCHAR* pString = pParamBlock->GetStr( paramDef.ID, 0, n );
					if ( !pString )
						continue;

					TiXmlElement* pValAttr = new TiXmlElement( "parameter" );
					pValAttr->SetAttribute( "name", n );
					pValAttr->SetAttribute( "type", "string" );
					pValAttr->SetAttribute( "value", pString );
					pAttr->LinkEndChild( pValAttr );
				}
			}
			break;
		case TYPE_RGBA:
			{
				const Color val = pParamBlock->GetColor( paramDef.ID );
				const AColor val1 = pParamBlock->GetAColor( paramDef.ID );
				const Point4 val2 = pParamBlock->GetPoint4( paramDef.ID );
				const Color* defval = (Color*)paramDef.def.p;

				if ( !defval || val != *defval )
				{
					TiXmlElement* pAttr = new TiXmlElement( "parameter" );
					pAttr->SetAttribute( "name", paramDefName );
					pAttr->SetAttribute( "type", "xyz" );
					pParamBlockElem->LinkEndChild( pAttr );
					TiXmlElement* pSubAttr = new TiXmlElement( "colour" );
					pSubAttr->SetDoubleAttribute( "x", val.r );
					pSubAttr->SetDoubleAttribute( "y", val.g );
					pSubAttr->SetDoubleAttribute( "z", val.b );
//					pSubAttr->SetDoubleAttribute( "w", val.a );
					pAttr->LinkEndChild( pSubAttr );
				}
			}
			break;
		case TYPE_POINT3:
			{
				const Point3 val = pParamBlock->GetPoint3( paramDef.ID );
				const Point3* defval = paramDef.def.p;

				if ( !defval || val != *defval )
				{
					TiXmlElement* pAttr = new TiXmlElement( "parameter" );
					pAttr->SetAttribute( "name", paramDefName );
					pAttr->SetAttribute( "type", "xyz" );
					pParamBlockElem->LinkEndChild( pAttr );
					TiXmlElement* pSubAttr = new TiXmlElement( "point3" );
					pSubAttr->SetDoubleAttribute( "x", val.x );
					pSubAttr->SetDoubleAttribute( "y", val.y );
					pSubAttr->SetDoubleAttribute( "z", val.z );
					pAttr->LinkEndChild( pSubAttr );
				}
			}
			break;
		case TYPE_POINT4:
			{
				const Point4 val = pParamBlock->GetPoint4( paramDef.ID );
				const Point4* defval = paramDef.def.p4;

				if ( !defval || val != *defval )
				{
					TiXmlElement* pAttr = new TiXmlElement( "parameter" );
					pAttr->SetAttribute( "name", paramDefName );
					pAttr->SetAttribute( "type", "xyzw" );
					pParamBlockElem->LinkEndChild( pAttr );
					TiXmlElement* pSubAttr = new TiXmlElement( "point4" );
					pSubAttr->SetDoubleAttribute( "x", val.x );
					pSubAttr->SetDoubleAttribute( "y", val.y );
					pSubAttr->SetDoubleAttribute( "z", val.z );
					pSubAttr->SetDoubleAttribute( "w", val.w );
					pAttr->LinkEndChild( pSubAttr );
				}
			}
			break;

		default:
			// No default case as it was breaking as the paramdef didn't
			// always have the parameter name information.
			break;
		}
	}
}

/**
 * @brief
 */
void
cSceneXmlExporter::ExportParamBlock( IParamBlock* pParamBlock, TiXmlElement* pElement )
{
	assert( pParamBlock );
	if ( !pParamBlock )
		return;

	int nParams = pParamBlock->NumParams( );
	if ( 0 == nParams )
		return;

	TiXmlElement* pParamBlockElem = new TiXmlElement( "paramblock" );
	pElement->LinkEndChild( pParamBlockElem );

	for ( int id = 0; id < nParams; ++id )
	{
//		const ParamDef& paramDef = pParamBlock->GetParamDef( id );
		ParamType paramType = pParamBlock->GetParameterType( id );
		MSTR pParamName = pParamBlock->SubAnimName(pParamBlock->GetAnimNum(id));

		switch ( paramType )
		{
		case TYPE_BOOL:
			{
				int val = pParamBlock->GetInt( id );
				if ( val  )
				{
					TiXmlElement* pAttr = new TiXmlElement( "parameter" );
					pAttr->SetAttribute( "name", id );
					pAttr->SetAttribute( "type", "bool" );
					pAttr->SetAttribute( "value", val ? "true" : "false" );
					pParamBlockElem->LinkEndChild( pAttr );
				}
			}
			break;
		case TYPE_INT:
			{
				int val = pParamBlock->GetInt( id );
				if ( val  )
				{
					TiXmlElement* pAttr = new TiXmlElement( "parameter" );
					pAttr->SetAttribute( "name", id );
					pAttr->SetAttribute( "type", "int" );
					pAttr->SetAttribute( "value", val );
					pParamBlockElem->LinkEndChild( pAttr );
				}
			}
			break;
		case TYPE_FLOAT:
			{
				float val = pParamBlock->GetFloat( id );
				if ( val  )
				{
					char value[100];
					sprintf_s( value, 100, "%f", val );

					TiXmlElement* pAttr = new TiXmlElement( "parameter" );
					pAttr->SetAttribute( "name", id );
					pAttr->SetAttribute( "type", "float" );
					pAttr->SetAttribute( "value", value );
					pParamBlockElem->LinkEndChild( pAttr );
				}
			}
			break;
		case TYPE_RGBA:
			{
				const Color val = pParamBlock->GetColor( id );
				const Point3 pval = pParamBlock->GetPoint3( id );
				const AColor cval = pParamBlock->GetColor( id );

				TiXmlElement* pAttr = new TiXmlElement( "parameter" );
				pAttr->SetAttribute( "name", id );
				pAttr->SetAttribute( "type", "xyz" );
				pParamBlockElem->LinkEndChild( pAttr );
				TiXmlElement* pSubAttr = new TiXmlElement( "colour" );
				pSubAttr->SetDoubleAttribute( "x", val.r );
				pSubAttr->SetDoubleAttribute( "y", val.g );
				pSubAttr->SetDoubleAttribute( "z", val.b );
				pAttr->LinkEndChild( pSubAttr );
			}
			break;
		case TYPE_POINT3:
			{
				const Point3 val = pParamBlock->GetPoint3( id );

				TiXmlElement* pAttr = new TiXmlElement( "parameter" );
				pAttr->SetAttribute( "name", id );
				pAttr->SetAttribute( "type", "xyz" );
				pParamBlockElem->LinkEndChild( pAttr );
				TiXmlElement* pSubAttr = new TiXmlElement( "point3" );
				pSubAttr->SetDoubleAttribute( "x", val.x );
				pSubAttr->SetDoubleAttribute( "y", val.y );
				pSubAttr->SetDoubleAttribute( "z", val.z );
				pAttr->LinkEndChild( pSubAttr );
			}
			break;
		default:
			// No default case as it was breaking as the paramdef didn't
			// always have the parameter name information.
			break;
		}
	}

	// duplicating for compatability
	for ( int id = 0; id < nParams; ++id )
	{
		//		const ParamDef& paramDef = pParamBlock->GetParamDef( id );
		ParamType paramType = pParamBlock->GetParameterType( id );
		MSTR pParamName = pParamBlock->SubAnimName(pParamBlock->GetAnimNum(id));

		switch ( paramType )
		{
		case TYPE_BOOL:
			{
				int val = pParamBlock->GetInt( id );
				if ( val  )
				{
					TiXmlElement* pAttr = new TiXmlElement( "parameter" );
					pAttr->SetAttribute( "name", pParamName.data() );
					pAttr->SetAttribute( "type", "bool" );
					pAttr->SetAttribute( "value", val ? "true" : "false" );
					pParamBlockElem->LinkEndChild( pAttr );
				}
			}
			break;
		case TYPE_INT:
			{
				int val = pParamBlock->GetInt( id );
				if ( val  )
				{
					TiXmlElement* pAttr = new TiXmlElement( "parameter" );
					pAttr->SetAttribute( "name", pParamName.data() );
					pAttr->SetAttribute( "type", "int" );
					pAttr->SetAttribute( "value", val );
					pParamBlockElem->LinkEndChild( pAttr );
				}
			}
			break;
		case TYPE_FLOAT:
			{
				float val = pParamBlock->GetFloat( id );
				if ( val  )
				{
					char value[100];
					sprintf_s( value, 100, "%f", val );

					TiXmlElement* pAttr = new TiXmlElement( "parameter" );
					pAttr->SetAttribute( "name", pParamName.data() );
					pAttr->SetAttribute( "type", "float" );
					pAttr->SetAttribute( "value", value );
					pParamBlockElem->LinkEndChild( pAttr );
				}
			}
			break;
		case TYPE_RGBA:
			{
				const Color val = pParamBlock->GetColor( id );
				const Point3 pval = pParamBlock->GetPoint3( id );
				const AColor cval = pParamBlock->GetColor( id );

				TiXmlElement* pAttr = new TiXmlElement( "parameter" );
				pAttr->SetAttribute( "name", pParamName.data() );
				pAttr->SetAttribute( "type", "xyz" );
				pParamBlockElem->LinkEndChild( pAttr );
				TiXmlElement* pSubAttr = new TiXmlElement( "colour" );
				pSubAttr->SetDoubleAttribute( "x", val.r );
				pSubAttr->SetDoubleAttribute( "y", val.g );
				pSubAttr->SetDoubleAttribute( "z", val.b );
				pAttr->LinkEndChild( pSubAttr );
			}
			break;
		case TYPE_POINT3:
			{
				const Point3 val = pParamBlock->GetPoint3( id );

				TiXmlElement* pAttr = new TiXmlElement( "parameter" );
				pAttr->SetAttribute( "name", pParamName.data() );
				pAttr->SetAttribute( "type", "xyz" );
				pParamBlockElem->LinkEndChild( pAttr );
				TiXmlElement* pSubAttr = new TiXmlElement( "point3" );
				pSubAttr->SetDoubleAttribute( "x", val.x );
				pSubAttr->SetDoubleAttribute( "y", val.y );
				pSubAttr->SetDoubleAttribute( "z", val.z );
				pAttr->LinkEndChild( pSubAttr );
			}
			break;
		default:
			// No default case as it was breaking as the paramdef didn't
			// always have the parameter name information.
			break;
		}
	}
}

/**
 * @brief Export transformation (translation, rotation only) for the node.
 *
 * Rotation is only exported if there is a rotation on the object.
 * Translation is always exported.
 */
void
cSceneXmlExporter::ExportTransformProperty( INode* pNode, TiXmlElement* pElement )
{
	assert( pNode );
	assert( pElement );

	// Object world-space transform.
	Matrix3 matWorldTransform = pNode->GetObjectTM( 0 );
	Matrix3 matNodeTransform = pNode->GetNodeTM( 0 );

	TiXmlElement* pTransformElem = XmlUtil::CreateTransformNode( "transform", matWorldTransform, matNodeTransform );
	pElement->LinkEndChild( pTransformElem );
}

/**
 * @brief Export local transformation for the node (relative to parent).
 */
void
cSceneXmlExporter::ExportLocalTransformProperty( INode* pNode, TiXmlElement* pElement )
{
	assert( pNode );
	assert( pElement );

	Matrix3 matId;
	matId.IdentityMatrix();

	Matrix3 matTransform = pNode->GetObjectTM( 0 );
	Matrix3 matNodeTransform = pNode->GetNodeTM( 0 );
	matTransform *= Inverse( matNodeTransform );

	TiXmlElement* pLocalTransElem = XmlUtil::CreateTransformNode( "localtransform", matTransform, matId );
	pElement->LinkEndChild( pLocalTransElem );
}

/**
 * @brief Export node's LOD parent and children (if applicable).
 */
void
cSceneXmlExporter::ExportLodHierarchy( INode* pNode, TiXmlElement* pElement )
{
	assert( pNode );
	assert( pElement );

	ExportSceneLodHierarchy( pNode, pElement );
	ExportDrawableLodHierarchy( pNode, pElement );
	ExportSceneLinkHierarchy( pNode, pElement );
}

void
cSceneXmlExporter::ExportSceneLodHierarchy( INode* pNode, TiXmlElement* pElement )
{
	IFpSceneHierarchy* pSceneLodHierarchy = GetSceneLinkInterface( );
	if ( !pSceneLodHierarchy )
		return;

	INode* pParent = pSceneLodHierarchy->GetParent( eLinkType_LOD, pNode );
	Tab<INode*> children;
	pSceneLodHierarchy->GetChildren( eLinkType_LOD, pNode, children );

	// Only add LOD XML if we have something to put in it.
	if ( !pParent && 0 == children.Count() )
		return;

	TiXmlElement* pLodElem = new TiXmlElement( "lod_hierarchy" );
	pElement->LinkEndChild( pLodElem );

	if ( pParent )
	{
		TiXmlElement* pLodParentElem = new TiXmlElement( "parent" );
		pLodElem->LinkEndChild( pLodParentElem );
		pLodParentElem->SetAttribute( "guid", GetNodeGuidString(pParent) );

		Object* pObjRef = MaxUtil::GetBaseObject( pParent );

		MCHAR* classname = pObjRef->GetObjectName( );
		std::string sClassName = MakeClassNameFriendly( classname );
		std::string sObjName = pParent->GetName( );

		pLodParentElem->SetAttribute( "name", sObjName.c_str() );
		pLodParentElem->SetAttribute( "classname", sClassName.c_str() );
	}
	if ( children.Count() > 0 )
	{
		TiXmlElement* pLodChildrenElem = new TiXmlElement( "children" );
		pLodElem->LinkEndChild( pLodChildrenElem );
		pLodChildrenElem->SetAttribute( "count", children.Count() );

		for ( int n = 0; n < children.Count(); ++n )
		{
			INode* pChild = children[n];
			TiXmlElement* pChildElem = new TiXmlElement( "child" );
			pLodChildrenElem->LinkEndChild( pChildElem );
			pChildElem->SetAttribute( "guid", GetNodeGuidString(pChild) );

			Object* pObjRef = MaxUtil::GetBaseObject( pChild );
			MCHAR* classname = pObjRef->GetObjectName( );
			std::string sClassName = MakeClassNameFriendly( classname );
			std::string sObjName = pChild->GetName( );

			pChildElem->SetAttribute( "name", sObjName.c_str() );
			pChildElem->SetAttribute( "classname", sClassName.c_str() );
		}
	}
}

void
cSceneXmlExporter::ExportDrawableLodHierarchy( INode* pNode, TiXmlElement* pElement )
{
	IFpSceneHierarchy* pSceneLodHierarchy = GetSceneLinkInterface( );
	if ( !pSceneLodHierarchy )
		return;

	INode* pLowerDetailNode = pSceneLodHierarchy->GetParent( eLinkType_DRAWLOD, pNode ); // parent
	Tab<INode*> pNodes;
	pSceneLodHierarchy->GetChildren( eLinkType_DRAWLOD, pNode, pNodes ); // child
	INode* pHigherDetailNode = pNodes.Count()>0 ? pNodes[0] : NULL;
	INode* pParentNode = pSceneLodHierarchy->GetParent( eLinkType_RENDERSIM, pNode ); // parent
	Tab<INode*> pNodes2;
	pSceneLodHierarchy->GetChildren( eLinkType_RENDERSIM, pNode, pNodes2 ); // child
	INode* pChildNode = pNodes2.Count()>0 ? pNodes2[0] : NULL;

	// Only add LOD XML if we have something to put in it.
	if ( !pLowerDetailNode && !pHigherDetailNode && !pParentNode && !pChildNode )
		return;

	TiXmlElement* pLodElem = new TiXmlElement( "lod_drawable_hierarchy" );
	pElement->LinkEndChild( pLodElem );

	if ( pLowerDetailNode )
	{
		TiXmlElement* pLodParentElem = new TiXmlElement( "parent" );
		pLodElem->LinkEndChild( pLodParentElem );
		pLodParentElem->SetAttribute( "guid", GetNodeGuidString(pLowerDetailNode) );

		Object* pObjRef = MaxUtil::GetBaseObject( pLowerDetailNode );

		MCHAR* classname = pObjRef->GetObjectName( );
		std::string sClassName = MakeClassNameFriendly( classname );
		std::string sObjName = pLowerDetailNode->GetName( );

		pLodParentElem->SetAttribute( "name", sObjName.c_str() );
		pLodParentElem->SetAttribute( "classname", sClassName.c_str() );
	}
	if ( pHigherDetailNode )
	{
		TiXmlElement* pLodChildrenElem = new TiXmlElement( "children" );
		pLodElem->LinkEndChild( pLodChildrenElem );
		pLodChildrenElem->SetAttribute( "count", 1 );

		TiXmlElement* pChildElem = new TiXmlElement( "child" );
		pLodChildrenElem->LinkEndChild( pChildElem );
		pChildElem->SetAttribute( "guid", GetNodeGuidString(pHigherDetailNode) );

		Object* pObjRef = MaxUtil::GetBaseObject( pHigherDetailNode );
		MCHAR* classname = pObjRef->GetObjectName( );
		std::string sClassName = MakeClassNameFriendly( classname );
		std::string sObjName = pHigherDetailNode->GetName( );

		pChildElem->SetAttribute( "name", sObjName.c_str() );
		pChildElem->SetAttribute( "classname", sClassName.c_str() );
	}
	if ( pParentNode )
	{
		TiXmlElement* pLodParentElem = new TiXmlElement( "secondaryParent" );
		pLodElem->LinkEndChild( pLodParentElem );
		pLodParentElem->SetAttribute( "guid", GetNodeGuidString(pParentNode) );

		Object* pObjRef = MaxUtil::GetBaseObject( pParentNode );

		MCHAR* classname = pObjRef->GetObjectName( );
		std::string sClassName = MakeClassNameFriendly( classname );
		std::string sObjName = pParentNode->GetName( );

		pLodParentElem->SetAttribute( "name", sObjName.c_str() );
		pLodParentElem->SetAttribute( "classname", sClassName.c_str() );
	}
	if ( pChildNode )
	{
		TiXmlElement* pLodChildrenElem = new TiXmlElement( "secondaryChildren" );
		pLodElem->LinkEndChild( pLodChildrenElem );
		pLodChildrenElem->SetAttribute( "count", 1 );

		TiXmlElement* pChildElem = new TiXmlElement( "child" );
		pLodChildrenElem->LinkEndChild( pChildElem );
		pChildElem->SetAttribute( "guid", GetNodeGuidString(pChildNode) );

		Object* pObjRef = MaxUtil::GetBaseObject( pChildNode );
		MCHAR* classname = pObjRef->GetObjectName( );
		std::string sClassName = MakeClassNameFriendly( classname );
		std::string sObjName = pChildNode->GetName( );

		pChildElem->SetAttribute( "name", sObjName.c_str() );
		pChildElem->SetAttribute( "classname", sClassName.c_str() );
	}
}

void
cSceneXmlExporter::GetNodeWithGuids( INode* pNode, char *name, TiXmlElement* pParentElement )
{
	TiXmlElement* pElem = new TiXmlElement( name );

	const TCHAR *pGuidString = GetNodeGuidString(pNode);
	pElem->SetAttribute( "guid", pGuidString );
	pElem->SetAttribute( "attr_guid", pGuidString);

	pParentElement->LinkEndChild( pElem );
}

void
cSceneXmlExporter::ExportSceneLinkHierarchy( INode* pNode, TiXmlElement* pElement )
{
	IFpSceneHierarchy* pSceneLodHierarchy = GetSceneLinkInterface( );
	if ( !pSceneLodHierarchy )
		return;

	TiXmlElement* pSceneLinkElem = new TiXmlElement( "scene_link_hierarchy" );
	pElement->LinkEndChild( pSceneLinkElem );

	Tab<int> parentChannels = pSceneLodHierarchy->GetNodeParentLinkTypes(pNode);
	Tab<int> childChannels = pSceneLodHierarchy->GetNodeChildLinkTypes(pNode);
	int highestChannel = 0;
	for(int pi=0;pi<parentChannels.Count();pi++)
		if(parentChannels[pi]>highestChannel)
			highestChannel = parentChannels[pi];
	for(int pi=0;pi<childChannels.Count();pi++)
		if(childChannels[pi]>highestChannel)
			highestChannel = childChannels[pi];

	for (int channelIndex=0;channelIndex<=highestChannel;channelIndex++)
	{
		TiXmlElement* pChannelElem = new TiXmlElement( "channel" );
		pChannelElem->SetAttribute( "index", channelIndex );
		pChannelElem->SetAttribute( "name", pSceneLodHierarchy->GetChannelName(channelIndex) );
		bool isChannelUsed = false;

		INodePtr pParentNode = pSceneLodHierarchy->GetParent(channelIndex, pNode);
		if(pParentNode)
		{
			GetNodeWithGuids(pParentNode, "parent", pChannelElem);
			isChannelUsed = true;
		}

		Tab<INode*> pNodes;
		pSceneLodHierarchy->GetChildren( channelIndex, pNode, pNodes ); // child
		TiXmlElement* pChildrenElem = new TiXmlElement( "children" );
		if(pNodes.Count()>0)
			pChannelElem->LinkEndChild( pChildrenElem );
		for(int i=0;i<pNodes.Count();i++)
		{
			GetNodeWithGuids(pNodes[i], "child", pChildrenElem);
			isChannelUsed = true;
		}

		if(isChannelUsed)
			pSceneLinkElem->LinkEndChild( pChannelElem );
	}
}

/**
 * @brief Export generic references to other INodes.
 */
void
cSceneXmlExporter::ExportReferences( INode* pNode, TiXmlElement* pElement )
{
	// DHM HACK - nasty that this is not generic!
	if (!(IsVehicleLink(pNode) || IsPatrolLink(pNode)))
		return;

	ULONG hHandle = pNode->GetHandle();
	Object* pObject = pNode->EvalWorldState(0).obj;
	int nNumRefs = pObject->NumRefs();
	if ( 0 == nNumRefs )
		return;

	TiXmlElement* pReferencesElem = new TiXmlElement( "references" );
	pElement->LinkEndChild( pReferencesElem );

	for ( int nRef = 0; nRef < nNumRefs; ++nRef )
	{
		RefTargetHandle hRef = pObject->GetReference( nRef );

#if (MAX_RELEASE >= 12000)
		DependentIterator pNodeItem(hRef);
		ReferenceMaker* maker = NULL;

		while(NULL != (maker = pNodeItem.Next()))
		{
			if ( INODE_CLASS_ID == maker->ClassID() )
			{
				INode* pRefNode = static_cast<INode*>( maker );
				if ( pRefNode )
				{
					INode* pRealRefNode = pRefNode->GetActualINode();
					ULONG hRefHandle = pRealRefNode->GetHandle();

					TiXmlElement* pRefElem = new TiXmlElement( "ref" );
					pRefElem->SetAttribute( "guid", GetNodeGuidString(pRealRefNode) );
					pReferencesElem->LinkEndChild( pRefElem );
				}
			}
		}
#else
		RefListItem* pNodeItem = hRef->GetRefList().FirstItem();

		while (pNodeItem)
		{
			if ( INODE_CLASS_ID == pNodeItem->maker->ClassID() )
			{
				INode* pRefNode = static_cast<INode*>( pNodeItem->maker );
				if ( pRefNode )
				{
					INode* pRealRefNode = pRefNode->GetActualINode();
					ULONG hRefHandle = pRealRefNode->GetHandle();

					TiXmlElement* pRefElem = new TiXmlElement( "ref" );
					pRefElem->SetAttribute( "guid", GetNodeGuidString(pRealRefNode) );
					pReferencesElem->LinkEndChild( pRefElem );
				}
			}
			pNodeItem = pNodeItem->next;
		}
#endif  //MAX_RELEASE >= 12000
	}
}

/**
 * @brief Export polycount property.
 */
void
cSceneXmlExporter::ExportPolycount( INode* pNode, TiXmlElement* pElement )
{
	IFpCollisionVolumes* pCollisionVolumes = GetCollisionVolumesInterface();
	assert( pCollisionVolumes );
	if ( !pCollisionVolumes )
		return;

	int polycount = 0;
	if ( HasAttributeClass( pNode, "Gta Collision" ) )
	{
		polycount = pCollisionVolumes->GetPolycount( pNode );

		if( m_mObjectStatsList.find( pNode->GetHandle( ) ) != m_mObjectStatsList.end( ) )
		{
			m_mObjectStatsList[pNode->GetHandle()].PolyCount = polycount;
		}
	}
	else if ( HasAttributeClass( pNode, "Gta Object" ) && !IsXref( pNode ) && !IsRSref( pNode ) )
	{
		int numFaces = 0;
		int numVerts = 0;

		Object* pObject = pNode->EvalWorldState( 0 ).obj;
		Object* pBaseObject = MaxUtil::GetBaseObject( pNode );
		if ( pBaseObject )
		{
			bool delTriObject = false;
			Object* pTriObject = NULL;
			if ( pBaseObject && Class_ID(EDITTRIOBJ_CLASS_ID, 0) == pBaseObject->ClassID( ) )
			{
				pTriObject = static_cast<TriObject*>( pBaseObject );
			}
			else if ( pBaseObject && Class_ID(TRIOBJ_CLASS_ID, 0) == pBaseObject->ClassID( ) )
			{
				pTriObject = static_cast<TriObject*>( pBaseObject );
			}
			else if( pBaseObject && pBaseObject->CanConvertToType( Class_ID(TRIOBJ_CLASS_ID, 0) ) )
			{
				delTriObject = true;
				pTriObject = static_cast<TriObject*>( pBaseObject->ConvertToType(0,Class_ID(TRIOBJ_CLASS_ID,0)) );
			}

			GetPolygonCount( 0, pTriObject, numFaces, numVerts );
			polycount = numFaces;
			if ( delTriObject )
			{
				pTriObject->DeleteThis( );
				pTriObject = NULL;
			}

			// Only add the polycount to the stats if it isn't a RSRef object and the object can be found in the map already
			if( m_mObjectStatsList.find( pNode->GetHandle( ) ) != m_mObjectStatsList.end( ) )
			{
				m_mObjectStatsList[pNode->GetHandle()].PolyCount = polycount;
			}
		}
	}

	TiXmlElement* pPolycountElem = new TiXmlElement( "polycount" );
	pElement->LinkEndChild( pPolycountElem );
	XmlUtil::SetAttr( pPolycountElem, "value", polycount );
}

/**
 * @brief Export additional light properties for IDE file.
 */
void
cSceneXmlExporter::ExportLightProperties( INode* pNode, TiXmlElement* pElement )
{
	assert( pNode );
	if ( !pNode )
		return;
	assert( pElement );
	if ( !pElement )
		return;

	LightObject* pLight = IsLight(pNode->EvalWorldState(0).obj);
	if ( !pLight )
		return;

	LightscapeLight* pPhotoLight = IsPhotometric( pNode->EvalWorldState(0).obj );
	if ( pPhotoLight )
		return;

	// Find attributes element and add to this.
	TiXmlElement* pPropertiesElem = new TiXmlElement( "properties" );
	pElement->LinkEndChild( pPropertiesElem );

	// RGB Colour
	Point3 colour;
	colour = pLight->GetRGBColor( 0 );
	TiXmlElement* pColourElem = new TiXmlElement( "property" );
	XmlUtil::SetAttr( pColourElem, "name", "Colour" );
	XmlUtil::SetAttr( pColourElem, "type", "rgba" );
	pColourElem->LinkEndChild( XmlUtil::CreateRGBAColour( colour ) );
	pPropertiesElem->LinkEndChild( pColourElem );

	TiXmlElement* pIntensityElem = new TiXmlElement( "property" );
	XmlUtil::SetAttr( pIntensityElem, "name", "Intensity" );
	XmlUtil::SetAttr( pIntensityElem, "type", "float" );
	XmlUtil::SetAttr( pIntensityElem, "value", pLight->GetIntensity( 0 ) );
	pPropertiesElem->LinkEndChild( pIntensityElem );

	// Edit GunnarD: We need these properties to always be defined!
//	if ( static_cast<GenLight*>(pLight)->GetUseAtten() || ( static_cast<GenLight*>(pLight)->GetUseAttenNear() ) )
	{
// 		float startAtten = pLight->GetAtten( 0, ATTEN1_START );
// 		TiXmlElement* pAttrStartElem = new TiXmlElement( "property" );
// 		XmlUtil::SetAttr( pAttrStartElem, "name", "Attenuation Start" );
// 		XmlUtil::SetAttr( pAttrStartElem, "type", "float" );
// 		XmlUtil::SetAttr( pAttrStartElem, "value", startAtten );
// 		pPropertiesElem->LinkEndChild( pAttrStartElem );

		float endAtten = pLight->GetAtten( 0, ATTEN_END );
		if(endAtten==0.0)
		{
			if(m_pULog)
			{
				char buffer[4096];
				sprintf(buffer, "Light %s has end attenuation (Range) of 0.0.\n", pNode->GetName());
				m_pULog->LogWarning(buffer, pNode->GetName());
			}
		}
		TiXmlElement* pAttrEndElem = new TiXmlElement( "property" );
		XmlUtil::SetAttr( pAttrEndElem, "name", "Attenuation End" );
		XmlUtil::SetAttr( pAttrEndElem, "type", "float" );
		XmlUtil::SetAttr( pAttrEndElem, "value", endAtten );
		pPropertiesElem->LinkEndChild( pAttrEndElem );
	}

	if ( static_cast<GenLight*>(pLight)->IsSpot( ) )
	{
		TiXmlElement* pAttrSpotElem = new TiXmlElement( "property" );
		XmlUtil::SetAttr( pAttrSpotElem, "name", "Is Spot" );
		XmlUtil::SetAttr( pAttrSpotElem, "type", "bool" );
		XmlUtil::SetAttr( pAttrSpotElem, "value", "true" );
		pPropertiesElem->LinkEndChild( pAttrSpotElem );

		TiXmlElement* pAttrHotspotElem = new TiXmlElement( "property" );
		XmlUtil::SetAttr( pAttrHotspotElem, "name", "Hotspot" );
		XmlUtil::SetAttr( pAttrHotspotElem, "type", "float" );
		XmlUtil::SetAttr( pAttrHotspotElem, "value", pLight->GetHotspot( 0 ) );
		pPropertiesElem->LinkEndChild( pAttrHotspotElem );

		TiXmlElement* pAttrFallsizeElem = new TiXmlElement( "property" );
		XmlUtil::SetAttr( pAttrFallsizeElem, "name", "Fallsize" );
		XmlUtil::SetAttr( pAttrFallsizeElem, "type", "float" );
		XmlUtil::SetAttr( pAttrFallsizeElem, "value", pLight->GetFallsize( 0 ) );
		pPropertiesElem->LinkEndChild( pAttrFallsizeElem );

		Texmap* pTexmap = static_cast<GenLight*>(pLight)->GetShadowProjMap( );
		if ( pTexmap && (pTexmap->ClassID() == Class_ID(BMTEX_CLASS_ID,0) ) )
		{
			std::string sMap = _T( static_cast<BitmapTex*>(pTexmap)->GetMapName() );
			Filesystem::cPath::normalise( sMap );

			TiXmlElement* pAttrMapElem = new TiXmlElement( "property" );
			XmlUtil::SetAttr( pAttrMapElem, "name", "Shadowmap" );
			XmlUtil::SetAttr( pAttrMapElem, "type", "string" );
			XmlUtil::SetAttr( pAttrMapElem, "value", sMap );
			pPropertiesElem->LinkEndChild( pAttrMapElem );
		}
	}

	if(IsRageLight(pNode))
	{
		IGameNode * pIGameNode =  m_iGameScene->GetIGameNode (pNode) ;
		IGameObject *pIGObject = pIGameNode->GetIGameObject();
		IPropertyContainer *pLightProps = pIGObject->GetIPropertyContainer();

		for(LightPropMap::const_iterator it = lightPropSceneXMLMap.begin();
			it!=lightPropSceneXMLMap.end();
			it++)
		{
			IGameProperty *pProp = pLightProps->QueryProperty((*it).first.data());
			if(pProp)
			{
				TiXmlElement* pAttrSpotElem = new TiXmlElement( "property" );
				XmlUtil::SetAttr( pAttrSpotElem, "name", (*it).second.data() );
				TCHAR *type = "unknown";
				MCHAR val[4096] = {0};
//				ZeroMemory(val, 4096);
				switch(pProp->GetType())
				{
				case IGAME_FLOAT_PROP:
					{
						type = "float";
						float floatval = -1;
						pProp->GetPropertyValue(floatval);
						sprintf_s(val,"%f", floatval);
					}
					break;
				case IGAME_POINT3_PROP:
					{
						type = "xyz";
						Point3 point3val;
						pProp->GetPropertyValue(point3val);
						pAttrSpotElem->LinkEndChild( XmlUtil::CreatePoint3Node( point3val ) );
					}
					break;
				case IGAME_INT_PROP:
					{
						type = "int";
						int intval = -1;
						pProp->GetPropertyValue(intval);
						sprintf_s(val,"%d", intval);
					}
					break;
				case IGAME_STRING_PROP:
					{
						type = "string";
						const MCHAR *stringval = NULL;
						pProp->GetPropertyValue(stringval);
						sprintf_s(val,"%s", stringval);
					}
					break;
				case IGAME_POINT4_PROP:
					{
						type = "xyzw";
						Point4 point4val;
						pProp->GetPropertyValue(point4val);
						pAttrSpotElem->LinkEndChild( XmlUtil::CreatePoint4Node( point4val ) );
					}
					break;
				default:
				case IGAME_UNKNOWN_PROP:
					/*Unkown property Type  */
					break;
				}
				XmlUtil::SetAttr( pAttrSpotElem, "type", type );
				if(strlen(val))
				{
					XmlUtil::SetAttr( pAttrSpotElem, "value", val );
				}
				pPropertiesElem->LinkEndChild( pAttrSpotElem );
			}
		}

		pIGameNode->ReleaseIGameObject();
	}
}

/**
 * @brief Write out custom ledge property (bit of a fudge).
 * We handle ledge_left, ledge_right and ledge_normal in here.
 */
void
cSceneXmlExporter::ExportLedgeProperty( INode* pNode, TiXmlElement* pElement, const char* ledge_property )
{
	Matrix3 localMat = pNode->GetObjectTM( 0 );
	Matrix3 parentMat = pNode->GetParentTM( 0 );
	localMat *= Inverse( parentMat );

	IParamBlock* p_Params = NULL;
	Object* pObj2 = pNode->GetObjectRef();
	MSPlugin* Iscriptobj = (MSPlugin*)pObj2->GetInterface(I_MAXSCRIPTPLUGIN);
	if ( Iscriptobj )
	{
		ReferenceTarget* delegate = Iscriptobj->get_delegate();
		if ( delegate )
		{
			int numRefs = delegate->NumRefs();

			for (int k = 0;k<numRefs;k++)
			{
				RefTargetHandle RefHandle = delegate->GetReference(k);

				if(	(RefHandle->ClassID() == Class_ID(PARAMETER_BLOCK_CLASS_ID,0)))
				{
					p_Params = (IParamBlock*)RefHandle;
				}
			}
		}
	}

	if(p_Params)
	{
		if ( 0 == stricmp( "ledge_left", ledge_property ) )
		{
			float width;
			Point3 left;
			p_Params->GetValue(1,0,width,FOREVER);

			left.x = ( -width / 2.0f );
			left.y = 0.0f;
			left.z = 0.0f;
			localMat.TransformPoints( &left, 1 );

			TiXmlElement* pLedgeLeft = new TiXmlElement( "custom" );
			XmlUtil::SetAttr( pLedgeLeft, "name", "ledge_left" );
			XmlUtil::SetAttr( pLedgeLeft, "type", "xyz" );
			TiXmlElement* pVector = new TiXmlElement( "vector" );
			XmlUtil::SetAttr( pVector, "x", left.x );
			XmlUtil::SetAttr( pVector, "y", left.y );
			XmlUtil::SetAttr( pVector, "z", left.z );
			pLedgeLeft->LinkEndChild( pVector );
			pElement->LinkEndChild( pLedgeLeft );
		}
		else if ( 0 == stricmp( "ledge_right", ledge_property ) )
		{
			float width;
			Point3 right;
			p_Params->GetValue(1,0,width,FOREVER);

			right.x = ( width / 2.0f );
			right.y = 0.0f;
			right.z = 0.0f;
			localMat.TransformPoints( &right, 1 );

			TiXmlElement* pLedgeRight = new TiXmlElement( "custom" );
			XmlUtil::SetAttr( pLedgeRight, "name", "ledge_right" );
			XmlUtil::SetAttr( pLedgeRight, "type", "xyz" );
			TiXmlElement* pVector = new TiXmlElement( "vector" );
			XmlUtil::SetAttr( pVector, "x", right.x );
			XmlUtil::SetAttr( pVector, "y", right.y );
			XmlUtil::SetAttr( pVector, "z", right.z );
			pLedgeRight->LinkEndChild( pVector );
			pElement->LinkEndChild( pLedgeRight );
		}
		else if ( 0 == stricmp( "ledge_normal", ledge_property ) )
		{
			Point3 normal = Point3(0.0f, 0.0f, 1.0f);
			localMat.SetTrans(Point3(0.0f, 0.0f, 0.0f));
			localMat.TransformVectors( &normal, 1 );
			normal = normal.Normalize( );

			TiXmlElement* pLedgeNormal = new TiXmlElement( "custom" );
			XmlUtil::SetAttr( pLedgeNormal, "name", "ledge_normal" );
			XmlUtil::SetAttr( pLedgeNormal, "type", "xyz" );
			TiXmlElement* pVector = new TiXmlElement( "vector" );
			XmlUtil::SetAttr( pVector, "x", normal.x );
			XmlUtil::SetAttr( pVector, "y", normal.y );
			XmlUtil::SetAttr( pVector, "z", normal.z );
			pLedgeNormal->LinkEndChild( pVector );
			pElement->LinkEndChild( pLedgeNormal );
		}
	}
}

/**
 * @brief Write out our material list to XML.
 */
void
cSceneXmlExporter::ExportMaterialList( TiXmlElement* pElement )
{
	const tMaterialConfig& matConfig = g_SceneXmlConfig.RetMaterialConfig();
	if ( !matConfig.bEnabled )
		return;
	assert( pElement );
	if ( !pElement )
		return;

	TiXmlElement* pMaterialsListElem = new TiXmlElement( "materials" );
	for ( itMaterialListConst itMat = m_mMaterials.begin();
		 itMat != m_mMaterials.end();
		 ++itMat )
	{
		TiXmlElement* pMatElem = (*itMat).second.ToXmlElement( "material" );
		if ( pMatElem )
			pMaterialsListElem->LinkEndChild( pMatElem );
	}
	pElement->LinkEndChild( pMaterialsListElem );
}

/**
 * @brief Converts a 3dsmax classname to lowercase, replacing spaces with underscores.
 *
 * This is required so that the 3dsmax classnames match the ones in the SceneXml.xml
 * configuration file.
 */
std::string
cSceneXmlExporter::MakeClassNameFriendly( const std::string& classname )
{
	if ( !is_printable( classname ) )
		return ("");

	std::string sClassName = classname;
	size_t it = sClassName.find(' ');
	while (it != sClassName.npos)
	{
		sClassName.replace(it, 1, "_");
		it = sClassName.find(' ');
	}

	// Now convert to lowercase
	transform(sClassName.begin(), sClassName.end(), sClassName.begin(), (int(*)(int))tolower);

	return ( sClassName );
}

/**
 * @brief Populate local material info from 3dsmax scene node.
 */
void
cSceneXmlExporter::PopulateMaterialsList( INode* pNode, TiXmlElement* pElement, Interface* pInterface )
{
	const tMaterialConfig& matConfig = g_SceneXmlConfig.RetMaterialConfig();
	if ( !matConfig.bEnabled )
		return;
	assert( pNode );
	assert( pElement );

	Mtl* pMaterial = pNode->GetMtl( );
	if ( !pMaterial )
		return;

	Object* pObject = pNode->GetObjectRef( );
	// In order to avoid modifiers in GetObjectRef...
	if ( HasAttributeClass( pNode, "Gta Object" ) && !IsXref( pNode ) )
		pObject = MaxUtil::GetBaseObject( pNode );

	bool deleteIt = false;
	TriObject* triObject = NULL;
	if (pObject->CanConvertToType(Class_ID(TRIOBJ_CLASS_ID, 0)))
	{
		triObject = (TriObject*)pObject->ConvertToType(0, Class_ID(TRIOBJ_CLASS_ID, 0));
		if (pObject != triObject)
		{
			deleteIt = true;
		}
	}

	if(triObject == NULL)
	{
		// B* 1123401 - fix to crash when serialising materials list for any object that
		// isn't convertible to Class_ID(TRIOBJ_CLASS_ID, 0)
		if(m_pULog)
		{
			std::stringstream errorStream;
			errorStream << "Unable to populate material list for node '" 
						<< pNode->GetName() 
						<< "' as it cannot be converted to a TriObject.";
			m_pULog->LogWarning(errorStream.str().c_str(), pNode->GetName());
		}
		return;
	}

	Mesh& meshObject = triObject->GetMesh();
	MtlID materialId = -1;
	std::map<int, int> materialIdMap;
	for (int i = 0; i < meshObject.numFaces; i++)
	{
		materialId = meshObject.getFaceMtlIndex(i);
		materialIdMap[materialId] = i;
	}

	if (deleteIt) triObject->DeleteThis();

	std::string fullname = pMaterial->GetFullName();
	cMaterial material( pMaterial );
	pElement->SetAttribute( "material", material.RetID().ToString( ).c_str() );

	if ( pMaterial->IsMultiMtl() )
	{
		for ( int nSubMtls = 0; nSubMtls < pMaterial->NumSubMtls(); ++nSubMtls )
		{
			Mtl* pSubMaterial = pMaterial->GetSubMtl( nSubMtls );
			if ( !pSubMaterial ) // valid state though, map is just set to "none"
				continue;

			// Determine whether this material already contains a sub
			// material with the associated id
			if ( materialIdMap.find(nSubMtls) == materialIdMap.end() )
				continue;

			cMaterial submaterial( pSubMaterial );
			submaterial.PopulateMaterialMapList( pSubMaterial );

			material.AddChildMaterial( submaterial );
		}
	}
	else
	{
		material.PopulateMaterialMapList( pMaterial );
	}

	// Add to material list.
	if ( m_mMaterials.end() != m_mMaterials.find( material.RetID().ToString() ) )
		return;
	m_mMaterials[material.RetID().ToString()] = material;
}

/**
* @brief Precache our selection set indexes so we don't redo for each INode.
*
* See differences between our IsDontExport and the version in the GTA3 plugin.
*/
void
cSceneXmlExporter::PrecacheSelectionSets( )
{
	std::vector<tSelectionSet>& selSets = g_SceneXmlConfig.RetSelectionSets( );

	INamedSelectionSetManager* pSelSetMngr = INamedSelectionSetManager::GetInstance();

	int iNumSelSets = pSelSetMngr->GetNumNamedSelSets();
	int iSelSet = -1;
	int iSelSetNumItems = -1;

	for ( std::vector<tSelectionSet>::iterator it = selSets.begin( );
		it != selSets.end( );
		++it )
	{
		for ( int i=0; i < iNumSelSets; i++ )
		{
			if ( 0 == strcmp( pSelSetMngr->GetNamedSelSetName(i), (*it).sName.c_str() ) )
			{
				(*it).nSetId = i;
				break;
			}
		}
	}
}

/**
 * @brief Return whether the current node has the specified attribute class.
 */
bool
cSceneXmlExporter::HasAttributeClass( INode* pNode, const std::string& classname )
{
	IFpMaxDataStore* pMaxDataStore = GetMaxDataStoreInterface();
	TSTR pStr = pMaxDataStore->GetAttrClass( pNode );
	std::string actualClassname( pStr );

	return ( actualClassname == classname );
}

/**
 * @brief Return attribute container for INode.
 */
dmat::AttributeInst*
cSceneXmlExporter::GetAttributes( INode* pNode )
{
	DataInstance *pData = theIMaxDataStore.AddData(pNode);
	bool bChanged;

	pData = theIMaxDataStore.GetDataHasModified(pNode,bChanged);

	dmat::AttributeInst *pInst = NULL;

	if(pData)
	{
		pInst = pData->GetAttributes();
	}

	return pInst;
}

/**
 * @brief Determine if the INode should be exported or not.
 *
 * @param pNode - pointer to 3dsmax INode.
 *
 * If the object is within a predefined selection set to ignore then we do not
 * export it to the SceneXml representation.
 */
bool
cSceneXmlExporter::IsDontExport( INode *pNode )
{
	// For each of our cached selection sets see if this node is in one
	// which we shouldn't export.
	INamedSelectionSetManager* pSelSetMngr = INamedSelectionSetManager::GetInstance();
	const std::vector<tSelectionSet>& selSets = g_SceneXmlConfig.RetSelectionSets( );
	for ( std::vector<tSelectionSet>::const_iterator it = selSets.begin();
		 it != selSets.end();
		 ++it )
	{
		int iSelSet = (*it).nSetId;
		// Ignore sets that are set to export.
		if ( (*it).bExport )
			continue;

		int iSelSetNumItems = pSelSetMngr->GetNamedSelSetItemCount( iSelSet );

		for ( int i = 0; i < iSelSetNumItems; i++ )
		{
			INode* pCompare = pSelSetMngr->GetNamedSelSetItem( iSelSet, i );

			if ( pCompare == pNode )
			{
				return true;
			}
		}
	}

	return false;
}

bool
cSceneXmlExporter::IsXref( INode* pNode )
{
	Object *pObj = pNode->GetObjectRef();
	TSTR xrefFile, xrefObj;

	if(pObj && pObj->SuperClassID()==SYSTEM_CLASS_ID && pObj->ClassID()==Class_ID(XREFOBJ_CLASS_ID_PART,0))
	{
		return true;
	}
	else if(pNode->GetUserPropString("xreffile", xrefFile) && pNode->GetUserPropString("xrefobj", xrefObj))
	{
		return true;
	}
	else
		return false;
}

bool
cSceneXmlExporter::IsRSref( INode* pNode )
{
	Object *pObj = pNode->GetObjectRef();
	TSTR xrefFile, xrefObj;

	// classid value copied from x:\gta5\src\dev\rage\framework\tools\dcc\current\max2011\scripts\pipeline\util\RSrefs.ms
	if(pObj && pObj->ClassID()==Class_ID(0x5f4d9171, 0x3501fe72))
	{
		return true;
	}
	else
		return false;
}

bool
cSceneXmlExporter::IsInternalRef( INode* pNode )
{
	Object *pObj = pNode->GetObjectRef();
	if (pObj && pObj->ClassID()==INTERNALREF_CLASS_ID)
		return true;
	else
		return false;
}

bool
cSceneXmlExporter::IsRageLight( INode* pNode )
{
	Object *pObj = pNode->GetObjectRef();
	if (pObj && pObj->ClassID()==RAGE_LGHT_CLASSID)
		return true;
	else
		return false;
}

bool
cSceneXmlExporter::IsVehicleLink(INode* pNode)
{
	return ( HasAttributeClass( pNode, "VehicleLink" ) );
}

bool
cSceneXmlExporter::IsPatrolLink(INode* pNode)
{
	return ( HasAttributeClass( pNode, "PatrolLink") );
}

bool
cSceneXmlExporter::IsPatrolNode( INode* pNode )
{
	return ( HasAttributeClass( pNode, "PatrolNode" ) );
}