//
// filename:	base64.h
// description:	
//
// references:	http://www.adp-gmbh.ch/cpp/common/base64.html
//				http://base64.sourceforge.net/b64.c
//				http://www.faqs.org/rfcs/rfc1113.html
//

#ifndef INC_CBASE64_H_
#define INC_CBASE64_H_

// --- Include Files ------------------------------------------------------------

// STL headers
#include <string>

// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

std::string base64_encode( unsigned char const* s, unsigned int len );
std::string base64_decode( std::string const& s );

// --- Globals ------------------------------------------------------------------

#endif // !INC_CBASE64_H_
