//
//
//    Filename: PatrolNode.h
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: 
//
//
#if !defined(INC_VEHICLE_NODE_H_)
#define INC_VEHICLE_NODE_H_

// Max SDK headers
#include <Max.h>
#include <istdplug.h>
#include <iparamm2.h>
// resources header
#include "resource.h"
// My headers
#include "IConnection.h"

// STD headers
#pragma warning (disable : 4786)
#include <vector>

//#define ONLY_LINEAR_PATHS

extern HINSTANCE hInstance;

TCHAR *GetString(int id);
ClassDesc* GetVehehicleNodeDesc();

using namespace rage;

class VehLink;

class ObjectValidator : public PBValidator
{
public:
	BOOL Validate(PB2Value& v)
	{
		// Only VehicleNodes are valid selections
		Object* pObj = ((INode*)v.r)->EvalWorldState(0).obj;
		if(pObj->CanConvertToType(Class_ID(TRIOBJ_CLASS_ID,0)))
			return TRUE;
		if(pObj->ClassID() == Class_ID(DUMMY_CLASS_ID,0))
			return TRUE;
		return FALSE;
	}
	void DeleteThis() {}
};

extern ObjectValidator theObjectValidator;

//
//   Class Name: PatrolNode
// Base Classes: HelperObject
//  Description: 
//    Functions: 
//
//
class PatrolNode : public HelperObject 
{
	friend class PatrolLink;

private:
	std::vector<RefTargetHandle> m_refList;

	static IObjParam*			m_ip;			//Access to the interface
	static HWND					m_hObjWnd;
	static HWND					m_hLinkWnd;
	static ICustButton*			m_pConnectButton;
	static ICustButton*			m_pMergeButton;
	static SelectModBoxCMode*	m_selectMode;
	static bool					m_creating;
	static PatrolLink*			m_pLinkEdited;

	s32						m_level;
	IParamBlock2*				m_pBlock2;

public:

	//Constructor/Destructor
	PatrolNode();
	~PatrolNode();

	// PatrolNode methods
	void PickConnectionTarget();
//	void PickConnectionMerge();
	PatrolLink* ConnectTo(INode *pNode);
//	void MergeWith(INode *pNode);
	class PatrolLink* GetLinkEdited() {return m_pLinkEdited;}
	class PatrolLink* GetLinkBetween(PatrolNode *pOther);
	class PatrolLink* GetOtherLink(PatrolLink* pLinkIgnore);
	INode *GetINode();

//	void SelectConnectingObjects(IParamBlock2 *pBlock, s32 id);
	s32 GetNumberOfLinks();
	// reference handling stuff
	s32 AddReference(RefTargetHandle rtarg);
	void RemoveReference(RefTargetHandle rtarg);
	void RefDeleted();

	// drawing
	float GetDrawingScaleFactor(TimeValue t, INode *pNode, ViewExp* pVpt);
	void Draw(TimeValue t, ViewExp *pVpt, INode *pNode);
	// UI
	void InitObjUI();
	void CloseObjUI();

	// Animatable methods
	int NumParamBlocks() {return 1;}
	IParamBlock2* GetParamBlock(int i) {assert(i == 0); return m_pBlock2;}
	IParamBlock2* GetParamBlockByID(short id) {assert(id == patrol_node_params); return m_pBlock2;}
	Class_ID ClassID() {return PATROL_NODE_CLASS_ID;}
	void GetClassName(TSTR& s) {s = _T("PatrolNode");}
	void BeginEditParams( IObjParam  *ip, ULONG flags,Animatable *prev);
	void EndEditParams( IObjParam *ip, ULONG flags,Animatable *next);
	void DeleteThis();// {delete this;}
	void DeleteAllLinks();
	bool GetBothLinks(PatrolLink* pLinks[2]);


	// ReferenceTarget methods
#if MAX_VERSION_MAJOR >= 9 
	ReferenceTarget* Clone(RemapDir &remap = DefaultRemapDir());
#else
	ReferenceTarget* Clone(RemapDir &remap = NoRemap());
#endif

	IOResult Load(ILoad *iload);
	IOResult Save(ISave *isave);
	int NumRefs();
	RefTargetHandle GetReference(int i);
	void SetReference(int i, RefTargetHandle pTarg);
	RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,  RefMessage message);

		
	//From BaseObject
	CreateMouseCallBack* GetCreateMouseCallBack();
	void GetLocalBoundBox(TimeValue t, INode* pNode, ViewExp* pVp, Box3& box);
	void GetWorldBoundBox(TimeValue t, INode* pNode, ViewExp* pVp, Box3& box);
	int Display(TimeValue t, INode* pNode, ViewExp *pVpt, int flags);
	int HitTest(TimeValue t, INode* pNode, int type, int crossing, int flags, IPoint2 *pSp, ViewExp *pVpt);
	TCHAR *GetObjectName() { return _T("PatrolNode"); }
	// Sub-Object stuff
	int HitTest(TimeValue t, INode* pNode, int type, int crossing, int flags, IPoint2 *p, ViewExp *pVpt, ModContext* mc);

	// From Object
	ObjectState Eval(TimeValue t) {return ObjectState(this);}
	void InitNodeName(TSTR &s) { s = _T("pnode"); }
	BOOL IsWorldSpaceObject() {return TRUE;}

	void UpdateAllLinks(Interface *ip);
	std::vector<RefTargetHandle>  GetRefListVector() { return m_refList; }

	bool IsDifferentFrom(PatrolNode* pNodeToCheck);
};


//
//   Class Name: VEHConnectionClassDesc
// Base Classes: ClassDesc
//  Description: 
//    Functions: 
//
//
class PatrolNodeClassDesc2 : public ClassDesc2
{

public:
	int IsPublic() {return 1;}
	void* Create(BOOL loading = FALSE);
	const TCHAR *ClassName() {return _T("PatrolNode");}
	SClass_ID SuperClassID() {return HELPER_CLASS_ID;}
	Class_ID ClassID() {return PATROL_NODE_CLASS_ID;}
	const TCHAR* Category() {return _T("Standard");}
	void ResetClassParams (BOOL fileReset);

	int BeginCreate(Interface *ip);
	int EndCreate(Interface *ip);

	// Hardwired name, used by MAX Script as unique identifier
	const TCHAR*	InternalName() { return _T("PatrolNode"); }
	HINSTANCE		HInstance()	{ return hInstance; }
};


#endif // INC_VEHICLE_NODE_H_
