//
//
//    Filename: Iladder.h
//     Creator: Greg Smith
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Interface header to the Stunt Jump Node
//
//

#ifndef INC_ILADDERNODE_H_
#define INC_ILADDERNODE_H_

#define LADDER_CLASS_ID		Class_ID(0x165300d0, 0x4c6a05db)
#define LADDERNODE_CLASS_ID	Class_ID(0x9b81dd4, 0x1ba9019b)

#include "resource.h"

TCHAR *GetString(int id);

// --- ObjPath data -------------------------------------------------------------------------------------------------

// reference ID's
#define LADDER_PBLOCK_REF_ID			0

#define MAX_NUM_LADDERNODE 3

//parameter block IDs
enum {ladder_params};

// reference ID's
#define LADDERNODE_PBLOCK_REF_ID	0
#define LADDERNODE_MESH_REF_ID 1

//parameter block IDs
enum { laddernode_params };

enum {	laddernode_length };

#endif // INC_ILADDERNODE_H_