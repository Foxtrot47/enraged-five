#include "RiverHelper.h"

/////////////////////////////////////////////////////////////////////////////////////////////////
class RiverHelperClassDesc : public ClassDesc2 
	/////////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	int 			IsPublic() { return TRUE; }
	void *			Create(BOOL loading = FALSE) { return new RiverHelper(); }
	const TCHAR *	ClassName() { return GetString(IDS_RIVER_HELPER_NAME); }
	SClass_ID		SuperClassID() { return HELPER_CLASS_ID; }
	Class_ID		ClassID() { return RIVER_HELPER_CLASS_ID; }
	const TCHAR* 	Category() { return GetString(IDS_CATEGORY); }

	const TCHAR*	InternalName() { return _T(GetString(IDS_RIVER_HELPER_NAME)); }
	HINSTANCE		HInstance() { return hInstance; }
};

/////////////////////////////////////////////////////////////////////////////////////////////////
static RiverHelperClassDesc RiverHelperDesc;

static ParamBlockDesc2* waterBaseBlockWave = CreateWaterBasePBlock2(&RiverHelperDesc, "River Helper Parameters");


ParamBlockDesc2 riverPBlockDesc (
								 water_river_params, 
								 _T("Extended Parameters"), 
								 IDS_RIVER_HELPER_NAME, 
								 &RiverHelperDesc, 
								 P_AUTO_CONSTRUCT | P_AUTO_UI | P_VERSION, 
								 WATER_HELPER_CURRENT_VERSION,
								 WATER_EXTEND_PBLOCK_REF,
								 IDD_PANEL_RIVER, IDS_EXTENDED_PARAMS, 0, 0, NULL,

								 water_type, _T("Type"), TYPE_INT, P_ANIMATABLE, IDS_BUTTSHAPE,
									 p_default,	1,
									 p_range,	0,	4,
									 end,
								 end
								 );

/////////////////////////////////////////////////////////////////////////////////////////////////
ClassDesc2* GetRiverHelperDesc()
{
	return &RiverHelperDesc;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void RiverHelper::BeginEditParams( IObjParam *ip, ULONG flags,Animatable *prev)
{
	Water::BeginEditParams(ip,flags,prev);
	RiverHelperDesc.BeginEditParams(ip,this,flags,prev);
	RiverHelperDesc.SetUserDlgProc(&riverPBlockDesc, new WaterDlgProc(this));
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void RiverHelper::EndEditParams( IObjParam *ip, ULONG flags,Animatable *next)
{
	RiverHelperDesc.EndEditParams(ip,this,flags,next);
	Water::EndEditParams(ip,flags,next);
}

RiverHelper::RiverHelper(void)
	: Water(&RiverHelperDesc)
{
	RiverHelperDesc.MakeAutoParamBlocks(this);
	mLineColour = Point3(1.0f,1.0f,0.0f);
}

int RiverHelper::GetType()
{
	return Water::pExtendPblock2->GetInt(water_type, GetCOREInterface()->GetTime());
}

void RiverHelper::BuildSpecificMesh(TimeValue t, Mesh &mesh, Point3 centre, Point3 dim)
{

}

/////////////////////////////////////////////////////////////////////////////////////////////////
RefTargetHandle RiverHelper::Clone(RemapDir& remap) 
{
	RiverHelper* newob = new RiverHelper();
	newob->ReplaceReference(0,pBasePblock2->Clone(remap));
	newob->ReplaceReference(1,pExtendPblock2->Clone(remap));

	BaseClone(this, newob, remap);

	return(newob);
}