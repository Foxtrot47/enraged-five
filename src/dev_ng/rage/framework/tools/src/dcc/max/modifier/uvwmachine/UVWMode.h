//
//
//    Filename: UVWMode.h
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 18/05/99 15:44 $
//   $Revision: 2 $
// Description: CommandMode for UVWmachine mouse operations
//
//
#ifndef _UVW_MODE_H_
#define _UVW_MODE_H_

#include <Max.h>
// my headers
//#include <dma.h>

// command mode id
#define UVW_CID		CID_USER + 0x561


enum MouseOper
{
	MOUSE_ROTATE, 
	MOUSE_SCALE, 
	MOUSE_TRANSLATE, 
	MOUSE_SCALEU, 
	MOUSE_TRANSLATEU, 
	MOUSE_SCALEV, 
	MOUSE_TRANSLATEV
};

//
//   Class Name: UVWMode
// Base Classes: CommandMode
//				MouseCallBack
//  Description: CommandMode and MouseCallback for UVWmachine mouse operations
//    Functions: 
//
//
class UVWCMode : public CommandMode, MouseCallBack
{
private:
	Interface *m_ip;
	IPoint2 m_pt;
	MouseOper m_oper;
	class UVWmachine *m_pMod;
	bool m_isMouseDown;

public:
	void SetInterface(Interface *ip) {m_ip = ip;}
	void SetModifier(class UVWmachine *pMod) {m_pMod = pMod;}
	void SetOperation(MouseOper oper) {m_oper = oper;}

	// from CommandMode
	int Class() {return MODIFY_COMMAND;}
	int ID() {return UVW_CID;}
	ChangeForegroundCallback *ChangeFGProc() {return CHANGE_FG_SELECTED;}
	BOOL ChangeFG( CommandMode *pOldMode ) { return (pOldMode->ChangeFGProc() != CHANGE_FG_SELECTED); }

	MouseCallBack *MouseProc(int *pNumPoints) {*pNumPoints = 2; return this;}
	void EnterMode();
	void ExitMode();

	// MouseCallback
	int proc(HWND hwnd, int msg, int point, int flags, IPoint2 m );
};

extern UVWCMode theUVWCMode;



#endif // _UVW_MODE_H_