//
//
//    Filename: NodeCreateMgr.h
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Creation manager for Vehicle Nodes. Connects previously created Nodes to new Nodes
//
//
#if !defined(INC_NODE_CREATE_MGR_H_)
#define INC_NODE_CREATE_MGR_H_

#include <Max.h>

#define CID_VEHICLE_NODE	CID_USER + 0x382
//
//   Class Name: VehicleNodeCreateMgr
// Base Classes: 
//  Description: 
//    Functions: 
//
//
class VehicleNodeCreateMgr : public MouseCallBack, public ReferenceMaker, public PickNodeCallback
{
private:
	INode*			m_pThis;
	INode*			m_pCanConnectTo;
	VehicleNode*	m_pPrev;
	VehicleNode*	m_pVehicleNode;
	IObjCreate*		m_ip;

	bool			m_creating;

	static	VehicleNode*	m_pDefaultVehicleNode;
	static	VehicleLink*	m_pDefaultVehicleLink;
	ISpinnerControl*	m_pSpinLinkLen;


public:
	VehicleNodeCreateMgr();
	~VehicleNodeCreateMgr();

	void Begin( IObjCreate *ioc, ClassDesc *desc );
	void End();

	// ReferenceMaker
	int NumRefs();
	RefTargetHandle GetReference(int i);
	void SetReference(int i, RefTargetHandle rtarg);
	RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,  RefMessage message);

	// MouseCallback
	int proc(HWND hwnd, int msg, int point, int flags, IPoint2 m);
	BOOL SupportAutoGrid() {return TRUE;}

	// PickNodeCallback
	BOOL Filter(INode *pNode);

	static void CreateVehicleNode(IObjCreate* ip, VehicleNode*& pVehicleNode);
	static void CreateVehicleNodeDefault(IObjCreate* ip, VehicleNode*& pVehicleNode, INode*& pINode, Matrix3* pMat, bool bCopyDefaultParams=true);
	static void CreateVehicleLink(IObjCreate* ip, VehicleLink*& pVehicleLink);
	static void CreateVehicleLinkDefault(IObjCreate* ip, VehicleLink*& pVehicleLink, INode*& pINode, bool bCopyDefaultParams=true);

	static HWND		m_hWndAutoCreate;
	static bool		m_bAutoCreate;
	static float	m_fAutoCreateDistance;
	static bool		m_bViewDifferent;

//	static	VehicleNode*	GetDefaultVehicleNode() { return &m_DefaultVehicleNode; }
//	static	VehicleLink*	GetDefaultVehicleLink() { return &m_DefaultVehicleLink; }
	static	VehicleNode*	GetDefaultVehicleNode() { return m_pDefaultVehicleNode; }
	static	VehicleLink*	GetDefaultVehicleLink()	{ return m_pDefaultVehicleLink; }

protected:
	void RemoveLink();
	void RemoveVehicleNode(INode* pNode);
	void AutoCreate(Interface* ip, VehicleNode* pNode1, VehicleNode* pNode2);
	void StopCreating(ViewExp *pVpt);

	VehicleLink* ConnectTo(VehicleNode* pNodeToConnect, INode *pINodeTarget);

	void InitUI();
	void CloseUI();
};

//
//   Class Name: VehicleConnectionCreateMode
// Base Classes: 
//  Description: 
//    Functions: 
//
//
class VehicleNodeCreateMode : public CommandMode
{
private:
	VehicleNodeCreateMgr m_createMgr;

public:

	void Begin( IObjCreate *ioc, ClassDesc *desc ) { m_createMgr.Begin( ioc, desc ); }
	void End() { m_createMgr.End(); }
	// from CommandMode
	int Class() {return CREATE_COMMAND;}
	int ID() {return CID_VEHICLE_NODE;}
	MouseCallBack *MouseProc(int *numPoints) {*numPoints = 10000; return &m_createMgr;}
	ChangeForegroundCallback *ChangeFGProc() { return CHANGE_FG_SELECTED; }
	BOOL ChangeFG( CommandMode *oldMode ) { return (oldMode->ChangeFGProc() != CHANGE_FG_SELECTED); }
	void EnterMode() {}
	void ExitMode() {}
};

extern VehicleNodeCreateMode theVehicleNodeCreateMode;

#endif // !defined(INC_NODE_CREATE_MGR_H_)