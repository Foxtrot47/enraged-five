//
//
//    Filename: StuntJump.h
//     Creator: Greg Smith
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: class describing a stunt jump
//
//

#ifndef INC_ladder_H_
#define INC_ladder_H_

#pragma warning (disable : 4275)

#include <Max.h>
#include <istdplug.h>
#include <iparamm2.h>

//#include "resource.h"
#include "Iladder.h"

extern HINSTANCE hInstance;

TCHAR *GetString(int id);

class ladderNode;

using namespace rage;

enum FxType
{
	FX_LADDER,

};

#ifdef LADDER_EXPORTS
#define LADDER_EXPORTS_API __declspec(dllexport)
#else
#define LADDER_EXPORTS_API __declspec(dllimport)
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////
class LADDER_EXPORTS_API ladder : public HelperObject
///////////////////////////////////////////////////////////////////////////////////////////////////
{
private:
	friend class CreatePathMouseCallback;

	static HWND m_hPanel;
	static HWND m_hNodePanel;
	static bool m_creating;
	static MoveModBoxCMode *m_moveMode;
	static SelectModBoxCMode *m_selectMode;
	static Object* m_pNodeEdited;

	IParamBlock2* m_pBlock2;
	ladderNode* m_pPathNodes[MAX_NUM_LADDERNODE];
	Control* m_pControls[MAX_NUM_LADDERNODE];
	bool m_nodeSelected[MAX_NUM_LADDERNODE];
	s32 m_selLevel;

public:
	static IObjParam* m_ip;

	//Constructor/Destructor
	ladder();
	~ladder();

	// Sub Object creation etc
	s32 AddPathNode();
	void SetPathNodePosn(s32 i, Point3 posn);
	void SelectPathNode(s32 i, bool sel = true) {m_nodeSelected[i] = sel;}
	Point3 GetPathNodePosn(s32 i);
	Matrix3 GetPathNodeMatrix(s32 i);
	Object* GetPathNode(s32 i);
	void DeletePathNode(s32 i);
	s32 GetPathNodeId(Object *pPathNode);
	s32 GetControlId(Control *pControl);
	void TidyAfterDelete(s32 i);
	int HitTest(TimeValue t, INode* inode, IPoint2 p, s32 flags, ViewExp *vpt, s32 ignore = -1);
	void Snap(TimeValue t, INode* inode, SnapInfo *snap, IPoint2 *p, ViewExp *vpt);
	static void SetSelectedPathNodeData();

	// drawing
	void Draw(TimeValue t, INode* pNode, ViewExp *pVpt, int flags);

	// UI
	void InitPathUI();
	void UpdatePathUI();
	void ClosePathUI();
	void InitPathNodeUI();
	void UpdatePathNodeUI();
	void ClosePathNodeUI();

	// Animatable methods
	int NumParamBlocks() {return 1;}
	IParamBlock2* GetParamBlock(int i) {assert(i == 0); return m_pBlock2;}
	IParamBlock2* GetParamBlockByID(short id) {assert(id == path_params); return m_pBlock2;}
	Class_ID ClassID() {return LADDER_CLASS_ID;}
	void GetClassName(TSTR& s) {s = _T(GetString(IDS_FXLADDER));}
	void BeginEditParams( IObjParam  *ip, ULONG flags,Animatable *prev);
	void EndEditParams( IObjParam *ip, ULONG flags,Animatable *next);
	void DeleteThis() {delete this;}

	// sub anims
	int NumSubs();
	Animatable* SubAnim(int i);
	MSTR SubAnimName(int i);

	// ReferenceTarget methods
#if MAX_VERSION_MAJOR >= 9 
	ReferenceTarget* Clone(RemapDir &remap = DefaultRemapDir());
#else
	ReferenceTarget* Clone(RemapDir &remap = NoRemap());
#endif

	IOResult Load(ILoad *iload);
	IOResult Save(ISave *isave);
	int NumRefs();
	RefTargetHandle GetReference(int i);
	void SetReference(int i, RefTargetHandle pTarg);
	RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,  RefMessage message);

	//From BaseObject
	CreateMouseCallBack* GetCreateMouseCallBack();
	void GetLocalBoundBox(TimeValue t, INode* pNode, ViewExp* pVp, Box3& box);
	void GetWorldBoundBox(TimeValue t, INode* pNode, ViewExp* pVp, Box3& box);
	int Display(TimeValue t, INode* pNode, ViewExp *pVpt, int flags);
	int HitTest(TimeValue t, INode* pNode, int type, int crossing, int flags, IPoint2 *pSp, ViewExp *pVpt);
	TCHAR *GetObjectName() { return _T(GetString(IDS_FXLADDER)); }
	int NumSubObjTypes() {return 1;}
	ISubObjType *GetSubObjType(int i);
	void SelectAll( int selLevel );

	// From Object
	ObjectState Eval(TimeValue t);
	Interval ObjectValidity(TimeValue t);
	void InitNodeName(TSTR &s) { s = _T(GetString(IDS_FXLADDER)); }

	// sub-object stuff
	void Move(TimeValue t, Matrix3& partm, Matrix3& tmAxis, Point3& val, BOOL localOrigin);
	int HitTest(TimeValue t, INode* inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt, ModContext* mc);
	void SelectSubComponent(HitRecord *hitRec, BOOL selected, BOOL all, BOOL invert);
	void ClearSelection(int selLevel);
	int SubObjectIndex(HitRecord *hitRec);
	void GetSubObjectCenters(SubObjAxisCallback *cb,TimeValue t,INode *node,ModContext *mc);
	void GetSubObjectTMs(SubObjAxisCallback *cb,TimeValue t,INode *node,ModContext *mc);
	void ActivateSubobjSel(int level, XFormModes& modes);
};



///////////////////////////////////////////////////////////////////////////////////////////////////
class ladderClassDesc : public ClassDesc2
///////////////////////////////////////////////////////////////////////////////////////////////////
{

public:
	int IsPublic() {return 1;}
	void* Create(BOOL loading = FALSE);
	const TCHAR *ClassName() {return _T(GetString(IDS_FXLADDER));}
	SClass_ID SuperClassID() {return HELPER_CLASS_ID;}
	Class_ID ClassID() {return LADDER_CLASS_ID;}
	const TCHAR* Category() {return _T("Gta");}
	void ResetClassParams (BOOL fileReset);

	int BeginCreate(Interface *ip);
	int EndCreate(Interface *ip);

	const TCHAR*	InternalName() { return _T(GetString(IDS_FXLADDER)); }
	HINSTANCE		HInstance()	{ return hInstance; }
};




#endif //INC_ladder_H_