//
// filename:	cSceneXmlExporterDesc.cpp
// description:	
//

// --- Include Files ------------------------------------------------------------

// Max Headers
#include "max.h"

// SceneXml Headers
#include "cSceneXmlExporterDesc.h"

// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

// --- Globals ------------------------------------------------------------------

// --- Static Globals -----------------------------------------------------------

static cSceneXmlExporterDesc SceneXmlExporterDesc;

cSceneXmlExporterDesc* GetSceneXmlExporterDesc()
{
	return ( &SceneXmlExporterDesc );
}

// --- Static class members -----------------------------------------------------

// --- Code ---------------------------------------------------------------------

int
cSceneXmlExporterDesc::IsPublic( ) 
{ 
	return 1; 
}

void* 
cSceneXmlExporterDesc::Create( BOOL loading ) 
{ 
	return new cSceneXmlExporter( );
}

const TCHAR*
cSceneXmlExporterDesc::ClassName() 
{ 
	return GetStringResource( g_hInstance, IDS_SCENEXML_CLASSNAME ); 
}

SClass_ID
cSceneXmlExporterDesc::SuperClassID() 
{ 
	return UTILITY_CLASS_ID; 
}

Class_ID
cSceneXmlExporterDesc::ClassID() 
{
	return SCENEXML_CLASS_ID;
}

const TCHAR* 	
cSceneXmlExporterDesc::Category() 
{
	return _T( GetStringResource( g_hInstance, IDS_SCENEXML_CATEGORY ) );
}

const TCHAR*	
cSceneXmlExporterDesc::InternalName() 
{ 
	return _T( "SceneXmlExporter" ); 
}

HINSTANCE
cSceneXmlExporterDesc::HInstance() 
{ 
	return g_hInstance; 
}
