//
//
//    Filename: PatrolNode.cpp
//     Creator: SDKAPWZ
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: 
//
//
#include "PatrolNode.h"
#include "NodeCreateMgr.h"
#include "PatrolLink.h"
#include "PickPatrolConnection.h"
#include "IMaxDataStore.h"

#include "DataInstance.h"
#include "Attribute.h"

#define LOAD_SPECIAL_STRINGS_MANUALY
#define USE_COMBO_FOR_SPECIAL
#if defined (USE_COMBO_FOR_SPECIAL)
#if !defined(LOAD_SPECIAL_STRINGS_MANUALY)
#error Need to define LOAD_SPECIAL_STRINGS_MANUALY
#endif //!defined(LOAD_SPECIAL_STRINGS_MANUALY)
#endif	// defined (USE_COMBO_FOR_SPECIAL)

#define NUM_CIRCLE_SEGMENTS	16
#define VEH_NODE_CIRCLE_RADIUS	1
#define VEH_NODE_SCREEN_RATIO		1.0f/80.0f

// load/save chunk ids
#define VEH_CONNECTION_NUMREFS_CHUNK	0x762

static PatrolNodeClassDesc2 PatrolNodeDesc2;
static PickPatrolConnection theConnectPatrolPickMode;
static PickPatrolConnection theMergePatrolPickMode;

IObjParam *PatrolNode::m_ip = NULL;
HWND PatrolNode::m_hObjWnd = NULL;
HWND PatrolNode::m_hLinkWnd = NULL;
ICustButton *PatrolNode::m_pConnectButton = NULL;
ICustButton *PatrolNode::m_pMergeButton = NULL;
SelectModBoxCMode* PatrolNode::m_selectMode = NULL;
bool PatrolNode::m_creating = false;
PatrolLink* PatrolNode::m_pLinkEdited = NULL;

#if !defined( _WIN64 )
BOOL CALLBACK EditPatrolNodeProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
#else
INT_PTR CALLBACK EditPatrolNodeProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
#endif

ClassDesc* GetPatrolNodeDesc() {return &PatrolNodeDesc2;}

ObjectValidator theObjectValidator;

//
// Max3 paramblock2 version
//

ParamBlockDesc2 patrolNodeDesc(patrol_node_params, _T("PatrolNode parameters"), IDS_CLASS_NAME, &PatrolNodeDesc2, P_AUTO_CONSTRUCT | P_AUTO_UI, 0,
						IDD_PATROL_NODE_PARAMS_PANEL, IDS_PARAMS, 0, 0, NULL,
						end
						);

// --- VehConnectionClassDesc ------------------------------------------------------------------------------------

void* PatrolNodeClassDesc2::Create(BOOL loading) 
{
	return new PatrolNode;
}
//TODO: Should implement this method to reset the plugin params when Max is reset
void PatrolNodeClassDesc2::ResetClassParams (BOOL fileReset) 
{

}

int PatrolNodeClassDesc2::BeginCreate (Interface *ip) 
{
	IObjCreate *pIob = ip->GetIObjCreate();
	
	thePatrolNodeCreateMode.Begin( pIob, this );
	pIob->PushCommandMode( &thePatrolNodeCreateMode);
	
	return TRUE;
}

int PatrolNodeClassDesc2::EndCreate (Interface *ip) 
{
	thePatrolNodeCreateMode.End();
	ip->RemoveMode( &thePatrolNodeCreateMode );
	return TRUE;
}


//
//        name: grow_vector
// description: Template function that grows a vector and places a default value 
//          in: vec = reference to vector
//				size = size want vector to be
//				value = value to place in new entries
//         out: 
//
template<class T> void grow_vector(std::vector<T>& vec, s32 size, T value)
{
	while(vec.size() <= size)
		vec.push_back(value);
}


//--- PatrolNode -------------------------------------------------------

PatrolNode::PatrolNode() : m_level(0),m_pBlock2(NULL)
{
	PatrolNodeDesc2.MakeAutoParamBlocks(this);
}

PatrolNode::~PatrolNode()
{
	s32 i=0;
}


void PatrolNode::BeginEditParams(IObjParam *ip,ULONG flags,Animatable *prev)
{
	m_ip = ip;

	m_level = 0;	
	if (flags&BEGIN_EDIT_CREATE) {
		m_creating = true;
	} 
	else 
	{
		InitObjUI();

		m_creating = false;
	}
}

void PatrolNode::EndEditParams( IObjParam *ip, ULONG flags,Animatable *next )
{
	if(!m_creating)
	{
		CloseObjUI();
	}

	m_ip = NULL;
	m_creating = false;
}

RefTargetHandle PatrolNode::Clone(RemapDir& remap) 
{
	PatrolNode* pNewObj = new PatrolNode();
	BaseClone(this, pNewObj, remap);

	grow_vector(pNewObj->m_refList, NumRefs()-1, (RefTargetHandle)NULL);
	for(s32 i=0; i<NumRefs(); i++)
		pNewObj->ReplaceReference(i, remap.CloneRef(GetReference(i)));

/*	for(i=0; i<pNewObj->m_refList.size(); i++)
	{
		PatrolLink* pLink = (PatrolLink*)pNewObj->m_refList[i];
		s32 count = 0;

		if(pLink)
		{
			// Get the two nodes referencing the two Vehicle Connections referencing this link
			RefListItem *pItem = pLink->GetRefList().FirstItem();

			while(pItem)
			{
				if(pItem->maker->ClassID() == VEHICLE_CONNECTION_CLASS_ID)
					count++;
				pItem = pItem->next;
			}
			if(count != 2)
			{
				pLink->DeleteMe();
			}
		}
	}*/

	return pNewObj;
}

//
//        name: PatrolNode::Load/Save
// description: Load and Save number of links PatrolNode references. Have to do this or none of the links
//				are loaded
//
IOResult PatrolNode::Load(ILoad *iload)
{
	ULONG length;
	IOResult res;
	s32 numRefs;

	while (IO_OK == (res=iload->OpenChunk()))
	{
		switch(iload->CurChunkID())  
		{
		case VEH_CONNECTION_NUMREFS_CHUNK:
			res = iload->Read(&numRefs, sizeof(numRefs), &length);
			if(res == IO_OK)
				m_refList.assign(numRefs, NULL);
			break;
		}
		iload->CloseChunk();
		if (res!=IO_OK) 
			return res;
	}

	return IO_OK;
}

IOResult PatrolNode::Save(ISave *isave)
{
	IOResult res=IO_OK;
	ULONG length;
	s32 numRefs = m_refList.size();

	isave->BeginChunk(VEH_CONNECTION_NUMREFS_CHUNK);
	res = isave->Write(&numRefs, sizeof(numRefs), &length);
	isave->EndChunk();

	return res;
}

//
//        name: PatrolNode::AddReference,RemoveReference
// description: Extra reference handling functions. To allow the user to add a reference without indicating a position
//
s32 PatrolNode::AddReference(RefTargetHandle rtarg)
{
	PatrolLink* pLink=(PatrolLink*)rtarg;

	s32 i=0;
	for(i=0; i<m_refList.size(); i++)
	{
		if(m_refList[i] == NULL)
			break;
	}
	grow_vector(m_refList, i, (RefTargetHandle)NULL);

	ReplaceReference(i+1, rtarg);
	return i;
}

void PatrolNode::RemoveReference(RefTargetHandle rtarg)
{
	PatrolLink* pLink=(PatrolLink*)rtarg;

	for(s32 i=0; i<m_refList.size(); i++)
	{
		if(m_refList[i] == rtarg)
		{
			ReplaceReference(i+1, NULL);
			return;
		}
	}
	assert(0);
}

void PatrolNode::RefDeleted()
{
	INode* pINode = GetINode();
	PatrolLink *pLink;

	if(NULL == pINode)
	{
		// go thru list of links attached to this connection
		for(int i=0; i<m_refList.size(); i++)
		{
			if(m_refList[i] && m_refList[i]->ClassID() == PATROL_LINK_CLASS_ID)
			{
				pLink = (PatrolLink *)m_refList[i];
				RemoveReference(pLink);

			}
		}
	}
}

//
//        name: PatrolNode::NumRefs, GetReference, SetReference
// description: Standard reference handling functions
//
int PatrolNode::NumRefs() 
{
	return m_refList.size()+1;
}

RefTargetHandle PatrolNode::GetReference(int i) 
{
	if(i == 0)
	{
		return m_pBlock2;
	}
	else
	{
		i--;
		assert(i < m_refList.size());
		return m_refList[i];
	}
}

void PatrolNode::SetReference(int i, RefTargetHandle rtarg) 
{	

	if(i == 0)
	{
		m_pBlock2 = (IParamBlock2 *)rtarg;
	}
	else
	{
		i--;
		grow_vector(m_refList, i, (RefTargetHandle)NULL);
		m_refList[i] = rtarg;
	}
}

RefResult PatrolNode::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget,
										   PartID& partID,  RefMessage message) 
{
	switch (message)
	{
	case REFMSG_TARGET_DELETED:
		for(s32 i=0; i<m_refList.size(); i++)
		{
			if(m_refList[i] == hTarget)
			{
				assert(m_refList[i]->ClassID() == PATROL_LINK_CLASS_ID);
				PatrolLink* pLink = (PatrolLink* )m_refList[i];
				m_refList[i] = NULL;
			}
		}
	break;

	}
	return REF_SUCCEED;
}



//From BaseObject
CreateMouseCallBack* PatrolNode::GetCreateMouseCallBack() 
{
	return NULL;
}

//
//        name: PatrolNode::GetLocalBoundBox,GetWorldBoundBox
// description: Return bounding box in local coords or world coords
//
void PatrolNode::GetLocalBoundBox(TimeValue t, INode* pNode, ViewExp* pVpt, Box3& box)
{
	box.Init();
}

void PatrolNode::GetWorldBoundBox(TimeValue t, INode* pNode, ViewExp* pVpt, Box3& box)
{
	Matrix3 objMat = pNode -> GetObjectTM(t);
	float scaleFactor = GetDrawingScaleFactor(t, pNode, pVpt) * VEH_NODE_CIRCLE_RADIUS;
	float radius = 1.0f; //m_pBlock2->GetFloat(ai_radius);
	Box3 box2, linkBox;

	// get box surrounding radius circle
	box = Box3(Point3(-1.0f, -1.0f, -1.0f)*radius, Point3(1.0f, 1.0f, 1.0f)*radius);
	box = box * objMat;

	// add box surrounding static size circle
    box2 = Box3(Point3(-1.0f, -1.0f, -1.0f)*scaleFactor, Point3(1.0f, 1.0f, 1.0f)*scaleFactor);

	objMat.NoScale();
	objMat.NoRot();
	box += box2 * objMat;


	for(s32 i=0; i<m_refList.size(); i++)
	{
		if(m_refList[i])
		{
			((PatrolLink *)m_refList[i])->GetWorldBoundBox(t, pNode, pVpt, linkBox);
			box += linkBox;
		}
	}
}

//
//        name: PatrolNode::Display
// description: Display the object
//
int PatrolNode::Display(TimeValue t, INode* pNode, ViewExp *pVpt, int flags)
{
	GraphicsWindow *gw = pVpt->getGW();

	if(true == PatrolNodeCreateMgr::m_bViewDifferent)
	{
		if(true == IsDifferentFrom(PatrolNodeCreateMgr::GetDefaultPatrolNode()))
		{
			gw->setColor(LINE_COLOR, 0.0f, 0.0f, 5.0f);
			Draw(t, pVpt, pNode);
			return 0;
		}
	}

	if (pNode->Selected()) 
	{
		gw->setColor(LINE_COLOR, 1.0f, 0.0f, 0.0f);
	}
	else if(0 == GetNumberOfLinks()) 
	{
		gw->setColor(LINE_COLOR, 1.0f, 0.5f, 0.5f);
	}
	else if(!pNode->IsFrozen()) 
	{
		gw->setColor(LINE_COLOR, 0.0f, 1.0f, 0.0f);
	}
	Draw(t, pVpt, pNode);

	return 0;
}

//
//        name: PatrolNode::HitTest
// description: Test if object is under point
//
int PatrolNode::HitTest(TimeValue t, INode* pNode, int type, int crossing, int flags, IPoint2 *pSp, ViewExp *pVpt)
{
	HitRegion hitRegion;
	DWORD savedLimits;
	GraphicsWindow *gw = pVpt->getGW();

	MakeHitRegion(hitRegion, type, crossing, 4, pSp);

	gw->setRndLimits(((savedLimits = gw->getRndLimits()) | GW_PICK) & ~GW_ILLUM);
	gw->setHitRegion(&hitRegion);
	gw->clearHitCode();	

	Draw(t, pVpt, pNode);

	int res = gw->checkHitCode();
	if(1 == res)
	{
		int i = 0;
		i++;
	}

	gw->setRndLimits(savedLimits);

	return res;
}

//
//        name: PatrolNode::GetDrawingScaleFactor
// description: Return how much to scale PatrolNode to keep it the same no matter how far or near it is to
//				the camera
//          in: pNode = pointer to PatrolNode's INode
//				pVpt = pointer to viewport
//         out: scale factor
//
float PatrolNode::GetDrawingScaleFactor(TimeValue t, INode* pNode, ViewExp* pVpt)
{
	Matrix3 mtx = pNode->GetObjectTM(t);
	float nonScale = pVpt->NonScalingObjectSize();
	float worldWidth = pVpt->GetVPWorldWidth(mtx.GetTrans());

	return (nonScale*worldWidth) * VEH_NODE_SCREEN_RATIO;
}

//
//        name: PatrolNode::Draw
// description: Code to draw the Vehicle Connection. This is used by both the Display and HitTest functions
//
void PatrolNode::Draw(TimeValue t, ViewExp* pVpt, INode *pNode)
{
	GraphicsWindow *gw = pVpt->getGW();
	DrawLineProc lineProc(gw);

	float scaleFactor = GetDrawingScaleFactor(t, pNode, pVpt);
	Point3 pt[NUM_CIRCLE_SEGMENTS+1];
	float u;
	int i;
	float radius = 1.9f; // m_pBlock2->GetFloat(ai_radius);

	Matrix3 mtx = pNode->GetObjectTM(t);
	mtx.NoRot();
	gw->setTransform(mtx);

	// Camera facing
	float multiplier = float(TWOPI) / float(NUM_CIRCLE_SEGMENTS);
	for (i = 0; i <= NUM_CIRCLE_SEGMENTS; i++)
	{
		u = float(i) * multiplier;
		pt[i].x = (float)cos(u) * radius;
		pt[i].y = (float)sin(u) * radius;
		pt[i].z = 0.0f;
	}
	lineProc.proc(pt,NUM_CIRCLE_SEGMENTS+1);

	mtx.NoRot();
	mtx.NoScale();
	mtx.Scale(Point3(scaleFactor, scaleFactor, scaleFactor));
	gw->setTransform(mtx);

//	gw->setColor(LINE_COLOR, 0, 0, 0);
	for (i = 0; i <= NUM_CIRCLE_SEGMENTS; i++)
	{
		u = float(i) * multiplier;
		pt[i].x = (float)cos(u) * VEH_NODE_CIRCLE_RADIUS;
		pt[i].y = (float)sin(u) * VEH_NODE_CIRCLE_RADIUS;
		pt[i].z = 0.0f;

	//	pt[i]=pt[i]*invCamMat;
	}
	lineProc.proc(pt,NUM_CIRCLE_SEGMENTS+1);
}

//
//        name: PatrolNode::PickConnectionTarget
// description: Start the pick mode for picking another PatrolNode to connect to
//
void PatrolNode::PickConnectionTarget()
{
	theConnectPatrolPickMode.SetButton(m_pConnectButton);
	theConnectPatrolPickMode.SetPickOperation(PickPatrolConnection::CONNECT);
	theConnectPatrolPickMode.SetVehicleNode(this);
	m_ip->SetPickMode(&theConnectPatrolPickMode);
}

//
//        name: PatrolNode::PickConnectionTarget
// description: Start the pick mode for picking another PatrolNode to connect to
//
/*
void PatrolNode::PickConnectionMerge()
{
	theMergePatrolPickMode.SetButton(m_pMergeButton);
	theMergePatrolPickMode.SetPickOperation(PickPatrolConnection::MERGE);
	theMergePatrolPickMode.SetVehicleNode(this);
	m_ip->SetPickMode(&theMergePatrolPickMode);
}
*/
//        name: PatrolNode::GetLinkBetween
// description: Returns the link between the two nodes, if one exists
//          in: pOther = the other PatrolNode
//
PatrolLink* PatrolNode::GetLinkBetween(PatrolNode *pOther)
{
	s32 i;
	PatrolLink *pLink;

	// go thru list of links attached to this connection
	for(i=0; i<m_refList.size(); i++)
	{
		if(m_refList[i])
		{
			pLink = (PatrolLink *)m_refList[i];

			// parse dependents to find the other connection
#if (MAX_RELEASE >= 12000)
			DependentIterator pItem(pLink);
			ReferenceMaker* maker = NULL;

			while(NULL != (maker = pItem.Next()))
			{
				if(maker == pOther)
				{
					return pLink;
				}
			}
#else
			RefList &refList = pLink->GetRefList();
			RefListItem *pItem;

			pItem = refList.FirstItem();
			while(pItem)
			{
				if(pItem->maker == pOther)
				{
					return pLink;
				}
				pItem = pItem->next;
			}
#endif  //MAX_RELEASE >= 12000
		}
	}
	return NULL;
}


//        name: PatrolNode::GetOtherLink
// description: Returns the link that isnt the one passed, if one exists
//          in: pLinkThis = the link not to get
//
PatrolLink* PatrolNode::GetOtherLink(PatrolLink* pLinkIgnore)
{
	s32 i;
	PatrolLink *pLink;

	// go thru list of links attached to this Node
	for(i=0; i<m_refList.size(); i++)
	{
		if(m_refList[i])
		{
			pLink = (PatrolLink *)m_refList[i];
			if(pLink != pLinkIgnore)
			{
				return pLink;
			}
		}
	}
	return NULL;
}


//
//        name: *PatrolNode::GetINode
// description: Get the INode that references this object. Assumes only one INode ie object hasn't been instanced
//
INode* PatrolNode::GetINode()
{
	INode *pThisNode = NULL;

	// parse dependents to find the INode
#if (MAX_RELEASE >= 12000)
	ReferenceTarget *rt = (ReferenceTarget*)this;
	DependentIterator pItem(rt);
	ReferenceMaker* maker = NULL;

	while(NULL != (maker = pItem.Next()))
	{
		if((maker) && (maker->SuperClassID() == BASENODE_CLASS_ID))
		{
			pThisNode = (INode *)maker;
			break;
		}
	}
#else
	RefList &refList = GetRefList();
	RefListItem *pItem;

	pItem = refList.FirstItem();
	while(pItem)
	{
		if((pItem->maker) && (pItem->maker->SuperClassID() == BASENODE_CLASS_ID))
		{
			pThisNode = (INode *)pItem->maker;
			break;
		}
		pItem = pItem->next;
	}
#endif  //MAX_RELEASE >= 12000

	//assert(pThisNode);

	return pThisNode;
}

//
//        name: ConnectTo
// description: Connect this PatrolNode to the other node
//

PatrolLink* PatrolNode::ConnectTo(INode *pNode)
{
	PatrolNode *pOther = (PatrolNode *)pNode->EvalWorldState(0).obj;

	assert(pOther->ClassID() == PATROL_NODE_CLASS_ID);

	if(GetLinkBetween(pOther) != NULL)
		return NULL;

	INode *pThisNode = GetINode();

	theHold.Begin();

	PatrolLink *pLink = NULL; //(PatrolLink *)m_ip->CreateInstance(HELPER_CLASS_ID, PATROL_LINK_CLASS_ID);
	INode *pLinkNode = NULL; //m_ip->CreateObjectNode(pLink);
	PatrolNodeCreateMgr::CreateVehicleLinkDefault((IObjCreate*)m_ip, pLink, pLinkNode);

	pLink->ReplaceReference(0, pThisNode->GetTMController());
	pLink->ReplaceReference(1, pNode->GetTMController());
	AddReference(pLink);
	pOther->AddReference(pLink);

	NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
	pOther->NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);

	theHold.Accept("Link PatrolNode");

	m_ip->RedrawViews(m_ip->GetTime());

	return pLink;
}

//
//        name: PatrolNode::SelectConnectingObjects
// description: Select objects in connected list
//
/*
void PatrolNode::SelectConnectingObjects(IParamBlock2 *pBlock2, s32 id)
{
	INodeTab nodeTab;
	INode *pNode;
	s32 i;
	Interface *ip = m_ip;


	for(i=0; i<pBlock2->Count(id); i++)
	{
		pNode = pBlock2->GetINode(id, 0, i);
		if(pNode)
			nodeTab.Insert(i, 1, &pNode);
	}

	// have to copy m_ip because the PatrolNode is deselected and EndEditParams is called
	ip->ClearNodeSelection();
	ip->SelectNodeTab(nodeTab, TRUE);
}
*/
//
//        name: PatrolNode::GetNumberOfLinks
// description: Return number of connections linked to this one
//
s32 PatrolNode::GetNumberOfLinks()
{
	s32 i, count=0;
	// go thru list of links attached to this connection
	for(i=0; i<m_refList.size(); i++)
	{
		if(m_refList[i] && m_refList[i]->ClassID() == PATROL_LINK_CLASS_ID)
		{
			count++;
		}
	}
	return count;
}

//
//        name: InitObjUI, CloseObjUI
//
void PatrolNode::InitObjUI()
{
	if(m_hObjWnd == NULL)
	{
		m_hObjWnd = m_ip->AddRollupPage(
			hInstance, 
			MAKEINTRESOURCE(IDD_VEH_NODE_PANEL),
			EditPatrolNodeProc,
			_T("Patrol Node"),
			(LPARAM)this);
		m_pConnectButton = GetICustButton(GetDlgItem(m_hObjWnd, IDC_CONNECT_BUTTON));
		m_pConnectButton->SetType(CBT_CHECK);
		m_pConnectButton->SetHighlightColor(GREEN_WASH);
		EnableWindow(GetDlgItem(m_hObjWnd, IDC_MERGE_BUTTON), FALSE);
		EnableWindow(GetDlgItem(m_hObjWnd, IDC_SELECTCONNECT_BUTTON), FALSE);
		SetWindowText(GetDlgItem(m_hObjWnd, IDC_MERGE_BUTTON), " ");
		SetWindowText(GetDlgItem(m_hObjWnd, IDC_SELECTCONNECT_BUTTON), " ");
	}
}

void PatrolNode::CloseObjUI()
{
	if(m_hObjWnd)
	{
		m_ip->ClearPickMode();
		ReleaseICustButton(m_pConnectButton);
		ReleaseICustButton(m_pMergeButton);
		m_ip->DeleteRollupPage(m_hObjWnd);
		m_hObjWnd = NULL;
	}
}

//
//        name: PatrolNode::HitTest
// description: Testing for selection of PatrolLink's 
//
int PatrolNode::HitTest(TimeValue t, INode* pNode, int type, int crossing, 
						int flags, IPoint2 *p, ViewExp *pVpt, ModContext* mc)
{	
	PatrolLink* pLink;
	s32 i;

	int res = 0;
	// go thru list of links attached to this connection

	for(i=0; i<m_refList.size(); i++)
	{
		if(m_refList[i])
		{
			pLink = (PatrolLink* )m_refList[i];
			
/*			if(!(flags&HIT_SELONLY && !pLink->IsSelected()) &&
				!(flags&HIT_UNSELONLY && pLink->IsSelected())) 
*/			{
				if(pLink->HitTest(t, pNode,type,crossing,flags,p,pVpt))
				{
					pVpt->LogHit(pNode, mc, 0, i, NULL);
					res = TRUE;
					if(flags & HIT_ABORTONHIT) 
						return TRUE;
				}
			}
		}
	}
	return res;
}

// --- Windows callback ------------------------------------------------------------------------------------
#if !defined( _WIN64 )
BOOL CALLBACK EditPatrolNodeProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
#else
INT_PTR CALLBACK EditPatrolNodeProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
#endif
{
	static PatrolNode *pObject;
	if(pObject == NULL && message != WM_INITDIALOG)
		return TRUE;

	switch(message)
	{
	case WM_INITDIALOG:
		pObject = (PatrolNode *)lParam;
		break;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDC_CONNECT_BUTTON:
			pObject->PickConnectionTarget();
			break;
//		case IDC_SELECTCONNECT_BUTTON:
//			pObject->SelectConnectingObjects(pObject->GetParamBlockByID(patrol_node_params), aic_objectlist);
//			break;
		default:
			break;
		}
		break;

	case WM_CLOSE:
		break;

	default:
		return FALSE;
	}
	return TRUE;
}

void PatrolNode::DeleteThis()
{
//	DeleteAllLinks();
	delete this;
}

//
//        name: PatrolNode::DeleteAllLinks
// description: Deletes all the links referenced ny this node
//          in: 
//
void PatrolNode::DeleteAllLinks()
{
	s32 i;
	PatrolLink *pLink;

	// go thru list of links attached to this connection
	for(i=0; i<m_refList.size(); i++)
	{
		if(m_refList[i] && m_refList[i]->ClassID() == PATROL_LINK_CLASS_ID)
		{
			pLink = (PatrolLink *)m_refList[i];
			RemoveReference(pLink);
			pLink->Delete(this);

		}
	}
}

//
//        name: PatrolNode::UpdateAllLinks
// description: Updates all the links belonging to this Node
//				Only the position just now
//          in: 
//
void PatrolNode::UpdateAllLinks(Interface *ip)
{
	s32 i;
	PatrolLink *pLink;

	// go thru list of links attached to this connection
	for(i=0; i<m_refList.size(); i++)
	{
		if(m_refList[i] && m_refList[i]->ClassID() == PATROL_LINK_CLASS_ID)
		{
			pLink = (PatrolLink *)m_refList[i];
			pLink->SetPos(ip);

		}
	}
}

//
//        name: PatrolNode::GetBothLinks
// description: Fills array of 2 pointers to links
//				Useed by the Autocreate system so Node should have 2 links
//				If node doesnt have 2 links returns false				
//          in: array of pointers to store the links
//
bool PatrolNode::GetBothLinks(PatrolLink* pLinks[2])
{
	s32 i;
	s32 index = 0;
	// go thru list of links attached to this connection
	for(i=0; i<m_refList.size(); i++)
	{
		if(m_refList[i] && m_refList[i]->ClassID() == PATROL_LINK_CLASS_ID)
		{
			pLinks[index] = (PatrolLink *)m_refList[i];
			index++;
			if(index > 2)
			{
				return false;
			}
		}
	}
	return (index == 2);
}

//        name: PatrolNode::IsDifferentFrom
// description: If this node is different from the Node passed 
//				then return true.
//          in: Node to check differences with
//
bool PatrolNode::IsDifferentFrom(PatrolNode* pNodeToCheck)
{
	DataInstance* pThisData = theIMaxDataStore.GetData(this);
	DataInstance* pDefData = theIMaxDataStore.GetData(pNodeToCheck);
	if( (NULL ==pThisData) || (NULL == pDefData) )
	{
		return false;
	}
	dmat::AttributeInst* pThisAttr = pThisData->GetAttributes();
	dmat::AttributeInst* pDefAttr = pDefData->GetAttributes();

	dmat::AttributeClass defClass = pDefAttr->GetClass();
	dmat::AttributeClass thisClass = pThisAttr->GetClass();

	s32 sizeDef = defClass.GetSize();
	s32 sizeThis = defClass.GetSize();
	if(sizeThis != sizeDef)
	{
		return true;
	}
	s32 matching=0;
	for(s32 i=0; i<sizeDef; i++)
	{
		dmat::Attribute defAttr = defClass.GetItem(i);
		switch(defAttr.GetType())
		{
			case dmat::Attribute::INT :
			{
				s32 val1 = pDefAttr->GetAttributeValue(i);
				s32 val2 = pThisAttr->GetAttributeValue(i);
				{
					if(val1 == val2)
					{
						matching++;
					}
				}
			}
			break;
			case dmat::Attribute::FLOAT :
			{
				float val1 = pDefAttr->GetAttributeValue(i);
				float val2 = pThisAttr->GetAttributeValue(i);
				{
					if(val1 == val2)
					{
						matching++;
					}
				}
			}
			break;
			case dmat::Attribute::BOOL	:
			{
				bool val1 = pDefAttr->GetAttributeValue(i);
				bool val2 = pThisAttr->GetAttributeValue(i);
				{
					if(val1 == val2)
					{
						matching++;
					}
				}
			}
			break;
			case dmat::Attribute::STRING :
			{	
				char* val1 = pDefAttr->GetAttributeValue(i);
				char* val2 = pThisAttr->GetAttributeValue(i);
				{
					if(0 == strcmp(val1, val2))
					{
						matching++;
					}
				}
			}
			break;
		}
	}

	if(matching == sizeDef)
	{
		return false;
	}
	return true;
}
