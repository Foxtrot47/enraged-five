//
// File:: InternalRef.cpp
// Description::
//
// Author::
// Date::
//

// --- Include Files ------------------------------------------------------------

// STL headers
#include <string>

// Local headers
#include "InternalRef.h"

// --- Defines ------------------------------------------------------------------
#define PB_LENGTH			(0)
#define INST_REF_SIZE_CHUNK	(0xaca0)

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

class InternRefClassDesc: public ClassDesc 
{
public:
	int 			IsPublic() { return 1; }
	void *			Create(BOOL loading = FALSE) { return new InternalRefObject; }
	const TCHAR * ClassName() {return _T(GetStringResource(hInstance, IDS_CLASS_NAME));}
	SClass_ID		SuperClassID() { return HELPER_CLASS_ID; }
	Class_ID 		ClassID() { return INTERNALREF_CLASS_ID; }
	const TCHAR* 	Category() { return GetStringResource(hInstance, IDS_CATEGORY);  }
};

class InternRefObjPick : public PickModeCallback 
{
public:		

	BOOL HitTest(IObjParam *ip,HWND hWnd,ViewExp *vpt,IPoint2 m,int flags);
	BOOL Pick(IObjParam *ip,ViewExp *vpt);	

	void EnterMode(IObjParam *ip);
	void ExitMode(IObjParam *ip);

	HCURSOR GetHitCursor(IObjParam *ip);
	void SetRefdObj(InternalRefObject *l) { intRefObj = l; }

private:
	InternalRefObject *intRefObj;
};

class InternRefCreateCallBack : public CreateMouseCallBack 
{
private:
	IPoint2 sp0;
	Point3 p0;
	InternalRefObject *intRefObject;

public:
	int proc( ViewExp *vpt,int msg, int point, int flags, IPoint2 m, 
		Matrix3& mat);
	void SetObj(InternalRefObject *obj) {intRefObject = obj;}
};

// --- Globals ------------------------------------------------------------------
ICustButton *InternalRefObject::intRefPickButton = NULL;

// --- Static Globals -----------------------------------------------------------
static InternRefClassDesc intRefObjDesc;
static InternRefObjPick theSelRef;
static InternRefCreateCallBack intRefCreateCB;

// --- Static class members -----------------------------------------------------
HWND InternalRefObject::hRollup = NULL;
int InternalRefObject::dlgPrevSel = -1;
IObjParam* InternalRefObject::iObjParams;

// --- Code ---------------------------------------------------------------------

ClassDesc* 
GetIntRefObjDesc( ) 
{ 
	return &intRefObjDesc; 
}

BOOL
InternRefObjPick::HitTest(IObjParam *ip,HWND hWnd,ViewExp *vpt,IPoint2 m,int flags)
{
	INode *node = ip->PickNode(hWnd, m);
	if (node == NULL)
		return FALSE;
    Object* obj = node->EvalWorldState(0).obj;
	if ((obj->SuperClassID() == HELPER_CLASS_ID &&
         obj->ClassID() == INTERNALREF_CLASS_ID))
		return FALSE;
	return TRUE;
}

void
InternRefObjPick::EnterMode(IObjParam *ip)
{
	ip->PushPrompt(GetStringResource(hInstance, IDS_INTERN_REF_PICK_MODE));
}

void
InternRefObjPick::ExitMode(IObjParam *ip)
{
	ip->PopPrompt();
}

BOOL
InternRefObjPick::Pick(IObjParam *ip,ViewExp *vpt)
{
	if (vpt->HitCount() == 0)
		return FALSE;

	INode *node;
	if ((node = vpt->GetClosestHit()) != NULL) {

		intRefObj->ReplaceReference(0,node);
		HWND hw = intRefObj->hRollup;
		intRefObj->intRefPickButton->SetText(node->GetName());

		intRefObj->intRefPickButton->SetCheck(false);
	}
	intRefObj->iObjParams->RedrawViews(0,REDRAW_NORMAL,intRefObj);
	intRefObj->iObjParams->ClearPickMode();
	return FALSE;
}


HCURSOR InternRefObjPick::GetHitCursor(IObjParam *ip)
{
	return 0;
}


INT_PTR CALLBACK
RollupDialogProc( HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam )
{
	InternalRefObject *th = (InternalRefObject *)GetWindowLongPtr( hDlg, GWLP_USERDATA );	
	if ( !th && message != WM_INITDIALOG ) return FALSE;

	switch ( message ) {
	case WM_INITDIALOG: 
		{
			th = (InternalRefObject *)lParam;
			SetWindowLongPtr( hDlg, GWLP_USERDATA, (LONG)th );
			SetDlgFont( hDlg, th->iObjParams->GetAppHFont() );

			th->intRefPickButton = GetICustButton(GetDlgItem(hDlg,IDC_INT_REF_PICK));
			th->intRefPickButton->SetType(CBT_CHECK);
			th->intRefPickButton->SetButtonDownNotify(TRUE);
			th->intRefPickButton->SetHighlightColor(GREEN_WASH);

			// Now we need to fill in the list box IDC_LOD_LIST
			th->hRollup = hDlg;

			if (th->GetReference(0) != 0)
				th->intRefPickButton->SetText(th->intRefObjects.node->GetName());

			//EnableWindow(GetDlgItem(hDlg, IDC_LOD_DEL), (th->lodObjects.Count() > 0));
			th->dlgPrevSel = -1;
		}
		return TRUE;			

	case WM_DESTROY:
		th->iObjParams->ClearPickMode();
		th->previousMode = NULL;
		ReleaseICustButton( th->intRefPickButton );
		return FALSE;


	case WM_MOUSEACTIVATE:
		th->iObjParams->RealizeParamPanel();
		return FALSE;

	case WM_LBUTTONDOWN:
	case WM_LBUTTONUP:
	case WM_MOUSEMOVE:
		th->iObjParams->RollupMouseMessage(hDlg,message,wParam,lParam);
		return FALSE;

	case WM_COMMAND:			
		switch( LOWORD(wParam) ) 
		{
		case IDC_INT_REF_PICK: // Pick an object from the scene
			// Set the pickmode...
			switch (HIWORD(wParam)) {
			case BN_BUTTONDOWN:
				if (th->previousMode) {
					// reset the command mode
					th->iObjParams->SetCommandMode(th->previousMode);
					th->previousMode = NULL;
				} else {
					th->previousMode = th->iObjParams->GetCommandMode();
					theSelRef.SetRefdObj(th);
					th->iObjParams->SetPickMode(&theSelRef);
				}
				break;
			}
			break;
		}
		return FALSE;

	default:
		return FALSE;
	}
}

void InternalRefObject::BeginEditParams( IObjParam *ip, ULONG flags,Animatable *prev )
{
	iObjParams = ip;

	if ( !hRollup ) {
        hRollup = ip->AddRollupPage(
                hInstance,
                MAKEINTRESOURCE(IDD_INTERNALREFPARAM),
                RollupDialogProc,
                GetStringResource(hInstance, IDS_TITLE),
                (LPARAM)this );
 
        ip->RegisterDlgWnd(hRollup);
	} else {
		SetWindowLongPtr( hRollup, GWLP_USERDATA, (LONG_PTR)this );
	}
	dlgCurSelDist = -1.0f; // Start with no visible distance sphere
	
}
		
void InternalRefObject::EndEditParams( IObjParam *ip, ULONG flags,Animatable *prev)
{
	if ( flags&END_EDIT_REMOVEUI ) {		
        if (hRollup) {
            ip->UnRegisterDlgWnd(hRollup);
            ip->DeleteRollupPage(hRollup);
            hRollup = NULL;
        }
	} else {
		if (hRollup)
			SetWindowLongPtr( hRollup, GWLP_USERDATA, 0 );
	}
	
	iObjParams = NULL;
	dlgCurSelDist = -1.0f; // End with no visible distance sphere
}

InternalRefObject::InternalRefObject() 
	: HelperObject() 
	, m_bRefreshMaterial( true )
{
	SetReference( 0, NULL );
	dlgCurSelDist = -1.0f;
	previousMode = NULL;
}

InternalRefObject::~InternalRefObject()
{
	DeleteAllRefsToMe();
	DeleteAllRefsFromMe();
}

// This is only called if the object MAKES references to other things.
RefResult
InternalRefObject::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, 
     PartID& partID, RefMessage message ) 
{
	switch (message) 
	{
	case REFMSG_TARGET_DELETED:
		SetReference( 0, NULL );
		RefreshMaterial( );
		break;
	case REFMSG_SUBANIM_STRUCTURE_CHANGED:
		RefreshMaterial( );
		break;
	}
	return REF_SUCCEED;
}

void 
InternalRefObject::RefreshMaterial( )
{
	
	INode* pNode = NULL;
	DependentIterator di(this);  
	ReferenceMaker* p_RefMaker = NULL;
	while ( NULL!=(p_RefMaker=di.Next()) ) 
	{    
		if ( INODE_CLASS_ID == p_RefMaker->ClassID() )
		{
			pNode = (INode*)p_RefMaker;
			break;
		}
	}
	/*
	RefList& refList = GetRefList( );
	RefListItem& refItem = refList.FirstItem();
	while ( refItem )
	{
		if ( INODE_CLASS_ID == refItem->maker->ClassID() )
		{
			pNode = (INode*)refItem->maker;
			break;
		}
		refItem = refItem->next;
	}
	*/
	// Refresh render material
	if ( pNode )
	{
		if ( this->intRefObjects.node )
			pNode->ReplaceReference( NODE_MTL_REF_ID, this->intRefObjects.node->GetMtl() );
		else 
			pNode->ReplaceReference( NODE_MTL_REF_ID, NULL );
	}
	
}

RefTargetHandle
InternalRefObject::GetReference(int ind)
{
	if (ind != 0) 
		return NULL;

	return intRefObjects.node;
}

void
InternalRefObject::SetReference(int ind, RefTargetHandle rtarg)
{
	assert( 0 == ind );
	if ( 0 != ind )
		return;
	INode* p_Node = (INode*)rtarg;

	if(p_Node == NULL)
	{
		intRefObjects.node = NULL;
		return;
	}

	Object* pObj = p_Node->GetObjectRef();

	if ( ( pObj->ClassID() == Class_ID(EDITTRIOBJ_CLASS_ID,0) ) || 
		 ( pObj->ClassID() == Class_ID(EPOLYOBJ_CLASS_ID) ) )
	{
		intRefObjects.node = (INode *)rtarg;
	}
	else
	{
		intRefObjects.node = NULL;
	}
}

ObjectState
InternalRefObject::Eval(TimeValue time){
	return ObjectState(this);
}

Interval
InternalRefObject::ObjectValidity(TimeValue time)
{
	Interval ivalid;
	ivalid.SetInfinite();
	return ivalid;	
}

void
InternalRefObject::GetMat(TimeValue t, INode* inode, ViewExp* vpt, Matrix3& tm)
{
	tm = inode->GetObjectTM(t);
}

void
InternalRefObject::GetLocalBoundBox(TimeValue t, INode* inode, ViewExp* vpt, Box3& box )
{
	Mesh mesh;
	BuildMesh(mesh);             

	Matrix3 m = inode->GetObjectTM(t);
	box = mesh.getBoundingBox();
	if (dlgCurSelDist >= 0.0f) {
		Point3 x[32], y[32], z[32];
		GetDistPoints(dlgCurSelDist, x, y, z);
		for (int i = 0; i < 32; i++) {
			box += x[i];
			box += y[i];
			box += z[i];
		}
	}

}

void 
InternalRefObject::GetWorldBoundBox(TimeValue t, INode* inode, ViewExp* vpt, Box3& box )
{
	Matrix3 tm;
	Mesh mesh;
    BuildMesh(mesh);             
	GetMat(t,inode,vpt,tm);

	int nv = mesh.getNumVerts();
	box.Init();
	for (int i=0; i<nv; i++) 
		box += tm*mesh.getVert(i);
	if (dlgCurSelDist >= 0.0f) {
		Point3 x[32], y[32], z[32];
		GetDistPoints(dlgCurSelDist, x, y, z);
		for (int i = 0; i < 32; i++) {
			box += tm*x[i];
			box += tm*y[i];
			box += tm*z[i];
		}
	}
}

void 
InternalRefObject::BuildMesh(Mesh& mesh)
{
	INode* pNode = intRefObjects.node;
	if ( pNode )
	{
		Object* pObj = pNode->GetObjectRef();

		if ( Class_ID( EDITTRIOBJ_CLASS_ID, 0 ) == pObj->ClassID() )
		{
			TriObject* pTriObj;
			pTriObj = static_cast<TriObject*>( pObj );

			mesh = pTriObj->GetMesh();
		}
		else if ( Class_ID( EPOLYOBJ_CLASS_ID ) == pObj->ClassID() )
		{
			TriObject *pTriObj = static_cast<TriObject*>( pObj->ConvertToType(0, Class_ID(TRIOBJ_CLASS_ID,0)) );
			mesh = pTriObj->GetMesh();
			delete pTriObj;
		}
	}

	// If we don't have a valid node or failed to initialise a mesh above
	// then set a default representation mesh.
	if ( ( !pNode ) || ( 0 == mesh.numVerts ) || ( 0 == mesh.numFaces ) )
	{		
		mesh.setNumVerts(5);
		mesh.setNumFaces(4);

		mesh.setVert(0, Point3(-0.5,-0.5,0.000000));
		mesh.setVert(1, Point3(0.5,-0.5,0.000000));
		mesh.setVert(2, Point3(-0.5,0.5,0.000000));
		mesh.setVert(3, Point3(0.5,0.5,0.000000));
		mesh.setVert(4, Point3(0.0,0.0,0.500000));

		mesh.faces[0].setVerts(0, 1, 4);
		mesh.faces[0].setEdgeVisFlags(1,1,1);
		mesh.faces[0].setSmGroup(2);
		mesh.faces[1].setVerts(0, 2, 4);
		mesh.faces[1].setEdgeVisFlags(1,1,1);
		mesh.faces[1].setSmGroup(2);
		mesh.faces[2].setVerts(2, 4, 3);
		mesh.faces[2].setEdgeVisFlags(1,1,1);
		mesh.faces[2].setSmGroup(2);
		mesh.faces[3].setVerts(3, 4, 1);
		mesh.faces[3].setEdgeVisFlags(1,1,1);
		mesh.faces[3].setSmGroup(2);
	}

	assert( mesh.numVerts > 0 );
	assert( mesh.numFaces > 0 );
	// Prevent crashing when we hit the asserts above.
	if ( ( mesh.numVerts > 0 ) && ( mesh.numFaces > 0 ) )
	{
		mesh.InvalidateGeomCache();
		mesh.EnableEdgeList(1);
	}
}




void 
InternalRefObject::GetDistPoints(float radius, Point3 *x, Point3 *y, Point3 *z)
{
	float dang = PI / ( 2.0f * 8.0f);
	float ang = 0.0f;
	for (int i = 0; i < 32; i++ ) {
		z[i].x = x[i].y = y[i].x = radius * (float) cos(ang);
		z[i].y = x[i].z = y[i].z = radius * (float) sin(ang);
		z[i].z = x[i].x = y[i].y = 0.0f;
		ang += dang;
	}
}



void 
InternalRefObject::DrawDistSphere(TimeValue t, INode* inode, GraphicsWindow *gw)
{
	Matrix3 tm = inode->GetObjectTM(t);
	gw->setTransform(tm);

	Point3 x[33], y[33], z[33];
	GetDistPoints(dlgCurSelDist, x, y, z);
	
	if(!inode->IsFrozen())
		gw->setColor( LINE_COLOR, 0.0f, 0.0f, 1.0f);
	gw->polyline(32, x, NULL, NULL, TRUE, NULL);
	gw->polyline(32, y, NULL, NULL, TRUE, NULL);
	gw->polyline(32, z, NULL, NULL, TRUE, NULL);
}


int 
InternalRefObject::Display(TimeValue t, INode* inode, ViewExp *vpt, int flags)
{
	Mesh mesh;
	BuildMesh(mesh);             
	Matrix3 m;
	GetMat( t, inode, vpt, m );
	GraphicsWindow* gw = vpt->getGW( );
	DWORD rlim = gw->getRndLimits( );

	gw->setTransform( m );
	mesh.render( gw, inode->Mtls(), NULL, COMP_ALL, inode->NumMtls() );

	gw->setRndLimits( rlim );
	return ( 0 );
}

int InternalRefObject::HitTest(TimeValue t, INode *inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt)
{
	Mesh mesh;
	BuildMesh(mesh);             
	
	HitRegion hitRegion;
	DWORD	savedLimits;
	int res = FALSE;
	Matrix3 m;
	GraphicsWindow *gw = vpt->getGW();	
	Material *mtl = gw->getMaterial();

	MakeHitRegion(hitRegion,type,crossing,4,p);	
	gw->setRndLimits(((savedLimits = gw->getRndLimits()) | GW_PICK) & ~GW_ILLUM);
	GetMat(t,inode,vpt,m);

	gw->setTransform(m);
	gw->clearHitCode();
	
	gw->setRndLimits(savedLimits);
	
	if (mesh.select( gw, mtl, &hitRegion, flags & HIT_ABORTONHIT )) 
		return TRUE;
	
	return FALSE;
}


void InternalRefObject::Snap(TimeValue t, INode* inode, SnapInfo *snap, IPoint2 *p, ViewExp *vpt, Mesh *mesh)
{
	Matrix3 tm;
	tm = inode->GetObjectTM(t);	
	
	GraphicsWindow *gw = vpt->getGW();	
	gw->setTransform(tm);
		
	mesh->snap( gw, snap, p, tm );
}

int InternalRefObject::CanConvertToType(Class_ID obtype)
{
	if(intRefObjects.node == NULL) 
		return false;
	return (obtype == Class_ID(TRIOBJ_CLASS_ID,0)) ? 1 : 0;
}

Object* InternalRefObject::ConvertToType(TimeValue t, Class_ID obtype)
{
	if(obtype == Class_ID(TRIOBJ_CLASS_ID, 0) && (intRefObjects.node != NULL))
	{
		Object* pObj = intRefObjects.node->GetObjectRef();

		if(pObj->ClassID() == Class_ID(EDITTRIOBJ_CLASS_ID,0))
		{
			TriObject *pTri = CreateNewTriObject();
			pTri->mesh = ((TriObject*)pObj)->GetMesh();
			return pTri;
		}
		else if(pObj->ClassID() == Class_ID(EPOLYOBJ_CLASS_ID))
		{
			return static_cast<TriObject *>(pObj->ConvertToType(0, Class_ID(TRIOBJ_CLASS_ID,0)));
		}
	}

	return NULL;
}
	

// This method allows MAX to access and call our proc method to 
// handle the user input.
CreateMouseCallBack*
InternalRefObject::GetCreateMouseCallBack() 
{
	intRefCreateCB.SetObj(this);
	return(&intRefCreateCB);
}

IOResult InternalRefObject::Save(ISave *isave)
{
	ULONG nb;

	isave->BeginChunk(INST_REF_SIZE_CHUNK);
	isave->Write(&intRefObjects,sizeof(InternalRef), &nb);
	isave->EndChunk();

	return IO_OK;
}

IOResult InternalRefObject::Load(ILoad *iload)
{

	IOResult res;
	ULONG nb;

	InternalRef tempRef;

	while (IO_OK==(res=iload->OpenChunk())) {
		switch(iload->CurChunkID())  {
			case INST_REF_SIZE_CHUNK:	
				InternalRef obj;
				//res = iload->Read(&obj,sizeof(InternalRef), &nb);
				break;
		}
		iload->CloseChunk();
		if (res!=IO_OK) 
			return res;
	}

	return IO_OK;
}

RefTargetHandle
InternalRefObject::Clone(RemapDir& remap)
{
	InternalRefObject* ts = new InternalRefObject();
	ts->radius = radius;
	ts->ReplaceReference(0, intRefObjects.node);
	BaseClone(this, ts, remap);
	return ts;
}


int InternRefCreateCallBack::proc(ViewExp *vpt,int msg, int point, int flags, IPoint2 m, Matrix3& mat)
{	
	Point3 p1,center;
	
	if (msg==MOUSE_FREEMOVE)
	{
		vpt->SnapPreview (m, m, NULL, SNAP_IN_3D);//|SNAP_SEL_OBJS_ONLY);
		
	}
	
	if (msg==MOUSE_POINT || MOUSE_MOVE) 
	{		
		switch ( point ) 
		{
		case 0:  // only happens with MOUSE_POINT msg				

			sp0 = m;
			p0 = vpt->SnapPoint(m,m,NULL,SNAP_IN_3D);
			mat.SetTrans(p0);
			
			return CREATE_STOP;
			break;
		
		case 1:
			mat.IdentityMatrix();
			p1 = vpt->SnapPoint(m,m,NULL,SNAP_IN_3D);
			mat.SetTrans(p1);
			intRefObject->radius = Length(p1-p0);
			//if (intRefObject->sizeSpin)
			//	intRefObject->sizeSpin->SetValue( lodObject->radius, FALSE );
			if (flags&MOUSE_CTRL) {
				float ang = (float)atan2(p1.y-p0.y,p1.x-p0.x);					
				mat.PreRotateZ(intRefObject->iObjParams->SnapAngle(ang));
			}

			if (msg==MOUSE_POINT) {										
				return (Length(m-sp0)<3)?CREATE_ABORT:CREATE_STOP;
			}
			break;					   
		}
	}
		
	return TRUE;
}

// InternalRef.cpp
