#pragma warning (disable : 4786)

// max headers
#include <max.h>
// define the new primitives using macros from SDK
#if MAX_RELEASE > MAX_RELEASE_R13
#include <maxscript\MaxScript.h>
#include <maxscript\maxwrapper\maxclasses.h>
#include <maxscript\foundation\Numbers.h>
#include <maxscript\foundation\Strings.h>
#include <maxscript\macros\define_instantiation_functions.h>
#include <maxscript\util\listener.h>
#else
#include <maxscrpt\MaxScrpt.h>
#include <maxscrpt\MAXclses.h>
#include <maxscrpt\Numbers.h>
#include <maxscrpt\Strings.h>
#include <maxscrpt\definsfn.h>
#include <maxscrpt\listener.h>
#endif

#include <string>

#include "VehicleNode.h"

def_visible_primitive( vehnode_numconnections,"VehNodeNumConn");
def_visible_primitive( vehnode_getconnection,"VehNodeGetConn");

def_visible_primitive( vehlink_getnodea,"VehLinkGetNodeA");
def_visible_primitive( vehlink_getnodeb,"VehLinkGetNodeB");

/////////////////////////////////////////////////////////////////////////////////////
Value *vehnode_numconnections_cf(Value** arg_list, int count)
{
	//check we have the right number of parameters
	check_arg_count(vehnode_numconnections, 1, count);

	//need to make sure that the object passed in is an aiconnection
	type_check(arg_list[0], MAXNode, "VehNodeNumConn <vehiclenode>");

	INode *pNode = arg_list[0]->to_node();
	Object *pObj = pNode->EvalWorldState(0).obj;

	if(pObj->ClassID() != VEHICLE_NODE_CLASS_ID)
		throw RuntimeError ("VehNodeNumConn requires a VehicleNode : ", arg_list[0]);

	VehicleNode* pVehicleNode = (VehicleNode *)pObj;

	//if it is, then return the number of other nodes it is linked to

	int iNumRefs = pVehicleNode->NumRefs();
	int iNumConnRefs = 0;
	
	for(int i=0;i<iNumRefs;i++)
	{
		RefTargetHandle handle = pVehicleNode->GetReference(i);

		if(!handle)
		{
			continue;
		}

		Object* pObject = static_cast<Object*>(handle);

		if(pObject->ClassID() == VEHICLE_LINK_CLASS_ID)
		{
			iNumConnRefs++;
		}
	}

	return_protected(Integer::intern(iNumConnRefs));
}

/////////////////////////////////////////////////////////////////////////////////////
INode* getINode(VehicleNode* pFrom)
{
	INode *pThisNode = NULL;
#if (MAX_RELEASE >= 12000)
	DependentIterator pItem(pFrom);
	ReferenceMaker* maker = NULL;

	while(NULL != (maker = pItem.Next()))
	{
		if(maker->SuperClassID() == BASENODE_CLASS_ID)
		{
			pThisNode = (INode *)maker;
			break;
		}
	}
#else
	RefListItem *pItem = pFrom->GetRefList().FirstItem();

	while(pItem)
	{
		if(pItem->maker->SuperClassID() == BASENODE_CLASS_ID)
		{
			pThisNode = (INode *)pItem->maker;
			break;
		}
		pItem = pItem->next;
	}
#endif  //MAX_RELEASE >= 12000

	return pThisNode;
}

/////////////////////////////////////////////////////////////////////////////////////
Value *vehnode_getconnection_cf(Value** arg_list, int count)
{
	//check we have the right number of parameters
	check_arg_count(aiconn_numconnections, 2, count);

	//need to make sure that we have the correct parameters passed in
	type_check(arg_list[0], MAXNode, "VehNodeGetConn <vehnode> <index>");
	type_check(arg_list[1], Integer, "VehNodeGetConn <vehnode> <index>");

	//get the parameters
	INode *pNode = arg_list[0]->to_node();
	int iIndex = arg_list[1]->to_int();
	Object *pObj = pNode->EvalWorldState(0).obj;

	if(pObj->ClassID() != VEHICLE_NODE_CLASS_ID)
		throw RuntimeError ("VehNodeGetConn requires an VehicleNode: ", arg_list[0]);

	VehicleNode* pVehicleNode = (VehicleNode*) pObj;
	VehicleNode* pVehicleLinkedNode = NULL;

	//check the index isnt out of range
	int iNumRefs = pVehicleNode->NumRefs();

	if(iIndex > iNumRefs)
		throw RuntimeError ("index is out of range in VehNodeGetConn: ", arg_list[1]);

	//get the link

	Object* pLink = NULL;
	int iCurrentIndex = 0;

	for(int i=0;i<iNumRefs;i++)
	{ 
		RefTargetHandle handle = pVehicleNode->GetReference(i);

		if(!handle)
		{
			continue;
		}

		Object* pObject = static_cast<Object*>(handle);

		if(pObject->ClassID() == VEHICLE_LINK_CLASS_ID)
		{
			iCurrentIndex++;
		}

		if(iCurrentIndex == iIndex)
		{
			pLink = pObject;
			break;
		}
	}

	if(!pLink)
		throw RuntimeError ("Couldnt find link for index: ", arg_list[1]);	
	

	//get the linked connection
#if (MAX_RELEASE >= 12000)
	DependentIterator pItem(pLink);
	ReferenceMaker* maker = NULL;

	while(NULL != (maker = pItem.Next()))
	{
		if(maker->ClassID() == VEHICLE_NODE_CLASS_ID && maker != pVehicleNode)
		{
			pVehicleLinkedNode = static_cast<VehicleNode*>(maker);
			break;
		}
	}
#else
	RefListItem *pItem = pLink->GetRefList().FirstItem();

	while(pItem)
	{
		if(pItem->maker->ClassID() == VEHICLE_NODE_CLASS_ID && pItem->maker != pVehicleNode)
		{
			pVehicleLinkedNode = static_cast<VehicleNode*>(pItem->maker);
			break;
		}
		pItem = pItem->next;
	}
#endif  //MAX_RELEASE >= 12000

	//we now have the two aiconnections, we now need to find the distance between them
	//so we need to get the INodes for each object
	INode* p_NodeA = getINode(pVehicleNode);
	INode* p_NodeB = getINode(pVehicleLinkedNode);

	if(!p_NodeA || !p_NodeB)
		throw RuntimeError ("problem getting INode from VehicleNode: ", arg_list[0]);		

	return_protected(new MAXNode(p_NodeB));
}

VehicleNode* getNthVehicleNode(Object* p_Obj,int Index)
{
	int Count = 0;

	VehicleNode* pVehicleLinkedNode = NULL;

#if (MAX_RELEASE >= 12000)
	DependentIterator pItem(p_Obj);
	ReferenceMaker* maker = NULL;

	while(NULL != (maker = pItem.Next()))
	{
		if(maker->ClassID() == VEHICLE_NODE_CLASS_ID)
		{
			if(Count == Index)
				pVehicleLinkedNode = static_cast<VehicleNode*>(maker);

			Count++;
		}
	}
#else
	RefListItem *pItem = p_Obj->GetRefList().FirstItem();

	while(pItem)
	{
		if(pItem->maker->ClassID() == VEHICLE_NODE_CLASS_ID)
		{
			if(Count == Index)
				pVehicleLinkedNode = static_cast<VehicleNode*>(pItem->maker);

			Count++;
		}
		pItem = pItem->next;
	}
#endif  //MAX_RELEASE >= 12000

	return pVehicleLinkedNode;
}

/////////////////////////////////////////////////////////////////////////////////////
Value *vehlink_getnodea_cf(Value** arg_list, int count)
{
	type_check(arg_list[0], MAXNode, "VehLinkGetNodeA <vehlink>");
	INode *pNode = arg_list[0]->to_node();
	Object *pObj = pNode->EvalWorldState(0).obj;

	if(pObj->ClassID() != VEHICLE_LINK_CLASS_ID)
		throw RuntimeError ("VehLinkGetNodeA requires an VehicleLink: ", arg_list[0]);

	VehicleNode* pVehicleLinkedNode = getNthVehicleNode(pObj,0);

	if(!pVehicleLinkedNode)
		return &undefined;

	INode* p_NodeA = getINode(pVehicleLinkedNode);

	return_protected(new MAXNode(p_NodeA));
}

/////////////////////////////////////////////////////////////////////////////////////
Value *vehlink_getnodeb_cf(Value** arg_list, int count)
{
	type_check(arg_list[0], MAXNode, "VehLinkGetNodeA <vehlink>");
	INode *pNode = arg_list[0]->to_node();
	Object *pObj = pNode->EvalWorldState(0).obj;

	if(pObj->ClassID() != VEHICLE_LINK_CLASS_ID)
		throw RuntimeError ("VehLinkGetNodeA requires an VehicleLink: ", arg_list[0]);

	VehicleNode* pVehicleLinkedNode = getNthVehicleNode(pObj,1);

	if(!pVehicleLinkedNode)
		return &undefined;

	INode* p_NodeA = getINode(pVehicleLinkedNode);

	return_protected(new MAXNode(p_NodeA));
}