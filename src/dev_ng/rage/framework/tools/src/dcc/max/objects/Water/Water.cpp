
#include "Water.h"

#include "Max.h"
#include "resource.h"
#include "istdplug.h"
#include "iparamm2.h"
#include "Simpobj.h"

#define IMAGE_W 16
#define IMAGE_H IMAGE_W
#define WATER_MOD_FACTOR 10000
#define WATER_TILE_SIZE 4

#define MAX_POINTS 5

//////////////////////////////////////////////////////////////////////////

void WaterPostLoad::proc(ILoad *iload) 
{			
	wh->PostLoadHook(iload);
	delete this;
}

//////////////////////////////////////////////////////////////////////////

ParamBlockDesc2 *CreateWaterBasePBlock2(ClassDesc2 *cd, char *paramBlockName)
{
	return new ParamBlockDesc2(
		 water_base_params, 
		 _T(paramBlockName), 
		 IDS_WATER_NAME, 
		 cd, 
		 P_AUTO_CONSTRUCT | P_AUTO_UI, 
		 WATER_BASE_PBLOCK_REF,
		 IDD_PANEL_BASE, IDS_PARAMS, 0, 0, NULL,

			water_length,	_T("Length"),	TYPE_WORLD, P_ANIMATABLE, IDS_LENGTH,
			p_default,		4.0,
			p_range, 		0.0, 444444.0, 
			p_ui,			TYPE_SPINNER, EDITTYPE_INT, IDC_LENGTHEDIT, IDC_LENSPINNER, float(WATER_TILE_SIZE),
			end,
			water_width,	_T("Width"),	TYPE_WORLD, P_ANIMATABLE, IDS_WIDTH,
			p_default,		4.0,
			p_range, 		0.0, 444444.0, 
			p_ui,			TYPE_SPINNER,	EDITTYPE_INT, IDC_WIDTHEDIT, IDC_WIDTHSPINNER, float(WATER_TILE_SIZE),
			end, 
			water_snapGrid, _T("snapGrid"), TYPE_INT, P_ANIMATABLE, IDS_SNAPGRID,
			p_default,		WATER_TILE_SIZE,
			p_range, 		1, 1000, 
			p_ui,			TYPE_SPINNER, EDITTYPE_INT, IDC_SNAPGRID, IDC_SPIN_SNAPGRID, 1,
			end,
// 			water_mesh,		_T("points"),	TYPE_POINT3_TAB, 0, P_ANIMATABLE, IDS_POINTS,
// 			end,
		end
	);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
Water::Water(ClassDesc2 *derivedDesc):
	m_pTriObject(NULL),
	pBasePblock2(NULL),
	pExtendPblock2(NULL),
	mLineColour(1.0f,1.0f,1.0f)
{
	TriObject *pNewTri = CreateNewTriObject();
	ReplaceReference(WATER_MESH_REF,pNewTri);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
Water::~Water()
{
	//delete m_pTriObject;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
RefTargetHandle Water::GetReference(int i)
{
	switch(i)
	{
	case WATER_BASE_PBLOCK_REF:
		return pBasePblock2;
	case WATER_EXTEND_PBLOCK_REF:
		return pExtendPblock2;
 	case WATER_MESH_REF:
 		return m_pTriObject;
	}

	return NULL;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void Water::SetReference(int i, RefTargetHandle rtarg)
{

	switch (i)
	{
	case WATER_BASE_PBLOCK_REF:
		{
			IParamBlock2* pBlock = dynamic_cast<IParamBlock2*>(rtarg);
			pBasePblock2 = pBlock; 
		}
		break;
	case WATER_EXTEND_PBLOCK_REF:
		{
			IParamBlock2* pBlock = dynamic_cast<IParamBlock2*>(rtarg);
			pExtendPblock2 = pBlock; 
		}
		break;
 	case WATER_MESH_REF:
 		{
 			TriObject* pTriObject = dynamic_cast<TriObject*>(rtarg);
 			m_pTriObject = pTriObject; 
 		}
 		break;
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////
RefResult Water::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,RefMessage message)
{
	return REF_SUCCEED;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
ObjectState Water::Eval(TimeValue t)
{
	BuildMesh(t);
	return ObjectState(this);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void Water::BeginEditParams( IObjParam *ip, ULONG flags,Animatable *prev)
{
	HelperObject::BeginEditParams(ip,flags,prev);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void Water::EndEditParams( IObjParam *ip, ULONG flags,Animatable *next)
{
	HelperObject::EndEditParams(ip,flags,next);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void Water::GetWorldBoundBox(TimeValue t, INode* inode, ViewExp* vp, Box3& box )
{
	Matrix3 local2world = inode -> GetObjectTM(t);
	Box3 localBox;
	
	GetLocalBoundBox(t, inode, vp, localBox);

	box = localBox * local2world;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void Water::GetLocalBoundBox(TimeValue t, INode* inode, ViewExp* vp, Box3& box )
{
	if(m_pTriObject)
		box = m_pTriObject->mesh.getBoundingBox();
// 	int numPoints = pBasePblock2->Count(water_mesh);
// 	if(numPoints)
// 	{
// 		Point3 pt[MAX_POINTS];
// 		for(int i=0; i<numPoints; i++)
// 		{
// 			pBasePblock2->GetValue(water_mesh, t, pt[i], FOREVER, i);
// 		}
// 		box.IncludePoints(pt, numPoints);
// 	}
	else
		box.Init();
}

/////////////////////////////////////////////////////////////////////////////////////////////////
int Water::Display(TimeValue t, INode* inode, ViewExp *vpt, int flags)
{
	GraphicsWindow *gw = vpt->getGW();
	Matrix3 mat = inode->GetObjectTM(t);
	gw->setTransform(mat);

	DrawMesh(gw,t,mat);

	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
int Water::HitTest(TimeValue t, INode* inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt)
{
	HitRegion hitRegion;
	DWORD savedLimits;
	GraphicsWindow *gw = vpt->getGW();
	Matrix3 objMat = inode->GetObjectTM(t);
	MakeHitRegion(hitRegion, type, crossing, 4, p);

	gw->setTransform(objMat);
	gw->setRndLimits(((savedLimits = gw->getRndLimits()) | GW_PICK) & ~GW_ILLUM);
	gw->setHitRegion(&hitRegion);
	gw->clearHitCode();	

	DrawMesh(gw, t, objMat);

	int res = gw->checkHitCode();

	gw->setRndLimits(savedLimits);

	return res;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void Water::DrawMesh(GraphicsWindow *gw, TimeValue t, Matrix3& objMat)
{
	if(!m_pTriObject)
//	int numPoints = pBasePblock2->Count(water_mesh);
//	if(!numPoints)
	{
		return;
	}

	Mesh& mesh = m_pTriObject->mesh;

	DrawLineProc lineProc(gw);
	int i;
	Point3 pt[4];
	Point3 normal;
	Matrix3 invCamMat;
	float camMat[4][4];
	int persp;
	float hither, yon;

	// get face normal array built if not already built
	mesh.checkNormals(TRUE);

	gw->getCameraMatrix(camMat, &invCamMat, &persp, &hither, &yon);
	invCamMat.NoTrans();
	invCamMat.NoScale();
	objMat.NoTrans();
	objMat.NoScale();

	invCamMat = objMat * Inverse(invCamMat);

	lineProc.SetLineColor(mLineColour);
	for(i=0; i<mesh.numFaces; i++)
	{
// 		normal = mesh.getFaceNormal(i);
// 		if((normal * invCamMat)[2] < 0)
// 			continue;
		DWORD *vertindeces = mesh.faces[i].v;
		pt[0] = mesh.verts[vertindeces[0]];
		pt[1] = mesh.verts[vertindeces[1]];
		pt[2] = mesh.verts[vertindeces[2]];
		pt[3] = mesh.verts[vertindeces[0]];
		lineProc.proc(pt,4);
	}

	BitArray isoVerts = mesh.GetIsoVerts();
	for(int i=0; i<mesh.numVerts && i<MAX_POINTS; i++)
	{
		if(isoVerts[i])
			continue;
		Color currColour = mLineColour * ((float)GetAlpha(i)/255.0f);
		gw->setColor(FILL_COLOR, currColour);
		gw->setColor(TEXT_COLOR, mLineColour);
		gw->setColor(LINE_COLOR, currColour);
		gw->marker(&mesh.verts[i],DOT_MRKR);

		char label[8] = {0};
		sprintf_s(label, sizeof(label), "%i", GetAlpha(i));

// 		_itoa(i, &label[0], 10);

		gw->text(&mesh.verts[i],label);
	}



// 	gw->text(&mesh.verts[0],"1");
// 	gw->text(&mesh.verts[1],"2");
// 	gw->text(&mesh.verts[2],"3");


// 
// 	Point3 pt[MAX_POINTS];
// 	Point3 c[MAX_POINTS];
// 
// 	int bType = GetType();
// 	int order[5][4] = 
// 	{
// 		{0,1,3,2},
// 		{2,3,0,0},
// 		{0,1,2,2},
// 		{3,0,1,1},
// 		{1,2,3,3}
// 	};
// 	DWORD			lim		= gw->getRndLimits();
// // 	BOOL			resetLimit = lim & GW_Z_BUFFER;
// // 	if (resetLimit) 
// // 		gw->setRndLimits(lim & ~GW_Z_BUFFER);
// 	gw->setRndLimits(GW_ILLUM|GW_COLOR_VERTS|GW_SHADE_CVERTS);
// 	gw->setTransform(objMat);
// 	gw->startMarkers();
// 
// 	for(int i=0; i<numPoints && i<MAX_POINTS; i++)
// 	{
// 		pBasePblock2->GetValue(water_mesh, t, pt[i], FOREVER, order[bType][i] );
// 		c[i] = mLineColour * GetAlpha(order[bType][i]);
// 		gw->setColor(FILL_COLOR, c[i]);
// 		gw->setColor(TEXT_COLOR, c[i]);
// 		gw->setColor(LINE_COLOR, c[i]);
// 		gw->marker(&pt[i],DOT_MRKR);
// 
// 		char label[8];
// 		_itoa(order[bType][i], &label[0], 10);
// 		gw->text(&pt[i],label);
// 	}
// 	gw->endMarkers();
// 	gw->setColor(LINE_COLOR, Point3(0,0,0));//mLineColour);
// 	gw->polyline(numPoints, pt, c, NULL, true, NULL);
// 
// 	gw->setRndLimits(lim);

}

/////////////////////////////////////////////////////////////////////////////////////////////////
// static void MakeQuad(Face *f, int a, int b , int c , int d, int sg) 
// {
// 	int sm = 1<<sg;
// 
// 	f[0].setVerts( a, b, c);
// 	f[0].setSmGroup(sm);
// 	f[0].setEdgeVisFlags(1,1,0);
// }

/////////////////////////////////////////////////////////////////////////////////////////////////
// void Water::addBox(Point3 pntCentre,Point3 pntDimension,int& iVertOffset,int& iFaceOffset)
// {
// 	m_pTriObject->mesh.setVert(iVertOffset + 0, Point3(pntCentre.x - pntDimension.x,pntCentre.y + pntDimension.y,pntCentre.z));
// 	m_pTriObject->mesh.setVert(iVertOffset + 1, Point3(pntCentre.x + pntDimension.x,pntCentre.y + pntDimension.y,pntCentre.z));
// 	m_pTriObject->mesh.setVert(iVertOffset + 2, Point3(pntCentre.x - pntDimension.x,pntCentre.y - pntDimension.y,pntCentre.z));
// 	m_pTriObject->mesh.setVert(iVertOffset + 3, Point3(pntCentre.x + pntDimension.x,pntCentre.y - pntDimension.y,pntCentre.z));
// 
// 	int sm = 1<<1;
// 
// 	m_pTriObject->mesh.faces[0].setVerts( iVertOffset + 2, iVertOffset + 3, iVertOffset + 0);
// 	m_pTriObject->mesh.faces[0].setSmGroup(sm);
// 	m_pTriObject->mesh.faces[0].setEdgeVisFlags(1,1,0);
// 
// 	m_pTriObject->mesh.faces[1].setVerts( iVertOffset + 2, iVertOffset + 3, iVertOffset + 1);
// 	m_pTriObject->mesh.faces[1].setSmGroup(sm);
// 	m_pTriObject->mesh.faces[1].setEdgeVisFlags(1,1,0);
// 
// 	iVertOffset += 4;
// 	iFaceOffset += 2;
// }

/////////////////////////////////////////////////////////////////////////////////////////////////
void Water::BuildMesh(TimeValue t)
{
	int nverts;
	int nfaces;
	float l, w;
	int iVertOffset,iFaceOffset;
	int bType = GetType();

	float h1=0.0f,h2=0.0f,h3=0.0f,h4=0.0f;

	iVertOffset = iFaceOffset = 0;

	pBasePblock2->GetValue(water_length,t,l,FOREVER);
	pBasePblock2->GetValue(water_width,t,w,FOREVER);	

	if(w<0.0f) w = -w;
	if(l<0.0f) l = -l;


	w /= 2.0f;
	l /= 2.0f;

	Point3 pntCentre(0.0f,0.0f,0.0f);
	Point3 pntDimension(w,l,0.0f);
	Point3 verts[4];
	verts[0] = Point3(pntCentre.x - pntDimension.x,pntCentre.y + pntDimension.y,pntCentre.z + h1);
	verts[1] = Point3(pntCentre.x + pntDimension.x,pntCentre.y + pntDimension.y,pntCentre.z + h1);
	verts[2] = Point3(pntCentre.x - pntDimension.x,pntCentre.y - pntDimension.y,pntCentre.z + h1);
	verts[3] = Point3(pntCentre.x + pntDimension.x,pntCentre.y - pntDimension.y,pntCentre.z + h1);

	int orderLUT[5][6] = 
	{
		{0,1,2,1,3,2},
		{2,3,0,0,0,0},
		{0,1,2,0,0,0},
		{3,0,1,0,0,0},
		{1,2,3,0,0,0}
	};

	int *order = orderLUT[bType];
	Mesh& mesh = m_pTriObject->mesh;
	nverts = 4;

	if(bType>0)
	{
		nfaces = 1;
		
		mesh.setNumVerts(nverts);
		mesh.setNumFaces(nfaces);
		mesh.InvalidateTopologyCache();

		for(int k=0;k<nverts;k++)
		{
			mesh.setVert(k, verts[k]);
		}
		
		int sm = 1<<1;
		mesh.faces[0].setVerts( order[0], order[1], order[2] );
		mesh.faces[0].setSmGroup(sm);
		mesh.faces[0].setEdgeVisFlags(1,1,0);
	}
	else
	{
		nfaces = 2;
		
		mesh.setNumVerts(nverts);
		mesh.setNumFaces(nfaces);
		mesh.InvalidateTopologyCache();

		for(int k=0;k<nverts;k++)
		{
			mesh.setVert(k, verts[k]);
		}
		
		int sm = 1<<1;
		mesh.faces[0].setVerts( order[0], order[1], order[2]);
		mesh.faces[0].setSmGroup(sm);
		mesh.faces[0].setEdgeVisFlags(EDGE_VIS,EDGE_INVIS,EDGE_VIS);

		mesh.faces[1].setVerts( order[3], order[4], order[5]);
		mesh.faces[1].setSmGroup(sm);
		mesh.faces[1].setEdgeVisFlags(EDGE_INVIS,EDGE_VIS,EDGE_VIS);

		// WaterWave is only one that adds to mesh so far.  Only called from
		// in here when bType == 0
		BuildSpecificMesh(t, mesh, pntCentre, pntDimension);

	}

	mesh.setNumTVerts(0);
	mesh.setNumTVFaces(0);
	mesh.DeleteFlaggedFaces();
	mesh.InvalidateGeomCache();
// 	mesh.DeleteIsoVerts();
	mesh.BuildStripsAndEdges();

	// Vertex Data support for alphas
	mesh.setVDataSupport(1, true);

	pBasePblock2->SetValue(water_mesh, t, m_pTriObject);

	//---------------------------------
// 
// 	if(pBasePblock2->Count(water_mesh)<4)
// 	{
// 		int success = pBasePblock2->Resize(water_mesh, 4);
// 	}
// 	int success =	pBasePblock2->SetValue(water_mesh, t, verts[0], 0);
// 	success =		pBasePblock2->SetValue(water_mesh, t, verts[1], 1);
// 	success =		pBasePblock2->SetValue(water_mesh, t, verts[2], 2);
// 	success =		pBasePblock2->SetValue(water_mesh, t, verts[3], 3);
// 
// 	NotifyDependents(FOREVER,PART_ALL,REFMSG_CHANGE);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
class WaterCreateCallBack : public CreateMouseCallBack 
/////////////////////////////////////////////////////////////////////////////////////////////////
{
	Water *ob;
	Point3 p0,p1;
	IPoint2 sp0, sp1;
	BOOL square;
public:
	int proc( ViewExp *vpt,int msg, int point, int flags, IPoint2 m, Matrix3& mat );
	void SetObj(Water *obj) { ob = obj; }
};

float Water::SnapValue(float fVal)
{
	int shiftedVal = (fVal*WATER_MOD_FACTOR);
	Interface * pi = GetCOREInterface();
	float gridSize = pBasePblock2->GetInt(water_snapGrid, pi->GetTime()); //GetCOREInterface()->GetGridSpacing();
	int scaledTileSize = (gridSize*WATER_MOD_FACTOR);
	int tileModulo = shiftedVal%scaledTileSize;
	if(tileModulo<0)
	{
		if(abs(tileModulo)>(scaledTileSize/2))
		{
			shiftedVal = (shiftedVal-(scaledTileSize+tileModulo));
		}
		else
		{
			shiftedVal = (shiftedVal-tileModulo);
		}
	}
	else
	{
		if(tileModulo>(scaledTileSize/2))
		{
			shiftedVal = (shiftedVal+(scaledTileSize-tileModulo));
		}
		else
		{
			shiftedVal = (shiftedVal-tileModulo);
		}
	}
	return ((float)shiftedVal)/WATER_MOD_FACTOR;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
int WaterCreateCallBack::proc(ViewExp *vpt,int msg, int point, int flags, IPoint2 m, Matrix3& mat) 
{
	Point3 d;
	Point3 pTemp;
	if(msg == MOUSE_FREEMOVE)
	{
		vpt->SnapPreview(m,m,NULL, SNAP_IN_3D);
	}
	else if(msg==MOUSE_POINT||msg==MOUSE_MOVE) 
	{
		switch(point) 
		{
		case 0:
			sp0 = m;
			ob->pBasePblock2->SetValue(water_width,0,0.0f);
			ob->pBasePblock2->SetValue(water_length,0,0.0f);
			p0 = vpt->SnapPoint(m,m,NULL,SNAP_IN_3D);
			p0[0] = ob->SnapValue(p0[0]);
			p0[1] = ob->SnapValue(p0[1]);
			p0[2] = ob->SnapValue(p0[2]);

			mat.SetTrans(p0);				

			break;
		case 1:
			sp1 = m;
			p1 = vpt->SnapPoint(m,m,NULL,SNAP_IN_3D);
			p1[0] = ob->SnapValue(p1[0]);
			p1[1] = ob->SnapValue(p1[1]);
			p1[2] = ob->SnapValue(p1[2]);

			pTemp = float(.5)*(p0+p1);

			mat.SetTrans(pTemp);				

			d = p1 - p0;
			
			square = FALSE;

			if (flags&MOUSE_CTRL) 
			{
				// Constrain to square base
				float len;
				if (fabs(d.x) > fabs(d.y)) len = d.x;
				else len = d.y;
				d.x = d.y = 4.0f * len;
				square = TRUE;
			}
			
			ob->pBasePblock2->SetValue(water_width,0,ob->SnapValue(fabs(d.x)));
			ob->pBasePblock2->SetValue(water_length,0,ob->SnapValue(fabs(d.y)));

			waterPBlockDesc.InvalidateUI();
			
			if(msg==MOUSE_POINT) 
			{
				return CREATE_STOP;
			}

			break;
		}
	}
	else if (msg == MOUSE_ABORT) 
	{		
		return CREATE_ABORT;
	}
	
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
static WaterCreateCallBack waterCreateCB;
/////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////
CreateMouseCallBack* Water::GetCreateMouseCallBack() 
{
	waterCreateCB.SetObj(this);
	return(&waterCreateCB);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
BOOL Water::OKtoDisplay(TimeValue t) 
{
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void Water::InvalidateUI() 
{
	IParamMap2 *pMapParam = pBasePblock2->GetMap();
	if (pMapParam) 
		pMapParam->Invalidate();
}

//////////////////////////////////////////////////////////////////////////

INT_PTR WaterDlgProc::DlgProc(TimeValue t,IParamMap2 *map, HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
	switch (msg)   
	{ // Respond to the message ...     
		case WM_INITDIALOG: // Initialize the Controls here.   
			{
				m_pw->typeButton = GetICustButton(GetDlgItem (hWnd, IDC_BUTTSHAPE));
				if(m_pw->typeButton)
				{
					FlyOffData fod[5] = { // A three button flyoff
						{ 0,0,0,0 }, // The first button uses a single image.
						{ 1,1,1,1 }, // So does the second button...
						{ 2,2,2,2 }, // So does the third...
						{ 3,3,3,3 }, // So does the fourth...
						{ 4,4,4,4 }, // So does the fifth...
					};
					m_pw->typeButton->SetFlyOff(
						5, 
						fod, 
						GetCOREInterface()->GetFlyOffTime(), 
						0 /* Initial value */, 
						FLY_RIGHT);
					// Here we load up the images for the flyoff...
					m_pw->hImageFO = ImageList_Create(IMAGE_W, IMAGE_H, TRUE, 3, 3);
					HBITMAP hBitmap = LoadBitmap(hInstance, MAKEINTRESOURCE(IDB_WATER_SHAPES));
					HBITMAP hMask = LoadBitmap(hInstance, MAKEINTRESOURCE(IDB_WATER_SHAPES_A));
					ImageList_Add(m_pw->hImageFO, hBitmap, hMask);
					DeleteObject(hBitmap);
					DeleteObject(hMask);
					int currSelectedShape;
					m_pw->pExtendPblock2->GetValue(water_type, t, currSelectedShape, FOREVER);
					m_pw->typeButton->SetImage(m_pw->hImageFO, currSelectedShape,currSelectedShape,currSelectedShape,currSelectedShape, IMAGE_W, IMAGE_H);
				}
			}
			return TRUE;     
		case WM_DESTROY: // Release the Controls here.     
			if(m_pw->typeButton)
			{
				ReleaseICustButton(m_pw->typeButton);
				m_pw->typeButton = NULL;
			}
			return FALSE;
			case WM_COMMAND:     
				switch(LOWORD(wParam))     
				{ // Switch on ID      
		// 		case IDC_BUTTON: // A specific button's ID.         
		// 			switch (HIWORD(wParam)) 
		// 			{ // Notification codes      
		// 			case BN_BUTTONDOWN: // Button is pressed.         
		// 				break;      
		// 			case BN_BUTTONUP: // Button is released.         
		// 				break;      
		// 			case BN_RIGHTCLICK: // User right clicked.        
		// 				break;     
		// 			}; 
		// 			break;   
				case IDC_BUTTSHAPE: // A specific fly off control ID     
					switch (HIWORD(wParam))     
					{ // Notification codes      
					case BN_FLYOFF:     
						// This notification code is sent when the     
						// user chooses a new fly off. 
						if(m_pw->typeButton)
						{
							m_pw->pExtendPblock2->SetValue(water_type, t, m_pw->typeButton->GetCurFlyOff());
							m_pw->BuildMesh(GetCOREInterface()->GetTime());
						}
						break;     
					};     
					break;
				}
				break;
			case WM_NOTIFY: // Others this way...      
			break;     // Other cases...     
		default:     
			break;   
	}   
	return FALSE;
}
