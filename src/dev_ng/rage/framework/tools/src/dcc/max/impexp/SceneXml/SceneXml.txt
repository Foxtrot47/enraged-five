Project SceneXml
Template max_configurations.xml

OutputFileName SceneXml.gup
ConfigurationType dll

WarningsAsErrors false
WarningLevel 3

Define GTA_PC;NORW;NO_OS_STUB;WIN32;_WINDOWS;GTAEXPORT_EXPORTS;_CRT_SECURE_NO_DEPRECATE;MAX8

IncludePath $(RAGE_DIR)\base\tools\dcc\libs
IncludePath $(RAGE_DIR)\base\tools\dcc\max
IncludePath $(RAGE_DIR)\base\tools\dcc\max\rageMaxDataStore
IncludePath $(RAGE_DIR)\base\tools\dcc\max\rageMaxLODHierarchy
IncludePath $(RAGE_DIR)/base/tools/libs/rageDataStoreAttributes
IncludePath $(RAGE_DIR)/base/src
IncludePath $(RS_TOOLSSRC)\libs
IncludePath $(RS_TOOLSSRC)\dcc\libs
IncludePath $(RS_TOOLSSRC)\dcc\libs\RsULogWrapper
IncludePath $(RS_TOOLSSRC)\dcc\max
IncludePath $(RS_TOOLSSRC)\GTA_tools\include
IncludePath $(RAGE_3RDPARTY)\cli\TinyXML


ForceInclude forceinclude.h

EmbeddedLibAll ws2_32.lib bmm.lib comctl32.lib core.lib geom.lib gfx.lib igame.lib mesh.lib maxutil.lib maxscrpt.lib paramblk2.lib odbc32.lib odbccp32.lib

Files {
	Folder "Header Files" { 
		cTexture.h
		cMaterial.h
		cSceneXmlConfig.h
		cSceneXmlExporter.h
		cSceneXmlExporterDesc.h
		cSceneXmlUtil.h
		forceinclude.h
		XmlUtil.h
	}
	Folder "Resource Files" {	
		resource.h
		Resources.rc
	}
	Folder "Source Files" {
		cTexture.cpp
		cMaterial.cpp
		cSceneXmlConfig.cpp
		cSceneXmlExporter.cpp
		cSceneXmlExporterDesc.cpp
		cSceneXmlUtil.cpp
		DLLEntry.cpp
		MaxScript.cpp
		SceneXml.def
		XmlUtil.cpp
	}	
}