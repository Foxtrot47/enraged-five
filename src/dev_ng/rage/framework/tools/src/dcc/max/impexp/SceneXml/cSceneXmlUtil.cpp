//
// filename:	cSceneXmlUtil.cpp
// description:	
//

// --- Include Files ------------------------------------------------------------

// Max headers
#include "max.h"
#include "lslights.h"

// Local headers
#include "cSceneXmlUtil.h"

// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

// --- Globals ------------------------------------------------------------------

// --- Static Globals -----------------------------------------------------------

// --- Static class members -----------------------------------------------------

// --- Code ---------------------------------------------------------------------

void 
cSubObjectCallback::Center( Point3 c, int id )
{
	m_vCenters.push_back( c );
}

void 
cSubObjectCallback::TM( Matrix3 tm, int id )
{
	m_matTMs.push_back( tm );
}

int 
cSubObjectCallback::Type( )
{
	return ( SO_CENTER_PIVOT );
}

void
cSubObjectCallback::Clear( )
{
	m_vCenters.clear( );
	m_matTMs.clear( );
}

LightObject* 
IsLight( Object* obj )
{
	TSTR ClassName;

	if(obj->SuperClassID() != LIGHT_CLASS_ID)
	{
		return NULL;
	}

	obj->GetClassName(ClassName);

	return static_cast<LightObject*>(obj);
}

LightscapeLight* 
IsPhotometric( Object* obj )
{
	return obj->IsSubClassOf(LIGHTSCAPE_LIGHT_CLASS) ? static_cast<LightscapeLight*>(obj) : NULL;
}

// cSceneXmlUtil.cpp
