//
//
//    Filename: script.cpp
//     Creator: Greg Smith
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: MaxScript interface to the attribute code
//
//
#pragma warning (disable : 4786)

// max headers
#include <max.h>
// define the new primitives using macros from SDK
#include <maxscript/maxscript.h>
#include <maxscript/maxwrapper/maxclasses.h>
#include <maxscript/foundation/numbers.h>
#include <maxscript/foundation/strings.h>
#include <maxscript/compiler/parser.h>
#include <maxscript/macros/define_instantiation_functions.h>
//#include <maxscript\3dmath.h>

#include "iqueue.h"
#include "queue.h"
#include "queuenode.h"

// STD headers
#include <string>

// DLL function
__declspec( dllexport ) void LibInit() {}


// ------------------------------------------------------------------------------
// 	Define our new functions
//	This will call the section operation code
// ------------------------------------------------------------------------------
def_visible_primitive( create_attractor,			"CreateAttractor");

Value *create_attractor_cf(Value** arg_list, int count)
{
	if(count < 4)
	{
		return &false_value;
	}

//	type_check(arg_list[0], point3, "CreateAttractor <point> <dirA> <dirB> <dirC>");
//	type_check(arg_list[1], point3, "CreateAttractor <point> <dirA> <dirB> <dirC>");
//	type_check(arg_list[2], point3, "CreateAttractor <point> <dirA> <dirB> <dirC>");
//	type_check(arg_list[3], point3, "CreateAttractor <point> <dirA> <dirB> <dirC>");

	Point3 pos = arg_list[0]->to_point3();
	Point3 dirA = arg_list[1]->to_point3();
	Point3 dirB = arg_list[2]->to_point3();
	Point3 dirC = arg_list[3]->to_point3();

	queue* p_Queue = new queue();
	INode *pNode = GetCOREInterface()->CreateObjectNode(p_Queue);

	Matrix3 mat(1);
	mat.SetTrans(pos);

	GetCOREInterface()->SetNodeTMRelConstPlane(pNode, mat);

	p_Queue->AddPathNode();
	((queueNode*)p_Queue->SubAnim(0))->GetParamBlock(0)->SetValue(queuenode_length,0,0);
//	p_Queue->SetPathNodePosn(0, m_pt0);

	p_Queue->AddPathNode();
	((queueNode*)p_Queue->SubAnim(1))->GetParamBlock(0)->SetValue(queuenode_length,0,1);
	p_Queue->SetPathNodePosn(1, dirA);

	p_Queue->AddPathNode();
	((queueNode*)p_Queue->SubAnim(2))->GetParamBlock(0)->SetValue(queuenode_length,0,2);
	p_Queue->SetPathNodePosn(2, dirB);

	p_Queue->AddPathNode();
	((queueNode*)p_Queue->SubAnim(3))->GetParamBlock(0)->SetValue(queuenode_length,0,3);
	p_Queue->SetPathNodePosn(3, dirC);

	return_protected(new MAXNode(pNode));
}