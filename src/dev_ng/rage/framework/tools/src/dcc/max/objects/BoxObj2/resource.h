//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by BoxObj2.rc
//
#define IDS_LIBDESCRIPTION              1
#define IDS_CATEGORY                    2
#define IDS_CLASS_NAME                  3
#define IDS_PARAMS                      4
#define IDS_OBJ_NAME                    5
#define IDS_CREATIONMETHOD              6
#define IDS_KEYBOARDENTRY               7
#define IDS_LENGTH                      8
#define IDS_WIDTH                       9
#define IDS_HEIGHT                      10
#define IDS_WSEGS                       11
#define IDS_LSEGS                       12
#define IDS_HSEGS                       13
#define IDS_NURBS_SURFACE               14
#define IDS_SURFACE                     15
#define IDS_GENUVS                      16
#define IDS_TOP                         17
#define IDS_BOTTOM                      18
#define IDS_FRONT                       19
#define IDS_BACK                        20
#define IDS_LEFT                        21
#define IDS_RIGHT                       22
#define IDD_PANEL                       101
#define IDD_BOXPARAM2                   103
#define IDB_DMAMAN                      111
#define IDD_BOXPARAM1                   145
#define IDD_BOXPARAM3                   146
#define IDC_CLOSEBUTTON                 1000
#define IDC_DOSTUFF                     1000
#define IDC_HIDE_STATIC                 1001
#define IDC_FRONT_CHECK                 1002
#define IDC_BACK_CHECK                  1003
#define IDC_TOP_CHECK                   1004
#define IDC_BOTTOM_CHECK                1005
#define IDC_LEFT_CHECK                  1006
#define IDC_RIGHT_CHECK                 1007
#define IDC_LENSPINNER                  1008
#define IDC_HEIGHTEDIT                  1010
#define IDC_WIDTHSPINNER                1011
#define IDC_HEIGHTSPINNER               1012
#define IDC_HSEGSPIN                    1013
#define IDC_HSEGS                       1014
#define IDC_WSEGSPIN                    1015
#define IDC_WSEGS                       1016
#define IDC_LSEGSPIN                    1017
#define IDC_LSEGS                       1018
#define IDC_EXPORT_TEXT_1               1019
#define IDC_VERSION                     1021
#define IDC_GENTEXTURE                  1028
#define IDC_DMAMAN_STATIC               1053
#define IDC_COLOR                       1456
#define IDC_EDIT                        1490
#define IDC_SPIN                        1496
#define IDC_LENGTHEDIT                  3009
#define IDC_WIDTHEDIT                   3010
#define IDC_TI_POSX                     3019
#define IDC_TI_POSXSPIN                 3020
#define IDC_TI_POSY                     3021
#define IDC_TI_POSYSPIN                 3022
#define IDC_TI_CREATE                   3023
#define IDC_TI_POSZ                     3024
#define IDC_TI_POSZSPIN                 3025
#define IDC_CREATECUBE                  3028
#define IDC_CREATEBOX                   3029

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1003
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
