// Author: M Wilson

#include "cutfileLoader.h"

#include <maxscrpt/3dmath.h>

#include <maxscrpt/maxscrpt.h>
#include <maxscrpt/strings.h>
#include <maxscrpt/MAXclses.h>
#include <maxscrpt/Numbers.h>
#include <maxscrpt/definsfn.h> //this always has to be defined last out of the maxscript headers
#include <dummy.h> 

#include "cutfile/cutfile2.h"
#include "cutfile/cutfevent.h"
#include "cutfile/cutfeventargs.h"
#include "notetrck.h"

#include "math/amath.h"
#include "vector/matrix34.h"
#include "vectormath/legacyconvert.h"
#include "math/simplemath.h"

#include "system/exec.h"
#include "file/device.h"

#include "LightAnimationExporter.h"

#include "dcc/max/rageMaxDataStore/ifpmaxdatastore.h"

#include <libxml/tree.h>
#include "cranimation/animation.h"
#include "crclip/clip.h"
#include <list>
#include "IGame/IGame.h"
#include <IGame/IGameProperty.h>

#include "configParser/configGameView.h"
#include "configParser/configExportView.h"
using namespace configParser;

static ConfigGameView gs_GameView;
static ConfigCoreView gs_CoreView;
static ConfigExportView gs_ExportView;

#include "atl/hashstring.h"

// used to load camera cuts into max
def_visible_primitive(LoadCutFile,"LoadCutFile");
def_visible_primitive(GetCutFileEndRange,"GetCutFileEndRange");
def_visible_primitive(IsCutFileLoaded,"IsCutFileLoaded");
def_visible_primitive(SetInteractionMode,"SetInteractionMode");

def_visible_primitive(GetSceneOffsetTranslation,"GetSceneOffsetTranslation");
def_visible_primitive(GetSceneOffsetRotation,"GetSceneOffsetRotation");
def_visible_primitive(SetLightAttributes,"SetLightAttributes");
def_visible_primitive(SetLightKeyFrames,"SetLightKeyFrames");

// used to create a light xml file
def_visible_primitive(CreateLightXML,"CreateLightXML");
def_visible_primitive(GetLightCount,"GetLightCount");
def_visible_primitive(GetLightPosition,"GetLightPosition");
def_visible_primitive(GetLightRotation,"GetLightRotation");
def_visible_primitive(GetLightRotationQuat,"GetLightRotationQuat");
def_visible_primitive(GetLightAttachedName,"GetLightAttachedName");
def_visible_primitive(GetLightAttachedPosition,"GetLightAttachedPosition");
def_visible_primitive(GetLightAttachedRotation,"GetLightAttachedRotation");
def_visible_primitive(GetLightName,"GetLightName");
def_visible_primitive(GetLightCamera,"GetLightCamera");
def_visible_primitive(GetLightColour,"GetLightColour");
def_visible_primitive(GetLightIntensity,"GetLightIntensity");
def_visible_primitive(GetLightFallOff,"GetLightFallOff");
def_visible_primitive(GetLightExpFallOff,"GetLightExpFallOff");
def_visible_primitive(GetLightConeAngle,"GetLightConeAngle");
def_visible_primitive(GetLightInnerConeAngle,"GetLightInnerConeAngle");
def_visible_primitive(GetLightType,"GetLightType");
def_visible_primitive(GetLightCameraAnimatedFlag,"GetLightCameraAnimatedFlag");

def_visible_primitive(IsAnimatable,"IsAnimatable");

def_visible_primitive(AddLightToLightXML,"AddLightToLightXML");
def_visible_primitive(SaveLightXML,"SaveLightXML");

def_visible_primitive(ImportLightXML,"ImportLightXML");

def_visible_primitive(CreateAnim,"CreateAnim");
def_visible_primitive(AddFrameToAnim,"AddFrameToAnim");
def_visible_primitive(SaveAnim,"SaveAnim");


def_visible_primitive(RenameLightsWithinFile,"RenameLightsWithinFile");

def_visible_primitive(RotationIsRelative,"RotationIsRelative");

def_visible_primitive(BuildZip,"BuildZip");

def_visible_primitive(Reset,"Reset");

def_visible_primitive(ValidateUniqueLightNames,"ValidateUniqueLightNames");
def_visible_primitive(ValidateUniqueLightName,"ValidateUniqueLightName");

def_visible_primitive(GetConcatList,"GetConcatList");

using namespace rage;

struct BoneEntry
{
	char* c_boneName;
	char* c_boneID;
};

static std::list<BoneEntry> s_bones;

bool GetAttachName(INode* pNode, std::string& outId)
{
	outId = "";

	TSTR userProp;
	if(pNode->GetUserPropString("attach",userProp))
	{
		if(strcmp(userProp,"0") == 0)
		{
			outId = "#0";
		}
		else
		{
			outId = userProp;
		}

		return true;
	}

	return false;
}

bool IsAnimatable(INode* pNode, std::string& outId)
{
	outId = "";

	TSTR userProp;
	if(pNode->GetUserPropString("animated",userProp))
	{
		if(strcmp(userProp,"0") == 0)
		{
			outId = "#0";
		}
		else
		{
			outId = userProp;
		}

		return true;
	}

	return false;
}

bool IsAttached(INode* pNode, std::string& outId)
{
	outId = "";

	TSTR userProp;
	if(pNode->GetUserPropString("attached",userProp))
	{
		if(strcmp(userProp,"0") == 0)
		{
			outId = "#0";
		}
		else
		{
			outId = userProp;
		}

		return true;
	}

	return false;
}

bool LoadBoneTranslationXmlConfigFile( const char* pFilename, const char* pExportType )
{
	parTree* pTree = PARSER.LoadTree( pFilename, "" );
	if ( !pTree )
	{
		return false;
	}

	parTreeNode* pRootNode = pTree->GetRoot();

	std::vector< parTreeNode* > boneIdEntries;
	boneIdEntries.reserve(256);

	if( (strcmpi(pExportType, "Ped") == 0) ||
		(strcmpi(pExportType, "All") == 0) )
	{
		parTreeNode* pPedsNode = pRootNode->FindChildWithName("peds");
		if(!pPedsNode)
		{
			delete pTree;
		}
		else
		{
			parTreeNode *pBoneIdsNode = pPedsNode->FindChildWithName("boneids");
			Assert(pBoneIdsNode); //This must be there or the file isn't formatted correctly...

			for(parTreeNode::ChildNodeIterator cIter = pBoneIdsNode->BeginChildren();
				cIter != pBoneIdsNode->EndChildren(); 
				++cIter) 
			{
				parTreeNode* pChildNode = (*cIter);
				boneIdEntries.push_back(pChildNode);
			}
		}
	}

	if( (strcmpi(pExportType, "Vehicle") == 0) ||
		(strcmpi(pExportType, "All") == 0) )
	{
		parTreeNode* pVehsNode = pRootNode->FindChildWithName("vehicles");
		if(!pVehsNode)
		{
			delete pTree;
		}
		else
		{
			parTreeNode *pBoneIdsNode = pVehsNode->FindChildWithName("boneids");
			Assert(pBoneIdsNode); //This must be there or the file isn't formatted correctly...

			for(parTreeNode::ChildNodeIterator cIter = pBoneIdsNode->BeginChildren();
				cIter != pBoneIdsNode->EndChildren(); 
				++cIter) 
			{
				parTreeNode* pChildNode = (*cIter);
				boneIdEntries.push_back(pChildNode);
			}
		}
	}

	if( (strcmpi(pExportType, "Weapon") == 0) ||
		(strcmpi(pExportType, "All") == 0) )
	{
		parTreeNode* pWeapsNode = pRootNode->FindChildWithName("weapons");
		if(!pWeapsNode)
		{
			delete pTree;
		}
		else
		{
			parTreeNode *pBoneIdsNode = pWeapsNode->FindChildWithName("boneids");
			Assert(pBoneIdsNode); //This must be there or the file isn't formatted correctly...

			for(parTreeNode::ChildNodeIterator cIter = pBoneIdsNode->BeginChildren();
				cIter != pBoneIdsNode->EndChildren(); 
				++cIter) 
			{
				parTreeNode* pChildNode = (*cIter);
				boneIdEntries.push_back(pChildNode);
			}
		}
	}

	if( (strcmpi(pExportType, "Prop") == 0) ||
		(strcmpi(pExportType, "All") == 0) )
	{
		parTreeNode* pPropsNode = pRootNode->FindChildWithName("props");
		if(!pPropsNode)
		{
			delete pTree;
		}
		else
		{
			parTreeNode *pBoneIdsNode = pPropsNode->FindChildWithName("boneids");
			Assert(pBoneIdsNode); //This must be there or the file isn't formatted correctly...

			for(parTreeNode::ChildNodeIterator cIter = pBoneIdsNode->BeginChildren();
				cIter != pBoneIdsNode->EndChildren(); 
				++cIter) 
			{
				parTreeNode* pChildNode = (*cIter);
				boneIdEntries.push_back(pChildNode);
			}
		}
	}

	for(std::vector< parTreeNode* >::iterator it = boneIdEntries.begin();
		it != boneIdEntries.end();
		++it)
	{
		parTreeNode *pNode = (*it);
		BoneEntry thisbone;
		thisbone.c_boneID = new char[1024];
		thisbone.c_boneID[0] = 0;
		thisbone.c_boneName = new char[1024];
		thisbone.c_boneName[0] = 0;

		parAttribute* pIdAttr = pNode->GetElement().FindAttribute("id");
		Assert(pIdAttr);
		strcpy(thisbone.c_boneID, pIdAttr->GetStringValue());

		parAttribute* pNameAttr = pNode->GetElement().FindAttribute("name");
		Assert(pNameAttr);
		strcpy(thisbone.c_boneName, pNameAttr->GetStringValue());

		s_bones.push_back(thisbone);
	}

	delete pTree;
	return true;
}

void TokenizeString( char* pString, char* pDelimiter, atArray<char*>& tokens )
{	
	char* pToken = strtok( pString, pDelimiter );	
	while ( pToken )
	{
		tokens.PushAndGrow( pToken );
		pToken = strtok( NULL, pDelimiter );
	}
}

atString GetBoneID( const char* pBoneName, bool bTranslate )
{	
	char name[1024];
	safecpy( name, pBoneName, sizeof(name) );

	atArray<char *> tokens;
	TokenizeString( name, ":", tokens );
	char *pToken = tokens[tokens.GetCount() - 1];    

	if ( pToken )
	{
		if ( !bTranslate )        
		{
			return atString( pToken );
		}

		for ( std::list<BoneEntry>::const_iterator iter = s_bones.begin(); iter != s_bones.end(); ++iter )
		{			
			BoneEntry thisbone = (*iter);

			if ( stricmp( pToken, thisbone.c_boneName ) == 0 )
			{
				return atString( thisbone.c_boneID );
			}
		}
	}

	if ( stricmp( pToken, "char" ) == 0 )
	{
		return atString( "0" );
	}

#if HACK_GTA4
	if( pToken )
		return atString(pToken);
	else
		return atString( pBoneName );
#else
	return atString( pBoneName );
#endif // HACK_GTA4
}


int Round(double x)
{
	return (int)floor(x + 0.5);
}

// convert from an actual frame to max time
int GetFrameFromMaxTime(TimeValue tMaxTime)
{
	return ((tMaxTime*30.0f)/4800.0f);
}

// convert from max time to an actual frame
TimeValue GetMaxTimeFromFrame(int iFrame)
{
	return Round((float(iFrame)/30.0f)*4800.0f);
}

bool PullBackFrame(cutfCutsceneFile2* pCutFile, float fTime)
{
	for(int j=0; j < pCutFile->GetConcatDataList().GetCount(); ++j)
	{
		if(static_cast<float>(static_cast<int>(pCutFile->GetConcatDataList()[j].fStartTime * 10.)) / 10. == static_cast<float>(static_cast<int>(fTime * 10.)) / 10.)
		{
			return false;
		}
	}

	return true;
}

INode* CreateDummyObject(const char* pName)
{
	DummyObject* pDummyObj = (DummyObject*)GetCOREInterface()->CreateInstance(HELPER_CLASS_ID, Class_ID(DUMMY_CLASS_ID, 0)); 
	Box3 box; 
	box.Init(); 
	box += Point3(10.f, 0.0f, 0.0f); 
	box += Point3( 0.0f, 10.f, 0.0f); 
	box += Point3( 0.0f, 0.0f, 10.f); 
	box += Point3(-10.f, 0.0f, 0.0f); 
	box += Point3(  0.0f, -10.f, 0.0f); 
	box += Point3(  0.0f, 0.0f, -10.f);
	pDummyObj->SetBox(box);
	INode* pDummyNode = GetCOREInterface()->CreateObjectNode(pDummyObj);
	pDummyNode->SetName((char*)pName);

	return pDummyNode;
}

bool ValidCutNames(const atArray<atString>& cameraCutNameList)
{
	for(int i=0; i < cameraCutNameList.GetCount(); ++i)
	{
		atString atCutName = cameraCutNameList[i];

		for(int j=0; j < cameraCutNameList.GetCount(); ++j)
		{
			atString atCutNameCompare = cameraCutNameList[j];

			if(i == j) continue;

			if(stricmp(atCutName.c_str(),atCutNameCompare.c_str()) == 0)
			{
				return false;
			}
		}
	}

	return true;
}

void SetLightAttributesOnNode(INode* pNode, cutfLightObject* lightObject)
{
	IFpMaxDataStore *pMaxDataStore = GetMaxDataStoreInterface();
	if(pMaxDataStore)
	{
		int idxVolumeIntensity = pMaxDataStore->GetAttrIndex("Gta LightPhoto", "Volume Intensity");
		if(idxVolumeIntensity != -1)
		{
			pMaxDataStore->SetAttrFloat(pNode,idxVolumeIntensity, lightObject->GetVolumeIntensity());
		}

		int idxVolumeScale = pMaxDataStore->GetAttrIndex("Gta LightPhoto", "Volume Size");
		if(idxVolumeScale != -1)
		{
			pMaxDataStore->SetAttrFloat(pNode,idxVolumeScale, lightObject->GetVolumeSizeScale());
		}

		int idxCoronaSize = pMaxDataStore->GetAttrIndex("Gta LightPhoto", "Corona Size");
		if(idxCoronaSize != -1)
		{
			pMaxDataStore->SetAttrFloat(pNode,idxCoronaSize, lightObject->GetCoronaSize());
		}

		int idxCoronaIntensity = pMaxDataStore->GetAttrIndex("Gta LightPhoto", "Corona Intensity");
		if(idxCoronaIntensity != -1)
		{
			pMaxDataStore->SetAttrFloat(pNode,idxCoronaIntensity, lightObject->GetCoronaIntensity());
		}

		int idxCoronaZBias = pMaxDataStore->GetAttrIndex("Gta LightPhoto", "Corona Z-Bias");
		if(idxCoronaZBias != -1)
		{
			pMaxDataStore->SetAttrFloat(pNode,idxCoronaZBias, lightObject->GetCoronaZBias());
		}

		int idxCastStaticObjectShadow = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"Cast Static Object Shadow");
		if(idxCastStaticObjectShadow != -1)
		{
			pMaxDataStore->SetAttrBool(pNode,idxCastStaticObjectShadow, lightObject->GetLightFlag(CUTSCENE_LIGHTFLAG_CAST_STATIC_GEOM_SHADOWS));
		}

		int idxCastDynamicObjectShadow = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"Cast Dynamic Object Shadow");
		if(idxCastDynamicObjectShadow != -1)
		{
			pMaxDataStore->SetAttrBool(pNode,idxCastDynamicObjectShadow, lightObject->GetLightFlag(CUTSCENE_LIGHTFLAG_CAST_DYNAMIC_GEOM_SHADOWS));
		}

		int idxEnableBuzzing = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"Enable Buzzing");
		if(idxEnableBuzzing != -1)
		{
			pMaxDataStore->SetAttrBool(pNode,idxEnableBuzzing, lightObject->GetLightFlag(CUTSCENE_LIGHTFLAG_ENABLE_BUZZING));
		}

		int idxForceBuzzing = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"Force Buzzing");
		if(idxForceBuzzing != -1)
		{
			pMaxDataStore->SetAttrBool(pNode,idxForceBuzzing, lightObject->GetLightFlag(CUTSCENE_LIGHTFLAG_FORCE_BUZZING));
		}

		int idxDrawVolume = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"Volume Drawing");
		if(idxDrawVolume != -1)
		{
			pMaxDataStore->SetAttrBool(pNode,idxDrawVolume, lightObject->GetLightFlag(CUTSCENE_LIGHTFLAG_DRAW_VOLUME));
		}

		int idxNoSpecular = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"No Specular");
		if(idxNoSpecular != -1)
		{
			pMaxDataStore->SetAttrBool(pNode,idxNoSpecular, lightObject->GetLightFlag(CUTSCENE_LIGHTFLAG_NO_SPECULAR));
		}

		int idxCoronaOnly = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"Corona Only");
		if(idxCoronaOnly != -1)
		{
			pMaxDataStore->SetAttrBool(pNode,idxCoronaOnly, lightObject->GetLightFlag(CUTSCENE_LIGHTFLAG_CORONA_ONLY));
		}

		int idxInteriorAndExterior = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"INT and EXT");
		if(idxInteriorAndExterior != -1)
		{
			pMaxDataStore->SetAttrBool(pNode,idxInteriorAndExterior, lightObject->GetLightFlag(CUTSCENE_LIGHTFLAG_BOTH_INTERIOR_AND_EXTERIOR));
		}

		int idxReflector = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"Reflector");
		if(idxReflector != -1)
		{
			pMaxDataStore->SetAttrBool(pNode,idxReflector, lightObject->GetLightFlag(CUTSCENE_LIGHTFLAG_REFLECTOR));
		}

		int idxDiffuser = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"Diffuser");
		if(idxDiffuser != -1)
		{
			pMaxDataStore->SetAttrBool(pNode,idxDiffuser, lightObject->GetLightFlag(CUTSCENE_LIGHTFLAG_DIFFUSER));
		}

		int idxDontLightAlpha = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"Dont Light Alpha");
		if(idxDontLightAlpha != -1)
		{
			pMaxDataStore->SetAttrBool(pNode, idxDontLightAlpha, lightObject->GetLightFlag(CUTSCENE_LIGHTFLAG_DONT_LIGHT_ALPHA));
		}

		int idxAddAmbientLight = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"Add Ambient Light");
		if(idxAddAmbientLight != -1)
		{
			pMaxDataStore->SetAttrBool(pNode, idxAddAmbientLight, lightObject->GetLightFlag(CUTSCENE_LIGHTFLAG_ADD_AMBIENT_LIGHT));
		}

		int idxRenderInReflection = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"Render In Reflection");
		if(idxRenderInReflection != -1)
		{
			pMaxDataStore->SetAttrInt(pNode, idxRenderInReflection, 0);

			if(lightObject->GetLightFlag(CUTSCENE_LIGHTFLAG_ONLY_IN_REFLECTION))
			{
				pMaxDataStore->SetAttrInt(pNode, idxRenderInReflection, 2);
			}
			else if(lightObject->GetLightFlag(CUTSCENE_LIGHTFLAG_NOT_IN_REFLECTION))
			{
				pMaxDataStore->SetAttrInt(pNode, idxRenderInReflection, 1);
			}
		}

		int idxCalcFromSunlight = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"Calc From Sunlight");
		if(idxCalcFromSunlight != -1)
		{
			pMaxDataStore->SetAttrBool(pNode,idxCalcFromSunlight, lightObject->GetLightFlag(CUTSCENE_LIGHTFLAG_CALC_FROM_SUN));
		}

		int idxUseAsPedLight = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"Use As Ped Light");
		if(idxUseAsPedLight != -1)
		{
			pMaxDataStore->SetAttrBool(pNode,idxUseAsPedLight, lightObject->GetLightFlag(CUTSCENE_LIGHTFLAG_IS_CHARACTER_LIGHT));
		}

		int idxUseIntensityAsMultiplier = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"Use Intensity As Multiplier");
		if(idxUseIntensityAsMultiplier != -1)
		{
			pMaxDataStore->SetAttrBool(pNode,idxUseIntensityAsMultiplier, lightObject->GetLightFlag(CUTSCENE_LIGHTFLAG_CHARACTER_LIGHT_INTENSITY_AS_MULTIPLIER));
		}

		int idxOnlyAffectsPeds = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"Only Affects Peds");
		if(idxOnlyAffectsPeds != -1)
		{
			pMaxDataStore->SetAttrBool(pNode,idxOnlyAffectsPeds, lightObject->GetLightFlag(CUTSCENE_LIGHTFLAG_IS_PED_ONLY_LIGHT));
		}

		// Hour Flags
		int idxHour1 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"1");
		if(idxHour1 != -1)
		{
			//if(lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_01))
			{
				pMaxDataStore->SetAttrBool(pNode, idxHour1, lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_01));
			}
		}

		int idxHour2 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"2");
		if(idxHour2 != -1)
		{
			//if(lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_02))
			{
				pMaxDataStore->SetAttrBool(pNode, idxHour2, lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_02));
			}
		}

		int idxHour3 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"3");
		if(idxHour3 != -1)
		{
			//if(lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_03))
			{
				pMaxDataStore->SetAttrBool(pNode, idxHour3, lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_03));
			}
		}

		int idxHour4 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"4");
		if(idxHour4 != -1)
		{
			//if(lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_04))
			{
				pMaxDataStore->SetAttrBool(pNode, idxHour4, lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_04));
			}
		}

		int idxHour5 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"5");
		if(idxHour5 != -1)
		{
			//if(lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_05))
			{
				pMaxDataStore->SetAttrBool(pNode, idxHour5, lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_05));
			}
		}

		int idxHour6 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"6");
		if(idxHour6 != -1)
		{
			//if(lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_06))
			{
				pMaxDataStore->SetAttrBool(pNode, idxHour6, lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_06));
			}
		}

		int idxHour7 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"7");
		if(idxHour7 != -1)
		{
			//if(lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_07))
			{
				pMaxDataStore->SetAttrBool(pNode, idxHour7, lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_07));
			}
		}

		int idxHour8 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"8");
		if(idxHour8 != -1)
		{
			//if(lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_08))
			{
				pMaxDataStore->SetAttrBool(pNode, idxHour8, lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_08));
			}
		}

		int idxHour9 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"9");
		if(idxHour9 != -1)
		{
			//if(lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_09))
			{
				pMaxDataStore->SetAttrBool(pNode, idxHour9, lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_09));
			}
		}

		int idxHour10 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"10");
		if(idxHour10 != -1)
		{
			//if(lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_10))
			{
				pMaxDataStore->SetAttrBool(pNode, idxHour10, lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_10));
			}
		}

		int idxHour11 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"11");
		if(idxHour11 != -1)
		{
			//if(lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_11))
			{
				pMaxDataStore->SetAttrBool(pNode, idxHour11, lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_11));
			}
		}

		int idxHour12 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"12");
		if(idxHour12 != -1)
		{
			//if(lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_12))
			{
				pMaxDataStore->SetAttrBool(pNode, idxHour12, lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_12));
			}
		}

		int idxHour13 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"13");
		if(idxHour13 != -1)
		{
			//if(lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_13))
			{
				pMaxDataStore->SetAttrBool(pNode, idxHour13, lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_13));
			}
		}

		int idxHour14 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"14");
		if(idxHour14 != -1)
		{
			//if(lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_14))
			{
				pMaxDataStore->SetAttrBool(pNode, idxHour14, lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_14));
			}
		}

		int idxHour15 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"15");
		if(idxHour15 != -1)
		{
			//if(lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_15))
			{
				pMaxDataStore->SetAttrBool(pNode, idxHour15, lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_15));
			}
		}

		int idxHour16 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"16");
		if(idxHour16 != -1)
		{
			//if(lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_16))
			{
				pMaxDataStore->SetAttrBool(pNode, idxHour16, lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_16));
			}
		}

		int idxHour17 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"17");
		if(idxHour17 != -1)
		{
			//if(lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_17))
			{
				pMaxDataStore->SetAttrBool(pNode, idxHour17, lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_17));
			}
		}

		int idxHour18 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"18");
		if(idxHour18 != -1)
		{
			//if(lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_18))
			{
				pMaxDataStore->SetAttrBool(pNode, idxHour18, lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_18));
			}
		}

		int idxHour19 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"19");
		if(idxHour19 != -1)
		{
			//if(lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_19))
			{
				pMaxDataStore->SetAttrBool(pNode, idxHour19, lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_19));
			}
		}

		int idxHour20 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"20");
		if(idxHour20 != -1)
		{
			//if(lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_20))
			{
				pMaxDataStore->SetAttrBool(pNode, idxHour20, lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_20));
			}
		}

		int idxHour21 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"21");
		if(idxHour21 != -1)
		{
			//if(lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_21))
			{
				pMaxDataStore->SetAttrBool(pNode, idxHour21, lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_21));
			}
		}

		int idxHour22 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"22");
		if(idxHour22 != -1)
		{
			//if(lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_22))
			{
				pMaxDataStore->SetAttrBool(pNode, idxHour22, lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_22));
			}
		}

		int idxHour23 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"23");
		if(idxHour23 != -1)
		{
			//if(lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_23))
			{
				pMaxDataStore->SetAttrBool(pNode, idxHour23, lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_23));
			}
		}

		int idxHour24 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"24");
		if(idxHour24 != -1)
		{
			//if(lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_24))
			{
				pMaxDataStore->SetAttrBool(pNode, idxHour24, lightObject->GetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_24));
			}
		}

		int idxUseTimeCycle = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"Use Time Cycle Values");
		if(idxUseTimeCycle != -1)
		{
			//if(lightObject->GetLightFlag(ECutsceneLightFlag::CUTSCENE_LIGHTFLAG_USE_TIMECYCLE_VALUES))
			{
				pMaxDataStore->SetAttrBool(pNode, idxUseTimeCycle, lightObject->GetLightFlag(ECutsceneLightFlag::CUTSCENE_LIGHTFLAG_USE_TIMECYCLE_VALUES));
			}
		}

		int idxShadowBlur = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"Shadow Blur");
		if(idxShadowBlur != -1)
		{
			//if(lightObject->GetLightFlag(ECutsceneLightFlag::CUTSCENE_LIGHTFLAG_USE_TIMECYCLE_VALUES))
			{
				pMaxDataStore->SetAttrFloat(pNode, idxShadowBlur, lightObject->GetShadowBlur()/255);
			}
		}
	}
}

void AddLightAttributesToObject(INode* pNode, cutfLightObject* lightObject)
{
	IFpMaxDataStore *pMaxDataStore = GetMaxDataStoreInterface();
	if(pMaxDataStore)
	{
		int idxVolumeIntensity = pMaxDataStore->GetAttrIndex("Gta LightPhoto", "Volume Intensity");
		if(idxVolumeIntensity != -1)
		{
			lightObject->SetVolumeIntensity(pMaxDataStore->GetAttrFloat(pNode,idxVolumeIntensity));
		}

		int idxVolumeScale = pMaxDataStore->GetAttrIndex("Gta LightPhoto", "Volume Size");
		if(idxVolumeScale != -1)
		{
			lightObject->SetVolumeSizeScale(pMaxDataStore->GetAttrFloat(pNode,idxVolumeScale));
		}

		int idxCoronaSize = pMaxDataStore->GetAttrIndex("Gta LightPhoto", "Corona Size");
		if(idxCoronaSize != -1)
		{
			lightObject->SetCoronaSize(pMaxDataStore->GetAttrFloat(pNode,idxCoronaSize));
		}

		int idxCoronaIntensity = pMaxDataStore->GetAttrIndex("Gta LightPhoto", "Corona Intensity");
		if(idxCoronaIntensity != -1)
		{
			lightObject->SetCoronaIntensity(pMaxDataStore->GetAttrFloat(pNode,idxCoronaIntensity));
		}

		int idxCoronaZBias = pMaxDataStore->GetAttrIndex("Gta LightPhoto", "Corona Z-Bias");
		if(idxCoronaZBias != -1)
		{
			lightObject->SetCoronaZBias(pMaxDataStore->GetAttrFloat(pNode,idxCoronaZBias));
		}

		int idxCastStaticObjectShadow = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"Cast Static Object Shadow");
		if(idxCastStaticObjectShadow != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxCastStaticObjectShadow))
			{
				lightObject->SetLightFlag(CUTSCENE_LIGHTFLAG_CAST_STATIC_GEOM_SHADOWS);
			}
		}

		int idxCastDynamicObjectShadow = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"Cast Dynamic Object Shadow");
		if(idxCastDynamicObjectShadow != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxCastDynamicObjectShadow))
			{
				lightObject->SetLightFlag(CUTSCENE_LIGHTFLAG_CAST_DYNAMIC_GEOM_SHADOWS);
			}
		}

		int idxEnableBuzzing = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"Enable Buzzing");
		if(idxEnableBuzzing != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxEnableBuzzing))
			{
				lightObject->SetLightFlag(CUTSCENE_LIGHTFLAG_ENABLE_BUZZING);
			}
		}

		int idxForceBuzzing = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"Force Buzzing");
		if(idxForceBuzzing != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxForceBuzzing))
			{
				lightObject->SetLightFlag(CUTSCENE_LIGHTFLAG_FORCE_BUZZING);
			}
		}

		int idxDrawVolume = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"Volume Drawing");
		if(idxDrawVolume != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxDrawVolume))
			{
				lightObject->SetLightFlag(CUTSCENE_LIGHTFLAG_DRAW_VOLUME);
			}
		}

		int idxNoSpecular = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"No Specular");
		if(idxNoSpecular != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxNoSpecular))
			{
				lightObject->SetLightFlag(CUTSCENE_LIGHTFLAG_NO_SPECULAR);
			}
		}

		int idxCoronaOnly = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"Corona Only");
		if(idxCoronaOnly != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxCoronaOnly))
			{
				lightObject->SetLightFlag(CUTSCENE_LIGHTFLAG_CORONA_ONLY);
			}
		}

		int idxInteriorAndExterior = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"INT and EXT");
		if(idxInteriorAndExterior != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxInteriorAndExterior))
			{
				lightObject->SetLightFlag(CUTSCENE_LIGHTFLAG_BOTH_INTERIOR_AND_EXTERIOR);
			}
		}

		int idxCalcFromSunlight = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"Calc From Sunlight");
		if(idxCalcFromSunlight != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxCalcFromSunlight))
			{
				lightObject->SetLightFlag(CUTSCENE_LIGHTFLAG_CALC_FROM_SUN);
			}
		}

		int idxReflector = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"Reflector");
		if(idxReflector != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxReflector))
			{
				lightObject->SetLightFlag(CUTSCENE_LIGHTFLAG_REFLECTOR);
			}
		}

		int idxDiffuser = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"Diffuser");
		if(idxDiffuser != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxDiffuser))
			{
				lightObject->SetLightFlag(CUTSCENE_LIGHTFLAG_DIFFUSER);
			}
		}

		int idxDontLightAlpha = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"Dont Light Alpha");
		if(idxDontLightAlpha != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxDontLightAlpha))
			{
				lightObject->SetLightFlag(CUTSCENE_LIGHTFLAG_DONT_LIGHT_ALPHA);
			}
		}

		int idxAddAmbientLight = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"Add Ambient Light");
		if(idxAddAmbientLight != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxAddAmbientLight))
			{
				lightObject->SetLightFlag(CUTSCENE_LIGHTFLAG_ADD_AMBIENT_LIGHT);
			}
		}

		int idxRenderInReflection = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"Render In Reflection");
		if(idxRenderInReflection != -1)
		{
			int reflectionIndex = pMaxDataStore->GetAttrInt(pNode,idxRenderInReflection);
			
			switch(reflectionIndex)
			{
			case 1:
				{
					lightObject->SetLightFlag(CUTSCENE_LIGHTFLAG_NOT_IN_REFLECTION);
				}
				break;
			case 2:
				{
					lightObject->SetLightFlag(CUTSCENE_LIGHTFLAG_ONLY_IN_REFLECTION);
				}
				break;
			}
		}

		int idxUseAsPedLight = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"Use As Ped Light");
		if(idxUseAsPedLight != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxUseAsPedLight))
			{
				lightObject->SetLightFlag(CUTSCENE_LIGHTFLAG_IS_CHARACTER_LIGHT);
			}
		}

		int idxUseIntensityAsMultiplier = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"Use Intensity As Multiplier");
		if(idxUseIntensityAsMultiplier != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxUseIntensityAsMultiplier))
			{
				lightObject->SetLightFlag(CUTSCENE_LIGHTFLAG_CHARACTER_LIGHT_INTENSITY_AS_MULTIPLIER);
			}
		}

		int idxOnlyAffectsPeds = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"Only Affects Peds");
		if(idxOnlyAffectsPeds != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxOnlyAffectsPeds))
			{
				lightObject->SetLightFlag(CUTSCENE_LIGHTFLAG_IS_PED_ONLY_LIGHT);
			}
		}

		// Hour flags

		lightObject->SetLightHourFlags(0); // Reset

		int idxHour1 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"1");
		if(idxHour1 != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxHour1))
			{
				lightObject->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_01);
			}
		}

		int idxHour2 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"2");
		if(idxHour2 != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxHour2))
			{
				lightObject->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_02);
			}
		}

		int idxHour3 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"3");
		if(idxHour3 != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxHour3))
			{
				lightObject->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_03);
			}
		}

		int idxHour4 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"4");
		if(idxHour4 != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxHour4))
			{
				lightObject->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_04);
			}
		}

		int idxHour5 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"5");
		if(idxHour5 != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxHour5))
			{
				lightObject->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_05);
			}
		}

		int idxHour6 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"6");
		if(idxHour6 != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxHour6))
			{
				lightObject->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_06);
			}
		}

		int idxHour7 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"7");
		if(idxHour7 != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxHour7))
			{
				lightObject->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_07);
			}
		}

		int idxHour8 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"8");
		if(idxHour8 != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxHour8))
			{
				lightObject->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_08);
			}
		}

		int idxHour9 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"9");
		if(idxHour9 != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxHour9))
			{
				lightObject->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_09);
			}
		}

		int idxHour10 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"10");
		if(idxHour10 != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxHour10))
			{
				lightObject->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_10);
			}
		}

		int idxHour11 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"11");
		if(idxHour11 != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxHour11))
			{
				lightObject->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_11);
			}
		}

		int idxHour12 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"12");
		if(idxHour12 != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxHour12))
			{
				lightObject->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_12);
			}
		}

		int idxHour13 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"13");
		if(idxHour13 != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxHour13))
			{
				lightObject->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_13);
			}
		}

		int idxHour14 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"14");
		if(idxHour14 != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxHour14))
			{
				lightObject->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_14);
			}
		}

		int idxHour15 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"15");
		if(idxHour15 != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxHour15))
			{
				lightObject->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_15);
			}
		}

		int idxHour16 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"16");
		if(idxHour16 != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxHour16))
			{
				lightObject->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_16);
			}
		}

		int idxHour17 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"17");
		if(idxHour17 != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxHour17))
			{
				lightObject->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_17);
			}
		}

		int idxHour18 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"18");
		if(idxHour18 != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxHour18))
			{
				lightObject->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_18);
			}
		}

		int idxHour19 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"19");
		if(idxHour19 != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxHour19))
			{
				lightObject->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_19);
			}
		}

		int idxHour20 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"20");
		if(idxHour20 != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxHour20))
			{
				lightObject->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_20);
			}
		}

		int idxHour21 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"21");
		if(idxHour21 != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxHour21))
			{
				lightObject->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_21);
			}
		}

		int idxHour22 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"22");
		if(idxHour22 != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxHour22))
			{
				lightObject->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_22);
			}
		}

		int idxHour23 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"23");
		if(idxHour23 != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxHour23))
			{
				lightObject->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_23);
			}
		}

		int idxHour24 = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"24");
		if(idxHour24 != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxHour24))
			{
				lightObject->SetLightHourFlag(CUTSCENE_LIGHTFLAG_HOURS_24);
			}
		}

		int idxUseTimeCycle = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"Use Time Cycle Values");
		if(idxUseTimeCycle != -1)
		{
			if(pMaxDataStore->GetAttrBool(pNode,idxUseTimeCycle))
			{
				lightObject->SetLightFlag(ECutsceneLightFlag::CUTSCENE_LIGHTFLAG_USE_TIMECYCLE_VALUES);
			}
		}

		int idxShadowBlur = pMaxDataStore->GetAttrIndex("Gta LightPhoto" ,"Shadow Blur");
		if(idxShadowBlur != -1)
		{
			lightObject->SetShadowBlur(pMaxDataStore->GetAttrFloat(pNode,idxShadowBlur)*255);
		}
	}
}

IPropertyContainer * SetUpContainer(INode* pNode)
{
	IGameScene* pIGameScene = GetIGameInterface();
	pIGameScene->InitialiseIGame(pNode);

	IGameNode* pIGameNode = pIGameScene->GetIGameNode(pNode) ;
	IGameObject * pIGameObj = pIGameNode->GetIGameObject();

	return pIGameObj->GetIPropertyContainer ();
}

void SetKeyFramesOnLight(INode* pNode, cutfLightObject* lightObject )
{
	parAttributeList& attList = const_cast<parAttributeList &>(lightObject->GetAttributeList());

	Control* p_Control = pNode->GetTMController()->GetPositionController();

	Control* p_ControlX = (Control*)p_Control->SubAnim(0);
	Control* p_ControlY = (Control*)p_Control->SubAnim(1);
	Control* p_ControlZ = (Control*)p_Control->SubAnim(2);

	if(p_ControlX->ClassID() == Class_ID(HYBRIDINTERP_FLOAT_CLASS_ID, 0) &&
		p_ControlY->ClassID() == Class_ID(HYBRIDINTERP_FLOAT_CLASS_ID, 0) && 
		p_ControlZ->ClassID() == Class_ID(HYBRIDINTERP_FLOAT_CLASS_ID, 0))
	{
		IKeyControl *ikeysX = GetKeyControlInterface(p_ControlX);
		IKeyControl *ikeysY = GetKeyControlInterface(p_ControlY);
		IKeyControl *ikeysZ = GetKeyControlInterface(p_ControlZ);
		
		for(int i=0; i < attList.GetAttributeArray().GetCount(); ++i)
		{
			if(stricmp(attList.GetAttributeArray()[i].GetName(), "PosKeyX") == 0)
			{
				atString keyString(attList.GetAttributeArray()[i].GetStringValue());

				atArray<atString> keyArray;
				keyString.Split(keyArray, ":");

				if(keyArray.GetCount() == 2)
				{
					IBezFloatKey key;
					key.time = GetMaxTimeFromFrame(atoi(keyArray[0].c_str()));
					key.val = atof(keyArray[1].c_str());
					ikeysX->AppendKey(&key);
				}
			}
			else if(stricmp(attList.GetAttributeArray()[i].GetName(), "PosKeyY") == 0)
			{
				atString keyString(attList.GetAttributeArray()[i].GetStringValue());

				atArray<atString> keyArray;
				keyString.Split(keyArray, ":");

				if(keyArray.GetCount() == 2)
				{
					IBezFloatKey key;
					key.time = GetMaxTimeFromFrame(atoi(keyArray[0].c_str()));
					key.val = atof(keyArray[1].c_str());
					ikeysY->AppendKey(&key);
				}
			}
			else if(stricmp(attList.GetAttributeArray()[i].GetName(), "PosKeyZ") == 0)
			{
				atString keyString(attList.GetAttributeArray()[i].GetStringValue());

				atArray<atString> keyArray;
				keyString.Split(keyArray, ":");

				if(keyArray.GetCount() == 2)
				{
					IBezFloatKey key;
					key.time = GetMaxTimeFromFrame(atoi(keyArray[0].c_str()));
					key.val = atof(keyArray[1].c_str());
					ikeysZ->AppendKey(&key);
				}
			}
		}
	}

	p_Control = pNode->GetTMController()->GetRotationController();

	p_ControlX = (Control*)p_Control->SubAnim(0);
	p_ControlY = (Control*)p_Control->SubAnim(1);
	p_ControlZ = (Control*)p_Control->SubAnim(2);

	if(p_ControlX->ClassID() == Class_ID(HYBRIDINTERP_FLOAT_CLASS_ID, 0) &&
		p_ControlY->ClassID() == Class_ID(HYBRIDINTERP_FLOAT_CLASS_ID, 0) && 
		p_ControlZ->ClassID() == Class_ID(HYBRIDINTERP_FLOAT_CLASS_ID, 0))
	{
		IKeyControl *ikeysX = GetKeyControlInterface(p_ControlX);
		IKeyControl *ikeysY = GetKeyControlInterface(p_ControlY);
		IKeyControl *ikeysZ = GetKeyControlInterface(p_ControlZ);

		for(int i=0; i < attList.GetAttributeArray().GetCount(); ++i)
		{
			if(stricmp(attList.GetAttributeArray()[i].GetName(), "RotKeyX") == 0)
			{
				atString keyString(attList.GetAttributeArray()[i].GetStringValue());

				atArray<atString> keyArray;
				keyString.Split(keyArray, ":");

				if(keyArray.GetCount() == 2)
				{
					IBezFloatKey key;
					key.time = GetMaxTimeFromFrame(atoi(keyArray[0].c_str()));
					key.val = atof(keyArray[1].c_str()) * DtoR;
					ikeysX->AppendKey(&key);
				}
			}
			else if(stricmp(attList.GetAttributeArray()[i].GetName(), "RotKeyY") == 0)
			{
				atString keyString(attList.GetAttributeArray()[i].GetStringValue());

				atArray<atString> keyArray;
				keyString.Split(keyArray, ":");

				if(keyArray.GetCount() == 2)
				{
					IBezFloatKey key;
					key.time = GetMaxTimeFromFrame(atoi(keyArray[0].c_str()));
					key.val = atof(keyArray[1].c_str()) * DtoR;
					ikeysY->AppendKey(&key);
				}
			}
			else if(stricmp(attList.GetAttributeArray()[i].GetName(), "RotKeyZ") == 0)
			{
				atString keyString(attList.GetAttributeArray()[i].GetStringValue());

				atArray<atString> keyArray;
				keyString.Split(keyArray, ":");

				if(keyArray.GetCount() == 2)
				{
					IBezFloatKey key;
					key.time = GetMaxTimeFromFrame(atoi(keyArray[0].c_str()));
					key.val = atof(keyArray[1].c_str()) * DtoR;
					ikeysZ->AppendKey(&key);
				}
			}
		}
	}

	IGameScene* pIGameScene = GetIGameInterface();
	pIGameScene->InitialiseIGame(pNode);

	IGameNode* pIGameNode =  pIGameScene->GetIGameNode(pNode) ;
	IGameObject* pIGameObj = pIGameNode->GetIGameObject();

	IPropertyContainer* pContainer = pIGameObj->GetIPropertyContainer ();

	IGameProperty* pProperty = pContainer->QueryProperty("lightIntensity");
	Control *pMaxControl = NULL;
	if(pProperty)
	{
		IGameControl* pPropControl = pProperty->GetIGameControl();
		// Control has no keys currently so we create an animated controller for it.
		if(!pPropControl)
		{
			IParamBlock2* pPBlock = pProperty->GetMaxParamBlock2();
			for (int j = 0; j < pPBlock->NumParams(); j++)
			{
				ParamID id = pPBlock->IndextoID(j);
				const ParamDef& paramDef = pPBlock->GetParamDef(id);
				TSTR name = paramDef.int_name;
				if (stricmp(name, "lightIntensity") == 0)
				{
					if (paramDef.type == TYPE_FLOAT)
					{
						pPBlock->AssignController(NewDefaultFloatController(), j);
						pContainer = SetUpContainer(pNode);
						pProperty = pContainer->QueryProperty("lightIntensity");
						if(pProperty)
						{
							pPropControl = pProperty->GetIGameControl();
						}
					}
				}
			}
		}

		if(pPropControl)
		{
			pMaxControl = pPropControl->GetMaxControl(IGAME_FLOAT);

			IKeyControl* pKeys = GetKeyControlInterface(pMaxControl);

			for(int i=0; i < attList.GetAttributeArray().GetCount(); ++i)
			{
				if(stricmp(attList.GetAttributeArray()[i].GetName(), "Intensity") == 0)
				{
					atString keyString(attList.GetAttributeArray()[i].GetStringValue());

					atArray<atString> keyArray;
					keyString.Split(keyArray, ":");

					if(keyArray.GetCount() == 3)
					{
						IBezFloatKey key;
						key.time = GetMaxTimeFromFrame(atoi(keyArray[0].c_str()));
						key.val = atof(keyArray[1].c_str());
						key.flags = atoi(keyArray[2].c_str()); // Flags define if this is a full bez or restricted to values
						pKeys->AppendKey(&key);
					}
				}
			}
		}
	}
}

void GetKeyFramesFromLight(INode* pNode, cutfLightObject* lightObject )
{
	Control* p_Control = pNode->GetTMController()->GetPositionController();
	Class_ID classID = p_Control->ClassID();

	Control* p_ControlX = (Control*)p_Control->SubAnim(0);
	Control* p_ControlY = (Control*)p_Control->SubAnim(1);
	Control* p_ControlZ = (Control*)p_Control->SubAnim(2);

	if(p_ControlX->ClassID() == Class_ID(HYBRIDINTERP_FLOAT_CLASS_ID, 0) &&
		p_ControlY->ClassID() == Class_ID(HYBRIDINTERP_FLOAT_CLASS_ID, 0) && 
		p_ControlZ->ClassID() == Class_ID(HYBRIDINTERP_FLOAT_CLASS_ID, 0))
	{
		IKeyControl *ikeysX = GetKeyControlInterface(p_ControlX);
		IKeyControl *ikeysY = GetKeyControlInterface(p_ControlY);
		IKeyControl *ikeysZ = GetKeyControlInterface(p_ControlZ);
		int Count;
		IBezFloatKey key;

		parAttributeList& attList = const_cast<parAttributeList &>(lightObject->GetAttributeList());

		Count = ikeysX->GetNumKeys();
		for(int i=0;i<Count;i++)
		{
			ikeysX->GetKey(i, &key);

			char cKey[RAGE_MAX_PATH];
			sprintf(cKey, "%d:%f", GetFrameFromMaxTime(key.time), key.val);
			attList.AddAttribute("PosKeyX", cKey);
		}

		Count = ikeysY->GetNumKeys();
		for(int i=0;i<Count;i++)
		{
			ikeysY->GetKey(i, &key);

			char cKey[RAGE_MAX_PATH];
			sprintf(cKey, "%d:%f", GetFrameFromMaxTime(key.time), key.val);
			attList.AddAttribute("PosKeyY", cKey);
		}

		Count = ikeysZ->GetNumKeys();
		for(int i=0;i<Count;i++)
		{
			ikeysZ->GetKey(i, &key);

			char cKey[RAGE_MAX_PATH];
			sprintf(cKey, "%d:%f", GetFrameFromMaxTime(key.time), key.val);
			attList.AddAttribute("PosKeyZ", cKey);
		}
	}

	p_Control = pNode->GetTMController()->GetRotationController();

	p_ControlX = (Control*)p_Control->SubAnim(0);
	p_ControlY = (Control*)p_Control->SubAnim(1);
	p_ControlZ = (Control*)p_Control->SubAnim(2);

	if(p_ControlX->ClassID() == Class_ID(HYBRIDINTERP_FLOAT_CLASS_ID, 0) &&
		p_ControlY->ClassID() == Class_ID(HYBRIDINTERP_FLOAT_CLASS_ID, 0) && 
		p_ControlZ->ClassID() == Class_ID(HYBRIDINTERP_FLOAT_CLASS_ID, 0))
	{
		IKeyControl *ikeysX = GetKeyControlInterface(p_ControlX);
		IKeyControl *ikeysY = GetKeyControlInterface(p_ControlY);
		IKeyControl *ikeysZ = GetKeyControlInterface(p_ControlZ);
		int Count;
		IBezFloatKey key;

		parAttributeList& attList = const_cast<parAttributeList &>(lightObject->GetAttributeList());

		Count = ikeysX->GetNumKeys();
		for(int i=0;i<Count;i++)
		{
			ikeysX->GetKey(i, &key);

			char cKey[RAGE_MAX_PATH];
			sprintf(cKey, "%d:%f", GetFrameFromMaxTime(key.time), (key.val * RtoD));
			attList.AddAttribute("RotKeyX", cKey);
		}

		Count = ikeysY->GetNumKeys();
		for(int i=0;i<Count;i++)
		{
			ikeysY->GetKey(i, &key);

			char cKey[RAGE_MAX_PATH];
			sprintf(cKey, "%d:%f", GetFrameFromMaxTime(key.time), (key.val * RtoD));
			attList.AddAttribute("RotKeyY", cKey);
		}

		Count = ikeysZ->GetNumKeys();
		for(int i=0;i<Count;i++)
		{
			ikeysZ->GetKey(i, &key);

			char cKey[RAGE_MAX_PATH];
			sprintf(cKey, "%d:%f", GetFrameFromMaxTime(key.time), (key.val * RtoD));
			attList.AddAttribute("RotKeyZ", cKey);
		}
	}

	IGameScene* pIGameScene = GetIGameInterface();
	pIGameScene->InitialiseIGame(pNode);

	IGameNode* pIGameNode =  pIGameScene->GetIGameNode(pNode) ;
	IGameObject *pIGameObj = pIGameNode->GetIGameObject();

	IPropertyContainer* pContainer = pIGameObj->GetIPropertyContainer ();

	IGameProperty* pProperty = pContainer->QueryProperty(_T("lightIntensity"));
	Control *pMaxControl = NULL;
	if(pProperty)
	{
		IGameControl* pPropControl = pProperty->GetIGameControl();
		if(pPropControl)
		{
			pMaxControl = pPropControl->GetMaxControl(IGAME_FLOAT);

			IKeyControl* pKeys = GetKeyControlInterface(pMaxControl);

			parAttributeList& attList = const_cast<parAttributeList &>(lightObject->GetAttributeList());

			for(int k=0; k < pKeys->GetNumKeys(); k++)
			{
				if(pMaxControl->ClassID().PartA() == HYBRIDINTERP_FLOAT_CLASS_ID)
				{
					IBezFloatKey key;
					pKeys->GetKey(k,&key);

					char cKey[RAGE_MAX_PATH];
					sprintf(cKey, "%d:%f:%d", GetFrameFromMaxTime(key.time), (key.val), key.flags);
					attList.AddAttribute("Intensity", cKey);
				}
			}
		}
	}
}

Value* SetInteractionMode_cf(Value** arg_list, int count)
{
	check_arg_count(SetInteractionMode, 1, count);
	//type_check(arg_list[0],bool,"bool [interactive mode]");

	cutfileLoader::GetInst().SetInteractionMode(arg_list[0]->to_bool());

	return &ok;
}

Value* IsCutFileLoaded_cf(Value** arg_list, int count)
{
	check_arg_count(IsCutFileLoaded, 0, count);

	if(!cutfileLoader::GetInst().IsCutFileLoaded())
	{
		if(cutfileLoader::GetInst().GetInteractionMode())
		{
			MessageBox(NULL,"Cutscene data is not loaded for this scene.","Error", MB_ICONERROR);
		}
		
		PrintToListener("Error", "Cutscene data is not loaded for this scene."); // print to listener
	}

	return_protected(new Integer(cutfileLoader::GetInst().IsCutFileLoaded() ? 1 : 0));
}

// load a cut file (cutxml) into max and plot the camera cuts onto a placeholder object in notekeys
Value* LoadCutFile_cf(Value** arg_list, int count)
{
	check_arg_count(LoadCutFile, 1, count);
	type_check(arg_list[0],String,"string [cutxml file path]");

	cutfileLoader::GetInst().GetCutFile()->Clear();

	if (cutfileLoader::GetInst().GetCutFile()->LoadFile(arg_list[0]->to_string()) )
	{
		if(cutfileLoader::GetInst().GetCutFile()->GetCutsceneFlags().IsSet(cutfCutsceneFile2::CUTSCENE_EXTERNAL_CONCAT_FLAG ))
		{
			char cMsg[RAGE_MAX_PATH];
			sprintf( cMsg, "'%s' load failed. File is external concat data.", arg_list[0]->to_string() );

			if(cutfileLoader::GetInst().GetInteractionMode())
			{
				MessageBox(NULL,cMsg,"Error", MB_ICONERROR);
			}

			PrintToListener("Error", cMsg); // print to listener

			cutfileLoader::GetInst().GetCutFile()->Clear();

			return &false_value;
		}

		// validation check as we should be importing streamed data 
		if(atString(arg_list[0]->to_string()).IndexOf("data.c") == -1)
		{
			char cMsg[RAGE_MAX_PATH];
			sprintf( cMsg, "'%s' does not appear to be the 'data.cutxml' file. Are you sure you want to load this?", arg_list[0]->to_string() );
			
			if(cutfileLoader::GetInst().GetInteractionMode())
			{
				if(MessageBox(NULL,cMsg,"Warning", MB_ICONWARNING | MB_YESNO) == IDNO)
				{
					cutfileLoader::GetInst().GetCutFile()->Clear();

					return &false_value;
				}
			}
		}

		atArray<int> cameraCutTimeList;
		atArray<atString> cameraCutNameList;
		const atArray<cutfEvent *>& eventList = cutfileLoader::GetInst().GetCutFile()->GetEventList();

		for(int i=0; i < eventList.GetCount(); ++i)
		{
			cutfEvent* pEvent = eventList[i];

			if(pEvent->GetEventId() == CUTSCENE_CAMERA_CUT_EVENT)
			{
				float t = pEvent->GetTime();
				int iFrame = Round(pEvent->GetTime()*30.f);

				// frame is processed so add the range to push it into mobo space
				int iStartRange = cutfileLoader::GetInst().GetCutFile()->GetRangeStart();

				iFrame += iStartRange;

				//if(iFrame < cutfileLoader::GetInst().GetCutFile()->GetRangeStart() ||
				//	iFrame > cutfileLoader::GetInst().GetCutFile()->GetRangeEnd())
				//	continue;

				const cutfCameraCutEventArgs* pCamera = dynamic_cast<const cutfCameraCutEventArgs*>(pEvent->GetEventArgs());
				if(pCamera)
				{
					cameraCutNameList.Grow() = atString(pCamera->GetName().GetCStr());
					cameraCutTimeList.Grow() = iFrame;
				}
			}
		}

		if(!ValidCutNames(cameraCutNameList))
		{
			char cMsg[RAGE_MAX_PATH];
			sprintf( cMsg, "cutxml file '%s' contains camera cuts which are not uniquely named.", arg_list[0]->to_string() );
			
			if(cutfileLoader::GetInst().GetInteractionMode())
			{
				MessageBox(NULL,cMsg,"Warning", MB_ICONWARNING);
			}
			
			PrintToListener("Warning", cMsg); // print to listener
			
			return &false_value;
		}

		if(cameraCutTimeList.GetCount() == 0)
		{
			char cMsg[RAGE_MAX_PATH];
			sprintf( cMsg, "'%s' contains no camera cut events.", arg_list[0]->to_string() );

			if(cutfileLoader::GetInst().GetInteractionMode())
			{
				MessageBox(NULL,cMsg,"Warning", MB_ICONWARNING);
			}

			PrintToListener("Warning", cMsg); // print to listener
		}

		char cMsg[RAGE_MAX_PATH];
		sprintf( cMsg, "camera cut events - %d", cameraCutTimeList.GetCount() );
		PrintToListener(NULL, cMsg);

		for(int i=0; i < cameraCutTimeList.GetCount(); ++i)
		{
			char cMsg[RAGE_MAX_PATH];
			sprintf( cMsg, "object created '%s' with time @ %d -> %d", cameraCutNameList[i].c_str(), cameraCutTimeList[i], GetMaxTimeFromFrame(cameraCutTimeList[i]) );
			PrintToListener(NULL, cMsg);

			INode* pNode = CreateDummyObject(cameraCutNameList[i].c_str());
			if(pNode)
			{
				NoteTrack* nt = NewDefaultNoteTrack();
				DefNoteTrack* dnt = (DefNoteTrack*)nt;

				TimeValue tValue = GetMaxTimeFromFrame(cameraCutTimeList[i]);
				NoteKey* n = new NoteKey(tValue, "Time" );
				dnt->keys.Append(1, &n);

				pNode->AddNoteTrack(dnt);
			}
		}

		INode* pSceneOffsetNode = CreateDummyObject("Scene_Offset");

		INode* pOnAllTimeNode = CreateDummyObject("OnAllTime");
		if(pOnAllTimeNode)
		{
			NoteTrack* nt = NewDefaultNoteTrack();
			DefNoteTrack* dnt = (DefNoteTrack*)nt;

			TimeValue tValue = GetMaxTimeFromFrame(cutfileLoader::GetInst().GetCutFile()->GetRangeStart());
			NoteKey* n = new NoteKey(tValue, "Time" );
			dnt->keys.Append(1, &n);

			pOnAllTimeNode->AddNoteTrack(dnt);
		}

		cutfileLoader::GetInst().SetCutFileLoaded(true);
	}
	else
	{
		char cMsg[RAGE_MAX_PATH];
		sprintf( cMsg, "Unable to load cutxml file '%s'.", arg_list[0]->to_string() );

		if(cutfileLoader::GetInst().GetInteractionMode())
		{
			MessageBox(NULL ,cMsg, "Error", MB_ICONERROR);
		}

		PrintToListener("Error", cMsg); // print to listener

		return &false_value;
	}

	char cMsg[RAGE_MAX_PATH];
	sprintf( cMsg, "'%s' load success.", arg_list[0]->to_string() );
	PrintToListener(NULL, cMsg); // print to listener

	cutfileLoader::GetInst().SetRangeEnd(cutfileLoader::GetInst().GetCutFile()->GetRangeEnd());

	cMsg[0] = 0;
	sprintf( cMsg, "end range - %d - data_stream.cutxml", cutfileLoader::GetInst().GetCutFile()->GetRangeEnd() );
	PrintToListener(NULL, cMsg); // print to listener

	LoadBoneTranslationXmlConfigFile("X:\\gta5\\tools\\etc\\content\\animconfig\\bone_tags.xml","All");

	return &true_value;
}

Value* GetCutFileEndRange_cf(Value** arg_list, int count)
{
	return_protected(new Integer(cutfileLoader::GetInst().GetRangeEnd()));
}

Value* GetSceneOffsetTranslation_cf(Value** arg_list, int count)
{
	return_protected(new Point3Value(cutfileLoader::GetInst().GetCutFile()->GetOffset().x, cutfileLoader::GetInst().GetCutFile()->GetOffset().y, cutfileLoader::GetInst().GetCutFile()->GetOffset().z));
}

Value* GetSceneOffsetRotation_cf(Value** arg_list, int count)
{
	return_protected(new Point3Value(0, 0, cutfileLoader::GetInst().GetCutFile()->GetRotation()));
}

// load a lightxml file into max into a static instance
Value* ImportLightXML_cf(Value** arg_list, int count)
{
	check_arg_count(ImportLightXML, 1, count);
	type_check(arg_list[0],String,"string [light xml file path]");

	if(!cutfileLoader::GetInst().IsCutFileLoaded())
	{
		if(cutfileLoader::GetInst().GetInteractionMode())
		{
			MessageBox(NULL,"Cutscene data is not loaded for this scene.","Error", MB_ICONERROR);
		}

		PrintToListener("Error", "Cutscene data is not loaded for this scene."); // print to listener
		
		return &false_value;
	}

	cutfileLoader::GetInst().GetLightCutFile()->Clear();
	if(!cutfileLoader::GetInst().GetLightCutFile()->LoadFile(arg_list[0]->to_string()))
	{
		char cMsg[RAGE_MAX_PATH];
		sprintf( cMsg, "'%s' load fail.", arg_list[0]->to_string() );

		if(cutfileLoader::GetInst().GetInteractionMode())
		{
			MessageBox(NULL ,cMsg, "Error", MB_ICONERROR);
		}

		PrintToListener("Error", cMsg); // print to listener

		return &false_value;
	}

	char cMsg[RAGE_MAX_PATH];
	sprintf( cMsg, "'%s' load success.", arg_list[0]->to_string() );
	PrintToListener(NULL, cMsg); // print to listener

	return &true_value;
}

Value* IsAnimatable_cf(Value** arg_list, int count)
{
	check_arg_count(IsAnimatable, 1, count);
	type_check(arg_list[0],MAXNode,"node [light node]");

	INode* pNode = arg_list[0]->to_node();
	
	std::string outID;
	if(IsAnimatable(pNode, outID))
	{
		return &true_value;
	}

	return &false_value;
}

Value* CreateLightXML_cf(Value** arg_list, int count)
{
	cutfileLoader::GetInst().GetLightCutFile()->Clear();
	
	return &ok;
}

// add light object to lightxml
// add set/clear events for the light with the boundaries of the camera cuts the light is active within
// add attributes for the keys that have being keyed to show which cut they are within
Value* AddLightToLightXML_cf(Value** arg_list, int count)
{
	check_arg_count(AddLightToLightXML, 11, count);
	type_check(arg_list[0],MAXNode,"node [light node]");
	type_check(arg_list[1],Point3Value,"point3 [light position]");
	type_check(arg_list[2],Point3Value,"point3 [light rotation]");
	type_check(arg_list[3],Matrix3Value,"matrix3 [light transform]");
	type_check(arg_list[4],Integer,"int [light type]");
	//type_check(arg_list[5],AColor,"colour [light colour]");
	type_check(arg_list[6],Float,"float [light intensity]");
	type_check(arg_list[7],Float,"float [light fall off]");
	type_check(arg_list[8],Float,"float [light cone angle]");

	INode* pLightNode = arg_list[0]->to_node();
	Point3 p3Position = arg_list[1]->to_point3();
	Point3 p3Rotation = arg_list[2]->to_point3();
	Matrix3 mTransform = arg_list[3]->to_matrix3();
	int iLightType = arg_list[4]->to_int();

	AColor aColour = arg_list[5]->to_acolor();
	float fIntensity = arg_list[6]->to_float();
	float fFallOff = arg_list[7]->to_float();
	float fConeAngle = arg_list[8]->to_float(); 
	float fInnerConeAngle = arg_list[9]->to_float(); 
	float fExpFallOff = arg_list[10]->to_float(); 

	// create light object
	cutfLightObject* pLightObject = NULL;

	std::string strAnimate;
	if(IsAnimatable(pLightNode, strAnimate))
	{
		pLightObject = rage_new cutfAnimatedLightObject;
		cutfAnimatedLightObject* pAnimatedLightObject = dynamic_cast<cutfAnimatedLightObject*>(pLightObject);
		pAnimatedLightObject->SetAnimStreamingBase(pLightNode->GetName());
		pLightObject->SetLightStatic(false);
	}
	else
	{
		pLightObject = rage_new cutfLightObject;
		pLightObject->SetLightStatic(true);
	}

	pLightObject->SetName(pLightNode->GetName());
	pLightObject->SetLightColour(Vector3(aColour.r, aColour.g, aColour.b));
	pLightObject->SetLightIntensity(fIntensity);
	pLightObject->SetLightFallOff(fFallOff);
	pLightObject->SetLightConeAngle(fConeAngle);
	pLightObject->SetInnerConeAngle(fInnerConeAngle);
	pLightObject->SetExponentialFallOff(fExpFallOff);

	////

	Matrix34 lightTransform;
	CopyMaxMatrixToRageMatrix(mTransform, lightTransform);

	Matrix34 lightTransformCopy = lightTransform;
	Vector3 vDirection(VEC3_ZERO);
	lightTransform.Transform3x3(Vector3(0,0,-1), vDirection);
	pLightObject->SetLightDirection(vDirection);

	Vector3 vec(0,0,cutfileLoader::GetInst().GetCutFile()->GetRotation()*DtoR); 

	Matrix34 sceneTransform;
	sceneTransform.Identity();
	sceneTransform.FromEulersXYZ(vec);
	sceneTransform.d = cutfileLoader::GetInst().GetCutFile()->GetOffset();
	lightTransform.DotTranspose(sceneTransform);
	pLightObject->SetLightPosition(lightTransform.d);

	////

	Quaternion qRotation;
	lightTransform.ToQuaternion(qRotation);

	parAttributeList& attList = const_cast<parAttributeList &>(pLightObject->GetAttributeList());

	char cPosition[RAGE_MAX_PATH];
	char cRotation[RAGE_MAX_PATH];
	sprintf(cPosition, "%f,%f,%f", lightTransform.d.x,lightTransform.d.y,lightTransform.d.z );
	sprintf(cRotation, "%f,%f,%f,%f", qRotation.x,qRotation.y,qRotation.z,qRotation.w );

	attList.AddAttribute("maxrelposition",cPosition);
	attList.AddAttribute("maxrelrotation",cRotation);

	cutfTriggerLightEffectEventArgs* pLightEventArgs = NULL;

	std::string strAttachedBoneName;
	if(IsAttached(pLightNode, strAttachedBoneName))
	{
		pLightEventArgs = rage_new cutfTriggerLightEffectEventArgs();

		// Convert to hash bone id
		pLightEventArgs->SetAttachBoneHash(atHash16U(GetBoneID(strAttachedBoneName.c_str(), true).c_str()));

		unsigned int pos = strAttachedBoneName.find(":");
		if(pos != -1)
		{
			std::string strNamespace = strAttachedBoneName.substr(0, pos);
			for(int i=0; i < cutfileLoader::GetInst().GetCutFile()->GetObjectList().GetCount(); ++i)
			{
				if(stricmp(cutfileLoader::GetInst().GetCutFile()->GetObjectList()[i]->GetDisplayName(), strNamespace.c_str()) == 0)
				{
					pLightEventArgs->SetAttachParentId(cutfileLoader::GetInst().GetCutFile()->GetObjectList()[i]->GetObjectId());
					pLightEventArgs->SetAttachParentName(cutfileLoader::GetInst().GetCutFile()->GetObjectList()[i]->GetDisplayName().c_str());
				}
			}
		}

		if(pLightEventArgs->GetAttachParentId() == -1)
		{
			char cMsg[RAGE_MAX_PATH];
			sprintf( cMsg, "ERROR: light (%s) - unable to find attached object '%s' in scene cutfile", pLightObject->GetName().GetCStr(), strAttachedBoneName.c_str() );
			PrintToListener(NULL, cMsg);
		}
		
		attList.AddAttribute("bonename", strAttachedBoneName.c_str());

		INode* pNode = GetCOREInterface()->GetINodeByName(strAttachedBoneName.c_str());
		if(pNode)
		{	
			Matrix3 mMatrix = pNode->GetObjectTM(0);
			Matrix34 boneTransform;
			CopyMaxMatrixToRageMatrix(mMatrix, boneTransform);
			CopyMaxMatrixToRageMatrix(mTransform, lightTransform);

			boneTransform.ToQuaternion(qRotation);

			sprintf(cPosition, "%f,%f,%f", boneTransform.d.x,boneTransform.d.y,boneTransform.d.z );
			sprintf(cRotation, "%f,%f,%f,%f", qRotation.x,qRotation.y,qRotation.z,qRotation.w );

			attList.AddAttribute("boneposition",cPosition);
			attList.AddAttribute("bonerotation",cRotation);

			lightTransform.DotTranspose(boneTransform);
			pLightObject->SetLightPosition(lightTransform.d);

			Vector3 vDirection(VEC3_ZERO);
			lightTransform.Transform3x3(Vector3(0,0,-1), vDirection);
			pLightObject->SetLightDirection(vDirection);
		}
		else
		{
			char cMsg[RAGE_MAX_PATH];
			sprintf( cMsg, "ERROR: light (%s) - unable to find attached object '%s'", pLightObject->GetName().GetCStr(), strAttachedBoneName.c_str() );
			PrintToListener(NULL, cMsg);
		}
	}
	
	AddLightAttributesToObject(pLightNode, pLightObject);
	if (dynamic_cast<cutfAnimatedLightObject*>(pLightObject))
	{
		GetKeyFramesFromLight(pLightNode, pLightObject);
	}

	char cMsg[RAGE_MAX_PATH];
	sprintf( cMsg, "light (%s) @ %f,%f,%f - %f,%f,%f", pLightObject->GetName().GetCStr(), p3Position.x, p3Position.y, p3Position.z, p3Rotation.x, p3Rotation.y, p3Rotation.z );
	PrintToListener(NULL, cMsg);

	switch(iLightType)
	{
	case OMNI:
		pLightObject->SetLightType(CUTSCENE_POINT_LIGHT_TYPE);
		break;
	case FREESPOT:
		pLightObject->SetLightType(CUTSCENE_SPOT_LIGHT_TYPE);
		break;
	case FREEDIRECT:
		pLightObject->SetLightType(CUTSCENE_DIRECTIONAL_LIGHT_TYPE);
		break;
	case TARGETSPOT:
	case TARGETDIRECT:
	case SAUSAGE:
		char cMsg[RAGE_MAX_PATH];
		sprintf( cMsg, "RageLight type not supported on light '%s'. Light will be skipped.", pLightObject->GetName() );
		
		if(cutfileLoader::GetInst().GetInteractionMode())
		{
			MessageBox(NULL, cMsg, "Error", MB_ICONERROR);
		}

		PrintToListener("Error", cMsg); // print to listener

		return &false_value;
	}

	cutfileLoader::GetInst().GetLightCutFile()->AddObject(pLightObject);

	// get camera dummy object
	INode* pCamera = pLightNode->GetParentNode();
	if(pCamera)
	{
		char cMsg[RAGE_MAX_PATH];
		sprintf( cMsg, "light (%s) attached to camera (%s)", pLightObject->GetName().GetCStr(), pCamera->GetName() );
		PrintToListener(NULL, cMsg);

		// get the note key from the camera, this is the time
		NoteTrack* pNoteTrack = pCamera->GetNoteTrack(0);
		if(pNoteTrack)
		{
			TimeValue tStartTime = pNoteTrack->GetKeyTime(0);

			// note: object id 1 is used as this is the animation manager - this will always be 1
			cutfObjectIdNameEventArgs* pEventArgs = rage_new cutfObjectIdNameEventArgs(pLightObject->GetObjectId(), pLightNode->GetName());
			int frame = GetFrameFromMaxTime(tStartTime);
			float time = float(frame)/30.0f;
			if(PullBackFrame(cutfileLoader::GetInst().GetCutFile(), time))
			{
				time -= (1.0f / 60.0f);
				time = (time < 0) ? 0 : time;
			}

			if(pLightObject->GetType() == CUTSCENE_ANIMATED_LIGHT_OBJECT_TYPE)
			{
				cutfObjectIdEvent* pSetEvent = rage_new cutfObjectIdEvent(-2, time, CUTSCENE_SET_ANIM_EVENT, pEventArgs);
				cutfileLoader::GetInst().GetLightCutFile()->AddEvent(pSetEvent);

				char cMsg[RAGE_MAX_PATH];
				sprintf( cMsg, "event set (%s) @ %f", pLightObject->GetName().GetCStr(), pSetEvent->GetTime() );
				PrintToListener(NULL, cMsg);
			}

			cutfObjectIdEvent* pSetLightEvent = rage_new cutfObjectIdEvent(pLightObject->GetObjectId(), time, CUTSCENE_SET_LIGHT_EVENT, pLightEventArgs);
			cutfileLoader::GetInst().GetLightCutFile()->AddEvent(pSetLightEvent);
			
			if(stricmp(pCamera->GetName(),"OnAllTime") == 0)
			{
				if(pLightObject->GetType() == CUTSCENE_ANIMATED_LIGHT_OBJECT_TYPE)
				{
					cutfObjectIdEvent* pClearEvent = rage_new cutfObjectIdEvent(-2, cutfileLoader::GetInst().GetCutFile()->GetTotalDuration(), CUTSCENE_CLEAR_ANIM_EVENT, pEventArgs);
					cutfileLoader::GetInst().GetLightCutFile()->AddEvent(pClearEvent);
				}

				cutfObjectIdEvent* pClearLightEvent = rage_new cutfObjectIdEvent(pLightObject->GetObjectId(), cutfileLoader::GetInst().GetCutFile()->GetTotalDuration(), CUTSCENE_CLEAR_LIGHT_EVENT, NULL);
				cutfileLoader::GetInst().GetLightCutFile()->AddEvent(pClearLightEvent);

				// add the camera name as an attribute
				parAttributeList& attList = const_cast<parAttributeList &>(pLightObject->GetAttributeList());
				attList.AddAttribute("camera",pCamera->GetName());
			}
			else
			{
				// we need the start time for the next camera, this will be a camera or the end
				INode* pRoot = GetCOREInterface()->GetRootNode();
				if(pRoot)
				{
					TimeValue tEndTime = INT_MAX;
					for(int i=0; i < pRoot->NumberOfChildren(); ++i)
					{
						INode* pChild = pRoot->GetChildNode(i);

						if(pChild->GetObjectRef()->ClassID() == Class_ID(DUMMY_CLASS_ID,0))
						{
							// get the next cut along
							NoteTrack* pNoteTrack = pChild->GetNoteTrack(0);
							if(pNoteTrack)
							{
								TimeValue tKeyValue = pNoteTrack->GetKeyTime(0);

								if(tStartTime < tKeyValue && tKeyValue < tEndTime)
								{
									tEndTime = tKeyValue;
								}
							}
						}
					}

					//HACK:  This is a legacy hack needed to deal with frame range exporter issues and ensuring that 
					//clear events occur at the end frame of the cutscene instead of the frame before.
					//Fixes popping issues from legacy files.
					int frameTime = GetFrameFromMaxTime(tEndTime);
					bool bEnd = false;
					if ( frameTime == cutfileLoader::GetInst().GetRangeEnd())
					{
						bEnd = true;
						tEndTime = GetMaxTimeFromFrame(cutfileLoader::GetInst().GetRangeEnd()+1);
					}

					if(tEndTime == INT_MAX)
					{
						bEnd = true;
						tEndTime = GetMaxTimeFromFrame(cutfileLoader::GetInst().GetRangeEnd()+1);
					}

					float fEnd = GetFrameFromMaxTime(tEndTime)/30.0f;
					if(!bEnd)
					{
						if(PullBackFrame(cutfileLoader::GetInst().GetCutFile(), fEnd))
						{
							fEnd -= (1.0f / 60.0f);
						}
					}
					
					if(pLightObject->GetType() == CUTSCENE_ANIMATED_LIGHT_OBJECT_TYPE)
					{
						cutfObjectIdEvent* pClearEvent = rage_new cutfObjectIdEvent(-2, fEnd, CUTSCENE_CLEAR_ANIM_EVENT, pEventArgs);
						cutfileLoader::GetInst().GetLightCutFile()->AddEvent(pClearEvent);

						cMsg[0] = 0;
						sprintf( cMsg, "event clear (%s) @ %f", pLightObject->GetName().GetCStr(), pClearEvent->GetTime() );
						PrintToListener(NULL, cMsg);
					}

					cutfObjectIdEvent* pLightClearEvent = rage_new cutfObjectIdEvent(pLightObject->GetObjectId(), fEnd, CUTSCENE_CLEAR_LIGHT_EVENT, NULL);
					cutfileLoader::GetInst().GetLightCutFile()->AddEvent(pLightClearEvent);

					// add the camera name as an attribute
					parAttributeList& attList = const_cast<parAttributeList &>(pLightObject->GetAttributeList());
					attList.AddAttribute("camera",pCamera->GetName());
				}
			}
		}
	}

	return &true_value;
}

Value* GetLightCount_cf(Value** arg_list, int count)
{
	return_protected(new Integer(cutfileLoader::GetInst().GetLightCutFile()->GetObjectList().GetCount()));
}

Value* GetLightPosition_cf(Value** arg_list, int count)
{
	check_arg_count(GetLightPosition, 1, count);
	type_check(arg_list[0],Integer,"integer [light index]");

	cutfObject* pObject = cutfileLoader::GetInst().GetLightCutFile()->GetObjectList()[arg_list[0]->to_int()];
	cutfLightObject* pLight = dynamic_cast<cutfLightObject*>(pObject);

	Vector3 vLightPosition(0,0,0);

	parAttributeList& attList = const_cast<parAttributeList &>(pLight->GetAttributeList());

	for(int i=0; i < attList.GetAttributeArray().GetCount(); ++i)
	{
		if(stricmp(attList.GetAttributeArray()[i].GetName(), "maxrelposition") == 0)
		{
			for(int j=0; j < attList.GetAttributeArray().GetCount(); ++j)
			{
				if(stricmp(attList.GetAttributeArray()[j].GetName(), "maxrelrotation") == 0)
				{
					// build matrix from the relatives
					atString strPosition(attList.GetAttributeArray()[i].GetStringValue());

					atArray<atString> atSplit;
					strPosition.Split(atSplit, "," ,true);

					vLightPosition.x = atof(atSplit[0]);
					vLightPosition.y = atof(atSplit[1]);
					vLightPosition.z = atof(atSplit[2]);

					Quaternion qLightRotation(0,0,0,0);

					atString strRotation(attList.GetAttributeArray()[j].GetStringValue());

					atSplit.clear();
					strRotation.Split(atSplit, "," ,true);

					qLightRotation.x = atof(atSplit[0]);
					qLightRotation.y = atof(atSplit[1]);
					qLightRotation.z = atof(atSplit[2]);
					qLightRotation.w = atof(atSplit[3]);

					//
					Matrix34 lightTransform; 
					lightTransform.Identity();
					lightTransform.FromQuaternion(qLightRotation);
					lightTransform.d = vLightPosition;

					Vector3 vec(0,0,cutfileLoader::GetInst().GetCutFile()->GetRotation()*DtoR);   

					Matrix34 sceneTransform; 
					sceneTransform.Identity(); 
					sceneTransform.FromEulersXYZ(vec); 
					sceneTransform.d = cutfileLoader::GetInst().GetCutFile()->GetOffset(); 

					Matrix34 fin;
					fin.Dot(lightTransform, sceneTransform);

					vLightPosition = fin.d;
				}
			}
		}
		else if(stricmp(attList.GetAttributeArray()[i].GetName(), "maxposition") == 0)
		{
			atString strPosition(attList.GetAttributeArray()[i].GetStringValue());

			atArray<atString> atSplit;
			strPosition.Split(atSplit, "," ,true);

			vLightPosition.x = atof(atSplit[0]);
			vLightPosition.y = atof(atSplit[1]);
			vLightPosition.z = atof(atSplit[2]);
		}
	}

	return_protected(new Point3Value(vLightPosition.x,vLightPosition.y,vLightPosition.z));
}

Value* RotationIsRelative_cf(Value** arg_list, int count)
{
	check_arg_count(RotationIsRelative, 1, count);
	type_check(arg_list[0],Integer,"integer [light index]");

	cutfObject* pObject = cutfileLoader::GetInst().GetLightCutFile()->GetObjectList()[arg_list[0]->to_int()];
	cutfLightObject* pLight = dynamic_cast<cutfLightObject*>(pObject);

	parAttributeList& attList = const_cast<parAttributeList &>(pLight->GetAttributeList());

	for(int i=0; i < attList.GetAttributeArray().GetCount(); ++i)
	{
		if(stricmp(attList.GetAttributeArray()[i].GetName(), "maxrelrotation") == 0)
		{
			return &true_value;
		}
	}

	return &false_value;
}

Value* GetLightRotation_cf(Value** arg_list, int count)
{
	check_arg_count(GetLightRotation, 1, count);
	type_check(arg_list[0],Integer,"integer [light index]");

	cutfObject* pObject = cutfileLoader::GetInst().GetLightCutFile()->GetObjectList()[arg_list[0]->to_int()];
	cutfLightObject* pLight = dynamic_cast<cutfLightObject*>(pObject);

	Vector3 vLightRotation(0,0,0);

	parAttributeList& attList = const_cast<parAttributeList &>(pLight->GetAttributeList());

	for(int i=0; i < attList.GetAttributeArray().GetCount(); ++i)
	{
		if(stricmp(attList.GetAttributeArray()[i].GetName(), "maxrotation") == 0)
		{
			atString strPosition(attList.GetAttributeArray()[i].GetStringValue());

			atArray<atString> atSplit;
			strPosition.Split(atSplit, "," ,true);

			vLightRotation.x = atof(atSplit[0]);
			vLightRotation.y = atof(atSplit[1]);
			vLightRotation.z = atof(atSplit[2]);
		}
	}

	return_protected(new Point3Value(vLightRotation.x,vLightRotation.y,vLightRotation.z));
}

Value* GetLightAttachedName_cf(Value** arg_list, int count)
{
	check_arg_count(GetLightAttachedName, 1, count);
	type_check(arg_list[0],Integer,"integer [light index]");

	cutfObject* pObject = cutfileLoader::GetInst().GetLightCutFile()->GetObjectList()[arg_list[0]->to_int()];
	cutfLightObject* pLight = dynamic_cast<cutfLightObject*>(pObject);

	parAttributeList& attList = const_cast<parAttributeList &>(pLight->GetAttributeList());
	parAttribute* pAttr = attList.FindAttribute("bonename");
	if(pAttr)
	{
		return_protected(new String(pAttr->GetStringValue()));
	}

	return_protected(new String(""));
}

Value* GetLightAttachedPosition_cf(Value** arg_list, int count)
{
	check_arg_count(GetLightAttachedPosition, 1, count);
	type_check(arg_list[0],Integer,"integer [light index]");

	cutfObject* pObject = cutfileLoader::GetInst().GetLightCutFile()->GetObjectList()[arg_list[0]->to_int()];
	cutfLightObject* pLight = dynamic_cast<cutfLightObject*>(pObject);

	parAttributeList& attList = const_cast<parAttributeList &>(pLight->GetAttributeList());
	parAttribute* pAttr = attList.FindAttribute("boneposition");
	if(pAttr)
	{
		atString strPosition(pAttr->GetStringValue());
		atArray<atString> atPosition;
		strPosition.Split(atPosition, ",", true);

		Point3 p(atof(atPosition[0]),atof(atPosition[1]),atof(atPosition[2]));
		return_protected(new Point3Value(p.x, p.y, p.z));
	}

	return_protected(new Point3Value(0.0f,0.0f,0.0f));
}

Value* GetLightAttachedRotation_cf(Value** arg_list, int count)
{
	check_arg_count(GetLightAttachedRotation, 1, count);
	type_check(arg_list[0],Integer,"integer [light index]");

	cutfObject* pObject = cutfileLoader::GetInst().GetLightCutFile()->GetObjectList()[arg_list[0]->to_int()];
	cutfLightObject* pLight = dynamic_cast<cutfLightObject*>(pObject);

	parAttributeList& attList = const_cast<parAttributeList &>(pLight->GetAttributeList());
	parAttribute* pAttr = attList.FindAttribute("bonerotation");
	if(pAttr)
	{
		atString strRotation(pAttr->GetStringValue());
		atArray<atString> atRotation;
		strRotation.Split(atRotation, ",", true);

		Quat q(atof(atRotation[0]),atof(atRotation[1]),atof(atRotation[2]),atof(atRotation[3]));
		return_protected(new Point4Value(q.x, q.y, q.z, q.w));
	}

	return_protected(new Point4Value(0.0f, 0.0f, 0.0f, 0.0f));
}

Value* GetLightRotationQuat_cf(Value** arg_list, int count)
{
	check_arg_count(GetLightRotationQuat, 1, count);
	type_check(arg_list[0],Integer,"integer [light index]");

	cutfObject* pObject = cutfileLoader::GetInst().GetLightCutFile()->GetObjectList()[arg_list[0]->to_int()];
	cutfLightObject* pLight = dynamic_cast<cutfLightObject*>(pObject);

	Quaternion qLightRotation(0,0,0,0);

	parAttributeList& attList = const_cast<parAttributeList &>(pLight->GetAttributeList());

	for(int i=0; i < attList.GetAttributeArray().GetCount(); ++i)
	{
		if(stricmp(attList.GetAttributeArray()[i].GetName(), "maxrelposition") == 0)
		{
			for(int j=0; j < attList.GetAttributeArray().GetCount(); ++j)
			{
				if(stricmp(attList.GetAttributeArray()[j].GetName(), "maxrelrotation") == 0)
				{
					atString strPosition(attList.GetAttributeArray()[i].GetStringValue());

					atArray<atString> atSplit;
					strPosition.Split(atSplit, "," ,true);
					
					Vector3 vLightPosition(0,0,0);
					vLightPosition.x = atof(atSplit[0]);
					vLightPosition.y = atof(atSplit[1]);
					vLightPosition.z = atof(atSplit[2]);

					atString strRotation(attList.GetAttributeArray()[j].GetStringValue());

					atSplit.clear();
					strRotation.Split(atSplit, "," ,true);

					qLightRotation.x = atof(atSplit[0]);
					qLightRotation.y = atof(atSplit[1]);
					qLightRotation.z = atof(atSplit[2]);
					qLightRotation.w = atof(atSplit[3]);

					Matrix34 lightTransform; 
					lightTransform.Identity(); 
					lightTransform.FromQuaternion(qLightRotation);
					lightTransform.d = vLightPosition;

					Vector3 vec(0,0,cutfileLoader::GetInst().GetCutFile()->GetRotation()*DtoR);   

					Matrix34 sceneTransform; 
					sceneTransform.Identity(); 
					sceneTransform.FromEulersXYZ(vec); 
					sceneTransform.d = cutfileLoader::GetInst().GetCutFile()->GetOffset(); 

					Matrix34 finalTransform;
					finalTransform.Dot(lightTransform, sceneTransform);

					finalTransform.ToQuaternion(qLightRotation);
				}
			}
		}
	}

	return_protected(new Point4Value(qLightRotation.x, qLightRotation.y, qLightRotation.z, qLightRotation.w));
}

Value* GetLightColour_cf(Value** arg_list, int count)
{
	check_arg_count(GetLightColour, 1, count);
	type_check(arg_list[0],Integer,"integer [light index]");

	cutfObject* pObject = cutfileLoader::GetInst().GetLightCutFile()->GetObjectList()[arg_list[0]->to_int()];
	cutfLightObject* pLight = dynamic_cast<cutfLightObject*>(pObject);

	return_protected(new Point3Value(pLight->GetLightColour().x, pLight->GetLightColour().y, pLight->GetLightColour().z));
}

Value* GetLightIntensity_cf(Value** arg_list, int count)
{
	check_arg_count(GetLightIntensity, 1, count);
	type_check(arg_list[0],Integer,"integer [light index]");

	cutfObject* pObject = cutfileLoader::GetInst().GetLightCutFile()->GetObjectList()[arg_list[0]->to_int()];
	cutfLightObject* pLight = dynamic_cast<cutfLightObject*>(pObject);

	return_protected(new Float(pLight->GetLightIntensity()));
}

Value* GetLightFallOff_cf(Value** arg_list, int count)
{
	check_arg_count(GetLightFallOff, 1, count);
	type_check(arg_list[0],Integer,"integer [light index]");

	cutfObject* pObject = cutfileLoader::GetInst().GetLightCutFile()->GetObjectList()[arg_list[0]->to_int()];
	cutfLightObject* pLight = dynamic_cast<cutfLightObject*>(pObject);

	return_protected(new Float(pLight->GetLightFallOff()));
}

Value* GetLightExpFallOff_cf(Value** arg_list, int count)
{
	check_arg_count(GetLightExpFallOff, 1, count);
	type_check(arg_list[0],Integer,"integer [light index]");

	cutfObject* pObject = cutfileLoader::GetInst().GetLightCutFile()->GetObjectList()[arg_list[0]->to_int()];
	cutfLightObject* pLight = dynamic_cast<cutfLightObject*>(pObject);

	return_protected(new Float(pLight->GetExponentialFallOff()));
}

Value* GetLightConeAngle_cf(Value** arg_list, int count)
{
	check_arg_count(GetLightConeAngle, 1, count);
	type_check(arg_list[0],Integer,"integer [light index]");

	cutfObject* pObject = cutfileLoader::GetInst().GetLightCutFile()->GetObjectList()[arg_list[0]->to_int()];
	cutfLightObject* pLight = dynamic_cast<cutfLightObject*>(pObject);

	return_protected(new Float(pLight->GetLightConeAngle()));
}

Value* GetLightInnerConeAngle_cf(Value** arg_list, int count)
{
	check_arg_count(GetLightInnerConeAngle, 1, count);
	type_check(arg_list[0],Integer,"integer [light index]");

	cutfObject* pObject = cutfileLoader::GetInst().GetLightCutFile()->GetObjectList()[arg_list[0]->to_int()];
	cutfLightObject* pLight = dynamic_cast<cutfLightObject*>(pObject);

	return_protected(new Float(pLight->GetInnerConeAngle()));
}

Value* GetLightType_cf(Value** arg_list, int count)
{
	check_arg_count(GetLightType, 1, count);
	type_check(arg_list[0],Integer,"integer [light index]");

	cutfObject* pObject = cutfileLoader::GetInst().GetLightCutFile()->GetObjectList()[arg_list[0]->to_int()];
	cutfLightObject* pLight = dynamic_cast<cutfLightObject*>(pObject);

	s32 fLightType = pLight->GetLightType();

	switch(fLightType)
	{
	case CUTSCENE_POINT_LIGHT_TYPE:
		{
			return_protected(new Float(OMNI));
			break;
		}
	case CUTSCENE_SPOT_LIGHT_TYPE:
		{
			return_protected(new Float(FREESPOT));
			break;
		}
	case CUTSCENE_DIRECTIONAL_LIGHT_TYPE:
		{
			return_protected(new Float(FREEDIRECT));
			break;
		}
	default:
		{
			return_protected(new Float(FREESPOT));
		}
	}

	return_protected(new Float(FREESPOT));
}

// get the name of the light object
Value* GetLightName_cf(Value** arg_list, int count)
{
	check_arg_count(GetLightName, 1, count);
	type_check(arg_list[0],Integer,"integer [light index]");

	cutfObject* pObject = cutfileLoader::GetInst().GetLightCutFile()->GetObjectList()[arg_list[0]->to_int()];
	cutfLightObject* pLight = dynamic_cast<cutfLightObject*>(pObject);

	return_protected(new String(pLight->GetName().GetCStr()));
}

Value* GetLightCameraAnimatedFlag_cf(Value** arg_list, int count)
{
	check_arg_count(GetLightCameraAnimatedFlag, 1, count);
	type_check(arg_list[0],Integer,"integer [light index]");

	cutfObject* pObject = cutfileLoader::GetInst().GetLightCutFile()->GetObjectList()[arg_list[0]->to_int()];
	cutfLightObject* pLight = dynamic_cast<cutfLightObject*>(pObject);

	if((pObject->GetType() != CUTSCENE_ANIMATED_LIGHT_OBJECT_TYPE))
	{
		if(!pLight->IsLightStatic())
			return &false_value;

		return &true_value;
	}

	return &false_value;
}

Value* GetLightCamera_cf(Value** arg_list, int count)
{
	check_arg_count(GetLightCamera, 1, count);
	type_check(arg_list[0],Integer,"integer [light index]");

	cutfObject* pObject = cutfileLoader::GetInst().GetLightCutFile()->GetObjectList()[arg_list[0]->to_int()];
	cutfLightObject* pLight = dynamic_cast<cutfLightObject*>(pObject);

	for(int i=0 ; i < pLight->GetAttributeList().GetAttributeArray().GetCount(); ++i)
	{
		if(stricmp(pLight->GetAttributeList().GetAttributeArray()[i].GetName(),"camera") == 0)
			return_protected(new String(pLight->GetAttributeList().GetAttributeArray()[i].GetStringValue()));
	}

	return_protected(new String(""));
}

// set attributes on a node
Value* SetLightAttributes_cf(Value** arg_list, int count)
{
	check_arg_count(SetLightAttributes, 2, count);
	type_check(arg_list[0],Integer,"integer [light index]");
	type_check(arg_list[1],MAXNode,"node [light node]");

	INode* pLightNode = arg_list[1]->to_node();

	cutfObject* pObject = cutfileLoader::GetInst().GetLightCutFile()->GetObjectList()[arg_list[0]->to_int()];
	cutfLightObject* pLight = dynamic_cast<cutfLightObject*>(pObject);

	SetLightAttributesOnNode(pLightNode, pLight);

	return_protected(new String(pLight->GetName().GetCStr()));
}

Value* SetLightKeyFrames_cf(Value** arg_list, int count)
{
	check_arg_count(SetLightAttributes, 2, count);
	type_check(arg_list[0],Integer,"integer [light index]");
	type_check(arg_list[1],MAXNode,"node [light node]");

	INode* pLightNode = arg_list[1]->to_node();

	cutfObject* pObject = cutfileLoader::GetInst().GetLightCutFile()->GetObjectList()[arg_list[0]->to_int()];
	cutfLightObject* pLight = dynamic_cast<cutfLightObject*>(pObject);

	SetKeyFramesOnLight(pLightNode, pLight);

	return_protected(new String(pLight->GetName().GetCStr()));
}

Value* SaveLightXML_cf(Value** arg_list, int count)
{
	check_arg_count(SaveLightXML, 2, count);
	type_check(arg_list[0],String,"string [light xml directory]");
	//type_check(arg_list[1],bool,"bool [remove animated lights from xml - tune]");
	
	char* pLightFile = arg_list[0]->to_string();

	// Store the concat lists in the light so we can use this to split it later
	atFixedArray<cutfCutsceneFile2::SConcatData, CUTSCENE_MAX_CONCAT> &concatDataList
		= const_cast<atFixedArray<cutfCutsceneFile2::SConcatData, CUTSCENE_MAX_CONCAT> &>( cutfileLoader::GetInst().GetLightCutFile()->GetConcatDataList() );
	
	concatDataList = cutfileLoader::GetInst().GetCutFile()->GetConcatDataList();

	atArray<cutfCutsceneFile2::SDiscardedFrameData> &discardedDataList
		= const_cast<atArray<cutfCutsceneFile2::SDiscardedFrameData> &>( cutfileLoader::GetInst().GetLightCutFile()->GetDiscardedFrameList() );

	discardedDataList = cutfileLoader::GetInst().GetCutFile()->GetDiscardedFrameList();

	cutfileLoader::GetInst().GetLightCutFile()->SetTotalDuration(cutfileLoader::GetInst().GetCutFile()->GetTotalDuration());
	cutfileLoader::GetInst().GetLightCutFile()->SetRangeStart(cutfileLoader::GetInst().GetCutFile()->GetRangeStart());
	cutfileLoader::GetInst().GetLightCutFile()->SetRangeEnd(cutfileLoader::GetInst().GetCutFile()->GetRangeEnd());

	// Remove animated lights from the file, this is used for tune files as we cannot hotload an animated light
	if(arg_list[1]->to_bool())
	{
		for(int i=0; i < cutfileLoader::GetInst().GetLightCutFile()->GetObjectList().GetCount(); ++i)
		{
			if(cutfileLoader::GetInst().GetLightCutFile()->GetObjectList()[i]->GetType() == CUTSCENE_ANIMATED_LIGHT_OBJECT_TYPE)
			{
				char cMsg[RAGE_MAX_PATH];
				sprintf( cMsg, "Ignoring animated light '%s' from '%s'", cutfileLoader::GetInst().GetLightCutFile()->GetObjectList()[i]->GetDisplayName().c_str(), pLightFile );
				PrintToListener("Warning", cMsg); // print to listener
				
				cutfileLoader::GetInst().GetLightCutFile()->RemoveObject(cutfileLoader::GetInst().GetLightCutFile()->GetObjectList()[i]);

				i=0;
			}
		}
	}

	if(cutfileLoader::GetInst().GetLightCutFile()->GetObjectList().GetCount() == 0)
	{
		char cMsg[RAGE_MAX_PATH];
		sprintf( cMsg, "'%s' contains no lights.", pLightFile );

		if(cutfileLoader::GetInst().GetInteractionMode())
		{
			MessageBox(NULL ,cMsg, "Warning", MB_ICONWARNING);
		}

		PrintToListener("Warning", cMsg); // print to listener
	}

	cutfileLoader::GetInst().GetLightCutFile()->SortEvents();
	if(!cutfileLoader::GetInst().GetLightCutFile()->SaveFile(pLightFile))
	{
		if((fiDeviceLocal::GetInstance().GetAttributes(pLightFile) & FILE_ATTRIBUTE_READONLY) == FILE_ATTRIBUTE_READONLY)
		{
			char cMsg[RAGE_MAX_PATH];
			sprintf( cMsg, "Unable to save lightxml file '%s'. File is read-only.", pLightFile );

			if(cutfileLoader::GetInst().GetInteractionMode())
			{
				MessageBox(NULL ,cMsg, "Error", MB_ICONERROR);
			}

			PrintToListener("Error", cMsg); // print to listener	
		}
		else
		{
			char cMsg[RAGE_MAX_PATH];
			sprintf( cMsg, "'%s' save fail.", pLightFile );

			if(cutfileLoader::GetInst().GetInteractionMode())
			{
				MessageBox(NULL ,cMsg, "Error", MB_ICONERROR);
			}

			PrintToListener("Error", cMsg); // print to listener
		}

		return &false_value;
	}

	char cMsg[RAGE_MAX_PATH];
	sprintf( cMsg, "'%s' save success.", pLightFile );
	PrintToListener(NULL, cMsg); // print to listener

	return &true_value;
}

Value* CreateAnim_cf(Value** arg_list, int count)
{
	check_arg_count(CreateAnim, 6, count);
	//type_check(arg_list[0],Float,"float [scene start time]");
	//type_check(arg_list[1],Float,"float [scene end time]");
	//type_check(arg_list[2],Integer,"integer [scene frame count]");
	type_check(arg_list[3],String,"string [light name]");
	type_check(arg_list[4],String,"string [save directory]");
	type_check(arg_list[5],Integer,"int [light type]");

	float fStartTime = arg_list[0]->to_float();
	float fEndTime =  arg_list[1]->to_float();
	int iFrames = arg_list[2]->to_int();
	

	//HACK: There is a floating point imprecision in Max where it may chop off a frame.
	//Without this, animations would be on the wrong duration.
	fEndTime = iFrames / 30.0f;
	
	char* pNodeName = arg_list[3]->to_string();
	char* pDirectory = arg_list[4]->to_string();
	int iLightType = arg_list[5]->to_int();

	int index = cutfileLoader::GetInst().CreateLightExporter(fStartTime, fEndTime, iFrames, pNodeName, pDirectory, iLightType);

	return_protected(new Integer(index));
}

Value* AddFrameToAnim_cf(Value** arg_list, int count)
{
	check_arg_count(AddFrameToAnim, 3, count);
	type_check(arg_list[0],Integer,"integer [anim index]");
	type_check(arg_list[1],MAXNode,"node [light transform]");
	//type_check(arg_list[2],TimeValue,"frame [light frame]");

	int index = arg_list[0]->to_int();
	INode* pNode = arg_list[1]->to_node();
	TimeValue frame = arg_list[2]->to_timevalue();

	cutfileLoader::GetInst().GetLightExporter(index)->AddFrameData(pNode, frame, cutfileLoader::GetInst().GetCutFile());

	return &ok;
}

Value* SaveAnim_cf(Value** arg_list, int count)
{
	check_arg_count(SaveAnim, 1, count);

	type_check(arg_list[0],Integer,"integer [anim index]");

	int index = arg_list[0]->to_int();

	if(!cutfileLoader::GetInst().GetLightExporter(index)->SaveAnim())
	{
		return &false_value;
	}

	return &true_value;
}

Value* Reset_cf(Value** arg_list, int count)
{
	check_arg_count(Reset, 0, count);

	cutfileLoader::GetInst().Reset();

	return &ok;
}

Value* RenameLightsWithinFile_cf(Value** arg_list, int count)
{
	check_arg_count(RenameLightsWithinFile, 1, count);

	type_check(arg_list[0],String,"string [light file dir]");

	char cLightFilePath[RAGE_MAX_PATH];
	sprintf_s(cLightFilePath, RAGE_MAX_PATH, "%s\\%s", arg_list[0]->to_string(), "data.lightxml");

	cutfCutsceneFile2* pCutFile = rage_new cutfCutsceneFile2();
	if(pCutFile->LoadFile(cLightFilePath))
	{
		char cMsg[RAGE_MAX_PATH];
		sprintf( cMsg, "RENAME - Loaded '%s'", cLightFilePath );
		PrintToListener(NULL, cMsg); // print to listener

		atString strPath(arg_list[0]->to_string());
		atArray<atString> atDirectories;
		strPath.Split(atDirectories, "\\", true);

		atArray<cutfObject *> &objectList = const_cast<atArray<cutfObject *> &>( pCutFile->GetObjectList() );

		for(int i=0; i < objectList.GetCount(); ++i)
		{
			cutfLightObject* pLightObject = dynamic_cast<cutfLightObject*>(objectList[i]);
			if(pLightObject)
			{
				atString strLightName(pLightObject->GetDisplayName());

				if(strLightName.IndexOf(atDirectories[atDirectories.GetCount()-1].c_str()) != -1) 
				{
					char cMsg[RAGE_MAX_PATH];
					sprintf( cMsg, "RENAME - skipped '%s'", strLightName.c_str());
					PrintToListener(NULL, cMsg); // print to listener
					continue;
				}

				char cNewLightName[RAGE_MAX_PATH];
				sprintf_s(cNewLightName, RAGE_MAX_PATH, "%s_%s", strLightName.c_str(), atDirectories[atDirectories.GetCount()-1].c_str());

				char cMsg[RAGE_MAX_PATH];
				sprintf( cMsg, "RENAME - '%s' to '%s'", strLightName.c_str(), cNewLightName );
				PrintToListener(NULL, cMsg); // print to listener

				pLightObject->SetName(cNewLightName);
			}
		}
		
		if(!pCutFile->SaveFile(cLightFilePath))
		{
			return &false_value;
		}
	}

	return &true_value;
}

bool ProcessContent(atString strContent, bool bRebuild, bool bPreview)
{
	atString convertExe;
	char cToolsRootPath[RAGE_MAX_PATH];

	gs_GameView.GetToolsRootDir( cToolsRootPath, sizeof(cToolsRootPath) );

	convertExe = atString( atString(cToolsRootPath), "\\ironlib\\lib\\RSG.Pipeline.Convert.exe" );

	char cCmdLine[RAGE_MAX_PATH * 2];
	memset(cCmdLine, 0, RAGE_MAX_PATH * 2);
	safecat( cCmdLine, convertExe.c_str(), sizeof(cCmdLine) );
	safecat( cCmdLine, " " , sizeof(cCmdLine) );

	if( bPreview )
		safecat( cCmdLine, " --preview" , sizeof(cCmdLine) );
	if( bRebuild )
		safecat( cCmdLine, " --rebuild" , sizeof(cCmdLine) );

	TCHAR _defaultBranch[_MAX_PATH]  = {0};
	if(gs_GameView.GetDefaultBranch(_defaultBranch, _MAX_PATH))
	{
		safecat( cCmdLine, " --branch " , sizeof(cCmdLine) );
		safecat( cCmdLine, _defaultBranch , sizeof(cCmdLine) );
	}

	safecat( cCmdLine, " " , sizeof(cCmdLine) );
	safecat( cCmdLine, strContent.c_str() , sizeof(cCmdLine) );

	PrintToListener(NULL, cCmdLine);

	int cmdResult = system( cCmdLine );
	if ( cmdResult != 0 )
	{
		char cMsg[RAGE_MAX_PATH];
		sprintf(cMsg,"ERROR: cmd '%s' returned %d.", cCmdLine, cmdResult );

		PrintToListener(NULL, cMsg);

		return false;
	}

	return true;
}

void GetClipFiles(const char* pDir, atArray<atString>& atFiles)
{
	char cPath[RAGE_MAX_PATH];
	sprintf(cPath, "%s\\*", pDir);

	WIN32_FIND_DATA fileData;
	HANDLE file = FindFirstFile( cPath, &fileData);

	while ( FindNextFile( file, &fileData ) != 0 )
	{
		atString strFile(fileData.cFileName);
		if(strFile.EndsWith(".clip"))
		{
			char cAbsolutePath[RAGE_MAX_PATH];
			sprintf(cAbsolutePath, "%s\\%s", pDir, fileData.cFileName);
			atFiles.PushAndGrow(atString(cAbsolutePath));
		}
	}
}

void GetSubDirs(const char* pDir, atArray<atString>& atDirs)
{
	WIN32_FIND_DATA dirData;
	HANDLE dir = FindFirstFileEx( pDir, FindExInfoStandard, &dirData, 
		FindExSearchLimitToDirectories, NULL, 0 );

	while ( FindNextFile( dir, &dirData ) != 0 )
	{
		if(dirData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			atDirs.PushAndGrow(atString(dirData.cFileName));
		}
	}
}

void GetDlcProjects(atMap<atString, atString>& atProjects)
{
	atProjects.Reset();

	for(int i=0; i < gs_GameView.GetNumDlcProjects(); ++i)
	{
		char cDlcName[RAGE_MAX_PATH];
		gs_GameView.GetDlcProjectName(i, cDlcName, RAGE_MAX_PATH);

		char cDlcRoot[RAGE_MAX_PATH];
		gs_GameView.GetDlcProjectRoot(i, cDlcRoot, RAGE_MAX_PATH);

		atProjects.Insert(atString(cDlcName), atString(cDlcRoot));
	}
}

atString GetProjectOrDlcArtDir(atString strSource)
{
	atMap<atString, atString> atDlcProjects;
	GetDlcProjects(atDlcProjects);

	atArray<atString> atFbxPathEntries;
	strSource.Replace("/", "\\");
	strSource.Split(atFbxPathEntries, "\\");

	atMap<atString, atString>::Iterator entry = atDlcProjects.CreateIterator();
	for ( entry.Start(); !entry.AtEnd(); entry.Next() )
	{
		for(int j=0; j < atFbxPathEntries.GetCount(); ++j)
		{
			if(stricmp(entry.GetKey().c_str(), atFbxPathEntries[j].c_str()) == 0)
			{
				atString strDlcName(entry.GetKey());
				strDlcName.Lowercase();

				TCHAR art[MAX_PATH];
				if(gs_GameView.GetDlcProjectArt(strDlcName.c_str(), art, MAX_PATH))
				{
					atString atArt(art);
					atArt.Replace("$(root)", entry.GetData());
					atArt.Replace("$(name)",entry.GetKey() );
					return atArt;
				}
			}
		}
	}

	TCHAR _artDir[_MAX_PATH]  = {0};
	gs_GameView.GetArtDir(_artDir, _MAX_PATH);

	return atString(_artDir);
}

atString GetProjectOrDlcAssetsDir(atString strSource)
{
	atMap<atString, atString> atDlcProjects;
	GetDlcProjects(atDlcProjects);

	atArray<atString> atFbxPathEntries;
	strSource.Replace("/", "\\");
	strSource.Split(atFbxPathEntries, "\\");

	atMap<atString, atString>::Iterator entry = atDlcProjects.CreateIterator();
	for ( entry.Start(); !entry.AtEnd(); entry.Next() )
	{
		for(int j=0; j < atFbxPathEntries.GetCount(); ++j)
		{
			if(stricmp(entry.GetKey().c_str(), atFbxPathEntries[j].c_str()) == 0)
			{
				atString strDlcName(entry.GetKey());
				strDlcName.Lowercase();

				TCHAR assets[MAX_PATH];
				if(gs_GameView.GetDlcProjectAssets(strDlcName.c_str(), assets, MAX_PATH))
				{
					atString atAssets(assets);
					atAssets.Replace("$(root)", entry.GetData());
					atAssets.Replace("$(name)",entry.GetKey() );
					return atAssets;
				}
			}
		}
	}

	TCHAR _assetDir[_MAX_PATH]  = {0};
	gs_GameView.GetAssetsDir(_assetDir, _MAX_PATH);

	return atString(_assetDir);
}

bool IsDlc(atString strSource)
{
	atMap<atString, atString> atDlcProjects;
	GetDlcProjects(atDlcProjects);

	atArray<atString> atFbxPathEntries;
	strSource.Replace("/", "\\");
	strSource.Split(atFbxPathEntries, "\\");

	atMap<atString, atString>::Iterator entry = atDlcProjects.CreateIterator();
	for ( entry.Start(); !entry.AtEnd(); entry.Next() )
	{
		for(int j=0; j < atFbxPathEntries.GetCount(); ++j)
		{
			if(stricmp(entry.GetKey().c_str(), atFbxPathEntries[j].c_str()) == 0)
			{
				return true;
			}
		}
	}

	return false;
}

bool BuildDictionary(const char* pSrcDir, const char* pFBXFile, bool bBuildZip, bool bPreview)
{
	char cFBXFile[RAGE_MAX_PATH];
	gs_GameView.GetEnvironment()->subst(cFBXFile, RAGE_MAX_PATH, pFBXFile);
	
	atString outputPath(cFBXFile);
	outputPath.Replace("/", "\\");
	outputPath.Lowercase();

	atString _artDir = GetProjectOrDlcArtDir(atString(cFBXFile));
	atString _assetDir = GetProjectOrDlcAssetsDir(atString(cFBXFile));

	char cScenePath[RAGE_MAX_PATH];
	sprintf(cScenePath, "%s\\animation\\cutscene\\%s", _artDir.c_str(), "!!scenes");
	
	// HACK FOR SCENES NOT EXPORTED IN NG, WE WILL USE $(art) EVENTUALLY.
	if(outputPath.StartsWith("x:\\gta5\\art\\animation\\cutscene\\!!scenes"))
	{
		sprintf(cScenePath, "x:\\gta5\\art\\animation\\cutscene\\!!scenes");
	}

	atString atScenePath(cScenePath);
	atScenePath.Replace("/", "\\");
	atScenePath.Lowercase();

	if(outputPath.StartsWith(atScenePath))
	{
		outputPath.Replace(atScenePath, "");
	}

	atArray<atString> outputPathTokens;
	outputPath.Split( outputPathTokens, "\\", true );

	const char* pSceneName = ASSET.FileName(pSrcDir);

	char czZip[RAGE_MAX_PATH];
	sprintf(czZip, "%s\\export\\anim\\cutscene\\%s\\%s.icd.zip", _assetDir.c_str(), outputPathTokens[0].c_str(), pSceneName);

	xmlDocPtr mDocument = xmlNewDoc(BAD_CAST "1.0");
	xmlNodePtr root_node = xmlNewNode(NULL, BAD_CAST "Assets");
	xmlDocSetRootElement(mDocument, root_node);

	xmlNodePtr elemZip = xmlNewChild(root_node, NULL, BAD_CAST "ZipArchive", NULL);
	xmlNewProp(elemZip, BAD_CAST "path", BAD_CAST czZip);

	xmlNodePtr elemDir = xmlNewChild(elemZip, NULL, BAD_CAST "Directory", NULL);
	xmlNewProp(elemDir, BAD_CAST "path", BAD_CAST pSrcDir);

	atString strPattern(atString(pSrcDir), "\\*");

	atArray<atString> subDirs;
	GetSubDirs(strPattern.c_str(), subDirs);

	for(int i=0; i < subDirs.GetCount(); ++i)
	{
		if(stricmp(subDirs[i].c_str(), "..") == 0 ||
			stricmp(subDirs[i].c_str(), "toybox") == 0 ||
			stricmp(subDirs[i].c_str(), "obj") == 0)
			continue;

		char czAbsolute[RAGE_MAX_PATH];
		sprintf(czAbsolute, "%s\\%s", pSrcDir, subDirs[i].c_str());
		atString strRelative(atString("\\"), (char*)subDirs[i].c_str());

		xmlNodePtr elemFaceDir = xmlNewChild(elemZip, NULL, BAD_CAST "Directory", NULL);
		xmlNewProp(elemFaceDir, BAD_CAST "path", BAD_CAST czAbsolute);
		xmlNewProp(elemFaceDir, BAD_CAST "destination", BAD_CAST strRelative.c_str());

		char czSubPattern[RAGE_MAX_PATH];
		sprintf(czSubPattern, "%s\\%s\\*", pSrcDir, subDirs[i].c_str());

		atArray<atString> subsubDirs;
		GetSubDirs(czSubPattern, subsubDirs);

		for(int j=0; j < subsubDirs.GetCount(); ++j)
		{
			if(stricmp(subsubDirs[j].c_str(), "..") == 0 ||
				stricmp(subsubDirs[j].c_str(), "toybox") == 0)
				continue;

			char czAbsolute[RAGE_MAX_PATH];
			sprintf(czAbsolute, "%s\\%s\\%s", pSrcDir, subDirs[i].c_str(), subsubDirs[j].c_str());
			char czRelative[RAGE_MAX_PATH];
			sprintf(czRelative, "\\%s\\%s", (char*)subDirs[i].c_str(), (char*)subsubDirs[j].c_str());

			xmlNodePtr elemFaceDir = xmlNewChild(elemZip, NULL, BAD_CAST "Directory", NULL);
			xmlNewProp(elemFaceDir, BAD_CAST "path", BAD_CAST czAbsolute);
			xmlNewProp(elemFaceDir, BAD_CAST "destination", BAD_CAST czRelative);
		}
	}

	char czXmlFile[RAGE_MAX_PATH];
	sprintf(czXmlFile, "%s\\export\\anim\\cutscene\\master_icd_list.xml", _assetDir.c_str());

	ASSET.CreateLeadingPath(czXmlFile);

	xmlSaveFormatFileEnc(czXmlFile, mDocument, "UTF-8", 1);

	if(ProcessContent(atString(czXmlFile), true, false))
	{
		if(bBuildZip)
		{
			if(!ProcessContent(atString(czZip), false, bPreview))
			{
				return false;
			}
		}

		return true;
	}

	return false;
}

Value* BuildZip_cf(Value** arg_list, int count)
{
	check_arg_count(BuildZip, 3, count);
	type_check(arg_list[0],String,"string [cutscene source data path]");
	type_check(arg_list[1],Boolean,"bool [build zip]");
	type_check(arg_list[1],Boolean,"bool [preview zip]");

	atArray<atString> atClipFiles;
	GetClipFiles(arg_list[0]->to_string(), atClipFiles);

	bool bBuildZip = arg_list[1]->to_bool();
	bool bPreview = arg_list[2]->to_bool();

	crAnimation::InitClass();
	crClip::InitClass();

	bool bFoundProperty = false;
	atString strSourceFile;

	for(int i=0; i < atClipFiles.GetCount(); ++i)
	{
		crClip* clip = crClip::AllocateAndLoad(atClipFiles[i].c_str());
		if(clip)
		{
			crProperty* pProperty = clip->GetProperties()->FindProperty("AbsoluteFBXFile_DO_NOT_RESOURCE");
			if(pProperty)
			{
				crPropertyAttributeString* pAttribute = dynamic_cast<crPropertyAttributeString*>(pProperty->GetAttribute("AbsoluteFBXFile_DO_NOT_RESOURCE"));
				if(pAttribute)
				{
					strSourceFile = pAttribute->GetString();

					if(!IsDlc(strSourceFile))
					{
						pAttribute = dynamic_cast<crPropertyAttributeString*>(pProperty->GetAttribute("FBXFile_DO_NOT_RESOURCE"));
						if(pAttribute)
						{
							strSourceFile = pAttribute->GetString();
						}
					}

					bFoundProperty = true;
					break;
				}
			}
			else
			{
				pProperty = clip->GetProperties()->FindProperty("FBXFile_DO_NOT_RESOURCE");
				if(pProperty)
				{
					crPropertyAttributeString* pAttribute = dynamic_cast<crPropertyAttributeString*>(pProperty->GetAttribute("FBXFile_DO_NOT_RESOURCE"));
					if(pAttribute)
					{
						strSourceFile = pAttribute->GetString();

						bFoundProperty = true;
						break;
					}
				}
			}
		}
	}

	if(!bFoundProperty)
	{
		char cMsg[RAGE_MAX_PATH];
		sprintf( cMsg, "Unable to determine mission for cutscene from folder '%s'.", arg_list[0]->to_string() );

		PrintToListener("Error", cMsg);
		return &false_value;
	}

	if(!BuildDictionary(arg_list[0]->to_string(), strSourceFile.c_str(), bBuildZip, bPreview))
	{
		return &false_value;
	}

	return &true_value;
}

void GetConcatListFiles(const char* pDir, atArray<atString>& atFiles)
{
	char cPath[RAGE_MAX_PATH];
	sprintf(cPath, "%s\\*", pDir);

	WIN32_FIND_DATA fileData;
	HANDLE file = FindFirstFile( cPath, &fileData);

	while ( FindNextFile( file, &fileData ) != 0 )
	{
		atString strFile(fileData.cFileName);
		if(strFile.EndsWith(".concatlist"))
		{
			char cAbsolutePath[RAGE_MAX_PATH];
			sprintf(cAbsolutePath, "%s\\%s", pDir, fileData.cFileName);
			atFiles.PushAndGrow(atString(cAbsolutePath));
		}
	}
}

bool LoadConcatListFile(const char* pName, const char* pFilename, atArray<atString>& atLightFiles)
{
	parTree* pTree = PARSER.LoadTree( pFilename, "" );
	if ( !pTree )
	{
		return false;
	}

	parTreeNode* pRootNode = pTree->GetRoot();

	parTreeNode* pPartsNode = pRootNode->FindChildWithName("parts");
	if(!pPartsNode)
	{
		delete pTree;
	}

	bool bFound = false;

	for(parTreeNode::ChildNodeIterator cIter = pPartsNode->BeginChildren();
		cIter != pPartsNode->EndChildren(); 
		++cIter) 
	{
		parTreeNode* pChildNode = (*cIter);
		
		parTreeNode *pFileNameNode = pChildNode->FindChildWithName("filename");

		char* pFile = pFileNameNode->GetData();

		char cbase[RAGE_MAX_PATH];
		ASSET.RemoveNameFromPath(cbase, RAGE_MAX_PATH, pFile);

		const char* p = ASSET.FileName(cbase);

		if(stricmp(p, pName) == 0)
		{
			bFound = true;
		}
		else
		{
			char cPath[RAGE_MAX_PATH];
			ASSET.RemoveNameFromPath(cPath, RAGE_MAX_PATH, pFile);

			char cLightFile[RAGE_MAX_PATH];
			sprintf(cLightFile, "%s\\data.lightxml", cPath);

			atLightFiles.PushAndGrow(atString(cLightFile));
		}
	}

	return bFound;
}

Value* ValidateUniqueLightNames_cf(Value** arg_list, int count)
{
	check_arg_count(ValidateUniqueLightNames, 1, count);

	type_check(arg_list[0],String,"string [scene dir]");

	const char* pSceneName = ASSET.FileName(arg_list[0]->to_string());

	atString _assetDir = GetProjectOrDlcAssetsDir(atString(arg_list[0]->to_string()));

	char cCutlistDir[RAGE_MAX_PATH];
	sprintf(cCutlistDir, "%s\\cuts\\!!Cutlists", _assetDir.c_str());

	atArray<atString> atConcatListFiles;
	GetConcatListFiles(cCutlistDir, atConcatListFiles);

	for(int iConcatFile=0; iConcatFile < atConcatListFiles.GetCount(); ++iConcatFile)
	{
		atArray<atString> atLightFiles;
		if(LoadConcatListFile(pSceneName, atConcatListFiles[iConcatFile].c_str(), atLightFiles))
		{
			const atArray<cutfObject *>& lightObjects = cutfileLoader::GetInst().GetLightCutFile()->GetObjectList();

			for(int iLightFile=0; iLightFile < atLightFiles.GetCount(); ++iLightFile)
			{
				atLightFiles[iLightFile].Replace("$(assets)",_assetDir.c_str());
				cutfCutsceneFile2 otherLightFile;
				if(otherLightFile.LoadFile(atLightFiles[iLightFile].c_str()))
				{
					const atArray<cutfObject *>& otherlightObjects = otherLightFile.GetObjectList();

					for(int iLightObject=0; iLightObject < lightObjects.GetCount(); ++iLightObject)
					{
						cutfNamedObject* pNamedLight = static_cast<cutfNamedObject*>(lightObjects[iLightObject]);

						for(int iOtherLightObject=0; iOtherLightObject < otherlightObjects.GetCount(); ++iOtherLightObject)
						{
							cutfNamedObject* pOtherNamedLight = static_cast<cutfNamedObject*>(otherlightObjects[iOtherLightObject]);

							if(stricmp(pNamedLight->GetName().GetCStr(), pOtherNamedLight->GetName().GetCStr()) == 0)
							{
								char cMsg[RAGE_MAX_PATH];
								sprintf(cMsg, "'%s' is already present within light file '%s' specified by concat list '%s'", pNamedLight->GetName().GetCStr(), atLightFiles[iLightFile].c_str(), atConcatListFiles[iConcatFile].c_str());

								if(cutfileLoader::GetInst().GetInteractionMode())
								{
									MessageBox(NULL, cMsg, "Error", MB_OK | MB_ICONERROR);
								}

								PrintToListener("Error", cMsg);
								
								return &false_value;
							}
						}
					}
				}
			}	
		}
	}

	return &true_value;
}

Value* ValidateUniqueLightName_cf(Value** arg_list, int count)
{
	check_arg_count(ValidateUniqueLightNames, 2, count);

	type_check(arg_list[0],String,"string [scene dir]");
	type_check(arg_list[1],String,"string [light name]");

	const MCHAR* pSceneName = ASSET.FileName(arg_list[0]->to_string());
	const MCHAR* pLightName = arg_list[1]->to_string();
	
	atString _assetDir = GetProjectOrDlcAssetsDir(atString(arg_list[0]->to_string()));

	char cCutlistDir[RAGE_MAX_PATH];
	sprintf(cCutlistDir, "%s\\cuts\\!!Cutlists", _assetDir.c_str());

	atArray<atString> atConcatListFiles;
	GetConcatListFiles(cCutlistDir, atConcatListFiles);

	for(int iConcatFile=0; iConcatFile < atConcatListFiles.GetCount(); ++iConcatFile)
	{
		atArray<atString> atLightFiles;
		if(LoadConcatListFile(pSceneName, atConcatListFiles[iConcatFile].c_str(), atLightFiles))
		{
			for(int iLightFile=0; iLightFile < atLightFiles.GetCount(); ++iLightFile)
			{
				atLightFiles[iLightFile].Replace("$(assets)",_assetDir.c_str());
				cutfCutsceneFile2 otherLightFile;
				if(otherLightFile.LoadFile(atLightFiles[iLightFile].c_str()))
				{
					const atArray<cutfObject *>& otherlightObjects = otherLightFile.GetObjectList();

					for(int iOtherLightObject=0; iOtherLightObject < otherlightObjects.GetCount(); ++iOtherLightObject)
					{
						cutfNamedObject* pOtherNamedLight = static_cast<cutfNamedObject*>(otherlightObjects[iOtherLightObject]);

						if(stricmp(pLightName, pOtherNamedLight->GetName().GetCStr()) == 0)
						{
							char cMsg[512];
							sprintf(cMsg, "'%s' is already present within light file '%s' specified by concat list '%s'", pLightName, atLightFiles[iLightFile].c_str(), atConcatListFiles[iConcatFile].c_str());
							return_protected(new String(cMsg));
						}
					}
				}
			}	
		}
	}

	return &undefined;
}

Value* GetConcatList_cf(Value** arg_list, int count)
{
	check_arg_count(GetConcatList, 1, count);

	type_check(arg_list[0],String,"string [scene dir]");

	const MCHAR* pSceneName = ASSET.FileName(arg_list[0]->to_string());

	atString _assetDir = GetProjectOrDlcAssetsDir(atString(arg_list[0]->to_string()));

	char cCutlistDir[RAGE_MAX_PATH];
	sprintf(cCutlistDir, "%s\\cuts\\!!Cutlists", _assetDir.c_str());

	atArray<atString> atConcatListFiles;
	GetConcatListFiles(cCutlistDir, atConcatListFiles);
	
	atArray<atString> atConcatFiles;

	for(int iConcatFile=0; iConcatFile < atConcatListFiles.GetCount(); ++iConcatFile)
	{
		atArray<atString> atLightFiles;
		if(LoadConcatListFile(pSceneName, atConcatListFiles[iConcatFile].c_str(), atLightFiles) && atLightFiles.GetCount())
		{
			atString atFiles = atString::Join(atLightFiles,";");
			atConcatFiles.PushAndGrow(atFiles);
		}
	}
	
	if (atConcatFiles.GetCount()!=0)
	{
		return_protected(new String(atString::Join(atConcatFiles,";").c_str()));
	}
	else
	{
		return &undefined;
	}
}