//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by PatrolPath.rc
//
#define IDS_VERSION                     1
#define IDS_CLASS_NAME                  2
#define IDS_RADIUS                      3
#define IDS_PARAMS                      4
#define IDS_OBJLIST                     5
#define IDS_ISVALID                     6
#define IDS_CONNECTIONLIST              7
#define IDS_DISABLED                    8
#define IDS_DISABLED_NODE               8
#define IDS_ROADBLOCK_NODE              9
#define IDS_WATER_NODE                  10
#define IDS_UNDER_BRIDGE_NODE           11
#define IDS_CAR_SPEED_NODE              12
#define IDS_SPECIAL_NODE                13
#define IDS_SPECIAL_NONE                14
#define IDS_SPECIAL_PARALLEL            15
#define IDS_SPECIAL_PERPENDICT          16
#define IDS_SPECIAL_VALET               17
#define IDS_SPECIAL_NIGHTCLUB           18
#define IDS_SPECIAL_DELIVER             19
#define IDS_SPECIAL_VALET_UN            20
#define IDS_SPECIAL_CLUB_UN             21
#define IDS_SPECIAL_DRIVE_THRU          22
#define IDS_SPECIAL_DRIVE_WIND          23
#define IDS_SPECIAL_DELIV_UN            24
#define IDS_LINK_LANES_IN               25
#define IDS_LINK_LANES_OUT              26
#define IDS_AUTOCREATE                  27
#define IDS_AUTOCREATE_MIN_LENGTH       28
#define IDS_LINK_WIDTH                  29
#define IDD_AINODE_PANEL                101
#define IDD_VEH_NODE_PANEL              101
#define IDD_PATROL_NODE_PANEL           101
#define IDC_CONNECT_CURSOR              102
#define IDC_CONNECT_CURSOR1             104
#define IDD_PATROL_NODE_PARAMS_PANEL       104
#define IDD_VEH_PATH_DEFAULT_PARAMS_PANEL 104
#define IDD_PATROL_PATH_DEFAULT_PARAMS_PANEL 104
#define IDD_PATROL_LINK_PARAMS_PANEL       106
#define IDD_PATROL_LINK_PARAMS_PANEL    106
#define IDD_PATROL_LINK_PANEL              107
#define IDD_PATROL_LINK_PANEL           107
#define IDD_VEH_PATH_AUTO_CREATE_PANEL  108
#define IDD_PATROL_PATH_AUTO_CREATE_PANEL 108
#define IDD_AUTO_CREATE_PARAMS_PANEL    109
#define IDB_DMAMAN                      111
#define IDC_CLOSEBUTTON                 1000
#define IDC_DOSTUFF                     1000
#define IDC_DELETE_BUTTON               1003
#define IDC_INSERT_BUTTON               1004
#define IDC_CONNECT_BUTTON              1005
#define IDC_NODE_LIST                   1005
#define IDC_MERGE_BUTTON                1006
#define IDC_ADDNODE_BUTTON              1007
#define IDC_RADIUS_SPIN                 1008
#define IDC_SELECTCONNECT_BUTTON        1009
#define IDC_MULTIATTR_BUTTON            1010
#define IDC_SINGLEATTR_BUTTON           1011
#define IDC_DISABLED_CHECK              1012
#define IDC_DELETENODE_BUTTON           1013
#define IDC_DELETENODE_BUTTON2          1014
#define IDC_ROADBLOCK_CHECK             1014
#define IDC_WATER_CHECK                 1015
#define IDC_BRIDGE_CHECK                1016
#define IDC_SLOW_SPEED_RADIO            1017
#define IDC_MEDIUM_SPEED_RADIO          1018
#define IDC_EXPORT_TEXT_1               1019
#define IDC_FAST_SPEED_RADIO            1020
#define IDC_VERSION                     1021
#define IDC_CBO_SPECIAL                 1022
#define IDC_CBO_SPECIAL2                1023
#define IDC_NODE_SPECIAL_LIST           1024
#define IDC_LANESIN_SPIN                1025
#define IDC_LANESIN_EDIT                1026
#define IDC_LANESOUT_SPIN               1027
#define IDC_FLIP_LINK_BUTTON            1028
#define IDC_LANESOUT_EDIT               1029
#define IDC_MERGE_LINK_BUTTON           1030
#define IDC_UNUSED1_BUTTON              1031
#define IDC_LINK_INSERT_NODE_BUTTON     1031
#define IDC_EDIT_LINK_LANES_IN          1034
#define IDC_EDIT_AUTOCREATE             1035
#define IDC_EDIT_LINK_LANESOUT          1036
#define IDC_EDIT_LINK_LANES_OUT         1036
#define IDC_EDIT_LINK_WIDTH             1038
#define IDC_SPIN_LINK_LANES_IN          1041
#define IDC_SPIN_LINK_LANES_OUT         1042
#define IDC_SPIN_LINK_WIDTH             1043
#define IDC_SPIN_AUTOCREATE             1044
#define IDC_CHECK_AUTOCREATE            1045
#define IDC_CREATE_DIST_SPIN            1049
#define IDC_CREATE_DIST_EDIT            1050
#define IDC_CHECK1                      1051
#define IDC_CHECK_VIEW                  1051
#define IDC_DMAMAN_STATIC               1053
#define IDC_COLOR                       1456
#define IDC_EDIT                        1490
#define IDC_SPIN                        1496
#define IDC_RADIUS_EDIT                 3009

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        107
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1052
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
