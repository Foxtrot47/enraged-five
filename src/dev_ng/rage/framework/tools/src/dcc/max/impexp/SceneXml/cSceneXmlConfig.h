//
// filename:	cSceneXmlConfig.h
// description:	
//

#ifndef INC_CSCENEXMLCONFIG_H_
#define INC_CSCENEXMLCONFIG_H_

// --- Include Files ------------------------------------------------------------

// STL headers
#include <string>
#include <vector>
#include <map>

// Max headers
#include "maxtypes.h"

// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------

// --- Forward Declarations -----------------------------------------------------

class TiXmlElement;

// --- Structure/Class Definitions ----------------------------------------------

typedef struct _tNodeProperty
{
	std::string sName;
	std::string sExportAs;
	std::string sType;
	std::vector<_tNodeProperty> vKeyedProperties;
} tNodeProperty;

typedef struct _tNodeDescription
{
	Interface_ID idInterface;
	std::string sClassName; // or SuperClass
	std::string sExportAs;
	std::vector<tNodeProperty> vProperties;

	_tNodeDescription()
		: idInterface( 0L, 0L )
	{
	}
} tNodeDescription;

/**
 * @brief Selection set export settings.
 */
typedef struct _tSelectionSet
{
	std::string sName;
	bool		bExport;
	int			nSetId;

	_tSelectionSet( const std::string& name, bool exports )
		: sName( name ), bExport( exports ), nSetId( -1 )
	{		
	}
} tSelectionSet;

/**
 * @brief Material export configuration parameters.
 */
typedef struct _tMaterialConfig
{
	bool bEnabled;	//!< Material export enabled/disabled?

	_tMaterialConfig( );
	_tMaterialConfig( TiXmlElement* pMatElem );
} tMaterialConfig;

class cSceneXmlConfig
{
public:
	cSceneXmlConfig( );
	~cSceneXmlConfig( );

	void Load( const char* filename );
	bool HasLoaded( ) const { return m_bHasLoaded; }
	bool LoadFailed( ) const { return m_bLoadFailed; }

	std::vector<tSelectionSet>& RetSelectionSets( ) { return m_vSelectionSets; }
	const tMaterialConfig& RetMaterialConfig( ) const { return m_MaterialConfig; }

	const std::string& RetNodeExportAs( ) const { return m_sNodeExportAs; }
	size_t RetNumClassNodeDesc( ) const { return ( m_mClassNodes.size() ); }
	size_t RetNumSuperClassNodeDesc( ) const { return ( m_mSuperclassNodes.size() ); }
	
	bool ContainsSuperclassDef( const SClass_ID& sid ) const { return ( m_mSuperclassIDs.end() != m_mSuperclassIDs.find( sid ) ); }
	bool ContainsNodeDescForClass( const std::string& classname ) const { return ( m_mClassNodes.end() != m_mClassNodes.find( classname ) ); }
	bool ContainsNodeDescForSuperClass( const std::string& superclass ) const { return ( m_mSuperclassNodes.end() != m_mSuperclassNodes.find( superclass ) ); }

	const std::string* const RetSuperclassName( const SClass_ID& sid ) const;	
	const tNodeDescription* const RetNodeDescForClass( const std::string& classname ) const;
	const tNodeDescription* const RetNodeDescForSuperClass( const std::string& superclass ) const;

private:
	void ParseConfigFile( const char* filename );

	bool									m_bHasLoaded;
	bool									m_bLoadFailed;
	tMaterialConfig							m_MaterialConfig;
	std::string								m_sFilename;
	std::string								m_sNodeExportAs;
	std::vector<tSelectionSet>				m_vSelectionSets;
	std::map<SClass_ID, std::string>		m_mSuperclassIDs;
	std::map<std::string, tNodeDescription>	m_mClassNodes;
	std::map<std::string, tNodeDescription> m_mSuperclassNodes;
};

// --- Globals ------------------------------------------------------------------

extern const char* CONFIG_FILE_DEFAULT;
extern cSceneXmlConfig g_SceneXmlConfig;

#endif // !INC_CSCENEXMLCONFIG_H_
