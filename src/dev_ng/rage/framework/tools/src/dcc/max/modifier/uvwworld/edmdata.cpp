#include "mods.h"
#include "MeshDLib.h"
#include "uvwworld.h"
#include "edmdata.h"
#include "edmrest.h"

#include <list>
using namespace std;

FILE* p_file = NULL;

////////////////////////////////////////////////////////////////////////////////////////////
Matrix3 getMatrixFromFace(Point3& r_A,Point3& r_B,Point3& r_C)
{
	Matrix3 matRet;

	if(r_A[0] == 0.0f)
	{
		r_A[0] = 0.00001f;
	}

	if(r_A[1] == 0.0f)
	{
		r_A[1] = 0.00001f;
	}

	if(r_A[2] == 0.0f)
	{
		r_A[2] = 0.00001f;
	}

	if(r_B[0] == 0.0f)
	{
		r_B[0] = 0.00001f;
	}

	if(r_B[1] == 0.0f)
	{
		r_B[1] = 0.00001f;
	}

	if(r_B[2] == 0.0f)
	{
		r_B[2] = 0.00001f;
	}

	if(r_C[0] == 0.0f)
	{
		r_C[0] = 0.00001f;
	}

	if(r_C[1] == 0.0f)
	{
		r_C[1] = 0.00001f;
	}

	if(r_C[2] == 0.0f)
	{
		r_C[2] = 0.00001f;
	}

	matRet.Zero();
	matRet.SetRow(0,r_A);
	matRet.SetRow(1,r_B);
	matRet.SetRow(2,r_C);

	return matRet;
}

////////////////////////////////////////////////////////////////////////////////////////////
//TempMoveRestore
////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////
TempMoveRestore::TempMoveRestore(Mesh *msh) 
{
	//then backup the current vertices and tvertices

	pos.SetCount(msh->numVerts);
	tex.SetCount(msh->numTVerts);

	if (msh->numVerts) 
	{
		memcpy (pos.Addr(0), msh->verts, msh->numVerts * sizeof(Point3));
	}

	if (msh->numTVerts) 
	{
		memcpy (tex.Addr(0), msh->tVerts, msh->numTVerts * sizeof(Point3));
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
void TempMoveRestore::Restore(Mesh *msh) 
{
	if (!pos.Count())
	{
		return;
	}

	if (!tex.Count()) 
	{
		return;
	}

	int min = (msh->numVerts < pos.Count()) ? msh->numVerts : pos.Count();
	memcpy(msh->verts, pos.Addr(0), min * sizeof(Point3));

	min = (msh->numTVerts < tex.Count()) ? msh->numTVerts : tex.Count();
	memcpy(msh->tVerts, tex.Addr(0), min * sizeof(Point3));
}

////////////////////////////////////////////////////////////////////////////////////////////
DWORD TempMoveRestore::ChannelsChanged() 
{
	DWORD ret = PART_GEOM|PART_TEXMAP;

	return ret;
}

////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////
EditMeshData::EditMeshData() 
{
	mp_tVerts = NULL;
	mp_ctVerts = NULL;
	m_iNoTVerts = 0;
	flags = 0;
	mod = NULL;
	mesh = NULL;
	tmr = NULL;
	tempData = NULL;
	mValid.SetEmpty ();
	topoValid.SetEmpty ();
	geomValid.SetEmpty ();
	lockInvalidate = FALSE;
}

////////////////////////////////////////////////////////////////////////////////////////////
EditMeshData::EditMeshData(EditMeshData& emc) 
{
	mdelta = emc.mdelta;

	for(int i=0;i<3;i++) 
	{
		selSet[i] = emc.selSet[i];
	}

	flags    = emc.flags & EMD_HASDATA;
	mod = NULL;
	mesh = NULL;
	tmr = NULL;
	tempData = NULL;
	mValid.SetEmpty ();
	topoValid.SetEmpty ();
	geomValid.SetEmpty ();
	lockInvalidate = FALSE;
}

////////////////////////////////////////////////////////////////////////////////////////////
EditMeshData::~EditMeshData() 
{
	if(tempData) 
	{
		delete tempData;
	}

	if(mesh)
	{
		delete mesh;
	}

	if(tmr) 
	{
		delete tmr;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshData::ChangeNamedSetSize (int nsl, int oldsize, int change) 
{
	if(change == 0)
	{
		return;
	}

	if(selSet[nsl].Count() == 0) 
	{
		return;
	}

	if (theHold.Holding())
	{
		theHold.Put (new NamedSetSizeChange (this, nsl, oldsize, change));
	}

	selSet[nsl].SetSize (oldsize + change);
}

////////////////////////////////////////////////////////////////////////////////////////////
bool MakeTVertsUnique(Mesh* msh)
{
	list<int> liNewValIndex;
	char* p_bVertUsed;
	p_bVertUsed = new char[msh->numTVerts];
	memset(p_bVertUsed,0,msh->numTVerts);
	int iNewSize = msh->numTVerts;
	int i,j;

	for(i=0;i<msh->numFaces;i++)
	{
		for(j=0;j<3;j++)
		{
			if(p_bVertUsed[msh->tvFace[i].t[j]] == 0)
			{
				p_bVertUsed[msh->tvFace[i].t[j]] = 1;
			}
			else
			{
				liNewValIndex.push_back(msh->tvFace[i].t[j]);
				msh->tvFace[i].t[j] = iNewSize;
				iNewSize++;
			}
		}
	}

	if(iNewSize > msh->numTVerts)
	{
		UVVert* p_uvvNew = new UVVert[iNewSize];

		memcpy(p_uvvNew,msh->tVerts,sizeof(UVVert) * msh->numTVerts);

		list<int>::iterator begin = liNewValIndex.begin();

		for(i=msh->numTVerts;i<iNewSize;i++)
		{
			p_uvvNew[i] = msh->tVerts[*begin];

			begin++;
		}

		msh->setNumTVerts(iNewSize);

		for(i=0;i<iNewSize;i++)
		{
			msh->tVerts[i] = p_uvvNew[i];
		}

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////////////////
void printTVerts(Mesh* msh)
{
	if(!p_file)
	{
		p_file = fopen("c:\\output.log","wtc");
	}

	fprintf(p_file,"****************************************\n");
	fprintf(p_file,"writing t verts\n");
	fprintf(p_file,"****************************************\n");

	for(int i=0;i<msh->numTVerts;i++)
	{
		fprintf(p_file,"%f %f %f\n",msh->tVerts[i][0],msh->tVerts[i][1],msh->tVerts[i][2]);
	}

	fflush(p_file);
}

////////////////////////////////////////////////////////////////////////////////////////////
void printFile(char* p_cPrint)
{
	if(!p_file)
	{
		p_file = fopen("c:\\output.log","wtc");
	}

	fprintf(p_file,p_cPrint);
	fflush(p_file);
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshData::Apply(TimeValue t,TriObject *triOb,EditMeshMod *mod) 
{
	Interval inGeomValid = triOb->ChannelValidity (t, GEOM_CHAN_NUM);
	Interval inTopoValid = triOb->ChannelValidity (t, TOPO_CHAN_NUM);
	Interval inVCValid = triOb->ChannelValidity (t, VERT_COLOR_CHAN_NUM);
	Interval inTexValid = triOb->ChannelValidity (t, TEXMAP_CHAN_NUM);

	//make sure that the mesh passed in has unique texture verts here
	MakeTVertsUnique(&triOb->mesh);
	triOb->PointsWereChanged();

	if(!mp_tVerts)
	{
		m_iNoTVerts = triOb->mesh.numTVerts;
		mp_tVerts = new Point3[m_iNoTVerts];

		memcpy(mp_tVerts,triOb->mesh.tVerts,sizeof(Point3) * m_iNoTVerts);
	}

	if(!GetFlag(EMD_UPDATING_CACHE) && MeshCached(t)) 
	{
		triOb->GetMesh().DeepCopy(	GetMesh(t),
									PART_GEOM|SELECT_CHANNEL|PART_SUBSEL_TYPE|PART_DISPLAY|PART_TOPO|PART_TEXMAP|PART_VERTCOLOR);
		triOb->PointsWereChanged();
	}
	else if (GetFlag(EMD_HASDATA)) 
	{
		triOb->GetMesh().freeVSelectionWeights();

		mdelta.SetInVNum(triOb->GetMesh().numVerts);
		mdelta.SetInFNum(triOb->GetMesh().numFaces);

		mdelta.Apply(triOb->GetMesh());
		memcpy(triOb->mesh.tVerts,mp_tVerts,sizeof(Point3) * m_iNoTVerts);

		selSet[NS_VERTEX].SetSize(triOb->GetMesh().getNumVerts());
		selSet[NS_FACE].SetSize(triOb->GetMesh().getNumFaces());
		selSet[NS_EDGE].SetSize(triOb->GetMesh().getNumFaces()*3);
		triOb->PointsWereChanged();
	}

	triOb->GetMesh().dispFlags = 0;
	bool dispVerts = FALSE;

	switch (mod->selLevel) 
	{
	case SL_VERTEX:
		dispVerts = TRUE;
		break;
	case SL_POLY:
	case SL_ELEMENT:
		triOb->GetMesh().SetDispFlag(DISP_SELPOLYS);
		break;
	case SL_FACE:
		triOb->GetMesh().SetDispFlag(DISP_SELFACES);
		break;
	case SL_EDGE:
		triOb->GetMesh().SetDispFlag(DISP_SELEDGES);
		break;
	}

	if (dispVerts && (!mod->ip || !mod->ip->GetShowEndResult()))
	{
		triOb->GetMesh().SetDispFlag(DISP_VERTTICKS|DISP_SELVERTS);
	}

	triOb->GetMesh().selLevel = meshLevel[mod->selLevel];

	if (GetFlag(EMD_UPDATING_CACHE)) 
	{
		UpdateCache(t,triOb);
		SetFlag(EMD_UPDATING_CACHE,FALSE);
	}

	// Output only changes over time in response to certain changes in input.
	triOb->UpdateValidity (GEOM_CHAN_NUM, inGeomValid & inTopoValid);
	triOb->UpdateValidity (TOPO_CHAN_NUM, inTopoValid);
	triOb->UpdateValidity (TEXMAP_CHAN_NUM, FOREVER);
	triOb->UpdateValidity (SELECT_CHAN_NUM, inTopoValid);
	triOb->UpdateValidity (SUBSEL_TYPE_CHAN_NUM, FOREVER);
	triOb->UpdateValidity (DISP_ATTRIB_CHAN_NUM, inTopoValid);
	triOb->UpdateValidity (TEXMAP_CHAN_NUM, inTopoValid & inTexValid);
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshData::BeginEdit (TimeValue t) 
{
	if (GetFlag(EMD_HASDATA)) 
	{
		return;
	}

	GetMesh(t);
	mdelta.InitToMesh(*mesh);

	if (mdelta.vdSupport.GetSize() > VDATA_SELECT)
	{
		mdelta.vdSupport.Clear (VDATA_SELECT);	// Clear out incoming soft selections.
	}

	SetFlag(EMD_HASDATA,TRUE);
}

////////////////////////////////////////////////////////////////////////////////////////////
BitArray EditMeshData::GetSel (int nsl) 
{
	switch (nsl) 
	{
	case NS_VERTEX: 
		return mdelta.vsel;
	case NS_EDGE: 
		return mdelta.esel;
	}

	return mdelta.fsel;
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshData::SetVertSel (BitArray &set, IMeshSelect *mod, TimeValue t) 
{
	EditMeshMod *em = (EditMeshMod *) mod;
	BeginEdit(t);

	if(theHold.Holding()) 
	{
		theHold.Put (new MeshSelectRestore (this, em, SL_VERTEX));
	}

	mesh->vertSel = set;
	mesh->vertSel.SetSize (mesh->numVerts);
	mesh->vertSel &= ~mesh->vertHide;
	mdelta.vsel = mesh->vertSel;

	if (mdelta.vsel.GetSize () != mdelta.outVNum()) 
	{
		mdelta.vsel.SetSize (mdelta.outVNum(), TRUE);
	}

	Invalidate(PART_SELECT);
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshData::SetFaceSel(BitArray &set, IMeshSelect *mod, TimeValue t) 
{
	EditMeshMod *em = (EditMeshMod *) mod;
	BeginEdit (t);

	if (theHold.Holding()) 
	{
		theHold.Put (new MeshSelectRestore(this, em, SL_FACE));
	}

	mdelta.fsel = set;

	if (set.GetSize () != mdelta.outFNum()) 
	{
		mdelta.fsel.SetSize (mdelta.outFNum(), TRUE);
	}

	GetMesh(t);
	mdelta.SelectFacesByFlags (*mesh, FALSE, FACE_HIDDEN, FACE_HIDDEN);
	mesh->faceSel = mdelta.fsel;
	mesh->faceSel.SetSize (mesh->numFaces);
	Invalidate(PART_SELECT);
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshData::SetEdgeSel(BitArray &set, IMeshSelect *mod, TimeValue t) 
{
	EditMeshMod *em = (EditMeshMod *) mod;
	BeginEdit (t);

	if (theHold.Holding()) 
	{
		theHold.Put (new MeshSelectRestore (this, em, SL_EDGE));
	}

	mdelta.esel = set;

	if(set.GetSize() != mdelta.outFNum()*3) 
	{
		mdelta.esel.SetSize (mdelta.outFNum()*3, TRUE);
	}

	if(MeshCached(t)) 
	{
		mesh->edgeSel = mdelta.esel;
		mesh->edgeSel.SetSize (mesh->numFaces*3);
		Invalidate(PART_SELECT);
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshData::SetSel(int nsl, BitArray & set, IMeshSelect *mod, TimeValue t) 
{
	switch (nsl)
	{
	case NS_VERTEX: 
		SetVertSel (set, mod, t); 
		break;
	case NS_EDGE: 
		SetEdgeSel (set, mod, t); 
		break;
	case NS_FACE: 
		SetFaceSel (set, mod, t);
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
MeshTempData *EditMeshData::TempData(TimeValue t) 
{
	if(!mod) 
	{
		return NULL;
	}

	if(!tempData) 
	{
		tempData = new MeshTempData();
	}

	tempData->SetMesh(GetMesh(t));

	return tempData;
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshData::Invalidate(PartID part,BOOL meshValid) 
{
	if(lockInvalidate) 
	{
		return;
	}

	if(!meshValid && mesh) 
	{
		delete mesh;
		mesh = NULL;
		mValid.SetEmpty();
	}

	if(part & PART_TOPO) 
	{
		topoValid.SetEmpty();
	}

	if(part & PART_GEOM) 
	{
		geomValid.SetEmpty();
	}

	if(tempData) 
	{
		tempData->Invalidate (part);
	}

	if(mod && (part & PART_SELECT)) 
	{
		mod->InvalidateNumberSelected();
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
BOOL EditMeshData::MeshCached(TimeValue t) 
{
	return (mValid.InInterval(t) && mesh);
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshData::UpdateCache (TimeValue t, TriObject *triOb) 
{
	if(tempData) 
	{
		DWORD invalidators = 0;

		if(!geomValid.InInterval (t)) 
		{
			invalidators |= PART_GEOM;
		}

		if(!topoValid.InInterval (t)) 
		{
			invalidators |= PART_TOPO;
		}

//		if(invalidators) 
		{
			tempData->Invalidate (invalidators);
		}
	}

	if(mesh)
	{
		delete mesh;
	}

	mesh = new Mesh(triOb->GetMesh());

	mValid = FOREVER;

	// These are the channels we care about.
	geomValid = triOb->ChannelValidity (t, GEOM_CHAN_NUM);
	topoValid = triOb->ChannelValidity (t, TOPO_CHAN_NUM);
	mValid &= geomValid;
	mValid &= topoValid;
	mValid &= triOb->ChannelValidity(t,TEXMAP_CHAN_NUM);
	mValid &= triOb->ChannelValidity(t,VERT_COLOR_CHAN_NUM);
}

////////////////////////////////////////////////////////////////////////////////////////////
Mesh *EditMeshData::GetMesh (TimeValue t) 
{
	if(mValid.InInterval(t) && mesh) 
	{
		return mesh;
	}

	if(!mod) 
	{
		return NULL;
	}

	SetFlag(EMD_UPDATING_CACHE,TRUE);
	lockInvalidate = TRUE;
	mod->NotifyDependents(	Interval(t,t),
							PART_GEOM|PART_TEXMAP|SELECT_CHANNEL|PART_SUBSEL_TYPE|PART_DISPLAY|PART_TOPO,
							REFMSG_MOD_EVAL);
	lockInvalidate = FALSE;
	// Following 2 lines basically active when the pipeline fails to provide us with a TriObject.
	// We set ourselves to be the empty mesh.
	if(!mValid.InInterval(t)) 
	{
		mValid.SetInstant (t);
	}

	if(!mesh) 
	{
		mesh = new Mesh;
	}

	SetFlag(EMD_UPDATING_CACHE,FALSE);
	//Invalidate (PART_GEOM, TRUE);	// Necessary to make the undo's in scrolling or dragging ops (move, extrude) work.
	return mesh;
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshData::MoveSelection(int level, TimeValue t, Matrix3& partm, Matrix3& tmAxis, Point3& val, BOOL localOrigin)
{
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshData::RotateSelection(int level, TimeValue t, Matrix3& partm, Matrix3& tmAxis, Quat& val, BOOL localOrigin)
{
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshData::ScaleSelection(int level, TimeValue t, Matrix3& partm, Matrix3& tmAxis, Point3& val, BOOL localOrigin)
{
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshData::ExtrudeSelection(int level, BitArray* sel, float amount, float bevel, BOOL groupNormal, Point3* direction)
{
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshData::DeleteNamedSetArray (int nsl, BitArray & del) 
{
	if(del.NumberSet() == 0) 
	{
		return;
	}

	if(selSet[nsl].Count() == 0) 
	{
		return;
	}

	if(theHold.Holding()) 
	{
		theHold.Put (new NamedSetDelete (this, nsl, del));
	}

	selSet[nsl].DeleteSetElements (del, (nsl==NS_EDGE) ? 3 : 1);
}

////////////////////////////////////////////////////////////////////////////////////////////
void Invert( float b[][4], float a[][4])
{
	long indxc[4], indxr[4], ipiv[4];
	long i, icol, irow, j, ir, ic;
	float big, dum, pivinv, temp, bb;

	ipiv[0] = -1;
	ipiv[1] = -1;
	ipiv[2] = -1;
	ipiv[3] = -1;
	a[0][0] = b[0][0];
	a[1][0] = b[1][0];
	a[2][0] = b[2][0];
	a[3][0] = b[3][0];
	a[0][1] = b[0][1];
	a[1][1] = b[1][1];
	a[2][1] = b[2][1];
	a[3][1] = b[3][1];
	a[0][2] = b[0][2];
	a[1][2] = b[1][2];
	a[2][2] = b[2][2];
	a[3][2] = b[3][2];
	a[0][3] = b[0][3];
	a[1][3] = b[1][3];
	a[2][3] = b[2][3];
	a[3][3] = b[3][3];

	for (i = 0; i < 4; i++) {
	big = 0.0f;
	for (j = 0; j < 4; j++) {
	if (ipiv[j] != 0) {
	if (ipiv[0] == -1) {
	if ((bb = ( float) fabs(a[j][0])) > big) {
	big = bb;
	irow = j;
	icol = 0;
	}
	} else if (ipiv[0] > 0) {
	return;
	}
	if (ipiv[1] == -1) {
	if ((bb = ( float) fabs(( float) a[j][1])) > big) {
	big = bb;
	irow = j;
	icol = 1;
	}
	} else if (ipiv[1] > 0) {
	return;
	}
	if (ipiv[2] == -1) {
	if ((bb = ( float) fabs(( float) a[j][2])) > big) {
	big = bb;
	irow = j;
	icol = 2;
	}
	} else if (ipiv[2] > 0) {
	return;
	}
	if (ipiv[3] == -1) {
	if ((bb = ( float) fabs(( float) a[j][3])) > big) {
	big = bb;
	irow = j;
	icol = 3;
	}
	} else if (ipiv[3] > 0) {
	return;
	}
	}
	}
	++(ipiv[icol]);
	if (irow != icol) {
	temp = a[irow][0];
	a[irow][0] = a[icol][0];
	a[icol][0] = temp;
	temp = a[irow][1];
	a[irow][1] = a[icol][1];
	a[icol][1] = temp;
	temp = a[irow][2];
	a[irow][2] = a[icol][2];
	a[icol][2] = temp;
	temp = a[irow][3];
	a[irow][3] = a[icol][3];
	a[icol][3] = temp;
	}
	indxr[i] = irow;
	indxc[i] = icol;
	if (a[icol][icol] == 0.0) {
	return;
	}
	pivinv = 1.0f / a[icol][icol];
	a[icol][icol] = 1.0f;
	a[icol][0] *= pivinv;
	a[icol][1] *= pivinv;
	a[icol][2] *= pivinv;
	a[icol][3] *= pivinv;
	if (icol != 0) {
	dum = a[0][icol];
	a[0][icol] = 0.0f;
	a[0][0] -= a[icol][0] * dum;
	a[0][1] -= a[icol][1] * dum;
	a[0][2] -= a[icol][2] * dum;
	a[0][3] -= a[icol][3] * dum;
	}
	if (icol != 1) {
	dum = a[1][icol];
	a[1][icol] = 0.0f;
	a[1][0] -= a[icol][0] * dum;
	a[1][1] -= a[icol][1] * dum;
	a[1][2] -= a[icol][2] * dum;
	a[1][3] -= a[icol][3] * dum;
	}
	if (icol != 2) {
	dum = a[2][icol];
	a[2][icol] = 0.0f;
	a[2][0] -= a[icol][0] * dum;
	a[2][1] -= a[icol][1] * dum;
	a[2][2] -= a[icol][2] * dum;
	a[2][3] -= a[icol][3] * dum;
	}
	if (icol != 3) {
	dum = a[3][icol];
	a[3][icol] = 0.0f;
	a[3][0] -= a[icol][0] * dum;
	a[3][1] -= a[icol][1] * dum;
	a[3][2] -= a[icol][2] * dum;
	a[3][3] -= a[icol][3] * dum;
	}
	}
	if (indxr[3] != indxc[3]) {
	ir = indxr[3];
	ic = indxc[3];
	temp = a[0][ir];
	a[0][ir] = a[0][ic];
	a[0][ic] = temp;
	temp = a[1][ir];
	a[1][ir] = a[1][ic];
	a[1][ic] = temp;
	temp = a[2][ir];
	a[2][ir] = a[2][ic];
	a[2][ic] = temp;
	temp = a[3][ir];
	a[3][ir] = a[3][ic];
	a[3][ic] = temp;
	}
	if (indxr[2] != indxc[2]) {
	ir = indxr[2];
	ic = indxc[2];
	temp = a[0][ir];
	a[0][ir] = a[0][ic];
	a[0][ic] = temp;
	temp = a[1][ir];
	a[1][ir] = a[1][ic];
	a[1][ic] = temp;
	temp = a[2][ir];
	a[2][ir] = a[2][ic];
	a[2][ic] = temp;
	temp = a[3][ir];
	a[3][ir] = a[3][ic];
	a[3][ic] = temp;
	}
	if (indxr[1] != indxc[1]) {
	ir = indxr[1];
	ic = indxc[1];
	temp = a[0][ir];
	a[0][ir] = a[0][ic];
	a[0][ic] = temp;
	temp = a[1][ir];
	a[1][ir] = a[1][ic];
	a[1][ic] = temp;
	temp = a[2][ir];
	a[2][ir] = a[2][ic];
	a[2][ic] = temp;
	temp = a[3][ir];
	a[3][ir] = a[3][ic];
	a[3][ic] = temp;
	}
	if (indxr[0] != indxc[0]) {
	ir = indxr[0];
	ic = indxc[0];
	temp = a[0][ir];
	a[0][ir] = a[0][ic];
	a[0][ic] = temp;
	temp = a[1][ir];
	a[1][ir] = a[1][ic];
	a[1][ic] = temp;
	temp = a[2][ir];
	a[2][ir] = a[2][ic];
	a[2][ic] = temp;
	temp = a[3][ir];
	a[3][ir] = a[3][ic];
	a[3][ic] = temp;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
void Invert2( float *mat, float *dst)
{
	float tmp[12]; /* temp array for pairs */
	float src[16]; /* array of transpose source matrix */
	float det; /* determinant */

	/* transpose matrix */
	for ( int i = 0; i < 4; i++) 
	{
		src[i] = mat[i*4];
		src[i + 4] = mat[i*4 + 1];
		src[i + 8] = mat[i*4 + 2];
		src[i + 12] = mat[i*4 + 3];
	}

	/* calculate pairs for first 8 elements (cofactors) */
	tmp[0] = src[10] * src[15];
	tmp[1] = src[11] * src[14];
	tmp[2] = src[9] * src[15];
	tmp[3] = src[11] * src[13];
	tmp[4] = src[9] * src[14];
	tmp[5] = src[10] * src[13];
	tmp[6] = src[8] * src[15];
	tmp[7] = src[11] * src[12];
	tmp[8] = src[8] * src[14];
	tmp[9] = src[10] * src[12];
	tmp[10] = src[8] * src[13];
	tmp[11] = src[9] * src[12];

	/* calculate first 8 elements (cofactors) */
	dst[0] = tmp[0]*src[5] + tmp[3]*src[6] + tmp[4]*src[7];
	dst[0] -= tmp[1]*src[5] + tmp[2]*src[6] + tmp[5]*src[7];
	dst[1] = tmp[1]*src[4] + tmp[6]*src[6] + tmp[9]*src[7];
	dst[1] -= tmp[0]*src[4] + tmp[7]*src[6] + tmp[8]*src[7];
	dst[2] = tmp[2]*src[4] + tmp[7]*src[5] + tmp[10]*src[7];
	dst[2] -= tmp[3]*src[4] + tmp[6]*src[5] + tmp[11]*src[7];
	dst[3] = tmp[5]*src[4] + tmp[8]*src[5] + tmp[11]*src[6];
	dst[3] -= tmp[4]*src[4] + tmp[9]*src[5] + tmp[10]*src[6];
	dst[4] = tmp[1]*src[1] + tmp[2]*src[2] + tmp[5]*src[3];
	dst[4] -= tmp[0]*src[1] + tmp[3]*src[2] + tmp[4]*src[3];
	dst[5] = tmp[0]*src[0] + tmp[7]*src[2] + tmp[8]*src[3];
	dst[5] -= tmp[1]*src[0] + tmp[6]*src[2] + tmp[9]*src[3];
	dst[6] = tmp[3]*src[0] + tmp[6]*src[1] + tmp[11]*src[3];
	dst[6] -= tmp[2]*src[0] + tmp[7]*src[1] + tmp[10]*src[3];
	dst[7] = tmp[4]*src[0] + tmp[9]*src[1] + tmp[10]*src[2];
	dst[7] -= tmp[5]*src[0] + tmp[8]*src[1] + tmp[11]*src[2];

	/* calculate pairs for second 8 elements (cofactors) */
	tmp[0] = src[2]*src[7];
	tmp[1] = src[3]*src[6];
	tmp[2] = src[1]*src[7];
	tmp[3] = src[3]*src[5];
	tmp[4] = src[1]*src[6];
	tmp[5] = src[2]*src[5];
	tmp[6] = src[0]*src[7];
	tmp[7] = src[3]*src[4];
	tmp[8] = src[0]*src[6];
	tmp[9] = src[2]*src[4];
	tmp[10] = src[0]*src[5];
	tmp[11] = src[1]*src[4];

	/* calculate second 8 elements (cofactors) */
	dst[8] = tmp[0]*src[13] + tmp[3]*src[14] + tmp[4]*src[15];
	dst[8] -= tmp[1]*src[13] + tmp[2]*src[14] + tmp[5]*src[15];
	dst[9] = tmp[1]*src[12] + tmp[6]*src[14] + tmp[9]*src[15];
	dst[9] -= tmp[0]*src[12] + tmp[7]*src[14] + tmp[8]*src[15];
	dst[10] = tmp[2]*src[12] + tmp[7]*src[13] + tmp[10]*src[15];
	dst[10]-= tmp[3]*src[12] + tmp[6]*src[13] + tmp[11]*src[15];
	dst[11] = tmp[5]*src[12] + tmp[8]*src[13] + tmp[11]*src[14];
	dst[11]-= tmp[4]*src[12] + tmp[9]*src[13] + tmp[10]*src[14];
	dst[12] = tmp[2]*src[10] + tmp[5]*src[11] + tmp[1]*src[9];
	dst[12]-= tmp[4]*src[11] + tmp[0]*src[9] + tmp[3]*src[10];
	dst[13] = tmp[8]*src[11] + tmp[0]*src[8] + tmp[7]*src[10];
	dst[13]-= tmp[6]*src[10] + tmp[9]*src[11] + tmp[1]*src[8];
	dst[14] = tmp[6]*src[9] + tmp[11]*src[11] + tmp[3]*src[8];
	dst[14]-= tmp[10]*src[11] + tmp[2]*src[8] + tmp[7]*src[9];
	dst[15] = tmp[10]*src[10] + tmp[4]*src[8] + tmp[9]*src[9];
	dst[15]-= tmp[8]*src[9] + tmp[11]*src[10] + tmp[5]*src[8];

	/* calculate determinant */
	det=src[0]*dst[0]+src[1]*dst[1]+src[2]*dst[2]+src[3]*dst[3];

	/* calculate matrix inverse */
	det = 1/det;
	for ( int j = 0; j < 16; j++)
	{
		dst[j] *= det;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
void Matrix3ToFloat16(Matrix3* p_matIn,float* p_matOut)
{
	p_matOut[0] = 1.0f;
	p_matOut[1] = 0.0f;
	p_matOut[2] = 0.0f;
	p_matOut[3] = 0.0f;
	p_matOut[4] = 0.0f;
	p_matOut[5] = 1.0f;
	p_matOut[6] = 0.0f;
	p_matOut[7] = 0.0f;
	p_matOut[8] = 0.0f;
	p_matOut[9] = 0.0f;
	p_matOut[10] = 1.0f;
	p_matOut[11] = 0.0f;
	p_matOut[12] = 0.0f;
	p_matOut[13] = 0.0f;
	p_matOut[14] = 0.0f;
	p_matOut[15] = 1.0f;


	for(int i=0;i<4;i++)
	{
		for(int j=0;j<3;j++)
		{
			p_matOut[j * 4 + i] = p_matIn->GetAddr()[i][j];
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
void Float16ToMatrix3(float* p_matIn,Matrix3* p_matOut)
{
	for(int i=0;i<4;i++)
	{
		for(int j=0;j<3;j++)
		{
			p_matOut->GetAddr()[i][j] = p_matIn[j * 4 + i];
		}
	}

	p_matOut->ValidateFlags();
}

////////////////////////////////////////////////////////////////////////////////////////////
void Matrix3ToFloat44(Matrix3* p_matIn,float p_matOut[][4])
{
	p_matOut[0][0] = 1.0f;
	p_matOut[0][1] = 0.0f;
	p_matOut[0][2] = 0.0f;
	p_matOut[0][3] = 0.0f;
	p_matOut[1][0] = 0.0f;
	p_matOut[1][1] = 1.0f;
	p_matOut[1][2] = 0.0f;
	p_matOut[1][3] = 0.0f;
	p_matOut[2][0] = 0.0f;
	p_matOut[2][1] = 0.0f;
	p_matOut[2][2] = 1.0f;
	p_matOut[2][3] = 0.0f;
	p_matOut[3][0] = 0.0f;
	p_matOut[3][1] = 0.0f;
	p_matOut[3][2] = 0.0f;
	p_matOut[3][3] = 1.0f;


	for(int i=0;i<4;i++)
	{
		for(int j=0;j<3;j++)
		{
			p_matOut[j][i] = p_matIn->GetAddr()[i][j];
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
void Float44ToMatrix3(float p_matIn[][4],Matrix3* p_matOut)
{
	for(int i=0;i<4;i++)
	{
		for(int j=0;j<3;j++)
		{
			p_matOut->GetAddr()[i][j] = p_matIn[j][i];
		}
	}

	p_matOut->ValidateFlags();
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshData::applyUVChange(MeshDelta& md,Mesh* mesh)
{
	Matrix3 matPos,matTex,matTrans,matTemp;
	float matTIn[4][4],matTOut[4][4];

	for (int i=0; i<md.vMove.Count(); i++)
	{
		DWORD j = md.vMove[i].vid;

		for(int k=0;k<mesh->numFaces;k++)
		{
			for(int l=0;l<3;l++)
			{
				if(mesh->faces[k].v[l] == j)
				{
					//create the matrix that moves from world to texture space

					matPos = getMatrixFromFace(	mesh->verts[mesh->faces[k].v[0]],
												mesh->verts[mesh->faces[k].v[1]],
												mesh->verts[mesh->faces[k].v[2]]);

					matTex = getMatrixFromFace(	mesh->tVerts[mesh->tvFace[k].t[0]],
												mesh->tVerts[mesh->tvFace[k].t[1]],
												mesh->tVerts[mesh->tvFace[k].t[2]]);

					//invert method needs changing

					Matrix3ToFloat44(&matPos,matTIn);
					Invert(matTIn,matTOut);
					Float44ToMatrix3(matTOut,&matTemp);
					matPos = matTemp;

//					matPos.Invert();

					matTrans = matPos * matTex;

					//and tranform our movement by it to move that movement
					//into texture space
					Point3 tempA = matTrans.PointTransform(md.vMove[i].dv);

					//need to calculate the new texture co-ord from the old one
					int v = mesh->tvFace[k].t[l];

					//these need to take into account the current
					//orientation and scale of the mapped vertices
					mesh->tVerts[v][0] += tempA[0];
					mesh->tVerts[v][1] += tempA[1];
					mesh->tVerts[v][2] += tempA[2];
					
					mp_ctVerts[v] = mesh->tVerts[v];
				}
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshData::createBackupUVW()
{
	delete[] mp_ctVerts;
	mp_ctVerts = new Point3[m_iNoTVerts];
	memcpy(mp_ctVerts,mp_tVerts,sizeof(Point3) * m_iNoTVerts);
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshData::saveUVW()
{
	memcpy(mp_tVerts,mp_ctVerts,sizeof(Point3) * m_iNoTVerts);
}

////////////////////////////////////////////////////////////////////////////////////////////
void EditMeshData::ApplyMeshDelta(MeshDelta& md, MeshDeltaUser *mdu, TimeValue t) 
{
	DWORD partsChanged = md.PartsChanged();

	EditMeshMod *mod = (EditMeshMod *) mdu;
	BeginEdit(t);

	if(theHold.Holding()) 
	{
		theHold.Put(new MeshEditRestore (this, mod, md));
	}

	mdelta.Compose(md);

	Invalidate (partsChanged, TRUE);

	mod->LocalDataChanged (partsChanged);
}