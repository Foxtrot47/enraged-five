
// rage headers
#include "cranimation/animation.h"
#include "crclip/clipanimation.h"
//#include "cranimation/animTrack.h"

#include "atl/array.h"
//#include "crclip/tags.h"
#include "crmetadata/properties.h"
#include "crmetadata/propertyattributes.h"

using namespace rage;

class ClipObject
{
public:
	ClipObject();
	~ClipObject();

	void Init( char* p_Filename );
	
	void LoadClip( char* p_Filename );
	void SaveClip( char* p_Filename );

	void GetProperty( const char* propName, int &propValue );
	void GetProperty( const char* propName, const char* &propValue );
	void GetProperty( const char* propName, void* &propValue);

	void Reset();
	bool IsInited() { return mb_Inited; }

	void BuildPropertyFromArgs( const char* propName, Value** args, int count );
	
	enum 
	{
		PROP_TYPE_INT = 0,
		PROP_TYPE_FLOAT,
		PROP_TYPE_STRING
	};


private:
	
	crAnimation *m_pAnim;
	crClipAnimation *m_pClipAnim;
	crProperties* m_pProperties;
	bool mb_Inited;

	char* m_pFilename;
};