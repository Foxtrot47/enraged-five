#include "WaterCalmingHelper.h"

/////////////////////////////////////////////////////////////////////////////////////////////////
class WaterCalmingClassDesc : public ClassDesc2 
	/////////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	int 			IsPublic() { return TRUE; }
	void *			Create(BOOL loading = FALSE) { return new WaterCalmingHelper(); }
	const TCHAR *	ClassName() { return GetString(IDS_WATER_CALM_NAME); }
	SClass_ID		SuperClassID() { return HELPER_CLASS_ID; }
	Class_ID		ClassID() { return WATER_CALMER_CLASS_ID; }
	const TCHAR* 	Category() { return GetString(IDS_CATEGORY); }

	const TCHAR*	InternalName() { return _T("GtaWaterCalmer"); }
	HINSTANCE		HInstance() { return hInstance; }
};

/////////////////////////////////////////////////////////////////////////////////////////////////
static WaterCalmingClassDesc WaterCalmingDesc;

static ParamBlockDesc2* waterBaseBlockCalmer = CreateWaterBasePBlock2(&WaterCalmingDesc, "Calming Parameters");

ParamBlockDesc2 waterCalmingPBlockDesc (
								water_calm_params, 
								_T("Water calming Parameters"),
								IDS_WATER_NAME, 
								&WaterCalmingDesc, 
								P_AUTO_CONSTRUCT | P_AUTO_UI | P_VERSION, 
								WATER_CALMER_CURRENT_VERSION,
								WATER_EXTEND_PBLOCK_REF,
								IDD_PANEL_CALMER, IDS_EXTENDED_PARAMS, 0, 0, NULL,

									water_calming, _T("Calming"), TYPE_WORLD, P_ANIMATABLE, IDS_CALMING,
									p_default,		1.0f,
									p_range, 		0.0f, 1000.0f, 
									p_ui,			TYPE_SPINNER, EDITTYPE_FLOAT, IDC_CALMING, IDC_SPIN_CALMING, 0.1f,
									end, 
								 end
								 );


/////////////////////////////////////////////////////////////////////////////////////////////////
ClassDesc2* GetWaterCalmingDesc()
{
	return &WaterCalmingDesc;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void WaterCalmingHelper::BeginEditParams( IObjParam *ip, ULONG flags,Animatable *prev)
{
	Water::BeginEditParams(ip,flags,prev);
	WaterCalmingDesc.BeginEditParams(ip,this,flags,prev);
	WaterCalmingDesc.SetUserDlgProc(&waterCalmingPBlockDesc, new WaterDlgProc(this));
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void WaterCalmingHelper::EndEditParams( IObjParam *ip, ULONG flags,Animatable *next)
{
	WaterCalmingDesc.EndEditParams(ip,this,flags,next);
	Water::EndEditParams(ip,flags,next);
}


WaterCalmingHelper::WaterCalmingHelper(void)
	: Water(&WaterCalmingDesc)
{
//	CreateWaterBasePBlock2(&WaterCalmingDesc, IDD_PANEL_CALMER);
//	WaterCalmingDesc.AddParamBlockDesc(&waterCalmingPBlockDesc);
	WaterCalmingDesc.MakeAutoParamBlocks(this);
	mLineColour = Point3(0.0f,1.0f,0.0f);
}

int WaterCalmingHelper::GetType()
{
	return 0;
}

void WaterCalmingHelper::BuildSpecificMesh(TimeValue t, Mesh &mesh, Point3 centre, Point3 dim)
{

}
/////////////////////////////////////////////////////////////////////////////////////////////////
RefTargetHandle WaterCalmingHelper::Clone(RemapDir& remap) 
{
	WaterCalmingHelper* newob = new WaterCalmingHelper();
	newob->ReplaceReference(0,pBasePblock2->Clone(remap));
	newob->ReplaceReference(1,pExtendPblock2->Clone(remap));

	BaseClone(this, newob, remap);

	return(newob);
}