Project clipImporter
Template max_configurations.xml

OutputFileName clipImporter.gup
ConfigurationType dll

WarningsAsErrors false
WarningLevel 3

IncludePath .\midl\Win32;$(RAGE_DIR)/base/src;$(RAGE_DIR)\suite\src;$(RAGE_DIR)\base\tools

ForceInclude forceinclude.h

EmbeddedLibAll odbc32.lib odbccp32.lib comctl32.lib bmm.lib core.lib geom.lib gfx.lib mesh.lib maxutil.lib maxscrpt.lib gup.lib paramblk2.lib

Files {
	Folder "Header Files" { 
		3dsmaxsdk_preinclude.h
		clipImporter.h
		clipObject.h
		forceinclude.h
		resource.h
	}
	Folder "Resource Files" {	
		clipImporter.rc
	}
	Folder "Source Files" {
		clipImporter.cpp
		clipImporter.def
		clipObject.cpp
		DllEntry.cpp
		script.cpp
	}	
}