#pragma once
#include "water.h"

#define WATER_CALMER_CLASS_ID	Class_ID(0x36cf2998, 0x3d56137d)

class WaterCalmingHelper :
	public Water
{
public:
	WaterCalmingHelper(void);

	RefTargetHandle Clone(RemapDir& remap);

	TCHAR *GetObjectName()
	{ 
		return GetString(IDS_WATER_CALM_NAME); 
	}

	int GetType();
	bool	ShowTypeButton()
	{
		return false;
	}
	float	GetAlphaFloat(int index)
	{
		return 1.0f;
	}
	int	GetAlpha(int index)
	{
		return 255;
	}

	// Animatable methods
	// 	int NumParamBlocks() {return 1;}
	// 	IParamBlock2* GetParamBlock(int i);
	// 	IParamBlock2* GetParamBlockByID(short id);
	Class_ID ClassID() 
	{ 
		return WATER_CALMER_CLASS_ID;
	}  
	void GetClassName(TSTR& s)
	{	
		s = GetString(IDS_WATER_CALM_NAME);
	}

	void BeginEditParams( IObjParam *ip, ULONG flags,Animatable *prev);
	void EndEditParams( IObjParam *ip, ULONG flags,Animatable *next);

	// 	int NumRefs() { return 2; }
	// 	RefTargetHandle GetReference(int i);
	// 	void SetReference(int i, RefTargetHandle rtarg);

	// From object
	void InitNodeName(TSTR& s)
	{	
		s = GetString(IDS_WATER_CALM_NAME);
	}

	void BuildSpecificMesh(TimeValue t, Mesh &mesh, Point3 centre, Point3 dim);

	// 
	// 	IParamBlock2* m_pblockWater;
};
