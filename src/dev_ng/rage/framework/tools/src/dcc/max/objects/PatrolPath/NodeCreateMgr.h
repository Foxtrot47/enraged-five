//
//
//    Filename: NodeCreateMgr.h
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Creation manager for Vehicle Nodes. Connects previously created Nodes to new Nodes
//
//
#if !defined(INC_NODE_CREATE_MGR_H_)
#define INC_NODE_CREATE_MGR_H_

#include <Max.h>

#define CID_PATROL_NODE	CID_USER + 0x383
//
//   Class Name: PatrolNodeCreateMgr
// Base Classes: 
//  Description: 
//    Functions: 
//
//
class PatrolNodeCreateMgr : public MouseCallBack, public ReferenceMaker, public PickNodeCallback
{
private:
	INode*			m_pThis;
	INode*			m_pCanConnectTo;
	PatrolNode*	m_pPrev;
	PatrolNode*	m_pVehicleNode;
	IObjCreate*		m_ip;

	bool			m_creating;

//	static	PatrolNode		m_DefaultVehicleNode;
//	static	PatrolLink		m_DefaultVehicleLink;
	static	PatrolNode*	m_pDefaultPatrolNode;
	static	PatrolLink*	m_pDefaultPatrolLink;
	static HWND		m_hWndIMaxNode;
	static HWND		m_hWndIMaxLink;
	ISpinnerControl*	m_pSpinLinkLen;


public:
	PatrolNodeCreateMgr();
	~PatrolNodeCreateMgr();

	void Begin( IObjCreate *ioc, ClassDesc *desc );
	void End();

	// ReferenceMaker
	int NumRefs();
	RefTargetHandle GetReference(int i);
	void SetReference(int i, RefTargetHandle rtarg);
	RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,  RefMessage message);

	// MouseCallback
	int proc(HWND hwnd, int msg, int point, int flags, IPoint2 m);
	BOOL SupportAutoGrid() {return TRUE;}

	// PickNodeCallback
	BOOL Filter(INode *pNode);

	static void CreateVehicleNode(IObjCreate* ip, PatrolNode*& pVehicleNode);
	static void CreateVehicleNodeDefault(IObjCreate* ip, PatrolNode*& pVehicleNode, INode*& pINode, Matrix3* pMat, bool bCopyDefaultParams=true);
	static void CreateVehicleLink(IObjCreate* ip, PatrolLink*& pVehicleLink);
	static void CreateVehicleLinkDefault(IObjCreate* ip, PatrolLink*& pVehicleLink, INode*& pINode, bool bCopyDefaultParams=true);

	static HWND		m_hWndAutoCreate;
	static bool		m_bAutoCreate;
	static float	m_fAutoCreateDistance;
	static bool		m_bViewDifferent;

	static	PatrolNode*	GetDefaultPatrolNode() { return m_pDefaultPatrolNode; }
	static	PatrolLink*	GetDefaultPatrolLink()	{ return m_pDefaultPatrolLink; }

protected:
	void RemoveLink();
	void RemovePatrolNode(INode* pNode);
	void AutoCreate(Interface* ip, PatrolNode* pNode1, PatrolNode* pNode2);
	void StopCreating(ViewExp *pVpt);

	PatrolLink* ConnectTo(PatrolNode* pNodeToConnect, INode *pINodeTarget);

	void InitUI();
	void CloseUI();
};

//
//   Class Name: PatrolNodeCreateMode
// Base Classes: 
//  Description: 
//    Functions: 
//
//
class PatrolNodeCreateMode : public CommandMode
{
private:
	PatrolNodeCreateMgr m_createMgr;

public:

	void Begin( IObjCreate *ioc, ClassDesc *desc ) { m_createMgr.Begin( ioc, desc ); }
	void End() { m_createMgr.End(); }
	// from CommandMode
	int Class() {return CREATE_COMMAND;}
	int ID() {return CID_PATROL_NODE;}
	MouseCallBack *MouseProc(int *numPoints) {*numPoints = 10000; return &m_createMgr;}
	ChangeForegroundCallback *ChangeFGProc() { return CHANGE_FG_SELECTED; }
	BOOL ChangeFG( CommandMode *oldMode ) { return (oldMode->ChangeFGProc() != CHANGE_FG_SELECTED); }
	void EnterMode() {}
	void ExitMode() {}
};

extern PatrolNodeCreateMode thePatrolNodeCreateMode;

#endif // !defined(INC_NODE_CREATE_MGR_H_)