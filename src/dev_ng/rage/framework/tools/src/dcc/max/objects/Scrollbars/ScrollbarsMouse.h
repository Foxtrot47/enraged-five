//
// filename:	ScrollbarsMouse.h
// author:		David Muir
// date:		23 January 2007
// description:	Mouse handling for creating/updating Scrollbars Helper Objects
//

#ifndef INC_SCROLLBARSMOUSE_H_
#define INC_SCROLLBARSMOUSE_H_

// --- Include Files ------------------------------------------------------------

// 3D Studio Max SDK headers
#include "Max.h"

using namespace rage;

// Local headers

// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------
class CScrollbars;

//
// class:		CreateScrollbarsMouseCallback
// description: Scrollbars object mouse interaction
//
class CScrollbarsMouse : public MouseCallBack, 
						 public ReferenceMaker
{
public:
	CScrollbarsMouse( );
	~CScrollbarsMouse( );

	void Begin( IObjCreate* pObjCreate, ClassDesc* pDescriptor );
	void End( );
	void CreateNewScrollbars( );

	// reference to object array being created
	int NumRefs() { return 1; }
	RefTargetHandle GetReference( int i );
	void SetReference( int i, RefTargetHandle rtarg );
	RefResult NotifyRefChanged( Interval changeInt, RefTargetHandle hTarget, 
								PartID& partID,  RefMessage message );

	int proc( HWND hwnd, int msg, int point, int flag, IPoint2 m );
private:
	bool			m_bIgnoreSelectionChange;
	bool			m_bIsAttachedToNode;
	s32			m_LastPutCount;
	s32			m_NumPointNodes;
	s32			m_CurrentPointNode;
	s32			m_PreviousPointNode;
	Point3			m_pt0;
	Point3			m_pt1;
	IObjCreate*		m_pInterface;
	CScrollbars*	m_pScrollbars;
	INode*			m_pNode;
};

// --- Globals ------------------------------------------------------------------

#endif // !INC_SCROLLBARSMOUSE_H_

// End of file
