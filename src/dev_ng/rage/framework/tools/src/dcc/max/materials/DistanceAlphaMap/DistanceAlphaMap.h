#pragma once

//**************************************************************************/
// Copyright (c) 1998-2007 Autodesk, Inc.
// All rights reserved.
// 
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Autodesk, Inc., and are
// protected by Federal copyright law. They may not be disclosed to third
// parties or copied or duplicated in any form, in whole or in part, without
// the prior written consent of Autodesk, Inc.
//**************************************************************************/
// DESCRIPTION: Includes for Plugins
// AUTHOR: 
//***************************************************************************/

#include "3dsmaxsdk_preinclude.h"
#include "Max.h"
#include "resource.h"
#include "istdplug.h"
#include "iparamb2.h"
#include "iparamm2.h"
//SIMPLE TYPE

#include "stdmat.h"
#include "imtl.h"
#include "macrorec.h"
#include "iFnPub.h"

#include "IRsExportTexture.h"

extern TCHAR *GetString(int id);

extern HINSTANCE hInstance;

#define DistanceAlphaMap_CLASS_ID	Class_ID(0xc61c5f5a, 0x61b774e4)

#define NSUBTEX		2 // TODO: number of sub-textures supported by this plugin 

enum eRefs
{
	COORD_REF = 0,
	PBLOCK_REF,
	SUBTEX1_REF,
	SUBTEX2_REF
};

enum { distancealphamap_params };


//TODO: Add enums for various parameters
enum { 
	pb_button_diffuse,
	pb_button_alpha,
	pb_spin_scale,
	pb_coords,
	pb_button_result,
};

#define DISTANCEMAP_INTERFACE Interface_ID(0x5e287dbb, 0x1fbb17bd)
#define GetEMFaceOpsInterface(cd) (EMFaceOps *)(cd)->GetFPInterface(DISTANCEMAP_INTERFACE) 
enum 
{ 
	fp_create,
	fp_getLastExportedPath,
	fp_getResultMap,
}; 

class DAMapInterface : public  FPMixinInterface
{   
public:     
	BEGIN_FUNCTION_MAP     
		FN_2(fp_create, TYPE_BOOL, CreateField, TYPE_BOOL, TYPE_STRING)
		FN_0(fp_getLastExportedPath, TYPE_STRING, GetMapName)
		FN_0(fp_getResultMap, TYPE_BITMAP, GetResultMap)
	END_FUNCTION_MAP

    FPInterfaceDesc* GetDesc();
	virtual BOOL CreateField (BOOL preview, const char *filepath)=0;
	virtual const char *GetMapName ()=0;
	virtual BitmapTex *GetResultMap ()=0;
};

class DistanceAlphaMap;

class DistanceAlphaMapSampler: public MapSampler {
	DistanceAlphaMap	*tex;
public:
	DistanceAlphaMapSampler() { tex= NULL; }
	DistanceAlphaMapSampler(DistanceAlphaMap *c) { tex= c; }
	void Set(DistanceAlphaMap *c) { tex = c; }
	AColor Sample(ShadeContext& sc, float u,float v);
	AColor SampleFilter(ShadeContext& sc, float u,float v, float du, float dv);
	float SampleMono(ShadeContext& sc, float u,float v);
	float SampleMonoFilter(ShadeContext& sc, float u,float v, float du, float dv);
} ;


class DistanceAlphaMap : public Texmap, public DAMapInterface, public IRsExportTexture
{
public:

	// Parameter block
	IParamBlock2	*pblock;	//ref 0

	Texmap			*subtex[NSUBTEX]; //array of sub-materials
	static ParamDlg *uvGenDlg;
	UVGen			*uvGen;
	Interval		ivalid;

	MaxSDK::Util::Path		m_lastExportedPath;
	Interval texHandleValid;
	TexHandle *texHandle;
	HWND			m_dialogHwnd;

	//From MtlBase
	ParamDlg* CreateParamDlg(HWND hwMtlEdit, IMtlParams *imp);
	BOOL SetDlgThing(ParamDlg* dlg);
	void Update(TimeValue t, Interval& valid);
	void Reset();
	Interval Validity(TimeValue t);
	ULONG LocalRequirements(int subMtlNum);

	//TODO: Return the number of sub-textures
	int NumSubTexmaps() { return NSUBTEX; }
	//TODO: Return the pointer to the 'i-th' sub-texmap
	Texmap* GetSubTexmap(int i);
	void SetSubTexmap(int i, Texmap *m);
	TSTR GetSubTexmapSlotName(int i);

	//From Texmap
	RGBA EvalColor(ShadeContext& sc);
	float EvalMono(ShadeContext& sc);
	Point3 EvalNormalPerturb(ShadeContext& sc);

	//TODO: Returns TRUE if this texture can be used in the interactive renderer
	BOOL SupportTexDisplay() { return TRUE; }
	void DiscardTexHandle();
	void ActivateTexDisplay(BOOL onoff);
	BITMAPINFO* GetVPDisplayDIB(TimeValue t, TexHandleMaker& thmaker, Interval &valid, BOOL mono=FALSE, BOOL forceW=0, BOOL forceH=0);
	DWORD_PTR GetActiveTexHandle(TimeValue t, TexHandleMaker& thmaker);
	//TODO: Return UV transformation matrix for use in the viewports
	void GetUVTransform(Matrix3 &uvtrans) { uvGen->GetUVTransform(uvtrans); }
	//TODO: Return the tiling state of the texture for use in the viewports
	int GetTextureTiling() { return  uvGen->GetTextureTiling(); }
	int GetUVWSource() { return uvGen->GetUVWSource(); }
	UVGen *GetTheUVGen() { return uvGen; }

	//TODO: Return anim index to reference index
	int SubNumToRefNum(int subNum) { return subNum; }

	// Loading/Saving
	IOResult Load(ILoad *iload);
	IOResult Save(ISave *isave);

	//From Animatable
	Class_ID ClassID() {return DistanceAlphaMap_CLASS_ID;}		
	SClass_ID SuperClassID() { return TEXMAP_CLASS_ID; }
	void GetClassName(TSTR& s) {s = GetString(IDS_CLASS_NAME);}

	RefTargetHandle Clone( RemapDir &remap );
	RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, 
		PartID& partID,  RefMessage message);


	int NumSubs() { return 2+NSUBTEX; }
	Animatable* SubAnim(int i); 
	TSTR SubAnimName(int i);

	// TODO: Maintain the number or references here 
	int NumRefs() { return 2+NSUBTEX; }
	RefTargetHandle GetReference(int i);
	void SetReference(int i, RefTargetHandle rtarg);

	int	NumParamBlocks() { return 1; }					// return number of ParamBlocks in this instance
	IParamBlock2* GetParamBlock(int i) { return pblock; } // return i'th ParamBlock
	IParamBlock2* GetParamBlockByID(BlockID id) { return (pblock->ID() == id) ? pblock : NULL; } // return id'd ParamBlock

	void DeleteThis() { delete this; }		
	//Constructor/Destructor

	BOOL CreateField(BOOL preview=TRUE, const char *filepath = NULL);
	BitmapTex *GetResultMap();
	const char *GetMapName();

	BaseInterface* GetInterface(Interface_ID id) 
	{     
	 if (id == DISTANCEMAP_INTERFACE)      
		 return (DAMapInterface*)this;
// 	 else if(id == RSEXPORTTEXTURE_INTERFACE)
// 		 return (IRsExportTexture*)this;
	 else       
		 return Texmap::GetInterface(id);
	}

	DistanceAlphaMap();
	~DistanceAlphaMap();

};

//extern DistanceAlphaMap *gp_DistanceAlphaMap;

class DistanceAlphaMapClassDesc : public ClassDesc2 
{
public:
	virtual int IsPublic() 							{ return TRUE; }
	virtual void* Create(BOOL /*loading = FALSE*/) 		
	{ 
		return new DistanceAlphaMap();
	}
	virtual const TCHAR *	ClassName() 			{ return GetString(IDS_CLASS_NAME); }
	virtual SClass_ID SuperClassID() 				{ return TEXMAP_CLASS_ID; }
	virtual Class_ID ClassID() 						{ return DistanceAlphaMap_CLASS_ID; }
	virtual const TCHAR* Category() 				{ return GetString(IDS_CATEGORY); }

	virtual const TCHAR* InternalName() 			{ return _T("DistanceAlphaMap"); }	// returns fixed parsable name (scripter-visible name)
	virtual HINSTANCE HInstance() 					{ return hInstance; }					// returns owning module handle


};

class DistanceAlphaMapWndProc : public ParamMap2UserDlgProc
{
public:
	DistanceAlphaMap *pDam;
// 	BOOL CALLBACK DialogProc(HWND hDlg, UINT msg,            
// 							 WPARAM wParam, LPARAM lParam)
	virtual INT_PTR  DlgProc (TimeValue t, IParamMap2 *map, HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{   
		switch (msg)
		{ 
		case WM_INITDIALOG: // Initialize the Controls here. 
			pDam->m_dialogHwnd = hWnd;
			return TRUE;     
		case WM_DESTROY: // Release the Controls here.      
			return FALSE;    
		case WM_COMMAND: // Various messages come in this way.
			{
				switch(LOWORD(wParam))     
				{ 
					// Switch on ID      
				case IDC_BUTT_PREVIEW: // A specific button's ID.        
					if(pDam)
					{
						pDam->CreateField();
					}
// 					switch (HIWORD(wParam)) 
// 					{ // Notification codes      
// 					case BN_BUTTONDOWN: // Button is pressed.
// 						break;      
// 					case BN_BUTTONUP: // Button is released.         
// 						break;      
// 					case BN_RIGHTCLICK: // User right clicked.         
// 						break;     
// 					};			
					break; 
				}
			}
// 		case WM_NOTIFY: // Others this way...      
// 			break;     // Other cases...     
		default:     
			break;   
		}   
		return FALSE;
	}
	DistanceAlphaMapWndProc(DistanceAlphaMap *dam)
		: pDam(dam)
	{}
	virtual void DeleteThis()
	{
		pDam = NULL;
		/**/
	}
};