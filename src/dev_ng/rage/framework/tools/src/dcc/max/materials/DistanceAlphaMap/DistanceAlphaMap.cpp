//**************************************************************************/
// Copyright (c) 1998-2007 Autodesk, Inc.
// All rights reserved.
// 
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Autodesk, Inc., and are
// protected by Federal copyright law. They may not be disclosed to third
// parties or copied or duplicated in any form, in whole or in part, without
// the prior written consent of Autodesk, Inc.
//**************************************************************************/
// DESCRIPTION: Appwizard generated plugin
// AUTHOR: 
//***************************************************************************/

#include "DistanceAlphaMap.h"
#include "DistanceField.h"
#include "bitmap.h"
#include "devil/il.h"
#include "devil/ilu.h"
#include "system/timer.h"
#include "assetmanagement/iassetmanager.h"

ParamDlg* DistanceAlphaMap::uvGenDlg;
//DistanceAlphaMap *gp_DistanceAlphaMap = NULL;

ClassDesc2* GetDistanceAlphaMapDesc() { 
	static DistanceAlphaMapClassDesc DistanceAlphaMapDesc;
	return &DistanceAlphaMapDesc; 
}

//DistanceAlphaMapWndProc gDistanceAlphaMapWndProc;

static ParamBlockDesc2 distancealphamap_param_blk( 
	distancealphamap_params, 
	_T("params"),  0, GetDistanceAlphaMapDesc(), 
	P_AUTO_CONSTRUCT + P_AUTO_UI, PBLOCK_REF, 
	//rollout
	IDD_PANEL, IDS_PARAMS, 0, 0, NULL,//&gDistanceAlphaMapWndProc,
	// params
	pb_spin_scale, 		_T("scale"), 		TYPE_FLOAT, 	P_ANIMATABLE, 	IDS_SPIN_SCALE, 
		p_default, 		96.0f, 
		p_range, 		0.0f,1024.0f, 
		p_ui, 			TYPE_SPINNER,		EDITTYPE_FLOAT, IDC_EDIT_SCALE,	IDC_SPIN_SCALE, 96.0f, 
	end,
	pb_button_diffuse,	_T("diffuseMap"), 	TYPE_TEXMAP, 	NULL, 	IDS_BUTT_DIFFUSE, 
		p_ui, 			TYPE_TEXMAPBUTTON,	IDC_BUTT_DIFFUSE,
	end,
	pb_button_alpha,	_T("alphaMap"), 	TYPE_TEXMAP, 	NULL, 	IDS_BUTT_ALPHA, 
		p_ui, 			TYPE_TEXMAPBUTTON,	IDC_BUTT_ALPHA, 
	end,
	pb_button_result,	_T("resultMap"), 	TYPE_TEXMAP, 	NULL, 	IDS_BUTT_RESULT, 
	end,
	pb_coords,			_T("coords"),		TYPE_REFTARG,	P_OWNERS_REF,	IDS_COORDS,
	p_refno,			COORD_REF, 
	end,
	end
	);

static  FPInterfaceDesc damap_mixininterface(
	DISTANCEMAP_INTERFACE,   
	_T("daMap"), 0,
	GetDistanceAlphaMapDesc(), FP_MIXIN,  
	fp_create, _T("Create"), IDS_CREATE, TYPE_BOOL, 0, 2,     
		_T("preview"), 0, TYPE_BOOL,   
		_T("filepath"), 0, TYPE_STRING,   
	fp_getLastExportedPath, _T("GetMapName"), IDS_LASTEXPORTEDPATH, TYPE_STRING, 0, 0,
	fp_getResultMap, _T("GetResultMap"), IDS_LASTEXPORTEDPATH, TYPE_BITMAP, 0, 0,
	end
	); 
FPInterfaceDesc* DAMapInterface::GetDesc()
{   
	return &damap_mixininterface;
}

//--- DistanceAlphaMap -------------------------------------------------------
DistanceAlphaMap::DistanceAlphaMap()
: texHandle(NULL)
{
	for (int i=0; i<NSUBTEX; i++) subtex[i] = NULL;
	//TODO: Add all the initializing stuff
	pblock = NULL;
	GetDistanceAlphaMapDesc()->MakeAutoParamBlocks(this);
	uvGen = NULL;
	Reset();
}

DistanceAlphaMap::~DistanceAlphaMap()
{

}

//From MtlBase
void DistanceAlphaMap::Reset() 
{
	if (uvGen) uvGen->Reset();
	else ReplaceReference( 0, GetNewDefaultUVGen());	
	//TODO: Reset texmap back to its default values

	ivalid.SetEmpty();

}

void DistanceAlphaMap::Update(TimeValue t, Interval& valid) 
{	
	//TODO: Add code to evaluate anything prior to rendering
	// Update the sub textures
	int count = NumSubTexmaps();
	for(int i = 0; i < count; ++i) {
		Texmap* subMap = GetSubTexmap(i);
		if(subMap != NULL)
			subMap->Update(t, valid);
	}
}

Interval DistanceAlphaMap::Validity(TimeValue t)
{
	//TODO: Update ivalid here
	Interval valid = FOREVER;
	int count = NumParamBlocks();
	int i;
	for(i = 0; i < count; ++i) {
		IParamBlock2* pBlock = GetParamBlock(i);
		if(pBlock != NULL)
			pBlock->GetValidity(t, valid);
	}

	count = NumSubTexmaps();
	for(i = 0; i < count; ++i) {
		Texmap* subMap = GetSubTexmap(i);
		if(subMap != NULL)
			valid &= subMap->Validity(t);
	}
	return valid;
//	return ivalid;
}

ParamDlg* DistanceAlphaMap::CreateParamDlg(HWND hwMtlEdit, IMtlParams *imp) 
{
	IAutoMParamDlg* masterDlg = GetDistanceAlphaMapDesc()->CreateParamDlgs(hwMtlEdit, imp, this);
	uvGenDlg = uvGen->CreateParamDlg(hwMtlEdit, imp);
	masterDlg->AddDlg(uvGenDlg);
	GetDistanceAlphaMapDesc()->SetUserDlgProc(&distancealphamap_param_blk, new DistanceAlphaMapWndProc(this));
	//TODO: Set the user dialog proc of the param block, and do other initialization	
	return masterDlg;	
}

BOOL DistanceAlphaMap::SetDlgThing(ParamDlg* dlg)
{	
	if (dlg == uvGenDlg)
		uvGenDlg->SetThing(uvGen);
	else 
		return FALSE;
	return TRUE;
}

Texmap* DistanceAlphaMap::GetSubTexmap(int i) 
{
	//return subtex[i];
	TimeValue t = GetCOREInterface()->GetTime();
	Texmap *pTex = NULL;
	
	if(GetParamBlock(0) != NULL) 
	{
		switch(i) {
		case 0:
			if(!GetParamBlock(0)->GetValue(pb_button_diffuse, t, pTex, FOREVER))
				pTex = NULL;
			break;
		case 1:
			if(!GetParamBlock(0)->GetValue(pb_button_alpha, t, pTex, FOREVER))
				pTex = NULL;
			break;
		case 2:
			if(!GetParamBlock(0)->GetValue(pb_button_result, t, pTex, FOREVER))
				pTex = NULL;
			break;
		default:
			break;
		}
	}

	return pTex;
}

void DistanceAlphaMap::SetSubTexmap(int i, Texmap *m) 
{
	//ReplaceReference(i+2,m);
	MSTR classname;
	m->GetClassName(classname);
	if(classname!=CStr("Bitmap"))
	{
		MessageBox(NULL, "You have to give this a bitmap texture. This will not get put into the texture slot.", "Invalid sub texture type", MB_ICONEXCLAMATION);
		return;	
	}
	if(GetParamBlock(0) != NULL) 
	{
		switch(i) {
		case 0:
			{
				// Set texmap name
				IParamBlock2* pb2 = m->GetParamBlock(0);   // get the bitmap parameter  
				int n = pb2->GetDesc()->NameToIndex("bitmap");  
				ParamID id = pb2->GetDesc()->IndextoID(n);
				PBBitmap* pDiffuseBmp = pb2->GetBitmap(id);	
				char path[255], filename[255], ext[16];
				SplitFilename(pDiffuseBmp->bi.Filename(), path,filename,ext);
				strcat(filename, "_dam");
				SetName(filename);
			}
			GetParamBlock(0)->SetValue(pb_button_diffuse, 0, m);
			GetParamBlock(0)->SetValue(pb_button_result, 0, NULL);
			break;
		case 1:
			GetParamBlock(0)->SetValue(pb_button_alpha, 0, m);
			GetParamBlock(0)->SetValue(pb_button_result, 0, NULL);
			break;
		case 2:
			GetParamBlock(0)->SetValue(pb_button_result, 0, m);
			break;
		default:
			break;
		}
	}
	//TODO Store the 'i-th' sub-texmap managed by the texture
}

TSTR DistanceAlphaMap::GetSubTexmapSlotName(int i) 
{	
	//TODO: Return the slot name of the 'i-th' sub-texmap
	switch (i) {
		case 0: return GetString(IDS_BUTT_DIFFUSE);		
		case 1: return GetString(IDS_BUTT_ALPHA);
		default: return GetString(IDS_BUTT_RESULT);
	}
}


//From ReferenceMaker
RefTargetHandle DistanceAlphaMap::GetReference(int i) 
{
	//TODO: Return the references based on the index	
	switch (i) {
		case 0: return uvGen;
		case 1: return pblock;
		default: return subtex[i-2];
		}
}

void DistanceAlphaMap::SetReference(int i, RefTargetHandle rtarg) 
{
	//TODO: Store the reference handle passed into its 'i-th' reference
	switch(i) {
		case 0: uvGen = (UVGen *)rtarg; break;
		case 1:	pblock = (IParamBlock2 *)rtarg; break;
		default: subtex[i-2] = (Texmap *)rtarg; break;
	}
}

//From ReferenceTarget 
RefTargetHandle DistanceAlphaMap::Clone(RemapDir &remap) 
{
	DistanceAlphaMap *mnew = new DistanceAlphaMap();
	int count = NumRefs();
	for(int i = 0; i < count; ++i) {
		ReferenceTarget* refTarg = GetReference(i);
		mnew->ReplaceReference(i, ((refTarg != NULL) ? remap.CloneRef(refTarg) : NULL));
	}
// 	*((MtlBase*)mnew) = *((MtlBase*)this); // copy superclass stuff
// 	//TODO: Add other cloning stuff
// 	BaseClone(this, mnew, remap);
	return (RefTargetHandle)mnew;
}

	 
Animatable* DistanceAlphaMap::SubAnim(int i) 
{
	//TODO: Return 'i-th' sub-anim
	switch (i) {
		case 0: return uvGen;
		case 1: return pblock;
		default: return subtex[i-2];
		}
}

TSTR DistanceAlphaMap::SubAnimName(int i) 
{
	//TODO: Return the sub-anim names
	switch (i) {
		case 0: return GetString(IDS_COORDS);		
		case 1: return GetString(IDS_PARAMS);
		default: return GetSubTexmapTVName(i-1);
		}
}

RefResult DistanceAlphaMap::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, 
   PartID& partID, RefMessage message ) 
{
	//TODO: Handle the reference changed messages here	
	return(REF_SUCCEED);
}

IOResult DistanceAlphaMap::Save(ISave *isave) 
{
	//TODO: Add code to allow plugin to save its data
	return IO_OK;
}

IOResult DistanceAlphaMap::Load(ILoad *iload) 
{ 
	//TODO: Add code to allow plugin to load its data
	return IO_OK;
}

AColor DistanceAlphaMap::EvalColor(ShadeContext& sc)
{
	//TODO: Evaluate the color of texture map for the context.
	if(GetParamBlock(0) != NULL) 
	{
		Point2 screenUV;
		Point2 screenDUV;
		sc.ScreenUV(screenUV, screenDUV);

		// Front map is used for top part of the image
		bool useFront = (screenUV.y > 0.5f);

		TimeValue t = sc.CurTime();

		// Return the color only
		Texmap* resultmap = GetParamBlock(0)->GetTexmap(pb_button_result, t);
		Texmap* diffusemap = GetParamBlock(0)->GetTexmap(pb_button_diffuse, t);
		if(resultmap != NULL)
			return resultmap->EvalColor(sc);
		else if(diffusemap!=NULL)
			return diffusemap->EvalColor(sc);
	}
	return AColor (0.0f,0.0f,0.0f,0.0f);
}

float DistanceAlphaMap::EvalMono(ShadeContext& sc)
{
	//TODO: Evaluate the map for a "mono" channel
	return Intens(EvalColor(sc));
}

Point3 DistanceAlphaMap::EvalNormalPerturb(ShadeContext& sc)
{
	//TODO: Return the perturbation to apply to a normal for bump mapping
	return Point3(0, 0, 0);
}

ULONG DistanceAlphaMap::LocalRequirements(int subMtlNum)
{
	//TODO: Specify various requirements for the material
	return uvGen->Requirements(subMtlNum); 
}

void DistanceAlphaMap::DiscardTexHandle() 
{
	if (texHandle) {
		texHandle->DeleteThis();
		texHandle = NULL;
	}
}

void DistanceAlphaMap::ActivateTexDisplay(BOOL onoff) {
	if (!onoff) 
		DiscardTexHandle();
}

BITMAPINFO* DistanceAlphaMap::GetVPDisplayDIB(TimeValue t, TexHandleMaker& thmaker, Interval &valid, BOOL mono, BOOL forceW, BOOL forceH) {
	Interval v;
	Update(t,v);

//	bm = BuildBitmap(thmaker.Size());
	BITMAPINFO *bmi = NULL;

	Bitmap* bm = NULL;
	BitmapInfo bi;
	bool bDeleteBM = true;
	Texmap* pResult = GetParamBlock(0)->GetTexmap(pb_button_result, t);
	Texmap* pDiffuse = GetParamBlock(0)->GetTexmap(pb_button_diffuse, t);
	if(pResult)
	{
		BitmapTex *pBitTex = dynamic_cast<BitmapTex *>(pResult);
		if(!pBitTex)
			return NULL;
		bm = pBitTex->GetBitmap(t);
		bDeleteBM = false;
	}
	else if(pDiffuse)
	{
		BitmapTex *pBitTex = dynamic_cast<BitmapTex *>(pDiffuse);
		if(!pBitTex)
			return NULL;
		bm = pBitTex->GetBitmap(t);
		bDeleteBM = false;
	}
	else
	{
		return NULL;
	}

	// if all went well until here...
	bmi = thmaker.BitmapToDIB(bm,uvGen->SymFlags(),0,forceW,forceH);
	if(bDeleteBM)
	 	bm->DeleteThis();
	valid.SetInfinite();
	return bmi;
}

DWORD_PTR DistanceAlphaMap::GetActiveTexHandle(TimeValue t, TexHandleMaker& thmaker) {
	if (texHandle) {
		if (texHandleValid.InInterval(t))
			return texHandle->GetHandle();
		else DiscardTexHandle();
	}
	texHandle = thmaker.MakeHandle(GetVPDisplayDIB(t,thmaker,texHandleValid));
	return texHandle->GetHandle();
}

using namespace MaxSDK::Util;
using namespace MaxSDK::AssetManagement;

BOOL DistanceAlphaMap::CreateField(BOOL preview, const char *filepath)
{
//	bool preview = filepath==NULL;

	Interface *ip = GetCOREInterface();
	TimeValue t = ip->GetTime();

	Texmap *pDiffuse=NULL, *pAlpha=NULL;
	GetParamBlock(0)->GetValue(pb_button_diffuse, t, pDiffuse, FOREVER);
	GetParamBlock(0)->GetValue(pb_button_alpha, t, pAlpha, FOREVER);
	float scale;
	GetParamBlock(0)->GetValue(pb_spin_scale, t, scale, FOREVER);
	if(!pDiffuse || !pAlpha)
	{
		MessageBox(NULL, "Both textures have to be defined.", "Missing parameter", MB_ICONEXCLAMATION);
		return FALSE;	
	}
	IParamBlock2* pb2 = pDiffuse->GetParamBlock(0);   // get the bitmap parameter  
	int n = pb2->GetDesc()->NameToIndex("bitmap");  
	ParamID id = pb2->GetDesc()->IndextoID(n);
	PBBitmap* pDiffuseBmp = pb2->GetBitmap(id);	
	DebugPrint("Diffuse map: %s\n", pDiffuseBmp->bi.GetPathEx().GetCStr());
	pb2 = pAlpha->GetParamBlock(0);   // get the bitmap parameter  
	n = pb2->GetDesc()->NameToIndex("bitmap");
	id = pb2->GetDesc()->IndextoID(n);
	PBBitmap* pAlphaBmp = pb2->GetBitmap(id);	
	DebugPrint("Alpha map: %s\n", pAlphaBmp->bi.GetPathEx().GetCStr());

	if( (pDiffuseBmp->bi.Width()>pAlphaBmp->bi.Width()) || (pDiffuseBmp->bi.Height()>pAlphaBmp->bi.Height()))
	{
		MessageBox(NULL, "DistanceField texture has to be at least equal in resolution than the diffuse texture.", "Size error", MB_ICONEXCLAMATION);
		return FALSE;	
	}

	//Read command-line arguments
	bool needsFileName = true;
	Path newFilePath;
	if(filepath)
	{
		newFilePath = Path(filepath);
		newFilePath.Normalize();
	}
	else
	{
		MSTR outputFilename(GetName());
		if(!strcmp(outputFilename, ""))
			outputFilename.Append("distancemap.dds");
		MSTR outputFilePath;
		outputFilePath.Append(newFilePath.GetCStr());
		FilterList filterList;
		filterList.Append(_M("DirectX draw surface(*.dds)"));
		filterList.Append(_M("*.dds"));
		bool res = GetCOREInterface8()->DoMaxSaveAsDialog(
			ip->GetMAXHWnd(),
			_T("Select file for Distancemap creation output."),
			outputFilename,
			outputFilePath,
			filterList
			);
		if (!res) 
		{
			return FALSE;
		}
		else
		{
// 			char tempPath[255];
// 			strcpy(tempPath,bi.Name());
// 			char *extension = strrchr(tempPath, '.');
// 			if(strcmp(extension, ".dds"))
// 				strcpy(extension, ".dds");
// 			newFilePath = tempPath;
//			outputFilePath.Append(outputFilename);
			newFilePath = outputFilename;
 			needsFileName = false;
		}
	}

	if(needsFileName)
	{
		char *outputFilename(GetName());
		if(!strcmp(outputFilename, ""))
			newFilePath.Append("distancemap.dds");
		else
		{
			strcat(outputFilename, ".dds");
			newFilePath.Append(outputFilename);
		}
	}
	newFilePath.Normalize();

	int outputWidth = -1, outputHeight = -1;
	int distScale = 96;

	rage::sysTimer timer;
	timer.Reset();
	ilInit();
	iluInit();
	// Generate distance field
	DistanceField distanceField;
	rage::atString *perror = distanceField.Compute(pDiffuseBmp->bi.GetPathEx().GetCStr(), pAlphaBmp->bi.GetPathEx().GetCStr(), newFilePath.GetCStr(), outputWidth, outputHeight, scale);
	if(perror)
	{
		MSTR msg;
		msg.Append("Diffuse path:");
		msg.Append(pDiffuseBmp->bi.GetPathEx().GetCStr());
		msg.Append("\nAlpha path:");
		msg.Append(pAlphaBmp->bi.GetPathEx().GetCStr());
		msg.Append("\nOutput path:");
		msg.Append(newFilePath.GetCStr());
		msg.Append("\nError:");
		msg.Append(perror->c_str());
		MessageBox(NULL, msg, "Error during conversion", MB_ICONERROR);
		return FALSE;
	}
	ilShutDown();
	float timeTaken = timer.GetTime();
	char msg[255];
	sprintf(msg, "Conversion took: %f\n", timeTaken);
	GetCOREInterface()->PushPrompt(msg);

	SetDlgItemText(m_dialogHwnd, IDC_SHOW_LAST_PATH, newFilePath.GetCStr());

	Bitmap *bmap;
	BitmapInfo bi;

	newFilePath.Normalize();
	char path[255], file[64], ext[16];
	BMMSplitFilename(newFilePath.GetCStr(), path, file, ext);
	strcat(file, ext);
	bi.SetName(newFilePath.GetCStr());
	bi.SetPath(newFilePath);
	// Load the selected image
	BMMRES status;
	bmap = TheManager->Load(&bi, &status);
	if (status != BMMRES_SUCCESS)
	{
		MessageBox(GetCOREInterface()->GetMAXHWnd(), _T("Error loading bitmap."), _T("Error"), MB_ICONSTOP);
		return FALSE;
	}
	BitmapTex *pBitTex = NewDefaultBitmapTex();
// 	pBitTex->SetBitmap(bmap);
// 	pBitTex->SetBitmapInfo(bi);
	IAssetManager *mngr = IAssetManager::GetInstance();
	pBitTex->SetMap(mngr->GetAsset(newFilePath.GetCStr(), kBitmapAsset, false));
	pBitTex->SetName("resultMap_instance");
	//GetParamBlock(0)->SetValue(pb_button_result, 0, pBitTex);
	SetSubTexmap(2, pBitTex);

	if(preview)
	{
		int success = bmap->Display("Preview", BMM_CN, FALSE, TRUE);
		if(!success)
			MessageBox(GetCOREInterface()->GetMAXHWnd(), _T("Failed to show newly created bitmap."), _T("Error"), MB_ICONSTOP);
	}
	return TRUE;
}

BitmapTex *DistanceAlphaMap::GetResultMap()
{
	Texmap* pResult = GetParamBlock(0)->GetTexmap(pb_button_result, GetCOREInterface()->GetTime());
	if(!pResult)
		return NULL;
//	return dynamic_cast<BitmapTex*>(pResult);
	return dynamic_cast<BitmapTex*>(pResult->GetInterface(BITMAPTEX_INTERFACE));
}

const char *DistanceAlphaMap::GetMapName()
{
	BitmapTex * pResult = GetResultMap();
	if(!pResult)
		return "";
	return pResult->GetMapName();
}

// namespace stlp_std {
// void _STLP_DECLSPEC _STLP_CALL __stl_throw_length_error(const char* __msg) { 
// 	//_STLP_THROW_MSG(length_error, __msg); 
// }
// }