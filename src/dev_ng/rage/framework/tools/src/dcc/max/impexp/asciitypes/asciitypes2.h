//
//
//    Filename: asciitypes.h
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Header file describing types for ASCII output
//
//
//
// These classes are used to load/save ASCII types files output by AsciiFile plugin for 3d Studio Max. To 
//	load the file create an instance of the class Scene and call the function Read. The Scene class is defined 
//	at the bottom of this file.
//
//	std::ifstream fs("MyFile.a3d");
//	Scene scene;
//
//	if(scene.Read(fs) == false)
//	{
//		failed;
//	}
//	else
//	{
//		successful;
//	}
//
// Scene has a collection of access functions to get information about the scene contents: GetRootComponent(), 
//	GetAIConnection() etc	
//
// The Component class is used to describe the model hierarchy and its contents. You can process the hierarchy 
//	using a very simple recursive function. The Component class is defined just above the Scene class
//
//	void ProcessHierarchy(Component* pComp)
//	{
//		Component* pChild = pComp->GetChild(0);		// get first child component
//
//		DoStuff();
//		while(pChild)
//		{
//			ProcessHierarchy(pChild);
//			pChild = pChild->GetSibling();
//		}
//	}
//
// Component also has a collection of access functions to get information about its contents: 
// GetCollisionVolume(), GetCullingSphere() etc.
//
// NB everything is in the namespace 'ascii' to stop name clashes with other code.
// The latest C++ headers are used so all the standard C++ library classes are in the namespace 'std'
//
#ifndef INC_ASCII_TYPES_H_
#define INC_ASCII_TYPES_H_

#include <dma.h>
// STD headers
#pragma warning (disable : 4786)
#include <fstream>
#include <string>
#include <vector>


namespace ascii {


// class prototypes
class Point3d;
class Point2d;
class BaseObject;
class Component;
class BoundingSphere;
class BoundingBox;
class CullingSphere;
class CollisionVolume;
class CollisionSphere;
class CollisionBox;
class CollisionPlane;
class CollisionMesh;
class AIConnection;
class AILink;

//
// --- Base Classes ------------------------------------------------------------------------------------------
//

//
//   Class Name: BaseObject
//  Description: Base class for everything in the scene
//	  Functions: BaseObject(): construct from type name
//				~BaseObject(): destructor
//				GetName(): return name of object.
//				Read/Write(): read and write object to file stream
//
class BaseObject
{
public:
	BaseObject(const std::string& name);
	virtual ~BaseObject() {}

	const char *GetName() const;

	virtual bool Read(std::ifstream& is);
	virtual bool Write(std::ofstream& os) const;
};

//
//   Class Name: Point2d
//  Description: Class describing a 2d point
//	  Functions: Point2d(): default constructor
//				Point2d(x,y): construct from 2 floats
//				operator[]: Array subscript operator. There are two versions of this, one that returns a const float
//							reference and one that returns a non-const float reference
//
class Point2d
{
public:
	Point2d() {}
	Point2d(float x, float y) {p[0] = x; p[1] = y;}

	float& operator[](int32 i) {return p[i];}
	const float& operator[](int32 i) const {return p[i];}

private:
	float p[2];
};

//
//   Class Name: Point3d
//  Description: Class describing a 3d point
//	  Functions: Point3d(): default constructor
//				Point3d(x,y,z): construct from 3 floats
//				operator[]: Array subscript operator. There are two versions of this, one that returns a const float
//							reference and one that returns a non-const float reference
//
class Point3d
{
public:
	Point3d() {}
	Point3d(float x, float y, float z) {p[0] = x; p[1] = y; p[2] = z;}

	float& operator[](int32 i) {return p[i];}
	const float& operator[](int32 i) const {return p[i];}

private:
	float p[3];
};

//
//   Class Name: Sphere
//  Description: Class describing a sphere with a position
//	  Functions: Read/Write(): read and write Sphere to file stream
//				GetPosition(): return the sphere position
//				GetRadius(): return the sphere radius
//
class Sphere
{
public:
	virtual bool Read(std::ifstream& is);
	virtual bool Write(std::ofstream& os) const;

	float GetRadius() const;
	const Point3d& GetPosition() const;
};

//
//   Class Name: Box
//  Description: Class describing a box
//	  Functions: Read/Write(): read and write Box to file stream
//				GetPosition(): return the box position
//				GetSize(): return the box size
//
class Box
{
public:
	virtual bool Read(std::ifstream& is);
	virtual bool Write(std::ofstream& os) const;

	const Point3d& GetPosition() const;
	const Point3d& GetSize() const;
};

//
// --- End of Base Classes ------------------------------------------------------------------------------------------
//

//
//   Class Name: CullingSphere
// Base Classes: BaseObject
//  Description: Class describing a culling sphere
//	  Functions: Read/Write(): read and write CullingSphere to file stream
//				GetRadius(): return radius of culling sphere
//
class CullingSphere : public BaseObject
{
public:
	virtual bool Read(std::ifstream& is);
	virtual bool Write(std::ofstream& os) const;

	float GetRadius() const;
};

//
//   Class Name: CollisionVolume
// Base Classes: BaseObject
//  Description: Base class for collision volumes
//	  Functions: As...Ptr(): All Return NULL. They are overriden by their indicated classes to produce some form
//					of runtime type information
//
class CollisionVolume : public BaseObject
{
public:
	virtual CollisionSphere *AsCollisionSpherePtr() {return NULL;}
	virtual CollisionBox *AsCollisionBoxPtr() {return NULL;}
	virtual CollisionPlane *AsCollisionPlanePtr() {return NULL;}
	virtual CollisionMesh *AsCollisionMeshPtr() {return NULL;}
};

//
//   Class Name: CollisionSphere
// Base Classes: CollisionVolume
//				Sphere: class containing a centre point and a radius for a sphere
//  Description: Class holding collision sphere information
//	  Functions: Read/Write(): read and write CollisionSphere to file stream
//				AsCollisionSpherePtr(): overrides CollisionVolume's version to return a pointer to this class
//
class CollisionSphere : public CollisionVolume, public Sphere
{
public:
	virtual bool Read(std::ifstream& is);
	virtual bool Write(std::ofstream& os) const;

	virtual CollisionSphere *AsCollisionSpherePtr() {return this;}
};


//
//   Class Name: CollisionBox
// Base Classes: CollisionVolume: base class for all collision volume classes
//				Box: class containing a centre point and a size for a box
//  Description: Class holding collision box information
//	  Functions: Read/Write(): read and write CollisionBox to file stream
//				AsCollisionBoxPtr(): overrides CollisionVolume's version to return a pointer to this class
//
class CollisionBox : public CollisionVolume, public Box
{
public:
	virtual bool Read(std::ifstream& is);
	virtual bool Write(std::ofstream& os) const;

	virtual CollisionBox *AsCollisionBoxPtr() {return this;}
};


//
//   Class Name: CollisionPlane
// Base Classes: CollisionVolume: base class for all collision volume classes
//  Description: Class holding collision plane information
//	  Functions: Read/Write(): read and write CollisionPlane to file stream
//				AsCollisionPlanePtr(): overrides CollisionVolume's version to return a pointer to this class
//				GetPosition(): return the position of collision plane
//				GetSize(): return the 2D size of collision plane
//				GetPlane(): return which plane collision plane is coplanar with (XY,XZ,YZ)
//
class CollisionPlane : public CollisionVolume
{
public:
	enum Plane {XY=0, XZ, YZ};

	virtual bool Read(std::ifstream& is);
	virtual bool Write(std::ofstream& os) const;

	virtual CollisionPlane *AsCollisionPlanePtr() {return this;}

	const Point3d& GetPosition() const;
	const Point2d& GetSize() const;
	Plane GetPlane() const;
};



//
//   Class Name: CollisionMesh
// Base Classes: CollisionVolume: base class for all collision volume classes
//  Description: Class holding collision mesh information
//	  Functions: Read/Write(): read and write CollisionMesh to file stream
//				AsCollisionMeshPtr(): overrides CollisionVolume's version to return a pointer to this class
//				GetPosition(): return the position of collision mesh
//				GetVertex(): return a vertex from the collision mesh
//				GetNumVertices(): return the number of vertices in the collision mesh
//				GetFace(): return a face from the collision mesh
//				GetNumFaces(): return the number of faces in the collision mesh
//
class CollisionMesh : public CollisionVolume
{
public:
	// Face class used in this class. Basically an array of 3 indices
	class Face
	{
	public:
		Face() {}
		Face(int32 x, int32 y, int32 z) {v[0] = x; v[1] = y; v[2] = z;}

		int32& operator[](int32 i) {return v[i];}
		const int32& operator[](int32 i) const {return v[i];}
	private:
		int32 v[3];
	};
	
	virtual bool Read(std::ifstream& is);
	virtual bool Write(std::ofstream& os) const;

	virtual CollisionMesh *AsCollisionMeshPtr() {return this;}

	const Point3d& GetPosition() const;
	Point3d& GetVertex(int32 i);
	int32 GetNumVertices();
	Face& GetFace(int32 i);
	int32 GetNumFaces();
};

//
//   Class Name: BoundingSphere
// Base Classes: BaseObject: base class for all objects
//				Sphere: class containing a centre point and a radius for a sphere
//  Description: Class holding component bounding sphere information
//	  Functions: Read/Write(): read and write BoundingSphere to file stream
//
class BoundingSphere : public BaseObject, public Sphere
{
public:
	virtual bool Read(std::ifstream& is);
	virtual bool Write(std::ofstream& os) const;
};


//
//   Class Name: BoundingBox
// Base Classes: BaseObject: base class for all objects
//				Box: class containing a centre point and a size for a box
//  Description: Class holding collision box information
//	  Functions: Read/Write(): read and write BoundingBox to file stream
//
class BoundingBox : public BaseObject, public Box
{
public:
	virtual bool Read(std::ifstream& is);
	virtual bool Write(std::ofstream& os) const;
};





//
//   Class Name: AILink
// Base Classes: BaseObject: base class for all objects
//  Description: Class holding information about the links from one AIConnection to another
//	  Functions: Read/Write(): read and write AILink to file stream
//				GetConnection(): return the AIConnection at the other end of the link
//
class AILink : public BaseObject
{
public:
	virtual bool Read(std::ifstream& is);
	virtual bool Write(std::ofstream& os) const;

	AIConnection* GetConnection();
};


//
//   Class Name: AIConnection
// Base Classes: BaseObject: base class for all objects
//  Description: Class holding information about the AIConnection plugin
//	  Functions: Read/Write(): read and write AIConnection to file stream
//				GetPosition(): return position of AI connection
//				GetLink(): return one of the links attached to the AIConnection
//				GetNumLinks(): return number of links attached to the AIConnection
//
class AIConnection : public BaseObject
{
public:
	virtual bool Read(std::ifstream& is);
	virtual bool Write(std::ofstream& os) const;

	const Point3d& GetPosition() const;

	AILink *GetLink(int32 i);
	int32 GetNumLinks() const;
};

//
//   Class Name: Component
// Base Classes: BaseObject: base class for all objects
//  Description: Class describing a component in the scene
//	  Functions: Read/Write(): read and write component to file stream
//				GetSibling(): return the next sibling component in hierarchy
//				GetPreviousSibling(): return the previous sibling component in hierarchy
//				GetParent(): return parent component in hierarchy
//				GetChild(): return one of the children components in hierarchy
//				GetCullingSphere(): return the culling sphere if one exists
//				GetBoundingSphere(): return the smallest sphere that contains this component
//				GetBoundingBox(): return the smallest cuboid that contains this component
//				GetCollisionVolume(): return a collision volume from the component's collision volume list
//				GetNumCollisionVolumes(): return the size of the component's collision volume list 
//
class Component : public BaseObject
{
public:
	virtual bool Read(std::ifstream& is);
	virtual bool Write(std::ofstream& os) const;

	Component* GetSibling();
	Component* GetPreviousSibling();
	Component* GetParent();
	Component* GetChild(int32 i);

	CullingSphere *GetCullingSphere();
	BoundingSphere *GetBoundingSphere();
	BoundingBox *GetBoundingBox();

	CollisionVolume *GetCollisionVolume(int32 i);
	int32 GetNumCollisionVolumes() const;
};

//
//   Class Name: Scene
//  Description: Interface class to the scene
//    Functions: Scene(): default constructor.
//				Clear(): Empty the scene.
//				Read(): Read scene from file stream.
//				GetComponent(): Access a component from the scene component list.
//				GetNumComponents(): Return the number of components in the scene.
//				GetRootComponent(): Return the root component of the hierarchy tree.
//				GetAIConnection(): Access an AI Connection from the scene.
//				GetNumAIConnections(): Return number of AI Connections in scene
//
class Scene 
{
public:
	// default constructor
	Scene();
	~Scene();

	void Clear();
	bool Read(std::ifstream& is);

	// components
	Component* GetComponent(int32 i);
	int32 GetNumComponents() const;
	Component* GetRootComponent();
	// AI Connections
	AIConnection* GetAIConnection(int32 i);
	int32 GetNumAIConnections() const;

private:
	void* m_pScene;
};



} // namespace ascii

#endif