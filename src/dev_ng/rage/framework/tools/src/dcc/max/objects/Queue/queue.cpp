//
//
//    Filename: queue.cpp
//     Creator: Greg Smith
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Definition of queue class
//
//

#include "queue.h"
#include "queueNode.h"
#include "queueCM.h"
#include "notify.h"
#include "resource.h"

static queueClassDesc thequeueDesc;

IObjParam* queue::m_ip = NULL;
HWND queue::m_hPanel = NULL;
HWND queue::m_hNodePanel = NULL;
MoveModBoxCMode* queue::m_moveMode = NULL;
SelectModBoxCMode* queue::m_selectMode = NULL;
bool queue::m_creating = false;
Object* queue::m_pNodeEdited = NULL;

#if !defined( _WIN64 )
BOOL CALLBACK EditqueueNodeDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
#else
INT_PTR CALLBACK EditqueueNodeDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
#endif

ClassDesc* GetqueueDesc() {return &thequeueDesc;}

// --- ObjPathClassDesc ------------------------------------------------------------------------------------

void *queueClassDesc::Create(BOOL loading)
{
	return new queue;
}

void queueClassDesc::ResetClassParams (BOOL fileReset)
{

}

int queueClassDesc::BeginCreate (Interface *ip)
{
	IObjCreate *pIob = ip->GetIObjCreate();

	theCreatePathCM.Begin( pIob, this );
	pIob->PushCommandMode( &theCreatePathCM );

	return TRUE;
}

int queueClassDesc::EndCreate (Interface *ip)
{
	theCreatePathCM.End();
	ip->RemoveMode( &theCreatePathCM );
	return TRUE;
}

//
// Max3 paramblock2
//
ParamBlockDesc2 pathDesc(path_params, _T("queue Parameters"), IDS_CLASS_NAME, &thequeueDesc, P_AUTO_CONSTRUCT | P_AUTO_UI,
						queue_PBLOCK_REF_ID,
						IDD_PATH_PANEL, IDS_PARAMS, 0,0, NULL,
						end);

//--- ObjPath -------------------------------------------------------

queue::queue() : m_pBlock2(NULL), m_selLevel(0)
{
	for(int i=0; i<MAX_NUM_queueNODE; i++)
	{
		m_pPathNodes[i] = NULL;
		m_pControls[i] = NULL;
		m_nodeSelected[i] = NULL;
	}

	thequeueDesc.MakeAutoParamBlocks(this);
}

queue::~queue()
{
}

void queue::BeginEditParams(IObjParam *ip,ULONG flags,Animatable *prev)
{
	m_ip = ip;

	if(m_selLevel)
		InitPathNodeUI();
	else
		InitPathUI();

	if (flags&BEGIN_EDIT_CREATE) {
		m_creating = true;
	} else {
		m_creating = false;
		// Create sub object editing modes.
		m_moveMode       = new MoveModBoxCMode(this,ip);
		m_selectMode     = new SelectModBoxCMode(this,ip);

		// Add our sub object type
		m_ip->SetSubObjectLevel(m_selLevel);
	}

}

void queue::EndEditParams( IObjParam *ip, ULONG flags,Animatable *next )
{
	if(!m_creating)
	{
		ip->DeleteMode(m_moveMode);
		ip->DeleteMode(m_selectMode);
		if ( m_moveMode )
			delete m_moveMode;
		m_moveMode = NULL;
		if ( m_selectMode )
			delete m_selectMode;
		m_selectMode = NULL;
	}

	ClosePathUI();
	ClosePathNodeUI();

	m_ip = NULL;
}

RefTargetHandle queue::Clone(RemapDir& remap)
{
	queue* pClone = new queue();
	int i;

	for(i=0; i<NumRefs(); i++)
		pClone->ReplaceReference(i, remap.CloneRef(GetReference(i)));

	BaseClone(this, pClone, remap);
	return pClone;
}

Interval queue::ObjectValidity(TimeValue t)
{
	Interval ivalid = FOREVER;
	Matrix3 mtx;

	for(int i=0; i<MAX_NUM_queueNODE; i++)
	{
		if(m_pPathNodes[i])
			ivalid &= m_pPathNodes[i]->ObjectValidity(t);
		if(m_pControls[i])
			m_pControls[i]->GetValue(t, &mtx, ivalid, CTRL_RELATIVE);
	}
	return ivalid;
}

IOResult queue::Load(ILoad *iload)
{
	return IO_OK;
}

IOResult queue::Save(ISave *isave)
{
	return IO_OK;
}

int queue::NumRefs()
{
	return 1 + 2 * MAX_NUM_queueNODE;
}

RefTargetHandle queue::GetReference(int i)
{
	if(i == queue_PBLOCK_REF_ID)
		return m_pBlock2;
	else if(i > 0 && i <= MAX_NUM_queueNODE)
		return m_pPathNodes[i - 1];
	else if(i > MAX_NUM_queueNODE && i <= (MAX_NUM_queueNODE * 2))
		return m_pControls[i - (MAX_NUM_queueNODE + 1)];
	return NULL;
}

void queue::SetReference(int i, RefTargetHandle rtarg)
{
	if(i == queue_PBLOCK_REF_ID)
		m_pBlock2 = (IParamBlock2 *)rtarg;
	else if(i > 0 && i <= MAX_NUM_queueNODE)
		m_pPathNodes[i - 1] = (queueNode*)rtarg;
	else if(i > MAX_NUM_queueNODE && i <= (MAX_NUM_queueNODE * 2))
		m_pControls[i - (MAX_NUM_queueNODE + 1)] = (Control*)rtarg;
}

RefResult queue::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget,
										   PartID& partID,  RefMessage message)
{
	int i;

	switch(message)
	{
	case REFMSG_TARGET_DELETED:
		for(i=0; i<MAX_NUM_queueNODE; i++)
		{
			if(m_pPathNodes[i] == hTarget)
			{
				m_pPathNodes[i] = NULL;
				TidyAfterDelete(i);
			}
		}
		break;
	}
	return REF_SUCCEED;
}

int queue::NumSubs()
{
	return MAX_NUM_queueNODE;
}

Animatable* queue::SubAnim(int i)
{
	return m_pPathNodes[i];
}

TSTR queue::SubAnimName(int i)
{
	TSTR name;
	name.printf("queue%2d", i+1);
	return name;
}

CreateMouseCallBack* queue::GetCreateMouseCallBack()
{
	return NULL;
}

void queue::GetLocalBoundBox(TimeValue t, INode* pNode, ViewExp* pVpt, Box3& box)
{
	int i;
	box.Init();

	Box3 firstBox(Point3(-5.0f,-5.0f,-5.0f),Point3(5.0f,5.0f,5.0f));

	box += firstBox;

	for(i=0; i<MAX_NUM_queueNODE; i++)
	{
		if(m_pPathNodes[i] == NULL)
			continue;

		Point3 posn = GetPathNodePosn(i);

		Box3 localBox(Point3(-0.0f,-0.0f,-0.0f),Point3(0.0f,0.0f,0.0f));

		m_pPathNodes[i]->GetLocalBoundBox(t,NULL,pVpt,localBox);

		localBox.pmin += posn;
		localBox.pmax += posn;

		box += localBox;
	}
}

void queue::GetWorldBoundBox(TimeValue t, INode* pNode, ViewExp* pVpt, Box3& box)
{
	Matrix3 local2world = pNode->GetObjectTM(t);
	Box3 localBox;

	GetLocalBoundBox(t, pNode, pVpt, localBox);

	box = localBox * local2world;
}

int queue::Display(TimeValue t, INode* pNode, ViewExp *pVpt, int flags)
{
	GraphicsWindow *gw = pVpt->getGW();

	Draw(t, pNode, pVpt, flags);

	return 0;
}

int queue::HitTest(TimeValue t, INode* pNode, int type, int crossing, int flags, IPoint2 *pSp, ViewExp *pVpt)
{
	HitRegion hitRegion;
	DWORD savedLimits;
	GraphicsWindow *gw = pVpt->getGW();

	MakeHitRegion(hitRegion, type, crossing, 4, pSp);

	gw->setRndLimits(((savedLimits = gw->getRndLimits()) | GW_PICK) & ~GW_ILLUM);
	gw->setHitRegion(&hitRegion);
	gw->clearHitCode();

	Draw(t, pNode, pVpt, flags);

	int res = gw->checkHitCode();

	gw->setRndLimits(savedLimits);

	return res;
}

void queue::Draw(TimeValue t, INode* pNode, ViewExp *pVpt, int flags)
{
	GraphicsWindow *gw = pVpt->getGW();
	Matrix3 objMtx = pNode->GetObjectTM(t);
	DrawLineProc lineProc(gw);
	Point3 pt[6];

	Matrix3 tmFirst(1);
	Interval ivalid;

	if(!m_pControls[0])
	{
		return;
	}

	m_pControls[0]->GetValue(t, &tmFirst, ivalid, CTRL_RELATIVE);

	gw->setTransform(objMtx);

	int iLines = 0;

	for(int i=1; i<MAX_NUM_queueNODE; i++)
	{
		Matrix3 tm(1);
		Interval ivalid;

		if(m_pPathNodes[i] == NULL)
			continue;

		m_pControls[i]->GetValue(t, &tm, ivalid, CTRL_RELATIVE);

		pt[(iLines * 2)] = tmFirst.GetTrans(); 
		pt[(iLines * 2) + 1] = tm.GetTrans();
		iLines++;
	}

	lineProc.proc(pt,iLines * 2);

	for(int i=0; i<MAX_NUM_queueNODE; i++)
	{
		Matrix3 tm(1);
		Interval ivalid;

		if(m_pPathNodes[i] == NULL)
			continue;

		m_pControls[i]->GetValue(t, &tm, ivalid, CTRL_RELATIVE);

		INodeTransformed node(pNode, tm);
		m_pPathNodes[i]->Display(t, &node, pVpt, flags);
	}
}

ISubObjType* queue::GetSubObjType(int i)
{
	static GenSubObjType subObjType("queueNode", "SubObjectIcons", 29);
	if(i == -1)
		return NULL;
	return &subObjType;
}

void queue::SelectAll( int selLevel )
{
	switch ( selLevel )
	{
	case QUEUE_NODE_LEVEL:
		for ( int i = 0; i < MAX_NUM_queueNODE; ++i )
		{
			// No method to fetch number of queue nodes.  Erk.
			if ( !GetPathNode( i ) )
				continue;

			SelectPathNode( i, true );
		}
	}
}

void queue::Move(TimeValue t, Matrix3& partm, Matrix3& tmAxis,
					  Point3& val, BOOL localOrigin)
{
	for(int i=0; i<MAX_NUM_queueNODE; i++)
	{
		if(m_nodeSelected[i] && m_pControls[i])
		{
			SetXFormPacket pckt(val,partm,tmAxis);
			m_pControls[i]->SetValue(t,&pckt,TRUE,CTRL_RELATIVE);
		}
	}
	NotifyDependents(FOREVER, PART_GEOM, REFMSG_CHANGE);
}

int queue::HitTest(TimeValue t, INode* pNode, int type, int crossing,int flags, IPoint2 *p, ViewExp *pVpt, ModContext* mc)
{
	int res = FALSE;

	for(int i=0; i<MAX_NUM_queueNODE; i++)
	{
		if(m_pPathNodes[i] &&
		!(flags&HIT_SELONLY && m_nodeSelected[i] == false) &&
		!(flags&HIT_UNSELONLY && m_nodeSelected[i] == true) )
		{
			assert(m_pControls[i]);

			Matrix3 tm(1);
			Interval ivalid;

			m_pControls[i]->GetValue(t, &tm, ivalid, CTRL_RELATIVE);

			INodeTransformed node(pNode, tm);

			if(m_pPathNodes[i]->HitTest(t, &node, type, crossing, flags, p, pVpt)) {
				pVpt->LogHit(pNode,mc,0,i,NULL);
				res = TRUE;
				if (flags & HIT_ABORTONHIT)
					return TRUE;
			}
		}
	}

	return res;
}

int queue::SubObjectIndex(HitRecord *hitRec)
{
	return hitRec->hitInfo;
}

void queue::GetSubObjectCenters(SubObjAxisCallback *cb,TimeValue t,INode *node,ModContext *mc)
{
	for(int i=0; i<MAX_NUM_queueNODE; i++)
	{
		if (m_nodeSelected[i] && m_pControls[i]) {
			Matrix3 tm(1);
			Interval ivalid;

			m_pControls[i]->GetValue(t, &tm, ivalid, CTRL_RELATIVE);
			tm *= node->GetObjectTM(t);
			cb->Center(tm.GetTrans(),i);
		}
	}
}

void queue::GetSubObjectTMs(SubObjAxisCallback *cb,TimeValue t,INode *node,ModContext *mc)
{
	for(int i=0; i<MAX_NUM_queueNODE; i++)
	{
		if (m_nodeSelected[i] && m_pControls[i]) {
			Matrix3 tm(1);
			Interval ivalid;

			m_pControls[i]->GetValue(t, &tm, ivalid, CTRL_RELATIVE);
			tm *= node->GetObjectTM(t);
			cb->TM(tm,i);
		}
	}
}

void queue::ActivateSubobjSel(int selLevel, XFormModes& modes)
{
	if (selLevel) {
		modes = XFormModes(m_moveMode,NULL,NULL,NULL,NULL,m_selectMode);
		NotifyDependents(FOREVER, PART_SUBSEL_TYPE|PART_DISPLAY, REFMSG_CHANGE);
		m_ip->PipeSelLevelChanged();
	}

	if(m_selLevel != selLevel)
	{
		if(selLevel)
		{
			ClosePathUI();
			InitPathNodeUI();
		}
		else
		{
			ClosePathNodeUI();
			InitPathUI();
		}

	}
	m_selLevel = selLevel;

	UpdatePathNodeUI();
}

int queue::AddPathNode()
{
	bool coreInterface = false;
	
	int i=0;
	for(i=0; i<MAX_NUM_queueNODE; i++)
	{
		if(m_pPathNodes[i] == NULL)
			break;
	}
	
	if(i == MAX_NUM_queueNODE)
		return -1;

	// if interface pointer is NULL use getCOREInterface to get a pointer
	if(m_ip == NULL)
	{
		m_ip = GetCOREInterface()->GetIObjParam();
		if(m_ip == NULL)
			return -1;
		coreInterface = true;
	}

	ReplaceReference(1 + i, static_cast<Object *>(m_ip->CreateInstance(HELPER_CLASS_ID, queueNODE_CLASS_ID)));
	ReplaceReference(1 + MAX_NUM_queueNODE + i, static_cast<Control*>(m_ip->CreateInstance(CTRL_MATRIX3_CLASS_ID, Class_ID(PRS_CONTROL_CLASS_ID, 0))));

	NotifyDependents(FOREVER, PART_ALL, REFMSG_SUBANIM_STRUCTURE_CHANGED);

	// if got interface pointer thru getCOREInterface set pointer to NULL
	if(coreInterface)
		m_ip = NULL;

	return i;
}

void queue::SetPathNodePosn(int i, Point3 posn)
{
	Point3 oldPosn = GetPathNodePosn(i);

	SetXFormPacket xform(posn - oldPosn);
	m_pControls[i]->SetValue(0, &xform, 1, CTRL_ABSOLUTE);
}

Point3 queue::GetPathNodePosn(int i)
{
	assert(i != -1);
	assert(m_pPathNodes[i] != NULL);
	assert(m_pControls[i] != NULL);

	Matrix3 tm(1);
	Interval ivalid;

	m_pControls[i]->GetValue(0, &tm, ivalid, CTRL_RELATIVE);
	return tm.GetTrans();
}

Matrix3 queue::GetPathNodeMatrix(int i)
{
	assert(i != -1);
	assert(m_pPathNodes[i] != NULL);
	assert(m_pControls[i] != NULL);

	Matrix3 tm(1);
	Interval ivalid;

	m_pControls[i]->GetValue(0, &tm, ivalid, CTRL_RELATIVE);
	return tm;
}

Object* queue::GetPathNode(int i)
{
	return m_pPathNodes[i];
}

void queue::DeletePathNode(int i)
{
	assert(i != -1);
	assert(m_pPathNodes[i] != NULL);
	assert(m_pControls[i] != NULL);

	DeleteReference(1 + i);
	TidyAfterDelete(i);
	NotifyDependents(FOREVER, PART_ALL, REFMSG_SUBANIM_STRUCTURE_CHANGED);
}

int queue::GetPathNodeId(Object *pPathNode)
{
	for(int i=0; i<MAX_NUM_queueNODE; i++)
	{
		if(m_pPathNodes[i] == pPathNode)
			return i;
	}
	return -1;
}

int queue::GetControlId(Control *pControl)
{
	for(int i=0; i<MAX_NUM_queueNODE; i++)
	{
		if(m_pControls[i] == pControl)
			return i;
	}
	return -1;
}

void queue::TidyAfterDelete(int i)
{
	DeleteReference(3 + i);
	m_nodeSelected[i] = false;

	for(int j=0; j<MAX_NUM_queueNODE; j++)
	{
		if(m_pPathNodes[j] == NULL)
			continue;
		IParamBlock2 *pBlock2 = m_pPathNodes[j]->GetParamBlockByID(queuenode_params);
	}
}

ObjectState queue::Eval(TimeValue t)
{
	for(int i=0;i<MAX_NUM_queueNODE;i++)
	{
		if(m_pPathNodes[i])
		{
			m_pPathNodes[i]->Eval(t);
		}
	}

	return ObjectState(this);
}

int queue::HitTest(TimeValue t, INode* pNode, IPoint2 p, int flags, ViewExp *pVpt, int ignore)
{
	for(int i=0; i<MAX_NUM_queueNODE; i++)
	{
		if(i == ignore)
			continue;
		if(m_pPathNodes[i] &&
			!(flags&HIT_SELONLY && m_nodeSelected[i] == false) &&
				!(flags&HIT_UNSELONLY && m_nodeSelected[i] ==true))
		{
			assert(m_pControls[i]);

			Matrix3 tm(1);
			Interval ivalid;

			m_pControls[i]->GetValue(t, &tm, ivalid, CTRL_RELATIVE);

			INodeTransformed node(pNode, tm);

			if(m_pPathNodes[i]->HitTest(t, &node, HITTYPE_POINT, FALSE, flags, &p, pVpt)) {
				return i;
			}
		}
	}

	return -1;
}

void queue::InitPathUI()
{
	thequeueDesc.BeginEditParams(m_ip, this, BEGIN_EDIT_CREATE, NULL);

	IParamMap2 *pMapParam = m_pBlock2->GetMap();
	m_hPanel = pMapParam->GetHWnd();
	SetDlgItemText(m_hPanel, IDC_VERSION, TSTR("Version ") + GetString(IDS_VERSION));
}

void queue::ClosePathUI()
{
	if(m_hPanel)
	{
		thequeueDesc.EndEditParams(m_ip, this, END_EDIT_REMOVEUI, NULL);
		m_creating = false;
		m_hPanel = NULL;
	}
}

void queue::SetSelectedPathNodeData()
{
	if(!m_pNodeEdited)
	{
		return;
	}

	IParamBlock2 *pNodeBlock2 = m_pNodeEdited->GetParamBlockByID(queuenode_params);

//	pNodeBlock2->SetValue(queuenode_length,0,mp_spinLength->GetFVal());
//	pNodeBlock2->SetValue(queuenode_width,0,mp_spinWidth->GetFVal());
//	pNodeBlock2->SetValue(queuenode_height,0,mp_spinHeight->GetFVal());

	m_ip->RedrawViews(m_ip->GetTime(), REDRAW_NORMAL, NULL);
}

void queue::UpdatePathNodeUI()
{
	int numSelected = 0;
	Object* pEdited = NULL;
	int iType = 0.0f;
//	float fWidth = 0.0f;
//	float fHeight = 0.0f;

	if(m_selLevel == 0)
		return;

	for(int i=0; i<MAX_NUM_queueNODE; i++)
	{
		if(m_nodeSelected[i] && m_pPathNodes[i])
		{
			numSelected++;
			pEdited = m_pPathNodes[i];
		}
	}

	if(numSelected != 1)
		pEdited = NULL;

	m_pNodeEdited = pEdited;

	if(m_pNodeEdited)
	{
		IParamBlock2 *pNodeBlock2 = m_pNodeEdited->GetParamBlockByID(queuenode_params);

		iType = pNodeBlock2->GetInt(queuenode_length);
	}

	HWND hDlgItem = GetDlgItem(m_hNodePanel,IDC_EDT_NAME);

	if(iType == 0)
	{
		SetWindowText(hDlgItem,"Queue");
	}
	else if(iType == 1)
	{
		SetWindowText(hDlgItem,"Direction");
	}
	else if(iType == 2)
	{
		SetWindowText(hDlgItem,"Use");
	}
	else if(iType == 3)
	{
		SetWindowText(hDlgItem,"Forward");
	}

	m_ip->RedrawViews(m_ip->GetTime(), REDRAW_NORMAL, this);
}

void queue::SelectSubComponent(HitRecord *hitRec, BOOL selected, BOOL all, BOOL invert)
{
	SetSelectedPathNodeData();

	while(hitRec)
	{
		if(selected)
			m_nodeSelected[hitRec->hitInfo] = true;
		else
			m_nodeSelected[hitRec->hitInfo] = false;

		if (all)
			hitRec = hitRec->Next();
		else
			break;
	}

	UpdatePathNodeUI();
	NotifyDependents(FOREVER, PART_SELECT, REFMSG_CHANGE);
}

void queue::ClearSelection(int selLevel)
{
	SetSelectedPathNodeData();

	if(selLevel)
	{
		for(int i=0; i<MAX_NUM_queueNODE; i++)
			m_nodeSelected[i] = false;

		UpdatePathNodeUI();
		NotifyDependents(FOREVER, PART_SELECT, REFMSG_CHANGE);
	}
}

void queue::InitPathNodeUI()
{
	m_hNodePanel = m_ip->AddRollupPage(	hInstance,
										MAKEINTRESOURCE(IDD_NODE_PANEL),
										EditqueueNodeDlgProc,
										_T("queueNode Parameters"),
										(LPARAM)this);

	SetDlgItemText(m_hNodePanel, IDC_VERSION, TSTR("Version ") + GetString(IDS_VERSION));

	UpdatePathNodeUI();
}

void queue::ClosePathNodeUI()
{
	if(m_hNodePanel)
	{
		m_ip->DeleteRollupPage(m_hNodePanel);

		m_hNodePanel = NULL;
		m_pNodeEdited = NULL;
	}
}

#if !defined( _WIN64 )
BOOL CALLBACK EditqueueNodeDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
#else
INT_PTR CALLBACK EditqueueNodeDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
#endif
{
	switch(msg)
	{
	case WM_INITDIALOG:
		SetDlgItemText(hWnd, IDC_VERSION, TSTR("Version ") + GetString(IDS_VERSION));
		break;
	case WM_CLOSE:
		break;
	default:
		queue::SetSelectedPathNodeData();
		return FALSE;
	}
	return TRUE;

}

