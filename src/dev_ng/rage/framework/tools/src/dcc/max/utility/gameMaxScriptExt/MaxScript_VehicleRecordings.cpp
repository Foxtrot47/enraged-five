
// 3dsmax SDK headers
#include <maxscript/maxscript.h>
#include <maxscript/foundation/numbers.h>
#include <maxscript/foundation/arrays.h>
#include <maxscript/foundation/strings.h>
#include <maxscript/foundation/name.h>
#include <maxscript/foundation/3dmath.h>
#include <maxscript/compiler/parser.h>
#include <maxscript/macros/define_instantiation_functions.h>

#include "basetypes.h"
#include "libwin/winfstream.h"


def_visible_primitive( load_rrr_file, "LoadRRRFile");
def_visible_primitive( save_rrr_file, "SaveRRRFile");


#define SPEEDMULT (32767.0f / 120.0f)	// Max speed = 120 m/s = 432km/h

class CVehicleStateEachFrameCompressed
{
public:
	UInt32	TimeInRecording;
	Int16	SpeedX, SpeedY, SpeedZ;
	Int8	Matrix_a_x, Matrix_a_y, Matrix_a_z;
	Int8	Matrix_b_x, Matrix_b_y, Matrix_b_z;
	Int8	SteerAngle, Gas, Brake, HandBrake;
	float	CoorsX, CoorsY, CoorsZ;

	float	GetSpeedX() { return(SpeedX / SPEEDMULT); };
	float	GetSpeedY() { return(SpeedY / SPEEDMULT); };
	float	GetSpeedZ() { return(SpeedZ / SPEEDMULT); };

	void	SetSpeedX(float Speed) { SpeedX = (Int16)(Speed * SPEEDMULT); };
	void	SetSpeedY(float Speed) { SpeedY = (Int16)(Speed * SPEEDMULT); };
	void	SetSpeedZ(float Speed) { SpeedZ = (Int16)(Speed * SPEEDMULT); };
};

class CVehicleStateEachFrame
{
public:
	UInt32	TimeInRecording;
	Matrix3 Trans;
	float	SpeedX, SpeedY, SpeedZ;
	Int8	SteerAngle, Gas, Brake, HandBrake;
};

////////////////////////////////////////////////////////////////////////////////////////////////
Value *load_rrr_file_cf(Value** arg_list, int count)
{
	const char* p_Filename = arg_list[0]->to_string();
	WinLib::WinFileStream rrrFile;

	if(rrrFile.open(p_Filename,"rb"))
	{
		UInt32 size = rrrFile.getPending();
		UInt32 numEntries = size / sizeof(CVehicleStateEachFrameCompressed);

		CVehicleStateEachFrameCompressed* p_framesComp = new CVehicleStateEachFrameCompressed[numEntries];

		rrrFile.read(p_framesComp,size);

		Array* p_rootList = new Array(numEntries);

		for(UInt32 i=0;i<numEntries;i++)
		{
			Array* p_itemList = new Array(9);
			
			p_itemList->append(new Integer(p_framesComp[i].TimeInRecording));
			p_itemList->append(new Float(p_framesComp[i].GetSpeedX()));
			p_itemList->append(new Float(p_framesComp[i].GetSpeedX()));
			p_itemList->append(new Float(p_framesComp[i].GetSpeedX()));
			p_itemList->append(new Float((1.0f / 20.0f) * p_framesComp[i].SteerAngle));
			p_itemList->append(new Float((1.0f / 100.0f) * p_framesComp[i].Gas));
			p_itemList->append(new Float((1.0f / 100.0f) * p_framesComp[i].Brake));
			p_itemList->append(new Integer(p_framesComp[i].HandBrake));

			Point3 FirstVec;
			Point3 SecondVec;
			Point3 ThirdVec;
			Point3 Trans;

			FirstVec.x = p_framesComp[i].Matrix_a_x / 127.0f;
			FirstVec.y = p_framesComp[i].Matrix_a_y / 127.0f;
			FirstVec.z = p_framesComp[i].Matrix_a_z / 127.0f;
			SecondVec.x = p_framesComp[i].Matrix_b_x / 127.0f;
			SecondVec.y = p_framesComp[i].Matrix_b_y / 127.0f;
			SecondVec.z = p_framesComp[i].Matrix_b_z / 127.0f;
			ThirdVec = CrossProd(FirstVec,SecondVec);
			ThirdVec.Normalize();
			Trans.x = p_framesComp[i].CoorsX;
			Trans.y = p_framesComp[i].CoorsY;
			Trans.z = p_framesComp[i].CoorsZ;

			p_itemList->append(new Matrix3Value(Matrix3(FirstVec,SecondVec,ThirdVec,Trans)));

			p_rootList->append(p_itemList);
		}

		delete[] p_framesComp;

		return p_rootList;
	}

	return &undefined;
}

////////////////////////////////////////////////////////////////////////////////////////////////
Value *save_rrr_file_cf(Value** arg_list, int count)
{
	const char* p_Filename = arg_list[0]->to_string();
	Array* p_Data = (Array*)arg_list[1];
	WinLib::WinFileStream rrrFile;

	if(rrrFile.open(p_Filename,"wtc"))
	{
		UInt32 numEntries = p_Data->size;
		CVehicleStateEachFrameCompressed* p_framesComp = new CVehicleStateEachFrameCompressed[numEntries];

		for(UInt32 i=0;i<numEntries;i++)
		{
			Array* p_ItemData = (Array*)((*p_Data)[i]);

			p_framesComp[i].TimeInRecording = (*p_ItemData)[0]->to_int();
			p_framesComp[i].SetSpeedX((*p_ItemData)[1]->to_float());
			p_framesComp[i].SetSpeedY((*p_ItemData)[2]->to_float());
			p_framesComp[i].SetSpeedZ((*p_ItemData)[3]->to_float());
			p_framesComp[i].SteerAngle = (*p_ItemData)[4]->to_float() * 20.0f;
			p_framesComp[i].Gas = (*p_ItemData)[5]->to_float() * 100.0f;
			p_framesComp[i].Brake = (*p_ItemData)[6]->to_float() * 100.0f;
			p_framesComp[i].HandBrake = (*p_ItemData)[7]->to_int();

			Matrix3 mat = (*p_ItemData)[8]->to_matrix3();
//			mat.Orthogonalize();

			p_framesComp[i].CoorsX = mat.GetTrans().x;
			p_framesComp[i].CoorsY = mat.GetTrans().y;
			p_framesComp[i].CoorsZ = mat.GetTrans().z;

			p_framesComp[i].Matrix_a_x = mat.GetRow(0)[0] * 127.0f;
			p_framesComp[i].Matrix_a_y = mat.GetRow(0)[1] * 127.0f;
			p_framesComp[i].Matrix_a_z = mat.GetRow(0)[2] * 127.0f;
			p_framesComp[i].Matrix_b_x = mat.GetRow(1)[0] * 127.0f;
			p_framesComp[i].Matrix_b_y = mat.GetRow(1)[1] * 127.0f;
			p_framesComp[i].Matrix_b_z = mat.GetRow(1)[2] * 127.0f;
		}

		rrrFile.write(p_framesComp,numEntries * sizeof(CVehicleStateEachFrameCompressed));

		delete[] p_framesComp;

		return &true_value;
	}

	return &false_value;
}
