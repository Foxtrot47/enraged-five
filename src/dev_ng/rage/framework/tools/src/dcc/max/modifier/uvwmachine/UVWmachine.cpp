/**********************************************************************
 *<
	FILE: UVWmachine.cpp

	DESCRIPTION:	Appwizard generated plugin

	CREATED BY: 

	HISTORY: 

 *>	Copyright (c) 1997, All Rights Reserved.
 **********************************************************************/

#include "UVWmachine.h"
#include "UVWData.h"
#include "UVWRestore.h"

#define UVWMACHINE_LOCAL_UVWFACE_CHUNK	0x4086
#define UVWMACHINE_LOCAL_UVW_CHUNK		0x4087
#define UVWMACHINE_LOCAL_MESHSEL_CHUNK	0x4088

//--- ClassDesc ------------------------------------------------------------------------

static UVWmachineClassDesc UVWmachineDesc;

ClassDesc* GetUVWmachineDesc() {return &UVWmachineDesc;}
void UVWmachineClassDesc::ResetClassParams (BOOL fileReset) {}


// selection info
const TCHAR *pSelTypeNames[] = {"Faces", "Vertices"};
const int32 pSelTypes[] = {IMESHSEL_OBJECT, IMESHSEL_FACE, IMESHSEL_VERTEX};

// static member variables
UVWmachine *UVWmachine::m_pCurrentModifier = NULL;

//--- UVWmachine -------------------------------------------------------

UVWmachine::UVWmachine()
{
	SetSelLevel(1);
	m_ui.SetModifier(this);
}

UVWmachine::~UVWmachine()
{
}

//
//        name: **UVWmachine::GetSelTypeNames,GetSelTypes, GetNumSelTypes
// description: Selection type functions overidden from StdMeshSelectModifier
//          in: 
//         out: 
//
const char **UVWmachine::GetSelTypeNames()
{
	return pSelTypeNames;
}
const int32 *UVWmachine::GetSelTypes()
{
	return pSelTypes;
}
int32 UVWmachine::GetNumSelTypes()
{
	return 2;
}

//
//        name: NumSubObjTypes
// description: New sub object handling
//
int UVWmachine::NumSubObjTypes() 
{
	return 2;
}
//
//        name: GetSubObjType
// description: New sub object handling
//
ISubObjType *UVWmachine::GetSubObjType(int i)
{
	static GenSubObjType subObjTypes[] = {
		GenSubObjType("Faces", "SubObjectIcons", 2),
		GenSubObjType("Vertices", "SubObjectIcons", 0)
	};
	if(i == -1)
		return NULL;
	return &subObjTypes[i];
}

//
//        name: UVWmachine::SetFaceSelLevel,SetSelLevel,LocalDataChanged
// description: These are all overriden from StdMeshSelectModifier so that they can also call
//				
//          in: 
//         out: 
//
void UVWmachine::SetFaceSelLevel(uint32 level)
{
	StdMeshSelectModifier::SetFaceSelLevel(level);
	DestroyExtractedArrays();
}
void UVWmachine::SetSelLevel(DWORD level)
{
	StdMeshSelectModifier::SetSelLevel(level);
	DestroyExtractedArrays();
}
void UVWmachine::LocalDataChanged()
{
	StdMeshSelectModifier::LocalDataChanged();
	DestroyExtractedArrays();
}

//
//        name: UVWmachine::LocalValidity
// description: Return the local validity interval of the modifier
//          in: t = time
//         out: validity interval
//
Interval UVWmachine::LocalValidity(TimeValue t)
{
	// if being edited, return NEVER forces a cache to be built 
	// after previous modifier.
	if (TestAFlag(A_MOD_BEING_EDITED))
		return NEVER;  
	return FOREVER;
}

RefTargetHandle UVWmachine::Clone(RemapDir& remap)
{
	UVWmachine* pNewMod = new UVWmachine();	

	pNewMod->m_selLevel = m_selLevel;
	
	BaseClone(this, pNewMod,remap);

	return(pNewMod);
}

void UVWmachine::ModifyObject(TimeValue t, ModContext &mc, ObjectState * os, INode *node) 
{
	Interval valid = LocalValidity(t);

	assert(os->obj->IsSubClassOf(triObjectClassID));
	// Get TriObject version of ObjectState
	TriObject *tobj = (TriObject*)os->obj;
	Mesh &mesh = tobj->mesh;
	bool topologyChanged = false;

	// apply UVW map if it doesn't exist
	if(mesh.tvFace == NULL || mesh.tVerts == NULL)
	{
		mesh.ApplyUVWMap(MAP_FACE, 1,1,1, 0,0,0, 0, Matrix3(1));
		topologyChanged = true;
	}

	// get modContext if it doesn't exist then create a new mod context
	UVWData *pSelData  = (UVWData*)mc.localData;
	if (!pSelData) 
		mc.localData = pSelData = new UVWData(mesh);

	// if the modContext is not valid then pass it a new mesh
	if(!pSelData->GetMesh())
	{
		pSelData->SetCache(mesh);
	}

	ModifyMesh(mesh, (UVWData*)mc.localData);


	//
	// copy vertex UVWs across
	//
	int32 i;

	if(pSelData->GetMesh())
	{
		// number of faces can't change
		assert(pSelData->GetUVWFaces().size() == mesh.numFaces);
		// if number of vertices are different then re-allocate number of texture coordinate vertices
		if(pSelData->GetUVW().size() != mesh.numTVerts)
		{
			mesh.setNumTVerts(pSelData->GetUVW().size());
			topologyChanged = true;
		}


		// copy across arrays
		for(i=0; i<pSelData->GetUVW().size(); i++)
			*(mesh.tVerts+i) = (pSelData->GetUVW())[i];
		for(i=0; i<pSelData->GetUVWFaces().size(); i++)
			*(mesh.tvFace+i) = (pSelData->GetUVWFaces())[i];
	}

	if(topologyChanged)
		NotifyDependents(Interval(t,t), PART_TOPO, REFMSG_CHANGE);
	NotifyDependents(Interval(t,t), PART_TEXMAP, REFMSG_CHANGE);
	tobj->UpdateValidity(SELECT_CHAN_NUM,valid);
	tobj->UpdateValidity(TOPO_CHAN_NUM,valid);
	tobj->UpdateValidity(TEXMAP_CHAN_NUM,valid);
	tobj->UpdateValidity(SUBSEL_TYPE_CHAN_NUM,FOREVER);
	tobj->PointsWereChanged();

}

void UVWmachine::BeginEditParams( IObjParam *ip, ULONG flags,Animatable *prev )
{
	m_pCurrentModifier = this;

	m_ui.SetInterface(ip);
	SetupSubObjEdit(ip);
}

void UVWmachine::EndEditParams( IObjParam *ip, ULONG flags,Animatable *next)
{
	m_ui.DeleteRollupPage();
	ClearSubObjEdit();

	m_ui.SetInterface(NULL);
	m_pCurrentModifier = NULL;
}

//
//        name: UVWmachine::ActivateSubobjSel
// description: Setup rollup page and call MeshSelect setup
//          in: 
//         out: 
//
void UVWmachine::ActivateSubobjSel(int level, XFormModes& modes)
{
	StdMeshSelectModifier::ActivateSubobjSel(level, modes);
	m_ui.AddRollupPage();
}

//From ReferenceMaker 
RefResult UVWmachine::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget,	PartID& partID,  RefMessage message) 
{
	return REF_SUCCEED;
}

//
// IO
//
IOResult UVWmachine::Load(ILoad *iload)
{
	Modifier::Load(iload);
	return IO_OK;
}
IOResult UVWmachine::Save(ISave *isave)
{
	Modifier::Save(isave);
	return IO_OK;
}
IOResult UVWmachine::LoadLocalData(ILoad *iload, LocalModData **ppModData)
{
	IOResult res;
	UVWData *pData = new UVWData;
	*ppModData = pData;

	while (IO_OK == (res = iload->OpenChunk())) {
		switch(iload->CurChunkID())  
		{
		case UVWMACHINE_LOCAL_UVWFACE_CHUNK:
			{
				unsigned long len, chunkLen;
				TVFace uvFace;

				// get number of colours and add them to list
				chunkLen = iload->CurChunkLength() / sizeof(TVFace);
				while(chunkLen--)
				{
					res = iload->Read(&uvFace, sizeof(TVFace), &len);
					pData->GetUVWFaces().push_back(uvFace);
				}
			}
			break;
		case UVWMACHINE_LOCAL_UVW_CHUNK:
			{
				unsigned long len, chunkLen;
				UVVert uv;

				// get number of colours and add them to list
				chunkLen = iload->CurChunkLength() / sizeof(UVVert);
				while(chunkLen--)
				{
					res = iload->Read(&uv, sizeof(UVVert), &len);
					pData->GetUVW().push_back(uv);
				}
			}
			break;
		case UVWMACHINE_LOCAL_MESHSEL_CHUNK:
			StdMeshSelectModifier::LoadLocalData(iload, ppModData);
			break;
		default:
			break;
		}
		iload->CloseChunk();
		if (res != IO_OK) 
			return res;
	}

	return IO_OK;
}
IOResult UVWmachine::SaveLocalData(ISave *isave, LocalModData *pModData)
{
	UVWData *pData = (UVWData*)pModData;

	if(pData)
	{
		unsigned long len;
		TVFace uvFace;
		UVVert uv;
		int32 i;

		isave->BeginChunk(UVWMACHINE_LOCAL_UVWFACE_CHUNK);
		// write every uv face
		for(i=0; i<pData->GetUVWFaces().size(); i++)
		{
			uvFace = (pData->GetUVWFaces())[i];
			isave->Write(&uvFace, sizeof(TVFace), &len);
		}
		isave->EndChunk();

		isave->BeginChunk(UVWMACHINE_LOCAL_UVW_CHUNK);
		// write every uv
		for(i=0; i<pData->GetUVW().size(); i++)
		{
			uv = (pData->GetUVW())[i];
			isave->Write(&uv, sizeof(UVVert), &len);
		}
		isave->EndChunk();

		isave->BeginChunk(UVWMACHINE_LOCAL_MESHSEL_CHUNK);
		StdMeshSelectModifier::SaveLocalData(isave, pModData);
		isave->EndChunk();
	}
	return IO_OK;
}

BOOL UVWEnumProc::proc(ModContext *mc)
{
	if(mc->localData == NULL)
		return TRUE;

	((UVWData *)mc->localData)->SetValuesPtr(m_pValues);

	if(m_operation == DESTROYEXTRACTED)
		((UVWData *)mc->localData)->DestroyExtractedArrays();

	if(m_select == IMESHSEL_FACE)
	{
		switch(m_operation)
		{
		case ROTATE:
			((UVWData *)mc->localData)->RotateTextureCoords(m_faceSel);
			break;
		case ROTATE90:
			((UVWData *)mc->localData)->ProcessFaces(&UVWData::Rotate90);
			break;
		case FLIPU:
			((UVWData *)mc->localData)->ProcessFaces(&UVWData::FlipU);
			break;
		case FLIPV:
			((UVWData *)mc->localData)->ProcessFaces(&UVWData::FlipV);
			break;
		case EXPLICITSETU:
			((UVWData *)mc->localData)->ExplicitSet(m_faceSel, m_index, 0);
			break;
		case EXPLICITSETV:
			((UVWData *)mc->localData)->ExplicitSet(m_faceSel, m_index, 1);
			break;
		case MOUSEROTATE:
			((UVWData *)mc->localData)->ProcessFaces(&UVWData::Rotate);
			break;
		case MOUSESCALE:
			((UVWData *)mc->localData)->ProcessFaces(&UVWData::Scale);
			break;
		case MOUSETRANSLATE:
			((UVWData *)mc->localData)->ProcessFaces(&UVWData::Translate);
			break;
		case MOUSESCALEU:
			((UVWData *)mc->localData)->ProcessFaces(&UVWData::ScaleU);
			break;
		case MOUSETRANSLATEU:
			((UVWData *)mc->localData)->ProcessFaces(&UVWData::TranslateU);
			break;
		case MOUSESCALEV:
			((UVWData *)mc->localData)->ProcessFaces(&UVWData::ScaleV);
			break;
		case MOUSETRANSLATEV:
			((UVWData *)mc->localData)->ProcessFaces(&UVWData::TranslateV);
			break;
		default:
			break;
		}
	}
	else if(m_select == IMESHSEL_VERTEX)
	{
		switch(m_operation)
		{
		case EXPLICITSETU:
			((UVWData *)mc->localData)->ProcessVertices(&UVWData::SetU);
			break;
		case EXPLICITSETV:
			((UVWData *)mc->localData)->ProcessVertices(&UVWData::SetV);
			break;
		case MOUSETRANSLATE:
			((UVWData *)mc->localData)->ProcessVertices(&UVWData::Translate);
			break;
		}
	}
	return TRUE;
}


//
//        name: UVWmachine::DestroyExtractedArrays
// description: Invalidates all the extracted vertex/face arrays in all the modcontexts
//
void UVWmachine::DestroyExtractedArrays(void)
{
	UVWEnumProc proc(UVWEnumProc::DESTROYEXTRACTED);
	EnumModContexts(&proc);
}

//
//        name: UVWmachine::ProcessTextureCoords
// description: Process vertex coordinates
//          in: proc = reference to enum proc
//				pHoldName = name of backup. If this is null don't backup
//         out: 
//
void UVWmachine::ProcessTextureCoords(UVWEnumProc &proc, char *pHoldName)
{
	proc.SetFaceSelection(m_faceSel);
	proc.SetSelection(GetSelTypes()[GetSelLevel()]);

	if(pHoldName != NULL)
		theHold.Begin();

	EnumModContexts(&proc);
	NotifyDependents(FOREVER, PART_TEXMAP, REFMSG_CHANGE);
	m_ip->RedrawViews(m_ip->GetTime());

	if(pHoldName != NULL)
		theHold.Accept(pHoldName);

}

//
//        name: UVWmachine::StartMouseOperation
// description: Start up a mouse rotate/scale command mode
//          in: oper = id of operation
//
void UVWmachine::StartMouseOperation(MouseOper oper)
{
	m_ip->DeleteMode(&theUVWCMode);
	theUVWCMode.SetInterface(m_ip);
	theUVWCMode.SetModifier(this);
	theUVWCMode.SetOperation(oper);
	m_ip->SetCommandMode(&theUVWCMode);
}

void UVWmachine::EndMouseOperation(void)
{
	m_ip->DeleteMode(&theUVWCMode);

}


//
//        name: UVWmachine::ClearMouseButtons
// description: sets the mouse operation button to off
//          in: oper = id of operation
//
void UVWmachine::ClearMouseButtons(MouseOper oper)
{
	switch(oper)
	{
	case MOUSE_ROTATE:
		m_ui.GetRotateButton()->SetCheck(FALSE);
		break;
	case MOUSE_SCALE:
		m_ui.GetScaleButton()->SetCheck(FALSE);
		break;
	case MOUSE_TRANSLATE:
		m_ui.GetTranslateButton()->SetCheck(FALSE);
		break;
	case MOUSE_SCALEU:
		m_ui.GetScaleButtonU()->SetCheck(FALSE);
		break;
	case MOUSE_TRANSLATEU:
		m_ui.GetTranslateButtonU()->SetCheck(FALSE);
		break;
	case MOUSE_SCALEV:
		m_ui.GetScaleButtonV()->SetCheck(FALSE);
		break;
	case MOUSE_TRANSLATEV:
		m_ui.GetTranslateButtonV()->SetCheck(FALSE);
		break;
	}
}