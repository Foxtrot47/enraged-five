//
// filename:	IScrollbars.h
// author:		David Muir
// date:		23 January 2007
// description:	Interface header for Scrollbars objects
//

#ifndef INC_ISCROLLBARS_H_
#define INC_ISCROLLBARS_H_

// --- Defines ------------------------------------------------------------------
#define SCROLLBARS_CLASSID					Class_ID( 0x1dd821aa, 0x34b25954 )
#define SCROLLBARS_POINTS_CLASSID			Class_ID( 0x26316bdd, 0x36841438 )

#define SCROLLBARS_PBLOCK_REF_ID			(0)

// --- Structure/Class Definitions ----------------------------------------------

// Scrollbars Parameter Block IDs
enum 
{
	scrollbars_params
};

// Scrollbars Parameters IDs
enum
{
	scrollbars_type,
	scrollbars_height,
	scrollbars_numpoints
};

#endif // !INC_ISCROLLBARS_H_

// End of file
