

#include "clipImporter.h"

#define clipImporter_CLASS_ID	Class_ID(0xcadb6648, 0x63c0e1e6)

clipImporter g_clipImporter;

class clipImporterClassDesc : public ClassDesc2 
{
public:
	virtual int IsPublic() 							{ return TRUE; }
	virtual void* Create(BOOL /*loading = FALSE*/) 		{ return &clipImporter::GetInst(); }
	virtual const TCHAR *	ClassName() 			{ return GetString(IDS_CLASS_NAME); }
	virtual SClass_ID SuperClassID() 				{ return GUP_CLASS_ID; }
	virtual Class_ID ClassID() 						{ return clipImporter_CLASS_ID; }
	virtual const TCHAR* Category() 				{ return GetString(IDS_CATEGORY); }

	virtual const TCHAR* InternalName() 			{ return _T("clipImporter"); }	// returns fixed parsable name (scripter-visible name)
	virtual HINSTANCE HInstance() 					{ return hInstance; }					// returns owning module handle
	

};


bool clipImporter::initialised = false;

static clipImporterClassDesc clipImporterDesc;


ClassDesc2* GetclipImporterDesc() { return &clipImporterDesc; }




clipImporter::clipImporter()
{
	
	initialised = false;
}

clipImporter::~clipImporter()
{
	
}

// Activate and Stay Resident
DWORD clipImporter::Start( ) {
	
	#pragma message(TODO("Do plugin initialization here"))
	
	#pragma message(TODO("Return if you want remain loaded or not"))
	return GUPRESULT_KEEP;
}

void clipImporter::Stop( ) {
	#pragma message(TODO("Do plugin un-initialization here"))
}

DWORD_PTR clipImporter::Control( DWORD parameter ) {
	return 0;
}

IOResult clipImporter::Save(ISave *isave)
{
	return IO_OK;
}

IOResult clipImporter::Load(ILoad *iload)
{
	return IO_OK;
}




clipImporter& clipImporter::GetInst()
{
	return g_clipImporter;
}


void clipImporter::InitClass()
{
	initialised = true;
}

void clipImporter::ShutdownClass()
{
 
}

ClipObject* clipImporter::GetClipObject()
{
	return &m_clipObject;
}

void clipImporter::DeleteClipObject()
{
	m_clipObject.Reset();
}


