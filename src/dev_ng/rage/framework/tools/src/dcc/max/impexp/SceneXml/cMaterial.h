//
// filename:	cMaterial.h
// description:
//

#ifndef INC_CMATERIAL_H_
#define INC_CMATERIAL_H_

// --- Include Files ------------------------------------------------------------
#include <Windows.h>

// STL headers
#include <map>
#include <string>
#include <vector>

// Win32HelperLib headers
#include "Win32HelperLib/Guid.h"
#include "cTexture.h"

// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------

// --- Forward Declarations -----------------------------------------------------

class TiXmlElement;

// --- Structure/Class Definitions ----------------------------------------------

class cMaterial;

//! Material list type definition, dictionary mapping material name to
//! a cMaterial object.
typedef std::map<std::string, cMaterial>					tMaterialList;
//! Iterator type definition.
typedef std::map<std::string, cMaterial>::iterator			itMaterialList;
//! Const-iterator type definition.
typedef std::map<std::string, cMaterial>::const_iterator	itMaterialListConst;

/**
 * @brief Material information.
 */
class cMaterial
{
public:
	cMaterial( );
	cMaterial( Mtl* material );
	virtual ~cMaterial( );

	TiXmlElement* ToXmlElement( const std::string& export_as ) const;

	const cGuid& RetID( ) const { return m_guidID; }
	const std::string& RetFullname( ) const { return m_sFullname; }
	const std::string& RetName( ) const { return m_sName; }
	const std::string& RetType( ) const { return m_sType; }
	const std::string& RetPreset( ) const { return m_sPreset; }

	void PopulateMaterialMapList( Mtl* pMaterial );
	void AddChildMaterial( const cMaterial& material );

protected:
	cGuid						m_guidID;			//!< Unique identifier
	std::string					m_sFullname;		//!< Material 3dsmax fullname
	std::string					m_sName;			//!< Material name
	std::string					m_sType;			//!< Material type (Multi, Standard, Rage Shader)
	std::string					m_sPreset;			//!< Rage shader preset name "" if not a rage shader
	std::map<std::string, cTexture> m_mTextures;	//!< Used textures
	tMaterialList				m_mChildren;		//!< Child materials
	bool						m_bRageShader;		//!< Flag is set if the material represents a rage material

	static std::string			m_sTextureTypes[12];	//!< An array of strings that match up to the texmap types
};

// --- Globals ------------------------------------------------------------------

#endif // !INC_CMATERIAL_H_