//
// filename:	Scrollbars.cpp
// author:		David Muir
// date:		23 January 2007
// description:	Scrollbars Object and associated descriptor class implementation
//

// --- Include Files ------------------------------------------------------------
#include "Scrollbars.h"

// STL headers
#include <vector>

// 3D Studio Max SDK headers
#include "Max.h"
#include "istdplug.h"
#include "iparamm2.h"

// Local headers
#include "ScrollbarsPoint.h"
#include "ScrollbarsCommandMode.h"
#include "IScrollbars.h"
#include "Win32Res.h"
#include "resource.h"

// --- Static Globals -----------------------------------------------------------
static CScrollbarsDesc g_ScrollbarsObjectDescriptor;

class cNumPointPBAccessor : public PBAccessor
{
public:
	void Set( PB2Value& v, ReferenceMaker* owner, 
		ParamID id, int tabIndex, TimeValue t )
	{
		CScrollbars* pScrollbars = (CScrollbars*)owner;
		int nActualNumPoints = 0;

		switch (id)
		{
		case scrollbars_numpoints:

			// Get actual number of points
			nActualNumPoints = pScrollbars->GetNumPoints();

			// Only allow decreasing the number of points
			if ( v.i > nActualNumPoints )
			{
				pScrollbars->GetParamBlock(0)->SetValue( scrollbars_numpoints, t, nActualNumPoints );
				MessageBox( NULL, 
							_T("Cannot set the visible number of points greater than the number of points defined."), 
							_T("Scrollbars Plugin"), MB_OK );
			}
			break;
		}
	}
};
static cNumPointPBAccessor numPointAccessor;

// Parameter Block Descriptor
static ParamBlockDesc2 scrollbarsDesc( scrollbars_params, _T("Scrollbars Parameters"),
								IDS_SCROLLBARS_CLASSNAME, GetScrollbarsDesc(),
								P_AUTO_CONSTRUCT | P_AUTO_UI, SCROLLBARS_PBLOCK_REF_ID,
								IDD_SCROLLBARS_PANEL, IDS_PARAMS, 0, 0, 
								NULL,

								// Height Parameter
								scrollbars_height, _T("Height"), TYPE_WORLD, 0, IDS_HEIGHT,
								p_default, 1.0f,
								p_range, 0.0f, float(1.0E30),
								p_ui, TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_SCROLLBARS_HEIGHT_EDIT, IDC_SCROLLBARS_HEIGHT_SPIN, 0.01f,
								end,

								// Type Parameter
								scrollbars_type, _T("Type"), TYPE_INT, 0, IDS_TYPE,
								p_default, 0,
								p_range, 0, 1,
								p_ui, TYPE_INTLISTBOX, IDC_SCROLLBARS_TYPE_COMBO, 
									  8,					// Item Count (UPDATE THIS IF LIST BELOW CHANGES)
															//   Damn, must change this to use attributes!
									  IDS_TYPE_FINANCIAL,	// Item Index 0: "Financial"
									  IDS_TYPE_THEATRE,		// Item Index 1: "Theatre"
									  IDS_TYPE_ADVERT,		// Item Index 2: "Advertisement"
									  IDS_TYPE_CLOCK,		// Item Index 3: "Clock"
									  IDS_TYPE_URLS,		// Item Index 4: "URLs"
									  IDS_TYPE_COMEDYCLUB,	// Item Index 5: "Comedy Club"
									  IDS_TYPE_TRAFFIC,		// Item Index 6: "Traffic"
									  IDS_TYPE_NEWS,		// Item Index 7: "News"
								end,

								// Number of Points Parameter
								scrollbars_numpoints, _T("NumPoints"), TYPE_INT, 0, IDS_NUMPOINTS,
								p_default, sC_cnt_MaxNumberOfPoints,
								p_range, 2, sC_cnt_MaxNumberOfPoints,
								p_ui, TYPE_SPINNER, EDITTYPE_INT, IDC_SCROLLBARS_NUMPOINTS_EDIT, IDC_SCROLLBARS_NUMPOINTS_SPIN, SPIN_AUTOSCALE,
								p_accessor, &numPointAccessor,
								end,

								end );

// --- Globals ------------------------------------------------------------------
extern CScrollbarsCommandMode g_ScrollbarsCommandMode;
#if !defined( _WIN64 )
BOOL CALLBACK EditPointDlgProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam );
#else
INT_PTR CALLBACK EditPointDlgProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam );
#endif 

// --- Class Static Initialisation ----------------------------------------------
HWND				CScrollbars::m_hScrollbarUI	= 0;
HWND				CScrollbars::m_hPointUI		= 0;	
IObjParam*			CScrollbars::m_pInterface	= NULL;
Object*				CScrollbars::m_pPointEdited = NULL;
bool				CScrollbars::m_bCreating	= false;
MoveModBoxCMode*	CScrollbars::m_pMoveMode	= NULL;
SelectModBoxCMode*	CScrollbars::m_pSelectMode	= NULL;


// --- Code ---------------------------------------------------------------------

CScrollbars::CScrollbars( )
	: m_pParamBlock( NULL ),
	m_SelLevel( 0 )
{
	for ( size_t nIndex = 0; nIndex < sC_cnt_MaxNumberOfPoints; ++nIndex )
	{
		m_vpPoints[ nIndex ] = NULL;
		m_vpControls[ nIndex ] = NULL;
		m_vbSelected[ nIndex ] = false;
	}

	GetScrollbarsDesc()->MakeAutoParamBlocks( this );
}

CScrollbars::~CScrollbars( )
{
	for ( size_t nIndex = 0; nIndex < sC_cnt_MaxNumberOfPoints; ++nIndex )
	{
		m_vpPoints[ nIndex ] = NULL;
		m_vpControls[ nIndex ] = NULL;
		m_vbSelected[ nIndex ] = NULL;
	}

	m_pParamBlock = NULL;
}

// ------------------------------------------------------------------------------
// Sub-Object Methods
// ------------------------------------------------------------------------------

void				
CScrollbars::Move( TimeValue t, Matrix3& partm, Matrix3& tmAxis, 
				   Point3& val, BOOL bLocalOrigin )
{
	for ( s32 i = 0; i<sC_cnt_MaxNumberOfPoints; ++i )
	{
		if ( m_vbSelected[i] && m_vpControls[i] )
		{
			SetXFormPacket pckt(val,partm,tmAxis);
			m_vpControls[i]->SetValue(t,&pckt,TRUE,CTRL_RELATIVE);
		}
	}
	NotifyDependents( FOREVER, PART_GEOM, REFMSG_CHANGE );
}

int					
CScrollbars::HitTest( TimeValue t, INode* pNode, int type, int crossing, 
					  int flags, IPoint2* p, ViewExp* pVpt, ModContext* pMc )
{
	int res = FALSE;

	for(s32 i=0; i<sC_cnt_MaxNumberOfPoints; i++)
	{
		if(m_vpPoints[i] &&
			!(flags&HIT_SELONLY && m_vbSelected[i] == false) &&
			!(flags&HIT_UNSELONLY && m_vbSelected[i] == true) )
		{
			assert( m_vpControls[i] );

			Matrix3 tm(1);
			Interval ivalid;

			m_vpControls[i]->GetValue(t, &tm, ivalid, CTRL_RELATIVE);

			INodeTransformed node(pNode, tm);

			if(m_vpPoints[i]->HitTest(t, &node, type, crossing, flags, p, pVpt)) {
				pVpt->LogHit(pNode,pMc,0,i,NULL);
				res = TRUE;
				if (flags & HIT_ABORTONHIT)
					return TRUE;
			}
		}
	}

	return res;
}

void				
CScrollbars::SelectSubComponent( HitRecord* pHitRec, BOOL bSelected, 
								 BOOL bAll, BOOL bInvert )
{
	SetSelectedPointData();

	while ( pHitRec )
	{
		if ( bSelected )
			m_vbSelected[pHitRec->hitInfo] = true;
		else
			m_vbSelected[pHitRec->hitInfo] = false;

		if ( bAll )
			pHitRec = pHitRec->Next();
		else
			break;
	}

	NotifyDependents( FOREVER, PART_SELECT, REFMSG_CHANGE );
}

void				
CScrollbars::ClearSelection( int nSelLevel )
{
	SetSelectedPointData( );

	if( nSelLevel )
	{
		for(s32 i=0; i<sC_cnt_MaxNumberOfPoints; ++i)
			m_vbSelected[i] = false;

		NotifyDependents(FOREVER, PART_SELECT, REFMSG_CHANGE);
	}
}

int					
CScrollbars::SubObjectIndex( HitRecord* pHitRec )
{
	return pHitRec->hitInfo;
}

void				
CScrollbars::GetSubObjectCenters( SubObjAxisCallback* pCB, TimeValue t, 
								  INode* pNode, ModContext* pMc )
{
	for(s32 i=0; i<sC_cnt_MaxNumberOfPoints; i++)
	{
		if ( m_vbSelected[i] && m_vpControls[i] ) 
		{
			Matrix3 tm(1);
			Interval ivalid;

			m_vpControls[i]->GetValue(t, &tm, ivalid, CTRL_RELATIVE);
			tm *= pNode->GetObjectTM(t);
			pCB->Center(tm.GetTrans(),i);
		}
	}
}

void				
CScrollbars::GetSubObjectTMs( SubObjAxisCallback* pCB, TimeValue t, 
							  INode* pNode, ModContext* pMc )
{
	for ( s32 i=0; i<sC_cnt_MaxNumberOfPoints; ++i )
	{
		if ( m_vbSelected[i] && m_vpControls[i] ) 
		{
			Matrix3 tm(1);
			Interval ivalid;

			m_vpControls[i]->GetValue(t, &tm, ivalid, CTRL_RELATIVE);
			tm *= pNode->GetObjectTM(t);
			pCB->TM(tm,i);
		}
	}
}

void				
CScrollbars::ActivateSubobjSel( int level, XFormModes& modes )
{
	if ( level ) 
	{
		modes = XFormModes( m_pMoveMode, NULL, NULL, NULL, NULL, m_pSelectMode );
		NotifyDependents( FOREVER, PART_SUBSEL_TYPE|PART_DISPLAY, REFMSG_CHANGE );
		m_pInterface->PipeSelLevelChanged();
	}

	if( m_SelLevel != level )
	{
		if( level )
		{
			CloseScrollbarsUI( );
			InitScrollbarsPointUI( );
		}
		else
		{
			CloseScrollbarsPointUI( );
			InitScrollbarsUI( );
		}

	}
	m_SelLevel = level;

	if ( 0 == m_SelLevel )
		UpdateScrollbarsUI( );
}

void 
CScrollbars::SetSelectedPointData( )
{
	if ( !m_pPointEdited )
	{
		return;
	}

	m_pInterface->RedrawViews( m_pInterface->GetTime(), REDRAW_NORMAL, NULL );
}

// ------------------------------------------------------------------------------
// Sub-Anims
// ------------------------------------------------------------------------------

int					
CScrollbars::NumSubs( )
{
	return GetNumPoints( );
}

Animatable*			
CScrollbars::SubAnim( int nIndex )
{
	assert( nIndex >= 0 );
	assert( nIndex < sC_cnt_MaxNumberOfPoints );
	assert( m_vpPoints[nIndex] );

	return m_vpPoints[nIndex];
}

TSTR				
CScrollbars::SubAnimName( int nIndex )
{
	assert( nIndex >= 0 );
	assert( nIndex < sC_cnt_MaxNumberOfPoints );
	assert( m_vpPoints[nIndex] );

	TSTR name;
	name.printf( "Point%2d", nIndex+1 );
	return name;
}

// ------------------------------------------------------------------------------
// Animatable Super-Class Overridden Methods
// ------------------------------------------------------------------------------

int				
CScrollbars::NumParamBlocks( ) 
{
	return 1;
}

IParamBlock2*	
CScrollbars::GetParamBlock( int nIndex ) 
{
	assert( 0 == nIndex ); 
	switch ( nIndex )
	{
	case 0:
		return m_pParamBlock;
	default:
		return NULL;
	}
}

IParamBlock2*
CScrollbars::GetParamBlockByID( short id ) 
{
	assert( scrollbars_params == id ); 
	switch ( id )
	{
	case scrollbars_params:
		return m_pParamBlock;
	default:
		return NULL;
	}
}

Class_ID		
CScrollbars::ClassID( ) 
{
	return GetScrollbarsDesc()->ClassID();
}

void			
CScrollbars::GetClassName( TSTR& sClassName ) 
{
	sClassName = GetStringResource( g_hInstance, IDS_SCROLLBARS_CLASSNAME );
}

void			
CScrollbars::BeginEditParams( IObjParam* ip, ULONG flags, Animatable *pPrev )
{
	assert( ip );
	m_pInterface = ip;

	if ( m_SelLevel )
		InitScrollbarsPointUI( );
	else
		InitScrollbarsUI( );

	if ( flags & BEGIN_EDIT_CREATE )
	{
		m_bCreating = true;
	}
	else
	{
		m_bCreating = false;
		
		// Create sub-object level editing modes
		m_pMoveMode = new MoveModBoxCMode( this, ip );
		m_pSelectMode = new SelectModBoxCMode( this, ip );

		// Add our sub-object type
		ip->SetSubObjectLevel( m_SelLevel );
	}
}

void			
CScrollbars::EndEditParams( IObjParam* ip, ULONG flags, Animatable *pNext )
{
	assert( ip );

	if ( !m_bCreating )
	{
		ip->DeleteMode( m_pMoveMode );
		ip->DeleteMode( m_pSelectMode );
		if ( m_pMoveMode )
			delete m_pMoveMode;
		m_pMoveMode = NULL;

		if ( m_pSelectMode )
			delete m_pSelectMode;
		m_pSelectMode = NULL;
	}

	CloseScrollbarsUI( );
	CloseScrollbarsPointUI( );

	m_pInterface = NULL;
}

// ------------------------------------------------------------------------------
// Rendering Methods
// ------------------------------------------------------------------------------

void			
CScrollbars::Draw( TimeValue t, INode* pNode, ViewExp* pVpt, int flags )
{
	assert( m_vpControls[0] );
	if ( !m_vpControls[0] )
		return;

	GraphicsWindow* pGW = pVpt->getGW( );
	Matrix3 objMtx = pNode->GetObjectTM(t);

	// Upper and Lower Outline Points for line drawing
	std::vector<Point3> vUpperOutline;
	std::vector<Point3> vLowerOutline;
	// Upper/Lower connection vertices
	std::vector<Point3> vConnOutline;

	vUpperOutline.clear( );
	vLowerOutline.clear( );
	vConnOutline.clear( );

	Matrix3 matHeightTranslation(1);
	Matrix3 matPrevious(1);
	Interval ivalid;

	m_vpControls[0]->GetValue(t, &matPrevious, ivalid, CTRL_RELATIVE);
	pGW->setTransform(objMtx);
	Point3 vZero( 0.0f, 0.0f, 0.0f );
	pGW->setColor( TEXT_COLOR, 1.0f, 1.0f, 1.0f );
	pGW->text( &vZero, _T( "Scrollbars") );

	float fHeight = 1.0f;
	int   nVisiblePoints = sC_cnt_MaxNumberOfPoints;
	IParamBlock2* pParamBlock = GetParamBlock( SCROLLBARS_PBLOCK_REF_ID );
	assert( pParamBlock );
	if ( !pParamBlock )
		return;

	pParamBlock->GetValue( scrollbars_height, t, fHeight, FOREVER );
	pParamBlock->GetValue( scrollbars_numpoints, t, nVisiblePoints, FOREVER );
	matHeightTranslation.Translate( Point3( 0.0f, 0.0f, -fHeight ) );

	for ( int i=1; i<nVisiblePoints; i++ )
	{
		if( !m_vpControls[i] )
			continue;

		Matrix3 matCurrent(1);
		Interval ivalid;
		m_vpControls[i]->GetValue(t, &matCurrent, ivalid, CTRL_RELATIVE);

		// Upper outline line
		vUpperOutline.push_back( matPrevious.GetTrans() );
		vUpperOutline.push_back( matCurrent.GetTrans() );
		// Lower outline line
		vLowerOutline.push_back( matHeightTranslation * matPrevious.GetTrans() );
		vLowerOutline.push_back( matHeightTranslation * matCurrent.GetTrans() );

		matPrevious = matCurrent;
	}

	// Check if we have some point sub-objects to draw
	if ( ( 0 == vUpperOutline.size() ) || ( 0 == vLowerOutline.size() ) )
		return;

	DrawLineProc lineProc( pGW );
	lineProc.SetLineColor( 1.0f, 1.0f, 1.0f );

	// This does bad stuff with larger numbers of points...
	// STL guarantees contiguous??!
	//	lineProc.proc( &(vUpperOutline[0]), vUpperOutline.size() );
	//	lineProc.proc( &(vLowerOutline[0]), vLowerOutline.size() );

	// Upper outline
	for ( std::vector<Point3>::iterator itPoint = vUpperOutline.begin();
		  itPoint != ( vUpperOutline.end() - 1 );
		  ++itPoint )
	{
		Point3 pt[2];

		pt[0] = *itPoint;
		pt[1] = *(itPoint+1);
		lineProc.proc(pt,2);
	}

	// Lower outline
	for ( std::vector<Point3>::iterator itPoint = vLowerOutline.begin();
		itPoint != ( vLowerOutline.end() - 1 );
		++itPoint )
	{
		Point3 pt[2];

		pt[0] = *itPoint;
		pt[1] = *(itPoint+1);
		lineProc.proc(pt,2);
	}

	// Construct connection vertex data and render
	vConnOutline.push_back( vUpperOutline.front() );
	vConnOutline.push_back( vLowerOutline.front() );
	lineProc.proc( &(vConnOutline[0]), vConnOutline.size() );
	vConnOutline.clear( );
	vConnOutline.push_back( vUpperOutline.back() );
	vConnOutline.push_back( vLowerOutline.back() );
	lineProc.proc( &(vConnOutline[0]), vConnOutline.size() );

	for(int i=0; i<nVisiblePoints; i++)
	{
		Matrix3 tm(1);
		Interval ivalid;

		if ( NULL == m_vpPoints[i] )
			continue;

		m_vpControls[i]->GetValue(t, &tm, ivalid, CTRL_RELATIVE);

		INodeTransformed node(pNode, tm);
#if MAX_RELEASE > MAX_RELEASE_R12
		m_vpPoints[i]->DisplayPoint( t, &node, pVpt, flags, fHeight );
#else
		m_vpPoints[i]->Display( t, &node, pVpt, flags, fHeight );
#endif
	}
}

// ------------------------------------------------------------------------------
// ReferenceTarget Super-Class Methods
// ------------------------------------------------------------------------------

RefTargetHandle 
CScrollbars::Clone( RemapDir& remap )
{
	CScrollbars* pClone = new CScrollbars;

	for ( s32 nRef=0; nRef<NumRefs(); ++nRef )
		pClone->ReplaceReference( nRef, remap.CloneRef( GetReference( nRef ) ) );

	BaseClone( this, pClone, remap );
	return pClone;
}

IOResult		
CScrollbars::Load( ILoad* iload )
{
	return IO_OK;
}

IOResult		
CScrollbars::Save( ISave* isave )
{
	return IO_OK;
}

int						
CScrollbars::NumRefs( )
{
	return ( 1 + ( 2 * sC_cnt_MaxNumberOfPoints ) );
}

RefTargetHandle			
CScrollbars::GetReference( int nIndex )
{
	assert( nIndex < NumRefs() );

	if ( SCROLLBARS_PBLOCK_REF_ID == nIndex )
		return m_pParamBlock;
	else if ( ( nIndex > SCROLLBARS_PBLOCK_REF_ID ) && ( nIndex <= sC_cnt_MaxNumberOfPoints ) )
		return m_vpPoints[ nIndex - 1 ];
	else if ( ( nIndex > sC_cnt_MaxNumberOfPoints ) && ( nIndex < NumRefs() ) )
		return m_vpControls[ nIndex - (sC_cnt_MaxNumberOfPoints + 1) ];
	
	return NULL;
}

void					
CScrollbars::SetReference( int nIndex, RefTargetHandle pTArg )
{
	assert( nIndex < NumRefs() );

	if ( SCROLLBARS_PBLOCK_REF_ID == nIndex )
		m_pParamBlock = static_cast<IParamBlock2*>( pTArg );
	else if ( ( nIndex > SCROLLBARS_PBLOCK_REF_ID ) && ( nIndex <= sC_cnt_MaxNumberOfPoints ) )
		m_vpPoints[ nIndex - 1 ] = static_cast<CScrollbarsPoint*>( pTArg );
	else if ( ( nIndex > sC_cnt_MaxNumberOfPoints ) && ( nIndex < NumRefs() ) )
		m_vpControls[ nIndex - (sC_cnt_MaxNumberOfPoints + 1) ] = static_cast<Control*>( pTArg );
}

RefResult				
CScrollbars::NotifyRefChanged( Interval changeInt, RefTargetHandle hTarget, 
							   PartID& partID, RefMessage message )
{
	switch ( message )
	{
	case REFMSG_CHANGE:
		scrollbarsDesc.InvalidateUI( );
		break;
	case REFMSG_TARGET_DELETED:
		for ( int i = 0; i < sC_cnt_MaxNumberOfPoints; ++i )
		{
			if ( m_vpPoints[ i ] == hTarget )
			{
				m_vpPoints[ i ] = NULL;
				TidyAfterDelete( i );
			}
		}
		break;
	}

	return REF_SUCCEED;
}

// ------------------------------------------------------------------------------
// User Interface
// ------------------------------------------------------------------------------

void			
CScrollbars::InitScrollbarsUI( )
{
	assert( !m_hScrollbarUI );
	if ( m_hScrollbarUI )
		return;

	GetScrollbarsDesc()->BeginEditParams( m_pInterface, this, BEGIN_EDIT_CREATE, NULL );

	IParamMap2* pMapParam = m_pParamBlock->GetMap( );
	m_hScrollbarUI = pMapParam->GetHWnd();
	SetDlgItemText( m_hScrollbarUI, IDC_VERSION, 
					TSTR("Version: ") + GetStringResource( g_hInstance, IDS_VERSION ) );
}

void			
CScrollbars::UpdateScrollbarsUI( )
{
	assert( m_hScrollbarUI );
	if ( !m_hScrollbarUI )
		return;
}

void			
CScrollbars::CloseScrollbarsUI( )
{
	if ( !m_hScrollbarUI )
		return;

	GetScrollbarsDesc()->EndEditParams( m_pInterface, this, END_EDIT_REMOVEUI, NULL );
	m_bCreating = false;
	m_hScrollbarUI = NULL;
}

void			
CScrollbars::InitScrollbarsPointUI( )
{
	m_hPointUI = m_pInterface->AddRollupPage( g_hInstance, 
											  MAKEINTRESOURCE( IDD_POINTS_PANEL ),
											  EditPointDlgProc,
											  _T("Scrollbars Point Parameters"),
											  (LPARAM)this );

	SetDlgItemText( m_hPointUI, IDC_VERSION, TSTR("Version ") + GetStringResource(g_hInstance, IDS_VERSION) );
}

void			
CScrollbars::CloseScrollbarsPointUI( )
{
	if ( m_hPointUI )
	{
		m_pInterface->DeleteRollupPage( m_hPointUI );

		m_hPointUI = NULL;
		m_pPointEdited = NULL;
	}
}

ObjectState 
CScrollbars::Eval( TimeValue t )
{
	for( int nIndex=0; nIndex<sC_cnt_MaxNumberOfPoints; ++nIndex )
	{
		if ( m_vpPoints[nIndex] )
		{
			m_vpPoints[nIndex]->Eval( t );
		}
	}

	return ObjectState( this );
}

// ------------------------------------------------------------------------------
// Object Super-Class
// ------------------------------------------------------------------------------

Interval		
CScrollbars::ObjectValidity( TimeValue t )
{
	return FOREVER;

	Interval ivalid = FOREVER;
	Matrix3 mtx;

	for ( s32 i=0; i<sC_cnt_MaxNumberOfPoints; ++i )
	{
		if ( m_vpPoints[i] )
			ivalid &= m_vpPoints[i]->ObjectValidity( t );
		if ( m_vpControls[i] )
			m_vpControls[i]->GetValue( t, &mtx, ivalid, CTRL_RELATIVE );
	}
	return ivalid;
}

void			
CScrollbars::InitNodeName( TSTR& sName )
{
	sName = _T( "Gta Scrollbars" );
}

// ------------------------------------------------------------------------------
// BaseObject
// ------------------------------------------------------------------------------

CreateMouseCallBack* 
CScrollbars::GetCreateMouseCallBack( )
{
	return NULL;
}

void			
CScrollbars::GetLocalBoundBox( TimeValue t, INode* pNode, ViewExp* pVp, Box3& box)
{
	s32 i;
	box.Init();

	float fHeight = 1.0f;
	Interval ivalid;
	IParamBlock2* pParamBlock = GetParamBlock( SCROLLBARS_PBLOCK_REF_ID );
	if ( !pParamBlock )
		return;

	pParamBlock->GetValue( scrollbars_height, t, fHeight, ivalid );

	for ( i=0; i<sC_cnt_MaxNumberOfPoints; ++i )
	{
		if ( NULL == m_vpPoints[i] )
			continue;

		Point3 ptUpperPos = GetPointPosition( i );
		Point3 ptLowerPos = GetPointPosition( i ) + Point3( 0.0f, 0.0f, -fHeight );

		Box3 localBox( Point3(-0.0f,-0.0f,-0.0f), Point3(0.0f,0.0f,0.0f) );

		m_vpPoints[i]->GetLocalBoundBox( t, NULL, pVp, localBox );
		localBox.pmin += ptLowerPos;
		localBox.pmax += ptUpperPos;

		box += localBox;
	}
}

void			
CScrollbars::GetWorldBoundBox( TimeValue t, INode* pNode, ViewExp* pVp, Box3& box)
{
	Matrix3 local2world = pNode->GetObjectTM( t );
	Box3 localBox;

	GetLocalBoundBox( t, pNode, pVp, localBox );
	box = localBox * local2world;
}

int				
CScrollbars::Display( TimeValue t, INode* pNode, ViewExp* pVpt, int flags )
{
	Draw( t, pNode, pVpt, flags );

	return 0;
}

int					
CScrollbars::HitTest(TimeValue t, INode* pNode, int type, int crossing, int flags, IPoint2 *pSp, ViewExp *pVpt)
{
	HitRegion hitRegion;
	DWORD savedLimits;
	GraphicsWindow *gw = pVpt->getGW();

	MakeHitRegion(hitRegion, type, crossing, 4, pSp);

	gw->setRndLimits(((savedLimits = gw->getRndLimits()) | GW_PICK) & ~GW_ILLUM);
	gw->setHitRegion(&hitRegion);
	gw->clearHitCode();

	Draw(t, pNode, pVpt, flags);

	int res = gw->checkHitCode();

	gw->setRndLimits(savedLimits);

	return res;
}

TCHAR*				
CScrollbars::GetObjectName( )
{
	return _T( "Gta Scrollbars" );
}

int					
CScrollbars::NumSubObjTypes( )
{
	return 1;
}

ISubObjType*		
CScrollbars::GetSubObjType( int nIndex )
{
	static GenSubObjType subObjType( "ScrollbarsPoint", "SubObjectIcons", 29 );
	assert( nIndex < NumSubObjTypes() );
	if ( nIndex >= NumSubObjTypes() )
		return NULL;

	return &subObjType;
}

void
CScrollbars::SelectAll( int selLevel )
{
	switch ( selLevel )
	{
	case SCROLLBAR_POINT_LEVEL:
		for ( int i = 0; i < GetNumPoints(); ++i )
		{
			SelectPoint( i, true );
		}
		break;
	}
}

// ------------------------------------------------------------------------------
// Sub-Object Construction, Selection etc.
// ------------------------------------------------------------------------------
s32			
CScrollbars::GetNumPoints( )
{
	size_t nIndex = 0;

	for ( size_t nIndex = 0; nIndex < sC_cnt_MaxNumberOfPoints; ++nIndex )
	{
		if ( NULL == m_vpPoints[nIndex] )
			break;
	}
	return nIndex;
}

s32
CScrollbars::AddPoint( )
{
	int nIndex = 0;
	for ( ; nIndex < sC_cnt_MaxNumberOfPoints; ++nIndex )
	{
		if ( NULL == m_vpPoints[ nIndex ] )
			break;
	}
	if ( sC_cnt_MaxNumberOfPoints == nIndex )
		return -1;

	Interface* ip = GetCOREInterface();
	assert( ip );
	if ( !ip )
		return -1;
						
	Class_ID classID = GetScrollbarsPointDesc()->ClassID();
	CScrollbarsPoint* pNewObj = static_cast<CScrollbarsPoint*>( ip->CreateInstance( HELPER_CLASS_ID, classID ) );
	Control* pNewControl = static_cast<Control*>( ip->CreateInstance( CTRL_MATRIX3_CLASS_ID, Class_ID( PRS_CONTROL_CLASS_ID, 0 ) ) );

	ReplaceReference( 1 + nIndex, pNewObj );
	ReplaceReference( 1 + sC_cnt_MaxNumberOfPoints + nIndex, pNewControl );

	NotifyDependents( FOREVER, PART_ALL, REFMSG_SUBANIM_STRUCTURE_CHANGED );

	return nIndex;
}

void			
CScrollbars::SetPointPosition( s32 nIndex, Point3 pos )
{
	assert( nIndex >= 0 );
	assert( nIndex < sC_cnt_MaxNumberOfPoints );
	assert( m_vpPoints[nIndex] );
	assert( m_vpControls[nIndex] );

	Point3 oldPos = GetPointPosition( nIndex );

	SetXFormPacket xform( pos - oldPos );
	if ( m_vpControls[nIndex] )
		m_vpControls[nIndex]->SetValue( 0, &xform, 1, CTRL_ABSOLUTE );
}

Point3			
CScrollbars::GetPointPosition( s32 nIndex )
{
	assert( nIndex >= 0 );
	assert( nIndex < sC_cnt_MaxNumberOfPoints );
	assert( NULL != m_vpPoints[nIndex] );
	assert( NULL != m_vpControls[nIndex] );

	Matrix3 tm(1);
	Interval ivalid;

	if ( m_vpControls[nIndex] )
		m_vpControls[nIndex]->GetValue(0, &tm, ivalid, CTRL_RELATIVE);

	return tm.GetTrans();
}

void	
CScrollbars::SelectPoint( s32 nIndex, bool bSelect )
{
	assert( nIndex >= 0 );
	assert( nIndex < sC_cnt_MaxNumberOfPoints );

	m_vbSelected[nIndex] = bSelect;
}

Matrix3	
CScrollbars::GetPointMatrix( s32 nIndex )
{
	assert( -1 != nIndex );
	assert( NULL != m_vpPoints[nIndex] );
	assert( NULL != m_vpControls[nIndex] );

	Matrix3 tm(1);
	Interval ivalid;

	if ( m_vpControls[nIndex] )
		m_vpControls[nIndex]->GetValue(0, &tm, ivalid, CTRL_RELATIVE);
	
	return tm;
}

CScrollbarsPoint*
CScrollbars::GetScrollbarPoint( s32 nIndex )
{
	assert( nIndex < sC_cnt_MaxNumberOfPoints );
	if ( nIndex >= sC_cnt_MaxNumberOfPoints )
		return NULL;

	assert( m_vpPoints[nIndex] );
	return ( m_vpPoints[nIndex] );
}

void	
CScrollbars::DeletePoint( s32 nIndex )
{
	assert( -1 != nIndex );
	assert( NULL != m_vpPoints[nIndex] );
	assert( NULL != m_vpControls[nIndex] );

	DeleteReference( 1 + nIndex );
	TidyAfterDelete( nIndex );
	NotifyDependents(FOREVER, PART_ALL, REFMSG_SUBANIM_STRUCTURE_CHANGED);
}

s32	
CScrollbars::GetPointId( Object* pPointObject )
{
	assert( pPointObject );

	for( s32 i=0; i<sC_cnt_MaxNumberOfPoints; ++i )
	{
		if ( m_vpPoints[i] == pPointObject )
			return i;
	}
	return -1;
}

s32	
CScrollbars::GetControlId( Control* pControl )
{
	assert( pControl );

	for ( s32 i=0; i<sC_cnt_MaxNumberOfPoints; ++i )
	{
		if ( m_vpControls[i] == pControl )
			return i;
	}
	return -1;
}

//
// name:		CScrollbars::TidyAfterDelete
// description: Cleans up the associated Point Control reference
//				and Point selected flag.
// in:			nIndex - index of Scrollbars Point
//
void	
CScrollbars::TidyAfterDelete( s32 nIndex )
{
	assert( nIndex >= 0 );
	assert( nIndex < sC_cnt_MaxNumberOfPoints );

	// Delete the associated Control reference
	DeleteReference( nIndex + sC_cnt_MaxNumberOfPoints + 1 );
	// Ensure the point is no longer selected
	m_vbSelected[nIndex] = false;
}

int		
CScrollbars::HitTest( TimeValue t, INode* pNode, IPoint2 p, s32 flags, ViewExp* pVpt, s32 ignore )
{
	for ( s32 i=0; i<sC_cnt_MaxNumberOfPoints; i++ )
	{
		if( ignore == i )
			continue;
		if ( m_vpPoints[i] &&
			!( ( flags & HIT_SELONLY ) && ( false == m_vbSelected[i] ) ) &&
			!( ( flags & HIT_UNSELONLY ) && ( true == m_vbSelected[i] ) ) )
		{
			assert( m_vpControls[i] );

			Matrix3 tm(1);
			Interval ivalid;

			if ( m_vpControls )
				m_vpControls[i]->GetValue(t, &tm, ivalid, CTRL_RELATIVE);

			INodeTransformed node(pNode, tm);

			if ( m_vpPoints[i]->HitTest( t, &node, HITTYPE_POINT, FALSE, flags, &p, pVpt ) ) 
			{
				return i;
			}
		}
	}

	return -1;
}

int						
CScrollbars::GetVisibleNumPoints( )
{
	IParamBlock2* pParamBlock = GetParamBlock( SCROLLBARS_PBLOCK_REF_ID );
	if ( !pParamBlock )
		return 0;

	Interval ivalid;
	int nResult = 0;
	pParamBlock->GetValue( scrollbars_numpoints, m_pInterface->GetTime(), nResult, ivalid );

	return nResult;
}

void					
CScrollbars::SetVisibleNumPoints( int nPoints )
{
	IParamBlock2* pParamBlock = GetParamBlock( SCROLLBARS_PBLOCK_REF_ID );
	if ( !pParamBlock )
		return;

	pParamBlock->SetValue( scrollbars_numpoints, m_pInterface->GetTime(), nPoints );
}

int CScrollbarsDesc::IsPublic( )
{
	return 1;
}

void* CScrollbarsDesc::Create( BOOL bLoading )
{
	return new CScrollbars;
}

const TCHAR* CScrollbarsDesc::ClassName( )
{
	return GetStringResource( g_hInstance, IDS_SCROLLBARS_CLASSNAME );
}

SClass_ID CScrollbarsDesc::SuperClassID( ) 
{
	return HELPER_CLASS_ID;
}

Class_ID CScrollbarsDesc::ClassID( ) 
{ 
	return SCROLLBARS_CLASSID; 
} 

const TCHAR* CScrollbarsDesc::Category( )
{
	return GetStringResource( g_hInstance, IDS_CATEGORY );
}

void CScrollbarsDesc::ResetClassParams( BOOL bFileReset )
{
	// Intentionally empty
	return;
}

int	CScrollbarsDesc::BeginCreate( Interface* pInterface )
{
	assert( pInterface );

	IObjCreate* pObjCreate = pInterface->GetIObjCreate( );
	assert( pObjCreate );
	if ( !pObjCreate )
		return FALSE;

	g_ScrollbarsCommandMode.Begin( pObjCreate, this );
	pObjCreate->PushCommandMode( &g_ScrollbarsCommandMode );

	return TRUE;
}

int	CScrollbarsDesc::EndCreate( Interface* pInterface )
{
	assert( pInterface );

	g_ScrollbarsCommandMode.End( );
	pInterface->RemoveMode( &g_ScrollbarsCommandMode );

	return TRUE;
}

const TCHAR* CScrollbarsDesc::InternalName( )
{
	return GetStringResource( g_hInstance, IDS_SCROLLBARS_INTERNALNAME );
}

HINSTANCE CScrollbarsDesc::HInstance( )
{
	return g_hInstance;
}
#if !defined( _WIN64 )
BOOL CALLBACK EditPointDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
#else
INT_PTR CALLBACK EditPointDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
#endif
{
	switch(msg)
	{
	case WM_INITDIALOG:
		SetDlgItemText(hWnd, IDC_VERSION, TSTR("Version ") + GetStringResource( g_hInstance, IDS_VERSION ) );
		break;
	case WM_CLOSE:
		break;
	default:
		CScrollbars::SetSelectedPointData( );
		return FALSE;
	}
	return TRUE;

}

ClassDesc2* GetScrollbarsDesc( )
{
	return &g_ScrollbarsObjectDescriptor;
}

// End of file
