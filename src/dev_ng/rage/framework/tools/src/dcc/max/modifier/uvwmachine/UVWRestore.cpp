//
//
//    Filename: UVWRestore.cpp
//     Creator: 
//     $Author: Adam $
//       $Date: 18/05/99 15:45 $
//   $Revision: 2 $
// Description: Restore objects for UVW machine
//
//
#include "UVWRestore.h"
#include "UVWData.h"

// --- UVWFaceRestore ------------------------------------------------------------------------------


UVWFaceRestore::UVWFaceRestore(Modifier *pM, UVWData *pData)
{
	m_pMod = pM;
	m_pUVWData = pData;
	m_pUVWData->SetFaceHeld(true);
	m_undoFaces = m_pUVWData->GetUVWFaces();
}


void UVWFaceRestore::Restore(int isUndo)
{
	if (isUndo) {
		m_redoFaces = m_pUVWData->GetUVWFaces();
	}
	m_pUVWData->SetUVWFaces(m_undoFaces);
	m_pMod->NotifyDependents(FOREVER, PART_TEXMAP, REFMSG_CHANGE);
}

void UVWFaceRestore::Redo()
{
	m_pUVWData->SetUVWFaces(m_redoFaces);
	m_pMod->NotifyDependents(FOREVER, PART_TEXMAP, REFMSG_CHANGE);
}

void UVWFaceRestore::EndHold()
{
	m_pUVWData->SetFaceHeld(false);
}


// --- UVWVertexRestore ------------------------------------------------------------------------------


UVWVertexRestore::UVWVertexRestore(Modifier *pM, UVWData *pData)
{
	m_pMod = pM;
	m_pUVWData = pData;
	m_pUVWData->SetVertexHeld(true);
	m_undoVerts = m_pUVWData->GetUVW();
}


void UVWVertexRestore::Restore(int isUndo)
{
	if (isUndo) {
		m_redoVerts = m_pUVWData->GetUVW();
	}
	m_pUVWData->SetUVW(m_undoVerts);
	m_pMod->NotifyDependents(FOREVER, PART_TEXMAP, REFMSG_CHANGE);
}

void UVWVertexRestore::Redo()
{
	m_pUVWData->SetUVW(m_redoVerts);
	m_pMod->NotifyDependents(FOREVER, PART_TEXMAP, REFMSG_CHANGE);
}

void UVWVertexRestore::EndHold()
{
	m_pUVWData->SetVertexHeld(false);
}

