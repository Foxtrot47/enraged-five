//
//
//    Filename: script.cpp
//     Creator: Greg Smith
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: MaxScript interface to the attribute code
//
//
#pragma warning (disable : 4786)

// max headers
#include <max.h>
// define the new primitives using macros from SDK
#include <maxscrpt\MaxScrpt.h>
#include <maxscrpt\MAXclses.h>
#include <maxscrpt\Numbers.h>
#include <maxscrpt\Strings.h>
#include <maxscrpt\Parser.h>
#include <maxscrpt\definsfn.h>
//#include <maxscrpt\3dmath.h>

#include "iladder.h"
#include "ladder.h"
#include "laddernode.h"

// STD headers
#include <string>

// DLL function
__declspec( dllexport ) void LibInit() {}


// ------------------------------------------------------------------------------
// 	Define our new functions
//	This will call the section operation code
// ------------------------------------------------------------------------------
def_visible_primitive( create_attractor,			"CreateAttractor");

Value *create_attractor_cf(Value** arg_list, int count)
{
	if(count < 4)
	{
		return &false_value;
	}

//	type_check(arg_list[0], point3, "CreateAttractor <point> <dirA> <dirB> <dirC>");
//	type_check(arg_list[1], point3, "CreateAttractor <point> <dirA> <dirB> <dirC>");
//	type_check(arg_list[2], point3, "CreateAttractor <point> <dirA> <dirB> <dirC>");
//	type_check(arg_list[3], point3, "CreateAttractor <point> <dirA> <dirB> <dirC>");

	Point3 pos = arg_list[0]->to_point3();
	Point3 dirA = arg_list[1]->to_point3();
	Point3 dirB = arg_list[2]->to_point3();
	Point3 dirC = arg_list[3]->to_point3();

	ladder* p_ladder = new ladder();
	INode *pNode = GetCOREInterface()->CreateObjectNode(p_ladder);

	Matrix3 mat(1);
	mat.SetTrans(pos);

	GetCOREInterface()->SetNodeTMRelConstPlane(pNode, mat);

	p_ladder->AddPathNode();
	((ladderNode*)p_ladder->SubAnim(0))->GetParamBlock(0)->SetValue(laddernode_length,0,0);
//	p_ladder->SetPathNodePosn(0, m_pt0);

	p_ladder->AddPathNode();
	((ladderNode*)p_ladder->SubAnim(1))->GetParamBlock(0)->SetValue(laddernode_length,0,1);
	p_ladder->SetPathNodePosn(1, dirA);

	p_ladder->AddPathNode();
	((ladderNode*)p_ladder->SubAnim(2))->GetParamBlock(0)->SetValue(laddernode_length,0,2);
	p_ladder->SetPathNodePosn(2, dirB);

	p_ladder->AddPathNode();
	((ladderNode*)p_ladder->SubAnim(3))->GetParamBlock(0)->SetValue(laddernode_length,0,3);
	p_ladder->SetPathNodePosn(3, dirC);

	return_protected(new MAXNode(pNode));
}