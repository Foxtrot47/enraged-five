//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by DistanceAlphaMap.rc
//
#define IDS_LIBDESCRIPTION              1
#define IDS_CATEGORY                    2
#define IDS_CLASS_NAME                  3
#define IDS_PARAMS                      4
#define IDS_SPIN                        5
#define IDS_SPIN_SCALE                  5
#define IDS_BUTT_DIFFUSE                6
#define IDS_COORDS                      7
#define IDS_BUTT_ALPHA                  8
#define IDS_CREATE                      9
#define IDS_LASTEXPORTEDPATH            10
#define IDS_BUTT_RESULT                 11
#define IDD_PANEL                       101
#define IDC_CLOSEBUTTON                 1000
#define IDC_DOSTUFF                     1000
#define IDC_BUTT_DIFFUSE                1001
#define IDC_BUTT_ALPHA                  1002
#define IDC_BUTT_PREVIEW                1004
#define IDC_SHOW_LAST_PATH              1005
#define IDC_COLOR                       1456
#define IDC_EDIT                        1490
#define IDC_EDIT_SCALE                  1490
#define IDC_SPIN                        1496
#define IDC_SPIN_SCALE                  1496

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1006
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
