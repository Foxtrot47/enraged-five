//
// filename:	SceneXmlConfig.cpp
// description:	
//

// --- Include Files ------------------------------------------------------------

// System headers
#include <Windows.h>

// Middleware headers
#include "tinyxml.h"

// Local headers
#include "cSceneXmlConfig.h"

// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

// --- Globals ------------------------------------------------------------------

// --- Static Globals -----------------------------------------------------------

const char* CONFIG_FILE_DEFAULT = "SceneXml.xml";
cSceneXmlConfig g_SceneXmlConfig;

// --- Static class members -----------------------------------------------------

// --- Code ---------------------------------------------------------------------

_tMaterialConfig::_tMaterialConfig( )
	: bEnabled( false )
{
}

_tMaterialConfig::_tMaterialConfig( TiXmlElement* pMatElem )
{
	assert( pMatElem );
	if ( !pMatElem )
		return;

	const char* enabled = pMatElem->Attribute( "export" );
	this->bEnabled = ( 0 == _stricmp( enabled, "true" ) ) ? true : false;
}

cSceneXmlConfig::cSceneXmlConfig( )
	: m_sNodeExportAs( "object" )
{
	m_bHasLoaded = false;
	m_bLoadFailed = false;
}

cSceneXmlConfig::~cSceneXmlConfig( )
{
	m_bHasLoaded = false;
	m_bLoadFailed = false;
	m_mSuperclassIDs.clear( );
	m_mClassNodes.clear( );
	m_mSuperclassNodes.clear( );
	m_sFilename.clear( );
}

void 
cSceneXmlConfig::Load( const char* filename )
{
	m_mSuperclassIDs.clear( );
	m_mClassNodes.clear( );
	m_mSuperclassNodes.clear( );
	m_sFilename.clear();
	ParseConfigFile( filename );
}

const std::string* const 
cSceneXmlConfig::RetSuperclassName( const SClass_ID& sid ) const
{
	const std::string* pSuperClassName = NULL;

	if ( ContainsSuperclassDef( sid ) )
	{
		std::map<SClass_ID, std::string>::const_iterator it =
			m_mSuperclassIDs.find( sid );
		pSuperClassName = &(it->second);
	}

	return ( pSuperClassName );
}

const tNodeDescription* const 
cSceneXmlConfig::RetNodeDescForClass( const std::string& classname ) const
{
	const tNodeDescription* pNodeDesc = NULL;

	if ( ContainsNodeDescForClass( classname ) )
	{
		std::map<std::string, tNodeDescription>::const_iterator it = 
			m_mClassNodes.find( classname );
		pNodeDesc = &(it->second);
	}

	return ( pNodeDesc );
}

const tNodeDescription* const 
cSceneXmlConfig::RetNodeDescForSuperClass( const std::string& superclass ) const
{
	const tNodeDescription* pNodeDesc = NULL;

	if ( ContainsNodeDescForSuperClass( superclass ) )
	{
		std::map<std::string, tNodeDescription>::const_iterator it = 
			m_mSuperclassNodes.find( superclass );
		pNodeDesc = &(it->second);
	}

	return ( pNodeDesc );
}


// --- Private Code -------------------------------------------------------------

void
cSceneXmlConfig::ParseConfigFile( const char* filename )
{
	this->m_sFilename.assign( filename );

	TiXmlDocument document;
	bool loaded = document.LoadFile( filename );
	if ( !loaded )
	{
		MessageBox( NULL, "SceneXml Configuration load failed.  Contact tools.", 
					"SceneXml Error", SW_NORMAL );
		this->m_bLoadFailed = true;
		this->m_bHasLoaded = false;

		return;
	}

	TiXmlElement* pRoot = document.RootElement();
	// Find our selection set configuration.
	m_vSelectionSets.clear();
	TiXmlNode* pSelectionSetsNode = pRoot->FirstChild( "selection_sets" );
	if ( pSelectionSetsNode )
	{
		TiXmlElement* pSelectionSetsElem = pSelectionSetsNode->ToElement( );
		for ( TiXmlElement* pChild = pSelectionSetsElem->FirstChild( "set" )->ToElement();
			pChild;
			pChild = pChild->NextSiblingElement() )
		{
			const char* pSetName = pChild->Attribute( "name" );
			const char* pSetExport = pChild->Attribute( "exports" );
			bool exports = ( 0 == _stricmp( pSetExport, "true" ) );

			m_vSelectionSets.push_back( tSelectionSet( pSetName, exports ) );
		}
	}

	// Find our top-level object node for global properties for all objects.
	// This currently only sets the top-level XML nodes for objects.
	TiXmlNode* pObjectNode = pRoot->FirstChild( "object" );
	if ( pObjectNode )
	{
		TiXmlElement* pObjectElem = pObjectNode->ToElement();
		if ( pObjectElem )
		{
			const char* pExportAs = pObjectElem->Attribute( "export_as" );
			if ( pExportAs )
				m_sNodeExportAs = std::string( pExportAs );
		}
	}
	// Find our top-level material node for global material properties.
	TiXmlNode* pMatNode = pRoot->FirstChild( "material" );
	if ( pMatNode )
	{
		TiXmlElement* pMatElem = pMatNode->ToElement( );
		if ( pMatElem )
			this->m_MaterialConfig = tMaterialConfig( pMatElem );
	}

	// Iterate through our superclass definition elements
	TiXmlNode* pSuperclassesNode = pRoot->FirstChild( "superclasses" );
	if ( pSuperclassesNode )
	{
		TiXmlElement* pSuperclassesElem = pSuperclassesNode->ToElement();
		if ( pSuperclassesElem )
		{
			for ( TiXmlElement* pChild = pSuperclassesElem->FirstChild( "superclass" )->ToElement();
				pChild;
				pChild = pChild->NextSiblingElement() )
			{
				const char* pSuperclassID = pChild->Attribute( "id" );
				const char* pSuperclassName = pChild->Attribute( "name" );
				long sid = 0L;

				sscanf( pSuperclassID, "%d", &sid );

				if ( pSuperclassID && ( 0 != sid) && pSuperclassName )
					m_mSuperclassIDs[sid] = std::string( pSuperclassName );
			}
		}
	}

	// Iterate through our top-level node elements
	for ( TiXmlElement* pChild = pRoot->FirstChild( "node" )->ToElement(); 
		  pChild; 
		  pChild = pChild->NextSiblingElement() )
	{
		tNodeDescription nodeDesc;
		const char* sClassName = pChild->Attribute( "class" );
		const char* sSuperclassName = pChild->Attribute( "superclass" );
		const char* sExportAs = pChild->Attribute( "export_as" );

		assert( !(sClassName && sSuperclassName) );

		int a = 0, b = 0;
		const char* sIDA = pChild->Attribute( "ida", &a );
		const char* sIDB = pChild->Attribute( "idb", &b );

		// Iterate through our node property configuration
		TiXmlElement* pProperties = pChild->FirstChild( "properties" )->ToElement();
		if ( !pProperties )
		{
			char msg[1024];
			sprintf_s( (char*)&msg, 1024, "SceneXml: %s node does not have properties defined.  " \
					   "Node will be exported as unknown.", 
					   sClassName );
			MessageBox( NULL, msg, "SceneXml Error", SW_NORMAL );
			
			// Skip setting up this node
			continue;
		}

		for ( TiXmlElement* pProperty = pProperties->FirstChild( "property" )->ToElement();
			  pProperty;
			  pProperty = pProperty->NextSiblingElement() )
		{
			tNodeProperty property;
			const char* sPropertyName = pProperty->Attribute( "name" );
			const char* sPropExportAs = pProperty->Attribute( "export_as" );
			const char* sPropType = pProperty->Attribute( "type" );
		
			property.sName = sPropertyName;
			property.sExportAs = ( NULL != sPropExportAs ) ? sPropExportAs : "unknownExportAs";
			property.sType = ( NULL != sPropType ) ? sPropType : "unknownExportType";

			TiXmlNode* pKeyedPropertyNode = pProperty->FirstChild( "keys" );
			if(pKeyedPropertyNode)
			{
				for ( TiXmlElement* pKeyedProperty = pKeyedPropertyNode->ToElement();
					pKeyedProperty;
					pKeyedProperty = pKeyedProperty->NextSiblingElement() )
				{
					tNodeProperty keyProperty;
					const char* sKeyedPropertyName = pKeyedProperty->Attribute( "name" );
					const char* sKeyedPropExportAs = pKeyedProperty->Attribute( "export_as" );
					const char* sKeyedPropType = pKeyedProperty->Attribute( "type" );

					keyProperty.sName = sKeyedPropertyName;
					keyProperty.sExportAs = ( NULL != sKeyedPropExportAs ) ? sKeyedPropExportAs : "unknownExportAs";
					keyProperty.sType = ( NULL != sKeyedPropType ) ? sKeyedPropType : "unknownExportType";
					property.vKeyedProperties.push_back( keyProperty );
				}
			}
			nodeDesc.vProperties.push_back( property );
		}

		Interface_ID id( (long)a, (long)b );
		nodeDesc.idInterface = id;
		nodeDesc.sClassName = ( NULL != sClassName ) ? sClassName : sSuperclassName;
		if ( NULL != sExportAs && strlen( sExportAs ) > 0 )
			nodeDesc.sExportAs = sExportAs;

		if ( sClassName )
			m_mClassNodes[nodeDesc.sClassName] = nodeDesc;
		else if ( sSuperclassName )
			m_mSuperclassNodes[nodeDesc.sClassName] = nodeDesc;
		else
		{
			char msg[1024];
			sprintf_s( msg, 1024, "[%d, %d] Error: Node has no class or superclass defined.  Definition shall be ignored.",
				pChild->Row(), pChild->Column() );
			MessageBox( NULL, msg, "SceneXml Error", SW_SHOW );
		}
	}
	m_bHasLoaded = true;
}
