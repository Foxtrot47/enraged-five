#include ".\utility.h"

CUtility g_utility;

CUtility::CUtility(void)
{
}

CUtility::~CUtility(void)
{
}

void CUtility::DrawCircle(int nPoints, float radius, IObjParam* pIp, Matrix3& mtx, float r/*=0.0f*/, float g/*=0.0f*/,float b/*=0.0f*/)
{
	ViewExp *pVpt = pIp->GetActiveViewport();
	DrawCircle(nPoints, radius, pVpt, mtx, r, g, b);
	pIp->ReleaseViewport(pVpt);
}

void CUtility::DrawCircle(int nPoints, float radius, ViewExp *pVpt, Matrix3& mtx, float r/*=0.0f*/, float g/*=0.0f*/,float b/*=0.0f*/)
{
	GraphicsWindow *gw = pVpt->getGW();
	DrawLineProc lineProc(gw);
	gw->setTransform(mtx);
	gw->setColor(LINE_COLOR, r, g, b);

	Point3* pPts = new Point3[nPoints+1];
	assert(pPts);

	float u;
	float multiplier = float(TWOPI) / float(nPoints);
	for (int i = 0; i <= nPoints; i++)
	{
		u = float(i) * multiplier;
		pPts[i].x = (float)cos(u) * radius;
		pPts[i].y = (float)sin(u) * radius;
		pPts[i].z = 0.0f;

	}
	lineProc.proc(pPts,nPoints+1);

	delete[] pPts;
}

#define USE_UTILITY_DEBUG
void CUtility::Debug(VehicleNode* pNode, char* strIn)
{
#ifdef USE_UTILITY_DEBUG
	INode* pINode= NULL;
	if(pNode)
		pINode = pNode->GetINode();

	char str[1024];
	char temp[] = "No Name";
	char* strName;
	if(pINode)
		strName = pINode->GetName();
	else
		strName = temp;
	sprintf(str, "%s : %s \n", strIn, strName);
	OutputDebugString(str);
#endif //#ifdef USE_UTILITY_DEBUG
}

void CUtility::Debug(VehicleLink* pNode, char* strIn)
{
#ifdef USE_UTILITY_DEBUG
	INode* pINode= NULL;
	if(pNode)
		pINode = pNode->GetINode();

	char str[1024];
	char temp[] = "No Name";
	char* strName;
	if(pINode)
		strName = pINode->GetName();
	else
		strName = temp;
	sprintf(str, "%s : %s \n", strIn, strName);
	OutputDebugString(str);
#endif // #ifdef USE_UTILITY_DEBUG
}

void CUtility::Debug(INode* pINode, char* strIn)
{
#ifdef USE_UTILITY_DEBUG
	char str[1024];
	char temp[] = "No Name";
	char* strName;
	if(pINode)
		strName = pINode->GetName();
	else
		strName = temp;
	sprintf(str, "%s : %s \n", strIn, strName);
	OutputDebugString(str);
#endif // #ifdef USE_UTILITY_DEBUG
}

void CUtility::Debug(char* strIn)
{
#ifdef USE_UTILITY_DEBUG
	if(NULL != strIn)
		OutputDebugString(strIn);
	else
		OutputDebugString("CUtility::Debug passed NULL\n");

#endif // #ifdef USE_UTILITY_DEBUG
}

void CUtility::WalkNodes(Interface *ip)
{
	INode *pRoot = ip->GetRootNode();

		//
	// parse all the objects
	//
	Debug("Walking Nodes \n");
	int numChilds = pRoot->NumberOfChildren();
	for(int i=0; i<numChilds; i++)
	{
		INode *pNode = pRoot->GetChildNode(i);
		Object* pObject = pNode->EvalWorldState(0).obj;

		if(VEHICLE_NODE_CLASS_ID == pObject->ClassID())
		{
			VehicleNode* pVehNode = ((VehicleNode*)pObject);
#if (MAX_RELEASE >= 12000)
			DependentIterator pItem((VehicleNode*)pObject);
			ReferenceMaker* maker = NULL;
			while(NULL != (maker = pItem.Next()))
			{
				if(BASENODE_CLASS_ID == maker->SuperClassID())
				{
					INode* pLinkNode = (INode *)maker;
					Debug(pLinkNode, "	WalkNodes : Found this NODE");
				}

				if(VEHICLE_LINK_CLASS_ID == maker->ClassID())
				{
					VehicleLink *pLink = static_cast<VehicleLink*>(maker);
					Debug(pLink, "			Has this LINK as a reference");
				}
			}
#else
			RefList& refList = ((VehicleNode*)pObject)->GetRefList();
			RefListItem *pItem = refList.first;
			while(pItem)
			{
				if(BASENODE_CLASS_ID == pItem->maker->SuperClassID())
				{
					INode* pLinkNode = (INode *)pItem->maker;
					Debug(pLinkNode, "	WalkNodes : Found this NODE");
				}

				if(VEHICLE_LINK_CLASS_ID == pItem->maker->ClassID())
				{
					VehicleLink *pLink = static_cast<VehicleLink*>(pItem->maker);
					Debug(pLink, "			Has this LINK as a reference");
				}
				pItem = pItem->next;
			}
#endif  //MAX_RELEASE >= 12000
			std::vector<RefTargetHandle>  refListVec = pVehNode->GetRefListVector();
			
			for(int j=0; j<refListVec.size(); j++)
			{
				if(refListVec[j] && refListVec[j]->ClassID() == VEHICLE_LINK_CLASS_ID)
				{
					VehicleLink* pLink = (VehicleLink* )refListVec[j];
					Debug(pLink, "		Has this LINK as a reference");
				}
			}
		}
		else if(VEHICLE_LINK_CLASS_ID == pObject->ClassID())
		{
#if (MAX_RELEASE >= 12000)
			DependentIterator pItem((VehicleLink*)pObject);
			ReferenceMaker* maker = NULL;
			while(NULL != (maker = pItem.Next()))
			{
				if(BASENODE_CLASS_ID == maker->SuperClassID())
				{
					INode* pLinkNode = (INode *)maker;
					Debug(pLinkNode, "	WalkNodes : Found this LINK");
				}
				else if(VEHICLE_NODE_CLASS_ID == maker->ClassID())
				{
					VehicleNode *pNode = static_cast<VehicleNode*>(maker);
					Debug(pNode, "			Has this NODE as a reference");
				}
			}
#else
			RefList& refList = ((VehicleLink*)pObject)->GetRefList();
			RefListItem *pItem = refList.first;
			while(pItem)
			{
				if(BASENODE_CLASS_ID == pItem->maker->SuperClassID())
				{
					INode* pLinkNode = (INode *)pItem->maker;
					Debug(pLinkNode, "	WalkNodes : Found this LINK");
				}
				else if(VEHICLE_NODE_CLASS_ID == pItem->maker->ClassID())
				{
					VehicleNode *pNode = static_cast<VehicleNode*>(pItem->maker);
					Debug(pNode, "			Has this NODE as a reference");
				}
				pItem = pItem->next;
			}
#endif  //MAX_RELEASE >= 12000
		}
	}
}

void CUtility::Debug(Control* pControl, char* strIn, Interface *ip)
{
#ifdef USE_UTILITY_DEBUG
	Matrix3 mat;
	Interval i;
	TimeValue t = ip->GetTime();

	mat.IdentityMatrix();
	pControl->GetValue(t, &mat, i, CTRL_RELATIVE);
	Point3 pt0 = mat.GetTrans();

	char str[1024];
	sprintf(str, "%s - x : %f y : %f\n", strIn, pt0.x, pt0.y);
	OutputDebugString(str);
#endif //#ifdef USE_UTILITY_DEBUG
}

