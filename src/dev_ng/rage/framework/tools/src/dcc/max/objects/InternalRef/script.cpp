//
// File:: script.cpp
// Description::
//
// Author::
// Date::
//

// --- Include Files ------------------------------------------------------------
#pragma warning ( push )
#pragma warning ( disable : 4100 4244 4238 4239 4245 4512 )
#include <max.h>
#include <modstack.h>
#include <imtledit.h>
// define the new primitives using macros from SDK
#include <maxscript\MaxScript.h>
#include <maxscript\maxwrapper\maxclasses.h>
#include <maxscript\foundation\numbers.h>
#include <maxscript\foundation\strings.h>
#include <maxscript\macros\define_instantiation_functions.h>
#include <maxscript\util\listener.h>
#pragma warning ( pop )

#include "internalref.h"

def_visible_primitive( ixref_gettarget,"ixref_gettarget" );
def_visible_primitive( ixref_settarget,"ixref_settarget" );
def_visible_primitive( ixref_setmaterial, "ixref_setmaterial" );
def_visible_primitive( ixref_setmaterial_override, "ixref_setmaterial_override" );

/**
 * @brief Get InternalRef target node.
 */
Value* 
ixref_gettarget_cf(Value** arg_list, int count)
{
	check_arg_count( ixref_gettarget, 1, count );
	type_check(arg_list[0], MAXNode, "ixref_gettarget [node]");

	INode* p_node = arg_list[0]->to_node();

	if(!p_node)
		return &undefined;

	Object* p_object = p_node->GetObjectRef();

	if(!p_object)
		return &undefined;
	
	if(p_object->ClassID() != INTERNALREF_CLASS_ID)
		return &undefined;

	InternalRefObject* intRefObj = (InternalRefObject*)p_object;

	INode* p_OutNode = (INode*)intRefObj->GetReference(0);

	if(p_OutNode)
		return_protected(new MAXNode(p_OutNode));

	return &undefined;
}

/**
 * @brief Set InternalRef target node.
 */
Value* 
ixref_settarget_cf(Value** arg_list, int count)
{
	check_arg_count( ixref_settarget, 2, count );
	type_check(arg_list[0], MAXNode, "ixref_settarget [node] [target node]");

	INode* p_node = arg_list[0]->to_node();
	INode* p_nodeTarget = arg_list[1]->to_node();

	if(!p_node)
		return &false_value;

	Object* p_object = p_node->GetObjectRef();

	if(!p_object)
		return &false_value;

	if(p_object->ClassID() != INTERNALREF_CLASS_ID)
		return &false_value;

	InternalRefObject* intRefObj = (InternalRefObject*)p_object;

	if(intRefObj->ReplaceReference(0,p_nodeTarget))
	{
		return ( &true_value );
	}
	
	return ( &false_value );
}

/**
 * @brief Force update InternalRef node's rendering material.
 */
Value*
ixref_setmaterial_cf( Value** arg_list, int arg_count )
{
	check_arg_count( ixref_setmaterial, 1, arg_count );
	type_check( arg_list[0], MAXNode, "ixref_setmaterial <node>" );

	INode* pNode = arg_list[0]->to_node( );
	Object* pObject = pNode->GetObjectRef();
	if ( !pObject || pObject->ClassID() != INTERNALREF_CLASS_ID )
		return ( &false_value );

	InternalRefObject* intRefObj = (InternalRefObject*)pObject;
	INode* pRefTarget = (INode*)intRefObj->GetReference(0);
	if ( pRefTarget )
	{
		pNode->ReplaceReference( NODE_MTL_REF_ID, pRefTarget->GetMtl() );
		return ( &true_value );
	}

	return ( &false_value );
}

/**
 * @brief Update InternalRef node's rendering material.
 *
 * This can be used to override the IxRef's render material to a specific material.
 * Its only for testing and you should use ixref_set_material instead.
 */
Value* 
ixref_setmaterial_override_cf( Value** arg_list, int arg_count )
{
	check_arg_count( ixref_setmaterial_override, 2, arg_count );
	type_check( arg_list[0], MAXNode, "ixref_setmaterial_override <node> <material>" );
	
	INode* pNode = arg_list[0]->to_node( );
	MtlBase* pMatBase = arg_list[1]->to_mtl( );
	if ( pNode && pMatBase )
	{
		pNode->ReplaceReference( NODE_MTL_REF_ID, pMatBase );
		return ( &true_value );
	}

	return ( &false_value );
}

// script.cpp
