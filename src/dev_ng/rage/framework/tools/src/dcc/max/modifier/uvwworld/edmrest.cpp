#include "mods.h"
#include "MeshDLib.h"
#include "uvwworld.h"
#include "edmdata.h"
#include "edmrest.h"

////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////
VertexEditRestore::VertexEditRestore (EditMeshData* md, EditMeshMod* mod)
{
	meshData  = md;
	this->mod = mod;

	int num;
	num = md->mdelta.vMove.Count();
	if(num != 0)
	{
		oMove.SetCount (num);
		memcpy (oMove.Addr(0), md->mdelta.vMove.Addr(0), num*sizeof(VertMove));
	}
	num = 0;
	num = md->mdelta.vClone.Count();
	if (num != 0)
	{
		oClone.SetCount (md->mdelta.vClone.Count());
		memcpy (oClone.Addr(0), md->mdelta.vClone.Addr(0), num*sizeof(VertMove));
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
VertexEditRestore::~VertexEditRestore()
{
}

////////////////////////////////////////////////////////////////////////////////////////////
void VertexEditRestore::Restore(int isUndo) 
{
	int num;

	nMove.SetCount (num=meshData->mdelta.vMove.Count());

	if(num) 
	{
		memcpy(nMove.Addr(0), meshData->mdelta.vMove.Addr(0), num*sizeof(VertMove));
	}

	nClone.SetCount (num=meshData->mdelta.vClone.Count());

	if(num) 
	{
		memcpy(nClone.Addr(0), meshData->mdelta.vClone.Addr(0), num*sizeof(VertMove));
	}

	meshData->mdelta.vMove.SetCount (num=oMove.Count());

	if(num) 
	{
		memcpy(meshData->mdelta.vMove.Addr(0), oMove.Addr(0), num*sizeof(VertMove));
	}

	meshData->mdelta.vClone.SetCount (num=oClone.Count());

	if(num) 
	{
		memcpy(meshData->mdelta.vClone.Addr(0), oClone.Addr(0), num*sizeof(VertMove));
	}

	meshData->Invalidate(PART_GEOM|PART_TEXMAP,FALSE);

	if(isUndo) 
	{
		mod->NotifyDependents(FOREVER, PART_GEOM|PART_TEXMAP, REFMSG_CHANGE);
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
void VertexEditRestore::Redo() 
{
	int num;

	meshData->mdelta.vMove.SetCount (num=nMove.Count());

	if(num) 
	{
		memcpy(meshData->mdelta.vMove.Addr(0), nMove.Addr(0), num*sizeof(VertMove));
	}

	meshData->mdelta.vClone.SetCount (num=nClone.Count());

	if(num) 
	{
		memcpy(meshData->mdelta.vClone.Addr(0), nClone.Addr(0), num*sizeof(VertMove));
	}

	meshData->Invalidate (PART_GEOM|PART_TEXMAP,FALSE);
	mod->NotifyDependents(FOREVER, PART_GEOM|PART_TEXMAP, REFMSG_CHANGE);
}

////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////
MeshEditRestore::MeshEditRestore (EditMeshData* md, EditMeshMod *mod, MeshDelta & changer):
	p_oTVerts(NULL),
	p_nTVerts(NULL)
{
	meshData = md;
	this->mod = mod;
	mapChanges.SetCount (NUM_HIDDENMAPS + changer.GetMapNum());
	changeFlags = changer.ChangeFlags (&mapChanges);
	omdelta.CopyMDChannels (meshData->mdelta, changeFlags, &mapChanges);

	delete[] p_oTVerts;
	m_oITVSize = md->m_iNoTVerts;
	p_oTVerts = new Point3[m_oITVSize];
	memcpy(p_oTVerts,md->mp_tVerts,sizeof(Point3) * m_oITVSize);
}

////////////////////////////////////////////////////////////////////////////////////////////
MeshEditRestore::MeshEditRestore (EditMeshData* md, EditMeshMod *mod, DWORD cF):
	p_oTVerts(NULL),
	p_nTVerts(NULL)
{
	meshData = md;
	this->mod = mod;
	changeFlags = cF;
	mapChanges.SetCount (md->mdelta.GetMapNum());

	for(int i=0; i<mapChanges.Count(); i++) 
	{
		mapChanges[i] = 0;
	}

	omdelta.CopyMDChannels (meshData->mdelta, changeFlags, &mapChanges);

	delete[] p_oTVerts;
	m_oITVSize = md->m_iNoTVerts;
	p_oTVerts = new Point3[m_oITVSize];
	memcpy(p_oTVerts,md->mp_tVerts,sizeof(Point3) * m_oITVSize);
}

////////////////////////////////////////////////////////////////////////////////////////////
MeshEditRestore::~MeshEditRestore()
{
	delete[] p_oTVerts;
	delete[] p_nTVerts;
}

////////////////////////////////////////////////////////////////////////////////////////////
void MeshEditRestore::Restore(int isUndo) 
{
	nmdelta.CopyMDChannels (meshData->mdelta, changeFlags, &mapChanges);
	meshData->mdelta.CopyMDChannels (omdelta, changeFlags, &mapChanges);
	meshData->Invalidate (PART_TOPO|PART_GEOM|PART_SELECT|PART_TEXMAP, FALSE);	
	mod->NotifyDependents (FOREVER, PART_TOPO|PART_GEOM|PART_SELECT|PART_TEXMAP, REFMSG_CHANGE);

	delete[] p_nTVerts;
	m_nITVSize = meshData->m_iNoTVerts;
	p_nTVerts = new Point3[m_nITVSize];
	memcpy(p_nTVerts,meshData->mp_tVerts,sizeof(Point3) * m_nITVSize);

	meshData->m_iNoTVerts = m_oITVSize;
	memcpy(meshData->mp_tVerts,p_oTVerts,sizeof(Point3) * m_oITVSize);
}

////////////////////////////////////////////////////////////////////////////////////////////
void MeshEditRestore::Redo () 
{
	meshData->mdelta.CopyMDChannels (nmdelta, changeFlags, &mapChanges);
	meshData->Invalidate (PART_TOPO|PART_GEOM|PART_SELECT, FALSE);	
	mod->NotifyDependents (FOREVER, PART_TOPO|PART_GEOM|PART_SELECT, REFMSG_CHANGE);

	meshData->m_iNoTVerts = m_nITVSize;
	memcpy(meshData->mp_tVerts,p_nTVerts,sizeof(Point3) * m_nITVSize);
}

////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////
MeshSelectRestore::MeshSelectRestore (EditMeshData * md, EditMeshMod * mm) 
{
	meshData = md;
	mod = mm;
	t = GetCOREInterface()->GetTime(); // LAM - defect 289808
	selLevel = mm->selLevel;

	switch (selLevel) 
	{
	case SL_VERTEX: 
		undo = md->mdelta.vsel; 
		break;
	case SL_EDGE: 
		undo = md->mdelta.esel; 
		break;
	default: 
		undo = md->mdelta.fsel; 
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
MeshSelectRestore::MeshSelectRestore (EditMeshData * md, EditMeshMod * mm, DWORD selLev) 
{
	meshData = md;
	mod = mm;
	t = GetCOREInterface()->GetTime(); // LAM - defect 289808
	selLevel = selLev;

	switch (selLev) 
	{
	case SL_VERTEX: 
		undo = md->mdelta.vsel; 
		break;
	case SL_EDGE: 
		undo = md->mdelta.esel; 
		break;
	default: 
		undo = md->mdelta.fsel; 
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
void MeshSelectRestore::Restore(int isUndo) 
{
	switch (selLevel) 
	{
	case SL_VERTEX:
		redo = meshData->mdelta.vsel; 
		meshData->mdelta.vsel = undo;
		break;
	case SL_EDGE:
		redo = meshData->mdelta.esel;
		meshData->mdelta.esel = undo;
		break;
	default:
		redo = meshData->mdelta.fsel; 
		meshData->mdelta.fsel = undo;
		break;
	}

	if(meshData->MeshCached(t)) 
	{
		Mesh *mesh = meshData->GetMesh(t);

		switch (selLevel) 
		{
		case SL_VERTEX: 
			mesh->vertSel = undo; 
			break;
		case SL_EDGE: 
			mesh->edgeSel = undo; 
			break;
		default: 
			mesh->faceSel = undo; 
			break;
		}

		meshData->Invalidate(PART_SELECT);
	}
	mod->LocalDataChanged();
}

////////////////////////////////////////////////////////////////////////////////////////////
void MeshSelectRestore::Redo() 
{
	switch (selLevel)
	{
	case SL_VERTEX:
		meshData->mdelta.vsel = redo;
		break;
	case SL_EDGE:   			
		meshData->mdelta.esel = redo;
		break;
	default:
		meshData->mdelta.fsel = redo;
		break;
	}

	if (meshData->MeshCached(t)) 
	{
		Mesh *mesh = meshData->GetMesh(t);

		switch (selLevel) 
		{
		case SL_VERTEX: 
			mesh->vertSel = redo; 
			break;
		case SL_EDGE: 
			mesh->edgeSel = redo; 
			break;
		default: 
			mesh->faceSel = redo; 
			break;
		}

		meshData->Invalidate(PART_SELECT);
	}

	mod->LocalDataChanged();
}
