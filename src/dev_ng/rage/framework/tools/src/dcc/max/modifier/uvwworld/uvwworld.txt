Project uvwworld
Template max_configurations.xml

OutputFileName uvwworld.dlm
ConfigurationType dll

WarningsAsErrors false
WarningLevel 3

Define GTA_PC

Include $(RS_TOOLSROOT)\src\GTA_tools\include

EmbeddedLibAll comctl32.lib acap.lib Amodeler.lib biped.lib bmm.lib composite.lib core.lib crowd.lib CustDlg.lib edmodel.lib expr.lib flt.lib geom.lib geomimp.lib gfx.lib gup.lib IGame.lib imageViewers.lib ManipSys.lib maxnet.lib Maxscrpt.lib maxutil.lib MenuMan.lib menus.lib mesh.lib MNMath.lib Paramblk2.lib particle.lib ParticleFlow.lib physique.lib Poly.lib RenderUtil.lib rtmax.lib SpringSys.lib tessint.lib viewfile.lib zlibdll.lib

Files {
	Folder "Header Files" { 
		edmdata.h
		edmrest.h
		mods.h
		modsres.h
		UVWworld.h
	}
	Folder "Resource Files" {	
		UVWworld.rc
	}
	Folder "Source Files" {
		DLLEntry.cpp
		editmops.cpp
		edmdata.cpp
		edmrest.cpp
		edmui.cpp
		UVWworld.cpp
		UVWworld.def
	}	
}