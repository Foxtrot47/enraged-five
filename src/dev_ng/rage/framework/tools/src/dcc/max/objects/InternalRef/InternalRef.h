#ifndef __INTERNALREF__H__ 
#define __INTERNALREF__H__

//
// File::			InternalRef.h
// Description::	
//
// Author::			
// Date::			
//

// --- Include Files ------------------------------------------------------------

// Max headers
#pragma warning ( push )
#pragma warning ( disable : 4100 4244 4238 4239 4245 4512 )
#include "Max.h"
#include "MaxAPI.h"
#include "resource.h"
#include "meshacc.h"
#include "osnapapi.h"
#include "maxscript/MaxScript.h"
#include "maxscript/maxwrapper/mxsobjects.h"
#pragma warning ( pop )

// Helper headers
#include "Win32HelperLib/Win32Res.h"

// --- Defines ------------------------------------------------------------------
#define INTERNALREF_CLASS_ID	Class_ID(0x62a94c64, 0x3635185)
#define NODE_MTL_REF_ID			(3)

// --- Constants ----------------------------------------------------------------

// --- Globals ------------------------------------------------------------------
extern ClassDesc* GetInternRefObjDesc();
extern HINSTANCE hInstance;

// --- Structure/Class Definitions ----------------------------------------------

class InternRefCreateCallBack;
class InternRefObjPick;

/**
 * @brief
 */
class InternalRef 
{
public:
	InternalRef(INode *n = NULL, float d = 0.0f) 
	{
		node = n;
		dist = d;
		ResetStr();
	}

	void ResetStr(void) {
		if (node)
			listStr.printf("%s   %g", node->GetName(), dist);
		else 
			listStr.printf("%s   %g", _T("NO_NAME"), dist);
	}

	void SetDist(float d) 
	{ 
		dist = d; 
		ResetStr(); 
	}
	
	// Public data!
	INode *node;
	TSTR listStr;
	float dist;
};

/**
 * @brief
 */
class InternalRefObject: public HelperObject 
{			   
public:
	InternalRefObject();
	~InternalRefObject();

#if MAX_VERSION_MAJOR >= 9 
	ReferenceTarget* Clone(RemapDir &remap = DefaultRemapDir());
#else
	ReferenceTarget* Clone(RemapDir &remap = NoRemap());
#endif

	// From BaseObject
	void GetMat(TimeValue t, INode* inode, ViewExp* vpt, Matrix3& tm);
	int HitTest(TimeValue t, INode* inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt);
	void Snap(TimeValue t, INode* inode, SnapInfo *snap, IPoint2 *p, ViewExp *vpt, Mesh *mesh);
	//void Snap(Object* pobj, IPoint2 *p, TimeValue t, ViewExp *vpt); 
	void SnapToVert(const Mesh& m, const char* sv, const Point2 cursorPos);
	int Display(TimeValue t, INode* inode, ViewExp *vpt, int flags);
	CreateMouseCallBack* GetCreateMouseCallBack();
	void BeginEditParams( IObjParam *ip, ULONG flags,Animatable *prev);
	void EndEditParams( IObjParam *ip, ULONG flags,Animatable *next);
	TCHAR *GetObjectName() { return _T(GetStringResource(hInstance, IDS_OBJ_NAME)); }

	// From Object
	ObjectState Eval(TimeValue time);
	void InitNodeName(TSTR& s) { s = _T(GetStringResource(hInstance, IDS_OBJ_NAME)); }
	Interval ObjectValidity();
	Interval ObjectValidity(TimeValue time);
	int CanConvertToType(Class_ID obtype);
	Object* ConvertToType(TimeValue t, Class_ID obtype);

	void GetWorldBoundBox(TimeValue t, INode *mat, ViewExp *vpt, Box3& box );
	void GetLocalBoundBox(TimeValue t, INode *mat, ViewExp *vpt, Box3& box );

	// Animatable methods
	void DeleteThis() { delete this; }
	Class_ID ClassID() { return INTERNALREF_CLASS_ID; }  
	void GetClassName(TSTR& s) { s = TSTR(_T(GetStringResource(hInstance, IDS_CLASS_NAME))); }
	int IsKeyable(){ return 1;}
	LRESULT CALLBACK TrackViewWinProc( HWND hwnd,  UINT message, 
		WPARAM wParam,   LPARAM lParam ){return(0);}

	int NumRefs() { return 1; }
	RefTargetHandle GetReference(int i);
	void SetReference(int i, RefTargetHandle rtarg);

	// IO
	IOResult Save(ISave *isave);
	IOResult Load(ILoad *iload);

private:
	friend class InternRefCreateCallBack;
	friend class InternRefObjPick;
	friend INT_PTR CALLBACK RollupDialogProc( HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam );

	// Class vars
	static HWND hRollup;
	static ICustButton *intRefPickButton;
	static int dlgPrevSel;

	RefResult NotifyRefChanged( Interval changeInt, RefTargetHandle hTarget, 
		PartID& partID, RefMessage message );
	void RefreshMaterial( );
	void BuildMesh(Mesh& mesh);
	void GetDistPoints(float radius, Point3 *x, Point3 *y, Point3 *z);
	void DrawDistSphere(TimeValue t, INode* inode, GraphicsWindow *gw);

	bool m_bRefreshMaterial;
	float radius;
	float dlgCurSelDist;
	static IObjParam *iObjParams;

	InternalRef intRefObjects;
	CommandMode *previousMode;
};	

// --- Static Globals -----------------------------------------------------------

// --- Static class members -----------------------------------------------------

// --- Code ---------------------------------------------------------------------

#endif __INTERNALREF__H__
