#ifndef EDMDATA_H
#define EDMDATA_H

#include <max.h>
#include <vector>

////////////////////////////////////////////////////////////////////////////////////////////
// Named selection set levels:
#define NS_VERTEX 0
#define NS_EDGE 1
#define NS_FACE 2

////////////////////////////////////////////////////////////////////////////////////////////
using namespace std;

////////////////////////////////////////////////////////////////////////////////////////////
class EditMeshMod;

////////////////////////////////////////////////////////////////////////////////////////////
class TempMoveRestore 
////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	Tab<Point3> pos;
	Tab<Point3> tex;
	//Tab<Tab<UVVert> *> maps;

	TempMoveRestore(Mesh *msh);
	void Restore(Mesh *msh);
	DWORD ChannelsChanged ();
};

////////////////////////////////////////////////////////////////////////////////////////////
class EditMeshData : public LocalModData, public IMeshSelectData, public MeshDeltaUserData 
////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	DWORD flags;

	//funcs to update uv's
	void applyUVChange(MeshDelta& md,Mesh* mesh);

	// This is the change record on the incoming object.
	MeshDelta mdelta;

	Point3* mp_tVerts;
	Point3* mp_ctVerts;
	int m_iNoTVerts;
	
	void createBackupUVW();
	void saveUVW();

	// This represents temporary drag movements:
	MeshDelta tempMove;
	TempMoveRestore *tmr;

	// Lists of named selection sets
	GenericNamedSelSetList selSet[3];

	// While an object is being edited, these exist:
	EditMeshMod *mod;
	Mesh *mesh;
	DWORD lastInVNum, lastInFNum;
	Tab<DWORD> lastInMVNum;
	MeshTempData *tempData;
	Interval mValid, topoValid, geomValid;
	BOOL lockInvalidate;

	EditMeshData();
	EditMeshData(EditMeshData& emc);
	~EditMeshData();

	// Applies modifications to a triOb
	void Apply(TimeValue t,TriObject *triOb, EditMeshMod *mod);

	// If this is the first edit, then the delta arrays will be allocated.  Returns cached mesh.
	void SetModifier (EditMeshMod *md) { mod = md; }
	void BeginEdit (TimeValue t);
	LocalModData *Clone() { return new EditMeshData(*this); }
	
	void SetFlag(DWORD f,BOOL on) { if ( on ) flags|=f; else flags&=~f; }
	DWORD GetFlag(DWORD f) { return flags&f; }

	// Temp data related methods in edmdata.cpp:
	Mesh *GetMesh (TimeValue t);
	MeshTempData *TempData (TimeValue t);
	void Invalidate (PartID part,BOOL meshValid=TRUE);
	BOOL MeshCached(TimeValue t);
	void UpdateCache(TimeValue t, TriObject *triOb);

	// LocalModData
	void* GetInterface(ULONG id) { if (id == I_MESHSELECTDATA) return(IMeshSelectData*)this; else return LocalModData::GetInterface(id); }

	// IMeshSelectData methods:
	BitArray GetVertSel() { return mdelta.vsel; }
	BitArray GetFaceSel() { return mdelta.fsel; }
	BitArray GetEdgeSel() { return mdelta.esel; }
	BitArray GetSel (int nsl);
	void SetVertSel(BitArray &set, IMeshSelect *mod, TimeValue t);
	void SetFaceSel(BitArray &set, IMeshSelect *mod, TimeValue t);
	void SetEdgeSel(BitArray &set, IMeshSelect *mod, TimeValue t);
	void SetSel (int nsl, BitArray &set, IMeshSelect *mod, TimeValue t);
	GenericNamedSelSetList & GetNamedVertSelList () { return selSet[NS_VERTEX]; }
	GenericNamedSelSetList & GetNamedEdgeSelList () { return selSet[NS_EDGE]; }
	GenericNamedSelSetList & GetNamedFaceSelList () { return selSet[NS_FACE]; }

	// Other named selection set methods:
	void ChangeNamedSetSize (int nsl, int oldsize, int increase);
	void DeleteNamedSetArray (int nsl, BitArray & del);

	// From MeshDeltaUserData
	void ApplyMeshDelta (MeshDelta & md, MeshDeltaUser *mdu, TimeValue t);
	MeshDelta *GetCurrentMDState () { return &mdelta; }
	void MoveSelection(int level, TimeValue t, Matrix3& partm, Matrix3& tmAxis, Point3& val, BOOL localOrigin);
	void RotateSelection(int level, TimeValue t, Matrix3& partm, Matrix3& tmAxis, Quat& val, BOOL localOrigin);
	void ScaleSelection(int level, TimeValue t, Matrix3& partm, Matrix3& tmAxis, Point3& val, BOOL localOrigin);
	void ExtrudeSelection(int level, BitArray* sel, float amount, float bevel, BOOL groupNormal, Point3* direction);
};

#endif //EDMDATA_H