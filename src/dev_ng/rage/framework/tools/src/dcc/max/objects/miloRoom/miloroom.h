#ifndef __MILOROOM__H__
 
#define __MILOROOM__H__

#include "Max.h"
#include "MaxAPI.h"
#include "resource.h"
 


//extern ClassDesc* GetInternRefObjDesc();
extern TCHAR *GetString(int id);

#define MILOROOM_CLASS_ID	Class_ID(0x35636012, 0x7abf523a)



class MiloRoom {
public:
	INode *node;
	TSTR listStr;
	float dist;
	void ResetStr(void) {
		if (node)
			listStr.printf("%s   %g", node->GetName(), dist);
		else listStr.printf("%s   %g", _T("NO_NAME"), dist);
	}
	void SetDist(float d) { dist = d; ResetStr(); }
	MiloRoom(INode *n = NULL, float d = 0.0f) {
		node = n;
		dist = d;
		ResetStr();
	}
};

class MiloRoomObject: public HelperObject {			   
	friend class MiloRoomCreateCallBack;
	friend class MiloRoomObjPick;
	friend INT_PTR CALLBACK RollupDialogProc( HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam );

	// Class vars
    static HWND hRollup;
	static ICustButton *intRefPickButton;
	static int dlgPrevSel;


	RefResult NotifyRefChanged( Interval changeInt, RefTargetHandle hTarget, 
	   PartID& partID, RefMessage message );
	float dlgCurSelDist;
	static IObjParam *iObjParams;

	Mesh mesh;
	void BuildMesh(void);
	void BuildBoundBox(Point3* vertArray1, Point3* vertArray2, Point3* vertArray3, Point3* vertArray4, Point3* vertArray5, Point3* vertArray6, Box3 &roomBounds);
	void DrawBoundBox(TimeValue t, INode* inode, GraphicsWindow *gw, Box3 &roomBounds);
	void GetDistPoints(float radius, Point3 *x, Point3 *y, Point3 *z);
	void DrawDistSphere(TimeValue t, INode* inode, GraphicsWindow *gw);
	void CalculateTotalRoomBounds(INode* inode, Box3 &roomBounds);

	MiloRoom miloRoomObject;
	CommandMode *previousMode;

public:
	MiloRoomObject();
	~MiloRoomObject();


#if MAX_VERSION_MAJOR >= 9 
	RefTargetHandle Clone(RemapDir& remap = DefaultRemapDir());
#else
	RefTargetHandle Clone(RemapDir& remap = NoRemap());
#endif

	// From BaseObject
	void GetMat(TimeValue t, INode* inode, ViewExp* vpt, Matrix3& tm);
	int HitTest(TimeValue t, INode* inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt);
//	void Snap(TimeValue t, INode* inode, SnapInfo *snap, IPoint2 *p, ViewExp *vpt);
	int Display(TimeValue t, INode* inode, ViewExp *vpt, int flags);
	CreateMouseCallBack* GetCreateMouseCallBack();
	void BeginEditParams( IObjParam *ip, ULONG flags,Animatable *prev);
	void EndEditParams( IObjParam *ip, ULONG flags,Animatable *next);
	TCHAR *GetObjectName() { return _T(GetString(IDS_OBJ_NAME)); }


	MiloRoom GetMiloRoomObject() { return miloRoomObject; }

	// From Object
	ObjectState Eval(TimeValue time);
	void InitNodeName(TSTR& s) { s = _T(GetString(IDS_OBJ_NAME)); }
	Interval ObjectValidity();
	Interval ObjectValidity(TimeValue time);
	//int DoOwnSelectHilite() { return 1; }

	void GetWorldBoundBox(TimeValue t, INode *mat, ViewExp *vpt, Box3& box );
	void GetLocalBoundBox(TimeValue t, INode *mat, ViewExp *vpt, Box3& box );

	// Animatable methods
	void DeleteThis() { delete this; }
	Class_ID ClassID() { return MILOROOM_CLASS_ID; }  
	void GetClassName(TSTR& s) { s = TSTR(_T(GetString(IDS_CLASS_NAME))); }
	int IsKeyable(){ return 1;}
	LRESULT CALLBACK TrackViewWinProc( HWND hwnd,  UINT message, 
            WPARAM wParam,   LPARAM lParam ){return(0);}

	

	// IO
	IOResult Save(ISave *isave);
	IOResult Load(ILoad *iload);
};				


#endif

