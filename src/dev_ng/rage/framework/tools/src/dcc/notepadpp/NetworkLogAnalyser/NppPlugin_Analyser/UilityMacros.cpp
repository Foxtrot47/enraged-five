//
// UtilityMacros.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

//Win32 Includes
#include <windows.h>
#include <stdio.h>

//Plugin Includes
#include "UtilityMacros.h"


void 
CDebugDisplay::DebugDisplay(const char *s) 
{
	::OutputDebugStringA(s);
}

void 
CDebugDisplay::DebugPrintf(const char *format, ...)
{
	char errorMsg[2000];
	va_list pArguments;
	va_start(pArguments, format);
	vsprintf(errorMsg,format,pArguments);
	va_end(pArguments);
	CDebugDisplay::DebugDisplay(errorMsg);
	CDebugDisplay::DebugDisplay("\n");
}

void 
CDebugDisplay::DebugAssert(const char *c, const char *file, int line)
{
	char errorMsg[2000];
	sprintf(errorMsg, "Assertion [%s] failed at %s %d", c, file, line);

	int idButton = ::MessageBoxA(0, errorMsg, "Assertion failure", MB_ABORTRETRYIGNORE|MB_ICONHAND|MB_SETFOREGROUND|MB_TASKMODAL);

	if (idButton == IDRETRY) 
	{
		::DebugBreak();
	} 
	else if (idButton == IDIGNORE) 
	{
		// all OK
	}
	else 
	{
		abort();
	}
}

