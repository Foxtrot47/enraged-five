//
// DockingDlg.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

//Template Includes
#include "resource1.h"
#include "Scintilla.h"

//Plugin Includes
#include "DockingDlg.h"
#include "UtilityMacros.h"
#include "Text.h"
#include "NetLogAnalyzerInterface.h"


//Combo Box options
#define NUM_COMBO_OPTIONS    3
#define COMBO_OPT_CHANNELLOG 0
#define COMBO_OPT_HIGHLIGHT  1
#define COMBO_OPT_MARKTEXT   2

static char ComboList[NUM_COMBO_OPTIONS][MAX_TXT_LEN] = 
{
	"Mark Text",       // COMBO_OPT_MARKTEXT
	"Highlight Text",  // COMBO_OPT_HIGHLIGHT
	"Filter tty",      // COMBO_OPT_CHANNELLOG
};


// ------- CDockingDlg

void 
CDockingDlg::display(bool toShow) const 
{
	DockingDlgInterface::display(toShow);
	if (toShow)
	{
		::SetFocus(::GetDlgItem(_hSelf, IDC_PLUGINNETANALYSER_EDIT1));
		//select the whole find editor text
		//::SendDlgItemMessage(_hSelf, IDC_NETPLUGIN_FINDTEXT, EM_SETSEL, 0, -1);
	}
}


void CDockingDlg::InitButtons()
{
	HWND hwndCombo = ::GetDlgItem(_hSelf, IDC_PLUGINNETANALYSER_COMBO);

	::SendMessage(hwndCombo, CB_RESETCONTENT, 0, 0);

	for(int i=0; i<=NUM_COMBO_OPTIONS; i++)
	{
		const TCHAR* comboText = StringConverter::Char2Wchar(ComboList[i], CNetLogAnalyzerInterface::ExecSc(SCI_GETCODEPAGE, NULL, NULL));
		::SendMessage(hwndCombo, CB_INSERTSTRING, 0, (LPARAM)comboText);
	}
	::SendMessage(hwndCombo, CB_DELETESTRING, 0, 0);
	::SendMessage(hwndCombo, CB_SETCURSEL, COMBO_OPT_CHANNELLOG, 0);

	SetRegularExpressions(false);
	SetWholeWord(false);
	SetTextHighlight(true);
	SetMatchCase(true);

	::SendDlgItemMessage(_hSelf, IDC_PLUGINNETANALYSER_PROP_HIGHLIGHT, BM_SETCHECK, BST_CHECKED,   0);
	::SendDlgItemMessage(_hSelf, IDC_PLUGINNETANALYSER_PROP_MATCHCASE, BM_SETCHECK, BST_CHECKED,   0);
	::SendDlgItemMessage(_hSelf, IDC_PLUGINNETANALYSER_PROP_WHOLEWORD, BM_SETCHECK, BST_UNCHECKED, 0);
	::SendDlgItemMessage(_hSelf, IDC_PLUGINNETANALYSER_PROP_REGEXP,    BM_SETCHECK, BST_UNCHECKED, 0);
}

BOOL CALLBACK CDockingDlg::run_dlgProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message) 
	{
	case WM_COMMAND: 
		{
			SetRegularExpressions(BST_CHECKED == ::SendDlgItemMessage(_hSelf, IDC_PLUGINNETANALYSER_PROP_REGEXP, BM_GETCHECK, 0, 0));
			SetWholeWord(BST_CHECKED == ::SendDlgItemMessage(_hSelf, IDC_PLUGINNETANALYSER_PROP_WHOLEWORD, BM_GETCHECK, 0, 0));
			SetTextHighlight(BST_CHECKED == ::SendDlgItemMessage(_hSelf, IDC_PLUGINNETANALYSER_PROP_HIGHLIGHT, BM_GETCHECK, 0, 0));
			SetMatchCase(BST_CHECKED == ::SendDlgItemMessage(_hSelf, IDC_PLUGINNETANALYSER_PROP_MATCHCASE, BM_GETCHECK, 0, 0));

			switch (wParam)
			{
			case IDC_PLUGINNETANALYSER_CLEARHIGHLIGHT:
				{
					ClearIndicators();
					return TRUE;
				}

			case IDC_PLUGINNETANALYSER_CLEARMARKER:
				{
					ClearMarkers();
					return TRUE;
				}

			case IDC_PLUGINNETANALYSER_NEXTMARKER:
				{
					TCHAR str2Search[MAX_TXT_LEN];
					ZeroMemory(str2Search, sizeof(str2Search));
					::SendMessage(::GetDlgItem(_hSelf, IDC_PLUGINNETANALYSER_EDIT1), WM_GETTEXT, MAX_TXT_LEN - 1, (LPARAM)str2Search);

					NextMarkers(('\0' != str2Search[0]) ? MARK_TEXTMARK : MARK_DELIMITER);

					return TRUE;
				}

			case IDC_PLUGINNETANALYSER_PREVIOUSMARKER:
				{
					TCHAR str2Search[MAX_TXT_LEN];
					ZeroMemory(str2Search, sizeof(str2Search));
					::SendMessage(::GetDlgItem(_hSelf, IDC_PLUGINNETANALYSER_EDIT1), WM_GETTEXT, MAX_TXT_LEN - 1, (LPARAM)str2Search);

					PreviousMarkers(('\0' != str2Search[0]) ? MARK_TEXTMARK : MARK_DELIMITER);

					return TRUE;
				}

			case  IDC_PLUGINNETANALYSER_GO:
				{
					TCHAR str2Search[MAX_TXT_LEN];
					ZeroMemory(str2Search, sizeof(str2Search));
					::SendMessage(::GetDlgItem(_hSelf, IDC_PLUGINNETANALYSER_EDIT1), WM_GETTEXT, MAX_TXT_LEN - 1, (LPARAM)str2Search);

					SetRegularExpressions(BST_CHECKED == ::SendDlgItemMessage(_hSelf, IDC_PLUGINNETANALYSER_PROP_REGEXP, BM_GETCHECK, 0, 0));
					SetWholeWord(BST_CHECKED == ::SendDlgItemMessage(_hSelf, IDC_PLUGINNETANALYSER_PROP_WHOLEWORD, BM_GETCHECK, 0, 0));
					SetTextHighlight(BST_CHECKED == ::SendDlgItemMessage(_hSelf, IDC_PLUGINNETANALYSER_PROP_HIGHLIGHT, BM_GETCHECK, 0, 0));
					SetMatchCase(BST_CHECKED == ::SendDlgItemMessage(_hSelf, IDC_PLUGINNETANALYSER_PROP_MATCHCASE, BM_GETCHECK, 0, 0));

					if ('\0' != str2Search[0])
					{
						SearchGameLog(str2Search); break;
					}

					//HWND hwndCombo = ::GetDlgItem(_hSelf, IDC_PLUGINNETANALYSER_COMBO);
					//LRESULT selection = ::SendMessage(hwndCombo, CB_GETCURSEL , 0, 0);
					//switch (selection)
					//{
					//case COMBO_OPT_CHANNELLOG: if ('\0' != str2Search[0]) SearchGameLog(str2Search); break;
					//case COMBO_OPT_HIGHLIGHT:  if ('\0' != str2Search[0]) SetIndicators(str2Search); break;
					//case COMBO_OPT_MARKTEXT:   if ('\0' != str2Search[0]) SetMarkers(str2Search); break;
					//}

					return TRUE;
				}
				break;

				//Close Dockable Dialog
			case  IDC_PLUGINNETANALYSER_CANCEL:
				{
					::SetFocus(_hSelf);
					display(false);
					return TRUE;
				}
				break;

			case IDC_PLUGINNETANALYSER_COMBO:
			case IDC_PLUGINNETANALYSER_PROP_WHOLEWORD:
			case IDC_PLUGINNETANALYSER_PROP_REGEXP:
			case IDC_PLUGINNETANALYSER_PROP_HIGHLIGHT:
			case IDC_PLUGINNETANALYSER_PROP_MATCHCASE:
			case IDC_PLUGINNETANALYSER_EDIT1:
				break;
			case IDOK:
				break;
			}
		}
		break;

	case WM_CONTEXTMENU:
		break;

	case WM_SIZE:
		break;

	case WM_NOTIFY:
		Notify(reinterpret_cast<SCNotification *>(lParam));
		break;
	}

	return FALSE;
}

bool 
CDockingDlg::Notify(SCNotification *notification)
{
	return true;
}

void
CDockingDlg::SearchNetLog(TCHAR* str2Search) const 
{
	int cp = CNetLogAnalyzerInterface::ExecSc(SCI_GETCODEPAGE, NULL, NULL);
	Assert(-1 < cp);

	const char* txt2Find = StringConverter::Wchar2Char(str2Search, cp);

	SearchOptions opt;
	opt.m_HighlightText = m_TextHighlight;
	opt.m_IsWholeWord = m_WholeWord;
	opt.m_IsMatchCase = m_MatchCase;
	opt.m_SearchType = m_RegularExpressions ? FindRegex : FindNormal;

	CText::SetSearchOptions(opt);
	CText::FindInNetLog(txt2Find);
}


void
CDockingDlg::SearchGameLog(TCHAR* str2Search) const 
{
	int cp = CNetLogAnalyzerInterface::ExecSc(SCI_GETCODEPAGE, NULL, NULL);
	Assert(-1 < cp);

	char* txt2Find = StringConverter::Wchar2Char(str2Search, cp);

	SearchOptions opt;
	opt.m_HighlightText = m_TextHighlight;
	opt.m_IsWholeWord = m_WholeWord;
	opt.m_IsMatchCase = m_MatchCase;
	opt.m_SearchType = m_RegularExpressions ? FindRegex : FindNormal;

	CText::SetSearchOptions(opt);
	CText::SetOperation(Op_FindText);
	CText::FindInGameLog(txt2Find);
}

void 
CDockingDlg::SetIndicators(TCHAR* str2Search) const
{
	int cp = CNetLogAnalyzerInterface::ExecSc(SCI_GETCODEPAGE, NULL, NULL);
	Assert(-1 < cp);

	const char* txt2Find = StringConverter::Wchar2Char(str2Search, cp);

	SearchOptions opt;
	opt.m_HighlightText = m_TextHighlight;
	opt.m_IsWholeWord = m_WholeWord;
	opt.m_IsMatchCase = m_MatchCase;
	opt.m_SearchType = m_RegularExpressions ? FindRegex : FindNormal;

	CText::SetSearchOptions(opt);
	CText::SetOperation(Op_SetTextIndicator);

	const unsigned textLen = static_cast<unsigned>(strlen(txt2Find));
	if (AssertVerify(textLen < MAX_TXT_LEN))
	{
		unsigned numberOfChannels = 1;

		char text[MAX_NUM_CHANNELS][MAX_TXT_LEN];
		for(int i=0; i<MAX_NUM_CHANNELS; i++)
		{
			memset(&text[i][0], 0, MAX_TXT_LEN);
		}

		bool commaFound = false;
		unsigned count = 0;
		for (unsigned i=0; i<textLen && numberOfChannels<=MAX_NUM_CHANNELS; i++)
		{
			if (',' == txt2Find[i])
			{
				numberOfChannels++;
				count = 0;
				commaFound = true;
			}
			else if (((' ' == txt2Find[i]) && commaFound) || ('[' == txt2Find[i]) || (']' == txt2Find[i]))
			{
				continue;
			}
			else
			{
				strncpy(&text[numberOfChannels-1][count], &txt2Find[i], 1);
				count++;
				commaFound = false;
			}
		}

		if (!text[numberOfChannels-1][0])
		{
			numberOfChannels--;
		}

		for (unsigned i=0; i<numberOfChannels; i++)
		{
			CText::Find(&text[i][0]);
		}
	}
}

void 
CDockingDlg::ClearIndicators() const
{
	const int docLength = CNetLogAnalyzerInterface::ExecSc(SCI_GETLENGTH, 0, 0);
	if (0 < docLength)
	{
		CText::ClearIndicator(0, docLength);
	}
}

void 
CDockingDlg::AnalyseLog() const
{
	SearchOptions opt;
	opt.m_HighlightText = m_TextHighlight;
	opt.m_IsWholeWord = m_WholeWord;
	opt.m_IsMatchCase = m_MatchCase;
	opt.m_SearchType = m_RegularExpressions ? FindRegex : FindNormal;
	opt.m_MarkerType = MARK_DELIMITER;

	CText::SetSearchOptions(opt);
	CText::SetOperation(Op_SetMarker);
	CNetLogAnalyzerInterface::ExecSc(SCI_MARKERDEFINE,  MARK_DELIMITER, SC_MARK_ARROWDOWN);
	CNetLogAnalyzerInterface::ExecSc(SCI_MARKERSETALPHA, MARK_DELIMITER, 70);
	CNetLogAnalyzerInterface::ExecSc(SCI_MARKERSETFORE, MARK_DELIMITER, COLOR_WHITE);
	CNetLogAnalyzerInterface::ExecSc(SCI_MARKERSETBACK, MARK_DELIMITER, COLOR_GREY);
	CText::Find(MARKER_TXT);
}

void 
CDockingDlg::SetMarkers(TCHAR* str2Search) const
{
	int cp = CNetLogAnalyzerInterface::ExecSc(SCI_GETCODEPAGE, NULL, NULL);
	Assert(-1 < cp);

	const char* txt2Find = StringConverter::Wchar2Char(str2Search, cp);

	SearchOptions opt;
	opt.m_HighlightText = m_TextHighlight;
	opt.m_IsWholeWord = m_WholeWord;
	opt.m_IsMatchCase = m_MatchCase;
	opt.m_SearchType = m_RegularExpressions ? FindRegex : FindNormal;
	opt.m_MarkerType = MARK_TEXTMARK;

	CText::SetSearchOptions(opt);
	CText::SetOperation(Op_SetMarker);

	CNetLogAnalyzerInterface::ExecSc(SCI_MARKERDEFINE,  MARK_TEXTMARK, SC_MARK_ARROWDOWN);
	CNetLogAnalyzerInterface::ExecSc(SCI_MARKERSETALPHA, MARK_TEXTMARK, 40);
	CNetLogAnalyzerInterface::ExecSc(SCI_MARKERSETFORE, MARK_TEXTMARK, COLOR_WHITE);
	CNetLogAnalyzerInterface::ExecSc(SCI_MARKERSETBACK, MARK_TEXTMARK, COLOR_GREY);

	const unsigned textLen = static_cast<unsigned>(strlen(txt2Find));
	if (AssertVerify(textLen < MAX_TXT_LEN))
	{
		unsigned numberOfChannels = 1;

		char text[MAX_NUM_CHANNELS][MAX_TXT_LEN];
		for(int i=0; i<MAX_NUM_CHANNELS; i++)
		{
			memset(&text[i][0], 0, MAX_TXT_LEN);
		}

		bool commaFound = false;
		unsigned count = 0;
		for (unsigned i=0; i<textLen && numberOfChannels<=MAX_NUM_CHANNELS; i++)
		{
			if (',' == txt2Find[i])
			{
				numberOfChannels++;
				count = 0;
				commaFound = true;
			}
			else if (((' ' == txt2Find[i]) && commaFound) || ('[' == txt2Find[i]) || (']' == txt2Find[i]))
			{
				continue;
			}
			else
			{
				strncpy(&text[numberOfChannels-1][count], &txt2Find[i], 1);
				count++;
				commaFound = false;
			}
		}

		if (!text[numberOfChannels-1][0])
		{
			numberOfChannels--;
		}

		for (unsigned i=0; i<numberOfChannels; i++)
		{
			CText::Find(&text[i][0]);
		}
	}
}

void 
CDockingDlg::ClearMarkers() const
{
	CNetLogAnalyzerInterface::ExecSc(SCI_MARKERDELETEALL, MARK_TEXTMARK, 0);
}

void  
CDockingDlg::NextMarkers(const int markerType) const
{
	//Current Line Number
	int lineno = CNetLogAnalyzerInterface::ExecSc(SCI_LINEFROMPOSITION, CNetLogAnalyzerInterface::ExecSc(SCI_GETCURRENTPOS, 0, 0), 0);
	//Scan starting from next line
	int lineStart = lineno + 1;
	//If not found, try from the beginning
	int lineRetry = 0;

	int nextLine = int(CNetLogAnalyzerInterface::ExecSc(SCI_MARKERNEXT, lineStart, 1 << markerType));
	if (nextLine < 0)
	{
		nextLine = int(CNetLogAnalyzerInterface::ExecSc(SCI_MARKERNEXT, lineRetry, 1 << markerType));
	}

	if (nextLine < 0)
		return;

	CNetLogAnalyzerInterface::ExecSc(SCI_ENSUREVISIBLEENFORCEPOLICY, nextLine);
	CNetLogAnalyzerInterface::ExecSc(SCI_GOTOLINE, nextLine);
}

void  
CDockingDlg::PreviousMarkers(const int markerType) const
{
	//Current Line Number
	int lineno = CNetLogAnalyzerInterface::ExecSc(SCI_LINEFROMPOSITION, CNetLogAnalyzerInterface::ExecSc(SCI_GETCURRENTPOS, 0, 0), 0);
	//Scan starting from previous line
	int lineStart = lineno - 1;
	//If not found, try from the end
	int lineRetry = CNetLogAnalyzerInterface::ExecSc(SCI_GETLINECOUNT);

	int nextLine = int(CNetLogAnalyzerInterface::ExecSc(SCI_MARKERPREVIOUS, lineStart, 1 << markerType));
	if (nextLine < 0)
	{
		nextLine = int(CNetLogAnalyzerInterface::ExecSc(SCI_MARKERPREVIOUS, lineRetry, 1 << markerType));
	}

	if (nextLine < 0)
		return;

	CNetLogAnalyzerInterface::ExecSc(SCI_ENSUREVISIBLEENFORCEPOLICY, nextLine);
	CNetLogAnalyzerInterface::ExecSc(SCI_GOTOLINE, nextLine);
}


// eof

