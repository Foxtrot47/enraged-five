//
// NetLogAnalyser.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

//Template Includes
#include "PluginInterface.h"
#include "menuCmdID.h"

//Plugin Includes
#include "NetLogAnalyzer.h"
#include "UtilityMacros.h"
#include "NetLogAnalyzerInterface.h"
#include "Text.h"

EditFileMap*     CNetLogAnalyzer::sm_EditFiles  = NULL;
CDockingDlg*     CNetLogAnalyzer::sm_DockingDlg = NULL;

void  
CNetLogAnalyzer::Shutdown()
{
	if (sm_DockingDlg)
	{
		delete sm_DockingDlg;
		sm_DockingDlg = NULL;
	}

	if (sm_EditFiles)
	{
		EditFileMapIterator i = sm_EditFiles->begin();
		while (i != sm_EditFiles->end())
		{
			delete i->second;
			++i;
		}

		sm_EditFiles->clear();

		delete sm_EditFiles;
		sm_EditFiles = NULL;
	}
}

void 
CNetLogAnalyzer::About()
{
	::MessageBox(CNetLogAnalyzerInterface::HandleNpp(),
				TEXT("  Network Log Analyser is a plugin that permits to analyse network logging files\n")
				TEXT("  Hopefully you find it to be a useful tool.  Enjoy!\r\n")
				TEXT("  Developed by the Network team at R*"),
				TEXT("About This Plugin"), MB_OK);
}

CEditFile* 
CNetLogAnalyzer::AddEditFile(const char *filename, int view, int index, int bufferID, void* scintillaDoc)
{
	CEditFile* editFile = NULL;

	if (AssertVerify(sm_EditFiles))
	{
		editFile = new CEditFile(view, index, filename, 0, bufferID, scintillaDoc);

		eFileStatus status = SAVED;
		editFile->SetFileStatus(status);

		sm_EditFiles->insert(EditFileMap::value_type(bufferID, editFile));
	}

	return editFile;
}

CEditFile* 
CNetLogAnalyzer::AddEditFile(int bufferID)
{
	CEditFile* editFile = NULL;

	if (sm_EditFiles)
	{
		TCHAR filePath[MAX_PATH];

		CNetLogAnalyzerInterface::ExecSc(NPPM_GETFULLPATHFROMBUFFERID, bufferID, reinterpret_cast<LPARAM>(filePath));
		int index = CNetLogAnalyzerInterface::ExecSc(NPPM_GETPOSFROMBUFFERID, bufferID, reinterpret_cast<LPARAM>(filePath));

		int which = VIEW(index);
		int readonly = CNetLogAnalyzerInterface::ExecSc(SCI_GETREADONLY, 0, 0);

		int cp = CNetLogAnalyzerInterface::ExecSc(SCI_GETCODEPAGE, NULL, NULL);
		Assert(-1 < cp);

		const char* path = StringConverter::Wchar2Char(filePath, cp);
		CEditFile* editFile = CNetLogAnalyzer::AddEditFile(path, VIEW(index), INDEX(index), bufferID, 0);	

		if (readonly && editFile)
		{
			editFile->SetFileStatus(READONLY);
		}
	}

	return editFile;
}

void 
CNetLogAnalyzer::RemoveEditFile(int bufferID)
{
	if (sm_EditFiles)
	{
		//Free the EditFile
		EditFileMapIterator iter = sm_EditFiles->find(bufferID);
		if (iter != sm_EditFiles->end())
		{
			while (iter != sm_EditFiles->end() && iter->second->GetBufferID() == bufferID)
			{
				delete iter->second;
				++iter;
			}

			//Erase the EditFile from the two lookup maps
			sm_EditFiles->erase(bufferID);
		}
	}
}

void 
CNetLogAnalyzer::Switch2NextTab()
{
	int currentView;
	CNetLogAnalyzerInterface::ExecSc(NPPM_GETCURRENTSCINTILLA, 0, reinterpret_cast<LPARAM>(&currentView));

	int nbFile = CNetLogAnalyzerInterface::ExecSc(NPPM_GETNBOPENFILES, 0, currentView + 1);

	int position = CNetLogAnalyzerInterface::ExecSc(NPPM_GETCURRENTDOCINDEX, 0, currentView);
	if (position < nbFile - 1)
	{
		++position;
	}
	else
	{
		position = 0;
	}

	CNetLogAnalyzerInterface::ExecSc(NPPM_ACTIVATEDOC, currentView, position);
}


void 
CNetLogAnalyzer::Switch2PreviousTab()
{
	int currentView;
	CNetLogAnalyzerInterface::ExecSc(NPPM_GETCURRENTSCINTILLA, 0, reinterpret_cast<LPARAM>(&currentView));

	int position = CNetLogAnalyzerInterface::ExecSc(NPPM_GETCURRENTDOCINDEX, 0, currentView);
	if (position > 0)
	{
		--position;
	}
	else
	{
		int nbFile = CNetLogAnalyzerInterface::ExecSc(NPPM_GETNBOPENFILES, 0, currentView + 1);
		position = nbFile - 1;
	}

	CNetLogAnalyzerInterface::ExecSc(NPPM_ACTIVATEDOC, currentView, position);
}

void
CNetLogAnalyzer::LaunchNetLogAnalyser()
{
	if (!sm_EditFiles)
	{
		sm_EditFiles = new EditFileMap;
	}

	if (!sm_DockingDlg)
	{
		sm_DockingDlg = new CDockingDlg();
		sm_DockingDlg->init((HINSTANCE)CNetLogAnalyzerInterface::HandleModule(), CNetLogAnalyzerInterface::HandleNpp());
	}

	sm_DockingDlg->SetParent(CNetLogAnalyzerInterface::HandleNpp());

	tTbData	data = {0};
	if (!sm_DockingDlg->isCreated())
	{
		sm_DockingDlg->create(&data);

		//Define the default docking behavior
		data.uMask = DWS_DF_CONT_BOTTOM;

		data.pszModuleName = sm_DockingDlg->getPluginFileName();

		//the dlgDlg should be the index of funcItem where the current function pointer is
		//in this case is DOCKABLE_DIALOG_INDEX
		data.dlgID = DOCKABLE_DIALOG_INDEX;

		::SendMessage(CNetLogAnalyzerInterface::HandleNpp(), NPPM_DMMREGASDCKDLG, 0, (LPARAM)&data);

		CNetLogAnalyzerInterface::InitScintilla();
	}

	sm_DockingDlg->display();
	sm_DockingDlg->InitButtons();
}


void 
CNetLogAnalyzer::SaveDocument()
{
	CNetLogAnalyzerInterface::ExecSc(NPPM_SAVECURRENTFILE, 0, IDM_FILE_SAVE);
}

void 
CNetLogAnalyzer::CloseFile(const TCHAR* file)
{
	::SendMessage(CNetLogAnalyzerInterface::HandleNpp(), NPPM_MENUCOMMAND, (LPARAM)file, IDM_FILE_CLOSEALL_BUT_CURRENT);
}

void 
CNetLogAnalyzer::OpenFile(const TCHAR* file)
{
	::SendMessage(CNetLogAnalyzerInterface::HandleNpp(), NPPM_DOOPEN, 0, (LPARAM)file);
	::SendMessage(CNetLogAnalyzerInterface::HandleNpp(), NPPM_RELOADFILE, 0, (LPARAM)file);
}

void 
CNetLogAnalyzer::OpenFileDialog()
{
	::SendMessage(CNetLogAnalyzerInterface::HandleNpp(), NPPM_MENUCOMMAND, 0, IDM_FILE_OPEN);
}

void 
CNetLogAnalyzer::OpenNewDocument()
{
	CNetLogAnalyzerInterface::ExecSc(NPPM_MENUCOMMAND, 0, IDM_FILE_NEW);
}

void  
CNetLogAnalyzer::WriteLine(const char* txt)
{
	Assert(txt);
	CNetLogAnalyzerInterface::ExecSc(SCI_INSERTTEXT, 0, (LPARAM)txt);
}

void  
CNetLogAnalyzer::AppendLine(const char* txt)
{
	Assert(txt);
	CNetLogAnalyzerInterface::ExecSc(SCI_APPENDTEXT, (WPARAM)strlen(txt), (LPARAM)txt);
}
