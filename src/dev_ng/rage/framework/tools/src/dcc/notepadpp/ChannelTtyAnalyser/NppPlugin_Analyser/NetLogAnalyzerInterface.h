//
// NetLogAnalyzerInterface.h 
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef NETLOGANALYSER_INTERFACE_H
#define NETLOGANALYSER_INTERFACE_H


//Win32 Includes
#include <stdio.h>
#include <vector>

//Plugin Includes
#include "PluginInterface.h"
#include "UtilityMacros.h"


#if defined(_MSC_VER) && _MSC_VER >= 1400 
#pragma warning(push) 
#pragma warning(disable:4996) 
#endif 

//Forward Definitions
struct SCNotification;

//Npp uses either ansi or unicode.
typedef std::basic_string< TCHAR > tstring;

//Index of funcItem where the Dockable Dialog is created.
#define  DOCKABLE_DIALOG_INDEX  0

typedef int (* SCintillaFunc) (void*, int, int, int);
typedef void * SCintillaPtr;


//Class responsible for Initializing network log analyzer.
class CNetLogAnalyzerInterface
{
private:

	//Container for this plugin's function commands.
	static std::vector< FuncItem >  sm_PluginFuncVector;	

	//The data of Notepad++ that you can use in your plugin commands
	static NppData sm_NppData;

	//Name of the 'Plugins' menu.
	static tstring  sm_Name;

	//Module name with the .dll extension.
	static tstring  sm_ModuleName;

	//Module name without the .dll extension.
	static tstring  sm_ModuleBaseName;

	//Notepad++'s handle for this plugin module.
	static HANDLE  sm_HandleModule;

	//Handle to the currently active Notepad++ view for messaging.
	static HWND  sm_HandleCurrView;

	//Indicates if hCurrView handle needs updating.
	static bool  sm_RefreshCurrViewH;

	//Flag if Notepad++ has finished initalizing.
	static bool  sm_IsReady;

	static SCintillaFunc sm_ScintillaFunc;
	static SCintillaPtr  sm_ScintillaPtr;


public:

	//Notepad++ Plugin Initialization:
	//Initialize your plugin data here
	//It will be called while plugin loading   
	static void  InitPlugin(tstring name, HANDLE hModule); 
	static void  InitScintilla();

	//Notepad++ Plugin Shutdown:
	//Here you can do the clean up, save the parameters (if any) for the next session
	static void  ShutdownPlugin(); 

	//This function stores Function Items to the FuncItem vector for retrieval by N++.
	static void  SetPluginFuncItem(tstring Name, PFUNCPLUGINCMD pFunction, int cmdID = NULL, bool init2Check = false, ShortcutKey* pShKey = NULL);

	//Notepad++ Required Function
	//N++ PluginsManager forwards handle information for use in messaging.
	static void  SetInfo(NppData& nppData);

	//Returns handle for the currently focused view.
	static HWND  HandleCurrView();

	//Update handle pointer to the currently active view.
	static HWND  UpdateCurrViewHandle();

	//Deals with Messages and notifications Notepad++ exchanges with 
	//or forwards to plugins for cooperative purposes.
	static void  HandleNotifications(SCNotification* notifyCode);

	//Plugin Name
	static const TCHAR * GetPluginName() { return &sm_Name[0]; }

	//Returns this module's full name.
	static tstring*  GetModuleName() { return &sm_ModuleName; }

	//Returns this module's name without the .dll extension.
	static tstring*  GetModuleBaseName() { return &sm_ModuleBaseName; }

	//Sets flag when Notepad++ is done with start up.
	static void  SetNppReady() { sm_IsReady = true; }

	//Returns Notepad++ initialization state.
	static bool  IsNppReady() { return sm_IsReady; }

	//Plugin module's handle.
	static HANDLE  HandleModule() { return sm_HandleModule; }

	//Returns the main Notepad++ handle.
	static HWND  HandleNpp() { Assert(sm_NppData._nppHandle); return sm_NppData._nppHandle; }

	//Returns handle used in messaging the main view.
	static HWND  HandleMainView() { Assert(sm_NppData._scintillaMainHandle); return sm_NppData._scintillaMainHandle; }

	//Returns handle used in messaging the secondary view.
	static HWND  HandleSecondView() { Assert(sm_NppData._scintillaSecondHandle); return sm_NppData._scintillaSecondHandle; }

	//Returns 0 for main view and 1 for second view.
	static int  IntCurrView() { return ( (HandleCurrView() == sm_NppData._scintillaMainHandle) ? ( 0 ) : ( 1 ) ); }

	//Sets flag to update hCurrView.
	static void  HandleCurrViewNeedsUpdate() { sm_RefreshCurrViewH = true; }

	//Returns the size of the Function vector.
	static int  GetPluginFuncCount() { return static_cast<int>(sm_PluginFuncVector.size()); }

	//Returns the size of the Function vector.
	static std::vector<FuncItem>  GetPluginFuncVector() { return ( sm_PluginFuncVector ); }

	//Returns this plugin's Function vector as an array.
	static FuncItem* GetPluginFuncArray() { return ( &sm_PluginFuncVector[0] ); }

	//Execute Scintilla command/function.
	static int ExecSc(UINT Msg, WPARAM wParam = 0, LPARAM lParam = 0);
};

#endif // NETLOGANALYSER_INTERFACE_H

//eof 
