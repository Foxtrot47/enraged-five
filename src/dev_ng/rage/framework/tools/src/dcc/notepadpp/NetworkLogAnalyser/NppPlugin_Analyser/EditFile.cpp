//
// EditFile.cpp 
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//


//Win32 Includes
#include <string.h>
#include <tchar.h>

//Template Includes

//Plugin Includes
#include "EditFile.h"



CEditFile::CEditFile(void)
{
	m_DataSet = false;
	m_IndexString = NULL;
	m_ViewString = NULL;
}

CEditFile::CEditFile(int view, int index, const char* filename, int searchFlags, int bufferID, void* scintillaDoc)
{
	//Copy the full filename
	int filenameLength = (int)strlen(filename);
	m_FullFilename = new char[filenameLength + 1];
	strcpy_s(m_FullFilename, filenameLength + 1, filename);

	int position, filenameOffset = 1;
	for (position = filenameLength; position >= 0 && m_FullFilename[position] != '\\'; position--)
		;
	if (position < 0)
	{
		position = 0;
		filenameOffset = 0;
	}

	m_Path = new char[position + 1];
	int filenameOnlyLength = (filenameLength - position) + 1;
	m_Filename = new char[filenameOnlyLength]; 
	m_Display = new char[filenameLength + 5];
	strncpy_s(m_Path, position + 1, m_FullFilename, position);
	m_Path[position] = '\0';
	strcpy_s(m_Filename, filenameOnlyLength, (m_FullFilename + position + filenameOffset));

	m_DataSet = true;
	m_IndexString = NULL;
	m_ViewString = NULL;
	SetIndex(view, index);
	m_BufferID = bufferID;
	m_FileStatus = SAVED;
	m_ScintillaDoc = scintillaDoc;
}

CEditFile::~CEditFile(void)
{
	if (m_DataSet)
	{
		delete[] m_FullFilename;
		delete[] m_Path;
		delete[] m_Filename;

		if (m_IndexString != NULL)
		{
			delete[] m_IndexString;
		}

		if (m_ViewString != NULL)
		{
			delete[] m_ViewString;
		}
	}
}

char*
CEditFile::GetIndexString(bool includeView)
{
	if (m_IndexString == NULL)
	{
		char tmp[16];
		_itoa_s(m_Index + 1, tmp, 16, 10);
		int length = (int)strlen(tmp);

		if (m_View == 1 && includeView)
			length += 4;

		m_IndexString = new char[length + 1];

		if (m_View == 1 && includeView)
		{
			strcpy_s(m_IndexString, length + 1, "[2] ");
			strcat_s(m_IndexString, length + 1, tmp);
		}
		else
		{
			strcpy_s(m_IndexString, length + 1, tmp);
		}
	}

	return m_IndexString;
}

char* 
CEditFile::GetViewString()
{
	if (m_ViewString == NULL)
	{
		m_ViewString = new char[2];
		_itoa_s(m_View + 1, m_ViewString, 2, 10);
	}

	return m_ViewString;
}

void 
CEditFile::SetIndex(int view, int index)
{
	m_View = view;
	m_Index = index;
	if (m_IndexString != NULL)
	{
		delete[] m_IndexString;
		m_IndexString = NULL;
	}

	if (m_ViewString != NULL)
	{
		delete[] m_ViewString;
		m_ViewString = NULL;
	}
}

void 
CEditFile::ClearIndexes()
{
	m_View = -1;
	m_Index = -1;
}

// EOF

