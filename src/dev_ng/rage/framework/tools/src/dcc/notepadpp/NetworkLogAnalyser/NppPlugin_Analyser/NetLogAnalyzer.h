//
// NetLogAnalyser.h 
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef NETWORK_LOG_ANALYSER_H
#define NETWORK_LOG_ANALYSER_H

//Plugin Includes
#include "EditFile.h"
#include "DockingDlg.h"

#define INDEX(x) (x & 0x3FFFFFFF)
#define VIEW(x)  ((x & 0xC0000000) >> 30)

class CNetLogAnalyzer
{
private:
	//To record opened files.
	static EditFileMap*  sm_EditFiles;

	//For the creation of a dockable dialog.
	static CDockingDlg*  sm_DockingDlg;

public:
	//Shutdown resources
	static void  Shutdown();

	//Responsible for showing information about the plugin.
	static void  About();

	//Record Open Files
	static CEditFile*  AddEditFile(const char *filename, int view, int index, int bufferID, void* scintillaDoc);
	static CEditFile*  AddEditFile(int bufferID);
	static void        RemoveEditFile(int bufferID);

	//Switch between tabs.
	static void  Switch2NextTab();
	static void  Switch2PreviousTab();

	//Creates a dockable dialog
	static void  LaunchNetLogAnalyser();

	//Save current document.
	static void SaveDocument();

	//Open a File.
	static void CloseFile(const TCHAR* file);
	static void OpenFile(const TCHAR* file);
	static void OpenFileDialog();

	//Open a new document.
	static void OpenNewDocument();

	//Write a line of text to current scintilla.
	static void  WriteLine(const char* txt);
	static void  AppendLine(const char* txt);
};

#endif // NETWORK_LOG_ANALYSER_H