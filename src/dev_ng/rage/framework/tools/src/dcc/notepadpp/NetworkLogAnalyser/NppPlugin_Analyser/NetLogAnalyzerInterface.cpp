//
// NetLogAnalyzerInterface.cpp 
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

//Plugin Includes
#include "NetLogAnalyzerInterface.h"
#include "NetLogAnalyzer.h"


std::vector< FuncItem >  CNetLogAnalyzerInterface::sm_PluginFuncVector;
NppData  CNetLogAnalyzerInterface::sm_NppData;
tstring  CNetLogAnalyzerInterface::sm_Name;
tstring  CNetLogAnalyzerInterface::sm_ModuleName;
tstring  CNetLogAnalyzerInterface::sm_ModuleBaseName;
HANDLE   CNetLogAnalyzerInterface::sm_HandleModule      = NULL;
HWND     CNetLogAnalyzerInterface::sm_HandleCurrView    = NULL;
bool     CNetLogAnalyzerInterface::sm_RefreshCurrViewH  = true;
bool     CNetLogAnalyzerInterface::sm_IsReady           = false;
SCintillaFunc CNetLogAnalyzerInterface::sm_ScintillaFunc = NULL;
SCintillaPtr  CNetLogAnalyzerInterface::sm_ScintillaPtr  = NULL;


void 
CNetLogAnalyzerInterface::InitPlugin(tstring name, HANDLE hModule)
{
	Assert(sm_Name.empty() );			//Assert that these values haven't already been set.
	Assert(sm_Name.size() <= nbChar );	//Ensure this plugin stays within name size limits.

	sm_Name.assign(name);

	TCHAR tmpName[MAX_PATH];
	::GetModuleFileName((HMODULE)hModule, tmpName, MAX_PATH);
	PathStripPath(tmpName);
	sm_ModuleName.assign(tmpName);
	PathRemoveExtension(tmpName);
	sm_ModuleBaseName.assign(tmpName);

	Assert(hModule);
	sm_HandleModule = hModule;
}

void 
CNetLogAnalyzerInterface::InitScintilla()
{
	HWND currView = CNetLogAnalyzerInterface::HandleCurrView();
	if (AssertVerify(currView))
	{
		sm_ScintillaFunc = (SCintillaFunc)::SendMessage(currView, SCI_GETDIRECTFUNCTION, 0, 0);
		sm_ScintillaPtr = (SCintillaPtr)::SendMessage(currView, SCI_GETDIRECTPOINTER, 0, 0);

		if (!sm_ScintillaFunc)
		{
			throw std::runtime_error("ScintillaEditView::init : SCI_GETDIRECTFUNCTION message failed");
		}

		if (!sm_ScintillaPtr)
		{
			throw std::runtime_error("ScintillaEditView::init : SCI_GETDIRECTPOINTER message failed");
		}
	}
}

void 
CNetLogAnalyzerInterface::ShutdownPlugin()
{
	CNetLogAnalyzer::Shutdown();

	sm_PluginFuncVector.clear();

	sm_NppData._nppHandle = NULL;
	sm_NppData._scintillaMainHandle = NULL;
	sm_NppData._scintillaSecondHandle = NULL;

	sm_Name.clear();
	sm_ModuleName.clear();
	sm_ModuleBaseName.clear();
	
	sm_HandleModule = NULL;
	sm_HandleCurrView = NULL;

	sm_RefreshCurrViewH = true;
	sm_IsReady = false;
}

void 
CNetLogAnalyzerInterface::SetPluginFuncItem(tstring Name, PFUNCPLUGINCMD pFunction, int cmdID, bool init2Check, ShortcutKey* pShKey)
{
	/*
	 *  If three basic entries 'separator', 'help', and 'about' are stored as PluginFuncItems
	 *  and an extension to this namespace provides another FuncItems vector, the two can
	 *  be merged while at the same time ensuring the main plugins menu functions are at
	 *  the bottom (or top) of the plugin's menu in Npp.
	 *  
	 */

	//Notify if length is too long.
	if ( !( Name.length() < nbChar ) )
	{
		::MessageBox(HandleNpp(),
					TEXT("Function name is too long and will be truncated."),
					TEXT("Function Item Name Alert"),
					MB_ICONINFORMATION);
	}

	FuncItem thisFunction;

	Name.copy(thisFunction._itemName, Name.length());
	thisFunction._itemName[ Name.length() ] = '\0';

	thisFunction._cmdID = cmdID;
	thisFunction._pFunc = pFunction;
	thisFunction._init2Check = init2Check;
	thisFunction._pShKey = pShKey;

	sm_PluginFuncVector.push_back(thisFunction);
}

void  
CNetLogAnalyzerInterface::SetInfo(NppData& nppData) 
{
	Assert(!sm_NppData._nppHandle);
	Assert(!sm_NppData._scintillaMainHandle);
	Assert(!sm_NppData._scintillaSecondHandle);

	sm_NppData = nppData; 
}

HWND 
CNetLogAnalyzerInterface::HandleCurrView()
{
	if (sm_RefreshCurrViewH || !sm_IsReady) 
	{
		sm_HandleCurrView = UpdateCurrViewHandle();
	}

	return sm_HandleCurrView;
}

HWND 
CNetLogAnalyzerInterface::UpdateCurrViewHandle()
{
	static HWND hPrevCurrView;

	sm_RefreshCurrViewH = false;

	int currentEdit;
	::SendMessage(sm_NppData._nppHandle, NPPM_GETCURRENTSCINTILLA, 0, (LPARAM)&currentEdit);

	if (currentEdit != -1) 
	{
		hPrevCurrView = ( currentEdit == 0 ) ? ( sm_NppData._scintillaMainHandle ): ( sm_NppData._scintillaSecondHandle );
	}

	return (hPrevCurrView);
}

void  
CNetLogAnalyzerInterface::HandleNotifications(SCNotification* notifyCode)
{
	Assertf(notifyCode, "Null SCNotification");

	switch (notifyCode->nmhdr.code)
	{
	case SCN_MODIFIED:
		if (CNetLogAnalyzerInterface::IsNppReady()) 
		{
			if ((notifyCode->modificationType & (SC_MOD_INSERTTEXT | SC_MOD_DELETETEXT)) 
				|| (notifyCode->modificationType & (SC_MOD_BEFOREDELETE))) 
			{
					CNetLogAnalyzerInterface::HandleCurrViewNeedsUpdate();
			}
		}
		break;

	case NPPN_SHUTDOWN:
		CNetLogAnalyzerInterface::ShutdownPlugin();
		break;

	case NPPN_READY:
		CNetLogAnalyzerInterface::SetNppReady();
		CNetLogAnalyzerInterface::HandleCurrViewNeedsUpdate();
		break;

	case NPPN_TBMODIFICATION:
		break;

	case NPPN_WORDSTYLESUPDATED:
		break;

	case NPPN_BUFFERACTIVATED:
		if (IsNppReady()) 
		{
			CNetLogAnalyzerInterface::HandleCurrViewNeedsUpdate();
		}
		break;

	case NPPN_FILEBEFORECLOSE:
		CNetLogAnalyzerInterface::HandleCurrViewNeedsUpdate();
		break;

	case NPPN_FILESAVED:
		CNetLogAnalyzerInterface::HandleCurrViewNeedsUpdate();
		break;

	case NPPN_FILEOPENED:
		CNetLogAnalyzer::AddEditFile((int)notifyCode->nmhdr.idFrom);
		break;

	case NPPN_FILECLOSED:
		CNetLogAnalyzer::RemoveEditFile((int)notifyCode->nmhdr.idFrom);
		break;
	}
}

int 
CNetLogAnalyzerInterface::ExecSc(UINT Msg, WPARAM wParam, LPARAM lParam) 
{
	return static_cast<int>(sm_ScintillaFunc(sm_ScintillaPtr, static_cast<int>(Msg), static_cast<int>(wParam), static_cast<int>(lParam)));
}
