//
// Text.h 
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef TEXT_H
#define TEXT_H

//Win32 Includes
#include <stdio.h>
#include <list>

//Forward Defenitions
struct Sci_TextToFind;

//Defines
#define MAX_TXT_LEN  1024
#define MARKER_TXT "-------------------------------------------------------------------------------------------------------------"

#define MAX_NUM_CHANNELS 4

#define INVALID_FILE_ATTRIBUTE ((DWORD)-1)

#define INDICATOR_DEFAULT      1
#define INDICATOR_ALTERNATE    2
#define INDICATOR_ACTIVE_MATCH 3

#define COLOR_DARKGREEN   (0x007f00)
#define COLOR_LIGHTBLUE   (0xff0000)
#define COLOR_RED         (0x0000ff)

const COLORREF COLOR_BLUE  = RGB(0,       0, 0xFF);
const COLORREF COLOR_BLACK = RGB(0,       0,    0);
const COLORREF COLOR_WHITE = RGB(0xFF, 0xFF, 0xFF);
const COLORREF COLOR_GREY  = RGB(128,   128,  128);

const int MARK_DELIMITER = 30;
const int MARK_TEXTMARK  = 29;

//Type of Searches
enum eSearchType { FindNormal, FindExtended, FindRegex };

//Search Directions
enum eSearchDir { DirDown, DirUp };

//Search Operations
enum eSearchOperations { Op_FindText, Op_SetTextIndicator, Op_ClearTextIndicator, Op_SetLineIndicator, Op_ClearLineIndicator, Op_SetMarker, Op_ClearMarker };

//Struct that contains all of our search Options
struct SearchOptions
{
	eSearchType m_SearchType;
	eSearchDir m_SearchDir;
	bool  m_IsWholeWord;
	bool  m_IsMatchCase;
	bool  m_IsWrapAround;
	bool  m_IsInSelection;
	bool  m_HighlightText;
	int   m_MarkerType;

	SearchOptions() 
		: m_SearchType(FindRegex)
		, m_SearchDir(DirDown)
		, m_IsWholeWord(true)
		, m_IsMatchCase(true)
		, m_IsWrapAround(true)
		, m_IsInSelection(false)
		, m_HighlightText(false)
		, m_MarkerType(MARK_TEXTMARK)
	{
	}

	int  BuildSearchFlags() const;

	SearchOptions& operator=(const SearchOptions &rhs)
	{
		m_SearchType    = rhs.m_SearchType;
		m_SearchDir     = rhs.m_SearchDir;
		m_IsWholeWord   = rhs.m_IsWholeWord;
		m_IsMatchCase   = rhs.m_IsMatchCase;
		m_IsWrapAround  = rhs.m_IsWrapAround;
		m_IsInSelection = rhs.m_IsInSelection;
		m_HighlightText = rhs.m_HighlightText;
		m_MarkerType    = rhs.m_MarkerType;

		return *this;
	}
};

//Struct that contains the start and end for the search.
struct SearchMark
{
	int  m_Start;
	int  m_End;
};


template <class T>
class TextBuffer
{
public:
	static const int InitSize = MAX_TXT_LEN;
	size_t m_AllocLen;
	T* m_Str;

public:
	TextBuffer() 
		: m_Str(0)
		, m_AllocLen(0)
	{
	}

	~TextBuffer() 
	{
		if(m_AllocLen)
		{
			delete [] m_Str;
		}
	}

	void SizeTo(size_t size) 
	{
		if(m_AllocLen < size)
		{
			if(m_AllocLen)
			{
				delete[] m_Str;
			}

			m_AllocLen = max(size, InitSize);

			m_Str = new T[m_AllocLen];
		}
	}

	void Empty()
	{
		//Routines may return an empty string, with null terminator, 
		//without allocating memory; a pointer to this null character 
		//will be returned in that case.
		static T nullStr = 0;

		if(m_AllocLen == 0)
		{
			m_Str = &nullStr;
		}
		else
		{
			m_Str[0] = 0;
		}
	}

	operator T*() { return m_Str; }

	TextBuffer(const TextBuffer& other) 
	{
		m_AllocLen = 0;

		if(m_AllocLen)
		{
			delete [] m_Str;
		}

		SizeTo(other.m_AllocLen);
		strcpy(m_Str, other.m_Str);
	}
};


//This class is responsible for making string conversions.
class StringConverter
{
private:
	static TextBuffer < char > sm_MultiByteStr;
	static TextBuffer < wchar_t > sm_WideCharStr;

public:

	//Maps a char to a wide-character.
	static wchar_t* Char2Wchar(const char *mbStr, UINT codepage, int lenIn = -1, int *pLenOut = NULL, int *pBytesNotProcessed = NULL);
	static wchar_t* Char2Wchar(const char* mbcs2Convert, unsigned codepage, int* mstart, int* mend);

	//Maps a wide-character to a char.
	static char* Wchar2Char(const wchar_t* wcStr, unsigned codepage, int lenIn = -1, int* pLenOut = NULL);
	static char* Wchar2Char(const wchar_t* wcStr, unsigned codepage, long* mstart, long* mend);
};


//This class is responsible for executing 
//searches within a document.
class CText
{
private:

	//Current Operation being processed.
	static eSearchOperations sm_Operation;

	//Serach options
	static SearchOptions sm_SearchOptions;

public:

	//set Search Options
	static void SetSearchOptions(const SearchOptions opt) { sm_SearchOptions = opt; }

	//Set the operation to be processed.
	static void  SetOperation(const eSearchOperations op) { sm_Operation = op; }

	//Retrieve text.
	//static void GetGenericText(TCHAR *dest, int start, int end, int *mstart, int *mend);
	//static void GetGenericText(TCHAR *dest, int start, int end);

	//Retrieve text.
	static void GetText(char* dest, int start, int end);

	//Search for text in a document line.
	static Sci_CharacterRange  FindInLine(const int line, char* txt2Find);

	//Search for text in all document.
	static void  Find(const char* txt2Find, HANDLE hFile = INVALID_HANDLE_VALUE);
	static void  FindInNetLog(const char* txt2Find);
	static void  FindInGameLog(char* txt2Find);

	//Search for text in document within a range.
	//The return value is the position of the start of the 
	//matching text. If the search fails, the result is -1.
	static int  FindInTarget(const char* txt2Find, const int sciTextLenght, const int targetStart, const int targetEnd);

	//Set the visual appearance of the indicators.
	static void  SetIndicator(const int startPos, const int fillLength, const int indicatorNumber, const int indicatorStyle, const int color);
	static void  ClearIndicator(const int startPos, const int fillLength);

private:

	//Return the lines of a section.
	static bool  GetNetLogSection(const int startRange, const int endRange, int* sectionStart, int* sectionEnd);

	//Retrieve a line of text.
	static void  GetTextLine(char* dest, const int lineNumber);

	//
	static void  ShowSwitchDialogNext();
	static void  ShowSwitchDialogPrevious();
};

#endif // TEXT_H

// EOF

