//
// Text.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

//Win32 Includes
#include <windows.h>
#include <string>
#include <tchar.h>
#include <map>

//Template Includes
#include "Notepad_plus_msgs.h"
#include "PluginInterface.h"
#include "Utf8.h"

//Plugin Includes
#include "Text.h"
#include "UtilityMacros.h"
#include "NetLogAnalyzer.h"
#include "NetLogAnalyzerInterface.h"

int  
SearchOptions::BuildSearchFlags() const 
{
	return	(m_IsWholeWord ? SCFIND_WHOLEWORD : 0)
			| (m_IsMatchCase ? SCFIND_MATCHCASE : 0)
			| (m_SearchType == FindRegex ? SCFIND_REGEXP|SCFIND_POSIX : 0);
};

//-------- String Converter


TextBuffer < char > StringConverter::sm_MultiByteStr;
TextBuffer < wchar_t > StringConverter::sm_WideCharStr;

wchar_t* 
StringConverter::Char2Wchar(const char* mbcs2Convert, unsigned codepage, int lenMbcs, int *pLenWc, int *pBytesNotProcessed)
{
	//Do not process NULL pointer
	if (!mbcs2Convert)
	{
		return NULL;
	}

	// Do not process Empty strings
	if (lenMbcs == 0 || lenMbcs == -1 && mbcs2Convert[0] == 0) 
	{
		sm_WideCharStr.Empty();
		return sm_WideCharStr;
	}

	int bytesNotProcessed = 0;
	int lenWc = 0;

	// If length not specified, simply convert without checking
	if (lenMbcs == -1)
	{
		lenWc = MultiByteToWideChar(codepage, 0, mbcs2Convert, lenMbcs, NULL, 0);
	}
	// Otherwise, test if we are cutting a multi-byte character at end of buffer
	else if(lenMbcs != -1 && codepage == CP_UTF8) // For UTF-8, we know how to test it
	{
		//Get index of last character
		int indexOfLastChar = Utf8::characterStart(mbcs2Convert, lenMbcs-1);

		//If it is not valid we do not process it right now (unless its the only character in string, 
		//to ensure that we always progress, e.g. that bytesNotProcessed < lenMbcs)
		if (indexOfLastChar != 0 && !Utf8::isValid(mbcs2Convert+indexOfLastChar, lenMbcs-indexOfLastChar)) 
		{
			bytesNotProcessed = lenMbcs-indexOfLastChar;
		}

		lenWc = MultiByteToWideChar(codepage, 0, mbcs2Convert, lenMbcs-bytesNotProcessed, NULL, 0);
	}
	else // For other encodings, ask system if there are any invalid characters; note that it will not correctly know if last character is cut when there are invalid characters inside the text
	{
		lenWc = MultiByteToWideChar(codepage, (lenMbcs == -1) ? 0 : MB_ERR_INVALID_CHARS, mbcs2Convert, lenMbcs, NULL, 0);
		if (lenWc == 0 && GetLastError() == ERROR_NO_UNICODE_TRANSLATION)
		{
			// Test without last byte
			if (lenMbcs > 1) lenWc = MultiByteToWideChar(codepage, MB_ERR_INVALID_CHARS, mbcs2Convert, lenMbcs-1, NULL, 0);
			if (lenWc == 0) // don't have to check that the error is still ERROR_NO_UNICODE_TRANSLATION, since only the length parameter changed
			{
				// TODO: should warn user about incorrect loading due to invalid characters
				// We still load the file, but the system will either strip or replace invalid characters (including the last character, if cut in half)
				lenWc = MultiByteToWideChar(codepage, 0, mbcs2Convert, lenMbcs, NULL, 0);
			}
			else
			{
				// We found a valid text by removing one byte.
				bytesNotProcessed = 1;
			}
		}
	}

	if (lenWc > 0)
	{
		sm_WideCharStr.SizeTo(lenWc);
		MultiByteToWideChar(codepage, 0, mbcs2Convert, lenMbcs-bytesNotProcessed, sm_WideCharStr, lenWc);
	}
	else
	{
		sm_WideCharStr.Empty();
	}

	if(pLenWc) *pLenWc = lenWc;
	if(pBytesNotProcessed) *pBytesNotProcessed = bytesNotProcessed;
	return sm_WideCharStr;
}


// "mstart" and "mend" are pointers to indexes in mbcs2Convert,
// which are converted to the corresponding indexes in the returned wchar_t string.
wchar_t * 
StringConverter::Char2Wchar(const char* mbcs2Convert, unsigned codepage, int* mstart, int* mend)
{
	// Do not process NULL pointer
	if (!mbcs2Convert) return NULL;

	int len = MultiByteToWideChar(codepage, 0, mbcs2Convert, -1, NULL, 0);
	if (len > 0)
	{
		sm_WideCharStr.SizeTo(len);
		len = MultiByteToWideChar(codepage, 0, mbcs2Convert, -1, sm_WideCharStr, len);

		if ((size_t)*mstart < strlen(mbcs2Convert) && (size_t)*mend <= strlen(mbcs2Convert))
		{
			*mstart = MultiByteToWideChar(codepage, 0, mbcs2Convert, *mstart, sm_WideCharStr, 0);
			*mend   = MultiByteToWideChar(codepage, 0, mbcs2Convert, *mend, sm_WideCharStr, 0);
			if (*mstart >= len || *mend >= len)
			{
				*mstart = 0;
				*mend = 0;
			}
		}
	}
	else
	{
		sm_WideCharStr.Empty();
		*mstart = 0;
		*mend = 0;
	}

	return sm_WideCharStr;
} 

char* 
StringConverter::Wchar2Char(const wchar_t * wcharStr2Convert, UINT codepage, int lenWc, int *pLenMbcs) 
{
	//Do not process NULL pointer
	if (!wcharStr2Convert)
	{
		return NULL;
	}

	int lenMbcs = WideCharToMultiByte(codepage, 0, wcharStr2Convert, lenWc, NULL, 0, NULL, NULL);

	if (lenMbcs > 0)
	{
		sm_MultiByteStr.SizeTo(lenMbcs);
		WideCharToMultiByte(codepage, 0, wcharStr2Convert, lenWc, sm_MultiByteStr, lenMbcs, NULL, NULL);
	}
	else
	{
		sm_MultiByteStr.Empty();
	}

	if(pLenMbcs)
	{
		*pLenMbcs = lenMbcs;
	}

	return sm_MultiByteStr;
}

char* 
StringConverter::Wchar2Char(const wchar_t * wcharStr2Convert, UINT codepage, long *mstart, long *mend) 
{
	//Do not process NULL pointer
	if (!wcharStr2Convert)
	{
		return NULL;
	}

	int len = WideCharToMultiByte(codepage, 0, wcharStr2Convert, -1, NULL, 0, NULL, NULL);

	if (len > 0)
	{
		sm_MultiByteStr.SizeTo(len);
		len = WideCharToMultiByte(codepage, 0, wcharStr2Convert, -1, sm_MultiByteStr, len, NULL, NULL); // not needed?

		if ((int)*mstart < lstrlenW(wcharStr2Convert) && (int)*mend < lstrlenW(wcharStr2Convert))
		{
			*mstart = WideCharToMultiByte(codepage, 0, wcharStr2Convert, *mstart, NULL, 0, NULL, NULL);
			*mend = WideCharToMultiByte(codepage, 0, wcharStr2Convert, *mend, NULL, 0, NULL, NULL);
			if (*mstart >= len || *mend >= len)
			{
				*mstart = 0;
				*mend = 0;
			}
		}
	}
	else
	{
		sm_MultiByteStr.Empty();
	}

	return sm_MultiByteStr;
}


//-------- CText


eSearchOperations CText::sm_Operation = Op_FindText;
SearchOptions CText::sm_SearchOptions;

void 
CText::GetText(char* dest, int start, int end)
{
	if (AssertVerify(dest))
	{
		TextRange tr;
		tr.chrg.cpMin = start;
		tr.chrg.cpMax = end;
		tr.lpstrText = dest;

		int ret = CNetLogAnalyzerInterface::ExecSc(SCI_GETTEXTRANGE, NULL, reinterpret_cast<LPARAM>(&tr));
		Assert(-1 < ret);
	}
}

//#ifdef UNICODE
//void 
//CText::GetGenericText(TCHAR* dest, int start, int end, int *mstart, int *mend)
//{
//	char* destA = new char[end - start + 1];
//	CText::GetText(destA, start, end);
//
//	int ret = static_cast<int>(::SendMessage(sm_ScintillaPtr, SCI_GETCODEPAGE, NULL, NULL));
//	Assert(-1 < ret);
//
//	const TCHAR *destW = StringConverter::Char2Wchar(destA, ret, mstart, mend);
//	lstrcpy(dest, destW);
//
//	delete [] destA;
//}
//#else
//void 
//CText::GetGenericText(TCHAR* dest, int start, int end, int *, int *) const
//{
//	CText::GetText(dest, start, end);
//}
//#endif // UNICODE

//void 
//CText::GetGenericText(TCHAR* dest, int start, int end)
//{
//	if (AssertVerify(dest) && AssertVerify(sm_ScintillaPtr))
//	{
//#ifdef UNICODE
//		char *destA = new char[end - start + 1];
//		CText::GetText(destA, start, end);
//
//		int ret = static_cast<int>(::SendMessage(sm_ScintillaPtr, SCI_GETCODEPAGE, NULL, NULL));
//		Assert(-1 < ret);
//
//		const TCHAR* destW = StringConverter::Char2Wchar(destA, ret);
//
//		lstrcpy(dest, destW);
//
//		delete [] destA;
//#else
//		CText::GetText(dest, start, end);
//#endif // UNICODE
//	}
//}

Sci_CharacterRange
CText::FindInLine(const int line, char* txt2Find)
{
	Sci_CharacterRange retRange;
	retRange.cpMin = retRange.cpMax = 0;

	if (AssertVerify(txt2Find))
	{
		const unsigned textLen = static_cast<unsigned>(strlen(txt2Find));
		if (AssertVerify(textLen < MAX_TXT_LEN))
		{
			char buffer[MAX_TXT_LEN];
			memset(buffer, '\0', MAX_TXT_LEN);
			sprintf(buffer, "%s", txt2Find);

			//This returns the document position that corresponds with the start of the line.
			int lineStart = CNetLogAnalyzerInterface::ExecSc(SCI_POSITIONFROMLINE, line, 0);
			Assert(-1 < lineStart);

			//This returns the position at the end of the line, before any line end characters.
			int lineEnd = CNetLogAnalyzerInterface::ExecSc(SCI_GETLINEENDPOSITION, line, 0);
			Assert(-1 < lineEnd);

			Sci_TextToFind sciText;
			sciText.lpstrText = &buffer[0];
			sciText.chrg.cpMin = lineStart;
			sciText.chrg.cpMax = lineEnd;

			//This message searches for text in the document.
			int ret = CNetLogAnalyzerInterface::ExecSc(SCI_FINDTEXT, sm_SearchOptions.BuildSearchFlags(), (LPARAM)&sciText);
			if(-1 < ret)
			{
				retRange.cpMin = sciText.chrgText.cpMin;
				retRange.cpMax = sciText.chrgText.cpMax;
			}
		}
	}

	return retRange;
}

void 
CText::Find(const char* txt2Find, HANDLE hFile) 
{
	if (AssertVerify(txt2Find))
	{
		const unsigned textLen = static_cast<unsigned>(strlen(txt2Find));
		if (AssertVerify(textLen < MAX_TXT_LEN))
		{
			int numberOfHits = 0;

			//This returns the document position that corresponds with the start of the line.
			const int docLength = CNetLogAnalyzerInterface::ExecSc(SCI_GETLENGTH, 0, 0);
			Assert(0 < docLength);

			//Set Search flags.
			int ret = CNetLogAnalyzerInterface::ExecSc(SCI_SETSEARCHFLAGS, sm_SearchOptions.BuildSearchFlags(), 0);
			Assert(-1 < ret);

			int line = 0;
			int lineEnd = CNetLogAnalyzerInterface::ExecSc(SCI_LINEFROMPOSITION, docLength, NULL);

			while(line <= lineEnd)
			{
				int linePosStart = CNetLogAnalyzerInterface::ExecSc(SCI_POSITIONFROMLINE, line, NULL);
				int linePosEnd   = CNetLogAnalyzerInterface::ExecSc(SCI_GETLINEENDPOSITION, line, NULL);

				int found = CText::FindInTarget(txt2Find, textLen, linePosStart, linePosEnd);
				if (-1 < found)
				{
					switch (sm_Operation)
					{
					case Op_FindText:
						{
							if (sm_SearchOptions.m_HighlightText)
							{
								CText::SetIndicator(linePosStart, linePosEnd - linePosStart, INDICATOR_DEFAULT, INDIC_ROUNDBOX, COLOR_DARKGREEN);
							}

							//Retrieve the line of text
							char lineBuffer[MAX_TXT_LEN];
							memset(lineBuffer, '\0', MAX_TXT_LEN);
							CText::GetTextLine(lineBuffer, line);

							if (INVALID_HANDLE_VALUE != hFile)
							{
								strncpy(&lineBuffer[strlen(lineBuffer)], "\n", 1);
								unsigned long lpNumberOfBytesWritten = 0;
								BOOL result = ::WriteFile(hFile, lineBuffer, (DWORD)strlen(lineBuffer), &lpNumberOfBytesWritten, NULL);
								Assert(result);
							}
						}
						break;

					case Op_SetTextIndicator:
						{
							CText::SetIndicator(found, textLen, INDICATOR_ACTIVE_MATCH, INDIC_BOX, COLOR_RED);
						}
						break;

					case Op_ClearTextIndicator:
						{
							CText::ClearIndicator(found, textLen);
						}
						break;

					case Op_SetLineIndicator:
						{
							CText::SetIndicator(linePosStart, linePosEnd - linePosStart, INDICATOR_DEFAULT, INDIC_ROUNDBOX, COLOR_LIGHTBLUE);
						}
						break;

					case Op_ClearLineIndicator:
						{
							CText::ClearIndicator(linePosStart, linePosEnd - linePosStart);
						}
						break;

					case Op_SetMarker:
						{
							CNetLogAnalyzerInterface::ExecSc(SCI_MARKERADD, line, sm_SearchOptions.m_MarkerType);
						}
						break;

					case Op_ClearMarker:
						{
							CNetLogAnalyzerInterface::ExecSc(SCI_MARKERDELETE, line, sm_SearchOptions.m_MarkerType);
						}
						break;
					}

				}

				const int previousLine = line;
				found = CText::FindInTarget(txt2Find, textLen, linePosEnd, docLength);
				if(-1 < found)
				{
					line = CNetLogAnalyzerInterface::ExecSc(SCI_LINEFROMPOSITION, found, NULL);
					Assert(previousLine < line);
				}
				else
				{
					break;
				}
			}

			CDebugDisplay::DebugPrintf("Number of hits = \"%d\"", numberOfHits);
		}
	}
}

int 
CText::FindInTarget(const char* txt2Find, const int sciTextLenght, const int targetStart, const int targetEnd)
{
	int ret = -1;

	if (AssertVerify(txt2Find))
	{
		ret = CNetLogAnalyzerInterface::ExecSc(SCI_SETTARGETSTART, targetStart, 0);
		Assert(-1 < ret);

		ret = CNetLogAnalyzerInterface::ExecSc(SCI_SETTARGETEND, targetEnd, 0);
		Assert(-1 < ret);

		ret = CNetLogAnalyzerInterface::ExecSc(SCI_SEARCHINTARGET, (WPARAM)sciTextLenght, (LPARAM)txt2Find);
	}

	return ret;
}

void  
CText::FindInNetLog(const char* txt2Find)
{
	if (AssertVerify(txt2Find))
	{
		const unsigned textLen = static_cast<unsigned>(strlen(txt2Find));
		if (AssertVerify(textLen < MAX_TXT_LEN))
		{
			//This returns the document position that corresponds with the start of the line.
			const int docLength = CNetLogAnalyzerInterface::ExecSc(SCI_GETLENGTH, 0, 0);
			if (AssertVerify(0 < docLength))
			{
				int cp = CNetLogAnalyzerInterface::ExecSc(SCI_GETCODEPAGE, NULL, NULL);

				char path[MAX_PATH];
				TCHAR* szDumpPath = NULL;

				for (int i=0; i<1000; i++)
				{
					memset(path, '\0', MAX_PATH);
					sprintf(path, "%s_%d%s", "C:\\NppLogSearch", i,".txt");

					szDumpPath = StringConverter::Char2Wchar(path, cp);

					DWORD fileAttr = ::GetFileAttributes(szDumpPath);
					if (INVALID_FILE_ATTRIBUTE == fileAttr)
						break;
				}

				if (szDumpPath)
				{
					//Delete the file if it exists
					HRESULT deleteResult = ::DeleteFile(szDumpPath);

					//create the file
					HANDLE hFile = ::CreateFile(szDumpPath, GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL );
					if (INVALID_HANDLE_VALUE != hFile)
					{
						CText::ClearIndicator(0, docLength);

						//Set Search flags.
						int ret = CNetLogAnalyzerInterface::ExecSc(SCI_SETSEARCHFLAGS, sm_SearchOptions.BuildSearchFlags(), 0);
						Assert(-1 < ret);

						int startRange = 0;
						const int endRange = docLength;

						int lineStart = 0;
						int lineEnd = 0;

						//Get the range of a section
						while (CText::GetNetLogSection(startRange, endRange, &lineStart, &lineEnd))
						{
							if (AssertVerify(lineStart < lineEnd))
							{
								const int findInSectionStart = CNetLogAnalyzerInterface::ExecSc(SCI_GETLINEENDPOSITION, lineStart, NULL);
								const int findInSectionEnd = CNetLogAnalyzerInterface::ExecSc(SCI_POSITIONFROMLINE, lineEnd, NULL);

								int found = CText::FindInTarget(txt2Find, textLen, findInSectionStart, findInSectionEnd);
								if (-1 < found)
								{
									if (sm_SearchOptions.m_HighlightText)
									{
										const int posStart = CNetLogAnalyzerInterface::ExecSc(SCI_POSITIONFROMLINE, lineStart, NULL);
										const int posEnd = CNetLogAnalyzerInterface::ExecSc(SCI_GETLINEENDPOSITION, lineEnd, NULL);
										CText::SetIndicator(posStart, posEnd - posStart, INDICATOR_DEFAULT, INDIC_ROUNDBOX, COLOR_DARKGREEN);
									}

									for (int line = lineStart; line < lineEnd; line++)
									{
										//Retrieve the line of text
										char lineBuffer[MAX_TXT_LEN];
										memset(lineBuffer, '\0', MAX_TXT_LEN);
										CText::GetTextLine(lineBuffer, line);

										if (INVALID_HANDLE_VALUE != hFile)
										{
											strncpy(&lineBuffer[strlen(lineBuffer)], "\n", 1);
											unsigned long lpNumberOfBytesWritten = 0;
											BOOL result = ::WriteFile(hFile, lineBuffer, (DWORD)strlen(lineBuffer), &lpNumberOfBytesWritten, NULL);
											Assert(result);
										}
									}
								}

								startRange = CNetLogAnalyzerInterface::ExecSc(SCI_POSITIONFROMLINE, lineEnd, NULL);
							}
						}

						::CloseHandle(hFile);

						//Open Log file
						CNetLogAnalyzer::OpenFile(szDumpPath);
					}
				}
			}
		}
	}
}

void  
CText::FindInGameLog(char* txt2Find)
{
	if (AssertVerify(txt2Find))
	{
		const unsigned textLen = static_cast<unsigned>(strlen(txt2Find));
		if (AssertVerify(textLen < MAX_TXT_LEN))
		{
			//This returns the document position that corresponds with the start of the line.
			const int docLength = CNetLogAnalyzerInterface::ExecSc(SCI_GETLENGTH, 0, 0);
			if (AssertVerify(0 < docLength))
			{
				TCHAR szDumpPath[MAX_PATH];
				memset(szDumpPath, '\0', MAX_PATH);
				lstrcat(szDumpPath, TEXT("C:\\NppLogSearch.txt"));

				//Delete the file if it exists
				::DeleteFile(szDumpPath);

				//create the file
				HANDLE hFile = ::CreateFile(szDumpPath, GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL );
				if (INVALID_HANDLE_VALUE != hFile)
				{
					CText::ClearIndicator(0, docLength);

					//Set Search flags.
					int ret = CNetLogAnalyzerInterface::ExecSc(SCI_SETSEARCHFLAGS, sm_SearchOptions.BuildSearchFlags(), 0);
					Assert(-1 < ret);

					unsigned numberOfChannels = 1;

					char text[MAX_NUM_CHANNELS][MAX_TXT_LEN];
					for(int i=0; i<MAX_NUM_CHANNELS; i++)
					{
						memset(&text[i][0], 0, MAX_TXT_LEN);
					}

					bool commaFound = false;
					unsigned count = 0;
					for (unsigned i=0; i<textLen && numberOfChannels<=MAX_NUM_CHANNELS; i++)
					{
						if (',' == txt2Find[i])
						{
							//char* endC = "]";
							//strncpy(&text[numberOfChannels-1][count], endC, 1);
							numberOfChannels++;
							count = 0;
							commaFound = true;
						}
						else if (((' ' == txt2Find[i]) && commaFound) || ('[' == txt2Find[i]) || (']' == txt2Find[i]))
						{
							continue;
						}
						else
						{
							if (0 == count)
							{
								char* startC = "[";
								strncpy(&text[numberOfChannels-1][count], startC, 1);
								count++;
								strncpy(&text[numberOfChannels-1][count], &txt2Find[i], 1);
							}
							else
							{
								strncpy(&text[numberOfChannels-1][count], &txt2Find[i], 1);
							}

							count++;
							commaFound = false;
						}
					}

					if (numberOfChannels >= 1)
					{
						if (!text[numberOfChannels-1][0])
						{
							numberOfChannels--;
						}
						//else
						//{
						//	char* endC = "]";
						//	strncpy(&text[numberOfChannels-1][count], endC, 1);
						//}

						for (unsigned i=0; i<numberOfChannels; i++)
						{
							CText::Find(&text[i][0], hFile);
						}
					}

					::CloseHandle(hFile);

					//Open Log file
					CNetLogAnalyzer::OpenFile(szDumpPath);
				}
			}
		}
	}
}

bool
CText::GetNetLogSection(const int startRange, const int endRange, int* sectionStart, int* sectionEnd)
{
	*sectionStart = -1;
	*sectionEnd = -1;

	int targetStart = -1;
	int targetEnd = -1;

	const int textLen = (int)strlen(MARKER_TXT);

	//Where it starts
	targetStart = CText::FindInTarget(MARKER_TXT, textLen, startRange, endRange);
	if (-1 != targetStart)
	{
		targetStart = CNetLogAnalyzerInterface::ExecSc(SCI_GETTARGETSTART, NULL, NULL);
		targetEnd = CNetLogAnalyzerInterface::ExecSc(SCI_GETTARGETEND, NULL, NULL);

		//We found a result but outside our range, therefore do not process it.
		if (targetEnd > endRange)
		{
			targetStart = -1;
			return false;
		}

		int NextRange = targetStart + textLen;
		targetEnd = CText::FindInTarget(MARKER_TXT, textLen, NextRange, endRange);
		if (-1 != targetEnd)
		{
			*sectionStart = CNetLogAnalyzerInterface::ExecSc(SCI_LINEFROMPOSITION, targetStart, NULL);
			*sectionEnd = CNetLogAnalyzerInterface::ExecSc(SCI_LINEFROMPOSITION, targetEnd, NULL);

			if (*sectionStart < *sectionEnd)
			{
				return true;
			}
		}
	}

	return false;
}

void 
CText::GetTextLine(char* dest, const int lineNumber)
{
	if (AssertVerify(dest))
	{
		int lineEnd = CNetLogAnalyzerInterface::ExecSc(SCI_GETLINEENDPOSITION, lineNumber, NULL);
		int lineStart = CNetLogAnalyzerInterface::ExecSc(SCI_POSITIONFROMLINE, lineNumber, NULL);
		const int nbChar = lineEnd - lineStart;

		if (nbChar > MAX_TXT_LEN - 3)
		{
			lineEnd = lineStart + 1020;
		}

		//Retrieve the line of text
		CText::GetText(dest, lineStart, lineEnd);
		CDebugDisplay::DebugPrintf(dest);
	}
}

void 
CText::SetIndicator(const int startPos, const int fillLength, const int indicatorNumber, const int indicatorStyle, const int color) 
{
	//Remove Previous indicator
	CText::ClearIndicator(startPos, fillLength);

	//Style
	CNetLogAnalyzerInterface::ExecSc(SCI_INDICSETSTYLE, indicatorNumber, indicatorStyle);

	//Color
	CNetLogAnalyzerInterface::ExecSc(SCI_INDICSETFORE, indicatorNumber, color);

	//Alpha
	CNetLogAnalyzerInterface::ExecSc(SCI_INDICSETALPHA, indicatorNumber, 50);

	//Under
	//CNetLogAnalyzerInterface::ExecuteScintilla(SCI_INDICSETUNDER, indicatorNumber, under);

	CNetLogAnalyzerInterface::ExecSc(SCI_SETINDICATORCURRENT, indicatorNumber, 0);
	CNetLogAnalyzerInterface::ExecSc(SCI_SETINDICATORVALUE, indicatorNumber, 0);
	CNetLogAnalyzerInterface::ExecSc(SCI_INDICATORFILLRANGE, startPos, fillLength);
}

void 
CText::ClearIndicator(const int startPos, const int fillLength)
{
	CNetLogAnalyzerInterface::ExecSc(SCI_SETINDICATORCURRENT, INDICATOR_DEFAULT, 0);
	CNetLogAnalyzerInterface::ExecSc(SCI_SETINDICATORVALUE, 0, 0);
	CNetLogAnalyzerInterface::ExecSc(SCI_INDICATORFILLRANGE, startPos, fillLength);
	CNetLogAnalyzerInterface::ExecSc(SCI_INDICATORCLEARRANGE, startPos, fillLength);

	CNetLogAnalyzerInterface::ExecSc(SCI_SETINDICATORCURRENT, INDICATOR_ALTERNATE, 0);
	CNetLogAnalyzerInterface::ExecSc(SCI_SETINDICATORVALUE, 0, 0);
	CNetLogAnalyzerInterface::ExecSc(SCI_INDICATORFILLRANGE, startPos, fillLength);
	CNetLogAnalyzerInterface::ExecSc(SCI_INDICATORCLEARRANGE, startPos, fillLength);

	CNetLogAnalyzerInterface::ExecSc(SCI_SETINDICATORCURRENT, INDICATOR_ACTIVE_MATCH, 0);
	CNetLogAnalyzerInterface::ExecSc(SCI_SETINDICATORVALUE, 0, 0);
	CNetLogAnalyzerInterface::ExecSc(SCI_INDICATORFILLRANGE, startPos, fillLength);
	CNetLogAnalyzerInterface::ExecSc(SCI_INDICATORCLEARRANGE, startPos, fillLength);
}

// EOF
