//
// Editfile.h 
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef EDITFILE_H
#define EDITFILE_H

//Win32 Includes
#include <map>

//Plugin Includes
#include "UtilityMacros.h"

enum eFileStatus
{
	SAVED
	,UNSAVED
	,READONLY
};

//This class is responsible for file editing.
class CEditFile
{

private:
	char*  m_FullFilename;
	char*  m_DearchFilename;
	char*  m_Path;
	char*  m_Filename;
	char*  m_Display;
	eFileStatus  m_FileStatus;

	bool  m_DataSet;
	int  m_BufferID;
	int  m_View;
	int  m_Index;

	char* m_IndexString;
	char* m_ViewString;

	void* m_ScintillaDoc;

public:
	CEditFile();
	CEditFile(int view, int index, const char*filename, int searchFlags, int bufferID, void* scintillaDoc);
	~CEditFile();

	void   SetIndex(int view, int index);
	void   ClearIndexes();

	char*  GetIndexString(bool includeView);
	char*  GetViewString();

	char*  CEditFile::GetFullFilename() const { Assert(m_FullFilename); return m_FullFilename; }
	char*  CEditFile::GetFilename()     const { Assert(m_Filename); return m_Filename; }
	char*  CEditFile::GetPath()         const { Assert(m_Path); return m_Path; }
	int    CEditFile::GetIndex()        const { return m_Index; }
	int    CEditFile::getView()         const { return m_View; }
	int    CEditFile::GetBufferID()     const { return m_BufferID; }

	eFileStatus  CEditFile::GetFileStatus() const { return m_FileStatus; }
	void  CEditFile::SetFileStatus(eFileStatus status) { m_FileStatus = status; }

	void*  CEditFile::GetScintillaDoc() const { return m_ScintillaDoc; }
	void  CEditFile::SetScintillaDoc(void* scintillaDoc) { Assert(scintillaDoc); m_ScintillaDoc = scintillaDoc; }
};

typedef std::multimap<int, CEditFile*> EditFileMap;
typedef std::multimap<int, CEditFile*>::iterator EditFileMapIterator;

#endif // EDITFILE_H

// EOF

