//
// UtilityMacros.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef UTILITY_MACROS_H
#define UTILITY_MACROS_H

class CDebugDisplay
{
public:
	static void  DebugDisplay(const char *s);
	static void  DebugPrintf(const char *format, ...);
	static void  DebugAssert(const char *c, const char *file, int line);
};

#define Likely(x)	(x)

#ifndef _DEBUG

#define  DEBUG_ONLY(x) 
#define  Assert(x)
#define  Assertf(x,fmt,...)
#define  Debugf(x,fmt,...)
#define  AssertVerify(x) (x)

#else

#define  DEBUG_ONLY(x) x
#define  Assert(x) ((x) ? (void)(0) : CDebugDisplay::DebugAssert(#x, __FILE__, __LINE__))
#define  Assertf(x,fmt,...) do { if (!x) { CDebugDisplay::DebugPrintf("%s: "fmt,#x,##__VA_ARGS__); CDebugDisplay::DebugAssert(#x,__FILE__,__LINE__); } } while(0)
#define  Debugf(x,fmt,...) CDebugDisplay::DebugPrintf(x, __VA_ARGS__)
#define AssertVerify(x) (Likely(x) || (CDebugDisplay::DebugAssert(#x,__FILE__,__LINE__), false))

#endif // _DEBUG

#ifndef NULL
#ifdef __cplusplus
#define NULL    0
#else
#define NULL    ((void *)0)
#endif
#endif // NULL

#endif // UTILITY_MACROS_H