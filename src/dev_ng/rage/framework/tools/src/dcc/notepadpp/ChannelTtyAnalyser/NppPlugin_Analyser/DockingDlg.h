//
// DockingDlg.h 
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef DOCKING_DLG_H
#define DOCKING_DLG_H

//Win32 Includes

//Plugin Includes
#include "DockingDlgInterface.h"
#include "resource1.h"

struct SCNotification;

//This Class is responsible for the creation of a dockable dialog.
class CDockingDlg : public DockingDlgInterface
{
private:
	bool  m_TextHighlight;
	bool  m_MatchCase;
	bool  m_WholeWord;
	bool  m_RegularExpressions;
	int   m_MarkerLine;

public:
	CDockingDlg() 
		: m_TextHighlight(false)
		 ,m_MatchCase(false)
		 ,m_WholeWord(false)
		 ,m_RegularExpressions(false)
		 ,DockingDlgInterface(IDD_PLUGINNETANALYSER_DIALOG)
		 ,m_MarkerLine(0)
	{
	};

	virtual void display(bool toShow = true) const;

	void SetParent(HWND parent2set) { _hParent = parent2set; };
	void InitButtons();

protected:
	virtual BOOL CALLBACK run_dlgProc(UINT message, WPARAM wParam, LPARAM lParam);
	bool Notify(SCNotification *notification);


private:
	//Finds text in the search box.
	void  SearchNetLog(TCHAR* str2Search) const;
	void  SearchGameLog(TCHAR* str2Search) const;

	//Clear document highlight
	void  SetIndicators(TCHAR* str2Search) const;
	void  ClearIndicators() const;

	//Add Markers
	void  AnalyseLog() const;
	void  SetMarkers(TCHAR* str2Search) const;
	void  ClearMarkers() const;
	void  NextMarkers(const int markerType) const;
	void  PreviousMarkers(const int markerType) const;

	//Setup text properties
	void  SetTextHighlight(const bool textHighlight) { m_TextHighlight = textHighlight; }
	void  SetMatchCase(const bool matchCase) { m_MatchCase = matchCase; }
	void  SetWholeWord(const bool wholeWord) { m_WholeWord = wholeWord; }
	void  SetRegularExpressions(const bool regularExpressions) { m_RegularExpressions = regularExpressions; }


};

#endif //DOCKING_DLG_H

// eof


