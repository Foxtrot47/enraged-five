//By Kristine Middlemiss, November 2013, Rockstar Toronto

// Class declaration
#include "multicameraviewer_data.h"

#define MULTICAMERAVIEWER__VERSION__1 "MultiCameraViewerTool1"
#define MULTICAMERAVIEWER__VERSION__2 "MultiCameraViewerTool2"
#define MULTICAMERAVIEWER__VERSION__3 "MultiCameraViewerTool3"
#define MULTICAMERAVIEWER__VERSION__4 "MultiCameraViewerTool4"
#define MULTICAMERAVIEWER__VERSION__5 "MultiCameraViewerTool5"

// MotionBuilder Registration & Implementation.
FBClassImplementation(  MultiCameraViewerData    );
// Register UserObject class
FBUserObjectImplement(  MultiCameraViewerData, "Class objects used to hold MultiCameraViewer tool data.", FB_DEFAULT_SDK_ICON     );                                          
// Register to the asset system
FBElementClassImplementation( MultiCameraViewerData, FB_DEFAULT_SDK_ICON );                  

/************************************************
 *  Constructor.
 ************************************************/
MultiCameraViewerData::MultiCameraViewerData(const char* pName, HIObject pObject, int numCams) : FBUserObject( pName, pObject )
{
    FBClassInit;

	cameraArray.SetCount(numCams);
	for(int i = 0; i < numCams; i++)
	{
		cameraArray.SetAt(i, NULL);	
	}
	refreshRate = 3;
	numRows = 3;
	bUpdateEachFrame = false;
	bTouchMode = false;
	bDoubleBuffered = true;
	bEvaluateMode = true;
}

/************************************************
 *  MotionBuilder Constructor.
 ************************************************/
bool MultiCameraViewerData::FBCreate()
{
    return true;
}

/************************************************
 *  MotionBuilder Destructor.
 ************************************************/
void MultiCameraViewerData::FBDestroy()
{
}

/************************************************
 *	FBX Storage.
 ************************************************/
bool MultiCameraViewerData::FbxStore ( FBFbxObject* pFbxObject, kFbxObjectStore pStoreWhat )
{
	//Loop through cameras and make sure they all still exist in the scene, before saving them
	for (int i = cameraArray.GetCount() - 1; i >= 0; --i)
	{
		FBCamera* pCamera = cameraArray.GetAt(i);
		if (pCamera != NULL)
		{
			if (mSystem.Scene->Cameras.Find(pCamera) == -1)
			{
				cameraArray[i] = NULL;
			}
		}
	}

	//Before we save, trim down the camera list remove null entries on the end.
	for (int i = cameraArray.GetCount() - 1; i >= 0; --i)
	{
		if (cameraArray.GetAt(i) == NULL)
			cameraArray.RemoveAt(i);
		else
			break;
	}

	//Need to store our persistent data
	if (pStoreWhat == kAttributes)
	{
		// Keep this backwards compatible for now, but remove this in later versions
		pFbxObject->FieldWriteBegin(MULTICAMERAVIEWER__VERSION__1);
		{
			//Transpose the camera because in version one we saved it that way 
			FBArrayTemplate<FBCamera*> camerasTranspose = TransposeCameras(cameraArray);
			for(int i = 0; i < 9; i++) 				
			{
				if (camerasTranspose.GetAt(i) == NULL)
					pFbxObject->FieldWriteC("noCamera");
				else
					pFbxObject->FieldWriteC(camerasTranspose.GetAt(i)->LongName) ;
			}

		}
		pFbxObject->FieldWriteEnd();

		pFbxObject->FieldWriteBegin(MULTICAMERAVIEWER__VERSION__2);
		{
			//Store the refresh rate
			pFbxObject->FieldWriteI(refreshRate);
			pFbxObject->FieldWriteI(bTouchMode);
			pFbxObject->FieldWriteI(bUpdateEachFrame);
		}
		pFbxObject->FieldWriteEnd();

		pFbxObject->FieldWriteBegin(MULTICAMERAVIEWER__VERSION__3);
		{
			pFbxObject->FieldWriteI(numRows);
			pFbxObject->FieldWriteI(cameraArray.GetCount());
			for (int i = 0; i < cameraArray.GetCount(); i++)
			{
				//I couldn't get FieldWriteObjectReference to work...me?... MotionBuilder?...who knows!??!?!
				if (cameraArray.GetAt(i) == NULL)
					pFbxObject->FieldWriteC("noCamera");
				//Need to store the name but can't get the Name on a NULL object to have to look for this situation
				else
					pFbxObject->FieldWriteC(cameraArray.GetAt(i)->LongName);
			}

		}
		pFbxObject->FieldWriteEnd();

		pFbxObject->FieldWriteBegin(MULTICAMERAVIEWER__VERSION__4);
		{
			pFbxObject->FieldWriteI(bDoubleBuffered);
		}
		pFbxObject->FieldWriteEnd();

		pFbxObject->FieldWriteBegin(MULTICAMERAVIEWER__VERSION__5);
		{
			pFbxObject->FieldWriteI(bEvaluateMode);
		}
		pFbxObject->FieldWriteEnd();
	}
	return true;
}

/************************************************
 *	FBX Retrieval.
 ************************************************/
bool MultiCameraViewerData::FbxRetrieve( FBFbxObject* pFbxObject, kFbxObjectStore pStoreWhat )
{
	//Need to retrieve our persistent data
	if( pStoreWhat == kAttributes )
    {
		if (pFbxObject->FieldReadBegin(MULTICAMERAVIEWER__VERSION__1))
		{			
			//Retrieve the values in the FBX (old default was 9 cams)
			cameraArray.Clear();
			cameraArray.SetCount(9);
			for(int i = 0; i < 9; i++) 		
			{
				FBString temp = pFbxObject->FieldReadC ();
				
				for (int j=0; j < mSystem.Scene->Cameras.GetCount(); j++ )
				{					
					if (temp == "noCamera")
						cameraArray.SetAt(i, NULL);		
					else if (temp == mSystem.Scene->Cameras[j]->LongName)
						cameraArray.SetAt(i, mSystem.Scene->Cameras[j]);
				}
			}
			//Transpose the camera because in version one we saved it that way 
			cameraArray = TransposeCameras(cameraArray);
			numRows = 3;
			pFbxObject->FieldReadEnd();
		}
		if (pFbxObject->FieldReadBegin(MULTICAMERAVIEWER__VERSION__2))
		{
			//Retrieve the refresh rate
			refreshRate = pFbxObject->FieldReadI();
			bTouchMode = pFbxObject->FieldReadI() != 0;
			bUpdateEachFrame = pFbxObject->FieldReadI() != 0;
		}
		if (pFbxObject->FieldReadBegin(MULTICAMERAVIEWER__VERSION__3))
		{
			numRows = pFbxObject->FieldReadI();
			int cameraCount = pFbxObject->FieldReadI();
			cameraArray.Clear();
			cameraArray.SetCount(cameraCount);
			for (int i = 0; i < cameraCount; i++)
			{
				FBString temp = pFbxObject->FieldReadC();
				if (temp == "noCamera")
				{
					cameraArray.SetAt(i, NULL);
				}
				else
				{
					for (int j = 0; j < mSystem.Scene->Cameras.GetCount(); j++)
					{
						if (temp == mSystem.Scene->Cameras[j]->LongName)
						{
							cameraArray.SetAt(i, mSystem.Scene->Cameras[j]);
							break;
						}
					}
				}
			}
			pFbxObject->FieldReadEnd();

			//After we load, trim down the camera list remove null entries on the end, then build it up to a flush list for the number of rows we are using
			for (int i = cameraArray.GetCount() - 1; i >= 0; --i)
			{
				if (cameraArray.GetAt(i) == NULL)
					cameraArray.RemoveAt(i);
				else
					break;
			}

			// Add cameras up to the new minimum
			int cameraMin = numRows*numRows;
			cameraCount = cameraArray.GetCount();
			for (int i = cameraCount; i < cameraMin; i++)
				cameraArray.InsertAt(i, NULL);

			// Add any extra cameras needed to bring the number of cameras up to a flush list
			cameraCount = cameraArray.GetCount();
			while ((cameraCount % numRows) != 0)
			{
				cameraArray.InsertAt(cameraCount++, NULL);
			}
		}
		if (pFbxObject->FieldReadBegin(MULTICAMERAVIEWER__VERSION__4))
		{
			bDoubleBuffered = pFbxObject->FieldReadI() != 0;
		}
		if (pFbxObject->FieldReadBegin(MULTICAMERAVIEWER__VERSION__5))
		{
			bEvaluateMode = pFbxObject->FieldReadI() != 0;
		}
	}
	return true;
}

// Get the 3x3 transpose of the input array - this cuts off at 9 elements on purpose, used for backwards compatibility saving
FBArrayTemplate<FBCamera*> MultiCameraViewerData::TransposeCameras(FBArrayTemplate<FBCamera*> theCameraArray)
{
	FBArrayTemplate<FBCamera*> transposedCameraArray;
	transposedCameraArray.SetCount(9);

	int origCamCount = theCameraArray.GetCount();
	for (int i = origCamCount; i < 9; i++)
	{
		theCameraArray.InsertAt(i, NULL);
	}

	transposedCameraArray.SetAt(0, theCameraArray.GetAt(0));
	transposedCameraArray.SetAt(1, theCameraArray.GetAt(3));
	transposedCameraArray.SetAt(2, theCameraArray.GetAt(6));
	transposedCameraArray.SetAt(3, theCameraArray.GetAt(1));
	transposedCameraArray.SetAt(4, theCameraArray.GetAt(4));
	transposedCameraArray.SetAt(5, theCameraArray.GetAt(7));
	transposedCameraArray.SetAt(6, theCameraArray.GetAt(2));
	transposedCameraArray.SetAt(7, theCameraArray.GetAt(5));
	transposedCameraArray.SetAt(8, theCameraArray.GetAt(8));

	return transposedCameraArray;
}