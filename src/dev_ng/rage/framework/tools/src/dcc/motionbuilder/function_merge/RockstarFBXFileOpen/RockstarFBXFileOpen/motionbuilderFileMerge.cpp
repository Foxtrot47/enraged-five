///////////////////////////////////////////////////////////////////////////

// ROCKSTAR IO FBX file merge
// test version

///////////////////////////////////////////////////////////////////////////

#include <fbxsdk.h>
#include <fbsdk/fbsdk.h>
#include <fbsdk/fbtypes.h>

#include <stdlib.h>
//#include <algorithm>
#include <vector>
//#include <set>
#include <stdio.h>
#include <string>
#include <iostream>


#if defined(_WIN32) || defined(_WIN64)
#define EXPORT_DLL extern "C" __declspec(dllexport)
#else
#define EXPORT_DLL
#endif

using namespace std;

class MOBUFile
{
	private:
		const char * mfileName;
		const char * mparentNamespace;
		static vector <MOBUFile> numFilesToMerge;
		//more attributes in the future

	public:
		MOBUFile(const char * file, const char * nameSpace){ mfileName = file; mparentNamespace= nameSpace;}

		const char* getFileName() { return this->mfileName; }
		const char* getNamespace() { return this->mparentNamespace; }

		//MOBUFile( const MOBUFile& other );

		static void AddFile(const MOBUFile &addThis);
		static void ResetFileList();
		static const size_t FileListSize();
		static const char * FileName(const int i);
		static const char * FileNameSpace(const int i);
		//static vector <MOBUFile> give(int i);
};


void MOBUFile::AddFile(const MOBUFile &addThis)
{
	numFilesToMerge.push_back(addThis);
}

void MOBUFile::ResetFileList()
{
	numFilesToMerge.erase(numFilesToMerge.begin(), numFilesToMerge.begin()+ numFilesToMerge.size());
}

const size_t MOBUFile::FileListSize()
{
	return numFilesToMerge.size();
}

const char * MOBUFile::FileName(const int i)
{
	return numFilesToMerge[i].mfileName;
}

//returns the namespace of the contents of the files 
const char * MOBUFile::FileNameSpace(const int i)
{
	return numFilesToMerge[i].mparentNamespace;
}

vector <MOBUFile> MOBUFile::numFilesToMerge;

void MergeMeshes(/*const char* file*/ /*string * filesToMerge*/);

// function exposed to python 
EXPORT_DLL void AddFiles(const char* fileName, const char* nameSpace)
{
	if(!fileName || !nameSpace)
	{
		cout<< "Filename or namespace missing" << endl;
		return;
	}

	MOBUFile mergeFile(fileName, nameSpace);
	MOBUFile::AddFile(mergeFile);

	//cout<< MOBUFile::FileListSize()<< endl;
	//delete mergeFile;
}

EXPORT_DLL void ClearFileNameList()
{
	MOBUFile::ResetFileList();
}

EXPORT_DLL void RockstarMBMerge()
{
	if (MOBUFile::FileListSize()==0)
	{
		cout<< "No files given";
		return;
	}

	FBSystem lSystem;
	FBTime start = lSystem.SystemTime;

	FBMergeTransactionBegin();
	MergeMeshes(/*numFilesToMerge*/);
	MOBUFile::ResetFileList();

	FBTime stop = lSystem.SystemTime;
	FBTime difference = stop-start;

	double dif = difference.GetSecondDouble();

	std::cout<< "time diff is: "<< difference.GetSecondDouble()<< endl;

	FBMergeTransactionEnd();
}

//test funcions for the uv

EXPORT_DLL void test()
{
    FBModel*	lModel = new FBModel( "cube" );
    FBMesh*		lMesh = new FBMesh("Cube");
    lModel->Geometry = lMesh;

    // Play with material values for the mesh.
    FBMaterial* lMaterial = new FBMaterial( "OR - Material" );

    //FBColor Diffuse (0.5,0.3,1.0);
    FBColor Diffuse (1.0,0.0,0.0);
    FBColor Ambient ( Diffuse[0]-0.15,Diffuse[1]-0.15,Diffuse[2]-0.15);
    FBColor Specular(0.0,0.0,0.0);

    if (Ambient[0]<0) Ambient[0]=0;
    if (Ambient[1]<0) Ambient[1]=0;
    if (Ambient[2]<0) Ambient[2]=0;

    lMaterial->Ambient	= Ambient;
    lMaterial->Diffuse	= Diffuse;
    lMaterial->Specular	= Specular;

    // Add materials to model.
    lModel->Materials.Add( lMaterial );

    // Add a new texture
    //FBTexture* lTexture = new FBTexture( FBString(mSystem.PathImages)+"/openreality_tooldemo-l.png" );
    // Connect texture to material
    //lMaterial->Diffuse.ConnectSrc( lTexture );

    double len = 10.0;//unit length

    // Object creation
    int P0a,P0e,P0d;
    int P1a,P1b,P1e;
    int P2a,P2b,P2c;
    int P3a,P3c,P3d;
    int P4d,P4e,P4f;
    int P5b,P5e,P5f;
    int P6b,P6c,P6f;
    int P7c,P7d,P7f;

    lMesh->GeometryBegin();
	lMesh->MaterialMappingMode = kFBGeometryMapping_BY_POLYGON_VERTEX;
    // Step 1: Create vertices

    // Because FiLMBOX doesn't yet support multiple UV or multiple Normals per vertex, 
    // the cube's vertices need to be duplicated
    lMesh->VertexInit(24, false, false);

    // Surface A: Y=0 N=(0,-1,0)
    lMesh->VertexNormalSet(0.0,-1.0,0.0);
    lMesh->VertexUVSet(0.0,0.0);
    P0a = lMesh->VertexAdd(	0.0,	0.0,	0.0	);
    lMesh->VertexUVSet(0.0,1.0);
    P1a = lMesh->VertexAdd(	0.0,	0.0,	len	);
    lMesh->VertexUVSet(1.0,1.0);
    P2a = lMesh->VertexAdd(	len,	0.0,	len	);
    lMesh->VertexUVSet(1.0,0.0);
    P3a = lMesh->VertexAdd(	len,	0.0,	0.0	);

    // Surface B: Z=1, N=(0,0,1)
    lMesh->VertexNormalSet(0,0,1);
    lMesh->VertexUVSet(0.0,0.0);
    P1b = lMesh->VertexAdd(	0.0,	0.0,	len	);
    lMesh->VertexUVSet(1.0,0.0);
    P2b = lMesh->VertexAdd(	len,	0.0,	len	);
    lMesh->VertexUVSet(0.0,1.0);
    P5b = lMesh->VertexAdd(	0.0,	len,	len	);
    lMesh->VertexUVSet(1.0,1.0);
    P6b = lMesh->VertexAdd(	len,	len,	len	);

    // Surface C: X=1 N=(1,0,0)
    lMesh->VertexNormalSet(1,0,0);
    P2c = lMesh->VertexAdd(	len,	0.0,	len	);
    P3c = lMesh->VertexAdd(	len,	0.0,	0.0	);
    P6c = lMesh->VertexAdd(	len,	len,	len	);
    P7c = lMesh->VertexAdd(	len,	len,	0.0	);

    // Surface D: Z=0 N=(0,0,-1)
    lMesh->VertexNormalSet(0,0,-1);
    P0d = lMesh->VertexAdd(	0.0,	0.0,	0.0	);
    P3d = lMesh->VertexAdd(	len,	0.0,	0.0	);
    P7d = lMesh->VertexAdd(	len,	len,	0.0	);
    P4d = lMesh->VertexAdd(	0.0,	len,	0.0	);

    // Surface E: X=0 N=(-1,0,0)  
    lMesh->VertexNormalSet(-1,0,0);
    P0e = lMesh->VertexAdd(	0.0,	0.0,	0.0	);
    P1e = lMesh->VertexAdd(	0.0,	0.0,	len	);
    P4e = lMesh->VertexAdd(	0.0,	len,	0.0	);
    P5e = lMesh->VertexAdd(	0.0,	len,	len	);

    // Surface F: Y=1 N=(0,1,0)
    lMesh->VertexNormalSet(0,1,0);
    P4f = lMesh->VertexAdd(	0.0,	len,	0.0	);
    P5f = lMesh->VertexAdd(	0.0,	len,	len	);
    P6f = lMesh->VertexAdd(	len,	len,	len	);
    P7f = lMesh->VertexAdd(	len,	len,	0.0	);


    // Step 2: Create polygons
    lMesh->PolygonBegin();
    lMesh->PolygonVertexAdd(P0a);
    lMesh->PolygonVertexAdd(P3a);
    lMesh->PolygonVertexAdd(P2a);
    lMesh->PolygonVertexAdd(P1a);
    lMesh->PolygonEnd();

    lMesh->PolygonBegin();
    lMesh->PolygonVertexAdd(P1b);
    lMesh->PolygonVertexAdd(P2b);
    lMesh->PolygonVertexAdd(P6b);
    lMesh->PolygonVertexAdd(P5b);
    lMesh->PolygonEnd();

    lMesh->PolygonBegin();
    lMesh->PolygonVertexAdd(P2c);
    lMesh->PolygonVertexAdd(P3c);
    lMesh->PolygonVertexAdd(P7c);
    lMesh->PolygonVertexAdd(P6c);
    lMesh->PolygonEnd();

    lMesh->PolygonBegin();
    lMesh->PolygonVertexAdd(P0d);
    lMesh->PolygonVertexAdd(P4d);
    lMesh->PolygonVertexAdd(P7d);
    lMesh->PolygonVertexAdd(P3d);
    lMesh->PolygonEnd();

    lMesh->PolygonBegin();
    lMesh->PolygonVertexAdd(P1e);
    lMesh->PolygonVertexAdd(P5e);
    lMesh->PolygonVertexAdd(P4e);
    lMesh->PolygonVertexAdd(P0e);
    lMesh->PolygonEnd();

    lMesh->PolygonBegin();
    lMesh->PolygonVertexAdd(P4f);
    lMesh->PolygonVertexAdd(P5f);
    lMesh->PolygonVertexAdd(P6f);
    lMesh->PolygonVertexAdd(P7f);
    lMesh->PolygonEnd();

    lMesh->GeometryEnd();

    // make visible
    lModel->Show = true;
    // Adjust the shading mode.
    lModel->ShadingMode  = kFBModelShadingTexture;
	
}
/*
EXPORT_DLL void createModel()
{
	FBModel* lMobuNode = new FBModel("test");

	FBMesh* lMobuMesh = new FBMesh(lMobuNode->Name);

	lMobuNode->Geometry = lMobuMesh;
	
	lMobuMesh->GeometryBegin();
	lMobuMesh->MaterialMappingMode = kFBGeometryMapping_BY_POLYGON_VERTEX;
	lMobuMesh->GeometryEnd();
}
*/
/*
FBModel*  findModelbyName(char const* name)
{
	FBModelList	lModelList;
	FBSystem lSystem;
	int	i;

	for( i = 0; i < lSystem.SceneRootModel->Children.GetCount(); i++ )
	{
		if( lSystem.SceneRootModel->Children[i] != nullptr )
		{
			char const* child_name = lSystem.SceneRootModel->Children[i]->Name;
			if( strcmp( lSystem.SceneRootModel->Children[i]->Name, name ) == 0 )
			{
				return lSystem.SceneRootModel->Children[i];
			}
			else
			{
				return findModelbyName(lSystem.SceneRootModel->Children[i]->Name);
			}
		}
		return nullptr;
	}
}
*/


FbxAMatrix GetGeometryTransformation(FbxNode* inNode)
{
	const FbxVector4 lT = inNode->GetGeometricTranslation(FbxNode::eSourcePivot);
	const FbxVector4 lR = inNode->GetGeometricRotation(FbxNode::eSourcePivot);
	const FbxVector4 lS = inNode->GetGeometricScaling(FbxNode::eSourcePivot);

	return FbxAMatrix(lT, lR, lS);
}

void CreateMBMeshNew(FBModel* lMobuNode, FbxNode* node)
{
	//Modify Geomentry
	if (!lMobuNode && !node)
	{
		cout<<"There are no nodes around";
		return;
	}

	const FbxGeometry* lGeometry = node->GetGeometry();						
						
	const FbxMesh* lMesh =  node->GetMesh();

	if (!lGeometry && !lMesh)
	{
		cout<<"There is no geometry";
		return;
	}

	FBMesh* lMobuMesh = new FBMesh(lMobuNode->Name);

	lMobuNode->Geometry = lMobuMesh;
	lMobuMesh->MaterialMappingMode = kFBGeometryMapping_BY_POLYGON_VERTEX;

	lMobuMesh->GeometryBegin();

	lMobuMesh->VertexInit(lMesh->GetControlPointsCount(), false, true);
	/*				
    normalArray = []
    uvArray = []
    positionArray = []
        
    //Process control points (vertices) first
    for fbxControlPointIndex in range( fbxMesh.GetControlPointsCount() ):
        fbxVector4_Position = fbxMesh.GetControlPointAt( fbxControlPointIndex )
        positionArray.append(fbxVector4_Position[0])
        positionArray.append(fbxVector4_Position[1])
        positionArray.append(fbxVector4_Position[2])
            
    for fbxPolyIndex in range( fbxMesh.GetPolygonCount() )
	{
        lMobuMesh->PolygonBegin();
        for fbxPolyVertexIndex in range( fbxMesh.GetPolygonSize(fbxPolyIndex) )
		{
            fbxVertexIndex = fbxMesh.GetPolygonVertex( fbxPolyIndex, fbxPolyVertexIndex )
            lMobuMesh->PolygonVertexAdd(fbxVertexIndex)
                
            //Process NORMAL
            fbxVector4_Normal = FbxVector4()
            fbxMesh.GetPolygonVertexNormal( fbxPolyIndex, fbxVertexIndex, fbxVector4_Normal )
            normalArray.append(fbxVector4_Normal[0])
            normalArray.append(fbxVector4_Normal[1])
            normalArray.append(fbxVector4_Normal[2])
                
            //Process UV
            fbxVector2_UV = FbxVector2()
            fbxMesh.GetPolygonVertexUV ( fbxPolyIndex, fbxVertexIndex, "", fbxVector2_UV )
            uvArray.append(fbxVector2_UV[0])
            uvArray.append(fbxVector2_UV[1])
		}
                
        lMobuMesh->PolygonEnd();
	}
        
    lMobuMesh->SetPositionsArray(positionArray);
    lMobuMesh->SetNormalsDirectArray(normalArray);       
    lMobuMesh->SetUVSetDirectArray(uvArray);     

	lMobuMesh->GeometryEnd();		*/
}

//Transfers the information of the mesh from the fbx to motionbuilder
void CreateMBMesh(FBModel* lMobuNode, FbxNode* node)
{
	//Modify Geomentry
	if (!lMobuNode && !node)
	{
		cout<<"There are no nodes around";
		return;
	}

	const FbxGeometry* lGeometry = node->GetGeometry();						
						
	const FbxMesh* lMesh =  node->GetMesh();

	if (!lGeometry && !lMesh)
	{
		cout<<"There is no geometry";
		return;
	}

	FBMesh* lMobuMesh = new FBMesh(lMobuNode->Name);

	lMobuNode->Geometry = lMobuMesh;
	
	lMobuMesh->GeometryBegin();
	lMobuMesh->MaterialMappingMode = kFBGeometryMapping_BY_POLYGON_VERTEX;
	//lMobuMesh->VertexInit(lMesh->GetControlPointsCount(), false, true);
						
	const int lPolygonCount = lMesh->GetPolygonCount();
	FbxVector4* lControlPoints = lMesh->GetControlPoints(); 
							
	//control points																					
	for(int i= 0; i < lMesh->GetControlPointsCount() ; ++i)
	{
		FBVector4d vertex = static_cast <FBVector4d>(lControlPoints[i]);
		FBVertex vertexVector = FBVertex(vertex[0], vertex[1], vertex[2], vertex[3]);								

		lMobuMesh->VertexAdd(vertexVector);								
	}
														 
	//Normals
	const FbxGeometryElementNormal* lNormalElement = lMesh->GetElementNormal();
	
	if (lNormalElement)
	{		
		for (int i = 0; i < lPolygonCount; i++)
		{
			const int lPolygonSize = lMesh->GetPolygonSize(i);
							
			int lIndexByPolygonVertex = 0;
			for (int j = 0; j < lPolygonSize; j++)
			{
				int lNormalIndex = 0;
				//reference mode is direct, the normal index is same as lIndexByPolygonVertex.
				if( lNormalElement->GetReferenceMode() == FbxGeometryElement::eDirect )
					lNormalIndex = lIndexByPolygonVertex;

				//reference mode is index-to-direct, get normals by the index-to-direct
				if(lNormalElement->GetReferenceMode() == FbxGeometryElement::eIndexToDirect)
					lNormalIndex = lNormalElement->GetIndexArray().GetAt(lIndexByPolygonVertex);

				//Got normals of each polygon-vertex.
				FbxVector4 lNormal = lNormalElement->GetDirectArray().GetAt(lNormalIndex);
				FBVector4d normal= static_cast <FBVector4d>(lNormal);
				FBNormal nomralVector = FBNormal(normal[0], normal[1], normal[2], normal[3]);

				const int vertexIndex = lMesh->GetPolygonVertex(i,j);

				lMobuMesh->VertexNormalSet(nomralVector, vertexIndex);

				lIndexByPolygonVertex++;
			}
		}
	}
							
	//UVs
	//Get all UV set names

	FbxStringList lUVSetNameList;
	lMesh->GetUVSetNames(lUVSetNameList);

	if(lUVSetNameList.GetCount() > 0)
	{
		//iterating over all uv sets
		for (int lUVSetIndex = 0; lUVSetIndex < lUVSetNameList.GetCount(); lUVSetIndex++)
		{
			//get lUVSetIndex-th uv set
			const char* lUVSetName = lUVSetNameList.GetStringAt(lUVSetIndex);
			const FbxGeometryElementUV* lUVElement = lMesh->GetElementUV(lUVSetName);

			if(!lUVElement)
				continue;

			// only support mapping mode eByPolygonVertex and eByControlPoint
			if( lUVElement->GetMappingMode() != FbxGeometryElement::eByPolygonVertex &&
				lUVElement->GetMappingMode() != FbxGeometryElement::eByControlPoint )
				return;

			//index array, where holds the index referenced to the uv data
			const bool lUseIndex = lUVElement->GetReferenceMode() != FbxGeometryElement::eDirect;
			const int lIndexCount= (lUseIndex) ? lUVElement->GetIndexArray().GetCount() : 0;

			//iterating through the data by polygon
			const int lPolyCount = lMesh->GetPolygonCount();

			if( lUVElement->GetMappingMode() == FbxGeometryElement::eByControlPoint )
			{
				for( int lPolyIndex = 0; lPolyIndex < lPolyCount; ++lPolyIndex )
				{
					// build the max index array that we need to pass into MakePoly
					const int lPolySize = lMesh->GetPolygonSize(lPolyIndex);
					for( int lVertIndex = 0; lVertIndex < lPolySize; ++lVertIndex )
					{
						FbxVector2 lUVValue;

						//get the index of the current vertex in control points array
						const int lPolyVertIndex = lMesh->GetPolygonVertex(lPolyIndex,lVertIndex);

						//the UV index depends on the reference mode
						const int lUVIndex = lUseIndex ? lUVElement->GetIndexArray().GetAt(lPolyVertIndex) : lPolyVertIndex;

						lUVValue = lUVElement->GetDirectArray().GetAt(lUVIndex);


						FBVector2d uv= static_cast <FBVector2d>(lUVValue);
						FBUV uvVector = FBUV(uv[0], uv[1]);
									
						int vertexIndex = lMesh->GetPolygonVertex(lPolyIndex, lVertIndex);

						lMobuMesh->VertexUVSet(uvVector, vertexIndex);
					}
				}
			}
			else if (lUVElement->GetMappingMode() == FbxGeometryElement::eByPolygonVertex)
			{
				int lPolyIndexCounter = 0;
				for( int lPolyIndex = 0; lPolyIndex < lPolyCount; ++lPolyIndex )
				{
					// build the max index array that we need to pass into MakePoly
					const int lPolySize = lMesh->GetPolygonSize(lPolyIndex);
					for( int lVertIndex = 0; lVertIndex < lPolySize; ++lVertIndex )
					{
						if (lPolyIndexCounter < lIndexCount)
						{
							FbxVector2 lUVValue;

							//the UV index depends on the reference mode
							const int lUVIndex = lUseIndex ? lUVElement->GetIndexArray().GetAt(lPolyIndexCounter) : lPolyIndexCounter;

							lUVValue = lUVElement->GetDirectArray().GetAt(lUVIndex);

							FBVector2d uv= static_cast <FBVector2d>(lUVValue);
							FBUV uvVector = FBUV(uv[0], uv[1]);

							int vertexIndex = lMesh->GetPolygonVertex(lPolyIndex, lVertIndex);

							lMobuMesh->VertexUVSet(uvVector, vertexIndex);

							lPolyIndexCounter++;													 
						}
					}
				}
			}
		}
	}

	//Adding the vertexes to the geometry
	for (int i = 0; i < lPolygonCount; i++)
	{
		const int lPolygonSize = lMesh->GetPolygonSize(i);
							
		lMobuMesh->PolygonBegin();
		for (int j = 0; j < lPolygonSize; j++)
		{
			const int vertexIndex = lMesh->GetPolygonVertex(i,j);
										
			lMobuMesh->PolygonVertexAdd(vertexIndex);										
		}

		lMobuMesh->PolygonEnd();
	}
						
	//lMobuMesh->ComputeVertexNormals(true);

	lMobuMesh->GeometryEnd();	

	//lMobuMesh->MaterialMappingMode = kFBGeometryMapping_BY_POLYGON_VERTEX;
}

//rockstar function
FBModel* RAGEFindModelByName(const char* pModelName)
{
	FBModel* pModel = NULL;

	FBComponentList pList;
	FBFindObjectsByName(pModelName, pList, true, true);

	if(pList.GetCount() == 0)
	{
		FBFindObjectsByName(pModelName, pList, false, true);
		if(pList.GetCount() != 0)
		{
			pModel = (FBModel*)pList.GetAt(0);
		}
	}
	else
	{
		pModel = (FBModel*)pList.GetAt(0);
	}

	return pModel;
}

void AddMBSkinClusters(FBModel* lMobuNode, FbxNode* node, string nameSpace )
{
	if (!lMobuNode && !node)
	{
		cout<<"There are no nodes around";
		return;
	}

	FBCluster* lMOBUcluster = lMobuNode->Cluster;
								
	if (lMOBUcluster != nullptr)
	{
		FBModel* linkModel;
		int lClusterCount = 0;
		const FbxGeometry* lGeometry = node->GetGeometry();

		if (lGeometry != nullptr)
		{
			const FbxCluster* lCluster;

			int lSkinCount= lGeometry->GetDeformerCount(FbxDeformer::eSkin);

			for(int i=0; i!= lSkinCount; ++i)
			{
				lClusterCount = ((FbxSkin *) lGeometry->GetDeformer(i, FbxDeformer::eSkin))->GetClusterCount();
				for (int j = 0; j != lClusterCount; ++j)
				{
					lMOBUcluster->ClusterBegin(j);

					lCluster=((FbxSkin *) lGeometry->GetDeformer(i, FbxDeformer::eSkin))->GetCluster(j);   

					if (lCluster)
					{
						char * clusterName;

						if(lCluster->GetLink() != NULL)
						{
							clusterName = (char *) lCluster->GetLink()->GetName();
						}

						const int lIndexCount = lCluster->GetControlPointIndicesCount();
						const int* lIndices = lCluster->GetControlPointIndices();
						const double* lWeights = lCluster->GetControlPointWeights();

						lMOBUcluster->LinkSetName((char *) lCluster->GetLink()->GetName(), j);
						
						/*
						FBComponentList cl;
						FBFindObjectsByName((char *) lCluster->GetLink()->GetName(), cl, false, false);

						if(cl.GetCount() > 0)
						{
							linkModel = (FBModel*) cl.GetAt(0);
							lMOBUcluster->LinkSetModel(linkModel);
						}											
						*/

						const char* linkName = lCluster->GetLink()->GetName();								
						string FullModelName = "";
						//string modelName= "";
						FullModelName.append(nameSpace);
						//modelName = linkName;
						FullModelName.append( linkName );
						linkModel = RAGEFindModelByName(FullModelName.c_str());
						
						if(linkModel)
						{
							lMOBUcluster->LinkSetModel(linkModel);
						}

						for(int k = 0; k < lIndexCount; k++)
						{
							const int vertexIndex		=	lIndices[k];
							const double vertexWeight	=	(float) lWeights[k];

							lMOBUcluster->VertexAdd(vertexIndex, vertexWeight);
						}
																						
						FbxAMatrix transformMatrix;						
						FbxAMatrix transformLinkMatrix;					
						FbxAMatrix globalBindposeInverseMatrix;
						FbxAMatrix geometryTransform = GetGeometryTransformation(node);

						lCluster->GetTransformMatrix(transformMatrix);

						lCluster->GetTransformLinkMatrix(transformLinkMatrix);
						globalBindposeInverseMatrix = transformLinkMatrix.Inverse() * transformMatrix * geometryTransform;

						lMOBUcluster->VertexSetTransform ((FBVector3d)globalBindposeInverseMatrix.GetT(), (FBVector3d)globalBindposeInverseMatrix.GetR(),FBVector3d(1.0,1.0,1.0)/*(FBVector3d)lMatrix.GetS()*/);
										
						lMOBUcluster->ClusterEnd();	
					}
				}
			}
		}
	}
}



bool IsMesh(const char * meshName, vector <const char *> &meshesNames)
{
	for(size_t i = 0; i< meshesNames.size(); ++i)
	{
		if (strcmp(meshName, meshesNames[i]) == 0)
			return true;
	}
	return false;
}

void BuildHierarchy(FbxNode* pStartNode, vector <const char *> &meshesNames, string nameSpace)
{
    // Write out Node Hierarchy recursively based on the vector of names we receive
	FbxNode* lChildNode = nullptr;
	string FullParentName = "";
	//string parentName = "";
	//parentName = const_cast<char *>(pStartNode->GetName());
	FullParentName.append(nameSpace);
	FullParentName.append(pStartNode->GetName());
	//strcat(FullParentName, pStartNode->GetName());

	int lNodeChildCount = pStartNode->GetChildCount();
	
	while (lNodeChildCount > 0)
	{
		//create the hierachy		
		FBModel* parent_model = RAGEFindModelByName(FullParentName.c_str());

		lNodeChildCount--;
		
		lChildNode		= pStartNode->GetChild (lNodeChildCount);
		FBModel* child	=  nullptr;
		
		char * childName		= nullptr;		
		childName				= const_cast<char *>(lChildNode->GetName());
		child					= RAGEFindModelByName(childName);

		if (child!= nullptr && parent_model!= nullptr && IsMesh(lChildNode->GetName(), meshesNames))
		{
			parent_model->Children.Add(child);
			FBNamespace* parentNameSpace(parent_model->GetOwnerNamespace());
			child->SetOwnerNamespace(parentNameSpace);
			//string childName = child->LongName;
		}

		if(lChildNode->GetChildCount()>0)
		{
			BuildHierarchy(lChildNode, meshesNames, nameSpace);  
		}
	}
}


//Builds the motionbuilder nodes in the current scene
void CreateMBNodes(FbxScene* lScene, vector <const char *> &meshesNames, string nameSpace)
{
	if (!lScene)
	{
		cout<<"There is no scene";
		return;
	}

	int count = lScene->GetSrcObjectCount();
	//create rest of the nodes
	for (int iter = 1; iter < count; ++iter)
	{
		FbxNode* node = lScene->GetNode(iter);

		if (node)
		{
			FbxNodeAttribute* lNodeAttribute = node->GetNodeAttribute();

			if ( lNodeAttribute )
			{
				if ( lNodeAttribute->GetAttributeType() == FbxNodeAttribute::eMesh)
				{
					const char* name = node->GetName();								
					string FullModelName = "";
					string modelName= "";
					FullModelName.append(nameSpace);
					modelName = name;
					FullModelName.append( modelName );

					if(RAGEFindModelByName(FullModelName.c_str()) == nullptr)
					{
						// create node in the mobu scene
						const char* lname = node->GetName();
						meshesNames.push_back(lname);
						FBModel* lMobuNode = new FBModel(lname);	

						CreateMBMesh(lMobuNode, node);
						AddMBSkinClusters(lMobuNode, node, nameSpace);

						// add transformation data
						FBVector3d rot		= node->EvaluateLocalRotation();
						FBVector3d tran		= node->EvaluateLocalTranslation();
						FBVector3d scale	= node->EvaluateLocalScaling();						

						lMobuNode->Rotation		= rot;
						lMobuNode->Translation	= tran;
						lMobuNode->Scaling		= scale;

						lMobuNode->Show			= true; 		
					}
				}
			}
		}
	}
}

bool AreThereBonesInTheScene(FBScene* lScene)
{
	int lCnt = lScene->Components.GetCount();
	for(int i=0; i<lCnt; i++)
	{
		FBComponent* lComponent = lScene->Components[i];
		if(lComponent->Is( FBModelSkeleton::TypeInfo ))
		{
			return true;
		}
	}
	return false;
}

/*
void getNumOfFiles(string * filesToMerge, vector<size_t>& positions)
{
	size_t pos = filesToMerge->find(";", 0);
	while(pos != string::npos)
	{
		positions.push_back(pos);
		pos = filesToMerge->find(";", pos+1);
	}	
}
*/

//Merges into the motionbuilder scene meshes from a a list given by the user
void MergeMeshes(/*const char* file*/ /*vector<MOBUFile*> filesToMerge*/)
{
	// create a SdkManager
	FbxManager* lSdkManager = FbxManager::Create();
	// create an IOSettings object

	FbxScene* lScene = FbxScene::Create( lSdkManager, "" );
	// create an importer
	FbxImporter* lImporter = FbxImporter::Create( lSdkManager, "" );

	// Initialize the importer by providing a filename- this will contain a list of paths that we will receive from the user with all the meshes that need importing, for testing purposes 
	// we are just using a few meshes

	//const bool lImportStatus = lImporter->Initialize("C:\\Users\\brudareanu\\Desktop\\Source.fbx", -1, lSdkManager->GetIOSettings());

	//Do this for all the files that are contained in filesToMerge

	for(int i= 0; i < MOBUFile::FileListSize() ; ++i)
	{	
		const bool lImportStatus = lImporter->Initialize(MOBUFile::FileName(i), -1, lSdkManager->GetIOSettings());

		bool lStatus = false;
		// Import the scene.
		lStatus = lImporter->Import(lScene);
	
		if(lStatus)
		{
			// Check if there is a bone hierarchy in the current motionbuilder scene, if not throw an error
			FBSystem    mSystem;
			if (!AreThereBonesInTheScene(mSystem.Scene))
			{
				cout<<"There are no bones to attach the mesh to";		
			}
			else
			{
				vector <const char *> meshesNames;
				CreateMBNodes(lScene, meshesNames, (string)MOBUFile::FileNameSpace(i));
				if(lScene->GetRootNode())
				{
					//BuildHierarchy(lScene->GetRootNode()->GetChild(0), meshesNames, (string)MOBUFile::FileNameSpace(i));
				}
			}
		}
	}

	lImporter->Destroy();
	lSdkManager->Destroy();
	//return;	
}

int main(int /*argc*/, char** /*argv*/)
{
}