// Need to create a custom manager that is started at the beginning of the lifetime of the application and and last until the application is closed.

// SDK include
#include <QtGui/QApplication>
#include <QtGui/QWidget>
#include <fbsdk/fbsdk.h>
#include <iostream>

#ifdef KARCH_ENV_WIN
    #include <windows.h>
#endif

// Library declaration
FBLibraryDeclare( CustomeManagerMoBuTitle )
{
    FBLibraryRegister( CustomeManagerMoBuTitle );
}
FBLibraryDeclareEnd;

/************************************************
 *  Library functions.
 ************************************************/
bool FBLibrary::LibInit()       { return true; }
bool FBLibrary::LibOpen()       { return true; }
bool FBLibrary::LibReady()      
{
	// We need to run the code here first so when MotionBuilder first opens up the menu bar is accurate.
	for each (QWidget *widget in qApp->allWidgets())
	{
		// We are using Qt to get the windowsTitle and add onto that for this tools functionality.
		QString winTitle = widget->windowTitle();		

		if(winTitle.startsWith("MotionBuilder 2014"))
		{						
			if((winTitle.contains("Project:")) || (winTitle.contains("Tools:")))
			{
				//There is a situation this is added twice, we want to do nothing here, or we get double project and Tools :)
			}
			else
			{			
				// I need to get the RS_PROJECT environment variable from the registry, it's not being set correctly within
				// Sometimes env vars don't update until after log in and out after a tools install so we obtain the RS_PROJECT direct from the registry.
				HKEY hKey;	
				TCHAR lValue[1024]; 
				DWORD bufLen = 1024*sizeof(TCHAR);
				long result;
				long result2;
			
				result = RegOpenKeyEx(HKEY_CURRENT_USER, "Environment", 0, KEY_READ, &hKey);
				if ( result == ERROR_SUCCESS )
				{
					result2 = RegQueryValueEx( hKey, TEXT("RS_PROJECT"),0,0,(LPBYTE) lValue,&bufLen );
					RegCloseKey(hKey);
					if (result2 == ERROR_SUCCESS)
					{
						std::string pProjectVarEnvironVar;
						pProjectVarEnvironVar = std::string(lValue, (size_t)bufLen - 1);
						QString pProjectVar = QString::fromUtf8(pProjectVarEnvironVar.c_str());
						pProjectVar = pProjectVar.toUpper();

						QString pToolsVar = getenv ("RS_TOOLSROOT");

						// Determine if you are in WildWest
						bool usingWildWest = false;
						bool toolsInstalled = true;
						const char * lWW = "wildwest";
						FBConfigFile lConfig("@Application.txt");
						if (strlen(lConfig.Get("Python", "PythonStartup")) > 1)
						{
							if (strstr(lConfig.Get("Python", "PythonStartup"), lWW))
								usingWildWest = true;
						}
						else //If startup path is zero and the Python Count is 
						{
							if (strstr(lConfig.Get("AdditionnalPluginPath", "Count"), "0"))
								toolsInstalled = false;
						}
						if (toolsInstalled == true) //There are no tools we, are in vanilla MotionBuilder mode.
						{
							QString projectMotionBuilderTitle = winTitle;
							if (!pProjectVar.isNull())
								projectMotionBuilderTitle = projectMotionBuilderTitle.append(QString("        Project: %1").arg(pProjectVar));
							// Honestly I don't know if ever one variable will not be set while the other will be, but I guess better safe then sorry.
							if( !pToolsVar.isNull())
								projectMotionBuilderTitle = projectMotionBuilderTitle.append(QString("        Tools: %1").arg(pToolsVar));
							if (usingWildWest == true)
								projectMotionBuilderTitle = projectMotionBuilderTitle.append("        --WORKING IN WILDWEST--");
							widget->setWindowTitle (projectMotionBuilderTitle);
						}
					}
				}
			}
		}
	}
	return true; 
}
bool FBLibrary::LibClose()      { return true; }
bool FBLibrary::LibRelease()    { return true; }