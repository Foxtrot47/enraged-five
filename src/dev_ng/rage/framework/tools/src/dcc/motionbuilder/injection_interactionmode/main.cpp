#include "StdAfx.h"
#include "PyInjection.h"

// Technique using depends.exe
/*
 Load kappcore.dll in depends (or using dumpbin)
 using decorated and undecorated names, extract the KHelpManager class functions you need
*/

//-----------------------------------------------------------------------------
typedef void KHelpManager ;
//#define kHelpManagerTheOne "?TheOne@KHelpManager@@SAAEAV1@XZ"
//static KHelpManager &TheOne () ;
#define kBCore_ManagerGetHelpManager "?BCore_ManagerGetHelpManager@@YAPEAVKHelpManager@@XZ"
typedef KHelpManager * (*fBCore_ManagerGetHelpManager) () ;
fBCore_ManagerGetHelpManager pfBCore_ManagerGetHelpManager =NULL ;
#define kInitInteractionMode "?InitInteractionMode@KHelpManager@@QEAAXXZ"
typedef void (*fInitInteractionMode) (KHelpManager *instance) ;
fInitInteractionMode pfInitInteractionMode =NULL ;
#define kSetInteractionMode "?SetInteractionMode@KHelpManager@@QEAAHPEBD@Z"
typedef int (*fSetInteractionMode) (KHelpManager *instance, const char *pCurrentSelection) ;
fSetInteractionMode pfSetInteractionMode =NULL ;

void SetInteractionMode (const char *configName) {
	HMODULE hKAppCore =::LoadLibrary ("kappcore.dll") ;
	pfBCore_ManagerGetHelpManager =(fBCore_ManagerGetHelpManager)::GetProcAddress (hKAppCore, kBCore_ManagerGetHelpManager) ;
	KHelpManager *pHelpManager =pfBCore_ManagerGetHelpManager () ;
	pfSetInteractionMode =(fSetInteractionMode)::GetProcAddress (hKAppCore, kSetInteractionMode) ;
	pfSetInteractionMode (pHelpManager, configName) ;
    ::FreeLibrary (hKAppCore) ;
}

//-----------------------------------------------------------------------------
typedef void KActionManager ;
#define kObject_GetActionManager "?Object_GetActionManager@@YAPEAVKActionManager@@XZ"
typedef KActionManager * (*fObject_GetActionManager) () ;
fObject_GetActionManager pfObject_GetActionManager =NULL ;
#define kLoadConfiguration "?LoadConfiguration@KActionManager@@QEAA_NPEBD@Z"
typedef bool (*fLoadConfiguration) (KActionManager *instance, const char *pConfigName) ;
fLoadConfiguration pfLoadConfiguration =NULL ;

void LoadConfiguration (const char *configName) {
	HMODULE hObject =::LoadLibrary ("object.dll") ;
	pfObject_GetActionManager =(fObject_GetActionManager)::GetProcAddress (hObject, kObject_GetActionManager) ;
	KActionManager *pActionManager =pfObject_GetActionManager () ;
	pfLoadConfiguration =(fLoadConfiguration)::GetProcAddress (hObject, kLoadConfiguration) ;
	pfLoadConfiguration (pActionManager, configName) ;
    ::FreeLibrary (hObject) ;
}

void LoadConfigurationForFBConfigFile (boost::python::object self, const char *configName, bool bSaveToConfigFile) {
	LoadConfiguration (configName) ;
	if ( bSaveToConfigFile ) {
		// Lazy approach of Python: obj.Set ("Keyboard", "Default", configName, NULL) but not using self :(
		//FBConfigFile configFile ("@Application.txt") ;
		//configFile.Set ("Keyboard", "Default", configName, NULL) ;
		
		// Standard Python way using self
		boost::python::call_method<void> (self.ptr (), "Set", "Keyboard", "Default", configName) ;
	}
}

//- Boost
//! Must exactly match the name of the module, usually it's the same name as $(TargetName).
BOOST_PYTHON_MODULE(pythoninjectioninteractionmode) {
	boost::python::def ("SetInteractionMode", SetInteractionMode) ;
	boost::python::def ("LoadConfiguration", LoadConfiguration) ;
	boost::python::def ("LoadConfigurationForFBConfigFile", LoadConfigurationForFBConfigFile) ;

	PyInjectionInit () ;
}

//- MoBu
FBLibraryDeclare(pythoninjectioninteractionmode) {
	//FBLibraryRegister(MyDeviceClass) ;
	//FBLibraryRegister(MyDeviceLayoutClass) ;
}
FBLibraryDeclareEnd ;

bool FBLibrary::LibInit () { return (true) ; }
bool FBLibrary::LibOpen () { return (true) ; }
bool FBLibrary::LibReady () { return (true) ; }
bool FBLibrary::LibClose ()	{ return (true) ; }
bool FBLibrary::LibRelease () { return (true) ; }

/*

from pythoninjectioninteractionmode import *
from pyfbsdk import *
#LoadConfiguration ("Softimage")

lApplication = FBApplication()
lSystem = FBSystem()
lconfig = FBConfigFile("@Application.txt")
print lconfig.Get("Keyboard", "Default")

lconfig.LoadConfiguration ("MotionBuilder", True)
print lconfig.Get("Keyboard", "Default")

*/