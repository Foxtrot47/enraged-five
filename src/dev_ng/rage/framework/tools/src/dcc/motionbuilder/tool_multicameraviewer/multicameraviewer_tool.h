//By Kristine Middlemiss, November 2013, Rockstar Toronto

#ifndef __MULTICAMERAVIEWER_TOOL_H__
#define __MULTICAMERAVIEWER_TOOL_H__

// SDK include
#include <fbsdk/fbsdk.h>

#include "multicameraviewer_camera_control.h"

#include <vector>

using namespace std;

// You need to define the class here, or the persistent data will not work for your tool.
class MultiCameraViewerData;

// Registration define
#define MULTICAMERAVIEWER__CLASSNAME	MultiCameraViewer
#define MULTICAMERAVIEWER__CLASSSTR		"MultiCameraViewer"

struct Version
{
	Version(int major, int minor, int revision)
	{
		Major = major;
		Minor = minor;
		Revision = revision;
	}
	int Major;
	int Minor;
	int Revision;
};

// Tool template
class MultiCameraViewer : public FBTool
{
	// Tool declaration.
	FBToolDeclare( MultiCameraViewer, FBTool );

public:
	// Construction/Destruction,
	virtual bool FBCreate();		//!< Creation function.
	virtual void FBDestroy();		//!< Destruction function.lS

	bool GetRefreshEachFrame() { return mUpdateEachFrame; }
	void SetRefreshEachFrame(bool bValue) 
	{
		mUpdateEachFrame = bValue; 
		FindPersistentData(true)->bUpdateEachFrame = mUpdateEachFrame;
	}

	bool GetDoubleBuffered() { return mDoubleBuffered; }
	void SetDoubleBuffered(bool bValue)
	{
		mDoubleBuffered = bValue;
		FindPersistentData(true)->bDoubleBuffered = mDoubleBuffered;
	}

	bool GetEvaluateMode() { return mEvaluateMode; }
	void SetEvaluateMode(bool bValue)
	{
		mEvaluateMode = bValue;
		FindPersistentData(true)->bEvaluateMode = mEvaluateMode;
	}

	void	RefreshLiveView();
	void	RefreshView();
private:
	// UI Management
	void	UICreate	();
	void	UIConfigure	();

	// Helper Functions
	void LoadingPersistantData(FBCamera* pPersistantCamera, int pPersistantData );
	void LoadPersistantData();
	void ExecuteChangeDetach(FBString pCameraName);
	void FlushCameraList();
	void AutoLoadCameras(int maxNumberToLoad);
	void SetRowSelection(int row);

	// UI callbacks
	void	EventToolIdle				( HISender pSender, HKEvent pEvent );
	void	EventSceneChange			( HISender pSender, HKEvent pEvent );
	void	EventOnFileNewCompleted		( HISender pSender, HKEvent pEvent );
	void    EventOnFileOpenCompleted	( HISender pSender, HKEvent pEvent );
	void    EventOnFileOpen				( HISender pSender, HKEvent pEvent );
	void	EventConnDataNotify			( HISender pSender, HKEvent pEvent );
	void	ScrollBoxResize				( HISender pSender, HKEvent pEvent );
	void	OptionsButton_Click			( HISender pSender, HKEvent pEvent );
	void	RTESetupButton_Click		( HISender pSender, HKEvent pEvent );
	void	TouchModeButton_Click		( HISender pSender, HKEvent pEvent );
	void	PerformanceSliderChanged	( HISender pSender, HKEvent pEvent );
	void	RowRadioBtns_Click			( HISender pSender, HKEvent pEvent );
	void	ControlListOnChange			( HISender pSender, HKEvent pEvent );

	// Refresh the view
	void	ClearView			();
	void	SetScrollWidth      ();

	// Persistent Data Storage
	MultiCameraViewerData*		FindPersistentData(bool pCreate);
	void						DeletePersistentData();
	FBArrayTemplate<FBCamera*>	GetPersistentData();

private:
	const static int			mMaxNumberOfRows = 4;
	const static int			mHeaderHeight = 30;
	const static int			mFooterHeight = 10;

	// UI Components
	FBLayout					mEntireLayout;
	
	FBScrollBox					mScrollBox;
	FBLayoutRegion				mLayoutCameraNavigator;
	FBButton					mRTESetupButton;
	FBButton					mTouchMode;
	FBSlider					mPerformanceSlider;
	FBLabel						mVersionLabel;
	FBLabel						mPerformanceLabel;
	FBLabel						mPerformanceInfoLabel;
	FBLabel						mRowsInfoLabel;
	FBButton					mRowsRadioBtns[mMaxNumberOfRows];
	FBButton					mOptionsButton;
	FBList						mControlBox;
	FBLabel						mControlLabel;
	
	FBList						mDefaultMB;
    FBSystem					mSystem;
	FBApplication				mApplication;
	FBPlayerControl				mPlayerControl;	

	vector<RSCameraControl*>	mCameras;

	ControlScheme				mControlScheme;
	FBString					mCameraRenameBuffer;
	FBTime						mLastTime;
	int							mLastLive;
	int							mFrameCounter;
	int							mRefreshRate;
	int							mFirstFrameUpdated;
	int							mNumRows;
	int							mCurrentWidth;
	int							mCurrentHeight;
	bool						mUpdateEachFrame;
	bool						mbLoading;
	bool						mDoubleBuffered;
	bool						mEvaluateMode;
};

#endif /* __MULTICAMERAVIEWER_TOOL_H__ */
