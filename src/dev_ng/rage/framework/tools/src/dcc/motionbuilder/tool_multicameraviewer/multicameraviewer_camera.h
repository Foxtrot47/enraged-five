// Encapsulate camera behaviour into class to allow different control schemes easier.
// Fraser Tomison <fraser.tomison@rockstarnorth.com>

#ifndef __MULTICAMERVIEWER_CAMERA_H__
#define __MULTICAMERVIEWER_CAMERA_H__

// SDK include
#include <fbsdk/fbsdk.h>

class RSCamera;

// Currently supported camera control schemes
enum ControlScheme
{
	MotionBuilder,
	Maya,
	Max,
	TotalSchemes
};

// View 3D class
class RSCameraController
{
private:
	RSCamera*	mRsCamera;
	FBCamera*	mCamera;
	ControlScheme mScheme;
	bool mUseTouch;
public:
	// Constructors
	RSCameraController ();
	RSCameraController (FBCamera* pCamera);

	// Destructor
	~RSCameraController ();

	// Initialisation
	void Init ();

	// Input callback.
	void Input (int pMouseX, int pMouseY, FBInputType pAction, int pButtonKey, int pModifier);

	// Attach camera
	void SetCamera (FBCamera* pCamera);

	// Reset Camera Motion
	void Reset ();

	// Override Control Scheme
	void SetControlScheme (ControlScheme scheme);

	bool isBeingControlled ();

	void SetUseTouch(bool bUseTouch){mUseTouch = bUseTouch;}
};


class RSCamera 
{
private:
	FBSystem					mSystem;
	FBApplication				mApplication;
public:
	enum
	{None};
	enum MouseButtons
	{ 
		LMB=1,
		MMB, 
		RMB
	};
protected:
	FBVector2d mStartPos;
	FBVector2d mLastPos;
	FBVector2d mNewPos;
	int mCameraMotion;

	FBModelNull* mlookat;
	FBCamera*	mCamera;
	
public:
	RSCamera();
	virtual ~RSCamera ();

	virtual void Input (int pMouseX, int pMouseY, FBInputType pAction, int pButtonKey, int pModifier)=0;

	void SetCamera (FBCamera* pCamera);

	bool isBeingControlled () {return (mCameraMotion!=None);}

	void Reset ();

	void SetSwitcher (FBInputType pAction, int pButtonKey);

	void Translate (FBMatrix& camMat, double X, double Y, double Z, FBModel* obj);

	void Rotate (FBMatrix& camMat, double X, double Y, double Z, FBModel* obj);

	FBMatrix GetMat(FBModel* obj, FBVector3d* trans=NULL,FBVector3d* rot=NULL,FBVector3d* scale=NULL);
};

class RSCameraMaya : public RSCamera
{
public:
	enum
	{
		None, 
		PointRotating,
		Paning, 
		Zooming
	};
public:
	void Input(int pMouseX, int pMouseY, FBInputType pAction, int pButtonKey, int pModifier);
};

class RSCameraMax : public RSCamera
{
public:
	enum
	{
		None, 
		PointRotating,
		Paning, 
		Zooming,
		LateralRotating,
		ZoomingPaning
	};
private:
	FBMatrix storedLatMat;
public:
	void Input(int pMouseX, int pMouseY, FBInputType pAction, int pButtonKey, int pModifier);
};

class RSCameraMotionbuilder : public RSCamera
{
public:
	enum
	{
		None, 
		PointRotating,
		LateralRotating,
		Rotating,
		Paning,
		Zooming,
		ZoomingPaning
	};
private:
	FBMatrix storedLatMat;
public:
	void Input(int pMouseX, int pMouseY, FBInputType pAction, int pButtonKey, int pModifier);
};

#endif /* __MULTICAMERVIEWER_CAMERA_H__ */
