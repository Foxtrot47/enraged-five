#include "multicameraviewer_options.h"
#include "multicameraviewer_tool.h"

MultiCameraViewerOptionsPopup::MultiCameraViewerOptionsPopup(MultiCameraViewer* multiCamTool)
{
	m_pMultiCamTool = multiCamTool;
	UICreate();
	UIConfigure();
}

void MultiCameraViewerOptionsPopup::UICreate()
{
	int margin = 5;
	Region.X = 200;
	Region.Y = 200;
	Region.Width = 255;
	Region.Height = 150;

	// Options
	AddRegion("ChkFrameRefresh", "ChkFrameRefresh",
		margin, kFBAttachLeft, "NULL", 1.0,
		margin, kFBAttachTop, "NULL", 1.0,
		margin, kFBAttachRight, NULL, 1.0,
		margin * 5, kFBAttachNone, NULL, 1.0);
	SetControl("ChkFrameRefresh", m_ChkFrameRefresh);

	AddRegion("ChkDoubleBuffered", "ChkDoubleBuffered",
		margin, kFBAttachLeft, "NULL", 1.0,
		margin, kFBAttachBottom, "ChkFrameRefresh", 1.0,
		margin, kFBAttachRight, NULL, 1.0,
		margin * 5, kFBAttachNone, NULL, 1.0);
	SetControl("ChkDoubleBuffered", m_ChkDoubleBuffered);

	AddRegion("ChkEvaluateMode", "ChkEvaluateMode",
		margin, kFBAttachLeft, "NULL", 1.0,
		margin, kFBAttachBottom, "ChkDoubleBuffered", 1.0,
		margin, kFBAttachRight, NULL, 1.0,
		margin * 5, kFBAttachNone, NULL, 1.0);
	SetControl("ChkEvaluateMode", m_ChkEvaluateMode);

	// OK/Defautls button
	AddRegion("OkButton", "OkButton",
		margin, kFBAttachLeft, "NULL", 1.0,
		-margin*6, kFBAttachBottom, "NULL", 1.0,
		120, kFBAttachNone, NULL, 1.0,
		margin * 5, kFBAttachNone, NULL, 1.0);
	SetControl("OkButton", m_okButton);

	AddRegion("DefaultsButton", "DefaultsButton",
		margin, kFBAttachRight, "OkButton", 1.0,
		-margin*6, kFBAttachBottom, "NULL", 1.0,
		120, kFBAttachNone, NULL, 1.0,
		margin * 5, kFBAttachNone, NULL, 1.0);
	SetControl("DefaultsButton", m_defaultsButton);
}

void MultiCameraViewerOptionsPopup::UIConfigure()
{
	Caption = "Options";

	m_ChkFrameRefresh.Caption = "Refresh Once Per Frame";
	m_ChkDoubleBuffered.Hint = "When true, the camera views will only update the frame value changes.";
	m_ChkFrameRefresh.Style = kFBCheckbox;
	m_ChkFrameRefresh.OnClick.Add(this, (FBCallback)&MultiCameraViewerOptionsPopup::ChkFrameRefresh_Click);

	m_ChkDoubleBuffered.Caption = "Double Buffered";
	m_ChkDoubleBuffered.Hint = "Indicates if each view is double buffered or not.";
	m_ChkDoubleBuffered.Style = kFBCheckbox;
	m_ChkDoubleBuffered.OnClick.Add(this, (FBCallback)&MultiCameraViewerOptionsPopup::ChkDoubleBuffered_Click);

	m_ChkEvaluateMode.Caption = "Evaluate Mode";
	m_ChkEvaluateMode.Hint = "When true (default), call to Render will perform evaluation. Can speed up render performance but still experimental.";
	m_ChkEvaluateMode.Style = kFBCheckbox;
	m_ChkEvaluateMode.OnClick.Add(this, (FBCallback)&MultiCameraViewerOptionsPopup::ChkEvaluateMode_Click);

	m_okButton.Caption = "Ok";
	m_okButton.OnClick.Add(this, (FBCallback)&MultiCameraViewerOptionsPopup::OkButton_Click);

	m_defaultsButton.Caption = "Restore Defaults";
	m_defaultsButton.OnClick.Add(this, (FBCallback)&MultiCameraViewerOptionsPopup::DefaultsButton_Click);

	LoadSettings();
}

void MultiCameraViewerOptionsPopup::LoadSettings()
{
	m_ChkFrameRefresh.State = (m_pMultiCamTool->GetRefreshEachFrame()) ? 1 : 0;
	m_ChkDoubleBuffered.State = (m_pMultiCamTool->GetDoubleBuffered()) ? 1 : 0;
	m_ChkEvaluateMode.State = (m_pMultiCamTool->GetEvaluateMode()) ? 1 : 0;
}

void MultiCameraViewerOptionsPopup::SetDefaults()
{
	m_ChkFrameRefresh.State = 0;
	m_pMultiCamTool->SetRefreshEachFrame(false);

	m_ChkDoubleBuffered.State = 1;
	m_pMultiCamTool->SetDoubleBuffered(true);

	m_ChkEvaluateMode.State = 1;
	m_pMultiCamTool->SetEvaluateMode(true);
}

void MultiCameraViewerOptionsPopup::ChkFrameRefresh_Click(HISender pSender, HKEvent pEvent)
{
	m_pMultiCamTool->SetRefreshEachFrame(m_ChkFrameRefresh.State == 1);
}

void MultiCameraViewerOptionsPopup::ChkDoubleBuffered_Click(HISender pSender, HKEvent pEvent)
{
	m_pMultiCamTool->SetDoubleBuffered(m_ChkDoubleBuffered.State == 1);
}

void MultiCameraViewerOptionsPopup::ChkEvaluateMode_Click(HISender pSender, HKEvent pEvent)
{
	m_pMultiCamTool->SetEvaluateMode(m_ChkEvaluateMode.State == 1);
}

void MultiCameraViewerOptionsPopup::OkButton_Click(HISender pSender, HKEvent pEvent)
{
	Close(true);
}

void MultiCameraViewerOptionsPopup::DefaultsButton_Click(HISender pSender, HKEvent pEvent)
{
	SetDefaults();
}