#ifndef __CUSTOMMANAGERMOBUTITLEMANAGER_H__
#define __CUSTOMMANAGERMOBUTITLEMANAGER_H__

// SDK include
#include <fbsdk/fbsdk.h>

// Registration defines
#define CUSTOMMANAGERMOBUTITLE__CLASSNAME CustomeManagerMoBuTitle
#define CUSTOMMANAGERMOBUTITLE__CLASSSTR  "CustomeManagerMoBuTitle"

/** Custom Manager Template.*/
class CustomeManagerMoBuTitle : public FBCustomManager
{
    // FiLMBOX box declaration.
    FBCustomManagerDeclare( CustomeManagerMoBuTitle );

public:
    virtual bool FBCreate();   
    virtual void FBDestroy();  

    virtual bool Init();
    virtual bool Open();
    virtual bool Clear();
    virtual bool Close();

private:
	void	FileSaveCallback	( HISender, HKEvent );	//!< \b Observe changes in the scene.FileOpenCompletedCallback
	void	FileSaveCompletedCallback	( HISender, HKEvent );	//!< \b Observe changes in the scene.
	void	FileNewCompletedCallback	( HISender, HKEvent );	//!< \b Observe changes in the scene.
	void	FileOpenCompletedCallback	( HISender, HKEvent );	//!< \b Observe changes in the scene.
	void	FileExitCallback	( HISender, HKEvent );	//!< \b Observe changes in the scene.

	std::string GetProjectPathFromRegistry();
	void	UpdateToolbar(); //All the various callbacks use the one function because we want the same thing to happen for each
	void	CheckFileSize();

private:
	FBApplication mApplication;
};

#endif /* __CUSTOMMANAGERMOBUTITLEMANAGER_H__ */
