// Encapsulate camera behaviour into class to allow different control schemes easier.
// Fraser Tomison <fraser.tomison@rockstarnorth.com>

#include "multicameraviewer_camera.h"
#include <math.h>

#define ROTATION_RADIUS 150
#define PI 3.14159265 

RSCameraController::RSCameraController() : mRsCamera(NULL), mCamera(NULL), mUseTouch(false)
{
	Init();
}

RSCameraController::RSCameraController(FBCamera* pCamera) : mRsCamera(NULL), mCamera(pCamera), mUseTouch(false)
{
	Init();
}

RSCameraController::~RSCameraController()
{
	delete mRsCamera;
}

// Default Motionbuilder control scheme
// This is overridden if a valid scheme is selcted in config
// of if dropbox control is changed on UI.
void RSCameraController::Init()
{
	mScheme = MotionBuilder;
	switch(mScheme)
	{
	case Maya:
		mRsCamera = new RSCameraMaya();
		break;
	case MotionBuilder:
		mRsCamera = new RSCameraMotionbuilder();
		break;
	case Max:
		mRsCamera = new RSCameraMax();
		break;
	}
	mRsCamera->SetCamera(mCamera);
}

// Input passed from view callback, 
void RSCameraController::Input(int pMouseX, int pMouseY, FBInputType pAction, int pButtonKey, int pModifier)
{
	if (mUseTouch)
	{
		// Hack for when using touchscreen mode
		mRsCamera->SetSwitcher( pAction, pButtonKey );		
	}
	else
	{
		// Pass down into our control scheme specific camera
		mRsCamera->Input( pMouseX, pMouseY, pAction, pButtonKey, pModifier );
	}
}

void RSCameraController::SetCamera(FBCamera* pCamera)
{
	mCamera = pCamera;
	mRsCamera->SetCamera(pCamera);
}

void RSCameraController::SetControlScheme(ControlScheme scheme)
{
	if (scheme != mScheme)
	{
		mScheme = scheme;
		delete mRsCamera;
		mRsCamera = NULL;
		switch(mScheme)
		{
		case Maya:
			mRsCamera = new RSCameraMaya();
			break;
		case MotionBuilder:
			mRsCamera = new RSCameraMotionbuilder();
			break;
		case Max:
			mRsCamera = new RSCameraMax();
			break;
		default:
			mRsCamera = new RSCameraMotionbuilder();
			break;
		}
		mRsCamera->SetCamera(mCamera);
		mRsCamera->Reset();
	}
}

// Update Motion, for now
void RSCameraController::Reset()
{
	mRsCamera->Reset();
}

bool RSCameraController::isBeingControlled() 
{ 
	return mRsCamera->isBeingControlled();
}

RSCamera::RSCamera() : mCameraMotion(0),mlookat(NULL),mCamera(NULL)
{
}

RSCamera::~RSCamera()
{
	FBDeleteObjectsByName("multicam_focus");
}

void RSCamera::SetCamera(FBCamera* pCamera)
{
	mCamera = pCamera;
	Reset();
}

void RSCamera::Reset()
{
	mCameraMotion = None;
	if (mCamera)
	{
		mCamera->LookAt = 0;
		mCamera->Animatable=true;
	}
}

void RSCamera::SetSwitcher(FBInputType pAction, int pButtonKey)
{
	if (pButtonKey == LMB && pAction == kFBButtonPress)
	{
		FBCameraSwitcher cameraSwitcher;
		FBProperty* cameraSwitcherIndexProp = cameraSwitcher.PropertyList.Find("Camera Index");
		if (cameraSwitcherIndexProp)
		{
			FBPropertyAnimatable* lPropertyAnimatable = (FBPropertyAnimatable*)cameraSwitcherIndexProp;
			lPropertyAnimatable->SetAnimated( true );

			double lCounter = 1.0; // The Camera Index starts a 1.0 not 0.0
			const char* lProducerName = "Producer";
			for( int i=0; i< mSystem.Scene->Cameras.GetCount(); i++ )
			{
				const char* lCameraName = (const char*)mSystem.Scene->Cameras[i]->Name.AsString();		
				if (strncmp(lProducerName, lCameraName, 8) == 0) // ignore if it's a default MoBu Camera as those are not to be put in the Camera Switcher
						continue;
				else
				{
					if (mSystem.Scene->Cameras[i]->Name == mCamera->Name)
					{
						FBAnimationNode*  lNode = lPropertyAnimatable->GetAnimationNode();
						lNode->SetCandidate ((double*)&lCounter); // Need to Key it this way rather then KeyAdd, as I was getting a really weird keying of the inbetween cameras thing...
						lNode->KeyCandidate ();
						break;
					}
					lCounter++;
				}

			}
		}
	}
}

void RSCamera::Translate(FBMatrix& camMat, double X, double Y, double Z, FBModel* obj)
{
	FBTVector transVec(X,Y,Z);
	FBMatrix transMat;
	FBTranslationToMatrix(transMat,transVec);
	camMat = camMat*transMat;
	FBTVector newTransVec;
	FBMatrixToTranslation(newTransVec,camMat);
	//I don't know why, but Motionbuilder acts funny when you either just set translation vec, or just set translation.
	//So we do both for good measure. Otherwise -> jumpy camera views
	obj->SetVector((FBVector3d)newTransVec,kModelTranslation);
	obj->Translation = (FBVector3d)newTransVec;
}

void RSCamera::Rotate(FBMatrix& camMat, double X, double Y, double Z, FBModel* obj)
{
	FBRVector initRot;
	FBMatrixToRotation(initRot,camMat);
	FBRVector rotVec(X,Y,Z);
	FBMatrix rotMat;
	FBRotationToMatrix(rotMat,rotVec);
	camMat = camMat*rotMat;
	FBRVector newRotVec;
	FBMatrixToRotation(newRotVec,camMat);
	FBGetContinuousRotation(newRotVec,newRotVec,initRot);
	obj->SetVector((FBVector3d)newRotVec,kModelRotation);
	obj->Rotation = (FBVector3d)newRotVec;
}


FBMatrix RSCamera::GetMat(FBModel* obj, FBVector3d* trans,FBVector3d* rot,FBVector3d* scale)
{
	// Get current cam matrix
	FBVector3d camTransVec,camRotVec,camScaleVec;
	FBMatrix camMat;
	if (trans) camTransVec = *trans; else obj->GetVector(camTransVec,kModelTranslation);
	if (rot) camTransVec = *rot; else obj->GetVector(camRotVec,kModelRotation);
	if (scale) camTransVec = *scale; else obj->GetVector(camScaleVec,kModelScaling);
	FBTRSToMatrix(camMat, (FBTVector)camTransVec,(FBRVector)camRotVec,(FBSVector)camScaleVec);
	return camMat;
}

// Handle interactions under maya control scheme
void RSCameraMaya::Input(int pMouseX, int pMouseY, FBInputType pAction, int pButtonKey, int pModifier)
{
	int newMotion = None;
	switch (pButtonKey)
	{
		case LMB: newMotion = PointRotating;break;
		case MMB: newMotion = Paning;break;
		case RMB: newMotion = Zooming;break;
	}

	if (pModifier != kFBKeyAlt || newMotion==None)
	{
		// If at any time alt is not held down, bail out.
		Reset();
		return;
	}

	FBMatrix camMat = GetMat(mCamera);

	switch ( pAction )
	{
	case kFBButtonPress:
		{
			mCameraMotion = newMotion;
			mNewPos = FBVector2d((double)pMouseX,(double)pMouseY);
			mCamera->Animatable=false;
			if (mCameraMotion==PointRotating)
			{
				//choose lookat point infront of camera
				double dist = ROTATION_RADIUS;
				FBTVector transVec(dist,0.0,0.0);
				FBMatrix transMat;
				FBTranslationToMatrix(transMat,transVec);
				FBMatrix lookAtMat = camMat*transMat;
				FBTVector newTransVec;
				FBMatrixToTranslation(newTransVec,lookAtMat);
				FBDeleteObjectsByName("multicam_focus");
				mlookat = new FBModelNull("multicam_focus");
				mlookat->Animatable=false;
				mlookat->Translation = (FBVector3d)newTransVec;
				mCamera->LookAt = mlookat;
			}
			break;
		}
	case kFBButtonRelease:
		{
			if (newMotion==mCameraMotion)
			{			
				Reset();
			}
			break;
		}
	case kFBMotionNotify:
		{
			if (newMotion == None)
			{
				Reset();
				break;
			}
			mLastPos = mNewPos;
			mNewPos = FBVector2d((double)pMouseX,(double)pMouseY);
			double xDiff=mLastPos[0]-mNewPos[0];
			double yDiff=mLastPos[1]-mNewPos[1];
			switch ( mCameraMotion )
			{
			case Paning:
				{
					mCamera->LookAt = 0;
				}
			case PointRotating:
				{	
					//Normalise
					if (mCamera->LookAt != 0)
					{
						FBVector3d camTransVec, lookatVec;
						FBTVector transVec;
						mCamera->GetVector(camTransVec,kModelTranslation);
						mlookat->GetVector(lookatVec,kModelTranslation);
						FBSub(transVec,(FBTVector)camTransVec,(FBTVector)lookatVec);
						double len = FBLength(transVec);
						transVec[0]=(transVec[0]/len)*ROTATION_RADIUS;
						transVec[1]=(transVec[1]/len)*ROTATION_RADIUS;
						transVec[2]=(transVec[2]/len)*ROTATION_RADIUS;
						FBAdd(transVec,(FBTVector)lookatVec,transVec);
						camMat = GetMat(mCamera,(FBVector3d*)&transVec);
						Rotate(camMat,0.0,0.0,0.0,mCamera);
					}	
					Translate(camMat,0.0,-yDiff,xDiff,mCamera);
					break;
				}
			case Zooming:
				{
					mCamera->LookAt = 0;
					Translate(camMat,-yDiff-xDiff,0.0,0.0,mCamera);
					break;
				}
			}
		}
	}
}


// Handle interactions under max control scheme
void RSCameraMax::Input(int pMouseX, int pMouseY, FBInputType pAction, int pButtonKey, int pModifier)
{
	int newMotion = None;
	switch (pModifier)
	{
	case kFBKeyAlt:
		if (pButtonKey==MMB)
			newMotion = PointRotating;
		break;
	case kFBKeyNone:
		if (pButtonKey==MMB)
			newMotion = Paning;
		break;
	case kFBKeyShift:
		if (pButtonKey==RMB)
			newMotion = LateralRotating;
		break;
	case kFBKeyCtrl:
		if (pButtonKey==RMB)
			newMotion = ZoomingPaning;
		break;
	case kFBKeyCtrl+kFBKeyAlt: 
		if (pButtonKey==MMB)
			newMotion = Zooming;
		break;
	}

	if (newMotion==None)
	{
		Reset();
		return;
	}

	FBMatrix camMat = GetMat(mCamera);

	switch ( pAction )
	{
	case kFBButtonPress:
		{
			mCameraMotion = newMotion;
			mNewPos = FBVector2d((double)pMouseX,(double)pMouseY);
			mCamera->Animatable=false;
			if (mCameraMotion==PointRotating || mCameraMotion==LateralRotating)
			{
				//choose lookat point infront of camera
				double dist = ROTATION_RADIUS;
				FBTVector transVec(dist,0.0,0.0);
				FBMatrix transMat;
				FBTranslationToMatrix(transMat,transVec);
				FBMatrix lookAtMat = camMat*transMat;
				FBTVector newTransVec;
				FBMatrixToTranslation(newTransVec,lookAtMat);
				FBDeleteObjectsByName("multicam_focus");
				mlookat = new FBModelNull("multicam_focus");
				mlookat->Animatable=false;
				mlookat->Translation = (FBVector3d)newTransVec;
				mCamera->LookAt = mlookat;
				storedLatMat = camMat;
			}
			break;
		}
	case kFBButtonRelease:
		{
			if (newMotion==mCameraMotion)
			{			
				Reset();
			}
			break;
		}
	case kFBMotionNotify:
		{
			if (newMotion == None)
			{
				Reset();
				break;
			}
			mLastPos = mNewPos;
			mNewPos = FBVector2d((double)pMouseX,(double)pMouseY);
			double xDiff=mLastPos[0]-mNewPos[0];
			double yDiff=mLastPos[1]-mNewPos[1];
			switch ( mCameraMotion )
			{
			case Paning:
				{
					mCamera->LookAt = 0;
				}
			case PointRotating:
				{	
					//Normalise
					if (mCamera->LookAt != 0)
					{
						FBVector3d camTransVec, lookatVec;
						FBTVector transVec;
						mCamera->GetVector(camTransVec,kModelTranslation);
						mlookat->GetVector(lookatVec,kModelTranslation);
						FBSub(transVec,(FBTVector)camTransVec,(FBTVector)lookatVec);
						double len = FBLength(transVec);
						transVec[0]=(transVec[0]/len)*ROTATION_RADIUS;
						transVec[1]=(transVec[1]/len)*ROTATION_RADIUS;
						transVec[2]=(transVec[2]/len)*ROTATION_RADIUS;
						FBAdd(transVec,(FBTVector)lookatVec,transVec);
						camMat = GetMat(mCamera,(FBVector3d*)&transVec);
						Rotate(camMat,0.0,0.0,0.0,mCamera);
					}	
					Translate(camMat,0.0,-yDiff,xDiff,mCamera);
					break;
				}
			case Zooming:
				{
					mCamera->LookAt = 0;
					Translate(camMat,yDiff-xDiff,0.0,0.0,mCamera);
					break;
				}
			case LateralRotating:
				{
					Translate(storedLatMat,0.0,-yDiff,xDiff,mCamera);
					Rotate(camMat,0.0,0.0,0.0,mCamera);
					break;
				}
			case ZoomingPaning:
				{
					mCamera->LookAt = 0;
					Translate(camMat,yDiff*1.5,0.0,xDiff/1.5,mCamera);
					break;
				}
			}
		}
	}
}

// Handle interactions under motionbuilder control scheme
void RSCameraMotionbuilder::Input(int pMouseX, int pMouseY, FBInputType pAction, int pButtonKey, int pModifier)
{
	int newMotion = None;
	switch (pModifier)
	{
	case kFBKeyCtrl:
		if (pButtonKey==LMB)
			newMotion = Zooming;
		else if (pButtonKey==RMB)
			newMotion = ZoomingPaning;
		break;
	case kFBKeyShift: 
		if (pButtonKey==LMB)
			newMotion = Paning;
		else if (pButtonKey==RMB)
			newMotion = LateralRotating;
		break;
	case kFBKeyCtrl+kFBKeyShift: 
		if (pButtonKey==LMB)
			newMotion = PointRotating;
		else if (pButtonKey==RMB)
			newMotion = Rotating;
		break;
	}

	if (!(pModifier&kFBKeyCtrl || pModifier&kFBKeyShift) || newMotion==None)
	{
		Reset();
		return;
	}

	FBMatrix camMat = GetMat(mCamera);

	switch ( pAction )
	{
	case kFBButtonPress:
		{
			mCameraMotion = newMotion;
			mNewPos = FBVector2d((double)pMouseX,(double)pMouseY);
			mCamera->Animatable=false;
			if (mCameraMotion==PointRotating || mCameraMotion==LateralRotating || mCameraMotion==Rotating)
			{
				//choose lookat point infront of camera
				double dist = ROTATION_RADIUS;
				FBTVector transVec(dist,0.0,0.0);
				FBMatrix transMat;
				FBTranslationToMatrix(transMat,transVec);
				FBMatrix lookAtMat = camMat*transMat;
				FBTVector newTransVec;
				FBMatrixToTranslation(newTransVec,lookAtMat);
				FBDeleteObjectsByName("multicam_focus");
				mlookat = new FBModelNull("multicam_focus");
				mlookat->Animatable=false;
				mlookat->Translation = (FBVector3d)newTransVec;
				mCamera->LookAt = mlookat;
				storedLatMat = camMat;
			}
			break;
		}
	case kFBButtonRelease:
		{
			if (newMotion==mCameraMotion)
			{			
				Reset();
			}
			break;
		}
	case kFBMotionNotify:
		{
			if (newMotion == None)
			{
				Reset();
				break;
			}
			mLastPos = mNewPos;
			mNewPos = FBVector2d((double)pMouseX,(double)pMouseY);

			double xDiff=mLastPos[0]-mNewPos[0];
			double yDiff=mLastPos[1]-mNewPos[1];

			switch ( mCameraMotion )
			{
			case Paning:
				{
					mCamera->LookAt = 0;
				}
			case PointRotating:
				{	
					//Normalise
					if (mCamera->LookAt != 0)
					{
						FBVector3d camTransVec, lookatVec;
						FBTVector transVec;
						mCamera->GetVector(camTransVec,kModelTranslation);
						mlookat->GetVector(lookatVec,kModelTranslation);
						FBSub(transVec,(FBTVector)camTransVec,(FBTVector)lookatVec);
						double len = FBLength(transVec);
						transVec[0]=(transVec[0]/len)*ROTATION_RADIUS;
						transVec[1]=(transVec[1]/len)*ROTATION_RADIUS;
						transVec[2]=(transVec[2]/len)*ROTATION_RADIUS;
						FBAdd(transVec,(FBTVector)lookatVec,transVec);
						camMat = GetMat(mCamera,(FBVector3d*)&transVec);
						Rotate(camMat,0.0,0.0,0.0,mCamera);
					}	
					Translate(camMat,0.0,-yDiff,xDiff,mCamera);
					break;
				}
			case LateralRotating:
				{
					Translate(storedLatMat,0.0,-yDiff,xDiff,mCamera);
					Rotate(camMat,0.0,0.0,0.0,mCamera);
					break;
				}
			case Rotating:
				{
					FBVector3d camTransVec, lookatVec, camRotVec;
					FBTVector transVec;
					mCamera->GetVector(camTransVec,kModelTranslation);
					mCamera->GetVector(camRotVec,kModelRotation);
					mlookat->GetVector(lookatVec,kModelTranslation);
					FBSub(transVec,(FBTVector)lookatVec,(FBTVector)camTransVec);
					double len = FBLength(transVec);
					transVec[0]=(transVec[0]/len)*ROTATION_RADIUS;
					transVec[1]=(transVec[1]/len)*ROTATION_RADIUS;
					transVec[2]=(transVec[2]/len)*ROTATION_RADIUS;
					FBAdd(transVec,(FBTVector)camTransVec,transVec);
					FBMatrix lookatMat = GetMat(mlookat,(FBVector3d*)&transVec);

					double X = sin((camRotVec[1] * (PI/180)))*(xDiff/2.0);
					double Y = cos((camRotVec[2] * (PI/180)))*(xDiff/2.0);
					Translate(lookatMat,X,-(yDiff/2.0),Y,mlookat);
					Rotate(camMat,0.0,0.0,0.0,mCamera);
					break;
				}
			case Zooming:
				{
					mCamera->LookAt = 0;
					Translate(camMat,yDiff-xDiff,0.0,0.0,mCamera);
					break;
				}
			case ZoomingPaning:
				{
					mCamera->LookAt = 0;
					Translate(camMat,yDiff*1.5,0.0,xDiff/1.5,mCamera);
					break;
				}
			}
		}
	}
}

