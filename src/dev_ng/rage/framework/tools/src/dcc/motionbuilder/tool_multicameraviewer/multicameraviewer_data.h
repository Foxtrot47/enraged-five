// By Kristine Middlemiss, November 2013, Rockstar Toronto
#ifndef __MULTICAMERVIEWER_DATA_H__
#define __MULTICAMERVIEWER_DATA_H__

// SDK include
#include <fbsdk/fbsdk.h>

// This class will make sure that the data related to the tool is persistent.
class MultiCameraViewerData : public FBUserObject {
    // MotionBuilder declaration.
    FBClassDeclare(MultiCameraViewerData, FBUserObject)
    FBDeclareUserObject(MultiCameraViewerData);

public:
    MultiCameraViewerData(const char *pName=NULL, HIObject pObject=NULL, int numCams = 9);

    // Construction/Destruction
    virtual bool FBCreate();
    virtual void FBDestroy();

    virtual bool FbxStore( FBFbxObject* pFbxObject, kFbxObjectStore pStoreWhat );
    virtual bool FbxRetrieve( FBFbxObject* pFbxObject, kFbxObjectStore pStoreWhat );

	// Data Declaration for storing the Camera, using a MotionBuilder Array Template, that's holds FBCamera Objects
	FBArrayTemplate<FBCamera*> cameraArray;
	int refreshRate;
	int numRows;
	bool bTouchMode;
	bool bUpdateEachFrame;
	bool bDoubleBuffered;
	bool bEvaluateMode;

private:
	FBArrayTemplate<FBCamera*> TransposeCameras(FBArrayTemplate<FBCamera*> cameraArray);

private:
	FBSystem mSystem;

};

#endif
