#ifndef __MULTICAMERVIEWER_OPTIONS_H__ 
#define __MULTICAMERVIEWER_OPTIONS_H__ 

// SDK include
#include <fbsdk/fbsdk.h>

// Forward Declarations
class MultiCameraViewer;

// PURPOSE: A popup for configuring tool options
class MultiCameraViewerOptionsPopup : public FBPopup
{
public:
	MultiCameraViewerOptionsPopup(MultiCameraViewer* multiCamTool);
	~MultiCameraViewerOptionsPopup() {};

	void UICreate();
	void UIConfigure();

private:
	// Options
	FBButton m_ChkFrameRefresh;
	FBButton m_ChkDoubleBuffered;
	FBButton m_ChkEvaluateMode;

	// General UI
	FBButton m_okButton;
	FBButton m_defaultsButton;

	// Option Callbacks
	void ChkFrameRefresh_Click(HISender pSender, HKEvent pEvent);
	void ChkDoubleBuffered_Click(HISender pSender, HKEvent pEvent);
	void ChkEvaluateMode_Click(HISender pSender, HKEvent pEvent);

	// UI
	void OkButton_Click(HISender pSender, HKEvent pEvent);
	void DefaultsButton_Click(HISender pSender, HKEvent pEvent);
	void SetDefaults();
	void LoadSettings();

	// Properties
	MultiCameraViewer* m_pMultiCamTool;
};
#endif