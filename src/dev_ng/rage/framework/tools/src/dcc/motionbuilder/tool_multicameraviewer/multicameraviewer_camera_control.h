#pragma once
// Class for an entire camera control, which builds the UI and holds the view widget too
// Fraser Tomison <fraser.tomison@rockstarnorth.com>

#ifndef __MULTICAMERVIEWER_CAMERA_CONTROL_H__
#define __MULTICAMERVIEWER_CAMERA_CONTROL_H__

// SDK include
#include <fbsdk/fbsdk.h>
#include <object/i/icallback.h>
#include <object/i/iobject.h>

#include "multicameraviewer_view.h"
#include "multicameraviewer_data.h"

class RSCameraControl : ICallback
{
public:
	// Constructors
	RSCameraControl();
	// Destructor
	virtual ~RSCameraControl();

	// Pure Virtual implementations to satisfy ICallback interface
	virtual HIObject	IQuery(kInterfaceID pInterfaceID, int IsLocal = 0) { return this; }
	virtual void		Destroy(int IsLocal = 0) {};

	// Initialisation
	void Init(FBLayout* pLayout, int index, int numRows, int windowSizeWidth, int windowSizeHeight, ControlScheme scheme);
	void ReInit(int numRows, int windowSizeWidth, int windowSizeHeight);
	void UIConfigure();

	//Methods
	void SetLensDropDownList(FBCamera* pCamera);
	void UpdateFOV(FBEventConnectionDataNotify &pEvent);
	void RemoveItemFromCameraList(FBString cameraName);
	bool AddItemToCameraList(FBCamera* pCamera);
	void RenameCameraEntry(FBString oldName, FBString newName);
	
	void Reset(bool bSetData);
	void AddCamera(FBCamera* pCamera);

	void MainWindowResized(int newWidth, int newHeight, int numRows);
	int  GetControlWidth();

	//Passthroughs
	void SetControlScheme(ControlScheme val) { mView.SetControlScheme(val); }
	void Refresh(bool bNow) { mView.Refresh(bNow); }
	void SetUseTouch(bool val) { mView.SetUseTouch(val); }
	void SetIsLive(bool val) { mLiveButton.State = 0; EventLiveButtonPressed(NULL, NULL); }
	bool IsBeingControlled() { return mView.isBeingControlled(); }
	bool IsActive() { return mContainer.Items.GetCount() > 0; }
	bool IsLive() { return mIsLive; }
	void SetDoubleBuffered(bool bValue);
	void SetEvaluateMode(bool bValue) { mView.SetEvaluateMode(bValue); }

	bool HasBeenRefreshed() { return mView.HasBeenRefreshed(); }
	FBCamera* GetCamera() { return (IsActive())? (FBCamera*)mContainer.Items.GetReferenceAt(0) : NULL; }

private:
	// Persistent Data Storage
	MultiCameraViewerData*		FindPersistentData(bool pCreate);
	FBArrayTemplate<FBCamera*>	GetPersistentData();
	void						SetPersistentData(FBCamera* pCamera);
	void						CleanUI();
	void						InitUI(int numRows, int width, int height);

	// Callbacks
	void EventContainerDragAndDrop(HISender pSender, HKEvent pEvent);
	void EventContainerDblClickSet(HISender pSender, HKEvent pEvent);
	void EventCameraListOnChange(HISender pSender, HKEvent pEvent);
	void EventLensListOnChange(HISender pSender, HKEvent pEvent);
	void EventLiveButtonPressed(HISender pSender, HKEvent pEvent);

private:
	// Properties
	FBLayout*			pParentLayout;

	ORView3D			mView;
	FBList				mCameraList;
	FBList				mLensList;
	FBVisualContainer	mContainer;
	FBButton			mLiveButton;

	FBSystem			mSystem;

	int					mIndex;

	float				mHeightModifier;
	float				mWidthModifier;

	bool				mIsLive;
	bool				mIsCustomCamera;

	// Control names
	char				cContainerName[256];
	char				cViewName[256];
	char				cCameraListName[256];
	char				cLensListName[256];
	char				cLiveButtonName[256];

	// Consts
	static const int	mMargin = 10;
};

#endif /* __MULTICAMERVIEWER_CAMERA_CONTROL_H__ */
