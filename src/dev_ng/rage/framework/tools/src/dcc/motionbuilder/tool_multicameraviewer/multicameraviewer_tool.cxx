// By Kristine Middlemiss, November 2013, Rockstar Toronto

// Class declaration
#include <math.h> 
#include <string>
#ifndef MOBU_2014
#include "fbsdk/fbevaluatemanager.h"
#endif


#include "multicameraviewer_tool.h"
#include "multicameraviewer_data.h"
#include "multicameraviewer_options.h"

#pragma warning(disable : 4996)

#ifdef KARCH_ENV_WIN
	#include <windows.h>
#endif

// Registration defines
#define MULTICAMERAVIEWER__CLASS	MULTICAMERAVIEWER__CLASSNAME
#define MULTICAMERAVIEWER__LABEL	"Multi Camera Viewer"
#define MULTICAMERAVIEWER__DESC	    "Multi Camera Viewer"

// Implementation and registration
FBToolImplementation(	MULTICAMERAVIEWER__CLASS	);
FBRegisterTool		(	MULTICAMERAVIEWER__CLASS,
						MULTICAMERAVIEWER__LABEL,
						MULTICAMERAVIEWER__DESC,
						FB_DEFAULT_SDK_ICON	);


static const Version s_version = Version(2, 0, 1);
static MultiCameraViewer *s_pCurrentViewer = NULL;

//Far from the best solution, must match the enum in multicameraviewer_camera.h !!!!
const char* ControlSchemeNames[] = 
{
	"MotionBuilder",
	"Maya",
	"3ds Max"
};

void PreRenderCallback(FBEvaluateInfo* pEvaluteInfo)
{
	if (s_pCurrentViewer != NULL)
	{
		s_pCurrentViewer->RefreshLiveView();
	}
}

/************************************************
 *	Constructor.
 ************************************************/
bool MultiCameraViewer::FBCreate()
{
	s_pCurrentViewer = this;

	mCurrentWidth = 1620;
	mCurrentHeight = 1058;
	StartSize[0] = mCurrentWidth;
	StartSize[1] = mCurrentHeight;

	((FBScene*)mSystem.Scene)->OnChange.Add (this, (FBCallback) &MultiCameraViewer::EventSceneChange ); //Monitor the scene for added, renamed and/or deleted cameras.
	
	mbLoading = true;
	mLastTime = mSystem.LocalTime;
	// Default to 3 rows
	mNumRows = 3;
	mControlScheme = MotionBuilder;
	mLastLive = -1;

	// Manage UI
	UICreate	();
	UIConfigure	();

	FBEvaluateManager::TheOne().RegisterEvaluationGlobalFunction((kFBEvaluationGlobalFunctionCallback)&PreRenderCallback, kFBGlobalEvalCallbackSyn);
	// Add tool callbacks
	OnIdle.Add ( this, (FBCallback) &MultiCameraViewer::EventToolIdle ); // Update the viewer when playing, recording & moving camera.

	// Register all the callbacks I am going to use for this tool, when the tools is opened, they call back only have the life line of the tool being open.
	mApplication.OnFileNewCompleted.Add ( this, (FBCallback)&MultiCameraViewer::EventOnFileNewCompleted ); //Clear tool out when file new is called.
    mApplication.OnFileOpenCompleted.Add ( this, (FBCallback)&MultiCameraViewer::EventOnFileOpenCompleted ); //Populates the tool based on persistent data or default camera names on file open.
	mApplication.OnFileOpen.Add(this, (FBCallback)&MultiCameraViewer::EventOnFileOpen);
	FBSystem::TheOne().OnConnectionDataNotify.Add(this, (FBCallback)&MultiCameraViewer::EventConnDataNotify); // Monitors the FieldOfView property on cameras and if this changes and the camera is in the tool, it updates the UI. 

	// When using a normal HUD (which uses ScaleForm) with this tool, the combination causes the ScaleForm HUD to flicker consistently, 
	// as a workaround Autodesk provided fix in MotionBuilder Hot Fix 22, where we need to add into the Application.txt [Display] section a flag that forces the HUD to use OpenGL rather then ScaleForm.
	const char * lGetOpenGLHUD = "No";
	const char * lUsingOpenGLHUD = "No";
	FBConfigFile lConfig("@Application.txt");
	lGetOpenGLHUD = lConfig.Get("Display", "UseOpenGLTextHUD");
	if ((lGetOpenGLHUD == NULL) || (strcmp(lGetOpenGLHUD,lUsingOpenGLHUD)==0))
	{
		lConfig.Set("Display", "UseOpenGLTextHUD", "Yes");
		FBMessageBox("Warning", "Currently you are using the ScaleForm HUD, for best performance with the Multi Camera Viewer, MotionBuilder is switching to the OpenGL HUD.\nPlease restart MotionBuilder to have this take affect.", "Ok");
	}
	return true;
}
/************************************************
 *	Destruction function.
 ************************************************/
void MultiCameraViewer::FBDestroy()
{
	if (s_pCurrentViewer == this)
	{
		s_pCurrentViewer = NULL;
	}

	for (int i = 0; i < mCameras.size(); i++)
	{
		delete mCameras[i];
		mCameras[i] = NULL;
	}
	mCameras.clear();

	// Remove tool callbacks
	mEntireLayout.OnResize.Remove(this, (FBCallback)&MultiCameraViewer::ScrollBoxResize);
	FBEvaluateManager::TheOne().UnregisterEvaluationGlobalFunction((kFBEvaluationGlobalFunctionCallback)&PreRenderCallback, kFBGlobalEvalCallbackSyn);
	OnIdle.Remove ( this, (FBCallback) &MultiCameraViewer::EventToolIdle);
	mApplication.OnFileOpen.Remove(this, (FBCallback)&MultiCameraViewer::EventOnFileOpen);
	mApplication.OnFileNewCompleted.Remove ( this, (FBCallback)&MultiCameraViewer::EventOnFileNewCompleted);
    mApplication.OnFileOpenCompleted.Remove ( this, (FBCallback)&MultiCameraViewer::EventOnFileOpenCompleted);
	((FBScene*)mSystem.Scene)->OnChange.Remove (this, (FBCallback) &MultiCameraViewer::EventSceneChange);
	FBSystem::TheOne().OnConnectionDataNotify.Remove(this, (FBCallback)&MultiCameraViewer::EventConnDataNotify);
}
/************************************************
 *	Create & configure the UI, the actual layout
 ************************************************/
void MultiCameraViewer::UICreate()
{
	// Tool options
	int SpacingTen = 10;
	//////////////////////////////////////////////////////////////////////////////////////////////
	// Add an layout for the entire Tool
	AddRegion( "EntireLayout",	"EntireLayout",
		0,	kFBAttachLeft,	"",	1.0,
		0,	kFBAttachTop,"",	1.0,
		0,  kFBAttachRight,	"",1.0,
		0,  kFBAttachBottom,"",1.0 );
	SetControl( "EntireLayout",	mEntireLayout	);
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	// TOP PANEL SETUP
	//////////////////////////////////////////////////////////////////////////////////////////////
	// Performance Options Control
	mEntireLayout.AddRegion( "PerformanceInfo",	"PerformanceInfo",
		SpacingTen,	kFBAttachLeft,		"",	1.0	,
		0,		kFBAttachTop,		"",	1.0,
		70,	kFBAttachNone,		NULL,	1.0,
		25,		kFBAttachNone,	NULL,	1.0 );
	mEntireLayout.SetControl( "PerformanceInfo", mPerformanceInfoLabel );
	mEntireLayout.AddRegion( "PerformanceSlider", "PerformanceSlider",
		5, kFBAttachRight, "PerformanceInfo", 1.0,
		2.5, kFBAttachTop, "", 1.0,
		200, kFBAttachNone, NULL, 1.0,
		20, kFBAttachNone, NULL, 1.0 );
	mEntireLayout.SetControl( "PerformanceSlider", mPerformanceSlider );
	mEntireLayout.AddRegion( "Performance",	"Performance",
		5,	kFBAttachRight,		"PerformanceSlider",	1.0	,
		0,		kFBAttachTop,		"",	1.0,
		SpacingTen,	kFBAttachNone,		NULL,	1.0,
		25,		kFBAttachNone,	NULL,	1.0 );
	mEntireLayout.SetControl( "Performance", mPerformanceLabel );
	// Rows Options Control
	mEntireLayout.AddRegion("RowsInfo", "RowsInfo",
		SpacingTen, kFBAttachRight, "Performance", 1.0,
		0, kFBAttachTop, "", 1.0,
		80, kFBAttachNone, NULL, 1.0,
		25, kFBAttachNone, NULL, 1.0);
	mEntireLayout.SetControl("RowsInfo", mRowsInfoLabel);

	// Row radio buttons
	char lastControl[256];
	sprintf_s(lastControl, 256, "RowsInfo");
	for (int idx = 0; idx < mMaxNumberOfRows; ++idx)
	{
		char newControl[256];
		sprintf_s(newControl, 256, "RowRadio%d", idx);
		mEntireLayout.AddRegion(newControl, newControl,
			1,		kFBAttachRight, lastControl,	1.0,
			1,		kFBAttachTop,	"",				1.0,
			40,		kFBAttachNone,	NULL,			1.0,
			25,		kFBAttachNone,	NULL,			1.0);
		mEntireLayout.SetControl(newControl, mRowsRadioBtns[idx]);
		strcpy_s(lastControl, newControl);
	}

	// RTE Create Button
	mEntireLayout.AddRegion( "RTESetup",	"RTESetup",
		30,		kFBAttachRight, lastControl,	1.0	,
		5,		kFBAttachTop,		"",	1.0,
		170,	kFBAttachNone,		NULL,	1.0,
		15,		kFBAttachNone,	NULL,	1.0 );
	mEntireLayout.SetControl( "RTESetup", mRTESetupButton );
	// Touch Mode Button
	mEntireLayout.AddRegion( "TouchMode",	"TouchMode",
		50,		kFBAttachRight,		"RTESetup",	1.0	,
		5,		kFBAttachTop,		"",	1.0,
		200,	kFBAttachNone,		NULL,	1.0,
		15,		kFBAttachNone,	NULL,	1.0 );
	mEntireLayout.SetControl( "TouchMode", mTouchMode );
	// Help Button
	mEntireLayout.AddRegion( "Options",	"Options",
		-78,	kFBAttachRight,		"",	1.0	,
		5,		kFBAttachTop,		"",	1.0,
		60,		kFBAttachNone,		NULL,	1.0,
		15,		kFBAttachNone,	NULL,	1.0 );
	mEntireLayout.SetControl( "Options", mOptionsButton );
	// Version Information
	mEntireLayout.AddRegion( "Version",	"Version",
		-145,	kFBAttachLeft,		"Options",	1.0	,
		0,		kFBAttachTop,		"",	1.0,
		135,	kFBAttachNone,		NULL,	1.0,
		25,		kFBAttachNone,	NULL,	1.0 );
	mEntireLayout.SetControl( "Version", mVersionLabel );
	mEntireLayout.AddRegion( "ControlScheme",	"ControlScheme",
		-100,	kFBAttachLeft,		"Version",	1.0	,
		0,		kFBAttachTop,		"",	1.0,
		135,	kFBAttachNone,		NULL,	1.0,
		25,		kFBAttachNone,	NULL,	1.0 );
	mEntireLayout.SetControl( "ControlScheme", mControlBox );
	mEntireLayout.AddRegion( "ControlSchemeLabel",	"ControlSchemeLabel",
		-135,	kFBAttachLeft,		"ControlScheme",	1.0	,
		0,		kFBAttachTop,		"",	1.0,
		135,	kFBAttachNone,		NULL,	1.0,
		25,		kFBAttachNone,	NULL,	1.0 );
	mEntireLayout.SetControl( "ControlSchemeLabel", mControlLabel );
	//////////////////////////////////////////////////////////////////////////////////////////////
	// BOTTOM PANEL SETUP
	//////////////////////////////////////////////////////////////////////////////////////////////
	// FBViews Layout
	mEntireLayout.AddRegion( "ScrollBox",	"ScrollBox",
		0,				kFBAttachLeft,		"",	1.0,
		mHeaderHeight,	kFBAttachTop,		"",	1.0,
		0,				kFBAttachRight,		"",	1.0,
		0,				kFBAttachBottom,	"",	1.0);	
	mEntireLayout.SetControl( "ScrollBox", mScrollBox);

	FBLayout* pScrollBoxLayout = mScrollBox.GetContent();

	int initialCams = mNumRows * mNumRows;
	mCameras.clear();
	// Set up initial cameras, we need a new control to select the number or rows though
	for (int i = 0; i < initialCams; ++i)
	{
		mCameras.push_back(new RSCameraControl());
		mCameras[i]->Init(pScrollBoxLayout, i, mNumRows, mCurrentWidth, mCurrentHeight - mHeaderHeight - mFooterHeight, mControlScheme);
	}

	//Add resize callback now
	mEntireLayout.OnResize.Add(this, (FBCallback)&MultiCameraViewer::ScrollBoxResize);
}

/*
Debug layout resized
*/
void MultiCameraViewer::ScrollBoxResize(HISender pSender, HKEvent pEvent)
{
	FBEventResize lEvent(pEvent);

	int w = lEvent.Width.AsInt();
	int h = lEvent.Height.AsInt();

	mCurrentWidth = w;
	mCurrentHeight = h;

	// width can be calculated by numCams/NumRows to get num Columns, then multiply that time control width
	if (mCameras.size() > 0)
	{
		SetScrollWidth();

		for (int i = 0; i < mCameras.size(); ++i)
		{
			mCameras[i]->MainWindowResized(mCurrentWidth, mCurrentHeight - mHeaderHeight - mFooterHeight, mNumRows);
		}
	}
}

void MultiCameraViewer::AutoLoadCameras(int maxNumberToLoad)
{
	//Loop through all cameras and detect any which match the r* pattern, and auto add them if the slot is empty
	for (int i = 0; i < mSystem.Scene->Cameras.GetCount(); i++)
	{
		int camIndex = 0;
		const char* pCamName = mSystem.Scene->Cameras[i]->LongName;
		if (strstr(pCamName, "RS_Camera_"))
		{
			size_t nameLen = strlen("RS_Camera_");
			if (strlen(pCamName) > nameLen)
			{
				camIndex = (int)atof(pCamName + nameLen);
			}
		}
		else if (strstr(pCamName, "R*Camera "))
		{
			size_t nameLen = strlen("R*Camera ");
			if (strlen(pCamName) > nameLen)
			{
				camIndex = (int)atof(pCamName + nameLen);
			}
		}

		if (camIndex != 0 && camIndex <= maxNumberToLoad)
		{
			for (int j = (int)mCameras.size(); j < camIndex; ++j)
			{
				mCameras.push_back(new RSCameraControl());
				mCameras[j]->Init(mScrollBox.GetContent(), j, mNumRows, mCurrentWidth, mCurrentHeight - mHeaderHeight - mFooterHeight, mControlScheme);
				mCameras[j]->SetUseTouch(mTouchMode.State == 1);
				mCameras[j]->UIConfigure();
			}
			
			mCameras[camIndex - 1]->AddCamera(mSystem.Scene->Cameras[i]);
		}
	}
}

/************************************************
 *	Configure the UI here, how you want it to look
 ************************************************/
void MultiCameraViewer::LoadingPersistantData(FBCamera * pPersistantCamera, int pPersistantData )
{
	int i;
	// If pPersistantData is greater than the number of cameras loaded, we need to add some more up to that number
	for (i = (int)mCameras.size(); i <= pPersistantData; i++)
	{
		mCameras.push_back(new RSCameraControl());
		mCameras[i]->Init(mScrollBox.GetContent(), i, mNumRows, mCurrentWidth, mCurrentHeight - mHeaderHeight - mFooterHeight, mControlScheme);
		mCameras[i]->SetUseTouch(mTouchMode.State == 1);
		mCameras[i]->UIConfigure();
	}

	// You cannot use FBFindModelByLabelName to get the default Camera's need to go through the camera list, so might as well do this for all of them.
	for(i=0; i<mSystem.Scene->Cameras.GetCount(); i++)
	{
		// If there persistent data, read it and setup the Multi Camera Viewer with the saved data.
		if (pPersistantCamera)
		{
			if (mSystem.Scene->Cameras[i]->LongName == pPersistantCamera->LongName)
			{
				FBCamera* lCamera = mSystem.Scene->Cameras[i];
				mCameras[pPersistantData]->AddCamera(lCamera);
			}
		}
	}
}
/************************************************
 *	Configure the UI here, how you want it to look
 ************************************************/
void MultiCameraViewer::UIConfigure()
{
	//This counter is for refreshing view using the enhanced performance option in the IDLE.
	mFrameCounter = 0;
	mRefreshRate = 3;
	mUpdateEachFrame = false;
	mDoubleBuffered = true;
	mEvaluateMode = true;
	mFirstFrameUpdated = -1;

	//////////////////////////////////////////////////////////////////////////////////////////////
	// Top Panel Controls
	mPerformanceSlider.Min = 0.0;
	mPerformanceSlider.Max = 9.0;
	mPerformanceSlider.Value = 3.0;
	mPerformanceSlider.Orientation = kFBHorizontal;
	mPerformanceSlider.OnChange.Add( this, (FBCallback)&MultiCameraViewer::PerformanceSliderChanged );
	mPerformanceSlider.Hint = "Usage:\n\nThis slider controls how many camera's are updated per frame.\nDescrease the number for better performance";
	mPerformanceInfoLabel.Caption = "Refresh Rate:";
	mPerformanceInfoLabel.Hint = "Usage:\n\nThis slider controls how many camera's are updated per frame.\nDescrease the number for better performance";
	mPerformanceInfoLabel.Justify = kFBTextJustifyRight;
	mPerformanceLabel.Caption = "3";
	mPerformanceLabel.Justify = kFBTextJustifyLeft;

	mRowsInfoLabel.Caption = "Camera Rows:";
	mRowsInfoLabel.Hint = "Usage:\n\nThese radio buttons control how rows of camera's are shown.";
	mRowsInfoLabel.Justify = kFBTextJustifyRight;

	for (int idx = 0; idx < mMaxNumberOfRows; ++idx)
	{
		mRowsRadioBtns[idx].Caption = std::to_string(idx+1).c_str();
		mRowsRadioBtns[idx].Hint = "Usage:\n\nThese radio buttons control how rows of camera's are shown.";
		mRowsRadioBtns[idx].Style = kFBRadioButton;
		mRowsRadioBtns[idx].State = (mNumRows == (idx+1)) ? 1 : 0;
		mRowsRadioBtns[idx].OnClick.Add(this, (FBCallback)&MultiCameraViewer::RowRadioBtns_Click);
	}

    // RTE Setup
    mRTESetupButton.Caption = "RTE Camera Create and Setup";
	mRTESetupButton.Hint = "Usage:\n\nClick here to create RTE cameras, put the create cameras and their constraint in folders and select all of the RTE base cameras and align -> translation to the stage,.";
    mRTESetupButton.OnClick.Add( this, (FBCallback)&MultiCameraViewer::RTESetupButton_Click );
	// Touchscreen Mode
	mTouchMode.Caption = "Touchscreen Mode";
	mTouchMode.Hint = "Usage:\n\nClick here to toggle between Touchscreen mode and Normal PC mode";
	mTouchMode.Style = kFBCheckbox;
	mTouchMode.State = 0;
	mTouchMode.OnClick.Add( this, (FBCallback)&MultiCameraViewer::TouchModeButton_Click );

	// Version
	char cBuffer[32];
	sprintf_s(cBuffer,"Version: %i.%i.%i", s_version.Major, s_version.Minor, s_version.Revision);
	mVersionLabel.Caption = cBuffer;
	mVersionLabel.Hint = cBuffer;
	mVersionLabel.Justify = kFBTextJustifyRight;
    // Help
    mOptionsButton.Caption = "Options";
	mOptionsButton.Hint = "Usage:\n\nClick here to open configurable tool options.";
	mOptionsButton.OnClick.Add( this, (FBCallback)&MultiCameraViewer::OptionsButton_Click );
	// Control Box
	mControlBox.Hint = "Usage:\n\nSelect a camera control scheme from this list";
	mControlBox.Items.Clear();
	for (int i=0; i<TotalSchemes; ++i)
		mControlBox.Items.Add(ControlSchemeNames[i]);
	mControlBox.OnChange.Add( this, (FBCallback) &MultiCameraViewer::ControlListOnChange);
	// Try to pull from config file
	const char * lSchemeName = "MotionBuilder";
	FBConfigFile lConfig("@Application.txt");
	lSchemeName = lConfig.Get("Keyboard", "Default");
	for (int i=0;i<mControlBox.Items.GetCount();++i)
	{
		if (stricmp(lSchemeName,mControlBox.Items.GetAt(i))==0)
		{
			mControlBox.Selected(i,true);
			ControlListOnChange(NULL,NULL);
		}
	}
	mControlLabel.Caption = "Interaction Mode:";
	mControlLabel.Hint = "Usage:\n\nSelect a camera control scheme from the list";
	mControlLabel.Justify = kFBTextJustifyRight;

	//////////////////////////////////////////////////////////////////////////////////////////////
	// Assign callbacks and setting up Camera Views Properties
	if (mCameras.size() > 0)
	{
		SetScrollWidth();

		for (int i = 0; i < mCameras.size(); ++i)
		{
			mCameras[i]->UIConfigure();
		}
	}

	LoadPersistantData();
}

void MultiCameraViewer::LoadPersistantData()
{
	//////////////////////////////////////////////////////////////////////////////////////////////
	// Populating the UI with any Persistent Data, need to check if I need to add camera into the UI
	FBArrayTemplate<FBCamera*> tempArray = GetPersistentData();

	for (int i=0; i<tempArray.GetCount(); ++i)
	{
		LoadingPersistantData(tempArray.GetAt(i), i);
	}

	// Auto Load up to 12 R* cams
	AutoLoadCameras(12);

	FlushCameraList();
	SetScrollWidth();

	// Update properties from saved data
	MultiCameraViewerData* pData = FindPersistentData(true);
	
	mRefreshRate = pData->refreshRate;
	mPerformanceSlider.Value = (double)mRefreshRate;
	PerformanceSliderChanged(NULL, NULL);
	SetRowSelection(pData->numRows);
	mUpdateEachFrame = pData->bUpdateEachFrame;
	mTouchMode.State = pData->bTouchMode;
	TouchModeButton_Click(NULL,NULL);
	mDoubleBuffered = pData->bDoubleBuffered;
	mEvaluateMode = pData->bEvaluateMode;

	mbLoading = false;
}

void MultiCameraViewer::PerformanceSliderChanged( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	mPerformanceSlider.Value = floor(mPerformanceSlider.Value);
	mRefreshRate = (int)mPerformanceSlider.Value;
	char cBuffer[8];
	sprintf_s(cBuffer, "%i",mRefreshRate);
	mPerformanceLabel.Caption = cBuffer;
	
	FindPersistentData(true)->refreshRate = mRefreshRate;
}

void MultiCameraViewer::SetRowSelection(int row)
{
	for (int idx = 0; idx < mMaxNumberOfRows; ++idx)
	{
		int buttonState = (row == (idx+1)) ? 1 : 0;
		if (mRowsRadioBtns[idx].State != buttonState)
		{
			mRowsRadioBtns[idx].State = buttonState;
		}
	}
	bool bNeedToRefresh = false;
	if (mNumRows != row)
	{
		bNeedToRefresh = true;
	}
	mNumRows = row;
	FindPersistentData(true)->numRows = mNumRows;

	if (bNeedToRefresh)
	{
		// Update views to new RowLength
		for (int i = 0; i < mCameras.size(); i++)
		{
			mCameras[i]->ReInit(mNumRows, mCurrentWidth, mCurrentHeight - mHeaderHeight - mFooterHeight);
		}
		// Add cameras up to the new minimum
		int cameraMin = mNumRows*mNumRows;
		for (int i = (int)mCameras.size(); i < cameraMin; i++)
		{
			mCameras.push_back(new RSCameraControl());
			mCameras[i]->Init(mScrollBox.GetContent(), i, mNumRows, mCurrentWidth, mCurrentHeight - mHeaderHeight - mFooterHeight, mControlScheme);
			mCameras[i]->SetUseTouch(mTouchMode.State == 1);
			mCameras[i]->UIConfigure();
		}
		FlushCameraList();
		SetScrollWidth();
	}
}

void MultiCameraViewer::FlushCameraList()
{
	// Add any extra cameras needed to bring the number of cameras up to a flush list
	while ((mCameras.size() % mNumRows) != 0)
	{
		mCameras.push_back(new RSCameraControl());
		mCameras[mCameras.size() - 1]->Init(mScrollBox.GetContent(), (int)mCameras.size() - 1, mNumRows, mCurrentWidth, mCurrentHeight - mHeaderHeight - mFooterHeight, mControlScheme);
		mCameras[mCameras.size() - 1]->SetUseTouch(mTouchMode.State == 1);
		mCameras[mCameras.size() - 1]->UIConfigure();
	}
}

void MultiCameraViewer::RowRadioBtns_Click(HISender pSender, HKEvent pEvent)
{
	FBComponent* lComp = FBGetFBComponent((HIObject)pSender);

	for (int idx = 0; idx < mMaxNumberOfRows; ++idx)
	{
		if (lComp == &mRowsRadioBtns[idx])
		{
			SetRowSelection(idx+1);
			break;
		}
	}
}

void MultiCameraViewer::ControlListOnChange( HISender /*pSender*/, HKEvent /*pEvent*/ )
{
	for (int i=0; i<mControlBox.Items.GetCount(); ++i)
	{
		if (mControlBox.IsSelected(i))
		{
			const char* scheme = mControlBox.Items.GetAt(i);
			for (int j=0;j<TotalSchemes;++j)
			{
				if (stricmp(scheme,ControlSchemeNames[j])==0)
				{
					for (int k=0; k<mCameras.size(); ++k)
					{
						mCameras[k]->SetControlScheme((ControlScheme)j);
						mControlScheme = (ControlScheme)j;
					}
				}
			}
		}
	}
}

/************************************************
 *	UI Idle callback.
 ************************************************/
void MultiCameraViewer::EventToolIdle( HISender pSender, HKEvent pEvent )
{
	RefreshView();
}

/************************************************
 *	Callback for OnConnectionDataNotify
 ************************************************/
void MultiCameraViewer::EventConnDataNotify( HISender pSender, HKEvent pEvent )
{
	FBEventConnectionDataNotify	lEvent(pEvent);
	FBPlug*						lPlug;
	FBString					lFOV	= "FieldOfView";
	FBString					lLens	= "Lens";
	FBString					lPropertyName;
	if( lEvent.Plug )
	{
		// Confirm the item that is changing is a Property
		if (lEvent.Plug->Is(FBProperty::TypeInfo))
		{
			lPlug = lEvent.Plug->GetOwner();
			// Confirm the item that is changing parent is a Camera
			if (lPlug->Is(FBCamera::TypeInfo))
			{
				// We only care about the property FOV
				lPlug = lEvent.Plug;
				lPropertyName = ((FBProperty*)lPlug)->GetName();
				if (lPropertyName == lFOV )
				{
					for (int i = 0; i < mCameras.size(); ++i)
					{
						mCameras[i]->UpdateFOV(lEvent);
					}
				}
				else if (lPropertyName == lLens)
				{
					for (int i = 0; i < mCameras.size(); ++i)
					{
						mCameras[i]->UpdateFOV(lEvent);
					}
				}
			}
		}
	}	
}

/************************************************
*	Update the live view right before the frame is rendered, this gives it better feedback
************************************************/
void MultiCameraViewer::RefreshLiveView()
{
	for (int i = 0; i < mCameras.size(); ++i)
	{
		if (mCameras[i]->IsLive())
		{
			mCameras[i]->Refresh(false);
		}
		else if (mCameras[i]->IsBeingControlled())
		{
			mCameras[i]->Refresh(false);
		}
	}
}

/************************************************
 *	UI Idle callback, this controls how much the tool views update
 ************************************************/
void MultiCameraViewer::RefreshView()
{	
	int viewsToRefresh = mRefreshRate;
	
	// Update all camera properties
	for (int i = 0; i < mCameras.size(); ++i)
	{
		mCameras[i]->SetDoubleBuffered(mDoubleBuffered);
		mCameras[i]->SetEvaluateMode(mEvaluateMode);
	}

	// Keep only one view live at a time
	for (int i = 0; i < mCameras.size(); ++i)
	{
		if (mCameras[i]->IsLive() && i != mLastLive)
		{
			// Set all other cameras to not live
			for (int j = 0; j < mCameras.size(); ++j)
			{
				if (j != i && mCameras[j]->IsLive())
				{
					mCameras[j]->SetIsLive(false);
				}
			}
			break;
		}
	}

	for (int i = 0; i < mCameras.size(); ++i)
	{
		if (mCameras[i]->IsLive())
		{
			// Dont update any other cams
			viewsToRefresh = 0;
			mLastLive = i;
			break;
		}
	}

	//If the user is manipulating the camera in one of the views, chop the framerate down on all other cams
	//Refresh rate defines how many views to update each frame
	int viewControlled = -1;
	for (int i=0; i<mCameras.size(); ++i)
	{
		if(mCameras[i]->IsBeingControlled())
		{
			viewsToRefresh = min(1, viewsToRefresh);
			viewControlled=i;
			break;
		}
	}

	if (mFrameCounter >= mCameras.size())
	{
		mFrameCounter = 0;
	}

	if (mUpdateEachFrame)
	{
		// Only update here if the time has changed since the last update
		if (mLastTime != mSystem.LocalTime)
		{
			mLastTime = mSystem.LocalTime;
			// This is the view index we need to loop back to in order to update all views for this frame
			mFirstFrameUpdated = mFrameCounter;
		}
		else if (mFirstFrameUpdated == -1)
		{
			// No views to update
			viewsToRefresh = 0;
		}

		while (viewsToRefresh > 0)
		{
			if (mFrameCounter != viewControlled && mCameras[mFrameCounter]->IsActive())
			{
				mCameras[mFrameCounter]->Refresh(false);
				viewsToRefresh--;
			}
			if (mFrameCounter == (int)mCameras.size() - 1)
			{
				mFrameCounter -= (int)mCameras.size();
			}
			mFrameCounter++;
			if (mFrameCounter == mFirstFrameUpdated)
			{
				// We have looped back round to where we started
				mFirstFrameUpdated = -1;
				break;
			}
		}
	}
	else
	{
		mFirstFrameUpdated = -1;
		//in the case of there being less cameras than refresh rate, we don't want to update the same frame more than once
		int startView = mFrameCounter;
		while (viewsToRefresh > 0)
		{
			if (mFrameCounter != viewControlled && mCameras[mFrameCounter]->IsActive())
			{
				mCameras[mFrameCounter]->Refresh(false);
				viewsToRefresh--;
			}
			if (mFrameCounter == (int)mCameras.size() - 1)
			{
				mFrameCounter -= (int)mCameras.size();
			}
			mFrameCounter++;
			if (mFrameCounter == startView)
			{
				// We have looped back round to where we started
				break;
			}
		}
	}
	
	// Follow up with any views still not refreshed at least once
	for (int i = 0; i < mCameras.size(); ++i)
	{
		if (mCameras[i]->IsActive() && !mCameras[i]->HasBeenRefreshed())
		{
			mCameras[i]->Refresh(false);
		}
	}

	// here we can see if we need to add new empty camera (the criteria is, if the right most column has an active cam we add a new column)
	bool bDeleteLastColumn = mCameras.size() > (mNumRows*mNumRows);
	for (int i = 1; i <= (mNumRows * 2); i++)
	{
		// check the last n number of cams
		int index = (int)mCameras.size() - i;
		if (index >= 0)
		{
			// If its in the last column
			if (i <= mNumRows && mCameras[index]->IsActive())
			{
				// Add mNumRows of new cameras
				int lastIndex = (int)mCameras.size();
				for (int j = lastIndex; j < (lastIndex + mNumRows); j++)
				{
					mCameras.push_back(new RSCameraControl());
					mCameras[j]->Init(mScrollBox.GetContent(), j, mNumRows, mCurrentWidth, mCurrentHeight - mHeaderHeight - mFooterHeight, mControlScheme);
					mCameras[j]->SetUseTouch(mTouchMode.State == 1);
					mCameras[j]->UIConfigure();
				}

				SetScrollWidth();
				bDeleteLastColumn = false;
				break;
			}
			// Else its in the second last column, and the last column was empty
			else
			{
				if (mCameras[index]->IsActive())
				{
					bDeleteLastColumn = false;
				}
			}
		}
		else
		{
			bDeleteLastColumn = false;
		}
	}

	// If the second last row is empty, remove the last row
	if (bDeleteLastColumn)
	{
		for (int i = 0; i < mNumRows; i++)
		{
			delete mCameras.back();
			mCameras.pop_back();
		}
		SetScrollWidth();
	}
}

void MultiCameraViewer::SetScrollWidth()
{
	if (mCameras.size() > 0)
	{
		int camWidth = mCameras[0]->GetControlWidth();
		int numCols = int(mCameras.size() / mNumRows);
		mScrollBox.SetContentWidth(numCols * camWidth);
		mScrollBox.SetContentHeight(mCurrentHeight - (mHeaderHeight)*2);
	}
}
/************************************************
 *	Code for ChangeDetach Callback
 ************************************************/
void MultiCameraViewer::ExecuteChangeDetach(FBString cameraRemoved)
{
	for (int i = 0; i < mCameras.size(); ++i)
	{
		FBCamera* pCamera = mCameras[i]->GetCamera();
		if (pCamera && pCamera->LongName == cameraRemoved)
		{
			mCameras[i]->Reset(true);
		}
	}
}

/************************************************
 *	Event scene change callback.
 ************************************************/
void MultiCameraViewer::EventSceneChange( HISender pSender, HKEvent pEvent )
{
	FBEventSceneChange lEvent( pEvent );

	// These are the three scenarios we need to monitor for the camera being added, renamed and deleted, so that we can reflect these changes in the Multi Camera Viewer.
	switch( lEvent.Type )
	{
		// Interesting Fact, here when items are renamed we are dealing with Component of the Event
		case kFBSceneChangeRename:
		{
			if (lEvent.Component && lEvent.Component->Is(FBCamera::TypeInfo))
			{
				mCameraRenameBuffer = lEvent.Component->LongName;
			}
		}
		case kFBSceneChangeRenamed: //Before Camera Rename
		{
			if( mCameraRenameBuffer.GetLen() > 0 && lEvent.Component && lEvent.Component->Is( FBCamera::TypeInfo ) && (mCameraRenameBuffer != (FBString)lEvent.Component->LongName) )
			{			
				for (int i = 0; i < mCameras.size(); ++i)
				{
					mCameras[i]->RenameCameraEntry(mCameraRenameBuffer, (FBString)lEvent.Component->LongName);
				}
				mCameraRenameBuffer = NULL;
			}
		}
		break;
		// Interesting Fact, here when items are deleted we are dealing with ChildComponent of the Event, not the Component which is the scene in this case.
		case kFBSceneChangeDetach:
		{
			// Check we are working with a camera, so nothing is run if other things are deleted.
			if (lEvent.ChildComponent->Is( FBCamera::TypeInfo ) )
			{									
				FBString lCameraName = lEvent.ChildComponent->LongName;
				for(int i=0;i<mCameras.size();++i)
					mCameras[i]->RemoveItemFromCameraList(lCameraName);

				// Need to clear the UI spot that is contains this deleted camera...
				FBArrayTemplate<FBCamera*> tempArray = GetPersistentData();

				ExecuteChangeDetach(lCameraName);
			}
		}
		break;
		// kFBSceneChangeAddChild are for the updating the Camera Scene Tree and for adding the camera to the camera drop down list when new.
		case kFBSceneChangeAddChild:
		{
			// Check we are working with a camera, so nothing is run if other things are added.
			// Need to add newly created camera's to the tool's drop down boxes.
			if (lEvent.ChildComponent->Is( FBCamera::TypeInfo ) )
			{				
				FBCamera* lCamera = (FBCamera*)FBFindModelByLabelName(lEvent.ChildComponent->LongName);
				if (lCamera)
				{
					bool newCamera = false;
					for (int i = 0; i < mCameras.size(); ++i)
					{
						newCamera = mCameras[i]->AddItemToCameraList(lCamera);
					}

					if (newCamera && !mbLoading)
					{
						int camIndex = 0;

						const char* pCamName = lCamera->LongName;
						if (strstr(pCamName, "RS_Camera_"))
						{
							size_t nameLen = strlen("RS_Camera_");
							if (strlen(pCamName) > nameLen)
							{
								camIndex = (int)atof(pCamName + nameLen);
							}
						}
						else if (strstr(pCamName, "R*Camera "))
						{
							size_t nameLen = strlen("R*Camera ");
							if (strlen(pCamName) > nameLen)
							{
								camIndex = (int)atof(pCamName + nameLen);
							}
						}

						if (camIndex != 0)
						{
							for (int i = (int)mCameras.size(); i < camIndex; ++i)
							{
								mCameras.push_back(new RSCameraControl());
								mCameras[i]->Init(mScrollBox.GetContent(), i, mNumRows, mCurrentWidth, mCurrentHeight - mHeaderHeight - mFooterHeight, mControlScheme);
								mCameras[i]->SetUseTouch(mTouchMode.State == 1);
								mCameras[i]->UIConfigure();
							}
							FlushCameraList();
							SetScrollWidth();
							mCameras[camIndex - 1]->AddCamera(lCamera);
						}
					}
				}
			}
		}
		break;
	}
}

/************************************************
 *	ClearView code
 ************************************************/
void MultiCameraViewer::ClearView()
{
	for(int i=0;i<mCameras.size();++i)
	{
		mCameras[i]->Reset(false);
	}
}
/************************************************************************************
 *  EventOnFileNewCompleted, if a user does a file new we need to clear out the tool.
 ************************************************************************************/
void MultiCameraViewer::EventOnFileNewCompleted( HISender pSender, HKEvent pEvent    )
{
	// When call file new, if there are default MotionBuilder camera's in your Multi-Viewer Tool they will stay in the tool, which we don't want, so we them out.
	ClearView(); // Clear what's in the tool.
}
/*************************************************************************************************
 *  EventOnFileOpenCompleted, if a user does a file open we need to update the tool for the scene.
 *************************************************************************************************/
void MultiCameraViewer::EventOnFileOpenCompleted( HISender pSender, HKEvent pEvent    )
{
	ClearView(); // Clear what's in the tool.
	LoadPersistantData(); // Check if the new scene has persistent data.!
}

void MultiCameraViewer::EventOnFileOpen(HISender pSender, HKEvent pEvent)
{
	mbLoading = true;
}

void MultiCameraViewer::OptionsButton_Click( HISender pSender, HKEvent pEvent)
{   
	//Open the options popup
	MultiCameraViewerOptionsPopup optionsPopup(this);
	optionsPopup.Show();
}

void MultiCameraViewer::RTESetupButton_Click( HISender pSender, HKEvent pEvent)
{
	char* lToolsPath;
	char* lModulePath = "\\techart\\dcc\\motionbuilder2014\\python\\RS\\Core\\Mocap\\Previz\\RTEBaseSetup.py";
	lToolsPath = getenv ("RS_TOOLSROOT");

	char buffer[256];
	strncpy(buffer, lToolsPath, sizeof(buffer));
	strncat(buffer, lModulePath, sizeof(buffer));

	mApplication.ExecuteScript(buffer);
}

void MultiCameraViewer::TouchModeButton_Click( HISender pSender, HKEvent pEvent)
{
	FindPersistentData(true)->bTouchMode = mTouchMode.State == 1;
	for (int i=0; i<mCameras.size(); ++i)
	{
		mCameras[i]->SetUseTouch(mTouchMode.State==1);
	}
}
/************************************************
 *  FindPersistentData
 ************************************************/
MultiCameraViewerData* MultiCameraViewer::FindPersistentData( bool pCreate )
{
    // First we want to see if there is any infos for this tool present in the scene
    FBScene* lScene = mSystem.Scene;
    int lIdx = 0;
    for( lIdx = 0; lIdx < lScene->UserObjects.GetCount(); ++lIdx ) {
        FBUserObject* lObject = lScene->UserObjects[lIdx];
        if( lObject->Is( MultiCameraViewerData::TypeInfo ))
            return (MultiCameraViewerData*)lObject;
    }
    MultiCameraViewerData* lData = 0;

	if( pCreate )
	{
        lData = new MultiCameraViewerData( "MultiCameraViewerData" );
	}
    return lData;
}
/************************************************
 *  DeletePersistentData
 ************************************************/
void MultiCameraViewer::DeletePersistentData()
{
    MultiCameraViewerData* lData = FindPersistentData( false );
    if( lData )
        lData->FBDelete();
}
/************************************************
 *  GetPersistentData
 ************************************************/
FBArrayTemplate<FBCamera*> MultiCameraViewer::GetPersistentData()
{
	return FindPersistentData( true )->cameraArray;
}
