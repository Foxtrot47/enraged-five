/* \file   orcustommanager_template_manager.cxx*/

//--- Class declaration
#include <QtGui/QApplication>
#include <QtGui/QWidget>
#include "custommanagermobutitlemanager.h"
#include <iostream>

#include <sys/types.h>
#include <sys/stat.h>


#ifdef KARCH_ENV_WIN
	#include <windows.h>
#endif

//--- Registration defines
#define CUSTOMMANAGERMOBUTITLE__CLASS CUSTOMMANAGERMOBUTITLE__CLASSNAME
#define CUSTOMMANAGERMOBUTITLE__NAME  CUSTOMMANAGERMOBUTITLE__CLASSSTR

//--- FiLMBOX implementation and registration
FBCustomManagerImplementation( CUSTOMMANAGERMOBUTITLE__CLASS  );  // Manager class name.
FBRegisterCustomManager( CUSTOMMANAGERMOBUTITLE__CLASS );         // Manager class name.

/************************************************
 *  FiLMBOX Creation
 ************************************************/
bool CustomeManagerMoBuTitle::FBCreate()
{
	mApplication.OnFileSave.Add( this, (FBCallback)&CustomeManagerMoBuTitle::FileSaveCallback );
	mApplication.OnFileSaveCompleted.Add( this, (FBCallback)&CustomeManagerMoBuTitle::FileSaveCompletedCallback ); //Needed to get a fix for this one to work correctly, this is in HF 17 of MotionBuilder 2014
	mApplication.OnFileNewCompleted.Add( this, (FBCallback)&CustomeManagerMoBuTitle::FileNewCompletedCallback );
    mApplication.OnFileOpenCompleted.Add( this, (FBCallback)&CustomeManagerMoBuTitle::FileOpenCompletedCallback );
	mApplication.OnFileExit.Add( this, (FBCallback)&CustomeManagerMoBuTitle::FileExitCallback );
	return true;
}

/************************************************
 *  FiLMBOX Destruction.
 ************************************************/
void CustomeManagerMoBuTitle::FBDestroy()
{
	mApplication.OnFileSave.Remove( this, (FBCallback)&CustomeManagerMoBuTitle::FileSaveCallback );
    mApplication.OnFileSaveCompleted.Remove( this, (FBCallback)&CustomeManagerMoBuTitle::FileSaveCompletedCallback );
	mApplication.OnFileNewCompleted.Remove( this, (FBCallback)&CustomeManagerMoBuTitle::FileNewCompletedCallback );
    mApplication.OnFileOpenCompleted.Remove( this, (FBCallback)&CustomeManagerMoBuTitle::FileOpenCompletedCallback );
	mApplication.OnFileExit.Remove( this, (FBCallback)&CustomeManagerMoBuTitle::FileExitCallback );
}

/************************************************
 *  Execution callback.
 ************************************************/
bool CustomeManagerMoBuTitle::Init()
{
	return true;
}

bool CustomeManagerMoBuTitle::Open()
{
	return true;
}

bool CustomeManagerMoBuTitle::Clear()
{
    return true;
}

bool CustomeManagerMoBuTitle::Close()
{
    return true;
}

/************************************************
	Event Callbacks
************************************************/


void CustomeManagerMoBuTitle::FileSaveCallback( HISender pSender, HKEvent pEvent )
{
	CustomeManagerMoBuTitle::UpdateToolbar();
}

void CustomeManagerMoBuTitle::FileSaveCompletedCallback( HISender pSender, HKEvent pEvent )
{
	CheckFileSize();
}

void CustomeManagerMoBuTitle::FileNewCompletedCallback( HISender pSender, HKEvent pEvent )
{
	CustomeManagerMoBuTitle::UpdateToolbar();
}

void CustomeManagerMoBuTitle::FileOpenCompletedCallback( HISender pSender, HKEvent pEvent )
{
	CustomeManagerMoBuTitle::UpdateToolbar();

	// Bug # 1778964 - Add a pop up box to notify user that the scene they have loaded is from a different project

	QString projRootVar = "x:\\";

	QString lProjVar = QString::fromUtf8(GetProjectPathFromRegistry().c_str());


	projRootVar = projRootVar.append(lProjVar);

	QString fileNameRootVar = mApplication.FBXFileName.AsString();

	bool compareFilePathToProjRoot = fileNameRootVar.contains(&projRootVar, Qt::CaseInsensitive);
	if ((!compareFilePathToProjRoot) && (fileNameRootVar.contains("X:", Qt::CaseInsensitive)))
	{
		FBMessageBox("Warning", "The scene you have loaded is from a different project.\n\nYou will not be able to work directly with the Reference Editor\nuntil you install the correct tools for the scene you are opening.", "OK");
	}
}

void CustomeManagerMoBuTitle::FileExitCallback( HISender pSender, HKEvent pEvent )
{
	CustomeManagerMoBuTitle::UpdateToolbar();
}

/************************************************
	Class Functions
************************************************/

void CustomeManagerMoBuTitle::CheckFileSize( )
{
	// Get the file size of the temp copy
	struct _stat64	filestatus;
	_stat64(mApplication.FBXFileName, &filestatus);
	UINT64 fileSize = filestatus.st_size/(1024);

	// if greater than 2 GB...
	if (fileSize >= (2*1024*1024))
	{
		//const char* warningText = "File size is over 2 GB (%1 KB).\nData will be lost if you Save and try to re-open.\nNeed to remove items from the scene and re-save until the scene is under 2 GB";//,fileSize;
		char warningText[256];
		sprintf(warningText,"File size is over 2 GB (%d KB).\nData will be lost if you Save and try to re-open.\nNeed to remove items from the scene and re-save until the scene is under 2 GB", fileSize);

		//throw a warning to user
		FBMessageBox("Warning", warningText, "OK");


		// Can we stop the save, or do we even need to? May need to if they chose close, and that triggered the save.
	}
	CustomeManagerMoBuTitle::UpdateToolbar();
}

std::string CustomeManagerMoBuTitle::GetProjectPathFromRegistry()
{
	// I need to get the RS_PROJECT environment variable from the registry, it's not being set correctly within
	//Sometimes env vars don't update until after log in and out after a tools install so we obtain the RS_PROJECT direct from the registry.
	HKEY hKey;
	TCHAR lValue[1024];
	DWORD bufLen = 1024*sizeof(TCHAR);
	long result;
	long result2;
	std::string pProjectVarEnvironVar;

	result = RegOpenKeyEx(HKEY_CURRENT_USER, "Environment", 0, KEY_READ, &hKey);
	if ( result == ERROR_SUCCESS )
	{
		result2 = RegQueryValueEx( hKey, TEXT("RS_PROJECT"),0,0,(LPBYTE) lValue,&bufLen );
		RegCloseKey(hKey);
		if (result2 == ERROR_SUCCESS)
		{
			pProjectVarEnvironVar = std::string(lValue, (size_t)bufLen - 1);
		}
	}
	return pProjectVarEnvironVar;
}

void CustomeManagerMoBuTitle::UpdateToolbar()
{
	//All the various callbacks use the one function because we want the same thing to happen for each, change the Toolbar to reflect the project
	for each (QWidget *widget in qApp->allWidgets())
	{
		QString winTitle = widget->windowTitle();

		if(winTitle.startsWith("MotionBuilder 2014"))
		{
			if((winTitle.contains("Project:")) || (winTitle.contains("Tools:")))
			{
				//This is the situation where a merge is called, we want to do nothing here, or we get double project and Tools :)
			}
			else
			{
				QString pProjectVar = QString::fromUtf8(GetProjectPathFromRegistry().c_str());
				pProjectVar = pProjectVar.toUpper();
				QString pToolsVar = getenv ("RS_TOOLSROOT");

				// Determine if you are in WildWest
				bool usingWildWest = false;
				bool toolsInstalled = true;
				const char * lWW = "wildwest";
				FBConfigFile lConfig("@Application.txt");
				if (strlen(lConfig.Get("Python", "PythonStartup")) > 1)
				{
					if (strstr(lConfig.Get("Python", "PythonStartup"), lWW))
						usingWildWest = true;
				}
				else //If startup path is zero and the Python Count is
				{
					if (strstr(lConfig.Get("AdditionnalPluginPath", "Count"), "0"))
						toolsInstalled = false;
				}
				if (toolsInstalled == true) //There are no tools we, are in vanilla MotionBuilder mode.
				{
					QString projectMotionBuilderTitle = winTitle;
					if (!pProjectVar.isNull())
						projectMotionBuilderTitle = projectMotionBuilderTitle.append(QString("        Project: %1").arg(pProjectVar));
					// Honestly I don't know if ever one variable will not be set while the other will be, but I guess better safe then sorry.
					if( !pToolsVar.isNull())
						projectMotionBuilderTitle = projectMotionBuilderTitle.append(QString("        Tools: %1").arg(pToolsVar));
					if (usingWildWest == true)
						projectMotionBuilderTitle = projectMotionBuilderTitle.append("        --WORKING IN WILDWEST--");
					widget->setWindowTitle (projectMotionBuilderTitle);

				}
			}
		}
	}
}