#include "StdAfx.h"
#include "PyInjection.h"

void PyInjectionInit () {
	// Not required since MoBu already initialized Python
	//if ( !Py_IsInitialized () )
	//	Py_Initialize() ;

	// We are using Boost vs. plain Python API
	//Py_InitModule ("PyInjection", PyInjectionMethods) ;

	//- Inject code now
	int ret =PyRun_SimpleString (
		"import pythoninjectioninteractionmode\n"
		"from pyfbsdk import *\n"
		"\n"
		// Standard technique
		"FBConfigFile.LoadConfiguration =pythoninjectioninteractionmode.LoadConfigurationForFBConfigFile\n"
		"\n"
	) ;

}
