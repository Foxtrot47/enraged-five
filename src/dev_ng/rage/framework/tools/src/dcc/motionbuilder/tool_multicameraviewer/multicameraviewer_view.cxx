// By Kristine Middlemiss, November 2013, Rockstar Toronto

//--- Class declaration
#include "multicameraviewer_view.h"

FBClassImplementation( ORView3D);

/************************************************
 *	Constructor
 ************************************************/
ORView3D::ORView3D() :
	mRender(NULL)
{
	mRender = new FBRenderer(0);
	FBViewingOptions* lVO = mRender->GetViewingOptions();
	// Since the Viewer is read only do not need to see any other things but models.
	lVO->PickingMode () = FBPickingMode::kFBPickingModeModelsOnly ; 
	mRender->SetViewingOptions (*lVO);
	mHasBeenRefreshed = false;
}
/************************************************
 *	Destructor
 ************************************************/
void ORView3D::FBDestroy()
{
	delete mRender;
}
/************************************************
 *	Refresh callback
 ************************************************/
void ORView3D::Refresh(bool pNow)
{
	FBView::Refresh(pNow);
	mHasBeenRefreshed = true;
}
/************************************************
 *	Expose callback
 ************************************************/
void ORView3D::ViewExpose()
{
	//Turns off the early frustum culling optimization, because with it on, it causes all the models to flicker randomly, very stroke inducing.
	mRender->FrustumCulling = false;
	//Indicate if a call to RenderBegin will also cause a re-evaluation of the scene. 
	mRender->AutoEvaluate = false;
	if(mRender->RenderBegin(0,0,Region.Position.X[1]-Region.Position.X[0],Region.Position.Y[1]-Region.Position.Y[0]))
	{
		// This call will destroy the frame buffer
		mRender->PreRender();
		mRender->Render();
		mRender->RenderEnd();
	}
}
/************************************************
 *	Input callback
 ************************************************/

void ORView3D::ViewInput(int pMouseX, int pMouseY, FBInputType pAction, int pButtonKey, int pModifier)
{
	mCamera.Input( pMouseX, pMouseY, pAction, pButtonKey, pModifier );
}
/************************************************
 *	Function to add the camera as a child to my FBView
 ************************************************/
// This was pretty cool, I just add the camera as a child of the view, so then it's accessible here!
bool ORView3D::AddChild (FBVisualComponent *pChild)
{
	FBCamera* lCamera = (FBCamera*)pChild;
	mRender->CurrentCamera = lCamera;
	mCamera.SetCamera(lCamera);
	return true;
}

/************************************************
*	Code to set the underlying renderer EvaluateMode property
************************************************/
void ORView3D::SetEvaluateMode(bool bEvalMode)
{
#ifndef MOBU_2014
	if (mRender->EvaluateMode != bEvalMode)
	{
		mRender->EvaluateMode = bEvalMode;
	}
#endif
}
