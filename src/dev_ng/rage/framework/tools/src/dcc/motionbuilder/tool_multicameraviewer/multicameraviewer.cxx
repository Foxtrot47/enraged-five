// By Kristine Middlemiss, November 2013, Rockstar Toronto

// SDK include
#include <fbsdk/fbsdk.h>

#ifdef KARCH_ENV_WIN
	#include <windows.h>

#endif

// Library declaration.
FBLibraryDeclare( multicameraviewer )
{
	// Registering my classes, only certain classes need to be done such as tool, FBView is not one of them, however my user object needs to be registered as a class and as an element
	// These names should be unique
	FBLibraryRegister( MultiCameraViewer );
	FBLibraryRegister( MultiCameraViewerData )
	FBLibraryRegisterElement( MultiCameraViewerData )
}
FBLibraryDeclareEnd; 

// Library functions
// If I wanted anything to happen on DLL load, I would add code here, but this tool is good!
bool FBLibrary::LibInit()		{ return true; }
bool FBLibrary::LibOpen()		{ return true; }
bool FBLibrary::LibReady()		{ return true; }
bool FBLibrary::LibClose()		{ return true; }
bool FBLibrary::LibRelease()	{ return true; }