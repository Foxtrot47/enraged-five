#include "multicameraviewer_camera_control.h"

#include <algorithm>
#include <math.h>

RSCameraControl::RSCameraControl() : pParentLayout(NULL), mIsLive(false), mIsCustomCamera(false)
{
}

RSCameraControl::~RSCameraControl()
{
	CleanUI();
}

void RSCameraControl::Init(FBLayout* pLayout, int index, int numRows, int windowSizeWidth, int windowSizeHeight, ControlScheme scheme)
{
	mIndex = index;
	pParentLayout = pLayout;

	sprintf(cContainerName, "CameraContainer%d", mIndex);
	sprintf(cViewName, "CameraView%d", mIndex);
	sprintf(cCameraListName, "CameraList%d", mIndex);
	sprintf(cLensListName, "LensList%d", mIndex);
	sprintf(cLiveButtonName, "LiveButton%d", mIndex);

	InitUI(numRows, windowSizeWidth, windowSizeHeight);

	mView.SetControlScheme(scheme);
}

void RSCameraControl::ReInit(int numRows, int windowSizeWidth, int windowSizeHeight)
{
	CleanUI();
	InitUI(numRows, windowSizeWidth, windowSizeHeight);
}

void RSCameraControl::InitUI(int numRows, int width, int height)
{
	mHeightModifier = (1.0 / numRows) * 0.99;
	mWidthModifier = (1.0 / numRows);

	int adjustedWidth = width - (mMargin * (numRows + 1));
	int adjustedHeight = height - (mMargin * (numRows + 1));

	// The number of rows determines which view this will attach itself next to
	int attachXIndex = -1, attachYIndex = -1;
	FBAttachType attachX = kFBAttachLeft, attachY = kFBAttachTop;
	if (mIndex >= numRows)
	{
		// We need to attach to the right of another view
		attachXIndex = mIndex - numRows;
		attachX = kFBAttachRight;
	}
	if (mIndex % numRows != 0)
	{
		// We need to attach to the bottom of another view
		attachYIndex = mIndex - 1;
		attachY = kFBAttachBottom;
	}

	char cAttachX[256];
	cAttachX[0] = 0;
	if (attachXIndex != -1)
	{
		sprintf(cAttachX, "CameraContainer%d", attachXIndex);
	}
	char cAttachY[256];
	cAttachY[0] = 0;
	if (attachYIndex != -1)
	{
		sprintf(cAttachY, "CameraContainer%d", attachYIndex);
	}

	pParentLayout->AddRegion(cContainerName, cContainerName,
		mMargin, attachX, cAttachX, 1.0,
		mMargin, attachY, cAttachY, 1.0,
		adjustedWidth * mWidthModifier, kFBAttachNone, NULL, 1.0,
		adjustedHeight * mHeightModifier, kFBAttachNone, NULL, 1.0);
	pParentLayout->SetControl(cContainerName, mContainer);

	pParentLayout->AddRegion(cViewName, cViewName,
		mMargin, kFBAttachLeft, cContainerName, 1.0,
		mMargin * 3, kFBAttachTop, cContainerName, 1.0,
		-mMargin, kFBAttachRight, cContainerName, 1.0,
		-40, kFBAttachBottom, cContainerName, 1.0);
	if (IsActive())
	{
		pParentLayout->SetView(cViewName, mView);
	}

	pParentLayout->AddRegion(cLiveButtonName, cLiveButtonName,
		0, kFBAttachLeft, cContainerName, 1.0,
		0, kFBAttachTop, cContainerName, 1.0,
		0, kFBAttachRight, cContainerName, 1.0,
		-mMargin, kFBAttachTop, cViewName, 1.0);
	pParentLayout->SetControl(cLiveButtonName, mLiveButton);

	pParentLayout->AddRegion(cCameraListName, cCameraListName,
		mMargin, kFBAttachLeft, cContainerName, 1.0,
		-mMargin * 2.5, kFBAttachBottom, cContainerName, 1.0,
		0, kFBAttachWidth, cContainerName, 0.4f,
		20, kFBAttachNone, NULL, 1.0);
	pParentLayout->SetControl(cCameraListName, mCameraList);

	pParentLayout->AddRegion(cLensListName, cLensListName,
		mMargin, kFBAttachRight, cCameraListName, 1.0,
		-mMargin * 2.5, kFBAttachBottom, cContainerName, 1.0,
		0, kFBAttachWidth, cContainerName, 0.4f,
		mMargin * 2, kFBAttachNone, NULL, 1.0f);
	pParentLayout->SetControl(cLensListName, mLensList);

	mContainer.ItemWidth = mContainer.Region.Width - 2;
	mContainer.ItemHeight = mContainer.Region.Height;
}

void RSCameraControl::CleanUI()
{
	pParentLayout->ClearControl(cLensListName);
	pParentLayout->ClearControl(cCameraListName);
	pParentLayout->ClearControl(cViewName);
	pParentLayout->SetView(cViewName, NULL);
	pParentLayout->ClearControl(cLiveButtonName);
	pParentLayout->ClearControl(cContainerName);
	pParentLayout->RemoveRegion(cLensListName);
	pParentLayout->RemoveRegion(cCameraListName);
	pParentLayout->RemoveRegion(cViewName);
	pParentLayout->RemoveRegion(cLiveButtonName);
	pParentLayout->RemoveRegion(cContainerName);
}

void RSCameraControl::UIConfigure()
{
	FBString lenTypes = "16mm~25mm~35mm~50mm~75mm~120mm~Custom";
	FBSystem system;

	// ONE: CONTAINER 
	mContainer.Hint = "Usage:\n\nDrag and drop a camera from the Navigator into this location to setup a camera (opt. use the drop down to select the camera).\n\nOnce you set a camera here, double click to add a key to the camera switcher for this camera at the current time.";
	mContainer.OnDragAndDrop.Add(this, (FBCallback)&RSCameraControl::EventContainerDragAndDrop);
	mContainer.OnDblClick.Add(this, (FBCallback)&RSCameraControl::EventContainerDblClickSet);

	mLiveButton.Caption = "LIVE";
	mLiveButton.Hint = "Usage:\n\nWhen enabled, only this camera will refresh - resulting in better performance";
	mLiveButton.Style = kFB2States;
	mLiveButton.SetStateColor(kFBButtonState0, FBColor(1.0, 0.0, 0.0));
	mLiveButton.SetStateColor(kFBButtonState1, FBColor(0.0, 1.0, 0.0));
	mLiveButton.OnClick.Add(this, (FBCallback)&RSCameraControl::EventLiveButtonPressed);

	// TWO: CAMERA DROP DOWN
	mCameraList.Hint = "Usage:\n\nSelect a camera from the list to set the viewer for the selected camera. \n\nTo clear the camera, select 'None' from the drop down list.";
	mCameraList.Items.Clear();
	mCameraList.Items.Add("None");
	//populating drop down list
	for (int i = 0; i < system.Scene->Cameras.GetCount(); i++)
	{
		FBCamera* lCamera = system.Scene->Cameras[i];
		// Do not include MotionBuilder Default camera (except Producer Perspective), as they are not used in our camera animation workflows.
		if (lCamera->Name != "Producer Front" && lCamera->Name != "Producer Back" && lCamera->Name != "Producer Right" && lCamera->Name != "Producer Left" && lCamera->Name != "Producer Top" && lCamera->Name != "Producer Bottom")
			mCameraList.Items.Add(lCamera->LongName, (kReference)lCamera);
	}
	mCameraList.Items.Add("Create Camera");
	mCameraList.OnChange.Add(this, (FBCallback)&RSCameraControl::EventCameraListOnChange);

	// THREE: LENS DROP DOWN
	mLensList.Hint = "Usage:\n\nSelect a lens from the list to change the camera lens that is being used. \n\nThe selection 'Custom' does not have any value assigned to it,\nit is there to represent an FOV that does not have an equivalent lens value.";
	mLensList.Items.SetString(lenTypes);
	mLensList.Selected(6, true);
	mLensList.OnChange.Add(this, (FBCallback)&RSCameraControl::EventLensListOnChange);
}

void RSCameraControl::MainWindowResized(int newWidth, int newHeight, int numRows)
{
	int adjustedWidth = newWidth - (mMargin * (numRows + 1));
	int adjustedHeight = newHeight - (mMargin * (numRows + 1));

	mContainer.Region.Width = adjustedWidth * mWidthModifier;
	mContainer.Region.Height = adjustedHeight * mHeightModifier;

	mContainer.ItemWidth = mContainer.Region.Width - 2;
	mContainer.ItemHeight = mContainer.Region.Height;
}

int RSCameraControl::GetControlWidth()
{
	return mContainer.Region.Width + mMargin;
}

/************************************************
*	Set Lens Drop Down List
************************************************/
void RSCameraControl::SetLensDropDownList(FBCamera* pCamera)
{
	FBString lenTypes = "16mm~25mm~35mm~50mm~75mm~120mm~Custom";
	mIsCustomCamera = false;

	// Determine if this is a curstom R* cam or not
	FBProperty* lCustProp = pCamera->PropertyList.Find("Lens");
	if (lCustProp && lCustProp->IsUserProperty())
	{
		FBStringList* pPropList = lCustProp->GetEnumStringList(false);
		if (pPropList)
		{
			lenTypes = pPropList->AsString();
			mIsCustomCamera = true;
		}
	}

	mLensList.Items.SetString(lenTypes);

	if (mIsCustomCamera)
	{
		FBPropertyEnum* pEnum = (FBPropertyEnum*)lCustProp;
		int enumIdx = pEnum->AsInt();
		mLensList.Selected(enumIdx, true);
	}
	else
	{
		// Set Lens Drop Down
		FBProperty* lProp = pCamera->PropertyList.Find("FieldOfView");
		if (lProp)
		{
			double lValue = 0.0;
			lProp->GetData(&lValue, sizeof(lValue));

			if (lValue == 70.0) 
				mLensList.Selected(0, true);
			else if (lValue == 48.0)
				mLensList.Selected(1, true);
			else if (lValue == 37.0)
				mLensList.Selected(2, true);
			else if (lValue == 25.25)
				mLensList.Selected(3, true);
			else if (lValue == 17.10) 
				mLensList.Selected(4, true);
			else if (lValue == 10.0) 
				mLensList.Selected(5, true);
			else 
				mLensList.Selected(6, true);
		}
	}
}

void RSCameraControl::UpdateFOV(FBEventConnectionDataNotify &pEvent)
{
	// Now I need to see if the camera is in the our viewer and I need to make sure there is a camera to set
	if (IsActive())
	{
		FBCamera* lCamera = (FBCamera*)mContainer.Items.GetReferenceAt(0); // Will always be element 0 as there is only one element in the list.
		if (lCamera)
		{
			// If Spot 1 holds the camera that the FOV is being changed for.
			if (lCamera == pEvent.Plug->GetOwner())
			{
				FBProperty* lProp = (FBProperty*)((FBPlug*)(pEvent.Plug));
				bool isLensProperty = (strcmp(lProp->GetName(), "Lens")==0);
				// Determine if this event is Lens data (custom camera) or FOV data (basic camera prop)
				if (!mIsCustomCamera && isLensProperty && lProp->IsUserProperty())
				{
					//Make this a custom camera
					FBStringList* pPropList = lProp->GetEnumStringList(false);
					if (pPropList)
					{
						mLensList.Items.SetString(pPropList->AsString());
						mIsCustomCamera = true;
					}
				}

				if (mIsCustomCamera)
				{
					if (isLensProperty)
					{
						double lValue = *((double*)pEvent.GetData());
						int enumIdx = std::min(std::max((int)lValue, 0), mLensList.Items.GetCount()-1);
						mLensList.Selected(enumIdx, true);
					}
				}
				else if (!isLensProperty)
				{
					double lValue = *((double*)pEvent.GetData());
					// Set Lens Drop Down
					if (lValue == 70.0) mLensList.Selected(0, true);
					else if (lValue == 48.0) mLensList.Selected(1, true);
					else if (lValue == 37.0) mLensList.Selected(2, true);
					else if (lValue == 25.25) mLensList.Selected(3, true);
					else if (lValue == 17.10) mLensList.Selected(4, true);
					else if (lValue == 10.0) mLensList.Selected(5, true);
					else mLensList.Selected(6, true);
				}
			}
		}
	}
}

void RSCameraControl::Reset(bool bSetData)
{
	SetIsLive(false);
	mContainer.Items.Clear();
	pParentLayout->ClearControl(cViewName);
	mLensList.Items.SetString("16mm~25mm~35mm~50mm~75mm~120mm~Custom");
	mLensList.Selected(6, true);
	mCameraList.Selected(0, true);
	if (bSetData)
	{
		SetPersistentData(NULL);
	}
}

void RSCameraControl::AddCamera(FBCamera* pCamera)
{
	if (!IsActive()) // Only populate if the spot is empty.
	{
		bool bCameraNameFound = false;
		for (int j = 0; j < mCameraList.Items.GetCount(); j++)
		{
			if (pCamera->LongName == mCameraList.Items.GetAt(j))
			{
				if (!mCameraList.IsSelected(j))
					mCameraList.Selected(j, true);
				bCameraNameFound = true;
				break;
			}
		}

		if (!bCameraNameFound)
		{
			// Probably a producer perspective cam, which we don't show in the list on purpose - just switch to None but don't select
			mCameraList.Selected(0, false);
		}

		// Actually do the camera add
		mContainer.Items.Add(pCamera->LongName, (kReference)pCamera);
		pParentLayout->SetView(cViewName, mView);
		mView.AddChild((FBVisualComponent*)pCamera); //Pass camera that is set to the FBView object
		SetLensDropDownList(pCamera);
		SetPersistentData(pCamera);
	}
}

/************************************************
*	Code to remove deleted cameras from the camera list
************************************************/
void RSCameraControl::RemoveItemFromCameraList(FBString pCameraName)
{
	// Need to remove newly deleted camera's to the tool's drop down boxes.
	for (int i = 0; i<mCameraList.Items.GetCount(); i++)
	{
		FBString existingItem = mCameraList.Items.GetAt(i);

		if (pCameraName == existingItem)
		{
			mCameraList.Items.RemoveAt(i);
			mCameraList.Selected(0, true);
			break;
		}
	}
}

/************************************************
*	Code to set the underlying view as double buffered or not
************************************************/
void RSCameraControl::SetDoubleBuffered(bool bValue)
{
	if (mView.DoubleBuffer != bValue)
	{
		mView.DoubleBuffer = bValue;
	}
}

/************************************************
*	Code to add new cameras to the camera list
************************************************/
bool RSCameraControl::AddItemToCameraList(FBCamera* pCamera)
{
	FBString cameraName = pCamera->LongName;

	for (int i = 0; i < mCameraList.Items.GetCount(); i++)
	{
		FBString existingItem = mCameraList.Items.GetAt(i);
		if (existingItem == cameraName)
			break;

		if (i == mCameraList.Items.GetCount() - 1)
		{
			// This is the last item, there was no entry found
			mCameraList.Items.InsertAt(i, cameraName, (kReference)pCamera);
			return true;
		}
	}
	return false;
}

/************************************************
*	Code to rename a camera in the camera list
************************************************/
void RSCameraControl::RenameCameraEntry(FBString oldName, FBString newName)
{
	for (int i = 0; i < mCameraList.Items.GetCount(); i++)
	{
		FBString existingItem = mCameraList.Items.GetAt(i);

		if (oldName == existingItem)
		{
			mCameraList.Items.SetAt(i, newName);
			break;
		}
	}
}

/************************************************
*	Drag and drop Event
************************************************/
void RSCameraControl::EventContainerDragAndDrop(HISender pSender, HKEvent pEvent)
{
	FBEventDragAndDrop	lDragAndDrop(pEvent);

	FBComponent* lComponent = NULL;
	if (lDragAndDrop.GetCount() > 0)
	{
		lComponent = lDragAndDrop.Get(0);

		switch (lDragAndDrop.State)
		{
		case kFBDragAndDropDrag:
		{
			if (lComponent && lComponent->Is(FBCamera::TypeInfo))
			{
				lDragAndDrop.Accept();
			}
		}
		break;
		case kFBDragAndDropDrop:
		{
			// Now need to check witch container is being altered so the correct one can be updated with the camera dropped in.
			if (lComponent && lComponent->Is(FBCamera::TypeInfo))
			{
				FBCamera* lCamera = (FBCamera*)lComponent;
				mContainer.Items.Clear(); //Ensure we only have one entry in the container
				AddCamera(lCamera);
			}
		}
		break;
		}
	}
}
/************************************************
*	Double Click Event
************************************************/
void RSCameraControl::EventContainerDblClickSet(HISender pSender, HKEvent pEvent)
{
	//Setting up camera switcher:
	FBCameraSwitcher cameraSwitcher;
	FBSystem system;
	FBProperty* cameraSwitcherIndexProp = cameraSwitcher.PropertyList.Find("Camera Index");
	if (cameraSwitcherIndexProp)
	{
		FBPropertyAnimatable* lPropertyAnimatable = (FBPropertyAnimatable*)cameraSwitcherIndexProp;
		lPropertyAnimatable->SetAnimated(true);

		//When the user double clicks the camera it keys the camera switcher.
		FBComponent* lComp = FBGetFBComponent((HIObject)pSender);
		if (lComp && lComp->Is(FBVisualContainer::TypeInfo))
		{
			//mContainer contains the container that was double clicked.
			FBVisualContainer* mContainer = (FBVisualContainer*)lComp;
			//Key the camera switcher to be the camera you have double clicked
			if (IsActive())
			{
				mContainer->ItemIndex = -1; // This de-selects the container from turning blue.

				double lCounter = 1.0; // The Camera Index starts a 1.0 not 0.0
				const char* lProducerName = "Producer";
				for (int i = 0; i< system.Scene->Cameras.GetCount(); i++)
				{
					const char* lCameraName = (const char*)system.Scene->Cameras[i]->Name.AsString();
					if (strncmp(lProducerName, lCameraName, 8) == 0) // ignore if it's a default MoBu Camera as those are not to be put in the Camera Switcher
						continue;
					else
					{
						if (system.Scene->Cameras[i]->Name == ((FBCamera*)mContainer->Items.GetReferenceAt(0))->Name)
						{
							FBAnimationNode*  lNode = lPropertyAnimatable->GetAnimationNode();
							lNode->SetCandidate((double*)&lCounter); // Need to Key it this way rather then KeyAdd, as I was getting a really weird keying of the inbetween cameras thing...
							lNode->KeyCandidate();
							break;
						}
						lCounter++;
					}
				}
			}
		}
	}
}

/************************************************
*	Camera List Change Event
************************************************/
void RSCameraControl::EventCameraListOnChange(HISender pSender, HKEvent pEvent)
{
	for (int i = 0; i < mCameraList.Items.GetCount(); i++)
	{
		if (mCameraList.IsSelected(i))
		{
			FBString lCameraName = mCameraList.Items.GetAt(i);
			if (lCameraName == "None")
			{
				Reset(true);
			}
			else if (lCameraName == "Create Camera")
			{
				FBCamera* lNewCamera = NULL;
				for (int j = 1; lNewCamera == NULL; ++j)
				{
					// Try to create a new camera with a unique name:
					char cName[64];
					sprintf_s(cName, "Camera %d", j);
					FBComponentList pList;
					FBFindObjectsByName(cName, pList, false);
					if (pList.GetCount() == 0)
					{
						lNewCamera = new FBCamera(cName);
					}
				}
				mContainer.Items.Clear();
				AddCamera(lNewCamera);
			}
			else
			{
				FBSystem system;
				// You cannot use FBFindModelByLabelName to get the default Camera's need to go through the camera list, so might as well do this for all of them.
				for (int j = 0; j < system.Scene->Cameras.GetCount(); j++)
				{
					if (system.Scene->Cameras[j]->LongName == lCameraName)
					{
						FBCamera* lCamera = system.Scene->Cameras[j];
						mContainer.Items.Clear(); //FBContrainer allows for multiple items to be added to one container, this ensures only one is added.
						AddCamera(lCamera);
						break;
					}
				}
			}
		}
	}
}

/************************************************
*	Live Button Pressed Event
************************************************/
void RSCameraControl::EventLiveButtonPressed(HISender pSender, HKEvent pEvent)
{
	mIsLive = mLiveButton.State != 0;
	if (mIsLive && !IsActive())
	{
		SetIsLive(false);
	}
}

/************************************************
*	Lens List Change Event
************************************************/
void RSCameraControl::EventLensListOnChange(HISender pSender, HKEvent pEvent)
{
	// Just as a side note, when some one switches the Lens list, it will set the FOV on the camera, which in turn will trigger the OnConnectionDataNotify callback.
	if (IsActive())
	{
		for (int i = 0; i < mLensList.Items.GetCount(); i++)
		{
			if (mLensList.IsSelected(i))
			{
				FBString lLenName = mLensList.Items.GetAt(i);
				FBCamera* lCamera = (FBCamera*)mContainer.Items.GetReferenceAt(0); // Will always be element 0 as there is only one element in the list.
				if (lCamera)
				{
					if (mIsCustomCamera)
					{
						// The CamRockTab drives the FOV from a custom property
						FBProperty* lProp = lCamera->PropertyList.Find("Lens");
						if (lProp->IsAnimatable())
						{
							FBPropertyAnimatable* lPropLens = (FBPropertyAnimatable*)lProp;
							if (lPropLens)
							{
								lPropLens->SetAnimated(true);
								lPropLens->SetData(&i);
							}
						}
					}
					else
					{
						FBProperty* lProp = lCamera->PropertyList.Find("FieldOfView");
						if (lProp->IsAnimatable())
						{
							FBPropertyAnimatable* lPropFOV = (FBPropertyAnimatable*)lProp;
							if (lPropFOV)
							{
								lPropFOV->SetAnimated(true);
								double lValue;

								if (lLenName == "16mm")
								{
									lValue = 70.0;
									lPropFOV->SetData(&lValue);
								}
								else if (lLenName == "25mm")
								{
									lValue = 48.0;
									lPropFOV->SetData(&lValue);
								}
								else if (lLenName == "35mm")
								{
									lValue = 37.0;
									lPropFOV->SetData(&lValue);
								}
								else if (lLenName == "50mm")
								{
									lValue = 25.25;
									lPropFOV->SetData(&lValue);
								}
								else if (lLenName == "75mm")
								{
									lValue = 17.10;
									lPropFOV->SetData(&lValue);
								}
								else if (lLenName == "120mm")
								{
									lValue = 10.0;
									lPropFOV->SetData(&lValue);
								}
							}
						}
					}
				}
				break;
			}
		}
	}
}

/************************************************
*  FindPersistentData
************************************************/
MultiCameraViewerData* RSCameraControl::FindPersistentData(bool pCreate)
{
	// First we want to see if there is any infos for this tool present in the scene
	FBScene* lScene = mSystem.Scene;
	int lIdx = 0;
	for (lIdx = 0; lIdx < lScene->UserObjects.GetCount(); ++lIdx) {
		FBUserObject* lObject = lScene->UserObjects[lIdx];
		if (lObject->Is(MultiCameraViewerData::TypeInfo))
			return (MultiCameraViewerData*)lObject;
	}
	MultiCameraViewerData* lData = 0;

	if (pCreate)
	{
		lData = new MultiCameraViewerData("MultiCameraViewerData");
	}
	return lData;
}
/************************************************
*  GetPersistentData
************************************************/
FBArrayTemplate<FBCamera*> RSCameraControl::GetPersistentData()
{
	return FindPersistentData(true)->cameraArray;
}
/************************************************
*  SetPersistentData
************************************************/
void RSCameraControl::SetPersistentData(FBCamera* pCamera)
{
	MultiCameraViewerData* pData = FindPersistentData(true);
	if (mIndex >= pData->cameraArray.GetCount())
	{
		for (int i = pData->cameraArray.GetCount(); i < mIndex+1; i++)
		{
			pData->cameraArray.InsertAt(i, NULL);
		}
	}
	pData->cameraArray.SetAt(mIndex, pCamera);
}