// By Kristine Middlemiss, November 2013, Rockstar Toronto

#ifndef __MULTICAMERVIEWER_VIEW_H__
#define __MULTICAMERVIEWER_VIEW_H__

// SDK include
#include <fbsdk/fbsdk.h>
#include "multicameraviewer_camera.h"

// View 3D class
class ORView3D : public FBView
{
	FBClassDeclare( ORView3D, FBView );
private:
	// Renderer object
	FBRenderer*	mRender;
	RSCameraController mCamera;
	bool mUseTouch;
	bool mHasBeenRefreshed;
public:
	// Constructor
	ORView3D();
	// Destructor
	virtual void FBDestroy();

	// Refresh callback.
	virtual void Refresh(bool pNow=false);
	// Expose callback.
	virtual void ViewExpose();
	// Input callback.
	virtual void ViewInput(int pMouseX, int pMouseY, FBInputType pAction, int pButtonKey, int pModifier);
	// Add the camera to the FBView
	virtual bool AddChild (FBVisualComponent *pChild);
	// Is the user positioning this camera?
	bool isBeingControlled(){return (mCamera.isBeingControlled());}
	// Set camera control scheme
	void SetControlScheme(ControlScheme scheme){mCamera.SetControlScheme(scheme);}
	// Set the touch mode flag on the camera
	void SetUseTouch(bool bUseTouch){mCamera.SetUseTouch(bUseTouch);}
	// Set the Evaluate mode
	void SetEvaluateMode(bool bEvalMode);
	// Return if this view has been refreshed at least once since creation
	bool HasBeenRefreshed() { return mHasBeenRefreshed; }
};

#endif /* __MULTICAMERVIEWER_VIEW_H__ */
