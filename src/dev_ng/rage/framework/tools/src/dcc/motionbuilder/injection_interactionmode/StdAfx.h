#pragma once

#undef _DEBUG
#define _CRT_SECURE_NO_WARNINGS
#define MOBU_PYTHON_PLUGIN

// Suppress warnings from boost header files.
#pragma warning(disable:4100) // Unreferenced formal parameter.
#pragma warning(disable:4127) // Conditional expression is constant.
#pragma warning(disable:4244) // Conversion from 'long double' to 'double', possible loss of data.
#pragma warning(disable:4512) // Assignment operator could not be generated.
#pragma warning(disable:4996) // 'function': was declared deprecated.

#define BOOST_ALL_NO_LIB // Otherwise will ask for boost_python-vc100-mt-1_46_1.lib
#include <boost/python.hpp>
using namespace boost::python ;
#pragma comment(lib, "boost_python-py27-vc100-mt-1_46_1.lib")

#include <fbsdk/fbsdk.h>
#include <pyfbsdk/pyfbsdk.h>
#ifdef KARCH_ENV_WIN
#include <windows.h>
#endif

#pragma comment(lib,"fbsdk.lib")
#pragma comment(lib,"fbxsdk.lib")
#pragma comment(lib,"pyfbsdk.lib")
#pragma comment(lib,"python27.lib")
