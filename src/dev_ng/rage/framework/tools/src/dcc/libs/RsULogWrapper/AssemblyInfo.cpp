#pragma managed
using namespace System::Reflection;
using namespace System::Runtime::CompilerServices;
using namespace System::Runtime::InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("RsULogWrapper")];
[assembly: AssemblyDescription("")];
[assembly: AssemblyConfiguration("")];
[assembly: AssemblyCompany("GDroege")];
[assembly: AssemblyProduct("RsULogWrapper")];
[assembly: AssemblyCopyright("Copyright � GDroege 2010")];
[assembly: AssemblyTrademark("")];
[assembly: AssemblyCulture("")];

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)];

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("6A805F57-28A7-4978-88AC-4600949478C2")];

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.0.*")];
[assembly: AssemblyFileVersion("1.0.0.0")];
