#include "Ulogger.h"

#include "system/param.h"
#include "file/asset.h"
#include "system/exec.h"
#include "file/device.h"

#pragma warning( push )
#pragma warning( disable : 4668 )

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <shellapi.h>
#pragma warning( pop )

void FindFiles(const fiFindData &data, void *userArg)
{
	atArray<atString>& FileList = *((atArray<atString>*)userArg);
	FileList.PushAndGrow( atString(data.m_Name) );
}

UniversalLogHelper::UniversalLogHelper()
{
	m_pULogFile = NULL;
	m_strTool = "";
}

UniversalLogHelper::~UniversalLogHelper()
{
	if(m_pULogFile)
	{
		m_pULogFile->Close();
		delete m_pULogFile;
		m_pULogFile = NULL;
	}
}

UniversalLogHelper& UniversalLogHelper::GetInstance()
{
	static UniversalLogHelper s_Instance;
	return s_Instance;
}

bool UniversalLogHelper::InitPath(const char* path, const char* context, bool append)
{
	if(m_pULogFile)
	{
		m_pULogFile->Init(atString(path), true, append);
	}
	else
	{
		m_pULogFile = rage_new UniversalLogFile(atString(path), atString(context), append, true);
	}

	if(m_pULogFile && context)
	{
		m_pULogFile->SetContext(atString(context));
	}

	return m_pULogFile->HasValidDoc();
}
void UniversalLogHelper::LogMessage(const char* msg, const char* context, const char* filecontext)
{
	if(m_pULogFile)
	{
		m_pULogFile->LogMessage(UniversalLogFile::sLogMessageEventArgs(atString(context),atString(msg),atString(filecontext)));
	}
	//	return RsULogWrapper::GetInstance()->LogMessage(msg, context);
}
void UniversalLogHelper::LogError(const char* msg, const char* context, const char* filecontext)
{
	if(m_pULogFile)
	{
		m_pULogFile->LogError(UniversalLogFile::sLogMessageEventArgs(atString(context),atString(msg),atString(filecontext)));
	}
	//	return RsULogWrapper::GetInstance()->LogError(msg, context);
}
void UniversalLogHelper::LogWarning(const char* msg, const char* context, const char* filecontext)
{
	if(m_pULogFile)
	{
		m_pULogFile->LogWarning(UniversalLogFile::sLogMessageEventArgs(atString(context),atString(msg),atString(filecontext)));
	}
	//	return RsULogWrapper::GetInstance()->LogWarning(msg, context);
}
void UniversalLogHelper::LogDebug(const char* msg, const char* context, const char* filecontext)
{
	if(m_pULogFile)
	{
		m_pULogFile->LogDebug(UniversalLogFile::sLogMessageEventArgs(atString(context),atString(msg),atString(filecontext)));
	}	
	//	return RsULogWrapper::GetInstance()->LogDebug(msg, context);
}
void UniversalLogHelper::ProfileStart(const char* context)
{
	if(m_pULogFile)
	{
		m_pULogFile->ProfileStart(UniversalLogFile::sProfileMessageEventArgs(atString(context)));
	}
}
void UniversalLogHelper::ProfileEnd(const char* context)
{
	if(m_pULogFile)
	{
		m_pULogFile->ProfileEnd(UniversalLogFile::sProfileMessageEventArgs(atString(context)));
	}
}

void UniversalLogHelper::Refresh()
{
	if(m_pULogFile)
	{
		m_pULogFile->Refresh();
	}
	//	return RsULogWrapper::GetInstance()->LogDebug(msg, context);
}
void UniversalLogHelper::Flush()
{
	if(m_pULogFile)
	{
		m_pULogFile->Flush();
	}
}
void UniversalLogHelper::ImmediateFlush(bool flush)
{
	if(m_pULogFile)
	{
		m_pULogFile->ImmediateFlush(flush);
	}
}
void UniversalLogHelper::ShowDialog(const char *parameters)
{
	char cToolsRoot[RAGE_MAX_PATH];
	sysGetEnv("RS_TOOLSROOT", cToolsRoot, RAGE_MAX_PATH);

	char cULogPath[RAGE_MAX_PATH];
	sprintf_s(cULogPath, RAGE_MAX_PATH, "%s/bin/UniversalLogViewer/UniversalLogViewer.exe", cToolsRoot);

	SHELLEXECUTEINFOA ExecuteInfo;
	memset(&ExecuteInfo, 0, sizeof(ExecuteInfo));

	ExecuteInfo.cbSize       = sizeof(ExecuteInfo);
	ExecuteInfo.fMask        = 0;                
	ExecuteInfo.hwnd         = 0;                
	ExecuteInfo.lpVerb       = "open";
	ExecuteInfo.lpFile       = cULogPath;  
	ExecuteInfo.lpParameters = parameters;           
	ExecuteInfo.lpDirectory  = 0;            
	ExecuteInfo.nShow        = SW_SHOW;
	ExecuteInfo.hInstApp     = 0;

	ShellExecuteExA(&ExecuteInfo);
}

void UniversalLogHelper::SetProgressMessage(const char * pMessage, ...)
{
	char cMsg[RAGE_MAX_PATH*4];
	memset(cMsg,0,RAGE_MAX_PATH*4);
	va_list args;
	va_start (args, pMessage);
	vsprintf_s (cMsg, RAGE_MAX_PATH*4, pMessage, args);
	va_end (args);

	atString msg(cMsg);
	//msg.Uppercase();
	if ( msg.StartsWith( "ERROR: " ) || (strstr( msg.c_str(), " ERROR : " ) != NULL) )
	{
		msg.Set( cMsg, (int)strlen(cMsg), 7 );

		Errorf(msg.c_str());

		LogError(msg.c_str(), m_strTool);
	}
	else if ( msg.StartsWith( "WARNING: " ) || (strstr( msg.c_str(), " WARNING : " ) != NULL))
	{
		msg.Set( cMsg, (int)strlen(cMsg), 9 );

		Warningf(msg.c_str());

		LogWarning(msg.c_str(), m_strTool);
	}
	else
	{
		Displayf(msg.c_str());

		LogMessage(msg.c_str(), m_strTool);
	}
}

void UniversalLogHelper::AddAdditionalLog(const char* pLogFilename)
{
	if (stricmp(ASSET.FindExtensionInPath(pLogFilename),".ulog")==0)
	{
		atString newLog = atString(pLogFilename);
		//make safe slashes
		newLog.Replace("/","\\");
		//dont add duplicates
		if(m_atAdditionalLogs.Find(newLog)==-1)
			m_atAdditionalLogs.PushAndGrow(atString(pLogFilename));
	}
}

void UniversalLogHelper::AddAdditionalLogDir(const char* pLogDir)
{
	atArray<atString> logFiles;
	ASSET.EnumFiles( pLogDir, &FindFiles, &logFiles );

	atString strLogFilenames("");
	for(int i=0; i < logFiles.GetCount(); ++i)
	{
		char cFullLogPath[RAGE_MAX_PATH];
		sprintf(cFullLogPath, "%s\\%s", pLogDir, logFiles[i].c_str());
		AddAdditionalLog(cFullLogPath);
	}
}

void UniversalLogHelper::PreExport(const char *pLogFilename, const char* pContext, bool bContextAppend, bool /*bMessageAppend */)
{
	InitPath(pLogFilename, pContext, bContextAppend);
}
		
void UniversalLogHelper::PostExport( bool bShowDialog, bool bShowUpAnyway)
{
	bool bHasWarningsOrErrors = false;

	if(m_pULogFile != NULL)
	{
		AddAdditionalLog(m_pULogFile->Filename());

		m_pULogFile->Close();
		delete m_pULogFile;
		m_pULogFile = NULL;

	}

	for(int i=0; i < m_atAdditionalLogs.GetCount(); ++i)
	{
		UniversalLogFile logFile(m_atAdditionalLogs[i],atString(""));
		if(logFile.HasErrorsOrWarnings()) 
			bHasWarningsOrErrors=true;
	}

	if(m_atAdditionalLogs.GetCount())
	{
		atString strLogFilenames = atString::Join(m_atAdditionalLogs, " ");

		m_atAdditionalLogs.clear();

		if(!bShowUpAnyway && (!bHasWarningsOrErrors || !bShowDialog) )
			return;

		ShowDialog(strLogFilenames.c_str());
	}
}


void UniversalLogHelper::PostExport( const char* pLogDir, bool bShowDialog, bool bShowUpAnyway )
{
	AddAdditionalLogDir(pLogDir);

	PostExport(bShowDialog, bShowUpAnyway);
}
