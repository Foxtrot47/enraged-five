//Switching to managed to include the managed HitPointsProperty ref class.
//#pragma managed

// Managed types cannot be used in unmanaged code, so call into managed code
// from here.

#ifdef _DOLIBEXPORT
#define __declspec(dllexport)
#else
#define __declspec(dllimport)
#endif

//extern "C"
//{
// namespace// anonymous namespace
// {
	//	using namespace System;
	bool gInit(const char *path, const char *context);
	void gLogError(const char *msg, const char *context = "");
	void gLogWarning(const char *msg, const char *context = "");
	void gLogMessage(const char *msg, const char *context = "");
	void gLogDebug(const char *msg, const char *context = "");
//}
//};