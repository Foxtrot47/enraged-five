//**************************************************************************/
// Copyright (c) 1998-2006 Autodesk, Inc.
// All rights reserved.
// 
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Autodesk, Inc., and are
// protected by Federal copyright law. They may not be disclosed to third
// parties or copied or duplicated in any form, in whole or in part, without
// the prior written consent of Autodesk, Inc.
//**************************************************************************/
// AUTHOR: Nicolas Desjardins
// DATE: 2007-07-24
//***************************************************************************/

#ifndef _RSULOG_H_
#define _RSULOG_H_

#pragma managed
// Declare references to .NET assemblies.  Notice that it doesn't matter if
// an assembly is originally written in C++/CLI or C#, we use it same way.  
// The only difference from an external point of view is that C++/CLI may be
// aware of native types.

// SceneExplorer.dll is a 3ds Max .NET assembly written in C++/CLI.
//#using <SceneExplorer.dll>
// ExplorerFramework is a 3ds Max .NET assembly written in C#.
#using "RSG.Base.dll"
// Microsoft .NET Framework assembly.
#using <System.dll>

namespace RSGULogManaged
{
using namespace RSG::Base::Logging::Universal;
using namespace RSG::Base::Logging;
using namespace System;
	// This is a managed class.  Note the 'ref' keyword.
	public ref class RsULog
	{
	public:
		static bool Init(const char path[], const char context[])
		{
			try
			{
				mLogObj = gcnew UniversalLogFile(gcnew String(path), gcnew String(context), true, true);
				return mLogObj!=nullptr;
			}
			catch (Exception^ e)
			{
				Console::WriteLine("Error: {0}\n", e);
				return false;
			}
		}
		static void LogError(const char msg[], const char context[])
		{
			if(nullptr==mLogObj)
			{
				return;
			}
			try
			{
				LogMessageEventArgs ^args = gcnew LogMessageEventArgs(gcnew String(context), gcnew String(msg));
				mLogObj->LogError(nullptr, args);
			}
			catch (Exception^ e)
			{
				Console::WriteLine("Error: {0}\n", e);
			}
		}
		static void LogWarning(const char msg[], const char context[])
		{
			if(nullptr==mLogObj)
			{
				return;
			}
			try
			{
				LogMessageEventArgs ^args = gcnew  LogMessageEventArgs(gcnew String(context), gcnew String(msg));
				mLogObj->LogWarning(nullptr, args);
			}
			catch (Exception^ e)
			{
				Console::WriteLine("Error: {0}\n", e);
			}
		}
		static void LogMessage(const char msg[], const char context[])
		{
			if(nullptr==mLogObj)
			{
				return;
			}
			try
			{
				LogMessageEventArgs ^args = gcnew  LogMessageEventArgs(gcnew String(context), gcnew String(msg));
				mLogObj->LogMessage((System::Object ^)nullptr, args);
			}
			catch (Exception^ e)
			{
				Console::WriteLine("Error: {0}\n", e);
			}
		}
		static void LogDebug(const char msg[], const char context[])
		{
			if(nullptr==mLogObj)
			{
				return;
			}
			try
			{
				LogMessageEventArgs ^args = gcnew  LogMessageEventArgs(gcnew String(context), gcnew String(msg));
				mLogObj->LogDebug(nullptr, args);
			}
			catch (Exception^ e)
			{
				Console::WriteLine("Error: {0}\n", e);
			}
		}

	private:
		static UniversalLogFile ^mLogObj = nullptr;
	};

}


#endif //_RSULOG_H_