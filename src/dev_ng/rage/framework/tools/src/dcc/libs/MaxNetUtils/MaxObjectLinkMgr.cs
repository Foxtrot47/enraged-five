﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.ObjectLinks;
using RSG.Pipeline.Core;
using RSG.Pipeline.Content;
using RSG.Base.Configuration;
using RSG.Base.Logging.Universal;

namespace RSG.MaxUtils
{
    /// <summary>
    /// Max Wrapper for maxscript access
    /// </summary>
    public class MaxObjectLinkMgr : ObjectLinkMgr
    {
        public MaxObjectLinkMgr(ContentTreeHelper ctHelper, IBranch branch, IUniversalLog log)
            : base(ctHelper, branch, log)
        {

        }

        /// <summary>
        ///  high level resolve wrapper by guid string
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="name"></param>
        /// <param name="contentNode"></param>
        /// <returns></returns>
        public IObjectLinkNode ResolveOrCreate(string guid, string name, IContentNode contentNode)
        {
            return ResolveOrCreate(new Guid(guid), name, contentNode);
        }

        /// <summary>
        /// Link two objects and write or update two link files
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="sourceContentNode"></param>
        /// <param name="sourceGuid"></param>
        /// <param name="sourceName"></param>
        /// <param name="targetContentNode"></param>
        /// <param name="targetGuid"></param>
        /// <param name="targetName"></param>
        /// <returns></returns>
        public LinkStatusFlags Link(int channel,
            IContentNode sourceContentNode, string sourceGuid, string sourceName,
            IContentNode targetContentNode, string targetGuid, string targetName)
        {
           return Link((LinkChannel)channel,
            sourceContentNode, new Guid(sourceGuid), sourceName,
            targetContentNode, new Guid(targetGuid), targetName);
        }

        /// <summary>
        /// high level unlink by guid string
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="sourceContentNode"></param>
        /// <param name="sourceGuid"></param>
        /// <param name="targetGuid"></param>
        /// <returns></returns>
        public LinkStatusFlags Unlink(int channel, string sourceGuidStr, string targetGuidStr="")
        {
            // Creating two-way linking

            Guid targetGuid = Guid.Empty;
            if (targetGuidStr != String.Empty)
                targetGuid = new Guid(targetGuidStr);

            if (Unlink((LinkChannel)channel, new Guid(sourceGuidStr), targetGuid))
                return LinkStatusFlags.LinkStatusResolved;

            return LinkStatusFlags.LinkStatusResolved;
        }

    }
}
