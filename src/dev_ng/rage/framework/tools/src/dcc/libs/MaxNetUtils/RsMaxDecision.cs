﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace RSG.MaxUtils
{
    [Serializable()]
    public class RsMaxDecision : ISerializable
    {
        public object[] optionVals;
        public object[] optionLabels;
        public object val;
        public string comment;

        public RsMaxDecision()
        {
            optionVals = new object[0];
            optionLabels = new object[0];
            val = new object();
            comment = "";
        }

        //Deserialization constructor.
        public RsMaxDecision(SerializationInfo info, StreamingContext ctxt)
        {
            //Get the values from info and assign them to the appropriate properties
            optionVals = (object[])info.GetValue("optionVals", typeof(object[]));
            optionLabels = (object[])info.GetValue("optionLabels", typeof(object[]));
            val = (object)info.GetValue("value", typeof(object));
            comment = (String)info.GetValue("comment", typeof(string));
        }

        //Serialization function.
        public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            //You can use any custom name for your name-value pair. But make sure you
            // read the values with the same name. For ex:- If you write EmpId as "EmployeeId"
            // then you should read the same with "EmployeeId"
            info.AddValue("optionVals", optionVals);
            info.AddValue("optionLabels", optionLabels);
            info.AddValue("value", val);
            info.AddValue("comment", comment);
        }
    }
}