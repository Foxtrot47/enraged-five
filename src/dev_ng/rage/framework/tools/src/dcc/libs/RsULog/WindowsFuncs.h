#ifndef _WINDOWS_FUNCS_
#define _WINDOWS_FUNCS_

typedef int BOOL;
#include "tchar.h"

BOOL UserName(char *, int size);
BOOL SysTime(char *, int size);
int TicksSinceEpoch(char * ret, int size);
BOOL MachineName(char *ret, int size);

#endif //_WINDOWS_FUNCS_