﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace RSG.MaxUtils
{
    [Serializable()]
    public class MaxDictionary : Dictionary<dynamic, dynamic>, ISerializable
    {
        public void put(dynamic key, dynamic value)
        {
            this[key] = value;
        }
        public dynamic get(dynamic key)
        {
            return this[key];
        }

        public event AddEventHandler AddEvent;

        public void Add(dynamic pKey, dynamic pValue)
        {
            if (AddEvent != null)
                AddEvent(new AddEventArgs(pKey, pValue));
            base.Add((object)pKey, (object)pValue);
        }

        #region constructors
        public MaxDictionary() : base() { }
        public MaxDictionary(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {
        }
        #endregion

        #region Events
        public delegate void AddEventHandler(AddEventArgs pAddEventArgs);

        public class AddEventArgs : EventArgs
        {
            private dynamic _key;
            private dynamic _value;

            public AddEventArgs(dynamic key, dynamic value)
            {
                _value = value;
                _key = key;
            }

            public dynamic Key
            {
                get
                {
                    return _key;
                }
            }

            public dynamic Value
            {
                get
                {
                    return _value;
                }
            }
        }
        #endregion
    }
}