﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using RSG.Base.Math;

namespace RSG.MaxUtils
{
    public class BoundsVector
    {
        [XmlAttribute]
        public float X;

        [XmlAttribute]
        public float Y;

        public BoundsVector() { }
    }

    public class VegetationColor
    {
        [XmlAttribute]
        public int Red;

        [XmlAttribute]
        public int Green;

        [XmlAttribute]
        public int Blue;

        public VegetationColor() { }
        public VegetationColor(int r, int g, int b)
        {
            Red = r;
            Green = g;
            Blue = b;
        }
    }

    public class TextureBounds
    {
        [XmlAttribute]
        public float TextureSize;  //This is the extents of a single texture, such as 1024 by 1024.

        [XmlAttribute]
        public float PixelResolution;

        public BoundsVector MinBounds;  //The bounds in world-coordinates for the texture.
        public BoundsVector MaxBounds;

        [XmlIgnore]
        public int MaxRowIndex;

        [XmlIgnore]
        public int MaxColIndex;

        public TextureBounds()
        {
            MinBounds = new BoundsVector();
            MaxBounds = new BoundsVector();
            TextureSize = 1024.0f;
            PixelResolution = 1.0f;
            MaxRowIndex = -1;
            MaxColIndex = -1;
        }
    }

    public class VegetationDefinition
    {
        [XmlAttribute]
        public string Name;

        [XmlAttribute]
        public string Category;

        [XmlIgnore]
        public int CategoryIndex;

        [XmlAttribute]
        public float Scale;

        [XmlAttribute]
        public float Density;

        public VegetationColor Color;  //The pixel associated with this definition.
        public VegetationDefinition()
        {
            Name = null;
            Category = null;
            Color = null;
            Scale = 1.0f;
            Density = 1.0f;
        }
    }

    /// <summary>
    /// A simple container object that will cache a particular entry in an accessible format for MaxScript.
    /// </summary>
    public class VegetationInstance
    {
        public VegetationDefinition Definition;
        public float Scale;
        public float Density;

        public VegetationInstance(VegetationDefinition definition, float scale, float density)
        {
            Definition = definition;
            Scale = scale;
            Density = density;
        }
    }

    public class VegetationTexture
    {
        public int Row;   //The index of the texture in the grid composing of the world texture.
        public int Col;
        public Bitmap Texture;      //The loaded texture section.

        [XmlIgnore]
        public Vector2f SectionStart;

        public VegetationTexture(int row, int col, Bitmap texture, TextureBounds bounds)
        {
            Row = row;
            Col = col;
            Texture = texture;

            SectionStart = new Vector2f();
            SectionStart.X = bounds.MinBounds.X + (bounds.TextureSize * col);
            SectionStart.Y = bounds.MinBounds.Y + (bounds.TextureSize * row);
        }
    }

    public class VegetationCategory
    {
        public VegetationCategory()
        {
            Name = null;
            DropType = null;
            TexturePath = null;
        }

        [XmlAttribute]
        public string Name;

        [XmlAttribute]
        public string DropType;

        [XmlAttribute]
        public string TexturePath;
    }

    public class RegisteredPixels : Dictionary<int, List<int>>
    {
        public int X;
        public int Y;

        public RegisteredPixels(int x, int y) { X = x; Y = y; }
    }

    public class VegetationData
    {
        [XmlAttribute("Version")]
        public int m_Version;
        private int m_CurrentVersion = 1;

        [XmlElement("TextureFilePrefix")]
        public string TextureFilePrefix;

        [XmlElement("SecondLayerDirectory")]
        public string SecondLayerDirectory;

        [XmlElement("TextureExtension")]
        public string TextureExtension;

        [XmlElement("RandomizeScaleAndDensity")]
        public bool RandomizeScaleAndDensity;  //Used only for debugging.

        [XmlElement("MaxAngle")]
        public float MaxAngle;

        private float MaxPositionOffset;

        public TextureBounds Bounds;

        [XmlArray("Categories")]
        [XmlArrayItem("Category")]
        public List<VegetationCategory> Categories;

        [XmlArray("Definitions")]
        [XmlArrayItem("Definition")]
        public List<VegetationDefinition> Definitions;

        [XmlIgnore]
        public VegetationInstance LastInstance;

        private int SelectedCategoryIndex;

        private List<VegetationTexture> FirstLayer;
        private List<VegetationTexture> SecondLayer;
        private List<RegisteredPixels> RegisteredLists;
        private string m_LastError;
        

        public VegetationData() 
        {
            Bounds = new TextureBounds();
            Definitions = new List<VegetationDefinition>();
            Categories = new List<VegetationCategory>();
            RegisteredLists = new List<RegisteredPixels>();
            MaxAngle = -1.0f;
            m_LastError = null;
            LastInstance = null;
            RandomizeScaleAndDensity = true;
        }

        public int GetVersion() { return m_Version; }
        public int GetCurrentVersion() { return m_CurrentVersion; }

        public string GetLastError() { return m_LastError; }

        /// <summary>
        /// Sets the selected category.
        /// </summary>
        /// <param name="categoryIndex"></param>
        public void SetSelectedCategoryIndex(int categoryIndex)
        {
            if (categoryIndex < 0 || categoryIndex >= Categories.Count)
            {
                //Invalid index specified.
                return;
            }

            SelectedCategoryIndex = categoryIndex;
        }

        /// <summary>
        /// Returns the list of categories in an array.
        /// </summary>
        /// <returns></returns>
        public string[] GetCategories()  
        {
            List<string> categories = new List<string>();
            foreach (VegetationCategory category in Categories)
            {
                categories.Add(category.Name);
            }

            return categories.ToArray();
        }

        /// <summary>
        /// Returns whether this category contains procedural objects or references.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public string GetDropType(int index)
        {
            return Categories[index].DropType;
        }

        /// <summary>
        /// Returns an angle in degrees to be used to check whether the given face is valid for dropping.
        /// </summary>
        /// <returns></returns>
        public float GetMaxAngle() { return MaxAngle; }

        /// <summary>
        /// Deserializes the Vegetation metadata file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static VegetationData Load(string filename)
        {
            StreamReader reader = null;
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(VegetationData));
                reader = new StreamReader(filename);
                VegetationData data = (VegetationData) serializer.Deserialize(reader);
                reader.Close();

                if (String.IsNullOrWhiteSpace(data.TextureFilePrefix) == true)
                {
                    Console.WriteLine("Unable to find texture file prefix!");
                    return null;
                }

                if (String.IsNullOrWhiteSpace(data.TextureExtension) == true)
                {
                    Console.WriteLine("Unable to find texture extension node!");
                    return null;
                }

                if (data.Bounds.TextureSize <= 0) 
                {
                    Console.WriteLine("Invalid bounds texture size.  Must be greater than zero!");
                    return null;
                }

                data.InitializeData();
           
                return data;
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                reader.Close();
                return null;
            }
        }

        /// <summary>
        /// Initializes the internal VegetationData structure after the XML serialization process.
        /// </summary>
        private void InitializeData()
        {
            TextureFilePrefix = Environment.ExpandEnvironmentVariables(TextureFilePrefix);

            float xLength = Bounds.MaxBounds.X - Bounds.MinBounds.X;
            float yLength = Bounds.MaxBounds.Y - Bounds.MinBounds.Y;

            int maxXIndex = (int)(xLength / Bounds.TextureSize);
            int maxYIndex = (int)(yLength / Bounds.TextureSize);
            Bounds.MaxRowIndex = maxXIndex;
            Bounds.MaxColIndex = maxYIndex;

            MaxPositionOffset = Bounds.PixelResolution / 2.0f;

            SelectedCategoryIndex = 0;

            foreach (VegetationCategory category in Categories)
            {
                category.TexturePath = Environment.ExpandEnvironmentVariables(category.TexturePath);
            }

            foreach (VegetationDefinition definition in Definitions)
            {
                bool found = false;
                for(int categoryIndex = 0; categoryIndex < Categories.Count; ++categoryIndex) 
                {
                    string category = Categories[categoryIndex].Name;
                    if (String.Compare(definition.Category, category, true) == 0)
                    {
                        definition.CategoryIndex = categoryIndex;
                        found = true;
                        break;
                    }
                }

                if (found == false)
                {
                    definition.CategoryIndex = Categories.Count;
                }
            }
        }

        /// <summary>
        /// Loads the current vegetation tetxure with the given row/column index.
        /// </summary>
        /// <param name="textureRowIndex"></param>
        /// <param name="textureColIndex"></param>
        /// <returns></returns>
        public VegetationTexture LoadTexture(int textureRowIndex, int textureColIndex)
        {
            Console.WriteLine("Loading texture (" + textureRowIndex + ", " + textureColIndex + ")");

            VegetationTexture firstLayerTexture = null;
            string filename = Path.Combine(Categories[SelectedCategoryIndex].TexturePath, TextureFilePrefix) + String.Format("{0:00}", textureRowIndex) + "_" + String.Format("{0:00}", textureColIndex) + "." + TextureExtension;
            if (File.Exists(filename) == true)
            {
                FileStream fileStream = new FileStream(filename, FileMode.Open, FileAccess.Read);
                Bitmap texture = new Bitmap(fileStream);

                VegetationTexture vegetationTexture = new VegetationTexture(textureRowIndex, textureColIndex, texture, Bounds);
                FirstLayer.Add(vegetationTexture);
                
                firstLayerTexture = vegetationTexture;
                fileStream.Close();
            }
            else
            {
                Console.WriteLine("First layer vegetation texture " + filename + " could not be found!");
            }

            //Second Layer.
            string secondLayerFilename = Path.Combine(Categories[SelectedCategoryIndex].TexturePath, SecondLayerDirectory, TextureFilePrefix) + String.Format("{0:00}", textureRowIndex) + "_" + String.Format("{0:00}", textureColIndex) + "." + TextureExtension;
            if (File.Exists(secondLayerFilename) == true)
            {
                FileStream fileStream = new FileStream(secondLayerFilename, FileMode.Open, FileAccess.Read);
                Bitmap texture = new Bitmap(fileStream);

                VegetationTexture vegetationTexture = new VegetationTexture(textureRowIndex, textureColIndex, texture, Bounds);
                SecondLayer.Add(vegetationTexture);

                fileStream.Close();
            }
            else
            {
                Console.WriteLine("Second layer texture " + secondLayerFilename + " could not be found!");
            }

            RegisteredPixels registedPixelsList = new RegisteredPixels(textureRowIndex, textureColIndex);
            RegisteredLists.Add(registedPixelsList);
            return firstLayerTexture;
        }

        /// <summary>
        /// Specifies an extent in world-coordinates, creating a bound.  From this bound, 
        /// determine what texture sections to load in the world grid of vegetation textures.
        /// </summary>
        /// <param name="minX"></param>
        /// <param name="minY"></param>
        /// <param name="maxX"></param>
        /// <param name="maxY"></param>
        /// <returns></returns>
        public bool LoadTexturesFromBound(float minX, float minY, float maxX, float maxY)
        {
            //Clear all previously loaded textures.
            FirstLayer = new List<VegetationTexture>();
            SecondLayer = new List<VegetationTexture>();

            RegisteredLists.Clear();

            int textureMinRowIndex;
            int textureMinColIndex;
            GetIndexFromPoint(minX, minY, out textureMinRowIndex, out textureMinColIndex);
            if (textureMinRowIndex < 0 || textureMinColIndex < 0)
            {
                m_LastError = "Minimum bounds specified is outside of the vegetation boundaries.";
                return false;
            }

            int textureMaxRowIndex;
            int textureMaxColIndex;
            GetIndexFromPoint(maxX, maxY, out textureMaxRowIndex, out textureMaxColIndex);

            if (textureMaxRowIndex < 0 || textureMaxColIndex < 0)
            {
                m_LastError = "Maximum bounds specified is outside of the vegetation boundaries.";
                return false;
            }

            //Now that we have a range of textures to load, load them.
            //
            for (int textureRowIndex = textureMinRowIndex; textureRowIndex <= textureMaxRowIndex; ++textureRowIndex)
            {
                for (int textureColIndex = textureMinColIndex; textureColIndex <= textureMaxColIndex; ++textureColIndex)
                {
                    LoadTexture(textureRowIndex, textureColIndex);
                }
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="definition"></param>
        /// <param name="pixelColor"></param>
        public VegetationInstance CreateInstance(VegetationDefinition definition, Color pixel, Color secondPixel)
        {
            float scaleValue = pixel.A;
            float densityValue = secondPixel.R;

            if (RandomizeScaleAndDensity == true)
            {
                Random random = new Random(); 
                scaleValue = random.Next() % 255;
                densityValue = random.Next() % 255;
            }

            float scale = (scaleValue / 255.0f);
            float density = (densityValue / 255.0f);
            return new VegetationInstance(definition, scale, density);
        }

        /// <summary>
        /// Casts the 2D grid on the 3D plane, determining if there are any points plotted on this face to drop on.
        /// </summary>
        /// <param name="x1"></param>
        /// <param name="y1"></param>
        /// <param name="z1"></param>
        /// <param name="x2"></param>
        /// <param name="y2"></param>
        /// <param name="z2"></param>
        /// <param name="x3"></param>
        /// <param name="y3"></param>
        /// <param name="z3"></param>
        /// <returns></returns>
        public void DropProceduralsOnFace(float x1, float y1, float z1, float x2, float y2, float z2, float x3, float y3, float z3)
        {
            x1 = RoundNearest(x1);
            y1 = RoundNearest(y1);
            z1 = RoundNearest(z1);
            x2 = RoundNearest(x2);
            y2 = RoundNearest(y2);
            z2 = RoundNearest(z2);
            x3 = RoundNearest(x3);
            y3 = RoundNearest(y3);
            z3 = RoundNearest(z3);
            
            //Find the nearest point on the megatexture grid from the first vertex.

            Vector3f vertex1 = new Vector3f(x1, y1, z1);
            Vector3f vertex2 = new Vector3f(x2, y2, z2);
            Vector3f vertex3 = new Vector3f(x3, y3, z3);

            Vector2f[] face = new Vector2f[3];
            face[0] = new Vector2f(vertex1.X, vertex1.Y);
            face[1] = new Vector2f(vertex2.X, vertex2.Y);
            face[2] = new Vector2f(vertex3.X, vertex3.Y);

            float minX = x1;
            if ( x2 < minX )
                minX = x2;
            if ( x3 < minX )
                minX = x3;

            float minY = y1;
            if ( y2 < minY )
                minY = y2;
            if ( y3 < minY )
                minY = y3;

            float maxX = x1;
            if ( x2 > maxX )
                maxX = x2;
            if ( x3 > maxX )
                maxX = x3;

            float maxY = y1;
            if ( y2 > maxY )
                maxY = y2;
            if ( y3 > maxY )
                maxY = y3;

            float minZ = z1;
            if (z2 < minZ)
                minZ = z2;
            if (z3 < minZ)
                minZ = z3;

            float maxZ = z1;
            if (z2 > maxZ)
                maxZ = z2;
            if (z3 > maxZ)
                maxZ = z3;

            float gridStartX = (float) Math.Ceiling( (double) minX);
            float gridStartY = (float) Math.Ceiling( (double) minY);

            Dictionary<VegetationDefinition, int> m_ObjectCounts = new Dictionary<VegetationDefinition, int>();
            Dictionary<VegetationDefinition, VegetationInstance> m_Instances = new Dictionary<VegetationDefinition, VegetationInstance>();
            for (float xCoordinate = gridStartX; xCoordinate <= maxX; xCoordinate = xCoordinate + Bounds.PixelResolution)
            {
                for (float yCoordinate = gridStartY; yCoordinate <= maxY; yCoordinate = yCoordinate + Bounds.PixelResolution)
                {
                    //Is the coordinate within the face?
                    Vector2f point = new Vector2f(xCoordinate, yCoordinate);
                    if (IsPointInPolygon(point, face) == true)
                    {
                        //Drop this object then!
                        Color pixelColor = GetPixelFromVector(xCoordinate, yCoordinate, FirstLayer);
                        Color secondColor = GetPixelFromVector(xCoordinate, yCoordinate, SecondLayer);
                        VegetationDefinition definition = GetObjectFromPixel(pixelColor);

                        if (definition != null)
                        {
                            VegetationInstance instance = CreateInstance(definition, pixelColor, secondColor);
                            Plane plane = new Plane(vertex1, vertex2, vertex3);

                            //Find a coordinate with a random offset that 
                            //1) Exists on this plane
                            //2) Within the boundaries of the offset.
                            //3) Consistently generated from the same seed.
                            if (m_ObjectCounts.ContainsKey(definition) == false)
                                m_ObjectCounts.Add(definition, 1);
                            else
                                m_ObjectCounts[definition]++;

                            if ( m_Instances.ContainsKey(definition) == false )
                            {
                                m_Instances.Add(definition, instance);
                            }
                        }

                        //This entry may have no definition associated with it; register it since we've done the check already.
                        //Note that this is the original x and y coordinate since we generated a random offset from this particular point.
                        RegisterPixel(xCoordinate, yCoordinate);
                    }
                }
            }

            //Determine the most influential definition on this face.  Return that string.
            VegetationDefinition selectedDefinition = null;
            int maxCount = 0;
            foreach (KeyValuePair<VegetationDefinition, int> keyValuePair in m_ObjectCounts)
            {
                if (keyValuePair.Value > maxCount)
                {
                    selectedDefinition = keyValuePair.Key;
                    maxCount = keyValuePair.Value;
                }
            }

            if (selectedDefinition != null)
            {
                LastInstance = m_Instances[selectedDefinition];
            }
            else
            {
                LastInstance = null;
            }
        }


        public string DropReferencesOnFace(float x1, float y1, float z1, float x2, float y2, float z2, float x3, float y3, float z3)
        {
            x1 = RoundNearest(x1);
            y1 = RoundNearest(y1);
            z1 = RoundNearest(z1);
            x2 = RoundNearest(x2);
            y2 = RoundNearest(y2);
            z2 = RoundNearest(z2);
            x3 = RoundNearest(x3);
            y3 = RoundNearest(y3);
            z3 = RoundNearest(z3);

            //Find the nearest point on the megatexture grid from the first vertex.

            Vector3f vertex1 = new Vector3f(x1, y1, z1);
            Vector3f vertex2 = new Vector3f(x2, y2, z2);
            Vector3f vertex3 = new Vector3f(x3, y3, z3);

            Vector2f[] face = new Vector2f[3];
            face[0] = new Vector2f(vertex1.X, vertex1.Y);
            face[1] = new Vector2f(vertex2.X, vertex2.Y);
            face[2] = new Vector2f(vertex3.X, vertex3.Y);

            float minX = x1;
            if (x2 < minX)
                minX = x2;
            if (x3 < minX)
                minX = x3;

            float minY = y1;
            if (y2 < minY)
                minY = y2;
            if (y3 < minY)
                minY = y3;

            float maxX = x1;
            if (x2 > maxX)
                maxX = x2;
            if (x3 > maxX)
                maxX = x3;

            float maxY = y1;
            if (y2 > maxY)
                maxY = y2;
            if (y3 > maxY)
                maxY = y3;

            float minZ = z1;
            if (z2 < minZ)
                minZ = z2;
            if (z3 < minZ)
                minZ = z3;

            float maxZ = z1;
            if (z2 > maxZ)
                maxZ = z2;
            if (z3 > maxZ)
                maxZ = z3;

            float gridStartX = (float)Math.Ceiling((double)minX);
            float gridStartY = (float)Math.Ceiling((double)minY);

            string returnString = null;
            for (float xCoordinate = gridStartX; xCoordinate <= maxX; xCoordinate = xCoordinate + Bounds.PixelResolution)
            {
                for (float yCoordinate = gridStartY; yCoordinate <= maxY; yCoordinate = yCoordinate + Bounds.PixelResolution)
                {
                    //Is the coordinate within the face?
                    Vector2f point = new Vector2f(xCoordinate, yCoordinate);
                    if (IsPointInPolygon(point, face) == true)
                    {
                        if (IsPixelRegistered(xCoordinate, yCoordinate) == false)
                        {
                            //Drop this object then!
                            Color pixelColor = GetPixelFromVector(xCoordinate, yCoordinate, FirstLayer);
                            Color secondColor = GetPixelFromVector(xCoordinate, yCoordinate, SecondLayer);
                            VegetationDefinition definition = GetObjectFromPixel(pixelColor);

                            if (definition != null)
                            {
                                Plane plane = new Plane(vertex1, vertex2, vertex3);

                                //Find a coordinate with a random offset that 
                                //1) Exists on this plane
                                //2) Within the boundaries of the offset.
                                //3) Consistently generated from the same seed.
                                Random random = new Random((int)(xCoordinate + yCoordinate));
                                Vector2f randomPoint = new Vector2f();
                                do
                                {
                                    randomPoint.X = xCoordinate + ((float)random.NextDouble() * Bounds.PixelResolution) - MaxPositionOffset;
                                    randomPoint.Y = yCoordinate + ((float)random.NextDouble() * Bounds.PixelResolution) - MaxPositionOffset;
                                }
                                while (IsPointInPolygon(randomPoint, face) == false);

                                float zCoordinate = plane.GetZCoordinate(randomPoint.X, randomPoint.Y);

                                if (zCoordinate < minZ)
                                {
                                    zCoordinate = minZ;
                                }
                                else if (zCoordinate > maxZ)
                                {
                                    zCoordinate = maxZ;
                                }

                                if (returnString != null)
                                    returnString += ",";

                                returnString += definition + "," + randomPoint.X + "," + randomPoint.Y + "," + zCoordinate;
                            }

                            //This entry may have no definition associated with it; register it since we've done the check already.
                            //Note that this is the original x and y coordinate since we generated a random offset from this particular point.
                            RegisterPixel(xCoordinate, yCoordinate);
                        }
                    }
                }
            }

            return returnString;
        }

        /// <summary>
        /// Specifies a world coordinate 2D point and get back the section index of the world grid.
        /// These indices relate directly to which texture contains this point's data.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="rowIndex"></param>
        /// <param name="colIndex"></param>
        private void GetIndexFromPoint(float x, float y, out int rowIndex, out int colIndex)
        {
            float adjustedMinX = x - Bounds.MinBounds.X;
            float adjustedMinY = y - Bounds.MinBounds.Y;

            rowIndex = (int)(adjustedMinY / (Bounds.TextureSize * Bounds.PixelResolution));
            colIndex = (int)(adjustedMinX / (Bounds.TextureSize * Bounds.PixelResolution));

            if (rowIndex < 0 || rowIndex > Bounds.MaxRowIndex || colIndex < 0 || colIndex > Bounds.MaxColIndex)
            {
                //Coordinate is out of bounds of the world.
                rowIndex = -1;
                colIndex = -1;
            }
        }

        /// <summary>
        /// Specifies a world-coordinate 2D point and determines the pixel in the world this point best
        /// maps to.  This algorithm rounds to the nearest integer to find the texture pixel.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public Color GetPixelFromVector(float x, float y, List<VegetationTexture> layer)
        {
            int rowIndex;
            int colIndex;
            GetIndexFromPoint(x, y, out rowIndex, out colIndex);

            foreach (VegetationTexture vegTexture in layer)
            {
                if (vegTexture.Row == rowIndex && vegTexture.Col == colIndex)
                {
                    float sectionXPixel = (x - vegTexture.SectionStart.X);
                    float sectionYPixel = (y - vegTexture.SectionStart.Y);

                    int roundedXPixel = (int) Math.Round(sectionXPixel);
                    int roundedYPixel = (int) Math.Round(sectionYPixel);

                    //Certain cases may result in rounding up to 1024 (e.g. roundedXPixel == 1023.98)
                    if (roundedXPixel < 0 || roundedXPixel >= vegTexture.Texture.Width || roundedYPixel < 0 || roundedYPixel >= vegTexture.Texture.Height)
                        break;

                    //NOTE:  Texture coordinates consider (0,0) as the upper-left corner of the texture.
                    //In our coordinate system, we assume that (0,0) is the bottom-left corner.
                    //Flip the y-coordinate then.
                    Color pixel = vegTexture.Texture.GetPixel(roundedXPixel, (vegTexture.Texture.Height-roundedYPixel-1));
                    return pixel;
                }
            }

            Console.WriteLine("Vector (" + x + ", " + y + ") is outside of the current vegetation textures!");
            return new Color();
        }

        /// <summary>
        /// Mark this pixel as having been dropped.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void RegisterPixel(float x, float y)
        {
            int rowIndex;
            int colIndex;
            GetIndexFromPoint(x, y, out rowIndex, out colIndex);

            foreach (VegetationTexture vegTexture in FirstLayer)
            {
                if (vegTexture.Row == rowIndex && vegTexture.Col == colIndex)
                {
                    float sectionXPixel = (x - vegTexture.SectionStart.X);
                    float sectionYPixel = (y - vegTexture.SectionStart.Y);

                    int roundedXPixel = (int)Math.Round(sectionXPixel);
                    int roundedYPixel = (int)Math.Round(sectionYPixel);

                    RegisteredPixels pixelList = GetPixelList(vegTexture.Row, vegTexture.Col);
                    if (pixelList.ContainsKey(roundedXPixel) == false)
                    {
                        pixelList.Add(roundedXPixel, new List<int>());
                    }

                    pixelList[roundedXPixel].Add(roundedYPixel);
                }
            }
        }

        /// <summary>
        /// Ensure that an object hasn't been dropped already for this pixel.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bool IsPixelRegistered(float x, float y)
        {
            int rowIndex;
            int colIndex;
            GetIndexFromPoint(x, y, out rowIndex, out colIndex);

            if (rowIndex < 0 || colIndex < 0)
            {
                return true;
            }

            foreach (VegetationTexture vegTexture in FirstLayer)
            {
                if (vegTexture.Row == rowIndex && vegTexture.Col == colIndex)
                {
                    float sectionXPixel = (x - vegTexture.SectionStart.X);
                    float sectionYPixel = (y - vegTexture.SectionStart.Y);

                    int roundedXPixel = (int)Math.Round(sectionXPixel);
                    int roundedYPixel = (int)Math.Round(sectionYPixel);

                    RegisteredPixels pixelList = GetPixelList(vegTexture.Row, vegTexture.Col);
                    if (pixelList.ContainsKey(roundedXPixel) == true)
                    {
                        return pixelList[roundedXPixel].Contains(roundedYPixel);
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Acquires the list of registered pixels.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        private RegisteredPixels GetPixelList(int x, int y)
        {
            foreach (RegisteredPixels pixelList in RegisteredLists)
            {
                if (pixelList.X == x && pixelList.Y == y)
                {
                    return pixelList;
                }
            }

            return null;
        }

        /// <summary>
        /// Acquires a list of pixels that haven't had an associated object dropped on it.
        /// </summary>
        /// <param name="minX"></param>
        /// <param name="minY"></param>
        /// <param name="maxX"></param>
        /// <param name="maxY"></param>
        /// <returns></returns>
        public Vector2f[] GetUnregisteredPixels(float minX, float minY, float maxX, float maxY)
        {
            List<Vector2f> unregisteredPixels = new List<Vector2f>();
            foreach (VegetationTexture vegTexture in FirstLayer)
            {
                RegisteredPixels pixelList = GetPixelList(vegTexture.Row, vegTexture.Col);

                int startX = 0;
                if (vegTexture.SectionStart.X < minX)
                {
                    startX = (int)(minX - vegTexture.SectionStart.X);
                }

                int startY = 0;
                if (vegTexture.SectionStart.Y < minY)
                {
                    startY = (int)(minY - vegTexture.SectionStart.Y);
                }

                for (int x = startX; x < Bounds.TextureSize; ++x)
                {
                    if (vegTexture.SectionStart.X + x > maxX)
                        break;

                    for (int y = startY; y < Bounds.TextureSize; ++y)
                    {
                        if (vegTexture.SectionStart.Y + y > maxY)
                            break;

                        Color pixel = GetPixelFromVector(x, y, FirstLayer);
                        VegetationDefinition definition = GetObjectFromPixel(pixel);

                        if (definition != null)
                        {
                            if (pixelList.ContainsKey(x) == false || pixelList[x].Contains(y) == false)
                            {
                                float sectionXWorld = x + vegTexture.SectionStart.X;
                                float sectionYWorld = y + vegTexture.SectionStart.Y;
                                unregisteredPixels.Add(new Vector2f(sectionXWorld, sectionYWorld));
                            }
                        }
                    }
                }
            }

            return unregisteredPixels.ToArray();
        }

        /// <summary>
        /// Specifies a pixel color and returns the corresponding procedural object.
        /// </summary>
        /// <param name="pixel"></param>
        /// <returns></returns>
        public VegetationDefinition GetObjectFromPixel(Color pixel)
        {
            foreach (VegetationDefinition definition in Definitions)
            {
                if (SelectedCategoryIndex == definition.CategoryIndex)
                {
                    if (definition.Color.Red == pixel.R && definition.Color.Green == pixel.G && definition.Color.Blue == pixel.B)
                    {
                        return definition;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Returns the color based on the definition name.
        /// </summary>
        /// <param name="definitionName"></param>
        /// <returns></returns>
        public Color GetColorFromDefinition(string definitionName)
        {
            foreach (VegetationDefinition definition in Definitions)
            {
                if (String.Compare(definition.Name, definitionName, true) == 0)
                {
                    return Color.FromArgb(definition.Color.Red, definition.Color.Green, definition.Color.Blue);
                }
            }

            return Color.White;
        }

        #region Debugging Functions

        /// <summary>
        /// Returns a list of information about the specific x-y pair in world coordinates.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public string GetPixelInfo(float x, float y)
        {
            int rowIndex;
            int colIndex;
            GetIndexFromPoint(x, y, out rowIndex, out colIndex);

            if (rowIndex < 0 || rowIndex > Bounds.MaxRowIndex || colIndex < 0 || colIndex > Bounds.MaxColIndex)
            {
                return "(" + x + ", " + y + ") is outside the vegetation bounds specified.";
            }

            VegetationTexture vegTexture = null;
            foreach (VegetationTexture currentTexture in FirstLayer)
            {
                if (currentTexture.Row == rowIndex && currentTexture.Col == colIndex)
                {
                    vegTexture = currentTexture; 
                    break;
                }
            }

            if (vegTexture == null)
            {
                vegTexture = LoadTexture(rowIndex, colIndex);
            }

            if (vegTexture == null)
            {
                return "(" + x + ", " + y + ") is outside the vegetation bounds.";
            }
            else
            {
                string output = "Texture Row/Col: (" + rowIndex + ", " + colIndex + ")\n";

                float sectionXPixel = (x - vegTexture.SectionStart.X);
                float sectionYPixel = (y - vegTexture.SectionStart.Y);

                int roundedXPixel = (int)Math.Round(sectionXPixel);
                int roundedYPixel = (int)Math.Round(sectionYPixel);

                //Certain cases may result in rounding up to 1024 (e.g. roundedXPixel == 1023.98)
                if (roundedXPixel < 0 || roundedXPixel >= vegTexture.Texture.Width || roundedYPixel < 0 || roundedYPixel >= vegTexture.Texture.Height)
                {
                    output += "Pixel Coordinate: Out of Range.\n";  //This really shouldn't be happening.
                    return output;
                }

                Color pixel = GetPixelFromVector(x, y, FirstLayer);
                Color secondPixel = GetPixelFromVector(x, y, SecondLayer);
                output += "Pixel Coordinate: (" + roundedXPixel + ", " + (vegTexture.Texture.Height - roundedYPixel - 1) + ")\n";
                output += "First Layer Color: (" + pixel.R + ", " + pixel.G + ", " + pixel.B + ")\n";
                output += "Second Layer Color: (" + secondPixel.R + ", " + secondPixel.G + ", " + secondPixel.B + ")\n";

                VegetationDefinition definition = GetObjectFromPixel(pixel);
                if (definition != null)
                {
                    VegetationInstance instance = CreateInstance(definition, pixel, secondPixel);
                    output += "Definition: " + definition.Name + "  Scale: " + instance.Scale + "\n";
                }
                else
                {
                    output += "Definition: null\n";
                }

                return output;
            }
        }

        /// <summary>
        /// Lists all textures to be loaded for the particular bound. 
        /// </summary>
        /// <param name="minX"></param>
        /// <param name="minY"></param>
        /// <param name="maxX"></param>
        /// <param name="maxY"></param>
        /// <returns></returns>
        public string GetLoadedTexturesFromBound(float minX, float minY, float maxX, float maxY)
        {
            int textureMinXIndex;
            int textureMinYIndex;
            GetIndexFromPoint(minX, minY, out textureMinXIndex, out textureMinYIndex);

            if (textureMinXIndex < 0 || textureMinYIndex < 0)
            {
                return "Minimum bounds specified is outside of the vegetation boundaries.";
            }

            int textureMaxXIndex;
            int textureMaxYIndex;
            GetIndexFromPoint(maxX, maxY, out textureMaxXIndex, out textureMaxYIndex);

            if (textureMaxXIndex < 0 || textureMaxYIndex < 0)
            {
                return "Maximum bounds specified is outside of the vegetation boundaries.";
            }

            string output = "";
            for (int xIndex = textureMinXIndex; xIndex <= textureMaxXIndex; ++xIndex)
            {
                for (int yIndex = textureMinYIndex; yIndex <= textureMaxYIndex; ++yIndex)
                {
                    output += TextureFilePrefix + String.Format("{0:00}", xIndex) + "_" + String.Format("{0:00}", yIndex) + "." + TextureExtension + "\n";
                }
            }

            return output;
        }

        #endregion

        #region Utility Functions

        /// <summary>
        /// A simple round function based on hardcoded thresholds.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private float RoundNearest(float input)
        {
            const float kMinThreshold = 0.001f;
            const float kMaxThreshold = 0.999f;

            float dec = Math.Abs(input - (int)input);
            if (dec < kMinThreshold || dec > kMaxThreshold)
            {
                return (float)Math.Round(input);
            }

            return input;
        }


        /// <summary>
        /// Determines if the given point is within the polygon using the even-odd algorithm.
        /// </summary>
        /// <param name="p"></param>
        /// <param name="poly"></param>
        /// <returns></returns>
        private bool IsPointInPolygon(Vector2f p, Vector2f[] poly)
        {
            bool inside = false;
            if (poly.Length < 3)
            {
                return inside;
            }

            Vector2f first = new Vector2f(poly[0].X, poly[0].Y);
            Vector2f node1 = new Vector2f();
            Vector2f node2 = new Vector2f();

            int j = poly.Length - 1;
            for (int i = 0; i < poly.Length; i++)
            {
                node1.X = poly[i].X;
                node1.Y = poly[i].Y;

                if (i == poly.Length - 1)
                {
                    node2 = first;
                }
                else
                {
                    node2 = poly[i + 1];
                }

                if ((node1.Y <= p.Y && node2.Y >= p.Y)
                    || (node2.Y <= p.Y && node1.Y >= p.Y))
                {
                    if (node1.Y == node2.Y)  //Straight horizontal line.  
                    {
                        //Test if the x-coordinate intersects the line.
                        if ( (node1.X <= p.X && node2.X >= p.X)
                            || (node2.X <= p.X && node2.X >= p.X) )
                        {
                            //This point is on this line.
                            return true;
                        }
                    }
                    else if (node1.X == node2.X && node1.X == p.X) //Straight vertical line.
                    {
                        inside = !inside;
                    }
                    if (node1.X + (p.Y - node1.Y) / (node2.Y - node1.Y) * (node2.X - node1.X) < p.X)
                    {
                        inside = !inside;
                    }
                }
            }

            return inside;
        }

        #endregion 

        #region Test Functions
        /// <summary>
        /// Creates a basic Vegetation XML file based on the XML serializer.
        /// Useful for figuring out the syntax of the Vegetation XML or to ensure
        /// the serializer is writing out data as expected.
        /// </summary>
        /// <param name="filename"></param>
        public void Save(string filename)
        {
            try
            {
                Bounds.MinBounds.X = 4000.0f;
                Bounds.MinBounds.Y = 4000.0f;
                Bounds.MaxBounds.X = 4000.0f;
                Bounds.MaxBounds.Y = 4000.0f;

                VegetationDefinition def = new VegetationDefinition();
                def.Name = "TestObject_01";
                def.Color = new VegetationColor(255, 128, 64);
                Definitions.Add(def);
                Definitions.Add(def);
                Definitions.Add(def);
                Definitions.Add(def);

                XmlSerializer serializer = new XmlSerializer(typeof(VegetationData));
                StreamWriter writer = new StreamWriter(filename);
                serializer.Serialize(writer, this);
                writer.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Creates a texture based on a specific distribution of how much of the texture
        /// will be a non-null pixel.  Assumes even distribution across all definitions otherwise.
        /// </summary>
        /// <param name="distribution"></param>
        public void GenerateTexture(float distribution, string output)
        {
            const int Size = 1024;
            int numObjects = (int) ((Size * Size) * distribution);
            int numEmpty = (int) ((Size * Size) * (1.0f - distribution));
            int numObjectsPerDefinition = numObjects / Definitions.Count;

            Bitmap newTexture = new Bitmap(Size, Size);

            List<int> objectCounts = new List<int>();
            objectCounts.Add(numEmpty);
            foreach(VegetationDefinition definition in Definitions)
            {
                objectCounts.Add(numObjectsPerDefinition);
            }

            Random random = new Random();

            for(int xIndex = 0; xIndex < Size; ++xIndex)
            {
                for(int yIndex = 0; yIndex < Size; ++yIndex)
                {
                    Color pixelColor = new Color();
                    int randNumber = random.Next(objectCounts.Count);
                    if ( randNumber == 0 )
                    {
                        //Empty slot.  Make it white.
                        pixelColor = Color.FromArgb(255, 255, 255);
                    }
                    else
                    {
                        pixelColor = Color.FromArgb(Definitions[randNumber - 1].Color.Red, Definitions[randNumber - 1].Color.Green, Definitions[randNumber - 1].Color.Blue);
                    }

                    if (objectCounts[randNumber] != 0)
                    {
                        newTexture.SetPixel(xIndex, yIndex, pixelColor);
                        objectCounts[randNumber]--;
                    }
                }
            }

            newTexture.Save(output);
        }
        #endregion
    }
}
