//**************************************************************************/
// Copyright (c) 1998-2007 Autodesk, Inc.
// All rights reserved.
// 
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Autodesk, Inc., and are
// protected by Federal copyright law. They may not be disclosed to third
// parties or copied or duplicated in any form, in whole or in part, without
// the prior written consent of Autodesk, Inc.
//**************************************************************************/
// DESCRIPTION: Appwizard generated plugin
// AUTHOR: 
//***************************************************************************/

#pragma unmanaged
#include "nativeInclude.h"
#include "IFPULog.h"
#include "RsULogWrapper.h"
#include "RsULog.h"
#include "ULogger.h"

RsULogWrapper *RsULogWrapper::sInstance = NULL;

ClassDesc2* GetRsULogWrapperDesc() { 
	return RsULogWrapper::GetInstance()->GetClassDesc();
}

RsULogWrapper::RsULogWrapper()
{
	static FPULog fpUlog(
		FP_ULOG_INTERFACE,
		_T("FPULog"),
		IDS_RSULOG_NAME,
		NULL,//RsULogWrapper::GetInstance()->GetClassDesc(),
		FP_CORE,

		IFPULog::emInitPath, _T("InitPath"), 0, TYPE_VOID, 0, 3,
		_T("filepath"), 0, TYPE_STRING,
		_T("context"), 0, TYPE_STRING,
		_T("append"), 0, TYPE_BOOL, f_keyArgDefault, TRUE,
		IFPULog::emLogError, _T("LogError"), 0, TYPE_VOID, 0, 3,
		_T("msg"), 0, TYPE_STRING,
		_T("context"), 0, TYPE_STRING,
		_T("filecontext"), 0, TYPE_STRING,
		IFPULog::emLogWarning, _T("LogWarning"), 0, TYPE_VOID, 0, 3,
		_T("msg"), 0, TYPE_STRING,
		_T("context"), 0, TYPE_STRING,
		_T("filecontext"), 0, TYPE_STRING,
		IFPULog::emLogMsg, _T("LogMessage"), 0, TYPE_VOID, 0, 3,
		_T("msg"), 0, TYPE_STRING,
		_T("context"), 0, TYPE_STRING,
		_T("filecontext"), 0, TYPE_STRING,
		IFPULog::emLogDebug, _T("LogDebug"), 0, TYPE_VOID, 0, 3,
		_T("msg"), 0, TYPE_STRING,
		_T("context"), 0, TYPE_STRING,
		_T("filecontext"), 0, TYPE_STRING,
		IFPULog::emLogRefresh, _T("Refresh"), 0, TYPE_VOID, 0, 0,
		IFPULog::emLogImFlush, _T("ImmediateFlush"), 0, TYPE_VOID, 0, 1,
		_T("doFlush"), 0, TYPE_BOOL,
		IFPULog::emLogFlush, _T("Flush"), 0, TYPE_VOID, 0, 0,
		IFPULog::emLogShowDialog, _T("ShowDialog"), 0, TYPE_VOID, 0, 0,
		IFPULog::emLogProfileStart, _T("ProfileStart"), 0, TYPE_VOID, 0, 1,
		_T("context"), 0, TYPE_STRING,
		IFPULog::emLogProfileEnd, _T("ProfileEnd"), 0, TYPE_VOID, 0, 1,
		_T("context"), 0, TYPE_STRING, f_keyArgDefault, "",
		end
		);
}
RsULogWrapper::~RsULogWrapper()
{
}

void FPULog::init()
{
	DebugPrint("unused init()");
}


RsULogWrapper* RsULogWrapper::GetInstance()
{
	if(NULL == sInstance)
	{
		sInstance = new RsULogWrapper();
	}

	return sInstance;
}

void RsULogWrapper::Destroy()
{
	delete sInstance;
	sInstance = NULL;
}

ClassDesc2* RsULogWrapper::GetClassDesc()
{
	return &mRsUlogWrapperDesc;
}

// Activate and Stay Resident
DWORD RsULogWrapper::Start( ) {
	
//	#pragma message(TODO("Do plugin initialization here"))
	
//	#pragma message(TODO("Return if you want remain loaded or not"))
	return GUPRESULT_NOKEEP;
}

void RsULogWrapper::Stop( ) {
//	#pragma message(TODO("Do plugin un-initialization here"))
}

DWORD_PTR RsULogWrapper::Control( DWORD parameter ) {
	return 0;
}

IOResult RsULogWrapper::Save(ISave *isave)
{
	return IO_OK;
}

IOResult RsULogWrapper::Load(ILoad *iload)
{
	return IO_OK;
}