﻿//#define USE_MAX_SDK

using System;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Data;
using RSG.Base.Collections;
using System.ComponentModel;
using System.Windows.Media.Imaging;

#if USE_MAX_SDK
using Autodesk.Max;
#endif

namespace RSG.MaxUtils
{
    /// <summary>
    /// Interaction logic for MaterialComparatorView.xaml
    /// </summary>
    /// 

    public class MaxObservableDictionary : ObservableDictionary<string, object>
    {
        public new object this[string key]
        {
            get
            {
                if(base.ContainsKey(key))
                    return base[key];
                return null;
            }
            set
            {
                base[key] = value;
            }
        }
    }

    public class Record
    {
        private readonly MaxObservableDictionary properties = new MaxObservableDictionary();
        public string Title
        {
            get;
            private set;
        }

        public Record(string title)
        {
            Title = title;
        }

        public void Set(string key, object prop)
        {
            if (!properties.ContainsKey(key))
                Properties.Add(key, prop);
            else
                properties[key] = prop;
        }

        public MaxObservableDictionary Properties
        {
            get { return properties; }
        }
    }

    public class MaterialComparatorModel
    {
//        public List<string> Columns;
        public ObservableCollection<Record> Records
        {
            get;
            set;
        }

        private DataGrid m_dg;

        public MaterialComparatorModel(ref DataGrid dg)
        {
            m_dg = dg;
            Records = new ObservableCollection<Record>();
            AddColumn("Analogies");
        }

        public void AddColumn(string name)
        {
            var binding = new Binding(string.Format("Properties[{0}]", name));
            binding.Mode = BindingMode.TwoWay;
            m_dg.Columns.Add(
                new DataGridTextColumn()//CustomBoundColumn() 
                {
                    Header = name,
                    Binding = binding
                    //                        TemplateName = "CustomTemplate" 
                });
        }

        public void AddRow(string name, MaxDictionary cols)
        {
            Record rec = new Record(name);
            foreach (KeyValuePair<dynamic, dynamic> colPair in cols)
            {
                rec.Set(colPair.Key, colPair.Value);

                List<DataGridColumn> colList = new List<DataGridColumn>(m_dg.Columns);
                if (colList.FindIndex(dgc => (dgc.Header as String) == colPair.Key) == -1)
                    AddColumn(colPair.Key);
            }
            Records.Add(rec);
        }


        public void Clear()
        {
            foreach (DataGridTextColumn dgc in m_dg.Columns)
                dgc.Binding = new Binding(null);
            m_dg.Columns.Clear();
            GC.Collect();
            Records.Clear();
            AddColumn("Analogies");
        }
    }

    public class CustomBoundColumn : DataGridBoundColumn
    {
        public string TemplateName { get; set; }

        protected override FrameworkElement GenerateElement(DataGridCell cell, object dataItem)
        {
            var binding = new Binding(((Binding)Binding).Path.Path);
            binding.Source = dataItem;

            var content = new ContentControl();
            content.ContentTemplate = (DataTemplate)cell.FindResource(TemplateName);
            content.SetBinding(ContentControl.ContentProperty, binding);
            return content;
        }

        protected override FrameworkElement GenerateEditingElement(DataGridCell cell, object dataItem)
        {
            return GenerateElement(cell, dataItem);
        }
    }

    public partial class MaterialComparatorView : UserControl
    {

        public MaterialComparatorModel Model
        {
            get { return m_model; }
        }
        private MaterialComparatorModel m_model;
        private Window m_window;
#if USE_MAX_SDK
       private IGlobal m_global;
#endif
        public MaterialComparatorView()
        {
            InitializeComponent();

            HorizontalAlignment = HorizontalAlignment.Stretch;
            VerticalAlignment = VerticalAlignment.Stretch;

            m_model = new MaterialComparatorModel(ref MtlComparatorGrid);
            MtlComparatorGrid.DataContext = m_model;
            MtlComparatorGrid.SelectionUnit = DataGridSelectionUnit.CellOrRowHeader;

            MtlComparatorGrid.MouseUp +=new MouseButtonEventHandler(MtlComparatorGrid_MouseDown);
//            MtlComparatorGrid.SelectionChanged += new SelectionChangedEventHandler(MtlComparatorGrid_SelectionChanged);

#if USE_MAX_SDK
            m_global = GlobalInterface.Instance;  //note that global will be an instance of an abstract class.
            var intfc = m_global.COREInterface13;
#endif
        }

        public Window CreateWindow()
        {
            m_window = new Window();
            m_window.Title = "Material Comparator";
            string iconResource = Environment.ExpandEnvironmentVariables("%RS_TOOLSROOT%/dcc/current/max2012/ui/usericons/WW_banner_rsLogo.png");
            if(System.IO.File.Exists(iconResource))
            {
                Uri iconUri = new Uri(Environment.ExpandEnvironmentVariables(iconResource), UriKind.Absolute);
                m_window.Icon = BitmapFrame.Create(iconUri);
            }

            if (null != Application.Current && m_window != Application.Current.MainWindow)
                m_window.Owner = Application.Current.MainWindow;
            m_window.Closed += new EventHandler(MyWindow_Closed);
            m_window.Content = this;
            return m_window;
        }

        public void ShowWindow()
        {
            if (null != m_window)
            {
                BringToFront();
                return;
            }
            CreateWindow().Show();
        }
        public void BringToFront()
        {
            m_window.Activate();
//            m_window.Topmost = true;
        }

        #region Event handlers
        void MyWindow_Closed(object sender, System.EventArgs e)
        {
            (sender as Window).Content = null;
            m_window = null;
        }

        private void CompareAllWithRow(Record selectedViewRow)
        {
            int highestAnalogy = 0;
            List<DataGridColumn> colList = new List<DataGridColumn>(MtlComparatorGrid.Columns);
            int analogiesColIndex = colList.FindIndex(col => (col.Header as String) == "Analogies");

            for (int k = 0; k < MtlComparatorGrid.Items.Count; k++)
            {
                int analogyCount = 0;

                object item = MtlComparatorGrid.Items[k];
                Record viewrow = item as Record;
                IEnumerator keyEnum = selectedViewRow.Properties.Keys.GetEnumerator();
                int colIndex = 1; // Starting behind Analogies
                //                    foreach (DataGridCellInfo selectedViewCell in dgv.SelectedCells)
                foreach (DataGridColumn col in MtlComparatorGrid.Columns)
//                while(keyEnum.MoveNext())
                {
                    string myKey = col.Header as string; //keyEnum.Current as string;
//                        DataRowView selectedViewRow = selectedViewCell.Item as DataRowView;
                    if (viewrow == selectedViewRow)
                    {
                        DataGridCellInfo cellInfo = new DataGridCellInfo(item, MtlComparatorGrid.Columns[colIndex]);
                        if (!MtlComparatorGrid.SelectedCells.Contains(cellInfo))
                            MtlComparatorGrid.SelectedCells.Add(cellInfo);
                        continue;
                    }

//                        int selColIndex = dgv.Columns.IndexOf(selectedViewCell.Column);
                    // Title used as Row header
                    if (myKey == "Title" || myKey == "Analogies")
                        continue;

                    object selectedRowItem = null;
                    object actualRowItem = null;
                    if (    selectedViewRow.Properties.TryGetValue(myKey, out selectedRowItem) &&
                            viewrow.Properties.TryGetValue(myKey, out actualRowItem) &&
                            selectedRowItem != null)
                    {
                        DataGridCellInfo cellInfo = new DataGridCellInfo(item, MtlComparatorGrid.Columns[colIndex]);
                        if (actualRowItem.Equals(selectedRowItem))
                        {
                            if (!MtlComparatorGrid.SelectedCells.Contains(cellInfo))
                                MtlComparatorGrid.SelectedCells.Add(cellInfo);
                            analogyCount++;
                        }
//                         else
//                         {
//                             if (MtlComparatorGrid.SelectedCells.Contains(cellInfo))
//                                 MtlComparatorGrid.SelectedCells.Remove(cellInfo);
//                        }
                    }
                    colIndex ++;
                }

                // counting differences
                viewrow.Properties["Analogies"] = analogyCount;
                if (analogyCount > highestAnalogy)
                    highestAnalogy = analogyCount;

            }
            selectedViewRow.Set("Analogies", highestAnalogy + 1);
        }

        private void SearchVisualTree(DependencyObject dep)
        {
            // iteratively traverse the visual tree
            while ((dep != null) &&
//                    !(dep is DataGridCell) &&
                    !(dep is DataGridRow))
            {
                dep = VisualTreeHelper.GetParent(dep);
            }
 
            if (dep == null)
                return;

            if (dep is DataGridRow)
            {
                DataGridRow row = dep as DataGridRow;
                row.IsSelected = true;
                CompareAllWithRow(row.Item as Record);
            }

            if (dep is DataGridCell)
            {
                DataGridCell cell = dep as DataGridCell;
 
                // navigate further up the tree
                while ((dep != null) && !(dep is DataGridRow))
                {
                    dep = VisualTreeHelper.GetParent(dep);
                }
 
                DataGridRow row = dep as DataGridRow;

                if (null != row)
                {
                    row.IsSelected = true;
                    CompareAllWithRow(row.Item as Record);
                }
                
            }
        }

        private void DatGridRowHeader_Click(object sender, RoutedEventArgs e)
        {
            DependencyObject dep = (DependencyObject)e.OriginalSource;
            SearchVisualTree(dep);
        }

        private void MtlComparatorGrid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DependencyObject dep = (DependencyObject)e.OriginalSource;
            SearchVisualTree(dep);
        }

        private void MtlComparatorGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid dgv = (DataGrid)sender;
            //User selected a cell (show the first cell in the row)
            if (dgv.SelectedCells.Count > 0)
            {
                Record selectedViewRow = dgv.SelectedCells[0].Item as Record;
                CompareAllWithRow(selectedViewRow);

            }
            object newSel = dgv.Items[0];
            dgv.SelectedItem = newSel;
        }
        #endregion
    }

    class Program
    {
        [STAThread]
        public static void Main()
        {
            var app = new Application();
            var comparer = new MaterialComparatorView();
            MaterialComparatorModel m = comparer.Model;
            MaxDictionary dict1 = new MaxDictionary();
            dict1["diffuse"] = "a texture";
            dict1["fresnel"] = 5;
            MaxDictionary dict2 = new MaxDictionary();
            dict2["fresnel"] = 5;
            dict2["spec map"] = "another texture";
            MaxDictionary dict3 = new MaxDictionary();
            dict3["diffuse"] = "a texture";
            dict3["normal map"] = "a third texture";

            m.AddRow("one", dict1);
            m.AddRow("two", dict2);
            m.AddRow("three", dict3);

            app.Run(comparer.CreateWindow());
        }
    }
}
