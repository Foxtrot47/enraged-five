﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using ragClient;
using ragCore;
using ragWidgets;

namespace RSG.RAG.RemoteConnection
{
    /// <summary>
    /// Remote connection to RAG.
    /// </summary>
    public class RemoteConnection : IDisposable
    {
        #region Static Member Data
        /// <summary>
        /// Number of times connect has been called.
        /// </summary>
        private static int s_connectionCount;
        #endregion // Static Member Data

        #region Constructor(s)
        /// <summary>
        /// Creates a remote connection object for communicating directly with the RAG proxy.
        /// </summary>
        public RemoteConnection()
        {
            Connect();
        }

        /// <summary>
        /// Static constructor.
        /// </summary>
        static RemoteConnection()
        {
            s_connectionCount = 0;
        }
        #endregion // Constructor(s)

        #region Connection Methods
        /// <summary>
        /// Attempts to connect to the RAG proxy.
        /// </summary>
	    private bool Connect()
        {
            if (s_connectionCount == 0)
            {
                s_connectionCount++;
                Client.ConsoleOnly = false;
                Client.SetupConnection();
                return true;
            }
            else
            {
                s_connectionCount++;

                if (Client.Context.Console.IsConnected() == false)
                {
                    Client.CloseConnections();
                    Client.SetupConnection();
                }

                return true;
            }
        }

        /// <summary>
        /// Disconnects the proxy.
        /// </summary>
        private void Disconnect()
        {
            if (s_connectionCount > 0)
                s_connectionCount--;

            if (s_connectionCount == 0)
            {
                Client.CloseConnections();
            }
        }

        /// <summary>
        /// Returns whether the proxy is currently connected.
        /// </summary>
        /// <returns></returns>
        public bool IsConnected()
        {
            return Client.Context.Console.IsConnected();
        }
        #endregion // Connection Methods

        #region Misc Methods
        /// <summary>
        /// Checks to see whether a particular widget exists.
        /// </summary>
        /// <param name="widgetName">Name of the widget to check</param>
        /// <returns></returns>
        public bool WidgetExists(String widgetName)
        {
            Widget rawwidget = Client.BM.FindFirstWidgetFromPath(widgetName);
            return (rawwidget != null);
        }

        /// <summary>
        /// Simulates a button press for a particular widget.
        /// </summary>
        /// <param name="widgetName"></param>
        /// <returns></returns>
        public bool PressButton(String widgetName)
        {
            Widget rawWidget = Client.BM.FindFirstWidgetFromPath(widgetName);
            if (rawWidget != null && rawWidget is WidgetButton)
            {
                WidgetButton buttonWidget = (WidgetButton)rawWidget;
                buttonWidget.Activate();
                return true;
            }
            return false;
        }
        #endregion // Misc Methods

        #region Read Methods
        /// <summary>
        /// Reads a data widget's value.
        /// </summary>
        /// <param name="widgetName">Name of the widget to read.</param>
        /// <param name="stream">Calling function owns the out Stream and needs to make sure they dispose of it properly.</param>
        /// <returns>Whether the widget exists is a data widget and it's data was successfully read.</returns>
        public bool ReadDataWidget(String widgetName, out Stream stream)
        {
            stream = null;

            Widget rawWidget = Client.BM.FindFirstWidgetFromPath(widgetName);
            if (rawWidget != null && rawWidget is WidgetData)
            {
                WidgetData dataWidget = (WidgetData)rawWidget;
                byte[] value = dataWidget.Data;

                stream = new MemoryStream();
                stream.Write(value, 0, value.Length);
                stream.Position = 0;
                return true;
            }
            return false;
        }
        #endregion // Read Methods

        #region Write Methods
        /// <summary>
        /// Writes data to a particular Data widget.
        /// </summary>
        /// <param name="widgetName">Name of the widget to write data to.</param>
        /// <param name="value">Data to send to the widget.</param>
        /// <returns>Whether the widget exists is a data widget and it's data was successfully written.</returns>
        public bool WriteDataWidget(String widgetName, Stream value)
        {
            Widget rawWidget = Client.BM.FindFirstWidgetFromPath(widgetName);
            if (rawWidget != null && rawWidget is WidgetData)
            {
                WidgetData dataWidget = (WidgetData)rawWidget;
                dataWidget.SetValue(value);
                return true;
            }
            return false;
        }
        #endregion // Write Methods

        #region IDisposable Implementation
        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            Disconnect();
        }
        #endregion // IDisposable Implementation
    } // RemoteConnection
}
