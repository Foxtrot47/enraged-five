#include "RsULog.h"
#include "file/asset.h"
// 
// #include <iostream>
// using namespace std;
// 
//#include <Lmcons.h>
// #include <windows.h>
#include "WindowsFuncs.h"
#define SYSTEM_INFO_BUFFER_SIZE 4098

atString UniversalLogFile::DEFAULT_VERSION("1.0");
atString UniversalLogFile::DEFAULT_XML_VERSION("1.0");
atString UniversalLogFile::DEFAULT_ENCODING("utf-8");
atString UniversalLogFile::DEFAULT_ROOT_ELEM("ulog");
atString UniversalLogFile::CONTEXT_ELEM("context");
atString UniversalLogFile::FILE_CONTEXT_ELEM("filecontext");
atString UniversalLogFile::CONTEXT_NAME_ATTR("name");
atString UniversalLogFile::xPath_Error("//context/error");
atString UniversalLogFile::xPath_Warning("//context/warning");

const char *validChars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLOMNOPQRSTUVWXYZ :,,.|-_$'\"=+[](){}<>?!%^&*/\\\0";
void ReplaceInvalidChars(char *outstr, const char *instr)
{
	strcpy(outstr, instr);
	int index = (int)strspn(outstr, validChars);
	while(index!=-1 && index<(int)strlen(outstr))
	{
		outstr[index] = ' ';
		index = (int)strspn(outstr, validChars);
	}
}

/// <summary>
/// 
/// </summary>
/// <param name="filename"></param>
UniversalLogFile::UniversalLogFile(atString filename, atString context, bool contextAppend, bool messageAppend)
: mCurrentContext(NULL)
, mDocument(NULL)
, mHasWarnings(false)
, mHasErrors(false)
, mTimestamp(true)
{
	Init(GetGlobalLogFile(filename), true, contextAppend);
	if(HasValidDoc())
		SetContext(context, !messageAppend);
}

/// <summary>
/// 
/// </summary>
/// <param name="filename"></param>
// UniversalLogFile::UniversalLogFile(String filename, FileMode mode)
// : mCurrentContext(NULL)
// , mDocument(NULL)
// {
// 	Init(GetGlobalLogFile(filename), true);
// }

/// <summary>
/// 
/// </summary>
/// <param name="filename"></param>
/// <param name="immediateFlush"></param>
// UniversalLogFile::UniversalLogFile(String filename, FileMode mode, bool immediateFlush)
// : mCurrentContext(NULL)
// , mDocument(NULL)
// {
// 	Init(GetGlobalLogFile(filename), immediateFlush);
// }

/// <summary>
/// 
/// </summary>
void UniversalLogFile::Refresh()
{
	Init(mFilename, true, true);
}

/// <summary>
/// 
/// </summary>
/// <param name="name"></param>
void UniversalLogFile::SetContext(atString name, bool dumpContents)
{
	AssertMsg(NULL != mDocument, "No XML document.");
//	AssertMsg(mContexts.GetNumUsed(), "No contexts stack.");

	// See if our document already has this context, if so we will 
	// append to it; otherwise we create it.
	mCurrentContext = NULL;
	if (mContexts.Access(name))
	{
		mCurrentContext = mContexts[name];
		if (dumpContents)
		{
			xmlUnlinkNode(mCurrentContext);
			xmlFreeNode(mCurrentContext);
//			xmlFreeDocElementContent(mDocument, mCurrentContext);

// 			XmlAttribute attrName = Document.CreateAttribute(CONTEXT_NAME_ATTR);
// 			attrName.Value = name;
// 			CurrentContext.Attributes.Append(attrName);
			xmlNewProp(mCurrentContext, BAD_CAST CONTEXT_NAME_ATTR.c_str(), BAD_CAST name.c_str());
		}
	}
	else
	{
		xmlNodePtr rootNode = xmlDocGetRootElement(mDocument);
		xmlNodePtr elemContext = xmlNewChild(rootNode, NULL, BAD_CAST CONTEXT_ELEM.c_str(), NULL);
		mContexts.Insert(name, elemContext);
		mCurrentContext = elemContext;

		xmlNewProp(mCurrentContext, BAD_CAST CONTEXT_NAME_ATTR.c_str(), BAD_CAST name.c_str());

// 		xmlElement elemContext = Document.CreateElement(CONTEXT_ELEM);
// 		XmlAttribute attrName = Document.CreateAttribute(CONTEXT_NAME_ATTR);
// 		attrName.Value = name;
// 		elemContext.Attributes.Append(attrName);
// 		Document.DocumentElement.AppendChild(elemContext);
	}
	// Always flush when adding a new context.
	Flush();
}

atString UniversalLogFile::GetGlobalLogFile(atString localLogFilePath)
{
// 	atString mapDir = localLogFilePath != String.Empty ? System.IO.Path.GetDirectoryName(localLogFilePath) : System.IO.Path.GetTempPath();
// 	String mapName = "temp";
// 	if (localLogFilePath != String.Empty)
// 	{
// 		String[] dirNames = mapDir.Split(System.IO.Path.DirectorySeparatorChar);
// 		mapName = dirNames[dirNames.Length - 1];
// 	}
// 	//            String[] pathParts = { mapDir, mapName + "_unilog.xml" };
// 	String uLogFilename = System.IO.Path.Combine(mapDir, mapName + "_unilog.ulog");
//	return uLogFilename;
	return localLogFilePath;
}

void UniversalLogFile::ParseForFormerWarnings()
{
// 	XPathDocument xPathDoc = new XPathDocument(Filename);
// 	XPathNavigator docRoot = xPathDoc.CreateNavigator();
// 	XPathNodeIterator nodes = docRoot.Select(xPath_Error);
// 	if (nodes.MoveNext())
// 	{
// 		HasErrors = true;
// 	}
// 	nodes = docRoot.Select(xPath_Warning);
// 	if (nodes.MoveNext())
// 	{
// 		HasWarnings = true;
// 	}
}

/// <summary>
/// Local initialisation method.
/// </summary>
void UniversalLogFile::Init(atString filename, bool immediateFlush, bool append)
{
	mFilename = filename;
	mImmediateFlush = immediateFlush;
	mContexts.Reset();

	// libxml
	xmlIndentTreeOutput = 1;
	xmlKeepBlanksDefault(0);
	xmlSubstituteEntitiesDefault(1);
	LIBXML_TEST_VERSION;

	if(filename.length()<=0)
		return;

	char thePath[2048]; 
	ASSET.RemoveNameFromPath(thePath, 2048, filename);
	// Determine whether the directory exists.
	if (!ASSET.Exists(thePath, NULL))
	{
		// Try to create the directory.
		bool success = ASSET.CreateLeadingPath(filename);
		if(!success)
		{
			Assertf(0,"Creation of logging directory failed: %s", thePath);
			return;
		}
	}

	bool createNewDocRoot = false;
	if (ASSET.Exists(filename, NULL))
	{
		// Initialise our document.
		mDocument = xmlReadFile(filename, NULL, 0);
		if (mDocument == NULL) 
		{
			fprintf(stderr, "Failed to parse %s: %s\n", filename, xmlLastError.message);
			return;
		}
//		xmlFreeDoc(doc);

		if (append)
		{
			// Parse existing contexts
			xmlNodePtr rootNode = xmlDocGetRootElement(mDocument);
			if(rootNode)
			{
				for(xmlNodePtr elemContext = xmlFirstElementChild(rootNode); 
					elemContext; 
					elemContext = elemContext->next)
				{
					if(elemContext->type != XML_ELEMENT_NODE)
						continue;
					atString name((char*)xmlGetProp(elemContext, (const xmlChar*)"name"));

					const xmlNodePtr oldContextNode = mContexts[name];
					if(oldContextNode)
					{
						mContexts.Delete(name);
					}
					
//					const ContextMap::Entry &newEntry = 
					mContexts.Insert(name, elemContext);

					// Check if the current file has any errors/warnings
					for(xmlNodePtr elemMsg = xmlFirstElementChild(elemContext); 
						elemMsg; 
						elemMsg = elemMsg->next)
					{
						if(elemMsg->type != XML_ELEMENT_NODE)
							continue;
						
						atString msgType((char*)elemMsg->name);
						
						if(stricmp(msgType.c_str(), "error") == 0)
							mHasErrors = true;

						if(stricmp(msgType.c_str(), "warning") == 0)
							mHasWarnings = true;
					}

//					xmlNodePtr newContextNode = newEntry.data;
// 					if(oldContextNode && oldContextNode->doc!=mDocument)
// 					{
// 						for(xmlNodePtr elemMsg = xmlFirstElementChild(oldContextNode); 
// 							elemMsg; 
// 							elemMsg = elemContext->next)
// 						{
// 							xmlAddChild(newContextNode, elemMsg);
// 						}
// 					}
				}
			}
//			ParseForFormerWarnings();
		}
		else
		{
			xmlFreeDoc(mDocument);
			createNewDocRoot = true;
		}
	}
	else
		createNewDocRoot = true;

	if(createNewDocRoot)
	{
// 		// Xml Declaration
// 		XmlDeclaration decl = Document.CreateXmlDeclaration(DEFAULT_XML_VERSION, DEFAULT_ENCODING, String.Empty);
// 		Document.AppendChild(decl);
// 
// 		// Document root.
// 		XmlElement root = Document.CreateElement(DEFAULT_ROOT_ELEM);
// 		Document.AppendChild(root);
		/* 
		 * Creates a new document, a node and set it as a root node
		 */
		mDocument = xmlNewDoc(BAD_CAST "1.0");
		xmlNodePtr root_node = xmlNewNode(NULL, BAD_CAST DEFAULT_ROOT_ELEM.c_str());
		xmlDocSetRootElement(mDocument, root_node);

// 		/*
// 		 * Creates a DTD declaration. Isn't mandatory. 
// 		 */
// 		xmlDtdPtr dtd = xmlCreateIntSubset(mDocument, BAD_CAST "root", NULL, BAD_CAST "tree2.dtd");

// 		XmlAttribute attrVersion = Document.CreateAttribute("version");
// 		attrVersion.Value = DEFAULT_VERSION;
// 		root.Attributes.Append(attrVersion);
		xmlNewProp(root_node, BAD_CAST "version", BAD_CAST DEFAULT_VERSION.c_str());

 		char userName[SYSTEM_INFO_BUFFER_SIZE];
		if(UserName(userName, SYSTEM_INFO_BUFFER_SIZE))
		{
// 		XmlAttribute attrUser = Document.CreateAttribute("user");
// 		attrUser.Value = Environment.UserName;
// 		root.Attributes.Append(attrUser);
			xmlNewProp(root_node, BAD_CAST "user", BAD_CAST userName);
		}

 		char hostName[SYSTEM_INFO_BUFFER_SIZE];
 		if(MachineName(hostName, SYSTEM_INFO_BUFFER_SIZE))
		{
// 		XmlAttribute attrMachine = Document.CreateAttribute("machine");
// 		attrMachine.Value = Environment.MachineName;
// 		root.Attributes.Append(attrMachine);
			xmlNewProp(root_node, BAD_CAST "machine", BAD_CAST hostName);
		}

		/* 
		 * Dumping document to stdio or file
		 */
		xmlSaveFormatFileEnc(filename, mDocument, "UTF-8", 1);

		/*free the document */
//		xmlFreeDoc(mDocument);
	}

	SetContext(atString("default"));
}

/// <summary>
/// 
/// </summary>
/// <param name="e"></param>
void UniversalLogFile::LogMessage(atString elemName, sLogMessageEventArgs e)
{ 
	if(!IsInited())
		return;

	if(elemName=="error")
		mHasErrors = true;
	if(elemName=="warning")
		mHasWarnings = true;

	char msgbuffer[4096];
	ReplaceInvalidChars(msgbuffer, e.mMessage.c_str());
	xmlNodePtr msgNode = xmlNewChild(mCurrentContext, NULL, BAD_CAST elemName.c_str(), BAD_CAST msgbuffer);

	if (e.mContext.length()!=0)
	{
		xmlNewProp(msgNode, BAD_CAST CONTEXT_ELEM.c_str(), BAD_CAST e.mContext.c_str());
	}
	if (e.mFileContext.length()!=0)
	{
		xmlNewProp(msgNode, BAD_CAST FILE_CONTEXT_ELEM.c_str(), BAD_CAST e.mFileContext.c_str());
	}
	if (mTimestamp)
	{
		char sysTime[SYSTEM_INFO_BUFFER_SIZE];
		if(TicksSinceEpoch(sysTime, SYSTEM_INFO_BUFFER_SIZE))
		{
			xmlNewProp(msgNode, BAD_CAST "timestamp_ticks", BAD_CAST sysTime);
		}
	}

	if (mImmediateFlush)
		Flush();
}


/// <summary>
/// 
/// </summary>
/// <param name="e"></param>
void UniversalLogFile::Profile(atString elemName, sProfileMessageEventArgs e)
{ 
	if(!IsInited())
		return;

	xmlNodePtr profileNode = xmlNewChild(mCurrentContext, NULL, BAD_CAST elemName.c_str(), NULL);

	if (e.mContext.length()!=0)
	{
		xmlNewProp(profileNode, BAD_CAST CONTEXT_ELEM.c_str(), BAD_CAST e.mContext.c_str());
	}
	if (mTimestamp)
	{
		char sysTime[SYSTEM_INFO_BUFFER_SIZE];
		if(TicksSinceEpoch(sysTime, SYSTEM_INFO_BUFFER_SIZE))
		{
			xmlNewProp(profileNode, BAD_CAST "timestamp_ticks", BAD_CAST sysTime);
		}
	}

	if (mImmediateFlush)
		Flush();
}
