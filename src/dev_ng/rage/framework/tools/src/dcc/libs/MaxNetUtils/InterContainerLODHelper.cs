﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;

using RSG.Base.ConfigParser;
using RSG.SceneXml;

namespace RSG.MaxUtils
{
    /// <summary>
    /// Class providing utility methods for more complex inter-container queries
    /// </summary>
    public class InterContainerLODHelper
    {
        public InterContainerLODHelper()
        {
            
        }

        /// <summary>
        /// Provided to prevent having ConfigGameView and Scene objects lying around
        /// </summary>
        public void Startup()
        {
            configGameView_ = new ConfigGameView();
            cachedScenes_ = new Dictionary<string, Scene>();
        }

        /// <summary>
        /// Provided to prevent having ConfigGameView and Scene objects lying around
        /// </summary>
        public void Shutdown()
        {
            cachedScenes_ = null;
            configGameView_.Dispose();
            configGameView_ = null;
        }

        /// <summary>
        /// Get the largest child LOD distance for a given node in a given container
        /// </summary>
        /// <param name="containerName">The container name is supplied to find sibling containers</param>
        /// <param name="nodeName">The node name is supplied to identify the node we're interested in</param>
        /// <returns></returns>
        public float GetMaxChildLODDistance(string containerName, string nodeName)
        {
            ContentNode containerNode = configGameView_.Content.Root.FindFirst(containerName, "map");

            if (containerNode == null)
                return 0;

            if (containerNode.Parent == null || !(containerNode.Parent is ContentNodeGroup))
                return 0;

            ContentNodeGroup mapRegionGroupNode = (ContentNodeGroup)containerNode.Parent;

            var siblingContainersQuery = from node in mapRegionGroupNode.Children.OfType<ContentNodeMap>()
                                         where (node.MapType == ContentNodeMap.MapSubType.Container || node.MapType == ContentNodeMap.MapSubType.ContainerProps)
                                         select node;

            float maxChildLODDistance = 0;
            foreach (ContentNodeMap siblingMapNode in siblingContainersQuery)
            {
                // Lookup the scene XML pathname
                if(siblingMapNode.Outputs.Length != 1 || !(siblingMapNode.Outputs[0] is ContentNodeMapZip))
                    continue;

                ContentNodeMapZip siblingMapZipNode = (ContentNodeMapZip)siblingMapNode.Outputs[0];
                ContentNodeFile sceneXmlFileNode = siblingMapZipNode.Children.OfType<ContentNodeFile>().FirstOrDefault(fileNode => fileNode.Extension == "xml");
                if (sceneXmlFileNode == null)
                    continue;

                string sceneXmlPathname = Path.Combine(siblingMapZipNode.Path, sceneXmlFileNode.Filename);

                if (!File.Exists(sceneXmlPathname))
                    continue;

                // Load the scene xml if it isn't cached
                if (!cachedScenes_.ContainsKey(sceneXmlPathname))
                {
                    cachedScenes_.Add(sceneXmlPathname, new Scene(sceneXmlPathname, siblingMapNode));
                }

                // Find any child nodes here
                Scene siblingScene = cachedScenes_[sceneXmlPathname];
                foreach (TargetObjectDef objectDef in siblingScene.Walk(Scene.WalkMode.DepthFirst))
                {
                    if(objectDef.IsObject() && 
                       objectDef.LOD != null && 
                       objectDef.LOD.Parent != null && 
                       objectDef.LOD.Parent.IsContainerLODRefObject() &&
                       (String.Compare(objectDef.LOD.Parent.RefName, nodeName, true) == 0))
                    {
                        float lodDistance = objectDef.GetAttribute(AttrNames.OBJ_LOD_DISTANCE, AttrDefaults.OBJ_LOD_DISTANCE);
                        if (lodDistance > maxChildLODDistance)
                            maxChildLODDistance = lodDistance;
                    }
                }
            }

            return maxChildLODDistance;
        }

        private ConfigGameView configGameView_;
        private Dictionary<string, Scene> cachedScenes_;
    }
}
