#ifndef MAXUTIL_H
#define MAXUTIL_H

//
// File:: MaxUtil.h
// Description:: Main header for MaxUtil static library.
//
// Author:: David Muir <david.muir@rockstarnorth.com>
// Date:: 10 March 2009
//

#include "MaxUtil_Mesh.h"
#include "iskin.h"

// Forward declarations
class INode;
class Object;

namespace MaxUtil
{

	/**
	 * @brief Return base object in geometry pipeline.
	 *
	 * This functions returns the base Object in the geometry pipeline, 
	 * ignoring modifiers etc which are normally returned by INode::GetObjectRef.
	 */
	Object* GetBaseObject( INode* pNode );

	/**
	 * @brief Return base object in geometry pipeline.
	 *
	 * Get unique blendshape name, this is so blends on different meshes dont hash clash
	 */
	CStr GetUniqueBlendShapeName(INode* p_Node, const CStr& strMorphName);

	/**
	 * @brief Return base object in geometry pipeline.
	 *
	 * Get unique blendshape name that uses micro morphs.
	 */
	CStr GetUniqueBlendshapeMicroMorphName(INode* p_Node, const CStr& strMorphName);

	/**
	* @brief Is this node a container node?
	*
	*/
	bool IsContainer(INode* node);

	/************************************************************************/
	/* Convert the inner TimeValue (1/4800 of a second) to the frame number
	/************************************************************************/
	int TimeValueToFrame(TimeValue t);

	/************************************************************************/
	/*  get a frame time of a control at key index
	/************************************************************************/
	int GetKeyFrameTime(Control *c, int index);

	bool IsRootSkelNode(INode* p_Node);

	bool IsBoneType(INode* p_Node);

	INode* GetParent(INode* p_Node);

	bool GetUserPropString(INode* pNode, const char* pEntry, TSTR& string);

	/************************************************************************/
	/*  get a UI name of a Max animation controller 
	/************************************************************************/
	BOOL GetControlName(Control* pControl, Animatable* pNode, char * nameBuffer, int bufferSize);

	bool pattern_match(const char *str, const char *pattern);

	/*	PURPOSE
		gets the skin modifier applied to a mesh
	*/
	ISkin* GetSkin(INode* p_Node);

	/*	PURPOSE
		get the connected skins root bone for a passed in skinned mesh
	*/
	INode* GetSkinRootBone(ISkin* p_Skin);

	/*
	Get modifier of cetain class from node
	*/
	Modifier *
	GetModifier(INodePtr pNode, Class_ID modID);

} // MaxUtil namespace

#endif // MAXUTIL_H

// MaxUtil.h
