#ifndef MAXUTIL_MESH_H
#define MAXUTIL_MESH_H

//
// File:: MaxUtil_Mesh.h
// Description:: Mesh Max utility functions header.
//
// Author:: David Muir <david.muir@rockstarnorth.com>
// Date:: 10 March 2009
//

// Max headers
#pragma warning ( push )
#pragma warning ( disable : 4100 4244 4238 4239 4245 4512 )
#include "Max.h"
#pragma warning ( pop )

namespace MaxUtil
{

	/**
	 * @brief GetMesh - return mesh from Object.
	 *
	 * Returns the mesh contained in a Object if one exists.
	 */
	Mesh* GetMesh( Object *pObject );

	/**
	 * @brief GetMesh - return mesh from INode.
	 *
	 * Returns the mesh contained in an INode if one exists.
	 */
	Mesh* GetMesh( INode *pNode );

} // MaxUtil namespace

#endif // MAXUTIL_MESH_H

// MaxUtil_Mesh.h
