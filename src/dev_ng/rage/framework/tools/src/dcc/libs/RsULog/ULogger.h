#ifndef __UNIVERSALLOGGERHELPER_H__
#define __UNIVERSALLOGGERHELPER_H__

#include "RsULog.h"

#define ULOGGER UniversalLogHelper::GetInstance()

class UniversalLogHelper
{	
private:
	atString m_strTool;
	atArray<atString> m_atAdditionalLogs;

	UniversalLogFile *m_pULogFile;

public:
	UniversalLogHelper();
	~UniversalLogHelper();

	// PURPOSE: Returns a static singleton instance. Avoid where possible.
	static UniversalLogHelper& GetInstance();

	UniversalLogFile* GetULogFile() { return m_pULogFile; }

	// standard log interface
	bool InitPath(const char* path, const char* context="", bool append=false);
	void LogMessage(const char* msg, const char* context="", const char* filecontext="");
	void LogError(const char* msg, const char* context="", const char* filecontext="");
	void LogWarning(const char* msg, const char* context="", const char* filecontext="");
	void LogDebug(const char* msg, const char* context="", const char* filecontext="");
	void ProfileStart(const char* context);
	void ProfileEnd(const char* context="");
	void Refresh();
	void Flush();
	void ImmediateFlush(bool flush);
	
	void ShowDialog(const char *parameters="");

	atString GetXmlErrors()
	{
		if(m_pULogFile)
			return m_pULogFile->GetXmlErrors();
		return atString();
	}

	// PURPOSE: Sets a log message.
	// PARAMS:
	//    pMessage - log message containing variable arguments (if any).
	//	  ... - variable arguments, values corresponding to pMessage %d %s etc
	void SetProgressMessage( const char * pMessage, ... );

	// PURPOSE: Adds an additional log to the list which will be displayed in the log dir version of PostExport
	void AddAdditionalLog(const char* pLogFilename);

	// PURPOSE: Adds additional logs to the list from provided directory which will be displayed in the log dir version of PostExport
	void AddAdditionalLogDir(const char* pLogDir);

	// PURPOSE: Readies the log, reads from the log filename provided.
	// PARAMS:
	//    pLogFilename - the file name of the log to create.
	void PreExport(const char *pLogFilename, const char* pContext, bool bContextAppend = true, bool bMessageAppend = true );

	// PURPOSE: Frees our progress meter and closes the log file stream after export.
	// PARAMS:
	//    bShowDialog - If true display the ulog viewer, else don't.
	void PostExport( bool bShowDialog = false, bool bShowUpAnyway = false );

	void PostExport(const char* pLogDir, bool bShowDialog, bool bShowUpAnyway = false );
};

#endif