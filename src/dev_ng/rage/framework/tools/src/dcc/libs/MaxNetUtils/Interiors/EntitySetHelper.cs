﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;

using RSG.Base.ConfigParser;
using RSG.SceneXml;

namespace RSG.MaxUtils.Interiors
{
    /// <summary>
    /// Class providing utility methods for complex interior entity set queries
    /// </summary>
    public class EntitySetHelper
    {
        public EntitySetHelper()
        {

        }

        /// <summary>
        /// Provided to prevent having ConfigGameView and Scene objects lying around
        /// </summary>
        public void Startup()
        {
            configGameView_ = new ConfigGameView();
            cachedScenes_ = new Dictionary<String, Scene>();
        }

        /// <summary>
        /// Provided to prevent having ConfigGameView and Scene objects lying around
        /// </summary>
        public void Shutdown()
        {
            cachedScenes_ = null;
            configGameView_.Dispose();
            configGameView_ = null;
        }

        public String[] GetInteriorEntitySetNames(String interiorMaxFilePathname, String miloObjectName)
        {
            ContentNodeMap interiorMapNode = null;
            if(!GetMapNodeFromMaxFilePathname(interiorMaxFilePathname, out interiorMapNode))
                return new String[0];

            String sceneXmlPathname = null;
            if (!GetSceneXmlPathnameFromMapNode(interiorMapNode, out sceneXmlPathname))
                return new String[0];

            if(!cachedScenes_.ContainsKey(sceneXmlPathname))
                cachedScenes_.Add(sceneXmlPathname, Scene.Load(sceneXmlPathname, LoadOptions.All, false));

            return GetInteriorEntitySetNames(cachedScenes_[sceneXmlPathname], miloObjectName);
        }

        private bool GetMapNodeFromMaxFilePathname(String maxFilePathname, out ContentNodeMap matchingMapNode)
        {
            foreach (ContentNodeMap mapNode in configGameView_.Content.Root.FindAll("map").OfType<ContentNodeMap>())
            {
                String maxFileBasename = Path.GetFileNameWithoutExtension(maxFilePathname);
                if (String.Compare(maxFileBasename, mapNode.Name, true) == 0)
                {
                    matchingMapNode = mapNode;
                    return true;
                }
            }

            matchingMapNode = null;
            return false;
        }

        private bool GetSceneXmlPathnameFromMapNode(ContentNodeMap mapNode, out String sceneXmlPathname)
        {
            sceneXmlPathname = null;
            ContentNodeMapZip mapZipNode = mapNode.Outputs.OfType<ContentNodeMapZip>().FirstOrDefault();
            if (mapZipNode == null)
                return false;

            ContentNodeFile sceneXmlFileNode = mapZipNode.Children.OfType<ContentNodeFile>().FirstOrDefault(fileNode => fileNode.Extension == "xml");
            if (sceneXmlFileNode == null)
                return false;

            sceneXmlPathname = Path.Combine(mapZipNode.Path, sceneXmlFileNode.Filename);
            return true;
        }

        private String[] GetInteriorEntitySetNames(Scene interiorScene, String miloObjectName)
        {
            if(miloObjectName.EndsWith("_milo_"))
                miloObjectName = miloObjectName.Replace("_milo_", "");

            List<String> entitySetNames = new List<String>();

            // Find the milo
            TargetObjectDef miloObjectDef = null;
            foreach (TargetObjectDef objectDef in interiorScene.Walk(Scene.WalkMode.BreadthFirst))
            {
                if (objectDef.IsMilo() && (String.Compare(objectDef.Name, miloObjectName, true) == 0))
                {
                    miloObjectDef = objectDef;
                    break;
                }
            }

            // Find the interior groups
            if (miloObjectDef != null)
            {
                GetInteriorGroupNamesRecursive(miloObjectDef, entitySetNames);
            }

            return entitySetNames.ToArray();
        }

        private void GetInteriorGroupNamesRecursive(TargetObjectDef objectDef, List<String> entitySetNames)
        {
            if (objectDef.IsInteriorGroup())
            {
                String entitySetName = objectDef.GetParameter(ParamNames.OBJECT_GROUP_NAME, ParamDefaults.OBJECT_GROUP_NAME);
                entitySetNames.Add(entitySetName);
            }

            foreach (TargetObjectDef childObjectDef in objectDef.Children)
            {
                GetInteriorGroupNamesRecursive(childObjectDef, entitySetNames);
            }
        }

        private ConfigGameView configGameView_;
        private Dictionary<String, Scene> cachedScenes_;
    }
}
