Project RsULogWrapper
Template max_configurations.xml

OutputFileName RsULogWrapper.gup
ConfigurationType dll

WarningsAsErrors false
WarningLevel 3

Define _CRT_SECURE_NO_DEPRECATE;_CRT_NONSTDC_NO_DEPRECATE;_SCL_SECURE_NO_DEPRECATE;ISOLATION_AWARE_ENABLED=1;MODULE_NAME=$(TargetFileName)

IncludePath $(RAGE_DIR)\framework\tools\src\dcc\libs\RsULog
IncludePath $(RAGE_DIR)\base\src
IncludePath $(RAGE_DIR)\base\tools\3rdParty\libxml2-2.7.6\include

ForceInclude forceinclude.h

EmbeddedLibAll odbc32.lib odbccp32.lib comctl32.lib bmm.lib core.lib geom.lib gfx.lib mesh.lib maxutil.lib maxscrpt.lib gup.lib paramblk2.lib

Files {
	Folder "Header Files" { 
		3dsmaxsdk_preinclude.h
		forceinclude.h
		IFPULog.h
		NativeInclude.h
		RsULogWrapper.h
		RsULogWrapperDesc.h
	}
	Folder "Resource Files" {	
		resource.h
		RsULogWrapper.rc
	}
	Folder "Source Files" {
		RsULogWrapper.cpp
		RsULogWrapper.def
		IFPULog.cpp
		RsULogWrapperDesc.cpp
		DllEntry.cpp
	}	
}