//
// File:: MaxUtil.cpp
// Description:: Main implementation of functions in MaxUtil static library.
//
// Author:: David Muir <david.muir@rockstarnorth.com>
// Date:: 16 April 2009
//

#include "MaxUtil.h"

// Max headers
#pragma warning ( push )
#pragma warning ( disable : 4100 4244 4238 4239 4245 4512 )
#include "Max.h"
#include "IContainerManager.h"
#include "cs\bipexp.h" // CLASS IDS
#include "modstack.h"
#pragma warning ( pop )

#include <string>
#include <sstream>
#include <algorithm>

namespace MaxUtil
{

Object* 
GetBaseObject( INode* pNode )
{
	Object* pObject = pNode->GetObjectRef( );
	if ( !pObject )
		return ( NULL );

	Object* pBaseObject = pObject->FindBaseObject( );
	return ( pBaseObject );
}

CStr 
GetUniqueBlendShapeName(INode* p_Node, const CStr& strMorphName)
{
	std::string strNodeName(p_Node->GetName());
	int iIndex = (int)strNodeName.find("_");
	if(iIndex != -1)
	{
		strNodeName[iIndex+1] = 0;
	}

	std::string strBlendName(strNodeName.c_str());
	strBlendName += strMorphName;

	return CStr(strBlendName.c_str());
}

CStr
GetUniqueBlendshapeMicroMorphName(INode* p_Node, const CStr& strMorphName)
{
	std::string strBlendName;
	strBlendName += "Micro_";
	strBlendName += strMorphName;

	return CStr(strBlendName.c_str());
}

bool IsContainer(INode* node) 
{  
	IContainerObject* co = GetContainerManagerInterface()->IsContainerNode( node);  
	return co != NULL;
}

int TimeValueToFrame(TimeValue t)
{
	float timeFloat = (float)t;
	float partOfSecond = timeFloat/4800.0f;
	return GetFrameRate() * partOfSecond;
}

int GetKeyFrameTime(Control *c, int index)
{
	TimeValue t = 0;
	IKeyControl *ikeys = GetKeyControlInterface(c);
	switch(c->ClassID().PartA())
	{
		//////////////////////////////////////////////////////////////////////////
	case TCBINTERP_POSITION_CLASS_ID:
	case TCBINTERP_POINT3_CLASS_ID:
		{
			ITCBPoint3Key lastKey;
			ikeys->GetKey(index, &lastKey);
			t = lastKey.time;
		}
		break;
	case TCBINTERP_ROTATION_CLASS_ID:
		{
			ITCBRotKey lastKey;
			ikeys->GetKey(index, &lastKey);
			t = lastKey.time;
		}
		break;
	case TCBINTERP_FLOAT_CLASS_ID:
		{
			ITCBFloatKey lastKey;
			ikeys->GetKey(index, &lastKey);
			t = lastKey.time;
		}
		break;
		//////////////////////////////////////////////////////////////////////////
	case HYBRIDINTERP_POINT3_CLASS_ID:
	case HYBRIDINTERP_POSITION_CLASS_ID:
	case HYBRIDINTERP_ROTATION_CLASS_ID:
		{
			IBezPoint3Key lastKey;
			ikeys->GetKey(index, &lastKey);
			t = lastKey.time;
		}
		break;
	case HYBRIDINTERP_FLOAT_CLASS_ID:
		{
			IBezFloatKey lastKey;
			ikeys->GetKey(index, &lastKey);
			t = lastKey.time;
		}
		break;
		//////////////////////////////////////////////////////////////////////////
	case LININTERP_POSITION_CLASS_ID:
		{
			ILinPoint3Key lastKey;
			ikeys->GetKey(index, &lastKey);
			t = lastKey.time;
		}
		break;
	case LININTERP_ROTATION_CLASS_ID:
		{
			ILinRotKey lastKey;
			ikeys->GetKey(index, &lastKey);
			t = lastKey.time;
		}
		break;
	case LININTERP_FLOAT_CLASS_ID:
		{
			ILinFloatKey lastKey;
			ikeys->GetKey(index, &lastKey);
			t = lastKey.time;
		}
		break;
	}
	//////////////////////////////////////////////////////////////////////////
	return MaxUtil::TimeValueToFrame(t);
}

INode* GetParent(INode* p_Node)
{
	INode* p_ParentNode = p_Node->GetParentNode();

	if(p_ParentNode == GetCOREInterface()->GetRootNode())
	{
		return NULL;
	}

	return p_ParentNode;
}

bool GetUserPropString(INode* pNode, const char* pEntry, TSTR& string)
{
	std::string entry(pEntry);
	std::transform(entry.begin(), entry.end(), entry.begin(), ::tolower);

	MSTR buffer;
	pNode->GetUserPropBuffer(buffer);

	std::string line;
	std::stringstream stream;
	stream.str(buffer.data());
	while( getline(stream, line) )
	{
		std::string lowercaseline = line;
		std::transform(lowercaseline.begin(), lowercaseline.end(), lowercaseline.begin(), ::tolower);

		// make sure the line starts with the entry we are looking for
		if(lowercaseline.find(entry) == 0)
		{
			int index = (int)line.find("=");
			if(index != -1)
			{
				std::string data = line.substr(index+1, line.length());
				data.erase(remove_if(data.begin(), data.end(), isspace), data.end());
				string = data.c_str();
				return true;
			}
		}
	}

	return false;
}

#define PROCOBJ_CLASSID Class_ID(0x2e205dbb, 0x2d724d25)

bool IsBoneType(INode* p_Node)
{
	if(!p_Node)
		return false;

	Object* p_Object = p_Node->EvalWorldState(0).obj;

	if(p_Object == NULL)
		return false;

	Class_ID ObjClassID = p_Object->ClassID();

	//if we get any more of these ill add a generic interface...
	if(ObjClassID == BONE_OBJ_CLASSID)
		return true;

	return false;
}

bool IsRootSkelNode(INode* p_Node)
{
	if(IsBoneType(p_Node))
	{
		INode* p_ParentNode = GetParent(p_Node);

		while(p_ParentNode != NULL)
		{
			if(IsBoneType(p_ParentNode))
				return false;

			p_ParentNode = GetParent(p_ParentNode);
		}

		return true;
	}

	return false;
}

BOOL GetControlName(Control* pControl, Animatable* pNode, char * nameBuffer, int bufferSize)
{
	int numSubs = pNode->NumSubs();
	for(int subIdx = 0; subIdx < numSubs; subIdx++)
	{	
		::Control* pCompCtrl = dynamic_cast<::Control*>(pNode->SubAnim(subIdx));
		if(pCompCtrl)
		{
			if(pCompCtrl==pControl)
			{
				MSTR nameString = pNode->SubAnimName(subIdx);
				strcpy_s(nameBuffer, bufferSize, nameString.data());
				return TRUE;
			}

			if(GetControlName(pControl, pCompCtrl, nameBuffer, bufferSize))
				return TRUE;
		}
	}
	return FALSE;
}

//
//  
// http://www.codeproject.com/Articles/19694/String-Wildcard-Matching-and
bool pattern_match(const char *str, const char *pattern) {
	enum State {
		Exact,      	// exact match
		Any,        	// ?
		AnyRepeat    	// *
	};

	const char *s = str;
	const char *p = pattern;
	const char *q = 0;
	int state = 0;

	bool match = true;
	while (match && *p) {
		if (*p == '*') {
			state = AnyRepeat;
			q = p+1;
		} else if (*p == '?') state = Any;
		else state = Exact;

		if (*s == 0) break;

		switch (state) {
			case Exact:
				match = *s == *p;
				s++;
				p++;
				break;

			case Any:
				match = true;
				s++;
				p++;
				break;

			case AnyRepeat:
				match = true;
				s++;

				if (*s == *q) p++;
				break;
		}
	}

	if (state == AnyRepeat) return (*s == *q);
	else if (state == Any) return (*s == *p);
	else return match && (*s == *p);
} 


//////////////////////////////////////////////////////////////////////////////////////////////////////
ISkin* GetSkin(INode* p_Node)//, IDerivedObject* p_DerObj, Modifier* p_Mod)
{
	Object* p_Obj = p_Node->GetObjectRef();

	if(!p_Obj)
		return NULL;

	while(p_Obj->SuperClassID() == GEN_DERIVOB_CLASS_ID)
	{
		IDerivedObject* p_DerObj = (IDerivedObject*)(p_Obj);

		int Idx = 0,Count = p_DerObj->NumModifiers();

		while(Idx < Count)
		{
			Modifier* p_Mod = p_DerObj->GetModifier(Idx);

			if(p_Mod->ClassID() == SKIN_CLASSID)
				return ((ISkin*)p_Mod->GetInterface(I_SKIN));

			Idx++;
		}

		p_Obj = p_DerObj->GetObjRef();
	}

	return NULL;
}
int GetNumLevels(INode* p_Node)
{
	int Levels = 0;
	INode* p_RootNode = GetCOREInterface()->GetRootNode();

	while(p_Node != p_RootNode)
	{
		p_Node = p_Node->GetParentNode();
		Levels++;
	}

	return Levels;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
INode* GetSkinRootBone(ISkin* p_Skin)
{
	//come back to this, but works in the test...

	if(!p_Skin)
		return NULL;

	// DHM 26 May 2009
	// Fix for when a mesh has a Skin modifier with no bones defined.
	// This could be integrated with the NumBones int below.
	if ( 0 == p_Skin->GetNumBones() )
		return NULL;

	INode* p_RootMostBone = p_Skin->GetBone(0);
	int i,NumBones = p_Skin->GetNumBones();

	for(i=1;i<NumBones;i++)
	{
		INode* p_NewBone = p_Skin->GetBone(i);

		int NumBones = GetNumLevels(p_NewBone);
		int NumRootBones = GetNumLevels(p_RootMostBone);

		TCHAR* pName = p_NewBone->GetName();
		TCHAR* pRootName = p_RootMostBone->GetName();

		if(NumBones < NumRootBones)	
		{
			p_RootMostBone = p_NewBone;
		}
	}

	return p_RootMostBone;

}

Modifier *GetModifier(INodePtr pNode, Class_ID modID)
{
	Object *pObj = pNode->GetObjectRef();
	if(pObj->SuperClassID()==GEN_DERIVOB_CLASS_ID)
	{
		IDerivedObject *pDerObj = dynamic_cast<IDerivedObject*>(pObj);
		for(int modIndex = 0;modIndex<pDerObj->NumModifiers(); modIndex++)
		{
			Modifier *pMod = pDerObj->GetModifier(modIndex);
			if(pMod->ClassID() == modID)
			{
				return pMod;
			}
		}
	}
	return NULL;
}
//-----------------------------------------------------------------------------
} // MaxUtil namespace

// MaxUtil.cpp
