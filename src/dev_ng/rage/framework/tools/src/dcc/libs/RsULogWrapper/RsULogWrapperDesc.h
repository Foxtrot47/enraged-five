#ifndef _RSULOG_DESC_
#define _RSULOG_DESC_

#pragma unmanaged
#include "nativeInclude.h"

class RsULogWrapperClassDesc : public ClassDesc2 
{
public:
	virtual int IsPublic() 							{ return TRUE; }
	virtual void* Create(BOOL /*loading = FALSE*/);
	virtual const TCHAR *	ClassName();
	virtual SClass_ID SuperClassID() 				{ return GUP_CLASS_ID; }
	virtual Class_ID ClassID();
	virtual const TCHAR* Category();
	virtual int NumActionTables();

 	virtual const TCHAR* InternalName() 			{ return _T("RsULog"); }	// returns fixed parsable name (scripter-visible name)
 	virtual HINSTANCE HInstance() 					{ return hInstance; }					// returns owning module handle


};

#endif // _RSULOG_DESC_