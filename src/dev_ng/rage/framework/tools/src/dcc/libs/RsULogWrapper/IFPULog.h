#ifndef _IFPULOG_H_
#define _IFPULOG_H_

#pragma unmanaged
#include "nativeInclude.h"

class UniversalLogHelper;

////////////////////////////////////////////////////////////////////////
//Function publishing
////////////////////////////////////////////////////////////////////////

#define FP_ULOG_INTERFACE Interface_ID(0x4a643ce2, 0x58da024b)

#define RsULogInst() \
	(IFPULog*)GetCOREInterface( FP_ULOG_INTERFACE );

class IFPULog : public FPStaticInterface
{
public:
	enum { 
		emInitPath, 
		emLogError, 
		emLogWarning, 
		emLogMsg, 
		emLogDebug, 
		emLogShowDialog, 
		emLogRefresh, 
		emLogFlush, 
		emLogImFlush,
		emLogProfileStart,
		emLogProfileEnd
	};

	virtual bool	InitPath(const char* path, const char* context, bool append=true)=0;
	virtual void	LogError(const char* msg, const char* context="", const char* filecontext="")=0;
	virtual void	LogWarning(const char* msg, const char* context="", const char* filecontext="")=0;
	virtual void	LogMessage(const char* msg, const char* context="", const char* filecontext="")=0;
	virtual void	LogDebug(const char* msg, const char* context="", const char* filecontext="")=0;
	virtual void	ProfileStart(const char* context="")=0;
	virtual void	ProfileEnd(const char* context="")=0;
	virtual void	ImmediateFlush(bool flush)=0;
	virtual void	Flush()=0;
	virtual void	Refresh()=0;
	virtual void	ShowDialog()=0;
};

class FPULog : public IFPULog
{
	bool	InitPath(const char* path, const char* context, bool append=true);

	void	LogError(const char* msg, const char* context="", const char* filecontext="");
	void	LogWarning(const char* msg, const char* context="", const char* filecontext="");
	void	LogMessage(const char* msg, const char* context="", const char* filecontext="");
	void	LogDebug(const char* msg, const char* context="", const char* filecontext="");
	void	ProfileStart(const char* context="");
	void	ProfileEnd(const char* context="");
	void	ImmediateFlush(bool flush);
	void	Flush();
	void	Refresh();
	void	ShowDialog();

	DECLARE_DESCRIPTOR_INIT(FPULog)
	BEGIN_FUNCTION_MAP;
	FN_3(IFPULog::emInitPath, TYPE_BOOL, InitPath, TYPE_STRING, TYPE_STRING, TYPE_BOOL);
	VFN_3(IFPULog::emLogError, LogError, TYPE_STRING, TYPE_STRING, TYPE_STRING);
	VFN_3(IFPULog::emLogWarning, LogWarning, TYPE_STRING, TYPE_STRING, TYPE_STRING);
	VFN_3(IFPULog::emLogMsg, LogMessage, TYPE_STRING, TYPE_STRING, TYPE_STRING);
	VFN_3(IFPULog::emLogDebug, LogDebug, TYPE_STRING, TYPE_STRING, TYPE_STRING);
	VFN_1(IFPULog::emLogImFlush, ImmediateFlush, TYPE_BOOL);
	VFN_1(IFPULog::emLogProfileStart, ProfileStart, TYPE_STRING);
	VFN_1(IFPULog::emLogProfileEnd, ProfileEnd, TYPE_STRING);
	VFN_0(IFPULog::emLogFlush, Flush);
	VFN_0(IFPULog::emLogRefresh, Refresh);
	VFN_0(IFPULog::emLogShowDialog, ShowDialog);
	END_FUNCTION_MAP;

	UniversalLogHelper *m_pUlogger;
};

#endif //_IFPULOG_H_