//Switching to managed to include the managed HitPointsProperty ref class.
//#pragma managed
#include "RsULogFunctions.h"
#include "RsULog.h"

// Managed types cannot be used in unmanaged code, so call into managed code
// from here.
// namespace// anonymous namespace
// {
	//	using namespace System;
	bool gInit(const char *path, const char *context)
	{
		bool success = true;
		success = RSGULogManaged::RsULog::Init(path, context);
		return success;
	}
	void gLogMessage(const char *msg, const char *context)
	{
		RSGULogManaged::RsULog::LogMessage(msg, context);
	}
	void gLogError(const char *msg, const char *context)
	{
		RSGULogManaged::RsULog::LogError(msg, context);
	}
	void gLogWarning(const char *msg, const char *context)
	{
		RSGULogManaged::RsULog::LogWarning(msg, context);
	}
	void gLogDebug(const char *msg, const char *context)
	{
		RSGULogManaged::RsULog::LogDebug(msg, context);
	}
//}