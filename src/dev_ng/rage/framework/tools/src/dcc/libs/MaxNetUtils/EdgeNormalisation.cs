﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;
using RSG.Base.Math;

namespace RSG.MaxUtils
{
    /// <summary>
    /// Helper class for the Adjacency List.
    /// </summary>
    [Serializable]
    public class ContainerEntry
    {
        [XmlAttribute("ContainerName")]
        public string ContainerName;

        [XmlAttribute("ReceivesNormals")]
        public bool ReceiveNormals;

        [XmlArray("AdjacencyContainers")]
        [XmlArrayItem("Container")]
        public List<string> AdjacentContainers;

        public ContainerEntry()
        {
            ContainerName = null;
            ReceiveNormals = false;
            AdjacentContainers = null;
        }

        public ContainerEntry(string containerName, bool receiveNormals)
        {
            ContainerName = containerName;
            ReceiveNormals = receiveNormals;
            AdjacentContainers = new List<string>();
        }

        public void AddAdjacentContainer(string adjacentContainer)
        {
            AdjacentContainers.Add(adjacentContainer);
        }
    }

    /// <summary>
    /// C# class resembling the adjacency list.  
    /// Can be serialized/deserialized from XML.
    /// </summary>
    [Serializable]
    public class AdjacencyList
    {
        [XmlElement("Containers")]
        public List<ContainerEntry> m_Entries;

        public AdjacencyList()
        {
            m_Entries = new List<ContainerEntry>();
        }

        public ContainerEntry Add(string containerName, bool receiveNormals)
        {
            ContainerEntry entry = new ContainerEntry(containerName, receiveNormals);
            m_Entries.Add(entry);

            return entry;
        }

        public ContainerEntry[] Entries
        {
            get { return m_Entries.ToArray(); }
        }

        /// <summary>
        /// Determines if the container is a giver or a receiver.
        /// </summary>
        /// <param name="containerName"></param>
        /// <returns></returns>
        public bool IsReceiver(string containerName)
        {
            foreach (ContainerEntry entry in m_Entries)
            {
                if (String.Compare(entry.ContainerName, containerName, true) == 0)
                {
                    return entry.ReceiveNormals;
                }
            }

            return false;
        }

        /// <summary>
        /// Acquires the list of all containers adjacent to this container.
        /// </summary>
        /// <param name="containerName"></param>
        /// <returns></returns>
        public string[] GetAdjacencyList(string containerName)
        {
            foreach (ContainerEntry entry in m_Entries)
            {
                if (String.Compare(entry.ContainerName, containerName, true) == 0)
                {
                    return entry.AdjacentContainers.ToArray();
                }
            }

            return null;
        }

        /// <summary>
        /// Deserializes the given XML.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static AdjacencyList Load(string filename)
        {
            StreamReader reader = null;
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(AdjacencyList));
                reader = new StreamReader(filename);
                AdjacencyList newList = (AdjacencyList)serializer.Deserialize(reader);
                reader.Close();
                return newList;
            }
            catch (Exception)
            {
                reader.Close();
                return null;
            }
        }

        /// <summary>
        /// Serializes the specified XML.
        /// </summary>
        /// <param name="list"></param>
        /// <param name="filename"></param>
        public static void Save(AdjacencyList list, string filename)
        {
            StreamWriter writer = null;
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(AdjacencyList));
                writer = new StreamWriter(filename);
                serializer.Serialize(writer, list);
                writer.Close();
            }
            catch (Exception e)
            {
                writer.Close();
                Console.WriteLine(e.Message);
            }
        }
    }
    /// <summary>
    /// Class containing a vertex position and the index associated with the vertex in 3D Studio Max.
    /// </summary>
    public class VertexInfo : IComparable<VertexInfo>
    {
        public Vector3f m_Vertex;
        public int m_Index;

        public VertexInfo(int index, Vector3f vertex)
        {
            m_Index = index;
            m_Vertex = vertex;
        }

        /// <summary>
        /// Comparator implementation; orders vertices by X then Y in ascending order.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(VertexInfo other)
        {
            if (other == null) return -1;

            if (other.m_Vertex.X < m_Vertex.X)
                return 1;
            else if (other.m_Vertex.X == m_Vertex.X)
            {
                if (other.m_Vertex.Y < m_Vertex.Y)
                    return 1;
                else if (other.m_Vertex.Y == m_Vertex.Y)
                    return 0;
                else
                    return -1;
            }
            else
                return -1;
        }
    }

    /// <summary>
    /// Class storing all relevant results of processing a given vertex.
    /// This class provides the specific vertex (or two vertices) closest to the target vertex.
    /// </summary>
    public class ResultInfo
    {
        public string BestContainer;
        public float BestDistance;
        public string BestMesh;
        public int BestVertexIndex;
        public int BestSecondVertexIndex;

        public ResultInfo()
        {
            BestContainer = null;
            BestMesh = null;
            BestDistance = float.MaxValue;
            BestVertexIndex = -1;
            BestSecondVertexIndex = -1;
        }

        public string GetBestContainer() { return BestContainer; }
        public string GetBestMesh() { return BestMesh; }
        public int GetBestVertexIndex() { return BestVertexIndex; }
        public int GetBestSecondVertexIndex() { return BestSecondVertexIndex; }
    }

    /// <summary>
    /// Class for holding data about a 3D Studio Max mesh, whose vertices can be processed.
    /// </summary>
    public class ContainerMesh 
    {
        public List<VertexInfo> m_Vertices;

        public ContainerMesh()
        {
            m_Vertices = new List<VertexInfo>();   
        }

        /// <summary>
        /// Adds a vertex to the container mesh.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="vertex"></param>
        public void AddVertex(int index, Vector3f vertex)
        {
            foreach (VertexInfo info in m_Vertices)
            {
                if (info.m_Index == index)
                    return;
            }

            VertexInfo newVertexInfo = new VertexInfo(index, vertex);
            m_Vertices.Add(newVertexInfo);
        }

        /// <summary>
        /// Sorts the vertex list.
        /// </summary>
        public void Sort()
        {
            m_Vertices.Sort();
        }

        /// <summary>
        /// Determines if the mesh has a vertex (or two) within a distance of the target vertex.
        /// </summary>
        /// <param name="vertex"></param>
        /// <param name="deltaThreshold"></param>
        /// <param name="closestDistance"></param>
        /// <param name="secondClosestIndex"></param>
        /// <returns></returns>
        private int FindClosestVertexIndex(Vector3f vertex, float deltaThreshold, out float closestDistance, out int secondClosestIndex)
        {
            closestDistance = float.MaxValue;

            int closestIndex = -1;
            secondClosestIndex = -1;

            for (int index = 0; index < m_Vertices.Count; ++index)
            {
                VertexInfo currentInfo = m_Vertices[index];

                if ((currentInfo.m_Vertex.X - vertex.X) > deltaThreshold) break;

                float distance2d = (float)vertex.Dist(currentInfo.m_Vertex);
                if (distance2d < closestDistance)
                {
                    closestDistance = distance2d;
                    secondClosestIndex = closestIndex;
                    closestIndex = index;
                }

                if (distance2d < deltaThreshold) break;
            }

            return closestIndex;
        }

        /// <summary>
        /// Process this mesh's vertex lists to find the closest vertex.
        /// </summary>
        /// <param name="vertex"></param>
        /// <returns></returns>
        public ResultInfo Process_EdgeNormalisation(Vector3f vertex)
        {
            ResultInfo resultInfo = new ResultInfo();

            float closestDistance = float.MaxValue;
            int secondClosestIndex = -1;
            int closestIndex = FindClosestVertexIndex(vertex, TerrainProcessing.EdgeNormalisation_DistanceThreshold, out closestDistance, out secondClosestIndex);

            if (closestIndex >= 0)
            {
                resultInfo.BestDistance = closestDistance;
                resultInfo.BestVertexIndex = m_Vertices[closestIndex].m_Index;

                if (secondClosestIndex >= 0)
                    resultInfo.BestSecondVertexIndex = m_Vertices[secondClosestIndex].m_Index;
            }

            if (closestDistance < TerrainProcessing.EdgeNormalisation_DistanceThreshold)
            {
                //Use the single vertex's normal.
                resultInfo.BestSecondVertexIndex = -1;
            }
            else
            {
                //Validate that the two vertices intersect the target point, if they don't, invaildate the search.
                //There are instances where edges not shared with other tiles will be incorrectly targets.
                bool invalidSelection = false;
                if (secondClosestIndex >= 0)
                {
                    Vector3f firstVertex = m_Vertices[closestIndex].m_Vertex;
                    Vector3f secondVertex = m_Vertices[secondClosestIndex].m_Vertex;

                    bool isPointBetweenLine = true;
                    float crossproduct = (vertex.Y - firstVertex.Y) * (secondVertex.X - firstVertex.X) - (vertex.X - firstVertex.X) * (secondVertex.Y - firstVertex.Y);
                    if ( Math.Abs(crossproduct) > 0.0001f )
                        isPointBetweenLine = false;
                    
                    float dotproduct = (vertex.X - firstVertex.X) * (secondVertex.X - firstVertex.X) + (vertex.Y - firstVertex.Y) * (secondVertex.Y - firstVertex.Y);
                    if ( dotproduct < 0.0f )
                        isPointBetweenLine = false;
                    
                    float squaredlengthba = (secondVertex.X - firstVertex.X) * (secondVertex.X - firstVertex.X) + (secondVertex.Y - firstVertex.Y) * (secondVertex.Y - firstVertex.Y);
                    if ( dotproduct > squaredlengthba )
                        isPointBetweenLine = false;

                    //bool isPointOnLine = ((vertex.Y - firstVertex.Y)) / ((vertex.X - firstVertex.X)) == ((secondVertex.Y - firstVertex.Y)) / ((secondVertex.X - firstVertex.X));
                    invalidSelection = !isPointBetweenLine;
                }
                else
                {
                    invalidSelection = true;
                }

                if (invalidSelection == true)
                {
                    resultInfo.BestDistance = float.MaxValue;
                    resultInfo.BestVertexIndex = -1;
                    resultInfo.BestSecondVertexIndex = -1;
                }
            }

            return resultInfo;
        }

        /// <summary>
        /// Processes this mesh's vertex list to find a closest match for the supplied vertex.
        /// </summary>
        /// <param name="vertex"></param>
        /// <returns></returns>
        public ResultInfo Process_VertexMatching(Vector3f vertex)
        {
            ResultInfo resultInfo = new ResultInfo();
            if (m_Vertices.Count > 0)
            {
                float closestDistance = float.MaxValue;
                int secondClosestIndex = -1;
                int closestIndex = FindClosestVertexIndex(vertex, TerrainProcessing.VertexMatching_DistanceThreshold, out closestDistance, out secondClosestIndex);

                if (closestIndex >= 0)
                {
                    resultInfo.BestDistance = closestDistance;
                    resultInfo.BestVertexIndex = m_Vertices[closestIndex].m_Index;

                    if (secondClosestIndex >= 0)
                        resultInfo.BestSecondVertexIndex = m_Vertices[secondClosestIndex].m_Index;
                }
            }

            return resultInfo;
        }
    }

    /// <summary>
    /// A class containing a collection of meshes.  3D Studio Max's equivalent of this structure
    /// is a Container.  
    /// </summary>
    public class ContainerInfo
    {       
        public string ContainerName;
        public Dictionary<string, ContainerMesh> m_MeshDictionary;

        public ContainerInfo(string name)
        {
            ContainerName = name;
            m_MeshDictionary = new Dictionary<string, ContainerMesh>();
        }

        /// <summary>
        /// Provides a ContainerMesh object if one already exists; creates one if not.
        /// </summary>
        /// <param name="meshName"></param>
        /// <returns></returns>
        public ContainerMesh GetMesh(string meshName)
        {
            if ( m_MeshDictionary.ContainsKey(meshName) == false )
                m_MeshDictionary.Add(meshName, new ContainerMesh());

            return m_MeshDictionary[meshName];
        }

        /// <summary>
        /// Iterates through this containers' meshes for a best match.
        /// </summary>
        /// <param name="vertex"></param>
        /// <returns></returns>
        public ResultInfo Process_EdgeNormalisation(Vector3f vertex)
        {
            ResultInfo resultInfo = new ResultInfo();

            foreach (KeyValuePair<string, ContainerMesh> keyValuePair in m_MeshDictionary)
            {
                if (TerrainProcessing.CurrentMeshName != null && String.Compare(keyValuePair.Key, TerrainProcessing.CurrentMeshName) == 0)
                    continue;

                ResultInfo currentInfo = keyValuePair.Value.Process_EdgeNormalisation(vertex);

                if (currentInfo.BestDistance < resultInfo.BestDistance)
                {
                    resultInfo.BestMesh = keyValuePair.Key;
                    resultInfo.BestDistance = currentInfo.BestDistance;
                    resultInfo.BestVertexIndex = currentInfo.BestVertexIndex;
                    resultInfo.BestSecondVertexIndex = currentInfo.BestSecondVertexIndex;
                }
            }

            return resultInfo;
        }

        /// <summary>
        /// Iterates through this containers'' meshes for a best match.
        /// </summary>
        /// <param name="vertex"></param>
        /// <returns></returns>
        public ResultInfo Process_VertexMatching(Vector3f vertex)
        {
            ResultInfo resultInfo = new ResultInfo();

            foreach (KeyValuePair<string, ContainerMesh> keyValuePair in m_MeshDictionary)
            {
                if (TerrainProcessing.CurrentMeshName != null && String.Compare(keyValuePair.Key, TerrainProcessing.CurrentMeshName) == 0)
                    continue;

                ResultInfo currentInfo = keyValuePair.Value.Process_VertexMatching(vertex);

                if (currentInfo.BestDistance < TerrainProcessing.VertexMatching_DistanceThreshold)
                {
                    resultInfo.BestMesh = keyValuePair.Key;
                    resultInfo.BestDistance = currentInfo.BestDistance;
                    resultInfo.BestVertexIndex = currentInfo.BestVertexIndex;
                    resultInfo.BestSecondVertexIndex = currentInfo.BestSecondVertexIndex;
                }
            }

            return resultInfo;
        }
    }

    public enum ProcessingMode
    {
        EdgeNormalisation,
        VertexMatching
    };

    /// <summary>
    /// The main class for processing edges, exposed and interfacable in 3D Studio Max.
    /// </summary>
    public class TerrainProcessing
    {
        public Dictionary<string, ContainerInfo> m_Info;

        public ContainerInfo m_CurrentTarget;
        public string m_CurrentTargetName;
        public List<ContainerInfo> m_TargetAdjacentList;
        public Dictionary<int, Vector3f> m_VertexList;
        public Dictionary<int, List<ResultInfo>> m_Results;

        public int m_MaxThreads;
        public List<Thread> m_ActiveThreads;

        private Dictionary<ProcessingMode, ParameterizedThreadStart> m_ProcessorDelegates;
        private ProcessingMode m_ProcessorMode;

        //Configuration Variables.
        static public float EdgeNormalisation_DistanceThreshold = 0.01f;
        static public float VertexMatching_DistanceThreshold = 0.01f;
        static public string CurrentMeshName = null;
        static public bool ProcessOnSelf = true;

        public TerrainProcessing()
        {
            m_CurrentTarget = null;
            m_Info = new Dictionary<string, ContainerInfo>();
            m_TargetAdjacentList = new List<ContainerInfo>();
            m_VertexList = new Dictionary<int, Vector3f>();
            m_Results = new Dictionary<int, List<ResultInfo>>();
            m_ActiveThreads = new List<Thread>();

            m_ProcessorMode = ProcessingMode.EdgeNormalisation;

            m_ProcessorDelegates = new Dictionary<ProcessingMode, ParameterizedThreadStart>();
            m_ProcessorDelegates.Add(ProcessingMode.EdgeNormalisation, new ParameterizedThreadStart(ProcessThread_EdgeNormalisation));
            m_ProcessorDelegates.Add(ProcessingMode.VertexMatching, new ParameterizedThreadStart(ProcessThread_VertexMatching));

            m_MaxThreads = Environment.ProcessorCount + 2; 
        }

        /// <summary>
        /// Specifies how the terrain processor will operate.
        /// </summary>
        /// <param name="mode"></param>
        public void SetMode(ProcessingMode mode)
        {
            m_ProcessorMode = mode;
        }

        /// <summary>
        /// Acquires information on the container if one exists; creates if one does not.
        /// </summary>
        /// <param name="containerName"></param>
        /// <returns></returns>
        public ContainerInfo GetInfo(string containerName)
        {
            if ( m_Info.ContainsKey(containerName) == false )
                m_Info.Add(containerName, new ContainerInfo(containerName));

            return m_Info[containerName];
        }

        /// <summary>
        /// Returns whether a container has been already added (used to prevent duplicate registrations).
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool IsRegistered(string name)
        {
            return m_Info.ContainsKey(name);
        }

        /// <summary>
        /// Clears all registered containers.  
        /// </summary>
        public void Clear()
        {
            m_Info.Clear();
        }

        /// <summary>
        /// Clears the vertex list to be used to iterate through the containers.
        /// </summary>
        public void ClearVertexList()
        {
            m_VertexList.Clear();
            m_Results.Clear();
        }

        /// <summary>
        /// Sets a target container to operate on.
        /// </summary>
        /// <param name="containerName"></param>
        /// <returns></returns>
        public bool SetTarget(string containerName)
        {
            m_TargetAdjacentList.Clear();
            ClearVertexList();
            m_CurrentTargetName = containerName;
            return true;
        }

        /// <summary>
        /// Adds an adjacent container information object to list to process.
        /// </summary>
        /// <param name="adjacentContainerName"></param>
        /// <returns></returns>
        public bool AddAdjacent(string adjacentContainerName)
        {
            if (m_Info.ContainsKey(adjacentContainerName) == false)
                m_Info.Add(adjacentContainerName, new ContainerInfo(adjacentContainerName));

            m_TargetAdjacentList.Add(m_Info[adjacentContainerName]);
            return true;
        }

        /// <summary>
        /// Adds a vertex with the given index and position.  Note that the vertex index is specific
        /// to a particular mesh.
        /// </summary>
        /// <param name="vertexIndex"></param>
        /// <param name="vertex"></param>
        public void AddVertex(int vertexIndex, Vector3f vertex)
        {
            m_VertexList.Add(vertexIndex, vertex);
        }

        /// <summary>
        /// Processes all containers' meshes' vertex lists for a best match.
        /// </summary>
        /// <param name="vertexObject"></param>
        public void ProcessThread_EdgeNormalisation(object vertexObject)
        {
            KeyValuePair<int, Vector3f> vertexInfo = (KeyValuePair<int, Vector3f>) vertexObject;
            List<ResultInfo> resultInfos = new List<ResultInfo>();
            ResultInfo firstResultInfo = new ResultInfo();
            resultInfos.Add(firstResultInfo);

            List<ContainerInfo> infos = new List<ContainerInfo>();
            infos.AddRange(m_TargetAdjacentList);
            if (ProcessOnSelf == true && m_CurrentTarget != null )
                infos.Add(m_CurrentTarget);            

            foreach (ContainerInfo info in infos)
            {
                ResultInfo currentInfo = info.Process_EdgeNormalisation(vertexInfo.Value);

                if ( currentInfo != null ) 
                {
                    if (currentInfo.BestDistance < resultInfos[0].BestDistance)
                    {
                        resultInfos.Clear();

                        firstResultInfo.BestContainer = info.ContainerName;
                        firstResultInfo.BestMesh = currentInfo.BestMesh;
                        firstResultInfo.BestDistance = currentInfo.BestDistance;
                        firstResultInfo.BestVertexIndex = currentInfo.BestVertexIndex;
						firstResultInfo.BestSecondVertexIndex = currentInfo.BestSecondVertexIndex;
                        resultInfos.Add(firstResultInfo);
                    }
                    else if (currentInfo.BestDistance != float.MaxValue && currentInfo.BestDistance == resultInfos[0].BestDistance)  //Then we have multiple vertices that need to be processed.
                    {
                        ResultInfo secondResultInfo = new ResultInfo();
                        secondResultInfo.BestContainer = info.ContainerName;
                        secondResultInfo.BestMesh = currentInfo.BestMesh;
                        secondResultInfo.BestDistance = currentInfo.BestDistance;
                        secondResultInfo.BestVertexIndex = currentInfo.BestVertexIndex;
						secondResultInfo.BestSecondVertexIndex = currentInfo.BestSecondVertexIndex;
                        resultInfos.Add(secondResultInfo);
                    }
                }
            }

            lock (m_Results)
            {
                m_Results.Add(vertexInfo.Key, resultInfos);
            }
        }

        /// <summary>
        /// Processes all containers' meshes' vertex lists for a best match.
        /// </summary>
        /// <param name="vertexObject"></param>
        public void ProcessThread_VertexMatching(object vertexObject)
        {
            KeyValuePair<int, Vector3f> vertexInfo = (KeyValuePair<int, Vector3f>)vertexObject;
            List<ResultInfo> resultInfos = new List<ResultInfo>();
            ResultInfo resultInfo = new ResultInfo();
            resultInfos.Add(resultInfo);  //Add a default result in case there are no actual matches returned.

            List<ContainerInfo> infos = new List<ContainerInfo>();
            infos.AddRange(m_TargetAdjacentList);
            if (ProcessOnSelf == true && m_CurrentTarget != null)
                infos.Add(m_CurrentTarget);     

            //For every vertex, find a corresponding vertex in an adjacent container.
            foreach (ContainerInfo info in infos)
            {
                ResultInfo currentInfo = info.Process_VertexMatching(vertexInfo.Value);

                if (currentInfo != null)
                {
                    if (currentInfo.BestDistance < TerrainProcessing.VertexMatching_DistanceThreshold)
                    {
                        resultInfos.Clear();

                        resultInfo.BestContainer = info.ContainerName;
                        resultInfo.BestMesh = currentInfo.BestMesh;
                        resultInfo.BestDistance = currentInfo.BestDistance;
                        resultInfo.BestVertexIndex = currentInfo.BestVertexIndex;
                        resultInfo.BestSecondVertexIndex = currentInfo.BestSecondVertexIndex;
                        resultInfos.Add(resultInfo);
                    }
                }
            }

            lock (m_Results)
            {
                m_Results.Add(vertexInfo.Key, resultInfos);
            }
        }
        /// <summary>
        /// Process all queued tasks by spawning multiple threads.
        /// </summary>
        public void Process(string meshName)
        {
            CurrentMeshName = meshName;

            if (m_CurrentTargetName != null && m_Info.ContainsKey(m_CurrentTargetName) == false)
                return;

            m_CurrentTarget = m_Info[m_CurrentTargetName];
            if (m_CurrentTarget == null)
                return;

            int currentIndex = 0;
            ParameterizedThreadStart threadDelegate = m_ProcessorDelegates[m_ProcessorMode];

            while (currentIndex < m_VertexList.Count)
            {
                //Remove any dead threads.
                for(int activeThreadIndex = 0; activeThreadIndex < m_ActiveThreads.Count; activeThreadIndex++) 
                {
                    Thread activeThread = m_ActiveThreads[activeThreadIndex];

                    if (activeThread.IsAlive == false)
                    {
                        m_ActiveThreads.RemoveAt(activeThreadIndex);
                        activeThreadIndex--;
                    }
                }

                //Execute any more threads.
                while (m_ActiveThreads.Count < m_MaxThreads)
                {
                    if (currentIndex >= m_VertexList.Count)
                        break;

                    Thread thread = new Thread(threadDelegate);
                    thread.Name = "Vertex Index " + currentIndex + " Processing Thread";
                    thread.Start(m_VertexList.ElementAt(currentIndex));

                    m_ActiveThreads.Add(thread);
                    currentIndex++;
                }

                Thread.Sleep(1);
            }

            foreach (Thread thread in m_ActiveThreads)
            {
                thread.Join();
            }   
        }

        /// <summary>
        /// Returns the results for a particular vertex index.
        /// </summary>
        /// <param name="vertexIndex"></param>
        /// <returns></returns>
        public ResultInfo[] GetResultInfo(int vertexIndex)
        {
            if (m_Results.ContainsKey(vertexIndex) == false)
                return null;

            return m_Results[vertexIndex].ToArray();
        }

#region Configuration Functions
        /// <summary>
        /// Sets the vertex matching distance threshold.
        /// </summary>
        /// <param name="distance"></param>
        public void SetVertexMatchingThreshold(float distance)
        {
            VertexMatching_DistanceThreshold = distance;
        }

        /// <summary>
        /// Sets the edge normalisation distance threshold.
        /// </summary>
        /// <param name="distance"></param>
        public void SetEdgeNormalisationThreshold(float distance)
        {
            EdgeNormalisation_DistanceThreshold = distance;
        }

        /// <summary>
        /// Determines whether a container will find a best match within itself.  
        /// Useful if two meshes need to be processes against each other in the same container.
        /// </summary>
        /// <param name="set"></param>
        public void SetProcessSourceContainer(bool set)
        {
            ProcessOnSelf = set;
        }
#endregion
    }
}
