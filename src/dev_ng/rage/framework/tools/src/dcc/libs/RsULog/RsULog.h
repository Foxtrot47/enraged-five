/// <summary>
/// Universal log file target.
/// </summary>
/// An instance of this object can be connected to an ILog instance by
/// connecting up the Log method handlers.  When log messages are then
/// generated they get written to the file.
/// 
/// https://devstar.rockstargames.com/wiki/index.php/Dev:Universal_Log_Format
/// 

#ifndef _RSULOG_
#define _RSULOG_

#pragma once
#include "atl/string.h"
#include "atl/map.h"
#include <stdio.h>
#include <libxml/parser.h>
#include <libxml/tree.h>

using namespace rage;

#define MAX_STRING_LENGTH 4096
typedef atMap<atString, xmlNodePtr> ContextMap;

class UniversalLogFile
{
public:

	struct sLogMessageEventArgs
	{
	public:
		/// <summary>
		/// Message string to be logged
		/// </summary>
		atString mMessage;

		/// <summary>
		/// Context string log message relates to (optional).
		/// </summary>
		atString mContext;

		/// <summary>
		/// Context string log message relates to (optional).
		/// </summary>
		atString mFileContext;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="sMessage">Message to be logged</param>
		sLogMessageEventArgs(atString message)
			: mMessage(message)
		{
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="sMessage">Message to be logged</param>
		sLogMessageEventArgs(atString message, atString fileContext)
			: mMessage(message)
			, mFileContext(fileContext)
		{
		}

		/// <summary>
		/// Constructor, with context string.
		/// </summary>
		/// <param name="sMessage"></param>
		/// <param name="sContext"></param>
		sLogMessageEventArgs(atString context, atString message, atString fileContext)
			: mMessage(message)
			, mContext(context)
			, mFileContext(fileContext)
		{
		}
	};

	struct sProfileMessageEventArgs
	{
	public:
		/// <summary>
		/// Context string log message relates to (optional).
		/// </summary>
		atString mContext;

		/// <summary>
		/// Constructor
		/// </summary>
		sProfileMessageEventArgs(){}

		/// <param name="context">context to be logged</param>
		sProfileMessageEventArgs(atString context)
			: mContext(context)
		{
		}
	};

private:

	atString mFilename;
	bool mImmediateFlush;
	bool mEnabled;
	bool mTimestamp;
	xmlDocPtr mDocument;
	xmlNodePtr mCurrentContext;
	ContextMap mContexts;
	bool mHasErrors;
	bool mHasWarnings;

	/// <summary>
	/// Default Univeral Log XML schema version.
	/// </summary>
	static atString DEFAULT_VERSION;

	/// <summary>
	/// Default XML version.
	/// </summary>
	static atString DEFAULT_XML_VERSION;

	/// <summary>
	/// Default Universal Log XML encoding.
	/// </summary>
	static atString DEFAULT_ENCODING;

	/// <summary>
	/// Default Univeral Log XML document root node name.
	/// </summary>
	static atString DEFAULT_ROOT_ELEM;

	/// <summary>
	/// The name for the process context attribute
	/// </summary>
	static atString CONTEXT_ELEM;

	/// <summary>
	/// The name for the file message context attribute
	/// </summary>
	static atString FILE_CONTEXT_ELEM;

	/// <summary>
	/// The name for the message context attribute
	/// </summary>
	static atString CONTEXT_NAME_ATTR;

	/// <summary>
	/// xpath query string for error elements
	/// </summary>
	static atString xPath_Error ;
	/// <summary>
	/// xpath query string for warning elements
	/// </summary>
	static atString xPath_Warning;


	/// Generic functions for message serialisation
	void Profile(atString elemName, sProfileMessageEventArgs e);
	void LogMessage(atString elemName, sLogMessageEventArgs e);

public:

	// (de)initialisation
	UniversalLogFile(atString filename, atString context, bool contextAppend = true, bool messageAppend = true);
	/// Simply closing the connection
	~UniversalLogFile()
	{
		Close();
	}
	void Init(atString filename, bool immediateFlush, bool append = true);
	bool IsInited()
	{
		return NULL!=mCurrentContext;
	}
	void SetContext(atString name, bool dumpContents=false);
	void Refresh();
	void Flush()
	{
		xmlSaveFormatFileEnc(mFilename, mDocument, "UTF-8", 1);
	}
	void Close()
	{
		Flush();
		if(mDocument)
		{
			xmlFreeDoc(mDocument);
			mDocument = NULL;
		}
	}
	bool HasValidDoc()
	{
		return NULL!=mDocument;
	}


	// Accessors
	void ImmediateFlush(bool doFlush){mImmediateFlush = doFlush;}
	bool ImmediateFlush(){ return mImmediateFlush;}
	atString Filename(){ return mFilename; }

	atString GetXmlErrors()
	{
		char errorBuffer[MAX_STRING_LENGTH] = {0};
		if(errorBuffer && xmlLastError.message)
			strcpy_s(errorBuffer, MAX_STRING_LENGTH, xmlLastError.message);
		if(xmlLastError.str1)
			strcat_s(errorBuffer, MAX_STRING_LENGTH, xmlLastError.str1);
		if(xmlLastError.str2)
			strcat_s(errorBuffer, MAX_STRING_LENGTH, xmlLastError.str2);
		if(xmlLastError.str3)
			strcat_s(errorBuffer, MAX_STRING_LENGTH, xmlLastError.str3);
		return atString(errorBuffer);
	}

	atString GetGlobalLogFile(atString localLogFilePath);

	bool HasErrorsOrWarnings()
	{
		return mHasErrors || mHasWarnings;
	}

	// Logging interface
	void LogMessage(sLogMessageEventArgs e)
	{
		LogMessage(atString("message"), e);
	}
	void LogWarning(sLogMessageEventArgs e)
	{
		LogMessage(atString("warning"), e);
	}
	void LogError(sLogMessageEventArgs e)
	{
		LogMessage(atString("error"), e);
	}
	void LogDebug(sLogMessageEventArgs e)
	{
		LogMessage(atString("debug"), e);
	}
	void ProfileStart(sProfileMessageEventArgs e)
	{
		Profile(atString("profile"), e);
	}
	void ProfileEnd(sProfileMessageEventArgs e = sProfileMessageEventArgs())
	{
		Profile(atString("profile_end"), e);
	}

	void ParseForFormerWarnings();
};

#endif //_RSULOG_