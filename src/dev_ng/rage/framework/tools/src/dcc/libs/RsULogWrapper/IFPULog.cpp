#include "ULogger.h"
#include "IFPULog.h"

#include <iostream>
#include <fstream>
#include <string>
using namespace std;

BOOL CopyLogFile(const char *srcPath, const char *dstPath)
{
	ofstream dst;
	dst.open(dstPath, ios::out|ios::trunc);
	if(!dst.is_open())
		return FALSE;

	ifstream src;
	src.open(srcPath);
	if(!src.is_open())
		return FALSE;

	string line;
	while ( src.good() )
	{
		getline (src,line);
		dst << line << endl;
	}
	src.close();
	dst.close();
	return TRUE;
}

bool FPULog::InitPath(const char* path, const char* context, bool append)
{
	if(!m_pUlogger)
		m_pUlogger = new UniversalLogHelper();

	Assert(m_pUlogger);

	bool success = m_pUlogger->InitPath(path, context, append);
	if(!success)
	{
		char copyBuffer[4096];
		strcpy_s(copyBuffer, 4096, path);
		strcat_s(copyBuffer, ".faulty");
		CopyLogFile(path, copyBuffer);
		char buffer[4096];
		sprintf_s(buffer, 4096, "try(gRsUlog.LogError (\"Native ULog: Error:%s\") files:\"%s\" )catch(print \"no universal log available.\")", m_pUlogger->GetXmlErrors().c_str(), copyBuffer);
		for(int k=0;k<strlen(buffer);k++)
			if(buffer[k]=='\n' || buffer[k]=='\r')
				buffer[k]=' ';
		FPValue fpv;
		ExecuteMAXScriptScript(buffer, FALSE, &fpv);
		return false;
	}
	return true;
}

void	FPULog::LogError(const char* msg, const char* context, const char* filecontext)
{
	Assert(m_pUlogger);
	m_pUlogger->LogError(msg, context, filecontext);
}
void	FPULog::LogWarning(const char* msg, const char* context, const char* filecontext)
{
	Assert(m_pUlogger);
	m_pUlogger->LogWarning(msg, context, filecontext);
}
void	FPULog::LogMessage(const char* msg, const char* context, const char* filecontext)
{
	Assert(m_pUlogger);
	m_pUlogger->LogMessage(msg, context, filecontext);
}
void	FPULog::LogDebug(const char* msg, const char* context, const char* filecontext)
{
	Assert(m_pUlogger);
	m_pUlogger->LogDebug(msg, context, filecontext);
}
void	FPULog::ProfileStart(const char* context)
{
	Assert(m_pUlogger);
	m_pUlogger->ProfileStart(context);
}
void	FPULog::ProfileEnd(const char* context)
{
	Assert(m_pUlogger);
	m_pUlogger->ProfileEnd(context);
}
void	FPULog::ImmediateFlush(bool flush)
{
	Assert(m_pUlogger);
	m_pUlogger->ImmediateFlush(flush);
}
void	FPULog::Flush()
{
	Assert(m_pUlogger);
	m_pUlogger->Flush();
}
void	FPULog::Refresh()
{
	Assert(m_pUlogger);
	m_pUlogger->Refresh();
}
void	FPULog::ShowDialog()
{
	Assert(m_pUlogger);
	m_pUlogger->ShowDialog();
}
