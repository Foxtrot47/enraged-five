#pragma unmanaged
#include "RsULogWrapperDesc.h"
#include "RsULogWrapper.h"

void* 
RsULogWrapperClassDesc::Create(BOOL loading = FALSE) 		
{
	if(loading)
		return NULL;
	return RsULogWrapper::GetInstance();
}

const TCHAR *	
RsULogWrapperClassDesc::ClassName() 			
{ 
	return GetString(IDS_CLASS_NAME); 
}

const TCHAR* 
RsULogWrapperClassDesc::Category() 				
{ 
	return GetString(IDS_CATEGORY); 
}

Class_ID 
RsULogWrapperClassDesc::ClassID() 						
{ 
	return RsULogWrapper_CLASS_ID; 
}

int 
RsULogWrapperClassDesc::NumActionTables()
{
	return 0;
}