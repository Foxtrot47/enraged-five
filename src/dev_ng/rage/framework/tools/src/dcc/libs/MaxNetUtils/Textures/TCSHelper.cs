﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.MaxUtils.Textures
{
    [Obsolete("Use RSG.Pipeline.Services.Platform.Texture.SpecificationFactory")]
    public static class TCSHelper
    {
        [Obsolete("Use RSG.Pipeline.Services.Platform.Texture.Specification.IsSpecification")]
        public static bool IsValidTCSFile(string tcsPathname)
        {
            try
            {
                XDocument document = XDocument.Load(tcsPathname);
                return (document.Root.Name == "rage__CTextureConversionSpecification");
            }
            catch (System.IO.FileNotFoundException)
            {
                return false;
            }
            catch (System.Xml.XmlException)
            {
                return false;
            }
        }

        public static bool HasResourceTexturesElement(string tcsPathname)
        {
            try
            {
                XDocument document = XDocument.Load(tcsPathname);
                if (document.Root.Name == "rage__CTextureConversionSpecification")
                {
                    XElement resourceTexturesElement = document.Root.Element("resourceTextures");
                    return (resourceTexturesElement != null);
                }

                return false;
            }
            catch (System.IO.FileNotFoundException)
            {
                return false;
            }
            catch (System.Xml.XmlException)
            {
                return false;
            }
        }

        public static bool HasTextureCompressionFormatElement(string tcsPathname)
        {
            try
            {
                XDocument document = XDocument.Load(tcsPathname);
                if (document.Root.Name == "rage__CTextureConversionSpecification")
                {
                    XElement resourceTexturesElement = document.Root.Element("resourceTextures");
                    if (resourceTexturesElement != null && resourceTexturesElement.Element("Item") != null)
                    {
                        XElement rootResourceTexturesElement = resourceTexturesElement.Element("Item");
                        return (rootResourceTexturesElement.Element("compressionFormat") != null);
                    }

                    return false;
                }

                return false;
            }
            catch (System.IO.FileNotFoundException)
            {
                return false;
            }
            catch (System.Xml.XmlException)
            {
                return false;
            }
        }

        public static string GetTextureCompressionFormat(string tcsPathname)
        {
            try
            {
                XDocument document = XDocument.Load(tcsPathname);
                if (document.Root.Name == "rage__CTextureConversionSpecification")
                {
                    XElement resourceTexturesElement = document.Root.Element("resourceTextures");
                    if (resourceTexturesElement.Element("Item") != null)
                    {
                        XElement rootResourceTexturesElement = resourceTexturesElement.Element("Item");
                        XElement compressionFormatElement = rootResourceTexturesElement.Element("compressionFormat");

                        if (compressionFormatElement != null)
                        {
                            return compressionFormatElement.Value;
                        }
                        
                        return "Unknown";
                    }

                    return "Unknown";
                }

                return "Unknown";
            }
            catch (System.IO.FileNotFoundException)
            {
                return "Unknown";
            }
            catch (System.Xml.XmlException)
            {
                return "Unknown";
            }
        }

        public static void GenerateResourceTexturesElement(string tcsPathname, string[] sourceTexturePathnames, string resourceTexturePathname)
        {
            XDocument document = XDocument.Load(tcsPathname);

            // Remove existing elements that we will replace
            XElement sourceTextureElement = document.Root.Element("sourceTexture");
            if (sourceTextureElement != null)
                sourceTextureElement.Remove();

            XElement resourceTexturesElement = document.Root.Element("resourceTextures");
            if (resourceTexturesElement != null)
                resourceTexturesElement.Remove();

            // Add the new data
            var newSourceTextureElementsQuery = from sourceTexturePathname in sourceTexturePathnames
                                                select new XElement("Item", new XAttribute("type", "rage__CTextureConversionSourceTexture"), 
                                                           new XElement("pathname", sourceTexturePathname));

            XElement newResourceTexturesElement =
                new XElement("resourceTextures",
                    new XElement("Item", new XAttribute("type", "rage__CTextureConversionResourceTexture"), 
                        new XElement("pathname", resourceTexturePathname),
                        new XElement("usage", "usage_Default"),
                        new XElement("sourceTextures", newSourceTextureElementsQuery.ToArray())));
            document.Root.Add(newResourceTexturesElement);

            document.Save(tcsPathname);
        }
    }
}
