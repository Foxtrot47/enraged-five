﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using RSG.Base.Configuration;
using RSG.Pipeline.Services;
using RSG.Base.Collections;
using RSG.Base.Logging;

namespace RSG.MaxUtils
{
    public static class Zip
    {
        /// <summary>
        /// Utility overwrite of above function for access from Maxscript to allow access without generics.
        /// </summary>
        /// <param name="branch">Branch.</param>
        /// <param name="filename"></param>
        /// <param name="files"></param>
        /// <param name="useFullPath"></param>
        /// <param name="callback"></param>
        /// <returns></returns>
        public static bool Patch(IBranch branch, String filename, String[] files, String[] destination)
        {
            if (!File.Exists(filename))
            {
                Log.StaticLog.Warning("File {0} does not exist, creating new.", filename);
                return RSG.Pipeline.Services.Zip.Create(branch, filename, files, false);
            }

            List<Pair<String, String>> file_list = new List<Pair<String, String>>();
            int fileDestIndex = 0;
            foreach (String fileEntry in files)
            {
                if (fileDestIndex >= destination.Length)
                {
                    Log.StaticLog.Error("file list and destination list not equal in length.");
                    return (false);
                }
                String dest = destination[fileDestIndex++];
                file_list.Add(new Pair<String, String>(fileEntry, dest));
            }
            return RSG.Pipeline.Services.Zip.Patch(branch, filename, file_list, false);
        }
    }
}
