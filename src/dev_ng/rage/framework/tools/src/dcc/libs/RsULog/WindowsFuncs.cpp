#pragma warning(disable: 4668) // '__midl' is not defined as a preprocessor macro, replacing with '0' for '#if/#elif'

#include "windows.h"
#include "WindowsFuncs.h"
#include <time.h>       /* time_t, struct tm, difftime, time, mktime */
#include <sys/timeb.h>

#include "strsafe.h"

#define SYSTEM_INFO_BUFFER_SIZE 4098

BOOL UserName(char *ret, int size)
{
	TCHAR buffer[SYSTEM_INFO_BUFFER_SIZE];
	int bufferlength = size;
	GetUserNameW((LPWSTR)buffer, (LPDWORD)&bufferlength);
	BOOL usedInvalidChar;
	BOOL succeess = WideCharToMultiByte(CP_ACP,WC_COMPOSITECHECK|WC_DEFAULTCHAR|WC_NO_BEST_FIT_CHARS,(LPWSTR)buffer,bufferlength,ret,size,"?",&usedInvalidChar);
	switch(GetLastError())
	{
	case ERROR_INSUFFICIENT_BUFFER:
		printf("A supplied buffer size was not large enough, or it was incorrectly set to NULL.");
		break;
	case ERROR_INVALID_FLAGS:
		printf("The values supplied for flags were not valid.");
		break;
	case ERROR_INVALID_PARAMETER:
		printf("Any of the parameter values was invalid.");
		break;
	case ERROR_NO_UNICODE_TRANSLATION:
		printf("Invalid Unicode was found in a string.");
		break;
	}
	return succeess;
}
BOOL SysTime(char * ret, int size)
{
	SYSTEMTIME st;//, lt;
	GetLocalTime(&st);
	char *months[] = 
	{
		"January","February","March","April","May","June","July","August","September","October","November","December"
	};
	if(st.wMonth<0 || st.wMonth>12)
		return FALSE;
	return sprintf_s(ret, size, "%02d %s %04d %02d:%02d:%02d.%03d",st.wDay ,months[st.wMonth-1], st.wYear, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
}
BOOL TicksSinceEpoch(char * ret, int size)
{
	/***
	 *** Need to write this out in http://msdn.microsoft.com/en-us/library/system.datetime.ticks.aspx
	 *** which are miliseconds * 10.000.
	 *** Getting seconds (* 1000), adding milliseconds and multiplying the lot by 10.000
	 ***/

	struct _timeb timebuffer;
	_ftime( &timebuffer );
	ctime( & ( timebuffer.time ) );

	int secondsSince70s = timebuffer.time;
	INT64 milliseconds = secondsSince70s;
	milliseconds *= 1000;
	milliseconds += timebuffer.millitm;

	INT64 ticksSince70s = milliseconds * 10000;
	INT64 ticksYearZeroTo70s = 621355968000000000L; // from max: (dotnetobject "System.DateTime" 1970 1 1).ticks 

	return sprintf_s(ret, size, "%I64d", ticksSince70s + ticksYearZeroTo70s);
}
BOOL MachineName(char *ret, int size)
{
	return 0==gethostname(ret, size);
}