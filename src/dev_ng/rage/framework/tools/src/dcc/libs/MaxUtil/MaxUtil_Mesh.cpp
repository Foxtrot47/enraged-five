//
// File:: MaxUtil_Mesh.cpp
// Description:: Mesh Max utility functions implementation.
//
// Author:: David Muir <david.muir@rockstarnorth.com>
// Date:: 10 March 2009
//

// Local headers
#include "MaxUtil_Mesh.h"

namespace MaxUtil
{

	Mesh* 
	GetMesh( Object *pObject )
	{
		TriObject *tri = NULL;
		Class_ID classID;

		classID = pObject->ClassID();

		if(classID == (Class_ID(EDITTRIOBJ_CLASS_ID, 0))) 
		{ 
			tri = (TriObject *) pObject;
		}
		else if(classID == (Class_ID(TRIOBJ_CLASS_ID,0)))
		{
			tri = (TriObject*)pObject;
		}
		else if(pObject->CanConvertToType(Class_ID(TRIOBJ_CLASS_ID,0)))
		{
			tri = (TriObject*)pObject->ConvertToType(0,Class_ID(TRIOBJ_CLASS_ID,0));
		}
		if(!tri)
		{
			return NULL;
		}

		Mesh& mesh = tri->GetMesh();

		return &mesh;
	}

	Mesh* 
	GetMesh( INode *pNode )
	{
		ObjectState os = pNode->EvalWorldState(GetCOREInterface()->GetTime());
		if(!os.obj)
			return NULL;
		return ( GetMesh(os.obj) );
	}

} // MaxUtil namespace
