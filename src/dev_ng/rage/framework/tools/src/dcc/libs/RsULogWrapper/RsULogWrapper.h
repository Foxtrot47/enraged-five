//**************************************************************************/
// Copyright (c) 1998-2007 Autodesk, Inc.
// All rights reserved.
// 
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Autodesk, Inc., and are
// protected by Federal copyright law. They may not be disclosed to third
// parties or copied or duplicated in any form, in whole or in part, without
// the prior written consent of Autodesk, Inc.
//**************************************************************************/
// DESCRIPTION: Includes for Plugins
// AUTHOR: 
//***************************************************************************/

#ifndef RSULOG_GUP_H
#define RSULOG_GUP_H

#pragma unmanaged
#define RsULogWrapper_CLASS_ID	Class_ID(0x74cf7b7f, 0x90880a98)

//#pragma unmanaged
#include "nativeInclude.h"
#include "RsULogWrapperDesc.h"
//#include "IFPULog.h"

extern TCHAR *GetString(int id);

extern HINSTANCE hInstance;


//////////////////////////////////////////////////////////////////////////
// implementation
//////////////////////////////////////////////////////////////////////////

class RsULogWrapper : public GUP, MaxSDK::Util::Noncopyable
	//, public IFPULog 
{
public:

	static HWND hParams;

	static RsULogWrapper*	GetInstance();
	static void				Destroy();
	ClassDesc2* GetClassDesc();

	// GUP Methods
	DWORD		Start			( );
	void		Stop			( );
	DWORD_PTR	Control			( DWORD parameter );

// 	BaseInterface* GetInterface(Interface_ID id) 
// 	{     
// 		if (id == FP_ULOG_INTERFACE)      
// 			return (IFPULog*)this;     
// 		else       
// 			return GUP::GetInterface(id);     
// 	}

// 	virtual bool Init(MCHAR* path, MCHAR* context);
// 	virtual void LogError(MCHAR* msg, MCHAR* context = "");
// 	virtual void LogWarning(MCHAR* msg, MCHAR* context = "");
// 	virtual void LogMessage(MCHAR* msg, MCHAR* context = "");
// 	virtual void LogDebug(MCHAR* msg, MCHAR* context = "");
// 	virtual void ShowDialog(){};

	// Loading/Saving
	IOResult Save(ISave *isave);
	IOResult Load(ILoad *iload);

	//Constructor/Destructor
	RsULogWrapper();
	~RsULogWrapper();
private:

	static RsULogWrapper *sInstance;

	RsULogWrapperClassDesc mRsUlogWrapperDesc;
};

#endif //RSULOG_GUP_H