#ifndef DISTANCEFIELD_H
#define DISTANCEFIELD_H

#include "devil\il.h"
#include "devil\ilu.h"
#include "atl/string.h"

#include "Grid.h"

static rage::atString *ilErrorCheck(char *label)
{
	ILenum Error;
	rage::atString *errorString = NULL;
	while ((Error = ilGetError()) != IL_NO_ERROR) 
	{
		if(!errorString) 
			errorString = new rage::atString("");
		char buffer[255];
		sprintf(buffer, "%s: %s (%d)\n", label, iluErrorString(Error), Error);
		(*errorString) += buffer;
	}
	return errorString;
}

class Image
{
public:

	Image() : m_imageId(ILuint(-1)), m_width(ILuint(-1)), m_height(ILuint(-1)), m_bytesPerPixel(ILuint(-1)), mp_data(NULL) {}
	
	~Image() 
	{ 
		if (m_imageId != -1)
			ilDeleteImages(1, &m_imageId); 
	}

	Image(const char *_fileName) 
	{
		Load(_fileName);
	}

	Image(ILuint _width, ILuint _height, ILuint type, ILubyte *data)
	{
		ilGenImages(1, &m_imageId);
		ilBindImage(m_imageId);

		ilTexImage(_width, _height, 1, 4, IL_RGBA, type, data);

		m_width = ilGetInteger(IL_IMAGE_WIDTH);
		m_height = ilGetInteger(IL_IMAGE_HEIGHT);
		m_bytesPerPixel = ilGetInteger(IL_IMAGE_BYTES_PER_PIXEL);

		mp_data = ilGetData();
	}

	rage::atString *Load(const char *fileName)
	{
		ilGenImages(1, &m_imageId);
		ilBindImage(m_imageId);
		
		ilLoadImage((const ILstring	)fileName);
		rage::atString *rt = ilErrorCheck("Loading image");

		m_width = ilGetInteger(IL_IMAGE_WIDTH);
		m_height = ilGetInteger(IL_IMAGE_HEIGHT);
		m_bytesPerPixel = ilGetInteger(IL_IMAGE_BYTES_PER_PIXEL);

		mp_data = ilGetData();
		return rt;
	}

	ILubyte* getPixel(ILuint x, ILuint y, ILuint mipLevel = 0)
	{
		int finalWidth = m_width / (1 << mipLevel);
		return (ILubyte*)mp_data + ((y * finalWidth) + x) * m_bytesPerPixel;
	}
	
	ILuint m_imageId;
	ILuint m_width, m_height;
	ILuint m_bytesPerPixel;
	ILubyte *mp_data;
};

class DistanceField
{
public:

	DistanceField();
	rage::atString *Compute(const char *inputFile, const char *inOutFile, const char *outputFile, unsigned int outputWidth, unsigned int outputHeight, double distScale = 96.0);
	void PopulateGrids(Image &inputImage);
	unsigned char FinalAveragedDF(const int x, const int y, Image &inOutImage, Image &destImage, const int mipLevel, const double distScale);

protected:

	Grid m_insideOut, m_outsideIn;
};


#endif