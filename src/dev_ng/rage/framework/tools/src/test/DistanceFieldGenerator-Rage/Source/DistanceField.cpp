#include "DistanceField.h"
#include <cmath>
#include <algorithm> 
#include <string> 

using namespace std;

DistanceField::DistanceField() {}

rage::atString *DistanceField::Compute(const char *inputFile, 
							const char *inOutFile, 
							const char *outputFile, 
							unsigned int outputWidth, 
							unsigned int outputHeight, 
							double distScale)
{
	ilSetInteger(IL_DXTC_FORMAT, IL_DXT5);   
	ilEnable(IL_FILE_OVERWRITE);

	// Load source image
	Image sourceImage;
	rage::atString *perror = sourceImage.Load(inputFile);
	if(perror)
	{
		return perror;
	}

	// If output width/height not specified, use the input image 
	if (outputWidth == -1 || outputHeight == -1)
	{
		outputWidth = sourceImage.m_width;
		outputHeight = sourceImage.m_height;
	}

	// Check to make sure output size isn't greather than input size
	if (outputWidth > sourceImage.m_width || outputHeight > sourceImage.m_height)
	{
		return new rage::atString("Destination width/height must be less than the input width/height");
	}

	// Load in/out image
	Image inOutImage;
	perror = inOutImage.Load(inOutFile);
	if(perror)
	{
		return perror;
	}

	// Clear grid
	m_insideOut.Init(inOutImage.m_width, inOutImage.m_height);
	m_outsideIn.Init(inOutImage.m_width, inOutImage.m_height);

	PopulateGrids(inOutImage);

	// Generate the SDF.
	m_insideOut.GenerateSDF();
	m_outsideIn.GenerateSDF();

	Image destImage(outputWidth, outputHeight, IL_UNSIGNED_BYTE, NULL);

	ilBindImage(destImage.m_imageId);

	/*
	// Render out the results (image + alpha).
	for(unsigned int y = 0; y < destImage.m_height; y++)
	{
		for (unsigned int x = 0; x < destImage.m_width; x++ )
		{
		    unsigned char alpha = DistanceField::FinalAveragedDF(x, y, inOutImage, destImage, 0, distScale);

			ILubyte *destPixel = destImage.getPixel(x, y);

			destPixel[0] = alpha;
			destPixel[1] = alpha;
			destPixel[2] = alpha;
			destPixel[3] = 255;
		}
	}

	ilSaveImage((ILstring)(outputFile + string(".tga")).c_str());
	ilErrorCheck("Save Image (.tga)");
	*/

	// Render out the results (image + alpha).
	const int scaleX = sourceImage.m_width  / destImage.m_width,
			  scaleY = sourceImage.m_height / destImage.m_height;

	for(unsigned int y = 0; y < destImage.m_height; y++)
	{
		for (unsigned int x = 0; x < destImage.m_width; x++ )
		{
			ILubyte *destPixel = destImage.getPixel(x, y);
			int color[3] = {0, 0, 0};

			for (int sy = 0; sy < scaleY; sy++)
			{
				for (int sx = 0; sx < scaleX; sx++)
				{
					const int nx = x * scaleX + sx,
							  ny = y * scaleY + sy;

					ILubyte *srcPixel = sourceImage.getPixel(nx, ny);
					
					color[0] += srcPixel[0];
					color[1] += srcPixel[1];
					color[2] += srcPixel[2];
				}
			}

			destPixel[0] = ILubyte(color[2] / (scaleX * scaleY));
			destPixel[1] = ILubyte(color[1] / (scaleX * scaleY));
			destPixel[2] = ILubyte(color[0] / (scaleX * scaleY));
			destPixel[3] = 255;
		}
	}

	ilBindImage(destImage.m_imageId);
	iluBuildMipmaps();

	// Generate optimal mip-map alpha per level
	int mipmaps = ilGetInteger(IL_NUM_MIPMAPS);
	for(int i = 0; i < mipmaps; i++)
	{
		// reset mipmap level to main image, otherwise the counter (i) is relative from the last mipmap level.
		ilBindImage(destImage.m_imageId);
		ilActiveMipmap(i);

		ILuint width =  ilGetInteger(IL_IMAGE_WIDTH),
			   height = ilGetInteger(IL_IMAGE_HEIGHT),
			   Bpp = ilGetInteger(IL_IMAGE_BYTES_PER_PIXEL);
	
		for(unsigned int y = 0; y < height; y++)
		{
			for (unsigned int x = 0; x < width; x++ )
			{
				unsigned char alpha = DistanceField::FinalAveragedDF(x, y, inOutImage, destImage, i, distScale);

				ILubyte *mipData = ilGetData();
				ILubyte *pixel = (ILubyte*)mipData + ((y * (width)) + x) * Bpp;

				pixel[3] = alpha;
			}
		}
	}

	ilBindImage(destImage.m_imageId);
	ilSaveImage((ILstring)outputFile);
	perror = ilErrorCheck("Save Image (.dds)");
	if(perror)
	{
		return perror;
	}

	return NULL;
}

void DistanceField::PopulateGrids(Image &inputImage)
{
	Point inside = { POINT_INSIDE, POINT_INSIDE };
	Point empty = { POINT_EMPTY, POINT_EMPTY };

	for(unsigned int y = 0; y < inputImage.m_height; y++)
	{
		for (unsigned int x = 0;x < inputImage.m_width; x++)
		{
			ILubyte *pixel = inputImage.getPixel(x, y);

			unsigned char inten = *pixel;

			// Points inside get marked with a dx/dy of zero.
			// Points outside get marked with an infinitely large distance.
			if (inten <= 128)
			{
				m_insideOut.Put(x, y, inside);
				m_outsideIn.Put(x, y, empty);
			} 
			else 
			{
				m_insideOut.Put(x, y, empty);
				m_outsideIn.Put(x, y, inside);
			}
		}
	}
}

unsigned char DistanceField::FinalAveragedDF(const int x, const int y, Image &inOutImage, Image &destImage, const int mipLevel, const double distScale)
{
	double minDist = 0.0;

	const int mipWidth = 1 << mipLevel;

	const int scaleX = (inOutImage.m_width  / (destImage.m_width / mipWidth)),
			  scaleY = (inOutImage.m_height / (destImage.m_height / mipWidth));

	for (int sy = 0; sy < scaleY; sy++)
	{
		for (int sx = 0; sx < scaleX; sx++)
		{
			const int nx = x * scaleX + sx,
				      ny = y * scaleY + sy;

			double dist1 = sqrt((double)m_insideOut.Get(nx, ny).DistSq());
			double dist2 = sqrt((double)m_outsideIn.Get(nx, ny).DistSq());

			double dist = dist1 - dist2;
			minDist += dist;
		}
	}

	minDist /= double(scaleX * scaleY);
	// Temporary fix to make mid-point 90 instead of 127 .. this will need to be passed in in the end
	int multiplier = (minDist > 0) ? 165 : 90;
	const int final = 90 + max(-90, min(165, int((minDist / distScale) * multiplier)));
	return (unsigned char)final;
}