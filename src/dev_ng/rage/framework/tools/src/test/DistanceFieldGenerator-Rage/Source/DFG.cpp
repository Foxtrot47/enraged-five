#include <iostream>
#include <sstream>

#include "Grid.h"
#include "DistanceField.h"

#include "devil/il.h"
#include "devil/ilu.h"

#include "system/timer.h"

using namespace std;
using namespace rage;

int main(int argc, char* args[])
{
	ilInit();
	iluInit();

	if (argc < 4)
	{
		Displayf("Minimum 3 parameters:");
		Displayf("1. Input texture");
		Displayf("2. Input hi-res in/out map");
		Displayf("3. Output file");
		Displayf("4. Distance scale (default 96)");
		Displayf("5. Output width (default = input width)");
		Displayf("6. Output height (default = input height)");
		Displayf("Example: 8ssedt \"input.dds\" \"inout.tga\" \"output.dds\" 96 128 128");
		exit(0);
	}

	// Read command-line arguments
	char inputFile[255], outputFile[255], inOutFile[255];
	int outputWidth = -1, outputHeight = -1;
	int distScale = 96;

	stringstream params;

	for (int i = 1; i < argc; i++)
		params << args[i] << " ";

	// Read all parameters
	params >> inputFile >> inOutFile >> outputFile;
	
	// Read the other optional parameters
	if (argc > 4)
	{
		if (argc < 6)	{ params >> distScale; }
		if (argc < 7)	{ params >> outputWidth; }
		if (argc < 8)	{ params >> outputHeight; }
	}

	// Generate distance field
	sysTimer timer;

	timer.Reset();
	DistanceField distanceField;
	distanceField.Compute(inputFile, inOutFile, outputFile, outputWidth, outputHeight, distScale);
	float timeTaken = timer.GetTime();
	Displayf("Conversion took: %f\n", timeTaken);

	ilShutDown();
	return 0;
}
