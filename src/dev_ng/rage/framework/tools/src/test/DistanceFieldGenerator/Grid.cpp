#include "Grid.h"
#include <xmmintrin.h>

Grid::Grid() {}

Grid::Grid(int _width, int _height) 
{
	Init(_width, _height);
}

Grid::~Grid() { _mm_free(points); }

void Grid::Init(int _width, int _height)
{
	buffered_width = _width + GUTTER*2;
	buffered_height = _height + GUTTER*2;
	width = _width;
	height = _height;

	// Points include the GUTTER to make filtering easier
	points = (Point*)_mm_malloc(sizeof(Point) * buffered_width * buffered_height, 16);

	for(int i = 0; i < buffered_width*buffered_height; i++)
	{
		points[i].dx = POINT_EMPTY;
		points[i].dy = POINT_EMPTY;
	}
}

// Functions
Point Grid::Get(const int x, const int y)
{
	return points[(y + GUTTER) * buffered_width + (x + GUTTER)];
}

void Grid::Put(const int x, const int y, const Point &p)
{
	points[(y + GUTTER) * buffered_width + (x + GUTTER)] = p;
}

void Grid::Compare(Point &p, const int x, const int y, const int offsetx, const int offsety)
{
	Point other = Get(x+offsetx, y+offsety);
	other.dx += offsetx;
	other.dy += offsety;

	if (other.DistSq() < p.DistSq())
		p = other;
}


void Grid::GenerateSDF()
{
	// Pass 0
	for (int y = 0; y < height; y++)
	{
		for (int x = 0; x < width; x++)
		{
			Point p = Get(x, y);
			Compare(p, x, y, -1,  0);
			Compare(p, x, y,  0, -1);
			Compare(p, x, y, -1, -1);
			Compare(p, x, y,  1, -1);
			Put(x, y, p);
		}

		for (int x = width; x >= 0; x--)
		{
			Point p = Get(x, y);
			Compare(p, x, y, 1, 0);
			Put(x, y, p);
		}
	}

	// Pass 1
	for (int y = height - 1 ; y >= 0; y--)
	{
		for (int x = width - 1; x >= 0; x--)
		{
			Point p = Get(x, y);
			Compare(p, x, y,  1,  0);
			Compare(p, x, y,  0,  1);
			Compare(p, x, y, -1,  1);
			Compare(p, x, y,  1,  1);
			Put(x, y, p);
		}

		for (int x = 0;x < width; x++)
		{
			Point p = Get(x, y);
			Compare(p, x, y, -1, 0);
			Put(x, y, p );
		}
	}
}