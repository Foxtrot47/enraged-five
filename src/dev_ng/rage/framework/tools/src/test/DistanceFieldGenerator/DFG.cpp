#include <iostream>
#include <sstream>

#include "Grid.h"
#include "DistanceField.h"

#include <IL/il.h>
#include <IL/ilu.h>

using namespace std;

int main( int argc, char* args[] )
{
	ilInit();
	iluInit();

	if (argc < 4)
	{
		cout << "Minimum 4 parameters:" << endl;
		cout << "1. Input texture" << endl;
		cout << "2. Input hi-res in/out map" << endl;
		cout << "3. Output file" << endl;
		cout << "4. Distance scale (default 96)" << endl;
		cout << "5. Output width (default = input width)" << endl;
		cout << "6. Output height (default = input height)" << endl;
		cout << "Example: 8ssedt \"input.dds\" \"inout.tga\" 96 \"output.dds\" 256 256" << endl;
		exit(0);
	}

	// Read command-line arguments
	char inputFile[255], outputFile[255], inOutFile[255];
	int outputWidth = -1, outputHeight = -1;
	double distScale = 96.0;

	stringstream params;

	for (int i = 1; i < argc; i++)
		params << args[i] << " ";

	// Read all parameters
	params >> inputFile >> inOutFile >> outputFile;
	
	// Read the other optional parameters
	if (argc > 4)
	{
		if (argc < 6)	{ params >> distScale; }
		if (argc < 7)	{ params >> outputWidth; }
		if (argc < 8)	{ params >> outputHeight; }
	}

	// Generate distance field
	DistanceField distanceField;
	distanceField.Compute(inputFile, inOutFile, outputFile, outputWidth, outputHeight, distScale);

	return 0;
}
