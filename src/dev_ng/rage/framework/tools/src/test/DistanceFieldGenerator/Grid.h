#ifndef GRID_H
#define GRID_H

#define POINT_EMPTY 9999
#define POINT_INSIDE 0

struct Point
{
	int dx, dy;
	int DistSq() const { return dx*dx + dy*dy; }
};

#define GUTTER 1

class Grid
{
public:

	// Constructors / Destructors 
	Grid();
	Grid(int _width, int _height);
	~Grid();

	// Functions
	void Init(int _width, int _height);
	Point Get(const int x, const int y);
	void Put(const int x, const int y, const Point &p);
	void Compare(Point &p, const int x, const int y, const int offsetx, const int offsety);
	void GenerateSDF();

protected:

	// Variables
	Point *points;
	int buffered_width, buffered_height,
		width, height;
};

#endif