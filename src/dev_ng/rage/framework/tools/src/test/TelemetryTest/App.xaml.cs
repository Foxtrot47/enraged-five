﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

using RSG.Model.Telemetry;

namespace TelemetryTest
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App(): base()
        {
            RSG.Model.Telemetry.WebService serv = new RSG.Model.Telemetry.WebService();
            //serv.CreateTicket();
            //serv.QueryByDate();
            serv.ProcessQueryByData();

            foreach (String bucket in TelemetryStats.Instance.Buckets)
            {
                Console.WriteLine("{0}", bucket);
            }
        }
    }
}
