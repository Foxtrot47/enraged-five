﻿using System;
using System.IO;
using System.Net;
using System.Web.Handlers;
using System.Web.Script.Serialization;

namespace RSG.Statistics.AspUtils
{
    public partial class TableauAuth : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string tableauUsername = Context.Request["TableauUsername"];
            string tableauUrl = Context.Request["TableauUrl"];

            if (String.IsNullOrEmpty(tableauUsername) || String.IsNullOrEmpty(tableauUrl))
                 return;

            getTicket(tableauUsername,  tableauUrl);
        }

        public void getTicket(string username, string url)
        {
            Context.Response.Clear();
            Context.Response.BufferOutput = true;
            Context.Response.ContentType = "application/json; charset=utf-8";

            JavaScriptSerializer serializer = new JavaScriptSerializer();

            try
            {
                var request = WebRequest.Create(url);
                request.ContentType = "application/x-www-form-urlencoded";
                request.Method = "POST";
                byte[] payload = System.Text.Encoding.ASCII.GetBytes(String.Format("username={0}", username));
                request.ContentLength = payload.Length;
                var stream = request.GetRequestStream();
                stream.Write(payload, 0, payload.Length);
                stream.Close();
                var response = request.GetResponse();
                if (response == null)
                    throw new Exception("Tableau server could not be reached");

                var reader = new StreamReader(response.GetResponseStream());
                var token = reader.ReadToEnd();

                Context.Response.Write(serializer.Serialize(new { authToken = token }));
            }
            catch (Exception ex)
            {
                //Logger.ErrorException(string.Format("Failed to get Tableau auth token for user {0}", username), ex);
                Context.Response.Write(serializer.Serialize(new { authToken = -1, message = ex.Message }));
            }

            Context.Response.End();
            Context.ApplicationInstance.CompleteRequest();
        }
    }

}