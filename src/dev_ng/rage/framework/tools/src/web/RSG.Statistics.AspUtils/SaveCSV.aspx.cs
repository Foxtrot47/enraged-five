﻿using System;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web;

namespace RSG.Statistics.AspUtils
{
  public partial class SaveCSV : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      Session["_bExportCSVCalled"] = "false";
      
      if (IsPostBack) 
      {
        string sData = Context.Request.Form["CsvData"];
        string sFilename = Context.Request.Form["CsvFilename"];

        if (String.IsNullOrEmpty(sData)) { return; }
        ExportCSV(sData, sFilename);
      }
    }

    public void ExportCSV(string sData, string sFilename)
    {
      Session["_bExportCSVCalled"] = "true";

      string fileName;
      if (sFilename != null)
          fileName = sFilename.ToString();
      else
         fileName = "Data.csv";
      
      Context.Response.Clear();
      Context.Response.BufferOutput = true;
      Context.Response.ContentType = "text/csv";
      Context.Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);

      //Context.Response.Write("Test1,Test2");
      JavaScriptSerializer serializer = new JavaScriptSerializer();
      TableData data = serializer.Deserialize<TableData>(sData);
      //write column header names
      int k = data.Columns.Length;
      for (int i = 0; i < k; ++i)
      {
        Context.Response.Write(data.Columns[i].ColumnNames);
        Context.Response.Write(Environment.NewLine);
      }

      //write data
      k = data.Rows.Length;
      for (int i = 0; i < k; ++i)
      {
        Context.Response.Write(data.Rows[i].row);
        Context.Response.Write(Environment.NewLine);
      }
      Context.ApplicationInstance.CompleteRequest();
      //using CompleteRequest() instead of context.Response.End 
      // for clean way to end Page request without exceptions (msdn recomended).
      //Then override of Render is required to prevent writing default page output (see below).
    }

    protected override void Render(HtmlTextWriter writer)
    {
      bool bExportCSVCalled = Convert.ToBoolean(Session["_bExportCSVCalled"]);
      if (bExportCSVCalled)
      {
        Context.Response.Flush();
        return; //stop the rest of the output from being written
      }
      base.Render(writer);
    }

  }

  public class columns
  {
    public string ColumnNames;
  }

  public class rows
  {
    public string row;
  }

  [Serializable]
  public class TableData
  {
    public columns[] Columns;
    public rows[] Rows;
  }
}
