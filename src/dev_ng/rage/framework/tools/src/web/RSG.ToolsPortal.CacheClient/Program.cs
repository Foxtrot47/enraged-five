﻿using System;
using System.Net;
using System.Text;
using System.Threading;
using System.Collections.Generic;

using RSG.Base.Configuration;
using RSG.Base.Configuration.Automation;
using RSG.Base.Configuration.Portal;

namespace RSG.ToolsPortal.CacheClient
{
    class Program
    {
        public static void Main(string[] args)
        {
            IConfig config = ConfigFactory.CreateConfig();
            IPortalConfig portalConfig = ConfigFactory.CreatePortalConfig(config.Project);
            IAutomationServicesConfig automationServicesConfig = ConfigFactory.CreateAutomationServicesConfig(config.Project);
            IEnumerable<IAutomationServiceConfig> automationServices = automationServicesConfig.GetAllServices();

            int c;
            do
            {
                c = 0;
                //Console.Clear();
                Console.WriteLine("\n-- Requesting Controllers to Cache: {0} --\n", DateTime.UtcNow);

                foreach (IAutomationServiceConfig automationService in automationServices)
                {
                    foreach (IAutomationServiceViewConfig automationServiceView in automationService.Views)
                    {
                        Uri uri = portalConfig.GetPortalAutomationMatrixUri(automationServiceView.WebAlias);
                        Console.WriteLine("{0}. {1}: {2}", ++c, automationServiceView.Name, uri.ToString());

                        // If it is enabled for static matrix view
                        if (automationServiceView.ViewMode == ViewMode.JobMatrixStatic)
                        {
                            try
                            {
                                if (automationServiceView.AllowAnonymous)
                                {
                                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                                    response.Close();

                                    Console.ForegroundColor = ConsoleColor.Green;
                                    //Console.WriteLine("Response: {0}", response.StatusCode);
                                    Console.WriteLine("   --> Request Sent Successfully");
                                }
                                else
                                {
                                    Console.ForegroundColor = ConsoleColor.Yellow;
                                    Console.WriteLine("   --> Warning: Service requires Authentication, Skipping ...");
                                }
                            }
                            catch (WebException we)
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("   --> Error: {0}", we.Message);
                            }
                        } // End of if (automationServiceView.ViewMode == ViewMode.JobMatrixStatic)
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine("   --> Warning: Service not configured for Static View, Skipping ...");
                        }
                        // Reset The Console's colour
                        Console.ResetColor();
                    }
                }

                if (portalConfig.UpdateInterval > 0)
                {
                    Console.WriteLine("\n-- End of Request Cycle. Next Update in {0} Seconds --\n", portalConfig.UpdateInterval/1000);
                    Thread.Sleep(portalConfig.UpdateInterval);
                }
            }
            while (portalConfig.UpdateInterval > 0);
        }
    }
}
