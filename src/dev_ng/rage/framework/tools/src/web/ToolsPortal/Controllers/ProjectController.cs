﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Collections;
using System.Net;
using System.Configuration;
using System.Text;
using System.IO.Compression;
using System.ServiceModel;

using RSG.Base.Configuration;
using RSG.Base.Configuration.Automation;
using RSG.Base.Configuration.Portal;

using RSG.Interop.Bugstar;
using RSG.Interop.Bugstar.Organisation;

using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Tasks;
using RSG.Pipeline.Automation.Common.Client;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Services.Messages;

using RSG.Model.Deployment.Game;
using RSG.Model.Deployment.Tools;

using RSG.ShortcutMenu.Clients;

using RSG.ToolsPortal.Classes;
using RSG.ToolsPortal.ViewModels;

namespace RSG.ToolsPortal.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class ProjectController : ApplicationController
    {
        #region Methods / Controllers
        /// <summary>
        /// GET: /Project/ 
        /// </summary>
        /// <param name="projectRouteName"></param>
        /// <returns></returns>
        public ActionResult Index(String projectRouteName)
        {
            // Get the Project Conf object via the passed url route name
            //PseudoProjectConfig projectConfig = WebUtils.GetProjectConfig(projectRouteName);
            IPortalConfig projectPortalConfig = WebUtils.GetProjectPortalConfig(projectRouteName);
            if (projectPortalConfig == null || !IsUserAuthorised(projectPortalConfig.BugstarConfigs, User.Identity.Name))
            {
                return HttpNotFound();
            }

            DeploymentWrapper deploymentWrapper = new DeploymentWrapper(projectPortalConfig);

            //Net to specify the Request's client machine
            String ClientMachine = System.Net.Dns.GetHostEntry(Request.UserHostAddress).HostName.Split('.')[0];

            return View(new DeploymentViewModel(projectPortalConfig, deploymentWrapper, ClientMachine));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectRouteName"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult SyncBuilds(String projectRouteName, String id)
        {
            // Get the Project Conf object via the passed url route name
            //PseudoProjectConfig projectConfig = WebUtils.GetProjectConfig(projectRouteName);
            IPortalConfig projectPortalConfig = WebUtils.GetProjectPortalConfig(projectRouteName);
            if (projectPortalConfig == null || !IsUserAuthorised(projectPortalConfig.BugstarConfigs, User.Identity.Name))
            {
                return HttpNotFound();
            }

            NetTcpBinding binding = new NetTcpBinding();
            EndpointAddress address = new EndpointAddress(
                String.Format(
                    ConfigurationManager.AppSettings["ShortcutMenuServiceFormat"],
                    Request.UserHostAddress
                )
            );
            ShortcutMenuClient shortcutMenu = new ShortcutMenuClient(binding, address);
            shortcutMenu.InvokeShortcutItem(new Guid(id));

            JsonResult jsonResult = Json(new { }, JsonRequestBehavior.AllowGet);

            return jsonResult;
        }

        #region Automation Controllers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectRouteName"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult AutomationStatus(String projectRouteName, String id = null)
        {
            // Get the Project Conf object via the passed url route name
            //PseudoProjectConfig projectConfig = WebUtils.GetProjectConfig(projectRouteName);
            IPortalConfig projectPortalConfig = WebUtils.GetProjectPortalConfig(projectRouteName);
            if (projectPortalConfig == null || !IsUserAuthorised(projectPortalConfig.BugstarConfigs, User.Identity.Name))
            {
                return HttpNotFound();
            }

            return (View(new BaseViewModel(projectPortalConfig)));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectRouteName"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult AutomationAllJobs(String projectRouteName)
        {
            // Get the Project Conf object via the passed url route name
            //PseudoProjectConfig projectConfig = WebUtils.GetProjectConfig(projectRouteName);
            IPortalConfig projectPortalConfig = WebUtils.GetProjectPortalConfig(projectRouteName);
            if (projectPortalConfig == null || !IsUserAuthorised(projectPortalConfig.BugstarConfigs, User.Identity.Name))
            {
                return HttpNotFound();
            }

            return (View(new BaseViewModel(projectPortalConfig)));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectRouteName"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult AutomationEvents(String projectRouteName, String id)
        {
            // Get the Project Conf object via the passed url route name
            //PseudoProjectConfig projectConfig = WebUtils.GetProjectConfig(projectRouteName);
            IPortalConfig projectPortalConfig = WebUtils.GetProjectPortalConfig(projectRouteName);
            // Get the AutomationServiceViewConfig object by passing the web alias id
            IAutomationServiceViewConfig automationServiceViewConfig = this.GetAutomationServiceViewConfig(projectPortalConfig, id);
            if (automationServiceViewConfig == null)
            {
                return HttpNotFound();
            }

            return View(new AutomationViewModel(projectPortalConfig, automationServiceViewConfig));
        }

        /// <summary>
        /// JobList View - Clients Table REST 
        /// </summary>
        /// <param name="projectRouteName"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Automation(String projectRouteName, String id = null, String JobId = null)
        {
            // Trick to keep the automation status controller under Automation without parameters
            if (id == null)
            {
                return RedirectToAction("AutomationStatus", new { projectRouteName = projectRouteName});
            }

            // Get the Project Conf object via the passed url route name
            //PseudoProjectConfig projectConfig = WebUtils.GetProjectConfig(projectRouteName);
            IPortalConfig projectPortalConfig = WebUtils.GetProjectPortalConfig(projectRouteName);
            // Get the AutomationServiceViewConfig object by passing the web alias id
            IAutomationServiceViewConfig automationServiceViewConfig = this.GetAutomationServiceViewConfig(projectPortalConfig, id);
            if (automationServiceViewConfig == null)
            {
                return RedirectToAction("Login", "Account", new { ReturnUrl = Request.Url.AbsolutePath });
            }

            return View(new AutomationViewModel(projectPortalConfig, automationServiceViewConfig, JobId));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectRouteName"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult AutomationGraphs(String projectRouteName, String id)
        {
            // Get the Project Conf object via the passed url route name
            //PseudoProjectConfig projectConfig = WebUtils.GetProjectConfig(projectRouteName);
            IPortalConfig projectPortalConfig = WebUtils.GetProjectPortalConfig(projectRouteName);
            // Get the AutomationServiceViewConfig object by passing the web alias id
            IAutomationServiceViewConfig automationServiceViewConfig = this.GetAutomationServiceViewConfig(projectPortalConfig, id);
            if (automationServiceViewConfig == null)
            {
                return HttpNotFound();
            }

            return View(new AutomationViewModel(projectPortalConfig, automationServiceViewConfig));
        }

        /// <summary>
        /// Automation Status REST Controller
        /// </summary>
        /// <param name="projectRouteName"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult AutomationMonitor(String projectRouteName, String id)
        {
            // Get the Project Conf object via the passed url route name
            //PseudoProjectConfig projectConfig = WebUtils.GetProjectConfig(projectRouteName);
            IPortalConfig projectPortalConfig = WebUtils.GetProjectPortalConfig(projectRouteName);
            // Get the AutomationServiceViewConfig object by passing the web alias id
            IAutomationServiceViewConfig automationServiceViewConfig = this.GetAutomationServiceViewConfig(projectPortalConfig, id);
            if (automationServiceViewConfig == null)
            {
                return HttpNotFound();
            }

            AutomationWrapper automationWrapper = new AutomationWrapper(automationServiceViewConfig);
            if (!automationWrapper.ServiceIsUp)
            {
                return HttpNotFound();
            }

            IEnumerable<TaskStatus> monitor = automationWrapper.AutomationConsumer.Monitor();
            IEnumerable<WorkerStatus> clients = automationWrapper.AutomationConsumer.Clients();

            return Json(new { Monitor = monitor, Clients = clients }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Automation Admin Operations
        /// </summary>
        /// <param name="projectRouteName"></param>
        /// <param name="id"></param>
        /// <param name="action"></param>
        /// <param name="job"></param>
        /// <returns></returns>
        public ActionResult AutomationAdmin(String projectRouteName, String id, String actionName, String jobID)
        {
            // Get the Project Conf object via the passed url route name
            //PseudoProjectConfig projectConfig = WebUtils.GetProjectConfig(projectRouteName);
            IPortalConfig projectPortalConfig = WebUtils.GetProjectPortalConfig(projectRouteName);
            // Get the AutomationServiceViewConfig object by passing the web alias id
            IAutomationServiceViewConfig automationServiceViewConfig = this.GetAutomationServiceViewConfig(projectPortalConfig, id);
            if (automationServiceViewConfig == null)
            {
                return HttpNotFound();
            }

            AutomationWrapper automationWrapper = new AutomationWrapper(automationServiceViewConfig);
            if (!automationWrapper.ServiceIsUp)
            {
                return HttpNotFound();
            }

            JobResponse actionResult;
            if (actionName == JobOp.Skip.ToString()) 
            {
                actionResult = automationWrapper.AutomationConsumer.SkipJob(new Guid(jobID));
            }
            else if (actionName == JobOp.Reprocess.ToString())
            {
                actionResult = automationWrapper.AutomationConsumer.ReprocessJob(new Guid(jobID));
            }
            else if (actionName == JobOp.Prioritise.ToString())
            {
                actionResult = automationWrapper.AutomationConsumer.PrioritiseJob(new Guid(jobID));
            }
            else 
            {
                return HttpNotFound();
            }

            return Json(actionResult, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectRouteName"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult AutomationMonitorJobs(String projectRouteName, String id)
        {
            // Get the Project Conf object via the passed url route name
            //PseudoProjectConfig projectConfig = WebUtils.GetProjectConfig(projectRouteName);
            IPortalConfig projectPortalConfig = WebUtils.GetProjectPortalConfig(projectRouteName);
            // Get the AutomationServiceViewConfig object by passing the web alias id
            IAutomationServiceViewConfig automationServiceViewConfig = this.GetAutomationServiceViewConfig(projectPortalConfig, id);
            if (automationServiceViewConfig == null)
            {
                return HttpNotFound();
            }

            AutomationWrapper automationWrapper = new AutomationWrapper(automationServiceViewConfig);
            IEnumerable<IJob> jobs = automationWrapper.GetAllJobsFromMonitor();

            return Json(jobs, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectRouteName"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [OutputCache(CacheProfile = "Cache90Secs")]
        [AllowAnonymous]
        public ActionResult AutomationMonitorGroupedJobs(String projectRouteName, String id)
        {
            // Get the Project Conf object via the passed url route name
            IPortalConfig projectPortalConfig = WebUtils.GetProjectPortalConfig(projectRouteName);
            // Get the AutomationServiceViewConfig object by passing the web alias id
            IAutomationServiceViewConfig automationServiceViewConfig = this.GetAutomationServiceViewConfig(projectPortalConfig, id);
            if (automationServiceViewConfig == null)
            {
                return HttpNotFound();
            }

            AutomationWrapper automationWrapper = new AutomationWrapper(automationServiceViewConfig);
            if (!automationWrapper.ServiceIsUp)
            {
                return HttpNotFound();
            }

            AutomationGroupedJobs automationGroupedJobs = automationWrapper.GetAllJobsGrouped();
            automationWrapper.StoreGroupedJobs(automationGroupedJobs);

            JsonResult jsonResult = Json(new { AutomationGroupedJobs = automationGroupedJobs }, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue; // otherwise it will fail for long serialisation

            return jsonResult;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectRouteName"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [OutputCache(CacheProfile = "Cache1Min")]
        [AllowAnonymous]
        public ActionResult AutomationMonitorGroupedJobsStatic(String projectRouteName, String id)
        {
            IPortalConfig projectPortalConfig = WebUtils.GetProjectPortalConfig(projectRouteName);
            // Get the AutomationServiceViewConfig object by passing the web alias id
            IAutomationServiceViewConfig automationServiceViewConfig = this.GetAutomationServiceViewConfig(projectPortalConfig, id);
            if (automationServiceViewConfig == null)
            {
                return HttpNotFound();
            }

            AutomationWrapper automationWrapper = new AutomationWrapper(automationServiceViewConfig);

            AutomationGroupedJobs automationGroupedJobs = automationWrapper.RetrieveGroupedJobs();
            if (automationGroupedJobs == null)
            {
                return HttpNotFound();
            }
            
            JsonResult jsonResult = Json(new { AutomationGroupedJobs = automationGroupedJobs}, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue; // Otherwise it will fail for long serialisation
            return jsonResult;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectRouteName"></param>
        /// <param name="id"></param>
        /// <param name="jobId"></param>
        /// <param name="ConsumerId"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult AutomationJobFullInfo(String projectRouteName, String id, String jobId)
        {
            // Get the Project Conf object via the passed url route name
            IPortalConfig projectPortalConfig = WebUtils.GetProjectPortalConfig(projectRouteName);
            // Get the AutomationServiceViewConfig object by passing the web alias id
            IAutomationServiceViewConfig automationServiceViewConfig = this.GetAutomationServiceViewConfig(projectPortalConfig, id);
            if (automationServiceViewConfig == null)
            {
                return HttpNotFound();
            }

            AutomationWrapper automationWrapper = new AutomationWrapper(automationServiceViewConfig);

            // if the job is not consumed the ConsumerId will be the same as the jobId
            IJob job = automationWrapper.GetJobFromMonitor(jobId);
            IJobResult jobResult = null;

            UlogClass ulogClass = new UlogClass(automationServiceViewConfig.Service.ServerHost.Host);
            IEnumerable<String> errors = ulogClass.GetErrors(job);

            return Json(new { Job = job, JobResults = jobResult, Errors = errors }, JsonRequestBehavior.AllowGet);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectRouteName"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult AutomationStatisticsPerHost(String projectRouteName, String id)
        {
            // Get the Project Conf object via the passed url route name
            //PseudoProjectConfig projectConfig = WebUtils.GetProjectConfig(projectRouteName);
            IPortalConfig projectPortalConfig = WebUtils.GetProjectPortalConfig(projectRouteName);
            // Get the AutomationServiceViewConfig object by passing the web alias id
            IAutomationServiceViewConfig automationServiceViewConfig = this.GetAutomationServiceViewConfig(projectPortalConfig, id);
            if (automationServiceViewConfig == null)
            {
                return HttpNotFound();
            }

            AutomationWrapper automationWrapper = new AutomationWrapper(automationServiceViewConfig);
            IEnumerable<AutomationHostStats> hosts = automationWrapper.GetServerStatsPerHost();

            JsonResult jsonResult = Json(hosts, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue; // otherwise it will fail for long serialisation

            return jsonResult;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectRouteName"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        //[HttpPost]
        public ActionResult AutomationStatisticsPerHostCSV(String projectRouteName, String id)
        {
            // Get the Project Conf object via the passed url route name
            //PseudoProjectConfig projectConfig = WebUtils.GetProjectConfig(projectRouteName);
            IPortalConfig projectPortalConfig = WebUtils.GetProjectPortalConfig(projectRouteName);
            // Get the AutomationServiceViewConfig object by passing the web alias id
            IAutomationServiceViewConfig automationServiceViewConfig = this.GetAutomationServiceViewConfig(projectPortalConfig, id);
            if (automationServiceViewConfig == null)
            {
                return HttpNotFound();
            }

            AutomationWrapper automationWrapper = new AutomationWrapper(automationServiceViewConfig);
            String csvString = automationWrapper.GetServerStatsPerHostCSV();

            String filename = id + "-GraphData-" + DateTime.UtcNow.ToString("u") + ".csv";
            return File(new System.Text.UTF8Encoding().GetBytes(csvString), "text/csv", filename);
        }
        #endregion // Automation Controllers

        #region Deployment Controllers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ProjectRouteName"></param>
        /// <returns></returns>
        public ActionResult GameBuildVersions(String projectRouteName)
        {
            // Get the Project Conf object via the passed url route name
            //PseudoProjectConfig projectConfig = WebUtils.GetProjectConfig(projectRouteName);
            IPortalConfig projectPortalConfig = WebUtils.GetProjectPortalConfig(projectRouteName);
            DeploymentWrapper deploymentWrapper = new DeploymentWrapper(projectPortalConfig);


            // Errors added from controllers that redirected here
            object message = string.Empty;
            if (TempData.TryGetValue("Error", out message))
            {
                ModelState.AddModelError("", message.ToString());
            }

            return View(new DeploymentViewModel(projectPortalConfig, deploymentWrapper));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectRouteName"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult GameBuildVersionDetails(String projectRouteName, String id)
        {
            // Get the Project Conf object via the passed url route name
            //PseudoProjectConfig projectConfig = WebUtils.GetProjectConfig(projectRouteName);
            IPortalConfig projectPortalConfig = WebUtils.GetProjectPortalConfig(projectRouteName);
            DeploymentWrapper deploymentWrapper = new DeploymentWrapper(projectPortalConfig);

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Tuple<String, String> versions = deploymentWrapper.GetSplitVersionsFromString(id);
            String majorVersion = versions.Item1;
            String minorVersion = versions.Item2;

            RSG.Model.Deployment.Game.GameBuildVersion gameBuildVersion =
                                                            deploymentWrapper.DeploymentConsumer.GetGameVersion(majorVersion, minorVersion);

            // Empty Object
            if (gameBuildVersion == null)
            {
                return RedirectToAction("GameBuildVersions", new { projectRouteName = projectRouteName });
            }

            return View(new DeploymentViewModel(projectPortalConfig, gameBuildVersion));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectRouteName"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GameBuildVersionCreate(String projectRouteName)
        {
            // Get the Project Conf object via the passed url route name
            //PseudoProjectConfig projectConfig = WebUtils.GetProjectConfig(projectRouteName);
            IPortalConfig projectPortalConfig = WebUtils.GetProjectPortalConfig(projectRouteName);
            DeploymentWrapper deploymentWrapper = new DeploymentWrapper(projectPortalConfig);

            RSG.Model.Deployment.Game.GameBuildVersion gameBuildVersion = new RSG.Model.Deployment.Game.GameBuildVersion();
            gameBuildVersion.Timestamp = DateTime.UtcNow;
            gameBuildVersion.MajorVersion = ++deploymentWrapper.AllGameBuildVersions.FirstOrDefault().MajorVersion;
            gameBuildVersion.MinorVersion = 0;

            return View(new DeploymentViewModel(projectPortalConfig, gameBuildVersion));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="DeploymentViewModel"></param>
        /// <param name="projectRouteName"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GameBuildVersionCreate(DeploymentViewModel DeploymentViewModel, String projectRouteName)
        {
            //PseudoProjectConfig projectConfig = WebUtils.GetProjectConfig(projectRouteName);
            IPortalConfig projectPortalConfig = WebUtils.GetProjectPortalConfig(projectRouteName);
            DeploymentViewModel.SelectedProject = projectPortalConfig;

            DeploymentWrapper deploymentWrapper = new DeploymentWrapper(projectPortalConfig);

            if (ModelState.IsValid)
            {
                try
                {
                    DeploymentViewModel.SelectedGameBuildVersion.Label = DeploymentViewModel.SelectedGameBuildVersion.Label ?? String.Empty;
                    deploymentWrapper.DeploymentConsumer.CreateGameVersion(DeploymentViewModel.SelectedGameBuildVersion);
                    return RedirectToAction("GameBuildVersionDetails",
                                            new
                                            {
                                                projectRouteName = projectRouteName,
                                                id = deploymentWrapper.GetGameBuildVersionURIString(DeploymentViewModel.SelectedGameBuildVersion)
                                            }
                         );
                }
                catch (Exception exp)
                {
                    ModelState.AddModelError("", exp.Message);
                }
            }
            return View(DeploymentViewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectRouteName"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GameBuildVersionEdit(String projectRouteName, String id)
        {
            // Get the Project Conf object via the passed url route name
            //PseudoProjectConfig projectConfig = WebUtils.GetProjectConfig(projectRouteName);
            IPortalConfig projectPortalConfig = WebUtils.GetProjectPortalConfig(projectRouteName);
            DeploymentWrapper deploymentWrapper = new DeploymentWrapper(projectPortalConfig);

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Tuple<String, String> versions = deploymentWrapper.GetSplitVersionsFromString(id);
            String majorVersion = versions.Item1;
            String minorVersion = versions.Item2;

            RSG.Model.Deployment.Game.GameBuildVersion gameBuildVersion = 
                                                            deploymentWrapper.DeploymentConsumer.GetGameVersion(majorVersion, minorVersion);

            // Empty Object
            if (gameBuildVersion == null)
            {
                return RedirectToAction("GameBuildVersions", new { projectRouteName = projectRouteName });
            }

            return View(new DeploymentViewModel(projectPortalConfig, gameBuildVersion));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="DeploymentViewModel"></param>
        /// <param name="projectRouteName"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GameBuildVersionEdit(DeploymentViewModel DeploymentViewModel, String projectRouteName)
        {
            //PseudoProjectConfig projectConfig = WebUtils.GetProjectConfig(projectRouteName);
            IPortalConfig projectPortalConfig = WebUtils.GetProjectPortalConfig(projectRouteName);
            DeploymentViewModel.SelectedProject = projectPortalConfig;

            DeploymentWrapper deploymentWrapper = new DeploymentWrapper(projectPortalConfig);

            if (ModelState.IsValid)
            {
                try
                {
                    DeploymentViewModel.SelectedGameBuildVersion.Label = DeploymentViewModel.SelectedGameBuildVersion.Label ?? String.Empty;
                    deploymentWrapper.DeploymentConsumer.UpdateGameVersion(DeploymentViewModel.SelectedGameBuildVersion);
                    return RedirectToAction("GameBuildVersionDetails",
                                            new { projectRouteName = projectRouteName,
                                                  id = deploymentWrapper.GetGameBuildVersionURIString(DeploymentViewModel.SelectedGameBuildVersion)
                                            }
                         );
                }
                catch (Exception exp)
                {
                    ModelState.AddModelError("", exp.Message);
                }
            }
            return View(DeploymentViewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectRouteName"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GameBuildVersionDelete(String projectRouteName, String id)
        {
            // Get the Project Conf object via the passed url route name
            //PseudoProjectConfig projectConfig = WebUtils.GetProjectConfig(projectRouteName);
            IPortalConfig projectPortalConfig = WebUtils.GetProjectPortalConfig(projectRouteName);
            DeploymentWrapper deploymentWrapper = new DeploymentWrapper(projectPortalConfig);

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Tuple<String, String> versions = deploymentWrapper.GetSplitVersionsFromString(id);
            String majorVersion = versions.Item1;
            String minorVersion = versions.Item2;

            try
            {
                GameBuildVersion gameBuildVersion = deploymentWrapper.DeploymentConsumer.GetGameVersion(majorVersion, minorVersion);
                deploymentWrapper.DeploymentConsumer.DeleteGameVersion(gameBuildVersion);
            }
            catch (Exception delexp) 
            {
                TempData.Add("Error", delexp.Message);
            }

            return RedirectToAction("GameBuildVersions", new { projectRouteName = projectRouteName });
        }
        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectRouteName"></param>
        /// <returns></returns>
        public ActionResult GameBuildMachineTracking(String projectRouteName)
        {
            // Get the Project Conf object via the passed url route name
            //PseudoProjectConfig projectConfig = WebUtils.GetProjectConfig(projectRouteName);
            IPortalConfig projectPortalConfig = WebUtils.GetProjectPortalConfig(projectRouteName);
            DeploymentWrapper deploymentWrapper = new DeploymentWrapper(projectPortalConfig);

            return View(new DeploymentViewModel(projectPortalConfig, deploymentWrapper));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectRouteName"></param>
        /// <returns></returns>
        public ActionResult ToolsVersions(String projectRouteName)
        {
            // Get the Project Conf object via the passed url route name
            //PseudoProjectConfig projectConfig = WebUtils.GetProjectConfig(projectRouteName);
            IPortalConfig projectPortalConfig = WebUtils.GetProjectPortalConfig(projectRouteName);
            DeploymentWrapper deploymentWrapper = new DeploymentWrapper(projectPortalConfig);

            return View(new DeploymentViewModel(projectPortalConfig, deploymentWrapper));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectRouteName"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult ToolsVersionDetails(String projectRouteName, String id)
        {
            // Get the Project Conf object via the passed url route name
            //PseudoProjectConfig projectConfig = WebUtils.GetProjectConfig(projectRouteName);
            IPortalConfig projectPortalConfig = WebUtils.GetProjectPortalConfig(projectRouteName);
            DeploymentWrapper deploymentWrapper = new DeploymentWrapper(projectPortalConfig);

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Tuple<String, String> versions = deploymentWrapper.GetSplitVersionsFromString(id);
            String majorVersion = versions.Item1;
            String minorVersion = versions.Item2;

            RSG.Model.Deployment.Tools.ToolsVersion toolsVersion = 
                                                        deploymentWrapper.DeploymentConsumer.GetToolVersion(majorVersion, minorVersion);

            // Empty Object
            if (toolsVersion == null)
            {
                return RedirectToAction("ToolsVersions", new { projectRouteName = projectRouteName });
            }

            return View(new DeploymentViewModel(projectPortalConfig, toolsVersion));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectRouteName"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ToolsVersionCreate(String projectRouteName)
        {
            // Get the Project Conf object via the passed url route name
            //PseudoProjectConfig projectConfig = WebUtils.GetProjectConfig(projectRouteName);
            IPortalConfig projectPortalConfig = WebUtils.GetProjectPortalConfig(projectRouteName);
            DeploymentWrapper deploymentWrapper = new DeploymentWrapper(projectPortalConfig);

            RSG.Model.Deployment.Tools.ToolsVersion toolsVersion = new RSG.Model.Deployment.Tools.ToolsVersion();
            toolsVersion.Timestamp = DateTime.UtcNow;
            toolsVersion.MajorVersion = ++deploymentWrapper.AllToolsVersions.FirstOrDefault().MajorVersion;
            toolsVersion.MinorVersion = 0;

            return View(new DeploymentViewModel(projectPortalConfig, toolsVersion));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="DeploymentViewModel"></param>
        /// <param name="projectRouteName"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ToolsVersionCreate(DeploymentViewModel DeploymentViewModel, String projectRouteName)
        {
            //PseudoProjectConfig projectConfig = WebUtils.GetProjectConfig(projectRouteName);
            IPortalConfig projectPortalConfig = WebUtils.GetProjectPortalConfig(projectRouteName);
            DeploymentViewModel.SelectedProject = projectPortalConfig;

            DeploymentWrapper deploymentWrapper = new DeploymentWrapper(projectPortalConfig);

            if (ModelState.IsValid)
            {
                try
                {
                    DeploymentViewModel.SelectedToolsVersion.AssetsUsed = DeploymentViewModel.SelectedToolsVersion.AssetsUsed ?? String.Empty;
                    deploymentWrapper.DeploymentConsumer.CreateToolsVersion(DeploymentViewModel.SelectedToolsVersion);
                    return RedirectToAction("ToolsVersionDetails",
                                            new
                                            {
                                                projectRouteName = projectRouteName,
                                                id = deploymentWrapper.GetToolsVersionURIString(DeploymentViewModel.SelectedToolsVersion)
                                            }
                         );
                }
                catch (Exception exp)
                {
                    ModelState.AddModelError("", exp.Message);
                }
            }
            return View(DeploymentViewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectRouteName"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ToolsVersionEdit(String projectRouteName, String id)
        {
            // Get the Project Conf object via the passed url route name
            //PseudoProjectConfig projectConfig = WebUtils.GetProjectConfig(projectRouteName);
            IPortalConfig projectPortalConfig = WebUtils.GetProjectPortalConfig(projectRouteName);
            DeploymentWrapper deploymentWrapper = new DeploymentWrapper(projectPortalConfig);

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Tuple<String, String> versions = deploymentWrapper.GetSplitVersionsFromString(id);
            String majorVersion = versions.Item1;
            String minorVersion = versions.Item2;

            RSG.Model.Deployment.Tools.ToolsVersion toolsVersion =
                                                        deploymentWrapper.DeploymentConsumer.GetToolVersion(majorVersion, minorVersion);

            // Empty Object
            if (toolsVersion == null)
            {
                return RedirectToAction("ToolsVersions", new { projectRouteName = projectRouteName });
            }

            return View(new DeploymentViewModel(projectPortalConfig, toolsVersion));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="DeploymentViewModel"></param>
        /// <param name="projectRouteName"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ToolsVersionEdit(DeploymentViewModel DeploymentViewModel, String projectRouteName)
        {
            //PseudoProjectConfig projectConfig = WebUtils.GetProjectConfig(projectRouteName);
            IPortalConfig projectPortalConfig = WebUtils.GetProjectPortalConfig(projectRouteName);
            DeploymentViewModel.SelectedProject = projectPortalConfig;

            DeploymentWrapper deploymentWrapper = new DeploymentWrapper(projectPortalConfig);

            if (ModelState.IsValid)
            {
                try
                {
                    DeploymentViewModel.SelectedToolsVersion.AssetsUsed = DeploymentViewModel.SelectedToolsVersion.AssetsUsed ?? String.Empty;
                    deploymentWrapper.DeploymentConsumer.UpdateToolsVersion(DeploymentViewModel.SelectedToolsVersion);
                    return RedirectToAction("ToolsVersionDetails",
                                            new
                                            {
                                                projectRouteName = projectRouteName,
                                                id = deploymentWrapper.GetToolsVersionURIString(DeploymentViewModel.SelectedToolsVersion)
                                            }
                         );
                }
                catch (Exception exp)
                {
                    ModelState.AddModelError("", exp.Message);
                }
            }
            return View(DeploymentViewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectRouteName"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ToolsVersionDelete(String projectRouteName, String id)
        {
            // Get the Project Conf object via the passed url route name
            //PseudoProjectConfig projectConfig = WebUtils.GetProjectConfig(projectRouteName);
            IPortalConfig projectPortalConfig = WebUtils.GetProjectPortalConfig(projectRouteName);
            DeploymentWrapper deploymentWrapper = new DeploymentWrapper(projectPortalConfig);

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Tuple<String, String> versions = deploymentWrapper.GetSplitVersionsFromString(id);
            String majorVersion = versions.Item1;
            String minorVersion = versions.Item2;

            try
            {
                RSG.Model.Deployment.Tools.ToolsVersion toolsVersion = 
                                                        deploymentWrapper.DeploymentConsumer.GetToolVersion(majorVersion, minorVersion);
                //deploymentWrapper.DeploymentConsumer.DeleteToolsVersion(toolsVersion);
            }
            catch (Exception delexp)
            {
                TempData.Add("Error", delexp.Message);
            }

            return RedirectToAction("ToolsVersions", new { projectRouteName = projectRouteName });
        }

        #endregion // Deployment Controllers

        #endregion // Methods / Controllers

        #region Private Methods
        /// <summary>
        /// Gets the AutomationServiceConfig of an Automation Server while checking for general / project permissions
        /// </summary>
        /// <param name="projectConfig"></param>
        /// <param name="webAlias"></param>
        /// <returns></returns>
        private IAutomationServiceViewConfig GetAutomationServiceViewConfig(IPortalConfig projectPortalConfig, String webAlias)
        {
            // Get the Automation Service View Conf Object via it's webAlias
            IAutomationServiceViewConfig automationServiceViewConfig = WebUtils.GetProjectAutomationServiceView(projectPortalConfig, webAlias);
            // Return null if objects are empty
            if ((projectPortalConfig == null) || (automationServiceViewConfig == null))
            {
                return null;
            }

            // When the user has to be authenticated / authorised
            if (!automationServiceViewConfig.AllowAnonymous)
            {
                if (!User.Identity.IsAuthenticated || !IsUserAuthorised(projectPortalConfig.BugstarConfigs, User.Identity.Name))
                {
                    return null;
                }
            }

            return automationServiceViewConfig;
        }

        /// <summary>
        /// Checks whether a user is authorised for a project
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        [Obsolete]
        private Boolean IsUserAuthorised(String projectId, String userName)
        {
            return ((projectId != null) && (Roles.RoleExists(projectId)) && Roles.IsUserInRole(userName, projectId));
        }

        /// <summary>
        /// Checks whether a user is authorised for a project's ids
        /// </summary>
        /// <param name="projectIds"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        private Boolean IsUserAuthorised(IEnumerable<IPortalBugstarConfig> projectIds, String userName)
        {
            Boolean isAuthorised = false;

            if (projectIds == null)
            {
                return isAuthorised;
            }

            foreach (IPortalBugstarConfig projectId in projectIds)
            {
                if (Roles.RoleExists(projectId.BugstarID) && Roles.IsUserInRole(userName, projectId.BugstarID))
                {
                    isAuthorised = true;
                    break;
                }
            }

            return isAuthorised;
        }

        #endregion //Private Methods

    }

} // RSG.ToolsPortal.Controllers namespace
