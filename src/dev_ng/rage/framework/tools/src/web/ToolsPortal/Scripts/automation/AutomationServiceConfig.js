﻿var automationConfig = {
    //reloadInterval: 30000, // 30 secs in millisecs
    reloadInterval: (60000*3), // 60*3 secs in millisecs
    tooltipAjaxDelay: 0,
    tooltipDelay: 1000,
    animationDuration: 2000,

    truncateChars: 18, //
    truncateDescChars: 40, //
    //fixedTableLeftWidth: 260, // need to match css sum +

    contentElementId: "content-body",
    notProcessedYear: 9999,
    
    /*
    prioritiseAdminPath: "/automation.svc/admin/prioritise",
    skipAdminPath: "/automation.svc/admin/skip",
    reprocessAdminPath: "/automation.svc/admin/reprocess",
    */

    /*
    monitorUrlPath: "/automation.svc/admin/monitor",
    clientsUrlPath: "/automation.svc/admin/clients",
    enumsPath: "/automation.svc/admin/EnumValues",
    startTaskPath: "/automation.svc/admin/starttask",
    stopTaskPath: "/automation.svc/admin/stoptask",

    jobStateEnumDataType: "RSG.Pipeline.Automation.Common.Jobs.JobState",
    jobPriorityEnumDataType: "RSG.Pipeline.Automation.Common.Jobs.JobPriority",
    taskStateEnumDataType: "RSG.Pipeline.Automation.Common.Tasks.TaskState",
    capabilityTypeEnumDataType: "RSG.Pipeline.Automation.Common.CapabilityType",
    clientStateEnumDataType: "RSG.Pipeline.Automation.Common.Client.WorkerState",
    */

    perforceUrlPrefix: "http://rsgedip4s1:8080/@md=d&cd=//&c=7FM@/",
    perforceUrlSuffix: "?ac=10",
    perforceSwarmUrl: "https://rsgedip4r1/changes/",

    usersPath: "/cache/users/",
    usersXml: "_users.xml",

    // Error codes enums
    // //rage/rdr3/dev/rage/framework/tools/src/Libs/RSG.Pipeline.Automation.Common/Jobs/JobSubmissionState.cs
    submissionStates: {
        "-1" : "Not Submitted",
        "-2" : "Revert Unchanged",
        "-3" : "No Files",
        "-4" : "Errored",
        "-5" : "Submit Disabled",
        "-6" : "Reverted",
        "-7" : "Integrated",
        "-8" : "Submit Disabled Due To Errors",
    },

    // Dict of the States that shouldn't have links to ulog viewer
    nonUlogStates: {
        "Pending": true,
        "Assigned": true,
        "Skipped": true,
        "SkippedConsumed": true,
    },
    // Dict of the States that should get the list of errors if any
    errorUlogStates: {
        "Errors": true,
        "Completed": true,
    },
    errorStateName: "Errors",
    completedStateName: "Completed",
    pendingStateName: "Pending",
    skippedStateName: "Skipped",
    assignedStateName: "Assigned",
    reassignedStateName: "Reassigned",
    consumeState: "SkippedConsumed",
    groupKeySeparator: " | ",

    titleSplitServiceTypes: {
        codebuilder: true,
        scriptbuilder: true,
    },
    toUnderBarSplitServiceTypes: {
        integrator: true,
    },

    // Name Mapping for Saving Up Space
    nameMappings: {
        navmeshgenerator: "navmesh generator",
        roadpathsgenerator: "roadpaths generator",
        ragebuilder: "rage builder",
        scriptcompiler: "script compiler",
        bankrelease: "bank rel",
        release: "rel",

        completed: "Success",
        errors: "Failed"
    },

    iconMappings: {
        completed: "state-icon-completed.png",
        errors: "state-icon-errors.png",
        pending: "state-icon-pending.png",
        assigned: "state-icon-assigned.png",
        skipped: "state-icon-skipped.png",
        ulogGet: "ulog-download.png"
    },

    // Job List View 
    jobFilters: [
        "Pending",
        "Assigned", 
        "Completed", 
        "Errors", 
        "Skipped",
        "SkippedConsumed",
        "Aborted", 
        "ClientError"
    ],

    jobActions: {
        "Prioritise": {
            disabled: true,
            //actionRestPath: "/automation.svc/admin/prioritise",
            actionName: "Prioritise",
        },
        "Skip": {
            disabled: false,
            //actionRestPath: "/automation.svc/admin/skip",
            actionName: "Skip",
        },
        "Reprocess": {
            disabled: false,
            //actionRestPath: "/automation.svc/admin/reprocess",
            actionName: "Reprocess",
        },
    },

    tooltipClass: "is-tooltip",
    /*
    jobTypes: {
        "JobModel": {
            "friendlyName": "Job",
            "className": "service-type-job",
        },
        "AssetBuilderJobModel": {
            "friendlyName": "AssetBuilder Job",
            "className": "service-type-assetbuilder",
        },
        "CodeBuilderJobModel": {
            "friendlyName": "CodeBuilder Job",
            "className": "service-type-codebuilder",
        },
        "ScriptBuilderJobModel": {
            "friendlyName": "ScriptBuilder Job",
            "className": "service-type-scriptbuilder",
        },
        "CommandRunnerJobModel": {
            "friendlyName": "CommandRunner Job",
            "className": "service-type-commandrunner",
        },
        "IntegratorJobModel": {
            "friendlyName": "Integrator Job",
            "className": "service-type-integrator",
        },
        "MapExportJobModel": {
            "friendlyName": "Map Export Job",
            "className": "service-type-mapnetworkexport",
        },
        "ReporterJobModel": {
            "friendlyName": "Reporter Job",
            "className": "service-type-reporter",
        },
        "BaseSmokeTestJobModel": {
            "friendlyName": "BaseSmokeTest Job",
            "className": "service-type-job",
        },
        "SmokeTesterMonitorOutputJobModel": {
            "friendlyName": "SmokeTesterMonitorOutput Job",
            "className": "service-type-job",
        },
        "FrameCaptureJobModel": {
            "friendlyName": "Frame Capture Job",
            "className": "service-type-framecapture",
        },
        "CutsceneRPFBuilderJobModel": {
            "friendlyName": "Cutscene RPF Builder Job",
            "className": "service-type-framecapture",
        },
    },

    triggerTypes: {
        "ChangelistTriggerModel": {
            "friendlyName": "Changelist Trigger",
            "className": "changelist-trigger-type",
        },
        "UserRequestTriggerModel": {
            "friendlyName": "User Request Trigger",
            "className": "user-request-trigger-type",
        },
        "ScheduleTriggerModel": {
            "friendlyName": "Schedule Trigger",
            "className": "time-trigger-type",
        },
        "JobChangesTriggerModel": {
            "friendlyName": "Job Changes Trigger",
            "className": "jobchanges-trigger-type",
        },
        "MultiTriggerModel": {
            "friendlyName": "MultiTrigger",
            "className": "multi-trigger-type",
        },
    },
    */

    jobTypes: {
        "Job": {
            "friendlyName": "Job",
            "className": "service-type-job",
        },
        "AssetBuilderJob": {
            "friendlyName": "AssetBuilder Job",
            "className": "service-type-assetbuilder",
        },
        "CodeBuilderJob": {
            "friendlyName": "CodeBuilder Job",
            "className": "service-type-codebuilder",
        },
        "CodeBuilder2Job": {
            "friendlyName": "CodeBuilder Job",
            "className": "service-type-codebuilder",
        },
        "ScriptBuilderJob": {
            "friendlyName": "ScriptBuilder Job",
            "className": "service-type-scriptbuilder",
        },
        "CommandRunnerJob": {
            "friendlyName": "CommandRunner Job",
            "className": "service-type-commandrunner",
        },
        "IntegratorJob": {
            "friendlyName": "Integrator Job",
            "className": "service-type-integrator",
        },
        "MapExportJob": {
            "friendlyName": "Map Export Job",
            "className": "service-type-mapnetworkexport",
        },
        "ReporterJob": {
            "friendlyName": "Reporter Job",
            "className": "service-type-reporter",
        },
        "BaseSmokeTestJob": {
            "friendlyName": "BaseSmokeTest Job",
            "className": "service-type-job",
        },
        "SmokeTesterMonitorOutputJob": {
            "friendlyName": "SmokeTesterMonitorOutput Job",
            "className": "service-type-job",
        },
        "FrameCaptureJob": {
            "friendlyName": "Frame Capture Job",
            "className": "service-type-framecapture",
        },
        "CutsceneRPFBuilderJob": {
            "friendlyName": "Cutscene RPF Builder Job",
            "className": "service-type-framecapture",
        },
    },

    triggerTypes: {
        "ChangelistTrigger": {
            "friendlyName": "Changelist Trigger",
            "className": "changelist-trigger-type",
        },
        "UserRequestTrigger": {
            "friendlyName": "User Request Trigger",
            "className": "user-request-trigger-type",
        },
        "ScheduleTrigger": {
            "friendlyName": "Schedule Trigger",
            "className": "time-trigger-type",
        },
        "JobChangesTrigger": {
            "friendlyName": "Job Changes Trigger",
            "className": "jobchanges-trigger-type",
        },
        "MultiTrigger": {
            "friendlyName": "MultiTrigger",
            "className": "multi-trigger-type",
        },
    },
    
};