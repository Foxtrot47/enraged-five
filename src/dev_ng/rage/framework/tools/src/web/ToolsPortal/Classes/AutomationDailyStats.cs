﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSG.ToolsPortal.Classes
{
    /// <summary>
    /// Automation Server Daily Stats Class
    /// </summary>
    public class AutomationDailyStats
    {
        #region Properties
        /// <summary>
        /// Date of the Stats
        /// </summary>
        public DateTime Date
        {
            get;
            private set;
        }
        
        /// <summary>
        /// Total number of jobs created
        /// </summary>
        public int CreatedJobs
        {
            get;
            set;
        }

        /// <summary>
        /// Total number of jobs completed / processed
        /// </summary>
        public int CompletedJobs
        {
            get;
            set;
        }

        /// <summary>
        /// Total number of jobs pending
        /// </summary>
        public int PendingJobs
        {
            get;
            set;
        }

        /// <summary>
        /// Total number of jobs assigned
        /// </summary>
        public int AssignedJobs
        {
            get;
            set;
        }

        /// <summary>
        /// Total number of jobs errored
        /// </summary>
        public int ErroredJobs
        {
            get;
            set;
        }

        /// <summary>
        /// Total number of jobs skipped
        /// </summary>
        public int SkippedJobs
        {
            get;
            set;
        }

        /// <summary>
        /// Total number of jobs skippedconsumed
        /// </summary>
        public int SkippedConsumedJobs
        {
            get;
            set;
        }

        /// <summary>
        /// Total number of jobs aborted
        /// </summary>
        public int AbortedJobs
        {
            get;
            set;
        }

        /// <summary>
        /// Total number of jobs client errored
        /// </summary>
        public int ClientErroredJobs
        {
            get;
            set;
        }

        /// <summary>
        /// Total Processing Time in milliseconds
        /// </summary>
        public Double TotalProcessingTime
        {
            get;
            set;
        }

        /// <summary>
        /// Total Lag Time (CompletedAt - CreatedAt) in milliseconds
        /// </summary>
        public Double TotalLagTime
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="date"></param>
        public AutomationDailyStats(DateTime date)
        {   
            this.Date = date;
            this.CreatedJobs = 0;
            this.CompletedJobs = 0;
            this.PendingJobs = 0;
            this.AssignedJobs = 0;
            this.ErroredJobs = 0;
            this.SkippedJobs = 0;
            this.SkippedConsumedJobs = 0;
            this.AbortedJobs = 0;
            this.ClientErroredJobs = 0;
            this.TotalProcessingTime = 0;
            this.TotalLagTime = 0;
        }
        #endregion // Constructor(s)
    }

} // RSG.ToolsPortal.Classes namespace