﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSG.ToolsPortal.Classes
{
    /// <summary>
    /// Automation Host Stats Class
    /// </summary>
    public class AutomationHostStats
    {
        #region Properties
        /// <summary>
        /// HostName of the Host
        /// </summary>
        public String HostName
        {
            get;
            private set;
        }

        /// <summary>
        /// IEnumerable of Daily Stats
        /// </summary>
        public IEnumerable<AutomationDailyStats> DailyStats
        {
            get;
            private set;
        }

        #endregion // Properties

        #region Constructor(s)
        public AutomationHostStats(String hostName, IEnumerable<AutomationDailyStats> dailyStats)
        {
            this.HostName = hostName;
            this.DailyStats = dailyStats;
        }
        #endregion // Constructor(s)
    }

} // RSG.ToolsPortal.Classes namespace