﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using RSG.Pipeline.Automation.Common.Jobs;
using RSG.SourceControl.Perforce;

namespace RSG.ToolsPortal.Classes
{
    public class SimplifiedJobModel
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public Guid ID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid ConsumedByJobID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public JobState State { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Integration Integration { get; set; }
        #endregion //Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public SimplifiedJobModel() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="jobModel"></param>
        public SimplifiedJobModel(IJob jobModel) 
        {
            this.ID = jobModel.ID;
            this.ConsumedByJobID = jobModel.ConsumedByJobID;
            this.State = jobModel.State;
            this.Integration = (jobModel is IntegratorJob) ? ((IntegratorJob) jobModel).Integration : null;
        }
        #endregion //Constructor(s)
    }
} //RSG.ToolsPortal.Classes namespace