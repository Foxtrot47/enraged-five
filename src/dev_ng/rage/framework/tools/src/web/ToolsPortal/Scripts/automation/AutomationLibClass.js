﻿/*
* Class AutomationLib
*/

var AutomationLib = function () {

    var _restProfiler,
        _jsRenderProfilerStart,
        _jsRenderProfilerEnd,
        _jsAjaxProfilerStart,
        _jsAjaxProfilerEnd;

    /*
    * Returns the respective JobListConf based on the automation service
    */
    function getJobListConf(serviceType) {
        var jobListConf;

        switch (serviceType) {
            case "Reporter":
                jobListConf = getReporterJobListConf();
                break;
            case "FrameCapture":
                jobListConf = getFrameCaptureJobListConf();
                break;
            case automationConfig.eventsServiceType:
                jobListConf = getEventsListConf();
                break;
            default:
                jobListConf = getDefaultJobListConf();
                break;
        }
        
        return jobListConf;
    }

    /*
    * Returns the default JobListConf
    */
    function getDefaultJobListConf() {
        
        var conf = {
            "columns":
                [
                    { "title": "<input title='Select / Deselect All Rows' type='checkbox' id='select-all-cb' />", "className": "" },
                    { "title": "Change list", "className": "data-chlist", "width": "90" },
                    { "title": "ID", "visible": false, "className": "data-id" },

                    { "title": "User", "className": "data-username", "width": "140" },
                    { "title": "Priority", "className": "centre" },

                    { "title": "Types", "className": "centre" },
                    { "title": "Triggered At", "className": "centre", "width": "120", "type": "date" },
                    { "title": "Completed At", "className": "centre", "width": "120", "type": "date" },

                    { "title": "Turnaround Time", "className": "centre" },
                    { "title": "Processing Time", "className": "centre" },
                    { "title": "Processing Host", "className": "centre" },

                    { "title": "State", "className": "data-state", "width": "120" },
                    { "title": "Submitted Changelists", "className": "" },
                    { "title": "Action", "className": "data-view" },
                ],
            "getValues": function (job, jobResults, tasks, automationServiceObj) {
                var state = (job.State) ? job.State : "";

                job.Trigger.TriggeredAt = (job.Trigger.TriggeredAt) ? job.Trigger.TriggeredAt
                                                                    : job.Trigger.SubmittedAt;
                var triggeredAtDate = parseJsonDate(job.Trigger.TriggeredAt);
                var processedAtDate = parseJsonDate(job.ProcessedAt);
                var completedAtDate = parseJsonDate(job.CompletedAt);

                return [
                    "<input type='checkbox' value='" + job.ID + "' title='Select / Deselect Row' class='select-row-cb'>",
                    getChangelistLink(job.Trigger.Changelist),
                    job.ID,

                    (job.Trigger.Username != undefined) ? job.Trigger.Username : "",
                    (job.Priority != undefined) ? job.Priority : "",
                    constructJobComponentsHtml(job),
                    getDatetimeString(parseJsonDate(job.Trigger.TriggeredAt), utc),
                    getDatetimeString(completedAtDate, utc),

                    formatSecs(datesDiffInSecs(completedAtDate, triggeredAtDate)),
                    formatSecs(datesDiffInSecs(completedAtDate, processedAtDate)),
                    ((job.ProcessingHost) ? "<span title='" + job.Server + "'>" + job.ProcessingHost + "</span>" : ""),
                    getStateLink(job, automationServiceObj),
                    ((jobResults.hasOwnProperty(job.ID)) ? getSubmittedCLtext(jobResults[job.ID]) : ""),
                     "<button title='View Job Details'>View</button>",
                ]
            },
        };

        return conf;
    }

    /*
    * Returns the Reporter's custom JobListConf
    */
    function getReporterJobListConf() {

        var conf = {
            "columns":
                [
                    { "title": "<input title='Select / Deselect All Rows' type='checkbox' id='select-all-cb' />", "className": "" },
                    { "title": "Report Name", "className": "centre data-chlist", "width": "90" },

                    { "title": "ID", "visible": false, "className": "data-id" },

                    { "title": "Priority", "className": "centre" },

                    { "title": "Types", "className": "centre" },
                    { "title": "Triggered At", "className": "centre", "width": "120", "type": "date" },
                    { "title": "Completed At", "className": "centre", "width": "120", "type": "date" },

                    { "title": "Turnaround Time", "className": "centre" },
                    { "title": "Processing Time", "className": "centre" },

                    { "title": "Notifications", "className": "centre data-notifications" },

                    { "title": "State", "className": "data-state", "width": "120" },
                    
                    { "title": "Action", "className": "data-view" },
                ],
            "getValues": function (job, jobResults, tasks, automationServiceObj) {
                job.Trigger.TriggeredAt = (job.Trigger.TriggeredAt) ? job.Trigger.TriggeredAt
                                                                    : job.Trigger.SubmittedAt;
                var completedAtDate = parseJsonDate(job.CompletedAt);

                return [
                    "<input type='checkbox' value='" + job.ID + "' title='Select / Deselect Row' class='select-row-cb'>",
                    (job.ReportName) ? job.ReportName : "",

                    job.ID,

                    (job.Priority != undefined) ? job.Priority : "",
                    constructJobComponentsHtml(job),
                    getDatetimeString(parseJsonDate(job.Trigger.TriggeredAt), utc),
                    getDatetimeString(completedAtDate, utc),

                    formatSecs(datesDiffInSecs(completedAtDate, parseJsonDate(job.Trigger.TriggeredAt))),
                    formatSecs(datesDiffInSecs(completedAtDate, parseJsonDate(job.ProcessedAt))),

                    ((jobResults.hasOwnProperty(job.ID) && jobResults[job.ID].hasOwnProperty("Notifications"))
                                                ? jobResults[job.ID].Notifications.length : ""),

                    getStateLink(job, automationServiceObj),
                    
                    "<button title='View Job Details'>View</button>",
                ]
            },
        };

        return conf;
    }

    /*
   * Returns the FrameCapture's custom JobListConf
   */
    function getFrameCaptureJobListConf() {

        var conf = {
            "columns":
                [
                    { "title": "<input title='Select / Deselect All Rows' type='checkbox' id='select-all-cb' />", "className": "" },
                    { "title": "Change list", "className": "data-chlist"},
                    { "title": "ID", "visible": false, "className": "data-id" },

                    { "title": "User", "className": "data-username", "width": "140" },

                    { "title": "Types", "className": "centre" },
                    { "title": "Triggered At", "className": "centre", "width": "120", "type": "date" },
                    { "title": "Completed At", "className": "centre", "width": "120", "type": "date" },

                    { "title": "Turnaround Time", "className": "centre" },
                    { "title": "Process Type", "className": "centre" },
                    { "title": "Processing Host" },
                    { "title": "Task Role" },
                    { "title": "FBX File" },

                    { "title": "State", "className": "data-state", "width": "120" },
                    { "title": "Submitted Change Lists"},
                    { "title": "Action", "className": "data-view" },
                ],
            "getValues": function (job, jobResults, tasks, automationServiceObj) {
                var state = (job.State) ? job.State : "";

                job.Trigger.TriggeredAt = (job.Trigger.TriggeredAt) ? job.Trigger.TriggeredAt
                                                                    : job.Trigger.SubmittedAt;
                var triggeredAtDate = parseJsonDate(job.Trigger.TriggeredAt);
                var processedAtDate = parseJsonDate(job.ProcessedAt);
                var completedAtDate = parseJsonDate(job.CompletedAt);

                return [
                    "<input type='checkbox' value='" + job.ID + "' title='Select / Deselect Row' class='select-row-cb'>",
                    getChangelistLink(job.Trigger.Changelist),
                    job.ID,

                    (job.Trigger.Username != undefined) ? job.Trigger.Username : "",
                    constructJobComponentsHtml(job),
                    getDatetimeString(parseJsonDate(job.Trigger.TriggeredAt), utc),
                    getDatetimeString(completedAtDate, utc),

                    formatSecs(datesDiffInSecs(completedAtDate, triggeredAtDate)),
                    formatSecs(datesDiffInSecs(completedAtDate, processedAtDate)),
                    ((job.ProcessingHost) ? "<span title='" + job.Server + "'>" + job.ProcessingHost + "</span>" : ""),
                    ((job.TaskID && tasks.hasOwnProperty(job.TaskID) && tasks[job.TaskID].Role)
                                                                    ?
                                                                    getLastArrayElement(splitUpperCasedWord(tasks[job.TaskID].Role))
                                                                    : ""),
                    extractFbxFilename(job),
                    getStateLink(job, automationServiceObj),
                    ((jobResults.hasOwnProperty(job.ID)) ? getSubmittedCLtext(jobResults[job.ID]) : ""),
                     "<button title='View Job Details'>View</button>",
                ]
            },
        };

        return conf;
    }

    /*
    * Returns the Events custom JobListConf
    */
    function getEventsListConf() {

        var conf = {
            "columns":
                [
                    { "title": "ID", "className": "data-chlist" },
                    { "title": "Event Type", "className": "centre" },
                    { "title": "hiddenID", "visible": false, "className": "data-id" },

                    { "title": "Occurred At", "className": "centre", "type": "date" },                    
                    { "title": "Host", "className": "centre" },
                    { "title": "Context", "className": "centre" },
                ],
            "getValues": function (job, jobResults, automationServiceObj) {
                return [
                    job.ID,
                    (job.EventType) ? job.EventType : "",
                    job.ID, // Hidden one
                    
                    getDatetimeString(parseJsonDate(job.OccurredAt), utc),
                    (job.Host) ? job.Host : "",
                    (job.Context) ? job.Context : "",
                ]
            },
        };

        return conf;
    }

    /**
    **
    **/
    function getSubmittedCLtext(jobResult) {
        return getChangelistLink(jobResult.SubmittedChangelistNumber);
    }

    /*
    *
    */
    function getStateLink(job, automationServiceObj) {
        var state = (job.State) ? job.State : "";

        var link = $("<div>").append(
            (automationConfig.nonUlogStates.hasOwnProperty(state))
                    ? $("<div>").addClass("state-text").text(state)
                    : $("<div>").addClass("relative")
                        .append(
                            $("<a>")
                                .addClass("state-text")
                                .attr("href", automationServiceObj.ulogHost + "?"
                                        + $.param({
                                            "ServerHost": job.Server,
                                            "JobId": job.ID,
                                            "ErrorsOnly": ((state == automationConfig.errorStateName) ? true : false),
                                        })
                                )
                                .attr("target", "_blank")
                                .attr("title", "View Log")
                                .text(state)
                        )
                        .append(
                             $("<a>")
                                .attr("href", automationServiceObj.ulogGetHost + "?"
                                        + $.param({
                                            "ServerHost": job.Server, "JobId": job.ID,
                                        })
                                )
                                .append(
                                    $("<img>")
                                        .addClass("download-ulog")
                                        .attr("src", automationServiceObj.imagesRootURI
                                                    + "/"
                                                    + automationConfig.iconMappings["ulogGet"]
                                        )
                                        .attr("title", "Download Log")
                                )
                        )
        ).html();

        return link;
    }
    
    /*
    * Builds html code with links to Perforce Web and Swarm for any given changelist
    */
    function getChangelistLink(changelist) {
        var html;

        if ((changelist) && (changelist != "undefined")) {
            if (automationConfig.submissionStates.hasOwnProperty(changelist)) {
                html = automationConfig.submissionStates[changelist];
            }
            else {
                html = $("<span>")
                    .append(
                        $("<a>")
                            .attr("title", "View Changelist on Perforce Swarm")
                            .attr("href", automationConfig.perforceSwarmUrl + changelist)
                            .attr("target", "_blank")
                            .text(changelist)
                    )
                    //.append(
                    //    $("<a>")
                    //        .attr("title", "View Changelist on Perforce Web")
                    //        .attr("href", automationConfig.perforceUrlPrefix + changelist + automationConfig.perforceUrlSuffix)
                    //        .attr("target", "_blank")
                    //        .text(" [W]")
                    //)
                    .html();
            }
        }
        else {
            html = "-";
        }

        return html;
    }

    /*
    * Adds html links to bugstar type url's
    */
    function createBugstarLinks(text) {
        // Search for only one bugstar url as there is one bug per changelist
        if (!text)
            return "";

        var bugstarUrlPattern = /url:bugstar:\d+/i;
        var matches = text.match(bugstarUrlPattern);

        if (matches) {
            var link = $("<a />", {
                text: matches[0],
                title: "Open in Bugstar client",
                href: matches[0].replace("url:", ""), // Remove the "url:" prefix
                //target: "_blank",
            })
            .prop("outerHTML");

            text = text.replace(matches[0], link)
        }

        return text;
    }

    /*
    * Retrieves and pases the user info xml file via a synchronous ajax request.
    * Return a dictionary of the results where key is the username
    */
    function processUsersInfo() {
        var usersInfo = {};

        $.ajax({
            url: automationConfig.usersPath + automationConfig.usersXml,
            type: "GET",
            dataType: "xml",
            async: false,

            success: function (xml, textStatus, jqXHR) {

                $(xml).find("User").each(function () {

                    usersInfo[$(this).find("UserName").text().toLowerCase()] = {
                        Name: $(this).find("Name").text(),
                        Email: $(this).find("Email").text(),
                        JobTitle: $(this).find("JobTitle").text(),
                        ImageFilename: $(this).find("ImageFilename").text(),
                    }

                });

            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.error("Failed loading content");
            },
            complete: function () {
                
            }
        });

        return usersInfo;
    }

    /*
    * Updates the custom tooltip's position so it's always visible between the browser's viewport
    */
    function updateTooltipPosition(element, event) {
        var ttw = element.width(),
            tth = element.height(),

            offset = {
                x: 10,
                y: 0,
            },

            xScroll = $(window).scrollLeft(),
            yScroll = $(window).scrollTop(),

            curX = (document.all) ? event.clientX + xScroll : event.pageX,
            curY = (document.all) ? event.clientY + yScroll : event.pageY,

            left = ((curX - xScroll + ttw) > $(window).width()) ? curX - ttw : curX,
            top = ((curY - yScroll + tth) > $(window).height()) ? curY - tth : curY;

        element.css({ "top": top + offset.y, "left": left + offset.x });
    }

    /*
    * Returns only the Job type from the long serialised full string
    */
    function simplifyJobType(jobType) {
        //var regexp = /Jobs.(.+?)(?:,)/i;
        var regexp = /Jobs.(.+?)(?:,)/i;
        var matches = jobType.match(regexp);

        return (matches && matches.length > 1) ? matches[1] : "";
    }

    /*
    * Returns only the trigger type from the long serialised full string
    */
    function simplifyTriggerType(triggerType) {
        var regexp = /Triggers.(.+?)(?:,)/i;
        var matches = triggerType.match(regexp);

        return (matches && matches.length > 1) ? matches[1] : "";
    }

    /*
    *
    */
    function extractHostFromService(fullUrl) {
        var regexp = /net.tcp:\/\/(.*?):7001\/automation_worker.svc/i;
        var matches = fullUrl.match(regexp);

        return (matches && matches.length > 1) ? matches[1] : "";
    }

    /*
    *
    */
    function extractFbxFilename(job) {
        var noFile = "";
        var regexp = /([^\\|\/|\ ]+)\.fbx/i;

        if (!job.Trigger) {
            return noFile;
        }
        
        if (job.Trigger.Description) {
            var matches = job.Trigger.Description.match(regexp);
            if (matches && matches.length > 1)
                return matches[1].toLowerCase();
        }
        
        if (job.Trigger.Files && job.Trigger.Files.length > 0) {
            var matches = job.Trigger.Files[0].match(regexp);
            if (matches && matches.length > 1)
                return matches[1].toLowerCase();
        }

        return noFile;
    }

    /*
    * Split A world into smaller ones based on the capital letters inside it
    * Return an array of the sub words
    */
    function splitUpperCasedWord(word) {
        return (word.match(/([A-Z]+)([^A-Z]+)/g) || [word]);
    }

    /*
    *
    */
    function getLastArrayElement(array) {
        return array[array.length - 1];
    }

    /*
    * Adds the "substr" substring to "string" every "repeat" characters
    */
    function addCharsToString(string, repeat, substr) {
        for (var i = 1; i <= (string.length/repeat); i++) {
            string = strInsert(string, i * repeat, substr);
        }
        return string;
    }
    
    /*
    * Inserts a substring at a string's index
    */
    function strInsert(string, index, substr) {
        return string.substr(0, index) + substr + string.substr(index);
    }
    /*
    * 
    */
    function constructMultiTriggerInfo(multiTrigger, ident) {
        var arrow = "> ";
        var dash = "--";

        var triggerInfo = arrow + simplifyTriggerType(multiTrigger.__type) + "\n";
        var subIdent = ident + dash;
        $.each(multiTrigger.Triggers, function (i, trigger) {
            var triggerSubType = simplifyTriggerType(trigger.__type);
            
            if (triggerSubType == automationConfig.multiTrigger) {
                var triggerSubInfo = constructMultiTriggerInfo(trigger, subIdent);
                triggerInfo += (subIdent + triggerSubInfo);
            }
            else {
                triggerInfo += (subIdent + arrow + triggerSubType + "\n");
            }
        });

        return triggerInfo;
    }

    /*
    *
    */
    function constructJobComponentsHtml(job) {
        var simplifiedJobType = simplifyJobType(job.__type);
        var simplifiedTriggerType = simplifyTriggerType(job.Trigger.__type);

        var components = $("<span>");
        if (automationConfig.jobTypes.hasOwnProperty(simplifiedJobType)) {
            components
                .append(
                    $("<span>")
                        .addClass("job-type-class")
                        .addClass(automationConfig.jobTypes[simplifiedJobType].className)
                        .attr("title", automationConfig.jobTypes[simplifiedJobType].friendlyName)
                )
        }
        if (job.hasOwnProperty("ReprocessedJobID")) {
            components
                .append(
                    $("<span>")
                        .addClass("job-type-class")
                        .addClass("reprocess-job-type")
                        .attr("title", "Reprocessed ID: " + job.ReprocessedJobID)
                )
        }
        if (automationConfig.triggerTypes.hasOwnProperty(simplifiedTriggerType)) {
            components
                .append(
                    $("<span>")
                        .addClass("trigger-type-class")
                        .addClass(automationConfig.triggerTypes[simplifiedTriggerType].className)
                        .attr("title",
                            (simplifiedTriggerType == automationConfig.multiTrigger)
                            ? constructMultiTriggerInfo(job.Trigger, "")
                            : automationConfig.triggerTypes[simplifiedTriggerType].friendlyName
                        )
                )
        }

        return components.html();

    }

    /*
    * Stores the REST Profiler object
    */
    function storeRestProfiler(profiler) {
        _restProfiler = profiler;
    }

    function getProfiler(profiler) {
        return _restProfiler;
    }

    /*
    * Starts the JS render profiler
    */
    function startJsRenderProfiler() {
        _jsRenderProfilerStart = new Date().getTime();
    }

    /*
    * Stops the JS Render Profiler and stores the elapsed ms in the local profiler object
    */
    function stopJsRenderProfiler() {
        _jsRenderProfilerEnd = new Date().getTime();
        var elapsedMilliseconds = _jsRenderProfilerEnd - _jsRenderProfilerStart

        if (_restProfiler == undefined) {
            _restProfiler = {};
        }
        _restProfiler["JSRender"] = elapsedMilliseconds;
    }

    /*
    * Starts the JS Ajax profiler
    */
    function startJsAjaxProfiler() {
        _jsAjaxProfilerStart = new Date().getTime();
    }

    /*
    * Stops the JS Ajax Profiler and stores the elapsed ms in the local profiler object
    */
    function stopJsAjaxProfiler() {
        _jsAjaxProfilerEnd = new Date().getTime();
        var elapsedMilliseconds = _jsAjaxProfilerEnd - _jsAjaxProfilerStart

        if (_restProfiler == undefined) {
            _restProfiler = {};
        }
        _restProfiler["JSAjaxResponse"] = elapsedMilliseconds;
    }

    /*
    * Returns the profile info in html, tool ready
    */
    function getProfilerInfoHtml() {
        var totalDBTime = (_restProfiler.hasOwnProperty("DBQueries"))
                                ? Object.keys(_restProfiler["DBQueries"])
                                                        .reduce(function (a, key) {
                                                            return (a + _restProfiler["DBQueries"][key]);
                                                        }, 0)
                                : 0;
        var ajaxResponse = (_restProfiler.hasOwnProperty("JSAjaxResponse")) ? _restProfiler["JSAjaxResponse"] : 0;
        var jsRender = (_restProfiler.hasOwnProperty("JSRender")) ? _restProfiler["JSRender"] : 0;
        
        var htmlInfo = "PROFILE INFO<hr/>";
            htmlInfo += ("<center><i>Ajax Request</i></center><hr/>");
            htmlInfo += ("Total Time: <b>" + (ajaxResponse / 1000) + "</b>s" + ((ajaxResponse < totalDBTime) ? " (cached)" : "") + "<br/>");
            htmlInfo += ("&nbsp;&nbsp;- DB Total: <i>" + (totalDBTime / 1000) + "</i>s <br/>");
            if (_restProfiler.hasOwnProperty("DBQueries")) {
                $.each(_restProfiler["DBQueries"], function (query, duration) {
                    htmlInfo += ("&nbsp;&nbsp;&nbsp;&nbsp;-- " + query + ": <i>" + (duration / 1000) + "</i>s <br/>");
                });
            }
            if (ajaxResponse > totalDBTime) {
                htmlInfo += ("&nbsp;&nbsp;- Other: <i>" + ((ajaxResponse - totalDBTime) / 1000) + "</i>s <br/>");
            }
            htmlInfo += ("<hr/><center><i>JavaScript Code</i></center><hr/>");
            htmlInfo += ("Total Time: <b>" + (jsRender / 1000) + "</b>s <br/>");

        return (htmlInfo);
    }

    /*
    * Update the tooltip with the profiler Info
    */
    function updateProfilerTooltip(tooltipId) {
        $("#" + tooltipId).tooltipster("content", getProfilerInfoHtml());
    }

    /*
    *
    */
    function isWindowHidden() {
        //console.log("window is hidden : " + window.document.hidden);
        return window.document.hidden;
    }

    // This code makes the following functions public
    return {
        getJobListConf: getJobListConf,
        getChangelistLink: getChangelistLink,
        createBugstarLinks: createBugstarLinks,
        processUsersInfo: processUsersInfo,
        updateTooltipPosition: updateTooltipPosition,
        simplifyJobType: simplifyJobType,
        simplifyTriggerType: simplifyTriggerType,
        constructMultiTriggerInfo: constructMultiTriggerInfo,
        constructJobComponentsHtml: constructJobComponentsHtml,
        isWindowHidden: isWindowHidden,
        extractHostFromService: extractHostFromService,
        storeRestProfiler: storeRestProfiler,
        getProfiler: getProfiler,
        startJsAjaxProfiler: startJsAjaxProfiler,
        stopJsAjaxProfiler: stopJsAjaxProfiler,
        startJsRenderProfiler: startJsRenderProfiler,
        stopJsRenderProfiler: stopJsRenderProfiler,
        updateProfilerTooltip: updateProfilerTooltip,
    }

};
