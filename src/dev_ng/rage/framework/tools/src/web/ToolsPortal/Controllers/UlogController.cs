﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using System.IO;
using System.Text;
using System.Configuration;

using RSG.Pipeline.Automation.Common.Jobs;

using RSG.ToolsPortal.Classes;
using RSG.ToolsPortal.ViewModels;

namespace RSG.ToolsPortal.Controllers
{
    public class UlogController : ApplicationController
    {
        #region Methods / Controllers
        /// <summary>
        /// GET: /Ulog/ 
        /// </summary>
        /// <param name="serverHost"></param>
        /// <param name="jobId"></param>
        /// <returns></returns>
        public ActionResult Index(String serverHost, String jobId, bool errorsOnly = false)
        {
            if ((serverHost == null) || (jobId == null)) {
                ModelState.AddModelError("", "Both ServerHost & JobId parameters must be provided");
                return View(new UlogViewModel());
            }

            UlogClass ulogWeb = new UlogClass(serverHost);
            Job job = ulogWeb.GetJob(jobId);
            XDocument ulogXMLDoc = ulogWeb.GetUlogXDocument(job);

            bool hasBuildMonitorFile = false;
            String buildMonitorFilename = ulogWeb.CheckFileAvailability(job, ConfigurationManager.AppSettings["BuildMonitorFilename"]);
            if (buildMonitorFilename != null)
            {
                hasBuildMonitorFile = true;
            }
            
            if (ModelState.IsValid && (ulogWeb.Exceptions.Count > 0))
            {
                foreach (KeyValuePair<String, Exception> ex in ulogWeb.Exceptions)
                {
                    ModelState.AddModelError("", ex.Key + ex.Value);
                }
            }

            return View(new UlogViewModel(serverHost, job, errorsOnly, ulogXMLDoc, hasBuildMonitorFile));
        }

        /// <summary>
        /// GET: /Ulog/Errors
        /// </summary>
        /// <param name="serverHost"></param>
        /// <param name="jobId"></param>
        /// <returns></returns>
        public ActionResult Errors(String serverHost, String jobId)
        {
            UlogClass ulogWeb = new UlogClass(serverHost);
            Job job = ulogWeb.GetJob(jobId);
            List<String> errors = ulogWeb.GetErrors(job);

            return Json(errors, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serverHost"></param>
        /// <param name="jobId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetULog(String serverHost, String jobId)
        {
            UlogClass ulogWeb = new UlogClass(serverHost);
            Job job = ulogWeb.GetJob(jobId);

            String ulogContent = ulogWeb.GetUlogContent(job);

            return File(new MemoryStream(Encoding.UTF8.GetBytes(ulogContent ?? "")),
                        ConfigurationManager.AppSettings["UniversalLogFileMimeType"],
                        jobId + "-" + ConfigurationManager.AppSettings["UniversalLogFileName"]
                        );
        }

        /// <summary>
        /// Post: /Ulog/DownloadULog
        /// </summary>
        /// <param name="serverHost"></param>
        /// <param name="jobId"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DownloadULog(String serverHost, String jobId)
        {
            UlogClass ulogWeb = new UlogClass(serverHost);
            Job job = ulogWeb.GetJob(jobId);

            String ulogContent = ulogWeb.GetUlogContent(job);

            return File(new MemoryStream(Encoding.UTF8.GetBytes(ulogContent ?? "")),
                        ConfigurationManager.AppSettings["UniversalLogFileMimeType"],
                        jobId + "-" + ConfigurationManager.AppSettings["UniversalLogFileName"]
                        );
        }

        /// <summary>
        /// Post: /Ulog/DownloadBuildMonitor
        /// </summary>
        /// <param name="serverHost"></param>
        /// <param name="jobId"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DownloadBuildMonitor(String serverHost, String jobId)
        {
            String localBuildMonitorFilepath = null;

            UlogClass ulogWeb = new UlogClass(serverHost);
            Job job = ulogWeb.GetJob(jobId);

            String buildMonitorFilename = ConfigurationManager.AppSettings["BuildMonitorFilename"];
            String remoteBuildMonitorFilename = ulogWeb.CheckFileAvailability(job, buildMonitorFilename);
            if (remoteBuildMonitorFilename != null)
            {
                localBuildMonitorFilepath = ulogWeb.DownloadJobFile(job, remoteBuildMonitorFilename);
            }

            MemoryStream stream = new MemoryStream(System.IO.File.ReadAllBytes(localBuildMonitorFilepath));
            ulogWeb.DeleteLocalFileIfExists(localBuildMonitorFilepath);

            return File(stream, ConfigurationManager.AppSettings["BuildMonitorMimeType"],
                        jobId + "-" +  buildMonitorFilename
                        );
        }
        #endregion //Methods / Controllers
    }

} //ToolsPortal.Controllers namespace
