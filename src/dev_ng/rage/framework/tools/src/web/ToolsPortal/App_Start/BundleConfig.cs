﻿using System.Web;
using System.Web.Optimization;

namespace RSG.ToolsPortal
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            // JQuery Script
            bundles.Add(new Bundle("~/bundles/jquery").Include(
                        "~/JSLibs/jquery/jquery-{version}.js",
                        "~/JSLibs/jquery-browser/jquery.browser.js"));
                        //"~/JSLibs/jquery/jquery-migrate-{version}.js"));

            // JQuery UI Script
            bundles.Add(new Bundle("~/bundles/jqueryui").Include(
                       "~/JSLibs/jquery-ui/js/jquery-ui-{version}.js")
                       );

            // Detect unsupported browser Script
            bundles.Add(new Bundle("~/bundles/detect-browser").Include(
                        "~/Scripts/shared/detect_browser_error.js")
                        );

            // Datatables Script
            bundles.Add(new Bundle("~/bundles/dataTables").Include(
                            "~/JSLibs/jquery-datatables/jquery.dataTables.js",
                            "~/JSLibs/jquery-datatables/datatables.FixedHeader.js",
                            "~/JSLibs/jquery-datatables/datatables.FixedColumns.js",
                            "~/JSLibs/jquery-datatables/datatables.colVis.js",
                            "~/JSLibs/jquery-datatables/datatables.colReorder.js",
                            "~/JSLibs/jquery-datatables/datatables.scroller.js"
                        ));

            // Generic legacy js stats config and functions
            bundles.Add(new Bundle("~/bundles/generic").Include(
                        "~/Scripts/shared/generic.js"));

            // Antiscroll js
            bundles.Add(new Bundle("~/bundles/antiscroll").Include(
                        "~/JSLibs/antiscroll/antiscroll.js"));

            // D3js library
            bundles.Add(new Bundle("~/bundles/d3js").Include(
                        "~/JSLibs/d3/d3.js"));
            // NVD3 js Library
            bundles.Add(new Bundle("~/bundles/nvd3").Include(
                        "~/JSLibs/nvd3/nv.d3.js"));

            bundles.Add(new Bundle("~/bundles/nvd3-linegraph").Include(
                        "~/Scripts/graphs/nvd3-linegraph.js"
                        ));
            bundles.Add(new Bundle("~/bundles/nvd3-stackedareagraph").Include(
                        "~/Scripts/graphs/nvd3-stackedareagraph.js"
                        ));
            bundles.Add(new Bundle("~/bundles/tabular-report").Include(
                        "~/Scripts/shared/tabular-report.js"
                        ));

            bundles.Add(new Bundle("~/bundles/Project").Include(
                        "~/Scripts/ProjectConfig.js",
                        "~/Scripts/ProjectClass.js"
                        ));

            bundles.Add(new Bundle("~/bundles/Deployment").Include(
                        "~/Scripts/deployment/DeploymentConfig.js",
                        "~/Scripts/deployment/DeploymentClass.js"
                        ));

            // Automation Client Scripts
            bundles.Add(new Bundle("~/bundles/automation").Include(
                        "~/Scripts/automation/AutomationServiceConfig.js",
                        "~/Scripts/automation/automation.js"
                        ));

            // New Automation Service Scripts
            bundles.Add(new Bundle("~/bundles/AutomationService").Include(
                        "~/Scripts/automation/AutomationServiceConfig.js",
                        "~/Scripts/automation/AutomationLibClass.js",
                        "~/Scripts/automation/AutomationServiceClass.js"
                        ));

            // Automation Status Scripts
            bundles.Add(new Bundle("~/bundles/AutomationStatus").Include(
                        "~/Scripts/automation/AutomationServiceConfig.js",
                        "~/Scripts/automation/AutomationStatusClass.js"
                        ));

            // Automation Graphs Scripts
            bundles.Add(new Bundle("~/bundles/AutomationGraphs").Include(
                        "~/Scripts/automation/AutomationGraphsClass.js"
                        ));

            // Ulog Script
            bundles.Add(new Bundle("~/bundles/ulog").Include(
                        "~/Scripts/ulog/ulog.js"
                        ));

            // jquery-tablesorter Script
            bundles.Add(new Bundle("~/bundles/jquery-tablesorter").Include(
                        "~/JSLibs/jquery-tablesorter/jquery.tablesorter.js",
                        "~/JSLibs/jquery-tablesorter/jquery.tablesorter.widgets.js"
                        ));

            // jquery-fullscreen-event Script
            bundles.Add(new Bundle("~/bundles/jquery-fullscreen-event").Include(
                        "~/JSLibs/jquery-fullscreen-event/jquery.onfullscreen-1.0.0.js"
                        ));
            
            /********** STYLES ************/
            bundles.Add(new Bundle("~/Content/shared/generic").Include(
                                "~/Content/shared/fonts.css",
                                "~/JSLibs/antiscroll/antiscroll.css",
                                "~/Content/shared/generic.css"
                            )
                        );

            //bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));
            
            // JQuery UI theme style
            bundles.Add(new Bundle("~/Content/themes/smoothness/jqueryuitheme")
                           .Include("~/Content/themes/smoothness/jquery-ui-{version}.custom.css")
                      );

            // Datatables style
            bundles.Add(new Bundle("~/Content/style/dataTablesStyle")
                            .Include(
                                "~/JSLibs/jquery-datatables/jquery.dataTables.css",
                                "~/JSLibs/jquery-datatables/dataTables.fixedColumns.css",
                                "~/JSLibs/jquery-datatables/dataTables.fixedHeader.css",
                                //"~/JSLibs/jquery-datatables/dataTables.colVis.css",
                                //"~/JSLibs/jquery-datatables/dataTables.scroller.css",
                                "~/Content/shared/jquery.dataTables-local.css",
                                "~/Content/shared/jquery.dataTables.colVis.css"
                            ));

            // Automation monitor style
            bundles.Add(new Bundle("~/Content/style/automation")
                            .Include("~/Content/automation/automation.css")
                       );

            // Ulog Style
            bundles.Add(new Bundle("~/Content/style/ulog")
                            .Include("~/Content/ulog/ulog.css")
                       );

            // NVD3 style
            bundles.Add(new Bundle("~/Content/style/nvd3style")
                            .Include("~/JSLibs/nvd3/nv.d3.css",
                                    "~/Content/shared/nv.d3-local.css"
                                    )
                       );

            /*
            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));
            */
        }
    }
}