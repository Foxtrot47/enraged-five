﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Reflection;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using System.IO;

using P4API;
using RSG.SourceControl.Perforce;

using RSG.Pipeline.Automation.Consumers;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Tasks;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Common.Client;

using RSG.Base.Configuration.Automation;
using RSG.Base.Logging.Universal;

using Newtonsoft.Json;

namespace RSG.ToolsPortal.Classes
{
    public class AutomationWrapper
    {
        #region Properties
        /// <summary>
        /// Automation Monitor Admin Server Host
        /// </summary>
        public Uri MonitorAdminServerHost { get; private set; }

        /// <summary>
        /// Automation Consumer Object
        /// </summary>
        public AutomationAdminConsumer AutomationConsumer { get; private set; }

        /// <summary>
        /// ServiceIsUp boolean
        /// </summary>
        public Boolean ServiceIsUp { get; private set; }

        /// <summary>
        /// IAutomationServiceViewConfig
        /// </summary>
        public IAutomationServiceViewConfig AutomationServiceView { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        private IDictionary<String, SimplifiedJobModel> ConsumerJobs { get; set; }

        /// <summary>
        /// 
        /// </summary>
        private IDictionary<String, JobResult> JobResults { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="serverHost"></param>
        public AutomationWrapper(IAutomationServiceViewConfig automationServiceView)
            : this(automationServiceView.Service.ServerHost)
        {
            this.AutomationServiceView = automationServiceView;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="ServerHost"></param>
        public AutomationWrapper(Uri serverHost)
        {
            this.MonitorAdminServerHost = new Uri(serverHost + ConfigurationManager.AppSettings["AutomationController"]);
            this.AutomationConsumer = new AutomationAdminConsumer(this.MonitorAdminServerHost);

            IEnumerable<TaskStatus> tasks = this.AutomationConsumer.Monitor();
            if (tasks == null || tasks.Count() == 0)
            {
                this.ServiceIsUp = false;
            }
            else
            {
                this.ServiceIsUp = true;
            }
        }
        #endregion // Constructor(s)

        #region public methods
        /// <summary>
        /// Returns all IJob objects as an IEnumerable from the Automation Monitor
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IJob> GetAllJobsFromMonitor()
        {
            IEnumerable<IJob> allJobs = new List<IJob>();
            IEnumerable<TaskStatus> tasks = this.AutomationConsumer.Monitor();

            // re-initialise the consumer & jobresutls jobs dictionary
            this.ConsumerJobs = new Dictionary<String, SimplifiedJobModel>();
            this.JobResults = new Dictionary<String, JobResult>();

            foreach (TaskStatus task in tasks)
            {
                IList<IJob> taskJobs = new List<IJob>();
                foreach (Job job in task.Jobs)
                {
                    // Ignore the Consumer Jobs and add them in the respective lookup dictionary
                    if (job.Trigger is IMultiTrigger)
                    {
                        this.ConsumerJobs.Add(job.ID.ToString(), new SimplifiedJobModel(job));
                    }
                    else if (this.AutomationServiceView.FilterKey != null)
                    {
                        // Filter the jobs on the given Job property / value using Reflection
                        Type automationServicetype = job.GetType();
                        PropertyInfo propertyInfo = automationServicetype.GetProperty(this.AutomationServiceView.FilterKey);

                        if (((String) propertyInfo.GetValue(job)) == this.AutomationServiceView.FilterValue) 
                        {
                            taskJobs.Add(job);
                        }
                    }
                    else {
                        // Deal with empty Processing Hosts
                        if ((job.ProcessingHost == null) || (job.ProcessingHost == String.Empty)) 
                        {
                            job.ProcessingHost = ConfigurationManager.AppSettings["EmptyProcessingHostString"];
                        }
                        // Otherwise just add the job to the list
                        taskJobs.Add(job);
                    }
                }
                allJobs = allJobs.Concat(taskJobs);

                // Create the dictionary of job results
                foreach (JobResult jobResult in task.JobResults)
                {
                    if (!this.JobResults.ContainsKey(jobResult.JobId.ToString()))
                    {
                        this.JobResults.Add(jobResult.JobId.ToString(), jobResult);
                    }
                }
            }
            return allJobs;
        }

        /// <summary>
        /// Returns all IJob objects grouped by triggered at time ticks and Job specific properties
        /// </summary>
        /// <returns></returns>
        public AutomationGroupedJobs GetAllJobsGrouped()
        {
            AutomationGroupedJobs automationGroupedJobs = new AutomationGroupedJobs();

            // Temporary Structure for Creating the grouped by trigger List
            IDictionary<String, AutomationTriggerGroupedJobs> triggerGroupedJobsDictionary
                                                                    = new Dictionary<String, AutomationTriggerGroupedJobs>();

            if (this.ServiceIsUp)
            {
                foreach (IJob job in this.GetAllJobsFromMonitor())
                {
                    // Ignore the orphaned SkippedConsumed Jobs (b* #1910723)
                    if (
                        ((job.State == JobState.SkippedConsumed) && !this.ConsumerJobs.ContainsKey(job.ConsumedByJobID.ToString()))
                        || (job.Trigger == null)
                    )
                    {
                        continue;
                    }
                    
                    String sortKey = job.Trigger.TriggerId.ToString();

                    // if the trigger id is missing, use the time ticks for job row grouping/sorting
                    if (sortKey == Guid.Empty.ToString())
                    {
                        sortKey = job.Trigger.TriggeredAt.Ticks.ToString();
                    }

                    //if (!triggerGroupedJobsDictionary.ContainsKey(sortKey)
                    //        && (triggerGroupedJobsDictionary.Count() < Convert.ToInt32(ConfigurationManager.AppSettings["MaxAutomationMatrixLines"]))
                    //)
                    if (!triggerGroupedJobsDictionary.ContainsKey(sortKey))
                    {
                        triggerGroupedJobsDictionary.Add(sortKey, new AutomationTriggerGroupedJobs());
                        triggerGroupedJobsDictionary[sortKey].GroupJobTrigger = new SimplifiedJobTriggerModel(job);
                    }

                    // This will run only when the triggerGroupedJobsDictionary has less than MaxAutomationMatrixLines items
                    if (triggerGroupedJobsDictionary.ContainsKey(sortKey))
                    {
                        String groupKey = ConstructGroupKey(job);
                        if (!triggerGroupedJobsDictionary[sortKey].GroupJobsDict.ContainsKey(groupKey))
                        {
                            triggerGroupedJobsDictionary[sortKey].GroupJobsDict.Add(groupKey, new SimplifiedJobModel(job));
                        }

                        // Build the column group keys
                        if (!automationGroupedJobs.ColumnGroups.ContainsKey(groupKey))
                        {
                            automationGroupedJobs.ColumnGroups.Add(groupKey, String.Empty);
                            // For Integrator Jobs get the Branch Info from Perforce
                            if (job is IntegratorJob)
                            {
                                automationGroupedJobs.ColumnGroups[groupKey] = GetBranchSpecInfo(((IntegratorJob)job).Integration);
                            }
                        }
                    }
                }
            }

            automationGroupedJobs.ConsumerJobs = this.ConsumerJobs;
            automationGroupedJobs.JobResults = this.JobResults;
            automationGroupedJobs.TriggerGroups =
                                triggerGroupedJobsDictionary.Values.OrderByDescending(x => x.GroupJobTrigger.Trigger.TriggeredAt).ToList();

            return automationGroupedJobs;
        }

        /// <summary>
        /// Stores the default automationGroupedJobs data to a local file
        /// </summary>
        /// <param name="automationGroupedJobs"></param>
        public void StoreGroupedJobs(AutomationGroupedJobs automationGroupedJobs)
        {
            String groupedJobsFilename = HttpContext.Current.Server.MapPath("~/")
                                + ConfigurationManager.AppSettings["JobMatrixStaticFilePath"]
                                + this.AutomationServiceView.WebAlias
                                + ConfigurationManager.AppSettings["JobMatrixStaticExtension"];

            try
            {
                using (StreamWriter streamw = File.CreateText(groupedJobsFilename))
                {
                    String json = JsonConvert.SerializeObject(automationGroupedJobs);
                    streamw.Write(json.ToArray());
                    streamw.Close();
                }
            }
            catch (Exception e)
            {
                // Do something with the exception
                e.Message.Count();
            }
        }

        /// <summary>
        /// Retrieves the latest automationGroupedJobs data from a local file
        /// </summary>
        /// <returns></returns>
        public AutomationGroupedJobs RetrieveGroupedJobs()
        {
            String groupedJobsFilename = HttpContext.Current.Server.MapPath("~/")
                                + ConfigurationManager.AppSettings["JobMatrixStaticFilePath"]
                                + this.AutomationServiceView.WebAlias
                                + ConfigurationManager.AppSettings["JobMatrixStaticExtension"];
            AutomationGroupedJobs automationGroupedJobs = null;

            try
            {
                using (StreamReader streamr = File.OpenText(groupedJobsFilename))
                {
                    String json = streamr.ReadToEnd();
                    automationGroupedJobs = JsonConvert.DeserializeObject<AutomationGroupedJobs>(json);
                    streamr.Close();
                }
            }
            catch (Exception e)
            {
                // Do something with the exception
                e.Message.Count();
            }

            return (automationGroupedJobs);
        }

        /// <summary>
        /// Retrieves the Job object from the Automation Monitor by its ID 
        /// </summary>
        /// <param name="jobId"></param>
        /// <returns></returns>
        public Job GetJobFromMonitor(String jobId)
        {
            Job matchedJob = null;
            Guid jobIdGuid = new Guid(jobId);

            IEnumerable<TaskStatus> tasks = this.AutomationConsumer.Monitor();
            foreach (TaskStatus task in tasks)
            {
                foreach (Job job in task.Jobs)
                {
                    if (job.ID == jobIdGuid)
                    {
                        matchedJob = job;
                        break;
                    }
                }

                // Break the outer loop if the job has been found at the inner one
                if (matchedJob != null)
                {
                    break;
                }
            }
            return matchedJob;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<AutomationHostStats> GetServerStatsPerHost()
        {
            IEnumerable<IJob> jobs = GetAllJobsFromMonitor();

            // Data structure for storing the stats per host and per day
            IDictionary<String, IDictionary<String, AutomationDailyStats>> hostsDict = 
                                                                new Dictionary<String, IDictionary<String, AutomationDailyStats>>();
            // This will keep track of all the Dates found
            IDictionary<String, DateTime> allDatesDict = new Dictionary<String, DateTime>();

            foreach(IJob job in jobs)
            {
                DateTime currentDate = job.CreatedAt.Date.ToUniversalTime();
                String currentDateString = currentDate.ToString("u");

                // Add the date to the Dates Dict
                if (!allDatesDict.ContainsKey(currentDateString))
                {
                    allDatesDict.Add(currentDateString, currentDate);
                }

                // Create the dictionary entries if they don't exist
                if (!hostsDict.ContainsKey(job.ProcessingHost))
                {
                    IDictionary<String, AutomationDailyStats> days = new Dictionary<String, AutomationDailyStats>();
                    days.Add(currentDateString, new AutomationDailyStats(currentDate));
                    hostsDict.Add(job.ProcessingHost, days);
                }
                else if (!hostsDict[job.ProcessingHost].ContainsKey(currentDateString))
                {
                    hostsDict[job.ProcessingHost][currentDateString] = new AutomationDailyStats(currentDate);
                }

                hostsDict[job.ProcessingHost][currentDateString].CreatedJobs++;
                if ((job.State == JobState.Completed) && (job.CompletedAt.ToUniversalTime() <= DateTime.UtcNow))
                {
                    hostsDict[job.ProcessingHost][currentDateString].CompletedJobs++;
                    hostsDict[job.ProcessingHost][currentDateString].TotalProcessingTime += job.ProcessingTime.TotalMilliseconds;
                    hostsDict[job.ProcessingHost][currentDateString].TotalLagTime +=
                        (new TimeSpan(job.CompletedAt.Ticks - job.CreatedAt.Ticks)).TotalMilliseconds;
                }
                else if (job.State == JobState.Pending)
                {
                    hostsDict[job.ProcessingHost][currentDateString].PendingJobs++;
                }
                else if (job.State == JobState.Assigned)
                {
                    hostsDict[job.ProcessingHost][currentDateString].AssignedJobs++;
                }
                else if (job.State == JobState.Errors)
                {
                    hostsDict[job.ProcessingHost][currentDateString].ErroredJobs++;
                }
                else if (job.State == JobState.Skipped)
                {
                    hostsDict[job.ProcessingHost][currentDateString].SkippedJobs++;
                }
                else if (job.State == JobState.SkippedConsumed)
                {
                    hostsDict[job.ProcessingHost][currentDateString].SkippedConsumedJobs++;
                }
                else if (job.State == JobState.Aborted)
                {
                    hostsDict[job.ProcessingHost][currentDateString].AbortedJobs++;
                }
                else if (job.State == JobState.ClientError)
                {
                    hostsDict[job.ProcessingHost][currentDateString].ClientErroredJobs++;
                }
            }

            // The list of host stats that is going to be Serialised
            IList<AutomationHostStats> hosts = new List<AutomationHostStats>();
            // Populate the list from the hosts dictionary
            foreach (KeyValuePair<String, IDictionary<String, AutomationDailyStats>> hostDict in hostsDict)
            {
                // Loop through all the dates and populate the missing host entries with empty AutomationDailyStats objects
                foreach (KeyValuePair<String, DateTime> date in allDatesDict)
                {
                    if (!hostDict.Value.ContainsKey(date.Key))
                    {
                        hostDict.Value.Add(date.Key, new AutomationDailyStats(date.Value));
                    }
                }
                // Order per host stats by date
                IEnumerable<AutomationDailyStats> sortedDailyStats = hostDict.Value.Values.OrderBy(x => x.Date).ToList();

                hosts.Add(new AutomationHostStats(hostDict.Key, sortedDailyStats));

                // Order hosts by hostname
                hosts = hosts.OrderBy(x => x.HostName).ToList();
            }

            return hosts;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public String GetServerStatsPerHostCSV()
        {
            IEnumerable<AutomationHostStats> hosts = this.GetServerStatsPerHost();

            StringBuilder csvString = new StringBuilder();
            foreach (AutomationHostStats hostStats in hosts)
            {
                csvString.Append(hostStats.HostName);
                csvString.AppendLine(",");

                csvString.Append("Date");
                csvString.Append(',');
                csvString.Append("Created Jobs");
                csvString.Append(',');
                csvString.Append("Completed Jobs");
                csvString.Append(',');
                csvString.Append("Pending Jobs");
                csvString.Append(',');
                csvString.Append("Assigned Jobs");
                csvString.Append(',');
                csvString.Append("Errored Jobs");
                csvString.Append(',');
                csvString.Append("Skipped Jobs");
                csvString.Append(',');
                csvString.Append("Skipped Consumed Jobs");
                csvString.Append(',');
                csvString.Append("Aborted Jobs");
                csvString.Append(',');
                csvString.Append("ClientErrored Jobs");
                csvString.Append(',');
                csvString.Append("Average Processing Time (secs)");
                csvString.Append(',');
                csvString.AppendLine("Average Lag Time (secs)");

                foreach (AutomationDailyStats dailyStats in hostStats.DailyStats)
                {
                    csvString.Append(dailyStats.Date);
                    csvString.Append(',');
                    csvString.Append(dailyStats.CreatedJobs);
                    csvString.Append(',');
                    csvString.Append(dailyStats.CompletedJobs);
                    csvString.Append(',');
                    csvString.Append(dailyStats.PendingJobs);
                    csvString.Append(',');
                    csvString.Append(dailyStats.AssignedJobs);
                    csvString.Append(',');
                    csvString.Append(dailyStats.ErroredJobs);
                    csvString.Append(',');
                    csvString.Append(dailyStats.SkippedJobs);
                    csvString.Append(',');
                    csvString.Append(dailyStats.SkippedConsumedJobs);
                    csvString.Append(',');
                    csvString.Append(dailyStats.AbortedJobs);
                    csvString.Append(',');
                    csvString.Append(dailyStats.ClientErroredJobs);
                    csvString.Append(',');
                    csvString.Append((dailyStats.CreatedJobs > 0) ? (dailyStats.TotalProcessingTime / dailyStats.CreatedJobs)/1000 : 0);
                    csvString.Append(',');
                    csvString.Append((dailyStats.CreatedJobs > 0) ? (dailyStats.TotalLagTime / dailyStats.CreatedJobs)/1000 : 0);
                    csvString.AppendLine();
                }
                csvString.AppendLine();
            }
            return csvString.ToString();
        }
        #endregion // public methods

        #region private methods
        /// <summary>
        /// Constructs the lookup key of an IJob based on its implementation
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        private String ConstructGroupKey(IJob job)
        {
            return String.Join(" | ", GetGroupHierarchy(job));
        }

        /// <summary>
        /// Defines the level and hierarchy of grouping for a IJob specific implementations
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        private String[] GetGroupHierarchy(IJob job)
        {
            String[] groupHierarchy;

            if (job is CodeBuilder2Job)
            {
                groupHierarchy = new String[] 
                    {
                        ((CodeBuilder2Job) job).SolutionFilename, 
                        ((CodeBuilder2Job) job).Platform, 
                        ((CodeBuilder2Job) job).BuildConfig,
                    };
            }
            else if (job is IntegratorJob)
            {
                groupHierarchy = new String[] 
                    {
                        ((IntegratorJob) job).Integration.BranchSpec, 
                        ((IntegratorJob) job).Integration.P4ResolveOptions, 
                    };
            }
            else if (job is ScriptBuilderJob)
            {
                groupHierarchy = new String[] 
                    {
                        ((ScriptBuilderJob) job).ScriptProjectFilename, 
                        ((ScriptBuilderJob) job).BuildConfig, 
                    };
            }
            else if (job is BaseSmokeTestJob)
            {
                groupHierarchy = new String[] 
                    {
                        ((BaseSmokeTestJob) job).FriendlyName,
                        ((BaseSmokeTestJob) job).Platform,
                        ((BaseSmokeTestJob) job).BuildConfig,
                    };
            }
            else if (job is CommandRunnerJob)
            {
                groupHierarchy = new String[] 
                    {
                        ((CommandRunnerJob) job).CommandName,
                        ((CommandRunnerJob) job).BranchName,
                    };
                /*
                groupHierarchy = ((CommandRunnerJob) job).GroupingName.Split(
                            Char.Parse(ConfigurationManager.AppSettings["JobMatrixGroupFromFriendlyNameSeparator"])
                         );
                */
            }
            else 
            {
                groupHierarchy = new String[] 
                    {
                        ((Job) job).ProcessingHost,
                    };
            }

            return groupHierarchy;
        }

        /// <summary>
        /// Used to Construct the Branch Spec Info
        /// </summary>
        /// <param name="integration"></param>
        /// <returns></returns>
        private String GetBranchSpecInfo(Integration integration)
        {
            HtmlTableRow row;
            HtmlTableCell cell;

            HtmlTable infoTable = new HtmlTable();
            infoTable.Attributes.Add("class", "monospace");

            // Title Row
            row = new HtmlTableRow();
            cell = new HtmlTableCell("th");
            cell.InnerText = "Branch Spec Information";
            cell.ColSpan = 2;
            cell.Attributes.Add("class", "title");
            row.Cells.Add(cell);
            infoTable.Rows.Add(row);

            try 
            {
                using (P4 p4 = WebUtils.Config.Project.SCMConnect())
                {
                    P4API.P4RecordSet recordsBranches = p4.Run("branch", "-o", integration.BranchSpec);
                    P4Record rec = recordsBranches.Records.FirstOrDefault();

                    // Owner Row:
                    row = new HtmlTableRow();

                    cell = new HtmlTableCell();
                    cell.InnerText = "Owner: ";
                    row.Cells.Add(cell);

                    cell = new HtmlTableCell();
                    cell.InnerText = rec.Fields["Owner"] + " (" + rec.Fields["Options"] + ")";
                    row.Cells.Add(cell);

                    infoTable.Rows.Add(row);

                    // Description Row: 
                    row = new HtmlTableRow();

                    cell = new HtmlTableCell();
                    cell.InnerText = "Description: ";
                    row.Cells.Add(cell);

                    cell = new HtmlTableCell();
                    cell.InnerText = rec.Fields["Description"];
                    row.Cells.Add(cell);

                    infoTable.Rows.Add(row);

                    // View Row: 
                    row = new HtmlTableRow();

                    cell = new HtmlTableCell();
                    cell.InnerText = "View: ";
                    row.Cells.Add(cell);

                    cell = new HtmlTableCell();
                                       
                    foreach (String viewLine in rec.ArrayFields["View"])
                    {
                        cell.InnerText += (viewLine + "\n");
                    }

                    row.Cells.Add(cell);

                    infoTable.Rows.Add(row);
                }
            }
            catch (Exception exp)
            {
                // Error Row: 
                row = new HtmlTableRow();

                cell = new HtmlTableCell();
                cell.InnerText = "Error: ";
                row.Cells.Add(cell);

                cell = new HtmlTableCell();
                cell.InnerText = "Failed to connect to P4.\n" + exp.Message;
                row.Cells.Add(cell);

                infoTable.Rows.Add(row);
            }
            
            StringWriter sw = new StringWriter();
            infoTable.RenderControl(new HtmlTextWriter(sw));

            return sw.ToString();
        }
        #endregion // private methods
    }
} // RSG.ToolsPortal.Classes namespace