﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Net;
using System.Text;

using RSG.Base.Configuration;
using RSG.Base.Configuration.Services;

using RSG.Deployment.Common.Config;
using RSG.Deployment.Consumers;

using RSG.Model.Deployment.Game;
using RSG.Model.Deployment.Tools;

using RSG.Base.Configuration.Portal;

namespace RSG.ToolsPortal.Classes
{
	public class DeploymentWrapper
	{

        #region Properties
        /// <summary>
        /// Deployment Version Consumer Object
        /// </summary>
        public DeploymentVersionConsumer DeploymentConsumer {get; private set; }

        /// <summary>
        /// All Game Build Versions Enumerable Ordered by Timestamp Desc
        /// </summary>
        public IEnumerable<GameBuildVersion> AllGameBuildVersions { get; private set; }

        /// <summary>
        /// All Game Build Tracking Enumerable Ordered by Timestamp Desc
        /// </summary>
        public IEnumerable<GameBuildTracking> AllGameBuildMachineTracking { get; private set; }

        /// <summary>
        /// All Tools Versions Enumerable Ordered by Timestamp Desc
        /// </summary>
        public IEnumerable<RSG.Model.Deployment.Tools.ToolsVersion> AllToolsVersions { get; private set; }

        /// <summary>
        /// The Current Game Build Version
        /// </summary>
        public GameBuildVersion CurrentGameBuildVersion { get; set; }

        /// <summary>
        /// The Next Game Build Version
        /// </summary>
        public GameBuildVersion NextGameBuildVersion { get; set; }

        /// <summary>
        /// ServiceIsUp boolean
        /// </summary>
        public Boolean ServiceIsUp { get; private set; }

        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Deployment Config Object
        /// </summary>
        private IDeploymentConfig _deploymentConfig;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor
        /// </summary>
        public DeploymentWrapper(IPortalConfig projectPortalConfig)
        {
            // Will use projectConfig eventually 
            IBranch defaultBranch = WebUtils.Config.Project.DefaultBranch;
            this._deploymentConfig = new DeploymentConfig(defaultBranch);

            IServiceHostConfig serverConfig = this._deploymentConfig.Servers[ConfigurationManager.AppSettings["CurrentDeploymentServer"]];
            this.DeploymentConsumer = new DeploymentVersionConsumer(serverConfig);

            try
            {
                this.AllGameBuildVersions = this.DeploymentConsumer.GetAllGameVersions().OrderByDescending(x => x.Timestamp);
                this.AllGameBuildMachineTracking = this.DeploymentConsumer.GetAllGameTrackingForAllMachines().OrderByDescending(x => x.Timestamp);
                this.AllToolsVersions = this.DeploymentConsumer.GetAllToolsVersions().OrderByDescending(x => x.Timestamp);
                this.CurrentGameBuildVersion = this.DeploymentConsumer.GetLatestDeployedGameVersion();
                this.NextGameBuildVersion = this.DeploymentConsumer.GetFirstUndeployedGameVersion();
            }
            catch (Exception e)
            {
                e.Message.Count();   
            }
        }
        #endregion // Constructor(s)

        #region Public Methods
        public GameBuildTracking GetLocalGameBuildTracking(String clientMachine) {
            GameBuildTracking LocalGameBuildTracking = null;

            IEnumerable<GameBuildTracking> GameBuildTrackings = this.DeploymentConsumer.GetAllGameTrackingForMachine(clientMachine);
            if (GameBuildTrackings != null)
            {
                if (GameBuildTrackings.Count() > 0)
                {
                    LocalGameBuildTracking = GameBuildTrackings.Last();
                }
            }

            return LocalGameBuildTracking;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="versionString"></param>
        /// <returns></returns>
        public Tuple<String, String> GetSplitVersionsFromString(String versionString)
        {
            String[] versions = versionString.Split(Char.Parse(ConfigurationManager.AppSettings["DeploymentVersionURISeparator"]));
            String majorVersion = versions[0];
            String minorVersion = (versions.Length > 1) ? versions[1] : "";

            return new Tuple<String, String>(majorVersion, minorVersion);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="GameBuildVersion"></param>
        /// <returns></returns>
        public String GetGameBuildVersionViewString(GameBuildVersion gameBuildVersion)
        {
            StringBuilder sb = new StringBuilder();
            if (gameBuildVersion != null)
            {
                sb.Append(gameBuildVersion.MajorVersion);
                sb.Append(ConfigurationManager.AppSettings["DeploymentVersionViewSeparator"]);
                sb.Append(gameBuildVersion.MinorVersion);
            }
            else
            {
                sb.Append(ConfigurationManager.AppSettings["DeploymentVersionViewSeparator"]);
            }

            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameBuildVersion"></param>
        /// <returns></returns>
        public String GetGameBuildVersionURIString(GameBuildVersion gameBuildVersion)
        {
            StringBuilder sb = new StringBuilder();
            if (gameBuildVersion != null)
            {
                sb.Append(gameBuildVersion.MajorVersion);
                sb.Append(ConfigurationManager.AppSettings["DeploymentVersionURISeparator"]);
                sb.Append(gameBuildVersion.MinorVersion);
            }
            else
            {
                sb.Append(ConfigurationManager.AppSettings["DeploymentVersionURISeparator"]);
            }

            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameBuildTracking"></param>
        /// <returns></returns>
        public String GetGameBuildTrackingString(GameBuildTracking gameBuildTracking)
        {
            StringBuilder sb = new StringBuilder();
            if (gameBuildTracking != null)
            {
                sb.Append(gameBuildTracking.MajorVersion);
                sb.Append(ConfigurationManager.AppSettings["DeploymentVersionViewSeparator"]);
                sb.Append(gameBuildTracking.MinorVersion);
            }
            else
            {
                sb.Append(ConfigurationManager.AppSettings["DeploymentVersionViewSeparator"]);
            }

            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ToolsVersion"></param>
        /// <returns></returns>
        public String GetToolsVersionViewString(RSG.Model.Deployment.Tools.ToolsVersion toolsVersion)
        {
            StringBuilder sb = new StringBuilder();
            if (toolsVersion != null)
            {
                sb.Append(toolsVersion.MajorVersion);
                sb.Append(ConfigurationManager.AppSettings["DeploymentVersionViewSeparator"]);
                sb.Append(toolsVersion.MinorVersion);
            }
            else
            {
                sb.Append(ConfigurationManager.AppSettings["DeploymentVersionViewSeparator"]);
            }

            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="toolsVersion"></param>
        /// <returns></returns>
        public String GetToolsVersionURIString(RSG.Model.Deployment.Tools.ToolsVersion toolsVersion)
        {
            StringBuilder sb = new StringBuilder();
            if (toolsVersion != null)
            {
                sb.Append(toolsVersion.MajorVersion);
                sb.Append(ConfigurationManager.AppSettings["DeploymentVersionURISeparator"]);
                sb.Append(toolsVersion.MinorVersion);
            }
            else
            {
                sb.Append(ConfigurationManager.AppSettings["DeploymentVersionURISeparator"]);
            }

            return sb.ToString();
        }
        #endregion //Public Methods
    }
}  //RSG.ToolsPortal.Classes namespace