﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using RSG.ToolsPortal.Classes;
using RSG.ToolsPortal.ViewModels;

namespace RSG.ToolsPortal.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class HomeController : ApplicationController
    {
        #region Methods / Controllers
        /// <summary>
        /// GET: /Home/ 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new BaseViewModel());
        }
        #endregion //Methods / Controllers
    }
} // RSG.ToolsPortal.Controllers namespace
