﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Configuration;
using System.Xml;
using System.Xml.Linq;

using System.Diagnostics;
using System.Text;
using System.Web.Hosting;

using RSG.Base.Logging;
using RSG.Base.Logging.Universal;

using RSG.Pipeline.Core;
using RSG.Pipeline.Automation.Consumers;
using RSG.Pipeline.Automation.Common.Jobs;


namespace RSG.ToolsPortal.Classes
{
    public class UlogClass
    {
        #region Constants
        /// <summary>
        /// Log context.
        /// </summary>
        private static readonly String LOG_CTX = "Portal's Ulog Helper Class";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Server Host
        /// </summary>
        public Uri ServerHostUri { get; private set; }

        /// <summary>
        /// File Transfer Server Host
        /// </summary>
        public Uri FileTransferServerHost { get; private set; }

        /// <summary>
        /// File Transfer Service Consumer
        /// </summary>
        public FileTransferServiceConsumer Filetransfer { get; private set; }

        /// <summary>
        /// Store the Exceptions of the Page in a Dictionary
        /// </summary>
        public Dictionary<String, Exception> Exceptions { get; private set; }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Log.
        /// </summary>
        private readonly IUniversalLog Log;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="ServerHost"></param>
        public UlogClass(String serverHost)
        {
            this.Exceptions = new Dictionary<String, Exception>();

            LogFactory.Initialize();
            this.Log = LogFactory.CreateUniversalLog(LOG_CTX);

            this.ServerHostUri = new Uri(String.Format(ConfigurationManager.AppSettings["ServerHostTcpFormat"], serverHost));
            this.FileTransferServerHost = new Uri(this.ServerHostUri, ConfigurationManager.AppSettings["FileTransferController"]);
            this.Filetransfer = new FileTransferServiceConsumer(WebUtils.Config, this.FileTransferServerHost);
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// Retrieves the Job object from its ID
        /// </summary>
        /// <param name="jobID"></param>
        /// <returns></returns>
        public Job GetJob(String jobID)
        {
            Job job = null;
            AutomationWrapper automationWrapper = new AutomationWrapper(this.ServerHostUri);

            // Throws an exception when the job Id is not in the format of a guid (32 digits / 4 dashes)
            try
            {
                job = automationWrapper.GetJobFromMonitor(jobID);
            }
            catch (Exception ex)
            {
                this.Exceptions.Add(ex.GetType().ToString(), ex);
            }

            if (job == null)
            {
                Exception ex = new Exception(String.Format("No Job Object for jobID='{0}' returned from the Automation Monitor '{1}'", jobID, automationWrapper.MonitorAdminServerHost));
                this.Exceptions.Add(ex.GetType().ToString(), ex);
            }

            return job;
        }

        /// <summary>
        /// Returns the Content of a ulog file as a String
        /// </summary>
        /// <param name="job">The IJob object </param>
        /// <returns></returns>
        public String GetUlogContent(IJob job)
        {

            String fileContent = null;
            String ulogFilename = ConfigurationManager.AppSettings["UniversalLogFileName"];

            if (job == null)
            {
                return fileContent;
            }

            // Start profiling
            Stopwatch sw = Stopwatch.StartNew();

            String remoteFilename = CheckFileAvailability(job, ulogFilename);
            if (remoteFilename != null)
            {
                fileContent = GetFileContent(job, remoteFilename);
            }
            else
            {
                sw.Reset();
                Exception ex = new FileNotFoundException(String.Format("No '{0}' found for '{1}' on '{2}'",
                                                                        ulogFilename,
                                                                        job.ID,
                                                                        this.FileTransferServerHost)
                                                            );
                this.Exceptions.Add(ex.GetType().ToString(), ex);
            }

            // Stop Profiling
            sw.Stop();

            StringBuilder profilerHeader = new StringBuilder();
            profilerHeader.Append("DateTime");
            profilerHeader.Append(',');
            profilerHeader.Append("ServerHost");
            profilerHeader.Append(',');
            profilerHeader.Append("Job ID");
            profilerHeader.Append(',');
            profilerHeader.Append("Seconds");

            StringBuilder profilerLine = new StringBuilder();
            profilerLine.Append(DateTime.UtcNow.ToString("u"));
            profilerLine.Append(',');
            profilerLine.Append(this.ServerHostUri);
            profilerLine.Append(',');
            profilerLine.Append(job.ID);
            profilerLine.Append(',');
            profilerLine.Append(sw.Elapsed.TotalSeconds);

            String profilerFile = HttpContext.Current.Server.MapPath("~/")
                                + ConfigurationManager.AppSettings["UniversalLogProfilingFullFilePath"];

            try
            {
                if (!File.Exists(profilerFile))
                {
                    using (StreamWriter streamw = File.CreateText(profilerFile))
                    {
                        streamw.WriteLine(profilerHeader.ToString());
                    }
                }

                using (StreamWriter streamw = File.AppendText(profilerFile))
                {
                    streamw.WriteLine(profilerLine.ToString());
                }
            }
            catch (Exception e)
            {
            	// Do something with the exception
                e.Message.Count();
            }

            return fileContent;
        }

        /// <summary>
        /// Checks whether a filename under a job folder exists on the Server via FileTransfer
        /// </summary>
        /// <param name="job">The IJob object</param>
        /// <param name="filename">The lookup filename</param>
        /// <returns>The file name of the remote file</returns>
        public String CheckFileAvailability(IJob job, String filename)
        {
            String remoteFilename = null;

            IEnumerable<String> AvailableFiles = new List<String>();

            try
            {
                AvailableFiles = this.Filetransfer.FileAvailability(job);
            }
            catch (Exception ex)
            {
                this.Exceptions.Add(ex.GetType().ToString(), ex);
            }

            if (AvailableFiles.Count<String>() > 0)
            {
                foreach (String availableFile in AvailableFiles)
                {
                    if (availableFile == filename)
                    {
                        remoteFilename = availableFile;
                        break;
                    }
                }
            }

            return remoteFilename;
        }

        /// <summary>
        /// Downloads a remote file locally, returns its content and finally deletes it
        /// </summary>
        /// <param name="job">The IJob object that the file refers to</param>
        /// <param name="remoteFilename">The filename on the remote server</param>
        /// <returns>The Content as a String from StreamReader</returns>
        public String GetFileContent(IJob job, String remoteFilename)
        {
            String localFile = null;
            String fileContent = null;
            try
            {
                localFile = DownloadJobFile(job, remoteFilename);

                if (localFile != null)
                {
                    StreamReader streamReader = new StreamReader(localFile);
                    fileContent = streamReader.ReadToEnd();
                    streamReader.Close();
                }
                else
                {
                    throw new Exception(String.Format("Could not Download file '{0}' from '{1}' to '{2}'",
                                                        remoteFilename, this.FileTransferServerHost, localFile));
                }

            }
            catch (System.Exception ex)
            {
                this.Exceptions.Add(ex.GetType().ToString(), ex);
            }
            finally
            {
                DeleteLocalFileIfExists(localFile);
            }

            return fileContent;
        }

        public String DownloadJobFile(IJob job, String remoteAndLocalFilename)
        {
            String fileResult = null;

            // This is where FileTransfer.DownloadFile() will store the file
            String fileDirectory = this.Filetransfer.GetClientFileDirectory(job);
            String localFile = Path.Combine(fileDirectory, remoteAndLocalFilename);

            Byte[] md5;
            bool success = this.Filetransfer.DownloadFile(job, remoteAndLocalFilename, remoteAndLocalFilename, out md5);
            if (success)
            {
                fileResult = localFile;
            }

            return fileResult;
        }

        /// <summary>
        /// Returns the Content of a ulog file as an XML Document
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public XDocument GetUlogXDocument(IJob job)
        {
            XDocument xDocument = new XDocument();
            String fileContent = GetUlogContent(job);

            if (fileContent != null)
            {
                try
                {
                    xDocument = XDocument.Parse(fileContent);
                }
                catch (XmlException ex)
                {
                    this.Exceptions.Add(ex.GetType().ToString(), ex);
                }
            }

            return xDocument;
        }

        /// <summary>
        /// Returns the Content of a ulog file as an XML Document
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public List<String> GetErrors(IJob job)
        {
            List<String> errors = new List<String>();
            XDocument xDocument = GetUlogXDocument(job);

            foreach (var systemContext in xDocument.Descendants("context"))
            {
                foreach (var element in systemContext.Elements())
                {
                    LogLevel logLevel = LogLevelUtils.GetLogLevelFromXmlElementName(element.Name.ToString());
                    if ((logLevel == LogLevel.Error) || (logLevel == LogLevel.ToolError))
                    {
                        errors.Add(element.Value);
                    }
                }
            }

            return errors;
        }

        /// <summary>
        /// Deletes a local file if exists
        /// </summary>
        /// <param name="localFile">The file's full path</param>
        public void DeleteLocalFileIfExists(String localFile)
        {
            try
            {
                if (File.Exists(localFile))
                {
                    File.Delete(localFile);
                }
            }
            catch (System.Exception ex)
            {
                Exceptions.Add(ex.GetType().ToString(), ex);
            }
        }
        #endregion // Public Methods
    }

} // RSG.ToolsPortal.Classes namespace