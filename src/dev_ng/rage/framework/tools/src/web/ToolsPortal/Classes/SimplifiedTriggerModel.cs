﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using RSG.Pipeline.Automation.Common.Triggers;

namespace RSG.ToolsPortal.Classes
{
    public class SimplifiedTriggerModel
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public Guid TriggerId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime TriggeredAt { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public uint Changelist { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public String Description { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public String Username { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public String[] Files { get; set; }
        #endregion //Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public SimplifiedTriggerModel() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="jobModel"></param>
        public SimplifiedTriggerModel(ITrigger trigger) 
        {
            //ITrigger trigger = triggerModel.Convert();

            this.TriggerId = trigger.TriggerId;
            this.TriggeredAt = trigger.TriggeredAt;

            if (trigger is IHasDescription)
            {
                this.Description = ((IHasDescription) trigger).Description;
            }
            if (trigger is IHasUsername)
            {
                this.Username = ((IHasUsername) trigger).Username;
            }

            if (trigger is ChangelistTrigger) 
            {
                this.Changelist = ((ChangelistTrigger) trigger).Changelist;
                this.Files = ((ChangelistTrigger) trigger).Files;
            }
        }
        #endregion //Constructor(s)
    }
} //RSG.ToolsPortal.Classes namespace