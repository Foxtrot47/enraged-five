﻿
var AutomationStatus = function (automationStatusObj) {
    var _validationId = "validation",
        _validationUlClass = "validation-summary-errors";

    var _completedRequests,
        _failedRequests;

    function init() {
        update();
        setInterval(update, automationConfig.reloadInterval*5);
    }

    function update() {
        _completedRequests = 0;
        _failedRequests = 0;
        $("#"+ _validationId).empty();

        // Reset all the status cells to the loading image
        $("." + automationStatusObj.statusClassname).html("<span class='service-loading'> </span>");
        // Reset the jobs/clients cells
        $("." + automationStatusObj.taskStatusClassname).html("-");
        $("." + automationStatusObj.jobsClassname).html("-");
        $("." + automationStatusObj.clientsClassname).html("-");

        var trList = $("#" + automationStatusObj.tableId + " tbody").find("tr");
        trList.each(function () {
            var tr = $(this);
            $(tr).removeClass("errors_gradient");

            var serverTd = $(this).find("." + automationStatusObj.serverClassname);
            var serverUrl = serverTd.find("." + automationStatusObj.serverUrlClassname).text().trim();
            var webAlias = serverTd.find("." + automationStatusObj.webAliasClassname).text().trim();
            var jobFilterKey = serverTd.find("." + automationStatusObj.jobFilterKeyClassname).text().trim();
            var jobFilterValue = serverTd.find("." + automationStatusObj.jobFilterValueClassname).text().trim();
            var statusTd = $(this).find("." + automationStatusObj.statusClassname);
            var taskStatusTd = $(this).find("." + automationStatusObj.taskStatusClassname);
            var jobsTd = $(this).find("." + automationStatusObj.jobsClassname);
            var clientsTd = $(this).find("." + automationStatusObj.clientsClassname);

            $.ajax({
                url: automationStatusObj.automationMonitorRest + "/" + webAlias,
                dataType: "json",
                async: true,
                success: function (jsonResult) {

                    var monitor = jsonResult.Monitor;
                    var clients = jsonResult.Clients;
                    // Group the job number per status
                    var jobsPerState = {};
                    var totalJobs = 0;

                    var taskStatusArray = [];

                    $.each(monitor, function (i, task) {
                        taskStatusArray.push({ "Name": task.Name, "TaskState": task.TaskState });

                        $.each(task.Jobs, function (j, job) {
                            // Filter the jobs if specified
                            if (jobFilterKey
                                && job.hasOwnProperty(jobFilterKey)
                                && job[jobFilterKey] != jobFilterValue) {

                                return; // This is equivalent to continue;
                            }
                            totalJobs++;
                            if (jobsPerState.hasOwnProperty(job.State))
                                jobsPerState[job.State]++;
                            else
                                jobsPerState[job.State] = 1;
                        });
                    });
                    statusTd.html("<span class='service-ok'> </span> Normal");

                    // Populate the task status column
                    taskStatusTd.html(
                        taskStatusArray
                            .map(function (d) {
                                var taskHtmlLine = "";
                                if (d.TaskState) {
                                    if (d.TaskState == "Active") {
                                        taskHtmlLine = "<span class='service-ok'></span> " + d.TaskState;
                                    }
                                    else if ((d.TaskState == "Inactive") || (d.TaskState == "Unknown")) {
                                        taskHtmlLine = "<span class='service-error'></span> " + d.TaskState;
                                    }
                                    else {
                                        taskHtmlLine = d.TaskState;
                                    }
                                }
                                return taskHtmlLine;
                            })
                            .join("<br/>")
                    );

                    // Populate the jobs column
                    jobsTd.html("Total: " + totalJobs + "<br/>");

                    var jobsBreakdownArray = [];
                    $.each(Object.keys(jobsPerState).sort(), function (k, jobState) {
                        jobsBreakdownArray.push(
                            "<span class='bgradius " + jobState.toLowerCase() + "'>"
                            + jobState
                            + ": "
                            + jobsPerState[jobState]
                            + "</span>"
                        );
                    });

                    if (jobsBreakdownArray.length > 0) {
                        jobsTd.append(" [ " + jobsBreakdownArray.join(", ") + " ]")
                    }

                    // Clients Column
                    var clientsPerState = {};
                    $.each(clients, function (i, client) {
                        if (clientsPerState.hasOwnProperty(client.State))
                            clientsPerState[client.State]++;
                        else
                            clientsPerState[client.State] = 1;
                    });

                    clientsTd.html("Total: " + clients.length + "<br/>");

                    var clientsBreakdownArray = [];
                    $.each(Object.keys(clientsPerState).sort(), function (k, clientState) {
                        clientsBreakdownArray.push(
                             clientState
                            + ": "
                            + clientsPerState[clientState]
                           );
                    });

                    if (clientsBreakdownArray.length > 0) {
                        clientsTd.append(" [ " + clientsBreakdownArray.join(", ") + " ]")
                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    statusTd.html("<span class='service-error'></span> Error &nbsp;&nbsp;&nbsp;");
                    $(tr).addClass("errors_gradient");
                    _failedRequests++;
                },
                complete: function () {
                    _completedRequests++;
                    //checkFirefoxIssue();
                }
            });

        });

        $("#" + automationStatusObj.generatedAtId)
            .switchClass("", "flash-red", automationConfig.animationDuration)
            .html(toCustomISO(new Date()));
        $("#" + automationStatusObj.generatedAtId).switchClass("flash-red", "", automationConfig.animationDuration);
    }

    function checkFirefoxIssue() {
        // If it's Firefox
        if ($.browser.mozilla) {
            // If all the requests are completed, check their total status
            if ((automationStatusObj.serviceCount == _completedRequests)
                && (automationStatusObj.serviceCount == _failedRequests)) {
                $("#" + _validationId).append(
                    $("<div>")
                        .addClass(_validationUlClass)
                        .append(
                            $("<ul>").append(
                                $("<li>").text("There is a known issue with Firefox and Take Two internal SSL Certificates. "
                                           + "Please contact Tools for more Info.")
                            )
                        )
                );
            }
        }
    }

    return {
        init: init,
    }
}

function enableSorting(tableId) {
    var oTable = $("#" + tableId).dataTable({
        "sScrollX": "100%",
        //"sScrollY": $("#content-body").height() - 100,
        sScrollY: "80%",
        //"sScrollXInner": "100%",
        "bScrollCollapse": true,
        //"sDom": 'C<"clear">lfrtip',
        "bSort": true,            // Disable Sorting
        "bPaginate": false,       // Disable Pagination
        "bFilter": false,         // Disable Filter
        "aaSorting": [],
        "aoColumnDefs": [
            { "bSortable": false, "aTargets": [3, 4] }
        ],
    });
}