﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Net.Mail;

using RSG.Base.Configuration.Automation;
using RSG.Base.Configuration.Portal;

using RSG.ToolsPortal.Classes;

namespace RSG.ToolsPortal.ViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public class BaseViewModel
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public IPortalConfig SelectedProject { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public String Title
        {
            get
            {
                return ConfigurationManager.AppSettings["AppTitle"];
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public String FullTitle
        {
            get
            {
                String title = ConfigurationManager.AppSettings["AppTitle"];
                if (this.SelectedProject != null)
                {
                    title += (this.TitleSeparator + this.SelectedProject.ShortName);
                }

                return title;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public String TitleSeparator
        {
            get
            {
                return ConfigurationManager.AppSettings["AppTitleSeparator"];
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual String IconPath
        {
            get
            {
                if (this.SelectedProject != null)
                {
                    return SelectedProject.IconPath;
                }

                return ConfigurationManager.AppSettings["AppDarkIconPath"];
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public String LogoPath
        {
            get
            {
                return ConfigurationManager.AppSettings["AppLogoPath"];
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public MailAddress ToolsEmail
        {
            get
            {
                return WebUtils.Config.Project.ToolsEmailAddresses.FirstOrDefault();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean InDevMode
        {
            get
            {
                return (ConfigurationManager.AppSettings["DevMode"] != null);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<IPortalConfig> AvailableProjects
        {
            get
            {
                return WebUtils.GetPortalProjects();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public IAutomationServicesConfig AutomationServicesConfig
        {
            get
            {
                return WebUtils.GetProjectAutomationServices(this.SelectedProject);
            }
        }
        #endregion //Properties

        #region Constructor(s)
        public BaseViewModel() { }

        public BaseViewModel(IPortalConfig projectPortalConfig)
        {
            this.SelectedProject = projectPortalConfig;
        }
        #endregion // Constructor(s)
    }

} // RSG.ToolsPortal.ViewModels namespace