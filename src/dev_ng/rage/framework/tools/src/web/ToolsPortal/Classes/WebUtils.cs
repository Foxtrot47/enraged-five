﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Hosting;
using System.Configuration;
using System.ServiceModel;

using RSG.Base.Configuration;
using RSG.Base.Configuration.Automation;
using RSG.Base.Configuration.Bugstar;
using RSG.Base.Configuration.Services;
using RSG.Base.Configuration.Portal;

using RSG.Interop.Bugstar;

using RSG.Deployment.Common.Config;
using RSG.Deployment.Consumers;

namespace RSG.ToolsPortal.Classes
{
    public class WebUtils
    {
        #region Private Properties
        /// <summary>
        /// 
        /// </summary>
        private static IConfig _Config = ConfigFactory.CreateConfig();
        public static IConfig Config { 
            get {return _Config;} 
        }

        /// <summary>
        /// 
        /// </summary>
        public static List<IPortalConfig> Projects { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public static IAutomationServicesConfig ProjectAutomationServicesConfig { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public static DeploymentVersionConsumer DeploymentConsumer { get; private set; }
        #endregion // Private Properties

        #region Static Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<IPortalConfig> GetPortalProjects()
        {
            if (WebUtils.Projects == null)
            {
                WebUtils.Projects = new List<IPortalConfig>();

                String filePath = Path.Combine(HostingEnvironment.ApplicationPhysicalPath,
                                                ConfigurationManager.AppSettings["ProjectsXML"]);
                IEnumerable<PseudoProjectConfig> pseudoProjectConfigs = PseudoProjectConfig.Load(filePath);
                foreach (PseudoProjectConfig pseudoProjectConfig in pseudoProjectConfigs)
                {
                    IPortalConfig portalConfig = ConfigFactory.CreatePortalConfig(Config.Project, pseudoProjectConfig.Path);
                    WebUtils.Projects.Add(portalConfig);
                }
            }
            return WebUtils.Projects;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectRouteName"></param>
        /// <returns></returns>
        public static IPortalConfig GetProjectPortalConfig(String projectRouteName)
        {
            return WebUtils.GetPortalProjects()
                                .Where(
                                    pc => pc.RouteName.Equals(projectRouteName, StringComparison.OrdinalIgnoreCase)
                                )
                                .FirstOrDefault();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectConfig"></param>
        /// <returns></returns>
        //public static IAutomationServicesConfig GetProjectAutomationServices(PseudoProjectConfig projectConfig)
        public static IAutomationServicesConfig GetProjectAutomationServices(IPortalConfig projectPortalConfig)
        {
            if (WebUtils.ProjectAutomationServicesConfig == null)
            {
                WebUtils.ProjectAutomationServicesConfig = ConfigFactory.CreateAutomationServicesConfig(Config.Project);
            }

            return WebUtils.ProjectAutomationServicesConfig;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectConfig"></param>
        /// <param name="webAlias"></param>
        /// <returns></returns>
        //public static IAutomationServiceViewConfig GetProjectAutomationServiceView(PseudoProjectConfig projectConfig, String webAlias)
        public static IAutomationServiceViewConfig GetProjectAutomationServiceView(IPortalConfig projectPortalConfig, String webAlias)
        {
            //return WebUtils.GetProjectAutomationServices(projectConfig).GetServiceView(webAlias);
            return WebUtils.GetProjectAutomationServices(projectPortalConfig).GetServiceView(webAlias);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BugstarConfig"></param>
        /// <returns></returns>
        public static BugstarConnection GetBugstarConnection(IBugstarConfig BugstarConfig)
        {
            BugstarConnection connection = new BugstarConnection(BugstarConfig.RESTService, BugstarConfig.AttachmentService);
            return (connection);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static DeploymentVersionConsumer GetDeploymentConsumer()
        {
            if ((WebUtils.DeploymentConsumer == null) || (WebUtils.DeploymentConsumer.State != CommunicationState.Created))
            {
                IDeploymentConfig configServers = new DeploymentConfig(WebUtils.Config.Project.DefaultBranch);
                IServiceHostConfig dsServerConfig = configServers.Servers[ConfigurationManager.AppSettings["CurrentDeploymentServer"]];

                WebUtils.DeploymentConsumer = new DeploymentVersionConsumer(dsServerConfig);
            }

            return WebUtils.DeploymentConsumer;
        }
        #endregion //Static Methods

    }
} //RSG.ToolsPortal.Classes namespace