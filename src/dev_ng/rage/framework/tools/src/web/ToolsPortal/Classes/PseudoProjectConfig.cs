﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using System.Text.RegularExpressions;

namespace RSG.ToolsPortal.Classes
{   
    /// <summary>
    /// 
    /// </summary>
    public class PseudoProjectConfig
    {
        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public String Path { get; private set; }
        #endregion // Properties

        #region Static Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static IEnumerable<PseudoProjectConfig> Load(String filename)
        {
            XDocument xmlDoc = XDocument.Load(filename);
            IEnumerable<XElement> xmlServiceElems = xmlDoc.Root.Elements("Project");
            return (xmlServiceElems.Select(xmlElem => Load(xmlElem)));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlElem"></param>
        /// <returns></returns>
        public static PseudoProjectConfig Load(XElement xmlElem)
        {
            PseudoProjectConfig config = new PseudoProjectConfig();
            config.Path = xmlElem.Element("Path").Value;
            
            return (config);
        }
        #endregion // Static Methods
    }

} // RSG.ToolsPortal.Classes namespace