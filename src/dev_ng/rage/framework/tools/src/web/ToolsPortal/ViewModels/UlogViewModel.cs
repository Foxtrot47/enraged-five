﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Xml.Linq;

using RSG.Pipeline.Core;
using RSG.Pipeline.Automation.Consumers;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Tasks;

using RSG.ToolsPortal.Classes;

namespace RSG.ToolsPortal.ViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public class UlogViewModel : BaseViewModel
    {
        #region Properties
        /// <summary>
        /// Override The IconPath
        /// </summary>
        public override String IconPath
        {
            get
            {
                return ConfigurationManager.AppSettings["AppLogIconPath"];
            }
        }

        /// <summary>
        /// </summary>
        public String ServerHost { get; private set; }

        /// <summary>
        /// Property for storing the Job object
        /// </summary>
        public Job Job { get; private set; }

        /// <summary>
        /// Property for storing the ErrorsOnly flag as bool
        /// </summary>
        public bool ErrorsOnly { get; private set; }

        /// <summary>
        /// Property for storing the Ulog Content as an XML Doc
        /// </summary>
        public XDocument UlogXDocument { get; private set; }

        /// <summary>
        /// Property for storing the flag to render the download build monitor file button 
        /// </summary>
        public bool HasBuildMonitorFile { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor
        /// </summary>
        public UlogViewModel() : base() { }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="serverHost"></param>
        /// <param name="jobId"></param>
        /// <param name="ulogXDocument"></param>
        public UlogViewModel(String serverHost, Job job, Boolean errorsOnly, XDocument ulogXDocument, bool hasBuildMonitorFile)
            : base()
        {
            this.ServerHost = serverHost;
            this.Job = job;
            this.ErrorsOnly = errorsOnly;
            this.UlogXDocument = ulogXDocument;
            this.HasBuildMonitorFile = hasBuildMonitorFile;
        }
        #endregion // Constructor(s)
    }

} // RSG.ToolsPortal.ViewModels namespace