﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

using RSG.ToolsPortal.Classes;

namespace RSG.ToolsPortal.ViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public class LoginViewModel
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "Username: ")]
        public String UserName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "Password: ")]
        public String Password { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "Remember Me")]
        public Boolean RememberMe { get; set; }
        #endregion //Properties

    } //LoginViewModel

} // RSG.ToolsPortal.ViewModels namespace