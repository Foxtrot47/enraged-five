﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using RSG.Pipeline.Automation.Common.Jobs;

namespace RSG.ToolsPortal.Classes
{
    /// <summary>
    /// AutomationGroupedJobs Class used to serialise Grouped by Trigger Automation Jobs
    /// for the JobMatrix View
    /// </summary>
    public class AutomationGroupedJobs
    {
        #region Properties
        /// <summary>
        /// The Consumer Jobs stored separately
        /// </summary>
        public IDictionary<String, SimplifiedJobModel> ConsumerJobs { get; set; }

        /// <summary>
        /// The Column Groups Found in the data
        /// </summary>
        public IDictionary<String, String> ColumnGroups { get; set; }

        /// <summary>
        /// The Jobs Grouped by the same trigger in the Class AutomationTriggerGroupedJobs
        /// </summary>
        public IEnumerable<AutomationTriggerGroupedJobs> TriggerGroups { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IDictionary<String, JobResult> JobResults { get; set; }

        /// <summary>
        /// DateTime of when the data structure has been generated.
        /// Very useful when the object is loaded from the file
        /// </summary>
        public DateTime GeneratedAt { get; set; }
        #endregion // Properties

        #region Constructor(s)
        public AutomationGroupedJobs()  {
            this.ColumnGroups = new Dictionary<String, String>();
            this.GeneratedAt = DateTime.UtcNow;
        }
        #endregion // Constructor(s)
    }

} // RSG.ToolsPortal.Classes namespace