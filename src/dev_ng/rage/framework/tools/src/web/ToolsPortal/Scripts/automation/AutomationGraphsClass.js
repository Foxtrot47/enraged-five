﻿/*
* Class AutomationGraphs
*
* Requires jQuery, generic.js, nvd3-linegraph.js, nvd3-stackedareagraph.js
*
*/

var AutomationGraphs = function (automationGraphsConfig) {
    var _elementId = automationGraphsConfig.elementId,
        _graphTypeSelectId = automationGraphsConfig.graphTypeSelectId,
        _graphMetricSelectId = automationGraphsConfig.graphMetricSelectId,
        _automationStatsRest = automationGraphsConfig.automationStatsRest,
        _automationStatsCSV = automationGraphsConfig.automationStatsCSV,

        _availableGraphTypes = [
            {
                title: "Line Graph",
                function: drawLineGraph,
            },
            {
                title: "Stacked Area Graph",
                function: drawStackedAreaGraph,
            },
            {
                title: "Tabular Data",
                function: generateTabularReport,
                isGraphData: true,
            },
        ],

        // The list of available graph metrics
        _availableGraphMetrics = [
            {
                title: "Created Jobs",
                yLabel: "Created Jobs",
                formatY: d3.format("d"),
                getY: function (d) { return d.CreatedJobs; },
            },
            {
                title: "Completed Jobs",
                yLabel: "Completed Jobs",
                formatY: d3.format("d"),
                getY: function (d) { return d.CompletedJobs; },
            },
            {
                title: "Pending Jobs",
                yLabel: "Pending Jobs",
                formatY: d3.format("d"),
                getY: function (d) { return d.PendingJobs; },
            },
            {
                title: "Assigned Jobs",
                yLabel: "Assigned Jobs",
                formatY: d3.format("d"),
                getY: function (d) { return d.AssignedJobs; },
            },
            {
                title: "Errored Jobs",
                yLabel: "Errored Jobs",
                formatY: d3.format("d"),
                getY: function (d) { return d.ErroredJobs; },
            },
            {
                title: "Skipped Jobs",
                yLabel: "Skipped Jobs",
                formatY: d3.format("d"),
                getY: function (d) { return d.SkippedJobs; },
            },
            {
                title: "Skipped Consumed Jobs",
                yLabel: "Skipped Consumed Jobs",
                formatY: d3.format("d"),
                getY: function (d) { return d.SkippedConsumedJobs; },
            },
            {
                title: "Aborted Jobs",
                yLabel: "Aborted Jobs",
                formatY: d3.format("d"),
                getY: function (d) { return d.AbortedJobs; },
            },
            {
                title: "Client Errored Jobs",
                yLabel: "Client Errored Jobs",
                formatY: d3.format("d"),
                getY: function (d) { return d.ClientErroredJobs; },
            },
	        {
	            title: "Average Processing Time",
	            yLabel: "Average Processing Time (hh:mm:ss)",
	            formatY: function (d) { return formatSecs(d); },
	            getY: function (d) { return (d.CompletedJobs) ? ((d.TotalProcessingTime / 1000) / d.CompletedJobs) : 0; },
	        },
            {
                title: "Average Lag Time",
                yLabel: "Average Lag Time (hh:mm:ss)",
                formatY: function (d) { return formatSecs(d); },
                getY: function (d) { return (d.CompletedJobs) ? ((d.TotalLagTime / 1000) / d.CompletedJobs) : 0; },
            },
        ],

        // The object with the graph parameters
        _graphOptions = {
            margin: { top: 80, right: 80, bottom: 80, left: 80 },

            xLabel: "",
            rotateXLabel: -45,

            getX: function (d) { return parseJsonDate(d.Date); },
            getXTick: function (d) { return d3.time.format("%d %b %Y")(new Date(d)); },

            yLabel: null,
            getY: null,
            getYTick: null,

            xTickValues: [],
        },

        _reportOptions = {
            reportTitle: automationGraphsConfig.reportTitle,
            getKey: function (d) { return d.key; },
            getValues: function (d) { return d.Values; },

            xLabel: "Date",
            getX: function (d) { return parseJsonDate(d.Date); },
            formatX: function (d) { return d3.time.format("%d %b %Y")(d); },
            
            availableMetrics: _availableGraphMetrics,
            csvDownloadUrl: _automationStatsCSV,
        },

        _datum = [];

    function init() {
        var storedGraphType = retrieveLocalObject(config.fullWebPath + _graphTypeSelectId);
        var storedGraphMetric = retrieveLocalObject(config.fullWebPath + _graphMetricSelectId);

        // Populate the select options and bind a redraw function on change
        $.each(_availableGraphTypes, function (i, availableGraphType) {
            var option = $("<option />");
            option.text(availableGraphType.title)
                    .attr("title", availableGraphType.title)
                    .val(i);
            if ((storedGraphType != null) && (storedGraphType == i)) {
                option.attr("selected", true);
            }
            $("select#" + _graphTypeSelectId).append(option);
        });

        $("select#" + _graphTypeSelectId).change(function () {
            cleanGraph(_elementId);
            drawGraph();
        });

        $.each(_availableGraphMetrics, function (i, availableGraphMetric) {
            var option = $("<option />");
            option.text(availableGraphMetric.title)
                    .attr("title", availableGraphMetric.title)
                    .val(i)
            if ((storedGraphMetric != null) && (storedGraphMetric == i)) {
                option.attr("selected", true);
            }
            $("select#" + _graphMetricSelectId).append(option);
        });
        $("select#" + _graphMetricSelectId).change(function () {
            drawGraph();
        });

        // Call the Draw function
        drawGraph();
    }
    
    function drawGraph() {
        var selectedGraphId = parseInt($("select#" + _graphTypeSelectId).val());
        var selectedGraph = _availableGraphTypes[selectedGraphId];
        storeLocalObject(config.fullWebPath + _graphTypeSelectId, selectedGraphId);

        var selectedMetricId = parseInt($("select#" + _graphMetricSelectId).val());
        var selectedMetric = _availableGraphMetrics[selectedMetricId];
        storeLocalObject(config.fullWebPath + _graphMetricSelectId, selectedMetricId);

        // Populate the selected 
        _graphOptions.yLabel = selectedMetric.yLabel;
        _graphOptions.getY = selectedMetric.getY;
        _graphOptions.getYTick = selectedMetric.formatY;


        // If this is the first time called, need to get the data via ajax
        if (_datum.length == 0) {

            $.ajax({
                url: _automationStatsRest,
                dataType: "json",
                async: false,
                success: function (jsonStats) {

                    var tickValuesDict = {};

                    $.each(jsonStats, function (i, host) {
                        var key = host.HostName;
                        var values = host.DailyStats;
                        if (_graphOptions.xTickValues.length == 0) {
                            _graphOptions.xTickValues = values.map(function (dailyStat) { return parseJsonDate(dailyStat.Date); });
                        }

                        _datum.push({
                            key: key,
                            values: values,
                        });

                    });

                    drawChosenGraph(selectedGraph);
                },
            });
        } // 
        else {
            drawChosenGraph(selectedGraph);
        }

    } // init()

    function drawChosenGraph(selectedGraph) {
        if (!hasSvg(_elementId)) {
            $("#" + _elementId).empty();
            initSvg(_elementId);
        }
        $("select#" + _graphMetricSelectId).attr("disabled", false);

        if (selectedGraph.function !== undefined) {
            if (selectedGraph.isGraphData) {
                clearSvg(_elementId);
                $("select#" + _graphMetricSelectId).attr("disabled", true);
                selectedGraph.function(_elementId, _reportOptions, _datum);
            }
            else {
                selectedGraph.function(_elementId, _graphOptions, _datum);
            }
        }
        else {
            console.error("The Graph module hasn't been imported!");
        }
    }

    return { init: init };
}