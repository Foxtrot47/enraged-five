﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSG.ToolsPortal.Classes
{
    /// <summary>
    /// AutomationTriggerGroupedJobs Class used to serialise Grouped by Trigger Automation Jobs for the JobMatrix View.
    /// The Trigger Info is stored separately so the rest of the jobs contain only the basic information for the report.
    /// </summary>
    public class AutomationTriggerGroupedJobs
    {
        #region Properties
        /// <summary>
        /// The Job object with the Trigger info
        /// </summary>
        public SimplifiedJobTriggerModel GroupJobTrigger { get; set; }

        /// <summary>
        /// The Grouped Jobs with only the info required by the report
        /// </summary>
        public IDictionary<String, SimplifiedJobModel> GroupJobsDict { get; set; }
        #endregion // Properties

        #region Constructor(s)
        public AutomationTriggerGroupedJobs()
        {
            this.GroupJobTrigger = null;
            this.GroupJobsDict = new Dictionary<String, SimplifiedJobModel>();
        }
        #endregion // Constructor(s)
    }
} // RSG.ToolsPortal.Classes namespace