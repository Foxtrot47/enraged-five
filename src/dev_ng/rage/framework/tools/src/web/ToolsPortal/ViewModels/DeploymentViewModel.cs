﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using RSG.Base.Configuration.Automation;
using RSG.Base.Configuration.Portal;

using RSG.Deployment.Consumers;

using RSG.Model.Deployment.Game;
using RSG.Model.Deployment.Tools;

using RSG.ToolsPortal.Classes;

namespace RSG.ToolsPortal.ViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public class DeploymentViewModel : BaseViewModel
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<GameBuildVersion> AllGameBuildVersions { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<GameBuildTracking> AllGameBuildMachineTracking { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<ToolsVersion> AllToolsVersions { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public GameBuildVersion SelectedGameBuildVersion { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ToolsVersion SelectedToolsVersion { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public String CurrentGameBuildVersionString { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public String NextGameBuildVersionString { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public String LocalBuildVersionString { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public bool ServiceIsUp { get; private set; }

        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public DeploymentViewModel() : base() { }

        public DeploymentViewModel(IPortalConfig projectPortalConfig, DeploymentWrapper deploymentWrapper, String clientMachine)
            : this(projectPortalConfig, deploymentWrapper)
        {
               this.LocalBuildVersionString = deploymentWrapper.GetGameBuildTrackingString(
                                                    deploymentWrapper.GetLocalGameBuildTracking(clientMachine)
                                            );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectConfig"></param>
        /// <param name="deploymentWrapper"></param>
        public DeploymentViewModel(IPortalConfig projectPortalConfig, DeploymentWrapper deploymentWrapper)
            : base(projectPortalConfig)
        {
            this.ServiceIsUp = deploymentWrapper.ServiceIsUp;
            this.AllGameBuildVersions = deploymentWrapper.AllGameBuildVersions;
            this.AllGameBuildMachineTracking = deploymentWrapper.AllGameBuildMachineTracking;
            this.AllToolsVersions = deploymentWrapper.AllToolsVersions;
            this.CurrentGameBuildVersionString = deploymentWrapper.GetGameBuildVersionViewString(deploymentWrapper.CurrentGameBuildVersion);
            this.NextGameBuildVersionString = deploymentWrapper.GetGameBuildVersionViewString(deploymentWrapper.NextGameBuildVersion);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectConfig"></param>
        /// <param name="gameVersion"></param>
        public DeploymentViewModel(IPortalConfig projectPortalConfig, GameBuildVersion gameBuildVersion)
            : base(projectPortalConfig)
        {
            this.SelectedGameBuildVersion = gameBuildVersion;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectConfig"></param>
        /// <param name="toolsVersion"></param>
        public DeploymentViewModel(IPortalConfig projectPortalConfig, ToolsVersion toolsVersion)
            : base(projectPortalConfig)
        {
            this.SelectedToolsVersion = toolsVersion;
        }

        #endregion //Constructor(s)
    }

} // RSG.ToolsPortal.ViewModels namespace