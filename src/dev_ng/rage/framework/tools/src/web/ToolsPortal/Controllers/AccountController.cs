﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Configuration.Provider;

using RSG.Base.Configuration;
using RSG.Base.Configuration.Bugstar;

using RSG.Interop.Bugstar;
using RSG.Interop.Bugstar.Organisation;

using RSG.Interop.Microsoft;

using RSG.ToolsPortal.Classes;
using RSG.ToolsPortal.ViewModels;

namespace RSG.ToolsPortal.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class AccountController : ApplicationController
    {
        #region Methods / Controllers
        /// <summary>
        /// GET: /Account/Login 
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View(new LoginViewModel());
        }

        /// <summary>
        /// POST: /Account/Login 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {

                // Try to authenticate to Active Directory and get the user properties
                IDictionary<String, Object> userProperties;
                bool ADAuthenticated = ActiveDirectory.TryGetUserProperties(
                                    model.UserName,
                                    model.Password,
                                    WebUtils.Config.Studios.ThisStudio.ActiveDirectoryDomain,
                                    out userProperties);
                // We can get user info from userProperties dict;
                // userProperties.ContainsKey("mail");

                if (!ADAuthenticated)
                {
                    // If the credentials are invalid don't proceed to bugstar auth
                    ModelState.AddModelError("", "Invalid Username / Password!");

                    return View(model); // Redisplay form
                }

                /* Try to Connect to Bugstar */
                IBugstarConfig bugstarConfig = ConfigFactory.CreateBugstarConfig(WebUtils.Config);
                BugstarConnection bugstarConnection = WebUtils.GetBugstarConnection(bugstarConfig);
                IEnumerable<Project> bugstarProjects;

                try
                {
                    bugstarConnection.Login(model.UserName, model.Password, bugstarConfig.UserDomain);

                    FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);

                    // Retrieve the bugstar project.
                    bugstarProjects = Project.GetProjects(bugstarConnection);
                }
                catch (Exception e)
                {
                    e.ToString();
                    // Catch Any Bugstar Exception
                    ModelState.AddModelError("", "Failed to connect / authenticate to Bugstar!");

                    return View(model); // Redisplay form
                }
                finally
                {
                    // Code to free the resources?
                    //bugstarConnection.
                }

                // Setup the Roles 
                try
                {
                    foreach (Project project in bugstarProjects)
                    {
                        if (!Roles.RoleExists(project.Id.ToString()))
                            Roles.CreateRole(project.Id.ToString());

                        if (!Roles.IsUserInRole(model.UserName, project.Id.ToString()))
                            Roles.AddUserToRole(model.UserName, project.Id.ToString());
                    }

                    // If everything went ok return to previous url
                    return RedirectToLocal(returnUrl);
                }
                catch (Exception pe)
                {
                    ModelState.AddModelError("", pe.Message);
                }
        
            }

            // If we got this far, something failed, redisplay the form
            return View(model);
        }

        /// <summary>
        /// POST: /Account/LogOff 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Login");
        }
        #endregion //Methods / Controllers

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        #endregion //Private Methods
  
    }

} // ToolsPortal.Controllers namespace
