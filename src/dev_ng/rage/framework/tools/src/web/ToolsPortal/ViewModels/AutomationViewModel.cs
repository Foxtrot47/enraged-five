﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

using RSG.Base.Configuration.Automation;
using RSG.Base.Configuration.Portal;

using RSG.ToolsPortal.Classes;

namespace RSG.ToolsPortal.ViewModels
{   
    /// <summary>
    /// 
    /// </summary>
    public class AutomationViewModel : BaseViewModel
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public IAutomationServiceViewConfig SelectedView { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public String JobId { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectConfig"></param>
        /// <param name="automationService"></param>
        public AutomationViewModel(IPortalConfig projectPortalConfig, IAutomationServiceViewConfig automationServiceView)
            : base(projectPortalConfig)
        {
            this.SelectedView = automationServiceView;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectConfig"></param>
        /// <param name="automationServiceView"></param>
        /// <param name="jobId"></param>
        public AutomationViewModel(IPortalConfig projectPortalConfig, IAutomationServiceViewConfig automationServiceView, String jobId)
            : base(projectPortalConfig)
        {
            this.SelectedView = automationServiceView;
            this.JobId = jobId;
        }


        #endregion //Constructor(s)
    }

} // RSG.ToolsPortal.ViewModels namespace