﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using RSG.Pipeline.Automation.Common;
using ServiceStack.Text;

namespace RSG.ToolsPortal.Controllers
{
    public abstract class ApplicationController : Controller
    {
        /*
        protected ActionResult RespondTo(Action<FormatCollection> format)
        {
            return new FormatResult(format);
        }
        */

        protected override JsonResult Json(object data, string contentType, Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new ServiceStackJsonResult
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding
            };
        }
    }

    public class ServiceStackJsonResult : JsonResult
    {
        public override void ExecuteResult(ControllerContext context)
        {
            HttpResponseBase response = context.HttpContext.Response;
            response.ContentType = !String.IsNullOrEmpty(ContentType) ? ContentType : "application/json";

            //JsConfig.ExcludeTypeInfo = true;
            JsConfig<Guid>.SerializeFn = guid => guid.ToString();
            JsConfig<TimeSpan>.SerializeFn = timespan => timespan.TotalSeconds.ToString();
            JsConfig<CapabilityType>.SerializeFn = type => type.ToString();

            if (ContentEncoding != null)
            {
                response.ContentEncoding = ContentEncoding;
            }

            // Compress the responses to gzip
            response.AddHeader("Content-Encoding", "gzip");
            response.Filter = new GZipStream(response.Filter, CompressionMode.Compress);

            if (Data != null)
            {
                response.Write(JsonSerializer.SerializeToString(Data));
            }
        }
    }
}
