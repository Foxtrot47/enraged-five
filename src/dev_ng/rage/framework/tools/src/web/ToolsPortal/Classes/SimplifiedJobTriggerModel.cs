﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using RSG.Pipeline.Automation.Common.Jobs;

namespace RSG.ToolsPortal.Classes
{
    public class SimplifiedJobTriggerModel : SimplifiedJobModel
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public SimplifiedTriggerModel Trigger { get; set; }
        #endregion //Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public SimplifiedJobTriggerModel() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="jobModel"></param>
        public SimplifiedJobTriggerModel(IJob jobModel)
            : base(jobModel)
        {
            this.Trigger = new SimplifiedTriggerModel(jobModel.Trigger);
        }
        #endregion //Constructor(s)
    }
} //RSG.ToolsPortal.Classes namespace