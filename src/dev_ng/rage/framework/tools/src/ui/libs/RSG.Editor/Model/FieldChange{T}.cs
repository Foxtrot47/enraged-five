﻿//---------------------------------------------------------------------------------------------
// <copyright file="FieldChange{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Model
{
    using System;
    using System.Reflection;

    /// <summary>
    /// Implements the <see cref="RSG.Editor.Model.IChange"/> interface for a single field
    /// change. This can be used when the field type and property type do not match.
    /// </summary>
    /// <typeparam name="T">
    /// The type of field that has changed.
    /// </typeparam>
    internal class FieldChange<T> : ChangeBase
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Model"/> property.
        /// </summary>
        private WeakReference<NotifyPropertyChangedBase> _model;

        /// <summary>
        /// The name of the property that changed.
        /// </summary>
        private string _name;

        /// <summary>
        /// The new value that the property is set to.
        /// </summary>
        private T _newValue;

        /// <summary>
        /// The old value of the property that changed.
        /// </summary>
        private T _oldValue;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="FieldChange{T}"/> class with the
        /// specified old and new values.
        /// </summary>
        /// <param name="oldValue">
        /// The value of the property before the change.
        /// </param>
        /// <param name="newValue">
        /// The value of the property after the change.
        /// </param>
        /// <param name="model">
        /// The model whose property changed.
        /// </param>
        /// <param name="name">
        /// The name of the property that changed.
        /// </param>
        public FieldChange(T oldValue, T newValue, NotifyPropertyChangedBase model, string name)
        {
            if (model == null)
            {
                throw new SmartArgumentNullException(() => model);
            }

            this._oldValue = oldValue;
            this._newValue = newValue;
            this._name = name;
            this._model = new WeakReference<NotifyPropertyChangedBase>(model);
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Redoes this single change.
        /// </summary>
        public override void Redo()
        {
            this.SetProperty(this._newValue);
        }

        /// <summary>
        /// Undoes this single change.
        /// </summary>
        public override void Undo()
        {
            this.SetProperty(this._oldValue);
        }

        /// <summary>
        /// Sets the field that this change is associated with to the specified value.
        /// </summary>
        /// <param name="value">
        /// The value to set the field that is associated with this change.
        /// </param>
        private void SetProperty(T value)
        {
            throw new NotImplementedException("This doesn't work as you'd expect due to the way it tries to determine the fieldName.");
            //NotifyPropertyChangedBase target = null;
            //if (!this._model.TryGetTarget(out target))
            //{
            //    return;
            //}

            //string fieldName = "_" + this._name.ToLower();
            //BindingFlags flags =
            //    BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
            //FieldInfo field = target.GetType().GetField(fieldName, flags);
            //if (field == null)
            //{
            //    return;
            //}

            //field.SetValue(target, value);
            //target.NotifyPropertyChanged(this._name);
        }
        #endregion Methods
    } // RSG.Editor.Model.FieldChange {Class}
} // RSG.Editor.Model.UndoRedo {Namespace}
