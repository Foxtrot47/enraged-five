﻿//---------------------------------------------------------------------------------------------
// <copyright file="SearchFieldType.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Search
{
    /// <summary>
    /// Defines the different typed searches supported.
    /// </summary>
    public enum SearchFieldType
    {
        /// <summary>
        /// No specific type. Same as invalid or empty.
        /// </summary>
        None,

        /// <summary>
        /// A string value is being searched for.
        /// </summary>
        String,

        /// <summary>
        /// A boolean value is being searched for.
        /// </summary>
        Boolean,

        /// <summary>
        /// A numerical value is being searched for.
        /// </summary>
        Numeric,
    } // RSG.Editor.Controls.Search.SearchFieldType {Enum}
} // RSG.Editor.Controls.Search {Namespace}
