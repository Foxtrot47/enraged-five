﻿//---------------------------------------------------------------------------------------------
// <copyright file="ISupportsExpansionEvents.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    using System.ComponentModel;

    /// <summary>
    /// When implemented represents a object that has methods related to the item getting
    /// expanded and collapsed in the user interface so that it can respond accordingly.
    /// </summary>
    public interface ISupportsExpansionEvents
    {
        #region Methods
        /// <summary>
        /// Called just before this object in the user interface is expanded inside a
        /// hierarchical control.
        /// </summary>
        void BeforeExpand();

        /// <summary>
        /// Called just after this object in the user interface is collapsed inside a
        /// hierarchical control.
        /// </summary>
        void AfterCollapse();
        #endregion Methods
    } // RSG.Editor.View.ISupportsExpansionEvents {Interface}
} // RSG.Editor.View {Namespace}
