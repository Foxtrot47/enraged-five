﻿//---------------------------------------------------------------------------------------------
// <copyright file="CommandType.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    /// <summary>
    /// Defines the different ways a command bar item can be displayed to the user.
    /// </summary>
    public enum CommandType
    {
        /// <summary>
        /// Specifies that the command is to be displayed as a batch control.
        /// </summary>
        Batch,

        /// <summary>
        /// Specifies that the command is to be displayed as a button.
        /// </summary>
        Button,

        /// <summary>
        /// Specifies that the command is to be displayed as a combo box.
        /// </summary>
        Combo,

        /// <summary>
        /// Specifies that the command is to be displayed as a controller item.
        /// </summary>
        Controller,

        /// <summary>
        /// Specifies that the command is to be displayed as a text box.
        /// </summary>
        Text,

        /// <summary>
        /// Specifies that the command is to be displayed as a check box.
        /// </summary>
        Toggle
    } // RSG.Editor.CommandType {Enumeration}
} // RSG.Editor {Namespace}
