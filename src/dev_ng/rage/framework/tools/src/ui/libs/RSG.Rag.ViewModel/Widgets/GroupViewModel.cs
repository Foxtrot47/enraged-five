﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using RSG.Rag.Core;
using RSG.Rag.Widgets;

namespace RSG.Rag.ViewModel.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    public class GroupViewModel : GroupBaseViewModel<WidgetGroup>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        /// <param name="vmFactory"></param>
        public GroupViewModel(WidgetGroup widget, WidgetViewModelFactory vmFactory)
            : base(widget, vmFactory)
        {
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Gets the bitmap source that is used to display the icon for this item.
        /// </summary>
        public override BitmapSource Icon
        {
            get { return CommonRagIcons.Group; }
        }
        #endregion // Properties
    } // GroupViewModel
}
