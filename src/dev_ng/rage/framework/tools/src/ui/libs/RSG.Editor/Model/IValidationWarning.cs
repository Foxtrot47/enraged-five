﻿//---------------------------------------------------------------------------------------------
// <copyright file="IValidationWarning.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Model
{
    /// <summary>
    /// When implemented represents a object that describes a warning that has been found
    /// through validating a model object.
    /// </summary>
    public interface IValidationWarning : IValidationItem
    {
    } // RSG.Editor.Model.IValidationWarning {Interface}
} // RSG.Editor.Model {Namespace}
