﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace RSG.Editor.Controls.ZoomCanvas
{
    /// <summary>
    /// 
    /// </summary>
    public class RoutedValueChangedEventArgs<T> : RoutedEventArgs
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="OldValue"/> property.
        /// </summary>
        private T _oldValue;

        /// <summary>
        /// The private field used for the <see cref="NewValue"/> property.
        /// </summary>
        private T _newValue;
        #endregion Fields

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="RoutedValueChangedEventArgs{T}"/>
        /// </summary>
        /// <param name="routedEvent">
        /// The routed event identifier for this instance.
        /// </param>
        /// <param name="oldValue">
        /// The value before the changed occurred.
        /// </param>
        /// <param name="newValue">
        /// The value after the changed occurred.
        /// </param>
        public RoutedValueChangedEventArgs(RoutedEvent routedEvent, T oldValue, T newValue)
            : base(routedEvent)
        {
            this._oldValue = oldValue;
            this._newValue = newValue;
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Gets the value before the changed occurred.
        /// </summary>
        public T OldValue
        {
            get { return this._oldValue; }
        }

        /// <summary>
        /// Gets the value after the changed occurred.
        /// </summary>
        public T NewValue
        {
            get { return this._newValue; }
        }
        #endregion Properties
    } // RoutedValueChangedEventArgs<T>
}
