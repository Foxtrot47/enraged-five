﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using RSG.Automation.ViewModel;
using RSG.Pipeline.Automation.Common.Triggers;

namespace RSG.Automation.View
{

    /// <summary>
    /// DataTemplate selector for Job Trigger.
    /// </summary>
    internal class TriggerTemplateSelector : DataTemplateSelector
    {
        /// <summary>
        /// DataTemplate for use with Changelist Trigger.
        /// </summary>
        public DataTemplate ChangelistTriggerTemplate { get; set; }

        /// <summary>
        /// DataTemplate for use with User Trigger.
        /// </summary>
        public DataTemplate UserTriggerTemplate { get; set; }

        /// <summary>
        /// Default (fallback) template.
        /// </summary>
        public DataTemplate DefaultTemplate { get; set; }

        /// <summary>
        /// Template selection.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="container"></param>
        /// <returns></returns>
        public override DataTemplate SelectTemplate(Object item, DependencyObject container)
        {
            if (null == item)
                return (this.DefaultTemplate);

            JobViewModel jobVM = null;
            if (item is JobViewModel)
                jobVM = (JobViewModel)item;
            else if (item is JobDownloadViewModel)
                jobVM = ((JobDownloadViewModel)item).Job;

            if (jobVM.Trigger is ChangelistTrigger)
                return (this.ChangelistTriggerTemplate);
            else if (jobVM.Trigger is UserRequestTrigger)
                return (this.UserTriggerTemplate);
            else
                return (this.DefaultTemplate);
        }
    }

} // RSG.Automation.View namespace
