﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using RSG.Editor.Controls.Helpers;
using RSG.Editor.Controls.ZoomCanvas;

namespace RSG.Editor.Controls.MapViewport.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public class ZoomExtentsAction : ButtonAction<MapViewport>
    {
        #region Constants
        /// <summary>
        /// Number of pixels (in viewport space) to pad the zoom by.
        /// </summary>
        private const double _zoomPadding = 10.0;
        #endregion
    
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="ZoomAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public ZoomExtentsAction(ParameterResolverDelegate<MapViewport> resolver)
            : base(resolver)
        {
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="control">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(MapViewport control)
        {
            Size adjustedRenderSize = new Size(
                control.RenderSize.Width - 2.0 * _zoomPadding,
                control.RenderSize.Height - 2.0 * _zoomPadding);

            Rect extents = Rect.Empty;
            foreach (MapLayer layer in control.GetVisualDescendents<MapLayer>())
            {
                if (layer.Visibility == Visibility.Visible)
                {
                    MapLayerPanel panel = layer.GetVisualDescendent<MapLayerPanel>();
                    if (panel != null)
                    {
                        extents.Union(panel.Extents);
                    }
                }
            }

            // Make sure that there is something displayed in the viewport.
            if (extents == Rect.Empty)
            {
                return;
            }

            // Adjust the extents by the scale.
            extents.Union(new Point(extents.TopLeft.X / control.ZoomCanvas.Scale.X, extents.TopLeft.Y / control.ZoomCanvas.Scale.Y));
            extents.Union(new Point(extents.BottomRight.X / control.ZoomCanvas.Scale.X, extents.BottomRight.Y / control.ZoomCanvas.Scale.Y));

            Vector newScale = control.ZoomCanvas.Scale;
            if (extents.Height > extents.Width)
            {
                newScale.X = adjustedRenderSize.Height / extents.Height;
                newScale.Y = adjustedRenderSize.Height / extents.Height;
            }
            else
            {
                newScale.X = adjustedRenderSize.Width / extents.Width;
                newScale.Y = adjustedRenderSize.Width / extents.Width;
            }

            Point newOffset = control.ZoomCanvas.Offset;
            newOffset.X = (extents.X + extents.Width / 2) * newScale.X;
            newOffset.X -= adjustedRenderSize.Width / 2 + _zoomPadding;
            newOffset.Y = (extents.Y + extents.Height / 2) * newScale.Y;
            newOffset.Y -= adjustedRenderSize.Height / 2 + _zoomPadding;

            control.ZoomCanvas.Scale = newScale;
            control.ZoomCanvas.Offset = newOffset;
        }
        #endregion
    }
}
