﻿//---------------------------------------------------------------------------------------------
// <copyright file="CreateDocumentContentController.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Project.Controllers
{
    using System;
    using System.IO;
    using System.Xml;
    using RSG.Metadata.Model.Definitions;
    using RSG.Metadata.Model.Tunables;
    using RSG.Metadata.ViewModel.Tunables;
    using RSG.Project.ViewModel;

    /// <summary>
    /// Used to create the document content for a project node that is representing a local
    /// dialogue file.
    /// </summary>
    public class CreateDocumentContentController : ICreateDocumentContentController
    {
        #region Methods
        /// <summary>
        /// Creates content to be used inside a document for the specified hierarchy node.
        /// </summary>
        /// <param name="node">
        /// The node whose content will be created.
        /// </param>
        /// <param name="e">
        /// A structure containing objects that can be used during construction, i.e. a log
        /// object to log errors and warnings to and the view site that the document is being
        /// opened in.
        /// </param>
        /// <returns>
        /// The object that represents the document content for the specified node if created;
        /// otherwise, null.
        /// </returns>
        /// <remarks>
        /// If for some reason the document shouldn't be created throw a
        /// <see cref="System.OperationCanceledException"/>.
        /// </remarks>
        public object CreateContent(HierarchyNode node, CreateContentArgs e)
        {
            string fullPath = node.ProjectItem.GetMetadata("FullPath");
            ProjectNode project = node.ParentProject;
            if (fullPath == null)
            {
                throw new NotSupportedException("Unable to retrieve full path from node.");
            }

            if (!File.Exists(fullPath))
            {
                throw new FileNotFoundException("Unable to open node", fullPath);
            }

            StandardProjectNode metadataProject = project as StandardProjectNode;
            IDefinitionDictionary definitions = null;
            if (metadataProject != null)
            {
                metadataProject.EnsureDefinitionsAreLoaded();
                definitions = metadataProject.MetadataDefinitions;
            }

            TunableStructure tunableStructure = null;
            using (XmlReader reader = XmlReader.Create(fullPath))
            {
                reader.MoveToContent();
                tunableStructure = new TunableStructure(
                    reader, e.UndoEngine, definitions, e.Log);
            }

            return new TunableStructureViewModel(tunableStructure);
        }
        #endregion Methods
    } // RSG.Metadata.ViewModel.Project.Controllers.CreateDocumentContentController {Class}
} // RSG.Metadata.ViewModel.Project.Controllers {Namespace}
