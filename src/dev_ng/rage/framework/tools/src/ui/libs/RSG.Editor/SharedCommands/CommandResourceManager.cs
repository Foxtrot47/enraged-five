﻿//---------------------------------------------------------------------------------------------
// <copyright file="CommandResourceManager.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.SharedCommands
{
    using System;
    using System.Globalization;
    using System.Reflection;
    using System.Resources;
    using System.Windows.Input;
    using RSG.Base;
    using RSG.Base.Extensions;

    /// <summary>
    /// A resource manager purposely built for dealing with a StringTable containing command
    /// related strings. This class cannot be inherited.
    /// </summary>
    public sealed class CommandResourceManager : ResourceManager
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CommandResourceManager"/> class.
        /// </summary>
        /// <param name="baseName">
        /// The root name of the resource file without its extension but including any fully
        /// qualified namespace name.
        /// </param>
        /// <param name="assembly">
        /// The main assembly for the resources.
        /// </param>
        public CommandResourceManager(string baseName, Assembly assembly)
            : base(baseName, assembly)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Retrieves the category string value for the command with the specified id.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command whose category should be retrieved.
        /// </param>
        /// <returns>
        /// The category value for the command with the specified id.
        /// </returns>
        public string GetCategory(string id)
        {
            return this.GetString(id, CommandStringCategory.Category);
        }

        /// <summary>
        /// Retrieves the description string value for the command with the specified id.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command whose description should be retrieved.
        /// </param>
        /// <returns>
        /// The description value for the command with the specified id.
        /// </returns>
        public string GetDescription(string id)
        {
            return this.GetString(id, CommandStringCategory.Description);
        }

        /// <summary>
        /// Creates a System.Windows.InputGestureCollection object that contains the default
        /// input gestures for the command with the specified identifier.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command whose default gestures should be created.
        /// </param>
        /// <returns>
        /// A System.Windows.InputGestureCollection object that contains the default input
        /// gestures for the command with the specified identifier.
        /// </returns>
        public InputGestureCollection GetGestures(string id)
        {
            InputGestureCollection inputGestureCollection = new InputGestureCollection();
            string keyGestures = this.GetString(id, CommandStringCategory.KeyGestures);
            if (String.IsNullOrWhiteSpace(keyGestures))
            {
                return inputGestureCollection;
            }

            KeyGestureConverter converter = new KeyGestureConverter();
            while (!String.IsNullOrEmpty(keyGestures))
            {
                int num = keyGestures.IndexOf(";", StringComparison.Ordinal);
                string token;
                if (num >= 0)
                {
                    token = keyGestures.Substring(0, num);
                    keyGestures = keyGestures.Substring(num + 1);
                }
                else
                {
                    token = keyGestures;
                    keyGestures = String.Empty;
                }

                KeyGesture gesture = null;
                try
                {
                    gesture = converter.ConvertFromInvariantString(token) as KeyGesture;
                }
                catch (NotSupportedException)
                {
                }

                if (gesture != null)
                {
                    inputGestureCollection.Add(gesture);
                }
            }

            return inputGestureCollection;
        }

        /// <summary>
        /// Retrieves the name string value for the command with the specified id.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command whose name should be retrieved.
        /// </param>
        /// <returns>
        /// The name value for the command with the specified id.
        /// </returns>
        public string GetName(string id)
        {
            return this.GetString(id, CommandStringCategory.Name) ?? id;
        }

        /// <summary>
        /// Retrieves the embedded string resource indexed by the specified identifier.
        /// </summary>
        /// <param name="id">
        /// The identifier for the embedded string resource to retrieve.
        /// </param>
        /// <returns>
        /// The embedded string resource indexed by the specified identifier.
        /// </returns>
        public override string GetString(string id)
        {
            if (String.IsNullOrWhiteSpace(id))
            {
                return "INVALID_RESOURCE_ID";
            }

            return this.TryToGetString(id, CultureInfo.CurrentUICulture).Value;
        }

        /// <summary>
        ///  Retrieves the specified string value for the command with the specified id from
        /// the command string table.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command whose string value should be retrieved.
        /// </param>
        /// <param name="category">
        /// Specifies which string value to retrieve.</param>
        /// <returns>
        /// The specified string value for the command with the specified id.
        /// </returns>
        public string GetString(string id, CommandStringCategory category)
        {
            string categoryId = "{0}{1}".FormatInvariant(id, category.ToString());
            return this.TryToGetString(categoryId, CultureInfo.CurrentUICulture).Value;
        }

        /// <summary>
        /// Retrieves the embedded string resource indexed by the specified identifier
        /// formatted with the specified arguments.
        /// </summary>
        /// <param name="id">
        /// The identifier for the embedded string resource to retrieve.
        /// </param>
        /// <param name="args">
        /// An object array that contains zero or more objects to format.
        /// </param>
        /// <returns>
        /// The embedded string resource indexed by the specified identifier in which the
        /// format items have been replaced by the string representations of the corresponding
        /// objects in <paramref name="args"/>.
        /// </returns>
        public string GetString(string id, params String[] args)
        {
            if (String.IsNullOrWhiteSpace(id))
            {
                return "INVALID_RESOURCE_ID";
            }

            TryResult<string> result = this.TryToGetString(id, CultureInfo.CurrentUICulture);
            if (result.Success == false)
            {
                return result.Value;
            }

            try
            {
                return result.Value.FormatCurrentUI(args);
            }
            catch (ArgumentNullException)
            {
                return "LOADING_'{0}'_RESOURCE_FAILED".FormatCurrent(id);
            }
            catch (FormatException)
            {
                return "INVALID_FORMAT_ON_'{0}'_RESOURCE".FormatCurrent(id);
            }
        }

        /// <summary>
        /// Retrieves the embedded string resource indexed by the specified identifier.
        /// </summary>
        /// <param name="id">
        /// The identifier for the embedded string resource to retrieve.
        /// </param>
        /// <param name="cultureInfo">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        /// <returns>
        /// A structure containing the result of the operation and a value indicating whether
        /// the resource was successfully located.
        /// </returns>
        private TryResult<string> TryToGetString(string id, CultureInfo cultureInfo)
        {
            try
            {
                return new TryResult<string>(base.GetString(id, cultureInfo), true);
            }
            catch (ArgumentNullException)
            {
                return new TryResult<string>("INVALID_RESOURCE_ID", false);
            }
            catch (InvalidOperationException)
            {
                string value = "LOADING_'{0}'_RESOURCE_FAILED".FormatCurrent(id);
                return new TryResult<string>(value, false);
            }
            catch (MissingManifestResourceException)
            {
                string value = "RESOURCE_'{0}'_MISSING".FormatCurrent(id);
                return new TryResult<string>(value, false);
            }
            catch (MissingSatelliteAssemblyException)
            {
                string value = "RESOURCE_'{0}'_MISSING".FormatCurrent(id);
                return new TryResult<string>(value, false);
            }
        }
        #endregion Methods
    } // RSG.Editor.SharedCommands.CommandResourceManager {Class}
} // RSG.Editor.SharedCommands {Namespace}
