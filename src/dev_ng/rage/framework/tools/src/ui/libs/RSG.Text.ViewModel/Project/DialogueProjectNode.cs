﻿//---------------------------------------------------------------------------------------------
// <copyright file="DialogueProjectNode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.ViewModel.Project
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;
    using RSG.Editor.Model;
    using RSG.Project.Model;
    using RSG.Project.ViewModel;
    using RSG.Project.ViewModel.Definitions;
    using RSG.Text.Model;

    /// <summary>
    /// The node inside the project explorer that represents a dialogue project. This class
    /// cannot be inherited.
    /// </summary>
    public class DialogueProjectNode : ProjectNode
    {
        #region Fields
        /// <summary>
        /// The private list of all of the item definition categories that have been created
        /// for this project using the associated definitions.
        /// </summary>
        private List<ItemDefinitionCategory> _categories;

        /// <summary>
        /// The private field used for the <see cref="Configurations"/> property.
        /// </summary>
        private DialogueConfigurations _configurations;

        /// <summary>
        /// A private field indicating whether the configurations have been loaded.
        /// </summary>
        private bool _configurationsLoaded;

        /// <summary>
        /// The private object used for the load synchronisation across multiple threads.
        /// </summary>
        private object _loadSyncObject = new object();
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DialogueProjectNode"/> class.
        /// </summary>
        /// <param name="definition">
        /// The project definition that was used to create this project node instance.
        /// </param>
        public DialogueProjectNode(ProjectDefinition definition, List<ProjectItemDefinition> itemDefinitions)
            : base(definition)
        {
            this._configurations = new DialogueConfigurations();
            this.Icon = DialogueProjectIcons.DialogueProject;
            this._categories = new List<ItemDefinitionCategory>();

            ItemDefinitionCategory category = new ItemDefinitionCategory("Miscellaneous");
            foreach (ProjectItemDefinition itemDefinition in itemDefinitions)
            {
                category.AddItemDefinition(itemDefinition);
            }

            this._categories.Add(category);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the dialogue configurations containing the characters etc that have been tied
        /// to this project.
        /// </summary>
        public DialogueConfigurations Configurations
        {
            get { return this._configurations; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public override FileNode CreateFileNode(IHierarchyNode parent, ProjectItem item)
        {
            return new DialogueFileNode(parent, item);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="undoEngine"></param>
        /// <returns></returns>
        public override IProjectPropertyContent CreatePropertyContent(UndoEngine undoEngine)
        {
            return new DialogueProjectProperties(this, undoEngine);
        }

        /// <summary>
        /// Ensures that the configurations for this project are loaded.
        /// </summary>
        public void EnsureConfigurationsAreLoaded()
        {
            lock (this._loadSyncObject)
            {
                if (!this._configurationsLoaded)
                {
                    this.LoadConfigurations();
                }
            }
        }

        /// <summary>
        /// Retrieves the item definition categories that have been setup for this project.
        /// </summary>
        /// <returns>
        /// The item definition categories that have been setup for this project.
        /// </returns>
        public override IReadOnlyList<ItemDefinitionCategory> GetDefinitionCategories()
        {
            this.EnsureConfigurationsAreLoaded();
            return this._categories;
        }

        /// <summary>
        /// Performs all post load actions.
        /// </summary>
        protected override void PostLoad()
        {
            base.PostLoad();
            if (this.Loaded)
            {
                Task.Factory.StartNew(
                    new Action(() =>
                    {
                        lock (this._loadSyncObject)
                        {
                            if (!this._configurationsLoaded)
                            {
                                this.LoadConfigurations();
                            }
                        }
                    }));
            }
        }

        /// <summary>
        /// Loads the configurations for this project.
        /// </summary>
        private void LoadConfigurations()
        {
            string configurationPath = this.GetProperty("Configurations");
            if (String.IsNullOrWhiteSpace(configurationPath))
            {
				this._configurationsLoaded = true;
                return;
            }

            string directoryName = Path.GetDirectoryName(this.FullPath);
            configurationPath = Path.Combine(directoryName, configurationPath);
            configurationPath = Path.GetFullPath(configurationPath);

            if (File.Exists(configurationPath))
            {
                this._configurations.LoadConfigurations(configurationPath);
            }

            this._configurationsLoaded = true;
        }
        #endregion Methods
    } // RSG.Text.ViewModel.Project.DialogueProjectNode {Class}
} // RSG.Text.ViewModel.Project {Namespace}
