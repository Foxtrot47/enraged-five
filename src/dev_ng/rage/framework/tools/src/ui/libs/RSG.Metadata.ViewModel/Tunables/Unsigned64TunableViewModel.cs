﻿//---------------------------------------------------------------------------------------------
// <copyright file="Unsigned64TunableViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Tunables
{
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// The view model for a <see cref="Unsigned64Tunable"/> model object.
    /// </summary>
    public class Unsigned64TunableViewModel : TunableViewModelBase<Unsigned64Tunable>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Unsigned64TunableViewModel"/> class.
        /// </summary>
        /// <param name="tunable">
        /// The unsigned 64-bit integer tunable that this view model is representing.
        /// </param>
        /// <param name="parent">
        /// The parent tunable view model for this view model.
        /// </param>
        public Unsigned64TunableViewModel(Unsigned64Tunable tunable, ITunableViewModel parent)
            : base(tunable, parent)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the value currently assigned to the unsigned 64-bit integer tunable.
        /// </summary>
        public ulong Value
        {
            get { return this.Model.Value; }
            set { this.Model.Value = value; }
        }
        #endregion Properties
    } // RSG.Metadata.ViewModel.Tunables.Unsigned64TunableViewModel {Class}
} // RSG.Metadata.ViewModel.Tunables {Namespace}
