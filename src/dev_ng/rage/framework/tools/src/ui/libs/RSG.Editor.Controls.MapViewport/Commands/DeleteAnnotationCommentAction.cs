﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor.Controls.MapViewport.Annotations;

namespace RSG.Editor.Controls.MapViewport.Commands
{
    /// <summary>
    /// The action logic that is responsible for deleting a comment from an annotation.
    /// </summary>
    public class DeleteAnnotationCommentAction : ButtonAction<Annotation, AnnotationComment>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="DeleteAnnotationCommentAction"/> class
        /// using the specified resolver.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public DeleteAnnotationCommentAction(ParameterResolverDelegate<Annotation> resolver)
            : base(resolver)
        {
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="annotation">
        /// The resolved command parameter that has been requested.
        /// </param>
        /// <param name="AnnotationComment">
        /// The original command parameter.
        /// </param>
        public override void Execute(Annotation annotation, AnnotationComment comment)
        {
            annotation.RemoveComment(comment);
        }
        #endregion
    }
}
