﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace RSG.Editor.Controls.MapViewport
{
    /// <summary>
    /// Simple class for wrapping the contents of a <see cref="MapLayer"/>.
    /// </summary>
    public class MapLayerItem : ContentControl
    {
        #region Constructors
        /// <summary>
        /// Initialises the static members of the <see cref="MapLayerItem"/> class.
        /// </summary>
        static MapLayerItem()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(MapLayerItem),
                new FrameworkPropertyMetadata(typeof(MapLayerItem)));
        }
        #endregion
        
    }
}
