﻿//---------------------------------------------------------------------------------------------
// <copyright file="IRenameable.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    /// <summary>
    /// When implemented represents a object publishes a <see cref="IRenameController"/> that
    /// can be used to rename the item from the user interface.
    /// </summary>
    public interface IRenameable
    {
        #region Properties
        /// <summary>
        /// Gets a value indicating whether this item can be currently renamed by the
        /// controller.
        /// </summary>
        bool CanBeRenamed { get; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Gets the rename controller that is used to rename this item with the specified
        /// container.
        /// </summary>
        /// <param name="containerDataContext">
        /// The data context of the container in the user interface that this item is being
        /// shown inside.
        /// </param>
        /// <returns>
        /// The rename controller to use for this item.
        /// </returns>
        IRenameController BeginRename(object containerDataContext);
        #endregion Methods
    } // RSG.Editor.View.IRenameable {Interface}
} // RSG.Editor.View {Namespace}
