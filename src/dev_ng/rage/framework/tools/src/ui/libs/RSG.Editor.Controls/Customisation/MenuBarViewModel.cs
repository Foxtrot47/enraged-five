﻿//---------------------------------------------------------------------------------------------
// <copyright file="MenuBarViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Customisation
{
    using System.Collections.Generic;
    using System.Linq;
    using RSG.Editor.Controls.Resources;

    /// <summary>
    /// Represents the view model that contains the main menu data.
    /// </summary>
    public class MenuBarViewModel : RootContainerViewModel
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="RootItems"/> property.
        /// </summary>
        private CommandContainerViewModel _menuRoot;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MenuBarViewModel"/> class.
        /// </summary>
        public MenuBarViewModel()
        {
            this._menuRoot = CommandMenuViewModel.CreateForMainMenu();
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the header used for this container.
        /// </summary>
        public override string Header
        {
            get { return CustomisationResources.MenuTabText; }
        }

        /// <summary>
        /// Gets the command container view model that contains the main menu command view
        /// models that the user can customise.
        /// </summary>
        public IEnumerable<CommandContainerViewModel> RootItems
        {
            get { return Enumerable.Repeat(this._menuRoot, 1); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Re-initialises the items in this container. This can be called once the commands
        /// have been reset to make sure the items shown to the user are correctly synced up
        /// against the model.
        /// </summary>
        public override void ResetItems()
        {
            this._menuRoot.ResetItems();
        }

        /// <summary>
        /// Gets the items that match the current search expression.
        /// </summary>
        /// <returns>
        /// The items that match the current search expression.
        /// </returns>
        protected override IEnumerable<object> GetSearchItems()
        {
            List<object> results = new List<object>();
            foreach (CommandContainerViewModel root in this.RootItems)
            {
                results.Add(root);
                foreach (CommandViewModel child in root.Children)
                {
                    results.AddRange(this.GetSearchItems(child));
                }
            }

            return results;
        }

        /// <summary>
        /// Gets the items that match the current search expression.
        /// </summary>
        /// <param name="root">
        /// The root item.
        /// </param>
        /// <returns>
        /// The items that match the current search expression.
        /// </returns>
        private List<object> GetSearchItems(CommandViewModel root)
        {
            List<object> results = new List<object>();
            if (this.SearchExpression.IsMatch(root.Text))
            {
                results.Add(root);
            }

            CommandContainerViewModel container = root as CommandContainerViewModel;
            if (container != null)
            {
                foreach (CommandViewModel child in container.Children)
                {
                    results.AddRange(this.GetSearchItems(child));
                }
            }

            return results;
        }
        #endregion Methods
    } // RSG.Editor.Controls.Customisation.MenuBarViewModel {Class}
} // RSG.Editor.Controls.Customisation {Namespace}
