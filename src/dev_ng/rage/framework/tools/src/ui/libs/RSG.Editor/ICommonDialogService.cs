﻿//---------------------------------------------------------------------------------------------
// <copyright file="ICommonDialogService.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Windows;

    /// <summary>
    /// When implemented represents a service that can be used to handle a request to show the
    /// user one of the common dialog windows.
    /// </summary>
    public interface ICommonDialogService
    {
        #region Methods
        /// <summary>
        /// Handles the situation of trying to save to a read-only file.
        /// </summary>
        /// <param name="fullPath">
        /// The full path to the file being saved.
        /// </param>
        /// <param name="cancelled">
        /// A value that is set to true if during the handling the user chooses to cancel
        /// the operation.
        /// </param>
        /// <returns>
        /// True if the specified full path is need writable; otherwise, false.
        /// </returns>
        bool HandleSavingOfReadOnlyFile(ref string fullPath, ref bool cancelled);

        /// <summary>
        /// Shows the open file dialog window to the user so that they can select a single file
        /// to read from.
        /// </summary>
        /// <param name="directory">
        /// The initial directory that is displayed by the file dialog.
        /// </param>
        /// <param name="filename">
        /// A string containing the initial full path of the file selected in the file
        /// dialog.
        /// </param>
        /// <param name="filter">
        /// The filter that determines what types of files are displayed in the file
        /// dialog.
        /// </param>
        /// <param name="filterIndex">
        /// The index of the filter currently selected in the file dialog.
        /// </param>
        /// <param name="location">
        /// When this method returns contains the full path to the file the user selected
        /// if valid.
        /// </param>
        /// <returns>
        ///  True if the user clicks the OK button; otherwise, false.
        /// </returns>
        bool ShowOpenFile(
                string directory,
                string filename,
                string filter,
                int filterIndex,
                out string location);

        /// <summary>
        /// Shows the open file dialog window to the user so that they can select a single file
        /// to read from.
        /// </summary>
        /// <param name="directory">
        /// The initial directory that is displayed by the file dialog.
        /// </param>
        /// <param name="filename">
        /// A string containing the initial full path of the file selected in the file
        /// dialog.
        /// </param>
        /// <param name="filter">
        /// The filter that determines what types of files are displayed in the file
        /// dialog.
        /// </param>
        /// <param name="filterIndex">
        /// The index of the filter currently selected in the file dialog.
        /// </param>
        /// <param name="filepaths">
        /// When this method returns contains the full path to the files the user selected
        /// if valid.
        /// </param>
        /// <returns>
        ///  True if the user clicks the OK button; otherwise, false.
        /// </returns>
        bool ShowOpenFile(
                string directory,
                string filename,
                string filter,
                int filterIndex,
                out string[] filepaths);

        /// <summary>
        /// Shows the save file dialog window to the user so that they can select a single file
        /// to write to.
        /// </summary>
        /// <param name="directory">
        /// The initial directory that is displayed by the file dialog.
        /// </param>
        /// <param name="filename">
        /// A string containing the initial full path of the file selected in the file dialog.
        /// </param>
        /// <param name="destination">
        /// When this method returns contains the full path to the file the user selected if
        /// valid.
        /// </param>
        /// <returns>
        ///  True if the user clicks the OK button; otherwise, false.
        /// </returns>
        bool ShowSaveFile(string directory, string filename, out string destination);

        /// <summary>
        /// Shows the save file dialog window to the user so that they can select a single file
        /// to write to.
        /// </summary>
        /// <param name="directory">
        /// The initial directory that is displayed by the file dialog.
        /// </param>
        /// <param name="filename">
        /// A string containing the initial full path of the file selected in the file dialog.
        /// </param>
        /// <param name="filter">
        /// The filter that determines what types of files are displayed in the file.
        /// </param>
        /// <param name="filterIndex">
        /// The index of the filter currently selected in the file dialog.
        /// </param>
        /// <param name="destination">
        /// When this method returns contains the full path to the file the user selected if
        /// valid.
        /// </param>
        /// <returns>
        ///  True if the user clicks the OK button; otherwise, false.
        /// </returns>
        bool ShowSaveFile(
            string directory,
            string filename,
            string filter,
            int filterIndex,
            out string destination);

        /// <summary>
        /// Shows the selected folder dialog to the user so that they can select a folder
        /// and returns the result back in the <paramref name="directory"/> parameter.
        /// </summary>
        /// <param name="id">
        /// The identifier to use to control the persistence of this dialog.
        /// </param>
        /// <param name="title">
        /// The title to show on the window.
        /// </param>
        /// <param name="directory">
        /// When this method returns contains the full path to the directory the user
        /// selected if valid.
        /// </param>
        /// <returns>
        /// True if the user selected a folder; otherwise, false.
        /// </returns>
        bool ShowSelectFolder(Guid id, string title, out string directory);

        /// <summary>
        /// Shows the selected folder dialog to the user so that they can select a folder
        /// and returns the result back in the <paramref name="directory"/> parameter.
        /// </summary>
        /// <param name="id">
        /// The identifier to use to control the persistence of this dialog.
        /// </param>
        /// <param name="initialDirectory">
        /// The initial directory that is displayed by the folder dialog.
        /// </param>
        /// <param name="title">
        /// The title to show on the window.
        /// </param>
        /// <param name="directory">
        /// When this method returns contains the full path to the directory the user
        /// selected if valid.
        /// </param>
        /// <returns>
        /// True if the user selected a folder; otherwise, false.
        /// </returns>
        bool ShowSelectFolder(Guid id, string initialDirectory, string title, out string directory);

        /// <summary>
        /// Shows the selected folder dialog to the user so that they can select folders
        /// and returns the result back in the <paramref name="directories"/> parameter.
        /// </summary>
        /// <param name="id">
        /// The identifier to use to control the persistence of this dialog.
        /// </param>
        /// <param name="title">
        /// The title to show on the window.
        /// </param>
        /// <param name="directories">
        /// When this method returns contains an array of full paths to the directories the user
        /// selected if valid.
        /// </param>
        /// <returns>
        /// True if the user selected a folder; otherwise, false.
        /// </returns>
        bool ShowMultiSelectFolder(Guid id, string title, out string[] directories);

        /// <summary>
        /// Shows the selected folder dialog to the user so that they can select folders
        /// and returns the result back in the <paramref name="directories"/> parameter.
        /// </summary>
        /// <param name="id">
        /// The identifier to use to control the persistence of this dialog.
        /// </param>
        /// <param name="initialDirectory">
        /// The initial directory that is displayed by the folder dialog.
        /// </param>
        /// <param name="title">
        /// The title to show on the window.
        /// </param>
        /// <param name="directories">
        /// When this method returns contains an array of full paths to the directories the user
        /// selected if valid.
        /// </param>
        /// <returns>
        /// True if the user selected a folder; otherwise, false.
        /// </returns>
        bool ShowMultiSelectFolder(Guid id, string initialDirectory, string title, out string[] directories);
        #endregion Methods
    } // RSG.Editor.ICommonDialogService {Class}
} // RSG.Editor {Namespace}
