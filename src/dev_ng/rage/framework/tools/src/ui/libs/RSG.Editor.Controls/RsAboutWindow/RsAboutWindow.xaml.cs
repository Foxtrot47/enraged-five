﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsAboutWindow.xaml.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System.Reflection;
    using System.Windows;
    using System.Windows.Data;
    using RSG.Interop.Microsoft.Windows;

    /// <summary>
    /// Contains the interactive logic for the <see cref="RsAboutWindow"/> window class.
    /// </summary>
    public partial class RsAboutWindow : RsWindow, INonClientArea
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="RsAboutWindow"/> class.
        /// </summary>
        /// <param name="assembly">
        /// The assembly that's information should be displayed. For a WPF application this
        /// should be the entry assembly.
        /// </param>
        public RsAboutWindow(Assembly assembly)
        {
            this.InitializeComponent();
            Binding binding = new Binding();
            binding.Source = assembly;
            binding.BindsDirectlyToSource = true;
            this.AboutBoxControl.SetBinding(RsAboutBox.AssemblyProperty, binding);
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Tests the specified point coordinate and returns a Hit Test result based on the
        /// system hot spot.
        /// </summary>
        /// <param name="point">
        /// The point to test.
        /// </param>
        /// <returns>
        /// A result indicating the position of the cursor hot spot.
        /// </returns>
        NcHitTestResult INonClientArea.HitTest(Point point)
        {
            return NcHitTestResult.CAPTION;
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsAboutWindow {Class}
} // RSG.Editor.Controls {Namespace}
