﻿//---------------------------------------------------------------------------------------------
// <copyright file="NativeKeyDownEventArgs.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Windows.Input;

    /// <summary>
    /// Represents the event args used for a native key down event message.
    /// </summary>
    public class NativeKeyDownEventArgs : EventArgs
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Key"/> property.
        /// </summary>
        private Key _key;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="NativeKeyDownEventArgs"/> class.
        /// </summary>
        /// <param name="key">
        /// The key that was pressed to cause the event to fire.
        /// </param>
        public NativeKeyDownEventArgs(Key key)
        {
            this._key = key;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the key that was pressed.
        /// </summary>
        public Key Key
        {
            get { return this._key; }
        }
        #endregion Properties
    } // RSG.Editor.Controls.NativeKeyDownEventArgs {Class}
} // RSG.Editor.Controls {Namespace}
