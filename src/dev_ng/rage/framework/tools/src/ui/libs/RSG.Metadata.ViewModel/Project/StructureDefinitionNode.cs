﻿//---------------------------------------------------------------------------------------------
// <copyright file="StructureDefinitionNode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Project
{
    using System.Collections.Generic;
    using RSG.Metadata.Model.Definitions;
    using RSG.Metadata.Model.Definitions.Members;
    using RSG.Project.ViewModel;

    /// <summary>
    /// The node inside the project explorer that represents a parCodeGen structure definition.
    /// This class cannot be inherited.
    /// </summary>
    public sealed class StructureDefinitionNode : HierarchyNode
    {
        #region Fields
        /// <summary>
        /// The structure that this node is representing.
        /// </summary>
        private IStructure _structure;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="StructureDefinitionNode"/> class.
        /// </summary>
        /// <param name="structure">
        /// The structure object this node is representing.
        /// </param>
        /// <param name="parent">
        /// The parent node.
        /// </param>
        public StructureDefinitionNode(IStructure structure, IHierarchyNode parent)
            : base(false)
        {
            this._structure = structure;
            this.Parent = parent;
            this.Icon = Icons.StructNodeIcon;
            this.Text = structure.DataType;
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Creates the child elements for this view model. This is only called once the item
        /// has been expanded in the user interface.
        /// </summary>
        /// <param name="collection">
        /// The collection to add the elements to.
        /// </param>
        protected override void CreateChildren(ICollection<IHierarchyNode> collection)
        {
            foreach (IMember member in this._structure.Members)
            {
                collection.Add(new StructureMemberNode(member, this));
            }
        }
        #endregion Methods
    } // RSG.Metadata.ViewModel.Project.StructureDefinitionNode {Class}
} // RSG.Metadata.ViewModel.Project {Namespace}
