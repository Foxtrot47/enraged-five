﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace RSG.Rag.View
{
    /// <summary>
    /// Control that can be used for hosting a widget.
    /// </summary>
    public class WidgetControl : ContentControl
    {
        #region Dependency Properties
        /// <summary>
        /// 
        /// </summary>
        public static readonly DependencyProperty DisplayNameProperty = 
            DependencyProperty.Register(
                "DisplayName",
                typeof(bool),
                typeof(WidgetControl),
                new FrameworkPropertyMetadata(true));

        /// <summary>
        /// Property to access the display name value.
        /// </summary>
        public bool DisplayName
        {
            get { return (bool)GetValue(DisplayNameProperty); }
            set { SetValue(DisplayNameProperty, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public static readonly DependencyProperty NameDockProperty =
            DependencyProperty.Register(
                "NameDock",
                typeof(Dock),
                typeof(WidgetControl),
                new FrameworkPropertyMetadata(Dock.Left, null, CoerceNameDock));

        /// <summary>
        /// 
        /// </summary>
        public Dock NameDock
        {
            get { return (Dock)GetValue(NameDockProperty); }
            set { SetValue(NameDockProperty, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="d"></param>
        /// <param name="baseValue"></param>
        public static Object CoerceNameDock(DependencyObject d, Object baseValue)
        {
            Dock newDockValue = (Dock)baseValue;
            if (newDockValue != Dock.Right && newDockValue != Dock.Top)
            {
                return Dock.Left;
            }
            return newDockValue;
        }
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Used to register the style key for the <see cref="WidgetControl"/> control.
        /// </summary>
        static WidgetControl()
        {
            // Setup the default style key.
            DefaultStyleKeyProperty.OverrideMetadata(
                   typeof(WidgetControl),
                   new FrameworkPropertyMetadata(typeof(WidgetControl)));
        }
        #endregion // Constructor(s)
    } // WidgetControl
}
