﻿//---------------------------------------------------------------------------------------------
// <copyright file="ColourToSolidBrushConverter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Converters
{
    using System.Globalization;
    using System.Windows.Media;

    /// <summary>
    /// Converters any colour value to a solid colour brush object. This class cannot be
    /// inherited.
    /// </summary>
    public sealed class ColourToSolidBrushConverter : ValueConverter<Color, Brush>
    {
        #region Methods
        /// <summary>
        /// Converters the given value and returns the result.
        /// </summary>
        /// <param name="value">
        /// The original value to convert.
        /// </param>
        /// <param name="param">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A converted value.
        /// </returns>
        protected override Brush Convert(Color value, object param, CultureInfo culture)
        {
            return new SolidColorBrush(value);
        }
        #endregion Methods
    } // RSG.Editor.Controls.Converters.ColourToSolidBrushConverter {Class}
} // RSG.Editor.Controls.Converters {Namespace}
