﻿//---------------------------------------------------------------------------------------------
// <copyright file="UniformPanelMarginSetter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.AttachedDependencyProperties
{
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Defines the dependency properties that can be attached to a Panel that applies
    /// a consistent margin to all child elements.
    /// </summary>
    public static class UniformPanelMarginSetter
    {
        #region Margin Attached Property
        /// <summary>
        /// Identifies the Margin dependency property.
        /// </summary>
        public static readonly DependencyProperty MarginProperty =
            DependencyProperty.RegisterAttached(
                "Margin",
                typeof(Thickness),
                typeof(UniformPanelMarginSetter),
                new UIPropertyMetadata(new Thickness(), OnMarginChanged));

        /// <summary>
        /// Retrieves the Margin dependency property value attached to the specified object.
        /// </summary>
        /// <param name="element">
        /// The object whose attached Margin dependency property value will be returned.
        /// </param>
        /// <returns>
        /// The Margin dependency property value attached to the specified object.
        /// </returns>
        public static Thickness GetMargin(DependencyObject element)
        {
            return (Thickness)element.GetValue(MarginProperty);
        }

        /// <summary>
        /// Sets the Margin dependency property value on the specified object to the specified
        /// value.
        /// </summary>
        /// <param name="element">
        /// The object whose attached Margin dependency property value will be set.
        /// </param>
        /// <param name="value">
        /// The value to set the attached Margin dependency property to on the specified
        /// object.
        /// </param>
        public static void SetMargin(DependencyObject element, Thickness value)
        {
            element.SetValue(MarginProperty, value);
        }

        /// <summary>
        /// Called whenever the Margin dependency property changes.
        /// </summary>
        /// <param name="sender">
        /// The object whose Margin dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnMarginChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            // Make sure this is put on a panel
            Panel panel = sender as Panel;
            if (panel == null)
            {
                return;
            }

            panel.Loaded += new RoutedEventHandler(Panel_Loaded);

        }

        /// <summary>
        /// Called when the panel that the Margin attached property is bound to is loaded.
        /// </summary>
        /// <param name="sender">
        /// The panel object that the Margin dependency property is bound to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs data that describes the loaded event.
        /// </param>
        private static void Panel_Loaded(object sender, RoutedEventArgs e)
        {
            Panel panel = sender as Panel;

            // Go over the children and set margin for them:
            foreach (UIElement child in panel.Children)
            {
                FrameworkElement fe = child as FrameworkElement;
                if (fe != null)
                {
                    fe.Margin = UniformPanelMarginSetter.GetMargin(panel);
                }
            }
        }
        #endregion
    }
}
