﻿// --------------------------------------------------------------------------------------------
// <copyright file="FileDescriptor.cs" company="Rockstar">
//     Copyright © Rockstar Games 2015. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Rpf.View
{
    using System;
    using System.IO;

    /// <summary>
    /// Class representing a virtual file for use by drag/drop.
    /// </summary>
    public class FileDescriptor
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="ChangeTimeUtc"/> property.
        /// </summary>
        private DateTime? _changeTimeUtc;

        /// <summary>
        /// The private field used for the <see cref="Length"/> property.
        /// </summary>
        private long? _length;

        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private string _name;

        /// <summary>
        /// The private field used for the <see cref="StreamContents"/> property.
        /// </summary>
        private Action<Stream> _streamContents;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="FileDescriptor"/> class.
        /// </summary>
        public FileDescriptor()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the (optional) change time of the file.
        /// </summary>
        public DateTime? ChangeTimeUtc
        {
            get { return this._changeTimeUtc; }
            set { this._changeTimeUtc = value; }
        }

        /// <summary>
        /// Gets or sets the (optional) length of the file.
        /// </summary>
        public long? Length
        {
            get { return this._length; }
            set { this._length = value; }
        }

        /// <summary>
        /// Gets or sets the name of the file.
        /// </summary>
        public string Name
        {
            get { return this._name; }
            set { this._name = value; }
        }

        /// <summary>
        /// Gets or sets an action that returns the contents of the file.
        /// </summary>
        public Action<Stream> StreamContents
        {
            get { return this._streamContents; }
            set { this._streamContents = value; }
        }
        #endregion Properties
    } // RSG.Rpf.View.FileDescriptor {Class}
} // RSG.Rpf.View {Namespace}
