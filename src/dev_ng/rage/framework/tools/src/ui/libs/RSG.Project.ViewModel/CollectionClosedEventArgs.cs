﻿//---------------------------------------------------------------------------------------------
// <copyright file="CollectionClosedEventArgs.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel
{
    using System;

    /// <summary>
    /// Provides data for the event representing a project collection node being closed by the
    /// user.
    /// </summary>
    public class CollectionClosedEventArgs : EventArgs
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="FullPath"/> property.
        /// </summary>
        private string _fullPath;

        /// <summary>
        /// The private field used for the <see cref="ProjectCollection"/> property.
        /// </summary>
        private ProjectCollectionNode _projectCollection;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CollectionClosedEventArgs"/> class.
        /// </summary>
        /// <param name="collection">
        /// The project whose load state has changed.
        /// </param>
        /// <param name="fullPath">
        /// The full path to the collection that was opened.
        /// </param>
        public CollectionClosedEventArgs(ProjectCollectionNode collection, string fullPath)
        {
            this._fullPath = fullPath;
            this._projectCollection = collection;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the full path to the location the collection was opened from.
        /// </summary>
        public string FullPath
        {
            get { return this._fullPath; }
        }

        /// <summary>
        /// Gets the value before the changed occurred.
        /// </summary>
        public ProjectCollectionNode ProjectCollection
        {
            get { return this._projectCollection; }
        }
        #endregion Properties
    } // RSG.Project.ViewModel.CollectionClosedEventArgs {Class}
} // RSG.Project.ViewModel {Namespace}
