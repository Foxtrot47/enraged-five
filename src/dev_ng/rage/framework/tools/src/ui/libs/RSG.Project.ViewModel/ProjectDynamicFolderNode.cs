﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProjectDynamicFolderNode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2015. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;
    using System.Windows.Media.Imaging;
    using RSG.Base.Extensions;
    using RSG.Editor;
    using RSG.Interop.Microsoft.Windows;
    using RSG.Project.Model;

    /// <summary>
    /// Represents a folder node inside the main project collection.
    /// </summary>
    public class ProjectDynamicFolderNode : FolderNode
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ProjectDynamicFolderNode"/> class.
        /// </summary>
        /// <param name="model">
        /// The model that this view model will be wrapping.
        /// </param>
        /// <param name="text">
        /// The text that is used to display this item.
        /// </param>
        public ProjectDynamicFolderNode(IHierarchyNode parent, ProjectItem model, string text)
            : base(parent, model)
        {
            this.Text = text;
            this.Icon = ProjectIcons.DynamicFolder;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether a new or existing items can be added to this
        /// hierarchy node.
        /// </summary>
        public override bool CanHaveItemsAdded
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether this hierarchy node can be removed from the parent
        /// items scope it belongs to.
        /// </summary>
        public override bool CanBeRemoved
        {
            get { return this.Parent is ProjectNode; }
        }

        /// <summary>
        /// Gets a value indicating whether a new or existing project folder can be added to
        /// this hierarchy node.
        /// </summary>
        public override bool CanHaveProjectFoldersAdded
        {
            get { return false; }
        }

        /// <summary>
        /// Gets a value indicating whether this item can be currently renamed by the
        /// controller.
        /// </summary>
        public override bool CanBeRenamed
        {
            get { return false; }
        }

        /// <summary>
        /// Gets the bitmap source that is used to display the icon for this item when it has
        /// been expanded.
        /// </summary>
        public override BitmapSource ExpandedIcon
        {
            get { return ProjectIcons.DynamicOpenedFolder; }
        }

        #endregion Properties

        #region Methods
        /// <summary>
        /// Adds a new dynamic folder underneath this node and returns the instance to the
        /// new folder.
        /// </summary>
        /// <param name="path">
        /// The full path to the dynamic directory. 
        /// </param>
        /// <param name="filter">
        /// The file filter string that is used to get the files that will be shown in this
        /// dynamic directory.
        /// </param>
        /// <param name="recursive">
        /// A value indicating whether this is recursive and will show sub dynamic directories
        /// or not.
        /// </param>
        /// <returns>
        /// The new dynamic folder that was created.
        /// </returns>
        public ProjectDynamicFolderNode AddNewDynamicFolder(string path, string filter, bool recursive)
        {
            Project model = this.Model.Scope as Project;
            if (model == null)
            {
                return null;
            }

            string relative = Shlwapi.MakeRelativeFromFileToDirectory(model.FullPath, path);
            if (String.Equals(relative, "."))
            {
                relative = String.Empty;
            }

            ProjectItem item = model.AddNewProjectItem("DynamicFolder", relative);
            item.SetMetadata("Filters", filter, MetadataLevel.Core);
            item.SetMetadata("Recursive", recursive.ToString(), MetadataLevel.Core);

            string text = Path.GetFileName(path);
            var node = new ProjectDynamicFolderNode(this, item, Path.GetFileName(path));
            this.AddChild(node);
            this.IsExpandable = true;
            return node;
        }

        /// <summary>
        /// Adds a new folder underneath this node and returns the instance to the new folder.
        /// </summary>
        /// <returns>
        /// The new folder that was created.
        /// </returns>
        public override IHierarchyNode AddNewFolder()
        {
            string include = "{0}\\{1}".FormatInvariant(this.Model.Include, "NewFolder");
            include = this.Model.Scope.GetUniqueInclude(include);
            ProjectItem item = this.Model.Scope.AddNewProjectItem("Folder", include);
            HierarchyNode node = new ProjectFolderNode(this, item);
            node.IsExpandable = false;
            this.AddChild(node);
            this.IsExpandable = true;
            return node;
        }

        /// <summary>
        /// Called just before this object in the user interface is expanded inside a
        /// hierarchical control.
        /// </summary>
        public async override void BeforeExpand()
        {
            base.BeforeExpand();
            List<FileNode> fileNodes = new List<FileNode>();
            foreach (IHierarchyNode child in this.GetChildren(false))
            {
                FileNode childFile = child as FileNode;
                if (childFile == null)
                {
                    continue;
                }

                fileNodes.Add(childFile);
            }

            IList<IPerforceFileMetaData> metadataList = await this.GetChildMetadataAsync();
            StringComparison comparisonType = StringComparison.OrdinalIgnoreCase;
            foreach (FileNode fileNode in fileNodes)
            {
                IPerforceFileMetaData metadata = null;
                if (metadataList != null)
                {
                    string filePath = fileNode.FullPath;
                    foreach (IPerforceFileMetaData fileMetadata in metadataList)
                    {
                        string localPath = fileMetadata.LocalPath;
                        if (String.Equals(localPath, filePath, comparisonType))
                        {
                            metadata = fileMetadata;
                            break;
                        }
                    }
                }

                fileNode.UpdatePerforceState(metadata);
            }
        }

        /// <summary>
        /// Gets the source control metadata for the child nodes.
        /// </summary>
        /// <returns>
        /// The source control metadata for the child nodes.
        /// </returns>
        private async Task<IList<IPerforceFileMetaData>> GetChildMetadataAsync()
        {
            IPerforceService ps = this.ParentCollection.PerforceService;
            if (ps == null)
            {
                return null;
            }

            List<string> filenames = new List<string>();
            List<FileNode> fileNodes = new List<FileNode>();
            foreach (IHierarchyNode child in this.GetChildren(false))
            {
                FileNode childFile = child as FileNode;
                if (childFile == null)
                {
                    continue;
                }

                filenames.Add(childFile.FullPath);
                fileNodes.Add(childFile);
            }

            if (filenames.Count == 0)
            {
                return new List<IPerforceFileMetaData>();
            }

            var action = new Func<IList<IPerforceFileMetaData>>(
                delegate
                {
                    using (ps.SuspendExceptionsScope())
                    {
                        return ps.GetFileMetaData(filenames);
                    }
                });

            return await Task.Factory.StartNew(action);
        }
        #endregion Methods
    } // RSG.Project.ViewModel.ProjectDynamicFolderNode {Class}
} // RSG.Project.ViewModel {Namespace}
