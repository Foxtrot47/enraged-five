﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace RSG.Editor.Controls.MapViewport
{
    /// <summary>
    /// Provides static properties that give access to common icons that can be used throughout
    /// an application.
    /// </summary>
    public static class MapViewportIcons
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="AddAnnotation"/> property.
        /// </summary>
        private static BitmapSource _addAnnotation;

        /// <summary>
        /// The private field used for the <see cref="DeleteAnnotation"/> property.
        /// </summary>
        private static BitmapSource _deleteAnnotation;

        /// <summary>
        /// The private field used for the <see cref="Layers"/> property.
        /// </summary>
        private static BitmapSource _layers;

        /// <summary>
        /// The private field used for the <see cref="MapAnnotation"/> property.
        /// </summary>
        private static BitmapSource _mapAnnotation;

        /// <summary>
        /// The private field used for the <see cref="Maps"/> property.
        /// </summary>
        private static BitmapSource _maps;

        /// <summary>
        /// The private field used for the <see cref="SaveToImage"/> property.
        /// </summary>
        private static BitmapSource _saveToImage;

        /// <summary>
        /// The private field used for the <see cref="SelectedMap"/> property.
        /// </summary>
        private static BitmapSource _selectedMap;

        /// <summary>
        /// A generic object that provides thread safety access to the image sources.
        /// </summary>
        private static object _syncRoot = new object();
        #endregion

        #region Properties
        /// <summary>
        /// Gets the icon that shows an icon that represents layers.
        /// </summary>
        public static BitmapSource AddAnnotation
        {
            get
            {
                if (_addAnnotation == null)
                {
                    lock (_syncRoot)
                    {
                        if (_addAnnotation == null)
                        {
                            _addAnnotation = EnsureLoaded("addAnnotation16.png");
                        }
                    }
                }

                return _addAnnotation;
            }
        }

        /// <summary>
        /// Gets the icon that shows an icon that represents layers.
        /// </summary>
        public static BitmapSource DeleteAnnotation
        {
            get
            {
                if (_deleteAnnotation == null)
                {
                    lock (_syncRoot)
                    {
                        if (_deleteAnnotation == null)
                        {
                            _deleteAnnotation = EnsureLoaded("deleteAnnotation16.png");
                        }
                    }
                }

                return _deleteAnnotation;
            }
        }

        /// <summary>
        /// Gets the icon that shows an icon that represents layers.
        /// </summary>
        public static BitmapSource Layers
        {
            get
            {
                if (_layers == null)
                {
                    lock (_syncRoot)
                    {
                        if (_layers == null)
                        {
                            _layers = EnsureLoaded("layers16.png");
                        }
                    }
                }

                return _layers;
            }
        }

        /// <summary>
        /// Gets the icon that shows an icon that represents a map annotation.
        /// </summary>
        public static BitmapSource MapAnnotation
        {
            get
            {
                if (_mapAnnotation == null)
                {
                    lock (_syncRoot)
                    {
                        if (_mapAnnotation == null)
                        {
                            _mapAnnotation = EnsureLoaded("mapAnnotation.png");
                        }
                    }
                }

                return _mapAnnotation;
            }
        }

        /// <summary>
        /// Gets the icon that shows an icon that represents maps.
        /// </summary>
        public static BitmapSource Maps
        {
            get
            {
                if (_maps == null)
                {
                    lock (_syncRoot)
                    {
                        if (_maps == null)
                        {
                            _maps = EnsureLoaded("maps16.png");
                        }
                    }
                }

                return _maps;
            }
        }

        /// <summary>
        /// Gets the icon for saving out the overlay to an image.
        /// </summary>
        public static BitmapSource SaveToImage
        {
            get
            {
                if (_saveToImage == null)
                {
                    lock (_syncRoot)
                    {
                        if (_saveToImage == null)
                        {
                            _saveToImage = EnsureLoaded("saveToImage16.png");
                        }
                    }
                }

                return _saveToImage;
            }
        }

        /// <summary>
        /// Gets the icon that shows an icon that indicates the currently selected map.
        /// </summary>
        public static BitmapSource SelectedMap
        {
            get
            {
                if (_selectedMap == null)
                {
                    lock (_syncRoot)
                    {
                        if (_selectedMap == null)
                        {
                            _selectedMap = EnsureLoaded("selectedMap16.png");
                        }
                    }
                }

                return _selectedMap;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Ensures that the specified image source is loaded with the specified resource name.
        /// </summary>
        /// <param name="source">
        /// The image source that should be loaded.
        /// </param>
        /// <param name="resourceName">
        /// The name of the resource the loaded image source should set as its source.
        /// </param>
        private static BitmapSource EnsureLoaded(String resourceName)
        {
            String assemblyName = typeof(MapViewportIcons).Assembly.GetName().Name;
            String bitmapPath = String.Format(CultureInfo.InvariantCulture,
                "pack://application:,,,/{0};component/Resources/{1}", assemblyName, resourceName);
            Uri uri = new Uri(bitmapPath, UriKind.RelativeOrAbsolute);
            return new BitmapImage(uri);
        }
        #endregion
    }
}
