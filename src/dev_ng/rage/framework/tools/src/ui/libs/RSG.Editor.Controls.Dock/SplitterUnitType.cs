﻿//---------------------------------------------------------------------------------------------
// <copyright file="SplitterUnitType.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock
{
    /// <summary>
    /// Defines the different unit types the value of a <see cref="SplitterLength"/> instance
    /// can have.
    /// </summary>
    public enum SplitterUnitType
    {
        /// <summary>
        /// Specifies that the value of a <see cref="SplitterLength"/> instance is a fill
        /// value. Which represents a splitter length that is specifying the item should fill
        /// the remaining available space.
        /// </summary>
        Fill,

        /// <summary>
        /// Specifies that the value of a <see cref="SplitterLength"/> instance is a stretch
        /// value. Which represents a splitter length that is specifying the item should
        /// stretch itself to fill the available space it has.
        /// </summary>
        Stretch
    } // RSG.Editor.Controls.Dock.SplitterUnitType {Enum}
} // RSG.Editor.Controls.Dock {Namespace}
