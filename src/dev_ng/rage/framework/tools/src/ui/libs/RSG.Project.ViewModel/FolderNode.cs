﻿//---------------------------------------------------------------------------------------------
// <copyright file="FolderNode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Windows;
    using System.Windows.Media.Imaging;
    using RSG.Base.Extensions;
    using RSG.Editor;
    using RSG.Editor.View;
    using RSG.Project.Model;

    /// <summary>
    /// 
    /// </summary>
    public abstract class FolderNode : HierarchyNode
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="ExpandedByDefault"/> property.
        /// </summary>
        private bool _isExpandedByDefault;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="FolderNode"/> class.
        /// </summary>
        /// <param name="model">
        /// The model that this view model will be wrapping.
        /// </param>
        public FolderNode(IHierarchyNode parent, ProjectItem model)
            : base(parent, model)
        {
            int index = model.Include.LastIndexOf('\\') + 1;
            this.Text = model.Include.Substring(index);
            string expandedText = model.GetMetadata("Expanded");
            this.Icon = CommonIcons.ClosedFolder;
            bool expanded = false;
            if (bool.TryParse(expandedText, out expanded))
            {
                this._isExpandedByDefault = expanded;
            }
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether this item can be currently renamed by the
        /// controller.
        /// </summary>
        public override bool CanBeRenamed
        {
            get { return true; }
        }

        /// <summary>
        /// Gets the bitmap source that is used to display the icon for this item when it has
        /// been expanded.
        /// </summary>
        public override BitmapSource ExpandedIcon
        {
            get { return CommonIcons.OpenedFolder; }
        }

        /// <summary>
        /// Gets a value indicating whether this item is expanded by default in the user
        /// interface when by rendered inside a hierarchical control.
        /// </summary>
        public override bool IsExpandedByDefault
        {
            get { return this._isExpandedByDefault; }
        }

        /// <summary>
        /// Gets the priority value for this object.
        /// </summary>
        public override int Priority
        {
            get { return 0; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Gets the rename controller that is used to rename this item with the specified
        /// container.
        /// </summary>
        /// <param name="containerDataContext">
        /// The data context of the container in the user interface that this item is being
        /// shown inside.
        /// </param>
        /// <returns>
        /// The rename controller to use for this item.
        /// </returns>
        public override IRenameController BeginRename(object containerDataContext)
        {
            return new FolderNodeRenameController(this, containerDataContext);
        }

        /// <summary>
        /// Changes the parent node for this node.
        /// </summary>
        /// <param name="newScope">
        /// The new parent node for this item.
        /// </param>
        public override void ChangeParent(IHierarchyNode parent)
        {
            base.ChangeParent(parent);
            if (parent is FolderNode)
            {
                this.Model.Include =
                    "{0}\\{1}".FormatInvariant(parent.ProjectItem.Include, this.Text);
            }
            else
            {
                this.Model.Include = this.Text;
            }

            foreach (IHierarchyNode child in this.GetChildren(false))
            {
                HierarchyNode childNode = child as HierarchyNode;
                if (childNode != null)
                {
                    childNode.ValidateAndEnsureFilter();
                }
            }
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// Encapsulates the rename logic for a folder node.
        /// </summary>
        private class FolderNodeRenameController : NotifyPropertyChangedBase, IRenameController
        {
            #region Fields
            /// <summary>
            /// The private field used for the <see cref="ContainerDataContext"/> property.
            /// </summary>
            private object _containerDataContext;

            /// <summary>
            /// The private field used for the <see cref="IsIncomplete"/> property.
            /// </summary>
            private bool _isIncomplete;

            /// <summary>
            /// The private field used for the <see cref="Item"/> property.
            /// </summary>
            private FolderNode _item;

            /// <summary>
            /// The value of the text as it original was before all renaming.
            /// </summary>
            private string _originalText;

            /// <summary>
            /// The private field used for the <see cref="Tet"/> property.
            /// </summary>
            private string _text;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="FolderNodeRenameController"/>
            /// class.
            /// </summary>
            /// <param name="item">
            /// The item that is being renamed.
            /// </param>
            /// <param name="containerDataContext">
            /// The data context of the container in the user interface that this item is being
            /// shown inside.
            /// </param>
            public FolderNodeRenameController(FolderNode item, object containerDataContext)
            {
                this._originalText = item.Text;
                this._text = this._originalText;
                this._item = item;
                this._containerDataContext = containerDataContext;
                this._isIncomplete = true;
            }
            #endregion Constructors

            #region Events
            /// <summary>
            /// Occurrs when the renaming operation has finished. This doesn't indicate success
            /// just that the operation has finished.
            /// </summary>
            public event EventHandler<RenameControllerCompletedEventArgs> Completed;
            #endregion Events

            #region Properties
            /// <summary>
            /// Gets the data context for the item that is being renamed. This is the object
            /// that comes from the view and is used to both scroll to the item and find the
            /// correct view object to put into its edit mode.
            /// </summary>
            public object ContainerDataContext
            {
                get { return this._containerDataContext; }
            }

            /// <summary>
            /// Gets a value indicating whether the controller is in a incomplete state. (i.e
            /// the cancel or commit methods hasn't been called).
            /// </summary>
            public bool IsIncomplete
            {
                get { return this._isIncomplete; }
            }

            /// <summary>
            /// Gets the item this controller is renaming.
            /// </summary>
            public IRenameable Item
            {
                get { return this._item; }
            }

            /// <summary>
            /// Gets or sets the text that is currently being edited.
            /// </summary>
            public string Text
            {
                get { return this._text; }
                set { this.SetProperty(ref this._text, value); }
            }
            #endregion Properties

            #region Methods
            /// <summary>
            /// Cancels the renaming operation.
            /// </summary>
            /// <param name="refocus">
            /// A value indicating whether the item being renamed should be refocused. This is true
            /// if the renaming was cancelled for any reason other than the user clicking
            /// elsewhere.
            /// </param>
            public void Cancel(bool refocus)
            {
                this._isIncomplete = false;
                EventHandler<RenameControllerCompletedEventArgs> handler = this.Completed;
                if (handler != null)
                {
                    handler(
                        this,
                        new RenameControllerCompletedEventArgs(
                            refocus, RenameControllerCompletedResult.Cancelled));
                }
            }

            /// <summary>
            /// Commits the renaming operation to validation.
            /// </summary>
            /// <param name="refocus">
            /// A value indicating whether the item being renamed should be refocused. This is true
            /// if the renaming was commit for any reason other than the user clicking elsewhere.
            /// </param>
            public void Commit(bool refocus)
            {
                System.Diagnostics.Debug.WriteLine("_isIncomplete = false");
                this._isIncomplete = false;
                if (this.Text == this._originalText)
                {
                    this.Cancel(refocus);
                    return;
                }

                bool valid = true;
                foreach (IHierarchyNode sibling in this._item.Parent.GetChildren(false))
                {
                    if (Object.ReferenceEquals(sibling, this._item))
                    {
                        continue;
                    }

                    if (sibling is FolderNode && sibling.Text == this.Text)
                    {
                        Application app = Application.Current;
                        ICommandServiceProvider sp = app as ICommandServiceProvider;
                        IMessageBoxService msgService = sp.GetService<IMessageBoxService>();
                        msgService.ShowStandardErrorBox("A folder already exists at the current scope in the project.", null);
                        valid = false;
                        break;
                    }
                }

                if (valid)
                {
                    this._item.Text = this.Text;
                    this._item.ModifiedScopeNode.IsModified = true;

                    string include = this._item.Model.Include;
                    int index = include.LastIndexOf('\\');
                    if (index == -1)
                    {
                        index = 0;
                    }

                    string baseInclude = include.Substring(0, index);
                    if (baseInclude.Length > 0)
                    {
                        baseInclude += '\\';
                    }

                    this._item.Model.Include = baseInclude + this._item.Text;
                    foreach (IHierarchyNode child in this._item.GetChildren(true))
                    {
                        HierarchyNode node = child as HierarchyNode;
                        if (node == null)
                        {
                            continue;
                        }

                        node.ValidateAndEnsureFilter();
                    }
                }

                EventHandler<RenameControllerCompletedEventArgs> handler = this.Completed;
                if (handler != null)
                {
                    RenameControllerCompletedResult result;
                    if (valid)
                    {
                        result = RenameControllerCompletedResult.Successful;
                    }
                    else
                    {
                        result = RenameControllerCompletedResult.ValidationFailed;
                    }

                    handler(this, new RenameControllerCompletedEventArgs(refocus, result));
                }

                if (valid)
                {
                    System.Diagnostics.Debug.WriteLine("_isIncomplete = false");
                    this._isIncomplete = false;
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("_isIncomplete = true");
                    this._isIncomplete = true;
                }
            }
            #endregion Methods
        } // FolderNode.FolderNodeRenameController {Class}
        #endregion
    } // RSG.Project.ViewModel.FolderNode {Class}
} // RSG.Project.ViewModel {Namespace}
