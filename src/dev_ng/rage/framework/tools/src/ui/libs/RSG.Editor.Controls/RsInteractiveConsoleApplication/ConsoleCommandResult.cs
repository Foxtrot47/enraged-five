﻿//---------------------------------------------------------------------------------------------
// <copyright file="ConsoleCommandResult.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    /// <summary>
    /// Enumeration containing the possible results of running a command at
    /// the interactive console.
    /// </summary>
    public enum ConsoleCommandResult
    {
        /// <summary>
        /// Command executed successfully.
        /// </summary>
        Ok,

        /// <summary>
        /// Command was ignored due to it either being not a registered command
        /// or because it contained only whitespace.
        /// </summary>
        Ignored,

        /// <summary>
        /// An exception occurred while executing the command.
        /// </summary>
        Faulted,

        /// <summary>
        /// The command requested that we exit the console.
        /// </summary>
        Exit
    } // RSG.Editor.Controls.ConsoleCommandResult {Enum}
} // RSG.Editor.Controls {Namespace}
