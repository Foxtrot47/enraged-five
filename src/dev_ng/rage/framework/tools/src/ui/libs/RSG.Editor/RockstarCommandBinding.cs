﻿//---------------------------------------------------------------------------------------------
// <copyright file="RockstarCommandBinding.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using RSG.Editor.Resources;

    /// <summary>
    /// Binds a System.Window.Input.ICommand object to the handlers that determine whether the
    /// command can be executed and what happens when it does get executed.
    /// </summary>
    internal sealed class RockstarCommandBinding : CommandBinding
    {
        #region Fields
        /// <summary>
        /// The private reference to the handler who is responsible for determining whether the
        /// command can be executed.
        /// </summary>
        private CanExecuteCommandDelegate _canExecuteHandler;

        /// <summary>
        /// The private reference to the handler who is responsible for the implementation of
        /// the command.
        /// </summary>
        private ExecuteCommandDelegate _executeHandler;

        /// <summary>
        /// A private value indicating whether this binding should be executed as a awaited
        /// operation.
        /// </summary>
        private bool _isAsync;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="RockstarCommandBinding"/> class.
        /// </summary>
        /// <param name="command">
        /// The command to base the new System.Windows.Input.RoutedCommand on.
        /// </param>
        /// <param name="executeHandler">
        /// The handler that gets called once the specified command is executed.
        /// </param>
        /// <param name="canExecuteHandler">
        /// The handler that gets called to determine whether the command can be executed.
        /// </param>
        internal RockstarCommandBinding(
            ICommand command,
            ExecuteCommandDelegate executeHandler,
            CanExecuteCommandDelegate canExecuteHandler)
            : base(command)
        {
            if (executeHandler != null)
            {
                this._executeHandler = executeHandler;
                this.Executed += this.WrapExecution;
            }

            if (canExecuteHandler != null)
            {
                this._canExecuteHandler = canExecuteHandler;
                this.CanExecute += this.WrapCanExecute;
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RockstarCommandBinding"/> class.
        /// </summary>
        /// <param name="command">
        /// The command to base the new System.Windows.Input.RoutedCommand on.
        /// </param>
        /// <param name="executeHandler">
        /// The handler that gets called once the specified command is executed.
        /// </param>
        /// <param name="canExecuteHandler">
        /// The handler that gets called to determine whether the command can be executed.
        /// </param>
        /// <param name="isAsync">
        /// A value indicating whether this binding should be executed as a awaited operation.
        /// </param>
        internal RockstarCommandBinding(
            ICommand command,
            ExecuteCommandDelegate executeHandler,
            CanExecuteCommandDelegate canExecuteHandler,
            bool isAsync)
            : base(command)
        {
            this._isAsync = isAsync;
            if (executeHandler != null)
            {
                this._executeHandler = executeHandler;
                this.Executed += this.WrapExecution;
            }

            if (canExecuteHandler != null)
            {
                this._canExecuteHandler = canExecuteHandler;
                this.CanExecute += this.WrapCanExecute;
            }
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Wraps the handler that is used to determine whether the command can be executed so
        /// that a more focused argument object can be passed to the handler and exceptions can
        /// be handled correctly.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The event data.
        /// </param>
        private void WrapCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (this._canExecuteHandler == null)
            {
                return;
            }

            CanExecuteCommandData args = new CanExecuteCommandData(e.Command);
            args.ContinueRouting = e.ContinueRouting;
            args.Handled = e.Handled;
            args.OriginalSource = e.OriginalSource;
            args.Sender = sender as UIElement;
            args.Source = e.Source;

            try
            {
                e.CanExecute = this._canExecuteHandler(args);
            }
            catch (Exception ex)
            {
                e.CanExecute = false;
                RoutedCommand command = e.Command as RoutedCommand;
                if (command == null || this._executeHandler.Method == null)
                {
                    throw new CanExecuteCommandException(ex);
                }
                else
                {
                    string msg = ErrorStringTable.GetString(
                        "CustomExecuteExceptionMessage",
                        (e.Command as RoutedCommand).Name,
                        this._executeHandler.Method.Name);

                    throw new CanExecuteCommandException(msg, ex);
                }
            }
            finally
            {
                e.ContinueRouting = args.ContinueRouting;
                e.Handled = args.Handled;
            }
        }

        /// <summary>
        /// Wraps the implementation of the command so that a more focused argument object can
        /// be passed to the handler, exceptions can be handled correctly, and threads can be
        /// managed.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The event data.
        /// </param>
        private async void WrapExecution(object sender, ExecutedRoutedEventArgs e)
        {
            if (this._executeHandler == null)
            {
                return;
            }

            CommandDefinition definition = RockstarCommandManager.GetDefinition(e.Command);
            if (definition != null)
            {
                if (definition.IgnoreNextExecution)
                {
                    definition.IgnoreNextExecution = false;
                    return;
                }
            }

            ExecuteCommandData args = new ExecuteCommandData(e.Command);
            args.Handled = e.Handled;
            args.OriginalSource = e.OriginalSource;
            args.Sender = sender as UIElement;
            args.Source = e.Source;

            try
            {
                args.Handled = true;
                if (this._isAsync)
                {
                    Task task = Task.Factory.StartNew(
                        new Action(
                            delegate
                            {
                                this._executeHandler(args);
                            }));

                    await Task.WhenAll(task);
                }
                else
                {
                    this._executeHandler(args);
                }
            }
            catch (Exception ex)
            {
                if (this._isAsync && ex is OperationCanceledException)
                {
                    return;
                }

                RoutedCommand command = e.Command as RoutedCommand;
                if (command == null || this._executeHandler.Method == null)
                {
                    throw new ExecuteCommandException(ex);
                }
                else
                {
                    string msg = ErrorStringTable.GetString(
                        "CustomExecuteExceptionMessage",
                        (e.Command as RoutedCommand).Name,
                        this._executeHandler.Method.Name);

                    throw new ExecuteCommandException(msg, ex);
                }
            }
            finally
            {
                e.Handled = args.Handled;
            }
        }
        #endregion Methods
    } // RSG.Editor.RockstarCommandBinding {Class}
} // RSG.Editor {Namespace}
