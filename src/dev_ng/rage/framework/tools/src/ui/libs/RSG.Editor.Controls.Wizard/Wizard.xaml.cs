﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RSG.Editor.Controls.Wizard
{
    /// <summary>
    /// Wizard UserControl; allowing navigation and page definitions in XAML.
    /// </summary>
    /// See the Rockstar Games Tools Installer for example usage.
    /// 
    public partial class Wizard : UserControl
    {
        #region Events
        /// <summary>
        /// Event trigged on page changing (can be canceled).
        /// </summary>
        public event EventHandler<WizardPageChangingEventArgs> PageChanging;

        /// <summary>
        /// Event triggers on page changed.
        /// </summary>
        public event EventHandler<WizardPageChangedEventArgs> PageChanged;
        #endregion // Events

        #region Properties
        /// <summary>
        /// Gets the active wizard page's index.
        /// </summary>
        public int ActivePageIndex
        {
            get { return (int)this.GetValue(ActivePageIndexProperty); }
            set { this.SetValue(ActivePageIndexProperty, value); }
        }

        /// <summary>
        /// Gets the active wizard page's index.
        /// </summary>
        public int ActivePageDisplayIndex
        {
            get { return (int)this.GetValue(ActivePageDisplayIndexProperty); }
            private set { this.SetValue(ActivePageDisplayIndexPropertyKey, value); }
        }

        /// <summary>
        /// Gets or sets the active wizard page.
        /// </summary>
        public WizardPage ActivePage
        {
            get { return (WizardPage)this.GetValue(ActivePageProperty); }
            set { this.SetValue(ActivePageProperty, value); }
        }

        /// <summary>
        /// Available wizard pages.
        /// </summary>
        public ObservableCollection<WizardPage> Pages
        {
            get { return (ObservableCollection<WizardPage>)this.GetValue(PagesProperty); }
            set { this.SetValue(PagesProperty, value); }
        }

        /// <summary>
        /// Number of available wizard pages.
        /// </summary>
        public int PageCount
        {
            get { return (int)this.GetValue(PageCountProperty); }
            private set { this.SetValue(PageCountProperty, value); }
        }

        /// <summary>
        /// Toggle for displaying the page progress in the bottom left.
        /// </summary>
        public bool ShowPageProgress
        {
            get { return (bool)this.GetValue(ShowPageProgressProperty); }
            set { this.SetValue(ShowPageProgressProperty, value); }
        }
        #endregion // Properties

        #region Dependency Properties
        /// <summary>
        /// Identifies the <see cref="ActivePageIndex"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ActivePageIndexProperty;

        /// <summary>
        /// Identifies the <see cref="ActivePageDisplayIndex"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ActivePageDisplayIndexProperty;
        public static readonly DependencyPropertyKey ActivePageDisplayIndexPropertyKey;

        /// <summary>
        /// Identifies the <see cref="ActivePage"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ActivePageProperty;

        /// <summary>
        /// Identifies the <see cref="Pages"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty PagesProperty;

        /// <summary>
        /// Identifies the <see cref="PageCount"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty PageCountProperty;
        public static readonly DependencyPropertyKey PageCountPropertyKey;

        /// <summary>
        /// Identifies the <see cref="ShowPageProgress"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ShowPageProgressProperty;
        #endregion // Dependency Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Wizard()
        {
            InitializeComponent();
            this.Pages = new ObservableCollection<WizardPage>();
        }

        /// <summary>
        /// Static constructor; initialising static members.
        /// </summary>
        static Wizard()
        {
            ActivePageIndexProperty = DependencyProperty.Register("ActivePageIndex",
                typeof(int), typeof(Wizard), new PropertyMetadata(-1, OnActivePageIndexChanged));
            ActivePageDisplayIndexPropertyKey = DependencyProperty.RegisterReadOnly("ActivePageDisplayIndex",
                typeof(int), typeof(Wizard), new PropertyMetadata(-1, null));
            ActivePageProperty = DependencyProperty.Register("ActivePage",
                typeof(WizardPage), typeof(Wizard), new PropertyMetadata(null, OnActivePageChanged));
            PagesProperty = DependencyProperty.Register("Pages",
                typeof(ObservableCollection<WizardPage>), typeof(Wizard), new PropertyMetadata(null, OnPagesChanged));
            PageCountPropertyKey = DependencyProperty.RegisterReadOnly("PageCount",
                typeof(int), typeof(Wizard), new PropertyMetadata(0, null));
            ShowPageProgressProperty = DependencyProperty.Register("ShowPageProgress",
                typeof(bool), typeof(Wizard), new PropertyMetadata(true, null));
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Navigate forwards to next page.
        /// </summary>
        /// <returns></returns>
        public bool NavigateNext()
        {
            return (NavigateTo(this.ActivePageIndex + 1));
        }

        /// <summary>
        /// Navigate back to previous page.
        /// </summary>
        /// <returns></returns>
        public bool NavigatePrevious()
        {
            return (NavigateTo(this.ActivePageIndex - 1));
        }

        /// <summary>
        /// Navigate to a particular page index.
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <returns>true iff active page was changed; false otherwise</returns>
        public bool NavigateTo(int pageIndex)
        {
            if (pageIndex == this.ActivePageIndex)
                return (false);

            if (pageIndex >= this.Pages.Count)
            {
                return false;
            }

            // Otherwise move; after range check.
            Debug.Assert(pageIndex >= 0 && pageIndex < this.Pages.Count,
                String.Format("Page index {0} out-of-range.  Internal error.", pageIndex));
            if (!(pageIndex >= 0 && pageIndex < this.Pages.Count))
                throw (new ArgumentOutOfRangeException(String.Format("Page index {0} out-of-range.  Internal error.", pageIndex)));

            WizardPageChangingEventArgs pce = new WizardPageChangingEventArgs(this.ActivePage, this.Pages.ElementAt(pageIndex));
            if (null != PageChanging)
                PageChanging(this, pce);
            
            // Move unless canceled by PageChanging event.
            if (pce.Cancel)
                return (false);

            // Set active page.
            NavigateDirection direction = (pageIndex > this.ActivePageIndex) ?
                NavigateDirection.Forwards : NavigateDirection.Backwards;
            WizardPage oldPage = this.ActivePage;
            this.ActivePage.DataContext = this.ActivePage.DataContext;
            this.ActivePage = this.Pages.ElementAt(pageIndex);

            if (null != PageChanged)
                PageChanged(this, new WizardPageChangedEventArgs(direction, oldPage, this.ActivePage));

            return (true);
        }

        /// <summary>
        /// Navigate to a particular page.
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public bool NavigateTo(WizardPage page)
        {
            int pageIndex = this.Pages.IndexOf(page);
            if (-1 == pageIndex)
                throw (new ArgumentException(String.Format("Page '{0}' invalid; not in Wizard pages collection.", page.Header)));

            return (this.NavigateTo(pageIndex));
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// ActivePageIndex dependency property change handler; maintains
        /// the ActivePage property.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="args"></param>
        private static void OnActivePageIndexChanged(DependencyObject s, 
            DependencyPropertyChangedEventArgs args)
        {
            Wizard wizard = (s as Wizard);
            if (null == wizard)
                return;

            int newIndex = (int)args.NewValue;
            Debug.Assert(newIndex > -1 && newIndex < wizard.Pages.Count,
                String.Format("Page index {0} out-of-range.  Internal error.", newIndex));
            if (!(newIndex >= -1 && newIndex < wizard.Pages.Count))
                throw (new ArgumentOutOfRangeException(String.Format("Page index {0} out-of-range.  Internal error.", newIndex)));

            WizardPage newPage = null;
            if (-1 != newIndex) 
                newPage = wizard.Pages.ElementAt(newIndex);
            if (wizard.ActivePage != newPage)
            {
                wizard.ActivePage = newPage;
                wizard.ActivePageDisplayIndex = newIndex + 1;
            }
        }

        /// <summary>
        /// ActivePage dependency property change handler; maintains the 
        /// ActivePageIndex property.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="args"></param>
        private static void OnActivePageChanged(DependencyObject s, 
            DependencyPropertyChangedEventArgs args)
        {
            Wizard wizard = (s as Wizard);
            if (null == wizard)
                return;

            WizardPage newPage = (WizardPage)args.NewValue;
            Debug.Assert(wizard.Pages.Contains(newPage),
                "Unknown wizard page; not in Wizard Pages collection.  Internal error.");
            if (!wizard.Pages.Contains(newPage))
                throw (new ArgumentException("Unknown wizard page; not in Wizard Pages collection.  Internal error."));

            int newPageIndex = wizard.Pages.IndexOf(newPage);
            if (wizard.ActivePageIndex != newPageIndex)
            {
                wizard.ActivePageIndex = newPageIndex;
                wizard.ActivePageDisplayIndex = newPageIndex + 1;
            }
        }

        /// <summary>
        /// Pages collection dependency property change handler; maintains the
        /// readonly PageCount property.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="args"></param>
        private static void OnPagesChanged(DependencyObject s,
            DependencyPropertyChangedEventArgs args)
        {
            Wizard wizard = (s as Wizard);
            if (null == wizard)
                return;

            ObservableCollection<WizardPage> newPages = (ObservableCollection<WizardPage>)args.NewValue;
            if (wizard.Pages.Count != newPages.Count)
                wizard.PageCount = newPages.Count;
        }
        #endregion // Private Methods
    }

} // RSG.Editor.Controls.Wizard namespace
