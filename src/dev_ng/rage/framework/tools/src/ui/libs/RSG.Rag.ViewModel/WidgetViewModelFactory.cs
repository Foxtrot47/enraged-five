﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Logging;
using RSG.Rag.Core;
using RSG.Rag.Widgets;
using System.Diagnostics;
using RSG.Rag.ViewModel.Widgets;

namespace RSG.Rag.ViewModel
{
    /// <summary>
    /// This class is responsible for creating view models for the various rag widgets.
    /// It uses reflection to determine the widget -> view model mapping.
    /// If a widget type isn't found in the lookup it iterates up the widget's inheritance
    /// hierarchy looking for a type that is.
    /// </summary>
    public class WidgetViewModelFactory
    {
        #region Constants
        /// <summary>
        /// Log context.
        /// </summary>
        private const String VM_FACTORY_CTX = "Widget ViewModel Factory";
        #endregion // Constants

        #region Member Data
        /// <summary>
        /// Reference to the log object.
        /// </summary>
        private readonly ILog _log;

        /// <summary>
        /// Mapping of widget types to view model types.
        /// </summary>
        private readonly IDictionary<Type, Type> _viewModelTypeForWidgetType = new Dictionary<Type, Type>();

        /// <summary>
        /// Set of unknown widget types used to remove the reflection overhead if we know a type doesn't
        /// exist, but get asked multiple times to create a view model for it.
        /// </summary>
        private readonly ISet<Type> _unknownTypes = new HashSet<Type>();
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// WidgetViewModel discovery.
        /// </summary>
        public WidgetViewModelFactory(ILog log)
        {
            _log = log;

            // Type discovery
            _log.ProfileCtx(VM_FACTORY_CTX, "Discovering widget view models");

            // Iterate over the loaded assemblies 
            Assembly[] loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies();
            Assembly viewModelAssembly = GetType().Assembly;

            foreach (Assembly assembly in loadedAssemblies)
            {
                // Is this an assembly we're interested in.
                if (assembly == viewModelAssembly ||
                    assembly.GetName().Name.StartsWith("RSG.Rag.Widgets", StringComparison.OrdinalIgnoreCase))
                {
                    RegisterTypesInAssembly(assembly);
                }
            }

            _log.ProfileEnd();
        }
        #endregion // Constructor(s)

        #region Factory Methods
        /// <summary>
        /// Creates a view model for the passed in widget.
        /// </summary>
        /// <param name="widget">Widget to create a view model for.</param>
        /// <returns>A new <see cref="WidgetViewModel"/> object.</returns>
        public IWidgetViewModel CreateViewModel(IWidget widget)
        {
            Type widgetType = widget.GetType();
            Type widgetVmType;
            if (!_viewModelTypeForWidgetType.TryGetValue(widgetType, out widgetVmType))
            {
                // Try and find a base type that we know about.
                Type baseWidgetType = widgetType.BaseType;
                while (baseWidgetType != null)
                {
                    if (_viewModelTypeForWidgetType.TryGetValue(baseWidgetType, out widgetVmType))
                    {
                        break;
                    }
                    else if (baseWidgetType.IsGenericType)
                    {
                        // Try and do something fancy if the base type is a generic type and the vm is a generic type definition.
                        Type baseGenericTypeDefinition = baseWidgetType.GetGenericTypeDefinition();
                        if (_viewModelTypeForWidgetType.TryGetValue(baseGenericTypeDefinition, out widgetVmType))
                        {
                            if (widgetVmType.IsGenericTypeDefinition)
                            {
                                widgetVmType = widgetVmType.MakeGenericType(baseWidgetType.GenericTypeArguments);
                            }
                            break;
                        }
                    }
                    baseWidgetType = baseWidgetType.BaseType;
                }
                    
                if (widgetVmType == null)
                {
                    Debug.Fail("Programmer error!  No widget view model type registered for widget type.");
                    throw new ArgumentNullException("widgetVmType", "Programmer error!  Unable to determine widget view model for widget.");
                }

                // Keep track of this additional widget -> vm mapping.
                _viewModelTypeForWidgetType.Add(widgetType, widgetVmType);
            }

            // Widget groups require a reference to the factory for generating their child vms.
            if (widget is IWidgetGroup)
            {
                return (IWidgetViewModel)Activator.CreateInstance(widgetVmType, widget, this);
            }
            else
            {
                return (IWidgetViewModel)Activator.CreateInstance(widgetVmType, widget);
            }
        }
        #endregion // Factory Methods

        #region Registration Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="assembly"></param>
        private void RegisterTypesInAssembly(Assembly assembly)
        {
            Type baseVmType = typeof(IWidgetViewModel);

            // Iterate over all types in the assembly that are classes and not abstract.
            foreach (Type vmType in assembly.GetTypes().Where(type => type.IsClass && !type.IsAbstract))
            {
                if (baseVmType.IsAssignableFrom(vmType))
                {
                    Type widgetType = GetWidgetTypeForVmType(vmType);
                    Debug.Assert(widgetType != null, "Couldn't determine the widget type from view model type.");
                    if (widgetType == null)
                    {
                        throw new ArgumentNullException("widgetType", "Couldn't determine the widget type from view model type.");
                    }

                    _viewModelTypeForWidgetType.Add(widgetType, vmType);
                    _log.MessageCtx(VM_FACTORY_CTX, "Registered '{0}' for '{1}'.", vmType.Name, widgetType.Name);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="widgetVmType"></param>
        /// <returns></returns>
        private Type GetWidgetTypeForVmType(Type widgetVmType)
        {
            Type widgetType = null;
            Type genericVmType = typeof(WidgetViewModel<>);
            Type iWidgetType = typeof(IWidget);

            while (widgetVmType.BaseType != null)
            {
                widgetVmType = widgetVmType.BaseType;

                if (widgetVmType.IsGenericType &&
                    widgetVmType.GetGenericTypeDefinition() == genericVmType &&
                    iWidgetType.IsAssignableFrom(widgetVmType.GenericTypeArguments[0]))
                {
                    widgetType = widgetVmType.GenericTypeArguments[0];
                    if (widgetType.ContainsGenericParameters)
                    {
                        widgetType = widgetType.GetGenericTypeDefinition();
                    }
                    break;
                }
            }

            return widgetType;
        }
        #endregion // Registration Methods
    } // WidgetViewModelFactory
}
