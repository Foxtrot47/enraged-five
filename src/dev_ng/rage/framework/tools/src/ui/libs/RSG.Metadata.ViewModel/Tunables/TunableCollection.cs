﻿//---------------------------------------------------------------------------------------------
// <copyright file="TunableCollection.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Tunables
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using RSG.Editor.Model;
    using RSG.Editor.View;
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// A collection class used by hierarchical tunable view models to construct its child
    /// collection.
    /// </summary>
    public class TunableCollection
        : CollectionConverter<ITunable, ITunableViewModel>,
        IReadOnlyViewModelCollection<ITunableViewModel>
    {
        #region Fields
        /// <summary>
        /// The tunable view model that is acting as the parent of any tunable that is created
        /// through this collection.
        /// </summary>
        private ITunableViewModel _parent;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="TunableCollection"/> class.
        /// </summary>
        /// <param name="source">
        /// The source model items for this collection.
        /// </param>
        /// <param name="parent">
        /// The tunable view model that is acting as the parent of any tunable created through
        /// this collection.
        /// </param>
        public TunableCollection(IEnumerable<ITunable> source, ITunableViewModel parent)
            : base(Enumerable.Empty<object>(), null)
        {
            this._parent = parent;
            this.ResetSourceItems(source);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="TunableCollection"/> class.
        /// </summary>
        /// <param name="parent">
        /// The tunable view model that is acting as the parent of any tunable created through
        /// this collection.
        /// </param>
        protected TunableCollection(ITunableViewModel parent)
            : base(Enumerable.Empty<object>(), null)
        {
            this._parent = parent;
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Creates a tunable view model from the specified tunable model object.
        /// </summary>
        /// <param name="tunable">
        /// The tunable used to construct the view model.
        /// </param>
        /// <returns>
        /// A new tunable view model constructed from the specified tunable model object.
        /// </returns>
        protected override ITunableViewModel ConvertItem(ITunable tunable)
        {
            Type type = tunable.GetType();
            string typeName = type.Name;
            if (String.Equals(typeName, "ArrayTunable"))
            {
                ArrayTunable castTunable = tunable as ArrayTunable;
                return new ArrayTunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "BitsetTunable"))
            {
                BitsetTunable castTunable = tunable as BitsetTunable;
                return new BitsetTunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "BoolTunable"))
            {
                BoolTunable castTunable = tunable as BoolTunable;
                return new BoolTunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "BoolVectorTunable"))
            {
                BoolVectorTunable castTunable = tunable as BoolVectorTunable;
                return new BoolVectorTunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "BoolVTunable"))
            {
                BoolVTunable castTunable = tunable as BoolVTunable;
                return new BoolVTunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "CharTunable"))
            {
                CharTunable castTunable = tunable as CharTunable;
                return new CharTunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "ColourTunable"))
            {
                ColourTunable castTunable = tunable as ColourTunable;
                return new ColourTunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "DoubleTunable"))
            {
                DoubleTunable castTunable = tunable as DoubleTunable;
                return new DoubleTunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "EnumTunable"))
            {
                EnumTunable castTunable = tunable as EnumTunable;
                return new EnumTunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "Float16Tunable"))
            {
                Float16Tunable castTunable = tunable as Float16Tunable;
                return new Float16TunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "FloatTunable"))
            {
                FloatTunable castTunable = tunable as FloatTunable;
                return new FloatTunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "IntTunable"))
            {
                IntTunable castTunable = tunable as IntTunable;
                return new IntTunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "Mat33VTunable"))
            {
                Mat33VTunable castTunable = tunable as Mat33VTunable;
                return new Mat33VTunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "Mat34VTunable"))
            {
                Mat34VTunable castTunable = tunable as Mat34VTunable;
                return new Mat34VTunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "Mat44VTunable"))
            {
                Mat44VTunable castTunable = tunable as Mat44VTunable;
                return new Mat44VTunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "Matrix34Tunable"))
            {
                Matrix34Tunable castTunable = tunable as Matrix34Tunable;
                return new Matrix34TunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "Matrix44Tunable"))
            {
                Matrix44Tunable castTunable = tunable as Matrix44Tunable;
                return new Matrix44TunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "PointerTunable"))
            {
                PointerTunable castTunable = tunable as PointerTunable;
                return new PointerTunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "PtrDiffTunable"))
            {
                PtrDiffTunable castTunable = tunable as PtrDiffTunable;
                return new PtrDiffTunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "ScalarVTunable"))
            {
                ScalarVTunable castTunable = tunable as ScalarVTunable;
                return new ScalarVTunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "ShortTunable"))
            {
                ShortTunable castTunable = tunable as ShortTunable;
                return new ShortTunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "Signed16Tunable"))
            {
                Signed16Tunable castTunable = tunable as Signed16Tunable;
                return new Signed16TunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "Signed32Tunable"))
            {
                Signed32Tunable castTunable = tunable as Signed32Tunable;
                return new Signed32TunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "Signed64Tunable"))
            {
                Signed64Tunable castTunable = tunable as Signed64Tunable;
                return new Signed64TunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "Signed8Tunable"))
            {
                Signed8Tunable castTunable = tunable as Signed8Tunable;
                return new Signed8TunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "SizeTTunable"))
            {
                SizeTTunable castTunable = tunable as SizeTTunable;
                return new SizeTTunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "StringTunable"))
            {
                StringTunable castTunable = tunable as StringTunable;
                return new StringTunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "StructTunable"))
            {
                StructTunable castTunable = tunable as StructTunable;
                return new StructTunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "Unsigned16Tunable"))
            {
                Unsigned16Tunable castTunable = tunable as Unsigned16Tunable;
                return new Unsigned16TunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "Unsigned32Tunable"))
            {
                Unsigned32Tunable castTunable = tunable as Unsigned32Tunable;
                return new Unsigned32TunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "Unsigned64Tunable"))
            {
                Unsigned64Tunable castTunable = tunable as Unsigned64Tunable;
                return new Unsigned64TunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "Unsigned8Tunable"))
            {
                Unsigned8Tunable castTunable = tunable as Unsigned8Tunable;
                return new Unsigned8TunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "Vec2VTunable"))
            {
                Vec2VTunable castTunable = tunable as Vec2VTunable;
                return new Vec2VTunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "Vec3VTunable"))
            {
                Vec3VTunable castTunable = tunable as Vec3VTunable;
                return new Vec3VTunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "Vec4VTunable"))
            {
                Vec4VTunable castTunable = tunable as Vec4VTunable;
                return new Vec4VTunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "Vector2Tunable"))
            {
                Vector2Tunable castTunable = tunable as Vector2Tunable;
                return new Vector2TunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "Vector3Tunable"))
            {
                Vector3Tunable castTunable = tunable as Vector3Tunable;
                return new Vector3TunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "Vector4Tunable"))
            {
                Vector4Tunable castTunable = tunable as Vector4Tunable;
                return new Vector4TunableViewModel(castTunable, this._parent);
            }
            else if (String.Equals(typeName, "MapTunable"))
            {
                MapTunable castTunable = tunable as MapTunable;
                return new MapTunableViewModel(castTunable, this._parent);
            }

            return null;
        }
        #endregion Methods
    } // RSG.Metadata.ViewModel.Tunables.TunableCollection {Class}
} // RSG.Metadata.ViewModel.Tunables {Namespace}
