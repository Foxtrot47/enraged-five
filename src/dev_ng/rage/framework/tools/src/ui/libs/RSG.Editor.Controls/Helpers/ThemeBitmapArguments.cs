﻿//---------------------------------------------------------------------------------------------
// <copyright file="ThemeBitmapArguments.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Helpers
{
    using System;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Defines the arguments needed to create a themed bitmap source object.
    /// </summary>
    public struct ThemeBitmapArguments
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Background"/> property.
        /// </summary>
        private Color _background;

        /// <summary>
        /// The private field used for the <see cref="Bias"/> property.
        /// </summary>
        private Color _bias;

        /// <summary>
        /// The private field used for the <see cref="Enabled"/> property.
        /// </summary>
        private bool _enabled;

        /// <summary>
        /// The private field used for the <see cref="Input"/> property.
        /// </summary>
        private BitmapSource _input;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ThemeBitmapArguments"/> structure.
        /// </summary>
        /// <param name="input">
        /// The bitmap source object that will be transformed into a themed bitmap source
        /// object.
        /// </param>
        public ThemeBitmapArguments(BitmapSource input)
        {
            this._input = input;
            this._enabled = true;
            this._background = Colors.Transparent;
            this._bias = Colors.Transparent;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the colour that is used as the background colour of the themed bitmap
        /// source object.
        /// </summary>
        public Color Background
        {
            get { return this._background; }
            set { this._background = value; }
        }

        /// <summary>
        /// Gets or sets the bias values for each colour channel when transforming the input
        /// bitmap source to a grey-scaled image after the theme transform.
        /// </summary>
        public Color Bias
        {
            get { return this._bias; }
            set { this._bias = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the input bitmap source should be converted
        /// to a grey-scaled image after the theme transform.
        /// </summary>
        public bool Enabled
        {
            get { return this._enabled; }
            set { this._enabled = value; }
        }

        /// <summary>
        /// Gets the bitmap source object that will be transformed into a themed bitmap source
        /// object.
        /// </summary>
        public BitmapSource Input
        {
            get { return this._input; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Determines whether the specified objects are not equal to each other.
        /// </summary>
        /// <param name="args1">
        /// The first object to compare.
        /// </param>
        /// <param name="args2">
        /// The second object to compare.
        /// </param>
        /// <returns>
        /// True if the specified objects are not equal to each other; otherwise, false.
        /// </returns>
        public static bool operator !=(ThemeBitmapArguments args1, ThemeBitmapArguments args2)
        {
            return !args1.Equals(args2);
        }

        /// <summary>
        /// Determines whether the specified objects are equal to each other.
        /// </summary>
        /// <param name="args1">
        /// The first object to compare.
        /// </param>
        /// <param name="args2">
        /// The second object to compare.
        /// </param>
        /// <returns>
        /// True if the specified objects are equal to each other; otherwise, false.
        /// </returns>
        public static bool operator ==(ThemeBitmapArguments args1, ThemeBitmapArguments args2)
        {
            return args1.Equals(args2);
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">
        /// The object to compare with the current object.
        /// </param>
        /// <returns>
        /// True if the specified object is equal to the current object; otherwise, false.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (!(obj is ThemeBitmapArguments))
            {
                return false;
            }

            return this.Equals((ThemeBitmapArguments)obj);
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="other">
        /// The object to compare with the current object.
        /// </param>
        /// <returns>
        /// True if the specified object is equal to the current object; otherwise, false.
        /// </returns>
        public bool Equals(ThemeBitmapArguments other)
        {
            if (!Object.ReferenceEquals(this._input, other._input))
            {
                return false;
            }

            if (this._enabled != other._enabled)
            {
                return false;
            }

            if (this._background != other._background)
            {
                return false;
            }

            return this._bias == other._bias;
        }

        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        /// A hash code for the current object.
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion Methods
    } // RSG.Editor.Controls.Helpers.ThemeBitmapArguments {Structure}
} // RSG.Editor.Controls.Helpers {Namespace}
