﻿//---------------------------------------------------------------------------------------------
// <copyright file="DocumentItem.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock.ViewModel
{
    using System;
    using System.ComponentModel;
    using System.IO;
    using System.Windows.Threading;
    using RSG.Editor.Model;

    /// <summary>
    /// Represents a single item inside the <see cref="DocumentPane"/> class.
    /// </summary>
    public abstract class DocumentItem
        : NotifyPropertyChangedBase, IDockingPaneItem, IDockingElement, ISaveableDocument
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="CanHide"/> property.
        /// </summary>
        private bool _canHide;

        /// <summary>
        /// The private field used for the <see cref="Content"/> property.
        /// </summary>
        private object _content;

        /// <summary>
        /// The private field used for the <see cref="Filename"/> property.
        /// </summary>
        private string _filename;

        /// <summary>
        /// The private field used for the <see cref="FullPath"/> property.
        /// </summary>
        private string _fullpath;

        /// <summary>
        /// The private field used for the <see cref="IsModified"/> property.
        /// </summary>
        private bool _isModified;

        /// <summary>
        /// The private field used for the <see cref="IsReadOnly"/> property.
        /// </summary>
        private bool _isReadOnly;

        /// <summary>
        /// The private field used for the <see cref="IsSelected"/> property.
        /// </summary>
        private bool _isSelected;

        /// <summary>
        /// The private field used for the <see cref="PaneIndex"/> property.
        /// </summary>
        private int _paneIndex;

        /// <summary>
        /// The private field used for the <see cref="ParentPane"/> property.
        /// </summary>
        private DockingPane _parentPane;

        /// <summary>
        /// The private field used for the <see cref="UndoEngine"/> property.
        /// </summary>
        private UndoEngine _undoEngine;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DocumentItem"/> class.
        /// </summary>
        /// <param name="parentPane">
        /// The parent docking pane.
        /// </param>
        /// <param name="fullpath">
        /// The full path to the location of the document.
        /// </param>
        protected DocumentItem(DockingPane parentPane, string fullpath)
        {
            this._parentPane = parentPane;
            this._fullpath = fullpath;
            this._filename = Path.GetFileName(this.FullPath);
            this._undoEngine = new UndoEngine();
            this._undoEngine.DirtyStateChanged += this.UndoEngineDirtyStateChanged;
            this._canHide = true;
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs just after this object has been saved successfully.
        /// </summary>
        public event EventHandler Saved;
        #endregion Events

        #region Properties
        /// <summary>
        /// Gets or sets a valid indicating whether this tool window item can be hidden from
        /// the user.
        /// </summary>
        public bool CanHide
        {
            get { return this._canHide; }
            set { this._canHide = value; }
        }

        /// <summary>
        /// Gets a value indicating whether this document can be saved by the user.
        /// </summary>
        public virtual bool CanSave
        {
            get { return true; }
        }

        /// <summary>
        /// Gets or sets the content used for this document. This usually is the root view
        /// model for the data contained within the document.
        /// </summary>
        public object Content
        {
            get { return this._content; }
            set { this.SetProperty(ref this._content, value); }
        }

        /// <summary>
        /// Gets or sets the filename to use to display this document.
        /// </summary>
        public virtual string Filename
        {
            get { return this._filename; }
            set { this.SetProperty(ref this._filename, value); }
        }

        /// <summary>
        /// Gets the friendly save path that this item pushes into the command system
        /// to display on menu items.
        /// </summary>
        public virtual string FriendlySavePath
        {
            get { return this.Filename; }
        }

        /// <summary>
        /// Gets or sets the full file path to the location of this file.
        /// </summary>
        public virtual string FullPath
        {
            get { return this._fullpath; }
            set { this.SetProperty(ref this._fullpath, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this document item currently contains
        /// modified unsaved data.
        /// </summary>
        public bool IsModified
        {
            get { return this._isModified; }
            protected set { this.SetProperty(ref this._isModified, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this document is currently locked inside
        /// the windows explorer.
        /// </summary>
        public bool IsReadOnly
        {
            get { return this._isReadOnly; }
            set { this.SetProperty(ref this._isReadOnly, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this document item is currently the
        /// selected item inside its parent pane.
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return this._isSelected;
            }

            set
            {
                if (this.SetProperty(ref this._isSelected, value) && value)
                {
                    this.ViewSite.ActiveDocument = this;
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether this document represents a temporary document. A
        /// temporary file cannot be saved to the same location and needs to be "Saved As...".
        /// </summary>
        public virtual bool IsTemporaryFile
        {
            get
            {
                if (this.FullPath.StartsWith(Path.GetTempPath()))
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Gets or sets the index for this item inside its parent pane.
        /// </summary>
        public int PaneIndex
        {
            get { return this._paneIndex; }
            set { this._paneIndex = value; }
        }

        /// <summary>
        /// Gets the parent docking element.
        /// </summary>
        public IDockingElement ParentElement
        {
            get { return this._parentPane; }
        }

        /// <summary>
        /// Gets the <see cref="DockingPane"/> that is a parent to this item.
        /// </summary>
        public DockingPane ParentPane
        {
            get { return this._parentPane; }
        }

        /// <summary>
        /// Gets the undo engine associated with this document.
        /// </summary>
        public UndoEngine UndoEngine
        {
            get { return this._undoEngine; }
        }

        /// <summary>
        /// Gets the root view site for this document item.
        /// </summary>
        public ViewSite ViewSite
        {
            get
            {
                IDockingElement parent = this._parentPane;
                ViewSite viewSite = parent as ViewSite;
                while (parent != null && viewSite == null)
                {
                    parent = parent.ParentElement;
                    viewSite = parent as ViewSite;
                }

                return viewSite;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Called whenever this item is about to be closed and gives it the opportunity to
        /// cancel.
        /// </summary>
        /// <param name="e">
        /// The System.ComponentModel.CancelEventArgs giving this method the chance to cancel
        /// the close operation.
        /// </param>
        public virtual void OnClosing(CancelEventArgs e)
        {
        }

        /// <summary>
        /// Raises the saved event on this object.
        /// </summary>
        public void RaiseSavedEvent()
        {
            EventHandler handler = this.Saved;
            if (handler == null)
            {
                return;
            }

            handler(this, EventArgs.Empty);
        }

        /// <summary>
        /// Saves the data that is associated with this item to the specified file.
        /// </summary>
        /// <param name="stream">
        /// The io stream that data for this document should be written to.
        /// </param>
        /// <returns>
        /// A value indicating whether the save operation was successful.
        /// </returns>
        public bool Save(Stream stream)
        {
            if (!this.CanSave)
            {
                return false;
            }

            bool result = this.SaveCore(stream);
            return result;
        }

        /// <summary>
        /// Override to provide core save logic to the data that this document represents.
        /// </summary>
        /// <param name="stream">
        /// The io stream that data for this document should be written to.
        /// </param>
        /// <returns>
        /// A value indicating whether the save operation was successful.
        /// </returns>
        protected abstract bool SaveCore(Stream stream);

        /// <summary>
        /// Called whenever the undo engine signals that its dirty state has been changed.
        /// </summary>
        /// <param name="sender">
        /// The undo engine attached to this handler.
        /// </param>
        /// <param name="e">
        /// The System.EventArgs containing the event data.
        /// </param>
        private void UndoEngineDirtyStateChanged(object sender, EventArgs e)
        {
            this.IsModified = this._undoEngine.DirtyState;
        }
        #endregion Methods
    } // RSG.Editor.Controls.Dock.ViewModel.DocumentItem {Class}
} // RSG.Editor.Controls.Dock.ViewModel {Namespace}
