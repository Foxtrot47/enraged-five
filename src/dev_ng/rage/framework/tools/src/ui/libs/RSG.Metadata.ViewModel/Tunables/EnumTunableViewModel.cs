﻿//---------------------------------------------------------------------------------------------
// <copyright file="EnumTunableViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Tunables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows.Data;
    using RSG.Editor;
    using RSG.Metadata.Model.Definitions;
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// The view model for a <see cref="EnumTunable"/> model object.
    /// </summary>
    public class EnumTunableViewModel : TunableViewModelBase<EnumTunable>
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Items"/> property.
        /// </summary>
        private List<object> _items;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="EnumTunableViewModel"/> class.
        /// </summary>
        /// <param name="tunable">
        /// The enum tunable that this view model is representing.
        /// </param>
        /// <param name="parent">
        /// The parent tunable view model for this view model.
        /// </param>
        public EnumTunableViewModel(EnumTunable tunable, ITunableViewModel parent)
            : base(tunable, parent)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the enum items that represent the individual values in the enumeration.
        /// </summary>
        public List<object> Items
        {
            get
            {
                if (this._items == null)
                {
                    this._items = new List<object>();
                    IEnumeration source = this.Model.EnumMember.Source;
                    if (source != null)
                    {
                        bool foundSelection = false;
                        foreach (IEnumConstant constant in source.EnumConstants)
                        {
                            bool selected = false;
                            if (!foundSelection)
                            {
                                if (String.Equals(constant.Name, this.Model.Value))
                                {
                                    foundSelection = true;
                                    selected = true;
                                }
                            }

                            this._items.Add(
                                new EnumItemViewModel(constant.Name, selected, this));
                        }
                    }

                    DispatcherHelper.CheckBeginInvokeOnUI(new Action(() =>
                    {
                        ListSortDirection ascending = ListSortDirection.Ascending;
                        ICollectionView view =
                            CollectionViewSource.GetDefaultView(this._items);

                        EnumerationSortMethod sortMethod = EnumerationSortMethod.Default;
                        if (source != null)
                        {
                            sortMethod = source.SortMethod;
                        }

                        switch (sortMethod)
                        {
                            case EnumerationSortMethod.Alphabetical:
                                view.SortDescriptions.Add(
                                    new SortDescription("Name", ascending));
                                break;
                            case EnumerationSortMethod.Value:
                                view.SortDescriptions.Add(
                                    new SortDescription("Value", ascending));
                                break;
                            case EnumerationSortMethod.None:
                            default:
                                view.SortDescriptions.Add(
                                    new SortDescription("Index", ascending));
                                break;
                        }

                        view.SortDescriptions.Add(
                            new SortDescription("Name", ascending));
                    }));
                }

                return this._items;
            }
        }

        /// <summary>
        /// Gets or sets the value currently assigned to the enum tunable.
        /// </summary>
        public string Value
        {
            get { return this.Model.Value; }
            set { this.Model.Value = value; }
        }
        #endregion Properties

        #region Classes
        /// <summary>
        /// Represents a single enum value inside a enum control.
        /// </summary>
        private class EnumItemViewModel : NotifyPropertyChangedBase
        {
            #region Fields
            /// <summary>
            /// The private field used for the <see cref="IsSelected"/> property.
            /// </summary>
            private bool _isSelected;

            /// <summary>
            /// The private field used for the <see cref="Name"/> property.
            /// </summary>
            private string _name;

            /// <summary>
            /// The private reference to the bitset view model that created this instance.
            /// </summary>
            private EnumTunableViewModel _parent;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="EnumItemViewModel"/> class.
            /// </summary>
            /// <param name="name">
            /// The text to display as content for this item.
            /// </param>
            /// <param name="isSelected">
            /// A value indicating the initial is checked state of the item.
            /// </param>
            /// <param name="parent">
            /// A reference to the enum view model that created this instance.
            /// </param>
            public EnumItemViewModel(string name, bool isSelected, EnumTunableViewModel parent)
            {
                this._name = name;
                this._isSelected = isSelected;
                this._parent = parent;
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets the index of this item in its zero-based non-sorted location.
            /// </summary>
            public int Index
            {
                get
                {
                    IEnumeration source = this._parent.Model.EnumMember.Source;
                    if (source != null)
                    {
                        return source.GetIndexOf(this._name);
                    }

                    return -1;
                }
            }

            /// <summary>
            /// Gets or sets a value indicating whether this enum value is currently the
            /// selected item or not.
            /// </summary>
            public bool IsSelected
            {
                get { return this._isSelected; }
                set { this.SetProperty(ref this._isSelected, value); }
            }

            /// <summary>
            /// Gets the text that is displayed for this enum item.
            /// </summary>
            public string Name
            {
                get { return this._name; }
            }

            /// <summary>
            /// Gets the value of the item based on the associated enum constant or index.
            /// </summary>
            public long Value
            {
                get
                {
                    IEnumeration source = this._parent.Model.EnumMember.Source;
                    if (source != null)
                    {
                        IEnumConstant constant = source[this._name];
                        if (constant != null)
                        {
                            return constant.Value;
                        }
                    }

                    return -1;
                }
            }
            #endregion Properties
        } // EnumTunableViewModel.EnumItemViewModel {Class}
        #endregion Classes
    } // RSG.Metadata.ViewModel.Tunables.EnumTunableViewModel {Class}
} // RSG.Metadata.ViewModel.Tunables {Namespace}
