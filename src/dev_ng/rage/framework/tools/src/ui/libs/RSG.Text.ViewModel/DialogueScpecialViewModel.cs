﻿namespace RSG.Text.ViewModel
{
    using System;
    using RSG.Editor.View;
    using RSG.Text.Model;

    public class DialogueSpecialViewModel : ViewModelBase<DialogueSpecial>
    {
        private int _usedCount;

        public DialogueSpecialViewModel(DialogueSpecial special)
            : base(special, true)
        {
        }

        public bool HasHeadsetFlag
        {
            get { return this.Model.HasHeadsetFlag; }
            set { this.Model.HasHeadsetFlag = value; }
        }

        public bool HasPadSpeakerFlag
        {
            get { return this.Model.HasPadSpeakerFlag; }
            set { this.Model.HasPadSpeakerFlag = value; }
        }

        public Guid Id
        {
            get { return this.Model.Id; }
        }

        public bool IsUsed
        {
            get { return this.UsedCount > 0; }
        }

        public string Name
        {
            get { return this.Model.Name; }
            set { this.Model.Name = value; }
        }

        public int UsedCount
        {
            get { return this._usedCount; }
            set { this.SetProperty(ref this._usedCount, value, "UsedCount", "IsUsed"); }
        }

    }
}
