﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsThemeImage.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Represents a control that displays a image which is automatically themed.
    /// </summary>
    public class RsThemeImage : Control
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="IconBitmap"/> dependency property.
        /// </summary>
        public static DependencyProperty IconBitmapProperty;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsThemeImage"/> class.
        /// </summary>
        static RsThemeImage()
        {
            IconBitmapProperty =
                DependencyProperty.Register(
                "IconBitmap",
                typeof(BitmapSource),
                typeof(RsThemeImage),
                new FrameworkPropertyMetadata(null));

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsThemeImage),
                new FrameworkPropertyMetadata(typeof(RsThemeImage)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsThemeImage"/> class.
        /// </summary>
        public RsThemeImage()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the bitmap source object that is shown as the image.
        /// </summary>
        public BitmapSource IconBitmap
        {
            get { return (BitmapSource)this.GetValue(IconBitmapProperty); }
            set { this.SetValue(IconBitmapProperty, value); }
        }
        #endregion Properties
    } // RSG.Editor.Controls.RsThemeImage {Class}
} // RSG.Editor.Controls {Namespace}
