﻿//---------------------------------------------------------------------------------------------
// <copyright file="IReadOnlyViewModelCollection{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    using System.Collections.Generic;
    using System.Collections.Specialized;

    /// <summary>
    /// When implemented represents a collection of objects that can be access via a index or
    /// iterated over and also fire an event whenever the items change.
    /// </summary>
    /// <typeparam name="T">
    /// The type of elements in the collection.
    /// </typeparam>
    public interface IReadOnlyViewModelCollection<T>
        : IReadOnlyList<T>, INotifyCollectionChanged
    {
    } // RSG.Editor.View.IReadOnlyViewModelCollection{T} {Interface}
} // RSG.Editor.View {Namespace}
