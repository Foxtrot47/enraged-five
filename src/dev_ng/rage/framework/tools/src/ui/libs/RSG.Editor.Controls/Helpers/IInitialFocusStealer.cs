﻿//---------------------------------------------------------------------------------------------
// <copyright file="IInitialFocusStealer.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Helpers
{
    using System.Windows;

    /// <summary>
    /// When implemented represents a control that when first initialised or activated can
    /// steal the focus for a specific object.
    /// </summary>
    public interface IInitialFocusStealer
    {
        #region Methods
        /// <summary>
        /// Called when this control should steal the keyboard focus.
        /// </summary>
        void StealFocus();
        #endregion Methods
    } // RSG.Editor.Controls.Helpers.IInitialFocusStealer {Interface}
} // RSG.Editor.Controls.Helpers {Namespace}
