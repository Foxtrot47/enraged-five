﻿//---------------------------------------------------------------------------------------------
// <copyright file="SuspendableObservableCollection{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2012. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    using System;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.ComponentModel;

    /// <summary>
    /// Represents a observable collection of the specified type that can have its change
    /// events suspended for a set period of time.
    /// </summary>
    /// <typeparam name="T">
    /// The type of elements in the collection.
    /// </typeparam>
    public class SuspendableObservableCollection<T> : ObservableCollection<T>
    {
        #region Fields
        /// <summary>
        /// The number of changes that have occurred and have been supressed.
        /// </summary>
        private int _changesCount;

        /// <summary>
        /// A value indicating whether a notification has been skipped while the collection
        /// events have been suspended.
        /// </summary>
        private bool _skippedNotification;
        #endregion

        #region Properties
        /// <summary>
        /// Gets a value indicating whether any notifications are currently suspended.
        /// </summary>
        private bool AreNotificationsSuspended
        {
            get { return this._changesCount > 0; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Retrieves a new instance of the change scope that can be used to manage the
        /// suppression of the collection changes.
        /// </summary>
        /// <returns>
        /// A new instance of the change scope that can be used to manage the suppression of
        /// the collection changes.
        /// </returns>
        public IDisposable SuspendChangeNotification()
        {
            return new SuspendableObservableCollection<T>.ChangesScope(this);
        }

        /// <summary>
        /// Raises the CollectionChanged event with the provided arguments.
        /// </summary>
        /// <param name="e">
        /// Arguments of the event being raised.
        /// </param>
        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (this.AreNotificationsSuspended)
            {
                this._skippedNotification = true;
                return;
            }

            base.OnCollectionChanged(e);
        }

        /// <summary>
        /// Raises the PropertyChanged event with the provided arguments.
        /// </summary>
        /// <param name="e">
        /// Arguments of the event being raised.
        /// </param>
        protected override void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (this.AreNotificationsSuspended)
            {
                this._skippedNotification = true;
                return;
            }

            base.OnPropertyChanged(e);
        }
        #endregion

        #region Classes
        /// <summary>
        /// A class that manages the scope of suppression.
        /// </summary>
        private class ChangesScope : DisposableObject
        {
            #region Fields
            /// <summary>
            /// The private reference to the collection being suspended.
            /// </summary>
            private SuspendableObservableCollection<T> _collection;
            #endregion

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="ChangesScope"/> class.
            /// </summary>
            /// <param name="collection">
            /// The collection whose events have been suspended.
            /// </param>
            public ChangesScope(SuspendableObservableCollection<T> collection)
            {
                this._collection = collection;
                if (this._collection._changesCount == 0)
                {
                    this._collection._skippedNotification = false;
                }

                this._collection._changesCount++;
            }
            #endregion

            #region Methods
            /// <summary>
            /// When overridden disposes of the managed resources.
            /// </summary>
            protected override void DisposeManagedResources()
            {
                this._collection._changesCount--;
                if (this._collection._changesCount != 0)
                {
                    return;
                }

                if (!this._collection._skippedNotification)
                {
                    return;
                }

                NotifyCollectionChangedAction action = NotifyCollectionChangedAction.Reset;
                this._collection.OnCollectionChanged(
                    new NotifyCollectionChangedEventArgs(action));
            }
            #endregion
        } // RSG.Editor.View.SuspendableObservableCollection{T}.ChangesScope {Class}
        #endregion
    } // RSG.Editor.View.SuspendableObservableCollection{T} {Class}
} // RSG.Editor.View {Namespace}
