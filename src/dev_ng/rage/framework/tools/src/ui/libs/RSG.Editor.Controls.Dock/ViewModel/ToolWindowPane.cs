﻿//---------------------------------------------------------------------------------------------
// <copyright file="ToolWindowPane.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock.ViewModel
{
    /// <summary>
    /// Represents a group of tool window items that are grouped together inside a single pane.
    /// </summary>
    public class ToolWindowPane : DockingPane
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ToolWindowPane"/> class.
        /// </summary>
        /// <param name="viewSite">
        /// The view site that this pane is a element of.
        /// </param>
        public ToolWindowPane(ViewSite viewSite)
            : base(viewSite)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Inserts the specified item into this tool window pane.
        /// </summary>
        /// <param name="index">
        /// The zero-based index where the specified item should be inserted.
        /// </param>
        /// <param name="item">
        /// The item that should be inserted.
        /// </param>
        public void InsertChild(int index, ToolWindowItem item)
        {
            base.InsertChild(index, item);
        }
        #endregion Methods
    } // RSG.Editor.Controls.Dock.ViewModel.ToolWindowPane {Class}
} // RSG.Editor.Controls.Dock.ViewModel {Namespace}
