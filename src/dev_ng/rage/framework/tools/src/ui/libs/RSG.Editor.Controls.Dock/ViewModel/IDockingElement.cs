﻿//---------------------------------------------------------------------------------------------
// <copyright file="IDockingElement.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock.ViewModel
{
    using System.ComponentModel;

    /// <summary>
    /// When implemented represents a docking element that is apart of the docking root chain
    /// with a root <see cref="ViewSite"/>.
    /// </summary>
    public interface IDockingElement : INotifyPropertyChanged
    {
        #region Properties
        /// <summary>
        /// Gets the parent docking element.
        /// </summary>
        IDockingElement ParentElement { get; }
        #endregion Properties
    } // RSG.Editor.Controls.Dock.ViewModel.IDockingElement {Interface}
} // RSG.Editor.Controls.Dock.ViewModel {Namespace}
