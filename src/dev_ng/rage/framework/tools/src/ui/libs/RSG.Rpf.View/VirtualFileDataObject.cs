﻿// --------------------------------------------------------------------------------------------
// <copyright file="VirtualFileDataObject.cs" company="Rockstar">
//     Copyright © Rockstar Games 2015. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Rpf.View
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Runtime.InteropServices.ComTypes;
    using System.Windows;
    using RSG.Interop.Microsoft.Windows;

    /// <summary>
    /// Class implementing drag/drop support for virtual files that are being extracted.
    /// </summary>
    public sealed class VirtualFileDataObject
        : System.Runtime.InteropServices.ComTypes.IDataObject
    {
        #region Fields
        /// <summary>
        /// In-order list of registered data objects.
        /// </summary>
        private List<DataObject> _dataObjects = new List<DataObject>();
        #endregion Fields

        #region Methods
        /// <summary>
        /// Gets or sets the CFSTR_PERFORMEDDROPEFFECT value for the object.
        /// </summary>
        public DragDropEffects? PerformedDropEffect
        {
            get
            {
                short id = (short)DataFormats.GetDataFormat("Performed DropEffect").Id;
                return this.GetDropEffect(id);
            }

            set
            {
                short id = (short)DataFormats.GetDataFormat("Performed DropEffect").Id;
                this.SetData(id, BitConverter.GetBytes((uint)value));
            }
        }

        /// <summary>
        /// Gets or sets the CFSTR_PREFERREDDROPEFFECT value for the object.
        /// </summary>
        public DragDropEffects? PreferredDropEffect
        {
            get
            {
                short id = (short)DataFormats.GetDataFormat("Preferred DropEffect").Id;
                return this.GetDropEffect(id);
            }

            set
            {
                short id = (short)DataFormats.GetDataFormat("Preferred DropEffect").Id;
                this.SetData(id, BitConverter.GetBytes((uint)value));
            }
        }

        /// <summary>
        /// Initiates a drag-and-drop operation.
        /// </summary>
        /// <param name="source">
        /// A reference to the dependency object that is the source of the data being dragged.
        /// </param>
        /// <param name="dataObject">
        /// A data object that contains the data being dragged.
        /// </param>
        /// <param name="allowedEffects">
        /// One of the DragDropEffects values that specifies permitted effects of the
        /// drag-and-drop operation.
        /// </param>
        /// <returns>
        /// One of the DragDropEffects values that specifies the final effect that was
        /// performed during the drag-and-drop operation.
        /// </returns>
        public static DragDropEffects DoDragDrop(
            DependencyObject source,
            System.Runtime.InteropServices.ComTypes.IDataObject dataObject,
            DragDropEffects allowedEffects)
        {
            int[] finalEffect = new int[1];
            try
            {
                var dropSource = new DropSource();
                Ole32.DoDragDrop(dataObject, dropSource, (int)allowedEffects, finalEffect);
            }
            catch
            {
            }

            return (DragDropEffects)finalEffect[0];
        }

        /// <summary>
        /// Creates a connection between a data object and an advisory sink. This method is
        /// called by an object that supports an advisory sink and enables the advisory sink
        /// to be notified of changes in the object's data.
        /// </summary>
        public int DAdvise(
            ref FORMATETC format, ADVF advf, IAdviseSink adviseSink, out int connection)
        {
            Marshal.ThrowExceptionForHR((int)HRESULT.E_ADVISENOTSUPPORTED);
            throw new NotImplementedException();
        }

        /// <summary>
        /// Destroys a notification connection that had been previously established.
        /// </summary>
        /// <param name="connection">
        /// A token that specifies the connection to remove.
        /// </param>
        public void DUnadvise(int connection)
        {
            Marshal.ThrowExceptionForHR((int)HRESULT.E_ADVISENOTSUPPORTED);
            throw new NotImplementedException();
        }

        /// <summary>
        /// Creates an object that can be used to enumerate the current advisory connections.
        /// </summary>
        public int EnumDAdvise(out IEnumSTATDATA enumAdvise)
        {
            Marshal.ThrowExceptionForHR((int)HRESULT.E_ADVISENOTSUPPORTED);
            throw new NotImplementedException();
        }

        /// <summary>
        /// Creates an object for enumerating the FORMATETC structures for a data object.
        /// </summary>
        public IEnumFORMATETC EnumFormatEtc(DATADIR direction)
        {
            if (direction == DATADIR.DATADIR_GET)
            {
                if (this._dataObjects.Count == 0)
                {
                    throw new InvalidOperationException(
                      "VirtualFileDataObject requires at least one data object to enumerate.");
                }

                IEnumFORMATETC enumerator;
                int result = Shell32.SHCreateStdEnumFmtEtc(
                    (uint)this._dataObjects.Count,
                    this._dataObjects.Select(d => d.Format).ToArray(),
                    out enumerator);

                if (Win32Errors.Succeeded(result))
                {
                    return enumerator;
                }

                Marshal.ThrowExceptionForHR((int)HRESULT.E_FAIL);
            }

            throw new NotImplementedException();
        }

        /// <summary>
        /// Provides a standard FORMATETC structure that is logically equivalent to a more
        /// complex structure.
        /// </summary>
        public int GetCanonicalFormatEtc(ref FORMATETC formatIn, out FORMATETC formatOut)
        {
            Marshal.ThrowExceptionForHR((int)HRESULT.E_ADVISENOTSUPPORTED);
            throw new NotImplementedException();
        }

        /// <summary>
        /// Obtains data from a source data object. The GetData method, which is called by a
        /// data consumer, renders the data described in the specified FORMATETC structure and
        /// transfers it through the specified STGMEDIUM structure.
        /// </summary>
        public void GetData(ref FORMATETC format, out STGMEDIUM medium)
        {
            medium = new STGMEDIUM();
            int hr = ((System.Runtime.InteropServices.ComTypes.IDataObject)this).QueryGetData(
                ref format);

            if (Win32Errors.Succeeded(hr))
            {
                FORMATETC formatCopy = format;
                var dataObject = this._dataObjects
                    .Where(d =>
                        (d.Format.cfFormat == formatCopy.cfFormat) &&
                        (d.Format.dwAspect == formatCopy.dwAspect) &&
                        (0 != (d.Format.tymed & formatCopy.tymed) &&
                        (d.Format.lindex == formatCopy.lindex))).FirstOrDefault();

                if (dataObject != null)
                {
                    medium.tymed = dataObject.Format.tymed;
                    var result = dataObject.GetData();
                    hr = result.Item2;
                    if (Win32Errors.Succeeded(hr))
                    {
                        medium.unionmember = result.Item1;
                    }
                }
                else
                {
                    // Couldn't find a match
                    hr = (int)HRESULT.E_FORMATETC;
                }
            }

            if (!Win32Errors.Succeeded(hr))
            {
                Marshal.ThrowExceptionForHR(hr);
            }
        }

        /// <summary>
        /// Obtains data from a source data object. This method, which is called by a data
        /// consumer, differs from the GetData method in that the caller must allocate and free
        /// the specified storage medium.
        /// </summary>
        public void GetDataHere(ref FORMATETC format, ref STGMEDIUM medium)
        {
            Marshal.ThrowExceptionForHR((int)HRESULT.E_ADVISENOTSUPPORTED);
            throw new NotImplementedException();
        }

        /// <summary>
        /// Determines whether the data object is capable of rendering the data described in
        /// the FORMATETC structure. Objects attempting a paste or drop operation can call this
        /// method before calling GetData to get an indication of whether the operation may be
        /// successful.
        /// </summary>
        public int QueryGetData(ref FORMATETC format)
        {
            FORMATETC formatCopy = format;
            var formatMatches =
                this._dataObjects.Where(d => d.Format.cfFormat == formatCopy.cfFormat);
            if (!formatMatches.Any())
            {
                return (int)HRESULT.E_FORMATETC;
            }

            var tymedMatches =
                formatMatches.Where(d => 0 != (d.Format.tymed & formatCopy.tymed));
            if (!tymedMatches.Any())
            {
                return (int)HRESULT.E_TYMED;
            }

            var aspectMatches =
                tymedMatches.Where(d => d.Format.dwAspect == formatCopy.dwAspect);
            if (!aspectMatches.Any())
            {
                return (int)HRESULT.E_DVASPECT;
            }

            return (int)HRESULT.S_OK;
        }

        /// <summary>
        /// Transfers data to the object that implements this method. This method is called by
        /// an object that contains a data source.
        /// </summary>
        public void SetData(ref FORMATETC format, ref STGMEDIUM medium, bool release)
        {
            bool handled = false;
            bool supportedFormat = medium.tymed == format.tymed;
            supportedFormat |= format.tymed == TYMED.TYMED_HGLOBAL;
            supportedFormat |= format.dwAspect == DVASPECT.DVASPECT_CONTENT;
            if (supportedFormat)
            {
                IntPtr ptr = Kernel32.GlobalLock(medium.unionmember);
                if (IntPtr.Zero != ptr)
                {
                    try
                    {
                        int length = Kernel32.GlobalSize(ptr).ToInt32();
                        byte[] data = new byte[length];
                        Marshal.Copy(ptr, data, 0, length);
                        this.SetData(format.cfFormat, data);
                        handled = true;
                    }
                    finally
                    {
                        Kernel32.GlobalUnlock(medium.unionmember);
                    }
                }

                if (release)
                {
                    Marshal.FreeHGlobal(medium.unionmember);
                }
            }

            if (!handled)
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Provides data for the specified data format (FILEGROUPDESCRIPTOR/FILEDESCRIPTOR).
        /// </summary>
        /// <param name="fileDescriptors">
        /// Collection of virtual files.
        /// </param>
        public void SetData(IEnumerable<FileDescriptor> fileDescriptors)
        {
            var bytes = new List<byte>();
            FILEGROUPDESCRIPTOR group = new FILEGROUPDESCRIPTOR
            {
                Count = fileDescriptors.Count()
            };

            bytes.AddRange(StructureBytes(group));
            foreach (var fileDescriptor in fileDescriptors)
            {
                var winFileDescriptor = new FILEDESCRIPTOR
                {
                    FileName = fileDescriptor.Name,
                };

                if (fileDescriptor.Length.HasValue)
                {
                    winFileDescriptor.Flags |= FileDescriptorFlags.FILESIZE;
                    winFileDescriptor.FileSizeLow = (uint)(fileDescriptor.Length & 0xffffffff);
                    winFileDescriptor.FileSizeHigh = (uint)(fileDescriptor.Length >> 32);
                }

                bytes.AddRange(StructureBytes(winFileDescriptor));
            }

            short format =
                (short)System.Windows.DataFormats.GetDataFormat("FileGroupDescriptorW").Id;
            this.SetData(format, bytes);

            var index = 0;
            foreach (var fileDescriptor in fileDescriptors)
            {
                format = (short)System.Windows.DataFormats.GetDataFormat("FileContents").Id;
                this.SetData(format, index, fileDescriptor.StreamContents);
                index++;
            }
        }

        /// <summary>
        /// Gets the bytes of the specified object.
        /// </summary>
        private static IEnumerable<byte> StructureBytes(object source)
        {
            // Set up for call to StructureToPtr
            var size = Marshal.SizeOf(source.GetType());
            var ptr = Marshal.AllocHGlobal(size);
            var bytes = new byte[size];
            try
            {
                Marshal.StructureToPtr(source, ptr, false);
                Marshal.Copy(ptr, bytes, 0, size);
            }
            finally
            {
                Marshal.FreeHGlobal(ptr);
            }

            return bytes;
        }

        /// <summary>
        /// Gets the DragDropEffects value (if any) previously set on the object.
        /// </summary>
        /// <param name="format">
        /// Clipboard format.
        /// </param>
        /// <returns>
        /// DragDropEffects value or null.
        /// </returns>
        private DragDropEffects? GetDropEffect(short format)
        {
            var dataObject = this._dataObjects
                .Where(d =>
                    (format == d.Format.cfFormat) &&
                    (DVASPECT.DVASPECT_CONTENT == d.Format.dwAspect) &&
                    (TYMED.TYMED_HGLOBAL == d.Format.tymed))
                .LastOrDefault();

            if (null != dataObject)
            {
                // Read the value and return it
                var result = dataObject.GetData();
                if (Win32Errors.Succeeded(result.Item2))
                {
                    var ptr = Kernel32.GlobalLock(result.Item1);
                    if (IntPtr.Zero != ptr)
                    {
                        try
                        {
                            var length = Kernel32.GlobalSize(ptr).ToInt32();
                            if (4 == length)
                            {
                                var data = new byte[length];
                                Marshal.Copy(ptr, data, 0, length);
                                return (DragDropEffects)BitConverter.ToUInt32(data, 0);
                            }
                        }
                        finally
                        {
                            Kernel32.GlobalUnlock(result.Item1);
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Provides data for the specified data format (HGLOBAL).
        /// </summary>
        /// <param name="dataFormat">
        /// Data format.
        /// </param>
        /// <param name="data">
        /// Sequence of data.
        /// </param>
        private void SetData(short dataFormat, IEnumerable<byte> data)
        {
            this._dataObjects.Add(
                new DataObject
                {
                    Format = new FORMATETC
                    {
                        cfFormat = dataFormat,
                        ptd = IntPtr.Zero,
                        dwAspect = DVASPECT.DVASPECT_CONTENT,
                        lindex = -1,
                        tymed = TYMED.TYMED_HGLOBAL
                    },

                    GetData = () =>
                    {
                        var dataArray = data.ToArray();
                        var ptr = Marshal.AllocHGlobal(dataArray.Length);
                        Marshal.Copy(dataArray, 0, ptr, dataArray.Length);
                        return new Tuple<IntPtr, int>(ptr, (int)HRESULT.S_OK);
                    },
                });
        }

        /// <summary>
        /// Provides data for the specified data format and index.
        /// </summary>
        /// <param name="dataFormat">
        /// Data format.
        /// </param>
        /// <param name="index">
        /// Index of data.
        /// </param>
        /// <param name="streamData">
        /// Action generating the data.
        /// </param>
        private void SetData(short dataFormat, int index, Action<Stream> streamData)
        {
            this._dataObjects.Add(
                new DataObject
                {
                    Format = new FORMATETC
                    {
                        cfFormat = dataFormat,
                        ptd = IntPtr.Zero,
                        dwAspect = DVASPECT.DVASPECT_CONTENT,
                        lindex = index,
                        tymed = TYMED.TYMED_ISTREAM
                    },

                    GetData = () =>
                    {
                        IStream stream = Ole32.CreateStream();
                        if (streamData != null)
                        {
                            using (var streamwrapper = new WrapperStream(stream))
                            {
                                streamData(streamwrapper);
                            }
                        }

                        IntPtr ptr = Marshal.GetComInterfaceForObject(stream, typeof(IStream));
                        Marshal.ReleaseComObject(stream);
                        return new Tuple<IntPtr, int>(ptr, (int)HRESULT.S_OK);
                    },
                });
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// Class representing the result of a SetData call.
        /// </summary>
        private class DataObject
        {
            /// <summary>
            /// Gets or sets the FORMATETC structure for the data.
            /// </summary>
            public FORMATETC Format { get; set; }

            /// <summary>
            /// Gets or sets the function returning the data as an IntPtr and an HRESULT
            /// success code.
            /// </summary>
            public Func<Tuple<IntPtr, int>> GetData { get; set; }
        }

        /// <summary>
        /// Contains the methods for generating visual feedback to the end user and for
        /// cancelling or completing the drag-and-drop operation.
        /// </summary>
        private class DropSource : IDropSource
        {
            #region Methods
            /// <summary>
            /// Enables a source application to give visual feedback to the end user during a
            /// drag-and-drop operation.
            /// </summary>
            /// <param name="effect">
            /// The effect to use.
            /// </param>
            /// <returns>
            /// Zero if successful; otherwise, non-zero.
            /// </returns>
            public int GiveFeedback(uint effect)
            {
                return (int)DragDropState.USEDEFAULTCURSORS;
            }

            /// <summary>
            /// Determines whether a drag-and-drop operation should be continued, cancelled, or
            /// completed.
            /// </summary>
            /// <param name="escapePressed">
            /// Indicates whether the Esc key has been pressed since the previous call.
            /// </param>
            /// <param name="keyState">
            /// The current state of the keyboard modifier keys on the keyboard.
            /// </param>
            /// <returns>
            /// A value indicating whether to continue, drop, or cancel.
            /// </returns>
            public int QueryContinueDrag(int escapePressed, uint keyState)
            {
                DragDropKeyStates keyStates = (DragDropKeyStates)keyState;
                DragDropKeyStates leftMouseButton = DragDropKeyStates.LeftMouseButton;
                if (escapePressed != 0)
                {
                    return (int)DragDropState.CANCEL;
                }
                else if (DragDropKeyStates.None == (keyStates & leftMouseButton))
                {
                    return (int)DragDropState.DROP;
                }

                return (int)HRESULT.S_OK;
            }
            #endregion Methods
        } // VirtualFileDataObject.DropSource {Class}

        /// <summary>
        /// Simple class that exposes a write-only IStream as a Stream.
        /// </summary>
        private class WrapperStream : Stream
        {
            #region Fields
            /// <summary>
            /// IStream instance being wrapped.
            /// </summary>
            private IStream _stream;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="WrapperStream"/> class.
            /// </summary>
            /// <param name="stream">
            /// IStream instance to wrap.
            /// </param>
            public WrapperStream(IStream stream)
            {
                this._stream = stream;
            }
            #endregion Constructors

            #region Methods
            /// <summary>
            /// Gets a value indicating whether the current stream supports reading.
            /// </summary>
            public override bool CanRead
            {
                get { return false; }
            }

            /// <summary>
            /// Gets a value indicating whether the current stream supports seeking.
            /// </summary>
            public override bool CanSeek
            {
                get { return false; }
            }

            /// <summary>
            /// Gets a value indicating whether the current stream supports writing.
            /// </summary>
            public override bool CanWrite
            {
                get { return true; }
            }

            /// <summary>
            /// Gets the length in bytes of the stream.
            /// </summary>
            public override long Length
            {
                get { throw new NotImplementedException(); }
            }

            /// <summary>
            /// Gets or sets the position within the current stream.
            /// </summary>
            public override long Position
            {
                get { throw new NotImplementedException(); }
                set { throw new NotImplementedException(); }
            }

            /// <summary>
            /// Clears all buffers for this stream and causes any buffered data to be written
            /// to the underlying device.
            /// </summary>
            public override void Flush()
            {
                throw new NotImplementedException();
            }

            /// <summary>
            /// Reads a sequence of bytes from the current stream and advances the position
            /// within the stream by the number of bytes read.
            /// </summary>
            /// <param name="buffer">
            /// An array of bytes. When this method returns, the buffer contains the specified
            /// byte array with the values between offset and (offset + count - 1) replaced by
            /// the bytes read from the current source.
            /// </param>
            /// <param name="offset">
            /// The zero-based byte offset in buffer at which to begin storing the data read
            /// from the current stream.
            /// </param>
            /// <param name="count">
            /// The maximum number of bytes to be read from the current stream.
            /// </param>
            /// <returns>
            /// The total number of bytes read into the buffer. This can be less than the
            /// number of bytes requested if that many bytes are not currently available, or
            /// zero (0) if the end of the stream has been reached.
            /// </returns>
            public override int Read(byte[] buffer, int offset, int count)
            {
                throw new NotImplementedException();
            }

            /// <summary>
            /// Sets the position within the current stream.
            /// </summary>
            /// <param name="offset">
            /// A byte offset relative to the origin parameter.
            /// </param>
            /// <param name="origin">
            /// A value of type SeekOrigin indicating the reference point used to obtain the
            /// new position.
            /// </param>
            /// <returns>
            /// The new position within the current stream.
            /// </returns>
            public override long Seek(long offset, SeekOrigin origin)
            {
                throw new NotImplementedException();
            }

            /// <summary>
            /// Sets the length of the current stream.
            /// </summary>
            /// <param name="value">
            /// The desired length of the current stream in bytes.
            /// </param>
            public override void SetLength(long value)
            {
                throw new NotImplementedException();
            }

            /// <summary>
            /// Writes a sequence of bytes to the current stream and advances the current
            /// position within this stream by the number of bytes written.
            /// </summary>
            /// <param name="buffer">
            /// An array of bytes. This method copies count bytes from buffer to the current
            /// stream.
            /// </param>
            /// <param name="offset">
            /// The zero-based byte offset in buffer at which to begin copying bytes to the
            /// current stream.
            /// </param>
            /// <param name="count">
            /// The number of bytes to be written to the current stream.
            /// </param>
            public override void Write(byte[] buffer, int offset, int count)
            {
                if (offset == 0)
                {
                    this._stream.Write(buffer, count, IntPtr.Zero);
                }
                else
                {
                    this._stream.Write(buffer.Skip(offset).ToArray(), count, IntPtr.Zero);
                }
            }
            #endregion Methods
        } // VirtualFileDataObject.IStreamWrapper {Class}
        #endregion Classes
    } // RSG.Rpf.View.VirtualFileDataObject {Class}
} // RSG.Rpf.View {Namespace}
