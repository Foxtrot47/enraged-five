﻿//---------------------------------------------------------------------------------------------
// <copyright file="IntegerToVisibilityConverter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Converters
{
    using System.Globalization;
    using System.Windows;

    /// <summary>
    /// Converts a integer value into a System.Windows.Visibility enum value.
    /// </summary>
    public class IntegerToVisibilityConverter : ValueConverter<int, Visibility>
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="VisibilityCount"/> property.
        /// </summary>
        private int _visibilityCount;

        /// <summary>
        /// The private field used for the <see cref="FailedValue"/> property.
        /// </summary>
        private Visibility _failedValue;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="IntegerToVisibilityConverter"/>
        /// class.
        /// </summary>
        /// <param name="visibilityCount">
        /// The count the bound to value needs to be at to return visible.
        /// </param>
        /// <param name="collapse">
        /// A value indicating whether the converted value should be collapsed if the bound to
        /// value isn't greater than or equal to the specified visibility count.
        /// </param>
        public IntegerToVisibilityConverter(int visibilityCount, bool collapse)
        {
            this._visibilityCount = visibilityCount;
            if (collapse)
            {
                this.FailedValue = Visibility.Collapsed;
            }
            else
            {
                this.FailedValue = Visibility.Hidden;
            }
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the count the value needs to be at to return visible.
        /// </summary>
        public int VisibilityCount
        {
            get { return this._visibilityCount; }
            set { this._visibilityCount = value; }
        }

        /// <summary>
        /// Gets or sets the visibility value that is returned during a convert if the bound
        /// to value isn't greater than or equal to the set count.
        /// </summary>
        public Visibility FailedValue
        {
            get { return this._failedValue; }
            set { this._failedValue = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Converters the given value and returns the result.
        /// </summary>
        /// <param name="value">
        /// The original value to convert.
        /// </param>
        /// <param name="param">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A converted value.
        /// </returns>
        protected override Visibility Convert(int value, object param, CultureInfo culture)
        {
            if (value >= this.VisibilityCount)
            {
                return Visibility.Visible;
            }

            return this.FailedValue;
        }
        #endregion Methods
    } // IntegerToVisibilityConverter.IntegerToVisibilityConverter {Class}
} // RSG.Editor.Controls.Converters {Namespace}
