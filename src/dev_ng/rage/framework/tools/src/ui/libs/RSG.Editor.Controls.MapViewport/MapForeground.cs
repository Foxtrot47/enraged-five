﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace RSG.Editor.Controls.MapViewport
{
    /// <summary>
    /// Control for containing the foreground elements such as the
    /// <see cref="MapZoomControl"/> and <see cref="MapPanControl"/> for the
    /// <see cref="MapViewport"/>.
    /// </summary>
    public class MapForeground : Control
    {
        #region Constructors
        /// <summary>
        /// Initialises the static fields of the <see cref="MapViewport"/> control.
        /// </summary>
        static MapForeground()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(MapForeground),
                new FrameworkPropertyMetadata(typeof(MapForeground)));
        }
        #endregion
    }
}
