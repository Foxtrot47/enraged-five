﻿//---------------------------------------------------------------------------------------------
// <copyright file="PtrDiffTunableViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Tunables
{
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// The view model for a <see cref="PtrDiffTunable"/> model object.
    /// </summary>
    public class PtrDiffTunableViewModel : TunableViewModelBase<PtrDiffTunable>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="PtrDiffTunableViewModel"/> class.
        /// </summary>
        /// <param name="tunable">
        /// The ptr diff tunable that this view model is representing.
        /// </param>
        /// <param name="parent">
        /// The parent tunable view model for this view model.
        /// </param>
        public PtrDiffTunableViewModel(PtrDiffTunable tunable, ITunableViewModel parent)
            : base(tunable, parent)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the value currently assigned to the ptr diff tunable.
        /// </summary>
        public long Value
        {
            get { return this.Model.Value; }
            set { this.Model.Value = value; }
        }
        #endregion Properties
    } // RSG.Metadata.ViewModel.Tunables.PtrDiffTunableViewModel {Class}
} // RSG.Metadata.ViewModel.Tunables {Namespace}
