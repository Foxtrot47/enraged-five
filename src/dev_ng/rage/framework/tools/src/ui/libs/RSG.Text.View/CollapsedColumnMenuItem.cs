﻿//---------------------------------------------------------------------------------------------
// <copyright file="CollapsedColumnMenuItem.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.View
{
    using System;
    using System.ComponentModel;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using RSG.Editor.Controls;
    using RSG.Editor.Controls.Commands;
    using RSG.Editor.Controls.Converters;
    using RSG.Editor.Controls.Helpers;

    /// <summary>
    /// Represents a menu item that can be placed onto a data grid rows context menu on the
    /// event of an associated column becoming invisible.
    /// </summary>
    public class CollapsedColumnMenuItem : RsMenuItem
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="Column"/> dependency property.
        /// </summary>
        public static DependencyProperty ColumnProperty;

        /// <summary>
        /// Identifies the <see cref="VisibleOnlyOnCollapsed"/> dependency property.
        /// </summary>
        public static DependencyProperty VisibleOnlyOnCollapsedProperty;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="CollapsedColumnMenuItem"/> class.
        /// </summary>
        static CollapsedColumnMenuItem()
        {
            ColumnProperty =
                DependencyProperty.Register(
                "Column",
                typeof(DataGridColumn),
                typeof(CollapsedColumnMenuItem),
                new FrameworkPropertyMetadata(null, OnColumnChanged));

            VisibleOnlyOnCollapsedProperty =
                DependencyProperty.Register(
                "VisibleOnlyOnCollapsed",
                typeof(bool),
                typeof(CollapsedColumnMenuItem),
                new FrameworkPropertyMetadata(true, OnVisibleOnlyOnCollapsedChanged));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="CollapsedColumnMenuItem"/> class.
        /// </summary>
        public CollapsedColumnMenuItem()
        {
            this.Loaded += this.OnLoaded;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the column that this menu item uses as a source for the header and
        /// determines its visibility off.
        /// </summary>
        public DataGridColumn Column
        {
            get { return (DataGridColumn)this.GetValue(ColumnProperty); }
            set { this.SetValue(ColumnProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this menu item should only be visible if
        /// the associated column is collapsed.
        /// </summary>
        public bool VisibleOnlyOnCollapsed
        {
            get { return (bool)this.GetValue(VisibleOnlyOnCollapsedProperty); }
            set { this.SetValue(VisibleOnlyOnCollapsedProperty, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Called whenever the <see cref="Column"/> dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The object whose <see cref="Column"/> dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnColumnChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            CollapsedColumnMenuItem item = s as CollapsedColumnMenuItem;
            if (item == null)
            {
                return;
            }

            DataGridColumn column = e.NewValue as DataGridColumn;
            if (column != null)
            {
                Binding headerBinding = new Binding("Header");
                headerBinding.Source = column;
                item.SetBinding(MenuItem.HeaderProperty, headerBinding);

                if (item.VisibleOnlyOnCollapsed)
                {
                    Binding visibilityBinding = new Binding("Visibility");
                    visibilityBinding.Source = column;
                    visibilityBinding.Converter = new ColumnVisibilityConverter();
                    item.SetBinding(MenuItem.VisibilityProperty, visibilityBinding);
                }

                if (item.IsLoaded)
                {
                    item.SetupStyleAndBindingsBasedOnColumn();
                }
            }
            else
            {
                BindingOperations.ClearBinding(item, MenuItem.HeaderProperty);
                BindingOperations.ClearBinding(item, MenuItem.VisibilityProperty);
                item.IsCheckable = false;
                BindingOperations.ClearBinding(item, MenuItem.IsCheckedProperty);
            }
        }

        /// <summary>
        /// Called whenever the <see cref="VisibleOnlyOnCollapsed"/> dependency property
        /// changes.
        /// </summary>
        /// <param name="s">
        /// The object whose <see cref="VisibleOnlyOnCollapsed"/> dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnVisibleOnlyOnCollapsedChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            CollapsedColumnMenuItem item = s as CollapsedColumnMenuItem;
            if (item == null)
            {
                return;
            }

            bool flag = (bool)e.NewValue;
            if (flag && item.Column != null)
            {
                Binding visibilityBinding = new Binding("Visibility");
                visibilityBinding.Source = item.Column;
                visibilityBinding.Converter = new ColumnVisibilityConverter();
                item.SetBinding(MenuItem.VisibilityProperty, visibilityBinding);
            }
            else
            {
                BindingOperations.ClearBinding(item, MenuItem.VisibilityProperty);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            this.SetupStyleAndBindingsBasedOnColumn();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= this.OnLoaded;
            this.SetupStyleAndBindingsBasedOnColumn();
        }

        /// <summary>
        /// 
        /// </summary>
        private void SetupStyleAndBindingsBasedOnColumn()
        {
            if (this.Column is RsDataGridCheckBoxColumn)
            {
                this.SetupItem((RsDataGridCheckBoxColumn)this.Column);
            }
            else if (this.Column is RsDataGridComboBoxColumn)
            {
                this.SetupItem((RsDataGridComboBoxColumn)this.Column);
            }
            else if (this.Column is RsDataGridIntegerSpinnerColumn)
            {
                this.SetupItem((RsDataGridIntegerSpinnerColumn)this.Column);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="column">
        /// 
        /// </param>
        private void SetupItem(RsDataGridCheckBoxColumn column)
        {
            ResourceKey resource = RsContextMenu.GridCheckStyleKey;
            this.SetResourceReference(FrameworkElement.StyleProperty, resource);
            if (column.Binding != null)
            {
                this.SetBinding(MenuItem.IsCheckedProperty, column.Binding);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="column">
        /// 
        /// </param>
        private void SetupItem(RsDataGridIntegerSpinnerColumn column)
        {
            ResourceKey resource = RsContextMenu.GridSpinnerStyleKey;
            this.SetResourceReference(FrameworkElement.StyleProperty, resource);
            RsIntegerSpinner spinner = this.GetVisualDescendent<RsIntegerSpinner>();
            if (spinner == null)
            {
                return;
            }

            if (column.Binding != null)
            {
                spinner.SetBinding(RsIntegerSpinner.ValueProperty, column.Binding);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="column">
        /// 
        /// </param>
        private void SetupItem(RsDataGridComboBoxColumn column)
        {
            ResourceKey resource = RsContextMenu.GridComboStyleKey;
            this.SetResourceReference(FrameworkElement.StyleProperty, resource);
            ComboBox cb = this.GetVisualDescendent<ComboBox>();
            if (cb == null)
            {
                return;
            }

            if (column.ItemsSourceBinding != null)
            {
                cb.SetBinding(ComboBox.ItemsSourceProperty, column.ItemsSourceBinding);
            }

            if (column.SelectedValueBinding != null)
            {
                cb.SetBinding(ComboBox.SelectedValueProperty, column.SelectedValueBinding);
            }
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// Converters the visibility of a data grid column object to a visibility value for a
        /// menu item that is associated with it.
        /// </summary>
        private class ColumnVisibilityConverter : ValueConverter<Visibility, Visibility>
        {
            #region Methods
            /// <summary>
            /// Converters the given value and returns the result.
            /// </summary>
            /// <param name="visibility">
            /// The original value to convert.
            /// </param>
            /// <param name="param">
            /// The converter parameter to use.
            /// </param>
            /// <param name="culture">
            /// The culture to use in the converter.
            /// </param>
            /// <returns>
            /// A converted value.
            /// </returns>
            protected override Visibility Convert(
                Visibility visibility, object param, CultureInfo culture)
            {
                if (visibility != Visibility.Collapsed)
                {
                    return Visibility.Collapsed;
                }
                else
                {
                    return Visibility.Visible;
                }
            }
            #endregion Methods
        } // CollapsedColumnMenuItem.ColumnVisibilityConverter {Class}
        #endregion Classes
    } // RSG.Text.View.CollapsedColumnMenuItem {Class}
} // RSG.Text.View {Namespace}
