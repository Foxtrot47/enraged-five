﻿//---------------------------------------------------------------------------------------------
// <copyright file="BrushToColourConverter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Converters
{
    using System.Globalization;
    using System.Windows.Media;

    /// <summary>
    /// Converters any brush value to a colour value based on the class type of brush
    /// specified. This class cannot be inherited.
    /// </summary>
    public sealed class BrushToColourConverter : ValueConverter<Brush, Color>
    {
        #region Methods
        /// <summary>
        /// Converters the given value and returns the result.
        /// </summary>
        /// <param name="value">
        /// The original value to convert.
        /// </param>
        /// <param name="param">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A converted value.
        /// </returns>
        protected override Color Convert(Brush value, object param, CultureInfo culture)
        {
            if (value == null)
            {
                return Colors.Transparent;
            }

            SolidColorBrush solidBrush = value as SolidColorBrush;
            if (solidBrush != null)
            {
                return solidBrush.Color;
            }

            GradientBrush gradientBrush = value as GradientBrush;
            if (gradientBrush != null && gradientBrush.GradientStops != null)
            {
                if (gradientBrush.GradientStops.Count > 0)
                {
                    return gradientBrush.GradientStops[0].Color;
                }
                else
                {
                    return Colors.Transparent;
                }
            }

            return Colors.Transparent;
        }
        #endregion Methods
    } // RSG.Editor.Controls.Converters.BrushToColourConverter {Class}
} // RSG.Editor.Controls.Converters {Namespace}
