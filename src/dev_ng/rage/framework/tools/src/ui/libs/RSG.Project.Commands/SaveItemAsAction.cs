﻿//---------------------------------------------------------------------------------------------
// <copyright file="SaveItemAsAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Commands
{
    using System.Collections.Generic;
    using System.Diagnostics;
    using RSG.Editor;
    using RSG.Editor.SharedCommands;
    using RSG.Project.ViewModel;

    /// <summary>
    /// Contains the logic for the <see cref="RockstarCommands.SaveAs"/> routed command. This
    /// class cannot be inherited.
    /// </summary>
    public sealed class SaveItemAsAction : ButtonAction<ProjectCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SaveItemAsAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public SaveItemAsAction(ProjectCommandResolver resolver)
            : base(new ParameterResolverDelegate<ProjectCommandArgs>(resolver))
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ProjectCommandArgs args)
        {
            string fallbackText = "Save Selected Items As...";
            if (args == null || args.SelectedNodes == null)
            {
                RockstarCommandManager.UpdateItemText(RockstarCommands.Save, fallbackText);
                return false;
            }

            List<ISaveable> items = new List<ISaveable>();
            foreach (IHierarchyNode node in args.SelectedNodes)
            {
                ISaveable savable = node as ISaveable;
                if (savable == null || !node.CanBeSaved)
                {
                    continue;
                }

                items.Add(savable);
            }

            if (items.Count == 0)
            {
                RockstarCommandManager.UpdateItemText(RockstarCommands.SaveAs, fallbackText);
                return false;
            }
            else if (items.Count > 1)
            {
                RockstarCommandManager.UpdateItemText(RockstarCommands.SaveAs, fallbackText);
                return true;
            }
            else
            {
                ISaveable item = items[0];
                string updatedText = "Save " + item.FriendlySavePath + " As...";
                RockstarCommandManager.UpdateItemText(RockstarCommands.SaveAs, updatedText);
                return true;
            }
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(ProjectCommandArgs args)
        {
            IDocumentManagerService docService = this.GetService<IDocumentManagerService>();
            if (docService == null)
            {
                Debug.Fail("Unable to save item due to the fact the service is missing.");
                return;
            }

            docService.SaveItems(args.SelectedNodes, true);
        }
        #endregion Methods
    } // RSG.Project.Commands.SaveItemAsAction {Class}
} // RSG.Project.Commands {Namespace}
