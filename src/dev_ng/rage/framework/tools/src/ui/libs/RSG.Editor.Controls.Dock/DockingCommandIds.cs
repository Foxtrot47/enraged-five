﻿//---------------------------------------------------------------------------------------------
// <copyright file="DockingCommandIds.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock
{
    using System;

    /// <summary>
    /// Contains global identifiers that are used throughout the docking system to add command
    /// instances to the controls.
    /// </summary>
    public static class DockingCommandIds
    {
        #region Fields
        /// <summary>
        /// The private instance used for the <see cref="DocumentContextMenu"/> property.
        /// </summary>
        private static Guid? _documentContextMenu;

        /// <summary>
        /// A private instance to a generic object used to support thread access.
        /// </summary>
        private static object _syncRoot = new object();
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets the global identifier used for the context menu for the individual items in
        /// the RPF viewer list.
        /// </summary>
        public static Guid DocumentContextMenu
        {
            get
            {
                if (!_documentContextMenu.HasValue)
                {
                    lock (_syncRoot)
                    {
                        if (!_documentContextMenu.HasValue)
                        {
                            _documentContextMenu =
                                new Guid("1D14EFEE-4B77-4EFE-8373-87B228810902");
                        }
                    }
                }

                return _documentContextMenu.Value;
            }
        }
        #endregion Properties
    } // RSG.Editor.Controls.Dock.DockingCommandIds {Class}
} // RSG.Editor.Controls.Dock {Namespace}
