﻿//---------------------------------------------------------------------------------------------
// <copyright file="ISaveable.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.IO;

    /// <summary>
    /// When implemented represents a object that's data can be written to a generic stream.
    /// </summary>
    public interface ISaveable
    {
        #region Events
        /// <summary>
        /// Occurs just after this object has been saved successfully.
        /// </summary>
        event EventHandler Saved;
        #endregion Events

        #region Properties
        /// <summary>
        /// Gets the friendly save path that this item pushes into the command system
        /// to display on menu items.
        /// </summary>
        string FriendlySavePath { get; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Raises the saved event on this object.
        /// </summary>
        void RaiseSavedEvent();

        /// <summary>
        /// Saves the data that is associated with this item to the specified file.
        /// </summary>
        /// <param name="stream">
        /// The io stream that data for this object should be written to.
        /// </param>
        /// <returns>
        /// A value indicating whether the save operation was successful.
        /// </returns>
        bool Save(Stream stream);
        #endregion Methods
    } // RSG.Editor.ISaveable {Interface}
} // RSG.Editor {Namespace}
