﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsDataGridCheckBoxColumn.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;

    /// <summary>
    /// Represents a data grid column that hosts a check box in its data grid cells.
    /// </summary>
    public class RsDataGridCheckBoxColumn : DataGridCheckBoxColumn
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="EditableStyleKey"/> property.
        /// </summary>
        private static ResourceKey _editableStyleKey = CreateStyleKey("EditableStyleKey");

        /// <summary>
        /// The private field used for the <see cref="ReadOnlyStyleKey"/> property.
        /// </summary>
        private static ResourceKey _readOnlyStyleKey = CreateStyleKey("ReadOnlyStyleKey");
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsDataGridCheckBoxColumn"/> class.
        /// </summary>
        static RsDataGridCheckBoxColumn()
        {
            DataGridBoundColumn.IsReadOnlyProperty.OverrideMetadata(
                typeof(RsDataGridCheckBoxColumn),
                new FrameworkPropertyMetadata(OnReadOnlyPropertyChanged));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsDataGridCheckBoxColumn"/> class.
        /// </summary>
        public RsDataGridCheckBoxColumn()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the resource key used to reference the style used on editable check boxes.
        /// </summary>
        public static ResourceKey EditableStyleKey
        {
            get { return _editableStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on read only check boxes.
        /// </summary>
        public static ResourceKey ReadOnlyStyleKey
        {
            get { return _readOnlyStyleKey; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates the visual tree that will become the content of the specified cell.
        /// </summary>
        /// <param name="cell">
        /// The cell the visual tree is being created for.
        /// </param>
        /// <param name="dataItem">
        /// The data item currently bound to the specified cell.
        /// </param>
        /// <returns>
        /// The root of the visual tree that will become the content of the specified cell.
        /// </returns>
        protected override FrameworkElement GenerateEditingElement(
            DataGridCell cell, object dataItem)
        {
            return this.GenerateCheckBox(cell);
        }

        /// <summary>
        /// Creates the visual tree that will become the content of the specified cell.
        /// </summary>
        /// <param name="cell">
        /// The cell the visual tree is being created for.
        /// </param>
        /// <param name="dataItem">
        /// The data item currently bound to the specified cell.
        /// </param>
        /// <returns>
        /// The root of the visual tree that will become the content of the specified cell.
        /// </returns>
        protected override FrameworkElement GenerateElement(DataGridCell cell, object dataItem)
        {
            return this.GenerateCheckBox(cell);
        }

        /// <summary>
        /// Creates a new resource key that references a style with the specified name.
        /// </summary>
        /// <param name="styleName">
        /// The name that the resource key will be referencing.
        /// </param>
        /// <returns>
        /// A new resource key that references a style with the specified name.
        /// </returns>
        private static ResourceKey CreateStyleKey(string styleName)
        {
            return new StyleKey<RsDataGridCheckBoxColumn>(styleName);
        }

        /// <summary>
        /// Called whenever the IsReadOnly dependency property changes.
        /// </summary>
        /// <param name="d">
        /// The object whose IsReadOnly dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnReadOnlyPropertyChanged(
            DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RsDataGridCheckBoxColumn column = d as RsDataGridCheckBoxColumn;
            if (column == null)
            {
                Debug.Assert(false, "Incorrect dependency object cast");
                return;
            }

            // This will make sure the visual tree for all of the cells are rebuilt.
            column.NotifyPropertyChanged("ElementStyle");
            column.NotifyPropertyChanged("EditingElementStyle");
        }

        /// <summary>
        /// Creates the check box control that will become the content of the specified cell.
        /// </summary>
        /// <param name="cell">
        /// The data grid cell whose check box is being created.
        /// </param>
        /// <returns>
        /// The check box control that should be the root of the specified cells visual tree.
        /// </returns>
        private CheckBox GenerateCheckBox(DataGridCell cell)
        {
            CheckBox checkBox = (cell != null) ? (cell.Content as CheckBox) : null;
            if (checkBox == null)
            {
                checkBox = new CheckBox();
                checkBox.HorizontalAlignment = HorizontalAlignment.Center;
                checkBox.VerticalAlignment = VerticalAlignment.Top;
                checkBox.ClickMode = ClickMode.Press;
            }

            checkBox.IsThreeState = this.IsThreeState;
            BindingBase binding = this.Binding;
            if (binding != null)
            {
                DependencyProperty property = CheckBox.IsCheckedProperty;
                BindingOperations.SetBinding(checkBox, property, binding);
            }
            else
            {
                DependencyProperty property = CheckBox.IsCheckedProperty;
                BindingOperations.ClearBinding(checkBox, property);
            }

            DependencyProperty styleProperty = FrameworkElement.StyleProperty;
            if (this.IsReadOnly == false)
            {
                checkBox.SetResourceReference(styleProperty, EditableStyleKey);
                checkBox.IsHitTestVisible = true;
                checkBox.Focusable = true;
                if (cell != null)
                {
                    cell.IsTabStop = false;
                }
            }
            else
            {
                checkBox.SetResourceReference(styleProperty, ReadOnlyStyleKey);
                checkBox.IsHitTestVisible = false;
                checkBox.Focusable = false;
                if (cell != null)
                {
                    cell.IsTabStop = true;
                }
            }

            return checkBox;
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsDataGridCheckBoxColumn {Class}
} // RSG.Editor.Controls {Namespace}
