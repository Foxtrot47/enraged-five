﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsComboBox.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Input;

    /// <summary>
    /// Represents a selection control with a drop-down list that can be shown or hidden.
    /// </summary>
    public class RsComboBox : ComboBox
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="Command" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty CommandProperty;

        /// <summary>
        /// Identifies the <see cref="CommandTarget" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty CommandTargetProperty;

        /// <summary>
        /// Identifies the <see cref="ItemsPadding" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty ItemsPaddingProperty;

        /// <summary>
        /// Identifies the <see cref="DisplayedItem" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty DisplayedItemProperty;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsComboBox" /> class.
        /// </summary>
        static RsComboBox()
        {
            CommandProperty =
                ButtonBase.CommandProperty.AddOwner(
                typeof(RsComboBox), new FrameworkPropertyMetadata(OnCommandChanged));

            CommandTargetProperty =
                ButtonBase.CommandTargetProperty.AddOwner(
                typeof(RsComboBox), new FrameworkPropertyMetadata(null));

            FrameworkPropertyMetadataOptions options =
                FrameworkPropertyMetadataOptions.AffectsMeasure |
                FrameworkPropertyMetadataOptions.AffectsArrange |
                FrameworkPropertyMetadataOptions.AffectsRender;

            DisplayedItemProperty =
                DependencyProperty.Register(
                "DisplayedItem",
                typeof(object),
                typeof(RsComboBox),
                new FrameworkPropertyMetadata(null, options));

            ItemsPaddingProperty =
                DependencyProperty.Register(
                "ItemsPadding",
                typeof(Thickness),
                typeof(RsComboBox),
                new FrameworkPropertyMetadata(new Thickness(0.0), options));

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsComboBox),
                new FrameworkPropertyMetadata(typeof(RsComboBox)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsComboBox" /> class.
        /// </summary>
        public RsComboBox()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the command associated with the menu item.
        /// </summary>
        public ICommand Command
        {
            get { return (ICommand)this.GetValue(CommandProperty); }
            set { this.SetValue(CommandProperty, value); }
        }

        /// <summary>
        /// Gets or sets the target element on which to raise the specified command.
        /// </summary>
        public IInputElement CommandTarget
        {
            get { return (IInputElement)this.GetValue(CommandTargetProperty); }
            set { this.SetValue(CommandTargetProperty, value); }
        }

        /// <summary>
        /// Gets or sets the padding around the items collection inside the combo box popup.
        /// </summary>
        public Thickness ItemsPadding
        {
            get { return (Thickness)this.GetValue(ItemsPaddingProperty); }
            set { this.SetValue(ItemsPaddingProperty, value); }
        }

        /// <summary>
        /// Gets or sets the item that is currently displayed to the user in the main combo box
        /// content area.
        /// </summary>
        public object DisplayedItem
        {
            get { return this.GetValue(DisplayedItemProperty); }
            set { this.SetValue(DisplayedItemProperty, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Handles the selection of one of the combo box items.
        /// </summary>
        /// <param name="selectedFromList">
        /// A value indicating whether the selection came from one of the items in the list
        /// directly.
        /// </param>
        /// <param name="item">
        /// The item that has been selected.
        /// </param>
        /// <param name="index">
        /// The index of the item that has been selected.
        /// </param>
        internal void HandleSelection(bool selectedFromList, RsComboBoxItem item, int index)
        {
            if (index != -1 && this.SelectedIndex == index)
            {
                this.DisplayedItem = this.SelectedItem;
                if (selectedFromList)
                {
                    this.IsDropDownOpen = false;
                }

                return;
            }

            object commandParameter = this.GetCommandParameter(item, index);
            if (index != -1)
            {
                this.SelectedIndex = index;
            }

            IInputElement inputElement = this.CommandTarget;
            RoutedCommand routedCommand = this.Command as RoutedCommand;
            if (routedCommand != null)
            {
                if (inputElement == null)
                {
                    inputElement = this as IInputElement;
                }

                if (routedCommand.CanExecute(commandParameter, inputElement))
                {
                    routedCommand.Execute(commandParameter, inputElement);
                }
            }
            else if (this.Command != null)
            {
                if (this.Command.CanExecute(commandParameter))
                {
                    this.Command.Execute(commandParameter);
                }
            }

            if (selectedFromList)
            {
                this.IsDropDownOpen = false;
            }
            else
            {
                this.SelectedItem = this.DisplayedItem;
            }
        }

        /// <summary>
        /// Creates or identifies the element used to display the child items.
        /// </summary>
        /// <returns>
        /// A new <see cref="RSG.Editor.Controls.RsComboBoxItem" />.
        /// </returns>
        protected override DependencyObject GetContainerForItemOverride()
        {
            RsComboBoxItem item = new RsComboBoxItem();

            item.OnCommandChanged(
                new DependencyPropertyChangedEventArgs(
                    RsComboBox.CommandProperty, null, this.Command));

            return item;
        }

        /// <summary>
        /// Determines whether the specified item is, or is eligible to be, its own item
        /// container.
        /// </summary>
        /// <param name="item">
        /// The item to check whether it is an item container.
        /// </param>
        /// <returns>
        /// True if the item is eligible to be its own item container; otherwise, false.
        /// </returns>
        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            if (item is RsComboBoxItem)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Invoked when a System.Windows.Input.Keyboard.KeyDown attached routed event occurs.
        /// </summary>
        /// <param name="e">
        /// The event data including the key that was pressed.
        /// </param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e == null)
            {
                throw new SmartArgumentNullException(() => e);
            }

            if (e.Key == Key.Left || e.Key == Key.Right)
            {
                e.Handled = false;
                return;
            }

            if (!this.IsDropDownOpen)
            {
                if (e.Key == Key.Down)
                {
                    int displayedIndex = this.Items.IndexOf(this.DisplayedItem);
                    for (int i = displayedIndex + 1; i < this.Items.Count; i++)
                    {
                        object parameter = this.GetCommandParameter(null, i);
                        if (this.Command != null)
                        {
                            if (this.Command.CanExecute(parameter))
                            {
                                this.DisplayedItem = this.Items[i];
                                break;
                            }
                        }
                        else
                        {
                            this.DisplayedItem = this.Items[i];
                            break;
                        }
                    }

                    e.Handled = true;
                    return;
                }
                else if (e.Key == Key.Up)
                {
                    int displayedIndex = this.Items.IndexOf(this.DisplayedItem);
                    for (int i = displayedIndex - 1; i >= 0; i--)
                    {
                        object parameter = this.GetCommandParameter(null, i);
                        if (this.Command != null)
                        {
                            if (this.Command.CanExecute(parameter))
                            {
                                this.DisplayedItem = this.Items[i];
                                break;
                            }
                        }
                        else
                        {
                            this.DisplayedItem = this.Items[i];
                            break;
                        }
                    }

                    e.Handled = true;
                    return;
                }
                else if (e.Key == Key.Return)
                {
                    int index = this.Items.IndexOf(this.DisplayedItem);
                    this.HandleSelection(false, index);
                }
            }

            base.OnKeyDown(e);
        }

        /// <summary>
        /// Gets called whenever the control losses focus so that the displayed item can be
        /// re-synced to the selected item.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.KeyboardFocusChangedEventArgs data used for the event.
        /// </param>
        protected override void OnLostKeyboardFocus(KeyboardFocusChangedEventArgs e)
        {
            this.DisplayedItem = this.SelectedItem;
        }

        /// <summary>
        /// Gets called whenever the controls selection changes so that the displayed item can
        /// be synced up to the selected item.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Controls.SelectionChangedEventArgs data used for the event.
        /// </param>
        protected override void OnSelectionChanged(SelectionChangedEventArgs e)
        {
            base.OnSelectionChanged(e);
            this.DisplayedItem = this.SelectedItem;
        }

        /// <summary>
        /// Called whenever the <see cref="Command"/> dependency property changes.
        /// </summary>
        /// <param name="d">
        /// The object whose <see cref="Command"/> dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnCommandChanged(
            DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RsComboBox comboBox = d as RsComboBox;
            if (comboBox == null)
            {
                return;
            }

            for (int i = 0; i < comboBox.Items.Count; i++)
            {
                DependencyObject obj = comboBox.ItemContainerGenerator.ContainerFromIndex(i);
                RsComboBoxItem item = obj as RsComboBoxItem;
                if (item == null)
                {
                    continue;
                }

                item.OnCommandChanged(e);
            }
        }

        /// <summary>
        /// Gets the command parameter for the specified item with the specified index.
        /// </summary>
        /// <param name="item">
        /// The item to get the command parameter for. Can be null.
        /// </param>
        /// <param name="index">
        /// The index of the item to get the command parameter for. Can be -1.
        /// </param>
        /// <returns>
        /// The command parameter that is associated with the specified item if not null or the
        /// item at the specified index.
        /// </returns>
        private object GetCommandParameter(RsComboBoxItem item, int index)
        {
            object commandParameter = null;
            if (index == -1)
            {
                commandParameter = this.Text;
            }
            else
            {
                if (item == null)
                {
                    object boundObj = this.Items[index];
                    IMultiCommandItem commandItem = boundObj as IMultiCommandItem;
                    if (commandItem != null)
                    {
                        commandParameter = commandItem.GetCommandParameter();
                    }
                }
                else
                {
                    commandParameter = item.CommandParameter;
                }
            }

            return commandParameter;
        }

        /// <summary>
        /// Handles the selection of one of the combo box items.
        /// </summary>
        /// <param name="selectedFromList">
        /// A value indicating whether the selection came from one of the items in the list
        /// directly.
        /// </param>
        /// <param name="index">
        /// The index of the item that has been selected.
        /// </param>
        private void HandleSelection(bool selectedFromList, int index)
        {
            DependencyObject obj = null;
            if (index != -1)
            {
                obj = this.ItemContainerGenerator.ContainerFromIndex(index);
            }

            this.HandleSelection(selectedFromList, obj as RsComboBoxItem, index);
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsComboBox {Class}
} // RSG.Editor.Controls {Namespace}
