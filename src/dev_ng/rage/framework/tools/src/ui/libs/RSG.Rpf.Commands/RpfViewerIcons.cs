﻿//---------------------------------------------------------------------------------------------
// <copyright file="RpfViewerIcons.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Rpf.Commands
{
    using System;
    using System.Globalization;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Provides static properties that give access to common icons that can be used throughout
    /// a application.
    /// </summary>
    public static class RpfViewerIcons
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Extract"/> property.
        /// </summary>
        private static BitmapSource _extract;

        /// <summary>
        /// The private field used for the <see cref="ExtractAll"/> property.
        /// </summary>
        private static BitmapSource _extractAll;

        /// <summary>
        /// A generic object that provides thread safety access to the image sources.
        /// </summary>
        private static object _syncRoot = new object();
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets the icon that shows a extract icon.
        /// </summary>
        public static BitmapSource Extract
        {
            get
            {
                if (_extract == null)
                {
                    lock (_syncRoot)
                    {
                        if (_extract == null)
                        {
                            EnsureLoaded(ref _extract, "extract16");
                        }
                    }
                }

                return _extract;
            }
        }

        /// <summary>
        /// Gets the icon that shows a extract all icon.
        /// </summary>
        public static BitmapSource ExtractAll
        {
            get
            {
                if (_extractAll == null)
                {
                    lock (_syncRoot)
                    {
                        if (_extractAll == null)
                        {
                            EnsureLoaded(ref _extractAll, "extractAll16");
                        }
                    }
                }

                return _extractAll;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Ensures that the specified image source is loaded with the specified resource name.
        /// </summary>
        /// <param name="source">
        /// The image source that should be loaded.
        /// </param>
        /// <param name="resourceName">
        /// The name of the resource the loaded image source should set as its source.
        /// </param>
        private static void EnsureLoaded(ref BitmapSource source, string resourceName)
        {
            if (source != null)
            {
                return;
            }

            string assemblyName = typeof(RpfViewerIcons).Assembly.GetName().Name;
            string path = "Resources/" + resourceName + ".png";
            string bitmapPath = "pack://application:,,,/{0};component/{1}";
            bitmapPath = String.Format(
                CultureInfo.InvariantCulture, bitmapPath, assemblyName, path);
            Uri uri = new Uri(bitmapPath, UriKind.RelativeOrAbsolute);

            source = new BitmapImage(uri);
        }
        #endregion Methods
    } // RSG.Rpf.Commands.RpfViewerIcons {Class}
} // RSG.Rpf.Commands {Namespace}
