﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Xml.Linq;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Editor;

namespace RSG.AnimationToolkit.ViewModel
{
    public class CheckedListItem : NotifyPropertyChangedBase
    {
        public CheckedListItem(string _name, bool _checked, string _template)
        {
            Name = _name;
            IsChecked = _checked;
            CompressionTemplate = _template;
        }
        public string Name { get; set; }
        public string CompressionTemplate 
        {
            get
            {
                return _CompressionTemplate;
            }
                
            set
            {
                _CompressionTemplate = value;
                NotifyPropertyChanged("CompressionTemplate");
            } 
        }
        public bool IsChecked 
        { 
            get
            {
                return _IsChecked;
            }
                
            set
            {
                _IsChecked = value;
                NotifyPropertyChanged("IsChecked");
            }
        }

        private bool _IsChecked;
        private string _CompressionTemplate;
    }

    /// <summary>
    /// </summary>
    public class AnimationToolkitDataContext : NotifyPropertyChangedBase
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MainWindowDataContext"/> class.
        /// </summary>
        public AnimationToolkitDataContext()
        {
            _viewModel = new AnimationToolkitViewModel();
        }
        #endregion // Constructors

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public AnimationToolkitViewModel GetViewModel
        {
            get { return _viewModel; }
        }

        private AnimationToolkitViewModel _viewModel;

        #endregion

        #region Methods

        /// <summary>
        /// 
        /// </summary>
        public ICommand PreviewIngameZipCommand
        {
            get
            {
                return new RelayCommand(GetViewModel.PreviewIngameZip);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ICommand CreateIngameZipCommand
        {
            get
            {
                return new RelayCommand(GetViewModel.CreateIngameZip);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ICommand CreateIngameRPFCommand
        {
            get
            {
                return new RelayCommand(GetViewModel.CreateIngameRPF);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ICommand CreateIngameClipRPFCommand
        {
            get
            {
                return new RelayCommand(GetViewModel.CreateIngameClipRPF);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ICommand CreateIngameClipZipCommand
        {
            get
            {
                return new RelayCommand(GetViewModel.CreateIngameClipZip);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ICommand PreviewIngameClipCommand
        {
            get
            {
                return new RelayCommand(GetViewModel.PreviewIngameClipZip);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ICommand ApplyIngameClipChangesCommand
        {
            get
            {
                return new RelayCommand(GetViewModel.ApplyIngameClipChange);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ICommand SaveCSVCommand
        {
            get
            {
                return new RelayCommand(GetViewModel.SaveCSV);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ICommand PreviewCutsceneZipCommand
        {
            get
            {
                return new RelayCommand(GetViewModel.PreviewCutsceneZip);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ICommand CreateCutsceneZipCommand
        {
            get
            {
                return new RelayCommand(GetViewModel.CreateCutsceneZip);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ICommand CreateBuildCutsceneZipCommand
        {
            get
            {
                return new RelayCommand(GetViewModel.CreateBuildCutsceneZip);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ICommand CreateCutsceneRPFCommand
        {
            get
            {
                return new RelayCommand(GetViewModel.CreateCutsceneRPF);
            }
        }

        #endregion // Methods
    }
}
