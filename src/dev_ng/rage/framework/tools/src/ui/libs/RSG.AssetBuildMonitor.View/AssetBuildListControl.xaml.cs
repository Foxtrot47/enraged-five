﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssetBuildListControl.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.AssetBuildMonitor.View
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Navigation;
    using System.Windows.Shapes;
    using RSG.Editor.Controls;
    
    /// <summary>
    /// Interaction logic for AssetBuildListControl.xaml
    /// </summary>
    public partial class AssetBuildListControl : RsUserControl
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public AssetBuildListControl()
        {
            InitializeComponent();
        }
        #endregion // Constructor(s)
    }

} // RSG.AssetBuildMonitor.View namespace
