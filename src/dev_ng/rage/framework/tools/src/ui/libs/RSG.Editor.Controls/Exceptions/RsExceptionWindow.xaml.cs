//---------------------------------------------------------------------------------------------
// <copyright file="RsExceptionWindow.xaml.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Exceptions
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;
    using System.Windows.Input;
    using RSG.Base.Logging;
    using RSG.Base.Extensions;
    using RSG.Editor.Controls.Resources;
    using RSG.Interop.Microsoft.Office;
    using RSG.Interop.Microsoft.Office.Outlook;
    using System.Windows;
    using RSG.Base.OS;
    using RSG.Interop.Bugstar.Application;
    using RSG.Interop.Bugstar;
    using System.IO;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Interaction logic for the exception window control.
    /// </summary>
    public partial class RsExceptionWindow : RsWindow
    {
        #region Fields
        /// <summary>
        /// Help url string for providing information about the exception dialog.
        /// </summary>
        private const string HelpUrl =
            "https://devstar.rockstargames.com/wiki/index.php/Dev:Exception_Dialog";

        /// <summary>
        /// Reference to the object that will provide us with information about the
        /// application that triggered the exception.
        /// </summary>
        private readonly IApplicationInfoProvider _applicationInfoProvider;

        /// <summary>
        /// Reference to the object that will provide us with information for logging a
        /// bug in bugstar for the application that triggered the exception.
        /// </summary>
        private readonly IBugstarInfoProvider _bugstarInfoProvider;

        /// <summary>
        /// The private field used for the <see cref="Exception"/> property.
        /// </summary>
        private readonly Exception _exception;

        /// <summary>
        /// The private field used for the <see cref="ExceptionDetails"/> property.
        /// </summary>
        private readonly string _exceptionDetails;

        /// <summary>
        /// The private field for the exception formatter.
        /// </summary>
        private readonly ExceptionFormatter _formatter;

        /// <summary>
        /// Value defining the buttons that are visible on the window.
        /// </summary>
        private readonly ExceptionWindowButtons _visibleButtons;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="RsExceptionWindow"/> class to display
        /// the specified exception.
        /// </summary>
        /// <param name="ex">
        /// The exception to display.
        /// </param>
        /// <param name="appInfoProvider">
        /// Reference to an object that will provide application information.
        /// </param>
        /// <param name="bugstarInfoProvider">
        /// Reference to an object that will provide bugstar information.
        /// </param>
        /// <param name="visibleButtons">
        /// The value defining which of the available buttons should be visible on the window.
        /// </param>
        public RsExceptionWindow(
            Exception ex,
            IApplicationInfoProvider appInfoProvider,
            IBugstarInfoProvider bugstarInfoProvider,
            ExceptionWindowButtons visibleButtons)
        {
            this._applicationInfoProvider = appInfoProvider;
            this._bugstarInfoProvider = bugstarInfoProvider;
            this._exception = ex;
            this._visibleButtons = visibleButtons;

            this._formatter = new ExceptionFormatter(ex);
            this._exceptionDetails = String.Join("\n", this._formatter.Format());

            this.DataContext = this;
            this.InitializeComponent();

            if (!String.IsNullOrWhiteSpace(appInfoProvider.ProductName))
            {
                this.Title += String.Format(" - {0}", appInfoProvider.ProductName);
            }

            this.CommandBindings.Add(
                new CommandBinding(
                    ExceptionCommands.Quit,
                    this.OnQuit));

            this.CommandBindings.Add(
                new CommandBinding(
                    ExceptionCommands.Continue,
                    this.OnContinue));

            this.CommandBindings.Add(
                new CommandBinding(
                    ExceptionCommands.LogBug,
                    this.OnLogBug));

            this.CommandBindings.Add(
                new CommandBinding(
                    ExceptionCommands.SendEmail,
                    this.OnSendEmail));
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the string containing the details regarding the exception (generally includes
        /// the stack trace).
        /// </summary>
        public string ExceptionDetails
        {
            get { return this._exceptionDetails; }
        }

        /// <summary>
        /// Gets the string containing the exception type.
        /// </summary>
        public string ExceptionType
        {
            get { return this._exception.GetType().FullName; }
        }

        /// <summary>
        /// Gets a value indicating whether we should be showing the continue button.
        /// </summary>
        public bool ShowContinue
        {
            get { return this._visibleButtons.HasFlag(ExceptionWindowButtons.Continue); }
        }

        /// <summary>
        /// Gets a value indicating whether we should be showing the log bug button.
        /// </summary>
        public bool ShowLogBug
        {
            get { return this._visibleButtons.HasFlag(ExceptionWindowButtons.LogBug); }
        }

        /// <summary>
        /// Gets a value indicating whether we should be showing the quit button.
        /// </summary>
        public bool ShowQuit
        {
            get { return this._visibleButtons.HasFlag(ExceptionWindowButtons.Quit); }
        }

        /// <summary>
        /// Gets a value indicating whether we should be showing the send email button.
        /// </summary>
        public bool ShowSendEmail
        {
            get
            {
                // Only show the button if we were requested to and outlook is installed.
                if (!OutlookApplication.IsOutlookInstalled())
                {
                    return false;
                }

                return this._visibleButtons.HasFlag(ExceptionWindowButtons.SendEmail);
            }
        }

        /// <summary>
        /// Gets the exception that this view model is wrapping.
        /// </summary>
        public Exception ThrownException
        {
            get { return this._exception; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Called whenever the window receives the WM_SYSCOMMAND message with the CONTEXTHELP
        /// identifier set in the word parameter.
        /// </summary>
        protected override void HandleContextHelp()
        {
            Process.Start(HelpUrl);
        }

        /// <summary>
        /// Called whenever the <see cref="ExceptionCommands.Continue"/> command is fired and
        /// needs handling at this instance level.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The execute data that has been sent with the command.
        /// </param>
        private void OnContinue(object s, ExecutedRoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Called whenever the <see cref="ExceptionCommands.LogBug"/> command is fired and
        /// needs handling at this instance level.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The execute data that has been sent with the command.
        /// </param>
        private void OnLogBug(object s, ExecutedRoutedEventArgs e)
        {
            BugstarApplication app = new BugstarApplication();
            if (!app.IsBugstarRunning())
            {
                RsMessageBox.Show(
                    WindowResources.BugstarNotRunningMessage,
                    WindowResources.BugstarNotRunningCaption);
                return;
            }

            try
            {
                PendingBug bug = new PendingBug();
                bug.Project = this._bugstarInfoProvider.BugstarProjectName;

                bug.Summary = String.Format(
                    "{0} - {1}",
                    this._bugstarInfoProvider.ProductName,
                    this._formatter.FormatHeading());
                bug.Description = String.Join("\n", this._formatter.Format());
                bug.Category = BugCategory.B;
                bug.Tags.Add("Editor Framework Generated Bug");
                bug.Tags.Add("Exception");

                bug.Owner = this._bugstarInfoProvider.DefaultBugOwner;
                bug.Component = this._bugstarInfoProvider.BugstarComponent;

                RSG.Base.Logging.LogFactory.FlushApplicationLog();
                bug.FileAttachments.Add(RSG.Base.Logging.LogFactory.ApplicationLogFilename);

                // Attempt to capture a screen shot of the main window.
                MemoryStream screenshotStream = null;

                RsWindow mainWindow = Application.Current.MainWindow as RsWindow;
                if (mainWindow != null)
                {
                    screenshotStream = new MemoryStream();
                    try
                    {
                        BitmapSource screenshotSource = mainWindow.CaptureScreenshot();

                        PngBitmapEncoder png = new PngBitmapEncoder();
                        png.Frames.Add(BitmapFrame.Create(screenshotSource));
                        png.Save(screenshotStream);

                        screenshotStream.Position = 0;
                        bug.AddStreamedAttachment("MainWindowScreenshot.png", screenshotStream);
                    }
                    catch (Exception exception)
                    {
                        // Ignore any exceptions that might happen while trying to capture
                        // the screen shot.
                        LogFactory.ApplicationLog.ToolException(exception,
                            "An exception occurred while attempting to capture a screen shot to attach to the bug.");
                    }
                }

                app.CreateBug(bug);

                // Clean up after ourselves.
                if (screenshotStream != null)
                {
                    screenshotStream.Dispose();
                }
            }
            catch (Exception)
            {
                RsMessageBox.Show(WindowResources.CreateBugFailedMsg);
            }
        }

        /// <summary>
        /// Called whenever the <see cref="ExceptionCommands.Quit"/> command is fired and
        /// needs handling at this instance level.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The execute data that has been sent with the command.
        /// </param>
        private void OnQuit(object s, ExecutedRoutedEventArgs e)
        {
            RsApplication app = Application.Current as RsApplication;
            if (app != null)
            {
                Environment.ExitCode = ExitCode.Failure;
                app.ForceExit();
            }
            else
            {
                Close();
            }
        }

        /// <summary>
        /// Called whenever the <see cref="ExceptionCommands.SendEmail"/> command is fired and
        /// needs handling at this instance level.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The execute data that has been sent with the command.
        /// </param>
        private void OnSendEmail(object s, ExecutedRoutedEventArgs e)
        {
            try
            {
                String[] defaultRecipients =
                    _applicationInfoProvider.AuthorEmailAddress.SafeSplit(';', true);

                using (IApplication app = OutlookApplication.Create())
                {
                    object obj = app.CreateItem(OlItemType.olMailItem);
                    using (IMailItem mail = (IMailItem)COMWrapper.Wrap(obj, typeof(IMailItem)))
                    {
                        // Fill in the default information.
                        foreach (String recipient in defaultRecipients)
                        {
                            mail.Recipients.Add(recipient);
                        }

                        mail.Recipients.ResolveAll();
                        mail.Subject = String.Format("[{0}] {1}",
                            _applicationInfoProvider.CommandOptions.Project.FriendlyName, this._formatter.FormatHeading());
                        mail.Body = String.Format(
                            CultureInfo.InvariantCulture,
                            ExceptionWindowResources.EmailBodyFormat,
                            String.Join("\n", this._formatter.Format()),
                            Environment.UserName,
                            Environment.MachineName);

                        mail.Save();
                        mail.Display(false);
                    }
                }
            }
            catch (System.Exception)
            {
                RsMessageBox.Show(ExceptionWindowResources.EmailFailedMsg);
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.Exceptions.RsExceptionWindow {Class}
} // RSG.Editor.Controls.Exceptions {Namespace}
