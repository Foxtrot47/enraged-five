﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Editor.Controls.ZoomCanvas
{
    /// <summary>
    /// Scale mode that the zoomable canvas should use.
    /// </summary>
    public enum ScaleMode
    {
        /// <summary>
        /// Uniform scaling has a single scale value which 
        /// </summary>
        Uniform,

        /// <summary>
        /// Non-uniform scaling allows for different X/Y scales.
        /// </summary>
        NonUniform
    }
}
