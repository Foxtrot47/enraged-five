﻿//---------------------------------------------------------------------------------------------
// <copyright file="ICommandWithScopes.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System.Collections.ObjectModel;

    /// <summary>
    /// When implemented represents a command definition class that contains a collection of
    /// strings that define the different scopes this command can have.
    /// </summary>
    public interface ICommandWithScopes
    {
        #region Properties
        /// <summary>
        /// Gets the collection defining the different scopes for this command.
        /// </summary>
        ObservableCollection<string> Scopes { get; }
        #endregion Properties
    }  // RSG.Editor.ICommandWithScopes {Interface}
} // RSG.Editor {Namespace}
