﻿//---------------------------------------------------------------------------------------------
// <copyright file="DialogueProjectIcons.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.ViewModel
{
    using System;
    using System.Windows.Media.Imaging;
    using RSG.Base.Extensions;

    /// <summary>
    /// Provides static properties that give access to the icons used in the project explorer
    /// for dialogue related objects.
    /// </summary>
    internal static class DialogueProjectIcons
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="DialogueFile"/> property.
        /// </summary>
        private static BitmapSource _dialogueFile;

        /// <summary>
        /// The private field used for the <see cref="DialogueProject"/> property.
        /// </summary>
        private static BitmapSource _dialogueProject;

        /// <summary>
        /// A generic object that provides thread safety access to the image sources.
        /// </summary>
        private static object _syncRoot = new object();
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets the icon that is used for a dialogue file in the project explorer.
        /// </summary>
        public static BitmapSource DialogueFile
        {
            get
            {
                if (_dialogueFile == null)
                {
                    lock (_syncRoot)
                    {
                        if (_dialogueFile == null)
                        {
                            EnsureLoaded(ref _dialogueFile, "dialogueFile16");
                        }
                    }
                }

                return _dialogueFile;
            }
        }

        /// <summary>
        /// Gets the icon that is used for a dialogue project in the project explorer.
        /// </summary>
        public static BitmapSource DialogueProject
        {
            get
            {
                if (_dialogueProject == null)
                {
                    lock (_syncRoot)
                    {
                        if (_dialogueProject == null)
                        {
                            EnsureLoaded(ref _dialogueProject, "dialogueProject16");
                        }
                    }
                }

                return _dialogueProject;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Ensures that the specified image source is loaded with the specified resource name.
        /// </summary>
        /// <param name="source">
        /// The image source that should be loaded.
        /// </param>
        /// <param name="resourceName">
        /// The name of the resource the loaded image source should set as its source.
        /// </param>
        private static void EnsureLoaded(ref BitmapSource source, string resourceName)
        {
            if (source != null)
            {
                return;
            }

            string assemblyName = typeof(DialogueProjectIcons).Assembly.GetName().Name;
            string path = "Resources/" + resourceName + ".png";
            string bitmapPath = "pack://application:,,,/{0};component/{1}";
            bitmapPath = bitmapPath.FormatInvariant(assemblyName, path);
            Uri uri = new Uri(bitmapPath, UriKind.RelativeOrAbsolute);

            source = new BitmapImage(uri);
            source.Freeze();
        }
        #endregion Methods
    } // RSG.Text.ViewModel.DialogueProjectIcons {Class}
} // RSG.Text.ViewModel.Commands {Namespace}
