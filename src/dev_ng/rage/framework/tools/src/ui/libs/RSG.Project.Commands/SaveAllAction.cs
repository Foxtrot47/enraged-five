﻿//---------------------------------------------------------------------------------------------
// <copyright file="SaveAllAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Commands
{
    using System.Collections.Generic;
    using System.Diagnostics;
    using RSG.Editor;
    using RSG.Editor.Controls.Dock.ViewModel;
    using RSG.Editor.SharedCommands;
    using RSG.Project.ViewModel;

    /// <summary>
    /// Contains the logic for the <see cref="RockstarCommands.SaveAll"/> routed command. This
    /// class cannot be inherited.
    /// </summary>
    public sealed class SaveAllAction : ButtonAction<ProjectCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SaveAllAction"/> class.
        /// </summary>
        /// <param name="serviceProvider">
        /// The service provider that is used by this action to perform application specific
        /// operations, for example selecting a file to open.
        /// </param>
        public SaveAllAction(ICommandServiceProvider serviceProvider)
            : base(serviceProvider)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="SaveAllAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public SaveAllAction(ProjectCommandResolver resolver)
            : base(new ParameterResolverDelegate<ProjectCommandArgs>(resolver))
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(ProjectCommandArgs args)
        {
            IDocumentManagerService docService = this.GetService<IDocumentManagerService>();
            if (docService == null)
            {
                Debug.Fail("Unable to save all due to the fact the service is missing.");
                return;
            }

            List<ISaveable> items = new List<ISaveable>();
            if (docService.IsProjectCollectionModified(args.CollectionNode))
            {
                docService.SaveCollection(args.CollectionNode, false);
            }

            foreach (ProjectNode project in args.CollectionNode.GetProjects())
            {
                if (docService.IsProjectModified(project))
                {
                    docService.SaveProject(project, false);
                }
            }

            List<DocumentItem> documentsToSave = new List<DocumentItem>();
            foreach (DocumentItem document in docService.OpenedDocuments)
            {
                if (docService.IsDocumentModified(document))
                {
                    documentsToSave.Add(document);
                }
            }

            docService.SaveDocuments(documentsToSave, false);
        }
        #endregion Methods
    } // RSG.Project.Commands.SaveAllAction {Class}
} // RSG.Project.Commands {Namespace}
