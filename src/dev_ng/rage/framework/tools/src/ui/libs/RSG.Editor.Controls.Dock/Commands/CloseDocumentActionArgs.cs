﻿//---------------------------------------------------------------------------------------------
// <copyright file="CloseDocumentActionArgs.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock.Commands
{
    using System.Collections.Generic;
    using RSG.Editor.Controls.Dock.Document;

    /// <summary>
    /// Represents the action arguments for the <see cref="CloseDocumentItemsAction"/> class.
    /// </summary>
    public class CloseDocumentActionArgs
    {
        #region Fields
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CloseDocumentActionArgs"/> class.
        /// </summary>
        /// <param name="items">
        /// The items that 
        /// </param>
        public CloseDocumentActionArgs(IEnumerable<RsDocumentItem> items)
        {
        }
        #endregion Constructors
    } // RSG.Editor.Controls.Dock.Commands.CloseDocumentActionArgs {Class}
} // RSG.Editor.Controls.Dock.Commands {Namespace}
