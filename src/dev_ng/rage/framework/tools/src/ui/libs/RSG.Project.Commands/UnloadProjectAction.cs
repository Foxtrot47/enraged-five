﻿//---------------------------------------------------------------------------------------------
// <copyright file="UnloadProjectAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Commands
{
    using System.Diagnostics;
    using System.Linq;
    using RSG.Editor;
    using RSG.Project.ViewModel;

    /// <summary>
    /// Contains the logic for the <see cref="ProjectCommands.UnloadProject"/> routed command.
    /// This class cannot be inherited.
    /// </summary>
    public sealed class UnloadProjectAction : ButtonAction<ProjectCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="UnloadProjectAction"/> class.
        /// </summary>
        /// <param name="serviceProvider">
        /// The service provider that is used by this action to perform application specific
        /// operations, for example selecting a file to open.
        /// </param>
        public UnloadProjectAction(ICommandServiceProvider serviceProvider)
            : base(serviceProvider)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="UnloadProjectAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public UnloadProjectAction(ProjectCommandResolver resolver)
            : base(new ParameterResolverDelegate<ProjectCommandArgs>(resolver))
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ProjectCommandArgs args)
        {
            if (args == null)
            {
                return false;
            }

            if (args.SelectionCount == 1)
            {
                ProjectNode node = args.SelectedNodes.FirstOrDefault() as ProjectNode;
                if (node != null)
                {
                    return node.Loaded;
                }
            }

            return false;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(ProjectCommandArgs args)
        {
            IDocumentManagerService docService = this.GetService<IDocumentManagerService>();
            if (docService == null)
            {
                Debug.Fail(
                    "Unable to unload project due to the fact the service is missing.");
                return;
            }

            ProjectNode project = args.SelectedNodes.FirstOrDefault() as ProjectNode;
            if (project == null)
            {
                return;
            }

            docService.UnloadProject(project, true);
        }
        #endregion Methods
    } // RSG.Project.Commands.UnloadProjectAction {Class}
} // RSG.Project.Commands {Namespace}
