﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsSplitterItem.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock
{
    using System.Windows;
    using System.Windows.Automation;
    using System.Windows.Controls;

    /// <summary>
    /// Represents the control that is created for each item inside a
    /// <see cref="RsSplitterPanel"/> control.
    /// </summary>
    public class RsSplitterItem : ContentControl
    {
        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsSplitterItem"/> class.
        /// </summary>
        static RsSplitterItem()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsSplitterItem),
                new FrameworkPropertyMetadata(typeof(RsSplitterItem)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsSplitterItem"/> class.
        /// </summary>
        public RsSplitterItem()
        {
            AutomationProperties.SetAutomationId(this, "RsSplitterItem");
        }
        #endregion Constructors
    } // RSG.Editor.Controls.Dock.RsSplitterItem {Class}
} // RSG.Editor.Controls.Dock {Namespace}
