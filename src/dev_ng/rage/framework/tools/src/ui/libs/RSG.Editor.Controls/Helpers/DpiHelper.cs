﻿//---------------------------------------------------------------------------------------------
// <copyright file="DpiHelper.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Helpers
{
    using System;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Media;
    using RSG.Interop.Microsoft.Windows;

    /// <summary>
    /// Provides helper methods to transform between device and logical dots per inch spaces.
    /// </summary>
    public static class DpiHelper
    {
        #region Fields
        /// <summary>
        /// The number of dots per inch in logical space.
        /// </summary>
        private const double LogicalDpi = 96.0;

        /// <summary>
        /// The private field used for the <see cref="DeviceDpiX"/> property.
        /// </summary>
        private static double _deviceDpiX;

        /// <summary>
        /// The private field used for the <see cref="DeviceDpiY"/> property.
        /// </summary>
        private static double _deviceDpiY;

        /// <summary>
        /// The private field used for the <see cref="TransformFromDevice"/> property.
        /// </summary>
        private static MatrixTransform _transformFromDevice;

        /// <summary>
        /// The private field used for the <see cref="TransformToDevice"/> property.
        /// </summary>
        private static MatrixTransform _transformToDevice;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="DpiHelper"/> class.
        /// </summary>
        static DpiHelper()
        {
            IntPtr dc = User32.GetDC(IntPtr.Zero);
            if (dc != IntPtr.Zero)
            {
                DpiHelper._deviceDpiX = (double)Gdi32.GetDeviceCaps(
                    dc, DeviceCapability.LOGPIXELSX);
                DpiHelper._deviceDpiY = (double)Gdi32.GetDeviceCaps(
                    dc, DeviceCapability.LOGPIXELSY);

                bool result = User32.ReleaseDC(IntPtr.Zero, dc);
                Debug.Assert(result, "Failed to release screen device context!");
            }
            else
            {
                DpiHelper._deviceDpiX = LogicalDpi;
                DpiHelper._deviceDpiY = LogicalDpi;
            }

            Matrix toDeviceMatrix = Matrix.Identity;
            Matrix fromDeviceMatrix = Matrix.Identity;

            double toDeviceScaleX = DpiHelper.DeviceDpiX / LogicalDpi;
            double toDeviceScaleY = DpiHelper.DeviceDpiY / LogicalDpi;
            toDeviceMatrix.Scale(toDeviceScaleX, toDeviceScaleY);

            double fromDeviceScaleX = LogicalDpi / DpiHelper.DeviceDpiX;
            double fromDeviceScaleY = LogicalDpi / DpiHelper.DeviceDpiY;
            fromDeviceMatrix.Scale(fromDeviceScaleX, fromDeviceScaleY);

            _transformToDevice = new MatrixTransform(toDeviceMatrix);
            _transformToDevice.Freeze();

            _transformFromDevice = new MatrixTransform(fromDeviceMatrix);
            _transformFromDevice.Freeze();
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the number of pixels per logical inch along the screen width.
        /// </summary>
        public static double DeviceDpiX
        {
            get { return DpiHelper._deviceDpiX; }
        }

        /// <summary>
        /// Gets the number of pixels per logical inch along the screen height.
        /// </summary>
        public static double DeviceDpiY
        {
            get { return DpiHelper._deviceDpiY; }
        }

        /// <summary>
        /// Gets the scaling factor from device to logical units in the x direction.
        /// </summary>
        public static double DeviceToLogicalScalingFactorX
        {
            get { return DpiHelper.TransformFromDevice.Matrix.M11; }
        }

        /// <summary>
        /// Gets the scaling factor from device to logical units in the y direction.
        /// </summary>
        public static double DeviceToLogicalScalingFactorY
        {
            get { return DpiHelper.TransformFromDevice.Matrix.M22; }
        }

        /// <summary>
        /// Gets the scaling factor from logical to device units in the x direction.
        /// </summary>
        public static double LogicalToDeviceScalingFactorX
        {
            get { return DpiHelper.TransformToDevice.Matrix.M11; }
        }

        /// <summary>
        /// Gets the scaling factor from logical to device units in the y direction.
        /// </summary>
        public static double LogicalToDeviceScalingFactorY
        {
            get { return DpiHelper.TransformToDevice.Matrix.M22; }
        }

        /// <summary>
        /// Gets the transform matrix to use to transform from device space to logical space.
        /// </summary>
        public static MatrixTransform TransformFromDevice
        {
            get { return DpiHelper._transformFromDevice; }
        }

        /// <summary>
        /// Gets the transform matrix to use to transform from logical space to device space.
        /// </summary>
        public static MatrixTransform TransformToDevice
        {
            get { return DpiHelper._transformToDevice; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Transforms the specified point from device space to logical space.
        /// </summary>
        /// <param name="devicePoint">
        /// The point to transform.
        /// </param>
        /// <returns>
        /// A point that is the specified point after being transformed into logical space.
        /// </returns>
        public static Point DeviceToLogicalUnits(this Point devicePoint)
        {
            return DpiHelper.TransformFromDevice.Transform(devicePoint);
        }

        /// <summary>
        /// Transforms the specified rectangle from device space to logical space.
        /// </summary>
        /// <param name="deviceRect">
        /// The rectangle to transform.
        /// </param>
        /// <returns>
        /// A rectangle that is the specified rectangle after being transformed into
        /// logical space.
        /// </returns>
        public static Rect DeviceToLogicalUnits(this Rect deviceRect)
        {
            Rect result = deviceRect;
            result.Transform(DpiHelper.TransformFromDevice.Matrix);
            return result;
        }

        /// <summary>
        /// Transforms the specified size from device space to logical space.
        /// </summary>
        /// <param name="deviceSize">
        /// The size to transform.
        /// </param>
        /// <returns>
        /// A size that is the specified size after being transformed into logical space.
        /// </returns>
        public static Size DeviceToLogicalUnits(this Size deviceSize)
        {
            double width = deviceSize.Width * DpiHelper.DeviceToLogicalScalingFactorX;
            double height = deviceSize.Height * DpiHelper.DeviceToLogicalScalingFactorY;
            return new Size(width, height);
        }

        /// <summary>
        /// Transforms the specified point from logical space to device space.
        /// </summary>
        /// <param name="logicalPoint">
        /// The point to transform.
        /// </param>
        /// <returns>
        /// A point that is the specified point after being transformed into device space.
        /// </returns>
        public static Point LogicalToDeviceUnits(this Point logicalPoint)
        {
            return DpiHelper.TransformToDevice.Transform(logicalPoint);
        }

        /// <summary>
        /// Transforms the specified rectangle from logical space to device space.
        /// </summary>
        /// <param name="logicalRect">
        /// The rectangle to transform.
        /// </param>
        /// <returns>
        /// A rectangle that is the specified rectangle after being transformed into
        /// device space.
        /// </returns>
        public static Rect LogicalToDeviceUnits(this Rect logicalRect)
        {
            Rect result = logicalRect;
            result.Transform(DpiHelper.TransformToDevice.Matrix);
            return result;
        }

        /// <summary>
        /// Transforms the specified size from logical space to device space.
        /// </summary>
        /// <param name="logicalSize">
        /// The size to transform.
        /// </param>
        /// <returns>
        /// A size that is the specified size after being transformed into device space.
        /// </returns>
        public static Size LogicalToDeviceUnits(this Size logicalSize)
        {
            double width = logicalSize.Width * DpiHelper.LogicalToDeviceScalingFactorX;
            double height = logicalSize.Height * DpiHelper.LogicalToDeviceScalingFactorY;
            return new Size(width, height);
        }
        #endregion Methods
    } // RSG.Editor.Controls.Helpers.DpiHelper {Class}
} // RSG.Editor.Controls.Helpers {Namespace}
