﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Attributes;

namespace RSG.Editor.Controls.CurveEditor
{
    /// <summary>
    /// Defines how the user wishes to interact with the curve editor.
    /// </summary>
    public enum InteractionMode
    {
        /// <summary>
        /// 
        /// </summary>
        [FieldDisplayName("Move")]
        [DisplayIcon("RSG.Editor.Controls.CurveEditor", "Resources/move16.png")]
        MoveFree,

        /// <summary>
        /// 
        /// </summary>
        [FieldDisplayName("Move Horizontally")]
        [DisplayIcon("RSG.Editor.Controls.CurveEditor", "Resources/moveHorizontally16.png")]
        MoveHorizontally,

        /// <summary>
        /// 
        /// </summary>
        [FieldDisplayName("Move Vertically")]
        [DisplayIcon("RSG.Editor.Controls.CurveEditor", "Resources/moveVertically16.png")]
        MoveVertically,

        /// <summary>
        /// 
        /// </summary>
        [FieldDisplayName("Insert Point")]
        [DisplayIcon("RSG.Editor.Controls.CurveEditor", "Resources/insertPoint16.png")]
        InsertPoint,

        /// <summary>
        /// 
        /// </summary>
        [FieldDisplayName("Pan")]
        [DisplayIcon("RSG.Editor.Controls.CurveEditor", "Resources/pan16.png")]
        Panning,

        /// <summary>
        /// 
        /// </summary>
        [FieldDisplayName("Zoom")]
        [DisplayIcon("RSG.Editor.Controls.CurveEditor", "Resources/zoom16.png")]
        Zooming,

        /// <summary>
        /// 
        /// </summary>
        [FieldDisplayName("Zoom Region")]
        [DisplayIcon("RSG.Editor.Controls.CurveEditor", "Resources/zoomRegion16.png")]
        ZoomRegion
    } // InteractionMode
}
