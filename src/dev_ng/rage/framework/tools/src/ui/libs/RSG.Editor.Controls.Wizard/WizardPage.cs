﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace RSG.Editor.Controls.Wizard
{

    /// <summary>
    /// 
    /// </summary>
    public class WizardPage : UserControl
    {
        #region Properties
        /// <summary>
        /// Gets or sets the page header text.
        /// </summary>
        public String Header
        {
            get { return (String)this.GetValue(HeaderProperty); }
            set { this.SetValue(HeaderProperty, value); }
        }

        /// <summary>
        /// Gets or sets the page Previous navigation enabled state.
        /// </summary>
        public bool AllowPrevious
        {
            get { return (bool)this.GetValue(AllowPreviousProperty); }
            set { this.SetValue(AllowPreviousProperty, value); }
        }

        /// <summary>
        /// Gets or sets the page Next navigation enabled state.
        /// </summary>
        public bool AllowNext
        {
            get { return (bool)this.GetValue(AllowNextProperty); }
            set { this.SetValue(AllowNextProperty, value); }
        }

        /// <summary>
        /// Gets or sets the page Finish navigation enabled state.
        /// </summary>
        public bool AllowFinish
        {
            get { return (bool)this.GetValue(AllowFinishProperty); }
            set { this.SetValue(AllowFinishProperty, value); }
        }
        #endregion // Properties

        #region Dependency Properties
        /// <summary>
        /// Identifies the <see cref="Header"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty HeaderProperty;

        /// <summary>
        /// Identifies the <see cref="AllowBack"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty AllowPreviousProperty;

        /// <summary>
        /// Identifies the <see cref="AllowNext"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty AllowNextProperty;

        /// <summary>
        /// Identifies the <see cref="AllowFinish"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty AllowFinishProperty;
        #endregion // Dependency Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public WizardPage()
        {
        }

        /// <summary>
        /// Static constructor.
        /// </summary>
        static WizardPage()
        {
            HeaderProperty = DependencyProperty.Register("Header",
                typeof(String), typeof(WizardPage), new PropertyMetadata(null, null, null));
            AllowPreviousProperty = DependencyProperty.Register("AllowPrevious",
                typeof(bool), typeof(WizardPage), new PropertyMetadata(true, null, null));
            AllowNextProperty = DependencyProperty.Register("AllowNext",
                typeof(bool), typeof(WizardPage), new PropertyMetadata(true, null, null));
            AllowFinishProperty = DependencyProperty.Register("AllowFinish",
                typeof(bool), typeof(WizardPage), new PropertyMetadata(false, null, null));
        }
        #endregion // Constructor(s)
    }

} // RSG.Editor.Controls.Wizard 
