﻿//---------------------------------------------------------------------------------------------
// <copyright file="Icons.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.ViewModel
{
    using System;
    using System.Windows.Media.Imaging;
    using RSG.Base.Extensions;

    /// <summary>
    /// Provides static properties that give access to the icons used in the project explorer
    /// for dialogue related objects and the text views.
    /// </summary>
    public static class Icons
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="AudioPlayback"/> property.
        /// </summary>
        private static BitmapSource _audioPlayback;

        /// <summary>
        /// The private field used for the <see cref="BuildCollection"/> property.
        /// </summary>
        private static BitmapSource _buildCollection;

        /// <summary>
        /// The private field used for the <see cref="BuildSelection"/> property.
        /// </summary>
        private static BitmapSource _buildSelection;

        /// <summary>
        /// The private field used for the <see cref="DialogueFile"/> property.
        /// </summary>
        private static BitmapSource _dialogueFile;

        /// <summary>
        /// The private field used for the <see cref="DialogueProject"/> property.
        /// </summary>
        private static BitmapSource _dialogueProject;

        /// <summary>
        /// The private field used for the <see cref="ExcelExport"/> property.
        /// </summary>
        private static BitmapSource _excelExport;

        /// <summary>
        /// The private field used for the <see cref="GenerateFilename"/> property.
        /// </summary>
        private static BitmapSource _generateFilename;

        /// <summary>
        /// The private field used for the <see cref="GenerateAllFilenames"/> property.
        /// </summary>
        private static BitmapSource _generateAllFilenames;

        /// <summary>
        /// The private field used for the <see cref="InsertConversation"/> property.
        /// </summary>
        private static BitmapSource _insertConversation;

        /// <summary>
        /// The private field used for the <see cref="InsertText"/> property.
        /// </summary>
        private static BitmapSource _insertLine;

        /// <summary>
        /// The private field used for the <see cref="NewConversation"/> property.
        /// </summary>
        private static BitmapSource _newConversation;

        /// <summary>
        /// The private field used for the <see cref="NewLine"/> property.
        /// </summary>
        private static BitmapSource _newLine;

        /// <summary>
        /// The private field used for the <see cref="NewText"/> property.
        /// </summary>
        private static BitmapSource _newText;

        /// <summary>
        /// The private field used for the <see cref="PlayConversation"/> property.
        /// </summary>
        private static BitmapSource _playConversation;

        /// <summary>
        /// The private field used for the <see cref="PlayLine"/> property.
        /// </summary>
        private static BitmapSource _playLine;

        /// <summary>
        /// The private field used for the <see cref="StopPlaying"/> property.
        /// </summary>
        private static BitmapSource _stopPlaying;

        /// <summary>
        /// A generic object that provides thread safety access to the image sources.
        /// </summary>
        private static object _syncRoot = new object();

        /// <summary>
        /// The private field used for the <see cref="WordExport"/> property.
        /// </summary>
        private static BitmapSource _wordExport;
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets the icon that is used for the audio playback button.
        /// </summary>
        public static BitmapSource AudioPlayback
        {
            get
            {
                if (_audioPlayback == null)
                {
                    lock (_syncRoot)
                    {
                        if (_audioPlayback == null)
                        {
                            EnsureLoaded(ref _audioPlayback, "audioPlayback16");
                        }
                    }
                }

                return _audioPlayback;
            }
        }

        /// <summary>
        /// Gets the icon that is used for BuildCollection.
        /// </summary>
        public static BitmapSource BuildCollection
        {
            get
            {
                if (_buildCollection == null)
                {
                    lock (_syncRoot)
                    {
                        if (_buildCollection == null)
                        {
                            EnsureLoaded(ref _buildCollection, "buildCollection16");
                        }
                    }
                }

                return _buildCollection;
            }
        }

        /// <summary>
        /// Gets the icon that is used for BuildSelection.
        /// </summary>
        public static BitmapSource BuildSelection
        {
            get
            {
                if (_buildSelection == null)
                {
                    lock (_syncRoot)
                    {
                        if (_buildSelection == null)
                        {
                            EnsureLoaded(ref _buildSelection, "buildSelection16");
                        }
                    }
                }

                return _buildSelection;
            }
        }

        /// <summary>
        /// Gets the icon that is used for a dialogue file in the project explorer.
        /// </summary>
        public static BitmapSource DialogueFile
        {
            get
            {
                if (_dialogueFile == null)
                {
                    lock (_syncRoot)
                    {
                        if (_dialogueFile == null)
                        {
                            EnsureLoaded(ref _dialogueFile, "dialogueFile16");
                        }
                    }
                }

                return _dialogueFile;
            }
        }

        /// <summary>
        /// Gets the icon that is used for a dialogue project in the project explorer.
        /// </summary>
        public static BitmapSource DialogueProject
        {
            get
            {
                if (_dialogueProject == null)
                {
                    lock (_syncRoot)
                    {
                        if (_dialogueProject == null)
                        {
                            EnsureLoaded(ref _dialogueProject, "dialogueProject16");
                        }
                    }
                }

                return _dialogueProject;
            }
        }

        /// <summary>
        /// Gets the icon that shows microsoft office excel image.
        /// </summary>
        public static BitmapSource ExcelExport
        {
            get
            {
                if (_excelExport == null)
                {
                    lock (_syncRoot)
                    {
                        if (_excelExport == null)
                        {
                            EnsureLoaded(ref _excelExport, "excelExport16");
                        }
                    }
                }

                return _excelExport;
            }
        }

        /// <summary>
        /// Gets the icon that shows the generate all filenames image.
        /// </summary>
        public static BitmapSource GenerateAllFilenames
        {
            get
            {
                if (_generateAllFilenames == null)
                {
                    lock (_syncRoot)
                    {
                        if (_generateAllFilenames == null)
                        {
                            EnsureLoaded(ref _generateAllFilenames, "generateAllFilenames16");
                        }
                    }
                }

                return _generateAllFilenames;
            }
        }

        /// <summary>
        /// Gets the icon that shows the generate filename image.
        /// </summary>
        public static BitmapSource GenerateFilename
        {
            get
            {
                if (_generateFilename == null)
                {
                    lock (_syncRoot)
                    {
                        if (_generateFilename == null)
                        {
                            EnsureLoaded(ref _generateFilename, "generateFilename16");
                        }
                    }
                }

                return _generateFilename;
            }
        }

        /// <summary>
        /// Gets the icon that shows InsertConversation.
        /// </summary>
        public static BitmapSource InsertConversation
        {
            get
            {
                if (_insertConversation == null)
                {
                    lock (_syncRoot)
                    {
                        if (_insertConversation == null)
                        {
                            EnsureLoaded(ref _insertConversation, "insertConversation16");
                        }
                    }
                }

                return _insertConversation;
            }
        }

        /// <summary>
        /// Gets the icon that shows InsertLine.
        /// </summary>
        public static BitmapSource InsertLine
        {
            get
            {
                if (_insertLine == null)
                {
                    lock (_syncRoot)
                    {
                        if (_insertLine == null)
                        {
                            EnsureLoaded(ref _insertLine, "insertLine16");
                        }
                    }
                }

                return _insertLine;
            }
        }

        /// <summary>
        /// Gets the icon that shows NewConversation.
        /// </summary>
        public static BitmapSource NewConversation
        {
            get
            {
                if (_newConversation == null)
                {
                    lock (_syncRoot)
                    {
                        if (_newConversation == null)
                        {
                            EnsureLoaded(ref _newConversation, "newConversation16");
                        }
                    }
                }

                return _newConversation;
            }
        }

        /// <summary>
        /// Gets the icon that shows NewLine.
        /// </summary>
        public static BitmapSource NewLine
        {
            get
            {
                if (_newLine == null)
                {
                    lock (_syncRoot)
                    {
                        if (_newLine == null)
                        {
                            EnsureLoaded(ref _newLine, "newLine16");
                        }
                    }
                }

                return _newLine;
            }
        }

        /// <summary>
        /// Gets the icon that shows NewText.
        /// </summary>
        public static BitmapSource NewText
        {
            get
            {
                if (_newText == null)
                {
                    lock (_syncRoot)
                    {
                        if (_newText == null)
                        {
                            EnsureLoaded(ref _newText, "newText16");
                        }
                    }
                }

                return _newText;
            }
        }

        /// <summary>
        /// Gets the icon that is used for PlayConversation.
        /// </summary>
        public static BitmapSource PlayConversation
        {
            get
            {
                if (_playConversation == null)
                {
                    lock (_syncRoot)
                    {
                        if (_playConversation == null)
                        {
                            EnsureLoaded(ref _playConversation, "playConversation16");
                        }
                    }
                }

                return _playConversation;
            }
        }

        /// <summary>
        /// Gets the icon that is used for PlayLine.
        /// </summary>
        public static BitmapSource PlayLine
        {
            get
            {
                if (_playLine == null)
                {
                    lock (_syncRoot)
                    {
                        if (_playLine == null)
                        {
                            EnsureLoaded(ref _playLine, "playLine16");
                        }
                    }
                }

                return _playLine;
            }
        }

        /// <summary>
        /// Gets the icon that is used for StopPlaying.
        /// </summary>
        public static BitmapSource StopPlaying
        {
            get
            {
                if (_stopPlaying == null)
                {
                    lock (_syncRoot)
                    {
                        if (_stopPlaying == null)
                        {
                            EnsureLoaded(ref _stopPlaying, "stopPlaying16");
                        }
                    }
                }

                return _stopPlaying;
            }
        }

        /// <summary>
        /// Gets the icon that shows the microsoft office word image.
        /// </summary>
        public static BitmapSource WordExport
        {
            get
            {
                if (_wordExport == null)
                {
                    lock (_syncRoot)
                    {
                        if (_wordExport == null)
                        {
                            EnsureLoaded(ref _wordExport, "wordExport16");
                        }
                    }
                }

                return _wordExport;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Ensures that the specified image source is loaded with the specified resource name.
        /// </summary>
        /// <param name="source">
        /// The image source that should be loaded.
        /// </param>
        /// <param name="resourceName">
        /// The name of the resource the loaded image source should set as its source.
        /// </param>
        private static void EnsureLoaded(ref BitmapSource source, string resourceName)
        {
            if (source != null)
            {
                return;
            }

            string assemblyName = typeof(Icons).Assembly.GetName().Name;
            string path = "Resources/" + resourceName + ".png";
            string bitmapPath = "pack://application:,,,/{0};component/{1}";
            bitmapPath = bitmapPath.FormatInvariant(assemblyName, path);
            Uri uri = new Uri(bitmapPath, UriKind.RelativeOrAbsolute);

            source = new BitmapImage(uri);
            source.Freeze();
        }
        #endregion Methods
    } // RSG.Text.ViewModel.Icons {Class}
} // RSG.Text.ViewModel.Commands {Namespace}
