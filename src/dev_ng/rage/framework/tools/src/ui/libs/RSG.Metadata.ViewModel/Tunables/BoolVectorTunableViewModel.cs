﻿//---------------------------------------------------------------------------------------------
// <copyright file="BoolVectorTunableViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Tunables
{
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// The view model for a <see cref="BoolVectorTunable"/> model object.
    /// </summary>
    public class BoolVectorTunableViewModel : TunableViewModelBase<BoolVectorTunable>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BoolVectorTunableViewModel"/> class.
        /// </summary>
        /// <param name="tunable">
        /// The bool vector tunable that this view model is representing.
        /// </param>
        /// <param name="parent">
        /// The parent tunable view model for this view model.
        /// </param>
        public BoolVectorTunableViewModel(BoolVectorTunable tunable, ITunableViewModel parent)
            : base(tunable, parent)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the value of the w component for this vector.
        /// </summary>
        public bool W
        {
            get { return this.Model.W; }
            set { this.Model.W = value; }
        }

        /// <summary>
        /// Gets or sets the value of the x component for this vector.
        /// </summary>
        public bool X
        {
            get { return this.Model.X; }
            set { this.Model.X = value; }
        }

        /// <summary>
        /// Gets or sets the value of the y component for this vector.
        /// </summary>
        public bool Y
        {
            get { return this.Model.Y; }
            set { this.Model.Y = value; }
        }

        /// <summary>
        /// Gets or sets the value of the z component for this vector.
        /// </summary>
        public bool Z
        {
            get { return this.Model.Z; }
            set { this.Model.Z = value; }
        }
        #endregion Properties
    } // RSG.Metadata.ViewModel.Tunables.BoolVectorTunableViewModel {Class}
} // RSG.Metadata.ViewModel.Tunables {Namespace}
