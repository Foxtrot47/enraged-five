﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Rag.Widgets;

namespace RSG.Rag.ViewModel.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    public class TextViewModel : WidgetViewModel<WidgetText>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        public TextViewModel(WidgetText widget)
            : base(widget)
        {
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public String Text
        {
            get { return Widget.Text; }
            set { Widget.Text = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public uint MaxLength
        {
            get { return Widget.MaxLength;}
        }

        /// <summary>
        /// 
        /// </summary>
        public override bool IsModified
        {
            get { return (Widget.Text != Widget.OriginalText); }
        }

        /// <summary>
        /// 
        /// </summary>
        public override string DefaultValue
        {
            get { return Widget.OriginalText; }
        }
        #endregion // Properties

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        protected override void OnWidgetPropertyChanged(String propertyName)
        {
            base.OnWidgetPropertyChanged(propertyName);
            if (propertyName == "Text")
            {
                NotifyPropertyChanged("IsModified");
            }
        }
        #endregion // Event Handlers
    } // TextViewModel
}
