﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor.Controls.MapViewport.Annotations;

namespace RSG.Editor.Controls.MapViewport.Commands
{
    /// <summary>
    /// The action logic that is responsible for deleting a comment from an annotation.
    /// </summary>
    public class DeleteAnnotationAction : ButtonAction<MapViewport, Annotation>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="DeleteAnnotationAction"/> class
        /// using the specified resolver.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public DeleteAnnotationAction(ParameterResolverDelegate<MapViewport> resolver)
            : base(resolver)
        {
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="viewport">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="annotation">
        /// Annotation to delete.
        /// </param>
        public override bool CanExecute(MapViewport viewport, Annotation annotation)
        {
            return true;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="viewport">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="annotation">
        /// Annotation to delete.
        /// </param>
        public override void Execute(MapViewport viewport, Annotation annotation)
        {
            viewport.Annotations.Remove(annotation);
        }
        #endregion
    }
}
