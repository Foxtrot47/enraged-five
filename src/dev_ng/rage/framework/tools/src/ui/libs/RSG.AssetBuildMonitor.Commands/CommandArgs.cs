﻿//---------------------------------------------------------------------------------------------
// <copyright file="CommandArgs.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.AssetBuildMonitor.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using RSG.AssetBuildMonitor.ViewModel;
    using RSG.AssetBuildMonitor.ViewModel.Project;
    using RSG.Editor.Controls.Dock.ViewModel;
    using RSG.Project.ViewModel;
    
    /// <summary>
    /// Argument class for Asset Build Monitor top-level actions.
    /// </summary>
    /// Most commands should be able to use the friendly controller methods defined
    /// in this class rather than deal with the error checking required of the
    /// various properties which can be null or empty.
    /// 
    public sealed class CommandArgs
    {
        #region Properties
        /// <summary>
        /// Gets or sets the currently load project collection.
        /// </summary>
        public ProjectCollectionNode CurrentProjectCollection
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the collection of currently opened documents.
        /// </summary>
        public IEnumerable<DocumentItem> AllDocuments
        {
            get { return this._allDocuments ?? Enumerable.Empty<DocumentItem>(); }
            set { this._allDocuments = value; }
        }

        /// <summary>
        /// Gets or sets the active document in the Dock Manager.
        /// </summary>
        public DocumentItem ActiveDocument
        {
            get;
            set;
        }

        /// <summary>
        /// All selected items in the Host Explorer.
        /// </summary>
        public IEnumerable<IHierarchyNode> SelectedNodes
        {
            get { return this._selectedNodes ?? Enumerable.Empty<IHierarchyNode>(); }
            set { this._selectedNodes = value; }
        }

        /// <summary>
        /// Selected hosts in the Host Explorer.
        /// </summary>
        public IEnumerable<HostNode> SelectedHosts
        {
            get 
            {
                if (null == this._selectedNodes)
                    return (Enumerable.Empty<HostNode>());
                return this._selectedNodes.OfType<HostNode>(); 
            }
        }

        /// <summary>
        /// Selected folders in the Host Explorer.
        /// </summary>
        public IEnumerable<FolderNode> SelectedFolders
        {
            get
            {
                if (null == this._selectedNodes)
                    return (Enumerable.Empty<FolderNode>());
                return this._selectedNodes.OfType<FolderNode>();
            }
        }

        /// <summary>
        /// Gets or sets the view site that can be used to open new documents or 
        /// manipulate the docking view for the user.
        /// </summary>
        public ViewSite ViewSite
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Bugstar Users enumerable.
        /// </summary>
        public IEnumerable<RSG.Interop.Bugstar.Organisation.User> BugstarUsers
        {
            get { return this._bugstarUsers ?? Enumerable.Empty<RSG.Interop.Bugstar.Organisation.User>(); }
            set { this._bugstarUsers = value; }
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// The private field used for the <see cref="AllDocuments"/> property.
        /// </summary>
        private IEnumerable<DocumentItem> _allDocuments;

        /// <summary>
        /// The private field used for the <see cref="SelectedNodes"/> property.
        /// </summary>
        private IEnumerable<IHierarchyNode> _selectedNodes;
        
        /// <summary>
        /// The private field used for the <see cref="BugstarUsers"/> property.
        /// </summary>
        private IEnumerable<RSG.Interop.Bugstar.Organisation.User> _bugstarUsers;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public CommandArgs()
        {
            this.SelectedNodes = Enumerable.Empty<IHierarchyNode>();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Return active document's Build Items.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<AssetBuildItemViewModel> GetActiveDocumentBuildItems()
        {
            if (null == this.ActiveDocument)
                return (Enumerable.Empty<AssetBuildItemViewModel>());

            Debug.Assert(this.ActiveDocument is HostDocumentItem);
            if (!(this.ActiveDocument is HostDocumentItem))
                throw (new NotSupportedException());
            HostDocumentItem document = (HostDocumentItem)this.ActiveDocument;

            Debug.Assert(document.Content is AssetBuildHistoryViewModel);
            if (!(document.Content is AssetBuildHistoryViewModel))
                throw (new NotSupportedException());

            AssetBuildHistoryViewModel buildHistoryVM = GetDocumentViewModel(document);
            return (buildHistoryVM.BuildItems);
        }

        /// <summary>
        /// Return active document's selected Build Items.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<AssetBuildItemViewModel> GetActiveDocumentSelectedBuildItems()
        {
            IEnumerable<AssetBuildItemViewModel> buildItems = GetActiveDocumentBuildItems();
            return (buildItems.Where(i => i.IsSelected));
        }

        /// <summary>
        /// Return all document's view-model objects.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<AssetBuildHistoryViewModel> GetAllDocumentViewModels()
        {
            ICollection<AssetBuildHistoryViewModel> vms = new List<AssetBuildHistoryViewModel>();
            foreach (DocumentItem document in this.AllDocuments)
            {
                vms.Add(GetDocumentViewModel(document));
            }
            return (vms);
        }

        /// <summary>
        /// Return document's view-model object.
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        public AssetBuildHistoryViewModel GetDocumentViewModel(DocumentItem document)
        {
            if (null == document)
                throw (new ArgumentNullException("document"));

            Debug.Assert(document is HostDocumentItem);
            if (!(document is HostDocumentItem))
                throw (new NotSupportedException());
            HostDocumentItem hostDocument = (HostDocumentItem)document;
            
            Debug.Assert(hostDocument.Content is AssetBuildHistoryViewModel);
            if (!(hostDocument.Content is AssetBuildHistoryViewModel))
                throw (new NotSupportedException());

            return ((AssetBuildHistoryViewModel)hostDocument.Content);
        }

        /// <summary>
        /// Return active 
        /// </summary>
        /// <returns></returns>
        public AssetBuildHistoryViewModel GetActiveDocumentViewModel()
        {
            if (null == this.ActiveDocument)
                return (null);

            return (GetDocumentViewModel(this.ActiveDocument));
        }
        #endregion // Controller Methods
    }

} // RSG.AssetBuildMonitor.Commands namespace
