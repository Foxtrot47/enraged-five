﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using RSG.Editor.Controls.MapViewport.OverlayComponents;

namespace RSG.Editor.Controls.MapViewport.OverlayComponents
{
    /// <summary>
    /// Represents a piece of text that can be rendered in the map viewport.
    /// </summary>
    public class Text : Component
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="FontSize"/> property.
        /// </summary>
        private double _fontSize;

        /// <summary>
        /// Private field for the <see cref="Foreground"/> property.
        /// </summary>
        private Brush _foreground;

        /// <summary>
        /// Private field for the <see cref="Position"/> property.
        /// </summary>
        private Point _position;

        /// <summary>
        /// Private field for the <see cref="PositionOrigin"/> property.
        /// </summary>
        private PositionOrigin _positionOrigin;

        /// <summary>
        /// Private field for the <see cref="Text"/> property.
        /// </summary>
        private string _text;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Text" class using the specified
        /// text, position and position origin.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="position"></param>
        /// <param name="positionOrigin"></param>
        public Text(string text, Point position, PositionOrigin positionOrigin = PositionOrigin.Center)
            : this(text, position, PositionOrigin.Center, Brushes.Black)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Text" class using the specified
        /// text, position, position origin and foreground color.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="position"></param>
        /// <param name="positionOrigin"></param>
        /// <param name="foreground"></param>
        public Text(string text, Point position, PositionOrigin positionOrigin, Brush foreground)
        {
            _text = text;
            _position = position;
            _positionOrigin = positionOrigin;
            _fontSize = 12;
            _foreground = foreground;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Text to display.
        /// </summary>
        public string DisplayText
        {
            get { return _text; }
            set { SetProperty(ref _text, value); }
        }

        /// <summary>
        /// Gets or sets the size of the text to display.
        /// </summary>
        public double FontSize
        {
            get { return _fontSize; }
            set { SetProperty(ref _fontSize, value); }
        }

        /// <summary>
        /// Brush to use to render the text in.
        /// </summary>
        public Brush Foreground
        {
            get { return _foreground; }
            set { SetProperty(ref _foreground, value); }
        }

        /// <summary>
        /// Position of this component in world space.
        /// </summary>
        public override Point Position
        {
            get { return _position; }
            set { SetProperty(ref _position, value); }
        }

        /// <summary>
        /// Where the position relates to in terms of the component's bounds.
        /// </summary>
        public override PositionOrigin PositionOrigin
        {
            get { return _positionOrigin; }
            set { SetProperty(ref _positionOrigin, value); }
        }
        #endregion
    }
}
