﻿//---------------------------------------------------------------------------------------------
// <copyright file="ViewSite.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Controls;

    /// <summary>
    /// Represents the main root view site view model used on the docking manager control.
    /// </summary>
    public class ViewSite : NotifyPropertyChangedBase, IDockingElement
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="ActiveDocument"/> property.
        /// </summary>
        private DocumentItem _activeDocument;

        /// <summary>
        /// The private field used for the <see cref="LastActiveDocumentPane"/> property.
        /// </summary>
        private DocumentPane _lastActiveDocumentPane;

        /// <summary>
        /// The private field used for the <see cref="RootGroup"/> property.
        /// </summary>
        private DockingGroup _rootGroup;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Prevents a default instance of the <see cref="ViewSite"/> class from being created.
        /// </summary>
        private ViewSite()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the active document within this view site.
        /// </summary>
        public DocumentItem ActiveDocument
        {
            get
            {
                return this._activeDocument;
            }

            internal set
            {
                if (this.SetProperty(ref this._activeDocument, value) && value != null)
                {
                    this.LastActiveDocumentPane = value.ParentPane as DocumentPane;
                }
            }
        }

        /// <summary>
        /// Gets a iterator around all of the document items currently opened inside this
        /// view site.
        /// </summary>
        public IEnumerable<DocumentItem> AllDocuments
        {
            get
            {
                List<DocumentItem> documents = new List<DocumentItem>();
                if (this.RootGroup != null)
                {
                    foreach (DocumentItem document in this.RootGroup.AllDocuments)
                    {
                        documents.Add(document);
                    }
                }

                return documents;
            }
        }

        /// <summary>
        /// Gets the first document pane in the logical order under this view site.
        /// </summary>
        public DocumentPane FirstDocumentPane
        {
            get
            {
                if (this._rootGroup == null)
                {
                    return null;
                }

                return this._rootGroup.FirstDocumentPane;
            }
        }

        /// <summary>
        /// Gets the document pane that was last activated.
        /// </summary>
        public DocumentPane LastActiveDocumentPane
        {
            get { return this._lastActiveDocumentPane; }
            private set { this.SetProperty(ref this._lastActiveDocumentPane, value); }
        }

        /// <summary>
        /// Gets a iterator around all of the document items currently opened inside this
        /// view site that contain modified data.
        /// </summary>
        public IEnumerable<DocumentItem> ModifiedDocuments
        {
            get
            {
                List<DocumentItem> documents = new List<DocumentItem>();
                foreach (DocumentItem document in this.AllDocuments)
                {
                    if (document.IsModified)
                    {
                        documents.Add(document);
                    }
                }

                return documents;
            }
        }

        /// <summary>
        /// Gets the parent docking element.
        /// </summary>
        public IDockingElement ParentElement
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the DockingGroup that is the root of the main docking site.
        /// </summary>
        public DockingGroup RootGroup
        {
            get { return this._rootGroup; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new instance of the <see cref="ViewSite"/> class to represent a main
        /// view site.
        /// </summary>
        /// <returns>
        /// A new instance of the <see cref="ViewSite"/> class.
        /// </returns>
        public static ViewSite CreateMainViewSite()
        {
            ViewSite site = new ViewSite();
            DockingGroup root = new DockingGroup(site);
            root.Orientation = Orientation.Horizontal;
            site._rootGroup = root;
            return site;
        }

        /// <summary>
        /// Performs logic due to a document being closed.
        /// </summary>
        /// <param name="document">
        /// The document being closed.
        /// </param>
        public void HandleDocumentClosing(DocumentItem document)
        {
            //// Makes sure if the document being closed is the active document to reset
            //// the reference to the active document.
            if (Object.ReferenceEquals(this.ActiveDocument, document))
            {
                this.ActiveDocument = null;
            }
        }
        #endregion
    } // RSG.Editor.Controls.Dock.ViewModel.ViewSite {Class}
} // RSG.Editor.Controls.Dock.ViewModel {Namespace}
