﻿//---------------------------------------------------------------------------------------------
// <copyright file="UppercaseConverter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Converters
{
    using System;
    using System.Globalization;

    /// <summary>
    /// Provides a base class for all multi value converters that convert 2 source objects
    /// into one target object.
    /// </summary>
    public class UppercaseConverter : ValueConverter<string, string>
    {
        #region Methods
        /// <summary>
        /// Converters the given value and returns the result.
        /// </summary>
        /// <param name="value">
        /// The original value to convert.
        /// </param>
        /// <param name="param">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A converted value.
        /// </returns>
        protected override string Convert(string value, object param, CultureInfo culture)
        {
            if (String.IsNullOrWhiteSpace(value))
            {
                return value;
            }

            return value.ToUpper(culture);
        }
        #endregion Methods
    } // RSG.Editor.Controls.Converters.UppercaseConverter {Class}
} // RSG.Editor.Controls.Converters {Namespace}
