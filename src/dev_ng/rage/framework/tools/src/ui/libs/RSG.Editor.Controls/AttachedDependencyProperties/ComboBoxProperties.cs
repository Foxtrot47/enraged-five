﻿//---------------------------------------------------------------------------------------------
// <copyright file="ComboBoxProperties.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.AttachedDependencyProperties
{
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Input;

    /// <summary>
    /// Contains attached properties that are related to the combo box.
    /// </summary>
    public static class ComboBoxProperties
    {
        #region Fields
        /// <summary>
        /// Identifies the IsReadOnly dependency property.
        /// </summary>
        public static readonly DependencyProperty IsReadOnlyProperty;

        /// <summary>
        /// Identifies the RefreshOnClose dependency property.
        /// </summary>
        public static readonly DependencyProperty RefreshOnCloseProperty;

        /// <summary>
        /// Identifies the RefreshOnOpen dependency property.
        /// </summary>
        public static readonly DependencyProperty RefreshOnOpenProperty;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="ComboBoxProperties"/> class.
        /// </summary>
        static ComboBoxProperties()
        {
            IsReadOnlyProperty =
                DependencyProperty.RegisterAttached(
                "IsReadOnly",
                typeof(bool),
                typeof(ComboBoxProperties),
                new PropertyMetadata(false, OnIsReadOnlyChanged));

            RefreshOnCloseProperty =
                DependencyProperty.RegisterAttached(
                "RefreshOnClose",
                typeof(bool),
                typeof(ComboBoxProperties),
                new PropertyMetadata(false, OnRefreshOnCloseChanged));

            RefreshOnOpenProperty =
                DependencyProperty.RegisterAttached(
                "RefreshOnOpen",
                typeof(bool),
                typeof(ComboBoxProperties),
                new PropertyMetadata(false, OnRefreshOnOpenChanged));
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Retrieves the IsReadOnly dependency property value attached to the specified
        /// object.
        /// </summary>
        /// <param name="element">
        /// The object whose attached IsReadOnly dependency property value will be
        /// returned.
        /// </param>
        /// <returns>
        /// The IsReadOnly dependency property value attached to the specified object.
        /// </returns>
        [AttachedPropertyBrowsableForChildrenAttribute(IncludeDescendants = true)]
        [AttachedPropertyBrowsableForType(typeof(ComboBox))]
        public static bool GetIsReadOnly(DependencyObject element)
        {
            return (bool)element.GetValue(IsReadOnlyProperty);
        }

        /// <summary>
        /// Retrieves the RefreshOnClose dependency property value attached to the specified
        /// object.
        /// </summary>
        /// <param name="element">
        /// The object whose attached RefreshOnClose dependency property value will be
        /// returned.
        /// </param>
        /// <returns>
        /// The RefreshOnClose dependency property value attached to the specified object.
        /// </returns>
        [AttachedPropertyBrowsableForChildrenAttribute(IncludeDescendants = true)]
        [AttachedPropertyBrowsableForType(typeof(ComboBox))]
        public static bool GetRefreshOnClose(DependencyObject element)
        {
            return (bool)element.GetValue(RefreshOnCloseProperty);
        }

        /// <summary>
        /// Retrieves the RefreshOnOpen dependency property value attached to the specified
        /// object.
        /// </summary>
        /// <param name="element">
        /// The object whose attached RefreshOnOpen dependency property value will be
        /// returned.
        /// </param>
        /// <returns>
        /// The RefreshOnOpen dependency property value attached to the specified object.
        /// </returns>
        [AttachedPropertyBrowsableForChildrenAttribute(IncludeDescendants = true)]
        [AttachedPropertyBrowsableForType(typeof(ComboBox))]
        public static bool GetRefreshOnOpen(DependencyObject element)
        {
            return (bool)element.GetValue(RefreshOnOpenProperty);
        }

        /// <summary>
        /// Sets the IsReadOnly dependency property value on the specified object to the
        /// specified value.
        /// </summary>
        /// <param name="element">
        /// The object whose attached IsReadOnly dependency property value will be set.
        /// </param>
        /// <param name="value">
        /// The value to set the attached IsReadOnly dependency property to on the specified
        /// object.
        /// </param>
        [AttachedPropertyBrowsableForChildrenAttribute(IncludeDescendants = true)]
        [AttachedPropertyBrowsableForType(typeof(ComboBox))]
        public static void SetIsReadOnly(DependencyObject element, bool value)
        {
            element.SetValue(IsReadOnlyProperty, value);
        }

        /// <summary>
        /// Sets the RefreshOnClose dependency property value on the specified object to
        /// the specified value.
        /// </summary>
        /// <param name="element">
        /// The object whose attached RefreshOnClose dependency property value will be
        /// set.
        /// </param>
        /// <param name="value">
        /// The value to set the attached RefreshOnClose dependency property to on the
        /// specified object.
        /// </param>
        [AttachedPropertyBrowsableForChildrenAttribute(IncludeDescendants = true)]
        [AttachedPropertyBrowsableForType(typeof(ComboBox))]
        public static void SetRefreshOnClose(DependencyObject element, bool value)
        {
            element.SetValue(RefreshOnCloseProperty, value);
        }

        /// <summary>
        /// Sets the RefreshOnOpen dependency property value on the specified object to
        /// the specified value.
        /// </summary>
        /// <param name="element">
        /// The object whose attached RefreshOnOpen dependency property value will be
        /// set.
        /// </param>
        /// <param name="value">
        /// The value to set the attached RefreshOnOpen dependency property to on the
        /// specified object.
        /// </param>
        [AttachedPropertyBrowsableForChildrenAttribute(IncludeDescendants = true)]
        [AttachedPropertyBrowsableForType(typeof(ComboBox))]
        public static void SetRefreshOnOpen(DependencyObject element, bool value)
        {
            element.SetValue(RefreshOnOpenProperty, value);
        }

        /// <summary>
        /// Called whenever a combo box that has been set to read only has a preview key down
        /// event. This event is handled so that the box can stay read only.
        /// </summary>
        /// <param name="sender">
        /// The combo box that this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.KeyEventArgs containing the event data.
        /// </param>
        private static void OnComboBoxPreviewKeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = true;
        }

        /// <summary>
        /// Called whenever a combo box that has been set to read only has a mouse wheel event.
        /// This event is handled so that the box can stay read only.
        /// </summary>
        /// <param name="sender">
        /// The combo box that this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.MouseWheelEventArgs containing the event data.
        /// </param>
        private static void OnComboBoxMouseWheel(object sender, MouseWheelEventArgs e)
        {
            e.Handled = true;
        }

        /// <summary>
        /// Called when a combo box drop down closes.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// Always EventArgs.Empty.
        /// </param>
        private static void OnDropDownStateChanged(object sender, EventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;
            if (comboBox == null)
            {
                return;
            }

            IEnumerable itemsSource = comboBox.ItemsSource;
            if (itemsSource == null)
            {
                return;
            }

            ICollectionView view = CollectionViewSource.GetDefaultView(itemsSource);
            if (view == null)
            {
                return;
            }

            view.Refresh();
        }

        /// <summary>
        /// Called whenever the IsReadOnly dependency property changes.
        /// </summary>
        /// <param name="d">
        /// The object whose IsReadOnly dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnIsReadOnlyChanged(
            DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ComboBox comboBox = d as ComboBox;
            if (comboBox == null)
            {
                return;
            }

            if ((bool)e.NewValue)
            {
                comboBox.PreviewMouseWheel += OnComboBoxMouseWheel;
                comboBox.PreviewKeyDown += OnComboBoxPreviewKeyDown;
            }
            else
            {
                comboBox.PreviewMouseWheel -= OnComboBoxMouseWheel;
                comboBox.PreviewKeyDown -= OnComboBoxPreviewKeyDown;
            }
        }

        /// <summary>
        /// Called whenever the RefreshOnClose dependency property changes.
        /// </summary>
        /// <param name="d">
        /// The object whose RefreshOnClose dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnRefreshOnCloseChanged(
            DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ComboBox comboBox = d as ComboBox;
            if (comboBox == null)
            {
                throw new NotSupportedException("RefreshOnClose only valid on Combo Boxes");
            }

            if ((bool)e.NewValue)
            {
                comboBox.DropDownClosed += OnDropDownStateChanged;
            }
            else
            {
                comboBox.DropDownClosed -= OnDropDownStateChanged;
            }
        }

        /// <summary>
        /// Called whenever the RefreshOnClose dependency property changes.
        /// </summary>
        /// <param name="d">
        /// The object whose RefreshOnClose dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnRefreshOnOpenChanged(
            DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ComboBox comboBox = d as ComboBox;
            if (comboBox == null)
            {
                throw new NotSupportedException("RefreshOnOpen only valid on Combo Boxes");
            }

            if ((bool)e.NewValue)
            {
                comboBox.DropDownOpened += OnDropDownStateChanged;
            }
            else
            {
                comboBox.DropDownOpened -= OnDropDownStateChanged;
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.AttachedDependencyProperties.ComboBoxProperties {Class}
} // RSG.Editor.Controls.AttachedDependencyProperties {Namespace}
