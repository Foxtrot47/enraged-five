﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using RSG.Base.Extensions;
using RSG.Editor.Controls;

namespace RSG.UniversalLog.View
{
    /// <summary>
    /// 
    /// </summary>
    [TemplatePart(Name = "PART_TextBlock", Type = typeof(TextBlock))]
    public class LogMessageTextBlock : RsHighlightTextBlock
    {
        #region Constants
        /// <summary>
        /// Regex for matching urls.
        /// </summary>
        private readonly Regex _urlRegex =
            new Regex(@"(http|https|ftp)\://[a-zA-Z0-9\-\.]+" +
                      @"\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?" +
                      @"([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*");
        #endregion // Constants

        #region Overridden Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void OnTextChanged()
        {
            if (this.TextBlock == null)
            {
                return;
            }

            this.TextBlock.Inlines.Clear();
            if (this.Text == null)
            {
                this.TextBlock.Text = this.Text;
                return;
            }

            // Try and match against the search and the highlight regex (if required).
            MatchCollection urlMatches = _urlRegex.Matches(this.Text);
            MatchCollection highlightMatches = null;
            if (this.HighlightExpression != null)
            {
                highlightMatches = this.HighlightExpression.Matches(this.Text);
            }
            
            // Check that we matched one of the regexes.
            if (urlMatches.Count == 0 && (highlightMatches == null || highlightMatches.Count == 0))
            {
                this.TextBlock.Text = this.Text;
            }
            else if (urlMatches.Count == 0)
            {
                // If we only have highlight matches, then let the base class take care of that.
                base.GenerateInlines(highlightMatches);
            }
            else if (highlightMatches == null || highlightMatches.Count == 0)
            {
                // We only have url matches.
                GenerateUrlInlines(urlMatches);
            }
            else
            {
                GenerateCombinedInlines(urlMatches, highlightMatches);
            }
        }

        /// <summary>
        /// Generates runs in the case where we only have url matches.
        /// </summary>
        /// <param name="matches"></param>
        private void GenerateUrlInlines(MatchCollection matches)
        {
            int currentIndex = 0;
            foreach (Match match in matches)
            {
                // Create a run up to the first character of the url.
                if (match.Index != 0)
                {
                    Run run = new Run();
                    run.Text = Text.Substring(currentIndex, match.Index - currentIndex);
                    TextBlock.Inlines.Add(run);
                }

                // Create a hyperlink for the regex match.
                try
                {
                    Hyperlink link = new Hyperlink();
                    link.Inlines.Add(new Run(match.Value));
                    link.NavigateUri = new Uri(match.Value);
                    link.RequestNavigate += (s, e) =>
                    {
                        Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
                        e.Handled = true;
                    };
                    TextBlock.Inlines.Add(link);
                }
                catch (System.UriFormatException)
                {
                    Run run = new Run(match.Value);
                    TextBlock.Inlines.Add(run);
                }

                // Update the current index.
                currentIndex = match.Index + match.Length;
            }

            // Check if we need to create an extra run from the end of the last match to the end of the line.
            Match lastMatch = matches[matches.Count - 1];
            if (currentIndex < Text.Length)
            {
                Run run = new Run();
                run.Text = Text.Substring(currentIndex);
                TextBlock.Inlines.Add(run);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="urlMatchCollection"></param>
        /// <param name="highlightMatchCollection"></param>
        private void GenerateCombinedInlines(MatchCollection urlMatchCollection, MatchCollection highlightMatchCollection)
        {
            IList<InlineInfo> infos = GenerateInlineInfos(urlMatchCollection, highlightMatchCollection);

            foreach (InlineInfo info in infos)
            {
                String text = Text.Substring(info.StartIndex, info.Length);

                // Create either a hyperlink or a simple text run for this string.
                Inline currentInline;
                if (info.Url != null)
                {
                    try
                    {
                        Hyperlink link = new Hyperlink();
                        link.Inlines.Add(new Run(text));
                        link.NavigateUri = new Uri(info.Url);
                        link.RequestNavigate += (s, e) =>
                        {
                            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
                            e.Handled = true;
                        };
                        currentInline = link;
                    }
                    catch (System.UriFormatException)
                    {
                        currentInline = new Run(text);
                    }
                }
                else
                {
                    currentInline = new Run(text);
                }

                // Set the colouring if so required next.
                if (info.Highlight)
                {
                    PropertyPath backgroundPath = new PropertyPath(HighlightBackgroundProperty);
                    currentInline.SetBinding(
                        TextElement.BackgroundProperty,
                        new Binding
                        {
                            Source = this,
                            Path = backgroundPath
                        });

                    PropertyPath foregroundPath = new PropertyPath(HighlightForegroundProperty);
                    currentInline.SetBinding(
                        TextElement.ForegroundProperty,
                        new Binding
                        {
                            Source = this,
                            Path = foregroundPath,
                            TargetNullValue = this.Foreground
                        });
                }

                // Finally add it to the list of inlines.
                this.TextBlock.Inlines.Add(currentInline);
            }
        }

        /// <summary>
        /// Combines the two match collections and generates a list of run information.
        /// </summary>
        /// <param name="urlMatches"></param>
        /// <param name="highlightMatches"></param>
        /// <returns></returns>
        private IList<InlineInfo> GenerateInlineInfos(MatchCollection urlMatches, MatchCollection highlightMatches)
        {
            IList<InlineInfo> inlineInfos = new List<InlineInfo>();

            // Build up the list of inline information for the urls.
            int currentIndex = 0;
            foreach (Match match in urlMatches)
            {
                // Create a run up to the first character of the url.
                if (match.Index != 0)
                {
                    inlineInfos.Add(
                        new InlineInfo
                        {
                            StartIndex = currentIndex,
                            EndIndex = match.Index
                        });
                }

                // Create a hyperlink for the regex match.
                inlineInfos.Add(
                    new InlineInfo
                    {
                        StartIndex = match.Index,
                        EndIndex = match.Index + match.Value.Length,
                        Url = match.Value
                    });

                // Update the current index.
                currentIndex = match.Index + match.Length;
            }

            // Check if we need to create an extra run from the end of the last match to the end of the line.
            Match lastMatch = urlMatches[urlMatches.Count - 1];
            if (currentIndex < Text.Length)
            {
                inlineInfos.Add(
                    new InlineInfo
                    {
                        StartIndex = currentIndex,
                        EndIndex = Text.Length
                    });
            }

            // Now go over the inline information we have gathered seeing if we need to split any of them up due to the highlighting.
            foreach (Match match in highlightMatches)
            {
                int matchStart = match.Index;
                int matchEnd = match.Index + match.Value.Length;

                IList<InlineInfo> infosToAdd = new List<InlineInfo>();
                foreach (InlineInfo info in inlineInfos)
                {
                    // Does the highlight encompass the entire info?
                    if (matchStart <= info.StartIndex && matchEnd >= info.EndIndex)
                    {
                        info.Highlight = true;
                    }
                    else if (matchStart >= info.StartIndex && matchEnd <= info.EndIndex)
                    {
                        if (matchStart > info.StartIndex)
                        {
                            InlineInfo newStart =
                                new InlineInfo
                                {
                                    StartIndex = info.StartIndex,
                                    EndIndex = matchStart,
                                    Highlight = false,
                                    Url = info.Url
                                };
                            infosToAdd.Add(newStart);
                        }

                        if (matchEnd < info.EndIndex)
                        {
                            InlineInfo newEnd =
                                new InlineInfo
                                {
                                    StartIndex = matchEnd,
                                    EndIndex = info.EndIndex,
                                    Highlight = false,
                                    Url = info.Url
                                };
                            infosToAdd.Add(newEnd);
                        }

                        // Change the existing inline into the highlighting one.
                        info.StartIndex = matchStart;
                        info.EndIndex = matchEnd;
                        info.Highlight = true;
                    }
                    else if (matchEnd > info.StartIndex && matchEnd < info.EndIndex)
                    {
                        InlineInfo newStart =
                            new InlineInfo
                            {
                                StartIndex = info.StartIndex,
                                EndIndex = matchEnd,
                                Highlight = true,
                                Url = info.Url
                            };
                        infosToAdd.Add(newStart);

                        info.StartIndex = matchEnd;
                    }
                    else if (matchStart < info.EndIndex && matchStart > info.StartIndex)
                    {
                        InlineInfo newEnd =
                            new InlineInfo
                            {
                                StartIndex = matchStart,
                                EndIndex = info.EndIndex,
                                Highlight = true,
                                Url = info.Url
                            };
                        infosToAdd.Add(newEnd);

                        info.EndIndex = matchStart;
                    }
                }

                inlineInfos.AddRange(infosToAdd);
            }

            // Make sure the inlines are ordered correctly.
            return inlineInfos.OrderBy(item => item.StartIndex).ToList();
        }

        /// <summary>
        /// Information
        /// </summary>
        private class InlineInfo
        {
            public int StartIndex { get; set; }
            public int EndIndex { get; set; }
            public int Length { get { return EndIndex - StartIndex; } }
            public String Url { get; set; }
            public bool Highlight { get; set; }
        }
        #endregion // Overridden Methods
    } // LogMessageTextBlock
}
