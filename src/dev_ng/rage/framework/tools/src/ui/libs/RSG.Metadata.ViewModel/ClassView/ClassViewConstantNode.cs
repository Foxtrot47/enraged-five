﻿//---------------------------------------------------------------------------------------------
// <copyright file="ClassViewConstantNode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.ClassView
{
    using RSG.Metadata.Model.Definitions;
    using RSG.Metadata.ViewModel.Project;

    /// <summary>
    /// The node inside the class view that represents a standard metadata project. This class
    /// cannot be inherited.
    /// </summary>
    public sealed class ClassViewConstantNode : ClassViewNodeBase
    {
        #region Fields
        /// <summary>
        /// The constant that this node is representing.
        /// </summary>
        private IConstant _constant;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ClassViewConstantNode"/> class.
        /// </summary>
        /// <param name="constant">
        /// The constant object this node is representing.
        /// </param>
        /// <param name="parent">
        /// The parent node.
        /// </param>
        public ClassViewConstantNode(IConstant constant, ClassViewNodeBase parent)
            : base(parent)
        {
            this.IsExpandable = false;
            this._constant = constant;
            this.Icon = Icons.ConstantNodeIcon;
            this.Text = constant.Name;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the priority value for this object.
        /// </summary>
        public override int Priority
        {
            get { return 3; }
        }
        #endregion Properties
    } // RSG.Metadata.ViewModel.ClassView.ClassViewConstantNode {Class}
} // RSG.Metadata.ViewModel.ClassView {Namespace}
