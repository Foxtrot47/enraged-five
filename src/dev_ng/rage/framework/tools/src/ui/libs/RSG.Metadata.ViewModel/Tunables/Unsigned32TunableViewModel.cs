﻿//---------------------------------------------------------------------------------------------
// <copyright file="Unsigned32TunableViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Tunables
{
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// The view model for a <see cref="Unsigned32Tunable"/> model object.
    /// </summary>
    public class Unsigned32TunableViewModel : TunableViewModelBase<Unsigned32Tunable>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Unsigned32TunableViewModel"/> class.
        /// </summary>
        /// <param name="tunable">
        /// The unsigned 32-bit integer tunable that this view model is representing.
        /// </param>
        /// <param name="parent">
        /// The parent tunable view model for this view model.
        /// </param>
        public Unsigned32TunableViewModel(Unsigned32Tunable tunable, ITunableViewModel parent)
            : base(tunable, parent)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the value currently assigned to the unsigned 32-bit integer tunable.
        /// </summary>
        public uint Value
        {
            get { return this.Model.Value; }
            set { this.Model.Value = value; }
        }
        #endregion Properties
    } // RSG.Metadata.ViewModel.Tunables.Unsigned32TunableViewModel {Class}
} // RSG.Metadata.ViewModel.Tunables {Namespace}
