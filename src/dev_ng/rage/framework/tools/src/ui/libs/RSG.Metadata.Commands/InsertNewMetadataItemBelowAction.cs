﻿//---------------------------------------------------------------------------------------------
// <copyright file="InsertNewMetadataItemBelowAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Commands
{
    using System.Linq;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Definitions;
    using RSG.Metadata.Model.Tunables;
    using RSG.Metadata.ViewModel.Tunables;

    /// <summary>
    /// Implements the logic for the <see cref="MetadataCommands.AddNewMetadataItem"/> routed
    /// command.
    /// </summary>
    public class InsertNewMetadataItemBelowAction
        : ButtonAction<MetadataCommandArgs, IStructure>
    {
        #region Fields
        /// <summary>
        /// The private reference to the command definition for this action.
        /// </summary>
        private CommandDefinition _definition;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="InsertNewMetadataItemBelowAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        /// <param name="definition">
        /// The definition for the add new metadata item command.
        /// </param>
        public InsertNewMetadataItemBelowAction(
            MetadataCommandArgsResolver resolver, CommandDefinition definition)
            : base(new ParameterResolverDelegate<MetadataCommandArgs>(resolver))
        {
            this._definition = definition;
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="structure">
        /// The command parameters that contains the array arguments for this command.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(MetadataCommandArgs args, IStructure structure)
        {
            ITunableViewModel selected = args.SelectedTunables.FirstOrDefault();
            if (selected == null)
            {
                return false;
            }

            IDynamicTunableParent parent = selected.Model.Parent as IDynamicTunableParent;
            if (parent == null)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="structure">
        /// The command parameters that contains the array arguments for this command.
        /// </param>
        public override void Execute(MetadataCommandArgs args, IStructure structure)
        {
            ITunableViewModel selected = args.SelectedTunables.FirstOrDefault();
            if (selected == null)
            {
                return;
            }

            ITunable tunable = selected.Model as ITunable;
            if (tunable == null)
            {
                return;
            }

            IDynamicTunableParent parent = tunable.Parent as IDynamicTunableParent;
            if (parent == null)
            {
                return;
            }

            int index = parent.IndexOf(tunable);
            if (index == -1)
            {
                return;
            }

            using (new UndoRedoBatch(args.UndoEngine))
            {
                ITunable newTunable = parent.InsertNewItem(index + 1);
                PointerTunable pointerTunable = newTunable as PointerTunable;
                if (pointerTunable != null)
                {
                    pointerTunable.ChangePointerType(structure);
                }

                if (args.TunableSelectionDelegate != null && selected.Parent != null)
                {
                    args.TunableSelectionDelegate(selected.Parent[newTunable]);
                }
            }
        }

        /// <summary>
        /// Returns the command definition that is associated with this action. This definition
        /// was past into the constructor of this action as external classes need to update it.
        /// </summary>
        /// <param name="command">
        /// The command the definition is being used for.
        /// </param>
        /// <returns>
        /// The command definition that is associated with this action.
        /// </returns>
        protected override CommandDefinition CreateDefinition(RockstarRoutedCommand command)
        {
            return this._definition;
        }
        #endregion Methods
    } // RSG.Metadata.Commands.InsertNewMetadataItemBelowAction {Class}
} // RSG.Metadata.Commands {Namespace}
