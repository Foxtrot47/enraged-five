﻿//---------------------------------------------------------------------------------------------
// <copyright file="PointerTunableViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Tunables
{
    using System.ComponentModel;
    using RSG.Editor.View;
    using RSG.Metadata.Model.Definitions.Members;
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// The view model for a <see cref="PointerTunable"/> model object.
    /// </summary>
    public class PointerTunableViewModel
        : TunableViewModelBase<PointerTunable>, ITunableViewModelParent
    {
        #region Fields
        /// <summary>
        /// A private value indicating whether the is expandable value has been initialised.
        /// </summary>
        private bool _initialised;

        /// <summary>
        /// The private field used for the <see cref="IsExpandable"/> property.
        /// </summary>
        private bool _isExpandable;

        /// <summary>
        /// The private field used for the <see cref="StructureKeyValue"/> property.
        /// </summary>
        private string _structureKeyValue;

        /// <summary>
        /// The private field used for the <see cref="Tunables"/> property.
        /// </summary>
        private TunableCollection _tunables;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="PointerTunableViewModel"/> class.
        /// </summary>
        /// <param name="tunable">
        /// The pointer tunable that this view model is representing.
        /// </param>
        /// <param name="parent">
        /// The parent tunable view model for this view model.
        /// </param>
        public PointerTunableViewModel(PointerTunable tunable, ITunableViewModel parent)
            : base(tunable, parent)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether this tunable can be expanded.
        /// </summary>
        public override bool IsExpandable
        {
            get
            {
                if (!this._initialised)
                {
                    this.Initialise();
                }

                return this._isExpandable;
            }
        }

        /// <summary>
        /// Gets a value indicating whether or not, based on the pointer policy the tunable
        /// owns the object that it points to or not.
        /// </summary>
        public bool OwnsObject
        {
            get { return this.Model.PointerMember.OwnsObject; }
        }

        /// <summary>
        /// Gets or sets the named reference for this pointer.
        /// </summary>
        public string Reference
        {
            get { return this.Model.Reference; }
            set { this.Model.Reference = value; }
        }

        /// <summary>
        /// Gets the value of the structure key for this tunable.
        /// </summary>
        public string StructureKeyValue
        {
            get { return this._structureKeyValue; }
            private set { this.SetProperty(ref this._structureKeyValue, value); }
        }

        /// <summary>
        /// Gets the collection of child tunables for this structure.
        /// </summary>
        public IReadOnlyViewModelCollection<ITunableViewModel> Tunables
        {
            get
            {
                if (this._tunables == null)
                {
                    this._tunables = new TunableCollection(this.Model.Tunables, this);
                }

                return this._tunables;
            }
        }

        /// <summary>
        /// Gets the value of the type for this tunable view model that should be shown to the
        /// user.
        /// </summary>
        public override string TypeValue
        {
            get
            {
                if (this.OwnsObject)
                {
                    string type = null;
                    if (this.Model.OwnedStructure == null)
                    {
                        type = this.Model.PointerMember.ReferencedStructure.ShortDataType;
                    }
                    else
                    {
                        type = this.Model.OwnedStructure.ShortDataType;
                    }

                    return type + "*";
                }
                else
                {
                    switch (this.Model.PointerMember.Policy)
                    {
                        case PointerPolicy.External:
                            return "external_named pointer";
                        case PointerPolicy.LazyLink:
                            return "lazylink pointer";
                        case PointerPolicy.Link:
                            return "link pointer";
                        default:
                            return this.Model.PointerMember.Policy + " pointer";
                    }
                }
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Makes sure the <see cref="TypeValue"/> property is registered to have changed when
        /// the owned structure changes.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.ComponentModel.PropertyChangedEventArgs containing the event data.
        /// </param>
        protected override void OnModelPropertyChanged(object s, PropertyChangedEventArgs e)
        {
            base.OnModelPropertyChanged(s, e);
            if (e.PropertyName == "OwnedStructure")
            {
                this.NotifyPropertyChanged("TypeValue");
                this.UpdateIsExpandable();
            }
        }

        /// <summary>
        /// Finds the tunable object whose value is being presented as the key to the
        /// structure.
        /// </summary>
        /// <returns>
        /// The tunable object whose value is being presented as the key to the structure.
        /// </returns>
        private ITunable FindStructureKeyTunable()
        {
            foreach (ITunable tunable in this.Model.Tunables)
            {
                if (tunable.Member.IsStructureKey)
                {
                    return tunable;
                }
            }

            return null;
        }

        /// <summary>
        /// Initialises the is expandable value for the first time.
        /// </summary>
        private void Initialise()
        {
            if (this._initialised)
            {
                return;
            }

            this._initialised = true;
            ITunable key = this.FindStructureKeyTunable();
            if (key != null)
            {
                this.UpdateStructureKey(key, new PropertyChangedEventArgs(null));
                key.PropertyChanged += this.UpdateStructureKey;
            }

            this.UpdateIsExpandable();
        }

        /// <summary>
        /// Updates the is expandable value for this tunable.
        /// </summary>
        private void UpdateIsExpandable()
        {
            bool value = this.OwnsObject && this.Model.OwnedStructure != null;
            this.SetProperty(ref this._isExpandable, value, "IsExpandable");
        }

        /// <summary>
        /// Updates the structure key value due to a change in the tunables data.
        /// </summary>
        /// <param name="s">
        /// The tunable whose property value changed.
        /// </param>
        /// <param name="e">
        /// The System.ComponentModel.PropertyChangedEventArgs containing the event data.
        /// </param>
        private void UpdateStructureKey(object s, PropertyChangedEventArgs e)
        {
            ITunable tunable = (ITunable)s;
            StringTunable stringTunable = tunable as StringTunable;
            if (stringTunable == null)
            {
                return;
            }

            this.StructureKeyValue = stringTunable.Value;
        }
        #endregion Methods
    } // RSG.Metadata.ViewModel.Tunables.PointerTunableViewModel {Class}
} // RSG.Metadata.ViewModel.Tunables {Namespace}
