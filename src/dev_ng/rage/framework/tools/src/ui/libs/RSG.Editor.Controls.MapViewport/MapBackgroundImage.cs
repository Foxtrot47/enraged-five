﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using RSG.Editor.Controls.MapViewport.OverlayComponents;

namespace RSG.Editor.Controls.MapViewport
{
    /// <summary>
    /// Contains the image source and bounds for a map that will be displayed as the main
    /// background image in the <see cref="MapViewport"/>.
    /// </summary>
    public class MapBackgroundImage : Image
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="ThumbSource"/> property.
        /// </summary>
        private ImageSource _thumbSource;

        /// <summary>
        /// Private field for the <see cref="DisplayName"/> property.
        /// </summary>
        private string _displayName;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MapBackgroundImage"/> class using
        /// the specified world extents, image source, thumb source and display name.
        /// </summary>
        /// <param name="worldExtents">The map image bounds.</param>
        /// <param name="imageSource">The source of the map image.</param>
        /// <param name="thumbSource">The source of the map thumbnail image.</param>
        /// <param name="displayName">The name to display in the UI for this image.</param>
        public MapBackgroundImage(Rect worldExtents, ImageSource imageSource, ImageSource thumbSource, string displayName)
            : base(worldExtents, imageSource)
        {
            _thumbSource = thumbSource;
            _displayName = displayName;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="MapBackgroundImage"/> class using
        /// the provided xml element.
        /// </summary>
        /// <param name="elem"></param>
        public MapBackgroundImage(XElement elem)
        {
            XElement displayNameElem = elem.Element("displayname");
            XElement sourceElem = elem.Element("source");
            XElement thumbElem = elem.Element("thumb");
            XElement boundsElem = elem.Element("bounds");

            XElement lowerLeftElem = boundsElem.Element("lowerleft");
            XElement upperRightElem = boundsElem.Element("upperright");

            XAttribute lowerLeftXAtt = lowerLeftElem.Attribute("x");
            XAttribute lowerLeftYAtt = lowerLeftElem.Attribute("y");
            XAttribute upperRightXAtt = upperRightElem.Attribute("x");
            XAttribute upperRightYAtt = upperRightElem.Attribute("y");

            double lowerLeftX = Double.Parse(lowerLeftXAtt.Value);
            double lowerLeftY = Double.Parse(lowerLeftYAtt.Value);
            double upperRightX = Double.Parse(upperRightXAtt.Value);
            double upperRightY = Double.Parse(upperRightYAtt.Value);
            
            // Set up the components of the MapImage.
            WorldExtents = new Rect(
                new Point(lowerLeftX, lowerLeftY),
                new Point(upperRightX, upperRightY));

            BitmapImage mainImageSource = new BitmapImage(new Uri(sourceElem.Value, UriKind.Relative));
            mainImageSource.Freeze();
            ImageSource = mainImageSource;

            BitmapImage thumbImageSource = new BitmapImage(new Uri(thumbElem.Value, UriKind.Relative));
            thumbImageSource.Freeze();
            _thumbSource = thumbImageSource;

            _displayName = displayNameElem.Value;
        }
        #endregion

        #region Properties
        /// <summary>
        /// The source of the image to use when displaying this map in the map selection view.
        /// </summary>
        public ImageSource ThumbSource
        {
            get { return _thumbSource; }
            set { SetProperty(ref _thumbSource, value); }
        }

        /// <summary>
        /// The name to display in the map viewport foreground for this image.
        /// </summary>
        public string DisplayName
        {
            get { return _displayName; }
            set { SetProperty(ref _displayName, value); }
        }
        #endregion
    }
}
