﻿//---------------------------------------------------------------------------------------------
// <copyright file="ICommandWithParameter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    /// <summary>
    /// When implemented represents a command definition class that contains a command
    /// parameter property.
    /// </summary>
    public interface ICommandWithParameter
    {
        #region Properties
        /// <summary>
        /// Gets the parameter that is routed through the element tree with the command.
        /// </summary>
        object CommandParameter { get; }
        #endregion Properties
    } // RSG.Editor.ICommandWithParameter {Interface}
} // RSG.Editor {Namespace}
