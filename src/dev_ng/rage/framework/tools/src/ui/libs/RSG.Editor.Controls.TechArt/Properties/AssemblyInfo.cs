﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssemblyInfo.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Markup;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("RSG.Editor.Controls.TechArt.dll")]
[assembly: AssemblyDescription("RSG.Editor.Controls.TechArt.dll")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Rockstar Games")]
[assembly: AssemblyProduct("Rockstar Editor Framework")]
[assembly: AssemblyCopyright("© Rockstar Games. All rights reserved.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: CLSCompliant(false)]

[assembly: XmlnsDefinition(
    "http://schemas.rockstargames.com/2013/xaml/editor",
    "RSG.Editor.Controls.TechArt")]

[assembly: XmlnsPrefix("http://schemas.rockstargames.com/2013/xaml/editor", "rsg")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

[assembly: ThemeInfo(
    ResourceDictionaryLocation.None, // where theme specific resource dictionaries are located
    // (used if a resource is not found in the page,
    // or application resource dictionaries)
    ResourceDictionaryLocation.SourceAssembly // where the generic resource dictionary is
    // located (used if a resource is not found in the page,
    // app, or any theme specific resource dictionaries)
)]

// Version information for an assembly consists of the following four values:
//
// Major Version
// Minor Version
// Build Number
// Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: GuidAttribute("EF060107-364E-4849-A21A-F40E1EAB3AE5")]
[assembly: NeutralResourcesLanguageAttribute("en-GB")]
