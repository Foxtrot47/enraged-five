﻿//---------------------------------------------------------------------------------------------
// <copyright file="DockingCommands.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock
{
    using System;
    using System.ComponentModel;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media.Imaging;
    using RSG.Editor;
    using RSG.Editor.Controls.Dock.ViewModel;
    using RSG.Editor.SharedCommands;

    /// <summary>
    /// Defines commands used on the core docking control. This class cannot be inherited.
    /// </summary>
    public sealed class DockingCommands
    {
        #region Fields
        /// <summary>
        /// The private cache of command objects.
        /// </summary>
        private static RockstarRoutedCommand[] _internalCommands = InitialiseInternalStorage();

        /// <summary>
        /// The private string table resource manager used for getting the command related
        /// strings and objects.
        /// </summary>
        private static CommandResourceManager _resourceManager = InitialiseResourceManager();
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="DockingCommands"/> class.
        /// </summary>
        static DockingCommands()
        {
            CommandManager.RegisterClassCommandBinding(
                typeof(UIElement),
                new CommandBinding(
                    DockingCommands.HideWindow,
                    OnHideWindow,
                    CanHideWindow));
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="DockingCommands"/> class from being
        /// created.
        /// </summary>
        private DockingCommands()
        {
        }
        #endregion Constructors

        #region Enumerations
        /// <summary>
        /// Defines all of the command identifiers for the different core Rockstar commands.
        /// </summary>
        private enum Id : byte
        {
            /// <summary>
            /// Used to identifier the <see cref="DockingCommands.CopyFullPath"/> routed
            /// command.
            /// </summary>
            CopyFullPath,

            /// <summary>
            /// Used to identifier the <see cref="DockingCommands.CloseAll"/> routed
            /// command.
            /// </summary>
            CloseAll,

            /// <summary>
            /// Used to identifier the <see cref="DockingCommands.CloseAllButItem"/> routed
            /// command.
            /// </summary>
            CloseAllButItem,

            /// <summary>
            /// Used to identifier the <see cref="DockingCommands.CloseItem"/> routed command.
            /// </summary>
            CloseItem,

            /// <summary>
            /// Used to identifier the <see cref="DockingCommands.HideWindow"/> routed command.
            /// </summary>
            HideWindow,

            /// <summary>
            /// Used to identifier the <see cref="DockingCommands.OpenContainingFolder"/>
            /// routed command.
            /// </summary>
            OpenContainingFolder,

            /// <summary>
            /// Used to identifier the <see cref="DockingCommands.MoveItemLeft"/> routed
            /// command.
            /// </summary>
            MoveItemLeft,

            /// <summary>
            /// Used to identifier the <see cref="DockingCommands.MoveItemRight"/> routed
            /// command.
            /// </summary>
            MoveItemRight,

            /// <summary>
            /// Used to identifier the <see cref="DockingCommands.ShiftLeft"/> routed
            /// command.
            /// </summary>
            ShiftLeft,

            /// <summary>
            /// Used to identifier the <see cref="DockingCommands.ShiftRight"/> routed
            /// command.
            /// </summary>
            ShiftRight,
        } // DockingCommands.Id {Enum}
        #endregion Enumerations

        #region Properties
        /// <summary>
        /// Gets the value that represents the Copy Full Path command.
        /// </summary>
        public static RockstarRoutedCommand CopyFullPath
        {
            get { return EnsureCommandExists(Id.CopyFullPath); }
        }

        /// <summary>
        /// Gets the value that represents the Open Containing Folder command.
        /// </summary>
        public static RockstarRoutedCommand OpenContainingFolder
        {
            get { return EnsureCommandExists(Id.OpenContainingFolder); }
        }

        /// <summary>
        /// Gets the value that represents the Close All command.
        /// </summary>
        public static RockstarRoutedCommand CloseAll
        {
            get { return EnsureCommandExists(Id.CloseAll, Icons.CloseAllDocuments); }
        }

        /// <summary>
        /// Gets the value that represents the Close All But Item command.
        /// </summary>
        public static RockstarRoutedCommand CloseAllButItem
        {
            get { return EnsureCommandExists(Id.CloseAllButItem); }
        }

        /// <summary>
        /// Gets the value that represents the Close Item command.
        /// </summary>
        public static RockstarRoutedCommand CloseItem
        {
            get { return EnsureCommandExists(Id.CloseItem); }
        }

        /// <summary>
        /// Gets the value that represents the Hide Window command.
        /// </summary>
        public static RockstarRoutedCommand HideWindow
        {
            get { return EnsureCommandExists(Id.HideWindow); }
        }

        /// <summary>
        /// Gets the value that represents the Shift Left command.
        /// </summary>
        public static RockstarRoutedCommand ShiftLeft
        {
            get { return EnsureCommandExists(Id.ShiftLeft); }
        }

        /// <summary>
        /// Gets the value that represents the Shift Right command.
        /// </summary>
        public static RockstarRoutedCommand ShiftRight
        {
            get { return EnsureCommandExists(Id.ShiftRight); }
        }

        /// <summary>
        /// Gets the value that represents the Move Item Left command.
        /// </summary>
        public static RockstarRoutedCommand MoveItemLeft
        {
            get { return EnsureCommandExists(Id.MoveItemLeft); }
        }

        /// <summary>
        /// Gets the value that represents the Move Item Right command.
        /// </summary>
        public static RockstarRoutedCommand MoveItemRight
        {
            get { return EnsureCommandExists(Id.MoveItemRight); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="RSG.Editor.RockstarRoutedCommand"/> object that is
        /// associated with the specified identifier.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command to create.
        /// </param>
        /// <param name="icon">
        /// The icon that the created command should be using.
        /// </param>
        /// <returns>
        /// The newly created <see cref="RSG.Editor.RockstarRoutedCommand"/> object.
        /// </returns>
        private static RockstarRoutedCommand CreateCommand(string id, BitmapSource icon)
        {
            string name = _resourceManager.GetName(id);
            InputGestureCollection gestures = _resourceManager.GetGestures(id);
            string category = _resourceManager.GetCategory(id);
            string description = _resourceManager.GetDescription(id);

            return new RockstarRoutedCommand(
                name, typeof(DockingCommands), gestures, category, description, icon);
        }

        /// <summary>
        /// Makes sure the internal value for the command associated with the specified
        /// identifier exists and returns it.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command that should be created and returned.
        /// </param>
        /// <returns>
        /// The <see cref="RSG.Editor.RockstarRoutedCommand"/> object associated with the
        /// specified identifier.
        /// </returns>
        private static RockstarRoutedCommand EnsureCommandExists(Id id)
        {
            return EnsureCommandExists(id, null);
        }

        /// <summary>
        /// Makes sure the internal value for the command associated with the specified
        /// identifier exists and returns it.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command that should be created and returned.
        /// </param>
        /// <param name="icon">
        /// The icon that the command should be using.
        /// </param>
        /// <returns>
        /// The <see cref="RSG.Editor.RockstarRoutedCommand"/> object associated with the
        /// specified identifier.
        /// </returns>
        private static RockstarRoutedCommand EnsureCommandExists(Id id, BitmapSource icon)
        {
            int commandIndex = (int)id;
            if (commandIndex < -1 || commandIndex >= _internalCommands.Length)
            {
                return null;
            }

            RockstarRoutedCommand command = _internalCommands[commandIndex];
            if (command == null)
            {
                lock (_internalCommands)
                {
                    if (command == null)
                    {
                        command = CreateCommand(id.ToString(), icon);
                        _internalCommands[commandIndex] = command;
                    }
                }
            }

            return command;
        }

        /// <summary>
        /// Initialises the array used to store the
        /// <see cref="RSG.Editor.RockstarRoutedCommand"/> objects that make up the core
        /// commands for a Rockstar application.
        /// </summary>
        /// <returns>
        /// The array used to store the internal command objects.
        /// </returns>
        private static RockstarRoutedCommand[] InitialiseInternalStorage()
        {
            return new RockstarRoutedCommand[Enum.GetValues(typeof(Id)).Length];
        }

        /// <summary>
        /// Initialises the resource manager to use to retrieve command related strings and
        /// objects from a resource data file.
        /// </summary>
        /// <returns>
        /// The resource manager to use with this class.
        /// </returns>
        private static CommandResourceManager InitialiseResourceManager()
        {
            Type type = typeof(DockingCommands);
            string baseName = type.Namespace + ".Resources.CommandStringTable";
            return new CommandResourceManager(baseName, type.Assembly);
        }

        /// <summary>
        /// Determines whether the <see cref="HideWindow"/> routed UI command can be executed.
        /// </summary>
        /// <param name="s">
        /// The object where the event handler is attached.
        /// </param>
        /// <param name="args">
        /// The System.Windows.Input.CanExecuteRoutedEventArgs data for the event.
        /// </param>
        private static void CanHideWindow(object s, CanExecuteRoutedEventArgs args)
        {
            IDockingPaneItem viewModel = args.Parameter as IDockingPaneItem;
            if (viewModel == null)
            {
                args.CanExecute = true;
                return;
            }

            args.CanExecute = viewModel.CanHide;
        }

        /// <summary>
        /// Get called once the <see cref="HideWindow"/> routed UI command gets executed.
        /// </summary>
        /// <param name="s">
        /// The object where the event handler is attached.
        /// </param>
        /// <param name="args">
        /// The System.Windows.Input.ExecutedRoutedEventArgs data for the event.
        /// </param>
        private static void OnHideWindow(object s, ExecutedRoutedEventArgs args)
        {
            IDockingPaneItem viewModel = args.Parameter as IDockingPaneItem;
            if (viewModel != null)
            {
                CancelEventArgs cancelArgs = new CancelEventArgs(false);
                viewModel.OnClosing(cancelArgs);
                if (cancelArgs != null && cancelArgs.Cancel)
                {
                    return;
                }

                
                return;
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.Dock.DockingCommands {Class}
} // RSG.Editor.Controls.Dock {Namespace}
