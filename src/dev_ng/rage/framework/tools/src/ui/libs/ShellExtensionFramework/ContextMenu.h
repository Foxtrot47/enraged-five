#ifndef SEF_CONTEXTMENU_H
#define SEF_CONTEXTMENU_H

#if defined _MSC_VER && _MSC_VER >= 1300
#pragma once
#endif

#include "ShellExtensionFramework.h"

//Here's what we need for the shell interfaces:
#include <shlobj.h>
#include <comdef.h>

// Windows headers
#include <atlbase.h>
#include <atlcom.h>
#include <Windows.h>

BEGIN_SHELL_EXTENSION_FRAMEWORK_NS

/**
	\brief Abstract Shell context-menu base class.
	This base class tries to abstract some of the context-menu complexities away from
	the calling class; simply override the HandleFilesContext and HandleDirectoryContext.

	Note: this does not support ownerdrawn items (only simple text menuitems).
 */
template <class T, const CLSID* pclsid = &CLSID_NULL> 
class ATL_NO_VTABLE CContextMenu :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<T, pclsid>,
	public IShellExtInit,
	public IContextMenu3,
	public IObjectWithSiteImpl<T>
{
public:
	CContextMenu( ) { }
	virtual ~CContextMenu( ) { }

protected:
#ifdef _UNICODE
	typedef std::wstring STL_STRING;
#else
#error _UNICODE must be defined.
#endif // _UNICODE
	typedef std::list<STL_STRING> string_list;

	typedef struct _TDirectoryContextEventArgs
	{
		STL_STRING directory;
	} TDirectoryContextEventArgs;

	typedef struct _TFilesContextEventArgs
	{
		string_list files;
	} TFilesContextEventArgs;

	/**
		\brief Method for handling file(s) click.
	 */
	virtual bool HandleFilesContext( const TFilesContextEventArgs& e ) const
	{
		return (false);
	}
	
	/**
		\brief Method for handling directory click.
	 */
	virtual bool HandleDirectoryContext( const TDirectoryContextEventArgs& e ) const
	{
		return (false);
	}
};

END_SHELL_EXTENSION_FRAMEWORK_NS

#endif // SEF_CONTEXTMENU_H
