﻿// --------------------------------------------------------------------------------------------
// <copyright file="RsSelectFolderDialog.cs" company="Rockstar">
//     Copyright © Rockstar Games 2012. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Windows;
    using System.Windows.Interop;
    using RSG.Interop.Microsoft.Windows;

    /// <summary>
    /// Represents a file open win32 dialog window that allows the user to select a folder
    /// instead of a file.
    /// </summary>
    public class RsSelectFolderDialog
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="AddExtension"/> property.
        /// </summary>
        private bool _addExtension;

        /// <summary>
        /// The private field used for the <see cref="AddToMruList"/> property.
        /// </summary>
        private bool _addToMruList = true;

        /// <summary>
        /// A private value indicating whether the native dialog window was cancelled.
        /// </summary>
        private bool? _cancelled;

        /// <summary>
        /// The private field used for the <see cref="CheckFileExists"/> property.
        /// </summary>
        private bool _checkFileExists;

        /// <summary>
        /// The private field used for the <see cref="CheckPathExists"/> property.
        /// </summary>
        private bool _checkPathExists;

        /// <summary>
        /// The private field used for the <see cref="CheckValidNames"/> property.
        /// </summary>
        private bool _checkValidNames;

        /// <summary>
        /// The private field used for the <see cref="DereferenceLinks"/> property.
        /// </summary>
        private bool _dereferenceLinks;

        /// <summary>
        /// The private field used for the <see cref="FileName"/> property.
        /// </summary>
        private string _fileName;

        /// <summary>
        /// A collection of filenames that are set to the folders that the user selects.
        /// </summary>
        private Collection<string> _filenames;

        /// <summary>
        /// The private field used for the <see cref="HidePinnedPlaces"/> property.
        /// </summary>
        private bool _hidePinnedPlaces;

        /// <summary>
        /// The private field used for the <see cref="InitialDirectory"/> property.
        /// </summary>
        private string _initialDirectory;

        /// <summary>
        /// A private reference to the IFileDialog object being shown.
        /// </summary>
        private IFileDialog _nativeDialog;

        /// <summary>
        /// A private value indicating whether the native dialog window is currently showing
        /// to the user.
        /// </summary>
        private bool _nativeDialogShowing;

        /// <summary>
        /// The private reference to the native file open dialog window that is being shown.
        /// </summary>
        private INativeFileOpenDialog _openDialogCoClass;

        /// <summary>
        /// A private reference to the window that owns this one.
        /// </summary>
        private Window _parentWindow;

        /// <summary>
        /// The identifier used to control the persistence of this folder dialog.
        /// </summary>
        private Guid _persistence;

        /// <summary>
        /// The private field used for the <see cref="ReadOnlyChecked"/> property.
        /// </summary>
        private bool _readOnlyChecked;

        /// <summary>
        /// The private field used for the <see cref="RestoreDirectory"/> property.
        /// </summary>
        private bool _restoreDirectory;

        /// <summary>
        /// The private field used for the <see cref="ShowHiddenItems"/> property.
        /// </summary>
        private bool _showHiddenItems;

        /// <summary>
        /// The private field used for the <see cref="Title"/> property.
        /// </summary>
        private string _title;

        /// <summary>
        /// The private field used for the <see cref="MultiSelect"/> property.
        /// </summary>
        private bool _multiSelect;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="RsSelectFolderDialog"/> class.
        /// </summary>
        /// <param name="persistence">
        /// The identifier to use to control the persistence of this dialog.
        /// </param>
        public RsSelectFolderDialog(Guid persistence)
        {
            this._filenames = new Collection<string>();
            this._persistence = persistence;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether a file dialog automatically adds an
        /// extension to a file name if the user omits an extension.
        /// </summary>
        public bool AddExtension
        {
            get { return this._addExtension; }
            set { this._addExtension = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the selected item is added to the recent
        /// documents list.
        /// </summary>
        public bool AddToMruList
        {
            get
            {
                return this._addToMruList;
            }

            set
            {
                this.ThrowIfDialogShowing();
                this._addToMruList = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a file dialog displays a warning if the
        /// user specifies a file name that does not exist.
        /// </summary>
        /// <exception cref="System.NotSupportedException">
        /// Thrown if set after the dialog has been shown.
        /// </exception>
        public bool CheckFileExists
        {
            get
            {
                return this._checkFileExists;
            }

            set
            {
                this.ThrowIfDialogShowing();
                this._checkFileExists = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether warnings are displayed if the user
        /// types invalid paths and file names.
        /// </summary>
        /// <exception cref="System.NotSupportedException">
        /// Thrown if set after the dialog has been shown.
        /// </exception>
        public bool CheckPathExists
        {
            get
            {
                return this._checkPathExists;
            }

            set
            {
                this.ThrowIfDialogShowing();
                this._checkPathExists = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a file dialog displays a warning if the
        /// user specifies a location that cannot be opening by the application for reasons
        /// including sharing violations and access denied errors.
        /// </summary>
        public bool CheckValidNames
        {
            get
            {
                return this._checkValidNames;
            }

            set
            {
                this.ThrowIfDialogShowing();
                this._checkValidNames = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a file dialog returns either the location
        /// of the file referenced by a shortcut or the location of the shortcut file.
        /// </summary>
        public bool DereferenceLinks
        {
            get
            {
                return this._dereferenceLinks;
            }

            set
            {
                this.ThrowIfDialogShowing();
                this._dereferenceLinks = value;
            }
        }

        /// <summary>
        /// Gets or sets a string containing the full path of the file selected in a file
        /// dialog.
        /// </summary>
        public string FileName
        {
            get
            {
                if (this._filenames.Count > 0)
                {
                    this._fileName = this._filenames[0];
                }

                return this._fileName;
            }

            set
            {
                this._fileName = value;
            }
        }

        /// <summary>
        /// Gets an array that contains one file name for each selected file.
        /// </summary>
        public string[] FileNames
        {
            get { return this._filenames.ToArray(); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the items in the view's navigation pane are
        /// shown to the user or not.
        /// </summary>
        public bool HidePinnedPlaces
        {
            get
            {
                return this._hidePinnedPlaces;
            }

            set
            {
                this.ThrowIfDialogShowing();
                this._hidePinnedPlaces = value;
            }
        }

        /// <summary>
        /// Gets or sets the initial directory that is displayed by a file dialog.
        /// </summary>
        public string InitialDirectory
        {
            get { return this._initialDirectory; }
            set { this._initialDirectory = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the user can select a read only item.
        /// </summary>
        public bool ReadOnlyChecked
        {
            get
            {
                return this._readOnlyChecked;
            }

            set
            {
                this.ThrowIfDialogShowing();
                this._readOnlyChecked = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether no change in directory should take place.
        /// Currently this property isn't used.
        /// </summary>
        public bool RestoreDirectory
        {
            get
            {
                return this._restoreDirectory;
            }

            set
            {
                this.ThrowIfDialogShowing();
                this._restoreDirectory = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether hidden items are shown to the user.
        /// </summary>
        public bool ShowHiddenItems
        {
            get
            {
                return this._showHiddenItems;
            }

            set
            {
                this.ThrowIfDialogShowing();
                this._showHiddenItems = value;
            }
        }

        /// <summary>
        /// Gets or sets the text that appears in the title bar of a file dialog.
        /// </summary>
        public string Title
        {
            get
            {
                return this._title;
            }

            set
            {
                this._title = value;
                if (this._nativeDialogShowing)
                {
                    this._nativeDialog.SetTitle(value);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a dialog is single or multi selection.
        /// </summary>
        public bool MultiSelect
        {
            get
            {
                return this._multiSelect;
            }

            set
            {
                this._multiSelect = value;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Displays a Select Folder dialog window.
        /// </summary>
        /// <param name="owner">
        /// Handle to the window that owns the dialog.
        /// </param>
        /// <returns>
        /// If the user clicks the OK button of the dialog that is displayed, true is returned;
        /// otherwise, false.
        /// </returns>
        public bool? ShowDialog(Window owner)
        {
            this._parentWindow = owner;
            return this.ShowDialog();
        }

        /// <summary>
        /// Displays a Select Folder dialog window.
        /// </summary>
        /// <returns>
        /// If the user clicks the OK button of the dialog that is displayed, true is returned;
        /// otherwise, false.
        /// </returns>
        public bool? ShowDialog()
        {
            bool? result = null;

            try
            {
                this._openDialogCoClass = new INativeFileOpenDialog();
                this._openDialogCoClass.SetClientGuid(ref this._persistence);
                this._nativeDialog = (IFileDialog)this._openDialogCoClass;
                this.ApplyNativeSettings(this._nativeDialog);

                this._nativeDialogShowing = true;
                IntPtr parentHandle = this.GetHandleFromWindow(this._parentWindow);
                int hresult = this._nativeDialog.Show(parentHandle);
                this._nativeDialogShowing = false;

                if (Win32Errors.Matches(hresult, Win32ErrorCode.ERROR_CANCELLED))
                {
                    this._cancelled = true;
                    this._filenames.Clear();
                }
                else
                {
                    this._cancelled = false;
                    this.PopulateWithFileNames();
                }

                result = !this._cancelled.Value;
            }
            finally
            {
                if (this._openDialogCoClass != null)
                {
                    Marshal.ReleaseComObject(this._openDialogCoClass);
                }

                this._nativeDialogShowing = false;
            }

            return result;
        }

        /// <summary>
        /// Apply the native settings onto the specified native dialog.
        /// </summary>
        /// <param name="dialog">
        /// The native dialog whose settings are being set.
        /// </param>
        private void ApplyNativeSettings(IFileDialog dialog)
        {
            if (this._parentWindow == null)
            {
                Application application = Application.Current;
                if (application != null)
                {
                    this._parentWindow = application.MainWindow;
                }
            }

            FileOpenDialogOptionFlags gottenOptions;
            dialog.GetOptions(out gottenOptions);

            dialog.SetOptions(this.CalculateNativeDialogOptionFlags());
            dialog.SetTitle(this._title);

            string directory = string.IsNullOrEmpty(this._fileName) ?
                this._initialDirectory : System.IO.Path.GetDirectoryName(this._fileName);
            if (directory != null && System.IO.Directory.Exists(directory))
            {
                IShellItem folder = Shell32.CreateShellItemFolder(directory);
                if (folder != null)
                {
                    dialog.SetFolder(folder);
                }
            }

            if (!string.IsNullOrEmpty(this._fileName))
            {
                string name = System.IO.Path.GetFileName(this._fileName);
                dialog.SetFileName(name);
            }
        }

        /// <summary>
        /// Calculates the native dialog setting flag based on this instances properties.
        /// </summary>
        /// <returns>
        /// The flag to set the native dialog settings to.
        /// </returns>
        private FileOpenDialogOptionFlags CalculateNativeDialogOptionFlags()
        {
            FileOpenDialogOptionFlags flags =
                FileOpenDialogOptionFlags.NOTESTFILECREATE |
                FileOpenDialogOptionFlags.FORCEFILESYSTEM |
                FileOpenDialogOptionFlags.PICKFOLDERS;

            // Apply other optional flags
            if (this._checkFileExists)
            {
                flags |= FileOpenDialogOptionFlags.FILEMUSTEXIST;
            }

            if (this._checkPathExists)
            {
                flags |= FileOpenDialogOptionFlags.PATHMUSTEXIST;
            }

            if (!this._checkValidNames)
            {
                flags |= FileOpenDialogOptionFlags.NOVALIDATE;
            }

            if (!this._readOnlyChecked)
            {
                flags |= FileOpenDialogOptionFlags.NOREADONLYRETURN;
            }

            if (this._restoreDirectory)
            {
                flags |= FileOpenDialogOptionFlags.NOCHANGEDIR;
            }

            if (this._hidePinnedPlaces)
            {
                flags |= FileOpenDialogOptionFlags.HIDEPINNEDPLACES;
            }

            if (!this._addToMruList)
            {
                flags |= FileOpenDialogOptionFlags.DONTADDTORECENT;
            }

            if (this._showHiddenItems)
            {
                flags |= FileOpenDialogOptionFlags.FORCESHOWHIDDEN;
            }

            if (!this._dereferenceLinks)
            {
                flags |= FileOpenDialogOptionFlags.NODEREFERENCELINKS;
            }

            if (this._multiSelect)
            {
                flags |= FileOpenDialogOptionFlags.ALLOWMULTISELECT;
            }

            return flags;
        }

        /// <summary>
        /// Gets the file name for the specified shell item.
        /// </summary>
        /// <param name="item">
        /// The shell item whose file name should be returned.
        /// </param>
        /// <returns>
        /// The file name of the specified shell item.
        /// </returns>
        private string GetFileNameFromShellItem(IShellItem item)
        {
            string filename;
            item.GetDisplayName(GetDisplayNameFlag.DESKTOPABSOLUTEPARSING, out filename);
            return filename;
        }

        /// <summary>
        /// Gets the window handle from the specified WPF window instance.
        /// </summary>
        /// <param name="window">
        /// The window to get the handler for.
        /// </param>
        /// <returns>
        /// The handle to the specified window.
        /// </returns>
        private IntPtr GetHandleFromWindow(Window window)
        {
            if (window == null)
            {
                return IntPtr.Zero;
            }

            return (new WindowInteropHelper(window)).Handle;
        }

        /// <summary>
        /// Gets the shell item in the specified array at the specified zero-based index.
        /// </summary>
        /// <param name="array">
        /// The array to retrieve the item from.
        /// </param>
        /// <param name="index">
        /// The zero-based index of the item to retrieve.
        /// </param>
        /// <returns>
        /// The shell item in the specified array at the specified zero-based index.
        /// </returns>
        private IShellItem GetShellItemAt(IShellItemArray array, int index)
        {
            IShellItem result;
            array.GetItemAt((uint)index, out result);
            return result;
        }

        /// <summary>
        /// Populates this instances filenames property with the filenames selected in the
        /// native dialog window.
        /// </summary>
        private void PopulateWithFileNames()
        {
            IShellItemArray resultsArray;
            uint count;

            this._openDialogCoClass.GetResults(out resultsArray);
            resultsArray.GetCount(out count);
            this._filenames.Clear();
            for (int i = 0; i < count; i++)
            {
                IShellItem item = this.GetShellItemAt(resultsArray, i);
                this._filenames.Add(this.GetFileNameFromShellItem(item));
            }

            if (count > 0)
            {
                this.FileName = this._filenames[0];
            }
        }

        /// <summary>
        /// Throws a System.NotSupportedException instance if the native dialog window is
        /// currently showing.
        /// </summary>
        private void ThrowIfDialogShowing()
        {
            if (!this._nativeDialogShowing)
            {
                return;
            }

            throw new NotSupportedException(
                "Cannot change a FOS property once the dialog is showing.");
        }
        #endregion
    } // RSG.Editor.Controls.RsSelectFolderIdentifer {Class}
} // RSG.Editor.Controls {Namespace}
