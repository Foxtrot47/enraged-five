﻿//---------------------------------------------------------------------------------------------
// <copyright file="ContainerEditor.xaml.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Customisation
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using RSG.Base.Extensions;
    using RSG.Editor.Controls.Helpers;

    /// <summary>
    /// Interaction logic for the container editor user control.
    /// </summary>
    internal partial class ContainerEditor : UserControl
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ContainerEditor"/> class.
        /// </summary>
        public ContainerEditor()
        {
            this.InitializeComponent();
            RockstarCommandManager.AddBinding<CommandContainerViewModel>(
                this,
                CustomViewCommands.AddCommand,
                this.OnAddCommand,
                this.CanAddCommand);

            RockstarCommandManager.AddBinding<CommandContainerViewModel>(
                this,
                CustomViewCommands.AddNewMenu,
                this.OnAddNewMenu,
                this.CanAddNewMenu);

            RockstarCommandManager.AddBinding<CommandContainerViewModel>(
                this,
                CustomViewCommands.AddNewToolbar,
                this.OnAddNewToolbar,
                this.CanAddNewToolbar);

            RockstarCommandManager.AddBinding<CommandViewModel>(
                this,
                CustomViewCommands.Delete,
                this.OnDelete,
                this.CanDelete);

            RockstarCommandManager.AddBinding<CommandViewModel>(
                this,
                CustomViewCommands.MoveDown,
                this.OnMoveDown,
                this.CanMoveDown);

            RockstarCommandManager.AddBinding<CommandViewModel>(
                this,
                CustomViewCommands.MoveUp,
                this.OnMoveUp,
                this.CanMoveUp);

            RockstarCommandManager.AddBinding<CommandContainerViewModel>(
                this,
                CustomViewCommands.Reset,
                this.OnReset,
                this.CanReset);
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the <see cref="CustomViewCommands.AddCommand"/> command can be
        /// executed based on the current state of the application.
        /// </summary>
        /// <param name="data">
        /// The can execute data that has been sent with the command.
        /// </param>
        /// <returns>
        /// True if the command can be executed; otherwise, false.
        /// </returns>
        private bool CanAddCommand(CanExecuteCommandData<CommandContainerViewModel> data)
        {
            if (data.Parameter == null)
            {
                return false;
            }

            return data.Parameter.Id != RockstarCommandManager.ToolBarTrayId;
        }

        /// <summary>
        /// Determines whether the <see cref="CustomViewCommands.AddNewMenu"/> command can be
        /// executed based on the current state of the application.
        /// </summary>
        /// <param name="data">
        /// The can execute data that has been sent with the command.
        /// </param>
        /// <returns>
        /// True if the command can be executed; otherwise, false.
        /// </returns>
        private bool CanAddNewMenu(CanExecuteCommandData<CommandContainerViewModel> data)
        {
            if (data.Parameter == null)
            {
                return false;
            }

            return data.Parameter.Id != RockstarCommandManager.ToolBarTrayId;
        }

        /// <summary>
        /// Determines whether the <see cref="CustomViewCommands.AddNewToolbar"/> command can
        /// be executed based on the current state of the application.
        /// </summary>
        /// <param name="data">
        /// The can execute data that has been sent with the command.
        /// </param>
        /// <returns>
        /// True if the command can be executed; otherwise, false.
        /// </returns>
        private bool CanAddNewToolbar(CanExecuteCommandData<CommandContainerViewModel> data)
        {
            if (data.Parameter == null)
            {
                return false;
            }

            return data.Parameter.Id == RockstarCommandManager.ToolBarTrayId;
        }

        /// <summary>
        /// Determines whether the <see cref="CustomViewCommands.Delete"/> command can be
        /// executed based on the current state of the application.
        /// </summary>
        /// <param name="data">
        /// The can execute data that has been sent with the command.
        /// </param>
        /// <returns>
        /// True if the command can be executed; otherwise, false.
        /// </returns>
        private bool CanDelete(CanExecuteCommandData<CommandViewModel> data)
        {
            return data.Parameter != null;
        }

        /// <summary>
        /// Determines whether the <see cref="CustomViewCommands.MoveDown"/> command can be
        /// executed based on the current state of the application.
        /// </summary>
        /// <param name="data">
        /// The can execute data that has been sent with the command.
        /// </param>
        /// <returns>
        /// True if the command can be executed; otherwise, false.
        /// </returns>
        private bool CanMoveDown(CanExecuteCommandData<CommandViewModel> data)
        {
            if (data.Parameter == null || data.Parameter.Container == null)
            {
                return false;
            }

            if (data.Parameter.Container.Id == RockstarCommandManager.ToolBarTrayId)
            {
                return false;
            }

            int index = data.Parameter.Container.Children.IndexOf(data.Parameter);
            return index < data.Parameter.Container.Children.Count - 1;
        }

        /// <summary>
        /// Determines whether the <see cref="CustomViewCommands.MoveUp"/> command can be
        /// executed based on the current state of the application.
        /// </summary>
        /// <param name="data">
        /// The can execute data that has been sent with the command.
        /// </param>
        /// <returns>
        /// True if the command can be executed; otherwise, false.
        /// </returns>
        private bool CanMoveUp(CanExecuteCommandData<CommandViewModel> data)
        {
            if (data.Parameter == null || data.Parameter.Container == null)
            {
                return false;
            }

            if (data.Parameter.Container.Id == RockstarCommandManager.ToolBarTrayId)
            {
                return false;
            }

            int index = data.Parameter.Container.Children.IndexOf(data.Parameter);
            return index != 0;
        }

        /// <summary>
        /// Determines whether the <see cref="CustomViewCommands.Reset"/> command can be
        /// executed based on the current state of the application.
        /// </summary>
        /// <param name="data">
        /// The can execute data that has been sent with the command.
        /// </param>
        /// <returns>
        /// True if the command can be executed; otherwise, false.
        /// </returns>
        private bool CanReset(CanExecuteCommandData<CommandContainerViewModel> data)
        {
            if (data.Parameter == null)
            {
                return false;
            }

            return RockstarCommandManager.HasModificationsForParent(data.Parameter.Id);
        }

        /// <summary>
        /// Called when the status on the items container generator for the command list
        /// changes.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.EventArgs instance containing the event data.
        /// </param>
        private void GeneratorStatusChanged(object sender, EventArgs e)
        {
            ItemContainerGenerator generator = sender as ItemContainerGenerator;
            if (generator == null)
            {
                Debug.Assert(generator != null, "Handler attached to incorrect item");
                return;
            }

            DependencyObject container = generator.ContainerFromIndex(0);
            if (container != null)
            {
                ListBoxItem listBoxItem = container as ListBoxItem;
                if (listBoxItem != null)
                {
                    listBoxItem.IsSelected = true;
                }

                generator.StatusChanged -= this.GeneratorStatusChanged;
            }
        }

        /// <summary>
        /// Retrieves the priority for a new command bar item when it is getting added after
        /// the specified command.
        /// </summary>
        /// <param name="viewModel">
        /// The command the new item is getting added after.
        /// </param>
        /// <returns>
        /// The priority for a new command bar item when it is getting added after the
        /// specified command.
        /// </returns>
        private ushort GetAddedPriority(CommandViewModel viewModel)
        {
            if (viewModel == null)
            {
                return 0;
            }

            var modifications = new List<Tuple<CommandBarItem, string>>();
            CommandContainerViewModel container = viewModel.Container;

            int indexCount = container.Children.Count - container.Children.IndexOf(viewModel);
            ushort maxPriority = (ushort)(ushort.MaxValue - indexCount);
            if (viewModel.CommandBarItem.Priority > maxPriority)
            {
                string oldValue = viewModel.CommandBarItem.Priority.ToStringInvariant();
                viewModel.CommandBarItem.Priority = maxPriority;
                modifications.Add(Tuple.Create(viewModel.CommandBarItem, oldValue));

                int index = container.Children.IndexOf(viewModel) - 1;
                ushort previousPriority = maxPriority;
                for (int i = index; i >= 0; i--)
                {
                    CommandBarItem item = container.Children[i].CommandBarItem;
                    if (item.Priority >= previousPriority)
                    {
                        string old = item.Priority.ToStringInvariant();
                        item.Priority = (ushort)(previousPriority - 1);
                        modifications.Add(Tuple.Create(item, old));
                        previousPriority = item.Priority;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            else
            {
                int index = container.Children.IndexOf(viewModel) + 1;
                ushort previousPriority = (ushort)(viewModel.CommandBarItem.Priority + 1);
                for (int i = index; i < container.Children.Count; i++)
                {
                    CommandBarItem item = container.Children[i].CommandBarItem;
                    if (item.Priority <= previousPriority)
                    {
                        string old = item.Priority.ToStringInvariant();
                        item.Priority = (ushort)(previousPriority + 1);
                        modifications.Add(Tuple.Create(item, old));
                        previousPriority = item.Priority;
                    }
                    else
                    {
                        break;
                    }
                }
            }

            ModificationField field = ModificationField.Priority;
            foreach (Tuple<CommandBarItem, string> modification in modifications)
            {
                CommandBarItem item = modification.Item1;
                string oldValue = modification.Item2;
                RockstarCommandManager.RegisterModification(item, field, oldValue);
            }

            return (ushort)(viewModel.CommandBarItem.Priority + 1);
        }

        /// <summary>
        /// Called whenever a binding for a dependency property on the command list is updated.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The event data containing the property that has been updated.
        /// </param>
        private void ListBoxTargetUpdated(object sender, DataTransferEventArgs e)
        {
            if (e.Property != ListBox.ItemsSourceProperty)
            {
                return;
            }

            ListBox listBox = sender as ListBox;
            if (listBox == null)
            {
                Debug.Assert(listBox != null, "Handler attached to incorrect item");
                return;
            }

            DependencyObject container = listBox.ItemContainerGenerator.ContainerFromIndex(0);
            if (container != null)
            {
                TreeViewItem treeViewItem = container as TreeViewItem;
                if (treeViewItem != null)
                {
                    treeViewItem.IsSelected = true;
                    treeViewItem.IsExpanded = true;
                }
            }
            else
            {
                listBox.ItemContainerGenerator.StatusChanged += this.GeneratorStatusChanged;
            }
        }

        /// <summary>
        /// Called whenever the <see cref="CustomViewCommands.AddCommand"/> command is fired
        /// and needs handling at this instance level.
        /// </summary>
        /// <param name="data">
        /// The execute data that has been sent with the command.
        /// </param>
        private void OnAddCommand(ExecuteCommandData<CommandContainerViewModel> data)
        {
            CommandContainerViewModel container = data.Parameter;
            AddCommandWindow window = new AddCommandWindow();
            AddCommandWindowDataContext dc = new AddCommandWindowDataContext();
            window.DataContext = dc;
            window.Owner = (data.Sender as DependencyObject).GetVisualAncestor<Window>();
            if (window.ShowDialog() == false || dc.SelectedDefinition == null)
            {
                return;
            }

            UserCreatedCommandInstance instance = null;
            CommandDefinition definition = dc.SelectedDefinition;
            using (DelayPriorityChanges delayer = new DelayPriorityChanges())
            {
                CommandViewModel selected = this.CommandList.SelectedItem as CommandViewModel;
                ushort priority = this.GetAddedPriority(selected);
                instance = new UserCreatedCommandInstance(definition, priority, container.Id);

                RockstarCommandManager.AddCommandBarItem(instance);
                RockstarCommandManager.RegisterCommandAdded(instance);
            }

            container.ResetItems();
        }

        /// <summary>
        /// Called whenever the <see cref="CustomViewCommands.AddNewMenu"/> command is fired
        /// and needs handling at this instance level.
        /// </summary>
        /// <param name="data">
        /// The execute data that has been sent with the command.
        /// </param>
        private void OnAddNewMenu(ExecuteCommandData<CommandContainerViewModel> data)
        {
            CommandContainerViewModel container = data.Parameter;
            UserCreateCommandMenu menu = null;
            using (DelayPriorityChanges delayer = new DelayPriorityChanges())
            {
                CommandViewModel selected = this.CommandList.SelectedItem as CommandViewModel;
                ushort priority = this.GetAddedPriority(selected);
                menu = new UserCreateCommandMenu(priority, container.Id);
                RockstarCommandManager.AddCommandBarItem(menu);
                RockstarCommandManager.RegisterCommandAdded(menu);
            }

            container.ResetItems();
        }

        /// <summary>
        /// Called whenever the <see cref="CustomViewCommands.AddNewToolbar"/> command is fired
        /// and needs handling at this instance level.
        /// </summary>
        /// <param name="data">
        /// The execute data that has been sent with the command.
        /// </param>
        private void OnAddNewToolbar(ExecuteCommandData<CommandContainerViewModel> data)
        {
            CommandContainerViewModel container = data.Parameter;
            UserCreateCommandToolbar toolbar = null;
            using (DelayPriorityChanges delayer = new DelayPriorityChanges())
            {
                toolbar = new UserCreateCommandToolbar();
                RockstarCommandManager.AddCommandBarItem(toolbar);
                RockstarCommandManager.RegisterCommandAdded(toolbar);
            }

            container.ResetItems();
        }

        /// <summary>
        /// Called whenever the <see cref="CustomViewCommands.Delete"/> command is fired and
        /// needs handling at this instance level.
        /// </summary>
        /// <param name="data">
        /// The execute data that has been sent with the command.
        /// </param>
        private void OnDelete(ExecuteCommandData<CommandViewModel> data)
        {
            CommandContainerViewModel container = data.Parameter.Container;
            RockstarCommandManager.RemoveCommandBarItem(data.Parameter.CommandBarItem);
            RockstarCommandManager.RegisterCommandRemoved(data.Parameter.CommandBarItem);

            container.ResetItems();
        }

        /// <summary>
        /// Called whenever the <see cref="CustomViewCommands.MoveDown"/> command is fired and
        /// needs handling at this instance level.
        /// </summary>
        /// <param name="data">
        /// The execute data that has been sent with the command.
        /// </param>
        private void OnMoveDown(ExecuteCommandData<CommandViewModel> data)
        {
            CommandContainerViewModel container = data.Parameter.Container;
            int index = container.Children.IndexOf(data.Parameter);
            CommandBarItem item = data.Parameter.CommandBarItem;
            CommandBarItem otherItem = container.Children[index + 1].CommandBarItem;
            if (item == null || otherItem == null)
            {
                return;
            }

            this.Rearrange(otherItem, item, container);
            container.ResetItems();
        }

        /// <summary>
        /// Called whenever the <see cref="CustomViewCommands.MoveUp"/> command is fired and
        /// needs handling at this instance level.
        /// </summary>
        /// <param name="data">
        /// The execute data that has been sent with the command.
        /// </param>
        private void OnMoveUp(ExecuteCommandData<CommandViewModel> data)
        {
            CommandContainerViewModel container = data.Parameter.Container;
            int index = container.Children.IndexOf(data.Parameter);
            CommandBarItem item = data.Parameter.CommandBarItem;
            CommandBarItem otherItem = container.Children[index - 1].CommandBarItem;
            if (item == null || otherItem == null)
            {
                return;
            }

            this.Rearrange(item, otherItem, container);
            container.ResetItems();
        }

        /// <summary>
        /// Called whenever the <see cref="CustomViewCommands.Reset"/> command is fired and
        /// needs handling at this instance level.
        /// </summary>
        /// <param name="data">
        /// The execute data that has been sent with the command.
        /// </param>
        private void OnReset(ExecuteCommandData<CommandContainerViewModel> data)
        {
            RockstarCommandManager.ClearModifications(data.Parameter.Id);
            data.Parameter.ResetItems();
        }

        /// <summary>
        /// Swaps the priorities of the specified items making sure the whole container is
        /// still valid and in the correct order and that in the finally order item2 is greater
        /// than item1.
        /// </summary>
        /// <param name="item1">
        /// The first item to swap the priority of.
        /// </param>
        /// <param name="item2">
        /// The second item to swap the priority of and the item that will have the greatest
        /// priority of the two specified items.
        /// </param>
        /// <param name="container">
        /// The container the two items come from.
        /// </param>
        private void Rearrange(
            CommandBarItem item1, CommandBarItem item2, CommandContainerViewModel container)
        {
            if (item1.ParentId != item2.ParentId)
            {
                throw new NotSupportedException(
                    "Unable to operator on two items with difference parents");
            }

            var modifications = new List<Tuple<CommandBarItem, string>>();
            using (DelayPriorityChanges delayer = new DelayPriorityChanges())
            {
                if (item1.Priority != item2.Priority)
                {
                    string item1OldValue = item1.Priority.ToStringInvariant();
                    string item2OldValue = item2.Priority.ToStringInvariant();

                    ushort temp = item1.Priority;
                    item1.Priority = item2.Priority;
                    item2.Priority = temp;

                    modifications.Add(Tuple.Create(item1, item1OldValue));
                    modifications.Add(Tuple.Create(item2, item2OldValue));
                }
                else
                {
                    ushort priority = 0;
                    foreach (CommandViewModel viewModel in container.Children)
                    {
                        if (viewModel.CommandBarItem.Priority != priority)
                        {
                            string old = viewModel.CommandBarItem.Priority.ToStringInvariant();
                            viewModel.CommandBarItem.Priority = priority;

                            modifications.Add(Tuple.Create(viewModel.CommandBarItem, old));
                        }

                        priority++;
                    }

                    string item1OldValue = item1.Priority.ToStringInvariant();
                    string item2OldValue = item2.Priority.ToStringInvariant();

                    ushort temp = item1.Priority;
                    item1.Priority = item2.Priority;
                    item2.Priority = temp;

                    modifications.Add(Tuple.Create(item1, item1OldValue));
                    modifications.Add(Tuple.Create(item2, item2OldValue));
                }
            }

            ModificationField field = ModificationField.Priority;
            foreach (Tuple<CommandBarItem, string> modification in modifications)
            {
                CommandBarItem item = modification.Item1;
                string oldValue = modification.Item2;
                RockstarCommandManager.RegisterModification(item, field, oldValue);
            }
        }

        /// <summary>
        /// Called whenever the selection on the command list changes. For a nice user
        /// experience we need to make sure that at least one item is selected at all times if
        /// at all possible.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Controls.SelectionChangedEventArgs object used as the event
        /// data.
        /// </param>
        private void SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBox listBox = sender as ListBox;
            if (listBox == null)
            {
                Debug.Assert(listBox != null, "Handler attached to incorrect item");
                return;
            }

            if (listBox.SelectedItem == null)
            {
                listBox.SelectedIndex = 0;
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.Customisation.ContainerEditor {Class}
} // RSG.Editor.Controls.Customisation {Namespace}
