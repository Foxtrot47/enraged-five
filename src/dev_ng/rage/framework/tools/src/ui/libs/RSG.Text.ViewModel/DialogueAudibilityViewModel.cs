﻿namespace RSG.Text.ViewModel
{
    using System;
    using RSG.Editor.View;
    using RSG.Text.Model;

    public class DialogueAudibilityViewModel : ViewModelBase<DialogueAudibility>
    {
        private int _usedCount;

        public DialogueAudibilityViewModel(DialogueAudibility audibility)
            : base(audibility, true)
        {
        }

        public int ExportIndex
        {
            get { return this.Model.ExportIndex; }
            set { this.Model.ExportIndex = value; }
        }

        public Guid Id
        {
            get { return this.Model.Id; }
        }


        public bool IsUsed
        {
            get { return this.UsedCount > 0; }
        }

        public string Name
        {
            get { return this.Model.Name; }
            set { this.Model.Name = value; }
        }

        public int UsedCount
        {
            get { return this._usedCount; }
            set { this.SetProperty(ref this._usedCount, value, "UsedCount", "IsUsed"); }
        }
    }
}
