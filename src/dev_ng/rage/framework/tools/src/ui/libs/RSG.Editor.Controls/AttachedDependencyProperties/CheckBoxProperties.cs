﻿//---------------------------------------------------------------------------------------------
// <copyright file="CheckBoxProperties.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.AttachedDependencyProperties
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;

    /// <summary>
    /// Contains attached properties that are related to the check box.
    /// </summary>
    public static class CheckBoxProperties
    {
        #region Fields
        /// <summary>
        /// Identifies the IsReadOnly dependency property.
        /// </summary>
        public static readonly DependencyProperty IsReadOnlyProperty;

        /// <summary>
        /// Identifies the LeftContentAlignment dependency property.
        /// </summary>
        public static readonly DependencyProperty LeftContentAlignmentProperty;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="CheckBoxProperties"/> class.
        /// </summary>
        static CheckBoxProperties()
        {
            IsReadOnlyProperty =
                DependencyProperty.RegisterAttached(
                "IsReadOnly",
                typeof(bool),
                typeof(CheckBoxProperties),
                new PropertyMetadata(false, OnIsReadOnlyChanged));

            LeftContentAlignmentProperty =
                DependencyProperty.RegisterAttached(
                "LeftContentAlignment",
                typeof(bool),
                typeof(CheckBoxProperties),
                new PropertyMetadata(false));
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Retrieves the IsReadOnly dependency property value attached to the specified
        /// object.
        /// </summary>
        /// <param name="element">
        /// The object whose attached IsReadOnly dependency property value will be returned.
        /// </param>
        /// <returns>
        /// The IsReadOnly dependency property value attached to the specified object.
        /// </returns>
        public static bool GetIsReadOnly(DependencyObject element)
        {
            return (bool)element.GetValue(IsReadOnlyProperty);
        }

        /// <summary>
        /// Retrieves the LeftContentAlignment dependency property value attached to the
        /// specified object.
        /// </summary>
        /// <param name="element">
        /// The object whose attached LeftContentAlignment dependency property value will be
        /// returned.
        /// </param>
        /// <returns>
        /// The LeftContentAlignment dependency property value attached to the specified
        /// object.
        /// </returns>
        public static bool GetLeftContentAlignment(DependencyObject element)
        {
            return (bool)element.GetValue(LeftContentAlignmentProperty);
        }

        /// <summary>
        /// Sets the IsReadOnly dependency property value on the specified object to the
        /// specified value.
        /// </summary>
        /// <param name="element">
        /// The object whose attached IsReadOnly dependency property value will be set.
        /// </param>
        /// <param name="value">
        /// The value to set the attached IsReadOnly dependency property to on the specified
        /// object.
        /// </param>
        public static void SetIsReadOnly(DependencyObject element, bool value)
        {
            element.SetValue(IsReadOnlyProperty, value);
        }

        /// <summary>
        /// Sets the LeftContentAlignment dependency property value on the specified object to
        /// the specified value.
        /// </summary>
        /// <param name="element">
        /// The object whose attached LeftContentAlignment dependency property value will be
        /// set.
        /// </param>
        /// <param name="value">
        /// The value to set the attached LeftContentAlignment dependency property to on the
        /// specified object.
        /// </param>
        public static void SetLeftContentAlignment(DependencyObject element, bool value)
        {
            element.SetValue(LeftContentAlignmentProperty, value);
        }

        /// <summary>
        /// Called whenever a check box that has been set to read only has a preview key down
        /// event. This event is handled so that the box can stay read only.
        /// </summary>
        /// <param name="sender">
        /// The check box that this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.KeyEventArgs containing the event data.
        /// </param>
        private static void OnCheckBoxPreviewKeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = true;
        }

        /// <summary>
        /// Called whenever a check box that has been set to read only has a mouse wheel event.
        /// This event is handled so that the box can stay read only.
        /// </summary>
        /// <param name="sender">
        /// The check box that this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.MouseWheelEventArgs containing the event data.
        /// </param>
        private static void OnCheckBoxPreviewMouseDown(object sender, MouseEventArgs e)
        {
            e.Handled = true;
        }

        /// <summary>
        /// Called whenever the IsReadOnly dependency property changes.
        /// </summary>
        /// <param name="d">
        /// The object whose IsReadOnly dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnIsReadOnlyChanged(
            DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            CheckBox checkBox = d as CheckBox;
            if (checkBox == null)
            {
                return;
            }

            if ((bool)e.NewValue)
            {
                checkBox.PreviewMouseDown += OnCheckBoxPreviewMouseDown;
                checkBox.PreviewKeyDown += OnCheckBoxPreviewKeyDown;
            }
            else
            {
                checkBox.PreviewMouseDown -= OnCheckBoxPreviewMouseDown;
                checkBox.PreviewKeyDown -= OnCheckBoxPreviewKeyDown;
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.AttachedDependencyProperties.CheckBoxProperties {Class}
} // RSG.Editor.Controls.AttachedDependencyProperties {Namespace}
