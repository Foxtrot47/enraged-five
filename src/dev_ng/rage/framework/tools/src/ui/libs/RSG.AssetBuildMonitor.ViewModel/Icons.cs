﻿//---------------------------------------------------------------------------------------------
// <copyright file="Icons.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.AssetBuildMonitor.ViewModel
{
    using System;
    using System.Windows.Media.Imaging;
    using RSG.Base.Extensions;

    /// <summary>
    /// Provides static properties that gives access to the icons that are used for the
    /// metadata types inside a tree view.
    /// </summary>
    internal static class Icons
    {
        #region Properties
        /// <summary>
        /// Gets the icon that is used for a node representing a Host inside the project
        /// tree view.
        /// </summary>
        public static BitmapSource HostNodeIcon
        {
            get
            {
                if (_hostNodeIcon == null)
                {
                    lock (_syncRoot)
                    {
                        if (_hostNodeIcon == null)
                        {
                            EnsureLoaded(ref _hostNodeIcon, "HostNode");
                        }
                    }
                }

                return _hostNodeIcon;
            }
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// The private field used for the <see cref="HostNodeIcon"/> property.
        /// </summary>
        private static BitmapSource _hostNodeIcon;

        /// <summary>
        /// A generic object that provides thread safety access to the image sources.
        /// </summary>
        private static object _syncRoot = new object();
        #endregion // Member Data

        #region Methods
        /// <summary>
        /// Ensures that the specified image source is loaded with the specified resource name.
        /// </summary>
        /// <param name="source">
        /// The image source that should be loaded.
        /// </param>
        /// <param name="resourceName">
        /// The name of the resource the loaded image source should set as its source.
        /// </param>
        private static void EnsureLoaded(ref BitmapSource source, string resourceName)
        {
            if (source != null)
            {
                return;
            }

            string assemblyName = typeof(Icons).Assembly.GetName().Name;
            string path = "Resources/" + resourceName + ".png";
            string bitmapPath = "pack://application:,,,/{0};component/{1}";
            bitmapPath = bitmapPath.FormatInvariant(assemblyName, path);
            Uri uri = new Uri(bitmapPath, UriKind.RelativeOrAbsolute);

            source = new BitmapImage(uri);
            source.Freeze();
        }
        #endregion // Methods
    }

} // RSG.AssetBuildMonitor.ViewModel namespace
