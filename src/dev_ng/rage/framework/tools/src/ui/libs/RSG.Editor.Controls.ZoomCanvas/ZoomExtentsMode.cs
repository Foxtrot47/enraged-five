﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Editor.Controls.ZoomCanvas
{
    /// <summary>
    /// Flags for options when zooming to extents.
    /// </summary>
    public enum ZoomExtentsMode
    {
        /// <summary>
        /// Zoom horizontal extents.
        /// </summary>
        Horizontal,

        /// <summary>
        /// Zoom vertical extents.
        /// </summary>
        Vertical,

        /// <summary>
        /// Zoom both horizontal and vertical extents.
        /// </summary>
        HorizontalAndVertical,

        /// <summary>
        /// Zooms so that the larger extents is visible (maintains aspect ratio).
        /// </summary>
        Inner,

        /// <summary>
        /// Zooms so that the smaller extents is visible (maintains aspect ratio).
        /// </summary>
        Outer
    }
}
