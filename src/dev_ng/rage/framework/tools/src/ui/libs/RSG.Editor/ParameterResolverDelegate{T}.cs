﻿//---------------------------------------------------------------------------------------------
// <copyright file="ParameterResolverDelegate{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System.Windows.Input;

    /// <summary>
    /// Represents the method that is used to retrieve a command parameter object of the
    /// specified type for the specified command.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the retrieved parameter.
    /// </typeparam>
    /// <param name="commandData">
    /// The data that was sent with the command containing among other things the command
    /// whose command parameter needs to be resolved.
    /// </param>
    /// <returns>
    /// The command parameter object of the specified type for the specified command.
    /// </returns>
    public delegate T ParameterResolverDelegate<T>(CommandData commandData);
} // RSG.Editor {Namespace}
