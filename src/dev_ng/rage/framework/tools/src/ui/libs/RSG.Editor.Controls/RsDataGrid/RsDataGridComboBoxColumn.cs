﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsDataGridComboBoxColumn.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
    using RSG.Editor.Controls.AttachedDependencyProperties;

    /// <summary>
    /// Represents a data grid column that hosts a combo box in its data grid cells.
    /// </summary>
    public class RsDataGridComboBoxColumn : DataGridComboBoxColumn
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="EditableStyleKey"/> property.
        /// </summary>
        private static ResourceKey _editableStyleKey = CreateStyleKey("EditableStyleKey");

        /// <summary>
        /// The private field used for the <see cref="ReadOnlyStyleKey"/> property.
        /// </summary>
        private static ResourceKey _readOnlyStyleKey = CreateStyleKey("ReadOnlyStyleKey");

        /// <summary>
        /// The private field used for the <see cref="GroupStyle"/> property.
        /// </summary>
        private ObservableCollection<GroupStyle> _groupStyle;

        /// <summary>
        /// The private field used for the <see cref="ItemsSourceBinding"/> property.
        /// </summary>
        private BindingBase _itemsSourceBinding;

        /// <summary>
        /// The private field used for the <see cref="RefreshOnClose"/> property.
        /// </summary>
        private bool _refreshOnClose;

        /// <summary>
        /// The private field used for the <see cref="RefreshOnOpen"/> property.
        /// </summary>
        private bool _refreshOnOpen;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsDataGridComboBoxColumn"/> class.
        /// </summary>
        static RsDataGridComboBoxColumn()
        {
            ScrollViewer.CanContentScrollProperty.AddOwner(
                typeof(RsDataGridComboBoxColumn),
                new FrameworkPropertyMetadata(true));

            DataGridBoundColumn.IsReadOnlyProperty.OverrideMetadata(
                typeof(RsDataGridComboBoxColumn),
                new FrameworkPropertyMetadata(OnReadOnlyPropertyChanged));

            VirtualizingStackPanel.IsVirtualizingWhenGroupingProperty.AddOwner(
                typeof(RsDataGridComboBoxColumn),
                new FrameworkPropertyMetadata(false));

            ComboBox.ItemTemplateProperty.AddOwner(typeof(RsDataGridComboBoxColumn));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsDataGridComboBoxColumn"/> class.
        /// </summary>
        public RsDataGridComboBoxColumn()
        {
            this._groupStyle = new ObservableCollection<GroupStyle>();
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the resource key used to reference the style used on editable check boxes.
        /// </summary>
        public static ResourceKey EditableStyleKey
        {
            get { return _editableStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on read only check boxes.
        /// </summary>
        public static ResourceKey ReadOnlyStyleKey
        {
            get { return _readOnlyStyleKey; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the items inside the combo box are allowed
        /// to scroll. Use this method to set whether this combo box is virtualizing.
        /// </summary>
        public bool CanContentScroll
        {
            get { return (bool)this.GetValue(ScrollViewer.CanContentScrollProperty); }
            set { this.SetValue(ScrollViewer.CanContentScrollProperty, value); }
        }

        /// <summary>
        /// Gets the collection of GroupStyle objects that describes the display of each level
        /// of grouping.
        /// </summary>
        public ObservableCollection<GroupStyle> GroupStyle
        {
            get { return this._groupStyle; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the virtualizing stack panel being used
        /// for the combo box items should be virtualizing based on grouped data.
        /// </summary>
        public bool IsVirtualizingWhenGrouping
        {
            get
            {
                return (bool)this.GetValue(
                    VirtualizingPanel.IsVirtualizingWhenGroupingProperty);
            }

            set
            {
                this.SetValue(VirtualizingPanel.IsVirtualizingWhenGroupingProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the value of the items source property on the generated combo boxes.
        /// </summary>
        public virtual BindingBase ItemsSourceBinding
        {
            get
            {
                return this._itemsSourceBinding;
            }

            set
            {
                if (this._itemsSourceBinding == value)
                {
                    return;
                }

                this._itemsSourceBinding = value;
                this.NotifyPropertyChanged("ItemsSource");
            }
        }

        /// <summary>
        /// Gets or sets the data template that will be applied to the ItemTemplate property of
        /// the ComboBox control.
        /// </summary>
        public virtual DataTemplate ItemTemplate
        {
            get { return (DataTemplate)this.GetValue(ComboBox.ItemTemplateProperty); }
            set { this.SetValue(ComboBox.ItemTemplateProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the combo box as the editing control for
        /// this column refreshes its items every time it closes.
        /// </summary>
        public bool RefreshOnClose
        {
            get { return this._refreshOnClose; }
            set { this._refreshOnClose = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the combo box as the editing control for
        /// this column refreshes its items before every time it opens.
        /// </summary>
        public bool RefreshOnOpen
        {
            get { return this._refreshOnOpen; }
            set { this._refreshOnOpen = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates the visual tree that will become the content of the specified cell.
        /// </summary>
        /// <param name="cell">
        /// The cell the visual tree is being created for.
        /// </param>
        /// <param name="dataItem">
        /// The data item currently bound to the specified cell.
        /// </param>
        /// <returns>
        /// The root of the visual tree that will become the content of the specified cell.
        /// </returns>
        protected override FrameworkElement GenerateEditingElement(
            DataGridCell cell, object dataItem)
        {
            ComboBox comboBox = (cell != null) ? (cell.Content as ComboBox) : null;
            if (comboBox == null)
            {
                comboBox = new ComboBox();
                comboBox.IsSynchronizedWithCurrentItem = false;
            }

            comboBox.SetResourceReference(FrameworkElement.StyleProperty, EditableStyleKey);
            this.ApplyProperties(comboBox);
            comboBox.IsHitTestVisible = true;
            comboBox.Focusable = true;
            cell.IsTabStop = false;
            comboBox.IsTabStop = true;

            return comboBox;
        }

        /// <summary>
        /// Creates the visual tree that will become the content of the specified cell.
        /// </summary>
        /// <param name="cell">
        /// The cell the visual tree is being created for.
        /// </param>
        /// <param name="dataItem">
        /// The data item currently bound to the specified cell.
        /// </param>
        /// <returns>
        /// The root of the visual tree that will become the content of the specified cell.
        /// </returns>
        protected override FrameworkElement GenerateElement(DataGridCell cell, object dataItem)
        {
            if (this.IsReadOnly == false)
            {
                return this.GenerateEditingElement(cell, dataItem);
            }

            FrameworkElement baseElement = base.GenerateElement(cell, dataItem);
            this.HandleItemsSource(baseElement, false);
            cell.IsTabStop = true;
            return baseElement;
        }

        /// <summary>
        /// Updates the contents of a cell in the column in response to a column property value
        /// that changed.
        /// </summary>
        /// <param name="element">
        /// The cell to update.
        /// </param>
        /// <param name="name">
        /// The name of the column property that changed.
        /// </param>
        protected override void RefreshCellContent(FrameworkElement element, string name)
        {
            DataGridCell dataGridCell = element as DataGridCell;
            if (dataGridCell == null)
            {
                base.RefreshCellContent(element, name);
                return;
            }

            ComboBox comboBox = dataGridCell.Content as ComboBox;
            if (comboBox == null)
            {
                base.RefreshCellContent(element, name);
                return;
            }

            switch (name)
            {
                case "ItemsSource":
                    this.HandleItemsSource(comboBox, true);
                    return;
            }

            base.RefreshCellContent(element, name);
        }

        /// <summary>
        /// Creates a new resource key that references a style with the specified name.
        /// </summary>
        /// <param name="styleName">
        /// The name that the resource key will be referencing.
        /// </param>
        /// <returns>
        /// A new resource key that references a style with the specified name.
        /// </returns>
        private static ResourceKey CreateStyleKey(string styleName)
        {
            return new StyleKey<RsDataGridComboBoxColumn>(styleName);
        }

        /// <summary>
        /// Called whenever the IsReadOnly dependency property changes.
        /// </summary>
        /// <param name="d">
        /// The object whose IsReadOnly dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnReadOnlyPropertyChanged(
            DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RsDataGridComboBoxColumn column = d as RsDataGridComboBoxColumn;
            if (column == null)
            {
                Debug.Assert(false, "Incorrect dependency object cast");
                return;
            }

            // This will make sure the visual tree for all of the cells are rebuilt.
            column.NotifyPropertyChanged("ElementStyle");
            column.NotifyPropertyChanged("EditingElementStyle");
        }

        /// <summary>
        /// Applies a single binding on the target object.
        /// </summary>
        /// <param name="binding">
        /// The binding to apply.
        /// </param>
        /// <param name="target">
        /// The target object for the binding.
        /// </param>
        /// <param name="property">
        /// The dependency property to set the binding on.
        /// </param>
        private void ApplyBinding(
            BindingBase binding, DependencyObject target, DependencyProperty property)
        {
            if (binding != null)
            {
                BindingOperations.SetBinding(target, property, binding);
                return;
            }

            BindingOperations.ClearBinding(target, property);
        }

        /// <summary>
        /// Applies all of the properties on the specified combo box by either applying a
        /// binding from the column or syncing the property value from the column.
        /// </summary>
        /// <param name="comboBox">
        /// The combo box the properties are being set on.
        /// </param>
        private void ApplyProperties(ComboBox comboBox)
        {
            DependencyProperty comboBoxProperty = Selector.SelectedItemProperty;
            this.ApplyBinding(this.SelectedItemBinding, comboBox, comboBoxProperty);

            comboBoxProperty = Selector.SelectedValueProperty;
            this.ApplyBinding(this.SelectedValueBinding, comboBox, comboBoxProperty);

            comboBoxProperty = ComboBox.TextProperty;
            this.ApplyBinding(this.TextBinding, comboBox, comboBoxProperty);

            DependencyProperty columnProperty =
                DataGridComboBoxColumn.SelectedValuePathProperty;
            comboBoxProperty = Selector.SelectedValuePathProperty;
            this.ApplyProperty(comboBox, comboBoxProperty, columnProperty);

            columnProperty = ComboBox.ItemTemplateProperty;
            comboBoxProperty = ComboBox.ItemTemplateProperty;
            this.ApplyProperty(comboBox, comboBoxProperty, columnProperty);

            columnProperty = VirtualizingPanel.IsVirtualizingWhenGroupingProperty;
            comboBoxProperty = VirtualizingPanel.IsVirtualizingWhenGroupingProperty;
            this.ApplyProperty(comboBox, comboBoxProperty, columnProperty);

            columnProperty = ScrollViewer.CanContentScrollProperty;
            comboBoxProperty = ScrollViewer.CanContentScrollProperty;
            this.ApplyProperty(comboBox, comboBoxProperty, columnProperty);

            columnProperty = DataGridComboBoxColumn.DisplayMemberPathProperty;
            comboBoxProperty = ItemsControl.DisplayMemberPathProperty;
            this.ApplyProperty(comboBox, comboBoxProperty, columnProperty);

            comboBox.SetValue(ComboBoxProperties.RefreshOnCloseProperty, this._refreshOnClose);
            comboBox.SetValue(ComboBoxProperties.RefreshOnOpenProperty, this._refreshOnOpen);

            columnProperty = TextSearch.TextPathProperty;
            comboBoxProperty = TextSearch.TextPathProperty;
            this.ApplyProperty(comboBox, comboBoxProperty, columnProperty);
            this.HandleItemsSource(comboBox, true);

            foreach (GroupStyle groupStyle in this.GroupStyle)
            {
                comboBox.GroupStyle.Add(groupStyle);
            }
        }

        /// <summary>
        /// Applies the specified column property onto the specified combo box.
        /// </summary>
        /// <param name="element">
        /// The combo box to set the values on.
        /// </param>
        /// <param name="comboBoxProperty">
        /// The property on the combo box to set.
        /// </param>
        /// <param name="columnProperty">
        /// The property on the column box to take.
        /// </param>
        private void ApplyProperty(
            FrameworkElement element,
            DependencyProperty comboBoxProperty,
            DependencyProperty columnProperty)
        {
            ValueSource source = DependencyPropertyHelper.GetValueSource(this, columnProperty);
            if (source.BaseValueSource == BaseValueSource.Default)
            {
                element.ClearValue(comboBoxProperty);
                return;
            }

            element.SetValue(comboBoxProperty, this.GetValue(columnProperty));
        }

        /// <summary>
        /// Handles the items source property on the specified element.
        /// </summary>
        /// <param name="element">
        /// The element to set the items source property on.
        /// </param>
        /// <param name="applyProperty">
        /// Specified whether the column property should be set on the element if a binding
        /// cannot be made.
        /// </param>
        private void HandleItemsSource(FrameworkElement element, bool applyProperty)
        {
            DependencyProperty comboBoxProperty = ItemsControl.ItemsSourceProperty;
            DependencyProperty columnProperty = DataGridComboBoxColumn.ItemsSourceProperty;

            if (this.ItemsSourceBinding != null)
            {
                this.ApplyBinding(this.ItemsSourceBinding, element, comboBoxProperty);
            }
            else if (applyProperty)
            {
                this.ApplyProperty(element, comboBoxProperty, columnProperty);
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsDataGridComboBoxColumn {Class}
} // RSG.Editor.Controls {Namespace}
