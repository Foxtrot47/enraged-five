﻿//---------------------------------------------------------------------------------------------
// <copyright file="FilterDescendantBehaviour.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    /// <summary>
    /// Defines the different behaviours a item can have when filtering descendants of a item.
    /// </summary>
    public enum FilterDescendantBehaviour
    {
        /// <summary>
        /// This excludes the descendants by default in the filter.
        /// </summary>
        ExcludeDescendantsByDefault,

        /// <summary>
        /// This includes the descendants by default in the filter.
        /// </summary>
        IncludeDescendantsByDefault
    } // RSG.Editor.View.FilterDescendantBehaviour {Enum}
} // RSG.Editor.View {Namespace}
