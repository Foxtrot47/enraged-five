﻿//---------------------------------------------------------------------------------------------
// <copyright file="HierarchicalViewModelBase{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    using RSG.Editor.Model;

    /// <summary>
    /// Provides a abstract base class that can be inherited by classes that wish to implement
    /// the <see cref="RSG.Editor.View.IViewModel{T}"/> interface.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the model that is being wrapped by this view model.
    /// </typeparam>
    public abstract class HierarchicalViewModelBase<T> : ViewModelBase<T> where T : IModel
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="HierarchicalViewModelBase{T}"/> class.
        /// </summary>
        /// <param name="model">
        /// The model that this view model will be wrapping.
        /// </param>
        protected HierarchicalViewModelBase(T model) : base(model)
        {
        }
        #endregion Constructors
    } // RSG.Editor.View.HierarchicalViewModelBase{T} {Class}
} // RSG.Editor.View {Namespace}
