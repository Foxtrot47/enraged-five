﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using RSG.Editor;
using RSG.Editor.SharedCommands;

namespace RSG.Automation.Commands
{

    /// <summary>
    /// Automation Monitor commands class.
    /// </summary>
    public static class AutomationMonitorCommands
    {
        #region Fields
        /// <summary>
        /// String table resource manager for getting at the command related strings.
        /// </summary>
        private static readonly CommandResourceManager _commandResourceManager =
            new CommandResourceManager(
                typeof(AutomationMonitorCommands).Namespace + ".Resources.CommandStringTable",
                typeof(AutomationMonitorCommands).Assembly);

        /// <summary>
        /// The private cache of System.Windows.Input.RoutedCommand objects.
        /// </summary>
        private static readonly RockstarRoutedCommand[] _internalCommands =
            new RockstarRoutedCommand[Enum.GetValues(typeof(CommandId)).Length];
        #endregion // Fields

        #region Enumerations
        /// <summary>
        /// Defines all of the command identifiers for the different core Rockstar commands.
        /// </summary>
        private enum CommandId
        {
            SelectProject,
            SelectBranch,
            SelectAutomationService,
            Refresh,
            UseUTC,
            SkipJob,
            PrioritiseJob,
            DownloadJobOutput,
            DownloadJobLog,
            OpenAutomationConsole,
            FilterByJobState,
            FilterMyJobs,
            OpenJobFolder,
            OpenJobLog,
            DeleteFiles,
            DeleteAll,

            /// <summary>
            /// Defines the number of commands defined as Automation Monitor commands.
            /// </summary>
            CommandCount
        }
        #endregion // Enumerations

        #region Properties
        /// <summary>
        /// Gets the value that represents the Select Project command.
        /// </summary>
        public static RockstarRoutedCommand SelectProject
        {
            get { return EnsureCommandExists(CommandId.SelectProject); }
        }

        /// <summary>
        /// Gets the value that represents the Select Branch command.
        /// </summary>
        public static RockstarRoutedCommand SelectBranch
        {
            get { return EnsureCommandExists(CommandId.SelectBranch); }
        }
        
        /// <summary>
        /// Gets the value that represents the Select Project command.
        /// </summary>
        public static RockstarRoutedCommand SelectAutomationService
        {
            get { return EnsureCommandExists(CommandId.SelectAutomationService); }
        }

        /// <summary>
        /// Gets the value that represents the Refresh command.
        /// </summary>
        public static RockstarRoutedCommand Refresh
        {
            get { return EnsureCommandExists(CommandId.Refresh, CommonIcons.Refresh); }
        }

        /// <summary>
        /// Gets the value that represents the Use UTC command.
        /// </summary>
        public static RockstarRoutedCommand UseUTC
        {
            get { return EnsureCommandExists(CommandId.UseUTC); }
        }

        /// <summary>
        ///
        /// </summary>
        public static RockstarRoutedCommand FilterMyJobs
        {
            get { return EnsureCommandExists(CommandId.FilterMyJobs); }
        }

        /// <summary>
        /// Gets the value that represents the Prioritise Job command.
        /// </summary>
        public static RockstarRoutedCommand PrioritiseJob
        {
            get { return EnsureCommandExists(CommandId.PrioritiseJob); }
        }

        /// <summary>
        /// Gets the value that represents the Abort Job command.
        /// </summary>
        public static RockstarRoutedCommand SkipJob
        {
            get { return EnsureCommandExists(CommandId.SkipJob, CommonIcons.Delete); }
        }

        /// <summary>
        /// Gets the value that represents the Download Job Output command.
        /// </summary>
        public static RockstarRoutedCommand DownloadJobOutput
        {
            get { return EnsureCommandExists(CommandId.DownloadJobOutput, AutomationMonitorCommandIcons.DownloadJobOutput); }
        }

        /// <summary>
        /// Gets the value that represents the Download Job Log command.
        /// </summary>
        public static RockstarRoutedCommand DownloadJobLog
        {
            get { return EnsureCommandExists(CommandId.DownloadJobLog, AutomationMonitorCommandIcons.DownloadJobJog); }
        }

        /// <summary>
        /// Gets the value that represents the Open Automation Console command.
        /// </summary>
        public static RockstarRoutedCommand OpenAutomationConsole
        {
            get { return EnsureCommandExists(CommandId.OpenAutomationConsole); }
        }

        /// <summary>
        /// Filters jobs by their... jobstate
        /// </summary>
        public static RockstarRoutedCommand FilterByJobState
        {
            get { return EnsureCommandExists(CommandId.FilterByJobState); }
        }

        /*
/// <summary>
/// Opens the download folder for the job
/// </summary>
public static RockstarRoutedCommand OpenJobFolder
{
    get { return EnsureCommandExists(CommandId.OpenJobFolder); }
}


/// <summary>
/// Opens the job uLog in the Universal Log Viewer
/// </summary>
public static RockstarRoutedCommand OpenJobJog
{
    get { return EnsureCommandExists(CommandId.OpenJobLog); }
}*/

        /// <summary>
        /// Deletes the downloaded files for the job
        /// </summary>
        public static RockstarRoutedCommand DeleteFiles
        {
            get { return EnsureCommandExists(CommandId.DeleteFiles); }
        }

        /// <summary>
        /// 
        /// </summary>
        public static RockstarRoutedCommand DeleteAll
        {
            get { return EnsureCommandExists(CommandId.DeleteAll); }
        }
        #endregion // Properties

        #region Private Methods
        /// <summary>
        /// Creates a new System.Windows.Input.RoutedCommand object that is associated with the
        /// specified identifier.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command to create.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        /// <returns>
        /// The newly create System.Windows.Input.RoutedCommand object.
        /// </returns>
        private static RockstarRoutedCommand CreateCommand(String id, BitmapSource icon)
        {
            String name = _commandResourceManager.GetString(id, CommandStringCategory.Name);
            String category = _commandResourceManager.GetString(id, CommandStringCategory.Category);
            String description = _commandResourceManager.GetString(id, CommandStringCategory.Description);
            InputGestureCollection gestures = _commandResourceManager.GetGestures(id);
            return new RockstarRoutedCommand(
                name, typeof(AutomationMonitorCommands), gestures, category, description, icon);
        }

        /// <summary>
        /// Makes sure the internal value for the command associated with the specified
        /// identifier exists and returns it.
        /// </summary>
        /// <param name="commandId">
        /// The identifier for the command that should be created and returned.
        /// </param>
        /// <returns>
        /// The System.Windows.Input.RoutedCommand object associated with the specified
        /// identifier.
        /// </returns>
        private static RockstarRoutedCommand EnsureCommandExists(CommandId commandId, BitmapSource icon = null)
        {
            int commandIndex = (int)commandId;
            if (commandIndex < -1 || commandIndex >= _internalCommands.Length)
            {
                return null;
            }

            RockstarRoutedCommand command = _internalCommands[commandIndex];
            if (command == null)
            {
                lock (_internalCommands)
                {
                    if (command == null)
                    {
                        command = CreateCommand(commandId.ToString(), icon);
                        _internalCommands[commandIndex] = command;
                    }
                }
            }

            return command;
        }

        /// <summary>
        /// Initialises the array used to store the System.Windows.Input.RoutedCommand objects
        /// that make up the core commands for a Rockstar application.
        /// </summary>
        /// <returns>
        /// The array used to store the internal System.Windows.Input.RoutedCommand objects.
        /// </returns>
        private static RoutedCommand[] InitialiseInternalStorage()
        {
            return (new RoutedCommand[(int)CommandId.CommandCount]);
        }
        #endregion // Private Methods
    }

} // RSG.Automation.Commands namespace
