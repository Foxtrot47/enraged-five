﻿//---------------------------------------------------------------------------------------------
// <copyright file="StringTunableViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Tunables
{
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// The view model for a <see cref="StringTunable"/> model object.
    /// </summary>
    public class StringTunableViewModel : TunableViewModelBase<StringTunable>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="StringTunableViewModel"/> class.
        /// </summary>
        /// <param name="tunable">
        /// The string tunable that this view model is representing.
        /// </param>
        /// <param name="parent">
        /// The parent tunable view model for this view model.
        /// </param>
        public StringTunableViewModel(StringTunable tunable, ITunableViewModel parent)
            : base(tunable, parent)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the value currently assigned to the string tunable.
        /// </summary>
        public string Value
        {
            get { return this.Model.Value; }
            set { this.Model.Value = value; }
        }
        #endregion Properties
    } // RSG.Metadata.ViewModel.Tunables.StringTunableViewModel {Class}
} // RSG.Metadata.ViewModel.Tunables {Namespace}
