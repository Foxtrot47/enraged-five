﻿//---------------------------------------------------------------------------------------------
// <copyright file="DirectionalDropAdorner.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Chromes
{
    using System;
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Represents a adorner decorator that visually shows the direction a dropped item will
    /// fall relative to the base control.
    /// </summary>
    public class DirectionalDropAdorner : Control
    {
        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="DirectionalDropAdorner"/> class.
        /// </summary>
        static DirectionalDropAdorner()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(DirectionalDropAdorner),
                new FrameworkPropertyMetadata(typeof(DirectionalDropAdorner)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="DirectionalDropAdorner"/> class.
        /// </summary>
        public DirectionalDropAdorner()
        {
        }
        #endregion Constructors
    } // RSG.Editor.Controls.Chromes.DirectionalDropAdorner {Class}
} // RSG.Editor.Controls.Chromes {Namespace}
