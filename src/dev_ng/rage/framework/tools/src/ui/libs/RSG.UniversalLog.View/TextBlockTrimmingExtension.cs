﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace RSG.UniversalLog.View
{
    /// <summary>
    /// Attached property provider which adds the attached property
    /// <c>TextBlockTrimmingExtension.IsTextTrimmed</c> to the framework's <see cref="TextBlock"/> control.
    /// Based off of the article found here:
    /// http://tranxcoder.wordpress.com/2008/10/12/customizing-lookful-wpf-controls-take-2/
    /// </summary>
    public class TextBlockTrimmingExtension
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        static TextBlockTrimmingExtension()
        {
            // Register for the SizeChanged event on all TextBlocks, even if the event was handled.
            EventManager.RegisterClassHandler(
                typeof(LogMessageTextBlock),
                FrameworkElement.SizeChangedEvent,
                new SizeChangedEventHandler(OnTextBlockSizeChanged),
                true);
        }
        #endregion // Constructor(s)

        #region Attached Property [TextBlockTrimmingExtension.IsTextTrimmed]
        /// <summary>
        /// Key returned upon registering the read-only attached property <c>IsTextTrimmed</c>.
        /// </summary>
        public static readonly DependencyProperty IsTextTrimmedProperty = DependencyProperty.RegisterAttached(
            "IsTextTrimmed",
            typeof(bool),
            typeof(TextBlockTrimmingExtension),
            new PropertyMetadata(false));    // defaults to false

        /// <summary>
        /// Returns the current effective value of the IsTextTrimmed attached property.
        /// </summary>
        /// <remarks>Invoked automatically by the framework when databound.</remarks>
        /// <param name="target"><see cref="TextBlock"/> to evaluate</param>
        /// <returns>Effective value of the IsTextTrimmed attached property</returns>
        [AttachedPropertyBrowsableForType(typeof(LogMessageTextBlock))]
        public static Boolean GetIsTextTrimmed(FrameworkElement frameworkElement)
        {
            return (Boolean)frameworkElement.GetValue(IsTextTrimmedProperty);
        }

        /// <summary>
        /// Sets the instance value of read-only dependency property <see cref="IsTextTrimmed"/>.
        /// </summary>
        /// <param name="target">Associated <see cref="TextBlock"/> instance</param>
        /// <param name="value">New value for IsTextTrimmed</param>
        [AttachedPropertyBrowsableForType(typeof(LogMessageTextBlock))]
        public static void SetIsTextTrimmed(FrameworkElement frameworkElement, Boolean value)
        {
            frameworkElement.SetValue(IsTextTrimmedProperty, value);
        }
        #endregion // Attached Property [TextBlockTrimmingExtension.IsTextTrimmed]

        #region Methods
        /// <summary>
        /// Event handler for TextBlock's SizeChanged routed event. Triggers evaluation of the
        /// IsTextTrimmed attached property.
        /// </summary>
        /// <param name="sender">Object where the event handler is attached</param>
        /// <param name="e">Event data</param>
        public static void OnTextBlockSizeChanged(object sender, SizeChangedEventArgs e)
        {
            var textBlock = sender as LogMessageTextBlock;
            if (null == textBlock)
            {
                return;
            }

            if (TextTrimming.None == textBlock.TextTrimming)
            {
                SetIsTextTrimmed(textBlock, false);
            }
            else
            {
                SetIsTextTrimmed(textBlock, CalculateIsTextTrimmed(textBlock));
            }
        }

        /// <summary>
        /// Determines whether or not the text in <paramref name="textBlock"/> is currently being
        /// trimmed due to width or height constraints.
        /// </summary>
        /// <remarks>Does not work properly when TextWrapping is set to WrapWithOverflow.</remarks>
        /// <param name="textBlock"><see cref="TextBlock"/> to evaluate</param>
        /// <returns><c>true</c> if the text is currently being trimmed; otherwise <c>false</c></returns>
        private static bool CalculateIsTextTrimmed(LogMessageTextBlock textBlock)
        {
            if (!textBlock.IsArrangeValid)
            {
                return GetIsTextTrimmed(textBlock);
            }
            if (textBlock.Text == null)
            {
                return false;
            }

            Typeface typeface = new Typeface(
                textBlock.FontFamily,
                textBlock.FontStyle,
                textBlock.FontWeight,
                textBlock.FontStretch);

            // FormattedText is used to measure the whole width of the text held up by TextBlock container
            FormattedText formattedText = new FormattedText(
                textBlock.Text,
                System.Threading.Thread.CurrentThread.CurrentCulture,
                textBlock.FlowDirection,
                typeface,
                textBlock.FontSize,
                textBlock.Foreground,
                new NumberSubstitution(),
                (TextFormattingMode)textBlock.GetValue(TextOptions.TextFormattingModeProperty));

            formattedText.MaxTextWidth = textBlock.ActualWidth;

            // When the maximum text width of the FormattedText instance is set to the actual
            // width of the textBlock, if the textBlock is being trimmed to fit then the formatted
            // text will report a larger height than the textBlock. Should work whether the
            // textBlock is single or multi-line.
            return (formattedText.Height > textBlock.ActualHeight);
        }
        #endregion // Methods
    } // TextBlockTrimmingExtension
}