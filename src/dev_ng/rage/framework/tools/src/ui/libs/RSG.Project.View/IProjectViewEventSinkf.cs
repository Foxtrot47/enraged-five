﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Project.View
{
    /// <summary>
    /// When implemented represents a class that can be registered to the project view to
    /// response to core events firing inside it.
    /// </summary>
    public interface IProjectViewEventSink
    {
        #region Methods
        void OnItemDoubleClicked();
        #endregion Methods
    } // RSG.Project.View.IProjectViewEventSink
} // RSG.Project.View
