﻿//---------------------------------------------------------------------------------------------
// <copyright file="IMultiCommandItem.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System.ComponentModel;
    using System.Windows.Input;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Provides a base class to objects representing a item for a
    /// <see cref="RSG.Editor.MultiCommand"/> definition object.
    /// </summary>
    public interface IMultiCommandItem : INotifyPropertyChanged
    {
        #region Properties
        /// <summary>
        /// Gets the description for this item. This is also what is shown as a tooltip along
        /// with the key gesture if one is set if it is being rendered as a toggle control.
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Gets the System.Windows.Media.Imaging.BitmapSource object that represents the icon
        /// that is used for this item if it is being rendered as a toggle control.
        /// </summary>
        BitmapSource Icon { get; }

        /// <summary>
        /// Gets or sets a value indicating whether this item is currently the selected item on
        /// the owning command definition.
        /// </summary>
        bool IsToggled { get; set; }

        /// <summary>
        /// Gets or sets the System.Windows.Input.KeyGesture object that is associated with
        /// this combo command item.
        /// </summary>
        KeyGesture KeyGesture { get; set; }

        /// <summary>
        /// Gets the parent command definition that this is an item for.
        /// </summary>
        MultiCommand ParentDefinition { get; }

        /// <summary>
        /// Gets or sets the text that is used to display this item inside the combo control.
        /// </summary>
        string Text { get; set; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Gets the command parameter used for this multi command item.
        /// </summary>
        /// <returns>
        /// The command parameter used for this multi command item.
        /// </returns>
        IMultiCommandParameter GetCommandParameter();

        /// <summary>
        /// Sets the <see cref="IsToggled"/> property from an internal operation.
        /// </summary>
        /// <param name="isToggled">
        /// A value to set the <see cref="IsToggled"/> property to.
        /// </param>
        /// <param name="handleSelectionOnParent">
        /// A value indicating whether the toggle change should result in it being handled in
        /// the parent command definition.
        /// </param>
        void SetIsToggled(bool isToggled, bool handleSelectionOnParent);
        #endregion Methods
    } // RSG.Editor.IMultiCommandItem {Interface}
} // RSG.Editor {Namespace}
