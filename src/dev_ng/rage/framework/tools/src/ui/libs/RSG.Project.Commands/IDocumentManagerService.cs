﻿//---------------------------------------------------------------------------------------------
// <copyright file="IDocumentManagerService.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Commands
{
    using System;
using System.Collections.Generic;
using RSG.Editor;
using RSG.Editor.Controls.Dock.ViewModel;
using RSG.Project.ViewModel;

    /// <summary>
    /// When implemented represents a service that contains methods that operate on documents
    /// inside a docking manager and project explorer.
    /// </summary>
    public interface IDocumentManagerService
    {
        #region Events
        /// <summary>
        /// Occurs whenever a collection is about to be closed.
        /// </summary>
        event EventHandler<CollectionClosedEventArgs> CollectionClosing;

        /// <summary>
        /// Occurs whenever a document is about to be closed.
        /// </summary>
        event EventHandler<DocumentClosingEventArgs> DocumentClosing;

        /// <summary>
        /// Occurs whenever a document is opened.
        /// </summary>
        event EventHandler<DocumentEventArgs> DocumentOpened;
        #endregion Events

        #region Properties
        /// <summary>
        /// Gets the currently active document if there is one; otherwise, null.
        /// </summary>
        DocumentItem ActiveDocument { get; }

        /// <summary>
        /// Gets the file backup manager that has beens et for this service.
        /// </summary>
        FileBackupManager BackupManager { get; }

        /// <summary>
        /// Gets the file watcher manager that has beens et for this service.
        /// </summary>
        FileWatcherManager FileWatcher { get; }

        /// <summary>
        /// Gets a iterator around all of the currently opened documents.
        /// </summary>
        IEnumerable<DocumentItem> OpenedDocuments { get; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Determines whether the specified node when opened needs to be attached to a backup
        /// manager. By default this returns false.
        /// </summary>
        /// <param name="node">
        /// The node to test.
        /// </param>
        /// <returns>
        /// True if the specified node when opened needs to be attached to a backup manager;
        /// otherwise, false.
        /// </returns>
        bool AttachToBackupManager(HierarchyNode node);

        /// <summary>
        /// Determines whether the specified node when opened needs to be attached to a file
        /// watcher manager. By default this returns false.
        /// </summary>
        /// <param name="node">
        /// The node to test.
        /// </param>
        /// <returns>
        /// True if the specified node when opened needs to be attached to a file watcher;
        /// otherwise, false.
        /// </returns>
        bool AttachToFileWatcher(HierarchyNode node);

        /// <summary>
        /// Determines whether the specified node can be opened.
        /// </summary>
        /// <param name="node">
        /// The hierarchy node to test.
        /// </param>
        /// <returns>
        /// True if the specified node can be opened; otherwise, false.
        /// </returns>
        bool CanOpenHierarchyNode(HierarchyNode node);

        /// <summary>
        /// Closes the active document optionally giving the user a chance to save if
        /// modifications are present.
        /// </summary>
        /// <param name="checkModifications">
        /// A value indicating whether the user should be given a chance to save if there are
        /// modifications.
        /// </param>
        void CloseActiveDocument(bool checkModifications);

        /// <summary>
        /// Closes the specified collection optionally giving the user a chance to save if
        /// modifications are present.
        /// </summary>
        /// <param name="collection">
        /// The collection to close.
        /// </param>
        /// <param name="checkModifications">
        /// A value indicating whether the user should be given a chance to save if there are
        /// modifications.
        /// </param>
        void CloseCollection(ProjectCollectionNode collection, bool checkModifications);

        /// <summary>
        /// Closes the specified document optionally giving the user a chance to save if
        /// modifications are present.
        /// </summary>
        /// <param name="document">
        /// The document to close.
        /// </param>
        /// <param name="checkModifications">
        /// A value indicating whether the user should be given a chance to save if there are
        /// modifications.
        /// </param>
        void CloseDocument(DocumentItem document, bool checkModifications);

        /// <summary>
        /// Closes all of the specified documents optionally giving the user a chance to save
        /// if modifications are present.
        /// </summary>
        /// <param name="documents">
        /// The documents to close.
        /// </param>
        /// <param name="checkModifications">
        /// A value indicating whether the user should be given a chance to save if there are
        /// modifications.
        /// </param>
        void CloseDocuments(IList<DocumentItem> documents, bool checkModifications);

        /// <summary>
        /// Closes all opened documents optionally giving the user a chance to save if
        /// modifications are present.
        /// </summary>
        /// <param name="checkModifications">
        /// A value indicating whether the user should be given a chance to save if there are
        /// modifications.
        /// </param>
        void CloseOpenedDocuments(bool checkModifications);

        /// <summary>
        /// Attempts to find the opened document that is associated with the specified node.
        /// </summary>
        /// <param name="node">
        /// The node whose associated document should be returned.
        /// </param>
        /// <returns>
        /// The document that is associated with the specified node; otherwise, null.
        /// </returns>
        DocumentItem FindDocument(IHierarchyNode node);

        /// <summary>
        /// Fires the <see cref="DocumentOpened"/> event.
        /// </summary>
        /// <param name="item">
        /// The document item that has been opened.
        /// </param>
        void FireDocumentOpenedEvent(DocumentItem item);

        /// <summary>
        /// Retrieves a value indicating whether the specified document is modified.
        /// </summary>
        /// <param name="document">
        /// The document to test.
        /// </param>
        /// <returns>
        /// True if the specified document is modified; otherwise, false.
        /// </returns>
        bool IsDocumentModified(DocumentItem document);

        /// <summary>
        /// Retrieves a value indicating whether the specified project is modified.
        /// </summary>
        /// <param name="project">
        /// The project to test.
        /// </param>
        /// <returns>
        /// True if the specified project is modified; otherwise, false.
        /// </returns>
        bool IsProjectModified(ProjectNode project);

        /// <summary>
        /// Retrieves a value indicating whether the specified collection is modified.
        /// </summary>
        /// <param name="collection">
        /// The collection to test.
        /// </param>
        /// <returns>
        /// True if the specified collection is modified; otherwise, false.
        /// </returns>
        bool IsProjectCollectionModified(ProjectCollectionNode collection);

        /// <summary>
        /// Opens the specified hierarchy node.
        /// </summary>
        /// <param name="node">
        /// The node to open.
        /// </param>
        void OpenHierarchyNode(IHierarchyNode node);

        /// <summary>
        /// Saves the currently active document.
        /// </summary>
        /// <param name="forceSaveAs">
        /// A value indicating whether this should force the user to select a location for the
        /// saved document in windows explorer.
        /// </param>
        void SaveActiveDocument(bool forceSaveAs);

        /// <summary>
        /// Saves the currently active document forcing the user to select a location for the
        /// saved document in windows explorer.
        /// </summary>
        void SaveActiveDocumentAs();

        /// <summary>
        /// Saves the specified collection.
        /// </summary>
        /// <param name="collection">
        /// The collection that will be saved.
        /// </param>
        /// <param name="forceSaveAs">
        /// A value indicating whether this should force the user to select a location for the
        /// saved collection in windows explorer.
        /// </param>
        void SaveCollection(ProjectCollectionNode collection, bool forceSaveAs);

        /// <summary>
        /// Saves the specified document.
        /// </summary>
        /// <param name="document">
        /// The document that will be saved.
        /// </param>
        /// <param name="forceSaveAs">
        /// A value indicating whether this should force the user to select a location for the
        /// saved document in windows explorer.
        /// </param>
        void SaveDocument(DocumentItem document, bool forceSaveAs);

        /// <summary>
        /// Saves all of the specified documents.
        /// </summary>
        /// <param name="documents">
        /// The documents to save.
        /// </param>
        /// <param name="forceSaveAs">
        /// A value indicating whether this should force the user to select a location for the
        /// saved documents in windows explorer.
        /// </param>
        void SaveDocuments(IList<DocumentItem> documents, bool forceSaveAs);

        /// <summary>
        /// Saves the specified item.
        /// </summary>
        /// <param name="item">
        /// The item to save.
        /// </param>
        /// <param name="forceSaveAs">
        /// A value indicating whether this should force the user to select a location for the
        /// saved item in windows explorer.
        /// </param>
        void SaveItem(IHierarchyNode item, bool forceSaveAs);

        /// <summary>
        /// Saves all of the specified items.
        /// </summary>
        /// <param name="items">
        /// The items to save.
        /// </param>
        /// <param name="forceSaveAs">
        /// A value indicating whether this should force the user to select a location for the
        /// saved items in windows explorer.
        /// </param>
        void SaveItems(IEnumerable<IHierarchyNode> items, bool forceSaveAs);

        /// <summary>
        /// Saves the specified project.
        /// </summary>
        /// <param name="project">
        /// The project that will be saved.
        /// </param>
        /// <param name="forceSaveAs">
        /// A value indicating whether this should force the user to select a location for the
        /// saved project in windows explorer.
        /// </param>
        void SaveProject(ProjectNode project, bool forceSaveAs);

        /// <summary>
        /// Sets the backup manager to use for the documents created using this manager.
        /// </summary>
        /// <param name="backupManager">
        /// The instance of the backup manager to use.
        /// </param>
        void SetBackupManager(FileBackupManager backupManager);

        /// <summary>
        /// Sets the file watcher to use for the documents created using this manager.
        /// </summary>
        /// <param name="fileWatcher">
        /// The instance of the file watcher to use.
        /// </param>
        void SetFileWatcher(FileWatcherManager fileWatcher);

        /// <summary>
        /// Sets the view site to use for the documents created using this manager.
        /// </summary>
        /// <param name="viewSite">
        /// The instance of the viewSite site to use.
        /// </param>
        void SetViewSite(ViewSite viewSite);

        /// <summary>
        /// Unloaded the specified project making sure that all files from the project are
        /// closed as well.
        /// </summary>
        /// <param name="project">
        /// The project to unload.
        /// </param>
        /// <param name="checkModifications">
        /// A value indicating whether the user should be given a chance to save if there are
        /// modifications.
        /// </param>
        void UnloadProject(ProjectNode project, bool checkModifications);

        /// <summary>
        /// Unloaded the specified project making sure that all files from the project are
        /// closed as well.
        /// </summary>
        /// <param name="projects">
        /// The project to unload.
        /// </param>
        /// <param name="checkModifications">
        /// A value indicating whether the user should be given a chance to save if there are
        /// modifications.
        /// </param>
        void UnloadProjects(IList<ProjectNode> projects, bool checkModifications);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        System.Windows.FrameworkElement GetControlForDocument(object content);
        #endregion Methods
    } // RSG.Project.Commands.IDocumentManagerService {Interface}
} // RSG.Project.Commands {Namespace}
