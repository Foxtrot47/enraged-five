﻿//---------------------------------------------------------------------------------------------
// <copyright file="SystemDropShadowChrome.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Chromes
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;

    /// <summary>
    /// Light weight decorator used to display a drop shadow for specific content.
    /// </summary>
    public class SystemDropShadowChrome : Decorator
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="Colour"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ColourProperty;

        /// <summary>
        /// Identifies the <see cref="CornerRadius"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty CornerRadiusProperty;

        /// <summary>
        /// Identifies the <see cref="ShadowDepth"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ShadowDepthProperty;

        /// <summary>
        /// A static cache of brushes that can be used between shadow chromes that have the
        /// same colour and corner radius properties.
        /// </summary>
        private static Brush[] _commonBrushes;

        /// <summary>
        /// A static value that specifies the corner radius used to create the static
        /// <see cref="_commonBrushes"/> cache.
        /// </summary>
        private static CornerRadius _commonCornerRadius;

        /// <summary>
        /// Generic static object used to synchronize access to the resources.
        /// </summary>
        private static object _syncRoot = new object();

        /// <summary>
        /// The private array that holds the brushes needed to render this particular shadow
        /// chrome if different from the common brushes.
        /// </summary>
        private Brush[] _customBrushes;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="SystemDropShadowChrome" /> class.
        /// </summary>
        static SystemDropShadowChrome()
        {
            ColourProperty =
                DependencyProperty.Register(
                "Colour",
                typeof(Color),
                typeof(SystemDropShadowChrome),
                new FrameworkPropertyMetadata(
                    Color.FromArgb(113, 0, 0, 0),
                    FrameworkPropertyMetadataOptions.AffectsRender,
                    new PropertyChangedCallback(SystemDropShadowChrome.ClearBrushes)));

            ShadowDepthProperty =
                DependencyProperty.Register(
                "ShadowDepth",
                typeof(double),
                typeof(SystemDropShadowChrome),
                new FrameworkPropertyMetadata(
                    5.0,
                    FrameworkPropertyMetadataOptions.AffectsRender));

            CornerRadiusProperty =
                DependencyProperty.Register(
                "CornerRadius",
                typeof(CornerRadius),
                typeof(SystemDropShadowChrome),
                new FrameworkPropertyMetadata(
                    default(CornerRadius),
                    FrameworkPropertyMetadataOptions.AffectsRender,
                    new PropertyChangedCallback(
                        SystemDropShadowChrome.ClearBrushes)),
                        new ValidateValueCallback(SystemDropShadowChrome.IsCornerRadiusValid));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="SystemDropShadowChrome" /> class.
        /// </summary>
        public SystemDropShadowChrome()
        {
        }
        #endregion Constructors

        #region Enums
        /// <summary>
        /// Defines all of the constants that can be used to index the brushes array for a
        /// brush to use in a specific location on the shadow chrome.
        /// </summary>
        private enum BrushArrayIndex
        {
            /// <summary>
            /// The index to use on the brushes array to get the brush used for the top left
            /// part of the shadow.
            /// </summary>
            TopLeft = 0,

            /// <summary>
            /// The index to use on the brushes array to get the brush used for the top part
            /// of the shadow.
            /// </summary>
            Top,

            /// <summary>
            /// The index to use on the brushes array to get the brush used for the top right
            /// part of the shadow.
            /// </summary>
            TopRight,

            /// <summary>
            /// The index to use on the brushes array to get the brush used for the left part
            /// of the shadow.
            /// </summary>
            Left,

            /// <summary>
            /// The index to use on the brushes array to get the brush used for the centre
            /// portion of the shadow.
            /// </summary>
            Centre,

            /// <summary>
            /// The index to use on the brushes array to get the brush used for the right part
            /// of the shadow.
            /// </summary>
            Right,

            /// <summary>
            /// The index to use on the brushes array to get the brush used for the bottom
            /// left part of the shadow.
            /// </summary>
            BottomLeft,

            /// <summary>
            /// The index to use on the brushes array to get the brush used for the bottom
            /// part of the shadow.
            /// </summary>
            Bottom,

            /// <summary>
            /// The index to use on the brushes array to get the brush used for the bottom
            /// right part of the shadow.
            /// </summary>
            BottomRight,

            /// <summary>
            /// The number of brushes that are used to render the shadow chrome.
            /// </summary>
            BrushCount
        }
        #endregion Enums

        #region Properties
        /// <summary>
        /// Gets or sets the colour the shadow should be drawn with.
        /// </summary>
        public Color Colour
        {
            get { return (Color)this.GetValue(ColourProperty); }
            set { this.SetValue(ColourProperty, value); }
        }

        /// <summary>
        /// Gets or sets the corner radius that the shadow should have.
        /// </summary>
        public CornerRadius CornerRadius
        {
            get { return (CornerRadius)this.GetValue(CornerRadiusProperty); }
            set { this.SetValue(CornerRadiusProperty, value); }
        }

        /// <summary>
        /// Gets or sets the depth of the shadow. The amount of pixels the shadows drop off
        /// gradient lasts for.
        /// </summary>
        public double ShadowDepth
        {
            get { return (double)this.GetValue(ShadowDepthProperty); }
            set { this.SetValue(ShadowDepthProperty, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Renders this element using the specified drawing context.
        /// </summary>
        /// <param name="drawingContext">
        /// The drawing instructions for a specific element. This context is provided to the
        /// layout system.
        /// </param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (drawingContext == null)
            {
                throw new SmartArgumentNullException(() => drawingContext);
            }

            Point offset = new Point(this.Margin.Right, this.Margin.Bottom);
            Size size = new Size(this.RenderSize.Width, this.RenderSize.Height);
            Rect rect = new Rect(offset, size);
            Color colour = this.Colour;
            if (rect.Width <= 0.0 || rect.Height <= 0.0 || colour.A <= 0)
            {
                return;
            }

            CornerRadius cornerRadius = this.CornerRadius;
            double centreWidth = rect.Right - rect.Left - this.ShadowDepth - this.ShadowDepth;
            double centreHeight = rect.Bottom - rect.Top - this.ShadowDepth - this.ShadowDepth;
            double smallestExtent = Math.Min(centreWidth * 0.5, centreHeight * 0.5);
            cornerRadius.TopLeft = Math.Min(cornerRadius.TopLeft, smallestExtent);
            cornerRadius.TopLeft = Math.Max(cornerRadius.TopLeft, 0.0);
            cornerRadius.TopRight = Math.Min(cornerRadius.TopRight, smallestExtent);
            cornerRadius.TopRight = Math.Max(cornerRadius.TopRight, 0.0);
            cornerRadius.BottomLeft = Math.Min(cornerRadius.BottomLeft, smallestExtent);
            cornerRadius.BottomLeft = Math.Max(cornerRadius.BottomLeft, 0.0);
            cornerRadius.BottomRight = Math.Min(cornerRadius.BottomRight, smallestExtent);
            cornerRadius.BottomRight = Math.Max(cornerRadius.BottomRight, 0.0);
            double[] array = new double[]
            {
                rect.Left + this.ShadowDepth,
                rect.Left + this.ShadowDepth + cornerRadius.TopLeft,
                rect.Right - this.ShadowDepth - cornerRadius.TopRight,
                rect.Left + this.ShadowDepth + cornerRadius.BottomLeft,
                rect.Right - this.ShadowDepth - cornerRadius.BottomRight,
                rect.Right - this.ShadowDepth
            };

            double[] array2 = new double[]
            {
                rect.Top + this.ShadowDepth,
                rect.Top + this.ShadowDepth + cornerRadius.TopLeft,
                rect.Top + this.ShadowDepth + cornerRadius.TopRight,
                rect.Bottom - this.ShadowDepth - cornerRadius.BottomLeft,
                rect.Bottom - this.ShadowDepth - cornerRadius.BottomRight,
                rect.Bottom - this.ShadowDepth
            };

            Brush[] brushes = this.GetBrushes(colour, cornerRadius);
            drawingContext.PushGuidelineSet(new GuidelineSet(array, array2));
            this.DrawTopSection(drawingContext, cornerRadius, rect, brushes);
            this.DrawSides(drawingContext, cornerRadius, rect, brushes);
            this.DrawBottomSection(drawingContext, cornerRadius, rect, brushes);
            this.DrawCentre(drawingContext, cornerRadius, rect, brushes);
            drawingContext.Pop();
        }

        /// <summary>
        /// Clears the cached brushes used to render the shadow chrome whenever the
        /// <see cref="Colour"/> or <see cref="CornerRadiusProperty"/> properties change.
        /// </summary>
        /// <param name="s">
        /// The object instance that the dependency property changed on.
        /// </param>
        /// <param name="e">
        /// The dependency property change arguments.
        /// </param>
        private static void ClearBrushes(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            SystemDropShadowChrome chrome = s as SystemDropShadowChrome;
            if (chrome == null)
            {
                return;
            }

            chrome._customBrushes = null;
        }

        /// <summary>
        /// Creates all of the brushes that are used to render the drop shadow chrome.
        /// </summary>
        /// <param name="oolour">
        /// The colour that has been set on the chrome.
        /// </param>
        /// <param name="cornerRadius">
        /// The corner radius that has been set on the chrome.
        /// </param>
        /// <returns>
        /// A array of all of the brushes that are needed to render the shadow chrome.
        /// </returns>
        private static Brush[] CreateBrushes(Color oolour, CornerRadius cornerRadius)
        {
            Brush[] array = new Brush[(int)BrushArrayIndex.BrushCount];
            array[(int)BrushArrayIndex.Centre] = new SolidColorBrush(oolour);
            array[(int)BrushArrayIndex.Centre].Freeze();

            GradientStopCollection gradientStops = CreateStops(oolour, 0.0);
            array[(int)BrushArrayIndex.Top] = new LinearGradientBrush(
                gradientStops, new Point(0.0, 1.0), new Point(0.0, 0.0));

            array[(int)BrushArrayIndex.Left] = new LinearGradientBrush(
                gradientStops, new Point(1.0, 0.0), new Point(0.0, 0.0));

            array[(int)BrushArrayIndex.Right] = new LinearGradientBrush(
                gradientStops, new Point(0.0, 0.0), new Point(1.0, 0.0));

            array[(int)BrushArrayIndex.Bottom] = new LinearGradientBrush(
                gradientStops, new Point(0.0, 0.0), new Point(0.0, 1.0));

            array[(int)BrushArrayIndex.Top].Freeze();
            array[(int)BrushArrayIndex.Left].Freeze();
            array[(int)BrushArrayIndex.Right].Freeze();
            array[(int)BrushArrayIndex.Bottom].Freeze();

            GradientStopCollection topLeftStops;
            if (cornerRadius.TopLeft == 0.0)
            {
                topLeftStops = gradientStops;
            }
            else
            {
                topLeftStops = CreateStops(oolour, cornerRadius.TopLeft);
            }

            GradientStopCollection topRightStops;
            if (cornerRadius.TopRight == 0.0)
            {
                topRightStops = gradientStops;
            }
            else if (cornerRadius.TopRight == cornerRadius.TopLeft)
            {
                topRightStops = topLeftStops;
            }
            else
            {
                topRightStops = CreateStops(oolour, cornerRadius.TopRight);
            }

            GradientStopCollection bottomLeftStops;
            if (cornerRadius.BottomLeft == 0.0)
            {
                bottomLeftStops = gradientStops;
            }
            else if (cornerRadius.BottomLeft == cornerRadius.TopLeft)
            {
                bottomLeftStops = topLeftStops;
            }
            else if (cornerRadius.BottomLeft == cornerRadius.TopRight)
            {
                bottomLeftStops = topRightStops;
            }
            else
            {
                bottomLeftStops = CreateStops(oolour, cornerRadius.BottomLeft);
            }

            GradientStopCollection bottomRightStops;
            if (cornerRadius.BottomRight == 0.0)
            {
                bottomRightStops = gradientStops;
            }
            else if (cornerRadius.BottomRight == cornerRadius.TopLeft)
            {
                bottomRightStops = topLeftStops;
            }
            else if (cornerRadius.BottomRight == cornerRadius.TopRight)
            {
                bottomRightStops = topRightStops;
            }
            else if (cornerRadius.BottomRight == cornerRadius.BottomLeft)
            {
                bottomRightStops = bottomLeftStops;
            }
            else
            {
                bottomRightStops = CreateStops(oolour, cornerRadius.BottomRight);
            }

            RadialGradientBrush topLeftBrush = new RadialGradientBrush(topLeftStops);
            topLeftBrush.RadiusX = 1.0;
            topLeftBrush.RadiusY = 1.0;
            topLeftBrush.Center = new Point(1.0, 1.0);
            topLeftBrush.GradientOrigin = new Point(1.0, 1.0);
            array[(int)BrushArrayIndex.TopLeft] = topLeftBrush;
            array[(int)BrushArrayIndex.TopLeft].Freeze();

            RadialGradientBrush topRightBrush = new RadialGradientBrush(topRightStops);
            topRightBrush.RadiusX = 1.0;
            topRightBrush.RadiusY = 1.0;
            topRightBrush.Center = new Point(0.0, 1.0);
            topRightBrush.GradientOrigin = new Point(0.0, 1.0);
            array[(int)BrushArrayIndex.TopRight] = topRightBrush;
            array[(int)BrushArrayIndex.TopRight].Freeze();

            RadialGradientBrush bottomLeftBrush = new RadialGradientBrush(bottomLeftStops);
            bottomLeftBrush.RadiusX = 1.0;
            bottomLeftBrush.RadiusY = 1.0;
            bottomLeftBrush.Center = new Point(1.0, 0.0);
            bottomLeftBrush.GradientOrigin = new Point(1.0, 0.0);
            array[(int)BrushArrayIndex.BottomLeft] = bottomLeftBrush;
            array[(int)BrushArrayIndex.BottomLeft].Freeze();

            RadialGradientBrush bottomRightBrush = new RadialGradientBrush(bottomRightStops);
            bottomRightBrush.RadiusX = 1.0;
            bottomRightBrush.RadiusY = 1.0;
            bottomRightBrush.Center = new Point(0.0, 0.0);
            bottomRightBrush.GradientOrigin = new Point(0.0, 0.0);
            array[(int)BrushArrayIndex.BottomRight] = bottomRightBrush;
            array[(int)BrushArrayIndex.BottomRight].Freeze();
            return array;
        }

        /// <summary>
        /// Creates the gradient stop collection for a linear brush using the specified
        /// colour and calculating for the specified corner radius.
        /// </summary>
        /// <param name="c">
        /// The colour to use when creating the gradient stops.
        /// </param>
        /// <param name="cornerRadius">
        /// The corner radius to take into account while creating the gradient stops.
        /// </param>
        /// <returns>
        /// A collection of gradient stops that can be used in a linear gradient brush to
        /// draw with.
        /// </returns>
        private static GradientStopCollection CreateStops(Color c, double cornerRadius)
        {
            double num = 1.0 / (cornerRadius + 5.0);
            GradientStopCollection gradientStopCollection = new GradientStopCollection();
            gradientStopCollection.Add(new GradientStop(c, (0.5 + cornerRadius) * num));
            Color color = c;
            color.A = (byte)(0.74336 * (double)c.A);
            gradientStopCollection.Add(new GradientStop(color, (1.5 + cornerRadius) * num));
            color.A = (byte)(0.38053 * (double)c.A);
            gradientStopCollection.Add(new GradientStop(color, (2.5 + cornerRadius) * num));
            color.A = (byte)(0.12389 * (double)c.A);
            gradientStopCollection.Add(new GradientStop(color, (3.5 + cornerRadius) * num));
            color.A = (byte)(0.02654 * (double)c.A);
            gradientStopCollection.Add(new GradientStop(color, (4.5 + cornerRadius) * num));
            color.A = 0;
            gradientStopCollection.Add(new GradientStop(color, (5.0 + cornerRadius) * num));
            gradientStopCollection.Freeze();
            return gradientStopCollection;
        }

        /// <summary>
        /// Determines whether the specified corner radius is valued of the
        /// <see cref="CornerRadius"/> dependency property.
        /// </summary>
        /// <param name="value">
        /// The value to determine if valid or not.
        /// </param>
        /// <returns>
        /// True if the specified value is a valid value for the <see cref="CornerRadius"/>
        /// dependency property to be set to; otherwise, false.
        /// </returns>
        private static bool IsCornerRadiusValid(object value)
        {
            CornerRadius cornerRadius = (CornerRadius)value;
            if (cornerRadius.TopLeft < 0.0 || double.IsNaN(cornerRadius.TopLeft))
            {
                return false;
            }

            if (cornerRadius.TopRight < 0.0 || double.IsNaN(cornerRadius.TopRight))
            {
                return false;
            }

            if (cornerRadius.BottomLeft < 0.0 || double.IsNaN(cornerRadius.BottomLeft))
            {
                return false;
            }

            if (cornerRadius.BottomRight < 0.0 || double.IsNaN(cornerRadius.BottomRight))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Calculates the actual corner radius to render based on the specified render
        /// bounds and shadow depth.
        /// </summary>
        /// <param name="bounds">
        /// The bounds of the shadow to render.
        /// </param>
        /// <param name="depth">
        /// The shadow depth that is being rendered.
        /// </param>
        /// <returns>
        /// The corner radius that the brushes need to take into account when rendering
        /// the shadow.
        /// </returns>
        private CornerRadius CalculateFinalCornerRadius(Rect bounds, double depth)
        {
            double centreWidth = bounds.Right - bounds.Left - (depth * 2.0);
            double centreHeight = bounds.Bottom - bounds.Top - (depth * 2.0);
            double maximumRadiusValue = Math.Min(centreWidth, centreHeight) * 0.5;
            CornerRadius cornerRadius = this.CornerRadius;
            cornerRadius.TopLeft = Math.Min(cornerRadius.TopLeft, maximumRadiusValue);
            cornerRadius.TopRight = Math.Min(cornerRadius.TopRight, maximumRadiusValue);
            cornerRadius.BottomLeft = Math.Min(cornerRadius.BottomLeft, maximumRadiusValue);
            cornerRadius.BottomRight = Math.Min(cornerRadius.BottomRight, maximumRadiusValue);
            return cornerRadius;
        }

        /// <summary>
        /// Draws the bottom section of the shadow.
        /// </summary>
        /// <param name="dc">
        /// The drawing context to use.
        /// </param>
        /// <param name="cornerRadius">
        /// The final corner radius that has been calculated for the shadow.
        /// </param>
        /// <param name="rect">
        /// The bounds to the shadow.
        /// </param>
        /// <param name="brushes">
        /// The brushes that are to be used to draw the shadow.
        /// </param>
        private void DrawBottomSection(
            DrawingContext dc, CornerRadius cornerRadius, Rect rect, Brush[] brushes)
        {
            double centerStart = rect.Left + this.ShadowDepth + cornerRadius.BottomLeft;
            double centerFinish = rect.Right - this.ShadowDepth - cornerRadius.BottomRight;
            double leftSize = cornerRadius.BottomLeft + this.ShadowDepth;
            double rightSize = cornerRadius.BottomRight + this.ShadowDepth;

            Rect left = new Rect()
            {
                X = rect.Left,
                Y = rect.Bottom - this.ShadowDepth - cornerRadius.BottomLeft,
                Width = leftSize,
                Height = leftSize
            };

            Rect right = new Rect()
            {
                X = centerFinish,
                Y = rect.Bottom - this.ShadowDepth - cornerRadius.BottomRight,
                Width = rightSize,
                Height = rightSize
            };

            dc.DrawRectangle(brushes[(int)BrushArrayIndex.BottomLeft], null, left);
            dc.DrawRectangle(brushes[(int)BrushArrayIndex.BottomRight], null, right);
            double centreWidth = centerFinish - centerStart;
            if (centreWidth <= 0.0)
            {
                return;
            }

            Rect centre = new Rect()
            {
                X = centerStart,
                Y = rect.Bottom - this.ShadowDepth,
                Width = centreWidth,
                Height = this.ShadowDepth
            };

            dc.DrawRectangle(brushes[(int)BrushArrayIndex.Bottom], null, centre);
        }

        /// <summary>
        /// Draws the centre of the shadow.
        /// </summary>
        /// <param name="dc">
        /// The drawing context to use.
        /// </param>
        /// <param name="cornerRadius">
        /// The final corner radius that has been calculated for the shadow.
        /// </param>
        /// <param name="rect">
        /// The bounds to the shadow.
        /// </param>
        /// <param name="brushes">
        /// The brushes that are to be used to draw the shadow.
        /// </param>
        private void DrawCentre(
            DrawingContext dc, CornerRadius cornerRadius, Rect rect, Brush[] brushes)
        {
            double height = rect.Bottom - rect.Top - this.ShadowDepth - this.ShadowDepth;
            double width = rect.Right - rect.Left - this.ShadowDepth - this.ShadowDepth;
            Rect centre = new Rect()
            {
                X = rect.Left + this.ShadowDepth,
                Y = rect.Top + this.ShadowDepth,
                Width = Math.Max(width, 0.0),
                Height = Math.Max(height, 0.0)
            };

            if (cornerRadius == new CornerRadius(0))
            {
                dc.DrawRectangle(brushes[(int)BrushArrayIndex.Centre], null, centre);
                return;
            }

            Point[] innerCentre = new Point[]
            {
                new Point()
                {
                    X = centre.Left + cornerRadius.TopLeft,
                    Y = centre.Top + cornerRadius.TopLeft
                },

                new Point()
                {
                    X = centre.Left + cornerRadius.BottomLeft,
                    Y = centre.Bottom - cornerRadius.BottomLeft
                },

                new Point()
                {
                    X = centre.Right - cornerRadius.BottomRight,
                    Y = centre.Bottom - cornerRadius.BottomRight
                },

                new Point()
                {
                    X = centre.Right - cornerRadius.TopRight,
                    Y = centre.Top + cornerRadius.TopRight
                }
            };

            Point[] path = new Point[]
            {
                new Point(innerCentre[0].X, centre.Top),
                innerCentre[0],
                new Point(centre.Left, innerCentre[0].Y),

                new Point(centre.Left, innerCentre[1].Y),
                innerCentre[1],
                new Point(innerCentre[1].X, centre.Bottom),

                new Point(innerCentre[2].X, centre.Bottom),
                innerCentre[2],
                new Point(centre.Right, innerCentre[2].Y),

                new Point(centre.Right, innerCentre[3].Y),
                innerCentre[3],
                new Point(innerCentre[3].X, centre.Top),
            };

            PathFigure figure = new PathFigure();
            if (cornerRadius.TopLeft > 0.0)
            {
                figure.StartPoint = path[0];
                figure.Segments.Add(new LineSegment(path[1], true));
                figure.Segments.Add(new LineSegment(path[2], true));
            }
            else
            {
                figure.StartPoint = centre.TopLeft;
            }

            if (cornerRadius.BottomLeft > 0.0)
            {
                figure.Segments.Add(new LineSegment(path[3], true));
                figure.Segments.Add(new LineSegment(path[4], true));
                figure.Segments.Add(new LineSegment(path[5], true));
            }
            else
            {
                figure.Segments.Add(new LineSegment(centre.BottomLeft, true));
            }

            if (cornerRadius.BottomRight > 0.0)
            {
                figure.Segments.Add(new LineSegment(path[6], true));
                figure.Segments.Add(new LineSegment(path[7], true));
                figure.Segments.Add(new LineSegment(path[8], true));
            }
            else
            {
                figure.Segments.Add(new LineSegment(centre.BottomRight, true));
            }

            if (cornerRadius.TopRight > 5.0)
            {
                figure.Segments.Add(new LineSegment(path[9], true));
                figure.Segments.Add(new LineSegment(path[10], true));
                figure.Segments.Add(new LineSegment(path[11], true));
            }
            else
            {
                figure.Segments.Add(new LineSegment(centre.TopRight, true));
            }

            figure.IsClosed = true;
            figure.Freeze();
            PathGeometry pathGeometry = new PathGeometry();
            pathGeometry.Figures.Add(figure);
            pathGeometry.Freeze();
            dc.DrawGeometry(brushes[(int)BrushArrayIndex.Centre], null, pathGeometry);
        }

        /// <summary>
        /// Draws the right and left section of the shadow.
        /// </summary>
        /// <param name="dc">
        /// The drawing context to use.
        /// </param>
        /// <param name="cornerRadius">
        /// The final corner radius that has been calculated for the shadow.
        /// </param>
        /// <param name="rect">
        /// The bounds to the shadow.
        /// </param>
        /// <param name="brushes">
        /// The brushes that are to be used to draw the shadow.
        /// </param>
        private void DrawSides(
            DrawingContext dc, CornerRadius cornerRadius, Rect rect, Brush[] brushes)
        {
            double leftStart = rect.Top + this.ShadowDepth + cornerRadius.TopLeft;
            double leftEnd = rect.Bottom - this.ShadowDepth - cornerRadius.BottomLeft;
            double leftHeight = leftEnd - leftStart;
            if (leftHeight > 0.0)
            {
                Rect left = new Rect(rect.Left, leftStart, this.ShadowDepth, leftHeight);
                dc.DrawRectangle(brushes[(int)BrushArrayIndex.Left], null, left);
            }

            double rightStart = rect.Top + this.ShadowDepth + cornerRadius.TopRight;
            double rightEnd = rect.Bottom - this.ShadowDepth - cornerRadius.BottomRight;
            double rightHeight = rightEnd - rightStart;
            if (rightHeight <= 0.0)
            {
                return;
            }

            Rect right = new Rect()
            {
                X = rect.Right - this.ShadowDepth,
                Y = rightStart,
                Width = this.ShadowDepth,
                Height = rightHeight
            };

            dc.DrawRectangle(brushes[(int)BrushArrayIndex.Right], null, right);
        }

        /// <summary>
        /// Draws the top section of the shadow.
        /// </summary>
        /// <param name="dc">
        /// The drawing context to use.
        /// </param>
        /// <param name="cornerRadius">
        /// The final corner radius that has been calculated for the shadow.
        /// </param>
        /// <param name="rect">
        /// The bounds to the shadow.
        /// </param>
        /// <param name="brushes">
        /// The brushes that are to be used to draw the shadow.
        /// </param>
        private void DrawTopSection(
            DrawingContext dc, CornerRadius cornerRadius, Rect rect, Brush[] brushes)
        {
            double centerStart = rect.Left + this.ShadowDepth + cornerRadius.TopLeft;
            double centerFinish = rect.Right - this.ShadowDepth - cornerRadius.TopRight;
            double leftSize = cornerRadius.TopLeft + this.ShadowDepth;
            double rightSize = cornerRadius.TopRight + this.ShadowDepth;

            Rect left = new Rect()
            {
                X = rect.Left,
                Y = rect.Top,
                Width = leftSize,
                Height = leftSize
            };

            Rect right = new Rect()
            {
                X = centerFinish,
                Y = rect.Top,
                Width = rightSize,
                Height = rightSize
            };

            dc.DrawRectangle(brushes[(int)BrushArrayIndex.TopLeft], null, left);
            dc.DrawRectangle(brushes[(int)BrushArrayIndex.TopRight], null, right);
            double centreWidth = centerFinish - centerStart;
            if (centreWidth <= 0.0)
            {
                return;
            }

            Rect centre = new Rect(centerStart, rect.Top, centreWidth, this.ShadowDepth);
            dc.DrawRectangle(brushes[(int)BrushArrayIndex.Top], null, centre);
        }

        /// <summary>
        /// Gets all of the brushes that are used to render the drop shadow chrome by either
        /// retrieving them after already being created or creating new ones.
        /// </summary>
        /// <param name="colour">
        /// The colour that has been set on the chrome.
        /// </param>
        /// <param name="cornerRadius">
        /// The corner radius that has been set on the chrome.
        /// </param>
        /// <returns>
        /// A array of all of the brushes that are needed to render the shadow chrome.
        /// </returns>
        private Brush[] GetBrushes(Color colour, CornerRadius cornerRadius)
        {
            if (_commonBrushes == null)
            {
                lock (_syncRoot)
                {
                    if (_commonBrushes == null)
                    {
                        _commonBrushes = CreateBrushes(colour, cornerRadius);
                        _commonCornerRadius = cornerRadius;
                    }
                }
            }

            Brush centre = _commonBrushes[(int)BrushArrayIndex.Centre];
            SolidColorBrush solidBrush = centre as SolidColorBrush;
            if (solidBrush != null && solidBrush.Color == colour)
            {
                if (cornerRadius == _commonCornerRadius)
                {
                    this._customBrushes = null;
                    return _commonBrushes;
                }
            }

            if (this._customBrushes == null)
            {
                this._customBrushes = CreateBrushes(colour, cornerRadius);
            }

            return this._customBrushes;
        }
        #endregion Methods
    } // RSG.Editor.Controls.Chromes.SystemDropShadowChrome {Class}
} // RSG.Editor.Controls.Chromes {Namespace}
