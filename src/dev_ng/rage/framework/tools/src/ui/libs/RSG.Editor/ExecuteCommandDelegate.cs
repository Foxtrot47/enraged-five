﻿//---------------------------------------------------------------------------------------------
// <copyright file="ExecuteCommandDelegate.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    /// <summary>
    /// Represents the method that will handle the implementation of a command.
    /// </summary>
    /// <param name="e">
    /// The object containing the data to use inside the implementation method.
    /// </param>
    public delegate void ExecuteCommandDelegate(ExecuteCommandData e);
} // RSG.Editor {Namespace}
