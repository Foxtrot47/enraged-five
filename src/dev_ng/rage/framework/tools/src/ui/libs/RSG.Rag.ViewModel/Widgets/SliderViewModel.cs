﻿using RSG.Rag.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace RSG.Rag.ViewModel.Widgets
{
    /// <summary>
    /// Base class for the various slider view models.
    /// </summary>
    public abstract class SliderViewModel<T> : WidgetViewModel<WidgetSlider<T>>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        protected SliderViewModel(WidgetSlider<T> widget)
            : base(widget)
        {
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public T Value
        {
            get { return Widget.Value; }
            set { Widget.Value = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public T MinValue
        {
            get { return Widget.MinValue; }
        }

        /// <summary>
        /// 
        /// </summary>
        public T MaxValue
        {
            get { return Widget.MaxValue; }
        }

        /// <summary>
        /// 
        /// </summary>
        public T Step
        {
            get { return Widget.Step; }
        }

        /// <summary>
        /// Flag indicating whether the slider has changed value from it's original one.
        /// </summary>
        public override bool IsModified
        {
            get { return !EqualityComparer<T>.Default.Equals(Widget.Value, Widget.OriginalValue); }
        }

        /// <summary>
        /// 
        /// </summary>
        public override String DefaultValue
        {
            get { return Widget.OriginalValue.ToString(); }
        }
        #endregion // Properties

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        protected override void OnWidgetPropertyChanged(String propertyName)
        {
            if (propertyName == "Value")
            {
                NotifyPropertyChanged("IsModified");
            }

            base.OnWidgetPropertyChanged(propertyName);
        }
        #endregion // Event Handlers
    } // SliderViewModel
}
