﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Editor.Controls.Helpers;
using RSG.Editor.Controls.MapViewport.Commands;

namespace RSG.Editor.Controls.MapViewport.Annotations
{
    /// <summary>
    /// Interaction logic for AnnotationControl.xaml
    /// </summary>
    public partial class AnnotationControl : RsUserControl
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="AnnotationControl"/> class.
        /// </summary>
        public AnnotationControl()
        {
            InitializeComponent();

            // Add the actions for adding/deleting a comment.
            new DeleteAnnotationCommentAction(ResolveAnnotation)
                .AddBinding(MapViewportCommands.DeleteAnnotationComment, this);
            new AddAnnotationCommentAction(ResolveControl)
                .AddBinding(MapViewportCommands.AddAnnotationComment, this);
        }
        #endregion

        #region Action Resolvers
        /// <summary>
        /// Retrieves the annotation that this control is displaying.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public Annotation ResolveAnnotation(CommandData data)
        {
            return DataContext as Annotation;
        }

        /// <summary>
        /// Retrieves the add comment text box.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public AnnotationControl ResolveControl(CommandData data)
        {
            return this;
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Popup popup = this.GetLogicalAncestor<Popup>(false);
            popup.IsOpen = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RsUserControl_Loaded(object sender, RoutedEventArgs e)
        {
            CommentsScrollViewer.ScrollToBottom();
        }
        #endregion
    }
}
