﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace RSG.Rag.ViewModel
{
    /// <summary>
    /// 
    /// </summary>
    public static class CommonRagIcons
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Bank"/> property.
        /// </summary>
        private static BitmapSource _bank;

        /// <summary>
        /// The private field used for the <see cref="Group"/> property.
        /// </summary>
        private static BitmapSource _group;

        /// <summary>
        /// A generic object that provides thread safety access to the image sources.
        /// </summary>
        private static object _syncRoot = new object();
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets the icon that shows a bank icon.
        /// </summary>
        public static BitmapSource Bank
        {
            get
            {
                if (_bank == null)
                {
                    lock (_syncRoot)
                    {
                        if (_bank == null)
                        {
                            _bank = EnsureLoaded("bank16.png");
                        }
                    }
                }

                return _bank;
            }
        }

        /// <summary>
        /// Gets the icon that shows a group icon.
        /// </summary>
        public static BitmapSource Group
        {
            get
            {
                if (_group == null)
                {
                    lock (_syncRoot)
                    {
                        if (_group == null)
                        {
                            _group = EnsureLoaded("group16.png");
                        }
                    }
                }

                return _group;
            }
        }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Ensures that the specified image source is loaded with the specified resource name.
        /// </summary>
        /// <param name="resourceName">
        /// The name of the resource the loaded image source should set as its source.
        /// </param>
        private static BitmapSource EnsureLoaded(String resourceName)
        {
            String assemblyName = typeof(CommonRagIcons).Assembly.GetName().Name;
            String bitmapPath = String.Format("pack://application:,,,/{0};component/Resources/{1}", assemblyName, resourceName);
            Uri uri = new Uri(bitmapPath, UriKind.RelativeOrAbsolute);

            BitmapSource source = new BitmapImage(uri);
            source.Freeze();
            return source;
        }
        #endregion //  Methods
    } // CommonRagIcons
}
