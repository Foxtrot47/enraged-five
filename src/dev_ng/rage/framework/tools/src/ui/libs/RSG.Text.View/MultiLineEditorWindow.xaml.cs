﻿//---------------------------------------------------------------------------------------------
// <copyright file="MultiLineEditorWindow.xaml.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.View
{
    using System.Collections.Generic;
    using System.Windows.Controls;
    using RSG.Editor;
    using RSG.Editor.Controls;
    using RSG.Text.ViewModel;

    /// <summary>
    /// Represents the window used to show a collection of lines that are having a single
    /// property of theirs changed.
    /// </summary>
    public partial class MultiLineEditorWindow : RsWindow
    {
        #region Fields
        /// <summary>
        /// A private reference to the composite property change object currently being shown.
        /// </summary>
        private CompositePropertyChange _propertyChange;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MultiLineEditorWindow"/> class.
        /// </summary>
        public MultiLineEditorWindow()
        {
            this.InitializeComponent();
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Sets the lines that will be affected by this single change.
        /// </summary>
        /// <param name="lines">
        /// The lines that will be affected by this single change.
        /// </param>
        public void SetAffectedLines(IEnumerable<LineViewModel> lines)
        {
            this.AffectedLinesGrid.ItemsSource = lines;
        }

        /// <summary>
        /// Sets the property object that represents the property that is being changed by this
        /// editor.
        /// </summary>
        /// <param name="compositeChange">
        /// The property object that represents the property that is being changed by this
        /// editor.
        /// </param>
        public void SetProperty(CompositePropertyChange compositeChange)
        {
            this._propertyChange = compositeChange;
            DataGridColumn column = compositeChange.Column;
            if (column != null)
            {
                this.AffectedLinesGrid.Columns.Insert(2, column);
            }

            ValueEditor.Content = compositeChange;
        }

        /// <summary>
        /// Determines whether the
        /// <see cref="RSG.Editor.SharedCommands.RockstarViewCommands.ConfirmDialog"/> command
        /// can be executed at this level.
        /// </summary>
        /// <param name="e">
        /// The command data that is past from the command system.
        /// </param>
        /// <returns>
        /// True if the command can be executed here; otherwise, false.
        /// </returns>
        protected override bool CanConfirmDialog(CanExecuteCommandData e)
        {
            return this._propertyChange == null ? true : !this._propertyChange.HasErrors;
        }
        #endregion Methods
    } // RSG.Text.View.MultiLineEditorWindow {Class}
} // RSG.Text.View {Namespace}
