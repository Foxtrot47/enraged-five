﻿//---------------------------------------------------------------------------------------------
// <copyright file="IDockingParent.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock.ViewModel
{
    using System.ComponentModel;

    /// <summary>
    /// When implemented represents a docking element that contains a child collection that can
    /// be manipulated.
    /// </summary>
    public interface IDockingParent : IDockingElement
    {
        #region Methods
        /// <summary>
        /// Moves the item at the specified index one space to the left.
        /// </summary>
        /// <param name="index">
        /// The index of the item to move.
        /// </param>
        void MoveItemLeft(int index);

        /// <summary>
        /// Moves the item at the specified index one space to the right.
        /// </summary>
        /// <param name="index">
        /// The index of the item to move.
        /// </param>
        void MoveItemRight(int index);

        /// <summary>
        /// Removes the specified item from this elements child collection.
        /// </summary>
        /// <param name="item">
        /// The item to remove.
        /// </param>
        void RemoveItem(IDockingPaneItem item);
        #endregion Methods
    } // RSG.Editor.Controls.Dock.ViewModel.IDockingParent {Interface}
} // RSG.Editor.Controls.Dock.ViewModel {Namespace}
