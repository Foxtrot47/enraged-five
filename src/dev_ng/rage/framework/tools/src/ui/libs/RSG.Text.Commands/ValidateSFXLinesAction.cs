﻿//---------------------------------------------------------------------------------------------
// <copyright file="ValidateSFXLinesAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Commands
{
    using RSG.Base.Logging;
    using RSG.Base.Logging.Universal;
    using RSG.Editor;
    using RSG.Text.Commands;
    using RSG.Text.ViewModel;

    /// <summary>
    /// Implements the <see cref="TextCommands.ValidateSfxLines"/> command.
    /// </summary>
    public class ValidateSFXLinesAction : ButtonAction<TextConversationCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ValidateSFXLinesAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public ValidateSFXLinesAction(
            ParameterResolverDelegate<TextConversationCommandArgs> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(TextConversationCommandArgs args)
        {
            return args.Dialogue != null;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(TextConversationCommandArgs args)
        {
            IUniversalLog log = LogFactory.CreateUniversalLog("SFXValidation");
            UniversalLogFile logFile = (UniversalLogFile)LogFactory.CreateUniversalLogFile(log);

            foreach (var conversation in args.Dialogue.Conversations)
            {
                if (conversation.IsRandom)
                {                
                    foreach (var line in conversation.Lines)
                    {
                        if (line.AudioFilepath.StartsWith("SFX"))
                        {
                            log.Error("SFX line in random conversation. Root: {0}.", conversation.Root);
                        }
                    }
                }
                else
                {
                    bool previousSfx = false;
                    foreach (var line in conversation.Lines)
                    {
                        if (line.SelectedCharacter.Name != "SFX")
                        {
                            previousSfx = false;
                            continue;
                        }

                        if (previousSfx)
                        {
                            log.Error("Two SFX lines in a row. Root: {0}.", conversation.Root);
                        }
                        else
                        {
                            previousSfx = true;
                        }
                    }
                }
            }

            if (!log.HasErrors)
            {
                IMessageBoxService msgService = this.GetService<IMessageBoxService>();
                msgService.Show("No validation errors found.");
            }
            else
            {
                LogFactory.ShowUniversalLogViewer(logFile.Filename);
            }
        }
        #endregion Methods
    } // RSG.Text.Commands.AddNewConversationAction {Class}
} // RSG.Text.Commands {Namespace}
