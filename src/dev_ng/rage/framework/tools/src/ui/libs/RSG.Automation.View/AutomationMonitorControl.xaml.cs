﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Automation.ViewModel;
using RSG.Pipeline.Automation.Common.Client;
using RSG.Pipeline.Automation.Common.Tasks;
using RSG.Pipeline.Automation.Consumers;
using RSG.Editor.Controls;
using RSG.Editor;

namespace RSG.Automation.View
{
    /// <summary>
    /// Interaction logic for AutomationMonitorControl.xaml
    /// </summary>
    public partial class AutomationMonitorControl : RsUserControl
    {
        #region Properties
        /// <summary>
        /// Sets the member which is initially sorted ascending.
        /// </summary>
        public InitialJobSortMember InitialSortMember
        {
            set { this._initialSortMember = value; }
        }
        private InitialJobSortMember _initialSortMember;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public AutomationMonitorControl()
        {
            InitializeComponent();
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedItemChanged(Object sender, RoutedPropertyChangedEventArgs<Object> e)
        {
#warning DHM FIX ME: is there a better way to do this?
            
            // Synchronise our selection.
            Debug.Assert(this.DataContext is AutomationMonitorDataContext);
            AutomationMonitorDataContext vm = (AutomationMonitorDataContext)this.DataContext;

            if (e.NewValue is AutomationServiceViewModel)
            {
                AutomationServiceViewModel itemVM = (AutomationServiceViewModel)e.NewValue;
                vm.SelectedAutomationService = itemVM;
                vm.UpdateTitle();
                vm.RefreshAsync();                
            }
            else if (e.NewValue is StudioViewModel)
            {
                // Auto-Select first service.
                //((StudioViewModel)e.NewValue).AutomationServices.First()
            }
        }
        #endregion // Private Methods

        #region Command Implementations
        public AutomationMonitorDataContext DataContextResolver(CommandData data)
        {
            AutomationMonitorDataContext dc = null;
            if (!this.Dispatcher.CheckAccess())
            {
                dc = this.Dispatcher.Invoke<AutomationMonitorDataContext>(() => this.DataContext as AutomationMonitorDataContext);
            }
            else
            {
                dc = this.DataContext as AutomationMonitorDataContext;
            }
            return dc;
        }

        /// <summary>
        /// Returns the JobDataGridControl (Used in the main window for the job state filtering toolbar)
        /// </summary>
        /// <returns></returns>
        public JobDataGridControl GetJobDataGridControl()
        {
            return this.JobDataGrid;
        }
        #endregion
    }

} // RSG.Automation.View namespace
