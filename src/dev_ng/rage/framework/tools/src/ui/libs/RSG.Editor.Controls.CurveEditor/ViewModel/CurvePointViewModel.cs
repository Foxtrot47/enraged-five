﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor.Controls.CurveEditor.Model;
using RSG.Editor.View;

namespace RSG.Editor.Controls.CurveEditor.ViewModel
{
    /// <summary>
    /// 
    /// </summary>
    public class CurvePointViewModel : ViewModelBase<Point>
    {
        #region Member Data
        /// <summary>
        /// Reference to the parent view model.
        /// </summary>
        private CurveViewModel _parent;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public CurvePointViewModel(Point model, CurveViewModel parent)
            : base(model)
        {
            _parent = parent;
            model.PropertyChanged += model_PropertyChanged;
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public CurveViewModel Parent
        {
            get { return _parent; }
        }

        /// <summary>
        /// 
        /// </summary>
        public double X
        {
            get { return Model.PositionX; }
            set
            {
                if (Model.PositionX != value)
                {
                    Model.PositionX = value;
                    NotifyPropertyChanged("X");
                    _parent.UpdateGeometry();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public double Y
        {
            get { return Model.PositionY; }
            set
            {
                if (Model.PositionY != value)
                {
                    Model.PositionY = value;
                    NotifyPropertyChanged("Y", "InvertedY");
                    _parent.UpdateGeometry();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public double InvertedY
        {
            get { return -Model.PositionY; }
            set
            {
                if (Model.PositionY != -value)
                {
                    Model.PositionY = -value;
                    NotifyPropertyChanged("Y", "InvertedY");
                    _parent.UpdateGeometry();
                }
            }
        }
        #endregion // Properties

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void model_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "PositionX")
            {
                NotifyPropertyChanged("X");
                _parent.UpdateGeometry();
            }
            else if (e.PropertyName == "PositionY")
            {
                NotifyPropertyChanged("Y", "InvertedY");
                _parent.UpdateGeometry();
            }
        }
        #endregion // Event Handlers
    }
}
