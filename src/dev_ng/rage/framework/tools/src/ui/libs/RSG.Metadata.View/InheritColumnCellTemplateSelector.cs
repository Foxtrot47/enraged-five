﻿//---------------------------------------------------------------------------------------------
// <copyright file="InheritColumnCellTemplateSelector.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.View
{
    using System.Windows;
    using System.Windows.Controls;
    using RSG.Editor.Controls.Helpers;
    using RSG.Metadata.ViewModel.Tunables;

    /// <summary>
    /// Represents the cell template that is associated with the inherit column in the tunable
    /// structure control.
    /// </summary>
    public class InheritColumnCellTemplateSelector : DataTemplateSelector
    {
        #region Fields
        /// <summary>
        /// The private dictionary containing the data templates that are used for this
        /// selector.
        /// </summary>
        private static DataTemplate _template;
        #endregion Fields

        #region Methods
        /// <summary>
        /// Returns the data template to use for the specified item inside the inherit column
        /// for the tunable structure control.
        /// </summary>
        /// <param name="item">
        /// The data content.
        /// </param>
        /// <param name="container">
        /// The element to which the template will be applied.
        /// </param>
        /// <returns>
        /// The data template to use for the specified item inside the inherit column for the
        /// tunable structure control.
        /// </returns>
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            VirtualisedTreeNode node = item as VirtualisedTreeNode;
            if (node == null)
            {
                return new DataTemplate();
            }

            ITunableViewModel tunableViewModel = node.Item as ITunableViewModel;
            if (tunableViewModel == null)
            {
                return new DataTemplate();
            }

            if (_template == null)
            {
                _template = CreateTemplate(GetRootControl(container));
            }

            return _template;
        }

        /// <summary>
        /// Creates the data template this selector uses for tunable view model items.
        /// </summary>
        /// <param name="control">
        /// The control to use to try and find the templates in.
        /// </param>
        /// <returns>
        /// A data template this selector uses for tunable view model items.
        /// </returns>
        private static DataTemplate CreateTemplate(TunableStructureControl control)
        {
            if (control == null)
            {
                return new DataTemplate();
            }

            object resource = control.TryFindResource("InheritanceEditor");
            DataTemplate template = resource as DataTemplate;
            if (template == null)
            {
                template = new DataTemplate();
            }

            return template;
        }

        /// <summary>
        /// Retrieves the root tunable structure control that the specified container is
        /// inside.
        /// </summary>
        /// <param name="container">
        /// The element to which the template will be applied.
        /// </param>
        /// <returns>
        /// The root tunable structure control that the specified container is inside.
        /// </returns>
        private static TunableStructureControl GetRootControl(DependencyObject container)
        {
            return container.GetVisualAncestor<TunableStructureControl>();
        }
        #endregion Methods
    } // RSG.Metadata.View.InheritColumnCellTemplateSelector {Class}
} // RSG.Metadata.View {Namespace}
