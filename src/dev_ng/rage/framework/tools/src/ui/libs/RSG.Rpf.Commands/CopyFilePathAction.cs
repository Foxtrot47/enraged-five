﻿//---------------------------------------------------------------------------------------------
// <copyright file="CloseFileAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Rpf.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using RSG.Editor;
    using RSG.Editor.SharedCommands;
    using RSG.Rpf.ViewModel;
    using System.Linq;
    using System.Windows;
    using System.IO;

    /// <summary>
    /// Implements the <see cref="RpfViewerCommands.CopyFileName"/> command.
    /// </summary>
    public class CopyFileNameAction : ButtonAction<IEnumerable<PackEntryViewModel>>
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="CopyFileNameAction"/> class.
        /// </summary>
        /// <param name="commandParameter"></param>
        public CopyFileNameAction(
            ParameterResolverDelegate<IEnumerable<PackEntryViewModel>> commandParameter)
            : base(commandParameter)
        {
        }

        /// <summary>
        /// Override to set logic against the can execute command handler.
        /// </summary>
        /// <param name="commandParameter"></param>
        /// <returns>True if the command can be fired; otherwise, false.</returns>
        public override bool CanExecute(IEnumerable<PackEntryViewModel> commandParameter)
        {
            return commandParameter.Count() > 0;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="commandParameter"></param>
        public override void Execute(IEnumerable<PackEntryViewModel> commandParameter)
        {
            List<string> fileNames = new List<string>();

            foreach (var p in commandParameter)
            {
                string file = Path.GetFileName(p.Name);
                fileNames.Add(file);
            }

            if(fileNames.Count > 0)
            {
                string result = string.Join(";", fileNames);
                Clipboard.SetText(result);
            }
        }
    } // RSG.Rpf.Commands.CopyFileName {Class}
} // RSG.Rpf.Commands {Namespace}
