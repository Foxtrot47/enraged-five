﻿//---------------------------------------------------------------------------------------------
// <copyright file="ChangePointerTypeAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Commands
{
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Definitions;
    using RSG.Metadata.ViewModel.Tunables;

    /// <summary>
    /// Contains the logic for the <see cref="MetadataCommands.ChangePointerType"/> routed
    /// command. This class cannot be inherited.
    /// </summary>
    public sealed class ChangePointerTypeAction : MetadataMultiCommandActionBase<IStructure>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ChangePointerTypeAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public ChangePointerTypeAction(MetadataCommandArgsResolver resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="structure">
        /// The structure that the pointer is being changed into.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        protected override bool CanExecute(MetadataCommandArgs args, IStructure structure)
        {
            return true;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="structure">
        /// The structure that the pointer is being changed into.
        /// </param>
        protected override void Execute(MetadataCommandArgs args, IStructure structure)
        {
            using (new UndoRedoBatch(args.UndoEngine))
            {
                foreach (ITunableViewModel tunable in args.SelectedTunables)
                {
                    PointerTunableViewModel pointer = tunable as PointerTunableViewModel;
                    pointer.Model.ChangePointerType(structure);
                }
            }
        }
        #endregion Methods
    } // RSG.Metadata.Commands.ChangePointerTypeAction {Class}
} // RSG.Metadata.Commands {Namespace}
