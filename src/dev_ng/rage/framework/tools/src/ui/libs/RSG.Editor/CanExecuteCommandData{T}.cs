﻿//---------------------------------------------------------------------------------------------
// <copyright file="CanExecuteCommandData{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System.Windows.Input;

    /// <summary>
    /// Provides data to a predicate method which determines whether a associated command can
    /// be executed based on a parameter of the specified type.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the parameter used to determine whether the associated command can be
    /// executed.
    /// </typeparam>
    public class CanExecuteCommandData<T> : CanExecuteCommandData
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="OriginalParameterWasNull"/> property.
        /// </summary>
        private bool _originalParameterWasNull;

        /// <summary>
        /// The private field used for the <see cref="Parameter"/> property.
        /// </summary>
        private T _parameter;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CanExecuteCommandData{T}"/> class.
        /// </summary>
        /// <param name="command">
        /// The command associated with this data.
        /// </param>
        /// <param name="parameter">
        /// The parameter that the predicate method can use to determine whether the command
        /// can be executed.
        /// </param>
        internal CanExecuteCommandData(ICommand command, T parameter)
            : base(command)
        {
            this._parameter = parameter;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether the parameter passed in from the source event data
        /// was set to null or not.
        /// </summary>
        public bool OriginalParameterWasNull
        {
            get { return this._originalParameterWasNull; }
            internal set { this._originalParameterWasNull = value; }
        }

        /// <summary>
        /// Gets the parameter that was associated with the command.
        /// </summary>
        public T Parameter
        {
            get { return this._parameter; }
        }
        #endregion Properties
    } // RSG.Editor.CanExecuteCommandData<T> {Class}
} // RSG.Editor {Namespace}
