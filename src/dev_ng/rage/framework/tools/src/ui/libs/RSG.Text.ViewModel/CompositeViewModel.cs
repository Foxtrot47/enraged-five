﻿//---------------------------------------------------------------------------------------------
// <copyright file="CompositeViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.ViewModel
{
    using RSG.Editor.View;
    using RSG.Text.Model;

    /// <summary>
    /// Provides a base class for a composite view model that's used to group a number the
    /// dialogue configuration values of the same type.
    /// </summary>
    public abstract class CompositeViewModel : ViewModelBase<DialogueConfigurations>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CompositeViewModel"/> class.
        /// </summary>
        /// <param name="configurations">
        /// The dialogue configurations class that the values have come from.
        /// </param>
        protected CompositeViewModel(DialogueConfigurations configurations)
            : base(configurations)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Deletes all of the values that this composite collection currently contains from
        /// the dialogue configuration class.
        /// </summary>
        public virtual void Delete()
        {
        }
        #endregion Methods
    } // RSG.Text.ViewModel.CompositeViewModel {Class}
} // RSG.Text.ViewModel {Namespace}
