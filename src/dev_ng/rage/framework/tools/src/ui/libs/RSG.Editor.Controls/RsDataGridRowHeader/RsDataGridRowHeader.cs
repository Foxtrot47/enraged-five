﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsDataGridRowHeader.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using RSG.Editor.Controls.Helpers;
    using RSG.Interop.Microsoft.Windows;

    /// <summary>
    /// Represents a individual data grid row header that has the ability to be dragged around
    /// its owning data grid.
    /// </summary>
    public class RsDataGridRowHeader : DataGridRowHeader
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="DragCompletedEvent"/> routed event.
        /// </summary>
        public static readonly RoutedEvent DragCompletedEvent;

        /// <summary>
        /// Identifies the <see cref="DragDeltaEvent"/> routed event.
        /// </summary>
        public static readonly RoutedEvent DragDeltaEvent;

        /// <summary>
        /// Identifies the <see cref="DragStartedEvent"/> routed event.
        /// </summary>
        public static readonly RoutedEvent DragStartedEvent;

        /// <summary>
        /// Identifies the <see cref="IsDragEnabled"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsDragEnabledProperty;

        /// <summary>
        /// Identifies the <see cref="IsDragging"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsDraggingProperty;

        /// <summary>
        /// Identifies the SupportsRowReordering attached dependency property.
        /// </summary>
        public static readonly DependencyProperty SupportsRowReorderingProperty;

        /// <summary>
        /// Identifies the <see cref="IsDragging"/> dependency property key.
        /// </summary>
        private static readonly DependencyPropertyKey _isDraggingPropertyKey;

        /// <summary>
        /// The dispatch timer that is used to periodically scroll the owner data grid.
        /// </summary>
        private DispatcherTimer _autoScrollTimer;

        /// <summary>
        /// A value indicating whether a auto scroll operation has been completed.
        /// </summary>
        private bool _hasAutoScrolled;

        /// <summary>
        /// The point in screen space of where the drag operation ended.
        /// </summary>
        private Point _lastScreenPoint;

        /// <summary>
        /// A value indicating whether the mouse moved during the drag operation.
        /// </summary>
        private bool _movedDuringDrag;

        /// <summary>
        /// The point in screen space of where the drag operation started.
        /// </summary>
        private Point _originalScreenPoint;

        /// <summary>
        /// A value indicating whether the drag started event has been raised.
        /// </summary>
        private bool _raisedDragStarted;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsDataGridRowHeader"/> class.
        /// </summary>
        static RsDataGridRowHeader()
        {
            DragStartedEvent =
                EventManager.RegisterRoutedEvent(
                "DragStarted",
                RoutingStrategy.Bubble,
                typeof(EventHandler<RsDataGridDragEventArgs>),
                typeof(RsDataGridRowHeader));

            DragDeltaEvent =
                EventManager.RegisterRoutedEvent(
                "DragDelta",
                RoutingStrategy.Bubble,
                typeof(EventHandler<RsDataGridDragEventArgs>),
                typeof(RsDataGridRowHeader));

            DragCompletedEvent =
                EventManager.RegisterRoutedEvent(
                "DragCompleted",
                RoutingStrategy.Bubble,
                typeof(EventHandler<RsDataGridDragEventArgs>),
                typeof(RsDataGridRowHeader));

            IsDragEnabledProperty =
                DependencyProperty.Register(
                "IsDragEnabled",
                typeof(bool),
                typeof(RsDataGridRowHeader),
                new FrameworkPropertyMetadata(true));

            _isDraggingPropertyKey =
                DependencyProperty.RegisterReadOnly(
                "IsDragging",
                typeof(bool),
                typeof(RsDataGridRowHeader),
                new FrameworkPropertyMetadata(false));

            IsDraggingProperty = _isDraggingPropertyKey.DependencyProperty;

            SupportsRowReorderingProperty =
                DependencyProperty.RegisterAttached(
                "SupportsRowReordering",
                typeof(bool),
                typeof(RsDataGridRowHeader),
                new FrameworkPropertyMetadata(false));

            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsDataGridRowHeader),
                new FrameworkPropertyMetadata(typeof(RsDataGridRowHeader)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsDataGridRowHeader"/> class.
        /// </summary>
        public RsDataGridRowHeader()
        {
            this.SetValue(UIElement.AllowDropProperty, true);
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs when a drag is completed.
        /// </summary>
        public event EventHandler<RsDataGridDragEventArgs> DragCompleted
        {
            add { this.AddHandler(RsDataGridRowHeader.DragCompletedEvent, value); }
            remove { this.RemoveHandler(RsDataGridRowHeader.DragCompletedEvent, value); }
        }

        /// <summary>
        /// Occurs when a drag is in progress.
        /// </summary>
        public event EventHandler<RsDataGridDragEventArgs> DragDelta
        {
            add { this.AddHandler(RsDataGridRowHeader.DragDeltaEvent, value); }
            remove { this.RemoveHandler(RsDataGridRowHeader.DragDeltaEvent, value); }
        }

        /// <summary>
        /// Occurs when a drag is started.
        /// </summary>
        public event EventHandler<RsDataGridDragEventArgs> DragStarted
        {
            add { this.AddHandler(RsDataGridRowHeader.DragStartedEvent, value); }
            remove { this.RemoveHandler(RsDataGridRowHeader.DragStartedEvent, value); }
        }
        #endregion Events

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether the dragging of this control is enabled.
        /// </summary>
        public bool IsDragEnabled
        {
            get { return (bool)this.ParentRow.GetValue(IsDragEnabledProperty); }
            set { this.SetValue(IsDragEnabledProperty, value); }
        }

        /// <summary>
        /// Gets a value indicating whether this control is currently being dragged.
        /// </summary>
        public bool IsDragging
        {
            get { return (bool)this.GetValue(IsDraggingProperty); }
            private set { this.SetValue(_isDraggingPropertyKey, value); }
        }

        /// <summary>
        /// Gets a value indicating whether this row header should support reordering through
        /// drag and drop.
        /// </summary>
        public bool SupportsDragging
        {
            get { return GetSupportsRowReordering(this.DataGridOwner); }
        }

        /// <summary>
        /// Gets the parent data grid for this row header.
        /// </summary>
        private DataGrid DataGridOwner
        {
            get
            {
                DataGridRow parent = this.ParentRow;
                if (parent != null)
                {
                    return parent.GetVisualAncestor<DataGrid>();
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the parent row of this row header.
        /// </summary>
        private DataGridRow ParentRow
        {
            get
            {
                FrameworkElement parent = this.TemplatedParent as FrameworkElement;

                while (parent != null)
                {
                    DataGridRow correctlyTyped = parent as DataGridRow;
                    if (correctlyTyped != null)
                    {
                        return correctlyTyped;
                    }

                    parent = parent.TemplatedParent as FrameworkElement;
                }

                return null;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Gets the SupportsRowReordering dependency property value for the specified element.
        /// </summary>
        /// <param name="element">
        /// The element to get the SupportsRowReordering dependency property value for.
        /// </param>
        /// <returns>
        /// The value of the SupportsRowReordering dependency property on the specified
        /// element.
        /// </returns>
        /// <exception cref="RSG.Editor.SmartArgumentNullException">
        /// Thrown if the specified <paramref name="element"/> is null.
        /// </exception>
        public static bool GetSupportsRowReordering(UIElement element)
        {
            if (element == null)
            {
                throw new SmartArgumentNullException(() => element);
            }

            return (bool)element.GetValue(SupportsRowReorderingProperty);
        }

        /// <summary>
        /// Sets the SupportsRowReordering dependency property on the specified element to the
        /// specified value.
        /// </summary>
        /// <param name="element">
        /// The element to set the SupportsRowReordering dependency property on.
        /// </param>
        /// <param name="value">
        /// The value to set the SupportsRowReordering dependency property to on the specified
        /// element.
        /// </param>
        /// <exception cref="RSG.Editor.SmartArgumentNullException">
        /// Thrown if the specified <paramref name="element"/> is null.
        /// </exception>
        public static void SetSupportsRowReordering(UIElement element, bool value)
        {
            if (element == null)
            {
                throw new SmartArgumentNullException(() => element);
            }

            element.SetValue(SupportsRowReorderingProperty, value);
        }

        /// <summary>
        /// Cancels the current dragging operation.
        /// </summary>
        public void CancelDrag()
        {
            if (!this.IsDragging)
            {
                return;
            }

            this.ReleaseCapture();
            this.OnDragCompleted(this._lastScreenPoint, true);
        }

        /// <summary>
        /// Tries to locate a given item within the visual tree, starting with the dependency
        /// object at a given position.
        /// </summary>
        /// <param name="point">
        /// The position to be evaluated on the origin.
        /// </param>
        /// <returns>
        /// The target object under the mouse position.
        /// </returns>
        public object FindTarget(Point point)
        {
            DataGridRow row = null;
            DataGrid dataGrid = this.DataGridOwner;
            DependencyObject element = dataGrid.InputHitTest(point) as DependencyObject;
            if (element == null)
            {
                row = dataGrid.GetVisualAncestor<DataGridRow>();
            }
            else if (element is DataGridRow)
            {
                row = element as DataGridRow;
            }
            else
            {
                row = element.GetVisualAncestor<DataGridRow>();
            }

            if (row != null)
            {
                return row.Item;
            }

            return null;
        }

        /// <summary>
        /// Called if the mouse starts or stops capturing this item and makes sure to cancel
        /// the drag operation if the mouse capture is set to a different control.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data used for this event.
        /// </param>
        protected override void OnIsMouseCapturedChanged(DependencyPropertyChangedEventArgs e)
        {
            if (!this.SupportsDragging)
            {
                base.OnIsMouseCapturedChanged(e);
                return;
            }

            base.OnIsMouseCapturedChanged(e);
            if (!this.IsMouseCaptured)
            {
                this.CancelDrag();
            }
        }

        /// <summary>
        /// Responds to the System.Windows.UIElement.MouseLeftButtonDown event.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.MouseButtonEventArgs data used for the event.
        /// </param>
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            if (this.ParentRow.IsSelected)
            {
                this.ClickMode = ClickMode.Release;
            }
            else
            {
                this.ClickMode = ClickMode.Press;
            }

            base.OnMouseLeftButtonDown(e);
            if (Mouse.LeftButton != MouseButtonState.Pressed || !this.IsDragEnabled)
            {
                return;
            }

            if (!(this as DependencyObject).IsConnectedToPresentationSource())
            {
                return;
            }

            Point screenPoint = this.PointToScreen(e.GetPosition(this));
            this.BeginDragging(screenPoint);

            this.Dispatcher.BeginInvoke(
                new Action(
                delegate
                {
                    UIElement element = this.ParentRow.GetVisualDescendent<DataGridCell>();
                    element.MoveFocus(new TraversalRequest(FocusNavigationDirection.First));
                }),
                DispatcherPriority.Render);
        }

        /// <summary>
        /// Responds to the System.Windows.UIElement.MouseLeftButtonUp event.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.MouseButtonEventArgs data used for the event.
        /// </param>
        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            if (!this.SupportsDragging)
            {
                base.OnMouseLeftButtonUp(e);
                return;
            }

            bool connected = PresentationSource.FromVisual(this) != null;
            if (this.IsMouseCaptured && this.IsDragging && connected)
            {
                this._lastScreenPoint = this.PointToScreen(e.GetPosition(this));
                this.CompleteDrag();
            }

            if (this._movedDuringDrag == false)
            {
                this.OnClick();
            }

            base.OnMouseLeftButtonUp(e);
        }

        /// <summary>
        /// Responds to the System.Windows.UIElement.MouseMove event.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.MouseButtonEventArgs data used for the event.
        /// </param>
        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (!this.SupportsDragging)
            {
                base.OnMouseMove(e);
                return;
            }
            else
            {
                e.Handled = true;
                bool connected = PresentationSource.FromVisual(this) != null;
                if (!this.IsMouseCaptured || !this.IsDragging || !connected)
                {
                    return;
                }

                this._movedDuringDrag = true;
                Point point = this.PointToScreen(e.GetPosition(this));
                if (this._raisedDragStarted)
                {
                    this.OnDragDelta(this._lastScreenPoint, point);
                }

                if (this.IsOutsideSensitivity(point) && !this._raisedDragStarted)
                {
                    this._raisedDragStarted = true;
                    this.OnDragStarted(this._originalScreenPoint);
                    this.OnDragDelta(this._originalScreenPoint, point);
                }

                this._lastScreenPoint = point;
                double pos = e.GetPosition(this.DataGridOwner).Y;
                double tolerance = this.DataGridOwner.ActualHeight * 0.05d;

                if (pos < tolerance || pos > this.DataGridOwner.ActualHeight - tolerance)
                {
                    if (this._hasAutoScrolled)
                    {
                        if (this.DoAutoScroll())
                        {
                            e.Handled = true;
                        }
                    }
                    else
                    {
                        this.StartAutoScroll();
                    }
                }
            }
        }

        /// <summary>
        /// Handles the event fired when the right mouse button is pressed while over this
        /// control.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.MouseButtonEventArgs data used for this event.
        /// </param>
        protected override void OnPreviewMouseRightButtonDown(MouseButtonEventArgs e)
        {
            if (!this.SupportsDragging)
            {
                base.OnPreviewMouseRightButtonDown(e);
                return;
            }

            if (!this.IsKeyboardFocusWithin)
            {
                this.Focus();
            }

            base.OnPreviewMouseRightButtonDown(e);
        }

        /// <summary>
        /// Beings the dragging operation.
        /// </summary>
        /// <param name="screenPoint">
        /// The screen pont at which the drag operation has been started.
        /// </param>
        private void BeginDragging(Point screenPoint)
        {
            if (!this.CaptureMouse())
            {
                return;
            }

            this.IsDragging = true;
            this._originalScreenPoint = screenPoint;
            this._lastScreenPoint = screenPoint;
            this._movedDuringDrag = false;
            this._raisedDragStarted = false;
        }

        /// <summary>
        /// Completes the current drag operation.
        /// </summary>
        private void CompleteDrag()
        {
            if (!this.IsDragging)
            {
                return;
            }

            this.ReleaseCapture();
            this.OnDragCompleted(this._lastScreenPoint, false);
        }

        /// <summary>
        /// Actual performs the automatic scrolling.
        /// </summary>
        /// <returns>
        /// A value indicating whether any scrolling was performed.
        /// </returns>
        private bool DoAutoScroll()
        {
            this._hasAutoScrolled = true;
            Point dataGridMousePosition = Mouse.GetPosition(this.DataGridOwner);
            ScrollViewer viewer = this.DataGridOwner.GetVisualDescendent<ScrollViewer>();
            if (viewer == null)
            {
                return false;
            }

            double offset = 1.0d;
            if (!(bool)this.DataGridOwner.GetValue(ScrollViewer.CanContentScrollProperty))
            {
                offset = 30.0d;
            }

            double tolerance = viewer.ActualHeight * 0.05d;
            if (dataGridMousePosition.Y < tolerance)
            {
                viewer.ScrollToVerticalOffset(viewer.VerticalOffset - offset);
                return true;
            }
            else if (dataGridMousePosition.Y > this.DataGridOwner.ActualHeight - tolerance)
            {
                viewer.ScrollToVerticalOffset(viewer.VerticalOffset + offset);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Determines whether the original starting point and the current point are a
        /// significant distance between each other to say the dragged item has been moved.
        /// </summary>
        /// <param name="point">
        /// The point to test.
        /// </param>
        /// <returns>
        /// True if the difference is significant between the two points.
        /// </returns>
        private bool IsOutsideSensitivity(Point point)
        {
            point.Offset(-this._originalScreenPoint.X, -this._originalScreenPoint.Y);
            point.X = Math.Abs(point.X);
            point.Y = Math.Abs(point.Y);
            double minimumDeltaX = SystemParameters.MinimumHorizontalDragDistance;
            double minimumDeltaY = SystemParameters.MinimumVerticalDragDistance;
            return point.X > minimumDeltaX || point.Y > minimumDeltaY;
        }

        /// <summary>
        /// Makes a single selection on the items control for this control.
        /// </summary>
        private void MakeSingleSelection()
        {
            DataGrid dataGrid = this.DataGridOwner;
            object item = dataGrid.ItemContainerGenerator.ItemFromContainer(this.ParentRow);
            if (item == null)
            {
                return;
            }

            if (dataGrid.SelectedItems.Count != 1 || dataGrid.SelectedItems[0] != item)
            {
                if (dataGrid.SelectionMode == DataGridSelectionMode.Extended)
                {
                    dataGrid.SelectedItems.Clear();
                }

                dataGrid.SelectedItem = item;
                this.ParentRow.MoveFocus(new TraversalRequest(FocusNavigationDirection.First));
            }

            if (dataGrid.IsKeyboardFocusWithin)
            {
                this.Focus();
            }
        }

        /// <summary>
        /// Makes a toggle selection on the items control for this control.
        /// </summary>
        private void MakeToggleSelection()
        {
            DataGrid dataGrid = this.DataGridOwner;
            object item = dataGrid.ItemContainerGenerator.ItemFromContainer(this.ParentRow);
            if (item == null)
            {
                return;
            }

            if (dataGrid.SelectedItems.Contains(item))
            {
                if (dataGrid.SelectionMode == DataGridSelectionMode.Single)
                {
                    dataGrid.SelectedItem = null;
                }
                else
                {
                    dataGrid.SelectedItems.Remove(item);
                }
            }
            else
            {
                if (dataGrid.SelectionMode == DataGridSelectionMode.Single)
                {
                    dataGrid.SelectedItem = item;
                }
                else
                {
                    dataGrid.SelectedItems.Add(item);
                }
            }

            if (dataGrid.IsKeyboardFocusWithin)
            {
                this.Focus();
            }
        }

        /// <summary>
        /// The call-back when the auto-scroll timer ticks.
        /// </summary>
        /// <param name="sender">
        /// The timer object whose interval has elapsed.
        /// </param>
        /// <param name="e">
        /// Always EventArgs.Empty.
        /// </param>
        private void OnAutoScrollTimeout(object sender, EventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                this.DoAutoScroll();
            }
            else
            {
                this.StopAutoScroll();
            }
        }

        /// <summary>
        /// Raises the <see cref="DragCompletedEvent"/> routed event.
        /// </summary>
        /// <param name="point">
        /// The screen position of the mouse when the drag operation finished.
        /// </param>
        /// <param name="cancelled">
        /// A value indicating whether the drag operation was cancelled or not.
        /// </param>
        private void OnDragCompleted(Point point, bool cancelled)
        {
            DataGrid dataGrid = this.DataGridOwner;
            object source = dataGrid.ItemContainerGenerator.ItemFromContainer(this.ParentRow);
            object target = this.FindTarget(dataGrid.PointFromScreen(point));

            DragState state = DragState.NotFinished;
            if (cancelled == true)
            {
                state = DragState.Cancelled;
            }
            else
            {
                if (this._movedDuringDrag)
                {
                    if (Object.ReferenceEquals(source, target))
                    {
                        state = DragState.FinishedWithoutMovement;
                    }
                    else
                    {
                        state = DragState.FinishedWithMovement;
                    }
                }
                else
                {
                    state = DragState.FinishedWithoutMovement;
                }
            }

            Vector totalDelta = point - this._originalScreenPoint;
            this.RaiseEvent(
                new RsDataGridDragEventArgs(
                    DragCompletedEvent,
                    point,
                    state,
                    source,
                    target,
                    totalDelta.X,
                    totalDelta.Y));
        }

        /// <summary>
        /// Raises the <see cref="DragDeltaEvent"/> routed event.
        /// </summary>
        /// <param name="previousPoint">
        /// The previous screen position of the mouse.
        /// </param>
        /// <param name="newPoint">
        /// The current screen position of the mouse.
        /// </param>
        private void OnDragDelta(Point previousPoint, Point newPoint)
        {
            DataGrid dataGrid = this.DataGridOwner;
            object source = dataGrid.ItemContainerGenerator.ItemFromContainer(this.ParentRow);
            object target = this.FindTarget(dataGrid.PointFromScreen(newPoint));

            DragState state = DragState.NotFinished;
            Vector delta = newPoint - previousPoint;
            this.RaiseEvent(
                new RsDataGridDragEventArgs(
                    DragDeltaEvent,
                    newPoint,
                    state,
                    source,
                    target,
                    delta.X,
                    delta.Y));
        }

        /// <summary>
        /// Raises the <see cref="DragStartedEvent"/> routed event.
        /// </summary>
        /// <param name="point">
        /// The original point where the drag operation started from.
        /// </param>
        private void OnDragStarted(Point point)
        {
            DataGrid dataGrid = this.DataGridOwner;
            object source = dataGrid.ItemContainerGenerator.ItemFromContainer(this.ParentRow);

            DragState state = DragState.NotFinished;
            this.RaiseEvent(
                new RsDataGridDragEventArgs(
                    DragStartedEvent,
                    point,
                    state,
                    source,
                    source));
        }

        /// <summary>
        /// Releases the mouse capture of this control if it had it.
        /// </summary>
        private void ReleaseCapture()
        {
            if (!this.IsDragging)
            {
                return;
            }

            this.ClearValue(_isDraggingPropertyKey);
            if (this.IsMouseCaptured)
            {
                this.ReleaseMouseCapture();
            }
        }

        /// <summary>
        /// Begins a timer that will periodically scroll the owner data grid.
        /// </summary>
        private void StartAutoScroll()
        {
            if (this._autoScrollTimer != null)
            {
                return;
            }

            this._hasAutoScrolled = false;
            this._autoScrollTimer = new DispatcherTimer(DispatcherPriority.SystemIdle);
            this._autoScrollTimer.Interval = User32.GetAutoScrollTimeSpan();
            this._autoScrollTimer.Tick += new EventHandler(this.OnAutoScrollTimeout);
            this._autoScrollTimer.Start();
        }

        /// <summary>
        /// Stops the timer that controls auto-scrolling.
        /// </summary>
        private void StopAutoScroll()
        {
            if (this._autoScrollTimer != null)
            {
                this._autoScrollTimer.Stop();
                this._autoScrollTimer = null;
                this._hasAutoScrolled = false;
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsDataGridRowHeader {Class}
} // RSG.Editor.Controls {Namespace}
