﻿//---------------------------------------------------------------------------------------------
// <copyright file="DockingPane.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock.ViewModel
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Windows.Controls;

    /// <summary>
    /// Represents a group of docking elements that can be packed into a group and displayed
    /// in a certain orientation with splitters between them.
    /// </summary>
    public abstract class DockingPane
        : NotifyPropertyChangedBase, IDockingGroupItem, IDockingParent, IDockingElement
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Children"/> property.
        /// </summary>
        private ObservableCollection<IDockingPaneItem> _children;

        /// <summary>
        /// The private field used for the <see cref="GroupIndex"/> property.
        /// </summary>
        private int _groupIndex;

        /// <summary>
        /// The private field used for the <see cref="ParentGroup"/> property.
        /// </summary>
        private DockingGroup _parentGroup;

        /// <summary>
        /// The private field used for the <see cref="Children"/> property.
        /// </summary>
        private ReadOnlyObservableCollection<IDockingPaneItem> _readOnlyChildren;

        /// <summary>
        /// The private field used for the <see cref="Children"/> property.
        /// </summary>
        private SplitterLength _splitterLength;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DockingPane"/> class.
        /// </summary>
        /// <param name="viewSite">
        /// The view site that this docking group is underneath.
        /// </param>
        protected DockingPane(ViewSite viewSite)
        {
            this._splitterLength = new SplitterLength(1, SplitterUnitType.Stretch);
            this._children = new ObservableCollection<IDockingPaneItem>();
            this._readOnlyChildren =
                new ReadOnlyObservableCollection<IDockingPaneItem>(this._children);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a iterator around all of the document items currently opened inside this
        /// docking pane.
        /// </summary>
        public IEnumerable<DocumentItem> AllDocuments
        {
            get
            {
                List<DocumentItem> documents = new List<DocumentItem>();
                foreach (IDockingPaneItem item in this._children)
                {
                    DocumentItem document = item as DocumentItem;
                    if (document != null)
                    {
                        documents.Add(document);
                    }
                }

                return documents;
            }
        }

        /// <summary>
        /// Gets the read only collection of docking group items that belong to this group.
        /// </summary>
        public ReadOnlyObservableCollection<IDockingPaneItem> Children
        {
            get { return this._readOnlyChildren; }
        }

        /// <summary>
        /// Gets or sets the index for this item inside its parent group.
        /// </summary>
        public int GroupIndex
        {
            get { return this._groupIndex; }
            set { this._groupIndex = value; }
        }

        /// <summary>
        /// Gets the parent docking element.
        /// </summary>
        public IDockingElement ParentElement
        {
            get { return this._parentGroup; }
        }

        /// <summary>
        /// Gets or sets the DockingGroup that is the parent to this element.
        /// </summary>
        public DockingGroup ParentGroup
        {
            get { return this._parentGroup; }
            set { this._parentGroup = value; }
        }

        /// <summary>
        /// Gets or sets the splitter length of this item inside its parent group.
        /// </summary>
        public SplitterLength SplitterLength
        {
            get { return this._splitterLength; }
            set { this.SetProperty(ref this._splitterLength, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Moves the item at the specified index one space to the left.
        /// </summary>
        /// <param name="index">
        /// The index of the item to move.
        /// </param>
        public void MoveItemLeft(int index)
        {
            this._children.Move(index, index - 1);
        }

        /// <summary>
        /// Moves the item at the specified index one space to the right.
        /// </summary>
        /// <param name="index">
        /// The index of the item to move.
        /// </param>
        public void MoveItemRight(int index)
        {
            this._children.Move(index, index + 1);
        }

        /// <summary>
        /// Removes the specified item from this elements child collection.
        /// </summary>
        /// <param name="item">
        /// The item to remove.
        /// </param>
        public void RemoveItem(IDockingPaneItem item)
        {
            this._children.Remove(item);
        }

        /// <summary>
        /// Inserts the specified item into this panes children at the specified location.
        /// </summary>
        /// <param name="index">
        /// The zero-based index where the specified item should be added.
        /// </param>
        /// <param name="item">
        /// The item to add to this panes child collection.
        /// </param>
        protected void InsertChild(int index, IDockingPaneItem item)
        {
            this._children.Insert(index, item);
        }
        #endregion Methods
    } // RSG.Editor.Controls.Dock.ViewModel.DockingPane {Class}
} // RSG.Editor.Controls.Dock.ViewModel {Namespace}
