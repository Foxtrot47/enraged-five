﻿//---------------------------------------------------------------------------------------------
// <copyright file="DoubleTunableViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Tunables
{
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// The view model for a <see cref="DoubleTunable"/> model object.
    /// </summary>
    public class DoubleTunableViewModel : TunableViewModelBase<DoubleTunable>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DoubleTunableViewModel"/> class.
        /// </summary>
        /// <param name="tunable">
        /// The double tunable that this view model is representing.
        /// </param>
        /// <param name="parent">
        /// The parent tunable view model for this view model.
        /// </param>
        public DoubleTunableViewModel(DoubleTunable tunable, ITunableViewModel parent)
            : base(tunable, parent)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the value currently assigned to the double tunable.
        /// </summary>
        public double Value
        {
            get { return this.Model.Value; }
            set { this.Model.Value = value; }
        }
        #endregion Properties
    } // RSG.Metadata.ViewModel.Tunables.DoubleTunableViewModel {Class}
} // RSG.Metadata.ViewModel.Tunables {Namespace}
