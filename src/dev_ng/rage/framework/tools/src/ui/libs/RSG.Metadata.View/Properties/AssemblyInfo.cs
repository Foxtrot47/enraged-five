﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssemblyInfo.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Markup;

// General Information about an assembly is controlled through the following set of attributes.
// Change these attribute values to modify the information associated with an assembly.
[assembly: AssemblyTitle("RSG.Metadata.View.dll")]
[assembly: AssemblyDescription("RSG.Metadata.View.dll")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Rockstar Games")]
[assembly: AssemblyProduct("Metadata Editor")]
[assembly: AssemblyCopyright("© Rockstar Games. All rights reserved.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: CLSCompliant(false)]

[assembly: XmlnsDefinition(
    "http://schemas.rockstargames.com/2013/xaml/editor",
    "RSG.Metadata.View")]

[assembly: XmlnsPrefix("http://schemas.rockstargames.com/2013/xaml/editor", "rsg")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("59D6C3B8-BBE7-4255-84A7-39A8498FEEA0")]

[assembly: ThemeInfo(
    ResourceDictionaryLocation.None, // where theme specific resource dictionaries are located
    // (used if a resource is not found in the page,
    // or application resource dictionaries)
    ResourceDictionaryLocation.SourceAssembly // where the generic resource dictionary is
    // located (used if a resource is not found in the page,
    // app, or any theme specific resource dictionaries)
)]

// Version information for an assembly consists of the following four values:
//
// Major Version
// Minor Version
// Build Number
// Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: NeutralResourcesLanguageAttribute("en-GB")]
