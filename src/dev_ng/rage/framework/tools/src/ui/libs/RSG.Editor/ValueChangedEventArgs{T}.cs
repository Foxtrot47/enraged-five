﻿//---------------------------------------------------------------------------------------------
// <copyright file="ValueChangedEventArgs{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;

    /// <summary>
    /// Provides data for a event that is representing a single change to a specific value.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the value to have changed.
    /// </typeparam>
    public class ValueChangedEventArgs<T> : EventArgs
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="OldValue"/> property.
        /// </summary>
        private T _oldValue;

        /// <summary>
        /// The private field used for the <see cref="NewValue"/> property.
        /// </summary>
        private T _newValue;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ValueChangedEventArgs{T}"/>
        /// class.
        /// </summary>
        /// <param name="oldValue">
        /// The value before the changed occurred.
        /// </param>
        /// <param name="newValue">
        /// The value after the changed occurred.
        /// </param>
        public ValueChangedEventArgs(T oldValue, T newValue)
        {
            this._oldValue = oldValue;
            this._newValue = newValue;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the value before the changed occurred.
        /// </summary>
        public T OldValue
        {
            get { return this._oldValue; }
        }

        /// <summary>
        /// Gets the value after the changed occurred.
        /// </summary>
        public T NewValue
        {
            get { return this._newValue; }
        }
        #endregion Properties
    } // RSG.Editor.ValueChangedEventArgs{T} {Class}
} // RSG.Editor {Namespace}
