﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsMenu.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Commands
{
    using System.Windows;
    using System.Windows.Automation.Peers;
    using System.Windows.Controls;

    /// <summary>
    /// Represents a control that enables you to hierarchically organise elements associated
    /// with commands and event handlers.
    /// </summary>
    public class RsMenu : Menu
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="IconSize"/> dependency property.
        /// </summary>
        public static DependencyProperty IconSizeProperty;

        /// <summary>
        /// Identifies the <see cref="LinkCheckBoxSizeToIconSize"/> dependency property.
        /// </summary>
        public static DependencyProperty LinkCheckBoxSizeToIconSizeProperty;

        /// <summary>
        /// Identifies the <see cref="MakeHeadersUppercase"/> dependency property.
        /// </summary>
        public static DependencyProperty MakeHeadersUppercaseProperty;

        /// <summary>
        /// The private field used for the <see cref="BatchStyleKey"/> property.
        /// </summary>
        private static ResourceKey _batchStyleKey = CreateStyleKey("BatchStyleKey");

        /// <summary>
        /// The private field used for the <see cref="ButtonStyleKey"/> property.
        /// </summary>
        private static ResourceKey _buttonStyleKey = CreateStyleKey("ButtonStyleKey");

        /// <summary>
        /// The private field used for the <see cref="ComboBoxStyleKey"/> property.
        /// </summary>
        private static ResourceKey _comboBoxStyleKey = CreateStyleKey("ComboBoxStyleKey");

        /// <summary>
        /// The private field used for the <see cref="ControllerStyleKey"/> property.
        /// </summary>
        private static ResourceKey _controllerStyleKey = CreateStyleKey("ControllerStyleKey");

        /// <summary>
        /// The private field used for the <see cref="MenuStyleKey"/> property.
        /// </summary>
        private static ResourceKey _menuStyleKey = CreateStyleKey("MenuStyleKey");

        /// <summary>
        /// The private field used for the <see cref="SeparatorStyleKey"/> property.
        /// </summary>
        private static ResourceKey _separatorStyleKey = CreateStyleKey("SeparatorStyleKey");

        /// <summary>
        /// The private field used for the <see cref="TextStyleKey"/> property.
        /// </summary>
        private static ResourceKey _textStyleKey = CreateStyleKey("TextStyleKey");

        /// <summary>
        /// The private field used for the <see cref="ToggleStyleKey"/> property.
        /// </summary>
        private static ResourceKey _toggleStyleKey = CreateStyleKey("ToggleStyleKey");
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsMenu"/> class.
        /// </summary>
        static RsMenu()
        {
            IconSizeProperty =
                   DependencyProperty.RegisterAttached(
                   "IconSize",
                   typeof(double),
                   typeof(RsMenu),
                   new FrameworkPropertyMetadata(
                       16.0, FrameworkPropertyMetadataOptions.Inherits));

            LinkCheckBoxSizeToIconSizeProperty =
                DependencyProperty.RegisterAttached(
                "LinkCheckBoxSizeToIconSize",
                typeof(bool),
                typeof(RsMenu),
                new FrameworkPropertyMetadata(
                    false, FrameworkPropertyMetadataOptions.Inherits));

            MakeHeadersUppercaseProperty =
                DependencyProperty.RegisterAttached(
                "MakeHeadersUppercase",
                typeof(bool),
                typeof(RsMenu),
                new FrameworkPropertyMetadata(
                    false, FrameworkPropertyMetadataOptions.Inherits));

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsMenu),
                new FrameworkPropertyMetadata(typeof(RsMenu)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsMenu"/> class.
        /// </summary>
        public RsMenu()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the resource key used to reference the style used on batch command items.
        /// </summary>
        public static ResourceKey BatchStyleKey
        {
            get { return _batchStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on button command items.
        /// </summary>
        public static ResourceKey ButtonStyleKey
        {
            get { return _buttonStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on combo box command items.
        /// </summary>
        public static ResourceKey ComboBoxStyleKey
        {
            get { return _comboBoxStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on controller command items.
        /// </summary>
        public static ResourceKey ControllerStyleKey
        {
            get { return _controllerStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on menu command items.
        /// </summary>
        public static ResourceKey MenuStyleKey
        {
            get { return _menuStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on separator command items.
        /// </summary>
        public static ResourceKey SeparatorStyleKey
        {
            get { return _separatorStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on text command items.
        /// </summary>
        public static ResourceKey TextStyleKey
        {
            get { return _textStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on toggle command items.
        /// </summary>
        public static ResourceKey ToggleStyleKey
        {
            get { return _toggleStyleKey; }
        }

        /// <summary>
        /// Gets or sets the number of pixels both height and width the icons within the menu
        /// system takes up.
        /// </summary>
        public double IconSize
        {
            get { return (double)this.GetValue(IconSizeProperty); }
            set { this.SetValue(IconSizeProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the checkbox used for checkable items gets
        /// sized based on the current icon size or is static as a fixed size.
        /// </summary>
        public bool LinkCheckBoxSizeToIconSize
        {
            get { return (bool)this.GetValue(LinkCheckBoxSizeToIconSizeProperty); }
            set { this.SetValue(LinkCheckBoxSizeToIconSizeProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the content for the top level item headers
        /// are all in uppercase or not.
        /// </summary>
        public bool MakeHeadersUppercase
        {
            get { return (bool)this.GetValue(MakeHeadersUppercaseProperty); }
            set { this.SetValue(MakeHeadersUppercaseProperty, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates or identifies the element used to display the child items.
        /// </summary>
        /// <returns>
        /// A new <see cref="RSG.Editor.Controls.Commands.RsMenuItem" />.
        /// </returns>
        protected override DependencyObject GetContainerForItemOverride()
        {
            RsMenuItem item = new RsMenuItem();
            return item;
        }

        /// <summary>
        /// Determines whether the specified item is, or is eligible to be, its own item
        /// container.
        /// </summary>
        /// <param name="item">
        /// The item to check whether it is an item container.
        /// </param>
        /// <returns>
        /// True if the item is eligible to be its own item container; otherwise, false.
        /// </returns>
        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            if (item is MenuItem || item is Separator)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Returns class-specific AutomationPeer implementations for the Windows Presentation
        /// Foundation (WPF) infrastructure.
        /// </summary>
        /// <returns>
        /// The type-specific AutomationPeer implementation associated with this control.
        /// </returns>
        protected override AutomationPeer OnCreateAutomationPeer()
        {
            return new RsMenuAutomationPeer(this);
        }

        /// <summary>
        /// Prepares the specified element to display the specified item.
        /// </summary>
        /// <param name="element">
        /// Element used to display the specified item.
        /// </param>
        /// <param name="item">
        /// Specified item.
        /// </param>
        protected override void PrepareContainerForItemOverride(
            DependencyObject element, object item)
        {
            CommandBarItemViewModel viewmodel = item as CommandBarItemViewModel;
            if (viewmodel == null)
            {
                base.PrepareContainerForItemOverride(element, item);
                return;
            }

            FrameworkElement frameworkElement = element as FrameworkElement;
            ResourceKey resource = null;
            switch (viewmodel.DisplayType)
            {
                case CommandItemDisplayType.Button:
                    resource = RsMenu.ButtonStyleKey;
                    break;
                case CommandItemDisplayType.Combo:
                    resource = RsMenu.ComboBoxStyleKey;
                    break;
                case CommandItemDisplayType.Menu:
                    resource = RsMenu.MenuStyleKey;
                    break;
                case CommandItemDisplayType.Separator:
                    resource = RsMenu.SeparatorStyleKey;
                    break;
                case CommandItemDisplayType.Toggle:
                    resource = RsMenu.ToggleStyleKey;
                    break;
            }

            if (resource == null)
            {
                return;
            }

            frameworkElement.SetResourceReference(FrameworkElement.StyleProperty, resource);
        }

        /// <summary>
        /// Creates a new resource key that references a style with the specified name.
        /// </summary>
        /// <param name="styleName">
        /// The name that the resource key will be referencing.
        /// </param>
        /// <returns>
        /// A new resource key that references a style with the specified name.
        /// </returns>
        private static ResourceKey CreateStyleKey(string styleName)
        {
            return new StyleKey<RsMenu>(styleName);
        }
        #endregion Methods
    } // RSG.Editor.Controls.Commands.RsMenu {Class}
} // RSG.Editor.Controls.Commands {Namespace}
