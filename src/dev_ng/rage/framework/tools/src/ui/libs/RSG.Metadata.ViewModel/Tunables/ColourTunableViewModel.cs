﻿//---------------------------------------------------------------------------------------------
// <copyright file="ColourTunableViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Tunables
{
    using System.Windows.Media;
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// The view model for a <see cref="ColourTunable"/> model object.
    /// </summary>
    public class ColourTunableViewModel : TunableViewModelBase<ColourTunable>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ColourTunableViewModel"/> class.
        /// </summary>
        /// <param name="tunable">
        /// The colour tunable that this view model is representing.
        /// </param>
        /// <param name="parent">
        /// The parent tunable view model for this view model.
        /// </param>
        public ColourTunableViewModel(ColourTunable tunable, ITunableViewModel parent)
            : base(tunable, parent)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the value currently assigned to the colour tunable.
        /// </summary>
        public Color Value
        {
            get
            {
                int value = this.Model.Value;
                byte alpha = (byte)((value & 0xFF000000) >> 24);
                byte red = (byte)((value & 0x00FF0000) >> 16);
                byte green = (byte)((value & 0x0000FF00) >> 8);
                byte blue = (byte)((value & 0x000000FF) >> 0);

                return Color.FromArgb(alpha, red, green, blue);
            }

            set
            {
                this.Model.Value = value.A << 24 | value.R << 16 | value.G << 8 | value.B << 0;
            }
        }
        #endregion Properties
    } // RSG.Metadata.ViewModel.Tunables.ColourTunableViewModel {Class}
} // RSG.Metadata.ViewModel.Tunables {Namespace}
