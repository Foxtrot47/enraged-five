﻿//---------------------------------------------------------------------------------------------
// <copyright file="ComboActionAsync{T,TItem}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System.Collections.ObjectModel;

    /// <summary>
    /// Provides a abstract base class to any class that is used to implement a combo command.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the parameter passed into the can execute and execute methods.
    /// </typeparam>
    /// <typeparam name="TItem">
    /// The type of the combo parameter.
    /// </typeparam>
    public abstract class ComboActionAsync<T, TItem>
        : CommandActionAsync<T, MultiCommandParameter<TItem>>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ComboActionAsync{T,TItem}"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        protected ComboActionAsync(ParameterResolverDelegate<T> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Create a new command definition for each command associated with this
        /// implementation.
        /// </summary>
        /// <param name="command">
        /// The command to create the definition for.
        /// </param>
        /// <returns>
        /// The new command definition created for the specified command.
        /// </returns>
        internal override CommandDefinition CreateDefinition(RockstarRoutedCommand command)
        {
            ComboCommand definition = new Definition(command);
            ObservableCollection<IMultiCommandItem> items = this.GetItems(definition);
            if (items != null)
            {
                definition.Items = items;
            }

            return definition;
        }

        /// <summary>
        /// Override to provide the created definition with the items to show in the combo.
        /// </summary>
        /// <param name="definition">
        /// The definition that the created items will be used for.
        /// </param>
        /// <returns>
        /// The items to show in the combo.
        /// </returns>
        protected virtual ObservableCollection<IMultiCommandItem> GetItems(
            MultiCommand definition)
        {
            return null;
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// The command definition class that is used to create the commands for this
        /// implementer.
        /// </summary>
        private class Definition : ComboCommand
        {
            #region Fields
            /// <summary>
            /// The private field used for the <see cref="Command"/> property.
            /// </summary>
            private RockstarRoutedCommand _command;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="Definition"/> class.
            /// </summary>
            /// <param name="command">
            /// The command that this definition is wrapping.
            /// </param>
            public Definition(RockstarRoutedCommand command)
            {
                this._command = command;
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets the System.Windows.Input.RoutedCommand that this definition is wrapping.
            /// </summary>
            public override RockstarRoutedCommand Command
            {
                get { return this._command; }
            }
            #endregion Properties
        } // ComboActionAsync{T,TItem}.Definition
        #endregion Classes
    }  // RSG.Editor.ComboActionAsync{T,TItem} {Class}
} // RSG.Editor {Namespace}
