﻿//---------------------------------------------------------------------------------------------
// <copyright file="ISaveableDocument.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using RSG.Editor.Model;

    /// <summary>
    /// When implemented represents a object that's data can be written to a generic stream and
    /// is itself representing a document contained on the local disk drive.
    /// </summary>
    public interface ISaveableDocument : ISaveable
    {
        #region Properties
        /// <summary>
        /// Gets or sets the filename to use to display this document.
        /// </summary>
        string Filename { get; set; }

        /// <summary>
        /// Gets or sets the full file path to the location of this file.
        /// </summary>
        string FullPath { get; set; }

        /// <summary>
        /// Gets a value indicating whether this document item currently contains modified
        /// unsaved data.
        /// </summary>
        bool IsModified { get; }

        /// <summary>
        /// Gets or sets a value indicating whether this document is currently locked inside
        /// the windows explorer.
        /// </summary>
        bool IsReadOnly { get; set; }

        /// <summary>
        /// Gets a value indicating whether this document represents a temporary document. A
        /// temporary file cannot be saved to the same location and needs to be "Saved As...".
        /// </summary>
        bool IsTemporaryFile { get; }

        /// <summary>
        /// Gets the undo engine associated with this document.
        /// </summary>
        UndoEngine UndoEngine { get; }
        #endregion Properties
    } // RSG.Editor.ISaveableDocument {Interface}
} // RSG.Editor {Namespace}
