﻿//---------------------------------------------------------------------------------------------
// <copyright file="LineColumnVisibilities.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.View
{
    using System.Windows;
    using RSG.Editor;

    /// <summary>
    /// Contains the visibility states for the columns inside the conversation user control.
    /// </summary>
    public class LineColumnVisibilities : NotifyPropertyChangedBase
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="AlwaysSubtitledColumn"/> property.
        /// </summary>
        private Visibility _alwaysSubtitledColumn;

        /// <summary>
        /// The private field used for the <see cref="AudibilityColumn"/> property.
        /// </summary>
        private Visibility _audibilityColumn;

        /// <summary>
        /// The private field used for the <see cref="AudioPlaybackColumn"/> property.
        /// </summary>
        private Visibility _audioPlaybackColumn;

        /// <summary>
        /// The private field used for the <see cref="AudioTypeColumn"/> property.
        /// </summary>
        private Visibility _audioTypeColumn;

        /// <summary>
        /// The private field used for the <see cref="ControllerPadSpeakerColumn"/> property.
        /// </summary>
        private Visibility _controllerPadSpeakerColumn;

        /// <summary>
        /// The private field used for the <see cref="DontInterruptForSpecialAbilityColumn"/>
        /// property.
        /// </summary>
        private Visibility _dontInterruptForSpecialAbilityColumn;

        /// <summary>
        /// The private field used for the <see cref="DucksRadioColumn"/> property.
        /// </summary>
        private Visibility _ducksRadioColumn;

        /// <summary>
        /// The private field used for the <see cref="DucksSourceColumn"/> property.
        /// </summary>
        private Visibility _ducksScoreColumn;

        /// <summary>
        /// The private field used for the <see cref="HeadsetSubmixColumn"/> property.
        /// </summary>
        private Visibility _headsetSubmixColumn;

        /// <summary>
        /// The private field used for the <see cref="InterruptibleColumn"/> property.
        /// </summary>
        private Visibility _interruptibleColumn;

        /// <summary>
        /// The private field used for the <see cref="ListenerColumn"/> property.
        /// </summary>
        private Visibility _listenerColumn;

        /// <summary>
        /// The private field used for the <see cref="RecordedColumn"/> property.
        /// </summary>
        private Visibility _recordedColumn;

        /// <summary>
        /// The private field used for the <see cref="SentForRecordingColumn"/> property.
        /// </summary>
        private Visibility _sentForRecordingColumn;

        /// <summary>
        /// The private field used for the <see cref="SpeakerColumn"/> property.
        /// </summary>
        private Visibility _speakerColumn;

        /// <summary>
        /// The private field used for the <see cref="SpecialColumn"/> property.
        /// </summary>
        private Visibility _specialColumn;

        /// <summary>
        /// The private field used for the <see cref="VoiceNameColumn"/> property.
        /// </summary>
        private Visibility _voiceNameColumn;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="LineColumnVisibilities"/> class.
        /// </summary>
        public LineColumnVisibilities()
        {
            this._alwaysSubtitledColumn = Visibility.Collapsed;
            this._audibilityColumn = Visibility.Collapsed;
            this._audioPlaybackColumn = Visibility.Visible;
            this._audioTypeColumn = Visibility.Collapsed;
            this._controllerPadSpeakerColumn = Visibility.Collapsed;
            this._dontInterruptForSpecialAbilityColumn = Visibility.Collapsed;
            this._ducksRadioColumn = Visibility.Visible;
            this._ducksScoreColumn = Visibility.Collapsed;
            this._headsetSubmixColumn = Visibility.Collapsed;
            this._interruptibleColumn = Visibility.Collapsed;
            this._listenerColumn = Visibility.Visible;
            this._recordedColumn = Visibility.Visible;
            this._sentForRecordingColumn = Visibility.Collapsed;
            this._speakerColumn = Visibility.Visible;
            this._specialColumn = Visibility.Collapsed;
            this._voiceNameColumn = Visibility.Visible;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the visibility for the animation triggered data grid column.
        /// </summary>
        public Visibility AlwaysSubtitledColumn
        {
            get { return this._alwaysSubtitledColumn; }
            set { this.SetProperty(ref this._alwaysSubtitledColumn, value); }
        }

        /// <summary>
        /// Gets or sets the visibility for the audibility data grid column.
        /// </summary>
        public Visibility AudibilityColumn
        {
            get { return this._audibilityColumn; }
            set { this.SetProperty(ref this._audibilityColumn, value); }
        }

        /// <summary>
        /// Gets or sets the visibility for the audio playback data grid column.
        /// </summary>
        public Visibility AudioPlaybackColumn
        {
            get { return this._audioPlaybackColumn; }
            set { this.SetProperty(ref this._audioPlaybackColumn, value); }
        }

        /// <summary>
        /// Gets or sets the visibility for the audio type data grid column.
        /// </summary>
        public Visibility AudioTypeColumn
        {
            get { return this._audioTypeColumn; }
            set { this.SetProperty(ref this._audioTypeColumn, value); }
        }

        /// <summary>
        /// Gets or sets the visibility for the controller pad speaker data grid column.
        /// </summary>
        public Visibility ControllerPadSpeakerColumn
        {
            get { return this._controllerPadSpeakerColumn; }
            set { this.SetProperty(ref this._controllerPadSpeakerColumn, value); }
        }

        /// <summary>
        /// Gets or sets the visibility for the don't interrupt for special ability data grid
        /// column.
        /// </summary>
        public Visibility DontInterruptForSpecialAbilityColumn
        {
            get { return this._dontInterruptForSpecialAbilityColumn; }
            set { this.SetProperty(ref this._dontInterruptForSpecialAbilityColumn, value); }
        }

        /// <summary>
        /// Gets or sets the visibility for the ducks radio data grid column.
        /// </summary>
        public Visibility DucksRadioColumn
        {
            get { return this._ducksRadioColumn; }
            set { this.SetProperty(ref this._ducksRadioColumn, value); }
        }

        /// <summary>
        /// Gets or sets the visibility for the ducks score data grid column.
        /// </summary>
        public Visibility DucksScoreColumn
        {
            get { return this._ducksScoreColumn; }
            set { this.SetProperty(ref this._ducksScoreColumn, value); }
        }

        /// <summary>
        /// Gets or sets the visibility for the headset sub-mix data grid column.
        /// </summary>
        public Visibility HeadsetSubmixColumn
        {
            get { return this._headsetSubmixColumn; }
            set { this.SetProperty(ref this._headsetSubmixColumn, value); }
        }

        /// <summary>
        /// Gets or sets the visibility for the interruptible data grid column.
        /// </summary>
        public Visibility InterruptibleColumn
        {
            get { return this._interruptibleColumn; }
            set { this.SetProperty(ref this._interruptibleColumn, value); }
        }

        /// <summary>
        /// Gets or sets the visibility for the listener data grid column.
        /// </summary>
        public Visibility ListenerColumn
        {
            get { return this._listenerColumn; }
            set { this.SetProperty(ref this._listenerColumn, value); }
        }

        /// <summary>
        /// Gets or sets the visibility for the recorded data grid column.
        /// </summary>
        public Visibility RecordedColumn
        {
            get { return this._recordedColumn; }
            set { this.SetProperty(ref this._recordedColumn, value); }
        }

        /// <summary>
        /// Gets or sets the visibility for the sent for recorded data grid column.
        /// </summary>
        public Visibility SentForRecordingColumn
        {
            get { return this._sentForRecordingColumn; }
            set { this.SetProperty(ref this._sentForRecordingColumn, value); }
        }

        /// <summary>
        /// Gets or sets the visibility for the speaker data grid column.
        /// </summary>
        public Visibility SpeakerColumn
        {
            get { return this._speakerColumn; }
            set { this.SetProperty(ref this._speakerColumn, value); }
        }

        /// <summary>
        /// Gets or sets the visibility for the special data grid column.
        /// </summary>
        public Visibility SpecialColumn
        {
            get { return this._specialColumn; }
            set { this.SetProperty(ref this._specialColumn, value); }
        }

        /// <summary>
        /// Gets or sets the visibility for the voice name data grid column.
        /// </summary>
        public Visibility VoiceNameColumn
        {
            get { return this._voiceNameColumn; }
            set { this.SetProperty(ref this._voiceNameColumn, value); }
        }
        #endregion Properties
    } // RSG.Text.View.View.LineColumnVisibilities {Class}
} // RSG.Text.View.View {Namespace}
