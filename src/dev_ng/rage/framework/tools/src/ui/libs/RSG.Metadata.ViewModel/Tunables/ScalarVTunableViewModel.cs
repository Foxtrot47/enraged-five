﻿//---------------------------------------------------------------------------------------------
// <copyright file="ScalarVTunableViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Tunables
{
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// The view model for a <see cref="ScalarVTunable"/> model object.
    /// </summary>
    public class ScalarVTunableViewModel : TunableViewModelBase<ScalarVTunable>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ScalarVTunableViewModel"/> class.
        /// </summary>
        /// <param name="tunable">
        /// The scalar tunable that this view model is representing.
        /// </param>
        /// <param name="parent">
        /// The parent tunable view model for this view model.
        /// </param>
        public ScalarVTunableViewModel(ScalarVTunable tunable, ITunableViewModel parent)
            : base(tunable, parent)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the value currently assigned to the scalar tunable.
        /// </summary>
        public float Value
        {
            get { return this.Model.Value; }
            set { this.Model.Value = value; }
        }
        #endregion Properties
    } // RSG.Metadata.ViewModel.Tunables.ScalarVTunableViewModel {Class}
} // RSG.Metadata.ViewModel.Tunables {Namespace}
