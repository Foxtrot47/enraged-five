﻿//---------------------------------------------------------------------------------------------
// <copyright file="OvertypeModeToTextConverter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Converters
{
    using System.Globalization;

    /// <summary>
    /// Converts a boolean value into a System.Windows.Visibility enum value.
    /// </summary>
    public class OvertypeModeToTextConverter : ValueConverter<bool, string>
    {
        #region Methods
        /// <summary>
        /// Converters the given value and returns the result.
        /// </summary>
        /// <param name="value">
        /// The original value to convert.
        /// </param>
        /// <param name="param">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A converted value.
        /// </returns>
        protected override string Convert(bool value, object param, CultureInfo culture)
        {
            if (value)
            {
                return "OVR";
            }

            return "INS";
        }
        #endregion Methods
    } // IntegerToVisibilityConverter.OvertypeModeToTextConverter {Class}
} // RSG.Editor.Controls.Converters {Namespace}
