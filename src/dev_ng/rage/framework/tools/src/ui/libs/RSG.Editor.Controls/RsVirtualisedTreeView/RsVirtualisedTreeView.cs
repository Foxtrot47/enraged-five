﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsVirtualisedTreeView.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Input;
    using RSG.Base.Collections;
    using RSG.Editor.Controls.Helpers;
    using RSG.Editor.SharedCommands;
    using RSG.Editor.View;

    /// <summary>
    /// Represents a control that displays hierarchical data in a tree structure.
    /// </summary>
    public class RsVirtualisedTreeView : ListBox
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="AutoExpandFilteredItems"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty AutoExpandFilteredItemsProperty;

        /// <summary>
        /// Identifies the <see cref="FilteredItemsSource"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty FilteredItemsSourceProperty;

        /// <summary>
        /// Identifies the <see cref="IsExpandablePath"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsExpandablePathProperty;

        /// <summary>
        /// Identifies the <see cref="ItemsPath"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ItemsPathProperty;

        /// <summary>
        /// Identifies the <see cref="RootItemsSource"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty RootItemsSourceProperty;

        /// <summary>
        /// Identifies the <see cref="ShowRootExpander"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ShowRootExpanderProperty;

        /// <summary>
        /// Identifies the <see cref="SourceItemToCollectionItemConverter"/> dependency
        /// property.
        /// </summary>
        public static readonly DependencyProperty SourceItemToCollectionItemConverterProperty;

        /// <summary>
        /// The private field used for the <see cref="AnchorNode"/> property.
        /// </summary>
        private VirtualisedTreeNode _anchorNode;

        /// <summary>
        /// A value indicating whether the keyboard focusing of the anchor item has been
        /// delayed.
        /// </summary>
        private bool _delayFocusAnchorItem;

        /// <summary>
        /// The private field used for the <see cref="DisableSorting"/> property.
        /// </summary>
        private bool _disableSorting;

        /// <summary>
        /// The private field used for the <see cref="GetContextMenuIdCallback"/> property.
        /// </summary>
        private GetContextMenuIdDelegate _getContextMenuIdCallback;

        /// <summary>
        /// The private field used for the <see cref="IsDirectlyGainingKeyboardFocus"/>
        /// property.
        /// </summary>
        private bool _isDirectlyGainingKeyboardFocus;

        /// <summary>
        /// The private field used for the <see cref="ItemsConverter"/> property.
        /// </summary>
        private ItemBindingConverter _itemsConverter;

        /// <summary>
        /// A count indicating how many times the focus change suspended lock has been
        /// initialised.
        /// </summary>
        private int _keyboardFocusChangeSuspendLock;

        /// <summary>
        /// The private field used for the <see cref="LastFocusedItem"/> property.
        /// </summary>
        private WeakReference<IInputElement> _lastFocusedElement;

        /// <summary>
        /// A set of unique items to be selected last.
        /// </summary>
        private ISet<object> _lastSelectedItems;

        /// <summary>
        /// The main logic tree and cache for the tree nodes used in this tree view.
        /// </summary>
        private Dictionary<object, List<VirtualisedTreeNode>> _logicalTree;

        /// <summary>
        /// A reference to the node that has focusing pended on it.
        /// </summary>
        private VirtualisedTreeNode _pendingFocusNode;

        /// <summary>
        /// The private field used for the <see cref="RootNode"/> property.
        /// </summary>
        private VirtualisedTreeNode _rootNode;

        /// <summary>
        /// A count indicating how many times the selection preservation scope has been
        /// initialised.
        /// </summary>
        private int _selectionPreservationScopeCount;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsVirtualisedTreeView" /> class.
        /// </summary>
        static RsVirtualisedTreeView()
        {
            ContentPresenter.RecognizesAccessKeyProperty.AddOwner(
                typeof(RsVirtualisedTreeViewItem), new PropertyMetadata(false));

            AutoExpandFilteredItemsProperty =
                DependencyProperty.Register(
                "AutoExpandFilteredItems",
                typeof(bool),
                typeof(RsVirtualisedTreeView),
                new PropertyMetadata(true));

            IsExpandablePathProperty =
                DependencyProperty.Register(
                "IsExpandablePath",
                typeof(string),
                typeof(RsVirtualisedTreeView),
                new PropertyMetadata(null));

            ItemsPathProperty =
                DependencyProperty.Register(
                "ItemsPath",
                typeof(string),
                typeof(RsVirtualisedTreeView));

            ShowRootExpanderProperty =
                DependencyProperty.Register(
                "ShowRootExpander",
                typeof(bool),
                typeof(RsVirtualisedTreeView),
                new FrameworkPropertyMetadata(true));

            SourceItemToCollectionItemConverterProperty =
                DependencyProperty.Register(
                "SourceItemToCollectionItemConverterProperty",
                typeof(Func<object, object>),
                typeof(RsVirtualisedTreeView),
                new FrameworkPropertyMetadata(null));

            FilteredItemsSourceProperty =
                DependencyProperty.Register(
                "FilteredItemsSource",
                typeof(IFilteredItemsSource),
                typeof(RsVirtualisedTreeView),
                new FrameworkPropertyMetadata(
                    null, new PropertyChangedCallback(OnFilteredItemsSourceChanged)));

            RootItemsSourceProperty =
                DependencyProperty.Register(
                "RootItemsSource",
                typeof(IEnumerable),
                typeof(RsVirtualisedTreeView),
                new FrameworkPropertyMetadata(
                    new PropertyChangedCallback(OnRootItemsSourceChanged)));

            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsVirtualisedTreeView),
                new FrameworkPropertyMetadata(typeof(RsVirtualisedTreeView)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsVirtualisedTreeView"/> class.
        /// </summary>
        public RsVirtualisedTreeView()
        {
            this._logicalTree = new Dictionary<object, List<VirtualisedTreeNode>>();
            this.RootNode = new VirtualisedTreeNode(this);
            this.RootNode.BeginInit();
            this.RootNode.IsVisible = true;
            this.RootNode.IsExpanded = true;
            this.RootNode.EndInit();

            this.CommandBindings.Add(
                new CommandBinding(
                    RockstarCommands.SelectAll,
                    this.OnSelectAll,
                    this.CanSelectAll));

            this.Loaded += this.OnLoaded;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether the items inside this hierarchical control
        /// are expanded automatically when filter is active and they're included in it.
        /// </summary>
        public bool AutoExpandFilteredItems
        {
            get { return (bool)this.GetValue(AutoExpandFilteredItemsProperty); }
            set { this.SetValue(AutoExpandFilteredItemsProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this tree view should sort its items based
        /// on priority first then text or not at all.
        /// </summary>
        public bool DisableSorting
        {
            get { return this._disableSorting; }
            set { this._disableSorting = value; }
        }

        /// <summary>
        /// Gets or sets the items source for the filtered items.
        /// </summary>
        public IFilteredItemsSource FilteredItemsSource
        {
            get { return (IFilteredItemsSource)this.GetValue(FilteredItemsSourceProperty); }
            set { this.SetValue(FilteredItemsSourceProperty, value); }
        }

        /// <summary>
        /// Gets or sets the delegate that can be used to retrieve the command ids for the
        /// context menu.
        /// </summary>
        public GetContextMenuIdDelegate GetContextMenuIdCallback
        {
            get { return this._getContextMenuIdCallback; }
            set { this._getContextMenuIdCallback = value; }
        }

        /// <summary>
        /// Gets a iterator around all of the tree nodes that have been initialised for this
        /// tree view.
        /// </summary>
        public IEnumerable<VirtualisedTreeNode> InitialisedNodes
        {
            get { return this.RootNode.DescendantNodes; }
        }

        /// <summary>
        /// Gets or sets the name of the property on the view model that should be bound to the
        /// is expandable property.
        /// </summary>
        public string IsExpandablePath
        {
            get { return (string)this.GetValue(IsExpandablePathProperty); }
            set { this.SetValue(IsExpandablePathProperty, value); }
        }

        /// <summary>
        /// Gets or sets the name of the property on the view model that should be bound to the
        /// items source property.
        /// </summary>
        public string ItemsPath
        {
            get { return (string)this.GetValue(ItemsPathProperty); }
            set { this.SetValue(ItemsPathProperty, value); }
        }

        /// <summary>
        /// Gets the last item to have focus.
        /// </summary>
        public RsVirtualisedTreeViewItem LastFocusedItem
        {
            get
            {
                if (this._lastFocusedElement == null)
                {
                    return null;
                }

                IInputElement inputElement;
                if (!this._lastFocusedElement.TryGetTarget(out inputElement))
                {
                    return null;
                }

                RsVirtualisedTreeViewItem item = inputElement as RsVirtualisedTreeViewItem;
                if (item == null || !item.IsConnectedToPresentationSource())
                {
                    return null;
                }

                return item;
            }
        }

        /// <summary>
        /// Gets the selected item. This is different to the SelectedItem property in that this
        /// returns an object that is bound to the tree not the internal view model object.
        /// </summary>
        public object RealSelectedItem
        {
            get
            {
                VirtualisedTreeNode node = this.SelectedItem as VirtualisedTreeNode;
                if (node != null)
                {
                    return node.Item;
                }

                return this.SelectedItem;
            }
        }

        /// <summary>
        /// Gets an array containing the selected items. This is different to the SelectedItems
        /// property in that this returns a list made up of the items that are bound to the
        /// tree not a list of the internal view model objects.
        /// </summary>
        public object[] RealSelectedItems
        {
            get
            {
                List<object> items = new List<object>();
                foreach (object item in this.SelectedItems)
                {
                    VirtualisedTreeNode node = item as VirtualisedTreeNode;
                    if (node != null)
                    {
                        items.Add(node.Item);
                    }
                    else if (item != null)
                    {
                        items.Add(item);
                    }
                }

                return items.ToArray();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the first underscore in the text should be
        /// ignored.
        /// </summary>
        public bool RecognizesAccessKey
        {
            get { return (bool)this.GetValue(ContentPresenter.RecognizesAccessKeyProperty); }
            set { this.SetValue(ContentPresenter.RecognizesAccessKeyProperty, value); }
        }

        /// <summary>
        /// Gets or sets the main items source property for the root items.
        /// </summary>
        public IEnumerable RootItemsSource
        {
            get
            {
                return (IEnumerable)this.GetValue(RootItemsSourceProperty);
            }

            set
            {
                if (value == null)
                {
                    this.ClearValue(RootItemsSourceProperty);
                    return;
                }

                this.SetValue(RootItemsSourceProperty, value);
            }
        }

        /// <summary>
        /// Gets the tree nodes that make up the root nodes of this tree view.
        /// </summary>
        public IEnumerable<VirtualisedTreeNode> RootNodes
        {
            get { return this.RootNode.ChildNodes; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this tree view should show the expander on
        /// the root items or not.
        /// </summary>
        public bool ShowRootExpander
        {
            get { return (bool)this.GetValue(ShowRootExpanderProperty); }
            set { this.SetValue(ShowRootExpanderProperty, value); }
        }

        /// <summary>
        /// Gets or sets the function that is used to converter the source items to the
        /// collection items inside this tree view.
        /// </summary>
        public Func<object, object> SourceItemToCollectionItemConverter
        {
            get
            {
                return (Func<object, object>)this.GetValue(
                    SourceItemToCollectionItemConverterProperty);
            }

            set
            {
                this.SetValue(SourceItemToCollectionItemConverterProperty, value);
            }
        }

        /// <summary>
        /// Gets the collection that is used to generator the content for this items control.
        /// </summary>
        internal IList InternalItems
        {
            get { return this.Items; }
        }

        /// <summary>
        /// Gets a value indicating whether this tree view is currently gaining keyboard
        /// focus and handling that event.
        /// </summary>
        internal bool IsDirectlyGainingKeyboardFocus
        {
            get { return this._isDirectlyGainingKeyboardFocus; }
            private set { this._isDirectlyGainingKeyboardFocus = value; }
        }

        /// <summary>
        /// Gets the items binding converter that can be used to converter the source items to
        /// their corresponding collection type.
        /// </summary>
        internal IValueConverter ItemsConverter
        {
            get
            {
                if (this._itemsConverter == null)
                {
                    this._itemsConverter = new ItemBindingConverter(this);
                }

                return this._itemsConverter;
            }
        }

        /// <summary>
        /// Gets the scroll viewer part that has been defined inside the control template.
        /// </summary>
        internal ScrollViewer TemplateScrollViewer
        {
            get { return this.GetTemplateChild("PART_ScrollViewer") as ScrollViewer; }
        }

        /// <summary>
        /// Gets or sets the tree node that is currently the anchored node for this tree view.
        /// </summary>
        private VirtualisedTreeNode AnchorNode
        {
            get { return this._anchorNode; }
            set { this._anchorNode = value; }
        }

        /// <summary>
        /// Gets a value indicating whether keyboard change has currently been suspended.
        /// </summary>
        private bool IsKeyboardFocusChangeSuspended
        {
            get { return this._keyboardFocusChangeSuspendLock > 0; }
        }

        /// <summary>
        /// Gets or sets the root node for this tree view.
        /// </summary>
        private VirtualisedTreeNode RootNode
        {
            get { return this._rootNode; }
            set { this._rootNode = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Changes the selection on the tree based on the specified parameters.
        /// </summary>
        /// <param name="action">
        /// The action that has occurred to change the selection.
        /// </param>
        /// <param name="forward">
        /// A value indicating whether the selection target should be relative to the anchor
        /// node extended forwards or backwards by the specified count.
        /// </param>
        /// <param name="count">
        /// A count representing the number of items away from the anchor the target item is.
        /// </param>
        public void ChangeSelection(SelectionAction action, bool forward, int count)
        {
            if (this.AnchorNode == null)
            {
                return;
            }

            int treeIndex = this.AnchorNode.TreeIndex;
            int index = treeIndex + (count * (forward ? 1 : -1));
            if (index < 0)
            {
                index = 0;
            }

            if (index > this.InternalItems.Count - 1)
            {
                index = this.InternalItems.Count - 1;
            }

            VirtualisedTreeNode target = this.InternalItems[index] as VirtualisedTreeNode;
            this.ChangeSelection(target, action, true);
        }

        /// <summary>
        /// Changes the selection based of the specified parameters.
        /// </summary>
        /// <param name="target">
        /// The target item for the selection.
        /// </param>
        /// <param name="action">
        /// The action on how the selection is to be handled.
        /// </param>
        /// <param name="scrollIntoView">
        /// A value indicating whether the target object should be scrolled into view.
        /// </param>
        public void ChangeSelection(
            VirtualisedTreeNode target, SelectionAction action, bool scrollIntoView)
        {
            EnsureExpanded(target);
            if (scrollIntoView)
            {
                this.ScrollIntoView(target);
            }

            switch (action)
            {
                case SelectionAction.SingleSelection:
                    {
                        this.SelectedItem = target;
                        this.UpdateAnchorItemCore(target);
                        return;
                    }

                case SelectionAction.ExtendSelection:
                    {
                        VirtualisedTreeNode treeNode = target as VirtualisedTreeNode;
                        int treeIndex = this.AnchorNode.TreeIndex;
                        int treeIndex2 = treeNode.TreeIndex;
                        bool flag = treeIndex < treeIndex2;
                        int num = flag ? 1 : -1;
                        this.SelectedItems.Clear();
                        int num2 = treeIndex;
                        while (flag ? (num2 <= treeIndex2) : (num2 >= treeIndex2))
                        {
                            this.SelectedItems.Add(this.InternalItems[num2]);
                            num2 += num;
                        }

                        break;
                    }

                case SelectionAction.ToggleSelection:
                    {
                        if (this.SelectedItems.Contains(target))
                        {
                            if (this.SelectedItems.Count > 1)
                            {
                                this.SelectedItems.Remove(target);
                            }
                        }
                        else
                        {
                            this.SelectedItems.Add(target);
                        }

                        this.UpdateAnchorItemCore(target);
                        return;
                    }

                case SelectionAction.AddToSelection:
                    {
                        if (!this.SelectedItems.Contains(target))
                        {
                            this.SelectedItems.Add(target);
                        }

                        this.UpdateAnchorItemCore(target);
                        return;
                    }

                case SelectionAction.RemoveFromSelection:
                    {
                        if (this.SelectedItems.Count > 1)
                        {
                            this.SelectedItems.Remove(target);
                            return;
                        }

                        break;
                    }

                case SelectionAction.SetAnchorItem:
                    {
                        this.UpdateAnchorItemCore(target);
                        return;
                    }

                default:
                    Debug.Assert(false, "Unrecognised selection action");
                    break;
            }
        }

        /// <summary>
        /// Makes sure the specified item is collapsed if found in the tree view.
        /// </summary>
        /// <param name="item">
        /// The item to collapse.
        /// </param>
        public void CollapseItem(object item)
        {
            List<VirtualisedTreeNode> nodes = this.GetTreeNodesCore(item);
            if (nodes != null)
            {
                foreach (VirtualisedTreeNode node in nodes)
                {
                    node.IsExpanded = false;
                }
            }
        }

        /// <summary>
        /// Makes sure the specified item is collapsed if found in the tree view and all of its
        /// descendants.
        /// </summary>
        /// <param name="item">
        /// The item to collapse.
        /// </param>
        public void CollapseItemRecursively(object item)
        {
            List<VirtualisedTreeNode> nodes = this.GetTreeNodesCore(item);
            if (nodes != null)
            {
                foreach (VirtualisedTreeNode node in nodes)
                {
                    this.CollapseNodeRecursively(node);
                }
            }
        }

        /// <summary>
        /// Makes sure the specified item is collapsed if found in the tree view and all of its
        /// descendants.
        /// </summary>
        /// <param name="node">
        /// The root item to start the collapse from.
        /// </param>
        public virtual void CollapseNodeRecursively(VirtualisedTreeNode node)
        {
            foreach (VirtualisedTreeNode current in node.ChildNodes)
            {
                this.CollapseNodeRecursively(current);
            }

            node.IsExpanded = false;
        }

        /// <summary>
        /// Makes sure the specified item is expanded if found in the tree view.
        /// </summary>
        /// <param name="item">
        /// The item to expand.
        /// </param>
        public void ExpandItem(object item)
        {
            List<VirtualisedTreeNode> nodes = this.GetTreeNodesCore(item);
            if (nodes != null)
            {
                foreach (VirtualisedTreeNode node in nodes)
                {
                    node.IsExpanded = true;
                }
            }
        }

        /// <summary>
        /// Makes sure the specified item is expanded if found in the tree view and all of its
        /// descendants.
        /// </summary>
        /// <param name="item">
        /// The root item to start the expansion from.
        /// </param>
        public void ExpandItemRecursively(object item)
        {
            List<VirtualisedTreeNode> nodes = this.GetTreeNodesCore(item);
            if (nodes != null)
            {
                foreach (VirtualisedTreeNode node in nodes)
                {
                    this.ExpandNodeRecursively(node);
                }
            }
        }

        /// <summary>
        /// Completely expands the specified node and all of its descendants.
        /// </summary>
        /// <param name="node">
        /// The root node to start the expansion from.
        /// </param>
        public virtual void ExpandNodeRecursively(VirtualisedTreeNode node)
        {
            node.IsExpanded = true;
            foreach (VirtualisedTreeNode current in node.ChildNodes)
            {
                this.ExpandNodeRecursively(current);
            }
        }

        /// <summary>
        /// Moves the keyboard focus to the currently selected item if there is one or the
        /// first item if there isn't.
        /// </summary>
        public void FocusSelectedItem()
        {
            if (this.SelectedItem == null)
            {
                this.SelectFirstItem();
            }

            VirtualisedTreeNode node = this.SelectedItem as VirtualisedTreeNode;
            if (node != null)
            {
                this.FocusNode(node);
            }
        }

        /// <summary>
        /// Gets a set containing unique items of the specified type that are currently
        /// selected.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the items to retrieve.
        /// </typeparam>
        /// <returns>
        /// A set containing unique items of the specified type that are currently selected.
        /// </returns>
        public ISet<T> GetDistinctSelection<T>() where T : class
        {
            HashSet<T> hashSet = new HashSet<T>();
            foreach (VirtualisedTreeNode selectedNode in this.SelectedItems)
            {
                T item = this.GetKeyFromItem(selectedNode.Item) as T;
                if (item != null)
                {
                    hashSet.Add(item);
                }
            }

            return hashSet;
        }

        /// <summary>
        /// Gets the first item that has been explicitly included in the filter if there is
        /// one; otherwise, null.
        /// </summary>
        /// <returns>
        /// The first item that has been explicitly included in the filter if there is one;
        /// otherwise, null.
        /// </returns>
        public VirtualisedTreeNode GetFirstFilteredItem()
        {
            foreach (object item in this.Items)
            {
                VirtualisedTreeNode node = item as VirtualisedTreeNode;
                if (node == null)
                {
                    continue;
                }

                if (node.IsExplicitlyIncludedInFilter)
                {
                    return node;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the very first tree node that is currently inside the logic tree that
        /// represents the specified item.
        /// </summary>
        /// <param name="item">
        /// The item whose first representing node should be returned.
        /// </param>
        /// <returns>
        /// The very first tree node that is currently inside the logic tree that represents
        /// the specified item.
        /// </returns>
        public VirtualisedTreeNode GetFirstTreeNode(object item)
        {
            List<VirtualisedTreeNode> nodes = this.GetTreeNodesCore(item);
            if (nodes == null || nodes.Count == 0)
            {
                return null;
            }

            nodes.Sort(delegate(VirtualisedTreeNode x, VirtualisedTreeNode y)
            {
                if (x.Depth == y.Depth)
                {
                    return x.TreeIndex.CompareTo(y.TreeIndex);
                }

                return x.Depth.CompareTo(y.Depth);
            });

            return nodes[0];
        }

        /// <summary>
        /// Retrieves a key for the specified item that can be used to index the logic tree.
        /// </summary>
        /// <param name="item">
        /// The item whose key should be retrieved.
        /// </param>
        /// <returns>
        /// The key for the specified item.
        /// </returns>
        public virtual object GetKeyFromItem(object item)
        {
            return item;
        }

        /// <summary>
        /// Retrieves a iterator around the nodes that are currently present inside the logic
        /// tree that represent the specified item.
        /// </summary>
        /// <param name="item">
        /// The item whose valid nodes should be retrieved.
        /// </param>
        /// <returns>
        /// A iterator around the nodes that are currently present inside the logic tree that
        /// represent the specified item.
        /// </returns>
        public IEnumerable<VirtualisedTreeNode> GetTreeNodes(object item)
        {
            IEnumerable<VirtualisedTreeNode> nodes = this.GetTreeNodesCore(item);
            if (nodes == null)
            {
                return Enumerable.Empty<VirtualisedTreeNode>();
            }

            return nodes;
        }

        /// <summary>
        /// Single selects the first item that is currently filtered.
        /// </summary>
        /// <returns>
        /// A value indicating whether or not a selection has taken place.
        /// </returns>
        public bool SelectFirstFilteredItem()
        {
            VirtualisedTreeNode target = this.GetFirstFilteredItem();
            if (target == null)
            {
                return false;
            }

            this.ChangeSelection(target, SelectionAction.SingleSelection, true);
            return true;
        }

        /// <summary>
        /// Selects the first item if there is one.
        /// </summary>
        public void SelectFirstItem()
        {
            if (this.Items.Count > 0)
            {
                this.SelectedItem = this.Items[0];
            }
        }

        /// <summary>
        /// Makes sure the specified item is selected if found in the tree view and makes sure
        /// the tree is expanded and scrolled correctly.
        /// </summary>
        /// <param name="item">
        /// The item to select.
        /// </param>
        /// <returns>
        /// A value indicating whether the item was found and selection was attempted.
        /// </returns>
        public bool SelectItem(object item)
        {
            List<VirtualisedTreeNode> nodes = this.GetTreeNodesCore(item);
            if (nodes == null)
            {
                return false;
            }

            VirtualisedTreeNode target = nodes.FirstOrDefault();
            if (target == null)
            {
                return false;
            }

            this.ChangeSelection(target, SelectionAction.SingleSelection, true);
            return true;
        }

        /// <summary>
        /// Creates a new keyboard suspender scope class that can be used to in a using
        /// statement to suspend the keyboard focus changes of this tree view.
        /// </summary>
        /// <returns>
        /// A new keyboard suspender scope class associated with this tree view.
        /// </returns>
        public DisposableObject SuspendKeyboardFocusChanges()
        {
            return new KeyboardFocusSuspender(this);
        }

        /// <summary>
        /// Ensures that the specified target tree node is completely expanded by expanding all
        /// of its parents.
        /// </summary>
        /// <param name="target">
        /// The tree node that should have all of its parents expanded.
        /// </param>
        internal static void EnsureExpanded(VirtualisedTreeNode target)
        {
            VirtualisedTreeNode parent = target.Parent;
            while (parent != null)
            {
                parent.IsExpanded = true;
                parent = parent.Parent;
            }
        }

        /// <summary>
        /// Gets the filter behaviour used on the specified tree node.
        /// </summary>
        /// <param name="node">
        /// The tree node whose filter behaviour should be retrieved.
        /// </param>
        /// <returns>
        /// The filter behaviour used on the specified tree node.
        /// </returns>
        internal FilterDescendantBehaviour GetFilterBehaviour(VirtualisedTreeNode node)
        {
            if (this.FilteredItemsSource == null)
            {
                return FilterDescendantBehaviour.IncludeDescendantsByDefault;
            }

            object key = this.GetKeyFromItem(node.Item);
            return this.FilteredItemsSource.GetFilterDescendantsBehavior(key);
        }

        /// <summary>
        /// Retrieves a tree node instance for the specified item with the specified parent by
        /// either creating a new instance or returning a existing instance.
        /// </summary>
        /// <param name="item">
        /// The item whose corresponding tree node should be retrieved.
        /// </param>
        /// <param name="parent">
        /// The parent to the retrieved node.
        /// </param>
        /// <returns>
        /// A tree node instance for the specified item with the specified parent.
        /// </returns>
        internal VirtualisedTreeNode GetOrCreateNode(object item, VirtualisedTreeNode parent)
        {
            if (item == null)
            {
                throw new InvalidOperationException(
                    "Item in the VirtualisedTreeView cannot be null");
            }

            object key = this.GetKeyFromItem(item);
            List<VirtualisedTreeNode> nodes;
            if (!this._logicalTree.TryGetValue(key, out nodes))
            {
                nodes = new List<VirtualisedTreeNode>();
                this._logicalTree.Add(key, nodes);
            }
            else
            {
                foreach (VirtualisedTreeNode node in nodes)
                {
                    if (node.Parent == parent)
                    {
                        return node;
                    }
                }
            }

            VirtualisedTreeNode newTreeNode = new VirtualisedTreeNode(this);
            newTreeNode.BeginInit();
            newTreeNode.Item = item;
            newTreeNode.Parent = parent;
            newTreeNode.IsExpanded = newTreeNode.IsExpandedByDefault;
            newTreeNode.EndInit();
            nodes.Add(newTreeNode);
            return newTreeNode;
        }

        /// <summary>
        /// Removes the specified node from the logic tree.
        /// </summary>
        /// <param name="node">
        /// The node to remove.
        /// </param>
        internal void RemoveNodeCore(VirtualisedTreeNode node)
        {
            object key = this.GetKeyFromItem(node.Item);
            List<VirtualisedTreeNode> list;
            if (!this._logicalTree.TryGetValue(key, out list))
            {
                return;
            }

            node.Disconnect();
            list.Remove(node);
            if (list.Count == 0)
            {
                this._logicalTree.Remove(key);
            }
        }

        /// <summary>
        /// Removes the specified node and all of its descendants from the logic tree.
        /// </summary>
        /// <param name="node">
        /// The node to remove.
        /// </param>
        internal void RemoveTreeNode(VirtualisedTreeNode node)
        {
            this.RemoveNodeCore(node);
            using (var enumerator = node.DescendantNodes.GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    VirtualisedTreeNode node2 = (VirtualisedTreeNode)enumerator.Current;
                    this.RemoveNodeCore(node2);
                }
            }
        }

        /// <summary>
        /// Sets the specified item as the anchored item.
        /// </summary>
        /// <param name="item">
        /// The item to set as the current anchor item.
        /// </param>
        internal void SetAnchorItem(object item)
        {
            this.AnchorItem = item;
        }

        /// <summary>
        /// Moves the keyboard focuses into the container that contains the specified node.
        /// </summary>
        /// <param name="node">
        /// The node whose container needs to receive the keyboard focus.
        /// </param>
        /// <returns>
        /// True if the focusing was successful; otherwise, false.
        /// </returns>
        protected bool FocusNode(VirtualisedTreeNode node)
        {
            if (!this.IsDirectlyGainingKeyboardFocus)
            {
                this.ScrollIntoView(node);
            }

            ItemContainerGenerator generator = this.ItemContainerGenerator;
            if (generator == null)
            {
                this._pendingFocusNode = node;
                return false;
            }

            IInputElement container = generator.ContainerFromItem(node) as IInputElement;
            if (container != null)
            {
                this.AnchorItem = node;
                container.Focus();
                this._pendingFocusNode = null;
                return true;
            }

            this._pendingFocusNode = node;
            return false;
        }

        /// <summary>
        /// Creates or identifies the element used to display the child items.
        /// </summary>
        /// <returns>
        /// A new <see cref="RsVirtualisedTreeViewItem"/>.
        /// </returns>
        protected override DependencyObject GetContainerForItemOverride()
        {
            return new RsVirtualisedTreeViewItem();
        }

        /// <summary>
        /// Determines whether the specified item is, or is eligible to be, its own item
        /// container.
        /// </summary>
        /// <param name="item">
        /// The item to check whether it is an item container.
        /// </param>
        /// <returns>
        /// True if the item is eligible to be its own item container; otherwise, false.
        /// </returns>
        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is RsVirtualisedTreeViewItem;
        }

        /// <summary>
        /// Override to add logic to the event of the specified node collapsed.
        /// </summary>
        /// <param name="node">
        /// The node that has been collapsed.
        /// </param>
        protected virtual void OnCollapsed(VirtualisedTreeNode node)
        {
        }

        /// <summary>
        /// Override to add logic to the event of the specified node expanding.
        /// </summary>
        /// <param name="node">
        /// The node that has been expanded.
        /// </param>
        protected virtual void OnExpanded(VirtualisedTreeNode node)
        {
        }

        /// <summary>
        /// Override to add logic to the event of the specified node having its items loaded.
        /// </summary>
        /// <param name="node">
        /// The node whose items have been loaded.
        /// </param>
        protected virtual void OnExpandedItemsLoaded(VirtualisedTreeNode node)
        {
        }

        /// <summary>
        /// Called whenever the <see cref="FilteredItemsSource"/> dependency property changes
        /// on this tree view.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data containing the old and
        /// new values for the <see cref="FilteredItemsSource"/> property.
        /// </param>
        protected virtual void OnFilteredItemsSourceChanged(
            DependencyPropertyChangedEventArgs e)
        {
            IReadOnlyObservableSet oldValue = e.OldValue as IReadOnlyObservableSet;
            IReadOnlyObservableSet newValue = e.NewValue as IReadOnlyObservableSet;
            using (new SelectionPreservationScope(this))
            {
                if (oldValue != null)
                {
                    CollectionChangedEventManager.RemoveHandler(
                        oldValue, this.OnFilteredItemsChanged);
                }

                if (oldValue != null && newValue != null)
                {
                    this.ChangeFilteredItems(oldValue, newValue);
                }
                else
                {
                    if (oldValue != null)
                    {
                        this.RootNode.IsFilterActive = false;
                        this.RemoveFilteredItems(oldValue);
                    }
                    else
                    {
                        if (newValue != null)
                        {
                            this.AddFilteredItems(newValue);
                            this.RootNode.IsFilterActive = true;
                        }
                    }
                }

                if (newValue != null)
                {
                    CollectionChangedEventManager.AddHandler(
                        newValue, this.OnFilteredItemsChanged);
                }
            }
        }

        /// <summary>
        /// Invoked when an unhandled System.Windows.Input.Keyboard.GotKeyboardFocus attached
        /// event reaches an element in its route that is derived from this class.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.KeyboardFocusChangedEventArgs that contains the event
        /// data.
        /// </param>
        protected override void OnGotKeyboardFocus(KeyboardFocusChangedEventArgs e)
        {
            if (e.NewFocus == this)
            {
                this.IsDirectlyGainingKeyboardFocus = true;
                try
                {
                    if (this._delayFocusAnchorItem)
                    {
                        this._delayFocusAnchorItem = false;
                        this.FocusNode(this.AnchorNode);
                    }
                    else
                    {
                        this.FocusDefaultItem();
                    }

                    base.OnGotKeyboardFocus(e);
                    return;
                }
                finally
                {
                    this.IsDirectlyGainingKeyboardFocus = false;
                }
            }

            this._lastFocusedElement = new WeakReference<IInputElement>(e.NewFocus);
            base.OnGotKeyboardFocus(e);
        }

        /// <summary>
        /// Called whenever the <see cref="RootItemsSource"/> dependency property changes on
        /// this tree view.
        /// </summary>
        /// <param name="newValue">
        /// The new iterator the items source has been set to.
        /// </param>
        protected virtual void OnRootItemsSourceChanged(IEnumerable newValue)
        {
            IEnumerable itemsSource = null;
            if (newValue != null)
            {
                itemsSource = new ItemCollectionConverter(newValue, this);
            }

            this.RootNode.ItemsSource = itemsSource;
        }

        /// <summary>
        /// Prepares the specified element to display the specified item.
        /// </summary>
        /// <param name="element">
        /// Element used to display the specified item.
        /// </param>
        /// <param name="item">
        /// Specified item.
        /// </param>
        protected override void PrepareContainerForItemOverride(
            DependencyObject element, object item)
        {
            base.PrepareContainerForItemOverride(element, item);

            RsVirtualisedTreeViewItem control = element as RsVirtualisedTreeViewItem;
            if (control != null)
            {
                VirtualisedTreeNode treeNode = item as VirtualisedTreeNode;
                control.TreeNode = treeNode;
                treeNode.TreeItem = control;
                control.IsSelected = treeNode.IsSelectedByDefault;
                if (control.IsSelected)
                {
                    this._pendingFocusNode = treeNode;
                }

                Binding binding = new Binding("IsExpandable");
                binding.Source = treeNode;
                binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                control.SetBinding(RsVirtualisedTreeViewItem.IsExpandableProperty, binding);
            }

            if (item == this._pendingFocusNode)
            {
                this._pendingFocusNode = null;
                IInputElement inputElement = element as IInputElement;
                if (inputElement != null)
                {
                    inputElement.Focus();
                }
            }
        }

        /// <summary>
        /// Called whenever the <see cref="FilteredItemsSource"/> dependency property changes
        /// on a instance of the <see cref="RsVirtualisedTreeView"/> class.
        /// </summary>
        /// <param name="s">
        /// The instance whose <see cref="FilteredItemsSource"/> dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs instance that contains the
        /// event data.
        /// </param>
        private static void OnFilteredItemsSourceChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            RsVirtualisedTreeView treeView = s as RsVirtualisedTreeView;
            if (treeView == null)
            {
                Debug.Assert(treeView != null, "Incorrect object attached to handler");
                return;
            }

            treeView.OnFilteredItemsSourceChanged(e);
        }

        /// <summary>
        /// Called whenever the <see cref="RootItemsSource"/> dependency property changes on a
        /// instance of the <see cref="RsVirtualisedTreeView"/> class.
        /// </summary>
        /// <param name="s">
        /// The instance whose <see cref="RootItemsSource"/> dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs instance that contains the
        /// event data.
        /// </param>
        private static void OnRootItemsSourceChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            RsVirtualisedTreeView treeView = s as RsVirtualisedTreeView;
            if (treeView == null)
            {
                Debug.Assert(treeView != null, "Incorrect object attached to handler");
                return;
            }

            treeView.OnRootItemsSourceChanged(e.NewValue as IEnumerable);
        }

        /// <summary>
        /// Called whenever new items have been added the filtered items collection.
        /// </summary>
        /// <param name="newItems">
        /// The iterator around the new items.
        /// </param>
        private void AddFilteredItems(IEnumerable newItems)
        {
            foreach (object item in newItems)
            {
                List<VirtualisedTreeNode> nodes = this.GetTreeNodesCore(item);
                if (nodes == null)
                {
                    continue;
                }

                foreach (VirtualisedTreeNode node in nodes)
                {
                    node.IsExplicitlyIncludedInFilter = true;
                }
            }
        }

        /// <summary>
        /// Determines whether the rockstar select all command can be executed at this level
        /// by forwarding the command on to the applications select all command.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.CanExecuteRoutedEventArgs containing the event data.
        /// </param>
        private void CanSelectAll(object sender, CanExecuteRoutedEventArgs e)
        {
            IInputElement target = e.OriginalSource as IInputElement;
            if (target == null)
            {
                return;
            }

            e.CanExecute = this.Items.Count > 0;
        }

        /// <summary>
        /// Swaps out the currently filtered items.
        /// </summary>
        /// <param name="oldItems">
        /// An iterator around the new items that have been removed from the filter.
        /// </param>
        /// <param name="newItems">
        /// An iterator around the new items that have been added to the filter.
        /// </param>
        private void ChangeFilteredItems(IEnumerable oldItems, IEnumerable newItems)
        {
            HashSet<object> lookup = new HashSet<object>(newItems.Cast<object>());

            this.AddFilteredItems(newItems);
            this.RemoveFilteredItems(
                from object item in oldItems
                where !lookup.Contains(item)
                select item);
        }

        /// <summary>
        /// Converts the single source item to the item that will be used in the collection.
        /// </summary>
        /// <param name="item">
        /// The item that will be converted.
        /// </param>
        /// <returns>
        /// The converted object.
        /// </returns>
        private object ConvertItemToCollectionSource(object item)
        {
            if (this.SourceItemToCollectionItemConverter == null)
            {
                return item;
            }

            return this.SourceItemToCollectionItemConverter(item);
        }

        /// <summary>
        /// Moves focus to the item that is said to be the default, which is the last focused
        /// element or the selected item.
        /// </summary>
        private void FocusDefaultItem()
        {
            IInputElement element;
            if (this._lastFocusedElement != null)
            {
                if (this._lastFocusedElement.TryGetTarget(out element))
                {
                    if (element != null)
                    {
                        DependencyObject target = element as DependencyObject;
                        if (target == null || target.IsConnectedToPresentationSource())
                        {
                            element.Focus();
                            return;
                        }
                    }
                }
            }

            this.FocusSelectedItem();
        }

        /// <summary>
        /// Retrieves a list of tree nodes that are representing the specified item currently
        /// inside the logic tree.
        /// </summary>
        /// <param name="item">
        /// The item whose representing tree nodes should be retrieved.
        /// </param>
        /// <returns>
        /// A list of tree nodes that are representing the specified item currently inside the
        /// logic tree.
        /// </returns>
        private List<VirtualisedTreeNode> GetTreeNodesCore(object item)
        {
            if (item == null)
            {
                return null;
            }

            object key = this.GetKeyFromItem(item);
            List<VirtualisedTreeNode> nodes;
            if (this._logicalTree.TryGetValue(key, out nodes))
            {
                return nodes;
            }

            return null;
        }

        /// <summary>
        /// Called whenever the collection currently bound to the
        /// <see cref="FilteredItemsSource"/> dependency property fires its collection changed
        /// event.
        /// </summary>
        /// <param name="sender">
        /// The collection this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Collection.Specialized.NotifyCollectionChangedEventArgs instance
        /// containing the collection changed data.
        /// </param>
        private void OnFilteredItemsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            using (new SelectionPreservationScope(this))
            {
                switch (e.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                        {
                            this.AddFilteredItems(e.NewItems);
                            break;
                        }

                    case NotifyCollectionChangedAction.Remove:
                        {
                            this.RemoveFilteredItems(e.OldItems);
                            break;
                        }

                    case NotifyCollectionChangedAction.Replace:
                        {
                            this.ChangeFilteredItems(e.OldItems, e.NewItems.Cast<object>());
                            break;
                        }

                    case NotifyCollectionChangedAction.Reset:
                        {
                            HashSet<object> hashSet =
                                new HashSet<object>(this.FilteredItemsSource.Cast<object>());
                            foreach (var entry in this._logicalTree)
                            {
                                foreach (VirtualisedTreeNode node in entry.Value)
                                {
                                    node.IsExplicitlyIncludedInFilter =
                                        this.FilteredItemsSource.Contains(node.Item);
                                    hashSet.Remove(node.Item);
                                }
                            }

                            this.AddFilteredItems(hashSet);
                            break;
                        }
                }
            }
        }

        /// <summary>
        /// Called whenever this control is loaded so that keyboard focus can be moved into the
        /// last focused item.
        /// </summary>
        /// <param name="sender">
        /// The objet this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            RsVirtualisedTreeViewItem lastFocusedItem = this.LastFocusedItem;
            if (lastFocusedItem == null)
            {
                return;
            }

            lastFocusedItem.MoveFocus(new TraversalRequest(FocusNavigationDirection.First));
        }

        /// <summary>
        /// Called whenever the select all rockstar routed command is caught at this visual
        /// level so that the application select all command can be fired.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.ExecutedRoutedEventArgs containing the event data.
        /// </param>
        private void OnSelectAll(object sender, ExecutedRoutedEventArgs e)
        {
            IInputElement target = e.OriginalSource as IInputElement;
            if (target == null)
            {
                return;
            }

            this.SelectAll();
        }

        /// <summary>
        /// Called whenever items have been removed from the filtered items collection.
        /// </summary>
        /// <param name="oldItems">
        /// The iterator around the old items.
        /// </param>
        private void RemoveFilteredItems(IEnumerable oldItems)
        {
            foreach (object item in oldItems)
            {
                List<VirtualisedTreeNode> nodes = this.GetTreeNodes(item).ToList();
                using (List<VirtualisedTreeNode>.Enumerator enumerator = nodes.GetEnumerator())
                {
                    while (enumerator.MoveNext())
                    {
                        enumerator.Current.IsExplicitlyIncludedInFilter = false;
                    }
                }
            }
        }

        /// <summary>
        /// Restores the selection based on the currently saved selection.
        /// </summary>
        private void RestorePreviousSelection()
        {
            ISet<object> distinctSelection = this.GetDistinctSelection<object>();
            bool intersects = distinctSelection.Intersect(this._lastSelectedItems).Any();
            if (!intersects)
            {
                VirtualisedTreeNode treeNode = null;
                foreach (object selectedItem in this._lastSelectedItems)
                {
                    IEnumerable<VirtualisedTreeNode> nodes = this.GetTreeNodes(selectedItem);
                    treeNode = nodes.FirstOrDefault();
                    break;
                }

                if (treeNode != null)
                {
                    VirtualisedTreeNode target = treeNode.FindNearestVisibleNode();
                    if (target != null)
                    {
                        this.ChangeSelection(target, SelectionAction.SingleSelection, true);
                    }
                }
            }

            this._lastSelectedItems = null;
        }

        /// <summary>
        /// Saves the current selection to the private <see cref="_lastSelectedItems"/> field.
        /// </summary>
        private void SaveCurrentSelection()
        {
            this._lastSelectedItems = this.GetDistinctSelection<object>();
        }

        /// <summary>
        /// Updates the anchored item to the specified item after a single selection has been
        /// handled.
        /// </summary>
        /// <param name="anchorItem">
        /// The item to set as the anchored item.
        /// </param>
        private void UpdateAnchorItemCore(VirtualisedTreeNode anchorItem)
        {
            this.AnchorNode = anchorItem;

            ItemContainerGenerator generator = this.ItemContainerGenerator;
            if (generator == null)
            {
                Debug.Assert(generator != null, "Unable to find generator");
                return;
            }

            IInputElement container = generator.ContainerFromItem(anchorItem) as IInputElement;
            if (container != null)
            {
                if (this.IsKeyboardFocusWithin && !this.IsKeyboardFocusChangeSuspended)
                {
                    if (!container.IsKeyboardFocusWithin)
                    {
                        this.FocusNode(anchorItem);
                        return;
                    }
                }
                else
                {
                    this._delayFocusAnchorItem = true;
                }
            }
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// The value converter that can be used inside a binding that converters a source
        /// collection to the bound collection for a specified tree view.
        /// </summary>
        private class ItemBindingConverter : IValueConverter
        {
            #region Fields
            /// <summary>
            /// The tree view whose items source will be converted using this converter.
            /// </summary>
            private readonly RsVirtualisedTreeView _treeView;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="ItemBindingConverter"/> class.
            /// </summary>
            /// <param name="treeView">
            /// The tree view whose source collection is being converted.
            /// </param>
            public ItemBindingConverter(RsVirtualisedTreeView treeView)
            {
                if (treeView == null)
                {
                    throw new SmartArgumentNullException(() => treeView);
                }

                this._treeView = treeView;
            }
            #endregion Constructors

            #region Methods
            /// <summary>
            /// Converts the specified value object into a equivalent value of the type
            /// specified by <paramref name="targetType"/>.
            /// </summary>
            /// <param name="value">
            /// The original value to convert.
            /// </param>
            /// <param name="targetType">
            /// The type that the source value will be turned into.
            /// </param>
            /// <param name="parameter">
            /// The converter parameter to use.
            /// </param>
            /// <param name="culture">
            /// The culture to use in the converter.
            /// </param>
            /// <returns>
            /// A converted value.
            /// </returns>
            public object Convert(
                object value, Type targetType, object parameter, CultureInfo culture)
            {
                IEnumerable enumerable = value as IEnumerable;
                if (enumerable == null)
                {
                    return null;
                }

                return new ItemCollectionConverter(enumerable, this._treeView);
            }

            /// <summary>
            /// Converts the specified converted value back to its source value.
            /// </summary>
            /// <param name="value">
            /// The already converted target value to convert back to its source value.
            /// </param>
            /// <param name="targetType">
            /// The type to convert the value to.
            /// </param>
            /// <param name="parameter">
            /// The converter parameter to use.
            /// </param>
            /// <param name="culture">
            /// The culture to use in the converter.
            /// </param>
            /// <returns>
            /// The source value for the specified value.
            /// </returns>
            public object ConvertBack(
                object value, Type targetType, object parameter, CultureInfo culture)
            {
                throw new NotSupportedException();
            }
            #endregion Methods
        } // RsVirtualisedTreeView.ItemBindingConverter {Class}

        /// <summary>
        /// Used to convert the items in the source collection of the tree view to another
        /// type.
        /// </summary>
        private class ItemCollectionConverter :
            CollectionConverter<object, object>,
            ISupportInitializeNotification,
            ISupportInitialize
        {
            #region Fields
            /// <summary>
            /// The source collection.
            /// </summary>
            private readonly IEnumerable _source;

            /// <summary>
            /// The tree view whose items source will be converted using this converter.
            /// </summary>
            private readonly RsVirtualisedTreeView _treeView;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="ItemCollectionConverter"/> class.
            /// </summary>
            /// <param name="source">
            /// The iterator around the source items for this collection.
            /// </param>
            /// <param name="treeView">
            /// The tree view whose source collection is being converted.
            /// </param>
            public ItemCollectionConverter(IEnumerable source, RsVirtualisedTreeView treeView)
                : base(Enumerable.Empty<object>(), null)
            {
                if (source == null)
                {
                    throw new SmartArgumentNullException(() => source);
                }

                if (treeView == null)
                {
                    throw new SmartArgumentNullException(() => treeView);
                }

                this._source = source;
                this._treeView = treeView;
                ICollectionView defaultView = CollectionViewSource.GetDefaultView(source);
                this.ResetSourceItems(defaultView);
                var pattern = this._source as ISupportInitializeNotification;
                if (pattern != null && !pattern.IsInitialized)
                {
                    pattern.Initialized += this.OnInitialized;
                }
            }
            #endregion Constructors

            #region Events
            /// <summary>
            /// Occurs when initialization of the component is completed.
            /// </summary>
            public event EventHandler Initialized;
            #endregion Events

            #region Properties
            /// <summary>
            /// Gets a value indicating whether the component is initialised.
            /// </summary>
            public bool IsInitialized
            {
                get
                {
                    var pattern = this._source as ISupportInitializeNotification;
                    return pattern == null || pattern.IsInitialized;
                }
            }
            #endregion Properties

            #region Methods
            /// <summary>
            /// Signals the object that initialisation is starting.
            /// </summary>
            void ISupportInitialize.BeginInit()
            {
            }

            /// <summary>
            /// Signals the object that initialisation is complete.
            /// </summary>
            void ISupportInitialize.EndInit()
            {
            }

            /// <summary>
            /// Converts the single specified source item to the target type.
            /// </summary>
            /// <param name="item">
            /// The item to convert.
            /// </param>
            /// <returns>
            /// A new item of the target type converted from the specified source item.
            /// </returns>
            protected override object ConvertItem(object item)
            {
                return this._treeView.ConvertItemToCollectionSource(item);
            }

            /// <summary>
            /// Called whenever the source collection has been initialised.
            /// </summary>
            /// <param name="sender">
            /// The collection this handler is attached to.
            /// </param>
            /// <param name="e">
            /// The System.EventArgs data containing the event data.
            /// </param>
            private void OnInitialized(object sender, EventArgs e)
            {
                ////var pattern = sender as ISupportInitializeNotification;
                ////if (pattern != null)
                ////{
                ////    pattern.Initialized -= this.OnInitialized;
                ////}

                EventHandler handler = this.Initialized;
                if (handler == null)
                {
                    return;
                }

                handler(this, EventArgs.Empty);
            }
            #endregion Methods
        } // RsVirtualisedTreeView.ItemCollectionConverter {Class}

        /// <summary>
        /// Provides a way of suspending the keyboard focusing on a specified tree view.
        /// </summary>
        private class KeyboardFocusSuspender : DisposableObject
        {
            #region Fields
            /// <summary>
            /// The instance to the tree view whose selection is being preserved.
            /// </summary>
            private RsVirtualisedTreeView _treeView;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="KeyboardFocusSuspender"/> class
            /// and increase the keyboard lock on the specified tree view.
            /// </summary>
            /// <param name="treeView">
            /// The tree view whose keyboard focus will be suspended.
            /// </param>
            public KeyboardFocusSuspender(RsVirtualisedTreeView treeView)
            {
                if (treeView == null)
                {
                    throw new SmartArgumentNullException(() => treeView);
                }

                this._treeView = treeView;
                this._treeView._keyboardFocusChangeSuspendLock++;
            }
            #endregion Constructors

            #region Methods
            /// <summary>
            /// Decreases the keyboard lock.
            /// </summary>
            protected override void DisposeManagedResources()
            {
                this._treeView._keyboardFocusChangeSuspendLock--;
            }
            #endregion Methods
        } // RsVirtualisedTreeView.VirtualisedTreeViewKeyboardFocusSuspender {Class}

        /// <summary>
        /// Provides a way to preserve the current selection inside a virtualised tree view.
        /// </summary>
        private class SelectionPreservationScope : DisposableObject
        {
            #region Fields
            /// <summary>
            /// The instance to the tree view whose selection is being preserved.
            /// </summary>
            private RsVirtualisedTreeView _treeView;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="SelectionPreservationScope"/>
            /// class.
            /// </summary>
            /// <param name="treeView">
            /// The tree view whose selection should be preserved.
            /// </param>
            public SelectionPreservationScope(RsVirtualisedTreeView treeView)
            {
                if (treeView == null)
                {
                    throw new SmartArgumentNullException(() => treeView);
                }

                this._treeView = treeView;
                if (this._treeView._selectionPreservationScopeCount == 0)
                {
                    this._treeView.SaveCurrentSelection();
                }

                this._treeView._selectionPreservationScopeCount++;
            }
            #endregion Constructors

            #region Methods
            /// <summary>
            /// Restores the selection on the associated tree view if this closes the
            /// preservation scope.
            /// </summary>
            protected override void DisposeManagedResources()
            {
                this._treeView._selectionPreservationScopeCount--;
                if (this._treeView._selectionPreservationScopeCount == 0)
                {
                    this._treeView.RestorePreviousSelection();
                }
            }
            #endregion Methods
        } // RsVirtualisedTreeView.SelectionPreservationScope {Class}
        #endregion Classes
    } // RSG.Editor.Controls.RsVirtualisedTreeView {Class}
} // RSG.Editor.Controls {Namespace}
