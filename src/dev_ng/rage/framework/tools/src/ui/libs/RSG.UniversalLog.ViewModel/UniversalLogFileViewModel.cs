﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Editor;
using RSG.Editor.View;

namespace RSG.UniversalLog.ViewModel
{
    /// <summary>
    /// 
    /// </summary>
    public class UniversalLogFileViewModel : NotifyPropertyChangedBase
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// File path this view model is for.
        /// </summary>
        public String Filepath
        {
            get { return _filepath; ; }
            private set { SetProperty(ref _filepath, value); }
        }
        public String _filepath;

        /// <summary>
        /// Gets the collection of pack entry view models that have been initialised along with
        /// this pack file view model.
        /// </summary>
        public IList<UniversalLogMessageViewModel> Messages
        {
            get { return _messages; }
            private set { SetProperty(ref _messages, value); }
        }
        private IList<UniversalLogMessageViewModel> _messages;
        #endregion // Properties and Associated Member Data

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="PackFileViewModel"/> class.
        /// </summary>
        public UniversalLogFileViewModel(String filepath)
        {
            _filepath = filepath;
            _messages = new List<UniversalLogMessageViewModel>();
        }
        #endregion // Constructors

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        public void OpenFile()
        {
            // Make sure that the file we are trying to load exists.
            if (!File.Exists(Filepath))
            {
                throw new FileNotFoundException(
                    "Unable to create a universal log view model from a path that doesn't exist.", Filepath);
            }

            Messages = LoadFile(Filepath);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filepath"></param>
        /// <returns></returns>
        public void RefreshFile()
        {
            // Update the list of messages for this file.
            Messages = LoadFile(Filepath);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Loads a Universal Log XML document.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private IList<UniversalLogMessageViewModel> LoadFile(object parameter)
        {
            String filepath = (String)parameter;

            // Determine how we should try and open this file up.
            if (Path.GetExtension(filepath) == ".ulogzip")
            {
                return LoadZipFile(filepath);
            }
            else
            {
                return LoadLogFile(filepath);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filepath"></param>
        /// <returns></returns>
        private IList<UniversalLogMessageViewModel> LoadZipFile(String filepath)
        {
            IList<UniversalLogMessageViewModel> results = new List<UniversalLogMessageViewModel>();
            try
            {
                using (ZipArchive zipFile = ZipFile.Open(filepath, ZipArchiveMode.Read))
                {
                    foreach (ZipArchiveEntry entry in zipFile.Entries)
                    {
                        using (Stream fileStream = entry.Open())
                        {
                            XDocument xmlDoc = XDocument.Load(fileStream);
                            results.AddRange(ParseXDocument(xmlDoc));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                String appContext = "File Parsing Error: " + Path.GetFileName(filepath);
                UniversalLogFileBufferedMessage msg = new UniversalLogFileBufferedMessage(LogLevel.ToolException, String.Empty, ex.Message, ex.Message, DateTime.UtcNow);
                results.Add(new UniversalLogMessageViewModel(msg, this, appContext));
            }

            return results;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filepath"></param>
        /// <returns></returns>
        private IList<UniversalLogMessageViewModel> LoadLogFile(String filepath)
        {
            try
            {
                // Parse the ulog file extracting all the messages.
                return ParseXDocument(XDocument.Load(filepath));
            }
            catch (Exception ex)
            {
                String appContext = "File Parsing Error: " + Path.GetFileName(filepath);
                UniversalLogFileBufferedMessage msg = new UniversalLogFileBufferedMessage(LogLevel.ToolException, String.Empty, ex.Message, ex.Message, DateTime.UtcNow);

                IList<UniversalLogMessageViewModel> results = new List<UniversalLogMessageViewModel>();
                results.Add(new UniversalLogMessageViewModel(msg, this, appContext));
                return results;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <returns></returns>
        private IList<UniversalLogMessageViewModel> ParseXDocument(XDocument xmlDoc)
        {
            IList<UniversalLogMessageViewModel> results = new List<UniversalLogMessageViewModel>();
            IDictionary<String, IList<UniversalLogFileBufferedMessage>> messages =
                    UniversalLogFile.GetContextualisedMessageList(xmlDoc);

            // Convert the messages to message vm's.
            foreach (KeyValuePair<String, IList<UniversalLogFileBufferedMessage>> systemContextPair in messages)
            {
                results.AddRange(systemContextPair.Value.Select(msg => new UniversalLogMessageViewModel(msg, this, systemContextPair.Key)));
            }

            // Patch up the profile timing information.
            Stack<UniversalLogMessageViewModel> profileStack = new Stack<UniversalLogMessageViewModel>();
            foreach (UniversalLogMessageViewModel result in results)
            {
                if (result.Level == LogLevel.Profile)
                {
                    profileStack.Push(result);
                }
                else if (result.Level == LogLevel.ProfileEnd && String.IsNullOrEmpty(result.Message))
                {
                    if (profileStack.Any())
                    {
                        UniversalLogMessageViewModel profileStart = profileStack.Pop();
                        TimeSpan elapsedTime = result.LocalTimestamp - profileStart.LocalTimestamp;
                        result.Message = String.Format("{0} took: {1}", profileStart.Message, elapsedTime.ToHumanReadableString());
                    }
                    else
                    {
                        result.Message = "Unmatched profile end";
                    }
                }
            }

            return results;
        }
        #endregion // Private Methods
    } // UniversalLogViewModel
}
