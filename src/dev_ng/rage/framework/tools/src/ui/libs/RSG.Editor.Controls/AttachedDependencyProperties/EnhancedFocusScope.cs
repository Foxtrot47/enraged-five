﻿//---------------------------------------------------------------------------------------------
// <copyright file="EnhancedFocusScope.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.AttachedDependencyProperties
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Media3D;
    using System.Windows.Threading;
    using RSG.Editor.Controls.Helpers;

    /// <summary>
    /// Defines dependency properties that can be attached to a UIElement that enhances the
    /// way the focus is preserved inside them.
    /// </summary>
    public static class EnhancedFocusScope
    {
        #region Fields
        /// <summary>
        /// Identifies the FocusedElement dependency property.
        /// </summary>
        public static readonly DependencyProperty FocusedElementProperty;

        /// <summary>
        /// Identifies the InitialFocusedElement dependency property.
        /// </summary>
        public static readonly DependencyProperty InitialFocusedElementProperty;

        /// <summary>
        /// Identifies the IsEnhancedFocusScope dependency property.
        /// </summary>
        public static readonly DependencyProperty IsEnhancedFocusScopeProperty;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="EnhancedFocusScope"/> class.
        /// </summary>
        static EnhancedFocusScope()
        {
            IsEnhancedFocusScopeProperty =
                DependencyProperty.RegisterAttached(
                "IsEnhancedFocusScope",
                typeof(bool),
                typeof(EnhancedFocusScope),
                new UIPropertyMetadata(false, OnIsEnhancedFocusScopeChanged));

            InitialFocusedElementProperty =
                DependencyProperty.RegisterAttached(
                "InitialFocusedElement",
                typeof(UIElement),
                typeof(EnhancedFocusScope),
                new UIPropertyMetadata(null));

            FocusedElementProperty =
                DependencyProperty.RegisterAttached(
                "FocusedElement",
                typeof(IInputElement),
                typeof(EnhancedFocusScope),
                new UIPropertyMetadata(null));

            EventManager.RegisterClassHandler(
                typeof(UIElement),
                FocusManager.GotFocusEvent,
                new RoutedEventHandler(OnGotFocusEvent));
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Find the closest visual ancestor that has IsEnhancedFocusScope set to true.
        /// </summary>
        /// <param name="element">
        /// The element whose closest visual ancestor will be found.
        /// </param>
        /// <returns>
        /// The closest visual ancestor that has IsEnhancedFocusScope set to true from the
        /// specified element.
        /// </returns>
        public static DependencyObject GetEnhancedFocusScope(DependencyObject element)
        {
            if (element == null)
            {
                throw new SmartArgumentNullException(() => element);
            }

            if (GetIsEnhancedFocusScope(element))
            {
                return element;
            }

            if (element is UIElement || element is ContentElement || element is UIElement3D)
            {
                DependencyObject logicalParent = LogicalTreeHelper.GetParent(element);
                if (logicalParent != null)
                {
                    return GetEnhancedFocusScope(logicalParent);
                }
            }

            if (element is Visual || element is Visual3D)
            {
                DependencyObject visualParent = VisualTreeHelper.GetParent(element);
                if (visualParent != null)
                {
                    return GetEnhancedFocusScope(visualParent);
                }
            }

            return null;
        }

        /// <summary>
        /// The <see cref="InitialFocusedElementProperty"/> getter method.
        /// </summary>
        /// <param name="element">
        /// The UI element that the property is attached to.
        /// </param>
        /// <returns>
        /// The value of the InitialFocusedElement attached dependency property on the
        /// specified UI element.
        /// </returns>
        public static FrameworkElement GetInitialFocusedElement(UIElement element)
        {
            return (FrameworkElement)element.GetValue(InitialFocusedElementProperty);
        }

        /// <summary>
        /// The <see cref="IsEnhancedFocusScopeProperty"/> getter method.
        /// </summary>
        /// <param name="element">
        /// The UI element that the property is attached to.
        /// </param>
        /// <returns>
        /// The value of the IsEnhancedFocusScope attached dependency property on the
        /// specified UI element.
        /// </returns>
        public static bool GetIsEnhancedFocusScope(DependencyObject element)
        {
            return (bool)element.GetValue(IsEnhancedFocusScopeProperty);
        }

        /// <summary>
        /// The <see cref="FocusedElementProperty"/> getter method.
        /// </summary>
        /// <param name="element">
        /// The UI element that the property is attached to.
        /// </param>
        /// <returns>
        /// The value of the FocusedElement attached dependency property on the specified UI
        /// element.
        /// </returns>
        public static IInputElement GetFocusedElement(DependencyObject element)
        {
            return (IInputElement)element.GetValue(FocusedElementProperty);
        }

        /// <summary>
        /// Sets the focus inside a enhanced focus scope.
        /// </summary>
        /// <param name="element">
        /// The scope to set the focus inside.
        /// </param>
        /// <param name="delay">
        /// A value indicating whether the operation should be started as a background action.
        /// </param>
        public static void SetFocusOnActiveElementInScope(FrameworkElement element, bool delay)
        {
            if (element == null)
            {
                return;
            }

            DependencyObject focusScope = GetEnhancedFocusScope(element);
            if (focusScope == null)
            {
                return;
            }

            IInputElement focusedInput = EnhancedFocusScope.GetFocusedElement(focusScope);
            FrameworkElement focusedElement = focusedInput as FrameworkElement;
            TraversalRequest traversal = new TraversalRequest(FocusNavigationDirection.First);
            if (focusedElement != null)
            {
                if (delay)
                {
                    DispatcherHelper.BeginInvokeOnUI(
                        new Action(() =>
                        {
                            focusedElement.MoveFocus(traversal);
                        }),
                        DispatcherPriority.Render);
                }
                else
                {
                    focusedElement.MoveFocus(traversal);
                }

                return;
            }
        }

        /// <summary>
        /// The <see cref="InitialFocusedElementProperty"/> setter method.
        /// </summary>
        /// <param name="element">
        /// The UI element that the property is attached to.
        /// </param>
        /// <param name="value">
        /// The value the property should be set to.
        /// </param>
        public static void SetInitialFocusedElement(FrameworkElement element, UIElement value)
        {
            element.SetValue(InitialFocusedElementProperty, value);
        }

        /// <summary>
        /// The <see cref="IsEnhancedFocusScopeProperty"/> setter method.
        /// </summary>
        /// <param name="element">
        /// The UI element that the property is attached to.
        /// </param>
        /// <param name="value">
        /// The value the property should be set to.
        /// </param>
        public static void SetIsEnhancedFocusScope(DependencyObject element, bool value)
        {
            element.SetValue(IsEnhancedFocusScopeProperty, value);
        }

        /// <summary>
        /// Gets the element that should initially receive the keyboard focus inside the
        /// specified root.
        /// </summary>
        /// <param name="root">
        /// The root element to start our search.
        /// </param>
        /// <returns>
        /// The element that should initially receive the keyboard focus inside the specified
        /// root.
        /// </returns>
        private static FrameworkElement GetInitialElement(FrameworkElement root)
        {
            if (root == null)
            {
                return null;
            }

            DependencyProperty property = EnhancedFocusScope.InitialFocusedElementProperty;
            FrameworkElement element = EnhancedFocusScope.GetInitialFocusedElement(root);

            FrameworkElement childElement = GetInitialElement(element);
            if (childElement != null)
            {
                return childElement;
            }

            return element;
        }

        /// <summary>
        /// Called whenever a control is loaded that has a InitialFocusedElement set on it.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private static void InitialFocusScopeLoaded(object sender, RoutedEventArgs e)
        {
            FrameworkElement item = sender as FrameworkElement;
            if (item == null)
            {
                return;
            }

            UIElement initialFocus = GetInitialFocusedElement(item);
            if (initialFocus == null)
            {
                return;
            }

            TraversalRequest request = new TraversalRequest(FocusNavigationDirection.First);
            initialFocus.MoveFocus(request);
        }

        /// <summary>
        /// Gets called after a control that is a enhanced focus scope is loaded.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs data for this event.
        /// </param>
        private static void OnEnhancedFocusScopeLoaded(object sender, RoutedEventArgs e)
        {
            FrameworkElement element = (FrameworkElement)sender;
            element.Loaded -= OnEnhancedFocusScopeLoaded;

            DependencyProperty property = EnhancedFocusScope.InitialFocusedElementProperty;
            FrameworkElement focusedElement = GetInitialElement(element);
            if (element == null)
            {
                focusedElement = element;
            }

            if (focusedElement is DataGrid)
            {
                DataGridCell cell = focusedElement.GetVisualDescendent<DataGridCell>();
                if (cell != null)
                {
                    focusedElement = cell;
                }
            }
            else
            {
                ItemsControl itemsControl = focusedElement as ItemsControl;
                if (itemsControl != null)
                {
                    DependencyObject container =
                        itemsControl.ItemContainerGenerator.ContainerFromIndex(0);
                    FrameworkElement containerElement = container as FrameworkElement;
                    if (containerElement != null)
                    {
                        focusedElement = containerElement;
                    }
                }
            }

            DispatcherHelper.BeginInvokeOnUI(
                new Action(
                delegate
                {
                    focusedElement.MoveFocus(
                        new TraversalRequest(FocusNavigationDirection.First));
                }),
                DispatcherPriority.Render);
        }

        /// <summary>
        /// Called whenever a UI element gets keyboard focus and registers itself with its
        /// parent scope.
        /// </summary>
        /// <param name="s">
        /// The object that has fired the event.
        /// </param>
        /// <param name="e">
        /// The object containing the event arguments, including the original sender.
        /// </param>
        private static void OnGotFocusEvent(object s, RoutedEventArgs e)
        {
            if (Object.ReferenceEquals(s, e.OriginalSource))
            {
                DependencyObject scope = GetEnhancedFocusScope(s as DependencyObject);
                if (scope != null)
                {
                    scope.SetValue(FocusedElementProperty, s as IInputElement);
                }
            }
        }

        /// <summary>
        /// Gets called whenever the value for the IsEnhancedFocusScope dependency
        /// property changes.
        /// </summary>
        /// <param name="s">
        /// The window whose IsEnhancedFocusScope dependency property changed on.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs that provides the data for
        /// this event.
        /// </param>
        private static void OnIsEnhancedFocusScopeChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            FrameworkElement item = s as FrameworkElement;
            if (item == null)
            {
                return;
            }

            if ((bool)e.NewValue)
            {
                item.Loaded += OnEnhancedFocusScopeLoaded;
            }
            else
            {
                item.Loaded -= OnEnhancedFocusScopeLoaded;
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.AttachedDependencyProperties.EnhancedFocusScope {Class}
} // RSG.Editor.Controls.AttachedDependencyProperties {Namespace}
