﻿using RSG.Rag.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.ViewModel.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    public class SliderFloatViewModel : SliderViewModel<Single>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        public SliderFloatViewModel(WidgetSlider<Single> widget)
            : base(widget)
        {
        }
        #endregion // Constructor(s)
    } // SliderFloatViewModel
}
