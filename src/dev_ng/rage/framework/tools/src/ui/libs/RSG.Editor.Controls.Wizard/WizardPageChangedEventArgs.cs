﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Editor.Controls.Wizard
{

    /// <summary>
    /// Event argument class for WizardPageChanged event; specifies the page
    /// user is navigating from and page navigating to.
    /// </summary>
    public class WizardPageChangedEventArgs : EventArgs
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public NavigateDirection Direction
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public WizardPage FromPage
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public WizardPage ToPage
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="direction"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        public WizardPageChangedEventArgs(NavigateDirection direction, WizardPage from, WizardPage to)
        {
            this.Direction = direction;
            this.FromPage = from;
            this.ToPage = to;
        }
        #endregion // Constructor(s)
    }

} // RSG.Editor.Controls.Wizard namespace
