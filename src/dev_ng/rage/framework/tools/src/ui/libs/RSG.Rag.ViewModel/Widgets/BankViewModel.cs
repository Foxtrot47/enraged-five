﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using RSG.Rag.Core;
using RSG.Rag.Widgets;

namespace RSG.Rag.ViewModel.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    public class BankViewModel : GroupBaseViewModel<WidgetBank>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        /// <param name="vmFactory"></param>
        public BankViewModel(WidgetBank widget, WidgetViewModelFactory vmFactory)
            : base(widget, vmFactory)
        {
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Gets the bitmap source that is used to display the icon for this item.
        /// </summary>
        public override BitmapSource Icon
        {
            get { return CommonRagIcons.Bank; }
        }
        #endregion // Properties
    } // BankViewModel
}
