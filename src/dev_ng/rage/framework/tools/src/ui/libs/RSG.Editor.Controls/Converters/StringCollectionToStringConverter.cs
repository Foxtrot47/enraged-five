﻿//---------------------------------------------------------------------------------------------
// <copyright file="StringCollectionToStringConverter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;

    /// <summary>
    /// Converter used to convert an enumerable of strings to a single string object for
    /// display.
    /// </summary>
    public class StringCollectionToStringConverter
        : ValueConverter<IEnumerable<string>, string>
    {
        #region Methods
        /// <summary>
        /// Converters the given value and returns the result.
        /// </summary>
        /// <param name="value">
        /// The original value to convert.
        /// </param>
        /// <param name="param">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A converted value.
        /// </returns>
        protected override string Convert(
            IEnumerable<string> value, object param, CultureInfo culture)
        {
            if (value == null)
            {
                return null;
            }

            return String.Join(Environment.NewLine, value);
        }
        #endregion Methods
    } // RSG.Editor.Controls.Converters.StringCollectionToStringConverter {Class}
} // RSG.Editor.Controls.Converters {Namespace}
