﻿//---------------------------------------------------------------------------------------------
// <copyright file="StructureProjectItemDefinition.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Project
{
    using System.IO;
    using System.Text;
    using System.Windows.Media.Imaging;
    using System.Xml;
    using RSG.Base;
    using RSG.Editor;
    using RSG.Metadata.Model.Definitions;
    using RSG.Project.ViewModel.Definitions;

    /// <summary>
    /// Provides the item definition that represents a parCodeGen structure definition for a
    /// specified extension.
    /// </summary>
    public class StructureProjectItemDefinition : ProjectItemDefinition
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Extension"/> property.
        /// </summary>
        private string _extension;

        /// <summary>
        /// The private field used for the <see cref="Structure"/> property.
        /// </summary>
        private IStructure _structure;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="StructureProjectItemDefinition"/>
        /// class.
        /// </summary>
        /// <param name="structure">
        /// The parCodeGen structure definition that this item definition is representing.
        /// </param>
        /// <param name="extension">
        /// The file extension for this item definition.
        /// </param>
        public StructureProjectItemDefinition(IStructure structure, string extension)
        {
            if (structure == null)
            {
                throw new SmartArgumentNullException(() => structure);
            }

            if (extension == null)
            {
                throw new SmartArgumentNullException(() => extension);
            }

            this._extension = extension;
            this._structure = structure;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the name that is used as the default value for a new instance of this
        /// definition.
        /// </summary>
        public override string DefaultName
        {
            get { return this._structure.ShortDataType; }
        }

        /// <summary>
        /// Gets the description for this item definition to be displayed in the add dialog.
        /// </summary>
        public override string Description
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the extension used for this defined item.
        /// </summary>
        public override string Extension
        {
            get { return this._extension; }
        }

        /// <summary>
        /// Gets the icon that is used to display this project item.
        /// </summary>
        public override BitmapSource Icon
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the name for this defined project.
        /// </summary>
        public override string Name
        {
            get { return this._structure.DataType; }
        }

        /// <summary>
        /// Gets a byte array containing the data that is used to create a new instance of the
        /// defined project.
        /// </summary>
        public override byte[] Template
        {
            get
            {
                Utf8StringWriter stringWriter = new Utf8StringWriter();
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;
                settings.Encoding = Encoding.UTF8;
                using (XmlWriter writer = XmlWriter.Create(stringWriter, settings))
                {
                    writer.WriteStartDocument();
                    writer.WriteStartElement(this._structure.MetadataName);
                    writer.WriteEndElement();
                }

                return Encoding.UTF8.GetBytes(stringWriter.GetStringBuilder().ToString());
            }
        }

        /// <summary>
        /// Gets the type of this item to be displayed in the add dialog.
        /// </summary>
        public override string Type
        {
            get { return this._extension.Replace('.', ' ').Trim().ToUpperInvariant(); }
        }
        #endregion Properties
    } // RSG.Metadata.ViewModel.Project.StructureProjectItemDefinition {Class}
} // RSG.Metadata.ViewModel.Project {Namespace}
