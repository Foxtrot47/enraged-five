﻿//---------------------------------------------------------------------------------------------
// <copyright file="DialogueProjectProperties.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.ViewModel.Project
{
    using RSG.Editor.Model;
    using RSG.Project.Model;
    using RSG.Project.ViewModel;

    /// <summary>
    /// Represents the properties shown to the user when editing the properties for a normal
    /// dialogue project.
    /// </summary>
    public class DialogueProjectProperties : ModelBase, IProjectPropertyContent
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="AudioPath"/> property.
        /// </summary>
        private string _audioPath;

        /// <summary>
        /// The private field used for the <see cref="BuildAmbient"/> property.
        /// </summary>
        private bool _buildAmbient;

        /// <summary>
        /// The private field used for the <see cref="BuildMultiplayer"/> property.
        /// </summary>
        private bool _buildMultiplayer;

        /// <summary>
        /// The private field used for the <see cref="BuildNonPlaceholder"/> property.
        /// </summary>
        private bool _buildNonPlaceholder;

        /// <summary>
        /// The private field used for the <see cref="BuildPlaceholder"/> property.
        /// </summary>
        private bool _buildPlaceholder;

        /// <summary>
        /// The private field used for the <see cref="BuildPsoData"/> property.
        /// </summary>
        private bool _buildPsoData;

        /// <summary>
        /// The private field used for the <see cref="BuildSingleplayer"/> property.
        /// </summary>
        private bool _buildSingleplayer;

        /// <summary>
        /// The private field used for the <see cref="ConfigurationPath"/> property.
        /// </summary>
        private string _configurationPath;
        /// <summary>
        /// The private field used for the <see cref="DialogueDataFilename"/> property.
        /// </summary>
        private string _dialogueDataFilename;

        /// <summary>
        /// The private field used for the <see cref="DialogueFilename"/> property.
        /// </summary>
        private string _dialogueFilename;

        /// <summary>
        /// The private field used for the <see cref="DialogueNetDataFilename"/> property.
        /// </summary>
        private string _dialogueNetDataFilename;

        /// <summary>
        /// The private field used for the <see cref="DialogueNetFilename"/> property.
        /// </summary>
        private string _dialogueNetFilename;

        /// <summary>
        /// The private field used for the <see cref="EdlTimingsDirectory"/> property.
        /// </summary>
        private string _edlTimingsDirectory;
        /// <summary>
        /// The private field used for the <see cref="OutputPath"/> property.
        /// </summary>
        private string _outputPath;

        /// <summary>
        /// The private reference to the project whose properties are being wrapped.
        /// </summary>
        private ProjectNode _project;

        /// <summary>
        /// The private field used for the <see cref="PsoDataOutputPath"/> property.
        /// </summary>
        private string _psoDataOutputPath;

        /// <summary>
        /// The private field used for the <see cref="UndoEngine"/> property.
        /// </summary>
        private UndoEngine _undoEngine;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DialogueProjectProperties"/> class.
        /// </summary>
        /// <param name="project">
        /// The project whose properties are being wrapped.
        /// </param>
        /// <param name="undoEngine">
        /// The undo engine that this class should be using.
        /// </param>
        public DialogueProjectProperties(ProjectNode project, UndoEngine undoEngine)
        {
            this._project = project;
            this._undoEngine = undoEngine;
            this._undoEngine.DirtyStateChanged += this.OnDirtyStateChanged;
            this.BindProperties();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender">
        /// 
        /// </param>
        /// <param name="e">
        /// 
        /// </param>
        void OnDirtyStateChanged(object sender, System.EventArgs e)
        {
            this._project.ModifiedProperties = this._undoEngine.DirtyState;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the path to the audio wav files that can be played.
        /// </summary>
        public string AudioPath
        {
            get { return this._audioPath; }
            set { this.SetProperty(ref this._audioPath, value); }
        }

        /// <summary>
        /// Gets or sets the build ambient property.
        /// </summary>
        public bool BuildAmbient
        {
            get { return this._buildAmbient; }
            set { this.SetProperty(ref this._buildAmbient, value); }
        }

        /// <summary>
        /// Gets or sets the build multiplayer property.
        /// </summary>
        public bool BuildMultiplayer
        {
            get { return this._buildMultiplayer; }
            set { this.SetProperty(ref this._buildMultiplayer, value); }
        }

        /// <summary>
        /// Gets or sets the build non-placeholder property.
        /// </summary>
        public bool BuildNonPlaceholder
        {
            get { return this._buildNonPlaceholder; }
            set { this.SetProperty(ref this._buildNonPlaceholder, value); }
        }

        /// <summary>
        /// Gets or sets the build placeholder property.
        /// </summary>
        public bool BuildPlaceholder
        {
            get { return this._buildPlaceholder; }
            set { this.SetProperty(ref this._buildPlaceholder, value); }
        }

        /// <summary>
        /// Gets or sets the build pso data property.
        /// </summary>
        public bool BuildPsoData
        {
            get { return this._buildPsoData; }
            set { this.SetProperty(ref this._buildPsoData, value); }
        }

        /// <summary>
        /// Gets or sets the build singleplayer property.
        /// </summary>
        public bool BuildSingleplayer
        {
            get { return this._buildSingleplayer; }
            set { this.SetProperty(ref this._buildSingleplayer, value); }
        }

        /// <summary>
        /// Gets or sets the value of the configuration path property.
        /// </summary>
        public string ConfigurationPath
        {
            get { return this._configurationPath; }
            set { this.SetProperty(ref this._configurationPath, value); }
        }
        /// <summary>
        /// Gets or sets the value of the dialogue files name path property.
        /// </summary>
        public string DialogueDataFilename
        {
            get { return this._dialogueDataFilename; }
            set { this.SetProperty(ref this._dialogueDataFilename, value); }
        }

        /// <summary>
        /// Gets or sets the value of the dialogue name path property.
        /// </summary>
        public string DialogueFilename
        {
            get { return this._dialogueFilename; }
            set { this.SetProperty(ref this._dialogueFilename, value); }
        }

        /// <summary>
        /// Gets or sets the value of the dialogue net files name path property.
        /// </summary>
        public string DialogueNetDataFilename
        {
            get { return this._dialogueNetDataFilename; }
            set { this.SetProperty(ref this._dialogueNetDataFilename, value); }
        }

        /// <summary>
        /// Gets or sets the value of the dialogue net name path property.
        /// </summary>
        public string DialogueNetFilename
        {
            get { return this._dialogueNetFilename; }
            set { this.SetProperty(ref this._dialogueNetFilename, value); }
        }

        /// <summary>
        /// Gets or sets the value of the path to the edl timing files from new york to
        /// retrieve timing information from.
        /// </summary>
        public string EdlTimingsDirectory
        {
            get { return this._edlTimingsDirectory; }
            set { this.SetProperty(ref this._edlTimingsDirectory, value); }
        }

        /// <summary>
        /// Gets or sets the value of the output path property.
        /// </summary>
        public string OutputPath
        {
            get { return this._outputPath; }
            set { this.SetProperty(ref this._outputPath, value); }
        }
        /// <summary>
        /// Gets the project that the propertirs in this class are bound to.
        /// </summary>
        public Project Project
        {
            get { return this._project.Model as Project; }
        }

        /// <summary>
        /// Gets or sets the output path that the conversation pso metadata data gets built to.
        /// </summary>
        public string PsoDataOutputPath
        {
            get { return this._psoDataOutputPath; }
            set { this.SetProperty(ref this._psoDataOutputPath, value); }
        }
        /// <summary>
        /// Gets the instance of the <see cref="RSG.Editor.Model.UndoEngine"/> class that this
        /// object is using as its undo engine.
        /// </summary>
        public override UndoEngine UndoEngine
        {
            get { return this._undoEngine; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        public void ApplyProperties()
        {
            this._project.SetDependentBoolProperty("PsoData", this.BuildPsoData);
            this._project.SetDependentBoolProperty("Multiplayer", this.BuildMultiplayer);
            this._project.SetDependentBoolProperty("Singleplayer", this.BuildSingleplayer);
            this._project.SetDependentBoolProperty("Placeholder", this.BuildPlaceholder);
            this._project.SetDependentBoolProperty("NonPlaceholder", this.BuildNonPlaceholder);
            this._project.SetDependentBoolProperty("Ambient", this.BuildAmbient);
            this._project.SetIndependentProperty("Configurations", this.ConfigurationPath);
            this._project.SetIndependentProperty("AudioPath", this.AudioPath);

            this._project.SetDependentProperty("OutputPath", this.OutputPath);
            this._project.SetDependentProperty("DialogueFilename", this.DialogueFilename);
            this._project.SetDependentProperty(
                "DialogueDataFilename", this.DialogueDataFilename);

            this._project.SetDependentProperty(
                "DialogueNetFilename", this.DialogueNetFilename);

            this._project.SetDependentProperty(
                "DialogueNetDataFilename", this.DialogueNetDataFilename);

            this._project.SetIndependentProperty(
                "PsoDataOutputPath", this.PsoDataOutputPath);

            this._project.SetIndependentProperty(
                "EdlTimingsDirectory", this.EdlTimingsDirectory);
        }

        /// <summary>
        /// 
        /// </summary>
        private void BindProperties()
        {
            using (new SuspendUndoEngine(this.UndoEngine, SuspendUndoEngineMode.Both))
            {
                this.UpdateBindingToProject();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void UpdateBindingToProject()
        {
            this.BuildPsoData = this._project.GetPropertyAsBool("PsoData");
            this.BuildMultiplayer = this._project.GetPropertyAsBool("Multiplayer");
            this.BuildSingleplayer = this._project.GetPropertyAsBool("Singleplayer");
            this.BuildPlaceholder = this._project.GetPropertyAsBool("Placeholder");
            this.BuildNonPlaceholder = this._project.GetPropertyAsBool("NonPlaceholder");
            this.BuildAmbient = this._project.GetPropertyAsBool("Ambient");
            this.ConfigurationPath = this._project.GetProperty("Configurations");
            this.AudioPath = this._project.GetProperty("AudioPath");

            this.PsoDataOutputPath = this._project.GetProperty("PsoDataOutputPath");
            this.EdlTimingsDirectory = this._project.GetProperty("EdlTimingsDirectory");

            this.OutputPath = this._project.GetProperty("OutputPath");
            this.DialogueFilename = this._project.GetProperty("DialogueFilename");
            this.DialogueDataFilename = this._project.GetProperty("DialogueDataFilename");
            this.DialogueNetFilename = this._project.GetProperty("DialogueNetFilename");
            this.DialogueNetDataFilename =
                this._project.GetProperty("DialogueNetDataFilename");

        }
        #endregion Methods
    } // RSG.Text.ViewModel.Project.DialogueProjectProperties {Class}
} // RSG.Text.ViewModel.Project {Namespace}
