﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsReorderTabPanel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Represents the base panel for all the panels used for the docking pane tab controls.
    /// </summary>
    public abstract class RsReorderTabPanel : Panel
    {
        #region Fields
        /// <summary>
        /// Identifies the PanelLayoutUpdated routed event.
        /// </summary>
        public static readonly RoutedEvent PanelLayoutUpdatedEvent;

        /// <summary>
        /// Indicates whether a event handler has been attached to the base UIElements
        /// layout updated event.
        /// </summary>
        private bool _layoutUpdatedHandlerAdded;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsReorderTabPanel"/> class.
        /// </summary>
        static RsReorderTabPanel()
        {
            PanelLayoutUpdatedEvent =
                EventManager.RegisterRoutedEvent(
                "PanelLayoutUpdated",
                RoutingStrategy.Direct,
                typeof(RoutedEventHandler),
                typeof(RsReorderTabPanel));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsReorderTabPanel"/> class.
        /// </summary>
        public RsReorderTabPanel()
        {
            this._layoutUpdatedHandlerAdded = false;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether this panel needs to raise its
        /// PanelLayoutUpdated routed event when the layout is updated.
        /// </summary>
        public bool IsNotificationNeeded
        {
            get
            {
                return this._layoutUpdatedHandlerAdded;
            }

            set
            {
                if (value == this._layoutUpdatedHandlerAdded)
                {
                    return;
                }

                if (value)
                {
                    this.LayoutUpdated += this.OnLayoutUpdated;
                }
                else
                {
                    this.LayoutUpdated -= this.OnLayoutUpdated;
                }

                this._layoutUpdatedHandlerAdded = value;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Gets called whenever the layout for this UIElement is signalled as updated.
        /// </summary>
        /// <param name="sender">
        /// The element that this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.EventArgs data for this event.
        /// </param>
        private void OnLayoutUpdated(object sender, EventArgs e)
        {
            this.RaiseEvent(new RoutedEventArgs(PanelLayoutUpdatedEvent));
            this.IsNotificationNeeded = false;
        }
        #endregion Methods
    } // RSG.Editor.Controls.Dock.RsReorderTabPanel {Class}
} // RSG.Editor.Controls.Dock {Namespace}
