﻿//---------------------------------------------------------------------------------------------
// <copyright file="DialogueLineControl.xaml.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.View
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Data;
    using System.Windows.Input;
    using System.Windows.Threading;
    using RSG.Editor;
    using RSG.Editor.Controls;
    using RSG.Editor.Controls.AttachedDependencyProperties;
    using RSG.Editor.Controls.Helpers;
    using RSG.Editor.Model;
    using RSG.Editor.SharedCommands;
    using RSG.Text.Commands;
    using RSG.Text.Model;
    using RSG.Text.ViewModel;

    /// <summary>
    /// Defines the control that can be used to display the lines of a single dialogue
    /// conversation and manipulate them.
    /// </summary>
    [GuidAttribute("477C3FD9-9398-4DE3-B8EE-C772C2310B9D")]
    public partial class DialogueLineControl : RsUserControl
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="SelectedItem" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty SelectedItemProperty;

        /// <summary>
        /// The private field used for the <see cref="ColumnVisibilities"/> property.
        /// </summary>
        private static LineColumnVisibilities _columnVisibilities;

        /// <summary>
        /// The private field used for the <see cref="MultiLineEditCommand"/> property.
        /// </summary>
        private static RoutedCommand _multiLineEditCommand;

        /// <summary>
        /// The private event handler that sorts the anonymous delegate for the items source
        /// changed event.
        /// </summary>
        private NotifyCollectionChangedEventHandler _collectionChangedHandler;

        /// <summary>
        /// Identifies the <see cref="SelectedItems" /> dependency property.
        /// </summary>
        private IEnumerable<LineViewModel> _selectedItems;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="DialogueLineControl"/> class.
        /// </summary>
        static DialogueLineControl()
        {
            _columnVisibilities = new LineColumnVisibilities();

            SelectedItemProperty =
                DependencyProperty.Register(
                "SelectedItem",
                typeof(object),
                typeof(DialogueLineControl),
                new FrameworkPropertyMetadata(null, OnSelectedItemChanged));

            ItemsControl.ItemsSourceProperty.AddOwner(
                typeof(DialogueLineControl), new PropertyMetadata(OnItemsSourceChanged));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="DialogueLineControl"/> class.
        /// </summary>
        public DialogueLineControl()
        {
            this.InitializeComponent();
            this.AttachCommandBindings();
            this.LineDataGrid.SelectionChanged += this.OnSelectionChanged;

            this.CommandBindings.Add(
                new CommandBinding(MultiLineEditCommand, this.ExecuteMultiEdit));

            ICollectionView _customerView = CollectionViewSource.GetDefaultView(
                this.LineDataGrid.Items);
            _customerView.Filter = FilterItem;

            LineFilters = (IEnumerable<LineFilter>)Enum.GetValues(typeof(LineFilter));

            MenuItem fieldChanger = this.TryFindResource("FieldChangerMenuItem") as MenuItem;
            Binding fieldChangerBinding = new Binding();
            fieldChangerBinding.Source = this.CompositePropertyChanges;
            fieldChangerBinding.BindsDirectlyToSource = true;
            fieldChanger.SetBinding(MenuItem.ItemsSourceProperty, fieldChangerBinding);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the static object containing the visibility property for the data grid
        /// columns.
        /// </summary>
        public static LineColumnVisibilities ColumnVisibilities
        {
            get { return _columnVisibilities; }
        }

        /// <summary>
        /// Gets the command that can be used to edit a single value of a collection of lines.
        /// </summary>
        public static RoutedCommand MultiLineEditCommand
        {
            get
            {
                if (_multiLineEditCommand == null)
                {
                    _multiLineEditCommand
                        = new RoutedCommand(
                            "MultiLineEditCommand", typeof(DialogueLineControl));
                }

                return _multiLineEditCommand;
            }
        }

        /// <summary>
        /// Gets a collection of composite property changes that are support for the line
        /// objects shown on the data grid.
        /// </summary>
        public List<CompositePropertyChange> CompositePropertyChanges
        {
            get
            {
                List<CompositePropertyChange> changes = new List<CompositePropertyChange>();
                changes.Add(new AudioTypePropertyChange());
                changes.Add(new AudibilityPropertyChange());
                changes.Add(new SpecialPropertyChange());
                changes.Add(new DucksRadioPropertyChange());
                return changes;
            }
        }

        /// <summary>
        /// Gets the command items that should be displayed on the context menu.
        /// </summary>
        public CommandBarItemViewModelCollection ContextMenuItems
        {
            get { return CommandBarItemViewModel.Create(TextViewCommandIds.LineContextMenu); }
        }

        /// <summary>
        /// Gets or sets a collection used to generate the tool bars for this tool bar tray.
        /// </summary>
        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)this.GetValue(ItemsControl.ItemsSourceProperty); }
            set { this.SetValue(ItemsControl.ItemsSourceProperty, value); }
        }

        /// <summary>
        /// Gets or sets the first item in the current selection or null if the selection is
        /// empty.
        /// </summary>
        public object SelectedItem
        {
            get { return (object)this.GetValue(SelectedItemProperty); }
            set { this.SetValue(SelectedItemProperty, value); }
        }

        /// <summary>
        /// Gets the currently selected items in this control.
        /// </summary>
        public IEnumerable<LineViewModel> SelectedItems
        {
            get { return this._selectedItems; }
        }

        private IEnumerable<LineFilter> _lineFilters;

        public IEnumerable<LineFilter> LineFilters
        {
            get
            {
                return _lineFilters;
            }
            set
            {
                _lineFilters = value;
                ICollectionView _customerView = CollectionViewSource.GetDefaultView(this.LineDataGrid.Items);
                _customerView.Filter = FilterItem;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the layout for this control get serialised on
        /// exit and deserialised after loading.
        /// </summary>
        protected override bool MakeLayoutPersistent
        {
            get { return true; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Called whenever the ItemsSource dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The object whose ItemsSource dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnItemsSourceChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            DialogueLineControl control = s as DialogueLineControl;
            if (control == null)
            {
                return;
            }

            INotifyCollectionChanged oldCollection = e.OldValue as INotifyCollectionChanged;
            if (oldCollection != null)
            {
                oldCollection.CollectionChanged -= control._collectionChangedHandler;
            }

            INotifyCollectionChanged collection = e.NewValue as INotifyCollectionChanged;
            if (collection != null)
            {
                control._collectionChangedHandler = (sender, args) =>
                {
                    if (args.Action == NotifyCollectionChangedAction.Add)
                    {
                        foreach (object item in args.NewItems)
                        {
                            control.Dispatcher.BeginInvoke(
                            (Action)(() =>
                            {
                                control.LineDataGrid.ScrollIntoView(item);
                                control.LineDataGrid.SelectedItem = item;

                                DependencyObject container = control.LineDataGrid.ItemContainerGenerator.ContainerFromItem(item);
                                if (container != null)
                                {
                                    UIElement element = container as UIElement;
                                    element.MoveFocus(new TraversalRequest(FocusNavigationDirection.First));
                                }
                            }),
                            DispatcherPriority.Render);
                        }
                    }
                };

                collection.CollectionChanged += control._collectionChangedHandler;
            }
        }

        /// <summary>
        /// Called whenever the SelectedItem dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The object whose SelectedItem dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnSelectedItemChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            DialogueLineControl control = s as DialogueLineControl;
            if (control == null)
            {
                return;
            }

            control.LineDataGrid.SelectedItem = e.NewValue;
        }

        private bool FilterItem(object item)
        {
            LineViewModel vm = item as LineViewModel;
            if (vm == null)
            {
                Debug.Assert(false, "Something is bound that isn't a line");
                return false;
            }

            return this.FilterItem(vm);
        }

        public bool FilterItem(LineViewModel lineViewModel)
        {

            if (LineFilters.Contains(LineFilter.ShowRecorded))
            {
                if (lineViewModel.Recorded)
                    return true;
            }

            if (LineFilters.Contains(LineFilter.ShowNotRecorded))
            {
                if (!lineViewModel.Recorded)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Retrieves the command argument structure that is used as a resolver to the bounded
        /// commands for this instance.
        /// </summary>
        /// <param name="commandData">
        /// The command data that has requested the arguments.
        /// </param>
        /// <returns>
        /// The command argument structure used for the command bindings.
        /// </returns>
        internal TextLineCommandArgs CommandResolver(CommandData commandData)
        {
            TextLineCommandArgs args = new TextLineCommandArgs();
            args.DataGrid = this.LineDataGrid;
            args.Conversation = this.DataContext as ConversationViewModel;

            List<LineViewModel> selection = new List<LineViewModel>();
            foreach (object selected in this.LineDataGrid.SelectedItems)
            {
                LineViewModel lineViewModel = selected as LineViewModel;
                if (lineViewModel == null)
                {
                    continue;
                }

                selection.Add(lineViewModel);
            }

            args.Selected = selection;
            return args;
        }

        /// <summary>
        /// Attaches all of the necessary command bindings to this instance.
        /// </summary>
        private void AttachCommandBindings()
        {
            ICommandAction action = new AddNewLineAction(this.CommandResolver);
            action.AddBinding(TextCommands.AddNewLine, this);

            action = new InsertNewLineAction(this.CommandResolver);
            action.AddBinding(TextCommands.InsertNewLine, this);

            action = new CopyLineAction(this.CommandResolver);
            action.AddBinding(RockstarCommands.Copy, this);

            action = new PasteLineAction(this.CommandResolver);
            action.AddBinding(RockstarCommands.Paste, this);

            action = new CutLineAction(this.CommandResolver);
            action.AddBinding(RockstarCommands.Cut, this);

            action = new DeleteLineAction(this.CommandResolver);
            action.AddBinding(RockstarCommands.Delete, this);

            action = new MoveLineUpAction(this.CommandResolver);
            action.AddBinding(TextCommands.MoveLineUp, this);

            action = new MoveLineDownAction(this.CommandResolver);
            action.AddBinding(TextCommands.MoveLineDown, this);

            action = new FilterLinesAction(this.ControlsResolver);
            action.AddBinding(TextCommands.FilterLines, this);

            this.CommandBindings.Add(
                new CommandBinding(RockstarCommands.SelectAll, this.OnSelectAll));
        }

        /// <summary>
        /// Called whenever the <see cref="MultiLineEditCommand"/> is caught at the location
        /// and needs handling.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.ExecutedRoutedEventArgs containing the event data
        /// including the composite object that is being executed.
        /// </param>
        private void ExecuteMultiEdit(object sender, ExecutedRoutedEventArgs e)
        {
            List<LineViewModel> selection = new List<LineViewModel>();
            foreach (object selected in this.LineDataGrid.SelectedItems)
            {
                LineViewModel lineViewModel = selected as LineViewModel;
                if (lineViewModel == null)
                {
                    continue;
                }

                selection.Add(lineViewModel);
            }

            if (selection.Count == 0)
            {
                return;
            }

            CompositePropertyChange compositeChange = e.Parameter as CompositePropertyChange;
            if (compositeChange == null)
            {
                return;
            }

            compositeChange.Initialise(selection);
            MultiLineEditorWindow editWindow = new MultiLineEditorWindow();
            editWindow.Owner = Application.Current.MainWindow;
            editWindow.Title = "Change " + compositeChange.Header;
            editWindow.SetAffectedLines(selection);
            editWindow.SetProperty(compositeChange);
            bool? result = editWindow.ShowDialog();

            if (result == true)
            {
                ConversationViewModel conversation = this.DataContext as ConversationViewModel;
                if (conversation != null && conversation.Model != null)
                {
                    using (new UndoRedoBatch(conversation.Model.UndoEngine))
                    {
                        compositeChange.ApplyChange(selection);
                    }
                }
                else
                {
                    compositeChange.ApplyChange(selection);
                }
            }
        }

        private IEnumerable<DialogueLineControl> ControlsResolver(CommandData commandData)
        {
            return Enumerable.Repeat(this, 1);
        }

        private void OnDragReorderingCompleted(object s, RsDataGridDragEventArgs e)
        {
            if (e.State != DragState.FinishedWithMovement)
            {
                return;
            }

            if (e.TargetRowItem == null)
            {
                //// Dropped outside of the datagrid.
                return;
            }

            Conversation conversation = (this.DataContext as ConversationViewModel).Model;

            ILine source = (e.SourceRowItem as LineViewModel).Model;
            ILine target = (e.TargetRowItem as LineViewModel).Model;
            int newIndex = conversation.Lines.IndexOf(target);
            int oldIndex = conversation.Lines.IndexOf(source);

            conversation.MoveLine(source, newIndex);
        }

        /// <summary>
        /// Called whenever the context menu for the line rows is closed after it has been
        /// opened for multiple lines.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to. The context menu that closed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private void OnRowContextMenuClosed(object sender, RoutedEventArgs e)
        {
            ContextMenu menu = sender as ContextMenu;
            if (menu != null)
            {
                menu.PlacementTarget = null;
                menu.Closed -= this.OnRowContextMenuClosed;
            }

            ContextMenuProperties.SetIsContextMenuOpen(this.LineDataGrid, false);
        }

        /// <summary>
        /// Called whenever the context menu for the line rows is closed after it has been
        /// opened for multiple lines.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to. The row which raised the right button up
        /// mouse event.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.MouseButtonEventArgs containing the event data.
        /// </param>
        private void OnRowMouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            DataGridRow row = sender as DataGridRow;
            if (row == null || row.IsSelected == false)
            {
                return;
            }

            row.ContextMenu.PlacementTarget = row;
            row.ContextMenu.Placement = PlacementMode.MousePoint;

            DataGrid dataGrid = this.LineDataGrid;
            if (dataGrid == null)
            {
                return;
            }

            int selectionCount = dataGrid.SelectedItems.Count;
            if (selectionCount == 0 || selectionCount == 1)
            {
                return;
            }

            e.Handled = true;

            row.ContextMenu.IsOpen = true;
            row.ContextMenu.PlacementTarget = row;
            row.ContextMenu.Closed += this.OnRowContextMenuClosed;
            ContextMenuProperties.SetIsContextMenuOpen(this.LineDataGrid, true);
        }

        /// <summary>
        /// Called whenever the RockstarCommands.SelectAll command is fired and needs handling
        /// at this instance level.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The execute data that has been sent with the command.
        /// </param>
        private void OnSelectAll(object s, ExecutedRoutedEventArgs e)
        {
            this.LineDataGrid.SelectAll();
        }

        /// <summary>
        /// Called whenever the selection on the internal data grid changes so that we can push
        /// the selected items out to any observers of this control.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Controls.SelectionChangedEventArgs containing the event data.
        /// </param>
        private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.SelectedItem = this.LineDataGrid.SelectedItem;
            this._selectedItems = this.LineDataGrid.SelectedItems.OfType<LineViewModel>();
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// A composite change object that represents a change to the audibility property on
        /// n-number of <see cref="LineViewModel"/> objects.
        /// </summary>
        private class AudibilityPropertyChange : ComboPropertyChange
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="AudibilityPropertyChange"/> class.
            /// </summary>
            public AudibilityPropertyChange()
                : base("Audibility", "Audibility.Name")
            {
            }
            #endregion Constructors

            #region Methods
            /// <summary>
            /// Applies the new value for the property to the specified collection of line
            /// view model objects.
            /// </summary>
            /// <param name="lineViewModels">
            /// The line view models that the property value should be applied to.
            /// </param>
            public override void ApplyChange(IEnumerable<LineViewModel> lineViewModels)
            {
                foreach (LineViewModel lineViewModel in lineViewModels)
                {
                    DialogueAudibility newValue = null;
                    foreach (DialogueAudibility audibility in lineViewModel.Audibilities)
                    {
                        if (String.Equals(audibility.Name, this.Value))
                        {
                            newValue = audibility;
                        }
                    }

                    lineViewModel.Audibility = newValue;
                }
            }

            /// <summary>
            /// Retrieves the items that the property can be set to.
            /// </summary>
            /// <param name="lineViewModels">
            /// The line view models that will have the property value applied to.
            /// </param>
            /// <returns>
            /// The items that the property can be set to.
            /// </returns>
            public override IList<string> GetItems(IEnumerable<LineViewModel> lineViewModels)
            {
                IEnumerable<string> audibilities = null;
                foreach (object selected in lineViewModels)
                {
                    LineViewModel lineViewModel = selected as LineViewModel;
                    if (lineViewModel == null)
                    {
                        continue;
                    }

                    IEnumerable<string> lineAudibilities = from a in lineViewModel.Audibilities
                                                           select a.Name;
                    if (audibilities == null)
                    {
                        audibilities = lineAudibilities;
                    }
                    else
                    {
                        audibilities = audibilities.Intersect(lineAudibilities);
                    }
                }

                return audibilities.ToList();
            }
            #endregion Methods
        } // DialogueLineControl.AudibilityPropertyChange {Class}

        /// <summary>
        /// A composite change object that represents a change to the audio type property on
        /// n-number of <see cref="LineViewModel"/> objects.
        /// </summary>
        private class AudioTypePropertyChange : ComboPropertyChange
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="AudioTypePropertyChange"/> class.
            /// </summary>
            public AudioTypePropertyChange()
                : base("Audio Type", "AudioType.Name")
            {
            }
            #endregion Constructors

            #region Methods
            /// <summary>
            /// Applies the new value for the property to the specified collection of line
            /// view model objects.
            /// </summary>
            /// <param name="lineViewModels">
            /// The line view models that the property value should be applied to.
            /// </param>
            public override void ApplyChange(IEnumerable<LineViewModel> lineViewModels)
            {
                foreach (LineViewModel lineViewModel in lineViewModels)
                {
                    DialogueAudioType newValue = null;
                    foreach (DialogueAudioType audioType in lineViewModel.AudioTypes)
                    {
                        if (String.Equals(audioType.Name, this.Value))
                        {
                            newValue = audioType;
                        }
                    }

                    lineViewModel.AudioType = newValue;
                }
            }

            /// <summary>
            /// Retrieves the items that the property can be set to.
            /// </summary>
            /// <param name="lineViewModels">
            /// The line view models that will have the property value applied to.
            /// </param>
            /// <returns>
            /// The items that the property can be set to.
            /// </returns>
            public override IList<string> GetItems(IEnumerable<LineViewModel> lineViewModels)
            {
                IEnumerable<string> audioTypes = null;
                foreach (object selected in lineViewModels)
                {
                    LineViewModel lineViewModel = selected as LineViewModel;
                    if (lineViewModel == null)
                    {
                        continue;
                    }

                    IEnumerable<string> lineAudioTypes = from a in lineViewModel.AudioTypes
                                                         select a.Name;
                    if (audioTypes == null)
                    {
                        audioTypes = lineAudioTypes;
                    }
                    else
                    {
                        audioTypes = audioTypes.Intersect(lineAudioTypes);
                    }
                }

                return audioTypes.ToList();
            }
            #endregion Methods
        } // DialogueLineControl.AudioTypePropertyChange {Class}

        /// <summary>
        /// A composite change object that represents a change to the ducks radio property on
        /// n-number of <see cref="LineViewModel"/> objects.
        /// </summary>
        private class DucksRadioPropertyChange : BooleanPropertyChange
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="DucksRadioPropertyChange"/> class.
            /// </summary>
            public DucksRadioPropertyChange()
                : base("Ducks Radio", "DucksRadio")
            {
            }
            #endregion Constructors

            #region Methods
            /// <summary>
            /// Applies the new value for the property to the specified collection of line
            /// view model objects.
            /// </summary>
            /// <param name="lineViewModels">
            /// The line view models that the property value should be applied to.
            /// </param>
            public override void ApplyChange(IEnumerable<LineViewModel> lineViewModels)
            {
                bool newValue = false;
                if (this.Value == true)
                {
                    newValue = true;
                }

                foreach (LineViewModel lineViewModel in lineViewModels)
                {
                    lineViewModel.DucksRadio = newValue;
                }
            }
            #endregion Methods
        } // DialogueLineControl.DucksRadioPropertyChange {Class}

        /// <summary>
        /// A composite change object that represents a change to the special property on
        /// n-number of <see cref="LineViewModel"/> objects.
        /// </summary>
        private class SpecialPropertyChange : ComboPropertyChange
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="SpecialPropertyChange"/> class.
            /// </summary>
            public SpecialPropertyChange()
                : base("Special", "Special.Name")
            {
            }
            #endregion Constructors

            #region Methods
            /// <summary>
            /// Applies the new value for the property to the specified collection of line
            /// view model objects.
            /// </summary>
            /// <param name="lineViewModels">
            /// The line view models that the property value should be applied to.
            /// </param>
            public override void ApplyChange(IEnumerable<LineViewModel> lineViewModels)
            {
                foreach (LineViewModel lineViewModel in lineViewModels)
                {
                    DialogueSpecial newValue = null;
                    foreach (DialogueSpecial special in lineViewModel.Specials)
                    {
                        if (String.Equals(special.Name, this.Value))
                        {
                            newValue = special;
                        }
                    }

                    lineViewModel.Special = newValue;
                }
            }

            /// <summary>
            /// Retrieves the items that the property can be set to.
            /// </summary>
            /// <param name="lineViewModels">
            /// The line view models that will have the property value applied to.
            /// </param>
            /// <returns>
            /// The items that the property can be set to.
            /// </returns>
            public override IList<string> GetItems(IEnumerable<LineViewModel> lineViewModels)
            {
                IEnumerable<string> specials = null;
                foreach (object selected in lineViewModels)
                {
                    LineViewModel lineViewModel = selected as LineViewModel;
                    if (lineViewModel == null)
                    {
                        continue;
                    }

                    IEnumerable<string> lineAudioTypes = from a in lineViewModel.Specials
                                                         select a.Name;
                    if (specials == null)
                    {
                        specials = lineAudioTypes;
                    }
                    else
                    {
                        specials = specials.Intersect(lineAudioTypes);
                    }
                }

                return specials.ToList();
            }
            #endregion Methods
        } // DialogueLineControl.SpecialPropertyChange {Class}
        #endregion Classes
    } // RSG.Text.View.DialogueLineControl {Class}
} // RSG.Text.View {Namespace}
