﻿//---------------------------------------------------------------------------------------------
// <copyright file="ThemeHelper.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Helpers
{
    using System;
    using System.Collections.Concurrent;
    using System.Runtime.CompilerServices;
    using System.Threading;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using RSG.Base.Drawing;

    /// <summary>
    /// A helper class containing utility methods in it that can be used to transform a bitmap
    /// source object to a themed bitmap source object.
    /// </summary>
    public static class ThemeHelper
    {
        #region Fields
        /// <summary>
        /// The weight used on the red channel when transforming a colour to a grey-scaled
        /// colour.
        /// </summary>
        private const float BlueChannelWeight = 0.0004296875f;

        /// <summary>
        /// The weight used on the green channel when transforming a colour to a grey-scaled
        /// colour.
        /// </summary>
        private const float GreenChannelWeight = 0.0023046874f;

        /// <summary>
        /// The weight used on the blue channel when transforming a colour to a grey-scaled
        /// colour.
        /// </summary>
        private const float RedChannelWeight = 0.001171875f;

        /// <summary>
        /// The private cache used to keep already transformed bitmap source objects around
        /// in-case multiple copies are needed.
        /// </summary>
        private static readonly ConcurrentDictionary<Key,
            ConditionalWeakTable<BitmapSource, BitmapSource>> _cache;

        /// <summary>
        /// The static byte array that is used to store colour data during the conversation.
        /// </summary>
        private static byte[] _conversionBuffer;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="ThemeHelper"/> class.
        /// </summary>
        static ThemeHelper()
        {
            _cache =
            new ConcurrentDictionary<Key, ConditionalWeakTable<BitmapSource, BitmapSource>>();
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Clears the current cache of bitmap source objects.
        /// </summary>
        internal static void ClearCache()
        {
            _cache.Clear();
        }

        /// <summary>
        /// Retrieves a themed bitmap source object by getting one from the cache or creating a
        /// new instance.
        /// </summary>
        /// <param name="args">
        /// A structure containing the arguments that should be used when creating the new
        /// themed bitmap source object.
        /// </param>
        /// <returns>
        /// A themed bitmap source object that represents the specified arguments.
        /// </returns>
        internal static BitmapSource GetOrCreateThemedBitmapSource(ThemeBitmapArguments args)
        {
            Color transformColour = Colors.Transparent;
            if (args.Enabled == false)
            {
                transformColour = args.Bias;
            }

            Key cacheKey = new Key(args.Background, args.Enabled, transformColour);
            ConditionalWeakTable<BitmapSource, BitmapSource> cached =
                _cache.GetOrAdd(
                cacheKey, new ConditionalWeakTable<BitmapSource, BitmapSource>());

            BitmapSource output = null;
            if (!cached.TryGetValue(args.Input, out output))
            {
                output = CreateThemedBitmapSource(args);
                cached.Add(args.Input, output);
            }

            return output;
        }

        /// <summary>
        /// Creates a new themed bitmap source object.
        /// </summary>
        /// <param name="args">
        /// A structure containing the arguments that should be used when creating the new
        /// themed bitmap source object.
        /// </param>
        /// <returns>
        /// A new themed bitmap source object that represents the specified arguments.
        /// </returns>
        private static BitmapSource CreateThemedBitmapSource(ThemeBitmapArguments args)
        {
            if (args.Input == null)
            {
                return null;
            }

            BitmapSource result;
            BitmapSource source = args.Input;
            if (args.Input.Format != PixelFormats.Bgra32)
            {
                source = new FormatConvertedBitmap(args.Input, PixelFormats.Bgra32, null, 0.0);
            }

            int stride = source.PixelWidth * 4;
            int totalBits = source.PixelWidth * source.PixelHeight * 4;
            byte[] array = Interlocked.Exchange<byte[]>(ref _conversionBuffer, null);
            if (array == null || array.Length < totalBits)
            {
                array = new byte[totalBits];
            }

            try
            {
                source.CopyPixels(array, stride, 0);
                ThemeTransform(array, totalBits, stride, args.Background);
                if (!args.Enabled)
                {
                    GrayscaleTransform(array, totalBits, args.Bias);
                }

                BitmapSource imageSource = BitmapSource.Create(
                    source.PixelWidth,
                    source.PixelHeight,
                    source.DpiX,
                    source.DpiY,
                    PixelFormats.Bgra32,
                    source.Palette,
                    array,
                    stride);

                imageSource.Freeze();
                result = imageSource;
            }
            finally
            {
                Interlocked.Exchange<byte[]>(ref _conversionBuffer, array);
            }

            return result;
        }

        /// <summary>
        /// Transforms the pixel data in the specified array to be grey-scaled.
        /// </summary>
        /// <param name="pixels">
        /// The array that contains the pixel data to transform.
        /// </param>
        /// <param name="pixelLength">
        /// The number of pixels in the specified array.
        /// </param>
        /// <param name="bias">
        /// The bias colour to use when transforming to a grey-scaled bitmap source object.
        /// </param>
        private static void GrayscaleTransform(byte[] pixels, int pixelLength, Color bias)
        {
            float biasOpacity = (float)bias.A / 256.0f;
            int bitIndex = 0;
            while (bitIndex + 4 <= pixelLength)
            {
                float blueChannel = (float)pixels[bitIndex] * BlueChannelWeight;
                float greenChannel = (float)pixels[bitIndex + 1] * GreenChannelWeight;
                float redChannel = (float)pixels[bitIndex + 2] * RedChannelWeight;
                float weight = blueChannel + greenChannel + redChannel;

                pixels[bitIndex] = (byte)(weight * (float)bias.B);
                pixels[bitIndex + 1] = (byte)(weight * (float)bias.G);
                pixels[bitIndex + 2] = (byte)(weight * (float)bias.R);
                pixels[bitIndex + 3] = (byte)(biasOpacity * (float)pixels[bitIndex + 3]);
                bitIndex += 4;
            }
        }

        /// <summary>
        /// Transforms the pixel data in the specified array to be themed based on the
        /// specified background colour.
        /// </summary>
        /// <param name="pixels">
        /// The array that contains the pixel data to transform.
        /// </param>
        /// <param name="pixelLength">
        /// The number of pixels in the specified array.
        /// </param>
        /// <param name="stride">
        /// Number of pixels per row.
        /// </param>
        /// <param name="bkgd">
        /// The bias colour to use when transforming to a themed bitmap source object.
        /// </param>
        private static void ThemeTransform(
            byte[] pixels, int pixelLength, int stride, Color bkgd)
        {
            // Check whether we should ignore the colour inversion for this icon.
            // A single cyan pixel in the top right corner of the image indicates this.
            if (pixels[stride - 4] == 255 &&
                pixels[stride - 3] == 255 &&
                pixels[stride - 2] == 0 &&
                pixels[stride - 1] == 255)
            {
                pixels[stride - 1] = 0;
                return;
            }

            // Determine how far from white the background colour is.
            RGBColor bkgdRgb = new RGBColor(bkgd.R, bkgd.G, bkgd.B);
            HSLColor bkgdHsl = (HSLColor)bkgdRgb;
            double whiteOffset = 1.0 - bkgdHsl.Lightness;

            for (int bitIndex = 0; bitIndex < pixelLength; bitIndex += 4)
            {
                // Ignore pixels with an alpha value of 0.
                if (pixels[bitIndex + 3] == 0)
                {
                    continue;
                }

                RGBColor rgb =
                    new RGBColor(pixels[bitIndex + 2], pixels[bitIndex + 1], pixels[bitIndex]);
                HSLColor hsl = (HSLColor)rgb;

                // Check whether the luminosity is within 5% that of the original background
                // (i.e. white).
                if (hsl.Lightness > 0.95)
                {
                    hsl = bkgdHsl;
                }
                else
                {
                    // Change the lightness based on how "different" the offset is.
                    if (whiteOffset <= 0.5)
                    {
                        hsl.Lightness -= hsl.Lightness * whiteOffset;
                    }
                    else
                    {
                        hsl.Lightness = 1.0 - (hsl.Lightness * whiteOffset);
                    }
                }

                // Convert back to RGB and set the pixel's new values.
                RGBColor newRgb = (RGBColor)hsl;
                pixels[bitIndex] = newRgb.Blue;
                pixels[bitIndex + 1] = newRgb.Green;
                pixels[bitIndex + 2] = newRgb.Red;
            }
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// The class that is used to index the bitmap source cache.
        /// </summary>
        private class Key : Tuple<Color, bool, Color>
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="Key"/> class.
            /// </summary>
            /// <param name="background">
            /// The background colour.
            /// </param>
            /// <param name="enabled">
            /// A value indicating whether the themed bitmap source image should be grey-scaled
            /// or not.
            /// </param>
            /// <param name="bias">
            /// The bias colour used to grey-scale the bitmap source image if disabled.
            /// </param>
            public Key(Color background, bool enabled, Color bias)
                : base(background, enabled, bias)
            {
            }
            #endregion Constructors
        } // RSG.Editor.Controls.ThemeHelper.Key {Class}
        #endregion Classes
    } // RSG.Editor.Controls.Helpers.ThemeHelper {Class}
} // RSG.Editor.Controls.Helpers {Namespace}
