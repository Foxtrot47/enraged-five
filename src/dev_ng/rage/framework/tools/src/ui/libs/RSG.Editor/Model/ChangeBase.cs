﻿//---------------------------------------------------------------------------------------------
// <copyright file="ChangeBase.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Model
{
    using System;

    /// <summary>
    /// Provides a abstract base class for objects wanted to implement the
    /// <see cref="RSG.Editor.Model.IChange"/> interface.
    /// </summary>
    internal abstract class ChangeBase : IChange
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ChangeBase"/> class.
        /// </summary>
        public ChangeBase()
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Redoes this single change.
        /// </summary>
        public abstract void Redo();

        /// <summary>
        /// Undoes this single change.
        /// </summary>
        public abstract void Undo();
        #endregion Methods
    } // RSG.Editor.Model.ChangeBase {Class}
} // RSG.Editor.Model {Namespace}
