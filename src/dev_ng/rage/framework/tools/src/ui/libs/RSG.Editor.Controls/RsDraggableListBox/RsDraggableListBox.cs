﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsDraggableListBox.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Contains a list of selectable items that supports multiple select and drag.
    /// </summary>
    public class RsDraggableListBox : ListBox
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="RsDraggableListBox" /> class.
        /// </summary>
        public RsDraggableListBox()
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Sets the item that is initially selected when the selection mode is set to Extended
        /// to the specified item.
        /// </summary>
        /// <param name="item">
        /// The item that should be initially selected when SelectionMode is Extended.
        /// </param>
        internal void SetAnchorItem(object item)
        {
            this.AnchorItem = item;
        }

        /// <summary>
        /// Creates or identifies the element used to display a specified item.
        /// </summary>
        /// <returns>
        /// A new instance of the <see cref="RsDraggableListBoxItem"/> class.
        /// </returns>
        protected override DependencyObject GetContainerForItemOverride()
        {
            return new RsDraggableListBoxItem();
        }

        /// <summary>
        /// Determines if the specified item is (or is eligible to be) its own ItemContainer.
        /// </summary>
        /// <param name="item">
        /// Specified item.
        /// </param>
        /// <returns>
        /// True if the item is its own ItemContainer; otherwise, false.
        /// </returns>
        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is RsDraggableListBoxItem;
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsDraggableListBox {Class}
} // RSG.Editor.Controls {Namespace}
