﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssetBuildHistoryViewModel.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.AssetBuildMonitor.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Linq;
    using RSG.Base.Logging;
    using RSG.Base.Logging.Universal;
    using RSG.Editor;
    using RSG.Pipeline.Services.Engine;
    using RSG.Pipeline.Services.Engine.Consumers;
    using RSG.Services.Configuration;
    using RSG.Services.Configuration.AssetBuildPipeline;

    /// <summary>
    /// Asset Build History View-Model for a particular host.
    /// </summary>
    public sealed class AssetBuildHistoryViewModel : 
        AssetBuildMonitorViewModelBase
    {
        #region Properties
        /// <summary>
        /// Gets the host associated with this Build History.
        /// </summary>
        public String Hostname
        {
            get { return _hostname; }
            private set { this.SetProperty(ref _hostname, value); }
        }
        
        /// <summary>
        /// Gets the current connection status.
        /// </summary>
        public bool IsConnected
        {
            get { return _isConnected; }
            private set { this.SetProperty(ref _isConnected, value); }
        }

        /// <summary>
        /// Gets the latest refresh DateTime object (in UTC).
        /// </summary>
        public DateTime RefreshDateTime
        {
            get { return _refreshDateTime; }
            private set { this.SetProperty(ref _refreshDateTime, value); }
        }

        /// <summary>
        /// Gets the currently available build items.
        /// </summary>
        public ObservableCollection<AssetBuildItemViewModel> BuildItems
        {
            get { return _buildItems; }
            private set { this.SetProperty(ref _buildItems, value); }
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private Uri _monitoringServiceUri;

        /// <summary>
        /// 
        /// </summary>
        private Uri _monitoringServiceCallbackUri;

        /// <summary>
        /// Latest refresh DateTime object (in UTC).
        /// </summary>
        private DateTime _refreshDateTime;

        /// <summary>
        /// 
        /// </summary>
        private bool _isConnected;

        /// <summary>
        /// 
        /// </summary>
        private AssetBuildHistoryServiceConsumer _buildHistoryConsumer;

        /// <summary>
        /// Host.
        /// </summary>
        private String _hostname;

        /// <summary>
        /// Build items collection.
        /// </summary>
        private ObservableCollection<AssetBuildItemViewModel> _buildItems;
        
        /// <summary>
        /// Bugstar user objects.
        /// </summary>
        private IEnumerable<RSG.Interop.Bugstar.Organisation.User> _bugstarUsers;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="hostname"></param>
        /// <param name="bugstarUsers"></param>
        public AssetBuildHistoryViewModel(IUniversalLog log, String hostname, 
            IEnumerable<RSG.Interop.Bugstar.Organisation.User> bugstarUsers)
        {
            this.Hostname = hostname;
            this._monitoringServiceUri = new Uri(
                String.Format("net.tcp://{0}:12000/AssetBuildHistoryService.svc",
                this.Hostname));
            this._monitoringServiceCallbackUri = new Uri(
                "net.tcp://localhost:12001/EngineProgressMonitoringServiceCallback.svc");

            this._buildHistoryConsumer = new AssetBuildHistoryServiceConsumer(LogFactory.ApplicationLog,
                _monitoringServiceUri);
            this._buildItems = new ObservableCollection<AssetBuildItemViewModel>();
            this._bugstarUsers = bugstarUsers;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Refresh Build History from our consumer service.
        /// </summary>
        public void Refresh()
        {
            try
            {
                if (this.IsConnected)
                {
                    IEnumerable<AssetBuildItem> buildHistory = this._buildHistoryConsumer.GetBuildHistory();
                    this.BuildItems = new ObservableCollection<AssetBuildItemViewModel>(
                        buildHistory.Select(m => new AssetBuildItemViewModel(m, _bugstarUsers)));
                }
                else
                {
                    this.BuildItems.Clear();
                }
                this.RefreshDateTime = DateTime.UtcNow;
            }
            catch (EngineProgressConsumerException)
            {
                this.IsConnected = false;
                this.AddError("BuildItems", "Disconnected.");
                this.BuildItems.Clear();
            }
        }

        /// <summary>
        /// Clear build history.
        /// </summary>
        public bool ClearBuildHistory()
        {
            try
            {
                if (this.IsConnected)
                {
                    this._buildHistoryConsumer.ClearBuildHistory();
                    this.Refresh();
                    return (true);
                }
                else
                {
                    return (false);
                }
            }
            catch (EngineProgressConsumerException)
            {
                this.IsConnected = false;
                this.AddError("BuildItems", "Disconnected.");
                this.BuildItems.Clear();
                return (false);
            }
        }
        #endregion // Controller Methods

        #region AssetBuildMonitorViewModelBase Abstract Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ViewModelValidation()
        {

        }
        #endregion // AssetBuildMonitorViewModelBase Abstract Methods
    }

} // RSG.AssetBuildMonitor.ViewModel namespace
