﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsRenameTreeViewItem.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Threading;
    using RSG.Editor.Controls.Helpers;
    using RSG.Editor.Controls.Presenters;
    using RSG.Editor.SharedCommands;
    using RSG.Editor.View;
    using RSG.Interop.Microsoft.Windows;

    /// <summary>
    /// Gets a selectable item inside a <see cref="RsRenameTreeView"/> control.
    /// </summary>
    public class RsRenameTreeViewItem : RsVirtualisedTreeViewItem
    {
        #region Fields
        /// <summary>
        /// A value indicating whether the rename logic should being when the mouse button up
        /// event is fired.
        /// </summary>
        private bool _beginRenameOnMouseUp;

        /// <summary>
        /// A private reference to a rename controller that is pending for when the presenter
        /// is initialised.
        /// </summary>
        private IRenameController _pendingRenameController;

        /// <summary>
        /// A timer that is used to set a delay between the user clicking the item and starting
        /// the rename logic.
        /// </summary>
        private DispatcherTimer _renameTimer;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="RsRenameTreeViewItem"/> class.
        /// </summary>
        public RsRenameTreeViewItem()
        {
            RockstarCommandManager.AddBinding(
                this,
                RockstarCommands.Rename,
                this.ExecuteRenameCmd,
                this.CanExecuteRenameCmd);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether this item is selected and is the only selected item
        /// currently in the parent tree view.
        /// </summary>
        public bool IsSingleSelected
        {
            get
            {
                RsVirtualisedTreeView treeView = this.TreeView;
                if (treeView == null)
                {
                    return false;
                }

                if (treeView.SelectedItems.Count != 1)
                {
                    return false;
                }

                return Object.ReferenceEquals(treeView.SelectedItem, this.TreeNode);
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Gets called every time the control template gets attached to this control.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            if (this._pendingRenameController != null)
            {
                this.EnterEditMode(this._pendingRenameController);
                this._pendingRenameController = null;
            }
        }

        /// <summary>
        /// Puts the tree item presenter being used by this item into its edit mode. If the
        /// presenter isn't loaded yet it will pend the edit mode until it is loaded.
        /// </summary>
        /// <param name="renameController">
        /// The rename controller that specifies the rename behaviour to use.
        /// </param>
        internal void EnterEditMode(IRenameController renameController)
        {
            TreeItemPresenter presenter = this.Presenter;
            if (presenter == null)
            {
                this._pendingRenameController = renameController;
                return;
            }

            presenter.EnterEditMode(renameController);
            this._pendingRenameController = null;
        }

        /// <summary>
        /// Removes the tree item presenter being used by this item from its edit mode.
        /// </summary>
        internal void ExitEditMode()
        {
            TreeItemPresenter presenter = this.Presenter;
            if (presenter != null)
            {
                presenter.ExitEditMode();
            }
        }

        /// <summary>
        /// Responds to the System.Windows.UIElement.MouseLeftButtonDown event.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.MouseButtonEventArgs data used for the event.
        /// </param>
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            this._beginRenameOnMouseUp = false;
            if (e.ClickCount == 1)
            {
                this.UpdateBeginRenameOnMouseUp(e);
            }

            if (e.ClickCount > 1)
            {
                this.StopRenameTimer();
            }

            base.OnMouseLeftButtonDown(e);
        }

        /// <summary>
        /// Responds to the System.Windows.UIElement.MouseLeftButtonUp event.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.MouseButtonEventArgs data used for the event.
        /// </param>
        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            if (Mouse.Captured == this && this._beginRenameOnMouseUp)
            {
                this.StartRenameTimer();
            }

            base.OnMouseLeftButtonUp(e);
        }

        /// <summary>
        /// On receiving keyboard focus we need to stop the rename timer as it would of started
        /// if we were previously editing this item.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.KeyboardFocusChangedEventArgs containing the event data.
        /// </param>
        protected override void OnGotKeyboardFocus(KeyboardFocusChangedEventArgs e)
        {
            base.OnGotKeyboardFocus(e);
            if (e.NewFocus == this)
            {
                this.Dispatcher.BeginInvoke(
                    new Action(
                    delegate
                    {
                        this.StopRenameTimer();
                    }),
                    DispatcherPriority.Input);
            }
        }

        /// <summary>
        /// Begins the renaming logic for this item.
        /// </summary>
        private void BeginRename()
        {
            this.StopRenameTimer();
            RsRenameTreeView treeView = this.TreeView as RsRenameTreeView;
            IRenameable renameable = this.TreeNode.Item as IRenameable;
            if (renameable != null && this.CanBeginRename(treeView, renameable))
            {
                IRenameController controller = renameable.BeginRename(this.DataContext);
                treeView.BeginRename(controller);
            }
        }

        /// <summary>
        /// Determines whether this item can start to be renamed.
        /// </summary>
        /// <param name="treeView">
        /// The parent tree view control.
        /// </param>
        /// <param name="renameable">
        /// The rename interface attached to this item.
        /// </param>
        /// <returns>
        /// True if this item can start to be renamed; otherwise, false.
        /// </returns>
        private bool CanBeginRename(RsRenameTreeView treeView, IRenameable renameable)
        {
            if (treeView == null || renameable == null)
            {
                return false;
            }

            if (!renameable.CanBeRenamed)
            {
                return false;
            }

            if (treeView.IsInRenameMode)
            {
                return false;
            }

            if (!this.IsKeyboardFocusWithin || !this.IsSingleSelected)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Determines whether the Rename command can be executed on this control.
        /// </summary>
        /// <param name="data">
        /// The can execute data from the command pipeline.
        /// </param>
        /// <returns>
        /// True if the Rename command can be executed; otherwise, false.
        /// </returns>
        private bool CanExecuteRenameCmd(CanExecuteCommandData data)
        {
            RsRenameTreeView treeView = this.TreeView as RsRenameTreeView;
            IRenameable renameable = this.TreeNode.Item as IRenameable;
            if (treeView == null || renameable == null)
            {
                return false;
            }

            if (!renameable.CanBeRenamed)
            {
                return false;
            }

            if (treeView.IsInRenameMode)
            {
                return false;
            }

            if (!this.IsSingleSelected)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Commits the current rename operation for validation.
        /// </summary>
        private void CommitCurrentRename()
        {
            RsRenameTreeView treeView = this.TreeView as RsRenameTreeView;
            if (treeView != null)
            {
                treeView.CommitRename(false);
            }
        }

        /// <summary>
        /// Executes the Rename command on this control. Just calls <see cref="BeginRename"/>
        /// and lets the rename system take over.
        /// </summary>
        /// <param name="data">
        /// The execute data from the command pipeline.
        /// </param>
        private void ExecuteRenameCmd(ExecuteCommandData data)
        {
            this.BeginRename();
        }

        /// <summary>
        /// Called whenever the rename timer reaches its interval time and fires its tick
        /// event. This sets the rename pipeline going.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to. (The rename timer object).
        /// </param>
        /// <param name="e">
        /// Always EventArgs.Empty.
        /// </param>
        private void OnRenameTimerTick(object s, EventArgs e)
        {
            this._renameTimer.Tick -= this.OnRenameTimerTick;
            this._renameTimer.Stop();
            this.BeginRename();
        }

        /// <summary>
        /// Starts the rename timer to initiate the rename logic.
        /// </summary>
        private void StartRenameTimer()
        {
            if (this._renameTimer == null)
            {
                uint time = User32.GetDoubleClickTime();
                TimeSpan interval = TimeSpan.FromMilliseconds(time);
                DispatcherPriority priority = DispatcherPriority.Input;
                this._renameTimer = new DispatcherTimer(priority, this.Dispatcher)
                {
                    Interval = interval,
                };

                this._renameTimer.Tick += this.OnRenameTimerTick;
                this._renameTimer.Start();
            }
        }

        /// <summary>
        /// Stops the timer that is being used to start the renaming logic for this item.
        /// </summary>
        private void StopRenameTimer()
        {
            if (this._renameTimer != null)
            {
                this._renameTimer.Tick -= this.OnRenameTimerTick;
                this._renameTimer.Stop();
                this._renameTimer = null;
            }
        }

        /// <summary>
        /// Updates the _beginRenameOnMouseUp field.
        /// </summary>
        /// <param name="e">
        /// The mouse event arguments received on the mouse down event.
        /// </param>
        private void UpdateBeginRenameOnMouseUp(MouseButtonEventArgs e)
        {
            RsRenameTreeView treeView = this.TreeView as RsRenameTreeView;
            IRenameable renameable = this.TreeNode.Item as IRenameable;
            if (this.CanBeginRename(treeView, renameable) && e.ClickCount == 1)
            {
                DependencyObject d = e.OriginalSource as DependencyObject;

                TextBox textBox = d.GetVisualOrLogicalAncestor<TextBox>();
                if (textBox == null)
                {
                    this._beginRenameOnMouseUp = true;
                }
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsRenameTreeViewItem {Class}
} // RSG.Editor.Controls {Namespace}
