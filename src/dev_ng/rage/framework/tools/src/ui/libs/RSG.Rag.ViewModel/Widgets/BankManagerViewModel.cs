﻿using RSG.Rag.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Rag.Core;
using System.Windows.Media.Imaging;

namespace RSG.Rag.ViewModel.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    public class BankManagerViewModel : GroupBaseViewModel<BankManager>
    {
        #region Fields
        /// <summary>
        /// 
        /// </summary>
        private IWidgetGroupViewModel _selectedGroupViewModel;

        /// <summary>
        /// Used for forcing the root level expander to be expanded in the widget view.
        /// </summary>
        private bool _rootGroupExpanded = true;
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        /// <param name="vmFactory"></param>
        public BankManagerViewModel(BankManager widget, WidgetViewModelFactory vmFactory)
            : base(widget, vmFactory)
        {
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Gets the bitmap source that is used to display the icon for this item.
        /// </summary>
        public override BitmapSource Icon
        {
            get { return CommonRagIcons.Bank; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String TimingInformation
        {
            get { return Widget.TimingInformation; }
        }

        /// <summary>
        /// TODO: Once the virtualised treeview exposes the RealSelectedItems remove this
        /// and bind directly to SelectedGroup in the view.  Also remove the reference to
        /// RSG.Editor.Controls from this project.
        /// </summary>
        public RSG.Editor.Controls.Helpers.VirtualisedTreeNode SelectedGroupNode
        {
            set
            {
                if (value == null)
                {
                    SelectedGroup = null;
                }
                else
                {
                    SelectedGroup = value.Item as IWidgetGroupViewModel;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public IWidgetGroupViewModel SelectedGroup
        {
            set
            {
                IWidgetGroupViewModel previousSelection = _selectedGroupViewModel;
                if (SetProperty(ref _selectedGroupViewModel, value))
                {
                    OnSelectedGroupChanged(previousSelection, _selectedGroupViewModel);
                    RootGroupExpanded = true;
                }
            }
        }

        /// <summary>
        /// Used for forcing the root level expander to be expanded in the widget view.
        /// </summary>
        public bool RootGroupExpanded
        {
            get { return _rootGroupExpanded; }
            set { SetProperty(ref _rootGroupExpanded, value); }
        }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="previousSelection"></param>
        /// <param name="newSelection"></param>
        private void OnSelectedGroupChanged(IWidgetGroupViewModel previousSelection, IWidgetGroupViewModel newSelection)
        {
            if (previousSelection != null)
            {
                previousSelection.OnHide();
            }

            if (_selectedGroupViewModel != null)
            {
                _selectedGroupViewModel.OnShow(true);
            }
        }
        #endregion // Methods
    } // BankManagerViewModel
}
