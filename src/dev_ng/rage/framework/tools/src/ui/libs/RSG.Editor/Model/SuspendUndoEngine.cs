﻿//---------------------------------------------------------------------------------------------
// <copyright file="SuspendUndoEngine.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Model
{
    /// <summary>
    /// Provides functionality so that a undo engine can be suspended from creating any undo
    /// events. The engine will still perform any Undo or Redo operations and those operations
    /// will create the necessary alteration event still.
    /// </summary>
    public class SuspendUndoEngine : DisposableObject
    {
        #region Fields
        /// <summary>
        /// Gets the <see cref="RSG.Editor.Model.UndoEngine"/> instance that this batch was
        /// created for.
        /// </summary>
        private UndoEngine _engine;

        /// <summary>
        /// The private value indicating the suspended mode this object represents.
        /// </summary>
        private SuspendUndoEngineMode _mode;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SuspendUndoEngine"/> class.
        /// </summary>
        /// <param name="engine">
        /// A instance of the <see cref="RSG.Editor.Model.UndoEngine"/> that this suspend
        /// region will be applied on.
        /// </param>
        public SuspendUndoEngine(UndoEngine engine)
            : this(engine, SuspendUndoEngineMode.Both)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="SuspendUndoEngine"/> class.
        /// </summary>
        /// <param name="engine">
        /// A instance of the <see cref="RSG.Editor.Model.UndoEngine"/> that this suspend
        /// region will be applied on.
        /// </param>
        /// <param name="mode">
        /// The mode in which the undo engine should be suspended.
        /// </param>
        public SuspendUndoEngine(UndoEngine engine, SuspendUndoEngineMode mode)
        {
            this._engine = engine;
            this._mode = mode;
            if (this._engine != null)
            {
                this._engine.StartSuspendedState(mode);
            }
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Finishes the suspended state that this object represents on the associated engine.
        /// </summary>
        protected override void DisposeManagedResources()
        {
            if (this._engine == null)
            {
                return;
            }

            this._engine.FinishSuspendedState(this._mode);
        }
        #endregion Methods
    } // RSG.Editor.Model.SuspendUndoEngine {Class}
} // RSG.Editor.Model {Namespace}
