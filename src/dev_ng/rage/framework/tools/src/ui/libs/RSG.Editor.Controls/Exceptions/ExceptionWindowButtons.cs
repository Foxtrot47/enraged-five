﻿//---------------------------------------------------------------------------------------------
// <copyright file="ExceptionWindowButtons.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Exceptions
{
    using System;

    /// <summary>
    /// Enumeration of the buttons that can appear on the exception window.
    /// </summary>
    [Flags]
    public enum ExceptionWindowButtons
    {
        /// <summary>
        /// Continue button.
        /// </summary>
        Continue = 0x1,

        /// <summary>
        /// Quit button.
        /// </summary>
        Quit = 0x2,

        /// <summary>
        /// Send email button.
        /// </summary>
        SendEmail = 0x4,

        /// <summary>
        /// Log bug button.
        /// </summary>
        LogBug = 0x8,

        /// <summary>
        /// Default set of buttons to show.
        /// </summary>
#if DEBUG
        Default = Continue | Quit | SendEmail | LogBug,
#else
        Default = Quit | SendEmail | LogBug,
#endif

        /// <summary>
        /// All buttons enabled.
        /// </summary>
        All = 0xF
    } // RSG.Editor.Controls.Exceptions.ExceptionWindowButtons {Enum}
} // RSG.Editor.Controls.Exceptions {Namespace}
