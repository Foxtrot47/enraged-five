﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Editor.Controls.MapViewport.OverlayComponents;
using RSG.Editor.Model;

namespace RSG.Editor.Controls.MapViewport.Annotations
{
    /// <summary>
    /// 
    /// </summary>
    public class Annotation : Image
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Comments"/> property.
        /// </summary>
        private readonly ModelCollection<AnnotationComment> _comments;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Annotation"/> class using the
        /// specified location.
        /// </summary>
        /// <param name="location"></param>
        /// <param name="undoEngineProvider"></param>
        public Annotation(Point location, IUndoEngineProvider undoEngineProvider)
            : base(new Rect(location.X, location.Y, MapViewportIcons.MapAnnotation.Width, MapViewportIcons.MapAnnotation.Height), MapViewportIcons.MapAnnotation)
        {
            _comments = new ModelCollection<AnnotationComment>(undoEngineProvider);
            UndoEngineProvider = undoEngineProvider;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Annotation"/> class using the
        /// specified <see cref="XElement"/>.
        /// </summary>
        /// <param name="elem"></param>
        /// <param name="undoEngineProvider"></param>
        public Annotation(XElement elem, IUndoEngineProvider undoEngineProvider)
        {
            UndoEngineProvider = undoEngineProvider;

            // Extract the position.
            XElement positionElem = elem.Element("Position");
            XAttribute positionXAtt = positionElem.Attribute("x");
            XAttribute positionYAtt = positionElem.Attribute("y");
            double posX = Double.Parse(positionXAtt.Value);
            double posY = Double.Parse(positionYAtt.Value);
            WorldExtents = new Rect(posX, posY, MapViewportIcons.MapAnnotation.Width, MapViewportIcons.MapAnnotation.Height);

            // Extract the comments.
            _comments = new ModelCollection<AnnotationComment>(undoEngineProvider);
            foreach (XElement commentElem in elem.XPathSelectElements("Comments/Comment"))
            {
                _comments.Add(new AnnotationComment(commentElem, undoEngineProvider));
            }
        }
        #endregion

        #region Properties
        /// <summary>
        /// Where the position relates to in terms of the component's bounds.
        /// </summary>
        public override PositionOrigin PositionOrigin
        {
            get { return PositionOrigin.BottomCenter; }
            set { throw new NotSupportedException(); }
        }

        /// <summary>
        /// List of comments associated with this annotation.
        /// </summary>
        public IReadOnlyCollection<AnnotationComment> Comments
        {
            get { return _comments; }
        }

        /// <summary>
        /// Retrieves the number of comments this annotation has.
        /// </summary>
        public int CommentCount
        {
            get { return _comments.Count; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Adds a comment to this annotation.
        /// </summary>
        /// <param name="message"></param>
        public void AddComment(string message)
        {
            AnnotationComment comment = new AnnotationComment(message, UndoEngineProvider);
            _comments.Add(comment);
            NotifyPropertyChanged("CommentCount");
        }

        /// <summary>
        /// Removes a comment from this annotation.
        /// </summary>
        /// <param name="comment"></param>
        public void RemoveComment(AnnotationComment comment)
        {
            _comments.Remove(comment);
            NotifyPropertyChanged("CommentCount");
        }

        /// <summary>
        /// Converts the annotation to an XElement.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public XElement ToXElement(string name)
        {
            return new XElement(name,
                new XElement("Position",
                    new XAttribute("x", Position.X),
                    new XAttribute("y", Position.Y)),
                new XElement("Comments",
                    Comments.Select(item => item.ToXElement("Comment")))
                );
        }
        #endregion
    }
}
