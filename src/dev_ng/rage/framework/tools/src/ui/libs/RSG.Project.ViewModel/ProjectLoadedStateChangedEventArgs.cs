﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProjectLoadedStateChangedEventArgs.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel
{
    using System;

    /// <summary>
    /// Provides data for the event representing a project node either being loaded or
    /// unloaded.
    /// </summary>
    public class ProjectLoadedStateChangedEventArgs : EventArgs
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Project"/> property.
        /// </summary>
        private ProjectNode _project;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ProjectLoadedStateChangedEventArgs"/>
        /// class.
        /// </summary>
        /// <param name="project">
        /// The project whose load state has changed.
        /// </param>
        public ProjectLoadedStateChangedEventArgs(ProjectNode project)
        {
            this._project = project;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the value before the changed occurred.
        /// </summary>
        public ProjectNode Project
        {
            get { return this._project; }
        }
        #endregion Properties
    } // RSG.Project.ViewModel.ProjectLoadedStateChangedEventArgs {Class}
} // RSG.Project.ViewModel {Namespace}
