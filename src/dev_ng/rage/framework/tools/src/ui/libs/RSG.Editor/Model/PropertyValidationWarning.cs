﻿//---------------------------------------------------------------------------------------------
// <copyright file="PropertyValidationWarning.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Model
{
    /// <summary>
    /// Represents a warning that has occurred during validation.
    /// </summary>
    public class PropertyValidationWarning : ValidationWarning, IPropertyValidation
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="PropertyName"/> property.
        /// </summary>
        private string _propertyName;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="PropertyValidationWarning"/> class.
        /// </summary>
        /// <param name="propertyName">
        /// The name of the property that this warning is associated with.
        /// </param>
        /// <param name="message">
        /// The message that describes the warning.
        /// </param>
        public PropertyValidationWarning(string propertyName, string message)
            : base(message)
        {
            this._propertyName = propertyName;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="PropertyValidationWarning"/> class.
        /// </summary>
        /// <param name="propertyName">
        /// The name of the property that this warning is associated with.
        /// </param>
        /// <param name="message">
        /// The message that describes the warning.
        /// </param>
        /// <param name="location">
        /// The file location for the warning if applicable.
        /// </param>
        public PropertyValidationWarning(
            string propertyName, string message, FileLocation location)
            : base(message, location)
        {
            this._propertyName = propertyName;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the name of the property this validation item is associated with.
        /// </summary>
        public string PropertyName
        {
            get { return this._propertyName; }
        }
        #endregion Properties
    } // RSG.Editor.Model.PropertyValidationWarning {Class}
} // RSG.Editor.Model {Namespace}
