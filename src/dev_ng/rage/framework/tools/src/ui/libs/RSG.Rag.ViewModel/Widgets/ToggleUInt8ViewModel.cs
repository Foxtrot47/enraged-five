﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Rag.Widgets;

namespace RSG.Rag.ViewModel.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    public class ToggleUInt8ViewModel : ToggleViewModel<Byte>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        public ToggleUInt8ViewModel(WidgetToggle<Byte> widget)
            : base(widget)
        {
        }
        #endregion // Constructor(s)
    } // ToggleUInt8ViewModel
}
