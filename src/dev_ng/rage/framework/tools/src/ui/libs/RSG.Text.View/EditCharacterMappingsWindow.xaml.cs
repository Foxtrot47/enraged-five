﻿
namespace RSG.Text.View
{
    using RSG.Editor.Controls;
using RSG.Text.Model;
    using RSG.Text.ViewModel;

    /// <summary>
    /// Interaction logic for EditCharacterMappingsWindow.xaml
    /// </summary>
    public partial class EditCharacterMappingsWindow : RsWindow
    {
        public EditCharacterMappingsWindow()
        {
            InitializeComponent();
        }

        public EditCharacterMappingsWindow(CharacterMappingsViewModel mappings)
        {
            InitializeComponent();
            this.Mappings = mappings;
            this.DataContext = this.Mappings;
        }

        public CharacterMappingsViewModel Mappings
        {
            get;
            private set;
        }
    }
}
