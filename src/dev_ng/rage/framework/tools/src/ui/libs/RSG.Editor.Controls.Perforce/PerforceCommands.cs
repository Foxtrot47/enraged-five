﻿//---------------------------------------------------------------------------------------------
// <copyright file="PerforceCommands.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Perforce
{
    using System;
    using System.Windows.Input;
    using System.Windows.Media.Imaging;
    using RSG.Editor.SharedCommands;

    /// <summary>
    /// Defines perforce commands for any Rockstar C#/WPF application. This class cannot be
    /// inherited.
    /// </summary>
    public sealed class PerforceCommands
    {
        #region Fields
        /// <summary>
        /// The private cache of command objects.
        /// </summary>
        private static RockstarRoutedCommand[] _internalCommands = InitialiseInternalStorage();

        /// <summary>
        /// The private string table resource manager used for getting the command related
        /// strings and objects.
        /// </summary>
        private static CommandResourceManager _resourceManager = InitialiseResourceManager();
        #endregion Fields

        #region Enumerations
        /// <summary>
        /// Defines all of the command identifiers for the different core Rockstar commands.
        /// </summary>
        private enum Id : byte
        {
            /// <summary>
            /// Used to identifier the <see cref="PerforceCommands.Add"/> routed command.
            /// </summary>
            Add,

            /// <summary>
            /// Used to identifier the <see cref="PerforceCommands.Delete"/> routed command.
            /// </summary>
            Delete,

            /// <summary>
            /// Used to identifier the <see cref="PerforceCommands.GetLatestRevision"/> routed
            /// command.
            /// </summary>
            GetLatestRevision,

            /// <summary>
            /// Used to identifier the <see cref="PerforceCommands.Edit"/> routed command.
            /// </summary>
            Edit,

            /// <summary>
            /// Used to identifier the <see cref="PerforceCommands.Refresh"/> routed command.
            /// </summary>
            Refresh,

            /// <summary>
            /// Used to identifier the <see cref="PerforceCommands.Revert"/> routed command.
            /// </summary>
            Revert,

            /// <summary>
            /// Used to identifier the <see cref="PerforceCommands.RevertIfUnchanged"/> routed
            /// command.
            /// </summary>
            RevertIfUnchanged,
        } // PerforceCommands.CommandId {Enum}
        #endregion Enumerations

        #region Properties
        /// <summary>
        /// Gets the value that represents the Add command.
        /// </summary>
        public static RockstarRoutedCommand Add
        {
            get { return EnsureCommandExists(Id.Add, PerforceIcons.MarkForAdd); }
        }

        /// <summary>
        /// Gets the value that represents the Delete command.
        /// </summary>
        public static RockstarRoutedCommand Delete
        {
            get { return EnsureCommandExists(Id.Delete); }
        }

        /// <summary>
        /// Gets the value that represents the Get Latest Revision command.
        /// </summary>
        public static RockstarRoutedCommand GetLatestRevision
        {
            get { return EnsureCommandExists(Id.GetLatestRevision, PerforceIcons.GetLatest); }
        }

        /// <summary>
        /// Gets the value that represents the Edit command.
        /// </summary>
        public static RockstarRoutedCommand Edit
        {
            get { return EnsureCommandExists(Id.Edit, PerforceIcons.CheckedOut); }
        }

        /// <summary>
        /// Gets the value that represents the Refresh command.
        /// </summary>
        public static RockstarRoutedCommand Refresh
        {
            get { return EnsureCommandExists(Id.Refresh, CommonIcons.Refresh); }
        }

        /// <summary>
        /// Gets the value that represents the Revert command.
        /// </summary>
        public static RockstarRoutedCommand Revert
        {
            get { return EnsureCommandExists(Id.Revert, PerforceIcons.Revert); }
        }

        /// <summary>
        /// Gets the value that represents the Revert If Unchanged command.
        /// </summary>
        public static RockstarRoutedCommand RevertIfUnchanged
        {
            get
            {
                return EnsureCommandExists(
                    Id.RevertIfUnchanged, PerforceIcons.RevertUnchanged);
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates the standard perforce menu inside the specified parent item.
        /// </summary>
        /// <param name="parent">
        /// The id for the parent menu.
        /// </param>
        /// <param name="getLatest">
        /// The id for the get latest menu item.
        /// </param>
        /// <param name="checkout">
        /// The id for the check out menu item.
        /// </param>
        /// <param name="add">
        /// The id for the add menu item.
        /// </param>
        /// <param name="revertUnchanged">
        /// The id for the revert unchanged menu item.
        /// </param>
        /// <param name="revert">
        /// The id for the revert menu item.
        /// </param>
        /// <param name="refresh">
        /// The id for the refresh menu item.
        /// </param>
        public static void CreateStandardMenu(
            Guid parent,
            Guid getLatest,
            Guid checkout,
            Guid add,
            Guid revertUnchanged,
            Guid revert,
            Guid refresh)
        {
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                PerforceCommands.GetLatestRevision,
                index++,
                false,
                "Get Latest Revision",
                getLatest,
                parent);

            RockstarCommandManager.AddCommandInstance(
                PerforceCommands.Edit,
                index++,
                true,
                "Check Out",
                checkout,
                parent);

            RockstarCommandManager.AddCommandInstance(
                PerforceCommands.Add,
                index++,
                false,
                "Mark for Add",
                add,
                parent);

            RockstarCommandManager.AddCommandInstance(
                PerforceCommands.RevertIfUnchanged,
                index++,
                true,
                "Revert If Unchanged",
                revertUnchanged,
                parent);

            RockstarCommandManager.AddCommandInstance(
                PerforceCommands.Revert,
                index++,
                false,
                "Revert",
                revert,
                parent);

            RockstarCommandManager.AddCommandInstance(
                PerforceCommands.Refresh,
                index++,
                true,
                "Refresh Perforce State",
                refresh,
                parent);
        }

        /// <summary>
        /// Creates a new <see cref="RSG.Editor.RockstarRoutedCommand"/> object that is
        /// associated with the specified identifier.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command to create.
        /// </param>
        /// <param name="icon">
        /// The icon that the created command should be using.
        /// </param>
        /// <returns>
        /// The newly created <see cref="RSG.Editor.RockstarRoutedCommand"/> object.
        /// </returns>
        private static RockstarRoutedCommand CreateCommand(string id, BitmapSource icon)
        {
            string name = _resourceManager.GetName(id);
            InputGestureCollection gestures = _resourceManager.GetGestures(id);
            string category = _resourceManager.GetCategory(id);
            string description = _resourceManager.GetDescription(id);

            return new RockstarRoutedCommand(
                name, typeof(PerforceCommands), gestures, category, description, icon);
        }

        /// <summary>
        /// Makes sure the internal value for the command associated with the specified
        /// identifier exists and returns it.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command that should be created and returned.
        /// </param>
        /// <returns>
        /// The <see cref="RSG.Editor.RockstarRoutedCommand"/> object associated with the
        /// specified identifier.
        /// </returns>
        private static RockstarRoutedCommand EnsureCommandExists(Id id)
        {
            return EnsureCommandExists(id, null);
        }

        /// <summary>
        /// Makes sure the internal value for the command associated with the specified
        /// identifier exists and returns it.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command that should be created and returned.
        /// </param>
        /// <param name="icon">
        /// The icon that the command should be using.
        /// </param>
        /// <returns>
        /// The <see cref="RSG.Editor.RockstarRoutedCommand"/> object associated with the
        /// specified identifier.
        /// </returns>
        private static RockstarRoutedCommand EnsureCommandExists(Id id, BitmapSource icon)
        {
            int commandIndex = (int)id;
            if (commandIndex < -1 || commandIndex >= _internalCommands.Length)
            {
                return null;
            }

            RockstarRoutedCommand command = _internalCommands[commandIndex];
            if (command == null)
            {
                lock (_internalCommands)
                {
                    if (command == null)
                    {
                        command = CreateCommand(id.ToString(), icon);
                        _internalCommands[commandIndex] = command;
                    }
                }
            }

            return command;
        }

        /// <summary>
        /// Initialises the array used to store the
        /// <see cref="RSG.Editor.RockstarRoutedCommand"/> objects that make up the core
        /// commands for a Rockstar application.
        /// </summary>
        /// <returns>
        /// The array used to store the internal command objects.
        /// </returns>
        private static RockstarRoutedCommand[] InitialiseInternalStorage()
        {
            return new RockstarRoutedCommand[Enum.GetValues(typeof(Id)).Length];
        }

        /// <summary>
        /// Initialises the resource manager to use to retrieve command related strings and
        /// objects from a resource data file.
        /// </summary>
        /// <returns>
        /// The resource manager to use with this class.
        /// </returns>
        private static CommandResourceManager InitialiseResourceManager()
        {
            Type type = typeof(PerforceCommands);
            string baseName = type.Namespace + ".Resources.CommandStringTable";
            return new CommandResourceManager(baseName, type.Assembly);
        }
        #endregion Methods
    } // RSG.Editor.Controls.Perforce.PerforceCommands {Class}
} // RSG.Editor.Controls.Perforce {Namespace}
