﻿//---------------------------------------------------------------------------------------------
// <copyright file="TreeViewModelBase.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.AssetBuildMonitor.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Abstract base class for all Host View-Model classes; so that we can represent
    /// a hierarchy.
    /// </summary>
    public abstract class TreeViewModelBase : 
        AssetBuildMonitorViewModelBase
    {
        #region AssetBuildMonitorViewModelBase Abstract Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ViewModelValidation()
        {

        }
        #endregion // AssetBuildMonitorViewModelBase Abstract Methods
    }

} // RSG.AssetBuildMonitor.ViewModel namespace
