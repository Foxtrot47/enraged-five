﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using RSG.Rag.Widgets;

namespace RSG.Rag.ViewModel.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class ComboViewModel<T> : WidgetViewModel<WidgetCombo<T>>
    {
        #region Fields
        /// <summary>
        /// 
        /// </summary>
        private readonly ObservableCollection<String> _items = new ObservableCollection<String>();
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        protected ComboViewModel(WidgetCombo<T> widget)
            : base(widget)
        {
            ResetItems();
            Widget.ItemChanged += Widget_ItemChanged;
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public IReadOnlyCollection<String> Items
        {
            get { return _items; }
        }

        /// <summary>
        /// 
        /// </summary>
        public abstract int SelectedItem { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public override bool IsModified
        {
            get { return !EqualityComparer<T>.Default.Equals(Widget.DefaultItem, Widget.SelectedItem); }
        }
        #endregion // Properties

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Widget_ItemChanged(object sender, ItemChangedEventArgs e)
        {
            Application app = Application.Current;
            if (!app.Dispatcher.CheckAccess())
            {
                app.Dispatcher.Invoke(() => _items[e.Index] = e.Value);
            }
            else
            {
                _items[e.Index] = e.Value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        protected override void OnWidgetPropertyChanged(String propertyName)
        {
            if (propertyName == "SelectedItem")
            {
                NotifyPropertyChanged("IsModified");
            }
            else if (propertyName == "Offset")
            {
                NotifyPropertyChanged("SelectedItem", "IsModified");
            }
            else if (propertyName == "Items")
            {
                ResetItems();
            }

            base.OnWidgetPropertyChanged(propertyName);
        }
        #endregion // Event Handlers

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        private void ResetItems()
        {
            Application app = Application.Current;
            if (!app.Dispatcher.CheckAccess())
            {
                app.Dispatcher.Invoke(() => ResetItems());
                return;
            }

            _items.Clear();

            foreach (String item in Widget.Items)
            {
                _items.Add(item);
            }
        }
        #endregion // Methods
    } // ComboViewModel<T>
}
