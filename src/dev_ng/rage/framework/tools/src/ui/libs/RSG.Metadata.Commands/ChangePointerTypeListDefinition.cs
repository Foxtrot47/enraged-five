﻿//---------------------------------------------------------------------------------------------
// <copyright file="ChangePointerTypeListDefinition.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Commands
{
    using System.Collections.Generic;
    using RSG.Editor;
    using RSG.Metadata.Model.Definitions;
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// The multi command definition for the valid list of items that can be added to the
    /// active array. This class cannot be inherited.
    /// </summary>
    public sealed class ChangePointerTypeListDefinition : MultiCommand
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ChangePointerTypeListDefinition"/>
        /// class.
        /// </summary>
        public ChangePointerTypeListDefinition()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the System.Windows.Input.RoutedCommand that this definition is wrapping.
        /// </summary>
        public override RockstarRoutedCommand Command
        {
            get { return MetadataCommands.ChangePointerType; }
        }

        /// <summary>
        /// Gets the name of this command definition.
        /// </summary>
        public override string Name
        {
            get { return "Change Pointer Type List"; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates this definitions items using the specified pointer as a base. This adds
        /// items to the definition based on the type hierarchy of the specified pointer.
        /// </summary>
        /// <param name="pointer">
        /// The array instance that the items should be created from.
        /// </param>
        public void CreateItemsFromPointer(PointerTunable pointer)
        {
            if (this.Items.Count > 0)
            {
                this.Items.Clear();
            }

            if (pointer == null || !pointer.PointerMember.CanBeDerivedType)
            {
                return;
            }

            if (!(pointer.Parent is ArrayTunable))
            {
                this.Items.Add(new ChangeTypeItemListItem(this, "NULL"));
            }

            IStructure structure = pointer.PointerMember.ReferencedStructure;
            List<IStructure> descendants = GetSelfAndAllDescendents(structure);
            foreach (IStructure descendant in descendants)
            {
                ChangeTypeItemListItem item = new ChangeTypeItemListItem(this, descendant);
                this.Items.Add(item);
            }
        }

        /// <summary>
        /// Iterators through the specified structure root object and all of its descendants,
        /// immediate and children.
        /// </summary>
        /// <param name="root">
        /// The structure to start the iterator from.
        /// </param>
        /// <returns>
        /// A iterator through the specified structure root object and all of its descendants.
        /// </returns>
        private static List<IStructure> GetSelfAndAllDescendents(IStructure root)
        {
            List<IStructure> descendants = new List<IStructure>();
            if (root == null)
            {
                return descendants;
            }

            descendants.Add(root);
            foreach (IStructure descendant in root.ImmediateDescendants)
            {
                descendants.AddRange(GetSelfAndAllDescendents(descendant));
            }

            return descendants;
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// Defines the item used for the <see cref="ChangePointerTypeListDefinition"/> command
        /// definition.
        /// </summary>
        private class ChangeTypeItemListItem : MultiCommandItem<IStructure>
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="ChangeTypeItemListItem"/> class.
            /// </summary>
            /// <param name="definition">
            /// The multi command definition that owns this item.
            /// </param>
            /// <param name="parameter">
            /// The parameter that is sent when this item is executed.
            /// </param>
            public ChangeTypeItemListItem(MultiCommand definition, IStructure parameter)
                : base(definition, parameter.ShortDataType, parameter)
            {
            }

            /// <summary>
            /// Initialises a new instance of the <see cref="ChangeTypeItemListItem"/> class.
            /// </summary>
            /// <param name="definition">
            /// The multi command definition that owns this item.
            /// </param>
            /// <param name="text">
            /// The text that is used to display this item to the user.
            /// </param>
            public ChangeTypeItemListItem(MultiCommand definition, string text)
                : base(definition, text, null)
            {
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets the description for this item. This is also what is shown as a tooltip
            /// along with the key gesture if one is set if it is being rendered as a
            /// toggle control.
            /// </summary>
            public override string Description
            {
                get { return null; }
            }
            #endregion Properties
        } // ChangePointerTypeListDefinition.ChangeTypeItemListItem {Class}
        #endregion Classes
    } // RSG.Metadata.Commands.ChangePointerTypeListDefinition {Class}
} // RSG.Metadata.Commands {Namespace}
