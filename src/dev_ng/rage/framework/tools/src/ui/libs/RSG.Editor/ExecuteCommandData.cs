﻿//---------------------------------------------------------------------------------------------
// <copyright file="ExecuteCommandData.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System.Windows;
    using System.Windows.Input;

    /// <summary>
    /// Provides data for a method that implements a command.
    /// </summary>
    public class ExecuteCommandData : CommandData
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="ContinueRouting"/> property.
        /// </summary>
        private bool _continueRouting;

        /// <summary>
        /// The private field used for the <see cref="Handled"/> property.
        /// </summary>
        private bool _handled;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ExecuteCommandData"/> class based on
        /// the specified command being invoked.
        /// </summary>
        /// <param name="command">
        /// The command that was invoked.
        /// </param>
        internal ExecuteCommandData(ICommand command)
            : base(command)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether the input routed event that invoked the
        /// command should continue to route through the element tree.
        /// </summary>
        public bool ContinueRouting
        {
            get { return this._continueRouting; }
            set { this._continueRouting = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the routed event has been handled somewhere
        /// as it travels the element tree.
        /// </summary>
        public bool Handled
        {
            get { return this._handled; }
            set { this._handled = value; }
        }
        #endregion Properties
    } // RSG.Editor.ExecuteCommandData {Class}
} // RSG.Editor {Namespace}
