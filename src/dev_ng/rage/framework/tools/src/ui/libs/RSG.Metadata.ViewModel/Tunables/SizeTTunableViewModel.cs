﻿//---------------------------------------------------------------------------------------------
// <copyright file="SizeTTunableViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Tunables
{
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// The view model for a <see cref="SizeTTunable"/> model object.
    /// </summary>
    public class SizeTTunableViewModel : TunableViewModelBase<SizeTTunable>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SizeTTunableViewModel"/> class.
        /// </summary>
        /// <param name="tunable">
        /// The size T tunable that this view model is representing.
        /// </param>
        /// <param name="parent">
        /// The parent tunable view model for this view model.
        /// </param>
        public SizeTTunableViewModel(SizeTTunable tunable, ITunableViewModel parent)
            : base(tunable, parent)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the value currently assigned to the size T tunable.
        /// </summary>
        public ulong Value
        {
            get { return this.Model.Value; }
            set { this.Model.Value = value; }
        }
        #endregion Properties
    } // RSG.Metadata.ViewModel.Tunables.SizeTTunableViewModel {Class}
} // RSG.Metadata.ViewModel.Tunables {Namespace}
