﻿//---------------------------------------------------------------------------------------------
// <copyright file="TabControlProperties.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.AttachedDependencyProperties
{
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Contains attached properties that are related to the textbox.
    /// </summary>
    public static class TabControlProperties
    {
        #region Fields
        /// <summary>
        /// Identifies the ShowBreakLine dependency property.
        /// </summary>
        public static readonly DependencyProperty ShowBreakLineProperty;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="TabControlProperties"/> class.
        /// </summary>
        static TabControlProperties()
        {
            ShowBreakLineProperty =
                DependencyProperty.RegisterAttached(
                "ShowBreakLine",
                typeof(bool),
                typeof(TabControlProperties),
                new PropertyMetadata(false));
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Retrieves the ShowBreakLine dependency property value attached to the specified
        /// object.
        /// </summary>
        /// <param name="element">
        /// The object whose attached ShowBreakLine dependency property value will be returned.
        /// </param>
        /// <returns>
        /// The ShowBreakLine dependency property value attached to the specified object.
        /// </returns>
        [AttachedPropertyBrowsableForChildrenAttribute(IncludeDescendants = false)]
        [AttachedPropertyBrowsableForType(typeof(TabControl))]
        public static bool GetShowBreakLine(DependencyObject element)
        {
            return (bool)element.GetValue(ShowBreakLineProperty);
        }

        /// <summary>
        /// Sets the ShowBreakLine dependency property value on the specified object to the
        /// specified value.
        /// </summary>
        /// <param name="element">
        /// The object whose attached ShowBreakLine dependency property value will be set.
        /// </param>
        /// <param name="value">
        /// The value to set the attached ShowBreakLine dependency property to on the specified
        /// object.
        /// </param>
        public static void SetShowBreakLine(DependencyObject element, bool value)
        {
            element.SetValue(ShowBreakLineProperty, value);
        }
        #endregion Methods
    } // RSG.Editor.Controls.AttachedDependencyProperties.TabControlProperties {Class}
} // RSG.Editor.Controls.AttachedDependencyProperties {Namespace}
