﻿//---------------------------------------------------------------------------------------------
// <copyright file="ReadOnlyModelCollection{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Model
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Linq;

    /// <summary>
    /// Represents a collection of objects that can be access via a index or iterated over.
    /// Also supports notifications and undo / redo.
    /// </summary>
    /// <typeparam name="T">
    /// The type of elements in the collection.
    /// </typeparam>
    public class ReadOnlyModelCollection<T>
        : NotifyPropertyChangedBase,
        INotifyCollectionChanged,
        IReadOnlyList<T>,
        IList<T> where T : IModel
    {
        #region Fields
        /// <summary>
        /// The private list that is used as the internal data for the collection.
        /// </summary>
        private ModelCollection<T> _internalList;

        /// <summary>
        /// A generic object used to synchronize access to the items.
        /// </summary>
        private object _syncRoot = new object();
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ReadOnlyModelCollection{T}"/> class.
        /// </summary>
        /// <param name="innerCollection">
        /// The inner model collection that this readonly collection wraps.
        /// </param>
        public ReadOnlyModelCollection(ModelCollection<T> innerCollection)
        {
            if (innerCollection == null)
            {
                throw new SmartArgumentNullException(() => innerCollection);
            }

            this._internalList = innerCollection;
            PropertyChangedEventManager.AddHandler(
                this._internalList, this.OnPropertyChange, String.Empty);

            CollectionChangedEventManager.AddHandler(
                this._internalList, this.OnCollectionChange);
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs when the collection changes.
        /// </summary>
        public event NotifyCollectionChangedEventHandler CollectionChanged;
        #endregion Events

        #region Properties
        /// <summary>
        /// Gets the number of elements contained in the collection.
        /// </summary>
        public int Count
        {
            get { return this._internalList.Count; }
        }

        /// <summary>
        /// Gets a value indicating whether the collection is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return true; }
        }

        /// <summary>
        /// Gets the undo engine provider instance that this collection uses.
        /// </summary>
        internal IUndoEngineProvider UndoEngineProvider
        {
            get { return this._internalList.UndoEngineProvider; }
        }

        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index of the element to get or set.
        /// </param>
        /// <returns>
        /// The element at the specified index.
        /// </returns>
        /// <exception cref="System.NotSupportedException">
        /// Always thrown on set.
        /// </exception>
        public T this[int index]
        {
            get
            {
                return (T)this._internalList[index];
            }

            set
            {
                this.ThrowReadOnlyException();
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Adds an item to the end of the collection.
        /// </summary>
        /// <param name="item">
        /// The object to add to the collection.
        /// </param>
        /// <exception cref="System.NotSupportedException">
        /// Always thrown.
        /// </exception>
        public void Add(T item)
        {
            this.ThrowReadOnlyException();
        }

        /// <summary>
        /// Adds multiple items to the end of the collection.
        /// </summary>
        /// <param name="items">
        /// The list of items that should be added to the collection.
        /// </param>
        /// <exception cref="System.NotSupportedException">
        /// Always thrown.
        /// </exception>
        public void AddRange(IList<T> items)
        {
            this.ThrowReadOnlyException();
        }

        /// <summary>
        /// Adds multiple items to the end of the collection.
        /// </summary>
        /// <param name="items">
        /// The list of items that should be added to the collection.
        /// </param>
        /// <exception cref="System.NotSupportedException">
        /// Always thrown.
        /// </exception>
        public void AddRange(IList items)
        {
            this.ThrowReadOnlyException();
        }

        /// <summary>
        /// Removes all items from the collection.
        /// </summary>
        /// <exception cref="System.NotSupportedException">
        /// Always thrown.
        /// </exception>
        public void Clear()
        {
            this.ThrowReadOnlyException();
        }

        /// <summary>
        /// Determines whether the collection contains a specific value.
        /// </summary>
        /// <param name="item">
        /// The object to locate in the collection.
        /// </param>
        /// <returns>
        /// True if item is found in the collection; otherwise false.
        /// </returns>
        public bool Contains(T item)
        {
            return this._internalList.Contains(item);
        }

        /// <summary>
        /// Copies the elements of the collection to an System.Array, starting at a
        /// particular index.
        /// </summary>
        /// <param name="array">
        /// The one-dimensional System.Array that is the destination of the elements.
        /// </param>
        /// <param name="arrayIndex">
        /// The zero-based index in array at which copying begins.
        /// </param>
        public void CopyTo(T[] array, int arrayIndex)
        {
            this._internalList.CopyTo(array, arrayIndex);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A System.Collections.Generic.IEnumerator{T} that can be used to iterate through the
        /// collection.
        /// </returns>
        public IEnumerator<T> GetEnumerator()
        {
            return new ModelCollectionEnumerator<T>(this._internalList.GetEnumerator());
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A System.Collections.Generic.IEnumerator{T} that can be used to iterate through the
        /// collection.
        /// </returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this._internalList.GetEnumerator();
        }

        /// <summary>
        /// Determines the index of a specific item in the collection.
        /// </summary>
        /// <param name="item">
        /// The object to locate in the collection.
        /// </param>
        /// <returns>
        /// The index of item if found in the list; otherwise, -1.
        /// </returns>
        public int IndexOf(T item)
        {
            return this._internalList.IndexOf(item);
        }

        /// <summary>
        /// Inserts the specified item into the collection at the specified index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index at which item should be inserted.
        /// </param>
        /// <param name="item">
        /// The object to insert into the collection.
        /// </param>
        /// <exception cref="System.NotSupportedException">
        /// Always thrown.
        /// </exception>
        public void Insert(int index, T item)
        {
            this.ThrowReadOnlyException();
        }

        /// <summary>
        /// Inserts the specified item into the collection at the specified index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index at which item should be inserted.
        /// </param>
        /// <param name="items">
        /// The objects to insert into the collection.
        /// </param>
        /// <exception cref="System.NotSupportedException">
        /// Always thrown.
        /// </exception>
        public void Insert(int index, IList<T> items)
        {
            this.ThrowReadOnlyException();
        }

        /// <summary>
        /// Inserts multiple items into the collection at the specified index.
        /// </summary>
        /// <param name="index">
        /// The non-zero index where the items should be inserted.
        /// </param>
        /// <param name="items">
        /// The list of items that should be added to the collection.
        /// </param>
        /// <exception cref="System.NotSupportedException">
        /// Always thrown.
        /// </exception>
        public void Insert(int index, IList items)
        {
            this.ThrowReadOnlyException();
        }

        /// <summary>
        /// Moves the item at the specified index to a new location in the collection.
        /// </summary>
        /// <param name="oldIndex">
        /// The zero-based index specifying the location of the item to be moved.
        /// </param>
        /// <param name="newIndex">
        /// The zero-based index specifying the new location of the item.
        /// </param>
        /// <exception cref="System.NotSupportedException">
        /// Always thrown.
        /// </exception>
        public void Move(int oldIndex, int newIndex)
        {
            this.ThrowReadOnlyException();
        }

        /// <summary>
        /// Moves the first occurrence of the specified item to the specified index.
        /// </summary>
        /// <param name="item">
        /// The item that should be moved.
        /// </param>
        /// <param name="newIndex">
        /// The zero-based index specifying the new location of the item.
        /// </param>
        /// <exception cref="System.NotSupportedException">
        /// Always thrown.
        /// </exception>
        public void Move(T item, int newIndex)
        {
            this.ThrowReadOnlyException();
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the collection.
        /// </summary>
        /// <param name="item">
        /// The object to remove from the collection.
        /// </param>
        /// <returns>
        /// True if item was successfully removed from the collection, otherwise, false. This
        /// method also returns false if item is not found in the original collection.
        /// </returns>
        /// <exception cref="System.NotSupportedException">
        /// Always thrown.
        /// </exception>
        public bool Remove(T item)
        {
            this.ThrowReadOnlyException();
            return false;
        }

        /// <summary>
        /// Removes the item at the specified index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index of the item to remove.
        /// </param>
        /// <exception cref="System.NotSupportedException">
        /// Always thrown.
        /// </exception>
        public void RemoveAt(int index)
        {
            this.ThrowReadOnlyException();
        }

        /// <summary>
        /// Removes a range of elements from the collection.
        /// </summary>
        /// <param name="index">
        /// The zero-based index specifying the starting location of the items to be removed.
        /// </param>
        /// <param name="count">
        /// The number of elements to remove.
        /// </param>
        /// <exception cref="System.NotSupportedException">
        /// Always thrown.
        /// </exception>
        public void RemoveRange(int index, int count)
        {
            this.ThrowReadOnlyException();
        }

        /// <summary>
        /// Replaces the first occurrence of the specified old item with the
        /// specified new item.
        /// </summary>
        /// <param name="oldItem">
        /// The item that is going to be replaced.
        /// </param>
        /// <param name="newItem">
        /// The item that is going to be replacing the old item.
        /// </param>
        /// <exception cref="System.NotSupportedException">
        /// Always thrown.
        /// </exception>
        public void Replace(T oldItem, T newItem)
        {
            this.ThrowReadOnlyException();
        }

        /// <summary>
        /// Adds an item to the end of the collection.
        /// </summary>
        /// <param name="index">
        /// The zero-based index specifying the location of the item to be replaced.
        /// </param>
        /// <param name="newItem">
        /// The item that is going to be replacing the old item.
        /// </param>
        /// <exception cref="System.NotSupportedException">
        /// Always thrown.
        /// </exception>
        public void Replace(int index, T newItem)
        {
            this.ThrowReadOnlyException();
        }

        /// <summary>
        /// Forwards the collection changes from the internal collection to any handles
        /// attached to this collection.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Collections.Specialized.NotifyCollectionChangedEventArgs data used for
        /// the event.
        /// </param>
        private void OnCollectionChange(object sender, NotifyCollectionChangedEventArgs e)
        {
            NotifyCollectionChangedEventHandler handler = this.CollectionChanged;
            if (handler == null)
            {
                return;
            }

            handler(this, e);
        }

        /// <summary>
        /// Forwards the property changes from the internal collection to any handles attached
        /// to this collection.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.ComponentModel.PropertyChangedEventArgs data used for the event.
        /// </param>
        private void OnPropertyChange(object sender, PropertyChangedEventArgs e)
        {
            this.NotifyPropertyChanged(e.PropertyName);
        }

        /// <summary>
        /// Throws the exception when a user tries to modify this collection directly.
        /// </summary>
        private void ThrowReadOnlyException()
        {
            throw new NotSupportedException("Unable to modify a read-only collection");
        }
        #endregion Methods
    } // RSG.Editor.Model.ReadOnlyModelCollection{T} {Class}
} // RSG.Editor.Model {Namespace}
