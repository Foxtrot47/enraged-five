﻿//---------------------------------------------------------------------------------------------
// <copyright file="MultiCommand.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.Xml;

    /// <summary>
    /// Represents a command definition that can automatically add command bar items within
    /// its own instance. This can be used for example for a most recently used list.
    /// </summary>
    public abstract class MultiCommand : CommandDefinition
    {
        #region Fields
        /// <summary>
        /// The private reference to the item that should be selected by default.
        /// </summary>
        private IMultiCommandItem _defaultSelection;

        /// <summary>
        /// The private field used for the <see cref="Items"/> property.
        /// </summary>
        private ObservableCollection<IMultiCommandItem> _items;

        /// <summary>
        /// The private field used for the <see cref="SelectedItem"/> property.
        /// </summary>
        private IMultiCommandItem _selectedItem;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MultiCommand"/> class.
        /// </summary>
        protected MultiCommand()
        {
            this._items = new ObservableCollection<IMultiCommandItem>();
            CollectionChangedEventManager.AddHandler(this._items, this.ItemsChanged);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether the individual items are selectable and only one
        /// item can be selected at a time.
        /// </summary>
        public bool AreItemsSingleSelectable
        {
            get
            {
                if (!this.CanItemsBeSelected)
                {
                    return false;
                }

                return this.SelectionMode == MultiCommandSelectionMode.Single;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the individual items are selectable.
        /// </summary>
        public virtual bool CanItemsBeSelected
        {
            get { return false; }
        }

        /// <summary>
        /// Gets a collection of items that represent the different items that can be executed
        /// through this command.
        /// </summary>
        public ObservableCollection<IMultiCommandItem> Items
        {
            get
            {
                return this._items;
            }

            internal set
            {
                if (Object.ReferenceEquals(this._items, value))
                {
                    return;
                }

                if (value == null)
                {
                    throw new SmartArgumentNullException(() => value);
                }

                if (this._items != null)
                {
                    CollectionChangedEventManager.RemoveHandler(
                        this._items, this.ItemsChanged);
                }

                this._items = value;
                if (value != null)
                {
                    CollectionChangedEventManager.AddHandler(this._items, this.ItemsChanged);
                }
            }
        }

        /// <summary>
        /// Gets or sets the item that is currently selected.
        /// </summary>
        public IMultiCommandItem SelectedItem
        {
            get
            {
                return this._selectedItem;
            }

            set
            {
                IMultiCommandItem oldItem = this._selectedItem;
                if (this.SetProperty(ref this._selectedItem, value))
                {
                    if (oldItem != null)
                    {
                        if (this.SelectionMode == MultiCommandSelectionMode.Single)
                        {
                            if (this.CanItemsBeSelected)
                            {
                                oldItem.SetIsToggled(false, false);
                            }
                        }
                    }

                    if (value != null)
                    {
                        value.SetIsToggled(true, false);
                    }
                }
            }
        }

        /// <summary>
        /// Gets a value indicating the selection mode style adopted by this instance.
        /// </summary>
        public virtual MultiCommandSelectionMode SelectionMode
        {
            get { return MultiCommandSelectionMode.Single; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Deserialises the commands state using the data contained within the specified xml
        /// reader.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the state data.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        /// <returns>
        /// A value indicating whether the command needs to be executed due to the
        /// de-serialisation process.
        /// </returns>
        public override bool DeserialiseState(XmlReader reader, IFormatProvider provider)
        {
            int selectedIndex;
            if (int.TryParse(reader.GetAttribute("selected"), out selectedIndex))
            {
                if (this.Items.Count > selectedIndex)
                {
                    IMultiCommandItem item = this.Items[selectedIndex];
                    if (!Object.ReferenceEquals(item, this.SelectedItem))
                    {
                        this.SelectedItem = item;
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Serialises the state of this definition into the specified xml writer.
        /// </summary>
        /// <param name="writer">
        /// The xml writer that the state gets serialised out to.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        public override void SerialiseState(XmlWriter writer, IFormatProvider provider)
        {
            int selectedIndex = this.Items.IndexOf(this.SelectedItem);
            writer.WriteAttributeString("selected", selectedIndex.ToString(provider));
        }

        /// <summary>
        /// Sets up the item that is selected by default.
        /// </summary>
        /// <param name="item">
        /// Specifies the item that should be selected by default.
        /// </param>
        protected void SetDefaultSelection(IMultiCommandItem item)
        {
            if (item != null && this._selectedItem == null && this._items.Contains(item))
            {
                this.SelectedItem = item;
                this.Command.Execute(item.GetCommandParameter(), null);
            }

            this._defaultSelection = item;
        }

        /// <summary>
        /// Gets called whenever the items collection is changed.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.ComponentModel.CollectionChangeEventArgs data for the event.
        /// </param>
        private void ItemsChanged(object s, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                if (!this.Items.Contains(this._defaultSelection))
                {
                    this._defaultSelection = null;
                }

                if (!this.Items.Contains(this.SelectedItem))
                {
                    if (this._defaultSelection != null)
                    {
                        this.SelectedItem = this._defaultSelection;
                    }
                    else if (this.Items.Count > 0)
                    {
                        this.SelectedItem = this.Items[0];
                    }
                    else
                    {
                        this.SelectedItem = null;
                    }

                    if (this.SelectedItem != null && this.AreItemsSingleSelectable)
                    {
                        this.Command.Execute(this.SelectedItem.GetCommandParameter(), null);
                    }
                }

                return;
            }

            if (e.Action == NotifyCollectionChangedAction.Reset)
            {
                if (!this.Items.Contains(this.SelectedItem))
                {
                    this.SelectedItem = null;
                }

                if (!this.Items.Contains(this._defaultSelection))
                {
                    this._defaultSelection = null;
                }

                return;
            }

            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                if (this.SelectedItem == null && this.Items.Contains(this._defaultSelection))
                {
                    this.SelectedItem = this._defaultSelection;
                    if (this.AreItemsSingleSelectable)
                    {
                        this.Command.Execute(this.SelectedItem.GetCommandParameter(), null);
                    }
                }
            }
        }
        #endregion Methods
    } // RSG.Editor.MultiCommand {Class}
} // RSG.Editor {Namespace}
