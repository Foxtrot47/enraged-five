﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Configuration;
using RSG.Base.Configuration.Automation;
using RSG.Base.Extensions;

namespace RSG.Automation.ViewModel
{

    /// <summary>
    /// Studio View-Model; storing all the Automation Services available
    /// at that studio.
    /// </summary>
    public class StudioViewModel : ViewModelBase
    {
        #region Properties
        /// <summary>
        /// Associated Studio.
        /// </summary>
        public IStudio Studio
        {
            get { return _studio; }
            private set
            {
                SetProperty(ref _studio, value, "Studio");
            }
        }
        private IStudio _studio;

        /// <summary>
        /// Studio services.
        /// </summary>
        public ObservableCollection<AutomationServiceViewModel> AutomationServices
        {
            get { return _automationServices; }
            private set
            {
                SetProperty(ref _automationServices, value, "AutomationServices");
            }
        }
        private ObservableCollection<AutomationServiceViewModel> _automationServices;
        
        /// <summary>
        /// Whether this is the current user's studio.
        /// </summary>
        public bool IsCurrentStudio
        {
            get { return _isCurrentStudio; }
            private set
            {
                SetProperty(ref _isCurrentStudio, value, "IsCurrentStudio");
            }
        }
        private bool _isCurrentStudio;

        /// <summary>
        /// If the tree view for this studio is expanded.
        /// </summary>
        public bool IsExpanded
        {
            get { return _isExpanded; }
            private set
            {
                SetProperty(ref _isExpanded, value, "IsExpanded");
            }
        }
        private bool _isExpanded;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="studio"></param>
        /// <param name="localStudio"></param>
        /// <param name="services"></param>
        public StudioViewModel(IStudio studio, bool localStudio, IEnumerable<IAutomationServiceConfig> services)
        {
            this.Studio = studio;
            this.IsCurrentStudio = localStudio;
            if (localStudio) 
                this.IsExpanded = true;
            this.AutomationServices = new ObservableCollection<AutomationServiceViewModel>();
            this.AutomationServices.AddRange(services.Select(s => new AutomationServiceViewModel(s)));
        }
        #endregion // Constructor(s)
    }

} // RSG.Automation.ViewModel namespace
