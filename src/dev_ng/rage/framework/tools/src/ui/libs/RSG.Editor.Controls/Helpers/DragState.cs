﻿//---------------------------------------------------------------------------------------------
// <copyright file="DragState.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Helpers
{
    /// <summary>
    /// Defines the different states the drag operation could have finished with.
    /// </summary>
    public enum DragState
    {
        /// <summary>
        /// The drag event hasn't actually finished yet.
        /// </summary>
        NotFinished,

        /// <summary>
        /// The drag event finished because it was cancelled.
        /// </summary>
        Cancelled,

        /// <summary>
        /// The drag event finished correctly with the mouse having moved.
        /// </summary>
        FinishedWithMovement,

        /// <summary>
        /// The drag event finished correctly with the mouse having not moved.
        /// </summary>
        FinishedWithoutMovement,
    } // RSG.Editor.Controls.Helpers.DragState {Enum}
} // RSG.Editor.Controls.Helpers {Namespace}
