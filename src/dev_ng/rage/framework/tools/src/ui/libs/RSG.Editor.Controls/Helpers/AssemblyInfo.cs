﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssemblyInfo.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Helpers
{
    using System;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Reflection;
    using System.Runtime.InteropServices;
    using RSG.Base.Attributes;

    /// <summary>
    /// Provides helper methods to retrieve information from inside a dll and format the
    /// values for display.
    /// </summary>
    public class AssemblyInfo
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="BuildTime"/> property.
        /// </summary>
        private DateTime _buildTime;

        /// <summary>
        /// The private field used for the <see cref="Product"/> property.
        /// </summary>
        private string _copyright;

        /// <summary>
        /// The private field used for the <see cref="Description"/> property.
        /// </summary>
        private string _description;

        /// <summary>
        /// The private field used for the <see cref="Edition"/> property.
        /// </summary>
        private string _edition;

        /// <summary>
        /// The private field used for the <see cref="HelpLink"/> property.
        /// </summary>
        private string _helpLink;

        /// <summary>
        /// The private field used for the <see cref="IsDebug"/> property.
        /// </summary>
        private bool _isDebug;

        /// <summary>
        /// The private field used for the <see cref="Product"/> property.
        /// </summary>
        private string _product;

        /// <summary>
        /// The private field used for the <see cref="Version"/> property.
        /// </summary>
        private Version _version;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="AssemblyInfo"/> class.
        /// </summary>
        /// <param name="assembly">
        /// The assembly to retrieve the information from.
        /// </param>
        public AssemblyInfo(Assembly assembly)
        {
            if (assembly == null)
            {
                throw new SmartArgumentNullException(() => assembly);
            }

            this._buildTime = GetUtcBuildTime(assembly);
            this._copyright = GetCopyright(assembly, null);
            this._description = GetDescription(assembly, null);
            this._edition = GetEdition(assembly);
            this._helpLink = GetHelpLink(assembly);
            this._isDebug = IsAssemblyDebugBuild(assembly);
            this._product = GetProductName(assembly, null);
            this._version = GetVersion(assembly);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the time the assembly was built.
        /// </summary>
        public DateTime BuildTime
        {
            get { return this._buildTime; }
        }

        /// <summary>
        /// Gets the copyright text defined in the associated assembly.
        /// </summary>
        public string Copyright
        {
            get { return this._copyright; }
        }

        /// <summary>
        /// Gets the description defined in the associated assembly.
        /// </summary>
        public string Description
        {
            get { return this._description; }
        }

        /// <summary>
        /// Gets the edition for the associated assembly.
        /// </summary>
        public string Edition
        {
            get { return this._edition; }
        }

        /// <summary>
        /// Gets the help link for the associated assembly.
        /// </summary>
        public string HelpLink
        {
            get { return this._helpLink; }
        }

        /// <summary>
        /// Gets a value indicating whether the associated assembly is for a debug build.
        /// </summary>
        public bool IsDebug
        {
            get { return this._isDebug; }
        }

        /// <summary>
        /// Gets the product name defined in the associated assembly.
        /// </summary>
        public string Product
        {
            get { return this._product; }
        }

        /// <summary>
        /// Gets the version for the associated assembly.
        /// </summary>
        public Version Version
        {
            get { return this._version; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Gets the time the specified assembly was built as a universal time code date time
        /// object.
        /// </summary>
        /// <param name="assembly">
        /// The assembly to retrieve the information from.
        /// </param>
        /// <returns>
        /// The build time of the specified assembly.
        /// </returns>
        public static DateTime GetUtcBuildTime(Assembly assembly)
        {
            return GetBuildTime(assembly, "UTC");
        }

        /// <summary>
        /// Gets the time the specified assembly was built in the specified time zone.
        /// </summary>
        /// <param name="assembly">
        /// The assembly to retrieve the information from.
        /// </param>
        /// <param name="timeZoneId">
        /// The time zone identifier that corresponds to the time zone information to retrieve
        /// the build time in.
        /// </param>
        /// <returns>
        /// The build time of the specified assembly.
        /// </returns>
        public static DateTime GetBuildTime(Assembly assembly, string timeZoneId)
        {
            if (assembly == null)
            {
                throw new SmartArgumentNullException(() => assembly);
            }

            TimeZoneInfo info = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
            if (info == null)
            {
                throw new InvalidTimeZoneException("Unrecognised time zone code specified");
            }

            string filename = assembly.Location;
            if (!File.Exists(filename))
            {
                throw new FileNotFoundException("Unable to location the assembly location");
            }

            byte[] buffer = new byte[Math.Max(Marshal.SizeOf(typeof(ImageFileHeader)), 4)];
            using (FileStream file = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                byte[] positionBuffer = new byte[4];
                file.Position = 60;
                file.Read(positionBuffer, 0, 4);
                file.Position = BitConverter.ToUInt32(positionBuffer, 0);
                file.Read(positionBuffer, 0, 4);
                file.Read(buffer, 0, buffer.Length);
            }

            GCHandle pinnedBuffer = GCHandle.Alloc(buffer, GCHandleType.Pinned);
            try
            {
                IntPtr ptr = pinnedBuffer.AddrOfPinnedObject();
                Type type = typeof(ImageFileHeader);
                ImageFileHeader header = (ImageFileHeader)Marshal.PtrToStructure(ptr, type);

                TimeSpan offset = new TimeSpan(header.TimeDateStamp * TimeSpan.TicksPerSecond);
                DateTime utcTime = new DateTime(1970, 1, 1) + offset;
                return TimeZoneInfo.ConvertTimeFromUtc(utcTime, info);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                pinnedBuffer.Free();
            }
        }

        /// <summary>
        /// Gets the configuration attribute value for the specified assembly or the specified
        /// default value if the attribute is missing.
        /// </summary>
        /// <param name="assembly">
        /// The assembly to retrieve the information from.
        /// </param>
        /// <param name="defaultValue">
        /// The value returned if the specified assembly doesn't have a copyright attribute.
        /// </param>
        /// <returns>
        /// The configuration text of the specified assembly.
        /// </returns>
        public static string GetConfiguration(Assembly assembly, string defaultValue)
        {
            if (assembly == null)
            {
                throw new SmartArgumentNullException(() => assembly);
            }

            var attribute = assembly.GetCustomAttribute<AssemblyConfigurationAttribute>();
            if (attribute == null)
            {
                Debug.Assert(attribute != null, "Assembly is missing configuration text.");
                return defaultValue;
            }

            return attribute.Configuration;
        }

        /// <summary>
        /// Gets the copyright attribute value for the specified assembly or the specified
        /// default value if the attribute is missing.
        /// </summary>
        /// <param name="assembly">
        /// The assembly to retrieve the information from.
        /// </param>
        /// <param name="defaultValue">
        /// The value returned if the specified assembly doesn't have a copyright attribute.
        /// </param>
        /// <returns>
        /// The copyright text of the specified assembly.
        /// </returns>
        public static string GetCopyright(Assembly assembly, string defaultValue)
        {
            if (assembly == null)
            {
                throw new SmartArgumentNullException(() => assembly);
            }

            var attribute = assembly.GetCustomAttribute<AssemblyCopyrightAttribute>();
            if (attribute == null)
            {
                Debug.Assert(attribute != null, "Assembly is missing copyright text.");
                return defaultValue;
            }

            return attribute.Copyright;
        }

        /// <summary>
        /// Gets the description attribute value for the specified assembly or the specified
        /// default value if the attribute is missing.
        /// </summary>
        /// <param name="assembly">
        /// The assembly to retrieve the information from.
        /// </param>
        /// <param name="defaultValue">
        /// The value returned if the specified assembly doesn't have the correct attribute.
        /// </param>
        /// <returns>
        /// The description for the specified assembly.
        /// </returns>
        public static string GetDescription(Assembly assembly, string defaultValue)
        {
            if (assembly == null)
            {
                throw new SmartArgumentNullException(() => assembly);
            }

            var attribute = assembly.GetCustomAttribute<AssemblyDescriptionAttribute>();
            if (attribute == null)
            {
                Debug.Assert(attribute != null, "Assembly is missing a description.");
                return defaultValue;
            }

            return attribute.Description;
        }

        /// <summary>
        /// Gets a string that indicates whether the specified assembly is built for x64 or
        /// x86. If AnyCPU is specified it returns the running processes architecture.
        /// </summary>
        /// <param name="assembly">
        /// The assembly to retrieve the information from.
        /// </param>
        /// <returns>
        /// The architecture edition of the specified assembly.
        /// </returns>
        public static string GetEdition(Assembly assembly)
        {
            if (assembly == null)
            {
                throw new SmartArgumentNullException(() => assembly);
            }

            ProcessorArchitecture architecture = assembly.GetName().ProcessorArchitecture;
            switch (architecture)
            {
                case ProcessorArchitecture.Amd64:
                case ProcessorArchitecture.IA64:
                    return "x64 Edition";
                case ProcessorArchitecture.MSIL:
                    if (Environment.Is64BitProcess)
                    {
                        return "x64 Edition";
                    }
                    else
                    {
                        return "x86 Edition";
                    }

                default:
                    return "x86 Edition";
            }
        }

        /// <summary>
        /// Gets the help link attribute value for the specified assembly or null if the
        /// attribute is missing.
        /// </summary>
        /// <param name="assembly">
        /// The assembly to retrieve the information from.
        /// </param>
        /// <returns>
        /// The description for the specified assembly.
        /// </returns>
        public static string GetHelpLink(Assembly assembly)
        {
            if (assembly == null)
            {
                throw new SmartArgumentNullException(() => assembly);
            }

            var attribute = assembly.GetCustomAttribute<AssemblyHelpLinkAttribute>();
            if (attribute == null)
            {
                Debug.Assert(attribute != null, "Assembly is missing a help link.");
                return null;
            }

            return attribute.HelpLink;
        }

        /// <summary>
        /// Gets the product name attribute value for the specified assembly or the specified
        /// default value if the attribute is missing.
        /// </summary>
        /// <param name="assembly">
        /// The assembly to retrieve the information from.
        /// </param>
        /// <param name="defaultValue">
        /// The value returned if the specified assembly doesn't have the correct attribute.
        /// </param>
        /// <returns>
        /// The product name of the specified assembly.
        /// </returns>
        public static string GetProductName(Assembly assembly, string defaultValue)
        {
            if (assembly == null)
            {
                throw new SmartArgumentNullException(() => assembly);
            }

            var attribute = assembly.GetCustomAttribute<AssemblyProductAttribute>();
            if (attribute == null)
            {
                Debug.Assert(attribute != null, "Assembly is missing product name.");
                return defaultValue;
            }

            return attribute.Product;
        }

        /// <summary>
        /// Gets the version number from the specified assembly.
        /// </summary>
        /// <param name="assembly">
        /// The assembly to retrieve the information from.
        /// </param>
        /// <returns>
        /// The version of the specified assembly.
        /// </returns>
        public static Version GetVersion(Assembly assembly)
        {
            if (assembly == null)
            {
                throw new SmartArgumentNullException(() => assembly);
            }

            return assembly.GetName().Version;
        }

        /// <summary>
        /// Gets a value indicating whether the specified assembly was built in debug.
        /// </summary>
        /// <param name="assembly">
        /// The assembly to retrieve the information from.
        /// </param>
        /// <returns>
        /// True if then specified assembly was built in debug; otherwise, false.
        /// </returns>
        public static bool IsAssemblyDebugBuild(Assembly assembly)
        {
            if (assembly == null)
            {
                throw new SmartArgumentNullException(() => assembly);
            }

            var attribute = assembly.GetCustomAttribute<DebuggableAttribute>();
            if (attribute != null)
            {
                return attribute.IsJITTrackingEnabled;
            }

            return false;
        }
        #endregion Methods

        #region Structures
        /// <summary>
        /// Represents the COFF header format.
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        private struct ImageFileHeader
        {
            #region Properties
            /// <summary>
            /// Gets or sets the architecture type of the computer.
            /// </summary>
            public ushort Machine
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the number of sections. This indicates the size of the section
            /// table, which immediately follows the headers.
            /// </summary>
            public ushort NumberOfSections
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the low 32 bits of the time stamp of the image. This represents
            /// the date and time the image was created by the linker. The value is represented
            /// in the number of seconds elapsed since midnight (00:00:00), January 1, 1970,
            /// Universal Coordinated Time, according to the system clock.
            /// </summary>
            public uint TimeDateStamp
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the offset of the symbol table, in bytes, or zero if no COFF
            /// symbol table exists.
            /// </summary>
            public uint PointerToSymbolTable
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the number of symbols in the symbol table.
            /// </summary>
            public uint NumberOfSymbols
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the size of the optional header, in bytes.
            /// </summary>
            public ushort SizeOfOptionalHeader
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the characteristics of the image.
            /// </summary>
            public ushort Characteristics
            {
                get;
                set;
            }
            #endregion Properties
        } // RSG.Editor.Controls.Helpers.AssemblyInfo.ImageFileHeader {Struct}
        #endregion Structures
    } // RSG.Editor.Controls.Helpers.AssemblyInfo {Class}
} // RSG.Editor.Controls.Helpers {Namespace}
