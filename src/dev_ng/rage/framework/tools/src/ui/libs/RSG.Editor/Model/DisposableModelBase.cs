﻿//---------------------------------------------------------------------------------------------
// <copyright file="DisposableModelBase.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Model
{
    using System;

    /// <summary>
    /// Provides a abstract class that can be inherited by classes that wish to implement the
    /// <see cref="RSG.Editor.Model.IDisposableModel"/> interface.
    /// </summary>
    public abstract class DisposableModelBase : ModelBase
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="IsDisposed"/> property.
        /// </summary>
        private bool _isDisposed;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DisposableModelBase"/> class.
        /// </summary>
        protected DisposableModelBase()
            : base()
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="DisposableModelBase"/> class.
        /// </summary>
        /// <param name="parent">
        /// A reference to the parent model used for this model.
        /// </param>
        protected DisposableModelBase(IModel parent)
            : base(parent)
        {
        }

        /// <summary>
        /// Finalises an instance of the <see cref="DisposableModelBase"/> class.
        /// </summary>
        ~DisposableModelBase()
        {
            this.Dispose(false);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether this instance has been disposed of.
        /// </summary>
        public bool IsDisposed
        {
            get { return this._isDisposed; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or
        /// resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// When overridden disposes of the managed resources.
        /// </summary>
        protected virtual void DisposeManagedResources()
        {
        }

        /// <summary>
        /// When overridden disposes of the unmanaged resources.
        /// </summary>
        protected virtual void DisposeNativeResources()
        {
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or
        /// resetting unmanaged resources.
        /// </summary>
        /// <param name="disposeManaged">
        /// If true the managed resources for this instance also get disposed of as well as
        /// the unmanaged resources.
        /// </param>
        private void Dispose(bool disposeManaged)
        {
            if (!this.IsDisposed)
            {
                try
                {
                    if (disposeManaged)
                    {
                        this.DisposeManagedResources();
                    }

                    this.DisposeNativeResources();
                }
                finally
                {
                    this._isDisposed = true;
                }
            }
        }
        #endregion Methods
    } // RSG.Editor.Model.DisposableModelBase {Class}
} // RSG.Editor.Model {Namespace}
