﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsRgbaColourSlider.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Data;
    using System.Windows.Media;
    using System.Windows.Threading;
    using RSG.Editor.Controls.Helpers;

    /// <summary>
    /// Represents a control that displays a slider and spinner that can control a single
    /// component of a base colour.
    /// </summary>
    public class RsRgbaColourSlider : Control, IColourSlider
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="Brush" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty BrushProperty;

        /// <summary>
        /// Identifies the <see cref="Colour" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty ColourProperty;

        /// <summary>
        /// Identifies the <see cref="ColourUpdateMode" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty ColourUpdateModeProperty;

        /// <summary>
        /// Identifies the <see cref="Component" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty ComponentProperty;

        /// <summary>
        /// Identifies the <see cref="Maximum" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty MaximumProperty;

        /// <summary>
        /// Identifies the <see cref="Minimum" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty MinimumProperty;

        /// <summary>
        /// Identifies the <see cref="InternalColour" /> dependency property.
        /// </summary>
        private static readonly DependencyProperty _internalColourProperty;

        /// <summary>
        /// Identifies the <see cref="Brush" /> dependency property.
        /// </summary>
        private static DependencyPropertyKey _brushPropertyKey;

        /// <summary>
        /// A value indicating whether the background brush for this control needs to be
        /// updated when the colour changes. It only ever needs to be updated if the colour
        /// changes from a external source.
        /// </summary>
        private bool _dontUpdateBrushOnColourChanged;

        /// <summary>
        /// A value indicating whether the value needs to be updated when the colour changes.
        /// </summary>
        private bool _dontUpdateValueOnColourChanged;

        /// <summary>
        /// The private reference to the PART_Slider control that has been setup in the
        /// controls template.
        /// </summary>
        private Slider _slider;

        /// <summary>
        /// The private reference to the PART_Spinner control that has been setup in the
        /// controls template.
        /// </summary>
        private RsIntegerSpinner _spinner;

        /// <summary>
        /// The dispatcher timer used to delay the colour update by a certain time span when
        /// the <see cref="ColourUpdateMode"/> is set to Delay.
        /// </summary>
        private DispatcherTimer _timer;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsRgbaColourSlider" /> class.
        /// </summary>
        static RsRgbaColourSlider()
        {
            _brushPropertyKey =
                DependencyProperty.RegisterReadOnly(
                "Brush",
                typeof(Brush),
                typeof(RsRgbaColourSlider),
                new PropertyMetadata(Brushes.Transparent));

            BrushProperty = _brushPropertyKey.DependencyProperty;

            ColourProperty =
                DependencyProperty.Register(
                "Colour",
                typeof(Color),
                typeof(RsRgbaColourSlider),
                new FrameworkPropertyMetadata(Colors.Transparent, OnColourChanged));

            ComponentProperty =
                DependencyProperty.Register(
                "Component",
                typeof(RgbaColourSliderComponent),
                typeof(RsRgbaColourSlider),
                new FrameworkPropertyMetadata(
                    RgbaColourSliderComponent.None, OnComponentChanged));

            ColourUpdateModeProperty =
                DependencyProperty.Register(
                "ColourUpdateMode",
                typeof(ColourUpdateMode),
                typeof(RsRgbaColourSlider),
                new FrameworkPropertyMetadata(ColourUpdateMode.Realtime));

            MaximumProperty =
                DependencyProperty.Register(
                "Maximum",
                typeof(int),
                typeof(RsRgbaColourSlider),
                new PropertyMetadata(255, null, CoerceRangeValue));

            MinimumProperty =
                DependencyProperty.Register(
                "Minimum",
                typeof(int),
                typeof(RsRgbaColourSlider),
                new PropertyMetadata(0, null, CoerceRangeValue));

            _internalColourProperty =
                DependencyProperty.Register(
                "InternalColour",
                typeof(Color),
                typeof(RsRgbaColourSlider),
                new FrameworkPropertyMetadata(
                    Colors.Transparent,
                    OnInternalColourChanged));

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsRgbaColourSlider),
                new FrameworkPropertyMetadata(typeof(RsRgbaColourSlider)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsRgbaColourSlider" /> class.
        /// </summary>
        public RsRgbaColourSlider()
        {
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs as soon as the drag operation has finished.
        /// </summary>
        public event EventHandler DragFinished;
        #endregion Events

        #region Properties
        /// <summary>
        /// Gets the brush that is used for the slider background of the control.
        /// </summary>
        public Brush Brush
        {
            get { return (Brush)this.GetValue(BrushProperty); }
            private set { this.SetValue(_brushPropertyKey, value); }
        }

        /// <summary>
        /// Gets or sets the colour that is being manipulated. This controls the colour range
        /// of the control.
        /// </summary>
        public Color Colour
        {
            get { return (Color)this.GetValue(ColourProperty); }
            set { this.SetValue(ColourProperty, value); }
        }

        /// <summary>
        /// Gets or sets the colour update mode that has been set on this control. This
        /// controls when and how often the colour gets updated from the user actions.
        /// </summary>
        public ColourUpdateMode ColourUpdateMode
        {
            get { return (ColourUpdateMode)this.GetValue(ColourUpdateModeProperty); }
            set { this.SetValue(ColourUpdateModeProperty, value); }
        }

        /// <summary>
        /// Gets or sets the colour component that this control will manipulate.
        /// </summary>
        public RgbaColourSliderComponent Component
        {
            get { return (RgbaColourSliderComponent)this.GetValue(ComponentProperty); }
            set { this.SetValue(ComponentProperty, value); }
        }

        /// <summary>
        /// Gets or sets the maximum range value for the slider and spinner.
        /// </summary>
        public int Maximum
        {
            get { return (int)this.GetValue(MaximumProperty); }
            set { this.SetValue(MaximumProperty, value); }
        }

        /// <summary>
        /// Gets or sets the minimum range value for the slider and spinner.
        /// </summary>
        public int Minimum
        {
            get { return (int)this.GetValue(MinimumProperty); }
            set { this.SetValue(MinimumProperty, value); }
        }

        /// <summary>
        /// Gets or sets the internal selected colour. This always gets updated in real time
        /// no matter what the <see cref="ColourUpdateMode"/> property has been set to.
        /// </summary>
        private Color InternalColour
        {
            get { return (Color)this.GetValue(_internalColourProperty); }
            set { this.SetValue(_internalColourProperty, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Clears the binding on the colour dependency property.
        /// </summary>
        public void ClearColourBinding()
        {
            BindingOperations.ClearBinding(this, RsRgbaColourSlider.ColourProperty);
        }

        /// <summary>
        /// Gets called whenever the template is applied to this control. This makes sure we
        /// have references to the selector and drag area objects in the template as well as
        /// handle all of the events for them.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            if (this._slider != null)
            {
                this._slider.ValueChanged -= this.OnValueChanged;
                this._slider.RemoveHandler(
                    Thumb.DragCompletedEvent,
                    new RoutedEventHandler(this.OnDragCompletedEvent));
            }

            this._slider = GetTemplateChild("PART_Slider") as Slider;
            if (this._slider != null)
            {
                this._slider.ValueChanged += this.OnValueChanged;
                this._slider.AddHandler(
                    Thumb.DragCompletedEvent,
                    new RoutedEventHandler(this.OnDragCompletedEvent));
            }

            this._spinner = GetTemplateChild("PART_Spinner") as RsIntegerSpinner;
            this.UpdateValue();
        }

        /// <summary>
        /// Set the colour dependency property to the specified binding object.
        /// </summary>
        /// <param name="binding">
        /// The binding object to set on the colour dependency property.
        /// </param>
        public void SetColourBinding(Binding binding)
        {
            this.SetBinding(RsRgbaColourSlider.ColourProperty, binding);
        }

        /// <summary>
        /// Called whenever the <see cref="Maximum"/> dependency property needs to be
        /// re-evaluated.
        /// </summary>
        /// <param name="s">
        /// The <see cref="RsRgbaColourSlider"/> instance whose dependency property needs
        /// evaluated.
        /// </param>
        /// <param name="baseValue">
        /// The new value of the property, prior to any coercion attempt.
        /// </param>
        /// <returns>
        /// The coerced value.
        /// </returns>
        private static object CoerceRangeValue(DependencyObject s, object baseValue)
        {
            if (baseValue is int)
            {
                int value = (int)baseValue;
                if (value < 0)
                {
                    return 0;
                }

                if (value > 255)
                {
                    return 255;
                }

                return baseValue;
            }

            return baseValue;
        }

        /// <summary>
        /// Called whenever the <see cref="Colour"/> dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The object whose <see cref="Colour"/> dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnColourChanged(object s, DependencyPropertyChangedEventArgs e)
        {
            RsRgbaColourSlider control = s as RsRgbaColourSlider;
            if (control == null)
            {
                Debug.Assert(false, "Incorrect dependency property changed cast.");
                return;
            }

            if (!control._dontUpdateValueOnColourChanged)
            {
                control.UpdateValue();
            }

            if (!control._dontUpdateBrushOnColourChanged)
            {
                control.UpdateBrush();
            }
        }

        /// <summary>
        /// Called whenever the <see cref="Component"/> dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The object whose <see cref="Component"/> dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnComponentChanged(object s, DependencyPropertyChangedEventArgs e)
        {
            RsRgbaColourSlider control = s as RsRgbaColourSlider;
            if (control == null)
            {
                Debug.Assert(false, "Incorrect dependency property changed cast.");
                return;
            }

            control.UpdateBrush();
            control.UpdateValue();
        }

        /// <summary>
        /// Called whenever the <see cref="InternalColour"/> dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The object whose <see cref="InternalColour"/> dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnInternalColourChanged(
            object s, DependencyPropertyChangedEventArgs e)
        {
            RsRgbaColourSlider slider = s as RsRgbaColourSlider;
            if (slider == null)
            {
                Debug.Assert(false, "Incorrect dependency property changed cast.");
                return;
            }

            if (slider._timer != null)
            {
                slider._timer.Stop();
                slider._timer = null;
            }

            if (slider.ColourUpdateMode == ColourUpdateMode.Realtime)
            {
                slider.SetCurrentValue(RsRgbaColourSlider.ColourProperty, e.NewValue);
            }
            else if (slider.ColourUpdateMode == ColourUpdateMode.Delay)
            {
                slider._timer = new DispatcherTimer();
                slider._timer.Interval = TimeSpan.FromMilliseconds(500);
                slider._timer.Tick += slider.OnTimerTick;
                slider._timer.Start();
            }
        }

        /// <summary>
        /// Called when the dragging event on the slider has finished. This is used to update
        /// the actual colour from the internal one if the <see cref="ColourUpdateMode"/> has
        /// been set to OnDragFinished.
        /// </summary>
        /// <param name="s">
        /// The object that sent the event.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private void OnDragCompletedEvent(object s, RoutedEventArgs e)
        {
            EventHandler handler = this.DragFinished;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }

            if (this.ColourUpdateMode == ColourUpdateMode.OnDragFinished)
            {
                this._dontUpdateValueOnColourChanged = true;
                this._dontUpdateBrushOnColourChanged = true;
                try
                {
                    this.SetCurrentValue(
                        RsRgbaColourSlider.ColourProperty, this.InternalColour);
                }
                finally
                {
                    this._dontUpdateValueOnColourChanged = false;
                    this._dontUpdateBrushOnColourChanged = false;
                }
            }
        }

        /// <summary>
        /// Called after the update timer on the selected colour expires so that the update
        /// from internal colour to selected colour can take place.
        /// </summary>
        /// <param name="sender">
        /// The object that fired the event.
        /// </param>
        /// <param name="e">
        /// The System.EventArgs containing the event data.
        /// </param>
        private void OnTimerTick(object sender, EventArgs e)
        {
            this._timer.Stop();
            this._timer = null;
            this._dontUpdateValueOnColourChanged = true;
            this._dontUpdateBrushOnColourChanged = true;
            try
            {
                this.SetCurrentValue(
                    RsRgbaColourSlider.ColourProperty, this.InternalColour);
            }
            finally
            {
                this._dontUpdateValueOnColourChanged = false;
                this._dontUpdateBrushOnColourChanged = false;
            }
        }

        /// <summary>
        /// Called whenever the value changes on the spinner and we need to update the internal
        /// colour based on the new value.
        /// </summary>
        /// <param name="sender">
        /// The object that sent the event.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedPropertyChangedEventArgs containing the event data.
        /// </param>
        private void OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            this.UpdateInternalColour();
        }

        /// <summary>
        /// Updates the brush property from the colour and component properties.
        /// </summary>
        private void UpdateBrush()
        {
            Color colour = this.Colour;
            switch (this.Component)
            {
                case RgbaColourSliderComponent.Red:
                    {
                        Color startColour = Color.FromRgb(0, colour.G, colour.B);
                        Color endColour = Color.FromRgb(255, colour.G, colour.B);
                        LinearGradientBrush linearBrush = new LinearGradientBrush();
                        linearBrush.StartPoint = new Point(0.0d, 0.0d);
                        linearBrush.EndPoint = new Point(1.0d, 0.0d);
                        linearBrush.GradientStops.Add(new GradientStop(startColour, 0.0d));
                        linearBrush.GradientStops.Add(new GradientStop(endColour, 1.0f));
                        this.Brush = linearBrush;
                    }

                    break;

                case RgbaColourSliderComponent.Green:
                    {
                        Color startColour = Color.FromRgb(colour.R, 0, colour.B);
                        Color endColour = Color.FromRgb(colour.R, 255, colour.B);
                        LinearGradientBrush linearBrush = new LinearGradientBrush();
                        linearBrush.StartPoint = new Point(0.0d, 0.0d);
                        linearBrush.EndPoint = new Point(1.0d, 0.0d);
                        linearBrush.GradientStops.Add(new GradientStop(startColour, 0.0d));
                        linearBrush.GradientStops.Add(new GradientStop(endColour, 1.0f));
                        this.Brush = linearBrush;
                    }

                    break;

                case RgbaColourSliderComponent.Blue:
                    {
                        Color startColour = Color.FromRgb(colour.R, colour.G, 0);
                        Color endColour = Color.FromRgb(colour.R, colour.G, 255);
                        LinearGradientBrush linearBrush = new LinearGradientBrush();
                        linearBrush.StartPoint = new Point(0.0d, 0.0d);
                        linearBrush.EndPoint = new Point(1.0d, 0.0d);
                        linearBrush.GradientStops.Add(new GradientStop(startColour, 0.0d));
                        linearBrush.GradientStops.Add(new GradientStop(endColour, 1.0f));
                        this.Brush = linearBrush;
                    }

                    break;

                case RgbaColourSliderComponent.Alpha:
                    {
                        Color startColour = Color.FromArgb(0, colour.R, colour.G, colour.B);
                        Color endColour = Color.FromArgb(255, colour.R, colour.G, colour.B);
                        LinearGradientBrush linearBrush = new LinearGradientBrush();
                        linearBrush.StartPoint = new Point(0.0d, 0.0d);
                        linearBrush.EndPoint = new Point(1.0d, 0.0d);
                        linearBrush.GradientStops.Add(new GradientStop(startColour, 0.0d));
                        linearBrush.GradientStops.Add(new GradientStop(endColour, 1.0f));
                        this.Brush = linearBrush;
                    }

                    break;

                case RgbaColourSliderComponent.None:
                    this.Brush = Brushes.Transparent;
                    break;
            }
        }

        /// <summary>
        /// Updates the colour property based on the current value.
        /// </summary>
        private void UpdateInternalColour()
        {
            Color colour = this.Colour;
            byte value = (byte)this._spinner.Value;
            switch (this.Component)
            {
                case RgbaColourSliderComponent.Red:
                    colour = Color.FromArgb(colour.A, value, colour.G, colour.B);
                    break;

                case RgbaColourSliderComponent.Green:
                    colour = Color.FromArgb(colour.A, colour.R, value, colour.B);
                    break;

                case RgbaColourSliderComponent.Blue:
                    colour = Color.FromArgb(colour.A, colour.R, colour.G, value);
                    break;

                case RgbaColourSliderComponent.Alpha:
                    colour = Color.FromArgb(value, colour.R, colour.G, colour.B);
                    break;

                case RgbaColourSliderComponent.None:
                    colour = Colors.Transparent;
                    break;
            }

            this.InternalColour = colour;
        }

        /// <summary>
        /// Updates the value property based on the colour and component values.
        /// </summary>
        private void UpdateValue()
        {
            if (this._spinner == null)
            {
                return;
            }

            switch (this.Component)
            {
                case RgbaColourSliderComponent.Red:
                    this._spinner.Value = this.Colour.R;
                    break;

                case RgbaColourSliderComponent.Green:
                    this._spinner.Value = this.Colour.G;
                    break;

                case RgbaColourSliderComponent.Blue:
                    this._spinner.Value = this.Colour.B;
                    break;

                case RgbaColourSliderComponent.Alpha:
                    this._spinner.Value = this.Colour.A;
                    break;

                case RgbaColourSliderComponent.None:
                    this._spinner.Value = 0;
                    break;
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsRgbaColourSlider {Class}
} // RSG.Editor.Controls {Namespace}
