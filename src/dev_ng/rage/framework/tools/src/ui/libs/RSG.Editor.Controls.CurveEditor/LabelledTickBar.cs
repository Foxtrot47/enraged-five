﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace RSG.Editor.Controls.CurveEditor
{
    /// <summary>
    /// 
    /// </summary>
    public enum LabelPosition
    {
        Before,
        On,
        After
    }

    /// <summary>
    /// 
    /// </summary>
    public class LabelledTickBar : TickBar
    {
        #region Font Dependeny Properties
        public static readonly DependencyProperty FontFamilyProperty =
            DependencyProperty.Register("FontFamily", typeof(FontFamily), typeof(LabelledTickBar), new FrameworkPropertyMetadata(SystemFonts.MessageFontFamily, FrameworkPropertyMetadataOptions.AffectsRender));

        public FontFamily FontFamily
        {
            get { return (FontFamily)GetValue(FontFamilyProperty); }
            set { SetValue(FontFamilyProperty, value); }
        }

        public static readonly DependencyProperty FontSizeProperty =
            DependencyProperty.Register("FontSize", typeof(double), typeof(LabelledTickBar), new FrameworkPropertyMetadata(SystemFonts.MessageFontSize, FrameworkPropertyMetadataOptions.AffectsRender));

        public double FontSize
        {
            get { return (double)GetValue(FontSizeProperty); }
            set { SetValue(FontSizeProperty, value); }
        }

        public static readonly DependencyProperty FontStretchProperty =
            DependencyProperty.Register("FontStretch", typeof(FontStretch), typeof(LabelledTickBar), new FrameworkPropertyMetadata(FontStretches.Normal, FrameworkPropertyMetadataOptions.AffectsRender));

        public FontStretch FontStretch
        {
            get { return (FontStretch)GetValue(FontStretchProperty); }
            set { SetValue(FontStretchProperty, value); }
        }

        public static readonly DependencyProperty FontStyleProperty =
            DependencyProperty.Register("FontStyle", typeof(FontStyle), typeof(LabelledTickBar), new FrameworkPropertyMetadata(SystemFonts.MessageFontStyle, FrameworkPropertyMetadataOptions.AffectsRender));

        public FontStyle FontStyle
        {
            get { return (FontStyle)GetValue(FontStyleProperty); }
            set { SetValue(FontStyleProperty, value); }
        }

        public static readonly DependencyProperty FontWeightProperty =
            DependencyProperty.Register("FontWeight", typeof(FontWeight), typeof(LabelledTickBar), new FrameworkPropertyMetadata(SystemFonts.MessageFontWeight, FrameworkPropertyMetadataOptions.AffectsRender));

        public FontWeight FontWeight
        {
            get { return (FontWeight)GetValue(FontWeightProperty); }
            set { SetValue(FontWeightProperty, value); }
        }
        #endregion // Font Dependency Properties

        #region Label Dependency Proprties

        public static readonly DependencyProperty LabelPositionProperty =
            DependencyProperty.Register("LabelPosition", typeof(LabelPosition), typeof(LabelledTickBar), new FrameworkPropertyMetadata(LabelPosition.On, FrameworkPropertyMetadataOptions.AffectsRender));

        public LabelPosition LabelPosition
        {
            get { return (LabelPosition)GetValue(LabelPositionProperty); }
            set { SetValue(LabelPositionProperty, value); }
        }
        #endregion // Label Dependency Properties

        #region OnRender Override
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dc"></param>
        protected override void OnRender(DrawingContext dc)
        {
            Size size = new Size(base.ActualWidth, base.ActualHeight);
            int tickCount = (int)((this.Maximum - this.Minimum) / this.TickFrequency) + 1;
            if ((this.Maximum - this.Minimum) % this.TickFrequency == 0)
            {
                tickCount -= 1;
            }
            Double tickFrequencySize;

            // Calculate tick's setting
            //width to height
            if (Placement == TickBarPlacement.Top || Placement == TickBarPlacement.Bottom)
                tickFrequencySize = (size.Width * this.TickFrequency / (this.Maximum - this.Minimum));
            else
                tickFrequencySize = (size.Height * this.TickFrequency / (this.Maximum - this.Minimum));

            // Draw each tick text
            for (int i = 0; i <= tickCount; i++)
            {
                String text = Convert.ToString(Convert.ToInt32(this.Minimum + this.TickFrequency * i), 10);
                FormattedText formattedText = new FormattedText(text, CultureInfo.InvariantCulture, FlowDirection.LeftToRight, new Typeface(FontFamily, FontStyle, FontWeight, FontStretch), FontSize, Brushes.Black);

                double tickOffset = tickFrequencySize * i;
                if (IsDirectionReversed)
                {
                    if (Placement == TickBarPlacement.Top || Placement == TickBarPlacement.Bottom)
                        tickOffset = size.Width - tickOffset;
                    else
                        tickOffset = size.Height - tickOffset;
                }

                dc.DrawText(formattedText, GetTextPoint(tickOffset, formattedText.Width, formattedText.Height));
                dc.DrawLine(new Pen(Fill, 1.0), GetLineStart(tickOffset, formattedText.Width, formattedText.Height), GetLineEnd(tickOffset, formattedText.Width, formattedText.Height));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tickOffset"></param>
        /// <param name="textWidth"></param>
        /// <param name="textHeight"></param>
        /// <returns></returns>
        private Point GetTextPoint(double tickOffset, double textWidth, double textHeight)
        {
            // Calculate the text's X position.
            double textX = 0.0;
            if (Placement == TickBarPlacement.Top || Placement == TickBarPlacement.Bottom)
            {
                if (LabelPosition == LabelPosition.On)
                    textX = tickOffset - (textWidth / 2.0);
                else if (LabelPosition == LabelPosition.Before)
                    textX = tickOffset - textWidth;
                else if (LabelPosition == LabelPosition.After)
                    textX = tickOffset;
            }
            else if (Placement == TickBarPlacement.Right)
            {
                textX = Width - textWidth;
            }

            // Calculate the text's Y position.
            double textY = 0.0;
            if (Placement == TickBarPlacement.Bottom)
            {
                textY = Height - textHeight;
            }
            else if (Placement == TickBarPlacement.Left || Placement == TickBarPlacement.Right)
            {
                if (LabelPosition == LabelPosition.On)
                    textY = tickOffset - (textHeight / 2.0);
                else if (LabelPosition == LabelPosition.Before)
                    textY = tickOffset - textHeight;
                else if (LabelPosition == LabelPosition.After)
                    textY = tickOffset;
            }

            return new Point(RoundToPoint5(textX), RoundToPoint5(textY));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tickOffset"></param>
        /// <param name="textWidth"></param>
        /// <param name="textHeight"></param>
        /// <returns></returns>
        private Point GetLineStart(double tickOffset, double textWidth, double textHeight)
        {
            double startX = 0.0;
            if (Placement == TickBarPlacement.Top || Placement == TickBarPlacement.Bottom)
            {
                startX = tickOffset;
            }
            else if (Placement == TickBarPlacement.Left && LabelPosition == LabelPosition.On)
            {
                startX = textWidth;
            }

            double startY = 0.0;
            if (Placement == TickBarPlacement.Top && LabelPosition == LabelPosition.On)
            {
                startY = textHeight;
            }
            else if (Placement == TickBarPlacement.Left || Placement == TickBarPlacement.Right)
            {
                startY = tickOffset;
            }

            return new Point(RoundToPoint5(startX), RoundToPoint5(startY));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tickOffset"></param>
        /// <param name="textWidth"></param>
        /// <param name="textHeight"></param>
        /// <returns></returns>
        private Point GetLineEnd(double tickOffset, double textWidth, double textHeight)
        {
            double endX = 0.0;
            if (Placement == TickBarPlacement.Top || Placement == TickBarPlacement.Bottom)
            {
                endX = tickOffset;
            }
            else if (Placement == TickBarPlacement.Left)
            {
                endX = Width;
            }
            else if (Placement == TickBarPlacement.Right)
            {
                if (LabelPosition == LabelPosition.On)
                    endX = Width - textWidth;
                else
                    endX = Width;
            }

            double endY = 0.0;
            if (Placement == TickBarPlacement.Top)
            {
                endY = Height;
            }
            else if (Placement == TickBarPlacement.Bottom)
            {
                if (LabelPosition == LabelPosition.On)
                    endY = Height - textHeight;
                else
                    endY = Height;
            }
            else if (Placement == TickBarPlacement.Left || Placement == TickBarPlacement.Right)
            {
                endY = tickOffset;
            }

            return new Point(RoundToPoint5(endX), RoundToPoint5(endY));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private double RoundToPoint5(double value)
        {
            return Math.Round(value - 0.5, MidpointRounding.AwayFromZero) + 0.5;
        }
        #endregion // OnRender Override
    }
}
