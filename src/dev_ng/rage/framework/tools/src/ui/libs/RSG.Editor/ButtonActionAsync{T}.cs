﻿//---------------------------------------------------------------------------------------------
// <copyright file="ButtonActionAsync{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    /// <summary>
    /// Provides a abstract base class to any class that is used to implement a button command.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the parameter pass into the can execute and execute methods.
    /// </typeparam>
    public abstract class ButtonActionAsync<T> : CommandActionAsync<T>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ButtonActionAsync{T}"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        protected ButtonActionAsync(ParameterResolverDelegate<T> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Create a new command definition for each command associated with this
        /// implementation.
        /// </summary>
        /// <param name="command">
        /// The command to create the definition for.
        /// </param>
        /// <returns>
        /// The new command definition created for the specified command.
        /// </returns>
        internal override CommandDefinition CreateDefinition(RockstarRoutedCommand command)
        {
            ButtonCommand definition = new ButtonCommand(command);
            return definition;
        }
        #endregion Methods
    }  // RSG.Editor.ButtonActionAsync{T} {Class}
} // RSG.Editor {Namespace}
