﻿//---------------------------------------------------------------------------------------------
// <copyright file="MapTunableCollection.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Tunables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Windows.Data;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Editor.View;
    using RSG.Metadata.Model.Definitions;
    using RSG.Metadata.Model.Definitions.Members;
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// A collection class used by hierarchical tunable view models to construct its child
    /// collection.
    /// </summary>
    public class MapTunableCollection : TunableCollection
    {
        #region Fields
        /// <summary>
        /// The private reference to the map tunable view model that owns this collection.
        /// </summary>
        private MapTunableViewModel _mapTunableViewModel;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MapTunableCollection"/> class.
        /// </summary>
        /// <param name="source">
        /// The source model items for this collection.
        /// </param>
        /// <param name="mapTunableViewModel">
        /// The map tunable view model that owns this collection.
        /// </param>
        public MapTunableCollection(
            IEnumerable<ITunable> source, MapTunableViewModel mapTunableViewModel)
            : base(mapTunableViewModel)
        {
            this._mapTunableViewModel = mapTunableViewModel;
            this.ResetSourceItems(source);
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Creates a tunable view model from the specified tunable model object.
        /// </summary>
        /// <param name="tunable">
        /// The tunable used to construct the view model.
        /// </param>
        /// <returns>
        /// A new tunable view model constructed from the specified tunable model object.
        /// </returns>
        protected override ITunableViewModel ConvertItem(ITunable tunable)
        {
            ITunableViewModel tunableViewModel = base.ConvertItem(tunable);
            if (tunableViewModel == null || this._mapTunableViewModel == null)
            {
                return tunableViewModel;
            }

            if (this._mapTunableViewModel.Model == null)
            {
                return tunableViewModel;
            }

            MapTunableViewModel mapTunable = this._mapTunableViewModel;
            KeyLookupItem model = mapTunable.Model.GetTunableKeyMappingModel(tunable);
            if (model == null)
            {
                return tunableViewModel;
            }

            MapMember mapMember = mapTunable.Model.MapMember;
            MapKeyViewModel mapKeyViewModel = null;
            switch (mapMember.Key)
            {
                case KeyType.AtHashString:
                case KeyType.AtHashValue:
                    {
                        StringMapKey key = new StringMapKey(mapTunable, tunable, model);
                        mapKeyViewModel = key;
                    }

                    break;

                case KeyType.Signed32:
                    {
                        NumericalMapKey key = new NumericalMapKey(mapTunable, tunable, model);
                        key.Maximum = Int32.MaxValue;
                        key.Minimum = Int32.MinValue;
                        mapKeyViewModel = key;
                    }

                    break;

                case KeyType.Signed16:
                    {
                        NumericalMapKey key = new NumericalMapKey(mapTunable, tunable, model);
                        key.Maximum = Int16.MaxValue;
                        key.Minimum = Int16.MinValue;
                        mapKeyViewModel = key;
                    }

                    break;

                case KeyType.Signed8:
                    {
                        NumericalMapKey key = new NumericalMapKey(mapTunable, tunable, model);
                        key.Maximum = SByte.MaxValue;
                        key.Minimum = SByte.MinValue;
                        mapKeyViewModel = key;
                    }

                    break;

                case KeyType.Unsigned32:
                    {
                        NumericalMapKey key = new NumericalMapKey(mapTunable, tunable, model);
                        key.Maximum = Int32.MaxValue;
                        key.Minimum = Int32.MinValue;
                        mapKeyViewModel = key;
                    }

                    break;

                case KeyType.Unsigned16:
                    {
                        NumericalMapKey key = new NumericalMapKey(mapTunable, tunable, model);
                        key.Maximum = UInt16.MaxValue;
                        key.Minimum = UInt16.MinValue;
                        mapKeyViewModel = key;
                    }

                    break;

                case KeyType.Unsigned8:
                    {
                        NumericalMapKey key = new NumericalMapKey(mapTunable, tunable, model);
                        key.Maximum = Byte.MaxValue;
                        key.Minimum = Byte.MinValue;
                        mapKeyViewModel = key;
                    }

                    break;

                case KeyType.Enum:
                    {
                        EnumMapKey key = new EnumMapKey(mapTunable, tunable, model);
                        mapKeyViewModel = key;
                    }

                    break;
            }

            tunableViewModel.MapKey = mapKeyViewModel;
            return tunableViewModel;
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// Represents a map key of type s8, s16, s32, u8, u16, or u32.
        /// </summary>
        private class NumericalMapKey : MapKeyViewModel
        {
            #region Fields
            /// <summary>
            /// The private field used for the <see cref="Maximum"/> property.
            /// </summary>
            private long _maximum;

            /// <summary>
            /// The private field used for the <see cref="Minimum"/> property.
            /// </summary>
            private long _minimum;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="NumericalMapKey"/> class.
            /// </summary>
            /// <param name="mapTunable">
            /// The map tunable whose key lookup is tied to this view model class.
            /// </param>
            /// <param name="item">
            /// The tunable item whose map key this view model represents.
            /// </param>
            /// <param name="model">
            /// The mapping model between the map item tunable and the map key.
            /// </param>
            public NumericalMapKey(
                MapTunableViewModel mapTunable, ITunable item, KeyLookupItem model)
                : base(mapTunable, item, model)
            {
                long value = 0;
                if (!long.TryParse(this.Model.Value, out value))
                {
                    using (this.SuspendValidation())
                    {
                        this.Model.Value = "0";
                    }
                }
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets or sets the maximum value the map key can have.
            /// </summary>
            public long Maximum
            {
                get { return this._maximum; }
                set { this.SetProperty(ref this._maximum, value); }
            }

            /// <summary>
            /// Gets or sets the minimum value the map key can have.
            /// </summary>
            public long Minimum
            {
                get { return this._minimum; }
                set { this.SetProperty(ref this._minimum, value); }
            }

            /// <summary>
            /// Gets or sets the value this map key currently has.
            /// </summary>
            public long Value
            {
                get
                {
                    long value = 0;
                    if (!long.TryParse(this.Model.Value, out value))
                    {
                        this.Model.Value = "0";
                        value = 0;
                    }

                    return value;
                }

                set
                {
                    this.Model.Value = value.ToString();
                }
            }
            #endregion Properties
        } // MapTunableCollection.NumericalMapKey {Class}

        /// <summary>
        /// Represents a map key of type enum.
        /// </summary>
        private class EnumMapKey : MapKeyViewModel
        {
            #region Fields
            /// <summary>
            /// The private field used for the <see cref="Items"/> property.
            /// </summary>
            private List<object> _items;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="EnumMapKey"/> class.
            /// </summary>
            /// <param name="mapTunable">
            /// The map tunable whose key lookup is tied to this view model class.
            /// </param>
            /// <param name="mapItem">
            /// The tunable item whose map key this view model represents.
            /// </param>
            /// <param name="model">
            /// The mapping model between the map item tunable and the map key.
            /// </param>
            public EnumMapKey(
                MapTunableViewModel mapTunable, ITunable mapItem, KeyLookupItem model)
                : base(mapTunable, mapItem, model)
            {
                this._items = new List<object>();
                IEnumeration source = mapTunable.Model.MapMember.EnumKey;
                string selection = mapTunable.Model[mapItem];
                bool foundSelection = false;
                if (source != null)
                {
                    foreach (IEnumConstant constant in source.EnumConstants)
                    {
                        bool selected = false;
                        if (!foundSelection && String.Equals(constant.Name, selection))
                        {
                            foundSelection = true;
                            selected = true;
                        }

                        this._items.Add(new EnumItemViewModel(constant.Name, selected, this));
                    }
                }

                ICollectionView collectionView = CollectionViewSource.GetDefaultView(
                    this._items);
                collectionView.SortDescriptions.Add(
                    new SortDescription("Name", ListSortDirection.Ascending));
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets the enum items that represent the individual values in the key.
            /// </summary>
            public List<object> Items
            {
                get { return this._items; }
            }

            /// <summary>
            /// Gets or sets the value currently assigned to the enum key.
            /// </summary>
            public object Value
            {
                get
                {
                    foreach (EnumItemViewModel item in this._items)
                    {
                        if (String.Equals(item.Name, this.Model.Value))
                        {
                            return item;
                        }
                    }

                    return null;
                }

                set
                {
                    this.Model.Value = ((EnumItemViewModel)value).Name;
                }
            }
            #endregion Properties

            #region Classes
            /// <summary>
            /// Represents a single enum value inside a enum control.
            /// </summary>
            private class EnumItemViewModel : NotifyPropertyChangedBase
            {
                #region Fields
                /// <summary>
                /// The private field used for the <see cref="IsSelected"/> property.
                /// </summary>
                private bool _isSelected;

                /// <summary>
                /// The private field used for the <see cref="Name"/> property.
                /// </summary>
                private string _name;

                /// <summary>
                /// The private reference to the enum map key object that created this
                /// instance.
                /// </summary>
                private EnumMapKey _parent;
                #endregion Fields

                #region Constructors
                /// <summary>
                /// Initialises a new instance of the <see cref="EnumItemViewModel"/> class.
                /// </summary>
                /// <param name="name">
                /// The text to display as content for this item.
                /// </param>
                /// <param name="isSelected">
                /// A value indicating the initial is checked state of the item.
                /// </param>
                /// <param name="parent">
                /// A reference to the enum map key object that created this instance.
                /// </param>
                public EnumItemViewModel(string name, bool isSelected, EnumMapKey parent)
                {
                    this._name = name;
                    this._isSelected = isSelected;
                    this._parent = parent;
                    if (this._isSelected)
                    {
                        this._parent.Value = this;
                    }
                }
                #endregion Constructors

                #region Properties
                /// <summary>
                /// Gets or sets a value indicating whether this enum value is currently the
                /// selected item or not.
                /// </summary>
                public bool IsSelected
                {
                    get
                    {
                        return this._isSelected;
                    }

                    set
                    {
                        if (this.SetProperty(ref this._isSelected, value))
                        {
                            if (value)
                            {
                                this._parent.Value = this;
                            }
                        }
                    }
                }

                /// <summary>
                /// Gets the text that is displayed for this enum item.
                /// </summary>
                public string Name
                {
                    get { return this._name; }
                }
                #endregion Properties
            } // EnumTunableViewModel.EnumItemViewModel {Class}
            #endregion Classes
        } // MapTunableCollection.EnumMapKey {Class}

        /// <summary>
        /// Represents a map key of type atHashString or atHashValue.
        /// </summary>
        private class StringMapKey : MapKeyViewModel
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="StringMapKey"/> class.
            /// </summary>
            /// <param name="mapTunable">
            /// The map tunable whose key lookup is tied to this view model class.
            /// </param>
            /// <param name="mapItem">
            /// The tunable item whose map key this view model represents.
            /// </param>
            /// <param name="model">
            /// The mapping model between the map item tunable and the map key.
            /// </param>
            public StringMapKey(
                MapTunableViewModel mapTunable, ITunable mapItem, KeyLookupItem model)
                : base(mapTunable, mapItem, model)
            {
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets or sets the value this map key currently has.
            /// </summary>
            public string Value
            {
                get { return this.Model.Value; }
                set { this.Model.Value = value; }
            }
            #endregion Properties
        } // MapTunableCollection.StringMapKey {Class}
        #endregion
    } // RSG.Metadata.ViewModel.Tunables.MapTunableCollection {Class}
} // RSG.Metadata.ViewModel.Tunables {Namespace}
