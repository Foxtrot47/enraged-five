﻿//---------------------------------------------------------------------------------------------
// <copyright file="VoiceCompositeViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.ViewModel
{
    using System.Collections.Generic;
    using System.Linq;
    using RSG.Text.Model;

    /// <summary>
    /// A composite class containing n-number of <see cref="DialogueVoiceViewModel"/> objects.
    /// This class cannot be inherited.
    /// </summary>
    public sealed class VoiceCompositeViewModel : CompositeViewModel
    {
        #region Fields
        /// <summary>
        /// The private collection containing the <see cref="DialogueVoiceViewModel"/> objects
        /// that are currently managed by this composite class.
        /// </summary>
        private List<DialogueVoiceViewModel> _items;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="VoiceCompositeViewModel"/> class.
        /// </summary>
        /// <param name="configurations">
        /// The <see cref="DialogueConfigurations"/> object that the configuration values have
        /// come from.
        /// </param>
        /// <param name="items">
        /// The collection of <see cref="DialogueVoiceViewModel"/> objects that this
        /// composite object should manage.
        /// </param>
        public VoiceCompositeViewModel(
            DialogueConfigurations configurations,
            IEnumerable<DialogueVoiceViewModel> items)
            : base(configurations)
        {
            this._items = new List<DialogueVoiceViewModel>(items);
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Deletes all of the values managed by this composite collection from the dialogue
        /// configuration object.
        /// </summary>
        public override void Delete()
        {
            List<DialogueVoice> delete = new List<DialogueVoice>();
            foreach (DialogueVoiceViewModel viewModel in this._items)
            {
                foreach (DialogueVoice voice in this.Model.Voices)
                {
                    if (viewModel.Id == voice.Id)
                    {
                        delete.Add(voice);
                    }
                }
            }

            foreach (DialogueVoice voice in delete)
            {
                this.Model.Voices.Remove(voice);
            }
        }
        #endregion Methods
    } // RSG.Text.ViewModel.VoiceCompositeViewModel {Class}
} // RSG.Text.ViewModel {Namespace}
