﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsHighlightTextBlock.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Text.RegularExpressions;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Media;

    /// <summary>
    /// Represents a text block control that can have a portion of it dynamically highlighted.
    /// </summary>
    [TemplatePart(Name = "PART_TextBlock", Type = typeof(TextBlock))]
    public class RsHighlightTextBlock : Control
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="HighlightBackground"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty HighlightBackgroundProperty;

        /// <summary>
        /// Identifies the <see cref="HighlightExpression"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty HighlightExpressionProperty;

        /// <summary>
        /// Identifies the <see cref="HighlightForeground"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty HighlightForegroundProperty;

        /// <summary>
        /// Identifies the <see cref="LineHeight" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty LineHeightProperty;

        /// <summary>
        /// Identifies the <see cref="SecondaryHighlightBackground"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty SecondaryHighlightBackgroundProperty;

        /// <summary>
        /// Identifies the <see cref="SecondaryHighlightForeground"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty SecondaryHighlightForegroundProperty;

        /// <summary>
        /// Identifies the <see cref="SecondaryHighlightIndex"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty SecondaryHighlightIndexProperty;

        /// <summary>
        /// Identifies the <see cref="TextAlignment" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty TextAlignmentProperty;

        /// <summary>
        /// Identifies the <see cref="Text"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty TextProperty;

        /// <summary>
        /// Identifies the <see cref="TextTrimming" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty TextTrimmingProperty;

        /// <summary>
        /// Identifies the <see cref="TextWrapping" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty TextWrappingProperty;

        /// <summary>
        /// The private field used for the <see cref="TextBlock"/> property.
        /// </summary>
        private TextBlock _textBlock;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsHighlightTextBlock"/> class.
        /// </summary>
        static RsHighlightTextBlock()
        {
            TextProperty =
                DependencyProperty.Register(
                "Text",
                typeof(string),
                typeof(RsHighlightTextBlock),
                new FrameworkPropertyMetadata(null, OnTextChanged));

            HighlightBackgroundProperty =
                DependencyProperty.Register(
                "HighlightBackground",
                typeof(Brush),
                typeof(RsHighlightTextBlock),
                new FrameworkPropertyMetadata(null, OnTextChanged));

            HighlightExpressionProperty =
                DependencyProperty.RegisterAttached(
                "HighlightExpression",
                typeof(Regex),
                typeof(RsHighlightTextBlock),
                new FrameworkPropertyMetadata(
                    null, FrameworkPropertyMetadataOptions.Inherits, OnTextChanged));

            HighlightForegroundProperty =
                DependencyProperty.Register(
                "HighlightForeground",
                typeof(Brush),
                typeof(RsHighlightTextBlock),
                new FrameworkPropertyMetadata(null));

            LineHeightProperty =
                Block.LineHeightProperty.AddOwner(typeof(RsHighlightTextBlock));

            SecondaryHighlightBackgroundProperty =
                DependencyProperty.Register(
                "SecondaryHighlightBackground",
                typeof(Brush),
                typeof(RsHighlightTextBlock),
                new FrameworkPropertyMetadata(null, OnTextChanged));

            SecondaryHighlightForegroundProperty =
                DependencyProperty.Register(
                "SecondaryHighlightForeground",
                typeof(Brush),
                typeof(RsHighlightTextBlock),
                new FrameworkPropertyMetadata(null, OnTextChanged));

            SecondaryHighlightIndexProperty =
                DependencyProperty.Register(
                "SecondaryHighlightIndex",
                typeof(int),
                typeof(RsHighlightTextBlock),
                new FrameworkPropertyMetadata(-1, OnTextChanged));

            TextAlignmentProperty =
                Block.TextAlignmentProperty.AddOwner(typeof(RsHighlightTextBlock));

            TextTrimmingProperty =
                TextBlock.TextTrimmingProperty.AddOwner(
                typeof(RsHighlightTextBlock),
                new FrameworkPropertyMetadata(TextTrimming.None));

            TextWrappingProperty =
                TextBlock.TextWrappingProperty.AddOwner(
                typeof(RsHighlightTextBlock),
                new FrameworkPropertyMetadata(TextWrapping.NoWrap));

            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsHighlightTextBlock),
                new FrameworkPropertyMetadata(typeof(RsHighlightTextBlock)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsHighlightTextBlock"/> class.
        /// </summary>
        public RsHighlightTextBlock()
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the brush used for the background of the highlighted part of the text.
        /// </summary>
        public Brush HighlightBackground
        {
            get { return (Brush)this.GetValue(HighlightBackgroundProperty); }
            set { this.SetValue(HighlightBackgroundProperty, value); }
        }

        /// <summary>
        /// Gets or sets the regular expression that defines the search pattern for the text
        /// to highlight.
        /// </summary>
        public Regex HighlightExpression
        {
            get { return GetHighlightExpression(this); }
            set { SetHighlightExpression(this, value); }
        }

        /// <summary>
        /// Gets or sets the brush used for the foreground of the highlighted part of the text.
        /// </summary>
        public Brush HighlightForeground
        {
            get { return (Brush)this.GetValue(HighlightForegroundProperty); }
            set { this.SetValue(HighlightForegroundProperty, value); }
        }

        /// <summary>
        /// Gets or sets the height of each line box.
        /// </summary>
        public double LineHeight
        {
            get { return (double)this.GetValue(LineHeightProperty); }
            set { this.SetValue(LineHeightProperty, value); }
        }

        /// <summary>
        /// Gets or sets the brush used for the background of the secondary highlighted part of
        /// the text.
        /// </summary>
        public Brush SecondaryHighlightBackground
        {
            get { return (Brush)this.GetValue(SecondaryHighlightBackgroundProperty); }
            set { this.SetValue(SecondaryHighlightBackgroundProperty, value); }
        }

        /// <summary>
        /// Gets or sets the brush used for the foreground of the secondary highlighted part of
        /// the text.
        /// </summary>
        public Brush SecondaryHighlightForeground
        {
            get { return (Brush)this.GetValue(SecondaryHighlightForegroundProperty); }
            set { this.SetValue(SecondaryHighlightForegroundProperty, value); }
        }

        /// <summary>
        /// Gets or sets the index of the current highlights that is considered the secondary
        /// highlight.
        /// </summary>
        public int SecondaryHighlightIndex
        {
            get { return (int)this.GetValue(SecondaryHighlightIndexProperty); }
            set { this.SetValue(SecondaryHighlightIndexProperty, value); }
        }

        /// <summary>
        /// Gets or sets the text that is displayed to the user.
        /// </summary>
        public string Text
        {
            get { return (string)this.GetValue(TextProperty); }
            set { this.SetValue(TextProperty, value); }
        }

        /// <summary>
        /// Gets or sets the horizontal alignment of the text content.
        /// </summary>
        public TextAlignment TextAlignment
        {
            get { return (TextAlignment)this.GetValue(TextAlignmentProperty); }
            set { this.SetValue(TextAlignmentProperty, value); }
        }

        /// <summary>
        /// Gets or sets how the textual content is clipped if it overflows the line box.
        /// </summary>
        public TextTrimming TextTrimming
        {
            get { return (TextTrimming)this.GetValue(TextTrimmingProperty); }
            set { this.SetValue(TextTrimmingProperty, value); }
        }

        /// <summary>
        /// Gets or sets whether the textual content is wrapped if it overflows the line box.
        /// </summary>
        public TextWrapping TextWrapping
        {
            get { return (TextWrapping)this.GetValue(TextWrappingProperty); }
            set { this.SetValue(TextWrappingProperty, value); }
        }

        /// <summary>
        /// Gets the reference to the text block that has been defined inside the template.
        /// </summary>
        protected TextBlock TextBlock
        {
            get { return this._textBlock; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Retrieves the the <see cref="HighlightExpressionProperty"/> value for the
        /// specified element.
        /// </summary>
        /// <param name="element">
        /// The element whose <see cref="HighlightExpressionProperty"/> value is being
        /// retrieved.
        /// </param>
        /// <returns>
        /// The value of the <see cref="HighlightExpressionProperty"/> for the specified
        /// element.
        /// </returns>
        public static Regex GetHighlightExpression(UIElement element)
        {
            if (element == null)
            {
                throw new SmartArgumentNullException(() => element);
            }

            return (Regex)element.GetValue(RsHighlightTextBlock.HighlightExpressionProperty);
        }

        /// <summary>
        /// Sets the <see cref="HighlightExpressionProperty"/> value on the specified element
        /// to the specified value.
        /// </summary>
        /// <param name="element">
        /// The element whose <see cref="HighlightExpressionProperty"/> value is being set.
        /// </param>
        /// <param name="value">
        /// The value to set.
        /// </param>
        public static void SetHighlightExpression(UIElement element, Regex value)
        {
            if (element == null)
            {
                throw new SmartArgumentNullException(() => element);
            }

            element.SetValue(RsHighlightTextBlock.HighlightExpressionProperty, value);
        }

        /// <summary>
        /// Gets called whenever the internal processes set the control template for
        /// this control.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            this._textBlock = this.GetTemplateChild("PART_TextBlock") as TextBlock;
            this.OnTextChanged();
        }

        /// <summary>
        /// Generates the in-lines for the text block based on the specified match collection.
        /// </summary>
        /// <param name="matches">
        /// The collection containing the matches that need to be highlighted.
        /// </param>
        protected void GenerateInlines(MatchCollection matches)
        {
            int startIndex = 0;
            for (int i = 0; i < matches.Count; i++)
            {
                Match match = matches[i];
                int index = match.Index;
                if (index > 0)
                {
                    Run run = new Run();
                    run.Text = this.Text.Substring(startIndex, index - startIndex);
                    this._textBlock.Inlines.Add(run);
                }

                Run run2 = new Run();
                run2.Text = this.Text.Substring(index, match.Length);
                PropertyPath backgroundPath = new PropertyPath(HighlightBackgroundProperty);
                if (this.SecondaryHighlightIndex == i)
                {
                    backgroundPath = new PropertyPath(SecondaryHighlightBackgroundProperty);
                }

                run2.SetBinding(
                    TextElement.BackgroundProperty,
                    new Binding
                    {
                        Source = this,
                        Path = backgroundPath
                    });

                PropertyPath foregroundPath = new PropertyPath(HighlightForegroundProperty);
                if (this.SecondaryHighlightIndex == i)
                {
                    foregroundPath = new PropertyPath(SecondaryHighlightForegroundProperty);
                }

                run2.SetBinding(
                    TextElement.ForegroundProperty,
                    new Binding
                    {
                        Source = this,
                        Path = foregroundPath,
                        TargetNullValue = this.Foreground
                    });

                this._textBlock.Inlines.Add(run2);

                startIndex = index + match.Length;
                int nextIndex = -1;
                if (i + 1 != matches.Count)
                {
                    nextIndex = matches[i + 1].Index;
                }

                if (nextIndex != -1)
                {
                    index = nextIndex;
                    continue;
                }

                int lastRunLength = this.Text.Length - startIndex;
                if (lastRunLength > 0)
                {
                    Run run3 = new Run();
                    run3.Text = this.Text.Substring(startIndex, lastRunLength);
                    this._textBlock.Inlines.Add(run3);
                }
            }
        }

        /// <summary>
        /// Gets called to handle when either the text or highlight text properties change.
        /// </summary>
        protected virtual void OnTextChanged()
        {
            if (this._textBlock == null)
            {
                return;
            }

            this._textBlock.Inlines.Clear();
            if (this.Text == null || this.HighlightExpression == null)
            {
                this._textBlock.Text = this.Text;
                return;
            }

            MatchCollection matches = this.HighlightExpression.Matches(this.Text);
            if (matches.Count == 0)
            {
                this._textBlock.Text = this.Text;
                return;
            }

            this.GenerateInlines(matches);
        }

        /// <summary>
        /// Called whenever a dependency property changes that affect the in-lines.
        /// </summary>
        /// <param name="s">
        /// The dependency object whose property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data used for this event.
        /// </param>
        private static void OnTextChanged(
            object s, DependencyPropertyChangedEventArgs e)
        {
            RsHighlightTextBlock highlightTextBlock = s as RsHighlightTextBlock;
            if (highlightTextBlock == null)
            {
                return;
            }

            highlightTextBlock.OnTextChanged();
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsHighlightTextBlock {Class}
} // RSG.Editor.Controls {Namespace}
