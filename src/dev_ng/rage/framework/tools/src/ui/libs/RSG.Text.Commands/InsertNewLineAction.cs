﻿//---------------------------------------------------------------------------------------------
// <copyright file="InsertNewLineAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Commands
{
    using RSG.Editor;
    using RSG.Text.ViewModel;

    /// <summary>
    /// Implements the <see cref="TextCommands.InsertNewLine"/> command.
    /// </summary>
    public class InsertNewLineAction : ButtonAction<TextLineCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="InsertNewLineAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public InsertNewLineAction(ParameterResolverDelegate<TextLineCommandArgs> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(TextLineCommandArgs args)
        {
            return args.Conversation != null && !args.Conversation.IsRandom;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(TextLineCommandArgs args)
        {
            int index = args.DataGrid.SelectedIndex + 1;

            args.Conversation.Model.AddNewLineAt(index);
            LineViewModel newitem = args.Conversation.Lines[index];
        }
        #endregion Methods
    } // RSG.Text.Commands.InsertNewLineAction {Class}
} // RSG.Text.Commands {Namespace}
