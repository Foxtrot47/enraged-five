﻿//---------------------------------------------------------------------------------------------
// <copyright file="ThemeProperties.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Themes
{
    /// <summary>
    /// Defines all of the resource keys to dynamic properties that are used to skin an
    /// application using the Editor Framework.
    /// </summary>
    public static class ThemeProperties
    {
        #region Properties
        /// <summary>
        /// Gets the resource key for the boolean value that indicates whether the break line
        /// on a tab control is shown.
        /// </summary>
        public static object ShowBreakLine
        {
            get { return "RsProperty.ShowBreakLine"; }
        }
        #endregion Properties
    } // RSG.Editor.Controls.Themes.ThemeProperties {Class}
} // RSG.Editor.Controls.Themes {Namespace}
