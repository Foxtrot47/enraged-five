﻿//---------------------------------------------------------------------------------------------
// <copyright file="ExtractionAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Rpf.Commands
{
    using System.Collections.Generic;
    using System.IO;
    using RSG.Editor;
    using RSG.Rpf.ViewModel;

    /// <summary>
    /// Provides a abstract base class for any command looking to extract one or more pack
    /// entry view model objects.
    /// </summary>
    public abstract class ExtractionAction : ButtonAction<IEnumerable<PackEntryViewModel>>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ExtractionAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public ExtractionAction(
            ParameterResolverDelegate<IEnumerable<PackEntryViewModel>> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Extracts the specified entry to the specified destination.
        /// </summary>
        /// <param name="viewModel">
        /// The pack entry to extract.
        /// </param>
        /// <param name="destination">
        /// The full path to where this entry should be extracted to.
        /// </param>
        /// <returns>
        /// True if the extraction was successful; otherwise, false.
        /// </returns>
        protected bool Extract(PackEntryViewModel viewModel, string destination)
        {
            using (MemoryStream memoryStream = this.GetMemoryStream(viewModel))
            {
                if (memoryStream != null && memoryStream.Length != 0)
                {
                    using (FileStream fileStream = File.OpenWrite(destination))
                    {
                        memoryStream.WriteTo(fileStream);
                    }

                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Gets the memory stream containing the data for the specified pack entry.
        /// </summary>
        /// <param name="entry">
        /// The entry to retrieve the memory stream for.
        /// </param>
        /// <returns>
        /// A new memory stream that contains the specified entries' data.
        /// </returns>
        protected MemoryStream GetMemoryStream(PackEntryViewModel entry)
        {
            return entry.Parent.ExtractEntry(entry);
        }
        #endregion Methods
    } // RSG.Rpf.Commands.ExtractionAction {Class}
} // RSG.Rpf.Commands {Namespace}
