﻿//---------------------------------------------------------------------------------------------
// <copyright file="CategoryViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.ViewModel
{
    using System.Collections;
    using RSG.Editor.View;
    using RSG.Text.Model;

    /// <summary>
    /// Provides the base class for a category of values inside the dialogue configuration
    /// editor of a specific type.
    /// </summary>
    public class CategoryViewModel : ViewModelBase<DialogueConfigurations>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CategoryViewModel"/> class.
        /// </summary>
        /// <param name="categoryName">
        /// The name for this category. This is used to display the category to the user.
        /// </param>
        /// <param name="configurations">
        /// The dialogue configurations whose values will be shown within this category.
        /// </param>
        public CategoryViewModel(string categoryName, DialogueConfigurations configurations)
            : base(configurations)
        {
            this.Text = categoryName;
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Creates a composite view model object for the individual items in the specified
        /// list.
        /// </summary>
        /// <param name="list">
        /// The collection that contains the individual items that should be grouped inside the
        /// resulting composite object.
        /// </param>
        /// <returns>
        /// A composite view model object that contains the individual items inside the
        /// specified list.
        /// </returns>
        public virtual CompositeViewModel CreateComposite(IList list)
        {
            return null;
        }
        #endregion Methods
    } // RSG.Text.ViewModel.CategoryViewModel {Class}
} // RSG.Text.ViewModel {Namespace}
