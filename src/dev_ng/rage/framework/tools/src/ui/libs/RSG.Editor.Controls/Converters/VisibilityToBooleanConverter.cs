﻿//---------------------------------------------------------------------------------------------
// <copyright file="VisibilityToBooleanConverter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Converters
{
    using System.Globalization;
    using System.Windows;

    /// <summary>
    /// Converts a System.Windows.Visibility value into a boolean value.
    /// </summary>
    public class VisibilityToBooleanConverter : ValueConverter<Visibility, bool>
    {
        #region Methods
        /// <summary>
        /// Converters the given value and returns the result.
        /// </summary>
        /// <param name="value">
        /// The original value to convert.
        /// </param>
        /// <param name="param">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A converted value.
        /// </returns>
        protected override bool Convert(Visibility value, object param, CultureInfo culture)
        {
            return value == Visibility.Collapsed ? false : true;
        }

        /// <summary>
        /// Converts a already converted value back to its original value and returns the
        /// result.
        /// </summary>
        /// <param name="value">
        /// The target value to convert back to its source value.
        /// </param>
        /// <param name="param">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// The source value for the already converted specified value.
        /// </returns>
        protected override Visibility ConvertBack(
            bool value, object param, CultureInfo culture)
        {
            return value == true ? Visibility.Visible : Visibility.Collapsed;
        }
        #endregion Methods
    } // RSG.Editor.Controls.Converters.VisibilityToBooleanConverter {Class}
} // RSG.Editor.Controls.Converters {Namespace}
