﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using RSG.Editor.Controls.MapViewport.OverlayComponents;

namespace RSG.Editor.Controls.MapViewport.AttachedProperties
{
    /// <summary>
    /// Contains attached properties that let you associate commands with mouse events.
    /// </summary>
    public static class MouseEventProperties
    {
        #region Fields
        /// <summary>
        /// Identifies the MouseLeftButtonDownCommand attached property.
        /// </summary>
        public static readonly DependencyProperty MouseLeftButtonDownCommandProperty =
            DependencyProperty.RegisterAttached(
                "MouseLeftButtonDownCommand",
                typeof(ICommand),
                typeof(MouseEventProperties),
                new PropertyMetadata(null, OnMouseLeftButtonDownCommandPropertyChanged));

        /// <summary>
        /// Retrieves the MouseLeftButtonDownCommand dependency property value attached to the
        /// specified object.
        /// </summary>
        /// <param name="obj">
        /// The object whose attached MouseLeftButtonDownCommand dependency property value will be
        /// returned.
        /// </param>
        /// <returns>
        /// The MouseLeftButtonDownCommand dependency property value attached to the specified
        /// object.
        /// </returns>
        public static ICommand GetMouseLeftButtonDownCommand(DependencyObject obj)
        {
            if (obj == null)
            {
                throw new SmartArgumentNullException(() => obj);
            }

            return (ICommand)obj.GetValue(MouseLeftButtonDownCommandProperty);
        }

        /// <summary>
        /// Sets the MouseLeftButtonDownCommand attached property value on the specified object to
        /// the specified value.
        /// </summary>
        /// <param name="obj">
        /// The object whose attached MouseLeftButtonDownCommand attached property value will be set.
        /// </param>
        /// <param name="value">
        /// The value to set the attached MouseLeftButtonDownCommand attached property to on the
        /// specified object.
        /// </param>
        public static void SetMouseLeftButtonDownCommand(
            DependencyObject obj, ICommand value)
        {
            if (obj == null)
            {
                throw new SmartArgumentNullException(() => obj);
            }

            obj.SetValue(MouseLeftButtonDownCommandProperty, value);
        }

        /// <summary>
        /// Call-back that fires whenever the MouseDownCommand attached property changes
        /// value.
        /// </summary>
        /// <param name="obj">
        /// The object whose attached MouseDownCommand attached property value has
        /// changed.
        /// </param>
        /// <param name="e">
        /// Event args containing information about the attached property that has changed.
        /// </param>
        private static void OnMouseLeftButtonDownCommandPropertyChanged(
            DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            UIElement element = obj as UIElement;
            if (element == null)
            {
                return;
            }

            if (e.OldValue != null)
            {
                element.PreviewMouseLeftButtonDown -= HandlePreviewMouseLeftButtonDown;
            }

            if (e.NewValue != null)
            {
                element.PreviewMouseLeftButtonDown += HandlePreviewMouseLeftButtonDown;
            }
        }

        /// <summary>
        /// Event handler for previewing a left mouse button down event.
        /// </summary>
        /// <param name="sender">
        /// Sender of the event.
        /// </param>
        /// <param name="e">
        /// Event args containing information about the mouse button that was pressed.
        /// </param>
        private static void HandlePreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ICommand command = GetMouseLeftButtonDownCommand(e.Source as DependencyObject);
            FrameworkElement element = e.Source as FrameworkElement;
            if (command == null || element == null)
            {
                return;
            }

            if (command.CanExecute(element.DataContext as Component))
            {
                command.Execute(element.DataContext as Component);
            }
        }
        #endregion
    }
}
