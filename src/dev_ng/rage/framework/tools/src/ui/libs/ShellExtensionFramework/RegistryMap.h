#ifndef SEF_REGISTRYMAP_H
#define SEF_REGISTRYMAP_H

/** 
	\brief Registry Map RGS File Support
	http://www.codeproject.com/Articles/6319/Registry-Map-for-RGS-files
 
	\example CRpfFileSystem
	
		DECLARE_REGISTRY_RESOURCEID_EX( IDR_RPFFILESYSTEM )
		BEGIN_REGISTRY_MAP(CRpfFileSystem)
			REGMAP_UUID( "CLSID", CLSID_RpfFileSystem )
			REGMAP_ENTRY( "DESCRIPTION", "Rockstar Games RPF FileSystem" )
			REGMAP_ENTRY( "ATTRIBUTES", "28000000" ) // HEX SFGAO_BROWSABLE | SFGAO_FOLDER
		END_REGISTRY_MAP()
 
 */

#include <atlbase.h>

struct _ATL_REGMAP_ENTRYKeeper : public _ATL_REGMAP_ENTRY
{
	_ATL_REGMAP_ENTRYKeeper(){ szKey=NULL; szData=NULL;}
	_ATL_REGMAP_ENTRYKeeper(LPCOLESTR key, LPCOLESTR data) 
	{
		szKey=key;
		LPOLESTR newData;
		szData = LPCOLESTR(newData =  new wchar_t[wcslen(data)+1]);
		wcscpy(newData,data);
	}
	_ATL_REGMAP_ENTRYKeeper(LPCOLESTR key, UINT resid)
	{
		static wchar_t Data[256];
		USES_CONVERSION;

		szKey=key;
		if( 0 == LoadStringW(_pModule->m_hInstResource, resid, Data, 255))
		{
			*Data = '\0';
		}

		int	length = wcslen(Data);

		szData = new wchar_t[length];
		wcscpy(const_cast<wchar_t *>(szData),Data);
	}

	_ATL_REGMAP_ENTRYKeeper(LPCOLESTR key, REFGUID guid) 
	{
		szKey=key;
		LPOLESTR newData;
		szData = LPCOLESTR(newData =  new wchar_t[40]);
		if(szData!=NULL)
		{
			if(0==StringFromGUID2(guid, newData,40))
			{
				*newData=NULL;
			}
		}
	}
	~_ATL_REGMAP_ENTRYKeeper()
	{
		delete [] (LPOLESTR)szData;
	}
};

#define BEGIN_REGISTRY_MAP(x) \
	static struct _ATL_REGMAP_ENTRY *_GetRegistryMap()\
	{\
		static const _ATL_REGMAP_ENTRYKeeper map[] = {
#define REGMAP_ENTRY(x,y) _ATL_REGMAP_ENTRYKeeper(OLESTR(##x),OLESTR(##y)),

#define REGMAP_RESOURCE(x,resid) _ATL_REGMAP_ENTRYKeeper(OLESTR(##x),), 

#define REGMAP_UUID(x,clsid) _ATL_REGMAP_ENTRYKeeper(OLESTR(##x),clsid),

#define END_REGISTRY_MAP() _ATL_REGMAP_ENTRYKeeper() }; return (_ATL_REGMAP_ENTRY *)map;}

#define DECLARE_REGISTRY_RESOURCEID_EX(x)\
	static HRESULT WINAPI UpdateRegistry(BOOL bRegister)\
	{\
		return _AtlModule.UpdateRegistryFromResource((UINT)x, bRegister, _GetRegistryMap() );\
	}


#endif // SEF_REGISTRYMAP_H
