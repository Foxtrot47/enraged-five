﻿//---------------------------------------------------------------------------------------------
// <copyright file="CoreFolderNode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel
{
    /// <summary>
    /// Represents a core folder node for a project node.
    /// </summary>
    public class CoreFolderNode : HierarchyNode
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CoreFolderNode"/> class.
        /// </summary>
        /// <param name="parent">
        /// The parent project node that created this node.
        /// </param>
        public CoreFolderNode(ProjectNode parent)
            : base(false)
        {
            this.Parent = parent;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the priority value for this object.
        /// </summary>
        public sealed override int Priority
        {
            get { return -1; }
        }
        #endregion Properties
    } // RSG.Project.ViewModel.CoreFolderNode {Class}
} // RSG.Project.ViewModel {Namespace}
