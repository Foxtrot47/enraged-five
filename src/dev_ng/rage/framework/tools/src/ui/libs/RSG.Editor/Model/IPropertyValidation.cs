﻿//---------------------------------------------------------------------------------------------
// <copyright file="IPropertyValidation.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Model
{
    /// <summary>
    /// When implemented represents a item that describes a situation that has been found
    /// through validating a model object that is associated with a individual property.
    /// </summary>
    public interface IPropertyValidation : IValidationItem
    {
        #region Properties
        /// <summary>
        /// Gets the name of the property this validation item is associated with.
        /// </summary>
        string PropertyName { get; }
        #endregion Properties
    } // RSG.Editor.Model.IPropertyValidation {Interface}
} // RSG.Editor.Model {Namespace}
