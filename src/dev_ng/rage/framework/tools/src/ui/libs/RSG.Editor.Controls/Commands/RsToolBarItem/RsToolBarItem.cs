﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsToolBarItem.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Commands
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Input;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Represents a clickable control that can be displayed on a tool bar.
    /// </summary>
    public class RsToolBarItem : Button
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="IconBitmap"/> dependency property.
        /// </summary>
        public static DependencyProperty IconBitmapProperty;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsToolBarItem"/> class.
        /// </summary>
        static RsToolBarItem()
        {
            IconBitmapProperty =
                DependencyProperty.Register(
                "IconBitmap",
                typeof(BitmapSource),
                typeof(RsToolBarItem),
                new FrameworkPropertyMetadata(null));

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsToolBarItem),
                new FrameworkPropertyMetadata(typeof(RsToolBarItem)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsToolBarItem"/> class.
        /// </summary>
        public RsToolBarItem()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the bitmap source object that is shown as the icon.
        /// </summary>
        public BitmapSource IconBitmap
        {
            get { return (BitmapSource)this.GetValue(IconBitmapProperty); }
            set { this.SetValue(IconBitmapProperty, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Called whenever the button is clicked by the user.
        /// </summary>
        protected override void OnClick()
        {
            if ((bool)this.GetValue(MenuItem.IsCheckableProperty))
            {
                bool isChecked = (bool)this.GetValue(ToggleButton.IsCheckedProperty);
                this.SetValue(ToggleButton.IsCheckedProperty, !isChecked);
            }

            base.OnClick();
        }

        /// <summary>
        /// Handles when this control receives keyboard focus.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.KeyboardFocusChangedEventArgs data used for this event.
        /// </param>
        protected override void OnGotKeyboardFocus(KeyboardFocusChangedEventArgs e)
        {
            if (e == null)
            {
                throw new SmartArgumentNullException(() => e);
            }

            if (e.NewFocus != this)
            {
                return;
            }

            UIElement uiElement = this.GetTemplateChild("PART_FocusTarget") as UIElement;
            if (uiElement == null)
            {
                return;
            }

            FocusNavigationDirection direction = FocusNavigationDirection.First;
            uiElement.MoveFocus(new TraversalRequest(direction));
        }
        #endregion Methods
    } // RSG.Editor.Controls.Commands.RsToolBarItem {Class}
} // RSG.Editor.Controls.Commands {Namespace}
