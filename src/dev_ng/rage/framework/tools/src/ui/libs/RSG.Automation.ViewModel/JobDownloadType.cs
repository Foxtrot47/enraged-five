﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Automation.ViewModel
{

    /// <summary>
    /// Download type.
    /// </summary>
    public enum JobDownloadType
    {
        Output,
        Log,
    }

} // RSG.Automation.ViewModel namespace
