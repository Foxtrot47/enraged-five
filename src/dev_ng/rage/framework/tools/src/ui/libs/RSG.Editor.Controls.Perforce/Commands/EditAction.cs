﻿//---------------------------------------------------------------------------------------------
// <copyright file="EditAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Perforce.Commands
{
    using System.Collections.Generic;
    using System.Diagnostics;

    /// <summary>
    /// Contains the logic for the <see cref="PerforceCommands.Edit"/> routed command.
    /// </summary>
    public class EditAction : ButtonAction<IList<string>>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="EditAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public EditAction(ParameterResolverDelegate<IList<string>> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="commandParameter">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(IList<string> commandParameter)
        {
            if (commandParameter == null || commandParameter.Count == 0)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="commandParameter">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(IList<string> commandParameter)
        {
            IPerforceService service = this.GetService<IPerforceService>();
            if (service == null)
            {
                Debug.Assert(false, "Unable to check out item due to missing service");
                return;
            }

            service.EditFiles(commandParameter);
        }
        #endregion Methods
    } // RSG.Editor.Controls.Perforce.Commands.EditAction {Class}
} // RSG.Editor.Controls.Perforce.Commands {Namespace}
