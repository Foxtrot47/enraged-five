﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Editor.Controls.MapViewport
{
    /// <summary>
    /// Specifies where a <see cref="MapLayerPanel.Position"/> is relative too.
    /// </summary>
    public enum PositionOrigin
    {
        /// <summary>
        /// Top left corner of the items bounds.
        /// </summary>
        TopLeft,

        /// <summary>
        /// Top center of the items bounds.
        /// </summary>
        TopCenter,

        /// <summary>
        /// Top right corner of the items bounds.
        /// </summary>
        TopRight,

        /// <summary>
        /// Center left of the items bounds.
        /// </summary>
        CenterLeft,

        /// <summary>
        /// Center of the items bounds.
        /// </summary>
        Center,

        /// <summary>
        /// Center right of the items bounds.
        /// </summary>
        CenterRight,

        /// <summary>
        /// Bottom left corner of the items bounds.
        /// </summary>
        BottomLeft,

        /// <summary>
        /// Bottom center of the items bounds.
        /// </summary>
        BottomCenter,

        /// <summary>
        /// Bottom right corner of the items bounds.
        /// </summary>
        BottomRight
    }
}
