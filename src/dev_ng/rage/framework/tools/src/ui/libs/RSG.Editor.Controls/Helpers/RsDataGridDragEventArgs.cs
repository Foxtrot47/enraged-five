﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsDataGridDragEventArgs.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Helpers
{
    using System.Windows;

    /// <summary>
    /// Contains information and event data for a data grid drag event.
    /// </summary>
    public class RsDataGridDragEventArgs : RsDragEventArgs
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="SourceRowItem"/> property.
        /// </summary>
        private object _sourceRowItem;

        /// <summary>
        /// The private field used for the <see cref="TargetRowItem"/> property.
        /// </summary>
        private object _targetRowItem;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="RsDataGridDragEventArgs"/> class.
        /// </summary>
        /// <param name="routedEvent">
        /// The routed event identifier for this instance.
        /// </param>
        /// <param name="screenPoint">
        /// The screen point that is associated with this event, either the original screen
        /// point, the current screen point, or the point the drag was completed.
        /// </param>
        /// <param name="state">
        /// The current state of the drag operation this event is reporting on.
        /// </param>
        /// <param name="source">
        /// 
        /// </param>
        /// <param name="target">
        /// 
        /// </param>
        /// <param name="horizontalChange">
        /// The distance the mouse cursor has changed in the horizontal direction.
        /// </param>
        /// <param name="verticalChange">
        /// The distance the mouse cursor has changed in the vertical direction.
        /// </param>
        public RsDataGridDragEventArgs(
            RoutedEvent routedEvent,
            Point screenPoint,
            DragState state,
            object source,
            object target,
            double horizontalChange = 0.0,
            double verticalChange = 0.0)
            : base(routedEvent, screenPoint, state, horizontalChange, verticalChange)
        {
            this._sourceRowItem = source;
            this._targetRowItem = target;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets
        /// </summary>
        public object SourceRowItem
        {
            get { return this._sourceRowItem; }
        }

        /// <summary>
        /// Gets
        /// </summary>
        public object TargetRowItem
        {
            get { return this._targetRowItem; }
        }
        #endregion Properties
    } // RSG.Editor.Controls.Helpers.RsDataGridDragEventArgs {Class}
} // RSG.Editor.Controls.Helpers {Namespace}
