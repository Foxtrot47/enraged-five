﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace RSG.Editor.Controls.MapViewport.OverlayComponents
{
    /// <summary>
    /// Represents an image that can be rendered in the map viewport.
    /// </summary>
    public class Image : Component
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="WorldExtents"/> property.
        /// </summary>
        private Rect _worldExtents;

        /// <summary>
        /// Private field for the <see cref="ImageSource"/> property.
        /// </summary>
        private ImageSource _imageSource;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Image"/> class.
        /// </summary>
        protected Image()
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Image" class using the specified
        /// world extents and image source.
        /// </summary>
        /// <param name="worldExtents">The bounds of the image in world space.s</param>
        /// <param name="imageSource">The source of the image to render.</param>
        public Image(Rect worldExtents, ImageSource imageSource)
        {
            _worldExtents = worldExtents;
            _imageSource = imageSource;
        }
        #endregion

        #region Properties
        /// <summary>
        /// The extents of the image (in world coordinates).
        /// </summary>
        public Rect WorldExtents
        {
            get { return _worldExtents; }
            set { SetProperty(ref _worldExtents, value, "WorldExtents", "Position"); }
        }

        /// <summary>
        /// The source of the image to use when rendering the map.
        /// </summary>
        public ImageSource ImageSource
        {
            get { return _imageSource; }
            set { SetProperty(ref _imageSource, value); }
        }

        /// <summary>
        /// Position of this component in world space.
        /// </summary>
        public override Point Position
        {
            get { return _worldExtents.TopLeft; }
            set { throw new NotSupportedException(); }
        }

        /// <summary>
        /// Where the position relates to in terms of the component's bounds.
        /// </summary>
        public override PositionOrigin PositionOrigin
        {
            get { return PositionOrigin.TopLeft; }
            set { throw new NotSupportedException(); }
        }
        #endregion
    }
}
