﻿//---------------------------------------------------------------------------------------------
// <copyright file="UseUTCAction.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.AssetBuildMonitor.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using RSG.AssetBuildMonitor.ViewModel;
    using RSG.Editor;
    using RSG.Editor.Controls.Dock.ViewModel;

    /// <summary>
    /// 
    /// </summary>
    public class UseUTCAction : ToggleButtonAction<CommandArgs>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="OpenLogAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public UseUTCAction(CommandResolver resolver)
            : base(new ParameterResolverDelegate<CommandArgs>(resolver))
        {
        }
        #endregion // Constructor(s)

        #region Methods
        /// <summary>
        /// Return initial toggled state of "Use UTC"; default to Local Time.
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        protected override bool GetInitialToggleState(RockstarRoutedCommand command)
        {
            return (false);
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(CommandArgs args)
        {
            // DHM FIX ME: notify application default UTC mode changed.
            
            IEnumerable<AssetBuildHistoryViewModel> documentViewModels = 
                args.GetAllDocumentViewModels();
            foreach (AssetBuildHistoryViewModel vm in documentViewModels)
            {
                // DHM FIX ME: switch each view-model.
            }
#if false
            dc.UseUTC = this.IsToggled;
            foreach (AssetBuildHistoryViewModel hist in dc.HostBuildHistory)
            {
                foreach (AssetBuildItemViewModel bi in hist.BuildItems)
                {
                    bi.UseUTC = this.IsToggled;
                }
            }
            dc.RefreshActiveBuildHistoryAsync();
#endif
        }
        #endregion // Methods
    }
}
