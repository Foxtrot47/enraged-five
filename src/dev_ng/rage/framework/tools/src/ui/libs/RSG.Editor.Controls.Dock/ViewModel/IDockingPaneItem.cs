﻿//---------------------------------------------------------------------------------------------
// <copyright file="IDockingPaneItem.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock.ViewModel
{
    using System.ComponentModel;

    /// <summary>
    /// When implemented represents a docking element that can be placed inside a docking pane
    /// as one of its children.
    /// </summary>
    public interface IDockingPaneItem
    {
        #region Properties
        /// <summary>
        /// Gets or sets a valid indicating whether this pane item can be hidden from the user.
        /// </summary>
        bool CanHide { get; set; }

        /// <summary>
        /// Gets or sets the index for this item inside its parent pane.
        /// </summary>
        int PaneIndex { get; set; }

        /// <summary>
        /// Gets the <see cref="DockingPane"/> that is a parent to this item.
        /// </summary>
        DockingPane ParentPane { get; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Called whenever this item is about to be closed and gives it the opportunity to
        /// cancel.
        /// </summary>
        /// <param name="e">
        /// The System.ComponentModel.CancelEventArgs giving this method the chance to cancel
        /// the close operation.
        /// </param>
        void OnClosing(CancelEventArgs e);
        #endregion Methods
    } // RSG.Editor.Controls.Dock.ViewModel.IDockingPaneItem {Interface}
} // RSG.Editor.Controls.Dock.ViewModel {Namespace}
