﻿//---------------------------------------------------------------------------------------------
// <copyright file="StructureMemberNode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Project
{
    using RSG.Metadata.Model.Definitions.Members;
    using RSG.Project.ViewModel;

    /// <summary>
    /// The node inside the project explorer that represents a member of a parCodeGen structure
    /// definition. This class cannot be inherited.
    /// </summary>
    public sealed class StructureMemberNode : HierarchyNode
    {
        #region Fields
        /// <summary>
        /// The structure member that this node is representing.
        /// </summary>
        private IMember _member;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="StructureMemberNode"/> class.
        /// </summary>
        /// <param name="member">
        /// The structure member object this node is representing.
        /// </param>
        /// <param name="parent">
        /// The parent node.
        /// </param>
        public StructureMemberNode(IMember member, IHierarchyNode parent)
            : base(false)
        {
            this.IsExpandable = false;
            this._member = member;
            this.Parent = parent;
            this.Icon = Icons.StructMemberNodeIcon;
            this.Text = member.Name;
        }
        #endregion Constructors
    } // RSG.Metadata.ViewModel.Project.StructureMemberNode {Class}
} // RSG.Metadata.ViewModel.Project {Namespace}
