﻿//---------------------------------------------------------------------------------------------
// <copyright file="SearcherViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Search
{
    using System;
    using RSG.Editor.View;

    /// <summary>
    /// A view model that represents a search class. This wraps a searcher object for the view
    /// and changes the underlying search type based on the search term specified.
    /// </summary>
    public class SearcherViewModel : ViewModelBase<Searcher>
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="SelectedSearchTerm"/> property.
        /// </summary>
        private RegisteredSearchTerm _selectedSearchTerm;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SearcherViewModel"/> class.
        /// </summary>
        /// <param name="model">
        /// The initial searcher object this view-model is using as the model.
        /// </param>
        public SearcherViewModel(Searcher model)
            : base(model, true)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the search condition for the value.
        /// </summary>
        public SearchCondition Condition
        {
            get { return this.Model.Condition; }
            set { this.Model.Condition = value; }
        }

        /// <summary>
        /// Gets or sets the search condition for the value.
        /// </summary>
        public string SearchTerm
        {
            get { return this.Model.SearchTerm; }
            set { this.Model.SearchTerm = value; }
        }

        /// <summary>
        /// Gets or sets the search term associated with this searcher. This changes out the
        /// underlying model class to make sure the correct searcher type is being used.
        /// </summary>
        public RegisteredSearchTerm SelectedSearchTerm
        {
            get
            {
                return this._selectedSearchTerm;
            }

            set
            {
                if (this.SetProperty(ref this._selectedSearchTerm, value))
                {
                    if (value != null)
                    {
                        switch (value.Type)
                        {
                            case SearchFieldType.Boolean:
                                this.ReplaceModel(new BooleanSearcher(), true);
                                break;

                            case SearchFieldType.Numeric:
                                this.ReplaceModel(new NumericalSearcher(), true);
                                break;

                            case SearchFieldType.String:
                                this.ReplaceModel(new StringSearcher(), true);
                                break;

                            case SearchFieldType.None:
                                this.ReplaceModel(new Searcher(), true);
                                break;
                        }

                        this.SearchTerm = value.Name;
                    }
                    else
                    {
                        this.ReplaceModel(new Searcher(), true);
                    }
                }
            }
        }
        #endregion Properties
    } // RSG.Editor.Controls.Search.SearcherViewModel {Class}
} // RSG.Editor.Controls.Search {Namespace}
