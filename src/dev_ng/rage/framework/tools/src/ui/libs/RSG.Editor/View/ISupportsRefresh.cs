﻿//---------------------------------------------------------------------------------------------
// <copyright file="ISupportsRefresh.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    using System.Threading.Tasks;

    /// <summary>
    /// When implemented represents a item that can be asynchronously refreshed.
    /// </summary>
    public interface ISupportsRefresh
    {
        #region Methods
        /// <summary>
        /// Refreshes this item asynchronously.
        /// </summary>
        /// <returns>
        /// A task that represents the work done to refresh this item.
        /// </returns>
        Task RefreshAsync();

        /// <summary>
        /// Cancels the asynchronous refresh operation.
        /// </summary>
        void CancelLoad();
        #endregion Methods
    } // RSG.Editor.View.ISupportsRefresh {Interface}
} // RSG.Editor.View {Namespace}
