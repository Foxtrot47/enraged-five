﻿namespace RSG.Text.ViewModel
{
    using System;
    using RSG.Editor.View;
    using RSG.Text.Model;

    public class DialogueVoiceViewModel : ViewModelBase<DialogueVoice>
    {
        private int _usedCount;

        public DialogueVoiceViewModel(DialogueVoice voice)
            : base(voice, true)
        {
        }

        public Guid Id
        {
            get { return this.Model.Id; }
        }


        public bool IsUsed
        {
            get { return this.UsedCount > 0; }
        }

        /// <summary>
        /// Gets or sets the name for this character.
        /// </summary>
        public string Name
        {
            get { return this.Model.Name; }
            set { this.Model.Name = value; }
        }

        /// <summary>
        /// Gets or sets a count that defines how many times this character is being used in
        /// the conversation.
        /// </summary>
        public int UsedCount
        {
            get { return this._usedCount; }
            set { this.SetProperty(ref this._usedCount, value, "UsedCount", "IsUsed"); }
        }

    }
}
