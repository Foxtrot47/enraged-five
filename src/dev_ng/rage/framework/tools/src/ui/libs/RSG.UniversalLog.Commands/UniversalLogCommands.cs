﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using RSG.Editor;
using RSG.Editor.SharedCommands;

namespace RSG.UniversalLog.Commands
{
    /// <summary>
    /// Defines core commands for the Universal Log Viewer Control.
    /// </summary>
    public static class UniversalLogCommands
    {
        #region Fields
        /// <summary>
        /// String table resource manager for getting at the command related strings.
        /// </summary>
        private static readonly CommandResourceManager _commandResourceManager =
            new CommandResourceManager(
                typeof(UniversalLogCommands).Namespace + ".Resources.CommandStringTable",
                typeof(UniversalLogCommands).Assembly);

        /// <summary>
        /// The private cache of System.Windows.Input.RoutedCommand objects.
        /// </summary>
        private static readonly RockstarRoutedCommand[] _internalCommands =
            new RockstarRoutedCommand[Enum.GetValues(typeof(CommandId)).Length];
        #endregion // Fields

        #region Enumerations
        /// <summary>
        /// Defines all of the command identifiers for the different core Rockstar commands.
        /// </summary>
        private enum CommandId
        {
            /// <summary>
            /// Used to identifier the <see cref="UniversalLogCommands.FilterByLogLevel"/>
            /// routed command.
            /// </summary>
            FilterByLogLevel,

            /// <summary>
            /// Used to identifier the <see cref="UniversalLogCommands.FilterByFile"/>
            /// routed command.
            /// </summary>
            FilterByFile,

            /// <summary>
            /// Used to identifier the <see cref="UniversalLogCommands.MergeFile"/>
            /// routed command.
            /// </summary>
            MergeFile,

            /// <summary>
            /// Used to identifier the <see cref="UniversalLogCommands.Refresh"/>
            /// routed command.
            /// </summary>
            Refresh,

            /// <summary>
            /// Used to identifier the <see cref="UniversalLogCommands.Email"/>
            /// routed command.
            /// </summary>
            Email
        }
        #endregion // Enumerations

        #region Properties
        /// <summary>
        /// Gets the value that represents the Filter By Log Level command.
        /// </summary>
        public static RockstarRoutedCommand FilterByLogLevel
        {
            get { return EnsureCommandExists(CommandId.FilterByLogLevel); }
        }

        /// <summary>
        /// Gets the value that represents the Filter By Log Level command.
        /// </summary>
        public static RockstarRoutedCommand FilterByFile
        {
            get { return EnsureCommandExists(CommandId.FilterByFile); }
        }

        /// <summary>
        /// Gets the value that represents the MergeFile command.
        /// </summary>
        public static RockstarRoutedCommand MergeFile
        {
            get { return EnsureCommandExists(CommandId.MergeFile); }
        }

        /// <summary>
        /// Gets the value that represents the Refresh command.
        /// </summary>
        public static RockstarRoutedCommand Refresh
        {
            get { return EnsureCommandExists(CommandId.Refresh, CommonIcons.Refresh); }
        }

        /// <summary>
        /// Gets the value that represents the MergeFile command.
        /// </summary>
        public static RockstarRoutedCommand Email
        {
            get { return EnsureCommandExists(CommandId.Email, UniversalLogIcons.Email); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new System.Windows.Input.RoutedCommand object that is associated with the
        /// specified identifier.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command to create.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        /// <returns>
        /// The newly create System.Windows.Input.RoutedCommand object.
        /// </returns>
        private static RockstarRoutedCommand CreateCommand(String id, BitmapSource icon)
        {
            String name = _commandResourceManager.GetString(id, CommandStringCategory.Name);
            String category = _commandResourceManager.GetString(id, CommandStringCategory.Category);
            String description = _commandResourceManager.GetString(id, CommandStringCategory.Description);
            InputGestureCollection gestures = _commandResourceManager.GetGestures(id);

            return new RockstarRoutedCommand(
                name, typeof(UniversalLogCommands), gestures, category, description, icon);
        }

        /// <summary>
        /// Makes sure the internal value for the command associated with the specified
        /// identifier exists and returns it.
        /// </summary>
        /// <param name="commandId">
        /// The identifier for the command that should be created and returned.
        /// </param>
        /// <param name="icon">
        /// The icon that the command should be using.
        /// </param>
        /// <returns>
        /// The <see cref="RSG.Editor.RockstarRoutedCommand"/> object associated with the
        /// specified identifier.
        /// </returns>
        private static RockstarRoutedCommand EnsureCommandExists(CommandId commandId, BitmapSource icon = null)
        {
            int commandIndex = (int)commandId;
            if (commandIndex < -1 || commandIndex >= _internalCommands.Length)
            {
                return null;
            }

            RockstarRoutedCommand command = _internalCommands[commandIndex];
            if (command == null)
            {
                lock (_internalCommands)
                {
                    if (command == null)
                    {
                        command = CreateCommand(commandId.ToString(), icon);
                        _internalCommands[commandIndex] = command;
                    }
                }
            }

            return command;
        }
        #endregion // Methods
    } // UniversalLogCommands
}
