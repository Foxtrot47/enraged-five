﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Rag.Widgets;

namespace RSG.Rag.ViewModel.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    public class ToggleFloatViewModel : ToggleViewModel<Single>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        public ToggleFloatViewModel(WidgetToggle<Single> widget)
            : base(widget)
        {
        }
        #endregion // Constructor(s)
    } // ToggleFloatViewModel
}
