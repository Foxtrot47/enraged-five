﻿//---------------------------------------------------------------------------------------------
// <copyright file="RgbaColourSliderComponent.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    /// <summary>
    /// Defines the different colour components that can be manipulated using a RGBA colour
    /// slider control.
    /// </summary>
    public enum RgbaColourSliderComponent
    {
        /// <summary>
        /// The control wont manipulate anything and show as a blank control.
        /// </summary>
        None,

        /// <summary>
        /// The control will manipulate the red component of the base colour.
        /// </summary>
        Red,

        /// <summary>
        /// The control will manipulate the green component of the base colour.
        /// </summary>
        Green,

        /// <summary>
        /// The control will manipulate the blue component of the base colour.
        /// </summary>
        Blue,

        /// <summary>
        /// The control will manipulate the alpha component of the base colour.
        /// </summary>
        Alpha,
    } // RSG.Editor.Controls.RgbaColourSliderComponent {Enum}
} // RSG.Editor.Controls {Namespace}
