﻿//---------------------------------------------------------------------------------------------
// <copyright file="ManualValidation.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Helpers
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Documents;
    using System.Windows.Media;

    /// <summary>
    /// Provides methods and attached properties that support manual data validation that can
    /// be shown without using the in built binding validation.
    /// </summary>
    public static class ManualValidation
    {
        #region Fields
        /// <summary>
        /// Identifies the ManualErrorTemplate dependency property.
        /// </summary>
        public static readonly DependencyProperty ManualErrorTemplateProperty;

        /// <summary>
        /// Identifies the ManualValidationAdorner dependency property.
        /// </summary>
        private static readonly DependencyProperty _manualValidationAdornerProperty;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="ManualValidation"/> class.
        /// </summary>
        static ManualValidation()
        {
            ManualErrorTemplateProperty =
                DependencyProperty.RegisterAttached(
                "ManualErrorTemplate",
                typeof(ControlTemplate),
                typeof(ManualValidation),
                new FrameworkPropertyMetadata(
                    null,
                    FrameworkPropertyMetadataOptions.NotDataBindable,
                    new PropertyChangedCallback(OnManualErrorTemplateChanged)));

            _manualValidationAdornerProperty =
                DependencyProperty.RegisterAttached(
                "ManualValidationAdorner",
                typeof(ManualValidationAdorner),
                typeof(ManualValidation),
                new FrameworkPropertyMetadata(
                    null, FrameworkPropertyMetadataOptions.NotDataBindable));
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Hides the manual validation adorner for the specified visual target.
        /// </summary>
        /// <param name="target">
        /// The visual whose manual validation adorner should be hidden.
        /// </param>
        public static void HideManualValidationAdorner(Visual target)
        {
            if (target == null)
            {
                return;
            }

            AdornerLayer layer = AdornerLayer.GetAdornerLayer(target);
            ManualValidationAdorner adorner = GetManualValidationAdorner(target);
            if (layer == null || adorner == null)
            {
                return;
            }

            adorner.ClearChild();
            layer.Remove(adorner);
            target.ClearValue(_manualValidationAdornerProperty);
        }

        /// <summary>
        /// Sets the ManualErrorTemplate dependency property value on the specified object
        /// to the specified value.
        /// </summary>
        /// <param name="obj">
        /// The object whose attached ManualErrorTemplate dependency property value will be
        /// set.
        /// </param>
        /// <param name="value">
        /// The value to set the attached ManualErrorTemplate dependency property to on the
        /// specified object.
        /// </param>
        public static void SetManualErrorTemplate(Visual obj, ControlTemplate value)
        {
            if (obj == null)
            {
                throw new SmartArgumentNullException(() => obj);
            }

            obj.SetValue(ManualErrorTemplateProperty, value);
        }

        /// <summary>
        /// Shows the manual validation adorner for the specified visual target.
        /// </summary>
        /// <param name="target">
        /// The visual whose manual validation adorner should be shown.
        /// </param>
        /// <param name="message">
        /// The validation message the adorner should display.
        /// </param>
        public static void ShowManualValidationAdorner(Visual target, string message)
        {
            FrameworkElement element = target as FrameworkElement;
            if (element == null || target == null)
            {
                return;
            }

            AdornerLayer layer = AdornerLayer.GetAdornerLayer(target);
            ManualValidationAdorner adorner = GetManualValidationAdorner(target);
            ControlTemplate errorTemplate = GetManualErrorTemplate(target);
            if (layer == null || errorTemplate == null || adorner != null)
            {
                if (adorner != null)
                {
                    HideManualValidationAdorner(target);
                    ShowManualValidationAdorner(target, message);
                    return;
                }
            }

            adorner = new ManualValidationAdorner(element, errorTemplate);
            adorner.ErrorMessage = message;
            layer.Add(adorner);
            target.SetValue(_manualValidationAdornerProperty, adorner);
        }

        /// <summary>
        /// Retrieves the ManualErrorTemplate dependency property value attached to the
        /// specified object.
        /// </summary>
        /// <param name="obj">
        /// The object whose attached ManualErrorTemplate dependency property value will be
        /// returned.
        /// </param>
        /// <returns>
        /// The ManualErrorTemplate dependency property value attached to the specified
        /// object.
        /// </returns>
        private static ControlTemplate GetManualErrorTemplate(Visual obj)
        {
            if (obj == null)
            {
                throw new SmartArgumentNullException(() => obj);
            }

            return obj.GetValue(ManualErrorTemplateProperty) as ControlTemplate;
        }

        /// <summary>
        /// Retrieves the ManualValidationAdorner dependency property value attached to the
        /// specified object.
        /// </summary>
        /// <param name="obj">
        /// The object whose attached ManualValidationAdorner dependency property value will be
        /// returned.
        /// </param>
        /// <returns>
        /// The ManualValidationAdorner dependency property value attached to the specified
        /// object.
        /// </returns>
        private static ManualValidationAdorner GetManualValidationAdorner(Visual obj)
        {
            if (obj == null)
            {
                throw new SmartArgumentNullException(() => obj);
            }

            return obj.GetValue(_manualValidationAdornerProperty) as ManualValidationAdorner;
        }

        /// <summary>
        /// Called whenever the ManualErrorTemplate dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The object whose ManualErrorTemplate dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnManualErrorTemplateChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            Visual visual = s as Visual;
            if (visual == null)
            {
                return;
            }

            ManualValidationAdorner adorner = GetManualValidationAdorner(visual);
            if (adorner == null)
            {
                return;
            }

            HideManualValidationAdorner(visual);
            ShowManualValidationAdorner(visual, adorner.ErrorMessage);
        }
        #endregion Methods
    } // RSG.Editor.Controls.Helpers.ManualValidation {Class}
} // RSG.Editor.Controls.Helpers {Namespace}
