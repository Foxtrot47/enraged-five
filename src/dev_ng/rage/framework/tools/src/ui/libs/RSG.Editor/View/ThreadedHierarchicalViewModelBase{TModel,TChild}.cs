﻿//---------------------------------------------------------------------------------------------
// <copyright file="ThreadedHierarchicalViewModelBase{TModel,TChild}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    using System.Collections.Generic;
    using RSG.Editor.Model;

    /// <summary>
    /// Provides a abstract base class that can be inherited by classes that wish to implement
    /// the <see cref="RSG.Editor.View.IViewModel{T}"/> interface.
    /// </summary>
    /// <typeparam name="TModel">
    /// The type of the model that is being wrapped by this view model.
    /// </typeparam>
    /// <typeparam name="TChild">
    /// The type of the child items for this view model.
    /// </typeparam>
    public abstract class ThreadedHierarchicalViewModelBase<TModel, TChild> :
        ViewModelBase<TModel>,
        ISupportsExpansionEvents,
        ISupportsExpansionState,
        IProvidesExpandableState
        where TChild : IViewModel
        where TModel : IModel
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="IsExpandable"/> property.
        /// </summary>
        private bool _isExpandable;

        /// <summary>
        /// The private field used for the <see cref="Items"/> property.
        /// </summary>
        private ThreadedViewModelCollection<TChild> _items;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="ThreadedHierarchicalViewModelBase{TModel,TChild}"/> class.
        /// </summary>
        /// <param name="model">
        /// The model that this view model will be wrapping.
        /// </param>
        protected ThreadedHierarchicalViewModelBase(TModel model)
            : base(model)
        {
            this._items = new ThreadedViewModelCollection<TChild>(this.CreateChildren);
            this._items.BatchAddChildren = this.BatchAddChildren;
            this._isExpandable = true;
            if (this.CreateChildrenDuringConstruction)
            {
                this.CreateChildren(this._items);
            }
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether this items children are destroyed when it is
        /// collapsed. The resulting behaviour if true is that the references are kept in a
        /// cache and the expansion state is persistent for child nodes.
        /// </summary>
        public virtual bool DestroyNodesOnCollapse
        {
            get { return false; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this item can be expanded.
        /// </summary>
        public bool IsExpandable
        {
            get { return this._isExpandable; }
            set { this.SetProperty(ref this._isExpandable, value); }
        }

        /// <summary>
        /// Gets a value indicating whether this item is expanded by default in the user
        /// interface when by rendered inside a hierarchical control.
        /// </summary>
        public virtual bool IsExpandedByDefault
        {
            get { return false; }
        }

        /// <summary>
        /// Gets the items that have been added underneath this view model as children.
        /// </summary>
        public ThreadedViewModelCollection<TChild> Items
        {
            get { return this._items; }
        }

        /// <summary>
        /// Gets a value indicating whether the children get added to the items collection in
        /// one go after initialisation or one at a time during initialisation.
        /// </summary>
        protected virtual bool BatchAddChildren
        {
            get { return false; }
        }

        /// <summary>
        /// Gets a value indicating whether the child items for this view model get created
        /// straight away during construction.
        /// </summary>
        protected virtual bool CreateChildrenDuringConstruction
        {
            get { return false; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Called just after this object in the user interface is collapsed inside a
        /// hierarchical control.
        /// </summary>
        public virtual void AfterCollapse()
        {
            if (this._items.IsInitialized && this.DestroyNodesOnCollapse)
            {
                this._items.ClearInitialisationFlag();
            }
        }

        /// <summary>
        /// Called just before this object in the user interface is expanded inside a
        /// hierarchical control.
        /// </summary>
        public virtual void BeforeExpand()
        {
            if (this._items.IsInitialized == false)
            {
                this._items.BeginInit();
            }
        }

        /// <summary>
        /// Override to create the child elements for this view model. This is only called once
        /// the item has been expanded in the user interface.
        /// </summary>
        /// <param name="collection">
        /// The collection to add the elements to.
        /// </param>
        protected abstract void CreateChildren(ICollection<TChild> collection);
        #endregion Methods
    } // RSG.Editor.View.ThreadedHierarchicalViewModelBase{TModel,TChild} {Class}
} // RSG.Editor.View {Namespace}
