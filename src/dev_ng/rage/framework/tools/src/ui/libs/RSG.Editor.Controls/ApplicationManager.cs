//---------------------------------------------------------------------------------------------
// <copyright file="ApplicationManager.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Runtime.Remoting;
    using System.Runtime.Remoting.Channels;
    using System.Runtime.Remoting.Channels.Ipc;
    using System.Runtime.Serialization.Formatters;
    using System.Text;
    using System.Threading;
    using System.Windows;
    using System.Windows.Threading;
    using RSG.Interop.Microsoft.Windows;

    /// <summary>
    /// This class checks to make sure that only one instance of the specified application type
    /// is running at a time and provides ways they can communicate between themselves.
    /// </summary>
    /// <typeparam name="T">
    /// The type of application we want to manager.
    /// </typeparam>
    [System.Runtime.InteropServices.GuidAttribute("CC95D5F1-4D44-4211-BA3F-5B0D62D89CED")]
    public static class ApplicationManager<T> where T : RsApplication
    {
        #region Fields
        /// <summary>
        /// IPC channel for communications.
        /// </summary>
        private static IpcServerChannel _channel;

        /// <summary>
        /// Application <see cref="Mutex"/>.
        /// </summary>
        private static Mutex _singleInstanceMutex;
        #endregion Fields

        #region Methods
        /// <summary>
        /// Cleans up single-instance code, destroying shared resources etc.
        /// </summary>
        public static void Cleanup()
        {
            if (_singleInstanceMutex != null)
            {
                _singleInstanceMutex.Close();
                _singleInstanceMutex = null;
            }

            if (_channel != null)
            {
                ChannelServices.UnregisterChannel(_channel);
                _channel = null;
            }
        }

        /// <summary>
        /// Checks if this instance of the application should be allowed to run based on the
        /// applications mode and whether there are any other instances currently running.
        /// </summary>
        /// <param name="application">
        /// The application that this will manage.
        /// </param>
        /// <param name="uniqueName">
        /// A unique name to give this application.
        /// </param>
        /// <returns>
        /// True if this instance of the application should be allowed to run; otherwise,
        /// false.
        /// </returns>
        public static InitialisationResult InitialiseApplication(T application, string uniqueName)
        {
            string[] commandLineArgs = GetCommandLineArgs(uniqueName);
            if (!application.SetStartArguments(commandLineArgs.Skip(1).ToArray()))
            {
                return InitialisationResult.SetArgumentsFailed;
            }
            if (application.Mode == ApplicationMode.Multiple)
            {
                return InitialisationResult.Success;
            }

            // Build the IPC channel name.
            string channelName = application.MutexName + ":SingeInstanceIPCChannel";

            // Create the mutex based on unique application identifier to check if this is the
            // first instance of the application.
            bool createdNew = false;
            _singleInstanceMutex = new Mutex(true, application.MutexName, out createdNew);
            if (createdNew)
            {
                CreateRemoteService(channelName);
                return InitialisationResult.Success;
            }

            if (application.Mode == ApplicationMode.Single)
            {
                SignalFirstInstance(channelName, commandLineArgs);
                return InitialisationResult.SingleInstanceRunning;
            }
            else
            {
                Debug.Assert(false, "Unsupported Application Mode.");
            }

            return InitialisationResult.Success;
        }

        /// <summary>
        /// Gets command line arguments - for ClickOnce deployed applications, command line
        /// arguments may not be passed directly, they have to be retrieved.
        /// </summary>
        /// <param name="uniqueName">
        /// A unique name given to this application.
        /// </param>
        /// <returns>
        /// List of command line arguments strings.
        /// </returns>
        internal static string[] GetCommandLineArgs(string uniqueName)
        {
            string[] args = null;
            if (AppDomain.CurrentDomain.ActivationContext == null)
            {
                // The application was not ClickOnce deployed, get arguments from
                // standard API's
                return Environment.GetCommandLineArgs() ?? new string[] { };
            }

            // The application was ClickOnce deployed. ClickOnce deployed apps cannot receive
            // traditional command line arguments. As a workaround command line arguments can
            // be written to a shared location before the app is launched and the app can
            // obtain its command line arguments from the shared location.
            string localData = Environment.GetFolderPath(
                Environment.SpecialFolder.LocalApplicationData);
            string appFolderPath = Path.Combine(localData, uniqueName);
            string cmdPath = Path.Combine(appFolderPath, "cmdline.txt");

            if (!File.Exists(cmdPath))
            {
                return new string[] { };
            }

            try
            {
                Encoding encoding = Encoding.Unicode;
                using (TextReader reader = new StreamReader(cmdPath, encoding))
                {
                    string cmdline = reader.ReadToEnd();
                    args = Shell32.CommandLineToArgs(cmdline);
                }

                File.Delete(cmdPath);
            }
            catch (Exception ex)
            {
                throw new IOException(
                    "Unable to read arguments for click once common location.", ex);
            }

            return args ?? new string[] { };
        }

        /// <summary>
        /// Activates the first instance of the application with arguments from a
        /// second instance.
        /// </summary>
        /// <param name="args">
        /// List of arguments to supply the first instance of the application.
        /// </param>
        private static void ActivateFirstInstance(string[] args)
        {
            // Set main window state and process command line arguments
            if (Application.Current == null)
            {
                return;
            }

            ((T)Application.Current).ActivationCallback();

            bool result = ((T)Application.Current).HandleExternalArguments(args);
            Debug.Assert(result, "First instance couldn't handle the specified arguments");
        }

        /// <summary>
        /// Call-back for activating first instance of the application.
        /// </summary>
        /// <param name="arg">
        /// Call-back argument.
        /// </param>
        /// <returns>
        /// Always null.
        /// </returns>
        private static object ActivateFirstInstanceCallback(object arg)
        {
            // Get command line arguments to be passed to first instance
            string[] args = arg as string[];
            ActivateFirstInstance(args);
            return null;
        }

        /// <summary>
        /// Creates a remote service for communication.
        /// </summary>
        /// <param name="channelName">
        /// Applications IPC channel name.
        /// </param>
        private static void CreateRemoteService(string channelName)
        {
            BinaryServerFormatterSinkProvider provider =
                new BinaryServerFormatterSinkProvider();
            provider.TypeFilterLevel = TypeFilterLevel.Full;
            IDictionary props = new Dictionary<string, string>();

            props["name"] = channelName;
            props["portName"] = channelName;
            props["exclusiveAddressUse"] = "false";

            // Create the IPC Server channel with the channel properties
            _channel = new IpcServerChannel(props, provider);

            // Register the channel with the channel services
            ChannelServices.RegisterChannel(_channel, true);

            // Expose the remote service with the RemoteServiceName
            IpcRemoteService remoteService = new IpcRemoteService();
            remoteService.InitializeLifetimeService();
            RemotingServices.Marshal(remoteService, "RockstarAppService");
        }

        /// <summary>
        /// Creates a client channel and obtains a reference to the remote service exposed by
        /// the server - in this case, the remote service exposed by the first instance.
        /// </summary>
        /// <param name="channelName">
        /// Application's IPC channel name.
        /// </param>
        /// <returns>
        /// A reference to the remote service exposed by the server with the given channel
        /// name.
        /// </returns>
        private static IpcRemoteService GetInstance(string channelName)
        {
            IpcClientChannel secondInstanceChannel = new IpcClientChannel();
            ChannelServices.RegisterChannel(secondInstanceChannel, true);

            string url = GetServiceUri(channelName);

            // Obtain a reference to the remote service exposed by the server i.e the first
            // instance of the application.
            Type ipcType = typeof(IpcRemoteService);
            return (IpcRemoteService)RemotingServices.Connect(ipcType, url);
        }

        /// <summary>
        /// Creates the service Url to use for the remote service.
        /// </summary>
        /// <param name="channelName">
        /// The channel name to use in the Url.
        /// </param>
        /// <returns>
        /// The System.String that contains the remote service Url.
        /// </returns>
        private static string GetServiceUri(string channelName)
        {
            return "ipc://" + channelName + "/RockstarAppService";
        }

        /// <summary>
        /// Creates a client channel and obtains a reference to the remote service exposed by
        /// the server - in this case, the remote service exposed by the first instance. Calls
        /// a function of the remote service class to pass on command line arguments from the
        /// second instance to the first and causes it to activate itself.
        /// </summary>
        /// <param name="channelName">
        /// Application's IPC channel name.
        /// </param>
        /// <param name="args">
        /// Command line arguments for the second instance, passed to the first instance to
        /// take appropriate action.
        /// </param>
        private static void SignalFirstInstance(string channelName, string[] args)
        {
            // Obtain a reference to the remote service exposed by the server i.e the first
            // instance of the application.
            IpcRemoteService firstInstance = GetInstance(channelName);

            // Check that the remote service exists, in some cases the first instance may not
            // yet have created one, in which case the second instance should just exit.
            if (firstInstance == null)
            {
                return;
            }

            // Invoke a method of the remote service exposed by the first instance passing
            // on the command line arguments and causing the first instance to activate itself.
            firstInstance.InvokeFirstInstance(args);
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// Remote service class which is exposed by the server i.e the first instance and
        /// called by the second instance to pass on the command line arguments to the first
        /// instance and cause it to activate itself.
        /// </summary>
        private class IpcRemoteService : MarshalByRefObject
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="IpcRemoteService"/> class.
            /// </summary>
            public IpcRemoteService()
            {
            }
            #endregion Constructors

            #region Methods
            /// <summary>
            /// Remoting object's expires after every 5 minutes by default. Returning
            /// null overrides this behaviour.
            /// </summary>
            /// <returns>Always null.</returns>
            public override object InitializeLifetimeService()
            {
                return null;
            }

            /// <summary>
            /// Activates the first instance of the application.
            /// </summary>
            /// <param name="args">
            /// List of arguments to pass to the first instance.
            /// </param>
            public void InvokeFirstInstance(string[] args)
            {
                if (Application.Current == null)
                {
                    return;
                }

                // Do an asynchronous call to ActivateFirstInstance function
                Application.Current.Dispatcher.BeginInvoke(
                    DispatcherPriority.Normal,
                    new DispatcherOperationCallback(
                        ApplicationManager<T>.ActivateFirstInstanceCallback),
                        args);
            }
            #endregion Methods
        } // IPCRemoteService
        #endregion Classes
    } // RSG.Editor.Controls.ApplicationManager {Class}
} // RSG.Editor.Controls {Namespace}
