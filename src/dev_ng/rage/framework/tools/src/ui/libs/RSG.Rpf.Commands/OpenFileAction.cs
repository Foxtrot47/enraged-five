﻿//---------------------------------------------------------------------------------------------
// <copyright file="OpenFileAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Rpf.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Threading.Tasks;
    using RSG.Editor;
    using RSG.Editor.SharedCommands;
    using RSG.Rpf.ViewModel;

    /// <summary>
    /// Implements the <see cref="RockstarCommands.OpenFile"/> command.
    /// </summary>
    public class OpenFileAction : ButtonActionAsync<IEnumerable<RpfViewerDataContext>>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="OpenFileAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public OpenFileAction(
            ParameterResolverDelegate<IEnumerable<RpfViewerDataContext>> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="viewModels">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(IEnumerable<RpfViewerDataContext> viewModels)
        {
            foreach (RpfViewerDataContext viewModel in viewModels)
            {
                if (viewModel != null)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="viewModels">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// A task that represents the work done by the command handler.
        /// </returns>
        public override async Task Execute(IEnumerable<RpfViewerDataContext> viewModels)
        {
            foreach (RpfViewerDataContext viewModel in viewModels)
            {
                if (viewModel == null)
                {
                    continue;
                }

                string path = null;
                ICommonDialogService dlgService = this.GetService<ICommonDialogService>();
                if (dlgService == null)
                {
                    Debug.Assert(false, "Unable to extract as service is missing.");
                    continue;
                }

                var filter = "Rage Packfile Archives (*.rpf)|*.rpf|All Files (*.*)|*.*";
                if (!dlgService.ShowOpenFile(null, null, filter, 0, out path))
                {
                    continue;
                }

                await viewModel.LoadAsync(path);
            }
        }
        #endregion Methods
    } // RSG.Rpf.Commands.OpenFileAction {Class}
} // RSG.Rpf.Commands {Namespace}
