﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssetBuildMonitorViewModelBase.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.AssetBuildMonitor.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using RSG.Editor;
    using RSG.Editor.View;

    /// <summary>
    /// Abstract base class for Asset Build Monitor view models.
    /// </summary>
    public abstract class AssetBuildMonitorViewModelBase : 
        NotifyPropertyChangedBase,
        INotifyDataErrorInfo
    {
        #region Constants
        /// <summary>
        /// Delay to wait after property changes to start validation.
        /// </summary>
        private const int WaitMillisecondsBeforeValidation = 200;
        #endregion // Constants

        #region INotifyDataErrorInfo Interface
        public System.Collections.IEnumerable GetErrors(String propertyName)
        {
            List<String> errorsForName;
            _errors.TryGetValue("Name", out errorsForName);
            return errorsForName;
        }
 
        public bool HasErrors
        {
            get { return _errors.Values.FirstOrDefault(l => l.Count > 0) != null; }
        }

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;
        public void RaiseErrorsChanged(string propertyName)
        {
            EventHandler<DataErrorsChangedEventArgs> handler = ErrorsChanged;
            if (handler == null) return;
            var arg = new DataErrorsChangedEventArgs(propertyName);
            handler.Invoke(this, arg);
        }
        #endregion // INotifyDataErrorInfo Interface

        private Dictionary<String, List<String>> _errors =
            new Dictionary<String, List<String>>();
 
        protected void Validate()
        {
            Task waitTask = new Task(() => Thread.Sleep(
                TimeSpan.FromMilliseconds(WaitMillisecondsBeforeValidation)));
            waitTask.ContinueWith((_) => ViewModelValidation());
            waitTask.Start();
        }
 
        protected abstract void ViewModelValidation();
    
        protected void AddError(String propertyName, String errorMessage)
        {
            lock (this._errors)
            {
                if (!this._errors.ContainsKey(propertyName))
                    this._errors.Add(propertyName, new List<String>());

                this._errors[propertyName].Add(errorMessage);
            }
        }
    }

} // RSG.AssetBuildMonitor.ViewModel namespace
