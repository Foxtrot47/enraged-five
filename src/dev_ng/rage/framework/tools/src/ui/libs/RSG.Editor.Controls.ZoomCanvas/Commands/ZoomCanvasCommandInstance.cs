﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace RSG.Editor.Controls.ZoomCanvas.Commands
{
    /// <summary>
    /// MET TODO
    /// </summary>
    public class ZoomCanvasCommandInstance : CommandInstance, ICommandWithParameter, ICommandWithIcon
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Icon"/> property.
        /// </summary>
        private readonly BitmapSource _icon;

        /// <summary>
        /// Private field for the <see cref="Parameter"/> property.
        /// </summary>
        private readonly object _parameter;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ZoomCanvasCommandInstance"/> class.
        /// </summary>
        /// <param name="command">
        /// The command that this command bar item is instancing.
        /// </param>
        /// <param name="priority">
        /// The initial priority that this command bar item should be given.
        /// </param>
        /// <param name="startsGroup">
        /// A value indicating whether this command bar item should start a new group. (I.e.
        /// Placed directly after a separator).
        /// </param>
        /// <param name="text">
        /// The text for this command bar item.
        /// </param>
        /// <param name="id">
        /// The unique identifier that is used to reference this item.
        /// </param>
        /// <param name="parentId">
        /// The unique identifier for the parent of this item inside the command bar hierarchy.
        /// </param>
        public ZoomCanvasCommandInstance(
            RockstarRoutedCommand command,
            ushort priority,
            bool startsGroup,
            string text,
            Guid id,
            Guid parentId,
            BitmapSource icon,
            object parameter)
            : base(command, priority, startsGroup, text, id, parentId)
        {
            _icon = icon;
            _parameter = parameter;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the System.Windows.Media.Imaging.BitmapSource object that represents the icon
        /// that is used for this command, this can be null.
        /// </summary>
        public BitmapSource Icon
        {
            get { return _icon; }
        }

        /// <summary>
        /// Gets the parameter that is routed through the element tree with the command.
        /// </summary>
        public object CommandParameter
        {
            get { return _parameter; }
        }
        #endregion
    }
}
