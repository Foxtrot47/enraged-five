﻿//---------------------------------------------------------------------------------------------
// <copyright file="ITunableViewModelParent.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Tunables
{
    using RSG.Editor.View;

    /// <summary>
    /// When implemented represents a view model that is used for view models wrapping a
    /// tunable class type that contains tunable children.
    /// </summary>
    public interface ITunableViewModelParent : ITunableViewModel
    {
        #region Properties
        /// <summary>
        /// Gets the collection of child tunables for this structure.
        /// </summary>
        IReadOnlyViewModelCollection<ITunableViewModel> Tunables { get; }
        #endregion Properties
    } // RSG.Metadata.ViewModel.Tunables.ITunableViewModelParent {Interface}
} // RSG.Metadata.ViewModel.Tunables {Namespace}
