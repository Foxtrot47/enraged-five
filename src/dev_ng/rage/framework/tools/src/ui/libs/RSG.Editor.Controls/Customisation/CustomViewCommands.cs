﻿//---------------------------------------------------------------------------------------------
// <copyright file="CustomViewCommands.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Customisation
{
    using System.Windows.Input;

    /// <summary>
    /// Contains the routed commands that are used within the customisation views.
    /// </summary>
    public sealed class CustomViewCommands
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="AddCommand"/> property.
        /// </summary>
        private static RoutedCommand _addCommand;

        /// <summary>
        /// The private field used for the <see cref="AddNewMenu"/> property.
        /// </summary>
        private static RoutedCommand _addNewMenu;

        /// <summary>
        /// The private field used for the <see cref="AddNewToolbar"/> property.
        /// </summary>
        private static RoutedCommand _addNewToolbar;

        /// <summary>
        /// The private field used for the <see cref="CancelCommandSelection"/> property.
        /// </summary>
        private static RoutedCommand _cancelCommandSelection;

        /// <summary>
        /// The private field used for the <see cref="ConfirmCommandSelection"/> property.
        /// </summary>
        private static RoutedCommand _confirmCommandSelection;

        /// <summary>
        /// The private field used for the <see cref="DecreasePriority"/> property.
        /// </summary>
        private static RoutedCommand _decreasePriority;

        /// <summary>
        /// The private field used for the <see cref="Delete"/> property.
        /// </summary>
        private static RoutedCommand _delete;

        /// <summary>
        /// The private field used for the <see cref="IncreasePriority"/> property.
        /// </summary>
        private static RoutedCommand _increasePriority;

        /// <summary>
        /// The private field used for the <see cref="MoveDown"/> property.
        /// </summary>
        private static RoutedCommand _moveDown;

        /// <summary>
        /// The private field used for the <see cref="MoveUp"/> property.
        /// </summary>
        private static RoutedCommand _moveUp;

        /// <summary>
        /// The private field used for the <see cref="Reset"/> property.
        /// </summary>
        private static RoutedCommand _reset;

        /// <summary>
        /// The private field used for the <see cref="ResetAll"/> property.
        /// </summary>
        private static RoutedCommand _resetAll;

        /// <summary>
        /// The generic object that is used to sync the creation of the commands.
        /// </summary>
        private static object _syncRoot = new object();
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets the command that is used to open the add command window.
        /// </summary>
        public static RoutedCommand AddCommand
        {
            get
            {
                if (_addCommand != null)
                {
                    return _addCommand;
                }

                lock (_syncRoot)
                {
                    if (_addCommand != null)
                    {
                        return _addCommand;
                    }

                    _addCommand = new RoutedCommand("AddCommand", typeof(CustomViewCommands));
                }

                return _addCommand;
            }
        }

        /// <summary>
        /// Gets the command that is used to add a new menu to a command container.
        /// </summary>
        public static RoutedCommand AddNewMenu
        {
            get
            {
                if (_addNewMenu != null)
                {
                    return _addNewMenu;
                }

                lock (_syncRoot)
                {
                    if (_addNewMenu != null)
                    {
                        return _addNewMenu;
                    }

                    _addNewMenu =
                        new RoutedCommand("AddNewMenuCommand", typeof(CustomViewCommands));
                }

                return _addNewMenu;
            }
        }

        /// <summary>
        /// Gets the command that is used to add a new toolbar to a command container.
        /// </summary>
        public static RoutedCommand AddNewToolbar
        {
            get
            {
                if (_addNewToolbar != null)
                {
                    return _addNewToolbar;
                }

                lock (_syncRoot)
                {
                    if (_addNewToolbar != null)
                    {
                        return _addNewToolbar;
                    }

                    _addNewToolbar =
                        new RoutedCommand("AddNewToolbarCommand", typeof(CustomViewCommands));
                }

                return _addNewToolbar;
            }
        }

        /// <summary>
        /// Gets the command that is used to cancel the selection of a command from the add
        /// command window.
        /// </summary>
        public static RoutedCommand CancelCommandSelection
        {
            get
            {
                if (_cancelCommandSelection != null)
                {
                    return _cancelCommandSelection;
                }

                lock (_syncRoot)
                {
                    if (_cancelCommandSelection != null)
                    {
                        return _cancelCommandSelection;
                    }

                    _cancelCommandSelection =
                        new RoutedCommand(
                            "CancelCommandSelection", typeof(CustomViewCommands));
                }

                return _cancelCommandSelection;
            }
        }

        /// <summary>
        /// Gets the command that is used to confirm the selection of a command from the add
        /// command window.
        /// </summary>
        public static RoutedCommand ConfirmCommandSelection
        {
            get
            {
                if (_confirmCommandSelection != null)
                {
                    return _confirmCommandSelection;
                }

                lock (_syncRoot)
                {
                    if (_confirmCommandSelection != null)
                    {
                        return _confirmCommandSelection;
                    }

                    _confirmCommandSelection =
                        new RoutedCommand(
                            "ConfirmCommandSelection", typeof(CustomViewCommands));
                }

                return _confirmCommandSelection;
            }
        }

        /// <summary>
        /// Gets the command that is used to decrease the priority of a command in such a way
        /// as it moves one space in the user interface.
        /// </summary>
        public static RoutedCommand DecreasePriority
        {
            get
            {
                if (_decreasePriority != null)
                {
                    return _decreasePriority;
                }

                lock (_syncRoot)
                {
                    if (_decreasePriority != null)
                    {
                        return _decreasePriority;
                    }

                    _decreasePriority =
                        new RoutedCommand("DecreasePriority", typeof(CustomViewCommands));
                }

                return _decreasePriority;
            }
        }

        /// <summary>
        /// Gets the command that is used to remove a command from the command system.
        /// </summary>
        public static RoutedCommand Delete
        {
            get
            {
                if (_delete != null)
                {
                    return _delete;
                }

                lock (_syncRoot)
                {
                    if (_delete != null)
                    {
                        return _delete;
                    }

                    _delete = new RoutedCommand("DeleteCommand", typeof(CustomViewCommands));
                }

                return _delete;
            }
        }

        /// <summary>
        /// Gets the command that is used to increase the priority of a command in such a way
        /// as it moves one space in the user interface.
        /// </summary>
        public static RoutedCommand IncreasePriority
        {
            get
            {
                if (_increasePriority != null)
                {
                    return _increasePriority;
                }

                lock (_syncRoot)
                {
                    if (_increasePriority != null)
                    {
                        return _increasePriority;
                    }

                    _increasePriority =
                        new RoutedCommand("IncreasePriority", typeof(CustomViewCommands));
                }

                return _increasePriority;
            }
        }

        /// <summary>
        /// Gets the command that is used to reposition a command bar item below the command
        /// underneath it.
        /// </summary>
        public static RoutedCommand MoveDown
        {
            get
            {
                if (_moveDown != null)
                {
                    return _moveDown;
                }

                lock (_syncRoot)
                {
                    if (_moveDown != null)
                    {
                        return _moveDown;
                    }

                    _moveDown =
                        new RoutedCommand("MoveDownCommand", typeof(CustomViewCommands));
                }

                return _moveDown;
            }
        }

        /// <summary>
        /// Gets the command that is used to reposition a command bar item above the command
        /// next to it.
        /// </summary>
        public static RoutedCommand MoveUp
        {
            get
            {
                if (_moveUp != null)
                {
                    return _moveUp;
                }

                lock (_syncRoot)
                {
                    if (_moveUp != null)
                    {
                        return _moveUp;
                    }

                    _moveUp = new RoutedCommand("MoveUpCommand", typeof(CustomViewCommands));
                }

                return _moveUp;
            }
        }

        /// <summary>
        /// Gets the command that is used to reset the commands in a single command container
        /// back to their default state.
        /// </summary>
        public static RoutedCommand Reset
        {
            get
            {
                if (_reset != null)
                {
                    return _reset;
                }

                lock (_syncRoot)
                {
                    if (_reset != null)
                    {
                        return _reset;
                    }

                    _reset = new RoutedCommand("Reset", typeof(CustomViewCommands));
                }

                return _reset;
            }
        }

        /// <summary>
        /// Gets the command that is used to reset all of the command options back to their
        /// default state.
        /// </summary>
        public static RoutedCommand ResetAll
        {
            get
            {
                if (_resetAll != null)
                {
                    return _resetAll;
                }

                lock (_syncRoot)
                {
                    if (_resetAll != null)
                    {
                        return _resetAll;
                    }

                    _resetAll = new RoutedCommand("ResetAll", typeof(CustomViewCommands));
                }

                return _resetAll;
            }
        }
        #endregion Properties
    } // RSG.Editor.Controls.Customisation.CustomViewCommands {Class}
} // RSG.Editor.Controls.Customisation {Namespace}
