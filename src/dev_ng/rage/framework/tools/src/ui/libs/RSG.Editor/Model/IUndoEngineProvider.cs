﻿//---------------------------------------------------------------------------------------------
// <copyright file="IUndoEngineProvider.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Model
{
    /// <summary>
    /// When implemented represents a object that can be used to retrieve a instance of the
    /// <see cref="UndoEngine"/> class.
    /// </summary>
    public interface IUndoEngineProvider
    {
        #region Properties
        /// <summary>
        /// Gets the instance of the <see cref="RSG.Editor.Model.UndoEngine"/> class that this
        /// object is using as its undo engine.
        /// </summary>
        UndoEngine UndoEngine { get; }
        #endregion Properties
    } // RSG.Editor.Model.IUndoEngineProvider {Interface}
} // RSG.Editor.Model {Namespace}
