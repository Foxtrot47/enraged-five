﻿using RSG.Rag.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.ViewModel.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    public class SliderInt8ViewModel : SliderViewModel<SByte>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        public SliderInt8ViewModel(WidgetSlider<SByte> widget)
            : base(widget)
        {
        }
        #endregion // Constructor(s)
    } // SliderInt8ViewModel
}
