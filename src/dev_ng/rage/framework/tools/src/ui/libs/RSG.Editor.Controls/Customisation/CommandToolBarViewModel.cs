﻿//---------------------------------------------------------------------------------------------
// <copyright file="CommandToolBarViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Customisation
{
    using System;
    using RSG.Editor.Controls.Resources;
    using RSG.Editor.SharedCommands;

    /// <summary>
    /// Represents a view model that wraps a single tool bar command instance.
    /// </summary>
    internal class CommandToolBarViewModel : CommandContainerViewModel
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CommandToolBarViewModel"/> class.
        /// </summary>
        /// <param name="name">
        /// The name of the menu.
        /// </param>
        /// <param name="id">
        /// The id to use to get the child commands from the
        /// <see cref="RockstarCommandManager"/> class.
        /// </param>
        private CommandToolBarViewModel(string name, Guid id)
            : base(name, id)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="CommandToolBarViewModel"/> class.
        /// </summary>
        /// <param name="item">
        /// The tool bar command that this view model is representing.
        /// </param>
        /// <param name="container">
        /// The container that this view model is currently inside.
        /// </param>
        private CommandToolBarViewModel(
            CommandToolBar item, CommandContainerViewModel container)
            : base(item, container)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Creates a new instance of the <see cref="CommandToolBarViewModel"/> class that is
        /// used for the tool bar tray.
        /// </summary>
        /// <returns>
        /// A new instance of the <see cref="CommandToolBarViewModel"/> class.
        /// </returns>
        public static CommandToolBarViewModel CreateForToolBarTray()
        {
            return new CommandToolBarViewModel(
                CustomisationResources.ToolBarTrayRootText,
                RockstarCommandManager.ToolBarTrayId);
        }

        /// <summary>
        /// Creates a new instance of the <see cref="CommandToolBarViewModel"/> class that
        /// wraps the specified command toolbar item.
        /// </summary>
        /// <param name="item">
        /// The command menu item the new instance will be wrapping.
        /// </param>
        /// <returns>
        /// A new instance of the <see cref="CommandToolBarViewModel"/> class.
        /// </returns>
        /// <param name="container">
        /// The container that the new view model is a child of.
        /// </param>
        public static CommandToolBarViewModel CreateChild(
            CommandToolBar item, CommandContainerViewModel container)
        {
            return new CommandToolBarViewModel(item, container);
        }
        #endregion Methods
    } // RSG.Editor.Controls.Customisation.CommandToolBarViewModel {Class}
} // RSG.Editor.Controls.Customisation {Namespace}
