﻿//---------------------------------------------------------------------------------------------
// <copyright file="FileOperation.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Rpf.Commands
{
    /// <summary>
    /// Defines the different operations that can be performed inside the file management
    /// implementer.
    /// </summary>
    public enum FileOperation
    {
        /// <summary>
        /// Opens a file by either using the specified path or using a the common dialog
        /// service.
        /// </summary>
        Open,

        /// <summary>
        /// Closes the current opened file.
        /// </summary>
        Close,
    } // RSG.Rpf.Commands.FileOperation {Enum}
} // RSG.Rpf.Commands {Namespace}
