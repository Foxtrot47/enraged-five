﻿//---------------------------------------------------------------------------------------------
// <copyright file="AudioTypeCategoryViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.ViewModel
{
    using System.Collections;
    using System.Collections.Generic;
    using RSG.Text.Model;
    using RSG.Text.ViewModel.Resources;

    /// <summary>
    /// Represents the dialogue configuration editor category for the audio type items in the
    /// configuration. This class cannot be inherited.
    /// </summary>
    public sealed class AudioTypeCategoryViewModel : CategoryViewModel
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Items"/> property.
        /// </summary>
        private AudioTypeCollection _audioTypes;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="AudioTypeCategoryViewModel"/> class
        /// for the specified configurations.
        /// </summary>
        /// <param name="configurations">
        /// The configurations whose audio type items will be shown as part of this category.
        /// </param>
        public AudioTypeCategoryViewModel(DialogueConfigurations configurations)
            : base(StringTable.AudioTypeCategoryHeader, configurations)
        {
            this._audioTypes = new AudioTypeCollection(configurations.AudioTypes);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the list of audibility values this category contains.
        /// </summary>
        public AudioTypeCollection Items
        {
            get { return this._audioTypes; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a composite view model object containing the
        /// <see cref="DialogueAudioTypeViewModel"/> objects inside the specified list.
        /// </summary>
        /// <param name="list">
        /// The collection that contains the individual items that should be grouped inside the
        /// resulting composite object.
        /// </param>
        /// <returns>
        /// A composite view model object that contains the
        /// <see cref="DialogueAudioTypeViewModel"/> objects inside the specified list.
        /// </returns>
        public override CompositeViewModel CreateComposite(IList list)
        {
            List<DialogueAudioTypeViewModel> items = new List<DialogueAudioTypeViewModel>();
            foreach (object item in list)
            {
                DialogueAudioTypeViewModel audioType = item as DialogueAudioTypeViewModel;
                if (audioType == null)
                {
                    continue;
                }

                items.Add(audioType);
            }

            return new AudioTypeCompositeViewModel(this.Model, items);
        }
        #endregion Methods
    } // RSG.Text.ViewModel.AudioTypeCategoryViewModel {Class}
} // RSG.Text.ViewModel {Namespace}
