﻿//---------------------------------------------------------------------------------------------
// <copyright file="CollectionFolderNode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel
{
    using RSG.Base.Extensions;
    using RSG.Project.Model;

    /// <summary>
    /// Represents a folder node inside the main project collection.
    /// </summary>
    public class CollectionFolderNode : FolderNode
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CollectionFolderNode"/> class.
        /// </summary>
        /// <param name="model">
        /// The model that this view model will be wrapping.
        /// </param>
        public CollectionFolderNode(IHierarchyNode parent, ProjectItem model)
            : base(parent, model)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether a new or existing project can be added to this
        /// hierarchy node.
        /// </summary>
        public override bool CanHaveProjectsAdded
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether this hierarchy node can be removed from the parent
        /// items scope it belongs to.
        /// </summary>
        public override bool CanBeRemoved
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether a new or existing collection folder can be added to
        /// this hierarchy node.
        /// </summary>
        public override bool CanHaveCollectionFoldersAdded
        {
            get { return true; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Adds a new folder underneath this node and returns the instance to the new folder.
        /// </summary>
        /// <returns>
        /// The new folder that was created.
        /// </returns>
        public override IHierarchyNode AddNewFolder()
        {
            string include = "{0}\\{1}".FormatInvariant(this.Model.Include, "NewFolder");
            include = this.Model.Scope.GetUniqueInclude(include);
            ProjectItem item = this.Model.Scope.AddNewProjectItem("Folder", include);
            HierarchyNode node = new CollectionFolderNode(this, item);
            node.IsExpandable = false;
            this.AddChild(node);
            this.IsExpandable = true;
            return node;
        }
        #endregion Methods
    } // RSG.Project.ViewModel.CollecionFolderNode {Class}
} // RSG.Project.ViewModel {Namespace}
