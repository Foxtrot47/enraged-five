﻿//---------------------------------------------------------------------------------------------
// <copyright file="TypeColumnCellTemplateSelector.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.View
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Media;
    using RSG.Editor.Controls;
    using RSG.Editor.Controls.Helpers;
    using RSG.Metadata.Model.Definitions.Members;
    using RSG.Metadata.Model.Tunables;
    using RSG.Metadata.ViewModel.Tunables;

    /// <summary>
    /// Represents the cell template that is associated with the type column in the tunable
    /// structure control.
    /// </summary>
    public class TypeColumnCellTemplateSelector : DataTemplateSelector
    {
        #region Fields
        /// <summary>
        /// The private data template that's used by this selector for the tunable view model
        /// objects that represent a numerical tunable.
        /// </summary>
        private static DataTemplate _numericTemplate;

        /// <summary>
        /// The private data template that's used by this selector for the normal tunable view
        /// model objects.
        /// </summary>
        private static DataTemplate _template;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="TypeColumnCellTemplateSelector" />
        /// class.
        /// </summary>
        static TypeColumnCellTemplateSelector()
        {
            _template = CreateNormalTemplate();
            _numericTemplate = CreateNumericTemplate();
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Returns the data template to use for the specified item inside the type column for
        /// the tunable structure control.
        /// </summary>
        /// <param name="item">
        /// The data content.
        /// </param>
        /// <param name="container">
        /// The element to which the template will be applied.
        /// </param>
        /// <returns>
        /// The data template to use for the specified item inside the type column for the
        /// tunable structure control.
        /// </returns>
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            VirtualisedTreeNode node = item as VirtualisedTreeNode;
            if (node == null)
            {
                return new DataTemplate();
            }

            ITunableViewModel tunableViewModel = node.Item as ITunableViewModel;
            if (tunableViewModel == null)
            {
                return new DataTemplate();
            }

            ITunable tunable = tunableViewModel.Model as ITunable;
            if (tunable != null)
            {
                IHasRange hasRange = tunable.Member as IHasRange;
                if (hasRange != null)
                {
                    return _numericTemplate;
                }
            }

            return _template;
        }

        /// <summary>
        /// Creates a new data template that normal tunable view models can use for the type
        /// column in the metadata tree structure.
        /// </summary>
        /// <returns>
        /// A new data template that normal tunable view models can use.
        /// </returns>
        private static DataTemplate CreateNormalTemplate()
        {
            FrameworkElementFactory factory = new FrameworkElementFactory(typeof(TextBlock));
            Binding textBinding = new Binding("Item.TypeValue");
            textBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            factory.SetBinding(TextBlock.TextProperty, textBinding);
            factory.SetValue(TextBlock.MarginProperty, new Thickness(0.0, 5.0, 0.0, 0.0));

            DataTemplate template = new DataTemplate();
            template.VisualTree = factory;
            return template;
        }

        /// <summary>
        /// Creates a new data template that numeric tunable view models can use for the type
        /// column in the metadata tree structure.
        /// </summary>
        /// <returns>
        /// A new data template that numeric tunable view models can use.
        /// </returns>
        private static DataTemplate CreateNumericTemplate()
        {
            FrameworkElementFactory factory = new FrameworkElementFactory(typeof(TextBlock));
            Binding textBinding = new Binding("Item.TypeValue");
            factory.SetBinding(TextBlock.TextProperty, textBinding);
            factory.SetValue(TextBlock.MarginProperty, new Thickness(0.0, 5.0, 0.0, 0.0));

            DataTemplate template = new DataTemplate();
            template.VisualTree = factory;
            return template;
        }
        #endregion Methods
    } // RSG.Metadata.View.TypeColumnCellTemplateSelector {Class}
} // RSG.Metadata.View {Namespace}
