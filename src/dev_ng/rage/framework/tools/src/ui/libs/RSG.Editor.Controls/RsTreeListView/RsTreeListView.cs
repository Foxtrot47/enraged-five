﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsTreeListView.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Media;
    using RSG.Editor.Controls.AttachedDependencyProperties;
    using RSG.Editor.Controls.Presenters;
    using RSG.Editor.Controls.Themes;

    /// <summary>
    /// Represents a control that displays hierarchical data in a tree structure that also
    /// supports multiple columns.
    /// </summary>
    public class RsTreeListView : RsVirtualisedTreeView
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="ExpansionColumnHeader"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ExpansionColumnHeaderProperty;

        /// <summary>
        /// Identifies the <see cref="ExpansionColumnWidth"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ExpansionColumnWidthProperty;

        /// <summary>
        /// Identifies the <see cref="ExpansionContentPath"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ExpansionContentPathProperty;

        /// <summary>
        /// Identifies the <see cref="ExpansionContentTemplateSelector"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ExpansionContentTemplateSelectorProperty;

        /// <summary>
        /// The private field used for the <see cref="Columns"/> property.
        /// </summary>
        private GridViewColumnCollection _columns;

        /// <summary>
        /// The private reference to the column that is setup as the column containing the
        /// tree view expansion item controls.
        /// </summary>
        private GridViewColumn _expansionColumn;

        /// <summary>
        /// The private field used for the <see cref="Columns"/> property.
        /// </summary>
        private ObservableCollection<GridViewColumn> _observableColumns;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsTreeListView" /> class.
        /// </summary>
        static RsTreeListView()
        {
            ExpansionContentPathProperty =
                DependencyProperty.Register(
                "ExpansionContentPath",
                typeof(string),
                typeof(RsTreeListView),
                new PropertyMetadata(null, OnExpansionContentPathChanged));

            ExpansionContentTemplateSelectorProperty =
                ContentControl.ContentTemplateSelectorProperty.AddOwner(
                typeof(RsTreeListView),
                new FrameworkPropertyMetadata(
                    null, FrameworkPropertyMetadataOptions.AffectsMeasure));

            ExpansionColumnHeaderProperty =
                DependencyProperty.Register(
                "ExpansionColumnHeader",
                typeof(object),
                typeof(RsTreeListView),
                new FrameworkPropertyMetadata(null, OnExpansionColumnHeaderChanged));

            ExpansionColumnWidthProperty =
                DependencyProperty.Register(
                "ExpansionColumnWidth",
                typeof(DataGridLength),
                typeof(RsTreeListView),
                new FrameworkPropertyMetadata(
                    new DataGridLength(100.0), OnExpansionColumnWidthChanged));

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsTreeListView),
                new FrameworkPropertyMetadata(typeof(RsTreeListView)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsTreeListView" /> class.
        /// </summary>
        public RsTreeListView()
        {
            this._columns = new GridViewColumnCollection();
            this._observableColumns = new ObservableCollection<GridViewColumn>();
            this._observableColumns.CollectionChanged += this.OnColumnsChanged;

            this._expansionColumn = new GridViewColumn();
            this._expansionColumn.Header = this.ExpansionColumnHeader;
            this._expansionColumn.Width = this.ExpansionColumnWidth.Value;

            this._expansionColumn.CellTemplate = this.CreateTemplateForExpansionColumn();
            this._columns.Add(this._expansionColumn);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the actual collection of columns that are bound to the view. This collection
        /// contains the column containing the tree view expansion controls.
        /// </summary>
        public GridViewColumnCollection ActualColumns
        {
            get { return this._columns; }
        }

        /// <summary>
        /// Gets the collection of column definitions describing the individual columns of each
        /// row.
        /// </summary>
        public ObservableCollection<GridViewColumn> Columns
        {
            get { return this._observableColumns; }
        }

        /// <summary>
        /// Gets or sets the content of the header for the column containing the expansion
        /// controls.
        /// </summary>
        public object ExpansionColumnHeader
        {
            get { return this.GetValue(ExpansionColumnHeaderProperty); }
            set { this.SetValue(ExpansionColumnHeaderProperty, value); }
        }

        /// <summary>
        /// Gets or sets the width for the column containing the expansion controls.
        /// </summary>
        public DataGridLength ExpansionColumnWidth
        {
            get { return (DataGridLength)this.GetValue(ExpansionColumnWidthProperty); }
            set { this.SetValue(ExpansionColumnWidthProperty, value); }
        }

        /// <summary>
        /// Gets or sets the name of the property on the view model that should be bound to the
        /// content property on the presenter.
        /// </summary>
        public string ExpansionContentPath
        {
            get { return (string)this.GetValue(ExpansionContentPathProperty); }
            set { this.SetValue(ExpansionContentPathProperty, value); }
        }

        /// <summary>
        /// Gets or sets the DataTemplateSelector for the content inside the expansion column.
        /// </summary>
        public DataTemplateSelector ExpansionContentTemplateSelector
        {
            get
            {
                return (DataTemplateSelector)this.GetValue(
                    ExpansionContentTemplateSelectorProperty);
            }

            set
            {
                this.SetValue(ExpansionContentTemplateSelectorProperty, value);
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates or identifies the element used to display the child items.
        /// </summary>
        /// <returns>
        /// A new <see cref="RsVirtualisedTreeViewItem"/>.
        /// </returns>
        protected override DependencyObject GetContainerForItemOverride()
        {
            return new RsTreeListViewItem();
        }

        /// <summary>
        /// Determines whether the specified item is, or is eligible to be, its own item
        /// container.
        /// </summary>
        /// <param name="item">
        /// The item to check whether it is an item container.
        /// </param>
        /// <returns>
        /// True if the item is eligible to be its own item container; otherwise, false.
        /// </returns>
        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is RsTreeListViewItem;
        }

        /// <summary>
        /// Called whenever the <see cref="ExpansionColumnHeader"/> dependency property
        /// changes.
        /// </summary>
        /// <param name="s">
        /// The object whose <see cref="ExpansionColumnHeader"/> dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnExpansionColumnHeaderChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            RsTreeListView treeListView = s as RsTreeListView;
            if (treeListView == null)
            {
                Debug.Assert(false, "Incorrect dependency object conversion.");
                return;
            }

            treeListView._expansionColumn.Header = e.NewValue;
        }

        /// <summary>
        /// Called whenever the <see cref="ExpansionColumnWidth"/> dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The object whose <see cref="ExpansionColumnWidth"/> dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnExpansionColumnWidthChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            RsTreeListView treeListView = s as RsTreeListView;
            if (treeListView == null)
            {
                Debug.Assert(false, "Incorrect dependency object conversion.");
                return;
            }

            if (!(e.NewValue is DataGridLength))
            {
                treeListView._expansionColumn.Width = 100.0;
                return;
            }

            DataGridLength newLength = (DataGridLength)e.NewValue;
            if (newLength.UnitType != DataGridLengthUnitType.Pixel)
            {
                throw new NotSupportedException("Only pixel lengths are currently supported");
            }

            treeListView._expansionColumn.Width = ((DataGridLength)e.NewValue).Value;
        }

        /// <summary>
        /// Called whenever the <see cref="ExpansionContentPath"/> dependency property
        /// changes.
        /// </summary>
        /// <param name="s">
        /// The object whose <see cref="ExpansionContentPath"/> dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnExpansionContentPathChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            RsTreeListView treeListView = s as RsTreeListView;
            if (treeListView == null)
            {
                Debug.Assert(false, "Incorrect dependency object conversion.");
                return;
            }

            treeListView._expansionColumn.CellTemplate =
                treeListView.CreateTemplateForExpansionColumn();
        }

        /// <summary>
        /// Creates the setter object that sets the brush resource of the glyph property on the
        /// presenter.
        /// </summary>
        /// <param name="active">
        /// A value indicating whether the active brush should be used or the inactive brush.
        /// </param>
        /// <returns>
        /// The new setter object.
        /// </returns>
        private Setter CreateGlyphBrushSetter(bool active)
        {
            Setter setter = new Setter();
            setter.Property = TreeListItemPresenter.GlyphBrushProperty;
            setter.TargetName = "Presenter";

            object resource = ThemeBrushes.ItemTextInactiveSelectedKey;
            if (active)
            {
                resource = ThemeBrushes.ItemTextActiveSelectedKey;
            }

            setter.Value = new DynamicResourceExtension(resource);
            return setter;
        }

        /// <summary>
        /// Creates the setter object that sets the brush resource of the glyph mouse over
        /// property on the presenter.
        /// </summary>
        /// <param name="active">
        /// A value indicating whether the active brush should be used or the inactive brush.
        /// </param>
        /// <returns>
        /// The new setter object.
        /// </returns>
        private Setter CreateGlyphMouseOverBrushSetter(bool active)
        {
            Setter setter = new Setter();
            setter.Property = TreeListItemPresenter.GlyphMouseOverBrushProperty;
            setter.TargetName = "Presenter";

            object resource = ThemeBrushes.ItemBackgroundActiveSelectedKey;
            if (active)
            {
                resource = ThemeBrushes.ItemTextActiveSelectedKey;
            }

            setter.Value = new DynamicResourceExtension(resource);
            return setter;
        }

        /// <summary>
        /// Creates the trigger that contains setters used when the presenter is currently
        /// selected but inactive.
        /// </summary>
        /// <returns>
        /// The new trigger object.
        /// </returns>
        private MultiTrigger CreateInactiveTrigger()
        {
            MultiTrigger trigger = new MultiTrigger();
            Condition condition1 = new Condition();
            condition1.Property = Selector.IsSelectedProperty;
            condition1.Value = true;
            trigger.Conditions.Add(condition1);

            Condition condition2 = new Condition();
            condition2.Property = Selector.IsSelectionActiveProperty;
            condition2.Value = false;
            trigger.Conditions.Add(condition2);

            Condition condition3 = new Condition();
            condition3.Property = ContextMenuProperties.IsContextMenuOpenProperty;
            condition3.Value = false;
            trigger.Conditions.Add(condition3);

            trigger.Setters.Add(this.CreateGlyphBrushSetter(false));
            trigger.Setters.Add(this.CreateGlyphMouseOverBrushSetter(false));
            return trigger;
        }

        /// <summary>
        /// Creates the framework element factory used to create the tree view item presenter.
        /// </summary>
        /// <returns>
        /// The new framework element factory object.
        /// </returns>
        private FrameworkElementFactory CreateTreeItemPresenterFactory()
        {
            FrameworkElementFactory presenterFactory = new FrameworkElementFactory(
                typeof(TreeListItemPresenter), "Presenter");

            presenterFactory.SetValue(
                TreeListItemPresenter.NameProperty,
                "Presenter");
            presenterFactory.SetValue(
                Validation.ErrorTemplateProperty,
                null);
            presenterFactory.SetValue(
                TreeListItemPresenter.MarginProperty,
                new Thickness(0.0, 3.0, 0.0, 0.0));
            presenterFactory.SetValue(
                TreeListItemPresenter.BackgroundProperty,
                Brushes.Transparent);
            presenterFactory.SetBinding(
                TreeListItemPresenter.DepthProperty,
                new Binding("Depth"));
            presenterFactory.SetBinding(
                TreeListItemPresenter.IsExpandedProperty,
                new Binding("IsExpanded") { Mode = BindingMode.TwoWay });
            presenterFactory.SetBinding(
                TreeListItemPresenter.DisplayItemProperty,
                new Binding("Item"));
            presenterFactory.SetBinding(
                TreeListItemPresenter.IsSpinAnimationVisibleProperty,
                new Binding("IsSpinAnimationVisible"));
            presenterFactory.SetBinding(
                TreeListItemPresenter.ExpandedIconProperty,
                new Binding("Item.ExpandedIcon"));
            presenterFactory.SetBinding(
                TreeListItemPresenter.IconProperty,
                new Binding("Item.Icon"));
            presenterFactory.SetBinding(
                TreeListItemPresenter.StateIconProperty,
                new Binding("Item.StateIcon"));
            presenterFactory.SetBinding(
                TextElement.FontStyleProperty,
                new Binding("Item.FontStyle"));
            presenterFactory.SetBinding(
                TextElement.FontWeightProperty,
                new Binding("Item.FontWeight"));
            presenterFactory.SetBinding(
                TreeListItemPresenter.TextProperty,
                new Binding("Item.Text"));
            presenterFactory.SetBinding(
                TreeListItemPresenter.GlyphBrushProperty,
                this.CreateRelativeBinding("Foreground"));
            presenterFactory.SetBinding(
                TreeListItemPresenter.IsExpandableProperty,
                this.CreateRelativeBinding("IsExpandable"));
            presenterFactory.SetBinding(
                TreeListItemPresenter.IsSelectedProperty,
                this.CreateRelativeBinding("IsSelected"));
            presenterFactory.SetBinding(
                TreeListItemPresenter.SnapsToDevicePixelsProperty,
                this.CreateRelativeBinding("SnapsToDevicePixels"));
            presenterFactory.SetBinding(
                TreeListItemPresenter.UseLayoutRoundingProperty,
                this.CreateRelativeBinding("UseLayoutRounding"));
            presenterFactory.SetResourceReference(
                TreeListItemPresenter.GlyphMouseOverBrushProperty,
                ThemeBrushes.ItemBackgroundActiveSelectedKey);

            if (this.ExpansionContentPath != null)
            {
                presenterFactory.SetBinding(
                    TreeListItemPresenter.ContentProperty,
                    new Binding(this.ExpansionContentPath)
                    {
                        UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
                    });
            }

            presenterFactory.SetBinding(
                TreeListItemPresenter.ContentTemplateSelectorProperty,
                new Binding("ExpansionContentTemplateSelector")
                {
                    Source = this
                });

            return presenterFactory;
        }

        /// <summary>
        /// Creates a binding for the specified path that is relative to the parent tree view
        /// item control.
        /// </summary>
        /// <param name="path">
        /// The path to set the binding to.
        /// </param>
        /// <returns>
        /// The new binding object.
        /// </returns>
        private Binding CreateRelativeBinding(string path)
        {
            RelativeSource source = new RelativeSource(
                RelativeSourceMode.FindAncestor, typeof(RsVirtualisedTreeViewItem), 1);

            Binding binding = new Binding(path);
            binding.RelativeSource = source;
            return binding;
        }

        /// <summary>
        /// Creates the trigger that contains setters used when the presenter is currently
        /// selected.
        /// </summary>
        /// <returns>
        /// The new trigger object.
        /// </returns>
        private Trigger CreateSelectionTrigger()
        {
            Trigger trigger = new Trigger();
            trigger.SourceName = "Presenter";
            trigger.Property = TreeListItemPresenter.IsSelectedProperty;
            trigger.Value = true;

            trigger.Setters.Add(this.CreateGlyphBrushSetter(true));
            trigger.Setters.Add(this.CreateGlyphMouseOverBrushSetter(true));
            return trigger;
        }

        /// <summary>
        /// Creates the data template that is used for the cells inside the column that
        /// contains the tree view expanders.
        /// </summary>
        /// <returns>
        /// The new data template object.
        /// </returns>
        private DataTemplate CreateTemplateForExpansionColumn()
        {
            DataTemplate template = new DataTemplate();
            template.VisualTree = this.CreateTreeItemPresenterFactory();
            template.Triggers.Add(this.CreateSelectionTrigger());
            template.Triggers.Add(this.CreateInactiveTrigger());

            return template;
        }

        /// <summary>
        /// Called whenever the column collection changes. This passes the changes to the
        /// actual column collection.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Collections.Specialized.NotifyCollectionChangedEventArgs containing the
        /// event data.
        /// </param>
        private void OnColumnsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    if (e.NewItems != null)
                    {
                        foreach (GridViewColumn column in e.NewItems)
                        {
                            this._columns.Add(column);
                        }
                    }

                    break;

                case NotifyCollectionChangedAction.Remove:
                    if (e.OldItems != null)
                    {
                        foreach (GridViewColumn column in e.OldItems)
                        {
                            this._columns.Remove(column);
                        }
                    }

                    break;

                case NotifyCollectionChangedAction.Reset:
                    this._columns.Clear();
                    break;

                case NotifyCollectionChangedAction.Replace:
                    if (e.OldItems != null)
                    {
                        foreach (GridViewColumn column in e.OldItems)
                        {
                            this._columns.Remove(column);
                        }
                    }

                    if (e.NewItems != null)
                    {
                        foreach (GridViewColumn column in e.NewItems)
                        {
                            this._columns.Add(column);
                        }
                    }

                    break;
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsTreeListView {Class}
} // RSG.Editor.Controls {Namespace}
