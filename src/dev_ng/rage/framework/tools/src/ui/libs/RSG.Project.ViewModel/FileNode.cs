﻿//---------------------------------------------------------------------------------------------
// <copyright file="FolderNode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel
{
    using System;
    using System.ComponentModel;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using RSG.Editor;
    using RSG.Editor.Controls.Perforce;
    using RSG.Editor.View;
    using RSG.Project.Model;
    using RSG.Project.ViewModel.Controllers;

    /// <summary>
    /// Represents an item in the project explorer tree view that is associated with a physical
    /// file on the users local machine and shows a document when invoked/opened.
    /// </summary>
    public class FileNode : DocumentNode
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="FileNode"/> class.
        /// </summary>
        protected FileNode()
        {
            this.Icon = ProjectIcons.GenericFile;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="FileNode"/> class.
        /// </summary>
        /// <param name="model">
        /// The model that this view model will be wrapping.
        /// </param>
        public FileNode(IHierarchyNode parent, ProjectItem model)
            : base(parent, model)
        {
            this.Icon = ProjectIcons.GenericFile;
            int index = model.Include.LastIndexOf('\\') + 1;
            this.Text = model.Include.Substring(index);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether this hierarchy nodes data can be saved.
        /// </summary>
        public override bool CanBeSaved
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether this hierarchy node can be removed from the parent
        /// items scope it belongs to.
        /// </summary>
        public override bool CanBeRemoved
        {
            get { return true; }
        }

        /// <summary>
        /// Gets the creates document content controller that can be used to create a content
        /// object for this object inside a document tab.
        /// </summary>
        public override ICreateDocumentContentController CreateDocumentContentController
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Gets a value indicating whether this item is expanded when the user double clicks
        /// on it.
        /// </summary>
        public override bool ExpandOnDoubleClick
        {
            get { return false; }
        }

        /// <summary>
        /// Gets the full path to the file this node is representing.
        /// </summary>
        public virtual string FullPath
        {
            get
            {
                return this.ProjectItem.GetMetadata("FullPath");
            }

            set
            {
                string fullPath = this.ProjectItem.GetMetadata("FullPath");
                if (String.Equals(fullPath, value))
                {
                    return;
                }

                this.ProjectItem.UpdateIncludeValueFromPathChange(value);
                if (this.ProjectItemScopeNode != null)
                {
                    this.ProjectItemScopeNode.IsModified = true;
                }
            }
        }

        /// <summary>
        /// Gets the invoke controller that can be used to perform a action on this object.
        /// </summary>
        public override IInvokeController InvokeController
        {
            get { return new FileNodeInvokeController(); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Called whenever a property inside the model changes.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The data for this event including the name of the property that changed.
        /// </param>
        protected override void OnModelPropertyChanged(object s, PropertyChangedEventArgs e)
        {
            if (String.Equals(e.PropertyName, "Include"))
            {
                int index = this.Model.Include.LastIndexOf('\\') + 1;
                this.Text = this.Model.Include.Substring(index);
            }
        }

        /// <summary>
        /// Updates the state icons and tooltip based on the specified source control metadata.
        /// </summary>
        /// <param name="metadata">
        /// The source control metadata for this file.
        /// </param>
        public void UpdatePerforceState(IPerforceFileMetaData metadata)
        {
            DispatcherHelper.CheckBeginInvokeOnUI(new Action(() =>
            {
                if (metadata != null && metadata.IsMapped)
                {
                    if (metadata.HeadAction == PerforceFileAction.Delete && metadata.HaveRevision == -1)
                    {
                        this.OverlayIcon = null;
                        this.Icon = ProjectIcons.InvalidLinkedItem;
                    }
                    else if (metadata.HeadRevision != -1)
                    {
                        if (metadata.HeadRevision == metadata.HaveRevision)
                        {
                            this.OverlayIcon = PerforceIcons.GotLatestOverlay;
                        }
                        else
                        {
                            this.OverlayIcon = PerforceIcons.NotLatestOverlay;
                        }
                    }
                    else
                    {
                        this.OverlayIcon = null;
                    }

                    int width = 7;
                    int height = 16;
                    PixelFormat format = PixelFormats.Bgra32;
                    int stride = width * (format.BitsPerPixel / 8);
                    double dpi = 96.0;
                    byte[] pixels = new byte[stride * height];
                    if (metadata.Action == PerforceFileAction.Edit)
                    {
                        PerforceIcons.CheckedOutState.CopyPixels(pixels, stride, 0);
                    }
                    else if (metadata.Action == PerforceFileAction.Add)
                    {
                        PerforceIcons.AddState.CopyPixels(pixels, stride, 0);
                    }

                    this.StateIcon = BitmapSource.Create(
                        width, height, dpi, dpi, PixelFormats.Bgra32, null, pixels, stride);
                }
                else
                {
                    this.StateIcon = null;
                    this.OverlayIcon = null;
                }
            }));
        }
        #endregion Methods
    } // RSG.Project.ViewModel.FileNode {Class}
} // RSG.Project.ViewModel {Namespace}
