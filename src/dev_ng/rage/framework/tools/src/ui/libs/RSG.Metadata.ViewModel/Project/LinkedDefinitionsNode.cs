﻿//---------------------------------------------------------------------------------------------
// <copyright file="LinkedDefinitionsNode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Project
{
    using System.Collections.Generic;
    using System.IO;
    using RSG.Project.ViewModel;

    /// <summary>
    /// The node inside the project explorer that represents a standard metadata project. This
    /// class cannot be inherited.
    /// </summary>
    public sealed class LinkedDefinitionsNode : CoreFolderNode
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="LinkedDefinitionsNode"/> class.
        /// </summary>
        /// <param name="parent">
        /// The parent project node that created this node.
        /// </param>
        public LinkedDefinitionsNode(ProjectNode parent)
            : base(parent)
        {
            this.Text = "ParCodeGen Definitions";
            this.Icon = Icons.DefinitionsNodeIcon;
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Creates the child elements for this view model. This is only called once the item
        /// has been expanded in the user interface.
        /// </summary>
        /// <param name="collection">
        /// The collection to add the elements to.
        /// </param>
        protected override void CreateChildren(ICollection<IHierarchyNode> collection)
        {
            ProjectNode project = this.ParentProject;
            if (project == null)
            {
                return;
            }

            string definitionRoot = project.GetProperty("DefinitionRoot");
            string directoryName = Path.GetDirectoryName(project.FullPath);
            definitionRoot = Path.Combine(directoryName, definitionRoot);
            definitionRoot = Path.GetFullPath(definitionRoot);

            if (Directory.Exists(definitionRoot))
            {
                string value = project.GetProperty("IncludeSubDirectories");
                bool includeSubDirectories = false;
                if (!bool.TryParse(value, out includeSubDirectories))
                {
                    includeSubDirectories = false;
                }

                if (includeSubDirectories)
                {
                    string[] directories = System.IO.Directory.GetDirectories(definitionRoot);
                    foreach (string directory in directories)
                    {
                        collection.Add(new DefinitionsFolderNode(directory, this));
                    }
                }

                string[] files = System.IO.Directory.GetFiles(definitionRoot, "*.psc");
                foreach (string file in files)
                {
                    collection.Add(new DefinitionsFileNode(file, this));
                }
            }
        }
        #endregion Methods
    } // RSG.Metadata.ViewModel.Project.LinkedDefinitionsNode {Class}
} // RSG.Metadata.ViewModel.Project {Namespace}
