//---------------------------------------------------------------------------------------------
// <copyright file="RsConsoleApplication.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Base.Logging.Universal;
    using RSG.Base.OS;
    using RSG.Configuration;
    using RSG.Configuration.Bugstar;
    using RSG.Editor.Controls.Exceptions;
    using RSG.Editor.Controls.Helpers;
    using RSG.Interop.Microsoft.Windows;
    using RSG.Services.Configuration;
    using RSG.Services.Configuration.Statistics;
    using RSG.Services.Statistics.Consumers;

    /// <summary>
    /// Provides an abstract base class for a console based application that supports
    /// displaying editor framework based windows.
    /// </summary>
    public abstract class RsConsoleApplication : IApplicationInfoProvider, IBugstarInfoProvider
    {
        #region Fields
        /// <summary>
        /// The time zone code to use to retrieve the assemblies build time.
        /// </summary>
        private const string TimeZoneCode = "GMT Standard Time";

        /// <summary>
        /// Reference to the application log.
        /// </summary>
        protected readonly IUniversalLog Log;

        /// <summary>
        /// Unique application session identifier used for stats tracking purposes.
        /// </summary>
        private readonly Guid _applicationSessionGuid = Guid.NewGuid();

        /// <summary>
        /// A value indicating whether the application session was tracked.
        /// </summary>
        private bool _applicationSessionTracked = false;

        /// <summary>
        /// The private field used for the <see cref="BugstarComponent"/> property.
        /// </summary>
        private string _bugstarComponent;

        /// <summary>
        /// The private field used for the <see cref="BugstarProjectName"/> property.
        /// </summary>
        private string _bugstarProjectName;

        /// <summary>
        /// The private field used for the <see cref="CommandOptions"/> property.
        /// </summary>
        private CommandOptions _commandOptions;

        /// <summary>
        /// The private field used for the <see cref="DefaultBugOwner"/> property.
        /// </summary>
        private string _defaultBugOwner;

        /// <summary>
        /// A private array containing the command-line options that will be parsed by the
        /// start up method.
        /// </summary>
        private readonly IList<LongOption> _options = new List<LongOption>();

        /// <summary>
        /// The private field used for the <see cref="StatisticsServer"/> property.
        /// </summary>
        private IServer _statisticsServer;

        /// <summary>
        /// The private field used for the <see cref="CommandLineOptions"/> property.
        /// </summary>
        private Getopt _commandLineOptions;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsConsoleApplication" /> class.
        /// </summary>
        static RsConsoleApplication()
        {
            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsConsoleApplication"/> class.
        /// </summary>
        public RsConsoleApplication()
        {
            this.Log = LogFactory.ApplicationLog;

            // Register the command line arguments.
            RegisterCommandlineArg("statistics-server", LongOption.ArgType.Required, "Statistics server to connect to.");
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the name of the bugstar project that bugs should be added to.
        /// </summary>
        public string BugstarProjectName
        {
            get { return _bugstarProjectName; }
        }

        /// <summary>
        /// Gets the name of the bugstar component that bugs should have their component
        /// set to.
        /// </summary>
        public string BugstarComponent
        {
            get { return _bugstarComponent; }
        }

        /// <summary>
        /// Gets the command line options that are passed into this instance of the
        /// application on start-up.
        /// </summary>
        [Obsolete("Use CommandOptions Instead")]
        public Getopt CommandLineOptions
        {
            get
            {
                if (this._commandLineOptions == null)
                {
                    Debug.Fail(
                        "Attempting to access command line options prior to the application " +
                        "being initialised");

                    throw new ArgumentNullException(
                        "Attempting to access command line options prior to the application " +
                        "being initialised");
                }

                return this._commandLineOptions;
            }
        }

        /// <summary>
        /// Gets the command line options that are passed into this instance of the
        /// application on start-up.
        /// </summary>
        public CommandOptions CommandOptions
        {
            get
            {
                if (this._commandOptions == null)
                {
                    Debug.Fail(
                        "Attempting to access command line options prior to the application " +
                        "being initialised");

                    throw new ArgumentNullException(
                        "Attempting to access command line options prior to the application " +
                        "being initialised");
                }

                return this._commandOptions;
            }
        }

        /// <summary>
        /// Gets the name of the default owner to assign bugs to.
        /// </summary>
        public string DefaultBugOwner
        {
            get { return _defaultBugOwner; }
        }

        /// <summary>
        /// Gets a flag containing the buttons to display in the unhandled exception window
        /// dialog.
        /// </summary>
        protected virtual ExceptionWindowButtons ExceptionWindowButtons
        {
            get { return ExceptionWindowButtons.Default; }
        }

        /// <summary>
        /// Gets the product name in the assembly information of the entry assembly. This value
        /// is used as a friendly name display on the different dialogues and splash screen.
        /// </summary>
        public string ProductName
        {
            get
            {
                Type type = this.GetType();
                string identifier = type.GUID.ToString("B");
                Assembly assembly = type.Assembly;

                string defaultValue = "Unknown Product - {0}".FormatInvariant(identifier);
                return AssemblyInfo.GetProductName(assembly, defaultValue);
            }
        }

        /// <summary>
        /// Gets the statistics server configuration object.
        /// </summary>
        public IServer StatisticsServer
        {
            get { return _statisticsServer; }
        }

        /// <summary>
        /// Gets a flag indicating whether we wish to track the application session on
        /// the statistics server.
        /// </summary>
        public virtual bool TrackSessionStatistics
        {
            get { return false; }
        }

        /// <summary>
        /// Gets the application version.
        /// </summary>
        public string Version
        {
            get
            {
                Type type = this.GetType();
                Assembly assembly = type.Assembly;
                return AssemblyInfo.GetVersion(assembly).ToString();
            }
        }

        /// <summary>
        /// Gets a semi-colon separate list of the application authors email addresses. These
        /// are the addresses which are used by the unhandled exception dialog to determine
        /// where the mail should be sent by default.
        /// e.g.
        /// *tools@rockstarnorth.com
        /// michael.taschler@rockstarnorth.com;dave.evans@rockstarnorth.com.
        /// </summary>
        public virtual string AuthorEmailAddress
        {
            get { return String.Join(";", CommandOptions.Project.ToolsEmailAddresses); }
        }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Starts the console application.
        /// </summary>
        public int Run()
        {
            int returnCode = 0;

            try
            {
                if (ConsoleStartup())
                {
                    returnCode = ConsoleMain();
                }
            }
            catch (System.Exception ex)
            {
                Log.ToolException(ex, "An unhandled exception occurred while running the application.");

                RsExceptionWindow window =
                    new RsExceptionWindow(
                        ex,
                        this,
                        this,
                        this.ExceptionWindowButtons);
                window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                window.ShowDialog();
                returnCode = 1;
            }
            finally
            {
                // Don't perform any logging after calling ConsoleShutdown() as the
                // LogFactory has already been shutdown.
                ConsoleShutdown(returnCode);
            }

            Environment.ExitCode = returnCode;
            return returnCode;
        }

        /// <summary>
        /// When overridden in derived classes, runs the console application's core logic.
        /// </summary>
        /// <returns>
        /// The exit code for the console application.
        /// </returns>
        protected abstract int ConsoleMain();

        /// <summary>
        /// Allows derived classes to execute code prior to the <see cref="ConsoleMain"/>
        /// method being called.
        /// </summary>
        /// <returns>
        /// Value indicating whether we should execute the <see cref="ConsoleMain"/>
        /// method.
        /// </returns>
        protected virtual bool OnConsoleStartup()
        {
            return true;
        }

        /// <summary>
        /// Allows derived classes to execute code prior to the <see cref="ConsoleMain"/>
        /// method being called.
        /// </summary>
        protected virtual void OnConsoleShutdown()
        {
        }

        /// <summary>
        /// Method for registering command line arguments. Note that arguments must be
        /// registered prior to initialising the application.
        /// </summary>
        /// <param name="option">
        /// The long option string for the command-line argument.
        /// </param>
        /// <param name="type">
        /// The flag specifying the <see cref="LongOption.ArgType"/> for the argument.
        /// </param>
        /// <param name="description">
        /// A optional description string for the command-line argument.
        /// </param>
        protected void RegisterCommandlineArg(
            string option, LongOption.ArgType type, string description)
        {
            RegisterCommandlineArg(new LongOption(option, type, description));
        }

        /// <summary>
        /// Method for registering command line arguments. Note that arguments must be
        /// registered prior to initialising the application.
        /// </summary>
        /// <param name="option">LongOption object.</param>
        /// 
        protected void RegisterCommandlineArg(LongOption option)
        {
            if (this._commandLineOptions != null)
            {
                throw new NotSupportedException(
                    "It is not possible to add additional command line args once the " +
                    "application has been initialised.  Add options in either the " +
                    "application's constructor or prior to application initialisation.");
            }

            this._options.Add(option);
        }

        /// <summary>
        /// Called prior to the <see cref="ConsoleMain"/> method.
        /// </summary>
        /// <returns>
        /// Value indicating whether we should execute the <see cref="ConsoleMain"/>
        /// method.
        /// </returns>
        private bool ConsoleStartup()
        {
            LogApplicationInformation();

            String[] args = Environment.GetCommandLineArgs().Skip(1).ToArray();
            this._commandLineOptions = new Getopt(args, this._options.ToArray());
            this._commandOptions = new CommandOptions(args, this._options.ToArray());

            // Set up the bugstar component for this app.
            IBugstarConfig bugstarConfig =
                ConfigFactory.CreateBugstarConfig(Log, this._commandOptions.Project);
            this._bugstarProjectName = bugstarConfig.ProjectName;
            this._defaultBugOwner = bugstarConfig.DefaultBugOwner;
            bugstarConfig.ApplicationComponents.TryGetValue(this.ProductName, out this._bugstarComponent);

            // Set up the statistics configuration.
            StatisticsConfig statisticsConfig = new StatisticsConfig(_commandOptions.Config);
            IServer serverConfig = statisticsConfig.DefaultServer;
            if (_commandOptions.ContainsOption("statistics-server"))
            {
                string serverName = (string)_commandOptions["statistics-server"];
                serverConfig = statisticsConfig.GetServerByName(serverName);
                if (serverConfig == null)
                {
                    Log.Warning("Unable to find a server named '{0}' in the statistics config file.  Falling back on the default server.",
                        serverName);
                    serverConfig = statisticsConfig.DefaultServer;
                }
            }
            _statisticsServer = serverConfig;

            // Check whether we just wish to show some help or if we actually want to run the application.
            if (CommandOptions.ShowHelp)
            {
                ShowUsage();
                return false;
            }
            else
            {
#if !DEBUG
                // Only track stats for Release builds that aren't attached to the debugger.
                if (TrackSessionStatistics && !Debugger.IsAttached)
                {
                    TrackApplicationSession();
                }
#endif
            }

            return this.OnConsoleStartup();
        }

        /// <summary>
        /// Shuts down the log factory.
        /// </summary>
        /// <param name="returnCode"></param>
        private void ConsoleShutdown(int returnCode)
        {
            this.OnConsoleShutdown();

#if !DEBUG
            // Only track stats for Release builds that aren't attached to the debugger.
            if (TrackSessionStatistics && !Debugger.IsAttached)
            {
                TrackApplicationExit(returnCode);
            }
#endif
            LogFactory.ApplicationShutdown();
        }

        /// <summary>
        /// Utility method for outputting some basic application information
        /// to the log.
        /// </summary>
        private void LogApplicationInformation()
        {
            Type type = this.GetType();
            Assembly assembly = type.Assembly;

            DateTime buildTime;
            try
            {
                buildTime = AssemblyInfo.GetBuildTime(assembly, TimeZoneCode);
            }
            catch (InvalidTimeZoneException)
            {
                Debug.Assert(false, "Unable to retrieve the GMT version of the build time.");
                buildTime = AssemblyInfo.GetUtcBuildTime(assembly);
            }

            string commandline = String.Join(" ", Environment.GetCommandLineArgs());

            Log.Message("Commandline {0}", commandline);
            Log.Message("Current Directory {0}", Directory.GetCurrentDirectory());
            Log.Message(AssemblyInfo.GetProductName(assembly, "Unknown Product"));
            Log.Message("{0} Version - {1}",
                AssemblyInfo.GetConfiguration(assembly, "Unknown"),
                AssemblyInfo.GetVersion(assembly));
            Log.Message("Build Time {0} (GMT)", buildTime);
            Log.Message(AssemblyInfo.GetEdition(assembly));
        }

        /// <summary>
        /// Print usage information to console output (supplying additional 
        /// trailing argument information).
        /// </summary>
        /// <param name="trailingArg">Trailing argument name.</param>
        /// <param name="trailingArgDescription">Trailing argument description.</param>
        protected void ShowUsage(String trailingArg, String trailingArgDescription)
        {
            Type type = this.GetType();
            Assembly assembly = type.Assembly;

            LogApplicationInformation();
            Log.Message("Description:");
            Log.Message(AssemblyInfo.GetDescription(assembly, String.Empty));
            Log.Message("Usage:");

            String usage = CommandOptions.GetUsage(trailingArg, trailingArgDescription);
            foreach (String usageLine in usage.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries))
            {
                Log.Message(usageLine);
            }
        }

        /// <summary>
        /// Print usage information to console output.
        /// </summary>
        protected void ShowUsage()
        {
            ShowUsage(String.Empty, String.Empty);
        }

        /// <summary>
        /// Split's a command string into individual tokens while respecting quoted
        /// parameters and escaped quotes and backslashes.
        /// </summary>
        /// <param name="commandString"></param>
        /// <returns></returns>
        protected string[] SplitCommandString(string commandString)
        {
            IList<string> tokens = new List<string>();
            StringBuilder argBuilder = new StringBuilder();
            bool lookingForEndQuote = false;

            for (int i = 0; i < commandString.Length; ++i)
            {
                char theChar = commandString[i];
                char nextChar = i < commandString.Length - 1 ? commandString[i + 1] : default(char);

                if (theChar == '\\' && (nextChar == '"' || nextChar == '\\'))
                {
                    if (nextChar == '"')
                    {
                        argBuilder.Append('"');
                    }
                    else if (nextChar == '\\')
                    {
                        argBuilder.Append('\\');
                    }
                    ++i;
                }
                else if (theChar == '"')
                {
                    if (lookingForEndQuote)
                    {
                        tokens.Add(argBuilder.ToString());
                        argBuilder.Clear();
                    }
                    lookingForEndQuote = !lookingForEndQuote;
                }
                else if (Char.IsWhiteSpace(theChar) && !lookingForEndQuote)
                {
                    if (argBuilder.Length > 0)
                    {
                        tokens.Add(argBuilder.ToString());
                        argBuilder.Clear();
                    }
                }
                else
                {
                    argBuilder.Append(theChar);
                }
            }

            // Keep track of the final argument.
            if (argBuilder.Length > 0)
            {
                tokens.Add(argBuilder.ToString());
            }

            // Return the tokens as an array.
            return tokens.ToArray();
        }

        /// <summary>
        /// Sends the application session exit information to the statistics server.
        /// </summary>
        private void TrackApplicationExit(int returnCode)
        {
            if (!this._applicationSessionTracked)
            {
                // We didn't track the startup, so don't bother tracking the exit.
                return;
            }

            this.Log.Message("Tracking application session exit info.");
            try
            {
                ApplicationSessionConsumer consumer =
                    new ApplicationSessionConsumer(this._statisticsServer);
                consumer.TrackApplicationExit(
                    this._applicationSessionGuid,
                    returnCode,
                    DateTime.UtcNow);
            }
            catch (Exception e)
            {
                this.Log.ToolException(
                    e,
                    "Unable to track application exit stat due to an unhandled exception.");
            }
        }

        /// <summary>
        /// Sends the application session information to the statistics server.
        /// </summary>
        private void TrackApplicationSession()
        {
            Log.Message("Tracking application session {0}.", _applicationSessionGuid);
            try
            {
                Process currentProcess = Process.GetCurrentProcess();
                ProcessModule mainModule = currentProcess.MainModule;

                ApplicationSessionConsumer consumer = new ApplicationSessionConsumer(_statisticsServer);
                consumer.TrackApplicationSession(
                    _applicationSessionGuid,
                    null,
                    ProductName,
                    Version,
                    mainModule.FileName,
                    currentProcess.StartTime.ToUniversalTime(),
                    CommandOptions.Project);
                this._applicationSessionTracked = true;
            }
            catch (Exception e)
            {
                this._applicationSessionTracked = false;
                Log.ToolException(e, "Unable to track application session stat due to an unhandled exception.");
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsConsoleApplication {Class}
} // RSG.Editor.Controls {Namespace}
