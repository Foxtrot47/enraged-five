﻿//---------------------------------------------------------------------------------------------
// <copyright file="SplitterLengthConverter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock
{
    using System;
    using System.ComponentModel;
    using System.ComponentModel.Design.Serialization;
    using System.Globalization;
    using System.Reflection;
    using RSG.Editor;

    /// <summary>
    /// A type converter that defines the conversion between a numerical value or a string
    /// value into a <see cref="SplitterLength"/> value.
    /// </summary>
    public class SplitterLengthConverter : TypeConverter
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SplitterLengthConverter"/> class.
        /// </summary>
        public SplitterLengthConverter()
        {
        }
        #endregion

        #region Methods
        /// <summary>
        /// Determines whether the specified source type can be converter to a
        /// <see cref="SplitterLength"/> value.
        /// </summary>
        /// <param name="context">
        /// An System.ComponentModel.ITypeDescriptorContext that provides a format context.
        /// </param>
        /// <param name="sourceType">
        /// A System.Type that represents the type you want to convert from.
        /// </param>
        /// <returns>
        /// True if the specified source type can be converted into a
        /// <see cref="SplitterLength"/> value; otherwise, false.
        /// </returns>
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            switch (Type.GetTypeCode(sourceType))
            {
                case TypeCode.Int16:
                case TypeCode.UInt16:
                case TypeCode.Int32:
                case TypeCode.UInt32:
                case TypeCode.Int64:
                case TypeCode.UInt64:
                case TypeCode.Single:
                case TypeCode.Double:
                case TypeCode.Decimal:
                case TypeCode.String:
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Determines whether this converter can convert a <see cref="SplitterLength"/> value
        /// into a value of
        /// the specified destination type.
        /// </summary>
        /// <param name="context">
        /// An System.ComponentModel.ITypeDescriptorContext that provides a format context.
        /// </param>
        /// <param name="destinationType">
        /// A System.Type that represents the type you want to convert to.
        /// </param>
        /// <returns>
        /// True if a <see cref="SplitterLength"/> value can be converted into the specified
        /// type; otherwise, false.
        /// </returns>
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType == typeof(string))
            {
                return true;
            }

            return !(destinationType != typeof(InstanceDescriptor));
        }

        /// <summary>
        /// Converts the given object to a <see cref="SplitterLength"/> value, using the
        /// specified context and culture information.
        /// </summary>
        /// <param name="context">
        /// An System.ComponentModel.ITypeDescriptorContext that provides a format context.
        /// </param>
        /// <param name="culture">
        /// The System.Globalization.CultureInfo to use as the current culture.
        /// </param>
        /// <param name="value">
        /// The System.Object to convert.
        /// </param>
        /// <returns>
        /// A <see cref="SplitterLength"/> value that represents the converted value.
        /// </returns>
        /// <exception cref="System.NotSupportedException">
        /// Thrown if the conversion cannot be performed.
        /// </exception>
        public override object ConvertFrom(
            ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (value == null || !base.CanConvertFrom(value.GetType()))
            {
                throw this.GetConvertFromException(value);
            }

            if (value is string)
            {
                return this.FromString((string)value, culture);
            }

            double num = Convert.ToDouble(value, culture);
            if (double.IsNaN(num))
            {
                num = 1.0;
            }

            return new SplitterLength(num, SplitterUnitType.Stretch);
        }

        /// <summary>
        /// Converts the given value object to the specified type, using the specified context
        /// and culture information.
        /// </summary>
        /// <param name="context">
        /// An System.ComponentModel.ITypeDescriptorContext that provides a format context.
        /// </param>
        /// <param name="culture">
        /// A System.Globalization.CultureInfo. If null is passed, the current culture is
        /// assumed.
        /// </param>
        /// <param name="value">
        /// The System.Object to convert.
        /// </param>
        /// <param name="destinationType">
        ///  The System.Type to convert the value parameter to.
        /// </param>
        /// <returns>
        /// An System.Object that represents the converted value.
        /// </returns>
        /// <exception cref="RSG.Editor.SmartArgumentNullException">
        /// Thrown if the destinationType parameter is null.
        /// </exception>
        /// <exception cref="System.NotSupportedException">
        /// Thrown if the conversion cannot be performed.
        /// </exception>
        public override object ConvertTo(
            ITypeDescriptorContext context,
            CultureInfo culture,
            object value,
            Type destinationType)
        {
            if (destinationType == null)
            {
                throw new SmartArgumentNullException(() => destinationType);
            }

            if (value != null && value is SplitterLength)
            {
                SplitterLength length = (SplitterLength)value;
                if (destinationType == typeof(string))
                {
                    return this.ToString(length, culture);
                }

                if (destinationType.IsEquivalentTo(typeof(InstanceDescriptor)))
                {
                    Type type = typeof(SplitterLength);
                    ConstructorInfo constructor = type.GetConstructor(
                        new Type[] { typeof(double), typeof(SplitterUnitType) });
                    return new InstanceDescriptor(
                        constructor,
                        new object[] { length.Value, length.SplitterUnitType });
                }
            }

            throw this.GetConvertToException(value, destinationType);
        }

        /// <summary>
        /// Converts the given object to a <see cref="SplitterLength"/> value, using the
        /// specified string value and culture information.
        /// </summary>
        /// <param name="s">
        /// The System.String object to convert.
        /// </param>
        /// <param name="cultureInfo">
        /// The System.Globalization.CultureInfo to use as the current culture.
        /// </param>
        /// <returns>
        /// A <see cref="SplitterLength"/> value that represents the converted value.
        /// </returns>
        /// <exception cref="System.FormatException">
        /// Thrown if s is not a number in a valid format.
        /// </exception>
        /// <exception cref="System.OverflowException">
        /// Thrown if s represents a number that is less than System.Double.MinValue or greater
        /// than System.Double.MaxValue.
        /// </exception>
        internal SplitterLength FromString(string s, CultureInfo cultureInfo)
        {
            string text = s.Trim();
            double value = 1.0;
            SplitterUnitType unitType = SplitterUnitType.Stretch;
            if (text == "*")
            {
                unitType = SplitterUnitType.Fill;
            }
            else if (text.EndsWith("*"))
            {
                unitType = SplitterUnitType.Fill;
                value = Convert.ToDouble(text.Substring(0, text.Length - 1), cultureInfo);
            }
            else
            {
                value = Convert.ToDouble(text, cultureInfo);
            }

            return new SplitterLength(value, unitType);
        }

        /// <summary>
        /// Converts the specified <see cref="SplitterLength"/> value into a string value.
        /// </summary>
        /// <param name="length">
        /// The <see cref="SplitterLength"/> value to convert to a string.
        /// </param>
        /// <param name="cultureInfo">
        /// The System.Globalization.CultureInfo to use as the current culture.
        /// </param>
        /// <returns>
        /// A string value that represents the converted <see cref="SplitterLength"/>.
        /// </returns>
        internal string ToString(SplitterLength length, CultureInfo cultureInfo)
        {
            if (length.SplitterUnitType == SplitterUnitType.Fill)
            {
                return "*";
            }

            return Convert.ToString(length.Value, cultureInfo);
        }
        #endregion
    } // RSG.Editor.Controls.Dock.SplitterLengthConverter {Class}
} // RSG.Editor.Controls.Dock {Namespace}
