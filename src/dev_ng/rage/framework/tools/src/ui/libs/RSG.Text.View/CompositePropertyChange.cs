﻿//---------------------------------------------------------------------------------------------
// <copyright file="CompositePropertyChange.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.View
{
    using System.Collections.Generic;
    using System.Windows.Controls;
    using RSG.Text.ViewModel;

    /// <summary>
    /// A composite change object that represents a change to a single property on n-number of
    /// <see cref="LineViewModel"/> objects.
    /// </summary>
    public abstract class CompositePropertyChange
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Binding"/> property.
        /// </summary>
        private string _binding;

        /// <summary>
        /// The private field used for the <see cref="Header"/> property.
        /// </summary>
        private string _header;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CompositePropertyChange"/> class.
        /// </summary>
        /// <param name="header">
        /// The header value for this property, can be thought of as the friendly name of the
        /// property.
        /// </param>
        /// <param name="binding">
        /// The binding path to the property through a line view model object.
        /// </param>
        public CompositePropertyChange(string header, string binding)
        {
            this._header = header;
            this._binding = binding;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the binding path to the property through a line view model object.
        /// </summary>
        public string Binding
        {
            get { return this._binding; }
        }

        /// <summary>
        /// Gets a data grid column object that shows the property this change object
        /// represents through a line view model object.
        /// </summary>
        public virtual DataGridColumn Column
        {
            get { return null; }
        }

        /// <summary>
        /// Gets a value indicating whether this entity or any of its properties currently have
        /// an error associated with them.
        /// </summary>
        public abstract bool HasErrors { get; }

        /// <summary>
        /// Gets the header value for the property. This can also be thought of as the friendly
        /// name of the property.
        /// </summary>
        public string Header
        {
            get { return this._header; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Applies the new value for the property to the specified collection of line
        /// view model objects.
        /// </summary>
        /// <param name="lineViewModels">
        /// The line view models that the property value should be applied to.
        /// </param>
        public abstract void ApplyChange(IEnumerable<LineViewModel> lineViewModels);

        /// <summary>
        /// Override to set properties that are related to the line view models that the
        /// change will be applied to.
        /// </summary>
        /// <param name="lineViewModels">
        /// The line view models that the change will be applied to on commit.
        /// </param>
        public virtual void Initialise(IEnumerable<LineViewModel> lineViewModels)
        {
        }
        #endregion Methods
    } // RSG.Text.View.CompositePropertyChange {Class}
} // RSG.Text.View {Namespace}
