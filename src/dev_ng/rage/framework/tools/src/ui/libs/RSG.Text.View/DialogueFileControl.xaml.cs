﻿//---------------------------------------------------------------------------------------------
// <copyright file="DialogueFileControl.xaml.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.View
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using RSG.Editor;
    using RSG.Editor.Controls;
    using RSG.Editor.Controls.AttachedDependencyProperties;
    using RSG.Interop.Microsoft.Office.Word;
    using RSG.Project.Model;
    using RSG.Text.Commands;
    using RSG.Text.Model;
    using RSG.Text.View.Commands;
    using RSG.Text.ViewModel;
    using RSG.Editor.Controls.Helpers;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Controls;

    /// <summary>
    /// Defines the control that can be used to display the contains of a dialogue star file.
    /// </summary>
    public partial class DialogueFileControl : RsUserControl
    {
        #region Fields
        /// <summary>
        /// The routed command used to open the editor for the character mappings.
        /// </summary>
        private static RoutedCommand _openCharaterMapper;

        /// <summary>
        /// The layout data for the conversation control.
        /// </summary>
        private string _conversationLayoutData;

        /// <summary>
        /// The private reference to the dialogue view model that this control is showing the
        /// data of.
        /// </summary>
        private DialogueViewModel _dialogueViewModel;

        /// <summary>
        /// The layout data for the line control.
        /// </summary>
        private string _lineLayoutData;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DialogueFileControl"/> class.
        /// </summary>
        static DialogueFileControl()
        {
            _openCharaterMapper = new RoutedCommand();
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="DialogueFileControl"/> class.
        /// </summary>
        public DialogueFileControl()
        {
            this.InitializeComponent();
            this.AttachCommandBindings();

            EnhancedFocusScope.SetIsEnhancedFocusScope(this, true);
            EnhancedFocusScope.SetInitialFocusedElement(this, this.ConversationList);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="DialogueFileControl"/> class.
        /// </summary>
        public DialogueFileControl(DialogueViewModel viewModel)
        {
            this._dialogueViewModel = viewModel;
            this.DataContext = viewModel;
            this.InitializeComponent();
            this.AttachCommandBindings();

            this.CommandBindings.Add(
                new CommandBinding(OpenCharaterMapper, OnOpenCharacterMapperCmdExecuted));

            EnhancedFocusScope.SetIsEnhancedFocusScope(this, true);
            EnhancedFocusScope.SetInitialFocusedElement(this, this.ConversationList);

            this.Loaded += OnLoaded;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void OnOpenCharacterMapperCmdExecuted(object s, ExecutedRoutedEventArgs e)
        {
            DialogueViewModel dialogueVM = e.Parameter as DialogueViewModel;
            if (dialogueVM != null)
            {
                var mappings = dialogueVM.CharacterMappings;
                EditCharacterMappingsWindow window = new EditCharacterMappingsWindow(mappings);
                window.Owner = this.GetVisualAncestor<Window>();
                window.Title = String.Format("Edit Character Mappings For {0}", dialogueVM.Id);
                window.ShowDialog();

                foreach (ConversationViewModel conversation in dialogueVM.Conversations)
                {
                    foreach (LineViewModel line in conversation.Lines)
                    {
                        line.NotifyPropertyChanged("Speaker", "VoiceName");
                        line.Validate();
                    }
                }
            }
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the reference to the view model whose data is being shown by this view.
        /// </summary>
        public DialogueViewModel ViewModel
        {
            get { return this._dialogueViewModel ?? this.DataContext as DialogueViewModel; }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool HasBeenLoaded
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public static RoutedCommand OpenCharaterMapper
        {
            get { return _openCharaterMapper; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="stream"></param>
        public void SerialiseDocumentState(Stream stream)
        {
            if (!this.HasBeenLoaded)
            {
                return;
            }

            XmlWriterSettings settings = new XmlWriterSettings()
            {
                Indent = true
            };

            using (XmlWriter writer = XmlWriter.Create(stream, settings))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Layout");

                writer.WriteStartElement("Conversations");
                this.ConversationList.ConversationDataGrid.SerialiseLayout(writer, CultureInfo.InvariantCulture, false);
                writer.WriteEndElement();

                writer.WriteStartElement("Lines");
                this.LineList.LineDataGrid.SerialiseLayout(writer, CultureInfo.InvariantCulture, false);
                writer.WriteEndElement();

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }

        public void DeserialiseDocumentState(StreamReader stream)
        {
            using (XmlReader reader = XmlReader.Create(stream))
            {
                while (reader.MoveToContent() != XmlNodeType.EndElement)
                {
                    if (reader.NodeType == XmlNodeType.None)
                    {
                        break;
                    }

                    if (reader.Name == "Conversations")
                    {
                        if (this.ConversationList.ConversationDataGrid.IsLoaded)
                        {
                            this.ConversationList.ConversationDataGrid.DeserialiseLayout(reader, CultureInfo.InvariantCulture);
                        }
                        else
                        {
                            this._conversationLayoutData = reader.ReadOuterXml();
                            this.ConversationList.ConversationDataGrid.Loaded += this.OnConversationControlLoaded;
                        }
                    }
                    else if (reader.Name == "Lines")
                    {
                        if (this.LineList.LineDataGrid.IsLoaded)
                        {
                            this.LineList.LineDataGrid.DeserialiseLayout(reader, CultureInfo.InvariantCulture);
                        }
                        else
                        {
                            this._lineLayoutData = reader.ReadOuterXml();
                            this.LineList.LineDataGrid.Loaded += this.OnLineControlLoaded;
                        }
                    }
                    else
                    {
                        reader.Read();
                    }
                }
            }
        }

        private void OnConversationControlLoaded(object sender, RoutedEventArgs e)
        {
            DataGrid dataGrid = this.ConversationList.ConversationDataGrid;

            dataGrid.Loaded -= this.OnConversationControlLoaded;
            using (StringReader stringReader = new StringReader(this._conversationLayoutData))
            {
                using (XmlReader reader = XmlReader.Create(stringReader))
                {
                    dataGrid.DeserialiseLayout(reader, CultureInfo.InvariantCulture);
                }
            }
        }

        private void OnLineControlLoaded(object sender, RoutedEventArgs e)
        {
            DataGrid dataGrid = this.LineList.LineDataGrid;

            dataGrid.Loaded -= this.OnLineControlLoaded;
            using (StringReader stringReader = new StringReader(this._lineLayoutData))
            {
                using (XmlReader reader = XmlReader.Create(stringReader))
                {
                    dataGrid.DeserialiseLayout(reader, CultureInfo.InvariantCulture);
                }
            }
        }

        /// <summary>
        /// Attaches all of the necessary command bindings to this instance.
        /// </summary>
        private void AttachCommandBindings()
        {
            ICommandAction action = new AddNewLineAction(this.LineResolver);
            action.AddBinding(TextCommands.AddNewLine, this);

            action = new InsertNewLineAction(this.LineResolver);
            action.AddBinding(TextCommands.InsertNewLine, this);

            action = new PlayLineAction(this.LineResolver);
            action.AddBinding(TextCommands.PlayLine, this);

            action = new StopPlayingAction(this.LineResolver);
            action.AddBinding(TextCommands.StopPlaying, this);

            action = new PlayConversationAction(this.ConversationResolver);
            action.AddBinding(TextCommands.PlayConversation, this);

            action = new AddNewConversationAction(this.ConversationResolver);
            action.AddBinding(TextCommands.AddNewConversation, this);

            action = new InsertNewConversationAction(this.ConversationResolver);
            action.AddBinding(TextCommands.InsertNewConversation, this);

            action = new GenerateFilenamesAction(this.ItemItemResolver);
            action.AddBinding(TextCommands.GenerateFilenamesForConversation, this);

            action = new GenerateFilenamesAction(this.ItemItemResolver);
            action.AddBinding(TextCommands.GenerateFilenamesForConversations, this);

            action = new ValidateSFXLinesAction(this.ConversationResolver);
            action.AddBinding(TextCommands.ValidateSfxLines, this);

            action = new ValidateAgainstFolderAction(this.ConversationResolver);
            action.AddBinding(TextCommands.ValidateAgainstFolder, this);

            action = new FilterConversationsAction(this.ConversationControlsResolver);
            action.AddBinding(TextCommands.FilterConversations, this);

            action = new FilterLinesAction(this.LineControlsResolver);
            action.AddBinding(TextCommands.FilterLines, this);

            action = new CopyFilenameAction(this.LineList.CommandResolver);
            action.AddBinding(TextCommands.CopyFilename, this);

            SearchAction searchAction = new SearchAction(this.SearchResolver);
            searchAction.AddBinding(this);

            RockstarCommandManager.AddBinding(
                this,
                TextCommands.ExportDocumentLinesToWord,
                this.OnExportDocumentLinesToWord);

            RockstarCommandManager.AddBinding(
                this,
                TextCommands.ExportConversationLinesToWord,
                this.OnExportConverationLinesToWord);
        }

        private IEnumerable<DialogueConversationControl> ConversationControlsResolver(CommandData commandData)
        {
            return Enumerable.Repeat(this.ConversationList, 1);
        }

        private IEnumerable<DialogueLineControl> LineControlsResolver(CommandData commandData)
        {
            return Enumerable.Repeat(this.LineList, 1);
        }

        /// <summary>
        /// Retrieves the command argument structure that is used as a resolver to the bounded
        /// commands for this instance.
        /// </summary>
        /// <param name="commandData">
        /// The command data that has requested the arguments.
        /// </param>
        /// <returns>
        /// The command argument structure used for the command bindings.
        /// </returns>
        private TextLineCommandArgs LineResolver(CommandData commandData)
        {
            TextLineCommandArgs args = new TextLineCommandArgs();
            args.DataGrid = this.LineList.LineDataGrid;
            args.Conversation = this.ConversationList.SelectedItem as ConversationViewModel;
            args.Selected = this.LineList.SelectedItems;
            
            DialogueViewModel viewModel = this.DataContext as DialogueViewModel;
            if (viewModel != null)
            {
                args.AudioPath = (viewModel.FileNode.ProjectNode.ProjectItem as Project).GetPropertyAsFullPath("AudioPath");
            }

            return args;
        }

        /// <summary>
        /// Retrieves the command argument structure that is used as a resolver to the bounded
        /// commands for this instance.
        /// </summary>
        /// <param name="commandData">
        /// The command data that has requested the arguments.
        /// </param>
        /// <returns>
        /// The command argument structure used for the command bindings.
        /// </returns>
        private TextConversationCommandArgs ConversationResolver(CommandData commandData)
        {
            TextConversationCommandArgs args = new TextConversationCommandArgs();
            args.DataGrid = this.ConversationList.ConversationDataGrid;
            args.Dialogue = this.DataContext as DialogueViewModel;
            args.Selected = this.ConversationList.SelectedItems;

            DialogueViewModel viewModel = this.DataContext as DialogueViewModel;
            if (viewModel != null)
            {
                args.AudioPath = (viewModel.FileNode.ProjectNode.ProjectItem as Project).GetPropertyAsFullPath("AudioPath");
            }

            return args;
        }

        /// <summary>
        /// Retrieves the command argument structure that is used as a resolver to commands
        /// that want the selected conversation or all of the conversations.
        /// </summary>
        /// <param name="commandData">
        /// The command data that has requested the arguments.
        /// </param>
        /// <returns>
        /// The command argument structure used for the command bindings.
        /// </returns>
        private List<Conversation> ItemItemResolver(CommandData commandData)
        {
            if (commandData.Command == TextCommands.GenerateFilenamesForConversation)
            {
                ConversationViewModel vm =
                    this.ConversationList.SelectedItem as ConversationViewModel;
                if (vm != null)
                {
                    return new List<Conversation>() { vm.Model };
                }

                return new List<Conversation>();
            }

            List<Conversation> allItems = new List<Conversation>();
            if (this.ConversationList.ItemsSource != null)
            {
                foreach (object item in this.ConversationList.ItemsSource)
                {
                    ConversationViewModel viewModel = item as ConversationViewModel;
                    if (viewModel != null)
                    {
                        allItems.Add(viewModel.Model);
                    }
                }
            }

            return allItems;
        }

        /// <summary>
        /// Retrieves the command argument used for the search action command on this control.
        /// </summary>
        /// <param name="commandData">
        /// The command data that has requested the argument.
        /// </param>
        /// <returns>
        /// Always returns this control.
        /// </returns>
        private DialogueFileControl SearchResolver(CommandData commandData)
        {
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data">
        /// 
        /// </param>
        private void OnExportConverationLinesToWord(ExecuteCommandData data)
        {
            Dialogue dialogue = (this.DataContext as DialogueViewModel).Model;
            if (dialogue == null)
            {
                return;
            }

            List<Conversation> conversations = new List<Conversation>();
            object selected = this.ConversationList.SelectedItem;
            if (selected == null)
            {
                return;
            }

            ConversationViewModel vm = selected as ConversationViewModel;
            if (vm == null)
            {
                return;
            }

            conversations.Add(vm.Model);
            this.OnExportLinesToWord(dialogue.Name, conversations);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data">
        /// 
        /// </param>
        private void OnExportDocumentLinesToWord(ExecuteCommandData data)
        {
            Dialogue dialogue = (this.DataContext as DialogueViewModel).Model;
            if (dialogue == null)
            {
                return;
            }

            List<Conversation> conversations = new List<Conversation>(dialogue.Conversations);
            this.OnExportLinesToWord(dialogue.Name, conversations);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="header">
        /// 
        /// </param>
        /// <param name="conversations">
        /// 
        /// </param>
        private void OnExportLinesToWord(string missionName, List<Conversation> conversations)
        {
            List<DialogueCharacter> characters = new List<DialogueCharacter>();
            List<Guid> characterIds = new List<Guid>();
            foreach (Conversation conversation in conversations)
            {
                DialogueConfigurations config = conversation.Configurations;
                foreach (ILine line in conversation.Lines)
                {
                    if (!characterIds.Contains(line.CharacterId))
                    {
                        characterIds.Add(line.CharacterId);
                        characters.Add(config.GetCharacter(line.CharacterId));
                    }
                }
            }

            ExportCharacterSelectWindow window = new ExportCharacterSelectWindow();
            window.CharacterItems = characters;
            if (window.ShowDialog() != true)
            {
                return;
            }

            WordApplication wordApplication = WordApplicationCreator.Create();
            string name = wordApplication.Name;
            string version = wordApplication.Version;

            WordDocument document = wordApplication.Documents.Add();
            document.AppendLine(missionName, BuiltInStyle.Heading1);

            foreach (Conversation conversation in conversations)
            {
                List<ExportLine> exportLines = new List<ExportLine>();
                foreach (ILine line in conversation.Lines)
                {
                    Tuple<string, bool> charData = window.ExportCharacter(line.CharacterId);
                    if (charData.Item2)
                    {
                        exportLines.Add(new ExportLine(charData.Item1, line.Dialogue));
                    }
                }

                document.AppendLine();
                StringBuilder header = new StringBuilder();
                header.Append(conversation.Root);
                header.Append("/");
                header.Append(conversation.Description);
                header.Append("/");
                header.Append(conversation.IsRandom ? "Random" : "Not Random");
                header.Append("/");
                header.Append(conversation.IsPlaceholder ? "Placeholder" : "Not Placeholder");

                document.AppendLine(header.ToString(), true, true);
                Alignment alignment = Alignment.AlignParagraphCenter;
                foreach (ExportLine line in exportLines)
                {
                    document.AppendLine(line.CharacterName, true, true, alignment);
                    document.AppendLine(line.Dialogue, false, false, alignment);
                }
            }

            wordApplication.Visible = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnLoaded(object sender, System.Windows.RoutedEventArgs e)
        {
            this.Loaded -= OnLoaded;
            this.HasBeenLoaded = true;
        }
        #endregion Methods

        #region Structures
        /// <summary>
        /// 
        /// </summary>
        private struct ExportLine
        {
            /// <summary>
            /// 
            /// </summary>
            /// <param name="characterName"></param>
            /// <param name="dialogue"></param>
            public ExportLine(string characterName, string dialogue)
                : this()
            {
                this.CharacterName = characterName;
                this.Dialogue = dialogue;
            }

            /// <summary>
            /// 
            /// </summary>
            public string CharacterName
            {
                get;
                set;
            }

            /// <summary>
            /// 
            /// </summary>
            public string Dialogue
            {
                get;
                set;
            }
        } // DialogueFileControl.ExportLine {Structure}
        #endregion Structures
    } // RSG.Text.View.DialogueFileControl {Class}
} // RSG.Text.View {Namespace}
