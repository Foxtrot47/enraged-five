﻿//---------------------------------------------------------------------------------------------
// <copyright file="UpdateProgressWindow.xaml.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.View
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using RSG.Base.Extensions;
    using RSG.Editor;
    using RSG.Editor.Controls;
    using RSG.Project.View.Resources;
    using RSG.Project.ViewModel;
    using RSG.Project.ViewModel.Definitions;

    /// <summary>
    /// Represents the code behind for the update progress window.
    /// </summary>
    public partial class UpdateProgressWindow : RsWindow
    {
        #region Fields
        /// <summary>
        /// A private list of the full paths to the files that need updating.
        /// </summary>
        private List<string> _filesToUpdate;

        /// <summary>
        /// The private field used for the <see cref="FilesUpdated"/> property.
        /// </summary>
        private List<string> _filesUpdated;

        /// <summary>
        /// A private value indicating whether the update has finished.
        /// </summary>
        private bool _finishedUpdating;

        /// <summary>
        /// The private reference to the project which the updated files are being added to.
        /// </summary>
        private ProjectNode _project;

        /// <summary>
        /// A private collection of the updaters.
        /// </summary>
        private List<Updater> _updaters;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="UpdateProgressWindow"/> class.
        /// </summary>
        /// <param name="project">
        /// The project that the files are being updated for.
        /// </param>
        /// <param name="filesToUpdate">
        /// A list of files to update.
        /// </param>
        public UpdateProgressWindow(ProjectNode project, List<string> filesToUpdate)
        {
            this._project = project;
            this._filesToUpdate = filesToUpdate;
            this._filesUpdated = new List<string>();
            this._updaters = new List<Updater>();
            this.InitializeComponent();
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets read-only list containing the full paths to all of the new files that have
        /// been updated.
        /// </summary>
        public IReadOnlyList<string> FilesUpdated
        {
            get { return this._filesUpdated; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Determines whether the
        /// <see cref="RSG.Editor.SharedCommands.RockstarViewCommands.ConfirmDialog"/> command
        /// can be executed at this level.
        /// </summary>
        /// <param name="e">
        /// The command data that is past from the command system.
        /// </param>
        /// <returns>
        /// True if the command can be executed here; otherwise, false.
        /// </returns>
        protected override bool CanConfirmDialog(CanExecuteCommandData e)
        {
            return this._finishedUpdating;
        }

        /// <summary>
        /// Called once the content for the window has been rendered. This starts the
        /// initialisation off.
        /// </summary>
        /// <param name="e">
        /// The System.EventArgs containing the event data.
        /// </param>
        protected async override void OnContentRendered(EventArgs e)
        {
            base.OnContentRendered(e);
            Task initialiseTask = new Task(
                new Action(() =>
                {
                    this.InitialiseUpdate();
                }));

            this.Message.Text = StringTable.UpdateInitialisingMsg;
            await Task.Factory.StartNew(() => { this.InitialiseUpdate(); });

            this.Progress.Minimum = 0;
            this.Progress.Maximum = this._updaters.Count;
            this.Progress.Value = 0;

            await this.PerformTheUpdate();
        }

        /// <summary>
        /// Constructs the updater objects that will be used later to perform the update.
        /// </summary>
        private void InitialiseUpdate()
        {
            foreach (string fullPath in this._filesToUpdate)
            {
                string ext = Path.GetExtension(fullPath);
                ProjectItemDefinition def = this._project.Definition.GetItemDefinition(ext);
                IRequiresUpdatingDefinition updateDef = def as IRequiresUpdatingDefinition;

                string newPath = Path.ChangeExtension(fullPath, updateDef.UpdatedExtension);
                Updater updater = new Updater();
                updater.FullPath = fullPath;
                updater.NewPath = newPath;
                updater.Definition = updateDef;
                this._updaters.Add(updater);
            }
        }

        /// <summary>
        /// Performs the actual update.
        /// </summary>
        /// <returns>
        /// The task representing the work done in this method.
        /// </returns>
        private async Task PerformTheUpdate()
        {
            foreach (Updater updater in this._updaters)
            {
                ////bool cancelled = false;
                ////if (!dlgService.HandleSavingOfReadOnlyFile(ref newPath, ref cancelled))
                ////{

                ////}
                ////FileInfo info = new FileInfo(newPath);
                ////if (info.Exists)
                ////{
                ////    string name = Path.GetFileName(fullPath);
                ////    IHierarchyNode existingNode = project.FindNodeWithFullPath(fullPath);
                ////    if (existingNode != null)
                ////    {
                ////        msgService.ShowStandardErrorBox(
                ////            StringTable.AddExistingOpenedFileMsg.FormatInvariant(name),
                ////            null);

                ////        continue;
                ////    }

                ////    MessageBoxResult result = msgService.Show(
                ////        StringTable.AddExistingFileWarningMsg.FormatInvariant(name),
                ////        null,
                ////        MessageBoxButton.YesNoCancel,
                ////        MessageBoxImage.Warning,
                ////        MessageBoxResult.Yes);

                ////    if (result == MessageBoxResult.No)
                ////    {
                ////        msgService.ShowStandardErrorBox(
                ////            StringTable.AddExistingFileErrorMsg.FormatInvariant(name),
                ////            null);

                ////        continue;
                ////    }
                ////    else if (result == MessageBoxResult.Yes)
                ////    {
                ////        info.IsReadOnly = false;
                ////        break;
                ////    }
                ////    else if (result == MessageBoxResult.Cancel)
                ////    {
                ////        throw new OperationCanceledException();
                ////    }
                ////}

                if (!Directory.Exists(Path.GetDirectoryName(updater.NewPath)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(updater.NewPath));
                }

                this.Message.Text = StringTable.UpdateMsg.FormatInvariant(updater.FullPath);
                using (FileStream inStream = File.OpenRead(updater.FullPath))
                {
                    FileMode mode = FileMode.OpenOrCreate;
                    using (FileStream outStream = new FileStream(updater.NewPath, mode))
                    {
                        outStream.SetLength(0);
                        await Task.Factory.StartNew(
                            new Action(() =>
                            {
                                updater.Definition.Update(inStream, outStream, this._project);
                            }));
                    }
                }

                _filesUpdated.Add(updater.NewPath);
                this.Progress.Value++;
            }

            this._finishedUpdating = true;
            this.ConfirmButton.Width = this.ConfirmButton.ActualWidth;
            this.SpinControl.Visibility = System.Windows.Visibility.Collapsed;
            this.Message.Text = StringTable.UpdateFinishedMsg;
            CommandManager.InvalidateRequerySuggested();
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// A updater structure that contains all of the needed properties to perform a single
        /// file update.
        /// </summary>
        private struct Updater
        {
            #region Properties
            /// <summary>
            /// Gets or sets the full path to the file that needs updating.
            /// </summary>
            public string FullPath
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the full path to the updated file.
            /// </summary>
            public string NewPath
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the update definition that is used to perform the update.
            /// </summary>
            public IRequiresUpdatingDefinition Definition
            {
                get;
                set;
            }
            #endregion Properties
        } // UpdateProgressWindow.Updater {Structure}
        #endregion Classes
    } // RSG.Project.View.UpdateProgressWindow {Class}
} // RSG.Project.View
