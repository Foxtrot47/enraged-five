﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using RSG.Editor;
using RSG.UniversalLog.ViewModel;

namespace RSG.UniversalLog.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public class OpenFromMruAction : MruCommandAction<UniversalLogDataContext>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="OpenFromMruImplementer"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public OpenFromMruAction(ParameterResolverDelegate<UniversalLogDataContext> resolver)
            : base(resolver)
        {
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="commandParameter">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(UniversalLogDataContext dc, String path, String[] additional)
        {
            if (File.Exists(path))
            {
                dc.OpenFiles(Enumerable.Repeat(path, 1), false);
            }
            else
            {
                IMessageBoxService msgService = this.GetService<IMessageBoxService>();
                if (msgService == null)
                {
                    Debug.Assert(false, "Unable to show message as service is missing.");
                    return;
                }

                msgService.Show(
                    "The file you are attempting to open no longer exists.",
                    String.Empty,
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }
        #endregion // Overrides
    } // OpenFromMruAction
}
