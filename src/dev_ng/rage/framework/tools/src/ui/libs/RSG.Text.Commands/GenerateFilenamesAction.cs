﻿//---------------------------------------------------------------------------------------------
// <copyright file="GenerateFilenamesAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Commands
{
    using System;
    using System.Collections.Generic;
    using RSG.Editor;
    using RSG.Text.Model;

    /// <summary>
    /// Implements the auto generate filename commands.
    /// </summary>
    public class GenerateFilenamesAction : ButtonAction<List<Conversation>>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="GenerateFilenamesAction"/> class.
        /// </summary>
        /// <param name="serviceProvider">
        /// The service provider that is used by this action to perform application specific
        /// operations, for example selecting a file to open.
        /// </param>
        public GenerateFilenamesAction(ICommandServiceProvider serviceProvider)
            : base(serviceProvider)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="GenerateFilenamesAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public GenerateFilenamesAction(ParameterResolverDelegate<List<Conversation>> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(List<Conversation> args)
        {
            if (args == null || args.Count == 0)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(List<Conversation> args)
        {
            foreach (Conversation conversation in args)
            {
                if (conversation.Dialogue == null)
                {
                    IMessageBoxService service = this.GetService<IMessageBoxService>();
                    if (service != null)
                    {
                        service.ShowStandardErrorBox(
                            "Unable to generate audio filepaths for a orphaned conversation.",
                            null);
                    }

                    return;
                }

                if (String.IsNullOrWhiteSpace(conversation.Dialogue.Id))
                {
                    IMessageBoxService service = this.GetService<IMessageBoxService>();
                    if (service != null)
                    {
                        service.ShowStandardErrorBox(
                            "Unable to generate audio filepaths without a valid mission id set on the dialogue.",
                            null);
                    }

                    return;
                }
            }

            foreach (Conversation conversation in args)
            {
                if (conversation.Dialogue == null)
                {
                    continue;
                }

                if (String.IsNullOrWhiteSpace(conversation.Dialogue.Id))
                {
                    IMessageBoxService service = this.GetService<IMessageBoxService>();
                    if (service != null)
                    {

                    }

                    return;
                }

                conversation.GenerateFilenames();
            }
        }
        #endregion Methods
    } // RSG.Text.Commands.GenerateFilenamesAction {Class}
} // RSG.Text.Commands {Namespace}
