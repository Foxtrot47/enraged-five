﻿//---------------------------------------------------------------------------------------------
// <copyright file="AudibilityCompositeViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.ViewModel
{
    using System.Collections.Generic;
    using System.Linq;
    using RSG.Text.Model;

    /// <summary>
    /// A composite class containing n-number of <see cref="DialogueAudibilityViewModel"/>
    /// objects. This class cannot be inherited.
    /// </summary>
    public sealed class AudibilityCompositeViewModel : CompositeViewModel
    {
        #region Fields
        /// <summary>
        /// The private collection containing the <see cref="DialogueAudibilityViewModel"/>
        /// objects that are currently managed by this composite class.
        /// </summary>
        private List<DialogueAudibilityViewModel> _items;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="AudibilityCompositeViewModel"/> class.
        /// </summary>
        /// <param name="configurations">
        /// The <see cref="DialogueConfigurations"/> object that the configuration values have
        /// come from.
        /// </param>
        /// <param name="items">
        /// The collection of <see cref="DialogueAudibilityViewModel"/> objects that this
        /// composite object should manage.
        /// </param>
        public AudibilityCompositeViewModel(
            DialogueConfigurations configurations,
            IEnumerable<DialogueAudibilityViewModel> items)
            : base(configurations)
        {
            this._items = new List<DialogueAudibilityViewModel>(items);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the collective export index for all of the configuration values
        /// managed by this class.
        /// </summary>
        public int? ExportIndex
        {
            get
            {
                if (this._items == null || this._items.Count == 0)
                {
                    return null;
                }

                IEnumerable<int> indices = this._items.Select(x => x.ExportIndex).Distinct();
                if (indices.Count() == 1)
                {
                    return indices.First();
                }

                return null;
            }

            set
            {
                if (this._items == null)
                {
                    return;
                }

                foreach (DialogueAudibilityViewModel item in this._items)
                {
                    item.ExportIndex = value.Value;
                }
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Deletes all of the values managed by this composite collection from the dialogue
        /// configuration object.
        /// </summary>
        public override void Delete()
        {
            List<DialogueAudibility> delete = new List<DialogueAudibility>();
            foreach (DialogueAudibilityViewModel viewModel in this._items)
            {
                foreach (DialogueAudibility audibility in this.Model.Audibilities)
                {
                    if (viewModel.Id == audibility.Id)
                    {
                        delete.Add(audibility);
                    }
                }
            }

            foreach (DialogueAudibility audibility in delete)
            {
                this.Model.Audibilities.Remove(audibility);
            }
        }
        #endregion Methods
    } // RSG.Text.ViewModel.AudibilityCompositeViewModel {Class}
} // RSG.Text.ViewModel {Namespace}
