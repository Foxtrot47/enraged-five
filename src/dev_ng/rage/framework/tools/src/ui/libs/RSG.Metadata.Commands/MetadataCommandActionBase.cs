﻿//---------------------------------------------------------------------------------------------
// <copyright file="MetadataCommandActionBase.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Commands
{
    using RSG.Editor;

    /// <summary>
    /// Provides a base class to all metadata command actions that implement save logic.
    /// </summary>
    public abstract class MetadataCommandActionBase : ButtonAction<MetadataCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MetadataCommandActionBase"/> class.
        /// </summary>
        /// <param name="serviceProvider">
        /// The service provider that is used by this action to perform application specific
        /// operations, for example selecting a file to open.
        /// </param>
        protected MetadataCommandActionBase(ICommandServiceProvider serviceProvider)
            : base(serviceProvider)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="MetadataCommandActionBase"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        protected MetadataCommandActionBase(MetadataCommandArgsResolver resolver)
            : base(new ParameterResolverDelegate<MetadataCommandArgs>(resolver))
        {
        }
        #endregion Constructors
    } // RSG.Metadata.Commands.MetadataCommandActionBase {Class}
} // RSG.Metadata.Commands {Namespace}
