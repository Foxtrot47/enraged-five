﻿//---------------------------------------------------------------------------------------------
// <copyright file="OpenProjectAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using RSG.Editor;
    using RSG.Project.Model;
    using RSG.Project.ViewModel;
    using RSG.Project.ViewModel.Definitions;

    /// <summary>
    /// Contains the logic for the <see cref="ProjectCommands.OpenProjectFile"/> routed
    /// command. This class cannot be inherited.
    /// </summary>
    public sealed class OpenProjectAction : ButtonAction<ProjectCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="OpenProjectAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public OpenProjectAction(ProjectCommandResolver resolver)
            : base(new ParameterResolverDelegate<ProjectCommandArgs>(resolver))
        {
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs when a project is loaded from using this action.
        /// </summary>
        public event EventHandler<ProjectOpenedEventArgs> ProjectOpened;
        #endregion Events

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ProjectCommandArgs args)
        {
            if (args == null)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(ProjectCommandArgs args)
        {
            IHierarchyNode parent = args.SelectedNodes.FirstOrDefault();
            ProjectCollectionNode collection = args.CollectionNode;
            if (parent == null || !parent.CanHaveProjectsAdded || args.SelectionCount > 1)
            {
                parent = args.CollectionNode;
            }

            ICommonDialogService dlgService = this.GetService<ICommonDialogService>();
            if (dlgService == null)
            {
                Debug.Assert(false, "Unable to open existing project, a service is missing.");
                return;
            }

            HashSet<string> dialogFilters = new HashSet<string>();
            foreach (ProjectDefinition definition in collection.Definition.ProjectDefinitions)
            {
                dialogFilters.Add(definition.Filter);
            }

            string filter = null;
            if (dialogFilters.Count > 1)
            {
                filter = "All Project Files (*.*)|*.*|";
            }

            filter += String.Join("|", dialogFilters);
            string fullPath = null;
            if (!dlgService.ShowOpenFile(null, null, filter, 0, out fullPath))
            {
                throw new OperationCanceledException();
            }

            string extension = Path.GetExtension(fullPath);
            ProjectDefinition selectedDefinition = null;
            StringComparison comparisonType = StringComparison.OrdinalIgnoreCase;
            foreach (ProjectDefinition definition in collection.Definition.ProjectDefinitions)
            {
                if (String.Equals(definition.Extension, extension, comparisonType))
                {
                    selectedDefinition = definition;
                    break;
                }
            }

            if (selectedDefinition == null)
            {
                Debug.Assert(
                    false,
                    "Failed to find project definition for the extension",
                    "{0}",
                    extension);
                return;
            }

            CloseCollectionAction close = new CloseCollectionAction(this.ServiceProvider);
            if (close.CanExecute(args))
            {
                close.Execute(args);
            }

            ProjectItem newItem = collection.Model.AddNewProjectItem("Project", fullPath);
            ProjectNode projectNode = selectedDefinition.CreateProjectNode();
            projectNode.SetParentAndModel(collection, newItem);
            collection.AddChild(projectNode);
            projectNode.Load();

            collection.IsModified = true;
            collection.Loaded = true;
            collection.IsTemporaryFile = true;
            collection.Name = Path.GetFileNameWithoutExtension(fullPath);
            if (this.ProjectOpened != null)
            {
                this.ProjectOpened(this, new ProjectOpenedEventArgs(projectNode, fullPath));
            }
        }
        #endregion Methods
    } // RSG.Project.Commands.OpenProjectAction {Class}
} // RSG.Project.Commands {Namespace}
