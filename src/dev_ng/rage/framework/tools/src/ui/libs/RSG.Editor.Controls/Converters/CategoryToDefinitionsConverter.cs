﻿//---------------------------------------------------------------------------------------------
// <copyright file="CategoryToDefinitionsConverter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Converters
{
    using System.Collections.Generic;
    using System.Globalization;

    /// <summary>
    /// Converters a command category name to the collection of command definitions that are
    /// inside that category.
    /// </summary>
    internal class CategoryToDefinitionsConverter :
        ValueConverter<string, IEnumerable<CommandDefinition>>
    {
        #region Methods
        /// <summary>
        /// Converters the given value and returns the result.
        /// </summary>
        /// <param name="category">
        /// The category name to get the definitions for.
        /// </param>
        /// <param name="param">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A converted value.
        /// </returns>
        protected override IEnumerable<CommandDefinition> Convert(
            string category, object param, CultureInfo culture)
        {
            return RockstarCommandManager.GetDefinitions(category);
        }
        #endregion Methods
    } // RSG.Editor.Controls.Converters.CategoryToDefinitionsConverter {Class}
} // RSG.Editor.Controls.Converters {Namespace}
