﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProjectCommands.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel
{
    using System;
    using System.Windows.Input;
    using System.Windows.Media.Imaging;
    using RSG.Editor;
    using RSG.Editor.SharedCommands;

    /// <summary>
    /// Defines core commands for the Project control. This class cannot be inherited.
    /// </summary>
    public sealed class ProjectCommands
    {
        #region Fields
        /// <summary>
        /// The private cache of command objects.
        /// </summary>
        private static RockstarRoutedCommand[] _internalCommands = InitialiseInternalStorage();

        /// <summary>
        /// The private field used for the <see cref="ProjectExternallyModified"/> property.
        /// </summary>
        private static RoutedCommand _projectExternallyModified;

        /// <summary>
        /// The private field used for the <see cref="ProjectLoadFailed"/> property.
        /// </summary>
        private static RoutedCommand _projectLoadFailed;

        /// <summary>
        /// The private string table resource manager used for getting the command related
        /// strings and objects.
        /// </summary>
        private static CommandResourceManager _resourceManager = InitialiseResourceManager();

        /// <summary>
        /// The generic object that is used to sync the creation of the commands.
        /// </summary>
        private static object _syncRoot = new object();
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Prevents a default instance of the <see cref="ProjectCommands"/> class from being
        /// created.
        /// </summary>
        private ProjectCommands()
        {
        }
        #endregion Constructors

        #region Enumerations
        /// <summary>
        /// Defines all of the command identifiers for the different core Rockstar commands.
        /// </summary>
        private enum Id : byte
        {
            /// <summary>
            /// Used to identifier the <see cref="ProjectCommands.AddExistingProject"/> routed
            /// command.
            /// </summary>
            AddExistingProject,

            /// <summary>
            /// Used to identifier the <see cref="ProjectCommands.AddNewProject"/> routed
            /// command.
            /// </summary>
            AddNewProject,

            /// <summary>
            /// Used to identifier the <see cref="ProjectCommands.AddCollectionFolder"/> routed
            /// command.
            /// </summary>
            AddCollectionFolder,

            /// <summary>
            /// Used to identifier the <see cref="ProjectCommands.AddProjectDynamicFolder"/>
            /// routed command.
            /// </summary>
            AddProjectDynamicFolder,

            /// <summary>
            /// Used to identifier the <see cref="ProjectCommands.AddProjectFolder"/> routed
            /// command.
            /// </summary>
            AddProjectFolder,

            /// <summary>
            /// Used to identifier the <see cref="ProjectCommands.AddNewItem"/> routed command.
            /// </summary>
            AddNewItem,

            /// <summary>
            /// Used to identifier the <see cref="ProjectCommands.AddExistingItem"/> routed
            /// command.
            /// </summary>
            AddExistingItem,

            /// <summary>
            /// Used to identifier the <see cref="ProjectCommands.BuildAllProjects"/> routed
            /// command.
            /// </summary>
            BuildAllProjects,

            /// <summary>
            /// Used to identifier the <see cref="ProjectCommands.BuildSelectedProjects"/>
            /// routed command.
            /// </summary>
            BuildSelectedProjects,

            /// <summary>
            /// Used to identifier the <see cref="ProjectCommands.CloseProjectCollection"/>
            /// routed command.
            /// </summary>
            CloseProjectCollection,

            /// <summary>
            /// Used to identifier the <see cref="ProjectCommands.OpenDocumentFromProject"/>
            /// routed command.
            /// </summary>
            OpenDocumentFromProject,

            /// <summary>
            /// Used to identifier the <see cref="ProjectCommands.OpenFileFromProject"/> routed
            /// command.
            /// </summary>
            OpenFileFromProject,

            /// <summary>
            /// Used to identifier the <see cref="ProjectCommands.OpenProjectCollectionFile"/>
            /// routed command.
            /// </summary>
            OpenProjectCollectionFile,

            /// <summary>
            /// Used to identifier the <see cref="ProjectCommands.CreateNewProject"/> routed
            /// command.
            /// </summary>
            CreateNewProject,

            /// <summary>
            /// Used to identifier the <see cref="ProjectCommands.OpenProjectFile"/> routed
            /// command.
            /// </summary>
            OpenProjectFile,

            /// <summary>
            /// Used to identifier the <see cref="ProjectCommands.LocateInExplorer"/> routed
            /// command.
            /// </summary>
            LocateInExplorer,

            /// <summary>
            /// Used to identifier the <see cref="ProjectCommands.UnloadProject"/> routed
            /// command.
            /// </summary>
            UnloadProject,

            /// <summary>
            /// Used to identifier the <see cref="ProjectCommands.Regenerate"/> routed command.
            /// </summary>
            Regenerate,

            /// <summary>
            /// Used to identifier the <see cref="ProjectCommands.ReloadProject"/> routed
            /// command.
            /// </summary>
            ReloadProject,

            /// <summary>
            /// Used to identifier the <see cref="ProjectCommands.OpenProjectProperties"/>
            /// routed command.
            /// </summary>
            OpenProjectProperties,

            /// <summary>
            /// Used to identify the <see cref="ProjectCommands.OpenContainingFolder"/>
            /// routed command.
            /// </summary>
            OpenContainingFolder,

            /// <summary>
            /// Used to identify the <see cref="ProjectCommands.OpenCommandPrompt"/>
            /// routed command.
            /// </summary>
            OpenCommandPrompt,
        } // ProjectCommands.Id {Enum}
        #endregion Enumerations

        #region Properties
        /// <summary>
        /// Gets the value that represents the Add Collection Folder command.
        /// </summary>
        public static RockstarRoutedCommand AddCollectionFolder
        {
            get
            {
                return EnsureCommandExists(Id.AddCollectionFolder, ProjectIcons.NewFolder);
            }
        }

        /// <summary>
        /// Gets the value that represents the Add Existing Item command.
        /// </summary>
        public static RockstarRoutedCommand AddExistingItem
        {
            get
            {
                return EnsureCommandExists(Id.AddExistingItem, ProjectIcons.AddExistingItem);
            }
        }

        /// <summary>
        /// Gets the value that represents the Add Existing Project command.
        /// </summary>
        public static RockstarRoutedCommand AddExistingProject
        {
            get { return EnsureCommandExists(Id.AddExistingProject, null); }
        }

        /// <summary>
        /// Gets the value that represents the Add New Item command.
        /// </summary>
        public static RockstarRoutedCommand AddNewItem
        {
            get { return EnsureCommandExists(Id.AddNewItem, ProjectIcons.AddNewItem); }
        }

        /// <summary>
        /// Gets the value that represents the Add New Project command.
        /// </summary>
        public static RockstarRoutedCommand AddNewProject
        {
            get { return EnsureCommandExists(Id.AddNewProject, null); }
        }

        /// <summary>
        /// Gets the value that represents the Add Project Dynamic Folder command.
        /// </summary>
        public static RockstarRoutedCommand AddProjectDynamicFolder
        {
            get
            {
                return EnsureCommandExists(Id.AddProjectDynamicFolder, ProjectIcons.NewFolder);
            }
        }

        /// <summary>
        /// Gets the value that represents the Add Project Folder command.
        /// </summary>
        public static RockstarRoutedCommand AddProjectFolder
        {
            get { return EnsureCommandExists(Id.AddProjectFolder, ProjectIcons.NewFolder); }
        }

        /// <summary>
        /// Gets the value that represents the Build All Projects command.
        /// </summary>
        public static RockstarRoutedCommand BuildAllProjects
        {
            get
            {
                return EnsureCommandExists(Id.BuildAllProjects, ProjectIcons.BuildCollection);
            }
        }

        /// <summary>
        /// Gets the value that represents the Build Selected Projects command.
        /// </summary>
        public static RockstarRoutedCommand BuildSelectedProjects
        {
            get
            {
                return EnsureCommandExists(
                    Id.BuildSelectedProjects, ProjectIcons.BuildSelection);
            }
        }

        /// <summary>
        /// Gets the value that represents the Close Project Collection command.
        /// </summary>
        public static RockstarRoutedCommand CloseProjectCollection
        {
            get { return EnsureCommandExists(Id.CloseProjectCollection, CommonIcons.Close); }
        }

        /// <summary>
        /// Gets the value that represents the Create New Project command.
        /// </summary>
        public static RockstarRoutedCommand CreateNewProject
        {
            get
            {
                return EnsureCommandExists(Id.CreateNewProject);
            }
        }

        /// <summary>
        /// Gets the value that represents the Locate In Explorer command.
        /// </summary>
        public static RockstarRoutedCommand LocateInExplorer
        {
            get
            {
                return EnsureCommandExists(Id.LocateInExplorer, ProjectIcons.LocateInExplorer);
            }
        }

        /// <summary>
        /// Gets the value that represents the Open Document From Project command.
        /// </summary>
        public static RockstarRoutedCommand OpenDocumentFromProject
        {
            get
            {
                return EnsureCommandExists(Id.OpenDocumentFromProject, ProjectIcons.OpenFile);
            }
        }

        /// <summary>
        /// Gets the value that represents the Open File From Project command.
        /// </summary>
        public static RockstarRoutedCommand OpenFileFromProject
        {
            get { return EnsureCommandExists(Id.OpenFileFromProject, ProjectIcons.OpenFile); }
        }

        /// <summary>
        /// Gets the value that represents the Open Project Collection File command.
        /// </summary>
        public static RockstarRoutedCommand OpenProjectCollectionFile
        {
            get
            {
                return EnsureCommandExists(
                    Id.OpenProjectCollectionFile, ProjectIcons.GenericOpenProjectCollection);
            }
        }

        /// <summary>
        /// Gets the value that represents the Open Project File command.
        /// </summary>
        public static RockstarRoutedCommand OpenProjectFile
        {
            get { return EnsureCommandExists(Id.OpenProjectFile); }
        }

        /// <summary>
        /// Gets the value that represents the Open Project Properties command.
        /// </summary>
        public static RockstarRoutedCommand OpenProjectProperties
        {
            get { return EnsureCommandExists(Id.OpenProjectProperties); }
        }

        /// <summary>
        /// Gets the value that represents the Open Containing Folder command.
        /// </summary>
        public static RockstarRoutedCommand OpenContainingFolder
        {
            get { return EnsureCommandExists(Id.OpenContainingFolder); }
        }

        /// <summary>
        /// Gets the value that represents the Open Command Prompt command.
        /// </summary>
        public static RockstarRoutedCommand OpenCommandPrompt
        {
            get { return EnsureCommandExists(Id.OpenCommandPrompt); }
        }

        /// <summary>
        /// Gets the value that represents the Project Externally Modified command.
        /// </summary>
        public static RoutedCommand ProjectExternallyModified
        {
            get
            {
                if (_projectExternallyModified != null)
                {
                    return _projectExternallyModified;
                }

                lock (_syncRoot)
                {
                    if (_projectExternallyModified != null)
                    {
                        return _projectExternallyModified;
                    }

                    _projectExternallyModified = new RoutedCommand(
                            "ProjectExternallyModified", typeof(ProjectCommands));
                }

                return _projectExternallyModified;
            }
        }

        /// <summary>
        /// Gets the value that represents the Project Load Failed command.
        /// </summary>
        public static RoutedCommand ProjectLoadFailed
        {
            get
            {
                if (_projectLoadFailed != null)
                {
                    return _projectLoadFailed;
                }

                lock (_syncRoot)
                {
                    if (_projectLoadFailed != null)
                    {
                        return _projectLoadFailed;
                    }

                    _projectLoadFailed = new RoutedCommand(
                            "ProjectLoadFailed", typeof(ProjectCommands));
                }

                return _projectLoadFailed;
            }
        }

        /// <summary>
        /// Gets the value that represents the Regenerate command.
        /// </summary>
        public static RockstarRoutedCommand Regenerate
        {
            get { return EnsureCommandExists(Id.Regenerate, CommonIcons.Refresh); }
        }

        /// <summary>
        /// Gets the value that represents the Reload Project command.
        /// </summary>
        public static RockstarRoutedCommand ReloadProject
        {
            get { return EnsureCommandExists(Id.ReloadProject); }
        }

        /// <summary>
        /// Gets the value that represents the Unload Project command.
        /// </summary>
        public static RockstarRoutedCommand UnloadProject
        {
            get { return EnsureCommandExists(Id.UnloadProject); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="RSG.Editor.RockstarRoutedCommand"/> object that is
        /// associated with the specified identifier.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command to create.
        /// </param>
        /// <param name="icon">
        /// The icon that the created command should be using.
        /// </param>
        /// <returns>
        /// The newly created <see cref="RSG.Editor.RockstarRoutedCommand"/> object.
        /// </returns>
        private static RockstarRoutedCommand CreateCommand(string id, BitmapSource icon)
        {
            string name = _resourceManager.GetName(id);
            InputGestureCollection gestures = _resourceManager.GetGestures(id);
            string category = _resourceManager.GetCategory(id);
            string description = _resourceManager.GetDescription(id);

            return new RockstarRoutedCommand(
                name, typeof(ProjectCommands), gestures, category, description, icon);
        }

        /// <summary>
        /// Makes sure the internal value for the command associated with the specified
        /// identifier exists and returns it.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command that should be created and returned.
        /// </param>
        /// <returns>
        /// The <see cref="RSG.Editor.RockstarRoutedCommand"/> object associated with the
        /// specified identifier.
        /// </returns>
        private static RockstarRoutedCommand EnsureCommandExists(Id id)
        {
            return EnsureCommandExists(id, null);
        }

        /// <summary>
        /// Makes sure the internal value for the command associated with the specified
        /// identifier exists and returns it.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command that should be created and returned.
        /// </param>
        /// <param name="icon">
        /// The icon that the command should be using.
        /// </param>
        /// <returns>
        /// The <see cref="RSG.Editor.RockstarRoutedCommand"/> object associated with the
        /// specified identifier.
        /// </returns>
        private static RockstarRoutedCommand EnsureCommandExists(Id id, BitmapSource icon)
        {
            int commandIndex = (int)id;
            if (commandIndex < -1 || commandIndex >= _internalCommands.Length)
            {
                return null;
            }

            RockstarRoutedCommand command = _internalCommands[commandIndex];
            if (command == null)
            {
                lock (_internalCommands)
                {
                    if (command == null)
                    {
                        command = CreateCommand(id.ToString(), icon);
                        _internalCommands[commandIndex] = command;
                    }
                }
            }

            return command;
        }

        /// <summary>
        /// Initialises the array used to store the
        /// <see cref="RSG.Editor.RockstarRoutedCommand"/> objects that make up the core
        /// commands for a Rockstar application.
        /// </summary>
        /// <returns>
        /// The array used to store the internal command objects.
        /// </returns>
        private static RockstarRoutedCommand[] InitialiseInternalStorage()
        {
            return new RockstarRoutedCommand[Enum.GetValues(typeof(Id)).Length];
        }

        /// <summary>
        /// Initialises the resource manager to use to retrieve command related strings and
        /// objects from a resource data file.
        /// </summary>
        /// <returns>
        /// The resource manager to use with this class.
        /// </returns>
        private static CommandResourceManager InitialiseResourceManager()
        {
            Type type = typeof(ProjectCommands);
            string baseName = type.Namespace + ".Resources.CommandStringTable";
            return new CommandResourceManager(baseName, type.Assembly);
        }
        #endregion Methods
    } // RSG.Project.ViewModel.ProjectCommands {Class}
} // RSG.Project.ViewModel {Namespace}
