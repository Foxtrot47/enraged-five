﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class StringTable {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal StringTable() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("RSG.Editor.Controls.Resources.StringTable", typeof(StringTable).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The application throw an exception before being fully initialised. Please make sure you are on the labelled tools and that your previous tools install was successful and try again. If this problem persists please contact tools..
        /// </summary>
        public static string ApplicationStartupFailed {
            get {
                return ResourceManager.GetString("ApplicationStartupFailed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        public static string CancelButtonText {
            get {
                return ResourceManager.GetString("CancelButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Clear Recent {0} List.
        /// </summary>
        public static string ClearMruListText {
            get {
                return ResourceManager.GetString("ClearMruListText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Clear search.
        /// </summary>
        public static string ClearSearchToolTip {
            get {
                return ResourceManager.GetString("ClearSearchToolTip", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Continue.
        /// </summary>
        public static string ContinueButtonText {
            get {
                return ResourceManager.GetString("ContinueButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unable to converter &apos;{0}&apos; to a double. Value not updated.
        ///Current Value: {1}.
        /// </summary>
        public static string DoubleSpinnerErrMsg {
            get {
                return ResourceManager.GetString("DoubleSpinnerErrMsg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unable to converter &apos;{0}&apos; to a float. Value not updated.
        ///Current Value: {1}.
        /// </summary>
        public static string FloatSpinnerErrMsg {
            get {
                return ResourceManager.GetString("FloatSpinnerErrMsg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unable to converter &apos;{0}&apos; to an integer. Value not updated.
        ///Current Value: {1}.
        /// </summary>
        public static string IntegerSpinnerErrMsg {
            get {
                return ResourceManager.GetString("IntegerSpinnerErrMsg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mru List....
        /// </summary>
        public static string MruListText {
            get {
                return ResourceManager.GetString("MruListText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Recent .
        /// </summary>
        public static string MruMenuText {
            get {
                return ResourceManager.GetString("MruMenuText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to _Overwrite.
        /// </summary>
        public static string OverwriteButtonText {
            get {
                return ResourceManager.GetString("OverwriteButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to There was an error trying to make the file {0} writable..
        /// </summary>
        public static string OverwriteFailedMsg {
            get {
                return ResourceManager.GetString("OverwriteFailedMsg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save _As....
        /// </summary>
        public static string SaveAsButtonText {
            get {
                return ResourceManager.GetString("SaveAsButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unable to find a server named &apos;{0}&apos; in the statistics config file. Falling back on the default server..
        /// </summary>
        public static string ServerNotFoundWarning {
            get {
                return ResourceManager.GetString("ServerNotFoundWarning", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Out of the specified range [{0},{1}]. Value not updated.
        ///Current Value: {2}.
        /// </summary>
        public static string SpinnerRangeErrMsg {
            get {
                return ResourceManager.GetString("SpinnerRangeErrMsg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Start search.
        /// </summary>
        public static string StartSearchToolTip {
            get {
                return ResourceManager.GetString("StartSearchToolTip", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The statistics config couldn&apos;t be created due to an exception firing. This will prevent the application adding its tracking statistics to the database..
        /// </summary>
        public static string StatisticsConfigFailure {
            get {
                return ResourceManager.GetString("StatisticsConfigFailure", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} (Modified).
        /// </summary>
        public static string TreeViewItemModifiedToolTip {
            get {
                return ResourceManager.GetString("TreeViewItemModifiedToolTip", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save of Read-Only File.
        /// </summary>
        public static string WriteProtectedCaption {
            get {
                return ResourceManager.GetString("WriteProtectedCaption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The file {0} cannot be saved to the specified location because it is write-protected.
        ///
        ///You can either save the file to a different location or {1} can attempt to remove the write-protection and overwrite the file in its current location..
        /// </summary>
        public static string WriteProtectedMsg {
            get {
                return ResourceManager.GetString("WriteProtectedMsg", resourceCulture);
            }
        }
    }
}
