﻿//---------------------------------------------------------------------------------------------
// <copyright file="ContainerCollectionEditor.xaml.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Customisation
{
    using System.Diagnostics;
    using System.Windows.Controls;
    using System.Windows.Data;

    /// <summary>
    /// Interaction logic for container collection editor user control.
    /// </summary>
    internal partial class ContainerCollectionEditor : UserControl
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ContainerCollectionEditor"/>
        /// class.
        /// </summary>
        public ContainerCollectionEditor()
        {
            this.InitializeComponent();
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Called whenever a binding for a dependency property on the container list is
        /// updated.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The event data containing the property that has been updated.
        /// </param>
        private void TreeViewTargetUpdated(object sender, DataTransferEventArgs e)
        {
            if (e.Property != RsVirtualisedTreeView.RootItemsSourceProperty)
            {
                return;
            }

            RsVirtualisedTreeView treeView = sender as RsVirtualisedTreeView;
            if (treeView == null)
            {
                Debug.Assert(treeView != null, "Handler attached to incorrect item");
                return;
            }

            treeView.SelectFirstItem();
        }
        #endregion Methods
    } // RSG.Editor.Controls.Customisation.ContainerCollectionEditor {Class}
} // RSG.Editor.Controls.Customisation {Namespace}
