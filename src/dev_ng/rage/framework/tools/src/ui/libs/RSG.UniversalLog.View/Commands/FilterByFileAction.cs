﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor;
using RSG.UniversalLog.ViewModel;

namespace RSG.UniversalLog.View.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public class FilterByFileAction : FilterAction<UniversalLogControl, String>
    {
        #region Fields
        /// <summary>
        /// Reference to the definition for updating the items.
        /// </summary>
        private static MultiCommand _definition = null;

        /// <summary>
        /// List of items this implementer has.
        /// </summary>
        private ObservableCollection<IMultiCommandItem> _items = new ObservableCollection<IMultiCommandItem>();
        #endregion // Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="FilterByLogLevelImplementer"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public FilterByFileAction(ParameterResolverDelegate<UniversalLogControl> resolver, UniversalLogDataContext dc)
            : base(resolver)
        {
            // We might not always get a dc passed in.
            if (dc != null)
            {
                ResetItems(dc);
                dc.LoadedFileViewModels.CollectionChanged += LoadedFileViewModels_CollectionChanged;
            }
        }
        #endregion // Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="control">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="secondaryParameter">
        /// The secondary command parameter that has been sent with the command.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(UniversalLogControl control, MultiCommandParameter<String> secondaryParameter)
        {
            if (control == null)
            {
                throw new SmartArgumentNullException(() => control);
            }
            UniversalLogDataContext dc = GetDataContext(control);
            return dc.AnyFilesLoaded;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="control">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="secondaryParameter">
        /// The secondary command parameter that has been sent with the command.
        /// </param>
        public override void Execute(UniversalLogControl control, MultiCommandParameter<String> secondaryParameter)
        {
            if (control == null)
            {
                throw new SmartArgumentNullException(() => control);
            }

            // Check whether wish to show or hide this file.
            if (secondaryParameter.IsToggled)
            {
                control.RemoveFileFilter(secondaryParameter.ItemParameter);
            }
            else
            {
                control.AddFileFilter(secondaryParameter.ItemParameter);
            }
        }

        /// <summary>
        /// Provide the specified definition with the items to show in the filter.
        /// </summary>
        /// <param name="definition">
        /// The definition that the created items will be used for.
        /// </param>
        /// <returns>
        /// The items to show in the filter.
        /// </returns>
        protected override ObservableCollection<IMultiCommandItem> GetItems(
            MultiCommand definition)
        {
            _definition = definition;
            return _items;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oldValue"></param>
        /// <param name="newValue"></param>
        public void UpdateDataContext(UniversalLogDataContext oldValue, UniversalLogDataContext newValue)
        {
            if (oldValue != null)
            {
                oldValue.LoadedFileViewModels.CollectionChanged -= LoadedFileViewModels_CollectionChanged;
            }
            if (newValue != null)
            {
                newValue.LoadedFileViewModels.CollectionChanged += LoadedFileViewModels_CollectionChanged;
                ResetItems(newValue);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void ReInitialise(UniversalLogControl control)
        {
            foreach (FilterByFileItem item in _definition.Items)
            {
                if (!item.IsToggled)
                {
                    this.Execute(control, item.GetCommandParameter() as MultiCommandParameter<String>);
                }
            }
        }
        #endregion // Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dc"></param>
        private void ResetItems(UniversalLogDataContext dc)
        {
            _items.Clear();
            foreach (UniversalLogFileViewModel vm in dc.LoadedFileViewModels)
            {
                _items.Add(new FilterByFileItem(_definition, vm.Filepath));
            }
        }

        /// <summary>
        /// Gets the data context associated with the control.
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        private UniversalLogDataContext GetDataContext(UniversalLogControl control)
        {
            UniversalLogDataContext dc = null;
            if (!control.Dispatcher.CheckAccess())
            {
                dc = control.Dispatcher.Invoke<UniversalLogDataContext>(() => control.DataContext as UniversalLogDataContext);
            }
            else
            {
                dc = control.DataContext as UniversalLogDataContext;
            }

            if (dc == null)
            {
                Debug.Fail("Data context isn't valid.");
                throw new ArgumentException("Data context isn't valid.");
            }
            return dc;
        }
        #endregion // Private Methods

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadedFileViewModels_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (UniversalLogFileViewModel vm in e.NewItems)
                {
                    _items.Add(new FilterByFileItem(_definition, vm.Filepath));
                }
            }
            if (e.OldItems != null)
            {
                foreach (UniversalLogFileViewModel vm in e.OldItems)
                {
                    _items.Remove(_items.First(item => ((FilterByFileItem)item).FilePath == vm.Filepath));
                }
            }
            if (e.Action == NotifyCollectionChangedAction.Reset)
            {
                _items.Clear();
            }
        }
        #endregion // Event Handlers

        #region Classes
        /// <summary>
        /// Defines the item published for the items of the
        /// <see cref="FilterByLogLevelItem"/> command definition.
        /// </summary>
        private class FilterByFileItem : MultiCommandItem<String>
        {
            #region Fields
            /// <summary>
            /// The private field used for the <see cref="Icon"/> property.
            /// </summary>
            private String m_filepath;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// 
            /// </summary>
            /// <param name="definition">
            /// The multi command definition that owns this item.
            /// </param>
            public FilterByFileItem(MultiCommand definition, String filepath)
                : base(definition, System.IO.Path.GetFileName(filepath), filepath)
            {
                m_filepath = filepath;
                IsToggled = true;
            }
            #endregion // Constructors

            #region Properties
            /// <summary>
            /// Gets the description for this item. This is also what is shown as a tooltip
            /// along with the key gesture if one is set if it is being rendered as a
            /// toggle control.
            /// </summary>
            public override String Description
            {
                get { return m_filepath; }
            }

            public String ToolTip
            {
                get { return m_filepath; }
            }

            /// <summary>
            /// 
            /// </summary>
            public String FilePath
            {
                get { return m_filepath; }
            }
            #endregion // Properties
        } // FilterByLogLevelItem
        #endregion // Classes
    } // FilterByFileAction
}
