﻿//---------------------------------------------------------------------------------------------
// <copyright file="DeleteConversationAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Commands
{
    using System;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Threading;
    using RSG.Editor;
    using RSG.Text.ViewModel;

    /// <summary>
    /// Implements the RockstarCommands.Delete command.
    /// </summary>
    public class DeleteConversationAction : ButtonAction<TextConversationCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DeleteConversationAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public DeleteConversationAction(
            ParameterResolverDelegate<TextConversationCommandArgs> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(TextConversationCommandArgs args)
        {
            return args.Selected != null && args.Selected.Count() > 0;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(TextConversationCommandArgs args)
        {
            int index = args.DataGrid.SelectedIndex;
            foreach (ConversationViewModel lineViewModel in args.Selected)
            {
                args.Dialogue.Model.RemoveConversation(lineViewModel.Model);
            }

            if (args.DataGrid.SelectedIndex == -1 && args.DataGrid.Items.Count > 0)
            {
                args.DataGrid.Dispatcher.BeginInvoke(
                    (Action)(() =>
                    {
                        DataGrid dataGtrid = args.DataGrid;
                        index = Math.Min(dataGtrid.Items.Count - 1, index);
                        dataGtrid.SelectedItem = dataGtrid.Items[index];
                    }),
                    DispatcherPriority.Input);
            }
        }
        #endregion Methods
    } // RSG.Text.Commands.DeleteConversationAction {Class}
} // RSG.Text.Commands {Namespace}
