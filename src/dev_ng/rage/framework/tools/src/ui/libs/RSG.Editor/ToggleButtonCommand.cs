﻿//---------------------------------------------------------------------------------------------
// <copyright file="ToggleButtonCommand.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Windows.Media.Imaging;
    using System.Xml;

    /// <summary>
    /// Provides a base class to a definition of a button command.
    /// </summary>
    public class ToggleButtonCommand : ButtonCommand, ICommandWithToggle
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="IsToggled"/> property.
        /// </summary>
        private bool _isToggled;

        /// <summary>
        /// The private field used for the <see cref="ToggledIcon"/> property.
        /// </summary>
        private BitmapSource _toggledIcon;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ToggleButtonCommand"/> class.
        /// </summary>
        /// <param name="command">
        /// The <see cref="RockstarRoutedCommand"/> that this definition is
        /// representing.
        /// </param>
        /// <param name="isToggled">
        /// A value indicating the initial toggle state for this command definition.
        /// </param>
        public ToggleButtonCommand(RockstarRoutedCommand command, bool isToggled)
            : base(command)
        {
            this._isToggled = isToggled;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the System.Windows.Media.Imaging.BitmapSource object that represents the icon
        /// that is used for this command, this can be null.
        /// </summary>
        public override BitmapSource Icon
        {
            get
            {
                if (this._isToggled && this.ToggledIcon != null)
                {
                    return this.ToggledIcon;
                }

                return base.Icon;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this definition is currently in the toggled
        /// state or not.
        /// </summary>
        public bool IsToggled
        {
            get
            {
                return this._isToggled;
            }

            set
            {
                if (this.SetProperty(ref this._isToggled, value))
                {
                    this.NotifyPropertyChanged("Icon");
                    this.OnIsToggledChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the System.Windows.Media.Imaging.BitmapSource object that represents
        /// the icon that is used for this command when it is toggled, this can be null.
        /// </summary>
        public BitmapSource ToggledIcon
        {
            get { return this._toggledIcon; }
            set { this.SetProperty(ref this._toggledIcon, value, "ToggledIcon", "Icon"); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Deserialises the commands state using the data contained within the specified xml
        /// reader.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the state data.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        /// <returns>
        /// A value indicating whether the command needs to be executed due to the
        /// de-serialisation process.
        /// </returns>
        public override bool DeserialiseState(XmlReader reader, IFormatProvider provider)
        {
            bool toggled;
            if (bool.TryParse(reader.GetAttribute("toggled"), out toggled))
            {
                if (this.IsToggled != toggled)
                {
                    this.IsToggled = toggled;
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Serialises the state of this definition into the specified xml writer.
        /// </summary>
        /// <param name="writer">
        /// The xml writer that the state gets serialised out to.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        public override void SerialiseState(XmlWriter writer, IFormatProvider provider)
        {
            writer.WriteAttributeString("toggled", this.IsToggled.ToString(provider));
        }

        /// <summary>
        /// Override to handle the event that this definitions <see cref="IsToggled"/> flag
        /// changes.
        /// </summary>
        protected virtual void OnIsToggledChanged()
        {
        }
        #endregion Methods
    }  // RSG.Editor.ButtonCommand {Class}
} // RSG.Editor {Namespace}
