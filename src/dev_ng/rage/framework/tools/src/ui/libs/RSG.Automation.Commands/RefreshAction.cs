﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor;
using RSG.Automation.ViewModel;

namespace RSG.Automation.Commands
{
    public class RefreshAction : ButtonAction<AutomationMonitorDataContext>
    {
        #region Constructors(s)
        /// <summary>
        /// Refresh the automation monitor job view model
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public RefreshAction(ParameterResolverDelegate<AutomationMonitorDataContext> resolver)
            : base(resolver)
        {
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// Override to set logic against the can execute command handler.
        /// </summary>
        /// <param name="commandParameter"></param>
        /// <returns></returns>
        public override bool CanExecute(AutomationMonitorDataContext dc)
        {
            return true;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="commandParameter">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(AutomationMonitorDataContext dc)
        {
            dc.RefreshAsync();
        }
        #endregion // Overrides
    } // RefreshAction
}
