﻿//---------------------------------------------------------------------------------------------
// <copyright file="EmptyTask{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System.Threading.Tasks;

    /// <summary>
    /// A static class used to retrieve a static task that has already completed.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the generic tasks type that is retrieved.
    /// </typeparam>
    public static class EmptyTask<T>
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Task"/> property.
        /// </summary>
        private static readonly Task<T> _completedTask = Task.FromResult(default(T));
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets a already completed generic task.
        /// </summary>
        public static Task<T> CompletedTask
        {
            get { return _completedTask; }
        }
        #endregion Properties
    } // RSG.Editor.EmptyTask{T} {Class}
} // RSG.Editor {Namespace}
