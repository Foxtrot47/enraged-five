﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsCustomiseWindowDataContext.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Customisation
{
    using System.Collections.Generic;
    using System.Linq;
    using RSG.Editor.Controls.Resources;

    /// <summary>
    /// Represents the data context that should be attached to an instance of the
    /// <see cref="RsCustomiseWindow"/> class.
    /// </summary>
    internal class RsCustomiseWindowDataContext
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Containers"/> property.
        /// </summary>
        private MenuBarViewModel _mainMenu;

        /// <summary>
        /// The private field used for the <see cref="Containers"/> property.
        /// </summary>
        private ToolBarTrayViewModel _toolBarTray;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="RsCustomiseWindowDataContext"/> class.
        /// </summary>
        public RsCustomiseWindowDataContext()
        {
            this._toolBarTray = new ToolBarTrayViewModel();
            this._mainMenu = new MenuBarViewModel();
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the view model root containers to display in the tab control.
        /// </summary>
        public List<RootContainerViewModel> Containers
        {
            get
            {
                List<RootContainerViewModel> containers = new List<RootContainerViewModel>();
                containers.Add(this._toolBarTray);
                containers.Add(this._mainMenu);
                return containers;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Re-initialises the items in this container. This can be called once the commands
        /// have been reset to make sure the items shown to the user are correctly synced up
        /// against the model.
        /// </summary>
        public void ResetItems()
        {
            foreach (RootContainerViewModel container in this.Containers)
            {
                container.ResetItems();
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.Customisation.RsCustomiseWindowDataContext {Class}
} // RSG.Editor.Controls.Customisation {Namespace}
