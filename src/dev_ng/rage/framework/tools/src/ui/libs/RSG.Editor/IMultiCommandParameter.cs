﻿//---------------------------------------------------------------------------------------------
// <copyright file="IMultiCommandParameter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    /// <summary>
    /// When implemented represents a command parameter that can be used for a multi command
    /// item object.
    /// </summary>
    public interface IMultiCommandParameter
    {
        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether this parameter is currently been sent in
        /// a toggled state or not.
        /// </summary>
        bool IsToggled { get; set; }
        #endregion Properties
    } // RSG.Editor.IMultiCommandParameter {Interface}
} // RSG.Editor {Namespace}
