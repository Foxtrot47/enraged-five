﻿//---------------------------------------------------------------------------------------------
// <copyright file="IViewModel{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    using RSG.Editor.Model;

    /// <summary>
    /// Represents a view model object that is representing a model of the specified type.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the model that is being wrapped by this view model.
    /// </typeparam>
    public interface IViewModel<T> : IViewModel where T : IModel
    {
        #region Properties
        /// <summary>
        /// Gets the model that this view model is currently wrapping.
        /// </summary>
        new T Model { get; }
        #endregion Properties
    } // RSG.Editor.View.IViewModel{T} {Interface}
} // RSG.Editor.View {Namespace}
