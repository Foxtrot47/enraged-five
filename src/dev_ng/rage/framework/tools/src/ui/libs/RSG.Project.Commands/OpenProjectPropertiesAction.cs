﻿//---------------------------------------------------------------------------------------------
// <copyright file="OpenProjectPropertiesAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using RSG.Base.Extensions;
    using RSG.Editor;
    using RSG.Editor.Controls.Dock.ViewModel;
    using RSG.Project.Commands.Resources;
    using RSG.Project.ViewModel;

    /// <summary>
    /// Contains the logic for the <see cref="ProjectCommands.OpenProjectProperties"/> routed
    /// command. This class cannot be inherited.
    /// </summary>
    public sealed class OpenProjectPropertiesAction : ButtonAction<ProjectCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="OpenProjectPropertiesAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public OpenProjectPropertiesAction(ProjectCommandResolver resolver)
            : base(new ParameterResolverDelegate<ProjectCommandArgs>(resolver))
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ProjectCommandArgs args)
        {
            if (args == null)
            {
                return false;
            }

            ProjectNode project = this.ProjectSelection(args.SelectedNodes).FirstOrDefault();
            if (project != null)
            {
                return project.Loaded;
            }

            return false;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(ProjectCommandArgs args)
        {
            IMessageBoxService messageService = this.GetService<IMessageBoxService>();
            if (messageService == null)
            {
                Debug.Assert(false, "Unable to open as services are missing.");
                return;
            }

            ProjectNode project = this.ProjectSelection(args.SelectedNodes).FirstOrDefault();
            string fullPath = project.ProjectItem.GetMetadata("FullPath");
            if (fullPath == null)
            {
                Debug.Assert(false, "Unable to determine the full path to the project.");
                return;
            }

            foreach (DocumentItem openedDocument in args.OpenedDocuments)
            {
                ProjectDocumentItem projectDocument = openedDocument as ProjectDocumentItem;
                if (String.Equals(projectDocument.FullPath, fullPath))
                {
                    openedDocument.IsSelected = true;
                    return;
                }
            }

            DocumentPane pane = args.ViewSite.LastActiveDocumentPane;
            if (pane == null)
            {
                pane = args.ViewSite.FirstDocumentPane;
                if (pane == null)
                {
                    Debug.Assert(false, "Cannot find the pane to open the document in.");
                    return;
                }
            }

            FileInfo info = new FileInfo(fullPath);
            if (!info.Exists)
            {
                messageService.ShowStandardErrorBox(
                    StringTable.FileMissingMsg.FormatInvariant(fullPath),
                    null);

                return;
            }

            FileNode fileNode = project as FileNode;
            ProjectDocumentItem item = new ProjectDocumentItem(pane, fullPath, fileNode);
            try
            {
                item.Content = project.CreatePropertyContent(item.UndoEngine);
            }
            catch (Exception ex)
            {
                messageService.ShowStandardErrorBox(
                    StringTable.FailedFileParsing.FormatInvariant(ex.Message),
                    null);
                return;
            }

            item.IsReadOnly = info.IsReadOnly;
            item.IsSelected = true;
            pane.InsertChild(pane.Children.Count, item);
        }
        
        /// <summary>
        /// Retrieves the project selection from the specified selection collection by
        /// collecting all of the selected nodes parent project and removing duplicates.
        /// </summary>
        /// <param name="selection">
        /// The collection containing all of the selected nodes.
        /// </param>
        /// <returns>
        /// The project selection from the specified selection collection.
        /// </returns>
        private List<ProjectNode> ProjectSelection(IEnumerable<IHierarchyNode> selection)
        {
            List<ProjectNode> projectSelection = new List<ProjectNode>();
            if (selection == null)
            {
                return projectSelection;
            }

            foreach (IHierarchyNode selectedNode in selection)
            {
                ProjectNode parentProject = selectedNode.ParentProject;
                if (parentProject != null && !projectSelection.Contains(parentProject))
                {
                    projectSelection.Add(parentProject);
                }
            }

            return projectSelection;
        }
        #endregion Methods
    } // RSG.Project.Commands.OpenProjectPropertiesAction {Class}
} // RSG.Project.Commands {Namespace}
