﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsGlyphButton.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;

    /// <summary>
    /// Represents a button control that contains a glyph and has additional styling
    /// properties.
    /// </summary>
    public class RsGlyphButton : Button
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="Glyph"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty GlyphProperty;

        /// <summary>
        /// Identifies the <see cref="HoverBackground"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty HoverBackgroundProperty;

        /// <summary>
        /// Identifies the <see cref="HoverBorderBrush"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty HoverBorderBrushProperty;

        /// <summary>
        /// Identifies the <see cref="HoverBorderThickness"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty HoverBorderThicknessProperty;

        /// <summary>
        /// Identifies the <see cref="HoverForeground"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty HoverForegroundProperty;

        /// <summary>
        /// Identifies the <see cref="PressedBackground"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty PressedBackgroundProperty;

        /// <summary>
        /// Identifies the <see cref="PressedBorderBrush"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty PressedBorderBrushProperty;

        /// <summary>
        /// Identifies the <see cref="PressedBorderThickness"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty PressedBorderThicknessProperty;

        /// <summary>
        /// Identifies the <see cref="PressedForeground"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty PressedForegroundProperty;

        /// <summary>
        /// Identifies the <see cref="Stretch"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty StretchProperty;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsGlyphButton"/> class.
        /// </summary>
        static RsGlyphButton()
        {
            GlyphProperty =
                DependencyProperty.Register(
                "Glyph",
                typeof(Geometry),
                typeof(RsGlyphButton),
                new FrameworkPropertyMetadata(null));

            HoverBackgroundProperty =
                DependencyProperty.Register(
                "HoverBackground",
                typeof(Brush),
                typeof(RsGlyphButton),
                new FrameworkPropertyMetadata(null));

            HoverForegroundProperty =
                DependencyProperty.Register(
                "HoverForeground",
                typeof(Brush),
                typeof(RsGlyphButton),
                new FrameworkPropertyMetadata(null));

            HoverBorderBrushProperty =
                DependencyProperty.Register(
                "HoverBorderBrush",
                typeof(Brush),
                typeof(RsGlyphButton),
                new FrameworkPropertyMetadata(null));

            HoverBorderThicknessProperty =
                DependencyProperty.Register(
                "HoverBorderThickness",
                typeof(Thickness),
                typeof(RsGlyphButton),
                new FrameworkPropertyMetadata(null));

            PressedBackgroundProperty =
                DependencyProperty.Register(
                "PressedBackground",
                typeof(Brush),
                typeof(RsGlyphButton),
                new FrameworkPropertyMetadata(null));

            PressedForegroundProperty =
                DependencyProperty.Register(
                "PressedForeground",
                typeof(Brush),
                typeof(RsGlyphButton),
                new FrameworkPropertyMetadata(null));

            PressedBorderBrushProperty =
                DependencyProperty.Register(
                "PressedBorderBrush",
                typeof(Brush),
                typeof(RsGlyphButton),
                new FrameworkPropertyMetadata(null));

            PressedBorderThicknessProperty =
                DependencyProperty.Register(
                "PressedBorderThickness",
                typeof(Thickness),
                typeof(RsGlyphButton),
                new FrameworkPropertyMetadata(null));

            FrameworkPropertyMetadataOptions options =
                FrameworkPropertyMetadataOptions.AffectsMeasure |
                    FrameworkPropertyMetadataOptions.AffectsArrange;

            StretchProperty =
                DependencyProperty.Register(
                "Stretch",
                typeof(Stretch),
                typeof(RsGlyphButton),
                new FrameworkPropertyMetadata(Stretch.Uniform, options));

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsGlyphButton), new FrameworkPropertyMetadata(typeof(RsGlyphButton)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsGlyphButton"/> class.
        /// </summary>
        public RsGlyphButton()
        {
        }
        #endregion ConstructorsFill

        #region Properties
        /// <summary>
        /// Gets or sets the geometry to use for the glyph inside the button.
        /// </summary>
        public Geometry Glyph
        {
            get { return (Geometry)this.GetValue(GlyphProperty); }
            set { this.SetValue(GlyphProperty, value); }
        }

        /// <summary>
        /// Gets or sets a brush that describes the background colour when the mouse is hovered
        /// over it.
        /// </summary>
        public Brush HoverBackground
        {
            get { return (Brush)this.GetValue(HoverBackgroundProperty); }
            set { this.SetValue(HoverBackgroundProperty, value); }
        }

        /// <summary>
        /// Gets or sets a brush that describes the border background colour when the mouse is
        /// hovered over it.
        /// </summary>
        public Brush HoverBorderBrush
        {
            get { return (Brush)this.GetValue(HoverBorderBrushProperty); }
            set { this.SetValue(HoverBorderBrushProperty, value); }
        }

        /// <summary>
        /// Gets or sets the border thickness  when the mouse is hovered over it.
        /// </summary>
        public Thickness HoverBorderThickness
        {
            get { return (Thickness)this.GetValue(HoverBorderThicknessProperty); }
            set { this.SetValue(HoverBorderThicknessProperty, value); }
        }

        /// <summary>
        /// Gets or sets a brush that describes the foreground colour when the mouse is hovered
        /// over it.
        /// </summary>
        public Brush HoverForeground
        {
            get { return (Brush)this.GetValue(HoverForegroundProperty); }
            set { this.SetValue(HoverForegroundProperty, value); }
        }

        /// <summary>
        /// Gets or sets a brush that describes the background colour when the mouse is pressed
        /// on it.
        /// </summary>
        public Brush PressedBackground
        {
            get { return (Brush)this.GetValue(PressedBackgroundProperty); }
            set { this.SetValue(PressedBackgroundProperty, value); }
        }

        /// <summary>
        /// Gets or sets a brush that describes the border background colour when the mouse is
        /// pressed on it.
        /// </summary>
        public Brush PressedBorderBrush
        {
            get { return (Brush)this.GetValue(PressedBorderBrushProperty); }
            set { this.SetValue(PressedBorderBrushProperty, value); }
        }

        /// <summary>
        /// Gets or sets the border thickness  when the mouse is pressed on it.
        /// </summary>
        public Thickness PressedBorderThickness
        {
            get { return (Thickness)this.GetValue(PressedBorderThicknessProperty); }
            set { this.SetValue(PressedBorderThicknessProperty, value); }
        }

        /// <summary>
        /// Gets or sets a brush that describes the foreground colour when the mouse is pressed
        /// on it.
        /// </summary>
        public Brush PressedForeground
        {
            get { return (Brush)this.GetValue(PressedForegroundProperty); }
            set { this.SetValue(PressedForegroundProperty, value); }
        }

        /// <summary>
        /// Gets or sets the Stretch property that determines how the shape may be stretched
        /// to accommodate shape size.
        /// </summary>
        public Stretch Stretch
        {
            get { return (Stretch)this.GetValue(StretchProperty); }
            set { this.SetValue(StretchProperty, value); }
        }
        #endregion Properties
    } // RSG.Editor.Controls.RsGlyphButton {Class}
} // RSG.Editor.Controls {Namespace}
