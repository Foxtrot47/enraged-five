﻿//---------------------------------------------------------------------------------------------
// <copyright file="CloseDocumentItemsAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock.Commands
{
    using System.Collections.Generic;
    using RSG.Editor.Controls.Dock.Document;
    using RSG.Editor.Controls.Dock.ViewModel;

    /// <summary>
    /// The command action that is responsible for saving document items.
    /// </summary>
    public class SaveDocumentItemsAction : ButtonAction<List<RsDocumentItem>>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SaveDocumentItemsAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public SaveDocumentItemsAction(
            ParameterResolverDelegate<List<RsDocumentItem>> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(List<RsDocumentItem> parameter)
        {
            if (parameter.Count == 1)
            {
                string updatedText = "Save ";
                DocumentItem item = parameter[0].DataContext as DocumentItem;
                if (item == null)
                {
                    updatedText += parameter[0].Header as string;
                }
                else
                {
                    updatedText += item.FriendlySavePath;
                }

                RockstarCommandManager.UpdateItemText(this.CurrentCommand, updatedText);
                return true;
            }
            else if (parameter.Count != 0)
            {
                string updatedText = "Save Selected Items";
                RockstarCommandManager.UpdateItemText(this.CurrentCommand, updatedText);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(List<RsDocumentItem> parameter)
        {
        }
        #endregion Methods
    } // RSG.Editor.Controls.Dock.Commands.SaveDocumentItemsAction {Interface}
} // RSG.Editor.Controls.Dock.Commands {Namespace}
