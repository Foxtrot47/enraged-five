﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssemblyInfo.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows.Markup;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("RSG.Editor.dll")]
[assembly: AssemblyDescription("RSG.Editor.dll")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Rockstar Games")]
[assembly: AssemblyProduct("Rockstar Editor Framework")]
[assembly: AssemblyCopyright("© Rockstar Games. All rights reserved.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: CLSCompliant(false)]

[assembly: XmlnsDefinition(
    "http://schemas.rockstargames.com/2013/xaml/editor",
    "RSG.Editor")]

[assembly: XmlnsDefinition(
    "http://schemas.rockstargames.com/2013/xaml/editor",
    "RSG.Editor.View")]

[assembly: XmlnsDefinition(
    "http://schemas.rockstargames.com/2013/xaml/editor",
    "RSG.Editor.Model")]

[assembly: XmlnsDefinition(
    "http://schemas.rockstargames.com/2013/xaml/editor",
    "RSG.Editor.SharedCommands")]

[assembly: XmlnsPrefix("http://schemas.rockstargames.com/2013/xaml/editor", "rsg")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("E77645B9-510D-4FFC-A6DA-1476EAF59CEB")]

// Makes it possible for the controls to see the internals of this assembly.
[assembly: InternalsVisibleTo("RSG.Editor.Controls")]

// Version information for an assembly consists of the following four values:
//
// Major Version
// Minor Version
// Build Number
// Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: NeutralResourcesLanguageAttribute("en-GB")]
