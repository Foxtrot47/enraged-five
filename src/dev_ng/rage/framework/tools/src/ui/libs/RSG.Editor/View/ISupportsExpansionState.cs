﻿//---------------------------------------------------------------------------------------------
// <copyright file="ISupportsExpansionState.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    /// <summary>
    /// When implemented represents a item that contains a default expansion state that can be
    /// used by the user interface.
    /// </summary>
    public interface ISupportsExpansionState
    {
        #region Properties
        /// <summary>
        /// Gets a value indicating whether this item is expanded by default in the user
        /// interface when by rendered inside a hierarchical control.
        /// </summary>
        bool IsExpandedByDefault { get; }

        /// <summary>
        /// Gets a value indicating whether this items children are destroyed when it is
        /// collapsed. The resulting behaviour if true is that the references are kept in a
        /// cache and the expansion state is persistent for child nodes.
        /// </summary>
        bool DestroyNodesOnCollapse { get; }
        #endregion Properties
    } // RSG.Editor.View.ISupportsExpansionState {Interface}
} // RSG.Editor.View {Namespace}
