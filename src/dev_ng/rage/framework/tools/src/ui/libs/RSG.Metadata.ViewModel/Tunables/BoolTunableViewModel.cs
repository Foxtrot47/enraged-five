﻿//---------------------------------------------------------------------------------------------
// <copyright file="BoolTunableViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Tunables
{
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// The view model for a <see cref="BoolTunable"/> model object.
    /// </summary>
    public class BoolTunableViewModel : TunableViewModelBase<BoolTunable>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BoolTunableViewModel"/> class.
        /// </summary>
        /// <param name="tunable">
        /// The bool tunable that this view model is representing.
        /// </param>
        /// <param name="parent">
        /// The parent tunable view model for this view model.
        /// </param>
        public BoolTunableViewModel(BoolTunable tunable, ITunableViewModel parent)
            : base(tunable, parent)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the value currently assigned to the bool tunable.
        /// </summary>
        public bool Value
        {
            get { return this.Model.Value; }
            set { this.Model.Value = value; }
        }
        #endregion Properties
    } // RSG.Metadata.ViewModel.Tunables.BoolTunableViewModel {Class}
} // RSG.Metadata.ViewModel.Tunables {Namespace}
