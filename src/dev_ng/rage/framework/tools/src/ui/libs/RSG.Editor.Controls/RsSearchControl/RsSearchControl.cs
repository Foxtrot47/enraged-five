﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsSearchControl.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Collections;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.Threading;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Data;
    using System.Windows.Input;
    using System.Windows.Threading;
    using RSG.Editor.Controls.Converters;
    using RSG.Editor.Controls.Helpers;
    using RSG.Editor.SharedCommands;
    using RSG.Interop.Microsoft.Windows;

    /// <summary>
    /// Represents a control that can be used to perform a search with additional search
    /// options.
    /// </summary>
    public class RsSearchControl : ComboBox
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="Command" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty CommandProperty;

        /// <summary>
        /// Identifies the <see cref="CommandTarget" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty CommandTargetProperty;

        /// <summary>
        /// Identifies the <see cref="FilteringChecked" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty FilteringCheckedProperty;

        /// <summary>
        /// Identifies the <see cref="HasDropDown" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty HasDropDownProperty;

        /// <summary>
        /// Identifies the <see cref="HasError" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty HasErrorProperty;

        /// <summary>
        /// Identifies the <see cref="MatchCaseChecked" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty MatchCaseCheckedProperty;

        /// <summary>
        /// Identifies the <see cref="MatchWordChecked" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty MatchWordCheckedProperty;

        /// <summary>
        /// Identifies the <see cref="RegularExpressionsChecked" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty RegularExpressionsCheckedProperty;

        /// <summary>
        /// Identifies the <see cref="Scopes" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty ScopesProperty;

        /// <summary>
        /// Identifies the <see cref="SearchStarted" /> routed event.
        /// </summary>
        public static readonly RoutedEvent SearchStartedEvent;

        /// <summary>
        /// Identifies the <see cref="SearchStopped" /> routed event.
        /// </summary>
        public static readonly RoutedEvent SearchStoppedEvent;

        /// <summary>
        /// Identifies the <see cref="SearchText" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty SearchTextProperty;

        /// <summary>
        /// Identifies the <see cref="SelectedScope" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty SelectedScopeProperty;

        /// <summary>
        /// Identifies the <see cref="ShowClear" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty ShowClearProperty;

        /// <summary>
        /// Identifies the <see cref="SupportFiltering" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty SupportFilteringProperty;

        /// <summary>
        /// Identifies the <see cref="SupportMatchCase" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty SupportMatchCaseProperty;

        /// <summary>
        /// Identifies the <see cref="SupportMatchWord" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty SupportMatchWordProperty;

        /// <summary>
        /// Identifies the <see cref="SupportRegularExpressions" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty SupportRegularExpressionsProperty;

        /// <summary>
        /// Identifies the <see cref="Watermark" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty WatermarkProperty;

        /// <summary>
        /// Identifies the <see cref="HasError" /> dependency property.
        /// </summary>
        private static DependencyPropertyKey _hasErrorPropertyKey;

        /// <summary>
        /// Identifies the <see cref="ShowClear" /> dependency property.
        /// </summary>
        private static DependencyPropertyKey _showClearPropertyKey;

        /// <summary>
        /// The data that should be used after the current search has been cancelled.
        /// </summary>
        private SearchData _cancelledSearchData;

        /// <summary>
        /// The private field used for the <see cref="CanExecute"/> property.
        /// </summary>
        private bool _canExecute;

        /// <summary>
        /// The data that is being used for the current search.
        /// </summary>
        private SearchData _currentSearchData;

        /// <summary>
        /// The last text string that was searched for.
        /// </summary>
        private string _lastSearchText;

        /// <summary>
        /// The private field used for the <see cref="MaximumMruListCount"/> property.
        /// </summary>
        private uint _maximumMruListCount;

        /// <summary>
        /// The private list of previous search strings that can be selected for quick search.
        /// </summary>
        private ObservableCollection<string> _mruList;

        /// <summary>
        /// The private field used for the <see cref="SearchStartMinChars"/> property.
        /// </summary>
        private ushort _searchStartMinChars;

        /// <summary>
        /// The status of the current search.
        /// </summary>
        private SearchStatus _searchStatus;

        /// <summary>
        /// The private reference to the text box used for the search string.
        /// </summary>
        private TextBox _searchTextBox;

        /// <summary>
        /// The private field used for the <see cref="SearchTrimsWhitespaces"/> property.
        /// </summary>
        private bool _searchTrimsWhitespaces;

        /// <summary>
        /// The private field used for the <see cref="SearchUsesMru"/> property.
        /// </summary>
        private bool _searchUsesMru;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsSearchControl" /> class.
        /// </summary>
        static RsSearchControl()
        {
            SearchStartedEvent =
                EventManager.RegisterRoutedEvent(
                "SearchStarted",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(RsSearchControl));

            SearchStoppedEvent =
                EventManager.RegisterRoutedEvent(
                "SearchStopped",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(RsSearchControl));

            SelectedScopeProperty =
                DependencyProperty.Register(
                "SelectedScope",
                typeof(string),
                typeof(RsSearchControl),
                new FrameworkPropertyMetadata(null));

            CommandProperty =
                ButtonBase.CommandProperty.AddOwner(
                typeof(RsSearchControl),
                new FrameworkPropertyMetadata(null, OnCommandChanged));

            CommandTargetProperty =
                ButtonBase.CommandTargetProperty.AddOwner(
                typeof(RsSearchControl), new FrameworkPropertyMetadata(null));

            WatermarkProperty =
                DependencyProperty.Register(
                "Watermark",
                typeof(string),
                typeof(RsSearchControl),
                new FrameworkPropertyMetadata(null));

            SearchTextProperty =
                DependencyProperty.Register(
                "SearchText",
                typeof(string),
                typeof(RsSearchControl),
                new FrameworkPropertyMetadata(String.Empty));

            HasDropDownProperty =
                DependencyProperty.Register(
                "HasDropDown",
                typeof(bool),
                typeof(RsSearchControl),
                new FrameworkPropertyMetadata(true));

            SupportMatchCaseProperty =
                DependencyProperty.Register(
                "SupportMatchCase",
                typeof(bool),
                typeof(RsSearchControl),
                new FrameworkPropertyMetadata(true));

            SupportMatchWordProperty =
                DependencyProperty.Register(
                "SupportMatchWord",
                typeof(bool),
                typeof(RsSearchControl),
                new FrameworkPropertyMetadata(true));

            SupportRegularExpressionsProperty =
                DependencyProperty.Register(
                "SupportRegularExpressions",
                typeof(bool),
                typeof(RsSearchControl),
                new FrameworkPropertyMetadata(true));

            SupportFilteringProperty =
                DependencyProperty.Register(
                "SupportFiltering",
                typeof(bool),
                typeof(RsSearchControl),
                new FrameworkPropertyMetadata(true));

            MatchCaseCheckedProperty =
                DependencyProperty.Register(
                "MatchCaseChecked",
                typeof(bool),
                typeof(RsSearchControl),
                new FrameworkPropertyMetadata(false, OnOptionChanged, CoerceMatchCaseChecked));

            MatchWordCheckedProperty =
                DependencyProperty.Register(
                "MatchWordChecked",
                typeof(bool),
                typeof(RsSearchControl),
                new FrameworkPropertyMetadata(false, OnOptionChanged, CoerceMatchWordChecked));

            RegularExpressionsCheckedProperty =
                DependencyProperty.Register(
                "RegularExpressionsChecked",
                typeof(bool),
                typeof(RsSearchControl),
                new FrameworkPropertyMetadata(
                    false, OnOptionChanged, CoerceRegularExpressionsChecked));

            ScopesProperty =
                DependencyProperty.Register(
                "Scopes",
                typeof(IEnumerable),
                typeof(RsSearchControl),
                new FrameworkPropertyMetadata(null));

            FilteringCheckedProperty =
                DependencyProperty.Register(
                "FilteringChecked",
                typeof(bool),
                typeof(RsSearchControl),
                new FrameworkPropertyMetadata(false, OnOptionChanged, CoerceFilteringChecked));

            _hasErrorPropertyKey =
                DependencyProperty.RegisterReadOnly(
                "HasError",
                typeof(bool),
                typeof(RsSearchControl),
                new PropertyMetadata(false));

            HasErrorProperty = _hasErrorPropertyKey.DependencyProperty;

            _showClearPropertyKey =
                DependencyProperty.RegisterReadOnly(
                "ShowClear",
                typeof(bool),
                typeof(RsSearchControl),
                new PropertyMetadata(false));

            ShowClearProperty = _showClearPropertyKey.DependencyProperty;

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsSearchControl),
                new FrameworkPropertyMetadata(typeof(RsSearchControl)));

            RockstarCommandManager.AddBinding(
                typeof(Window), RockstarCommands.QuickFind, OnQuickFind);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsSearchControl" /> class.
        /// </summary>
        public RsSearchControl()
        {
            this.IsEditable = true;
            this._mruList = new ObservableCollection<string>();

            Binding binding = new Binding();
            binding.BindsDirectlyToSource = true;
            binding.Source = this._mruList;
            this.SetBinding(ComboBox.ItemsSourceProperty, binding);

            this._searchStatus = SearchStatus.NotStarted;
            this._searchStartMinChars = 1;
            this._maximumMruListCount = 15;
            this._canExecute = true;
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs when a search is initialised.
        /// </summary>
        public event RoutedEventHandler SearchStarted
        {
            add { this.AddHandler(RsSearchControl.SearchStartedEvent, value); }
            remove { this.RemoveHandler(RsSearchControl.SearchStartedEvent, value); }
        }

        /// <summary>
        /// Occurs when a search is cancelled or cleared.
        /// </summary>
        public event RoutedEventHandler SearchStopped
        {
            add { this.AddHandler(RsSearchControl.SearchStoppedEvent, value); }
            remove { this.RemoveHandler(RsSearchControl.SearchStoppedEvent, value); }
        }
        #endregion Events

        #region Enums
        /// <summary>
        /// Defines the different status the actual search can be in at any one time.
        /// </summary>
        private enum SearchStatus
        {
            /// <summary>
            /// Represents that a search has not been started yet.
            /// </summary>
            NotStarted,

            /// <summary>
            /// Represents that a search is currently in progress.
            /// </summary>
            InProgress,

            /// <summary>
            /// Represents that a search has been completed and the results have been returned.
            /// </summary>
            Complete
        } // RSG.Editor.Controls.RsSearchControl.SearchStatus {Enum}
        #endregion Enums

        #region Properties
        /// <summary>
        /// Gets or sets the command associated with the menu item.
        /// </summary>
        public ICommand Command
        {
            get { return (ICommand)this.GetValue(CommandProperty); }
            set { this.SetValue(CommandProperty, value); }
        }

        /// <summary>
        /// Gets or sets the target element on which to raise the specified command.
        /// </summary>
        public IInputElement CommandTarget
        {
            get { return (IInputElement)this.GetValue(CommandTargetProperty); }
            set { this.SetValue(CommandTargetProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the filtering option is currently checked
        /// or not.
        /// </summary>
        public bool FilteringChecked
        {
            get { return (bool)this.GetValue(FilteringCheckedProperty); }
            set { this.SetValue(FilteringCheckedProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the drop down portion of this control is
        /// included.
        /// </summary>
        public bool HasDropDown
        {
            get { return (bool)this.GetValue(HasDropDownProperty); }
            set { this.SetValue(HasDropDownProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this control is currently showing a error.
        /// </summary>
        public bool HasError
        {
            get { return (bool)this.GetValue(HasErrorProperty); }
            set { this.SetValue(_hasErrorPropertyKey, value); }
        }

        /// <summary>
        /// Gets a value indicating whether this control is editable using keyboard input.
        /// </summary>
        public new bool IsEditable
        {
            get { return base.IsEditable; }
            private set { base.IsEditable = value; }
        }

        /// <summary>
        /// Gets the collection used to generate the items of this control.
        /// </summary>
        public new IEnumerable ItemsSource
        {
            get { return base.ItemsSource; }
            private set { base.ItemsSource = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the match case option is currently checked
        /// or not.
        /// </summary>
        public bool MatchCaseChecked
        {
            get { return (bool)this.GetValue(MatchCaseCheckedProperty); }
            set { this.SetValue(MatchCaseCheckedProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the match word option is currently checked
        /// or not.
        /// </summary>
        public bool MatchWordChecked
        {
            get { return (bool)this.GetValue(MatchWordCheckedProperty); }
            set { this.SetValue(MatchWordCheckedProperty, value); }
        }

        /// <summary>
        /// Gets or sets the maximum number of items that can be added to the MRU list.
        /// </summary>
        public uint MaximumMruListCount
        {
            get
            {
                return this._maximumMruListCount;
            }

            set
            {
                this._maximumMruListCount = value;
                this.LimitMruSize();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the regular expressions option is currently
        /// checked or not.
        /// </summary>
        public bool RegularExpressionsChecked
        {
            get { return (bool)this.GetValue(RegularExpressionsCheckedProperty); }
            set { this.SetValue(RegularExpressionsCheckedProperty, value); }
        }

        /// <summary>
        /// Gets or sets the collection of scopes this control should show.
        /// </summary>
        public IEnumerable Scopes
        {
            get { return (IEnumerable)this.GetValue(ScopesProperty); }
            set { this.SetValue(ScopesProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating what the minimum number of characters needs to be
        /// to start searching.
        /// </summary>
        public ushort SearchStartMinChars
        {
            get { return this._searchStartMinChars; }
            set { this._searchStartMinChars = value; }
        }

        /// <summary>
        /// Gets or sets the search text that is currently set.
        /// </summary>
        public string SearchText
        {
            get { return (string)this.GetValue(SearchTextProperty); }
            set { this.SetValue(SearchTextProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the search automatically trims whitespaces
        /// off of the search text.
        /// </summary>
        public bool SearchTrimsWhitespaces
        {
            get { return this._searchTrimsWhitespaces; }
            set { this._searchTrimsWhitespaces = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the search most recent list is being used.
        /// </summary>
        public bool SearchUsesMru
        {
            get { return this._searchUsesMru; }
            set { this._searchUsesMru = value; }
        }

        /// <summary>
        /// Gets or sets the currently selected scope.
        /// </summary>
        public string SelectedScope
        {
            get { return (string)this.GetValue(SelectedScopeProperty); }
            set { this.SetValue(SelectedScopeProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this control should be showing the clear
        /// button.
        /// </summary>
        public bool ShowClear
        {
            get { return (bool)this.GetValue(ShowClearProperty); }
            set { this.SetValue(_showClearPropertyKey, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this control shows the user the filter
        /// results toggle button.
        /// </summary>
        public bool SupportFiltering
        {
            get { return (bool)this.GetValue(SupportFilteringProperty); }
            set { this.SetValue(SupportFilteringProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this control shows the user the match case
        /// toggle button.
        /// </summary>
        public bool SupportMatchCase
        {
            get { return (bool)this.GetValue(SupportMatchCaseProperty); }
            set { this.SetValue(SupportMatchCaseProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this control shows the user the match whole
        /// word toggle button.
        /// </summary>
        public bool SupportMatchWord
        {
            get { return (bool)this.GetValue(SupportMatchWordProperty); }
            set { this.SetValue(SupportMatchWordProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this control shows the user the use
        /// regular expressions toggle button.
        /// </summary>
        public bool SupportRegularExpressions
        {
            get { return (bool)this.GetValue(SupportRegularExpressionsProperty); }
            set { this.SetValue(SupportRegularExpressionsProperty, value); }
        }

        /// <summary>
        /// Gets or sets the watermark used when the control doesn't have keyboard focus and
        /// no search text is set.
        /// </summary>
        public string Watermark
        {
            get { return (string)this.GetValue(WatermarkProperty); }
            set { this.SetValue(WatermarkProperty, value); }
        }

        /// <summary>
        /// Gets a value that indicates whether the IsEnabled property is true for the current
        /// combo box.
        /// </summary>
        protected override bool IsEnabledCore
        {
            get { return base.IsEnabledCore && this.CanExecute; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the command attached to this control can be
        /// executed by the user.
        /// </summary>
        private bool CanExecute
        {
            get
            {
                return this._canExecute;
            }

            set
            {
                if (this._canExecute != value)
                {
                    this._canExecute = value;
                    this.CoerceValue(UIElement.IsEnabledProperty);
                }
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Called whenever application code or framework processes call
        /// System.Windows.FrameworkElement.ApplyTemplate.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            this.AddHandler(
                RsSearchControlItem.MruItemSelectedEvent,
                new RoutedEventHandler(this.OnMruItemSelected));

            this._searchTextBox = this.GetTemplateChild("PART_EditableTextBox") as TextBox;
            if (this._searchTextBox != null)
            {
                this._searchTextBox.LostKeyboardFocus += this.OnLostKeyboardFocus;
                this._searchTextBox.TextChanged += this.OnTextChanged;
            }

            ComboBox scopes = this.GetTemplateChild("PART_ScopeSelector") as ComboBox;
            if (scopes != null)
            {
                Binding binding = new Binding("Scopes");
                binding.Source = this;
                scopes.SetBinding(ComboBox.ItemsSourceProperty, binding);

                Binding visibilityBinding = new Binding("Count");
                visibilityBinding.Source = scopes.Items;
                visibilityBinding.Converter = new IntegerToVisibilityConverter(2, true);
                scopes.SetBinding(UIElement.VisibilityProperty, visibilityBinding);
            }

            Button searchButton = this.GetTemplateChild("PART_SearchButton") as Button;
            if (searchButton != null)
            {
                searchButton.Click += this.OnSearchButtonClicked;
            }
        }

        /// <summary>
        /// Creates or identifies the element used to display a specified item.
        /// </summary>
        /// <returns>
        /// A new instance of the <see cref="RsSearchControlItem"/> class.
        /// </returns>
        protected override DependencyObject GetContainerForItemOverride()
        {
            return new RsSearchControlItem();
        }

        /// <summary>
        /// Determines if the specified item is (or is eligible to be) its own ItemContainer.
        /// </summary>
        /// <param name="item">
        /// Specified item.
        /// </param>
        /// <returns>
        /// True if the item is its own ItemContainer; otherwise, false.
        /// </returns>
        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is RsSearchControlItem;
        }

        /// <summary>
        /// Invoked when an unhandled System.Windows.Input.Keyboard.KeyDown attached event
        /// reaches an element in its route that is derived from this class.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.KeyEventArgs that contains the event data.
        /// </param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.Key != Key.Enter || e.KeyboardDevice.Modifiers != ModifierKeys.None)
            {
                base.OnKeyDown(e);
                return;
            }

            if (this._searchTextBox == null || this._mruList == null)
            {
                base.OnKeyDown(e);
                return;
            }

            string text = this.TrimSearchString(this._searchTextBox.Text);
            if (this._mruList.Contains(text))
            {
                IInputElement inputElement = this.CommandTarget;
                if (inputElement == null)
                {
                    inputElement = this as IInputElement;
                }

                ICommand command = RockstarCommands.FindNext;
                RoutedCommand routedCommand = command as RoutedCommand;
                if (routedCommand != null)
                {
                    if (routedCommand.CanExecute(this._currentSearchData, inputElement))
                    {
                        routedCommand.Execute(this._currentSearchData, inputElement);
                    }
                }
                else if (command != null)
                {
                    if (command.CanExecute(this._currentSearchData))
                    {
                        command.Execute(this._currentSearchData);
                    }
                }
            }
            else
            {
                if (!String.IsNullOrWhiteSpace(this._searchTextBox.Text))
                {
                    this.AddToMruItems(text);
                }
            }

            return;
        }

        /// <summary>
        /// Called whenever the <see cref="FilteringChecked"/> dependency property needs to be
        /// re-evaluated.
        /// </summary>
        /// <param name="d">
        /// The <see cref="RsSearchControl"/> instance whose dependency property needs
        /// evaluated.
        /// </param>
        /// <param name="value">
        /// The new value of the property, prior to any coercion attempt.
        /// </param>
        /// <returns>
        /// The coerced value.
        /// </returns>
        private static object CoerceFilteringChecked(DependencyObject d, object value)
        {
            RsSearchControl searchControl = (RsSearchControl)d;
            if (!searchControl.SupportFiltering)
            {
                return false;
            }

            return value;
        }

        /// <summary>
        /// Called whenever the <see cref="MatchCaseChecked"/> dependency property needs to be
        /// re-evaluated.
        /// </summary>
        /// <param name="d">
        /// The <see cref="RsSearchControl"/> instance whose dependency property needs
        /// evaluated.
        /// </param>
        /// <param name="value">
        /// The new value of the property, prior to any coercion attempt.
        /// </param>
        /// <returns>
        /// The coerced value.
        /// </returns>
        private static object CoerceMatchCaseChecked(DependencyObject d, object value)
        {
            RsSearchControl searchControl = d as RsSearchControl;
            if (d == null)
            {
                Debug.Assert(d != null, "Coerce method called from a foreign source.");
                return value;
            }

            if (!searchControl.SupportMatchCase)
            {
                return false;
            }

            return value;
        }

        /// <summary>
        /// Called whenever the <see cref="MatchWordChecked"/> dependency property needs to be
        /// re-evaluated.
        /// </summary>
        /// <param name="d">
        /// The <see cref="RsSearchControl"/> instance whose dependency property needs
        /// evaluated.
        /// </param>
        /// <param name="value">
        /// The new value of the property, prior to any coercion attempt.
        /// </param>
        /// <returns>
        /// The coerced value.
        /// </returns>
        private static object CoerceMatchWordChecked(DependencyObject d, object value)
        {
            RsSearchControl searchControl = d as RsSearchControl;
            if (d == null)
            {
                Debug.Assert(d != null, "Coerce method called from a foreign source.");
                return value;
            }

            if (!searchControl.SupportMatchWord)
            {
                return false;
            }

            return value;
        }

        /// <summary>
        /// Called whenever the <see cref="RegularExpressionsChecked"/> dependency property
        /// needs to be re-evaluated.
        /// </summary>
        /// <param name="d">
        /// The <see cref="RsSearchControl"/> instance whose dependency property needs
        /// evaluated.
        /// </param>
        /// <param name="value">
        /// The new value of the property, prior to any coercion attempt.
        /// </param>
        /// <returns>
        /// The coerced value.
        /// </returns>
        private static object CoerceRegularExpressionsChecked(DependencyObject d, object value)
        {
            RsSearchControl searchControl = d as RsSearchControl;
            if (d == null)
            {
                Debug.Assert(d != null, "Coerce method called from a foreign source.");
                return value;
            }

            if (!searchControl.SupportRegularExpressions)
            {
                return false;
            }

            return value;
        }

        /// <summary>
        /// Called whenever the <see cref="Command"/> dependency property changes.
        /// </summary>
        /// <param name="d">
        /// The object whose <see cref="Command"/> dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnCommandChanged(
            DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RsSearchControl searchControl = d as RsSearchControl;
            if (searchControl == null)
            {
                return;
            }

            ICommand oldCommand = e.OldValue as ICommand;
            if (oldCommand != null)
            {
                CanExecuteChangedEventManager.RemoveHandler(
                    oldCommand, searchControl.OnCanExecuteChanged);
            }

            ICommand newCommand = e.NewValue as ICommand;
            if (newCommand != null)
            {
                CanExecuteChangedEventManager.AddHandler(
                    newCommand, searchControl.OnCanExecuteChanged);
            }

            searchControl.UpdateCanExecute();
        }

        /// <summary>
        /// Called whenever an options dependency property changes.
        /// </summary>
        /// <param name="d">
        /// The object whose option dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnOptionChanged(
            DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RsSearchControl searchControl = d as RsSearchControl;
            if (searchControl == null || searchControl._searchTextBox == null)
            {
                return;
            }

            searchControl._lastSearchText = null;
            searchControl.StartSearch(searchControl._searchTextBox.Text, true);
        }

        /// <summary>
        /// Called whenever the <see cref="RockstarCommands.QuickFind"/> command is fired and
        /// needs handling at this instance level.
        /// </summary>
        /// <param name="data">
        /// The execute data that has been sent with the command.
        /// </param>
        private static void OnQuickFind(ExecuteCommandData data)
        {
            DependencyObject source = data.OriginalSource as DependencyObject;
            if (source == null)
            {
                return;
            }

            RsSearchControl searchControl = source.GetVisualDescendent<RsSearchControl>();
            if (searchControl == null || !searchControl.IsEnabled)
            {
                return;
            }

            TextBox textBox = searchControl.GetVisualDescendent<TextBox>();
            if (textBox == null || !textBox.IsEnabled)
            {
                return;
            }

            textBox.Focus();
        }

        /// <summary>
        /// Adds the specified text to the MRU list associated with this control.
        /// </summary>
        /// <param name="searchedText">
        /// The text that the added item will represent.
        /// </param>
        private void AddToMruItems(string searchedText)
        {
            if (!this.SearchUsesMru)
            {
                return;
            }

            if (searchedText.Length == 0)
            {
                return;
            }

            int index = this._mruList.IndexOf(searchedText);
            while (index != -1)
            {
                this._mruList.RemoveAt(index);
                index = this._mruList.IndexOf(searchedText);
            }

            this._mruList.Insert(0, searchedText);
            this.LimitMruSize();
        }

        /// <summary>
        /// Currents the search data to send with the associated command using the specified
        /// text as the search text.
        /// </summary>
        /// <param name="searchText">
        /// The search text that should be added to the returned search data object.
        /// </param>
        /// <returns>
        /// The new instance of <see cref="SearchData"/> that describes the current state of
        /// the controls options as well as the specified search text.
        /// </returns>
        private SearchData CreateCurrentSearchData(string searchText)
        {
            SearchData data = new SearchData();
            data.FilteringResults = this.FilteringChecked;
            data.MatchCase = this.MatchCaseChecked;
            data.MatchWord = this.MatchWordChecked;
            data.RegularExpressions = this.RegularExpressionsChecked;
            data.SearchText = searchText;
            data.CancellationTokenSource = new CancellationTokenSource();
            return data;
        }

        /// <summary>
        /// Makes sure the size of the MRU list doesn't exceed the
        /// <see cref="MaximumMruListCount"/> value.
        /// </summary>
        private void LimitMruSize()
        {
            while (this._mruList.Count > this._maximumMruListCount)
            {
                this._mruList.RemoveAt(this._mruList.Count - 1);
            }
        }

        /// <summary>
        /// Called whenever the command attached to this control sends an event that indicates
        /// the can execute state of it might have changed.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.EventArgs data for this event.
        /// </param>
        private void OnCanExecuteChanged(object sender, EventArgs e)
        {
            this.UpdateCanExecute();
        }

        /// <summary>
        /// Gets called whenever the keyboard focus is lost on the search text box defined
        /// inside the control template.
        /// </summary>
        /// <param name="s">
        /// The object this event handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.KeyboardFocusChangedEventArgs data sent with this event.
        /// </param>
        private void OnLostKeyboardFocus(object s, KeyboardFocusChangedEventArgs e)
        {
            if (this.IsKeyboardFocusWithin)
            {
                return;
            }

            this.IsDropDownOpen = false;
            if (this._searchStatus != SearchStatus.NotStarted)
            {
                this.AddToMruItems(this.TrimSearchString(this._searchTextBox.Text));
            }
        }

        /// <summary>
        /// Called whenever the user selects a item from the MRU list and kicks of a new search
        /// if allowed.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs data that is sent with this event that is used
        /// to determine which MRU item was selected.
        /// </param>
        private void OnMruItemSelected(object sender, RoutedEventArgs e)
        {
            RsSearchControlItem item = e.OriginalSource as RsSearchControlItem;
            if (item == null)
            {
                return;
            }

            string searchText = item.Content as string;
            if (searchText == null)
            {
                Debug.Assert(searchText != null, "Mru list found with no search text!");
                return;
            }

            searchText = this.TrimSearchString(searchText);
            if (this._searchStatus == SearchStatus.InProgress)
            {
                if (!this.ShouldSearchRestart(searchText))
                {
                    return;
                }
            }

            this.StartSearch(searchText, true);
        }

        /// <summary>
        /// Gets called whenever the search button is clicked.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private void OnSearchButtonClicked(object sender, RoutedEventArgs e)
        {
            if (this.ShowClear)
            {
                this.IsDropDownOpen = false;
                this.Text = String.Empty;
                if (this._searchStatus == SearchStatus.InProgress)
                {
                    if (!this.ShouldSearchRestart(String.Empty))
                    {
                        return;
                    }
                }

                this.StartSearch(String.Empty, false);
            }
            else
            {
                this.StartSearch(this.TrimSearchString(this._searchTextBox.Text), false);
            }
        }

        /// <summary>
        /// Called to register that a search has finished. This event comes from the
        /// client of the current search data and is used for searches that are done on a
        /// background thread.
        /// </summary>
        /// <param name="sender">
        /// The object that this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.EventArgs data sent with this event.
        /// </param>
        private void OnSearchFinished(object sender, SearchFinishedEventArgs e)
        {
            this._searchStatus = SearchStatus.Complete;
            this._currentSearchData.CancellationTokenSource.Dispose();
            this._currentSearchData.SearchStarting -= this.OnSearchStarting;
            this._currentSearchData.SearchFinished -= this.OnSearchFinished;

            if (!this._currentSearchData.Cleared && !e.FoundEntries)
            {
                this.HasError = true;
            }
            else
            {
                this.HasError = false;
            }

            if (this._cancelledSearchData != null)
            {
                this._lastSearchText = this._currentSearchData.SearchText;
                this.Dispatcher.Invoke(
                    new Action(
                        delegate
                        {
                            this.StartSearch(this._cancelledSearchData.SearchText, true);
                        }));
            }
        }

        /// <summary>
        /// Called to register that a search is about to begin. This event comes from the
        /// client of the current search data and is used for searches that are done on a
        /// background thread.
        /// </summary>
        /// <param name="sender">
        /// The object that this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.EventArgs data sent with this event.
        /// </param>
        private void OnSearchStarting(object sender, EventArgs e)
        {
            this._searchStatus = SearchStatus.InProgress;
        }

        /// <summary>
        /// Called whenever the text changes inside the search text box defined within the
        /// controls template.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Controls.TextChangedEventArgs data sent with the event.
        /// </param>
        private void OnTextChanged(object sender, TextChangedEventArgs e)
        {
            this.StartSearch(this.TrimSearchString(this._searchTextBox.Text), true);
        }

        /// <summary>
        /// Determines whether the current search should be restarted.
        /// </summary>
        /// <param name="searchText">
        /// The new search text.
        /// </param>
        /// <returns>
        /// True if the current search should be restarted with the specified text; otherwise,
        /// false.
        /// </returns>
        private bool ShouldSearchRestart(string searchText)
        {
            return searchText != this._lastSearchText;
        }

        /// <summary>
        /// Starts a new search by firing the applicable events and sending the associated
        /// command with the current search data.
        /// </summary>
        /// <param name="searchText">
        /// The text that is currently being searched.
        /// </param>
        /// <param name="checkLength">
        /// A value indicating whether the length of the specified text should be checked
        /// against the minimum number of characters property.
        /// </param>
        private void StartSearch(string searchText, bool checkLength)
        {
            if (!this.ShouldSearchRestart(searchText))
            {
                return;
            }

            this._lastSearchText = searchText;
            bool cleared = false;
            if (searchText.Length == 0 || searchText.Length < this.SearchStartMinChars)
            {
                cleared = true;
                this.ShowClear = false;
            }
            else if (searchText.Length < this.SearchStartMinChars && checkLength)
            {
                cleared = true;
                this.ShowClear = false;
            }
            else
            {
                this.ShowClear = true;
            }

            RoutedEventArgs e = new RoutedEventArgs();
            if (cleared)
            {
                e = new RoutedEventArgs(RsSearchControl.SearchStoppedEvent, this);
            }
            else
            {
                e = new RoutedEventArgs(RsSearchControl.SearchStartedEvent, this);
            }

            this.RaiseEvent(e);
            if (this.Command == null)
            {
                return;
            }

            IInputElement inputElement = this.CommandTarget;
            if (inputElement == null)
            {
                inputElement = this as IInputElement;
            }

            if (this._searchStatus == SearchStatus.InProgress)
            {
                this._cancelledSearchData = this.CreateCurrentSearchData(searchText);
                this._currentSearchData.CancellationTokenSource.Cancel();
                return;
            }

            this._cancelledSearchData = null;
            this._currentSearchData = this.CreateCurrentSearchData(searchText);
            this._currentSearchData.Cleared = cleared;
            this._currentSearchData.SearchStarting += this.OnSearchStarting;
            this._currentSearchData.SearchFinished += this.OnSearchFinished;

            ICommand command = this.Command;
            RoutedCommand routedCommand = command as RoutedCommand;
            if (routedCommand != null)
            {
                if (routedCommand.CanExecute(this._currentSearchData, inputElement))
                {
                    routedCommand.Execute(this._currentSearchData, inputElement);
                }
            }
            else if (command != null)
            {
                if (command.CanExecute(this._currentSearchData))
                {
                    command.Execute(this._currentSearchData);
                }
            }
        }

        /// <summary>
        /// Trims the specified text is necessary and returns the result.
        /// </summary>
        /// <param name="originalText">
        /// The text to trim.
        /// </param>
        /// <returns>
        /// The trimmed text.
        /// </returns>
        private string TrimSearchString(string originalText)
        {
            if (!this.SearchTrimsWhitespaces)
            {
                return originalText;
            }

            return originalText.Trim();
        }

        /// <summary>
        /// Updates the <see cref="CanExecute"/> property based on the attached command.
        /// </summary>
        private void UpdateCanExecute()
        {
            if (this.Command == null)
            {
                this.CanExecute = true;
                return;
            }

            string searchText = null;
            if (this._searchTextBox != null)
            {
                searchText = this._searchTextBox.Text;
            }

            object commandParameter = this.CreateCurrentSearchData(searchText);
            IInputElement inputElement = this.CommandTarget;
            RoutedCommand routedCommand = this.Command as RoutedCommand;
            if (routedCommand != null)
            {
                if (inputElement == null)
                {
                    inputElement = this as IInputElement;
                }

                this.CanExecute = routedCommand.CanExecute(commandParameter, inputElement);
            }
            else
            {
                this.CanExecute = this.Command.CanExecute(commandParameter);
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsSearchControl {Class}
} // RSG.Editor.Controls {Namespace}
