﻿
<!--
    <copyright file="RsContextMenu.generic.xaml" company="Rockstar">
    Copyright © Rockstar Games 2013. All rights reserved
    </copyright>
-->

<ResourceDictionary xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
                    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
                    xmlns:adp="clr-namespace:RSG.Editor.Controls.AttachedDependencyProperties"
                    xmlns:chromes="clr-namespace:RSG.Editor.Controls.Chromes"
                    xmlns:ctrls="clr-namespace:RSG.Editor.Controls"
                    xmlns:ext="clr-namespace:RSG.Editor.Controls.Extensions"
                    xmlns:local="clr-namespace:RSG.Editor.Controls.Commands"
                    xmlns:po="http://schemas.microsoft.com/winfx/2006/xaml/presentation/options"
                    xmlns:rsg="http://schemas.rockstargames.com/2013/xaml/editor">

    <ResourceDictionary.MergedDictionaries>
        <ResourceDictionary Source="../CommonCommandTemplates.generic.xaml" />
        <ResourceDictionary Source="/RSG.Editor.Controls;component/FocusVisualStyle.xaml" />
    </ResourceDictionary.MergedDictionaries>

    <!--
        Default style for the type :-
        x:Type={RSG.Editor.Controls.RsContextMenu}
    -->
    <Style TargetType="{x:Type local:RsContextMenu}">
        <Style.Resources>
            <Style x:Key="{x:Static MenuItem.SeparatorStyleKey}"
                   TargetType="{x:Type Separator}">
                <Setter Property="Background"
                        Value="{ext:ThemeResource PopupBorderKey}" />
                <Setter Property="RenderOptions.ClearTypeHint"
                        Value="Enabled" />
                <Setter Property="Focusable"
                        Value="False" />
                <Setter Property="Template">
                    <Setter.Value>
                        <ControlTemplate TargetType="{x:Type Separator}">
                            <Grid Background="Transparent"
                                  SnapsToDevicePixels="True">
                                <Grid.ColumnDefinitions>
                                    <ColumnDefinition Width="{Binding RelativeSource={RelativeSource AncestorType=Grid,
                                                                                                     AncestorLevel=2},
                                                                      Path=Children[1].ActualWidth,
                                                                      UpdateSourceTrigger=PropertyChanged,
                                                                      FallbackValue=0.0,
                                                                      TargetNullValue=0.0}"
                                                      MinWidth="26" />
                                    <ColumnDefinition Width="*" />
                                </Grid.ColumnDefinitions>
                                <Rectangle Grid.Column="1"
                                           Height="1"
                                           Margin="4,1,1,1"
                                           Fill="{TemplateBinding Background}" />
                            </Grid>
                        </ControlTemplate>
                    </Setter.Value>
                </Setter>
            </Style>
        </Style.Resources>
        <Setter Property="FontWeight"
                Value="Normal" />
        <Setter Property="Control.Template">
            <Setter.Value>
                <ControlTemplate TargetType="{x:Type local:RsContextMenu}">
                    <chromes:SystemDropShadowChrome x:Name="Shdw"
                                                    Margin="0,0,5,5"
                                                    Colour="{ext:ThemeResource PopupShadowKey}"
                                                    SnapsToDevicePixels="True">
                        <Grid Name="PART_PopupRoot"
                              Background="{ext:ThemeResource PopupIconColumnBackgroundKey}">
                            <ScrollViewer Grid.IsSharedSizeScope="True"
                                          Style="{DynamicResource {ComponentResourceKey TypeInTargetAssembly={x:Type FrameworkElement},
                                                                                        ResourceId=MenuScrollViewer}}">
                                <Grid Name="PopupGrid"
                                      Background="{ext:ThemeResource PopupBackgroundKey}"
                                      RenderOptions.ClearTypeHint="Enabled"
                                      adp:ThemeProperties.ImageBackgroundColour="{ext:ThemeResource PopupIconColumnBackgroundKey}">
                                    <Grid.ColumnDefinitions>
                                        <ColumnDefinition Width="Auto" />
                                        <ColumnDefinition Width="Auto"
                                                          MinWidth="25"
                                                          SharedSizeGroup="MenuItemIconColumnGroup" />
                                        <ColumnDefinition Width="*" />
                                    </Grid.ColumnDefinitions>
                                    <Border Grid.Column="0"
                                            Width="3"
                                            HorizontalAlignment="Stretch"
                                            Background="{ext:ThemeResource PopupIconColumnBackgroundKey}" />
                                    <Border Grid.Column="1"
                                            HorizontalAlignment="Stretch"
                                            Background="{ext:ThemeResource PopupIconColumnBackgroundKey}" />
                                    <ItemsPresenter Grid.ColumnSpan="3"
                                                    Margin="3"
                                                    KeyboardNavigation.DirectionalNavigation="Cycle"
                                                    KeyboardNavigation.TabNavigation="Cycle"
                                                    SnapsToDevicePixels="{TemplateBinding UIElement.SnapsToDevicePixels}" />
                                </Grid>
                            </ScrollViewer>
                            <Border BorderBrush="{ext:ThemeResource PopupBorderKey}"
                                    BorderThickness="1" />
                            <Border Height="2"
                                    Margin="{ext:MenuPopupPositioner ElementName=Bd}"
                                    HorizontalAlignment="Left"
                                    VerticalAlignment="Top"
                                    Background="{Binding ElementName=PopupGrid,
                                                         Path=Background,
                                                         Converter={StaticResource BrushToSolidColourConverter}}" />
                        </Grid>
                    </chromes:SystemDropShadowChrome>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>

    <!--
        Style for the embedded style resource key -
        resource key={RSG.Editor.Controls.Commands.RsContextMenu+ToolBarStyleKey}
    -->
    <Style x:Key="{x:Static local:RsContextMenu.ToolBarStyleKey}"
           TargetType="{x:Type local:RsMenuItem}">
        <Setter Property="UIElement.IsEnabled"
                Value="{Binding IsEnabled,
                                UpdateSourceTrigger=PropertyChanged}" />
        <Setter Property="HeaderedItemsControl.Header"
                Value="{Binding Text,
                                UpdateSourceTrigger=PropertyChanged}" />
        <Setter Property="IsCheckable"
                Value="True" />
        <Setter Property="IsChecked"
                Value="{Binding IsVisible,
                                Mode=TwoWay}" />
        <Setter Property="local:RsMenuItem.IconBitmap"
                Value="{x:Null}" />
        <Setter Property="Control.Template">
            <Setter.Value>
                <StaticResource ResourceKey="{ComponentResourceKey TypeInTargetAssembly={x:Type local:RsMenuItem}, ResourceId=SubmenuItemTemplateKey}" />
            </Setter.Value>
        </Setter>
        <Setter Property="Control.Padding"
                Value="1,0,0,1" />
        <Setter Property="BorderThickness"
                Value="1,1,1,1" />
        <Style.Triggers>
            <Trigger Property="UIElement.IsEnabled" Value="False">
                <Setter Property="FrameworkElement.ToolTip">
                    <Setter.Value>
                        <ToolTip Content="{Binding ToolTip,
                                                   Mode=OneTime}"
                                 ContentStringFormat="{}{0} (Disabled)" />
                    </Setter.Value>
                </Setter>
            </Trigger>
        </Style.Triggers>
    </Style>

    <!--
        Style for the embedded style resource key -
        resource key={RSG.Editor.Controls.Commands.RsContextMenu+MenuStyleKey}
    -->
    <Style x:Key="{x:Static local:RsContextMenu.MenuStyleKey}"
           TargetType="{x:Type local:RsMenuItem}">
        <Setter Property="UIElement.IsEnabled"
                Value="{Binding IsEnabled,
                                UpdateSourceTrigger=PropertyChanged}" />
        <Setter Property="HeaderedItemsControl.Header"
                Value="{Binding Text,
                                UpdateSourceTrigger=PropertyChanged}" />
        <Setter Property="FrameworkElement.ToolTip"
                Value="{x:Null}" />
        <Setter Property="ItemsControl.ItemsSource"
                Value="{Binding Children}" />
        <Setter Property="MenuItem.IsCheckable"
                Value="False" />
        <Setter Property="Control.Template">
            <Setter.Value>
                <StaticResource ResourceKey="{ComponentResourceKey TypeInTargetAssembly={x:Type local:RsMenuItem}, ResourceId=SubmenuHeaderTemplateKey}" />
            </Setter.Value>
        </Setter>
        <Setter Property="BorderThickness"
                Value="0,0,0,0" />
    </Style>

    <!--
        Style for the embedded style resource key -
        resource key={RSG.Editor.Controls.Commands.RsContextMenu+SeparatorStyleKey}
    -->
    <Style x:Key="{x:Static local:RsContextMenu.SeparatorStyleKey}"
           TargetType="{x:Type local:RsMenuItem}">
        <Setter Property="Focusable"
                Value="False" />
        <Setter Property="Background"
                Value="{ext:ThemeResource PopupBorderKey}" />
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="{x:Type local:RsMenuItem}">
                    <Grid Background="Transparent"
                          SnapsToDevicePixels="True">
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="{Binding RelativeSource={RelativeSource AncestorType=Grid,
                                                                                             AncestorLevel=2},
                                                              Path=Children[1].ActualWidth,
                                                              UpdateSourceTrigger=PropertyChanged,
                                                              FallbackValue=0.0,
                                                              TargetNullValue=0.0}"
                                              MinWidth="26" />
                            <ColumnDefinition Width="*" />
                        </Grid.ColumnDefinitions>
                        <Rectangle Grid.Column="1"
                                   Height="1"
                                   Margin="4,1,1,1"
                                   Fill="{TemplateBinding Background}" />
                    </Grid>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>

    <!--
        Style for the embedded style resource key -
        resource key={RSG.Editor.Controls.Commands.RsContextMenu+ButtonStyleKey}
    -->
    <Style x:Key="{x:Static local:RsContextMenu.ButtonStyleKey}"
           TargetType="{x:Type local:RsMenuItem}">
        <Setter Property="UIElement.IsEnabled"
                Value="{Binding IsEnabled,
                                UpdateSourceTrigger=PropertyChanged}" />
        <Setter Property="HeaderedItemsControl.Header"
                Value="{Binding Text,
                                UpdateSourceTrigger=PropertyChanged}" />
        <Setter Property="ToolTipService.ShowOnDisabled"
                Value="True" />
        <Setter Property="MenuItem.IsCheckable"
                Value="False" />
        <Setter Property="local:RsMenuItem.IconBitmap"
                Value="{Binding Icon}" />
        <Setter Property="MenuItem.Command"
                Value="{Binding Command,
                                UpdateSourceTrigger=PropertyChanged}" />
        <Setter Property="CommandTarget"
                Value="{Binding Path=PlacementTarget,
                                RelativeSource={RelativeSource AncestorType=ContextMenu},
                                FallbackValue={x:Null},
                                TargetNullValue={x:Null}}" />
        <Setter Property="CommandParameter"
                Value="{Binding Path=PlacementTarget.(adp:ContextMenuProperties.ContextCommandParameter),
                                RelativeSource={RelativeSource AncestorType=ContextMenu},
                                FallbackValue={x:Null},
                                TargetNullValue={x:Null}}" />
        <Setter Property="MenuItem.InputGestureText"
                Value="{Binding KeyGestureText}" />
        <Setter Property="Control.Template">
            <Setter.Value>
                <StaticResource ResourceKey="{ComponentResourceKey TypeInTargetAssembly={x:Type local:RsMenuItem}, ResourceId=SubmenuItemTemplateKey}" />
            </Setter.Value>
        </Setter>
        <Setter Property="Control.Padding"
                Value="1,0,0,1" />
        <Setter Property="BorderThickness"
                Value="1,1,1,1" />
        <Style.Triggers>
            <Trigger Property="UIElement.IsEnabled" Value="False">
                <Setter Property="FrameworkElement.ToolTip">
                    <Setter.Value>
                        <ToolTip Content="{Binding ToolTip,
                                                   Mode=OneTime}"
                                 ContentStringFormat="{}{0} (Disabled)" />
                    </Setter.Value>
                </Setter>
            </Trigger>
            <DataTrigger Binding="{Binding DisplayStyle,
                                           UpdateSourceTrigger=PropertyChanged}"
                         Value="{x:Static rsg:CommandItemDisplayStyle.TextOnly}">
                <Setter Property="local:RsMenuItem.IconBitmap"
                        Value="{x:Null}" />
            </DataTrigger>
        </Style.Triggers>
    </Style>

    <!--
        Style for the embedded style resource key -
        resource key={RSG.Editor.Controls.Commands.RsContextMenu+ToggleStyleKey}
    -->
    <Style x:Key="{x:Static local:RsContextMenu.ToggleStyleKey}"
           TargetType="{x:Type local:RsMenuItem}">
        <Setter Property="UIElement.IsEnabled"
                Value="{Binding IsEnabled,
                                UpdateSourceTrigger=PropertyChanged}" />
        <Setter Property="HeaderedItemsControl.Header"
                Value="{Binding Text,
                                UpdateSourceTrigger=PropertyChanged}" />
        <Setter Property="ToolTipService.ShowOnDisabled"
                Value="True" />
        <Setter Property="IsCheckable"
                Value="True" />
        <Setter Property="IsChecked"
                Value="{Binding IsToggled,
                                Mode=TwoWay}" />
        <Setter Property="local:RsMenuItem.IconBitmap"
                Value="{x:Null}" />
        <Setter Property="MenuItem.Command"
                Value="{Binding Command,
                                UpdateSourceTrigger=PropertyChanged}" />
        <Setter Property="MenuItem.CommandParameter"
                Value="{Binding RelativeSource={RelativeSource Self},
                                Path=IsChecked}" />
        <Setter Property="MenuItem.InputGestureText"
                Value="{Binding KeyGestureText}" />
        <Setter Property="Control.Template">
            <Setter.Value>
                <StaticResource ResourceKey="{ComponentResourceKey TypeInTargetAssembly={x:Type local:RsMenuItem}, ResourceId=SubmenuItemTemplateKey}" />
            </Setter.Value>
        </Setter>
        <Setter Property="Control.Padding"
                Value="1,0,0,1" />
        <Setter Property="BorderThickness"
                Value="1,1,1,1" />
        <Style.Triggers>
            <Trigger Property="UIElement.IsEnabled" Value="False">
                <Setter Property="FrameworkElement.ToolTip">
                    <Setter.Value>
                        <ToolTip Content="{Binding ToolTip,
                                                   Mode=OneTime}"
                                 ContentStringFormat="{}{0} (Disabled)" />
                    </Setter.Value>
                </Setter>
            </Trigger>
            <DataTrigger Binding="{Binding DisplayStyle,
                                           UpdateSourceTrigger=PropertyChanged}"
                         Value="{x:Static rsg:CommandItemDisplayStyle.ImageAndText}">
                <Setter Property="local:RsMenuItem.IconBitmap"
                        Value="{Binding Icon}" />
            </DataTrigger>
            <DataTrigger Binding="{Binding CommandParameter,
                                           UpdateSourceTrigger=PropertyChanged,
                                           Converter={StaticResource NotNullConverter}}"
                         Value="True">
                <Setter Property="MenuItem.CommandParameter"
                        Value="{Binding CommandParameter}" />
            </DataTrigger>
        </Style.Triggers>
    </Style>

    <!--
        Style for the embedded style resource key -
        resource key={RSG.Editor.Controls.Commands.RsContextMenu+ComboBoxStyleKey}
    -->
    <Style x:Key="{x:Static local:RsContextMenu.ComboBoxStyleKey}"
           TargetType="{x:Type local:RsMenuItem}">
        <Setter Property="UIElement.IsEnabled"
                Value="{Binding IsEnabled,
                                UpdateSourceTrigger=PropertyChanged}" />
        <Setter Property="ToolTipService.ShowOnDisabled"
                Value="True" />
        <Setter Property="MenuItem.IsCheckable"
                Value="False" />
        <Setter Property="MenuItem.StaysOpenOnClick"
                Value="True" />
        <Setter Property="Control.Template">
            <Setter.Value>
                <ControlTemplate>
                    <Grid Name="EnclosingGrid"
                          Background="Transparent"
                          SnapsToDevicePixels="True"
                          adp:ThemeProperties.ImageBackgroundColour="{ext:ThemeResource PopupIconColumnBackgroundKey}">
                        <Grid MinHeight="22">
                            <Grid.ColumnDefinitions>
                                <ColumnDefinition Width="Auto"
                                                  MinWidth="26"
                                                  SharedSizeGroup="MenuItemIconColumnGroup" />
                                <ColumnDefinition Width="*" />
                            </Grid.ColumnDefinitions>
                            <Rectangle Grid.ColumnSpan="2"
                                       Fill="{TemplateBinding Control.Background}"
                                       Stroke="{TemplateBinding Control.BorderBrush}"
                                       StrokeThickness="0" />

                            <StackPanel Grid.Column="1"
                                        Margin="8,2,2,2"
                                        HorizontalAlignment="Left"
                                        VerticalAlignment="Center"
                                        Orientation="Horizontal"
                                        SnapsToDevicePixels="{TemplateBinding UIElement.SnapsToDevicePixels}">
                                <TextBlock VerticalAlignment="Center"
                                           Text="{Binding Text}" />
                                <ctrls:RsComboBox x:Name="PART_FocusTarget"
                                                  Width="{Binding Width}"
                                                  MinHeight="22"
                                                  Margin="7,0,0,0"
                                                  Command="{Binding Command}"
                                                  ItemsSource="{Binding ComboItems}"
                                                  SelectedItem="{Binding SelectedItem,
                                                                         Mode=TwoWay}">
                                    <ctrls:RsComboBox.ItemTemplate>
                                        <DataTemplate>
                                            <StackPanel Orientation="Horizontal">
                                                <ctrls:RsThemeImage Width="16"
                                                                    Height="16"
                                                                    Margin="2,0,0,0"
                                                                    IconBitmap="{Binding Icon,
                                                                                         UpdateSourceTrigger=PropertyChanged}"
                                                                    adp:ThemeProperties.ImageBackgroundColour="{Binding RelativeSource={RelativeSource AncestorType=Border},
                                                                                                                        Path=Background}" />
                                                <TextBlock Margin="5,0,0,0"
                                                           VerticalAlignment="Center"
                                                           Text="{Binding Text}"
                                                           TextTrimming="CharacterEllipsis" />
                                            </StackPanel>
                                        </DataTemplate>
                                    </ctrls:RsComboBox.ItemTemplate>
                                    <ctrls:RsComboBox.ItemContainerStyle>
                                        <Style TargetType="ctrls:RsComboBoxItem">
                                            <Setter Property="CommandParameter"
                                                    Value="{Binding CommandParameter,
                                                                    UpdateSourceTrigger=PropertyChanged}" />
                                        </Style>
                                    </ctrls:RsComboBox.ItemContainerStyle>
                                </ctrls:RsComboBox>
                            </StackPanel>
                        </Grid>
                    </Grid>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
        <Setter Property="Control.Padding"
                Value="1,0,0,1" />
        <Setter Property="BorderThickness"
                Value="1,1,1,1" />
        <Style.Triggers>
            <Trigger Property="UIElement.IsEnabled" Value="False">
                <Setter Property="FrameworkElement.ToolTip">
                    <Setter.Value>
                        <ToolTip Content="{Binding ToolTip,
                                                   Mode=OneTime}"
                                 ContentStringFormat="{}{0} (Disabled)" />
                    </Setter.Value>
                </Setter>
            </Trigger>
            <DataTrigger Binding="{Binding DisplayStyle,
                                           UpdateSourceTrigger=PropertyChanged}"
                         Value="{x:Static rsg:CommandItemDisplayStyle.TextOnly}">
                <Setter Property="local:RsMenuItem.IconBitmap"
                        Value="{x:Null}" />
            </DataTrigger>
        </Style.Triggers>
    </Style>


    <!--
        Style for the embedded style resource key -
        resource key={RSG.Editor.Controls.Commands.RsContextMenu+GridCheckStyleKey}
    -->
    <Style x:Key="{x:Static local:RsContextMenu.GridCheckStyleKey}"
           TargetType="{x:Type local:RsMenuItem}">
        <Setter Property="ToolTipService.ShowOnDisabled"
                Value="True" />
        <Setter Property="IsCheckable"
                Value="True" />
        <Setter Property="local:RsMenuItem.IconBitmap"
                Value="{x:Null}" />
        <Setter Property="Control.Template">
            <Setter.Value>
                <StaticResource ResourceKey="{ComponentResourceKey TypeInTargetAssembly={x:Type local:RsMenuItem}, ResourceId=SubmenuItemTemplateKey}" />
            </Setter.Value>
        </Setter>
        <Setter Property="Control.Padding"
                Value="1,0,0,1" />
        <Setter Property="BorderThickness"
                Value="1,1,1,1" />
    </Style>

    <!--
        Style for the embedded style resource key -
        resource key={RSG.Editor.Controls.Commands.RsContextMenu+GridComboStyleKey}
    -->
    <Style x:Key="{x:Static local:RsContextMenu.GridComboStyleKey}"
           TargetType="{x:Type local:RsMenuItem}">
        <Setter Property="ToolTipService.ShowOnDisabled"
                Value="True" />
        <Setter Property="local:RsMenuItem.IconBitmap"
                Value="{x:Null}" />
        <Setter Property="MenuItem.IsCheckable"
                Value="False" />
        <Setter Property="MenuItem.StaysOpenOnClick"
                Value="True" />
        <Setter Property="Control.Template">
            <Setter.Value>
                <ControlTemplate TargetType="{x:Type local:RsMenuItem}">
                    <Grid Name="EnclosingGrid"
                          Background="Transparent"
                          SnapsToDevicePixels="True"
                          adp:ThemeProperties.ImageBackgroundColour="{ext:ThemeResource PopupIconColumnBackgroundKey}">
                        <Grid MinHeight="22">
                            <Grid.ColumnDefinitions>
                                <ColumnDefinition Width="Auto"
                                                  MinWidth="26"
                                                  SharedSizeGroup="MenuItemIconColumnGroup" />
                                <ColumnDefinition Width="*" />
                            </Grid.ColumnDefinitions>
                            <Rectangle Grid.ColumnSpan="2"
                                       Fill="{TemplateBinding Control.Background}"
                                       Stroke="{TemplateBinding Control.BorderBrush}"
                                       StrokeThickness="0" />

                            <StackPanel Grid.Column="1"
                                        Margin="8,2,2,2"
                                        HorizontalAlignment="Left"
                                        VerticalAlignment="Center"
                                        Orientation="Horizontal"
                                        SnapsToDevicePixels="{TemplateBinding UIElement.SnapsToDevicePixels}">
                                <TextBlock VerticalAlignment="Center"
                                           Text="{TemplateBinding Header}" />
                                <ComboBox x:Name="PART_FocusTarget"
                                          Width="160"
                                          MinHeight="22"
                                          Margin="7,0,0,0"/>
                            </StackPanel>
                        </Grid>
                    </Grid>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
        <Setter Property="Control.Padding"
                Value="1,0,0,1" />
        <Setter Property="BorderThickness"
                Value="1,1,1,1" />
    </Style>

    <!--
        Style for the embedded style resource key -
        resource key={RSG.Editor.Controls.Commands.RsContextMenu+GridSpinnerStyleKey}
    -->
    <Style x:Key="{x:Static local:RsContextMenu.GridSpinnerStyleKey}"
           TargetType="{x:Type local:RsMenuItem}">
        <Setter Property="ToolTipService.ShowOnDisabled"
                Value="True" />
        <Setter Property="local:RsMenuItem.IconBitmap"
                Value="{x:Null}" />
        <Setter Property="MenuItem.IsCheckable"
                Value="False" />
        <Setter Property="MenuItem.StaysOpenOnClick"
                Value="True" />
        <Setter Property="Control.Template">
            <Setter.Value>
                <ControlTemplate TargetType="{x:Type local:RsMenuItem}">
                    <Grid Name="EnclosingGrid"
                          Background="Transparent"
                          SnapsToDevicePixels="True"
                          adp:ThemeProperties.ImageBackgroundColour="{ext:ThemeResource PopupIconColumnBackgroundKey}">
                        <Grid MinHeight="22">
                            <Grid.ColumnDefinitions>
                                <ColumnDefinition Width="Auto"
                                                  MinWidth="26"
                                                  SharedSizeGroup="MenuItemIconColumnGroup" />
                                <ColumnDefinition Width="*" />
                            </Grid.ColumnDefinitions>
                            <Rectangle Grid.ColumnSpan="2"
                                       Fill="{TemplateBinding Control.Background}"
                                       Stroke="{TemplateBinding Control.BorderBrush}"
                                       StrokeThickness="0" />

                            <StackPanel Grid.Column="1"
                                        Margin="8,2,2,2"
                                        HorizontalAlignment="Left"
                                        VerticalAlignment="Center"
                                        Orientation="Horizontal"
                                        SnapsToDevicePixels="{TemplateBinding UIElement.SnapsToDevicePixels}">
                                <TextBlock VerticalAlignment="Center"
                                           Text="{TemplateBinding Header}" />
                                <ctrls:RsIntegerSpinner x:Name="PART_FocusTarget"
                                                        Width="160"
                                                        MinHeight="22"
                                                        Margin="7,0,0,0"/>
                            </StackPanel>
                        </Grid>
                    </Grid>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
        <Setter Property="Control.Padding"
                Value="1,0,0,1" />
        <Setter Property="BorderThickness"
                Value="1,1,1,1" />
    </Style>

    <!--
        Style for the embedded style resource key -
        resource key={RSG.Editor.Controls.Commands.RsContextMenu+DynamicMenuStyleKey}
    -->
    <Style x:Key="{x:Static local:RsContextMenu.DynamicMenuStyleKey}"
           TargetType="{x:Type local:RsMenuItem}">
        <Setter Property="HeaderedItemsControl.Header"
                Value="{Binding Text,
                                UpdateSourceTrigger=PropertyChanged}" />
        <Setter Property="FrameworkElement.ToolTip"
                Value="{x:Null}" />
        <Setter Property="ItemsControl.ItemsSource"
                Value="{Binding DynamicChildren}" />
        <Setter Property="MenuItem.IsCheckable"
                Value="False" />
        <Setter Property="Control.Template">
            <Setter.Value>
                <StaticResource ResourceKey="{ComponentResourceKey TypeInTargetAssembly={x:Type local:RsMenuItem}, ResourceId=SubmenuHeaderTemplateKey}" />
            </Setter.Value>
        </Setter>
        <Setter Property="BorderThickness"
                Value="0,0,0,0" />
        <Style.Triggers>
            <DataTrigger Binding="{Binding Path=DynamicChildren.Count}" Value="0">
                <Setter Property="Control.Template">
                    <Setter.Value>
                        <StaticResource ResourceKey="{ComponentResourceKey TypeInTargetAssembly={x:Type local:RsMenuItem}, ResourceId=SubmenuItemTemplateKey}" />
                    </Setter.Value>
                </Setter>
                <Setter Property="ItemsControl.ItemsSource"
                        Value="{x:Null}" />
                <Setter Property="MenuItem.Command"
                        Value="{Binding Command,
                                        UpdateSourceTrigger=PropertyChanged}" />
                <Setter Property="CommandTarget"
                        Value="{Binding Path=PlacementTarget,
                                        RelativeSource={RelativeSource AncestorType=ContextMenu},
                                        FallbackValue={x:Null},
                                        TargetNullValue={x:Null}}" />
                <Setter Property="CommandParameter"
                        Value="{Binding Path=PlacementTarget.(adp:ContextMenuProperties.ContextCommandParameter),
                                        RelativeSource={RelativeSource AncestorType=ContextMenu},
                                        FallbackValue={x:Null},
                                        TargetNullValue={x:Null}}" />
            </DataTrigger>
            <DataTrigger Binding="{Binding Path=DynamicChildren.Count}" Value="1">
                <Setter Property="Control.Template">
                    <Setter.Value>
                        <StaticResource ResourceKey="{ComponentResourceKey TypeInTargetAssembly={x:Type local:RsMenuItem}, ResourceId=SubmenuItemTemplateKey}" />
                    </Setter.Value>
                </Setter>
                <Setter Property="ItemsControl.ItemsSource"
                        Value="{x:Null}" />
                <Setter Property="MenuItem.Command"
                        Value="{Binding Command,
                                        UpdateSourceTrigger=PropertyChanged}" />
                <Setter Property="CommandTarget"
                        Value="{Binding Path=PlacementTarget,
                                        RelativeSource={RelativeSource AncestorType=ContextMenu},
                                        FallbackValue={x:Null},
                                        TargetNullValue={x:Null}}" />
                <Setter Property="CommandParameter"
                        Value="{Binding DynamicChildren[0].CommandParameter,
                                        FallbackValue={x:Null},
                                        TargetNullValue={x:Null}}" />
            </DataTrigger>
            <Trigger Property="UIElement.IsEnabled" Value="False">
                <Setter Property="FrameworkElement.ToolTip">
                    <Setter.Value>
                        <ToolTip Content="{Binding ToolTip,
                                                   Mode=OneTime}"
                                 ContentStringFormat="{}{0} (Disabled)" />
                    </Setter.Value>
                </Setter>
            </Trigger>
        </Style.Triggers>
    </Style>
</ResourceDictionary>