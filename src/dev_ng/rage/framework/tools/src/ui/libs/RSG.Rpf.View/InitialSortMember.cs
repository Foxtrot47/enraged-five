﻿//---------------------------------------------------------------------------------------------
// <copyright file="InitialSortMember.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Rpf.View
{
    /// <summary>
    /// Defines the different members of the RPF Viewers data grid that can be initially
    /// sorted before the control is loaded.
    /// </summary>
    public enum InitialSortMember
    {
        /// <summary>
        /// Specifies that the pack entries should be sorted by their name.
        /// </summary>
        Name,

        /// <summary>
        /// Specifies that the pack entries should be sorted by their uncompressed size.
        /// </summary>
        Size,

        /// <summary>
        /// Specifies that the pack entries should be sorted by their compressed size.
        /// </summary>
        Packed,

        /// <summary>
        /// Specifies that the pack entries should be sorted by their index.
        /// </summary>
        Index,

        /// <summary>
        /// Specifies that the pack entries should be sorted by their offset.
        /// </summary>
        Offset,

        /// <summary>
        /// Specifies that the pack entries should be sorted by their virtual size.
        /// </summary>
        VirtualSize,

        /// <summary>
        /// Specifies that the pack entries should be sorted by their physical size.
        /// </summary>
        PhysicalSize
    } // RSG.Rpf.View.InitialSortMember {Enum}
} // RSG.Rpf.View {Namespace}
