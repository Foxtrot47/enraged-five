﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Editor.Controls.Wizard
{

    /// <summary>
    /// 
    /// </summary>
    public enum NavigateDirection
    {
        Forwards,
        Backwards
    }

} // RSG.Editor.Controls.Wizard namespace
