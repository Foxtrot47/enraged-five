﻿//---------------------------------------------------------------------------------------------
// <copyright file="CreateContentArgs.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel
{
    using RSG.Base.Logging;
    using RSG.Editor.Controls.Dock.ViewModel;
    using RSG.Editor.Model;

    /// <summary>
    /// A argument class used to pass objects into the create document content controller
    /// method and used during the creation.
    /// </summary>
    public class CreateContentArgs
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Log"/> property.
        /// </summary>
        private ILog _log;

        /// <summary>
        /// The private field used for the <see cref="UndoEngine"/> property.
        /// </summary>
        private UndoEngine _undoEngine;

        /// <summary>
        /// The private field used for the <see cref="ViewSite"/> property.
        /// </summary>
        private ViewSite _viewSite;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CreateContentArgs"/> class.
        /// </summary>
        /// <param name="log">
        /// A log object that can be used to output warnings and errors.
        /// </param>
        /// <param name="viewSite">
        /// The view site instance that the document is being opened against.
        /// </param>
        /// <param name="undoEngine">
        /// A undo engine that can be used in creating the document content.
        /// </param>
        public CreateContentArgs(ILog log, ViewSite viewSite, UndoEngine undoEngine)
        {
            this._log = log;
            this._viewSite = viewSite;
            this._undoEngine = undoEngine;
        }
        #endregion Constructors
        #region Properties
        /// <summary>
        /// Gets a log object that can be used to output warnings and errors caught during the
        /// creation of the document content.
        /// </summary>
        public ILog Log
        {
            get { return this._log; }
        }

        /// <summary>
        /// Gets a undo engine that can be used in creating the document content.
        /// </summary>
        public UndoEngine UndoEngine
        {
            get { return this._undoEngine; }
        }

        /// <summary>
        /// Gets the view site instance that the document is being opened against.
        /// </summary>
        public ViewSite ViewSite
        {
            get { return this._viewSite; }
        }
        #endregion Properties
    } // RSG.Project.ViewModel.FileNode {Class}
} // RSG.Project.ViewModel {Namespace}
