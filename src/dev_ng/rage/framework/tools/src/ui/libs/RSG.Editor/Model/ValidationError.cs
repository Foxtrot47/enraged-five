﻿//---------------------------------------------------------------------------------------------
// <copyright file="ValidationError.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Model
{
    /// <summary>
    /// Represents a error that has occurred during validation.
    /// </summary>
    public class ValidationError : IValidationError
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Location"/> property.
        /// </summary>
        private FileLocation _location;

        /// <summary>
        /// The private field used for the <see cref="Message"/> property.
        /// </summary>
        private string _message;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ValidationError"/> class.
        /// </summary>
        /// <param name="message">
        /// The message that describes the error.
        /// </param>
        public ValidationError(string message)
        {
            this._message = message;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ValidationError"/> class.
        /// </summary>
        /// <param name="message">
        /// The message that describes the error.
        /// </param>
        /// <param name="location">
        /// The file location for the error if applicable.
        /// </param>
        public ValidationError(string message, FileLocation location)
        {
            this._location = location;
            this._message = message;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether this object can provide information about the
        /// location of the error inside a file.
        /// </summary>
        public bool HasLocationInformation
        {
            get { return this._location != null; }
        }

        /// <summary>
        /// Gets the file location that this error is associated with.
        /// </summary>
        public FileLocation Location
        {
            get { return this._location ?? FileLocation.NotApplicable; }
        }

        /// <summary>
        /// Gets the message that describes the error.
        /// </summary>
        public string Message
        {
            get { return this._message; }
        }
        #endregion Properties
    } // RSG.Editor.Model.ValidationError {Class}
} // RSG.Editor.Model {Namespace}
