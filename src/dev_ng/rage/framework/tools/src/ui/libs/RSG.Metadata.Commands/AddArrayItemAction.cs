﻿//---------------------------------------------------------------------------------------------
// <copyright file="AddArrayItemAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Commands
{
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Tunables;
    using RSG.Metadata.ViewModel.Tunables;

    /// <summary>
    /// Contains the logic for the <see cref="MetadataCommands.AddArrayItem"/> routed command.
    /// This class cannot be inherited.
    /// </summary>
    public sealed class AddArrayItemAction
        : MetadataMultiCommandActionBase<MetadataCommandArrayArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="AddArrayItemAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public AddArrayItemAction(MetadataCommandArgsResolver resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="arrayArgs">
        /// The command parameters that contains the array arguments for this command.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        protected override bool CanExecute(
            MetadataCommandArgs args, MetadataCommandArrayArgs arrayArgs)
        {
            return true;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="arrayArgs">
        /// The command parameters that contains the array arguments for this command.
        /// </param>
        protected override void Execute(
            MetadataCommandArgs args, MetadataCommandArrayArgs arrayArgs)
        {
            using (new UndoRedoBatch(args.UndoEngine))
            {
                ITunable tunable = arrayArgs.DynamicParent.AddNewItem();
                PointerTunable pointerTunable = tunable as PointerTunable;
                if (pointerTunable != null)
                {
                    pointerTunable.ChangePointerType(arrayArgs.Structure);
                }
            }
        }
        #endregion Methods
    } // RSG.Metadata.Commands.AddArrayItemAction {Class}
} // RSG.Metadata.Commands {Namespace}
