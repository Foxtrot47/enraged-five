﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProjectFolderNode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using RSG.Base.Extensions;
    using RSG.Editor;
    using RSG.Project.Model;

    /// <summary>
    /// Represents a folder node inside the main project collection.
    /// </summary>
    public class ProjectFolderNode : FolderNode
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ProjectFolderNode"/> class.
        /// </summary>
        /// <param name="model">
        /// The model that this view model will be wrapping.
        /// </param>
        public ProjectFolderNode(IHierarchyNode parent, ProjectItem model)
            : base(parent, model)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether a new or existing items can be added to this
        /// hierarchy node.
        /// </summary>
        public override bool CanHaveItemsAdded
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether this hierarchy node can be removed from the parent
        /// items scope it belongs to.
        /// </summary>
        public override bool CanBeRemoved
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether a new or existing project folder can be added to
        /// this hierarchy node.
        /// </summary>
        public override bool CanHaveProjectFoldersAdded
        {
            get { return true; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Adds a new folder underneath this node and returns the instance to the new folder.
        /// </summary>
        /// <returns>
        /// The new folder that was created.
        /// </returns>
        public override IHierarchyNode AddNewFolder()
        {
            string include = "{0}\\{1}".FormatInvariant(this.Model.Include, "NewFolder");
            include = this.Model.Scope.GetUniqueInclude(include);
            ProjectItem item = this.Model.Scope.AddNewProjectItem("Folder", include);
            HierarchyNode node = new ProjectFolderNode(this, item);
            node.IsExpandable = false;
            this.AddChild(node);
            this.IsExpandable = true;
            return node;
        }

        /// <summary>
        /// Called just before this object in the user interface is expanded inside a
        /// hierarchical control.
        /// </summary>
        public async override void BeforeExpand()
        {
            base.BeforeExpand();
            List<FileNode> fileNodes = new List<FileNode>();
            foreach (IHierarchyNode child in this.GetChildren(false))
            {
                FileNode childFile = child as FileNode;
                if (childFile == null)
                {
                    continue;
                }

                fileNodes.Add(childFile);
            }

            IList<IPerforceFileMetaData> metadataList = await this.GetChildMetadataAsync();
            StringComparison comparisonType = StringComparison.OrdinalIgnoreCase;
            foreach (FileNode fileNode in fileNodes)
            {
                IPerforceFileMetaData metadata = null;
                if (metadataList != null)
                {
                    string filePath = fileNode.FullPath;
                    foreach (IPerforceFileMetaData fileMetadata in metadataList)
                    {
                        string localPath = fileMetadata.LocalPath;
                        if (String.Equals(localPath, filePath, comparisonType))
                        {
                            metadata = fileMetadata;
                            break;
                        }
                    }
                }

                fileNode.UpdatePerforceState(metadata);
            }
        }

        /// <summary>
        /// Gets the source control metadata for the child nodes.
        /// </summary>
        /// <returns>
        /// The source control metadata for the child nodes.
        /// </returns>
        private async Task<IList<IPerforceFileMetaData>> GetChildMetadataAsync()
        {
            IPerforceService ps = this.ParentCollection.PerforceService;
            if (ps == null)
            {
                return null;
            }

            List<string> filenames = new List<string>();
            List<FileNode> fileNodes = new List<FileNode>();
            foreach (IHierarchyNode child in this.GetChildren(false))
            {
                FileNode childFile = child as FileNode;
                if (childFile == null)
                {
                    continue;
                }

                filenames.Add(childFile.FullPath);
                fileNodes.Add(childFile);
            }

            if (filenames.Count == 0)
            {
                return new List<IPerforceFileMetaData>();
            }

            var action = new Func<IList<IPerforceFileMetaData>>(
                delegate
                {
                    using (ps.SuspendExceptionsScope())
                    {
                        return ps.GetFileMetaData(filenames);
                    }
                });

            return await Task.Factory.StartNew(action);
        }
        #endregion Methods
    } // RSG.Project.ViewModel.ProjectFolderNode {Class}
} // RSG.Project.ViewModel {Namespace}
