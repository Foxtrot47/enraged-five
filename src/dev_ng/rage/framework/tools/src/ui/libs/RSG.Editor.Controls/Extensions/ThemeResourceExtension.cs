﻿//---------------------------------------------------------------------------------------------
// <copyright file="ThemeResourceExtension.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Extensions
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Reflection;
    using System.Windows;
    using RSG.Editor.Controls.Themes;

    /// <summary>
    /// Implements a markup extension that supports resource references made from XAML to theme
    /// resources.
    /// </summary>
    public class ThemeResourceExtension : DynamicResourceExtension
    {
        #region Fields
        /// <summary>
        /// A private dictionary containing the resource keys for the command brushes indexed
        /// by their names.
        /// </summary>
        private static Dictionary<string, object> _resourceKeys = GetResourceKeys();
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ThemeResourceExtension" /> class.
        /// </summary>
        public ThemeResourceExtension()
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ThemeResourceExtension" /> class, with
        /// the provided initial key.
        /// </summary>
        /// <param name="resourceKey">
        /// The key of the resource that this markup extension references.
        /// </param>
        public ThemeResourceExtension(string resourceKey)
        {
            object actualResourceKey;
            bool exists = _resourceKeys.TryGetValue(resourceKey, out actualResourceKey);

            if (exists)
            {
                this.ResourceKey = actualResourceKey;
            }
            else if (!DesignerProperties.GetIsInDesignMode(new DependencyObject()))
            {
                Debug.Assert(
                    exists, "Specified key is unrecognised as a resource", "{0]", resourceKey);
            }
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Creates and returns the static lookup table that contains the properties inside the
        /// theme brush class indexed by their names.
        /// </summary>
        /// <returns>
        /// The static lookup table containing the properties of the theme brush class indexed
        /// by their names.
        /// </returns>
        private static Dictionary<string, object> GetResourceKeys()
        {
            Dictionary<string, object> resourceKeys = new Dictionary<string, object>();
            foreach (PropertyInfo property in typeof(ThemeBrushes).GetProperties())
            {
                object value = property.GetValue(null);
                Debug.Assert(value != null, "Invalid theme resource value.");
                if (value != null)
                {
                    resourceKeys.Add(property.Name, value);
                }
            }

            return resourceKeys;
        }
        #endregion Methods
    } // RSG.Editor.Controls.Extensions.ThemeResourceExtension {Class}
} // RSG.Editor.Controls.Extensions {Namespace}
