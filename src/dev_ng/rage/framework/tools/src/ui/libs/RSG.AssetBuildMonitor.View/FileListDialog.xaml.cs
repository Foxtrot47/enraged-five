﻿//---------------------------------------------------------------------------------------------
// <copyright file="FileListDialog.xaml.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.AssetBuildMonitor.View
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Windows;
    using RSG.Editor.Controls;

    /// <summary>
    /// Interaction logic for FileListDialog.xaml
    /// </summary>
    public partial class FileListDialog : RsWindow
    {
        #region Properties
        /// <summary>
        /// Dialog user-message.
        /// </summary>
        public String Message
        {
            get;
            private set;
        }

        /// <summary>
        /// Collection of filenames to display.
        /// </summary>
        public ObservableCollection<String> Filenames
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public FileListDialog(String title, String message, IEnumerable<String> filenames)
        {
            InitializeComponent();
            this.DataContext = this;
            this.Title = title;
            this.Message = message;
            this.Filenames = new ObservableCollection<String>(filenames);
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OkButtonClick(Object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
        #endregion // Private Methods
    }

} // RSG.AssetBuildMonitor.View namespace
