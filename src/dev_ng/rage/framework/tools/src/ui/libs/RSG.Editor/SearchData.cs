﻿//---------------------------------------------------------------------------------------------
// <copyright file="SearchData.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Threading;

    /// <summary>
    /// Represents the data that is sent as the command parameter for a search command.
    /// </summary>
    public class SearchData
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Cleared"/> property.
        /// </summary>
        private bool _cleared;

        /// <summary>
        /// The private field used for the <see cref="CancellationTokenSource"/> property.
        /// </summary>
        private CancellationTokenSource _cancellationTokenSource;

        /// <summary>
        /// The private field used for the <see cref="FilteringResults"/> property.
        /// </summary>
        private bool _filteringResults;

        /// <summary>
        /// The private field used for the <see cref="MatchCase"/> property.
        /// </summary>
        private bool _matchCase;

        /// <summary>
        /// The private field used for the <see cref="MatchWord"/> property.
        /// </summary>
        private bool _matchWord;

        /// <summary>
        /// The private field used for the <see cref="RegularExpressions"/> property.
        /// </summary>
        private bool _regularExpressions;

        /// <summary>
        /// The private field used for the <see cref="SearchText"/> property.
        /// </summary>
        private string _searchText;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SearchData"/> class.
        /// </summary>
        internal SearchData()
        {
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs when a general error has been detected during the current search.
        /// </summary>
        internal event EventHandler<SearchErrorEventArgs> GeneralErrorDetected;

        /// <summary>
        /// Occurs when a parsing error has been detected within the current search text.
        /// </summary>
        internal event EventHandler<SearchErrorEventArgs> ParseErrorDetected;

        /// <summary>
        /// Occurs when the current search has finished.
        /// </summary>
        internal event EventHandler<SearchFinishedEventArgs> SearchFinished;

        /// <summary>
        /// Occurs when a new search is just about to be started.
        /// </summary>
        internal event EventHandler SearchStarting;
        #endregion Events

        #region Properties
        /// <summary>
        /// Gets a value indicating whether the search has been cleared.
        /// </summary>
        public bool Cleared
        {
            get { return this._cleared; }
            internal set { this._cleared = value; }
        }

        /// <summary>
        /// Gets a value indicating whether the search described by this data class should
        /// filter the user interface so that only the valid search results are shown.
        /// </summary>
        public bool FilteringResults
        {
            get { return this._filteringResults; }
            internal set { this._filteringResults = value; }
        }

        /// <summary>
        /// Gets a value indicating whether the search described by this data class is case
        /// sensitive or not.
        /// </summary>
        public bool MatchCase
        {
            get { return this._matchCase; }
            internal set { this._matchCase = value; }
        }

        /// <summary>
        /// Gets a value indicating whether the search described by this data class should
        /// match the whole word or not.
        /// </summary>
        public bool MatchWord
        {
            get { return this._matchWord; }
            internal set { this._matchWord = value; }
        }

        /// <summary>
        /// Gets a value indicating whether the search described by this data class can be
        /// using regular expressions.
        /// </summary>
        public bool RegularExpressions
        {
            get { return this._regularExpressions; }
            internal set { this._regularExpressions = value; }
        }

        /// <summary>
        /// Gets the actual string that should be searched for.
        /// </summary>
        public string SearchText
        {
            get { return this._searchText; }
            internal set { this._searchText = value; }
        }

        /// <summary>
        /// Gets or sets the cancellation token source that is used to cancel the search.
        /// </summary>
        internal CancellationTokenSource CancellationTokenSource
        {
            get { return this._cancellationTokenSource; }
            set { this._cancellationTokenSource = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Registers that there has been a general error with the search and specifies a
        /// message to display to the user.
        /// </summary>
        /// <param name="message">
        /// The message that describes the error.
        /// </param>
        public void RegisterError(string message)
        {
            EventHandler<SearchErrorEventArgs> handler = this.GeneralErrorDetected;
            if (handler == null)
            {
                return;
            }

            SearchErrorEventArgs args = new SearchErrorEventArgs();
            args.Message = message;
            args.ParsingError = false;

            handler(this, args);
        }

        /// <summary>
        /// Registers that there has been a parsing error with the search text and specifies a
        /// message to display to the user.
        /// </summary>
        /// <param name="message">
        /// The message that describes the parsing error.
        /// </param>
        public void RegisterParseError(string message)
        {
            EventHandler<SearchErrorEventArgs> handler = this.ParseErrorDetected;
            if (handler == null)
            {
                return;
            }

            SearchErrorEventArgs args = new SearchErrorEventArgs();
            args.Message = message;
            args.ParsingError = true;

            handler(this, args);
        }

        /// <summary>
        /// Registers that the search has finished successfully.
        /// </summary>
        /// <param name="foundResults">
        /// A value indicating whether any results have been found during the search.
        /// </param>
        public void RegisterSearchFinished(bool foundResults)
        {
            EventHandler<SearchFinishedEventArgs> handler = this.SearchFinished;
            if (handler == null)
            {
                return;
            }

            SearchFinishedEventArgs args = new SearchFinishedEventArgs();
            args.FoundEntries = foundResults;

            handler(this, args);
        }

        /// <summary>
        /// Registers that the search is about to start.
        /// </summary>
        public void RegisterSearchStarting()
        {
            EventHandler handler = this.SearchStarting;
            if (handler == null)
            {
                return;
            }

            handler(this, EventArgs.Empty);
        }

        /// <summary>
        /// Throws a System.OperationCancelledException if the search has been cancelled.
        /// </summary>
        public void ThrowIfCancellationRequested()
        {
            if (!this.CancellationTokenSource.IsCancellationRequested)
            {
                return;
            }

            CancellationToken token = this.CancellationTokenSource.Token;
            this.RegisterSearchFinished(false);
            token.ThrowIfCancellationRequested();
        }
        #endregion Methods
    } // RSG.Editor.SearchData {Class}
} // RSG.Editor {Namespace}
