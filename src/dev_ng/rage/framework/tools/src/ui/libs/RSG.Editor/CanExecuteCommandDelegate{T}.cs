﻿//---------------------------------------------------------------------------------------------
// <copyright file="CanExecuteCommandDelegate{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    /// <summary>
    /// Represents the predicate method that determines whether a command can be executed.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the command parameter contained within the specified data.
    /// </typeparam>
    /// <param name="e">
    /// The object containing the data to use inside the predicate method to determine whether
    /// the command can be executed.
    /// </param>
    /// <returns>
    /// A value indicating whether the command associated with the specified data can be
    /// executed.
    /// </returns>
    public delegate bool CanExecuteCommandDelegate<T>(CanExecuteCommandData<T> e);
} // RSG.Editor {Namespace}
