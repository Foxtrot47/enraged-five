﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Interop.Microsoft.Windows;

namespace RSG.Editor.Controls
{
    /// <summary>
    /// Receives messages from the taskbar icon through
    /// window messages of an underlying helper window.
    /// </summary>
    public class WindowMessageSink : IDisposable
    {
        #region Fields
        /// <summary>
        /// The ID of the message that is being received if the
        /// taskbar is (re)started.
        /// </summary>
        private int _taskbarRestartMessageId;

        /// <summary>
        /// Used to track whether a mouse-up event is just
        /// the aftermath of a double-click and therefore needs
        /// to be suppressed.
        /// </summary>
        private bool _isDoubleClick;

        /// <summary>
        /// A delegate that processes messages of the hidden
        /// native window that receives window messages. Storing
        /// this reference makes sure we don't loose our reference
        /// to the message window.
        /// </summary>
        private WndProcDelegate _messageHandler;

        /// <summary>
        /// Window class ID.
        /// </summary>
        internal string WindowId { get; private set; }

        /// <summary>
        /// Handle for the message window.
        /// </summary> 
        internal IntPtr MessageWindowHandle { get; private set; }

        /// <summary>
        /// The version of the underlying icon. Defines how
        /// incoming messages are interpreted.
        /// </summary>
        public NotifyIconVersion Version { get; set; }
        #endregion

        #region Events
        /// <summary>
        /// The custom tooltip should be closed or hidden.
        /// </summary>
        public event Action<bool> ChangeToolTipStateRequest;

        /// <summary>
        /// Fired in case the user clicked or moved within
        /// the taskbar icon area.
        /// </summary>
        public event Action<MouseEvent> MouseEventReceived;

        /// <summary>
        /// Fired if a balloon ToolTip was either displayed
        /// or closed (indicated by the boolean flag).
        /// </summary>
        public event Action<bool> BalloonToolTipChanged;

        /// <summary>
        /// Fired if the taskbar was created or restarted. Requires the taskbar
        /// icon to be reset.
        /// </summary>
        public event Action TaskbarCreated;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Creates a new message sink that receives message from
        /// a given taskbar icon.
        /// </summary>
        /// <param name="version"></param>
        public WindowMessageSink(NotifyIconVersion version)
        {
            Version = version;
            CreateMessageWindow();
        }

        /// <summary>
        /// Don't allow creation of a <see cref="WindowMessageSink"/> object without
        /// specifying the version.
        /// </summary>
        private WindowMessageSink()
        {
        }

        /// <summary>
        /// Creates a dummy instance that provides an empty
        /// pointer rather than a real window handler.<br/>
        /// Used at design time.
        /// </summary>
        /// <returns></returns>
        internal static WindowMessageSink CreateEmpty()
        {
            return new WindowMessageSink
            {
                MessageWindowHandle = IntPtr.Zero,
                Version = NotifyIconVersion.Vista
            };
        }
        #endregion

        #region CreateMessageWindow
        /// <summary>
        /// Creates the helper message window that is used
        /// to receive messages from the taskbar icon.
        /// </summary>
        private void CreateMessageWindow()
        {
            //generate a unique ID for the window
            WindowId = "WPFTaskbarIcon_" + DateTime.Now.Ticks;

            //register window message handler
            _messageHandler = new WndProcDelegate(OnWindowMessageReceived);

            // Create a simple window class which is reference through
            //the messageHandler delegate
            WNDCLASS wc = default(WNDCLASS);
            wc.Style = 0u;
            wc.WndProc = _messageHandler;
            wc.ClassExtraBytes = 0;
            wc.WindowExtraBytes = 0;
            wc.Instance = IntPtr.Zero;
            wc.Icon = IntPtr.Zero;
            wc.Cursor = IntPtr.Zero;
            wc.Background = IntPtr.Zero;
            wc.MenuName = null;
            wc.ClassName = WindowId;

            // Register the window class
            IntPtr windowAtom = (IntPtr)User32.RegisterClass(ref wc);

            // Get the message used to indicate the taskbar has been restarted
            // This is used to re-add icons when the taskbar restarts
            _taskbarRestartMessageId = User32.RegisterWindowMessage("TaskbarCreated");

            // Create the message window
            MessageWindowHandle = User32.CreateWindowEx(0, windowAtom, "", 0, 0, 0, 1, 1, IntPtr.Zero, IntPtr.Zero,
                IntPtr.Zero, IntPtr.Zero);

            if (MessageWindowHandle == IntPtr.Zero)
            {
                throw new Win32Exception("Message window handle was not a valid pointer");
            }
        }
        #endregion

        #region Handle Window Messages
        /// <summary>
        /// Callback method that receives messages from the taskbar area.
        /// </summary>
        private IntPtr OnWindowMessageReceived(IntPtr hwnd, WindowMessage messageId, IntPtr wparam, IntPtr lparam)
        {
            if ((int)messageId == _taskbarRestartMessageId)
            {
                //recreate the icon if the taskbar was restarted (e.g. due to Win Explorer shutdown)
                TaskbarCreated();
            }

            //forward message
            ProcessWindowMessage(messageId, wparam, lparam);

            // Pass the message to the default window procedure
            return User32.DefaultWindowProc(hwnd, messageId, wparam, lparam);
        }

        /// <summary>
        /// Processes incoming system messages.
        /// </summary>
        /// <param name="msg">Callback ID.</param>
        /// <param name="wParam">If the version is <see cref="NotifyIconVersion.Vista"/>
        /// or higher, this parameter can be used to resolve mouse coordinates.
        /// Currently not in use.</param>
        /// <param name="lParam">Provides information about the event.</param>
        private void ProcessWindowMessage(WindowMessage msg, IntPtr wParam, IntPtr lParam)
        {
            if (msg != WindowMessage.USER)
            {
                return;
            }

            switch ((WindowMessage)lParam.ToInt32())
            {
                case WindowMessage.MOUSEMOVE:
                    MouseEventReceived(MouseEvent.MouseMove);
                    break;

                case WindowMessage.LBUTTONDOWN:
                    MouseEventReceived(MouseEvent.IconLeftMouseDown);
                    break;

                case WindowMessage.LBUTTONUP:
                    if (!_isDoubleClick)
                    {
                        MouseEventReceived(MouseEvent.IconLeftMouseUp);
                    }
                    _isDoubleClick = false;
                    break;

                case WindowMessage.LBUTTONDBLCLK:
                    _isDoubleClick = true;
                    MouseEventReceived(MouseEvent.IconDoubleClick);
                    break;

                case WindowMessage.RBUTTONDOWN:
                    MouseEventReceived(MouseEvent.IconRightMouseDown);
                    break;

                case WindowMessage.RBUTTONUP:
                    MouseEventReceived(MouseEvent.IconRightMouseUp);
                    break;

                case WindowMessage.RBUTTONDBLCLK:
                    //double click with right mouse button - do not trigger event
                    break;

                case WindowMessage.MBUTTONDOWN:
                    MouseEventReceived(MouseEvent.IconMiddleMouseDown);
                    break;

                case WindowMessage.MBUTTONUP:
                    MouseEventReceived(MouseEvent.IconMiddleMouseUp);
                    break;

                case WindowMessage.MBUTTONDBLCLK:
                    //double click with middle mouse button - do not trigger event
                    break;

                case (WindowMessage)0x402:
                    BalloonToolTipChanged(true);
                    break;

                case (WindowMessage)0x403:
                case (WindowMessage)0x404:
                    BalloonToolTipChanged(false);
                    break;

                case (WindowMessage)0x405:
                    MouseEventReceived(MouseEvent.BalloonToolTipClicked);
                    break;

                case (WindowMessage)0x406:
                    ChangeToolTipStateRequest(true);
                    break;

                case (WindowMessage)0x407:
                    ChangeToolTipStateRequest(false);
                    break;

                default:
                    Debug.WriteLine("Unhandled NotifyIcon message ID: " + msg);
                    break;
            }
        }
        #endregion

        #region Dispose
        /// <summary>
        /// Set to true as soon as <c>Dispose</c> has been invoked.
        /// </summary>
        public bool IsDisposed { get; private set; }

        /// <summary>
        /// Disposes the object.
        /// </summary>
        /// <remarks>This method is not virtual by design. Derived classes
        /// should override <see cref="Dispose(bool)"/>.
        /// </remarks>
        public void Dispose()
        {
            Dispose(true);

            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue 
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// This destructor will run only if the <see cref="Dispose()"/>
        /// method does not get called. This gives this base class the
        /// opportunity to finalize.
        /// <para>
        /// Important: Do not provide destructors in types derived from
        /// this class.
        /// </para>
        /// </summary>
        ~WindowMessageSink()
        {
            Dispose(false);
        }

        /// <summary>
        /// Removes the windows hook that receives window
        /// messages and closes the underlying helper window.
        /// </summary>
        private void Dispose(bool disposing)
        {
            //don't do anything if the component is already disposed
            if (IsDisposed)
            {
                return;
            }
            IsDisposed = true;

            //always destroy the unmanaged handle (even if called from the GC)
            User32.DestroyWindow(MessageWindowHandle);
            _messageHandler = null;
        }
        #endregion
    }
}
