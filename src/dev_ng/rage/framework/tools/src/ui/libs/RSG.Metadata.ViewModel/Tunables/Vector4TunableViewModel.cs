﻿//---------------------------------------------------------------------------------------------
// <copyright file="Vector4TunableViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 4014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Tunables
{
    using System.Collections.Generic;
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// The view model for a <see cref="Vector4Tunable"/> model object.
    /// </summary>
    public class Vector4TunableViewModel : TunableViewModelBase<Vector4Tunable>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Vector4TunableViewModel"/> class.
        /// </summary>
        /// <param name="tunable">
        /// The 4d vector tunable that this view model is representing.
        /// </param>
        /// <param name="parent">
        /// The parent tunable view model for this view model.
        /// </param>
        public Vector4TunableViewModel(Vector4Tunable tunable, ITunableViewModel parent)
            : base(tunable, parent)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the incremental step for the W parameter.
        /// </summary>
        public float StepW
        {
            get { return this.Model.Vector4Member.Step[3]; }
        }

        /// <summary>
        /// Gets the incremental step for the X parameter.
        /// </summary>
        public float StepX
        {
            get { return this.Model.Vector4Member.Step[0]; }
        }

        /// <summary>
        /// Gets the incremental step for the Y parameter.
        /// </summary>
        public float StepY
        {
            get { return this.Model.Vector4Member.Step[1]; }
        }

        /// <summary>
        /// Gets the incremental step for the Z parameter.
        /// </summary>
        public float StepZ
        {
            get { return this.Model.Vector4Member.Step[2]; }
        }

        /// <summary>
        /// Gets or sets the value of the w component for this vector.
        /// </summary>
        public float W
        {
            get { return this.Model.W; }
            set { this.Model.W = value; }
        }

        /// <summary>
        /// Gets or sets the value of the x component for this vector.
        /// </summary>
        public float X
        {
            get { return this.Model.X; }
            set { this.Model.X = value; }
        }

        /// <summary>
        /// Gets or sets the value of the y component for this vector.
        /// </summary>
        public float Y
        {
            get { return this.Model.Y; }
            set { this.Model.Y = value; }
        }

        /// <summary>
        /// Gets or sets the value of the z component for this vector.
        /// </summary>
        public float Z
        {
            get { return this.Model.Z; }
            set { this.Model.Z = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Determines whether the validation pipeline should be started due to the property
        /// with the specified name changing.
        /// </summary>
        /// <param name="propertyName">
        /// The property name to test.
        /// </param>
        /// <returns>
        /// True if the validation should be run due to the specified property changing;
        /// otherwise, false.
        /// </returns>
        protected override bool ShouldRunValidation(string propertyName)
        {
            HashSet<string> propertyNames = new HashSet<string>()
            {
                "X",
                "Y",
                "Z",
                "W",
            };

            if (propertyNames.Contains(propertyName))
            {
                return true;
            }

            return base.ShouldRunValidation(propertyName);
        }
        #endregion Methods
    } // RSG.Metadata.ViewModel.Tunables.Vector4TunableViewModel {Class}
} // RSG.Metadata.ViewModel.Tunables {Namespace}
