﻿//---------------------------------------------------------------------------------------------
// <copyright file="CommandDefinition.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Input;
    using System.Windows.Media.Imaging;
    using System.Xml;
    using RSG.Editor.Resources;

    /// <summary>
    /// Provides a base class to all of the command definitions that wrap a command object.
    /// </summary>
    public abstract class CommandDefinition : NotifyPropertyChangedBase
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="IgnoreNextExecution"/> property.
        /// </summary>
        private bool _ignoreNextExecution;

        /// <summary>
        /// The private field used for the <see cref="IsEnabled"/> property.
        /// </summary>
        private bool _isEnabled;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CommandDefinition"/> class.
        /// </summary>
        protected CommandDefinition()
        {
            this._isEnabled = true;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the command category name this definition belongs to.
        /// </summary>
        public virtual string CategoryName
        {
            get
            {
                if (this.Command == null)
                {
                    return StringTable.UnknownCommandCategory;
                }

                return this.Command.Category;
            }
        }

        /// <summary>
        /// Gets the <see cref="RockstarRoutedCommand"/> that this definition is wrapping.
        /// </summary>
        public abstract RockstarRoutedCommand Command { get; }

        /// <summary>
        /// Gets the description for this command. This is also what is shown as a tooltip
        /// along with the key gesture if one is set.
        /// </summary>
        public virtual string Description
        {
            get
            {
                if (this.Command == null)
                {
                    return null;
                }

                if (String.IsNullOrWhiteSpace(this.Command.Description))
                {
                    return this.Command.Name;
                }

                return this.Command.Description;
            }
        }

        /// <summary>
        /// Gets the System.Windows.Media.Imaging.BitmapSource object that represents the icon
        /// that is used for this command, this can be null.
        /// </summary>
        public virtual BitmapSource Icon
        {
            get
            {
                if (this.Command == null)
                {
                    return null;
                }

                return this.Command.Icon;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the command that this class is defining
        /// should have its next execution ignored.
        /// </summary>
        public bool IgnoreNextExecution
        {
            get { return this._ignoreNextExecution; }
            set { this._ignoreNextExecution = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the instances for this command definition
        /// which be enabled or not.
        /// </summary>
        public bool IsEnabled
        {
            get { return this._isEnabled; }
            set { this.SetProperty(ref this._isEnabled, value); }
        }

        /// <summary>
        /// Gets or sets the System.Windows.Input.KeyGesture object that is associated with
        /// this command definition.
        /// </summary>
        public virtual KeyGesture KeyGesture
        {
            get
            {
                if (this.Command == null)
                {
                    return null;
                }

                InputGestureCollection gestures = this.Command.InputGestures;
                if (gestures == null || gestures.Count == 0)
                {
                    return null;
                }

                return this.Command.InputGestures.OfType<KeyGesture>().FirstOrDefault();
            }

            set
            {
                if (this.Command == null)
                {
                    return;
                }

                InputGestureCollection gestures = this.Command.InputGestures;
                if (gestures == null || gestures.IsReadOnly)
                {
                    return;
                }

                if (gestures.Count == 0)
                {
                    gestures.Add(value);
                }
                else
                {
                    gestures[0] = value;
                }

                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets the number of key gestures currently associated with this command.
        /// </summary>
        public int KeyGestureCount
        {
            get
            {
                if (this.Command == null || this.Command.InputGestures == null)
                {
                    return 0;
                }

                return this.Command.InputGestures.Count;
            }
        }

        /// <summary>
        /// Gets the name of this command definition.
        /// </summary>
        public virtual string Name
        {
            get
            {
                if (this.Command == null)
                {
                    return String.Empty;
                }

                return this.Command.Name;
            }
        }

        /// <summary>
        /// Gets a iterator around all of the key gestures that are associated with this
        /// command.
        /// </summary>
        internal IEnumerable<KeyGesture> KeyGestures
        {
            get
            {
                if (this.Command == null)
                {
                    return Enumerable.Empty<KeyGesture>();
                }

                InputGestureCollection gestures = this.Command.InputGestures;
                if (gestures == null)
                {
                    return Enumerable.Empty<KeyGesture>();
                }

                return gestures.OfType<KeyGesture>();
            }
        }

        /// <summary>
        /// Gets or sets the key gesture within the commands gesture collection at the
        /// specified zero-based index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index for the gesture to get or set.
        /// </param>
        /// <returns>
        /// The key gesture within the commands gesture collection at the specified zero-based
        /// index.
        /// </returns>
        public KeyGesture this[int index]
        {
            get
            {
                if (this.Command == null || this.Command.InputGestures == null)
                {
                    return null;
                }

                if (this.Command.InputGestures.Count <= index)
                {
                    return null;
                }

                return this.Command.InputGestures[index] as KeyGesture;
            }

            set
            {
                if (this.Command == null)
                {
                    return;
                }

                InputGestureCollection gestures = this.Command.InputGestures;
                if (gestures == null || gestures.IsReadOnly || gestures.Count <= index)
                {
                    return;
                }

                this.Command.InputGestures[index] = value;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Adds the specified key gesture to the commands collection of associated gestures.
        /// </summary>
        /// <param name="gesture">
        /// The gesture to add to the command.
        /// </param>
        /// <returns>
        /// The zero-based index into the commands gesture collection where the command was
        /// added if it was added successfully; otherwise, -1.
        /// </returns>
        public int AddKeyGesture(KeyGesture gesture)
        {
            if (this.Command == null)
            {
                return -1;
            }

            InputGestureCollection gestures = this.Command.InputGestures;
            if (gestures == null || gestures.IsReadOnly)
            {
                return -1;
            }

            gestures.Add(gesture);
            if (gestures.Count == 1)
            {
                this.NotifyPropertyChanged("KeyGesture");
            }

            return this.Command.InputGestures.IndexOf(gesture);
        }

        /// <summary>
        /// Deserialises the commands state using the data contained within the specified xml
        /// reader.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the state data.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        /// <returns>
        /// A value indicating whether the command needs to be executed due to the
        /// de-serialisation process.
        /// </returns>
        public virtual bool DeserialiseState(XmlReader reader, IFormatProvider provider)
        {
            return false;
        }

        /// <summary>
        /// Removes the commands key gesture located at the specified index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index to the key gesture to remove.
        /// </param>
        public void RemoveKeyGesture(int index)
        {
            if (this.Command == null)
            {
                return;
            }

            InputGestureCollection gestures = this.Command.InputGestures;
            if (gestures == null || gestures.IsReadOnly || index >= gestures.Count)
            {
                return;
            }

            gestures.RemoveAt(index);
        }

        /// <summary>
        /// Replaces the first occurrence of the specified old gesture with the specified new
        /// gesture.
        /// </summary>
        /// <param name="oldGesture">
        /// The old gesture that will be replaced by the specified new gesture.
        /// </param>
        /// <param name="newGesture">
        /// The new gesture to replace the old gesture with.
        /// </param>
        public void ReplaceKeyGesture(KeyGesture oldGesture, KeyGesture newGesture)
        {
            if (this.Command == null)
            {
                return;
            }

            InputGestureCollection gestures = this.Command.InputGestures;
            if (gestures == null || gestures.IsReadOnly)
            {
                return;
            }

            int index = gestures.IndexOf(oldGesture);
            if (index == -1)
            {
                if (newGesture != null)
                {
                    gestures.Add(newGesture);
                }

                return;
            }

            if (newGesture != null)
            {
                gestures[index] = newGesture;
            }
            else
            {
                gestures.RemoveAt(index);
            }
        }

        /// <summary>
        /// Serialises the state of this definition into the specified xml writer.
        /// </summary>
        /// <param name="writer">
        /// The xml writer that the state gets serialised out to.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        public virtual void SerialiseState(XmlWriter writer, IFormatProvider provider)
        {
        }
        #endregion Methods
    } // RSG.Editor.CommandDefinition {Class}
} // RSG.Editor {Namespace}
