﻿//---------------------------------------------------------------------------------------------
// <copyright file="CloseFileAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Rpf.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using RSG.Editor;
    using RSG.Editor.SharedCommands;
    using RSG.Rpf.ViewModel;

    /// <summary>
    /// Implements the <see cref="RockstarCommands.Close"/> command.
    /// </summary>
    public class CloseFileAction : ButtonAction<IEnumerable<RpfViewerDataContext>>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CloseFileAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public CloseFileAction(
            ParameterResolverDelegate<IEnumerable<RpfViewerDataContext>> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="viewModels">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(IEnumerable<RpfViewerDataContext> viewModels)
        {
            foreach (RpfViewerDataContext viewModel in viewModels)
            {
                if (viewModel != null)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="viewModels">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(IEnumerable<RpfViewerDataContext> viewModels)
        {
            foreach (RpfViewerDataContext viewModel in viewModels)
            {
                if (viewModel == null)
                {
                    continue;
                }

                viewModel.Unload();
            }
        }
        #endregion Methods
    } // RSG.Rpf.Commands.CloseFileAction {Class}
} // RSG.Rpf.Commands {Namespace}
