﻿//---------------------------------------------------------------------------------------------
// <copyright file="RelayCommandAsync{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Threading.Tasks;
    using System.Windows.Input;

    /// <summary>
    /// Asynchronous version of the RelayCommand.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the command parameter that should be passed into the delegated can execute
    /// and execute methods.
    /// </typeparam>
    public class RelayCommandAsync<T> : IAsyncCommand, ICommand
    {
        #region Fields
        /// <summary>
        /// Action to execute when the command is invoked.
        /// </summary>
        private readonly Func<T, Task> _action;

        /// <summary>
        /// Function to execute when determining if this command can be executed.
        /// </summary>
        private readonly Predicate<T> _prediction;

        /// <summary>
        /// Flag indicating whether we are currently executing the command.
        /// </summary>
        private bool _executing = false;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="RelayCommandAsync{T}"/> class, using
        /// the specified action method as a command delegate.
        /// </summary>
        /// <param name="action">
        /// The method to call when this command executes.
        /// </param>
        public RelayCommandAsync(Func<T, Task> action)
            : this(action, null)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RelayCommandAsync{T}"/> class, using
        /// the specified prediction and action methods as command delegates.
        /// </summary>
        /// <param name="action">
        /// The method to call when this command executes. This only gets called if the
        /// specified prediction method returns true.
        /// </param>
        /// <param name="prediction">
        /// The method to call when determining if this command can be executed or not. Can
        /// be null.
        /// </param>
        public RelayCommandAsync(Func<T, Task> action, Predicate<T> prediction)
        {
            if (action == null)
            {
                throw new SmartArgumentNullException(() => action);
            }

            this._action = action;
            this._prediction = prediction;
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs when changes occur that affect whether or not the command can be executed by
        /// the user.
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
        #endregion Events

        #region Methods
        /// <summary>
        /// Gets called when the manager is deciding if this command can be executed. This
        /// returns true be default.
        /// </summary>
        /// <param name="parameter">
        /// The parameter that has been sent with the command.
        /// </param>
        /// <returns>
        /// True if the command can be executed; otherwise, false.
        /// </returns>
        public bool CanExecute(Object parameter)
        {
            if (this._prediction != null)
            {
                return !this._executing && this._prediction((T)parameter);
            }

            return !this._executing;
        }

        /// <summary>
        /// Asynchronously executes the action.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter that has been setup in the binding to this command.
        /// </param>
        /// <returns>
        /// A task that is executing the command logic.
        /// </returns>
        public async Task ExecuteAsync(Object parameter)
        {
            this._executing = true;
            this.OnCanExecuteChanged();

            try
            {
                await this._action((T)parameter);
            }
            finally
            {
                this._executing = false;
                this.OnCanExecuteChanged();
            }
        }

        /// <summary>
        /// Asynchronously executes the command.
        /// </summary>
        /// <param name="parameter">
        /// The parameter that has been sent with the command.
        /// </param>
        public async void Execute(Object parameter)
        {
            await this.ExecuteAsync(parameter);
        }

        /// <summary>
        /// Utility method for raising the CanExecuteChanged event.
        /// </summary>
        private void OnCanExecuteChanged()
        {
            CommandManager.InvalidateRequerySuggested();
        }
        #endregion Methods
    } // RSG.Editor.RelayCommandAsync{T] {Class}
} // RSG.Editor {Namespace}
