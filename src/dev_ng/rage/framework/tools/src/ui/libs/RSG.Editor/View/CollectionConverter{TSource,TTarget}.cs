﻿//---------------------------------------------------------------------------------------------
// <copyright file="CollectionConverter{TSource,TTarget}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2012. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.Diagnostics;
    using System.Windows;

    /// <summary>
    /// Represents a collection of objects that have been converted from a source collection of
    /// objects that also supports collection changes.
    /// </summary>
    /// <typeparam name="TSource">
    /// The type of elements in the source collection.
    /// </typeparam>
    /// <typeparam name="TTarget">
    /// The type of elements in the collection.
    /// </typeparam>
    public class CollectionConverter<TSource, TTarget> :
        ReadOnlyObservableCollection<TTarget>, IWeakEventListener
    {
        #region Fields
        /// <summary>
        /// A private reference to the function to use as a converter between the source
        /// collection and this collection.
        /// </summary>
        private Func<TSource, TTarget> _converter;

        /// <summary>
        /// The private reference to the source collection of items.
        /// </summary>
        private IEnumerable _sourceCollection;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="CollectionConverter{TSource,TTarget}"/> class.
        /// </summary>
        /// <param name="source">
        /// The iterator around the source items for this collection.
        /// </param>
        /// <param name="converter">
        /// The converter that will be used once per source item before adding them to this
        /// collection.
        /// </param>
        public CollectionConverter(IEnumerable source, Func<TSource, TTarget> converter)
            : base(new SuspendableObservableCollection<TTarget>())
        {
            this._converter = converter;
            this._sourceCollection = source;
            this.Initialise();
        }

        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="CollectionConverter{TSource,TTarget}"/> class.
        /// </summary>
        protected CollectionConverter()
            : base(new SuspendableObservableCollection<TTarget>())
        {
        }

        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="CollectionConverter{TSource,TTarget}"/> class.
        /// </summary>
        /// <param name="source">
        /// The iterator around the source items for this collection.
        /// </param>
        protected CollectionConverter(IEnumerable source)
            : base(new SuspendableObservableCollection<TTarget>())
        {
            this.ResetSourceItems(source);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets this collections items wrapped up in a Suspended Observable Collection.
        /// </summary>
        protected SuspendableObservableCollection<TTarget> InnerItems
        {
            get { return (SuspendableObservableCollection<TTarget>)this.Items; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Adds the specified handler to this collections collection changed event. This saves
        /// you from having to cast the collection in your code.
        /// </summary>
        /// <param name="handler">
        /// The handler to add to the collection changed event.
        /// </param>
        public void AddCollectionChangedHandler(NotifyCollectionChangedEventHandler handler)
        {
            INotifyCollectionChanged notifyChanged = this as INotifyCollectionChanged;
            notifyChanged.CollectionChanged += handler;
        }

        /// <summary>
        /// Receives events from the centralized event manager.
        /// </summary>
        /// <param name="managerType">
        /// The type of the System.Windows.WeakEventManager calling this method.
        /// </param>
        /// <param name="sender">
        /// Object that originated the event.
        /// </param>
        /// <param name="e">
        /// Event data.
        /// </param>
        /// <returns>
        /// True if the listener handled the event; otherwise, false.
        /// </returns>
        public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
        {
            NotifyCollectionChangedEventArgs args = e as NotifyCollectionChangedEventArgs;
            if (args == null || managerType != typeof(CollectionChangedEventManager))
            {
                return false;
            }

            switch (args.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        for (int i = 0; i < args.NewItems.Count; i++)
                        {
                            TSource item = (TSource)args.NewItems[i];
                            this.InsertSourceItem(args.NewStartingIndex + i, item);
                        }
                    }

                    break;
                case NotifyCollectionChangedAction.Remove:
                    {
                        for (int i = 0; i < args.OldItems.Count; i++)
                        {
                            this.RemoveSourceItem(args.OldStartingIndex);
                        }
                    }

                    break;
                case NotifyCollectionChangedAction.Replace:
                    {
                        for (int i = 0; i < args.NewItems.Count; i++)
                        {
                            TSource item = (TSource)args.NewItems[i];
                            this.ReplaceSourceItem(args.OldStartingIndex + i, item);
                        }
                    }

                    break;
                case NotifyCollectionChangedAction.Move:
                    {
                        for (int i = 0; i < args.NewItems.Count; i++)
                        {
                            int oldIndex = args.OldStartingIndex + i;
                            int newIndex = args.NewStartingIndex + i;
                            this.MoveSourceItem(oldIndex, newIndex);
                        }
                    }

                    break;
                case NotifyCollectionChangedAction.Reset:
                    {
                        this.ResetSourceItems(this._sourceCollection);
                    }

                    break;
            }

            this.HandleCollectionChange();
            return true;
        }

        /// <summary>
        /// Removes the specified handler to this collections collection changed event. This
        /// saves you from having to cast the collection in your code.
        /// </summary>
        /// <param name="handler">
        /// The handler to remove from the collection changed event.
        /// </param>
        public void RemoveCollectionChangedHandler(NotifyCollectionChangedEventHandler handler)
        {
            INotifyCollectionChanged notifyChanged = this as INotifyCollectionChanged;
            notifyChanged.CollectionChanged -= handler;
        }

        /// <summary>
        /// Converts the single specified source item to the target type.
        /// </summary>
        /// <param name="item">
        /// The item to convert.
        /// </param>
        /// <returns>
        /// A new item of the target type converted from the specified source item.
        /// </returns>
        protected virtual TTarget ConvertItem(TSource item)
        {
            if (this._converter == null)
            {
                return default(TTarget);
            }

            return this._converter(item);
        }

        /// <summary>
        /// Gets called whenever the collection changes.
        /// </summary>
        protected virtual void HandleCollectionChange()
        {
        }

        /// <summary>
        /// Initialises this class to handle the associated source collection.
        /// </summary>
        protected void Initialise()
        {
            while (true)
            {
                using (var enumerable = new StableEnumerable(this._sourceCollection))
                {
                    foreach (object current in enumerable)
                    {
                        this.Items.Add(this.ConvertItem((TSource)current));
                        if (enumerable.DetectedChange)
                        {
                            break;
                        }
                    }

                    if (enumerable.DetectedChange)
                    {
                        this.Items.Clear();
                        continue;
                    }
                }

                break;
            }

            this.SetupListener();
        }

        /// <summary>
        /// Inserts the converted item from the specified source item into this collection at
        /// the specified index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index where the new item should be inserted.
        /// </param>
        /// <param name="item">
        /// The item whose converted value will added to this collection.
        /// </param>
        protected virtual void InsertSourceItem(int index, TSource item)
        {
            this.InnerItems.Insert(index, this.ConvertItem(item));
        }

        /// <summary>
        /// Moves the item at the specified old index to the new index in the collection.
        /// </summary>
        /// <param name="oldIndex">
        /// The zero-based index specifying the location of the item to move.
        /// </param>
        /// <param name="newIndex">
        /// The zero-based index specifying the new location of the item.
        /// </param>
        protected virtual void MoveSourceItem(int oldIndex, int newIndex)
        {
            this.InnerItems.Move(oldIndex, newIndex);
        }

        /// <summary>
        /// Removes the item at the specified zero-based index from this collection.
        /// </summary>
        /// <param name="index">
        /// The zero-based index of the item to remove.
        /// </param>
        protected virtual void RemoveSourceItem(int index)
        {
            this.InnerItems.RemoveAt(index);
        }

        /// <summary>
        /// Replaces the item at the specified zero-based index with the specified item.
        /// </summary>
        /// <param name="index">
        /// The zero-based index of the item to replace.
        /// </param>
        /// <param name="item">
        /// The item that will replace the item at the specified index.
        /// </param>
        protected virtual void ReplaceSourceItem(int index, TSource item)
        {
            this.InnerItems[index] = this.ConvertItem(item);
        }

        /// <summary>
        /// Resets the items in this collection to the items specified.
        /// </summary>
        /// <param name="newItems">
        /// The items that will replace the items currently in this collection.
        /// </param>
        protected virtual void ResetSourceItems(IEnumerable newItems)
        {
            this.RemoveListener();
            using (this.InnerItems.SuspendChangeNotification())
            {
                this.InnerItems.Clear();
                int num = 0;
                if (newItems != null)
                {
                    IEnumerator enumerator = newItems.GetEnumerator();
                    try
                    {
                        while (enumerator.MoveNext())
                        {
                            TSource item = (TSource)enumerator.Current;
                            this.InsertSourceItem(num, item);
                            num++;
                        }
                    }
                    finally
                    {
                        IDisposable disposable = enumerator as IDisposable;
                        if (disposable != null)
                        {
                            disposable.Dispose();
                        }
                    }
                }
            }

            this._sourceCollection = newItems;
            this.HandleCollectionChange();
            this.SetupListener();
        }

        /// <summary>
        /// Uses the CollectionChangedEventManager to stop listening to the associated source
        /// collection for any changes.
        /// </summary>
        private void RemoveListener()
        {
            var collection = this._sourceCollection as INotifyCollectionChanged;
            if (collection != null)
            {
                CollectionChangedEventManager.RemoveListener(collection, this);
            }
        }

        /// <summary>
        /// Sets up the CollectionChangedEventManager to start listening to the associated
        /// source collection for any changes.
        /// </summary>
        private void SetupListener()
        {
            var collection = this._sourceCollection as INotifyCollectionChanged;
            if (collection != null)
            {
                CollectionChangedEventManager.RemoveListener(collection, this);
                CollectionChangedEventManager.AddListener(collection, this);
            }
        }
        #endregion Methods
    } // RSG.Editor.View.CollectionConverter{TSource, TTarget} {Class}
} // RSG.Editor.View {Namespace}
