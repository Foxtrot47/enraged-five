﻿using RSG.Editor.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RSG.Text.View
{
    /// <summary>
    /// Interaction logic for RenameWIndow.xaml
    /// </summary>
    public partial class RenameWindow : RsWindow
    {
        public RenameWindow(string currentName)
        {
            InitializeComponent();

            this.ResultTextBox.Text = currentName;
            this.ResultTextBox.Focus();
            this.ResultTextBox.CaretIndex = this.ResultTextBox.Text.Length;
        }

        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }
    }
}
