﻿//---------------------------------------------------------------------------------------------
// <copyright file="InsertNewMetadataItemBelowDefinition.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Commands
{
    /// <summary>
    /// The command definition for the dynamic add new item menu that lists the valid items
    /// that can be added to the active item. This class cannot be inherited.
    /// </summary>
    public sealed class InsertNewMetadataItemBelowDefinition : NewMetadataItemDefinitionBase
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="InsertNewMetadataItemBelowDefinition"/> class.
        /// </summary>
        public InsertNewMetadataItemBelowDefinition()
            : base(MetadataCommands.InsertNewMetadataItemBelow)
        {
        }
        #endregion Constructors
    } // RSG.Metadata.Commands.InsertNewMetadataItemBelowDefinition {Class}
} // RSG.Metadata.Commands {Namespace}
