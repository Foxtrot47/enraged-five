﻿//---------------------------------------------------------------------------------------------
// <copyright file="INonClientArea.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System.Windows;
    using RSG.Interop.Microsoft.Windows;

    /// <summary>
    /// When implemented represents a control that can be positioned inside the non-client
    /// portion of a window.
    /// </summary>
    internal interface INonClientArea
    {
        #region Methods
        /// <summary>
        /// Tests the specified point coordinate and returns a Hit Test result based on the
        /// system hot spot.
        /// </summary>
        /// <param name="point">
        /// The point to test.
        /// </param>
        /// <returns>
        /// A result indicating the position of the cursor hot spot.
        /// </returns>
        NcHitTestResult HitTest(Point point);
        #endregion Methods
    } // RSG.Editor.Controls.INonClientArea {Interface}
} // RSG.Editor.Controls.Helpers {Namespace}
