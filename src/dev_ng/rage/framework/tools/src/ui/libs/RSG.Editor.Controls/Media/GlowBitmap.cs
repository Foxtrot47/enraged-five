﻿//---------------------------------------------------------------------------------------------
// <copyright file="GlowBitmap.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Media
{
    using System;
    using System.Globalization;
    using System.Runtime.InteropServices;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using RSG.Base.Extensions;
    using RSG.Interop.Microsoft.Windows;

    /// <summary>
    /// Represents a native bitmap image that is used to display a glow effect around a managed
    /// window.
    /// </summary>
    internal sealed class GlowBitmap : DisposableObject
    {
        #region Fields
        /// <summary>
        /// The private array that defines the transparency masks for the different parts
        /// that make up the glow bitmap.
        /// </summary>
        private static readonly CachedBitmapInfo[] _transparencyMasks;

        /// <summary>
        /// A private handle to the bitmap.
        /// </summary>
        private readonly IntPtr _bitmap;

        /// <summary>
        /// The <see cref="BITMAPINFO"/> structure that defines the dimensions and colour
        /// information of the glow bitmap.
        /// </summary>
        private readonly BITMAPINFO _bitmapInfo;

        /// <summary>
        /// A private pointer that points to the bits that makeup the bitmap.
        /// </summary>
        private readonly IntPtr _bits;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="GlowBitmap"/> class.
        /// </summary>
        static GlowBitmap()
        {
            _transparencyMasks = new CachedBitmapInfo[(int)GlowBitmapPart.Count];
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="GlowBitmap"/> class using the
        /// specified screen drawing context and the specified width and height.
        /// </summary>
        /// <param name="screenDrawingContext">
        /// A handle to a screen device context.
        /// </param>
        /// <param name="width">
        /// The width in pixels of the glow bitmap.
        /// </param>
        /// <param name="height">
        /// The height in pixels of the glow bitmap.
        /// </param>
        public GlowBitmap(IntPtr screenDrawingContext, int width, int height)
        {
            this._bitmapInfo.Size = (uint)Marshal.SizeOf(typeof(BITMAPINFOHEADER));
            this._bitmapInfo.Planes = 1;
            this._bitmapInfo.BitCount = 32;
            this._bitmapInfo.Compression = 0;
            this._bitmapInfo.XPixelsPerMeter = 0;
            this._bitmapInfo.YPixelsPerMeter = 0;
            this._bitmapInfo.Width = width;
            this._bitmapInfo.Height = -height;

            this._bitmap = Gdi32.CreateDeviceIndependenctBitmapSection(
                screenDrawingContext, ref this._bitmapInfo, out this._bits);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the pointer to the device-independent bits for this glow bitmap.
        /// </summary>
        public IntPtr Bits
        {
            get { return this._bits; }
        }

        /// <summary>
        /// Gets the handle to the glow bitmap.
        /// </summary>
        public IntPtr Handle
        {
            get { return this._bitmap; }
        }

        /// <summary>
        /// Gets the height of this glow bitmap.
        /// </summary>
        public int Height
        {
            get { return -this._bitmapInfo.Height; }
        }

        /// <summary>
        /// Gets the width of this glow bitmap.
        /// </summary>
        public int Width
        {
            get { return this._bitmapInfo.Width; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new glow bitmap with the given drawing context for the specified part
        /// and with the specified colour.
        /// </summary>
        /// <param name="drawingContext">
        /// The drawing context to use to draw the glow bitmap.
        /// </param>
        /// <param name="part">
        /// The bitmap part that this glow bitmap will represent.
        /// </param>
        /// <param name="color">
        /// The glow colour.
        /// </param>
        /// <returns>
        /// A newly created glow bitmap that represents a bitmap with the specified
        /// parameters.
        /// </returns>
        public static GlowBitmap Create(
            GlowDrawingContext drawingContext,
            GlowBitmapPart part,
            Color color)
        {
            CachedBitmapInfo alpha = GlowBitmap.GetOrCreateAlphaMask(part);
            IntPtr screenDC = drawingContext.ScreenDC;
            GlowBitmap glowBitmap = new GlowBitmap(screenDC, alpha.Width, alpha.Height);

            for (int i = 0; i < alpha.Data.Length; i += 4)
            {
                byte b = alpha.Data[i + 3];
                byte val = GlowBitmap.PremultiplyAlpha(color.R, b);
                byte val2 = GlowBitmap.PremultiplyAlpha(color.G, b);
                byte val3 = GlowBitmap.PremultiplyAlpha(color.B, b);
                Marshal.WriteByte(glowBitmap.Bits, i, val3);
                Marshal.WriteByte(glowBitmap.Bits, i + 1, val2);
                Marshal.WriteByte(glowBitmap.Bits, i + 2, val);
                Marshal.WriteByte(glowBitmap.Bits, i + 3, b);
            }

            return glowBitmap;
        }

        /// <summary>
        /// Disposes of the native resources being used by the glow bitmap.
        /// </summary>
        protected override void DisposeNativeResources()
        {
            Gdi32.DeleteObject(this._bitmap);
        }

        /// <summary>
        /// Gets a per-cached alpha mask for the glow bitmap part or creates one if one
        /// isn't available.
        /// </summary>
        /// <param name="bitmapPart">
        /// The part of the glow bitmap to get the alpha mask for.
        /// </param>
        /// <returns>
        /// A cached bitmap info for the specified glow bitmap part.
        /// </returns>
        private static CachedBitmapInfo GetOrCreateAlphaMask(GlowBitmapPart bitmapPart)
        {
            if (GlowBitmap._transparencyMasks[(int)bitmapPart] == null)
            {
                string name = typeof(GlowBitmap).Assembly.GetName().Name;
                string path = "Resources/GlowBorders/" + bitmapPart.ToString() + ".png";
                string bitmapPath = "pack://application:,,,/{0};component/{1}";
                bitmapPath = bitmapPath.FormatInvariant(name, path);
                Uri uri = new Uri(bitmapPath, UriKind.Absolute);

                BitmapImage bitmapImage = new BitmapImage(uri);
                byte[] data = new byte[4 * bitmapImage.PixelWidth * bitmapImage.PixelHeight];
                int stride = 4 * bitmapImage.PixelWidth;
                bitmapImage.CopyPixels(data, stride, 0);

                _transparencyMasks[(int)bitmapPart] = new CachedBitmapInfo(
                    data, bitmapImage.PixelWidth, bitmapImage.PixelHeight);
            }

            return GlowBitmap._transparencyMasks[(int)bitmapPart];
        }

        /// <summary>
        /// Determines the colour value of the specified channel with the specified alpha
        /// pre applied to it.
        /// </summary>
        /// <param name="channel">
        /// The colour channel that will have the alpha value applied to it.
        /// </param>
        /// <param name="alpha">
        /// The amount of alpha to apply to the channel.
        /// </param>
        /// <returns>
        /// The resulting colour value of the channel having the alpha applied to it.
        /// </returns>
        private static byte PremultiplyAlpha(byte channel, byte alpha)
        {
            return (byte)((double)(channel * alpha) / 255.0);
        }
        #endregion Methods
    } // RSG.Editor.Controls.Media.GlowBitmap {Class}
} // RSG.Editor.Controls.Media {Namespace}
