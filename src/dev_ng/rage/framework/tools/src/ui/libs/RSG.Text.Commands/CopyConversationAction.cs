﻿//---------------------------------------------------------------------------------------------
// <copyright file="CopyConversationAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Commands
{
    using System.Linq;
    using RSG.Editor;
    using RSG.Text.ViewModel;

    /// <summary>
    /// Implements the RockstarCommands.Copy command.
    /// </summary>
    public class CopyConversationAction : CopyAndPasteConversationAction
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CopyConversationAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public CopyConversationAction(
            ParameterResolverDelegate<TextConversationCommandArgs> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(TextConversationCommandArgs args)
        {
            return args.Selected != null && args.Selected.Count() > 0;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(TextConversationCommandArgs args)
        {
            this.CopyConversationsToClipboard(args.Selected);
        }
        #endregion Methods
    } // RSG.Text.Commands.CopyConversationAction {Class}
} // RSG.Text.Commands {Namespace}
