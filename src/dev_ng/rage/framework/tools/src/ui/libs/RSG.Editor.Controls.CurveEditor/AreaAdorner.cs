﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace RSG.Editor.Controls.CurveEditor
{
    /// <summary>
    /// Adorner for displaying a selection area over the top of another control.
    /// </summary>
    internal sealed class AreaAdorner : Adorner
    {
        #region Fields
        /// <summary>
        /// Brush to use for the fill of the area.
        /// </summary>
        private Brush _fill;

        /// <summary>
        /// Pen to use for the border of the area.
        /// </summary>
        private Pen _pen;

        /// <summary>
        /// Backing field for the <see cref="Area" /> property.
        /// </summary>
        private Rect _area;
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parent"></param>
        public AreaAdorner(UIElement parent)
            : base(parent)
        {
            // Make sure the mouse doesn't see us.
            this.IsHitTestVisible = false;

            // We only draw a rectangle when we're enabled.
            this.IsEnabledChanged += delegate { this.InvalidateVisual(); };

            _fill = SystemColors.HighlightBrush.Clone();
            _fill.Opacity = 0.4;

            _pen = new Pen(SystemColors.HighlightBrush, 1.0);
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Gets or sets the area of the selection rectangle.
        /// </summary>
        public Rect Area
        {
            get { return this._area; }
            set
            {
                if (this._area != value)
                {
                    this._area = value;
                    this.InvalidateVisual();
                }
            }
        }
        #endregion // Properties

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="drawingContext"></param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            if (this.IsEnabled)
            {
                // Make the lines snap to pixels (add half the pen width [0.5])
                double[] x = { this.Area.Left + 0.5, this.Area.Right + 0.5 };
                double[] y = { this.Area.Top + 0.5, this.Area.Bottom + 0.5 };
                drawingContext.PushGuidelineSet(new GuidelineSet(x, y));

                drawingContext.DrawRectangle(_fill, _pen, this.Area);
            }
        }
        #endregion // Overrides
    } // AreaAdorner
}
