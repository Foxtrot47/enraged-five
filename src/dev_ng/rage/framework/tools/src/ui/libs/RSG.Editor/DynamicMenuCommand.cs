﻿//---------------------------------------------------------------------------------------------
// <copyright file="DynamicMenuCommand.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System.Collections.ObjectModel;

    /// <summary>
    /// Represents a command definition that contains a dynamic list of instances inside of it
    /// bound to the same routed command with different parameters. This command definition
    /// when no items are specified shows the menu item as a command item with no command
    /// parameter. This can be thought of as a menu controller with just one routed command.
    /// </summary>
    public class DynamicMenuCommand : CommandDefinition
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Command"/> property.
        /// </summary>
        private RockstarRoutedCommand _command;

        /// <summary>
        /// The private field used for the <see cref="Items"/> property.
        /// </summary>
        private ObservableCollection<IDynamicMenuItem> _items;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DynamicMenuCommand"/> class.
        /// </summary>
        /// <param name="command">
        /// The routed command that is being wrapped by this definition.
        /// </param>
        public DynamicMenuCommand(RockstarRoutedCommand command)
        {
            this._command = command;
            this._items = new ObservableCollection<IDynamicMenuItem>();
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the System.Windows.Input.RoutedCommand that this definition is wrapping.
        /// </summary>
        public override RockstarRoutedCommand Command
        {
            get { return _command; }
        }

        /// <summary>
        /// Gets a collection of items that represent the different items that can be executed
        /// through this command.
        /// </summary>
        public ObservableCollection<IDynamicMenuItem> Items
        {
            get { return this._items; }
            internal set { this._items = value; }
        }
        #endregion Properties
    } // RSG.Editor.DynamicMenuCommand {Class}
} // RSG.Editor {Namespace}
