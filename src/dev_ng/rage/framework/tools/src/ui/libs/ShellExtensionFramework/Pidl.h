#ifndef SEF_PIDL_H
#define SEF_PIDL_H

#if defined _MSC_VER && _MSC_VER >= 1300
#pragma once
#endif

#include "ShellExtensionFramework.h"

BEGIN_SHELL_EXTENSION_FRAMEWORK_NS

/**
	\brief Abstract PIDL data class.
 */
class CPidl
{
public:
	virtual LONG GetSize( ) = 0;
	virtual void CopyTo( LPVOID pTarget ) = 0;
};

END_SHELL_EXTENSION_FRAMEWORK_NS

#endif // SEF_PIDL_H