﻿//---------------------------------------------------------------------------------------------
// <copyright file="ComboItemsCollection.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System.Collections;
    using System.Collections.Specialized;
    using System.Linq;
    using RSG.Editor.View;

    /// <summary>
    /// The collection that is used for the combo items collection inside a
    /// <see cref="CommandBarItemViewModel"/> object.
    /// </summary>
    public sealed class ComboItemsCollection :
        CollectionConverter<IMultiCommandItem, MultiCommandItemViewModel>
    {
        #region Fields
        /// <summary>
        /// The parent view model for this object.
        /// </summary>
        private CommandBarItemViewModel _parent;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ComboItemsCollection"/> class.
        /// </summary>
        /// <param name="source">
        /// The iterator around the source items for this collection.
        /// </param>
        /// <param name="parent">
        /// The parent view model.
        /// </param>
        public ComboItemsCollection(IEnumerable source, CommandBarItemViewModel parent)
            : base(Enumerable.Empty<object>(), null)
        {
            this._parent = parent;
            this.ResetSourceItems(source);
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Converts the single specified source item to the target type.
        /// </summary>
        /// <param name="item">
        /// The item to convert.
        /// </param>
        /// <returns>
        /// A new item of the target type converted from the specified source item.
        /// </returns>
        protected override MultiCommandItemViewModel ConvertItem(IMultiCommandItem item)
        {
            return new MultiCommandItemViewModel(item, this._parent);
        }

        /// <summary>
        /// Gets called whenever the collection changes.
        /// </summary>
        protected override void HandleCollectionChange()
        {
            this._parent.NotifyPropertyChanged("FilterCount");
        }
        #endregion Methods
    } // RSG.Editor.ComboItemsCollection {Class}
} // RSG.Editor {Namespace}
