﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RSG.Project.Commands.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class StringTable {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal StringTable() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("RSG.Project.Commands.Resources.StringTable", typeof(StringTable).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to A file with the name &apos;{0}&apos; already exists. Please give a unique name to the item you are adding, or delete the existing item first..
        /// </summary>
        internal static string AddExistingFileErrorMsg {
            get {
                return ResourceManager.GetString("AddExistingFileErrorMsg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to A file with the name &apos;{0}&apos; already exists. Do you want to replace it?.
        /// </summary>
        internal static string AddExistingFileWarningMsg {
            get {
                return ResourceManager.GetString("AddExistingFileWarningMsg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to A file with the name &apos;{0}&apos; already exists and is open in an editor. Please give a unique name to the item you are adding, or delete the existing item first..
        /// </summary>
        internal static string AddExistingOpenedFileMsg {
            get {
                return ResourceManager.GetString("AddExistingOpenedFileMsg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        internal static string CancelButtonContent {
            get {
                return ResourceManager.GetString("CancelButtonContent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Delete.
        /// </summary>
        internal static string DeleteButtonContent {
            get {
                return ResourceManager.GetString("DeleteButtonContent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Failed to delete &apos;{0}&apos;. {1}.
        /// </summary>
        internal static string DeleteFailedMsg {
            get {
                return ResourceManager.GetString("DeleteFailedMsg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Choose Remove to remove the selected items.
        ///
        ///Choose Delete to permanently delete the selected items..
        /// </summary>
        internal static string DeleteRemoveMultipleOptionMsg {
            get {
                return ResourceManager.GetString("DeleteRemoveMultipleOptionMsg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Choose Remove to remove &apos;{0}&apos; from its project.
        ///
        ///Choose Delete to permanently delete &apos;{0}&apos;..
        /// </summary>
        internal static string DeleteRemoveSingleOptionMsg {
            get {
                return ResourceManager.GetString("DeleteRemoveSingleOptionMsg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot open file &apos;{0}&apos; due to parsing error.
        ///
        ///{1}.
        /// </summary>
        internal static string FailedFileParsing {
            get {
                return ResourceManager.GetString("FailedFileParsing", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The item &apos;{0}&apos; does not exist in the project directory. It may have been moved, renamed or deleted..
        /// </summary>
        internal static string FileMissingMsg {
            get {
                return ResourceManager.GetString("FileMissingMsg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save of File Failed.
        /// </summary>
        internal static string PathCreationFailedCaption {
            get {
                return ResourceManager.GetString("PathCreationFailedCaption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unable to create the full save path..
        /// </summary>
        internal static string PathCreationFailedMsg {
            get {
                return ResourceManager.GetString("PathCreationFailedMsg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Remove.
        /// </summary>
        internal static string RemoveButtonContent {
            get {
                return ResourceManager.GetString("RemoveButtonContent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The selected items will be removed..
        /// </summary>
        internal static string RemoveMultipleMsg {
            get {
                return ResourceManager.GetString("RemoveMultipleMsg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &apos;{0}&apos; and all its contents will be removed..
        /// </summary>
        internal static string RemoveSingleBranchMsg {
            get {
                return ResourceManager.GetString("RemoveSingleBranchMsg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &apos;{0}&apos; will be removed..
        /// </summary>
        internal static string RemoveSingleMsg {
            get {
                return ResourceManager.GetString("RemoveSingleMsg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save of File Failed.
        /// </summary>
        internal static string SaveFailedCaption {
            get {
                return ResourceManager.GetString("SaveFailedCaption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unable to save the file {0} due to a unknown error..
        /// </summary>
        internal static string SaveFailedMsg {
            get {
                return ResourceManager.GetString("SaveFailedMsg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        internal static string UpdateCancelButtonContent {
            get {
                return ResourceManager.GetString("UpdateCancelButtonContent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The file &apos;{0}&apos; will need to be updated before it can be added to the project.\n\nWould you like to update it?.
        /// </summary>
        internal static string UpdateMsg {
            get {
                return ResourceManager.GetString("UpdateMsg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No.
        /// </summary>
        internal static string UpdateNoButtonContent {
            get {
                return ResourceManager.GetString("UpdateNoButtonContent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No to All.
        /// </summary>
        internal static string UpdateNoToAllButtonContent {
            get {
                return ResourceManager.GetString("UpdateNoToAllButtonContent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yes.
        /// </summary>
        internal static string UpdateYesButtonContent {
            get {
                return ResourceManager.GetString("UpdateYesButtonContent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yes to All.
        /// </summary>
        internal static string UpdateYesToAllButtonContent {
            get {
                return ResourceManager.GetString("UpdateYesToAllButtonContent", resourceCulture);
            }
        }
    }
}
