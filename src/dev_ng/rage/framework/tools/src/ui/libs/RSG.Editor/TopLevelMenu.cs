﻿//---------------------------------------------------------------------------------------------
// <copyright file="TopLevelMenu.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;

    /// <summary>
    /// Represents a menu that is positioned on the main menu.
    /// </summary>
    public class TopLevelMenu : CommandMenu
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="TopLevelMenu"/> class.
        /// </summary>
        /// <param name="priority">
        /// The initial priority that this command bar item should be given.
        /// </param>
        /// <param name="text">
        /// The text for this command bar item.
        /// </param>
        /// <param name="id">
        /// The unique identifier that is used to reference this item.
        /// </param>
        public TopLevelMenu(ushort priority, string text, Guid id)
            : base(priority, false, text, id, RockstarCommandManager.MainMenuId)
        {
        }
        #endregion Constructors
    } // RSG.Editor.TopLevelMenu {Class}
} // RSG.Editor {Namespace}
