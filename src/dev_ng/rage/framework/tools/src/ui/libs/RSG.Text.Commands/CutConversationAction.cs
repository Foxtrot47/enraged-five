﻿//---------------------------------------------------------------------------------------------
// <copyright file="CutConversationAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Commands
{
    using System;
    using System.Linq;
    using System.Windows.Input;
    using RSG.Editor;
    using RSG.Text.ViewModel;

    /// <summary>
    /// Implements the RockstarCommands.Cut command.
    /// </summary>
    public class CutConversationAction : CopyAndPasteConversationAction
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CutConversationAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public CutConversationAction(
            ParameterResolverDelegate<TextConversationCommandArgs> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(TextConversationCommandArgs args)
        {
            return args.Selected != null && args.Selected.Count() > 0;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(TextConversationCommandArgs args)
        {
            int index = args.DataGrid.SelectedIndex;
            this.CopyConversationsToClipboard(args.Selected);
            foreach (ConversationViewModel conversationViewModel in args.Selected)
            {
                args.Dialogue.Model.RemoveConversation(conversationViewModel.Model);
            }

            if (args.DataGrid.SelectedIndex == -1 && args.DataGrid.Items.Count > 0)
            {
                index = Math.Min(args.DataGrid.Items.Count - 1, index);
                args.DataGrid.SelectedItem = args.DataGrid.Items[index];

                FocusNavigationDirection direction = FocusNavigationDirection.Next;
                args.DataGrid.MoveFocus(new TraversalRequest(direction));
            }
        }
        #endregion Methods
    } // RSG.Text.Commands.CutConversationAction {Class}
} // RSG.Text.Commands {Namespace}
