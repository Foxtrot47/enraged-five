﻿//---------------------------------------------------------------------------------------------
// <copyright file="SaveDocumentAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Commands
{
    using System.Collections.Generic;
    using System.Diagnostics;
    using RSG.Editor;
    using RSG.Editor.SharedCommands;
    using RSG.Project.ViewModel;

    /// <summary>
    /// Contains the logic for the <see cref="RockstarCommands.Save"/> routed command. This
    /// class cannot be inherited.
    /// </summary>
    public sealed class SaveDocumentAction : ButtonAction<ProjectCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SaveDocumentAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public SaveDocumentAction(ProjectCommandResolver resolver)
            : base(new ParameterResolverDelegate<ProjectCommandArgs>(resolver))
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ProjectCommandArgs args)
        {
            string fallbackText = "Save Selected Items";
            if (args == null || args.ViewSite == null || args.ViewSite.ActiveDocument == null)
            {
                RockstarCommandManager.UpdateItemText(RockstarCommands.Save, fallbackText);
                return false;
            }

            string updatedText = "Save " + args.ViewSite.ActiveDocument.FriendlySavePath;
            RockstarCommandManager.UpdateItemText(RockstarCommands.Save, updatedText);
            return true;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(ProjectCommandArgs args)
        {
            IDocumentManagerService docService = this.GetService<IDocumentManagerService>();
            if (docService == null)
            {
                Debug.Fail("Unable to save document due to the fact the service is missing.");
                return;
            }

            docService.SaveActiveDocument(false);
        }
        #endregion Methods
    } // RSG.Project.Commands.SaveDocumentAction {Class}
} // RSG.Project.Commands {Namespace}
