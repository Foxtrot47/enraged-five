﻿//---------------------------------------------------------------------------------------------
// <copyright file="TypeOverwriteManager.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Input;
    using System.Windows.Interop;

    /// <summary>
    /// A manager class handling all of the type over write controls inside a application.
    /// </summary>
    public class TypeOverwriteManager
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="OvertypeMode"/> property.
        /// </summary>
        private static bool _overtypeMode;

        /// <summary>
        /// The property info instance representing the overtype node property on a text editor
        /// class.
        /// </summary>
        private static PropertyInfo _overtypeModePropertyInfo;

        /// <summary>
        /// The property info instance representing the text editor property on a text box
        /// base class.
        /// </summary>
        private static PropertyInfo _textEditorPropertyInfo;
        #endregion Fields

        #region Events
        /// <summary>
        /// Occurs whenever the <see cref="OvertypeMode"/> property changes.
        /// </summary>
        public static event EventHandler OvertypeModeChanged;
        #endregion Events

        #region Properties
        /// <summary>
        /// Gets a value indicating whether the current overtype mode for the application is
        /// set to true or false.
        /// </summary>
        public static bool OvertypeMode
        {
            get
            {
                return _overtypeMode;
            }

            private set
            {
                if (_overtypeMode == value)
                {
                    return;
                }

                _overtypeMode = value;
                EventHandler handler = OvertypeModeChanged;
                if (handler != null)
                {
                    handler(null, EventArgs.Empty);
                }
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Initialises this static class by attaching event handlers onto the command controls
        /// and set some global properties.
        /// </summary>
        public static void Initialise()
        {
            HwndSource.DefaultAcquireHwndFocusInMenuMode = false;
            EventManager.RegisterClassHandler(
                typeof(TextBox),
                UIElement.GotFocusEvent,
                new RoutedEventHandler(OnTextControlGotFocus));

            EventManager.RegisterClassHandler(
                typeof(Window),
                Keyboard.PreviewKeyDownEvent,
                new RoutedEventHandler(OnWindowPreviewKeyDown));

            Type type = typeof(TextBoxBase);
            BindingFlags flags = BindingFlags.NonPublic | BindingFlags.Instance;

            _textEditorPropertyInfo = type.GetProperty("TextEditor", flags);
            var textEditor = _textEditorPropertyInfo.GetValue(new TextBox(), null);
            type = textEditor.GetType();

            _overtypeModePropertyInfo = type.GetProperty("_OvertypeMode", flags);
        }

        /// <summary>
        /// Occurs when a text control receives the keyboard focus so we can push through the
        /// current overtype mode.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private static void OnTextControlGotFocus(object s, RoutedEventArgs e)
        {
            var textEditor = _textEditorPropertyInfo.GetValue(s, null);
            _overtypeModePropertyInfo.SetValue(textEditor, _overtypeMode, null);
        }

        /// <summary>
        /// Occurs when the PreviewKeyDownEvent routed event is caught at a window class.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private static void OnWindowPreviewKeyDown(object s, RoutedEventArgs e)
        {
            KeyEventArgs keyArgs = e as KeyEventArgs;
            if (keyArgs == null || keyArgs.Key != Key.Insert)
            {
                return;
            }

            OvertypeMode = !OvertypeMode;
            e.Handled = true;
            IInputElement element = Keyboard.FocusedElement;
            if (!(element is TextBoxBase))
            {
                return;
            }

            var textEditor = _textEditorPropertyInfo.GetValue(element, null);
            _overtypeModePropertyInfo.SetValue(textEditor, _overtypeMode, null);
        }
        #endregion Methods
    } // RSG.Editor.Controls.TypeOverwriteManager {Class}
} // RSG.Editor.Controls {Namespace}
