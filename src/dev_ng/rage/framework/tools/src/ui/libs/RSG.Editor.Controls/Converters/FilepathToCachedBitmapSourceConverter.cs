﻿//---------------------------------------------------------------------------------------------
// <copyright file="FilepathToCachedBitmapSourceConverter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Converters
{
    using System;
    using System.Globalization;
    using System.IO;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Converts a single file path to a cached bitmap source for a image control.
    /// </summary>
    public class FilepathToCachedBitmapSourceConverter : ValueConverter<string, BitmapSource>
    {
        #region Methods
        /// <summary>
        /// Converters the given value and returns the result.
        /// </summary>
        /// <param name="value">
        /// The original value to convert.
        /// </param>
        /// <param name="param">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A converted value.
        /// </returns>
        protected override BitmapSource Convert(
            string value, object param, CultureInfo culture)
        {
            if (!File.Exists(value))
            {
                return null;
            }

            BitmapImage bitmapImage = null;
            try
            {
                bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource =
                    new FileStream(value, FileMode.Open, FileAccess.Read);
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();
                bitmapImage.Freeze();
                bitmapImage.StreamSource.Dispose();
            }
            catch (IOException)
            {
            }

            return bitmapImage;
        }
        #endregion // Methods
    } // RSG.Editor.Controls.Converters.FilepathToCachedBitmapSourceConverter {Class}
} // RSG.Editor.Controls.Converters {Namespace}
