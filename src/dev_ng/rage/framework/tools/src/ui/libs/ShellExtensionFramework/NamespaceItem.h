#ifndef SEF_NAMESPACEITEM_H
#define SEF_NAMESPACEITEM_H

#if defined _MSC_VER && _MSC_VER >= 1300
#pragma once
#endif

#include "ShellExtensionFramework.h"

BEGIN_SHELL_EXTENSION_FRAMEWORK_NS

/**
	\brief Abstract Shell namespace item base class.
 */
class CNamespaceItem
{
public:
	virtual ~CNamespaceItem( ) { }
};

END_SHELL_EXTENSION_FRAMEWORK_NS

#endif // SEF_NAMESPACEITEM_H
