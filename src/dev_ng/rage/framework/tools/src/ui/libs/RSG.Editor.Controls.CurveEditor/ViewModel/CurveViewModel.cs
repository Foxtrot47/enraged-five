﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;
using RSG.Editor.Controls.CurveEditor.Model;
using RSG.Editor.Model;
using RSG.Editor.View;

namespace RSG.Editor.Controls.CurveEditor.ViewModel
{
    /// <summary>
    /// 
    /// </summary>
    public class CurveViewModel : ViewModelBase<Curve>, System.Windows.IWeakEventListener
    {
        #region Member Data
        /// <summary>
        /// Private field for the <see cref="IsVisible"/> property.
        /// </summary>
        private bool _isVisible = true;

        /// <summary>
        /// Private field for the <see cref="IsLocked"/> property.
        /// </summary>
        private bool _isLocked = false;

        /// <summary>
        /// 
        /// </summary>
        private readonly PointCollection _points;

        /// <summary>
        /// 
        /// </summary>
        private readonly PathGeometry _segmentGeometry;

        /// <summary>
        /// Transform group that gets applied to all to the path geometry.
        /// </summary>
        private readonly TransformGroup _transformGroup = new TransformGroup();

        /// <summary>
        /// Scale transform applied to the path geometry.
        /// </summary>
        private readonly ScaleTransform _scaleTransform = new ScaleTransform(1.0, 1.0);

        /// <summary>
        /// Translation transform applied to the path geometry.
        /// </summary>
        private readonly TranslateTransform _translateTransform = new TranslateTransform(0.0, 0.0);

        /// <summary>
        /// 
        /// </summary>
        private readonly PathFigure _pathFigure;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public CurveViewModel(Curve curve)
            : base(curve)
        {
            _points = new PointCollection(curve.Points, this);
            (_points as INotifyCollectionChanged).CollectionChanged += CurveViewModel_CollectionChanged;
            _transformGroup.Children.Add(_scaleTransform);
            _transformGroup.Children.Add(_translateTransform);
            _transformGroup.Children.Add(new ScaleTransform(1.0, -1.0));

            _pathFigure = new PathFigure();

            _segmentGeometry = new PathGeometry();
            _segmentGeometry.Transform = _transformGroup;
            _segmentGeometry.Figures.Add(_pathFigure);

            PropertyChangedEventManager.AddListener(curve as INotifyPropertyChanged, this, String.Empty);
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public String Name
        {
            get { return Model.Name; }
            set { Model.Name = value; }
        }

        /// <summary>
        /// Color that should be used for rendering this curve in the curve editor.
        /// </summary>
        public Color Color
        {
            get { return Model.Color; }
            set { Model.Color = value; }
        }

        /// <summary>
        /// Flag indicating whether this curve is visible.
        /// </summary>
        public bool IsVisible
        {
            get { return _isVisible; }
            set { SetProperty(ref _isVisible, value); }
        }

        /// <summary>
        /// Flag indicating whether this curve is locked to prevent editing.
        /// </summary>
        public bool IsLocked
        {
            get { return _isLocked; }
            set { SetProperty(ref _isLocked, value); }
        }

        /// <summary>
        /// List of points that this curve is composed of.
        /// </summary>
        public PointCollection Points
        {
            get { return _points; }
        }

        /// <summary>
        /// Scale that this curve should be displayed at.
        /// </summary>
        public System.Windows.Vector Scale
        {
            set
            {
                _scaleTransform.ScaleX = value.X;
                _scaleTransform.ScaleY = value.Y;
            }
        }

        /// <summary>
        /// Offset where this curve should be displayed at.
        /// </summary>
        public System.Windows.Point Offset
        {
            set
            {
                _translateTransform.X = -value.X;
                _translateTransform.Y = value.Y;
            }
        }

        /// <summary>
        /// Geometry to use for rendering the segments of this curve.
        /// </summary>
        public PathGeometry SegmentGeometry
        {
            get { return _segmentGeometry; }
        }
        #endregion // Properties

        #region Public Methods
        /// <summary>
        /// Add a new point in at the requested position.
        /// </summary>
        /// <param name="position"></param>
        public void AddPoint(System.Windows.Point position)
        {
            // Create the new point.
            Point newPoint = new Point(Model, position);

            // Determine where the new point should go.
            int insertionIdx = 0;
            for (insertionIdx = 0; insertionIdx < Model.Points.Count; ++insertionIdx)
            {
                if (newPoint.PositionX <= Model.Points[insertionIdx].PositionX)
                {
                    break;
                }
            }

            // Finally add the new point.
            Model.InsertPoint(insertionIdx, newPoint);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pointVm"></param>
        public void RemovePoint(CurvePointViewModel pointVm)
        {
            Point point = pointVm.Model;
            Model.RemovePoint(point);
        }

        /// <summary>
        /// 
        /// </summary>
        public void UpdateGeometry()
        {
            System.Windows.Application app = System.Windows.Application.Current;
            if (!app.Dispatcher.CheckAccess())
            {
                app.Dispatcher.Invoke(() => PointsReset());
                return;
            }
            else
            {
                PointsReset();
            }
        }
        #endregion // Public Methods

        #region IWeakEventListener Implementation
        /// <summary>
        /// Receives events from the centralized event manager.
        /// </summary>
        /// <param name="managerType">
        /// The type of the System.Windows.WeakEventManager calling this method.
        /// </param>
        /// <param name="sender">
        /// Object that originated the event.
        /// </param>
        /// <param name="e">
        /// Event data.
        /// </param>
        /// <returns>
        /// True if the listener handled the event; otherwise, false.
        /// </returns>
        public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
        {
            if (managerType != typeof(PropertyChangedEventManager))
            {
                PropertyChangedEventArgs args = e as PropertyChangedEventArgs;
                if (args == null)
                {
                    return false;
                }

                NotifyPropertyChanged(args.PropertyName);
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion // IWeakEventListener Implementation

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public void CurveViewModel_CollectionChanged(object sender, NotifyCollectionChangedEventArgs args)
        {
            System.Windows.Application app = System.Windows.Application.Current;
            if (!app.Dispatcher.CheckAccess())
            {
                app.Dispatcher.Invoke(() => CurveViewModel_CollectionChanged(sender, args));
                return;
            }

            switch (args.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        for (int i = 0; i < args.NewItems.Count; i++)
                        {
                            CurvePointViewModel item = (CurvePointViewModel)args.NewItems[i];
                            this.PointAdded(args.NewStartingIndex + i, item);
                        }
                    }

                    break;
                case NotifyCollectionChangedAction.Remove:
                    {
                        for (int i = 0; i < args.OldItems.Count; i++)
                        {
                            this.PointRemoved(args.OldStartingIndex);
                        }
                    }

                    break;
                case NotifyCollectionChangedAction.Replace:
                    {
                        for (int i = 0; i < args.NewItems.Count; i++)
                        {
                            CurvePointViewModel item = (CurvePointViewModel)args.NewItems[i];
                            this.PointReplaced(args.OldStartingIndex + i, item);
                        }
                    }

                    break;
                case NotifyCollectionChangedAction.Move:
                    {
                        for (int i = 0; i < args.NewItems.Count; i++)
                        {
                            int oldIndex = args.OldStartingIndex + i;
                            int newIndex = args.NewStartingIndex + i;
                            this.PointMoved(oldIndex, newIndex);
                        }
                    }

                    break;
                case NotifyCollectionChangedAction.Reset:
                    {
                        this.PointsReset();
                    }
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <param name="point"></param>
        private void PointAdded(int index, CurvePointViewModel point)
        {
            PointsReset();
            /*
            if (index == 0)
            {
                PathFigure.Segments.Add(new )
                CurveSegmentViewModel newSegment = new CurveSegmentViewModel(point, _segments[0].StartPoint, this);
                _segments.Insert(0, newSegment);
            }
            else if (index == Model.Points.Count - 1)
            {
                CurveSegmentViewModel newSegment = new CurveSegmentViewModel(_segments[Model.Points.Count - 1].EndPoint, point, this);
                _segments.Insert(Model.Points.Count - 1, newSegment);
            }
            else
            {
                CurveSegmentViewModel segmentToSplit = _segments[index - 1];
                CurveSegmentViewModel segmentA = new CurveSegmentViewModel(segmentToSplit.StartPoint, point, this);
                CurveSegmentViewModel segmentB = new CurveSegmentViewModel(point, segmentToSplit.EndPoint, this);

                _segments.Remove(segmentToSplit);
                _segments.Insert(index - 1, segmentA);
                _segments.Insert(index, segmentB);
            }
            */
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        private void PointRemoved(int index)
        {
            PointsReset();
            /*
            if (index == 0)
            {
                _segments.RemoveAt(0);
            }
            else if (index == Model.Points.Count)
            {
                _segments.RemoveAt(_segments.Count - 1);
            }
            else
            {
                CurveSegmentViewModel previousSegment = _segments[index - 1];
                CurveSegmentViewModel nextSegment = _segments[index];
                CurveSegmentViewModel newSegment = new CurveSegmentViewModel(previousSegment.StartPoint, nextSegment.EndPoint, this);

                _segments.Remove(previousSegment);
                _segments.Remove(nextSegment);
                _segments.Insert(index - 1, newSegment);
            }
            */
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <param name="point"></param>
        private void PointReplaced(int index, CurvePointViewModel point)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oldIndex"></param>
        /// <param name="newIndex"></param>
        private void PointMoved(int oldIndex, int newIndex)
        {
            /*
            CurveSegmentViewModel previousSegment = (oldIndex == 0 ? null : _segments[oldIndex]);
            CurveSegmentViewModel nextSegment = (oldIndex == Model.Points.Count - 1 ? null : _segments[oldIndex]);
            */
        }

        /// <summary>
        /// 
        /// </summary>
        private void PointsReset()
        {
            // Update the segment list.
            _pathFigure.Segments.Clear();

            if (Model.Points.Any())
            {
                _pathFigure.StartPoint = new System.Windows.Point(Model.Points[0].PositionX, Model.Points[0].PositionY);

                for (int i = 1; i < Model.Points.Count; ++i)
                {
                    Point currentPoint = Model.Points[i];
                    Point previousPoint = Model.Points[i - 1];

                    if (currentPoint.InTangent.Mode == TangentMode.Linear)
                    {
                        LineSegment segment = new LineSegment(new System.Windows.Point(currentPoint.PositionX, currentPoint.PositionY), true);
                        segment.IsSmoothJoin = true;
                        _pathFigure.Segments.Add(segment);
                    }
                    else if (currentPoint.InTangent.Mode == TangentMode.Constant)
                    {
                        System.Windows.Point[] points = new System.Windows.Point[2];
                        points[0] = new System.Windows.Point(currentPoint.PositionX, previousPoint.PositionY);
                        points[1] = new System.Windows.Point(currentPoint.PositionX, currentPoint.PositionY);

                        PolyLineSegment segment = new PolyLineSegment(points, true);
                        segment.IsSmoothJoin = true;
                        _pathFigure.Segments.Add(segment);
                    }
                }
            }
        }

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void Widget_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "ReadOnly")
            {
                NotifyPropertyChanged("IsEnabled");
            }
            NotifyPropertyChanged(e.PropertyName);
        }
        #endregion // Event Handlers

        #region Classes
        /// <summary>
        /// A collection class specifically for the use of the Conversation View Model for its
        /// line collection.
        /// </summary>
        public class PointCollection
            : CollectionConverter<Point, CurvePointViewModel>,
            IReadOnlyViewModelCollection<CurvePointViewModel>
        {
            #region Fields
            /// <summary>
            /// The conversation view model that owns this collection.
            /// </summary>
            private CurveViewModel _owner;

            /// <summary>
            /// The generic object that provides a synchronisation lock for this collection.
            /// </summary>
            private object _lock = new object();
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="LineCollection"/> class.
            /// </summary>
            /// <param name="source">
            /// The source model items for this collection.
            /// </param>
            public PointCollection(
                ReadOnlyModelCollection<Point> source, CurveViewModel owner)
                : base(source, null)
            {
                this._owner = owner;

                System.Windows.Application app = System.Windows.Application.Current;
                if (app != null && app.Dispatcher != null)
                {
                    app.Dispatcher.BeginInvoke(
                    (Action)(() =>
                    {
                        BindingOperations.EnableCollectionSynchronization(this, this._lock);
                    }));
                }
            }
            #endregion // Constructor(s)

            #region Methods
            /// <summary>
            /// Converts the single specified line to a new instance of the line view model
            /// class.
            /// </summary>
            /// <param name="item">
            /// The item to convert.
            /// </param>
            /// <returns>
            /// A new instance of the line view model class created from the specified line.
            /// </returns>
            protected override CurvePointViewModel ConvertItem(Point item)
            {
                CurvePointViewModel newViewModel = new CurvePointViewModel(item, this._owner);
                return newViewModel;
            }
            #endregion // Methods
        }
        #endregion // Classes
    } // CurveViewModel
}
