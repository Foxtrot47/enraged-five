﻿using RSG.Rag.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.ViewModel.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    public class TitleViewModel : WidgetViewModel<WidgetTitle>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        public TitleViewModel(WidgetTitle widget)
            : base(widget)
        {
        }
        #endregion // Constructor(s)
    } // TitleViewModel
}
