﻿//---------------------------------------------------------------------------------------------
// <copyright file="MenuPopupPositionerExtension.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Extensions
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Interop;
    using System.Windows.Markup;
    using RSG.Editor.Controls.Helpers;
    using RSG.Interop.Microsoft.Windows;

    /// <summary>
    /// A XAML mark-up extension that is used to place a simple border on a menu popup bar to
    /// make it look more pleasing by positioning it over the top inner border.
    /// </summary>
    public class MenuPopupPositionerExtension : MarkupExtension
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="ElementName"/> property.
        /// </summary>
        private string _elementName;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MenuPopupPositionerExtension"/> class.
        /// </summary>
        public MenuPopupPositionerExtension()
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="MenuPopupPositionerExtension"/> class.
        /// </summary>
        /// <param name="elementName">
        /// The name of the element the popup control will be associated with.
        /// </param>
        public MenuPopupPositionerExtension(string elementName)
        {
            this._elementName = elementName;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the name of the element that the popup control is associated with.
        /// </summary>
        [ConstructorArgument("_elementName")]
        public string ElementName
        {
            get { return this._elementName; }
            set { this._elementName = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Returns an object that is set as the value of the target property for this
        /// mark-up extension.
        /// </summary>
        /// <param name="serviceProvider">
        /// Object that can provide services for the mark-up extension.
        /// </param>
        /// <returns>
        /// The object value to set on the property where the extension is applied.
        /// </returns>
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (serviceProvider == null)
            {
                return this;
            }

            object service = serviceProvider.GetService(typeof(IProvideValueTarget));
            IProvideValueTarget provideValueTarget = service as IProvideValueTarget;
            if (provideValueTarget == null)
            {
                return this;
            }

            Border border = provideValueTarget.TargetObject as Border;
            if (border == null)
            {
                return this;
            }

            Popup popup = border.GetLogicalAncestor<Popup>(false);
            if (popup != null)
            {
                popup.Opened += this.OnPopupOpened;
            }

            return new Thickness(1.0);
        }

        /// <summary>
        /// Gets called when the associated popup control is opened.
        /// </summary>
        /// <param name="sender">
        /// The object that this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.EventArgs data for this event.
        /// </param>
        private void OnPopupOpened(object sender, EventArgs e)
        {
            Popup popup = sender as Popup;
            if (popup == null)
            {
                return;
            }

            Border border = null;
            foreach (Border childBorder in popup.GetLogicalDescendents<Border>())
            {
                if (childBorder.Height == 2.0)
                {
                    border = childBorder;
                    break;
                }
            }

            if (border == null)
            {
                return;
            }

            object element = border.FindName(this.ElementName);
            FrameworkElement frameworkElement = element as FrameworkElement;
            if (frameworkElement == null)
            {
                return;
            }

            Point point = frameworkElement.PointToScreen(new Point(0.0, 0.0));
            HwndSource hwndSource = (HwndSource)PresentationSource.FromVisual(popup.Child);
            Rect rect = User32.GetWindowRect(hwndSource.Handle);
            if (popup.Placement == PlacementMode.Left)
            {
                border.Visibility = Visibility.Collapsed;
                if (rect.Left > point.X)
                {
                    popup.HorizontalOffset = 2.0;
                }

                return;
            }
            else if (popup.Placement == PlacementMode.Right)
            {
                border.Visibility = Visibility.Collapsed;
                if (rect.Left < point.X)
                {
                    popup.HorizontalOffset = -2.0;
                }

                return;
            }
            else
            {
                if (point.Y > rect.Top)
                {
                    border.Visibility = Visibility.Hidden;
                    return;
                }

                double scale = DpiHelper.DeviceToLogicalScalingFactorX;
                double left = 1.0 + ((point.X - rect.Left) * scale);
                double val = Math.Max(0.0, (rect.Left + rect.Width - point.X) * scale);

                border.Margin = new Thickness(left, 0.0, 0.0, 0.0);
                border.Width = Math.Min(frameworkElement.ActualWidth - 2.0, val);
                border.Visibility = Visibility.Visible;
                return;
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.Extensions.MenuPopupPositionerExtension {Class}
} // RSG.Editor.Controls.Extensions {Namespace}
