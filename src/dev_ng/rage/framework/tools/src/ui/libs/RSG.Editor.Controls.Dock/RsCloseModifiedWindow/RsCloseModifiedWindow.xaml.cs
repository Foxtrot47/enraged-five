﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsCloseModifiedWindow.xaml.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock
{
    using System;
    using System.Collections.Generic;
    using System.Windows;

    /// <summary>
    /// Interaction logic for the <see cref="RsCloseModifiedWindow"/> class.
    /// </summary>
    public partial class RsCloseModifiedWindow : RsWindow
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="CloseModifiedItems"/> property.
        /// </summary>
        private List<CloseModifiedItemViewModel> _closeModifiedItems;

        /// <summary>
        /// A private value indicating whether the select all modifier is currently being
        /// processed.
        /// </summary>
        private bool _selectingAllModifying;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="RsCloseModifiedWindow"/> class.
        /// </summary>
        public RsCloseModifiedWindow()
        {
            this.InitializeComponent();
            Application application = Application.Current;
            if (application != null)
            {
                Window mainWindow = application.MainWindow;
                if (mainWindow != null)
                {
                    this.Owner = mainWindow;
                }
            }

            this.SelectAllCheckBox.Checked += this.SelectAllStateChanged;
            this.SelectAllCheckBox.Unchecked += this.SelectAllStateChanged;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the list of close modified items that should be displayed to the user.
        /// </summary>
        public List<CloseModifiedItemViewModel> CloseModifiedItems
        {
            get
            {
                return this._closeModifiedItems;
            }

            set
            {
                this._closeModifiedItems = value;
                if (value.Count > 0)
                {
                    this.SelectAllCheckBox.IsChecked = true;
                }
                else
                {
                    this.SelectAllCheckBox.IsChecked = false;
                }
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Retrieves a value indicating whether the specified item was marked for save or not.
        /// </summary>
        /// <param name="saveable">
        /// The item to test.
        /// </param>
        /// <returns>
        /// True if the specified item was marked for save; otherwise, false.
        /// </returns>
        public bool MarkForSave(ISaveable saveable)
        {
            foreach (CloseModifiedItemViewModel vm in this._closeModifiedItems)
            {
                if (Object.ReferenceEquals(vm.SaveableItem, saveable))
                {
                    return vm.Save;
                }
            }

            return false;
        }

        /// <summary>
        /// Sets the dialog result to true after the continue button is pressed.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private void ContinueButtonClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        /// <summary>
        /// Sets the dialog result to false after the cancel button is pressed.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private void CancelButtonClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        /// <summary>
        /// Sets the dialog result to true after the don't save button is pressed and all of
        /// the bound items have been set to don't save.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private void DontSaveButtonClick(object sender, RoutedEventArgs e)
        {
            foreach (CloseModifiedItemViewModel vm in this._closeModifiedItems)
            {
                if (vm == null)
                {
                    continue;
                }

                vm.Save = false;
            }

            this.DialogResult = true;
        }

        /// <summary>
        /// Called whenever the master select all check box is either checked or unchecked.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private void SelectAllStateChanged(object sender, RoutedEventArgs e)
        {
            if (this._closeModifiedItems == null)
            {
                return;
            }

            try
            {
                this._selectingAllModifying = true;
                if (this._closeModifiedItems != null)
                {
                    foreach (CloseModifiedItemViewModel vm in this._closeModifiedItems)
                    {
                        if (vm == null)
                        {
                            continue;
                        }

                        vm.Save = (bool)this.SelectAllCheckBox.IsChecked;
                    }
                }
            }
            finally
            {
                this._selectingAllModifying = false;
            }
        }

        /// <summary>
        /// Called whenever a individual items check box is either checked or unchecked.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private void CheckBoxStateChanged(object sender, RoutedEventArgs e)
        {
            if (this._selectingAllModifying)
            {
                return;
            }

            bool containsFalse = false;
            bool containsTrue = false;
            foreach (CloseModifiedItemViewModel vm in this._closeModifiedItems)
            {
                if (vm == null)
                {
                    continue;
                }

                if (vm.Save == true && containsFalse)
                {
                    this.SelectAllCheckBox.IsChecked = null;
                    return;
                }

                if (!vm.Save == true && containsTrue)
                {
                    this.SelectAllCheckBox.IsChecked = null;
                    return;
                }

                if (!vm.Save)
                {
                    containsFalse = true;
                }
                else
                {
                    containsTrue = true;
                }
            }

            this.SelectAllCheckBox.IsChecked = !containsFalse;
        }
        #endregion Methods
    } // RSG.Editor.Controls.Dock.RsCloseModifiedWindow {Class}
} // RSG.Editor.Controls.Dock {Namespace}
