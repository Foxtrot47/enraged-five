﻿//---------------------------------------------------------------------------------------------
// <copyright file="AddExistingProjectAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using RSG.Editor;
    using RSG.Interop.Microsoft.Windows;
    using RSG.Project.Model;
    using RSG.Project.ViewModel;
    using RSG.Project.ViewModel.Definitions;

    /// <summary>
    /// Contains the logic for the <see cref="ProjectCommands.AddExistingProject"/> routed
    /// command. This class cannot be inherited.
    /// </summary>
    public sealed class AddExistingProjectAction : ButtonAction<ProjectCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="AddExistingProjectAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public AddExistingProjectAction(ProjectCommandResolver resolver)
            : base(new ParameterResolverDelegate<ProjectCommandArgs>(resolver))
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ProjectCommandArgs args)
        {
            if (args == null || args.CollectionNode == null || !args.CollectionNode.Loaded)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(ProjectCommandArgs args)
        {
            IHierarchyNode parent = args.SelectedNodes.FirstOrDefault();
            ProjectCollectionNode collection = args.CollectionNode;
            if (parent == null || !parent.CanHaveProjectsAdded || args.SelectionCount > 1)
            {
                parent = args.CollectionNode;
            }

            ICommonDialogService dlgService = this.GetService<ICommonDialogService>();
            if (dlgService == null)
            {
                Debug.Assert(false, "Unable to open existing project, a service is missing.");
                return;
            }

            IProjectItemScope scope = parent.Model as IProjectItemScope;
            if (scope == null)
            {
                scope = parent.ProjectItemScope;
                if (scope == null)
                {
                    Debug.Assert(
                        false,
                        "Unable to open existing project, unable to find the correct scope.");
                    return;
                }
            }

            HashSet<string> dialogFilters = new HashSet<string>();
            foreach (ProjectDefinition definition in collection.Definition.ProjectDefinitions)
            {
                dialogFilters.Add(definition.Filter);
            }

            string filter = null;
            if (dialogFilters.Count > 1)
            {
                filter = "All Project Files (*.*)|*.*|";
            }

            filter += String.Join("|", dialogFilters);
            string fullPath = null;
            string directory = scope.DirectoryPath;
            if (!dlgService.ShowOpenFile(directory, null, filter, 0, out fullPath))
            {
                throw new OperationCanceledException();
            }

            string relativePath = Shlwapi.MakeRelativeFromDirectoryToFile(directory, fullPath);
            if (relativePath == String.Empty)
            {
                relativePath = fullPath;
            }

            ProjectItem existing = scope.Find(relativePath);
            if (existing != null)
            {
                if (args.NodeSelectionDelegate != null)
                {
                    foreach (ProjectNode project in collection.GetProjects())
                    {
                        if (String.Equals(project.Include, relativePath))
                        {
                            args.NodeSelectionDelegate(project);
                            break;
                        }
                    }
                }

                return;
            }

            string extension = Path.GetExtension(fullPath);
            ProjectDefinition selectedDefinition = null;
            StringComparison comparisonType = StringComparison.OrdinalIgnoreCase;
            foreach (ProjectDefinition definition in collection.Definition.ProjectDefinitions)
            {
                if (String.Equals(definition.Extension, extension, comparisonType))
                {
                    selectedDefinition = definition;
                    break;
                }
            }

            if (selectedDefinition == null)
            {
                Debug.Assert(
                    false,
                    "Failed to find project definition for the extension",
                    "{0}",
                    extension);
                return;
            }

            ProjectItem newItem = scope.AddNewProjectItem("Project", relativePath);
            if (!(parent is ProjectCollectionNode))
            {
                newItem.SetMetadata("Filter", parent.Include, MetadataLevel.Core);
            }

            ProjectNode projectNode = selectedDefinition.CreateProjectNode();
            projectNode.SetParentAndModel(parent, newItem);
            parent.AddChild(projectNode);
            projectNode.ModifiedScopeNode.IsModified = true;
            if (args.NodeSelectionDelegate != null)
            {
                args.NodeSelectionDelegate(projectNode);
            }

            projectNode.Load();
        }
        #endregion Methods
    } // RSG.Project.Commands.CreateNewProjectAction {Class}
} // RSG.Project.Commands {Namespace}
