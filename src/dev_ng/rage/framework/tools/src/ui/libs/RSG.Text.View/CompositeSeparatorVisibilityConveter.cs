﻿//---------------------------------------------------------------------------------------------
// <copyright file="CompositeSeparatorVisibilityConveter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.View
{
    using System.Globalization;
    using System.Windows;
    using RSG.Editor.Controls.Converters;

    /// <summary>
    /// Converters n-number of visibility values to a single visibility value. Collapsed if all
    /// source visibility values are equal to collapsed; otherwise, visible.
    /// </summary>
    public class CompositeSeparatorVisibilityConveter
        : MultiValueConverter<Visibility, Visibility>
    {
        #region Methods
        /// <summary>
        /// Converters the specified source values to a single target value.
        /// </summary>
        /// <param name="values">
        /// The pre-converted source values.
        /// </param>
        /// <param name="param">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A target value created by converting the two specified source values.
        /// </returns>
        protected override Visibility Convert(
            Visibility[] values, object param, CultureInfo culture)
        {
            foreach (Visibility value in values)
            {
                if (value == Visibility.Collapsed)
                {
                    return Visibility.Visible;
                }
            }

            return Visibility.Collapsed;
        }
        #endregion Methods
    } // RSG.Text.View.CompositeSeparatorVisibilityConveter {Class}
} // RSG.Text.View {Namespace}
