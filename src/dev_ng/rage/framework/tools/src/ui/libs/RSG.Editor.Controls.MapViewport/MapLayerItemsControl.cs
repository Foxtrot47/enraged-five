﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace RSG.Editor.Controls.MapViewport
{
    /// <summary>
    /// A custom control for displaying the <see cref="MapViewport"/> map layers
    /// in a secondary items control.
    /// </summary>
    internal class MapLayerItemsControl : ItemsControl
    {
        #region Methods
        /// <summary>
        /// Creates or identifies the element used to display the child items.
        /// </summary>
        /// <returns>
        /// A new <see cref="RSG.Editor.Controls.MapViewport.MapLayer" />.
        /// </returns>
        protected override DependencyObject GetContainerForItemOverride()
        {
            return new ContentControl();
        }

        /// <summary>
        /// Determines whether the specified item is, or is eligible to be, its own item
        /// container.
        /// </summary>
        /// <param name="item">
        /// The item to check whether it is an item container.
        /// </param>
        /// <returns>
        /// True if the item is eligible to be its own item container; otherwise, false.
        /// </returns>
        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is ContentControl;
        }
        #endregion
    }
}
