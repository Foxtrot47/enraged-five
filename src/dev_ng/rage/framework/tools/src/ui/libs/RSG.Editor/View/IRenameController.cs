﻿//---------------------------------------------------------------------------------------------
// <copyright file="IRenameController.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    using System;

    /// <summary>
    /// When implemented represents a class that can be used to rename a single item.
    /// </summary>
    public interface IRenameController
    {
        #region Events
        /// <summary>
        /// Occurrs when the renaming operation has finished. This doesn't indicate success
        /// just that the operation has finished.
        /// </summary>
        event EventHandler<RenameControllerCompletedEventArgs> Completed;
        #endregion Events

        #region Properties
        /// <summary>
        /// Gets the data context for the item that is being renamed. This is the object that
        /// comes from the view and is used to both scroll to the item and find the correct
        /// view object to put into its edit mode.
        /// </summary>
        object ContainerDataContext { get; }

        /// <summary>
        /// Gets a value indicating whether the controller is in a incomplete state. (i.e
        /// the cancel or commit methods hasn't been called).
        /// </summary>
        bool IsIncomplete { get; }

        /// <summary>
        /// Gets the item this controller is renaming.
        /// </summary>
        IRenameable Item { get; }

        /// <summary>
        /// Gets or sets the text that is currently being edited.
        /// </summary>
        string Text { get; set; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Cancels the renaming operation.
        /// </summary>
        /// <param name="refocus">
        /// A value indicating whether the item being renamed should be refocused. This is true
        /// if the renaming was cancelled for any reason other than the user clicking
        /// elsewhere.
        /// </param>
        void Cancel(bool refocus);

        /// <summary>
        /// Commits the renaming operation to validation.
        /// </summary>
        /// <param name="refocus">
        /// A value indicating whether the item being renamed should be refocused. This is true
        /// if the renaming was commit for any reason other than the user clicking elsewhere.
        /// </param>
        void Commit(bool refocus);
        #endregion Methods
    } // RSG.Editor.View.IRenameController {Interface}
} // RSG.Editor.View {Namespace}
