﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsLoginWindow.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Static utility class used to show a login window to the user.
    /// </summary>
    public static class RsLoginWindow
    {
        #region Methods
        /// <summary>
        /// Shows the login window using the specified parameters and returns a value
        /// indicating whether the login was successful based on the specified delegate.
        /// </summary>
        /// <param name="title">
        /// The title for the login window.
        /// </param>
        /// <param name="loginDelegate">
        /// The delegate that is called when the user attempts to login.
        /// </param>
        /// <returns>
        /// True if the user has successfully logged in; otherwise, false.
        /// </returns>
        public static bool Show(string title, LoginAttemptDelegate loginDelegate)
        {
            return Show(null, null, String.Empty, String.Empty, title, loginDelegate);
        }

        /// <summary>
        /// Shows the login window using the specified parameters and returns a value
        /// indicating whether the login was successful based on the specified delegate.
        /// </summary>
        /// <param name="icon">
        /// The image source to use for the icon.
        /// </param>
        /// <param name="title">
        /// The title for the login window.
        /// </param>
        /// <param name="loginDelegate">
        /// The delegate that is called when the user attempts to login.
        /// </param>
        /// <returns>
        /// True if the user has successfully logged in; otherwise, false.
        /// </returns>
        public static bool Show(
            ImageSource icon, string title, LoginAttemptDelegate loginDelegate)
        {
            return Show(null, icon, String.Empty, String.Empty, title, loginDelegate);
        }

        /// <summary>
        /// Shows the login window using the specified parameters and returns a value
        /// indicating whether the login was successful based on the specified delegate.
        /// </summary>
        /// <param name="owner">
        /// The window that will own the login window that is shown.
        /// </param>
        /// <param name="title">
        /// The title for the login window.
        /// </param>
        /// <param name="loginDelegate">
        /// The delegate that is called when the user attempts to login.
        /// </param>
        /// <returns>
        /// True if the user has successfully logged in; otherwise, false.
        /// </returns>
        public static bool Show(
            Window owner,
            string title,
            LoginAttemptDelegate loginDelegate)
        {
            return Show(owner, null, String.Empty, String.Empty, title, loginDelegate);
        }

        /// <summary>
        /// Shows the login window using the specified parameters and returns a value
        /// indicating whether the login was successful based on the specified delegate.
        /// </summary>
        /// <param name="owner">
        /// The window that will own the login window that is shown.
        /// </param>
        /// <param name="icon">
        /// The image source to use for the icon.
        /// </param>
        /// <param name="title">
        /// The title for the login window.
        /// </param>
        /// <param name="loginDelegate">
        /// The delegate that is called when the user attempts to login.
        /// </param>
        /// <returns>
        /// True if the user has successfully logged in; otherwise, false.
        /// </returns>
        public static bool Show(
            Window owner,
            ImageSource icon,
            string title,
            LoginAttemptDelegate loginDelegate)
        {
            return Show(owner, icon, String.Empty, String.Empty, title, loginDelegate);
        }

        /// <summary>
        /// Shows the login window using the specified parameters and returns a value
        /// indicating whether the login was successful based on the specified delegate.
        /// </summary>
        /// <param name="username">
        /// The initial username to display.
        /// </param>
        /// <param name="password">
        /// The initial password to set.
        /// </param>
        /// <param name="title">
        /// The title for the login window.
        /// </param>
        /// <param name="loginDelegate">
        /// The delegate that is called when the user attempts to login.
        /// </param>
        /// <returns>
        /// True if the user has successfully logged in; otherwise, false.
        /// </returns>
        public static bool Show(
            string username,
            string password,
            string title,
            LoginAttemptDelegate loginDelegate)
        {
            return Show(null, null, username, password, title, loginDelegate);
        }

        /// <summary>
        /// Shows the login window using the specified parameters and returns a value
        /// indicating whether the login was successful based on the specified delegate.
        /// </summary>
        /// <param name="icon">
        /// The image source to use for the icon.
        /// </param>
        /// <param name="username">
        /// The initial username to display.
        /// </param>
        /// <param name="password">
        /// The initial password to set.
        /// </param>
        /// <param name="title">
        /// The title for the login window.
        /// </param>
        /// <param name="loginDelegate">
        /// The delegate that is called when the user attempts to login.
        /// </param>
        /// <returns>
        /// True if the user has successfully logged in; otherwise, false.
        /// </returns>
        public static bool Show(
            ImageSource icon,
            string username,
            string password,
            string title,
            LoginAttemptDelegate loginDelegate)
        {
            return Show(null, icon, username, password, title, loginDelegate);
        }

        /// <summary>
        /// Shows the login window using the specified parameters and returns a value
        /// indicating whether the login was successful based on the specified delegate.
        /// </summary>
        /// <param name="owner">
        /// The window that will own the login window that is shown.
        /// </param>
        /// <param name="icon">
        /// The image source to use for the icon.
        /// </param>
        /// <param name="username">
        /// The initial username to display.
        /// </param>
        /// <param name="password">
        /// The initial password to set.
        /// </param>
        /// <param name="title">
        /// The title for the login window.
        /// </param>
        /// <param name="loginDelegate">
        /// The delegate that is called when the user attempts to login.
        /// </param>
        /// <returns>
        /// True if the user has successfully logged in; otherwise, false.
        /// </returns>
        public static bool Show(
            Window owner,
            ImageSource icon,
            string username,
            string password,
            string title,
            LoginAttemptDelegate loginDelegate)
        {
            RsLoginInternalWindow window = new RsLoginInternalWindow();
            window.Title = title;
            window.SetLoginDelegate(loginDelegate);
            window.SetIcon(icon, 32, 32);
            window.SetUsername(username);
            window.SetPassword(password);
            window.Owner = owner;
            window.ShowHelpButton = true;
            bool? result = window.ShowDialog();
            return (bool)result;
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsLoginWindow {Class}
} // RSG.Editor.Controls {Namespace}
