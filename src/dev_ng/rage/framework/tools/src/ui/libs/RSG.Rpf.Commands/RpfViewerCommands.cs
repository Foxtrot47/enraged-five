﻿//---------------------------------------------------------------------------------------------
// <copyright file="RpfViewerCommands.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Rpf.Commands
{
    using System;
    using System.Diagnostics;
    using System.Windows.Input;
    using System.Windows.Media.Imaging;
    using RSG.Editor;
    using RSG.Rpf.Commands.Resources;

    /// <summary>
    /// Defines core commands for the RPF Viewer control. This class cannot be inherited.
    /// </summary>
    public sealed class RpfViewerCommands
    {
        #region Fields
        /// <summary>
        /// The private cache of command objects.
        /// </summary>
        private static RockstarRoutedCommand[] _internalCommands = InitialiseInternalStorage();
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Prevents a default instance of the <see cref="RpfViewerCommands"/> class from being
        /// created.
        /// </summary>
        private RpfViewerCommands()
        {
        }
        #endregion Constructors

        #region Enumerations
        /// <summary>
        /// Defines all of the command identifiers for the different core Rockstar commands.
        /// </summary>
        private enum Id : byte
        {
            /// <summary>
            /// Used to identifier the <see cref="RpfViewerCommands.ExtractEntireRpf"/> routed
            /// command.
            /// </summary>
            ExtractEntireRpf,

            /// <summary>
            /// Used to identifier the <see cref="RpfViewerCommands.ExtractRpfItems"/> routed
            /// command.
            /// </summary>
            ExtractRpfItems,

            /// <summary>
            /// Used to identifier the <see cref="RpfViewerCommands.FilterRpfByCategory"/>
            /// routed command.
            /// </summary>
            FilterRpfByCategory,

            /// <summary>
            /// Used to identifier the <see cref="RpfViewerCommands.SearchRpf"/>
            /// routed command.
            /// </summary>
            SearchRpf,

            /// <summary>
            /// Used to identifier the <see cref="RpfViewerCommands.ViewRpfItem"/> routed
            /// command.
            /// </summary>
            ViewRpfItem,

            /// <summary>
            /// Used to identify the <see cref="RpfViewerCommands.CopyFileName"/> routed command.
            /// </summary>
            CopyFileName,

            /// <summary>
            /// Defines the number of commands defined as RPF viewer commands.
            /// </summary>
            CommandCount
        } // RpfViewerCommands.Id {Enum}

        /// <summary>
        /// Defines the different string values a command has that can be retrieved from the
        /// string table.
        /// </summary>
        private enum StringValue : byte
        {
            /// <summary>
            /// Defines the category string value located in the string table as
            /// {id}Category.
            /// </summary>
            Category,

            /// <summary>
            /// Defines the description string value located in the string table as
            /// {id}Description.
            /// </summary>
            Description,

            /// <summary>
            /// Defines the description string value located in the string table as
            /// {id}KeyGestures.
            /// </summary>
            KeyGestures,

            /// <summary>
            /// Defines the name string value located in the string table as
            /// {id}Name.
            /// </summary>
            Name,
        } // RpfViewerCommands.StringValue {Enum}
        #endregion Enumerations

        #region Properties
        /// <summary>
        /// Gets the value that represents the Extract Entire RPF command.
        /// </summary>
        public static RockstarRoutedCommand ExtractEntireRpf
        {
            get { return EnsureCommandExists(Id.ExtractEntireRpf, RpfViewerIcons.ExtractAll); }
        }

        /// <summary>
        /// Gets the value that represents the Extract RPF Items command.
        /// </summary>
        public static RockstarRoutedCommand ExtractRpfItems
        {
            get { return EnsureCommandExists(Id.ExtractRpfItems, RpfViewerIcons.Extract); }
        }

        /// <summary>
        /// Gets the value that represents the Filter RPF By Category command.
        /// </summary>
        public static RockstarRoutedCommand FilterRpfByCategory
        {
            get { return EnsureCommandExists(Id.FilterRpfByCategory); }
        }

        /// <summary>
        /// Gets the value that represents the Search RPF command.
        /// </summary>
        public static RockstarRoutedCommand SearchRpf
        {
            get { return EnsureCommandExists(Id.SearchRpf, CommonIcons.Search); }
        }

        /// <summary>
        /// Gets the value that represents the View RPF Item command.
        /// </summary>
        public static RockstarRoutedCommand ViewRpfItem
        {
            get { return EnsureCommandExists(Id.ViewRpfItem, CommonIcons.ViewDocument); }
        }

        /// <summary>
        /// Gets the value that represents the Copy File Path command.
        /// </summary>
        public static RockstarRoutedCommand CopyFileName
        {
            get { return EnsureCommandExists(Id.CopyFileName); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="RSG.Editor.RockstarRoutedCommand"/> object that is
        /// associated with the specified identifier.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command to create.
        /// </param>
        /// <param name="icon">
        /// The icon that the created command should be using.
        /// </param>
        /// <returns>
        /// The newly created <see cref="RSG.Editor.RockstarRoutedCommand"/> object.
        /// </returns>
        private static RockstarRoutedCommand CreateCommand(string id, BitmapSource icon)
        {
            string name = GetStringValue(id, StringValue.Name);
            string category = GetStringValue(id, StringValue.Category);
            string description = GetStringValue(id, StringValue.Description);

            InputGestureCollection gestures = GetGestures(id);
            return new RockstarRoutedCommand(
                name, typeof(RpfViewerCommands), gestures, category, description, icon);
        }

        /// <summary>
        /// Makes sure the internal value for the command associated with the specified
        /// identifier exists and returns it.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command that should be created and returned.
        /// </param>
        /// <returns>
        /// The <see cref="RSG.Editor.RockstarRoutedCommand"/> object associated with the
        /// specified identifier.
        /// </returns>
        private static RockstarRoutedCommand EnsureCommandExists(Id id)
        {
            return EnsureCommandExists(id, null);
        }

        /// <summary>
        /// Makes sure the internal value for the command associated with the specified
        /// identifier exists and returns it.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command that should be created and returned.
        /// </param>
        /// <param name="icon">
        /// The icon that the command should be using.
        /// </param>
        /// <returns>
        /// The <see cref="RSG.Editor.RockstarRoutedCommand"/> object associated with the
        /// specified identifier.
        /// </returns>
        private static RockstarRoutedCommand EnsureCommandExists(Id id, BitmapSource icon)
        {
            int commandIndex = (int)id;
            if (commandIndex < -1 || commandIndex >= _internalCommands.Length)
            {
                return null;
            }

            RockstarRoutedCommand command = _internalCommands[commandIndex];
            if (command == null)
            {
                lock (_internalCommands)
                {
                    if (command == null)
                    {
                        command = CreateCommand(id.ToString(), icon);
                        _internalCommands[commandIndex] = command;
                    }
                }
            }

            return command;
        }

        /// <summary>
        /// Creates a System.Windows.InputGestureCollection object that contains the default
        /// input gestures for the command with the specified identifier.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command whose default gestures should be created.
        /// </param>
        /// <returns>
        /// A System.Windows.InputGestureCollection object that contains the default input
        /// gestures for the command with the specified identifier.
        /// </returns>
        private static InputGestureCollection GetGestures(string id)
        {
            InputGestureCollection inputGestureCollection = new InputGestureCollection();
            string keyGestures = GetStringValue(id, StringValue.KeyGestures);
            if (String.IsNullOrWhiteSpace(keyGestures))
            {
                return inputGestureCollection;
            }

            KeyGestureConverter converter = new KeyGestureConverter();
            while (!String.IsNullOrEmpty(keyGestures))
            {
                int num = keyGestures.IndexOf(";", StringComparison.Ordinal);
                string token;
                if (num >= 0)
                {
                    token = keyGestures.Substring(0, num);
                    keyGestures = keyGestures.Substring(num + 1);
                }
                else
                {
                    token = keyGestures;
                    keyGestures = String.Empty;
                }

                KeyGesture gesture = null;

                try
                {
                    gesture = converter.ConvertFromInvariantString(token) as KeyGesture;
                }
                catch (NotSupportedException)
                {
                }

                if (gesture != null)
                {
                    inputGestureCollection.Add(gesture);
                }
            }

            return inputGestureCollection;
        }

        /// <summary>
        /// Retrieves the specified string value for the command with the specified id from
        /// the command string table.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command whose string value should be retrieved.
        /// </param>
        /// <param name="value">
        /// Specifies which string value to retrieve.
        /// </param>
        /// <returns>
        /// The specified string value for the command with the specified id.
        /// </returns>
        private static string GetStringValue(string id, StringValue value)
        {
            string tableId = id;
            switch (value)
            {
                case StringValue.Category:
                    tableId += "Category";
                    break;
                case StringValue.Description:
                    tableId += "Description";
                    break;
                case StringValue.Name:
                    tableId += "Name";
                    break;
                case StringValue.KeyGestures:
                    tableId += "KeyGestures";
                    break;
                default:
                    tableId = null;
                    Debug.Assert(false, "Unknown string value specified.");
                    break;
            }

            if (tableId == null)
            {
                return String.Empty;
            }

            return CommandStringTable.GetString(tableId);
        }

        /// <summary>
        /// Initialises the array used to store the
        /// <see cref="RSG.Editor.RockstarRoutedCommand"/> objects that make up the core
        /// commands for a Rockstar application.
        /// </summary>
        /// <returns>
        /// The array used to store the internal command objects.
        /// </returns>
        private static RockstarRoutedCommand[] InitialiseInternalStorage()
        {
            return new RockstarRoutedCommand[(int)Id.CommandCount];
        }
        #endregion Methods
    } // RSG.Rpf.Commands.RpfViewerCommands {Class}
} // RSG.Rpf.Commands {Namespace}
