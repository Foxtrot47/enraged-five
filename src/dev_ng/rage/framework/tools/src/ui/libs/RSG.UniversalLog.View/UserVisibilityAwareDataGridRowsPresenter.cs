﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using RSG.Editor.Controls.Helpers;

namespace RSG.UniversalLog.View
{
    /// <summary>
    /// 
    /// </summary>
    public class UserVisibilityAwareDataGridRowsPresenter : DataGridRowsPresenter
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public UserVisibilityAwareDataGridRowsPresenter()
            : base()
        {
            _partiallyVisibleChildren = new List<UIElement>();
            _fullyVisibleChildren = new List<UIElement>();
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public IList<UIElement> PartiallyVisibleChildren
        {
            get { return _partiallyVisibleChildren; }
        }
        private IList<UIElement> _partiallyVisibleChildren;

        /// <summary>
        /// 
        /// </summary>
        public IList<UIElement> FullyVisibleChildren
        {
            get { return _fullyVisibleChildren; }
        }
        private IList<UIElement> _fullyVisibleChildren;
        #endregion // Properties

        #region ArrangeOverride
        /// <summary>
        /// 
        /// </summary>
        /// <param name="arrangeSize"></param>
        /// <returns></returns>
        protected override Size ArrangeOverride(Size arrangeSize)
        {
            Size size = base.ArrangeOverride(arrangeSize);
            _partiallyVisibleChildren.Clear();
            _fullyVisibleChildren.Clear();

            // Get the scroll presenter parent.
            ScrollContentPresenter scrollPresenter = this.GetVisualAncestor<ScrollContentPresenter>();
            if (scrollPresenter != null)
            {
                Rect rect = new Rect(0.0, 0.0, scrollPresenter.ActualWidth, scrollPresenter.ActualHeight);

                foreach (FrameworkElement child in InternalChildren)
                {
                    if (child != null)
                    {
                        Rect bounds = child.TransformToAncestor(scrollPresenter).TransformBounds(new Rect(0.0, 0.0, child.ActualWidth, child.ActualHeight));
                        bool topLeftVisible = rect.Contains(bounds.TopLeft);
                        bool bottomRightVisible = rect.Contains(bounds.BottomRight);

                        if (topLeftVisible && bottomRightVisible)
                        {
                            _fullyVisibleChildren.Add(child);
                        }
                        if (topLeftVisible || bottomRightVisible)
                        {
                            _partiallyVisibleChildren.Add(child);
                        }
                    }
                }
            }

            return size;
        }
        #endregion // ArrangeOverride
    } // UserVisibilityAwareDataGridRowsPresenter
}
