﻿//---------------------------------------------------------------------------------------------
// <copyright file="DialogueViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.IO;
    using System.Linq;
    using System.Windows.Data;
    using System.Xml;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Editor.View;
    using RSG.Project.ViewModel;
    using RSG.Text.Model;

    /// <summary>
    /// The view model the represents a <see cref="RSG.Text.Model.Dialogue"/> class instance.
    /// </summary>
    public class DialogueViewModel : ViewModelBase<Dialogue>, ISaveable
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Characters"/> property.
        /// </summary>
        private CharacterCollection _characters;

        /// <summary>
        /// The private field used for the <see cref="Conversations"/> property.
        /// </summary>
        private ConversationCollection _conversations;

        /// <summary>
        /// 
        /// </summary>
        private double _conversationScrollOffset;

        /// <summary>
        /// 
        /// </summary>
        private double _lineScrollOffset;

        /// <summary>
        /// 
        /// </summary>
        private CharacterMappingsViewModel _characterMappings;

        /// <summary>
        /// 
        /// </summary>
        private object _selectedConversation;

        /// <summary>
        /// 
        /// </summary>
        private object _selectedLine;

        private FileNode _fileNode;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DialogueViewModel"/> class.
        /// </summary>
        /// <param name="dialogue">
        /// The <see cref="RSG.Text.Model.Dialogue"/> instance this view model is wrapping.
        /// </param>
        public DialogueViewModel(Dialogue dialogue)
            : this(dialogue, null)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="DialogueViewModel"/> class.
        /// </summary>
        /// <param name="dialogue">
        /// The <see cref="RSG.Text.Model.Dialogue"/> instance this view model is wrapping.
        /// </param>
        public DialogueViewModel(Dialogue dialogue, FileNode fileNode)
            : base(dialogue, true)
        {
            this._conversations = new ConversationCollection(dialogue.Conversations, this);
            this._conversations.AddCollectionChangedHandler(this.ConversationsChanged);
            this.SelectedConversation = this._conversations.FirstOrDefault();
            this._fileNode = fileNode;
            this._characterMappings = new CharacterMappingsViewModel(this);
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs just after this object has been saved successfully.
        /// </summary>
        public event EventHandler Saved;
        #endregion Events

        #region Properties
        public FileNode FileNode
        {
            get { return this._fileNode; }
            set { this._fileNode = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this dialogue is for ambient speech.
        /// </summary>
        public bool AmbientFile
        {
            get { return this.Model.AmbientFile; }
            set { this.Model.AmbientFile = value; }
        }

        /// <summary>
        /// Gets the character mappings object for this dialogue object.
        /// </summary>
        public CharacterMappingsViewModel CharacterMappings
        {
            get { return this._characterMappings; }
        }

        /// <summary>
        /// Gets the collection of characters that can be used by the lines inside this
        /// dialogue with each character maintaining their used state.
        /// </summary>
        public CharacterCollection Characters
        {
            get
            {
                if (this._characters == null)
                {
                    DialogueConfigurations configurations = this.Model.Configurations;
                    if (configurations == null)
                    {
                        this._characters =
                            new CharacterCollection(new ModelCollection<DialogueCharacter>());
                    }
                    else
                    {
                        this._characters = new CharacterCollection(configurations.Characters);
                    }

                    DispatcherHelper.CheckBeginInvokeOnUI(new Action(() =>
                    {
                        ICollectionView view =
                            CollectionViewSource.GetDefaultView(this._characters);
                        if (view != null)
                        {
                            view.GroupDescriptions.Add(new PropertyGroupDescription("IsUsed"));
                            view.SortDescriptions.Add(
                                new SortDescription("IsUsed", ListSortDirection.Descending));
                            view.SortDescriptions.Add(
                                new SortDescription("Name", ListSortDirection.Ascending));
                        }
                    }));
                }

                return this._characters;
            }
        }

        /// <summary>
        /// Gets the collection of conversations belonging to this dialogue.
        /// </summary>
        public IReadOnlyViewModelCollection<ConversationViewModel> Conversations
        {
            get { return this._conversations; }
        }

        /// <summary>
        /// Gets or sets the short id used to identify the dialogue.
        /// </summary>
        public string Id
        {
            get { return this.Model.Id; }
            set { this.Model.Id = value; }
        }

        /// <summary>
        /// Gets the friendly save path that this item pushes into the command system
        /// to display on menu items.
        /// </summary>
        public string FriendlySavePath
        {
            get { return null; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this dialogue is for a multiplayer mission
        /// or activity.
        /// </summary>
        public bool Multiplayer
        {
            get { return this.Model.Multiplayer; }
            set { this.Model.Multiplayer = value; }
        }

        /// <summary>
        /// Gets or sets a longer version of the id that is used to comment text files.
        /// </summary>
        public string Name
        {
            get { return this.Model.Name; }
            set { this.Model.Name = value; }
        }

        /// <summary>
        /// Gets or sets the subtitle group id for all of the subtitles defined in this
        /// dialogue. This is the short identifier with either AUD or AU appended onto it.
        /// </summary>
        public string SubtitleId
        {
            get { return this.Model.SubtitleId; }
            set { this.Model.SubtitleId = value; }
        }

        /// <summary>
        /// Gets or sets a string indicating the type of dialogue this is.
        /// </summary>
        public string Type
        {
            get { return this.Model.Type; }
            set { this.Model.Type = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public double ConversationScrollOffset
        {
            get { return this._conversationScrollOffset; }
            set { this.SetProperty(ref this._conversationScrollOffset, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public double LineScrollOffset
        {
            get { return this._lineScrollOffset; }
            set { this.SetProperty(ref this._lineScrollOffset, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public object SelectedConversation
        {
            get { return this._selectedConversation; }
            set { this.SetProperty(ref this._selectedConversation, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public object SelectedLine
        {
            get { return this._selectedLine; }
            set { this.SetProperty(ref this._selectedLine, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Raises the saved event on this object.
        /// </summary>
        public void RaiseSavedEvent()
        {
            EventHandler handler = this.Saved;
            if (handler == null)
            {
                return;
            }

            handler(this, EventArgs.Empty);
        }

        /// <summary>
        /// Saves the data that is associated with this item to the specified file.
        /// </summary>
        /// <param name="stream">
        /// The io stream that data for this object should be written to.
        /// </param>
        /// <returns>
        /// A value indicating whether the save operation was successful.
        /// </returns>
        public bool Save(Stream stream)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.ConformanceLevel = ConformanceLevel.Document;
            using (XmlWriter writer = XmlWriter.Create(stream, settings))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Dialogue");
                this.Model.Serialise(writer);
                writer.WriteEndElement();
                writer.WriteEndDocument();
            }

            return true;
        }

        /// <summary>
        /// Called whenever the conversation collection is modified.
        /// </summary>
        /// <param name="s">
        /// The character collection that raised the event.
        /// </param>
        /// <param name="e">
        /// The System.Collections.Specialized.NotifyCollectionChangedEventArgs containing the
        /// event data.
        /// </param>
        private void ConversationsChanged(object s, NotifyCollectionChangedEventArgs e)
        {
            foreach (ConversationViewModel conversation in this._conversations)
            {
                conversation.Validate();
            }
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// A collection class specifically for the use of the Dialogue View Model for its
        /// conversation collection.
        /// </summary>
        public class ConversationCollection
            : CollectionConverter<Conversation, ConversationViewModel>,
            IReadOnlyViewModelCollection<ConversationViewModel>
        {
            #region Fields
            /// <summary>
            /// The dialogue view model that owns this collection.
            /// </summary>
            private DialogueViewModel _owner;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="ConversationCollection"/> class.
            /// </summary>
            /// <param name="source">
            /// The source model items for this collection.
            /// </param>
            public ConversationCollection(
                ReadOnlyModelCollection<Conversation> source, DialogueViewModel owner)
            {
                this._owner = owner;
                this.ResetSourceItems(source);
            }
            #endregion Constructors

            #region Methods
            /// <summary>
            /// Converts the single specified conversation to a new instance of the
            /// conversation view model class.
            /// </summary>
            /// <param name="item">
            /// The item to convert.
            /// </param>
            /// <returns>
            /// A new instance of the conversation view model class created from the specified
            /// conversation.
            /// </returns>
            protected override ConversationViewModel ConvertItem(Conversation item)
            {
                return new ConversationViewModel(item, this._owner);
            }
            #endregion Methods
        } // DialogueViewModel.ConversationCollection {Class}
        #endregion Classes
    } // RSG.Text.ViewModel.DialogueViewModel {Class}
} // RSG.Text.ViewModel {Namespace}
