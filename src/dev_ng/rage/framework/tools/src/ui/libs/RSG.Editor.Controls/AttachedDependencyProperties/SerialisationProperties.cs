﻿//---------------------------------------------------------------------------------------------
// <copyright file="SerialisationProperties.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.AttachedDependencyProperties
{
    using System.Windows;

    /// <summary>
    /// Contains attached properties that are related to the theming of the user-interface.
    /// </summary>
    public static class SerialisationProperties
    {
        #region Fields
        /// <summary>
        /// Identifies the Serialise dependency property.
        /// </summary>
        public static readonly DependencyProperty SerialiseProperty;

        /// <summary>
        /// Identifies the SerialiseUid dependency property.
        /// </summary>
        public static readonly DependencyProperty SerialiseUidProperty;

        /// <summary>
        /// Identifies the SerialiseWidth dependency property.
        /// </summary>
        public static readonly DependencyProperty SerialiseWidthProperty;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="SerialisationProperties"/> class.
        /// </summary>
        static SerialisationProperties()
        {
            SerialiseProperty =
                DependencyProperty.RegisterAttached(
                "Serialise",
                typeof(bool),
                typeof(SerialisationProperties),
                new FrameworkPropertyMetadata(false));

            SerialiseUidProperty =
                DependencyProperty.RegisterAttached(
                "SerialiseUid",
                typeof(string),
                typeof(SerialisationProperties),
                new FrameworkPropertyMetadata(null));

            SerialiseWidthProperty =
                DependencyProperty.RegisterAttached(
                "SerialiseWidth",
                typeof(bool),
                typeof(SerialisationProperties),
                new FrameworkPropertyMetadata(true));
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Retrieves the SerialiseProperty dependency property value attached to the specified
        /// object.
        /// </summary>
        /// <param name="obj">
        /// The object whose attached SerialiseProperty dependency property value will be
        /// returned.
        /// </param>
        /// <returns>
        /// The SerialiseProperty dependency property value attached to the specified object.
        /// </returns>
        public static bool GetSerialise(DependencyObject obj)
        {
            if (obj == null)
            {
                throw new SmartArgumentNullException(() => obj);
            }

            return (bool)obj.GetValue(SerialiseProperty);
        }

        /// <summary>
        /// Retrieves the SerialiseUid dependency property value attached to the specified
        /// object.
        /// </summary>
        /// <param name="obj">
        /// The object whose attached SerialiseUid dependency property value will be returned.
        /// </param>
        /// <returns>
        /// The SerialiseUid dependency property value attached to the specified object.
        /// </returns>
        public static string GetSerialiseUid(DependencyObject obj)
        {
            if (obj == null)
            {
                throw new SmartArgumentNullException(() => obj);
            }

            return (string)obj.GetValue(SerialiseUidProperty);
        }

        /// <summary>
        /// Retrieves the SerialiseWidth dependency property value attached to the specified
        /// object.
        /// </summary>
        /// <param name="obj">
        /// The object whose attached SerialiseWidth dependency property value will be
        /// returned.
        /// </param>
        /// <returns>
        /// The SerialiseWidthProperty dependency property value attached to the specified
        /// object.
        /// </returns>
        public static bool GetSerialiseWidth(DependencyObject obj)
        {
            if (obj == null)
            {
                throw new SmartArgumentNullException(() => obj);
            }

            return (bool)obj.GetValue(SerialiseWidthProperty);
        }

        /// <summary>
        /// Sets the SerialiseProperty dependency property value on the specified object to the
        /// specified value.
        /// </summary>
        /// <param name="obj">
        /// The object whose attached SerialiseProperty dependency property value will be set.
        /// </param>
        /// <param name="value">
        /// The value to set the attached SerialiseProperty dependency property to on the
        /// specified object.
        /// </param>
        public static void SetSerialise(DependencyObject obj, bool value)
        {
            if (obj == null)
            {
                throw new SmartArgumentNullException(() => obj);
            }

            obj.SetValue(SerialiseProperty, value);
        }

        /// <summary>
        /// Sets the SerialiseUid dependency property value on the specified object to the
        /// specified value.
        /// </summary>
        /// <param name="obj">
        /// The object whose attached SerialiseUid dependency property value will be set.
        /// </param>
        /// <param name="value">
        /// The value to set the attached SerialiseUid dependency property to on the specified
        /// object.
        /// </param>
        public static void SetSerialiseUid(DependencyObject obj, string value)
        {
            if (obj == null)
            {
                throw new SmartArgumentNullException(() => obj);
            }

            obj.SetValue(SerialiseUidProperty, value);
        }

        /// <summary>
        /// Sets the SerialiseWidth dependency property value on the specified object to the
        /// specified value.
        /// </summary>
        /// <param name="obj">
        /// The object whose attached SerialiseWidth dependency property value will be set.
        /// </param>
        /// <param name="value">
        /// The value to set the attached SerialiseWidthProperty dependency property to on the
        /// specified object.
        /// </param>
        public static void SetSerialiseWidth(DependencyObject obj, bool value)
        {
            if (obj == null)
            {
                throw new SmartArgumentNullException(() => obj);
            }

            obj.SetValue(SerialiseWidthProperty, value);
        }
        #endregion Methods
    } // RSG.Editor.Controls.AttachedDependencyProperties.SerialisationProperties {Class}
} // RSG.Editor.Controls.AttachedDependencyProperties {Namespace}
