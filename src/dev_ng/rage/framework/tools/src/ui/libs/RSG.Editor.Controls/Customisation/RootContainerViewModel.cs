﻿//---------------------------------------------------------------------------------------------
// <copyright file="RootContainerViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Customisation
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Text.RegularExpressions;
    using RSG.Editor.View;

    /// <summary>
    /// Provides a abstract base class to the view models that are representing the main root
    /// containers.
    /// </summary>
    public abstract class RootContainerViewModel : NotifyPropertyChangedBase
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="FilterItemsSource"/> property.
        /// </summary>
        private FilterCollection _filterCollection;

        /// <summary>
        /// The private field used for the <see cref="SearchExpression"/> property.
        /// </summary>
        private Regex _searchExpression;

        /// <summary>
        /// The private field used for the <see cref="SearchText"/> property.
        /// </summary>
        private string _searchText;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="RootContainerViewModel"/> class.
        /// </summary>
        protected RootContainerViewModel()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the items collection that contains the current filter items.
        /// </summary>
        public FilterCollection FilterItemsSource
        {
            get { return this._filterCollection; }
            set { this.SetProperty(ref this._filterCollection, value); }
        }

        /// <summary>
        /// Gets the header used for this container.
        /// </summary>
        public abstract string Header { get; }

        /// <summary>
        /// Gets or sets the expression that is currently being searched for.
        /// </summary>
        public Regex SearchExpression
        {
            get
            {
                return this._searchExpression;
            }

            set
            {
                if (this.SetProperty(ref this._searchExpression, value))
                {
                    if (value == null)
                    {
                        this.FilterItemsSource = null;
                        return;
                    }

                    this.FilterItemsSource = new FilterCollection(this.GetSearchItems());
                }
            }
        }

        /// <summary>
        /// Gets or sets the text that is currently being searched for.
        /// </summary>
        public string SearchText
        {
            get
            {
                return this._searchText;
            }

            set
            {
                if (this.SetProperty(ref this._searchText, value))
                {
                    if (String.IsNullOrEmpty(value))
                    {
                        this.SearchExpression = null;
                        return;
                    }

                    string pattern = value;
                    RegexOptions options = RegexOptions.None;
                    pattern = Regex.Escape(pattern);
                    options |= RegexOptions.IgnoreCase;
                    this.SearchExpression = new Regex(pattern, options);
                }
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Re-initialises the items in this container. This can be called once the commands
        /// have been reset to make sure the items shown to the user are correctly synced up
        /// against the model.
        /// </summary>
        public abstract void ResetItems();

        /// <summary>
        /// Gets the items that match the current search expression.
        /// </summary>
        /// <returns>
        /// The items that match the current search expression.
        /// </returns>
        protected abstract IEnumerable<object> GetSearchItems();
        #endregion Methods

        #region Classes
        /// <summary>
        /// Represents the collection of filtered items.
        /// </summary>
        public class FilterCollection : IFilteredItemsSource
        {
            #region Fields
            /// <summary>
            /// The private inner set of filtered items.
            /// </summary>
            private HashSet<object> _innerSet;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="FilterCollection"/> class.
            /// </summary>
            public FilterCollection()
            {
                this._innerSet = new HashSet<object>();
            }

            /// <summary>
            /// Initialises a new instance of the <see cref="FilterCollection"/> class.
            /// </summary>
            /// <param name="collection">
            /// The collection whose items are copied to the new set.
            /// </param>
            public FilterCollection(IEnumerable<object> collection)
            {
                this._innerSet = new HashSet<object>(collection);
            }
            #endregion Constructors

            #region Events
            /// <summary>
            /// Occurs when the collection changes.
            /// </summary>
            public event NotifyCollectionChangedEventHandler CollectionChanged;
            #endregion Events

            #region Properties
            /// <summary>
            /// Gets the number of elements actually contained in the
            /// <see cref="FilterCollection"/>.
            /// </summary>
            public int Count
            {
                get { return this._innerSet.Count; }
            }
            #endregion Properties

            #region Methods
            /// <summary>
            /// Add a new item to the filter collection.
            /// </summary>
            /// <param name="item">
            /// The item to add to the filter set.
            /// </param>
            public void AddItem(CommandViewModel item)
            {
                this._innerSet.Add(item);
                NotifyCollectionChangedEventHandler handler = this.CollectionChanged;
                if (handler == null)
                {
                    return;
                }

                NotifyCollectionChangedEventArgs e =
                    new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset);

                handler(this, e);
            }

            /// <summary>
            /// Determines whether the <see cref="FilterCollection"/> contains a specific item.
            /// </summary>
            /// <param name="item">
            /// The object to locate in the <see cref="FilterCollection"/>.
            /// </param>
            /// <returns>
            /// True if System.Object is found in the <see cref="FilterCollection"/>;
            /// otherwise, false.
            /// </returns>
            public bool Contains(object item)
            {
                return this._innerSet.Contains(item);
            }

            /// <summary>
            /// Returns an enumerator that iterates through a collection.
            /// </summary>
            /// <returns>
            /// An System.Collections.IEnumerator object that can be used to iterate through
            /// the collection.
            /// </returns>
            public IEnumerator GetEnumerator()
            {
                return this._innerSet.GetEnumerator();
            }

            /// <summary>
            /// Retrieves a value indicating the behaviour of the filter for the specified
            /// item.
            /// </summary>
            /// <param name="item">
            /// The item to get the filter behaviour for.
            /// </param>
            /// <returns>
            /// The filter behaviour for the specified item.
            /// </returns>
            public FilterDescendantBehaviour GetFilterDescendantsBehavior(object item)
            {
                return FilterDescendantBehaviour.ExcludeDescendantsByDefault;
            }
            #endregion Methods
        } // RootContainerViewModel.FilterCollection {Class}
        #endregion
    } // RSG.Editor.Controls.Customisation.RootContainerViewModel {Class}
} // RSG.Editor.Controls.Customisation {Namespace}
