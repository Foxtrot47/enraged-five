﻿//---------------------------------------------------------------------------------------------
// <copyright file="NotNullConverter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Converters
{
    using System.Globalization;

    /// <summary>
    /// Converters any object value to either true or false based on whether that object is
    /// equal to the null reference or not.
    /// </summary>
    public class NotNullConverter : ValueConverter<object, bool>
    {
        #region Methods
        /// <summary>
        /// Converters the given value and returns the result.
        /// </summary>
        /// <param name="value">
        /// The original value to convert.
        /// </param>
        /// <param name="param">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A converted value.
        /// </returns>
        protected override bool Convert(object value, object param, CultureInfo culture)
        {
            return value != null;
        }
        #endregion Methods
    } // RSG.Editor.Controls.Converters.NotNullConverter {Class}
} // RSG.Editor.Controls.Converters {Namespace}
