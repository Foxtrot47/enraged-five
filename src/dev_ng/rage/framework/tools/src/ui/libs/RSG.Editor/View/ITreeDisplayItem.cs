﻿//---------------------------------------------------------------------------------------------
// <copyright file="ITreeDisplayItem.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    using System.Windows.Media.Imaging;

    /// <summary>
    /// When implemented represents a view model that acts as a single item that the view can
    /// render as a tree view item.
    /// </summary>
    public interface ITreeDisplayItem : IDisplayItemWithState
    {
        #region Properties
        /// <summary>
        /// Gets the bitmap source that is used to display the icon for this item when it has
        /// been expanded.
        /// </summary>
        BitmapSource ExpandedIcon { get; }
        #endregion Properties
    } // RSG.Editor.View.ITreeDisplayItem {Interface}
} // RSG.Editor.View {Namespace}
