﻿//---------------------------------------------------------------------------------------------
// <copyright file="UserCreateCommandMenu.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Diagnostics;
    using System.Xml;
    using RSG.Editor.Resources;

    /// <summary>
    /// A class that represents a command menu that has been created by the user during
    /// customisation.
    /// </summary>
    internal sealed class UserCreateCommandMenu : CommandMenu, IUserCreatedCommandBarItem
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="UserCreateCommandMenu"/> class.
        /// </summary>
        /// <param name="priority">
        /// The initial priority that this command bar item should be given.
        /// </param>
        /// <param name="parentId">
        /// The unique identifier for the parent of this item inside the command bar hierarchy.
        /// </param>
        public UserCreateCommandMenu(ushort priority, Guid parentId)
            : base(priority, false, StringTable.NewMenuName, Guid.NewGuid(), parentId)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="UserCreateCommandMenu"/> class
        /// using the specified reader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The reader to use as a data provider.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        public UserCreateCommandMenu(XmlReader reader, IFormatProvider provider)
            : base(0, false, String.Empty, Guid.Empty, Guid.Empty)
        {
            this.DeserialiseCommand(reader, provider);
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Serialises this command to the end of the specified writer.
        /// </summary>
        /// <param name="writer">
        /// The xml writer that the command gets serialised out to.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        public void SerialiseCommand(XmlWriter writer, IFormatProvider provider)
        {
            writer.WriteAttributeString("id", this.Id.ToString("D").ToUpperInvariant());
            writer.WriteAttributeString(
                "parent", this.ParentId.ToString("D").ToUpperInvariant());
            writer.WriteAttributeString("priority", this.Priority.ToString(provider));
        }

        /// <summary>
        /// Deserialises the command using the data contained within the specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the command data.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        private void DeserialiseCommand(XmlReader reader, IFormatProvider provider)
        {
            Guid guidResult = Guid.Empty;

            string id = reader.GetAttribute("id");
            if (id != null)
            {
                bool parsed = Guid.TryParseExact(id, "D", out guidResult);
                Debug.Assert(parsed, "Failed to parse a user created command parameter.");
                if (parsed)
                {
                    this.Id = guidResult;
                }
            }

            string parent = reader.GetAttribute("parent");
            if (parent != null)
            {
                bool parsed = Guid.TryParseExact(parent, "D", out guidResult);
                Debug.Assert(parsed, "Failed to parse a user created command parameter.");
                if (parsed)
                {
                    this.ParentId = guidResult;
                }
            }

            string priority = reader.GetAttribute("priority");
            if (priority != null)
            {
                ushort result = 0;
                bool parsed = ushort.TryParse(priority, out result);
                Debug.Assert(parsed, "Failed to parse a user created command parameter.");
                if (parsed)
                {
                    this.Priority = result;
                }
            }
        }
        #endregion Methods
    } // RSG.Editor.UserCreateCommandMenu {Class}
} // RSG.Editor {Namespace}
