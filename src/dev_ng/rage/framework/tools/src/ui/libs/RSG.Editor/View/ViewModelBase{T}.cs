﻿//---------------------------------------------------------------------------------------------
// <copyright file="ViewModelBase{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Media.Imaging;
    using RSG.Base.Extensions;
    using RSG.Editor.Model;

    /// <summary>
    /// Provides a abstract base class that can be inherited by classes that wish to implement
    /// the <see cref="RSG.Editor.View.IViewModel{T}"/> interface.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the model that is being wrapped by this view model.
    /// </typeparam>
    public abstract class ViewModelBase<T>
        : NotifyPropertyChangedBase, IViewModel<T> where T : IModel
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="FontStyle"/> property.
        /// </summary>
        private FontStyle _fontStyle;

        /// <summary>
        /// The private field used for the <see cref="FontWeight"/> property.
        /// </summary>
        private FontWeight _fontWeight;

        /// <summary>
        /// A private value indicating whether the property changed events from the model
        /// should be forwarded on from the model.
        /// </summary>
        private bool _forwardPropertyChanges;

        /// <summary>
        /// The private field used for the <see cref="Icon"/> property.
        /// </summary>
        private BitmapSource _icon;

        /// <summary>
        /// The private field used for the <see cref="Model"/> property.
        /// </summary>
        private T _model;

        /// <summary>
        /// The private field used for the <see cref="Text"/> property.
        /// </summary>
        private string _text;

        /// <summary>
        /// The private field used for the <see cref="ToolTip"/> property.
        /// </summary>
        private string _tooltip;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ViewModelBase{T}"/> class.
        /// </summary>
        /// <param name="model">
        /// The model that this view model will be wrapping.
        /// </param>
        protected ViewModelBase(T model)
            : this(model, false)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ViewModelBase{T}"/> class.
        /// </summary>
        /// <param name="model">
        /// The model that this view model will be wrapping.
        /// </param>
        /// <param name="forwardPropertyChanges">
        /// A value indicating whether the property changed events from the specified model are
        /// forwarded on through this object.
        /// </param>
        protected ViewModelBase(T model, bool forwardPropertyChanges)
        {
            this._model = model;
            this._fontStyle = FontStyles.Normal;
            this._fontWeight = FontWeights.Normal;
            this._forwardPropertyChanges = forwardPropertyChanges;
            INotifyPropertyChanged pattern = model as INotifyPropertyChanged;
            if (pattern != null)
            {
                pattern.PropertyChanged += this.OnModelPropertyChanged;
            }
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the style that the font on this item should be using.
        /// </summary>
        public FontStyle FontStyle
        {
            get { return this._fontStyle; }
            protected set { this.SetProperty(ref this._fontStyle, value); }
        }

        /// <summary>
        /// Gets or sets the weight or thickness that the font on this item should be using.
        /// </summary>
        public FontWeight FontWeight
        {
            get { return this._fontWeight; }
            protected set { this.SetProperty(ref this._fontWeight, value); }
        }

        /// <summary>
        /// Gets or sets the bitmap source that is used to display the icon for this item.
        /// </summary>
        public virtual BitmapSource Icon
        {
            get { return this._icon; }
            protected set { this.SetProperty(ref this._icon, value); }
        }

        /// <summary>
        /// Gets the model that this view model is currently wrapping.
        /// </summary>
        IModel IViewModel.Model
        {
            get { return this._model; }
        }

        /// <summary>
        /// Gets the model that this view model is currently wrapping.
        /// </summary>
        public T Model
        {
            get { return this._model; }
        }

        /// <summary>
        /// Gets or sets the text used to display this item.
        /// </summary>
        public string Text
        {
            get { return this._text; }
            set { this.SetProperty(ref this._text, value); }
        }

        /// <summary>
        /// Gets or sets the text that is used in the tooltip for this item.
        /// </summary>
        public string ToolTip
        {
            get { return this._tooltip ?? this._text; }
            protected set { this.SetProperty(ref this._tooltip, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Called whenever the model is replaced with a new value after first initialisation.
        /// </summary>
        /// <param name="oldValue">
        /// The original value that has been replaced.
        /// </param>
        /// <param name="newValue">
        /// The new value that is now the model for this view model.
        /// </param>
        protected virtual void OnModelChanged(T oldValue, T newValue)
        {
        }

        /// <summary>
        /// Forwards any property changed event received from the model onto any object whose
        /// listening to the same event on this object.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.ComponentModel.PropertyChangedEventArgs containing the event data.
        /// </param>
        protected virtual void OnModelPropertyChanged(object s, PropertyChangedEventArgs e)
        {
            if (!this._forwardPropertyChanges)
            {
                return;
            }

            string resolved = this.ResolvePropertyNameOnChange(e.PropertyName);
            string[] resolvedNames = resolved.SafeSplit(',', true);
            foreach (string resolvedName in resolvedNames)
            {
                this.NotifyPropertyChanged(resolvedName);
            }
        }

        /// <summary>
        /// Replaces the current model with the specified model.
        /// </summary>
        /// <param name="model">
        /// The reference that will become the new model.
        /// </param>
        /// <param name="forwardPropertyChanges">
        /// A valid indicating whether the property changes from the new model should be
        /// forwarded through this object.
        /// </param>
        protected void ReplaceModel(T model, bool forwardPropertyChanges)
        {
            if (Object.ReferenceEquals(this._model, model))
            {
                Debug.Assert(false, "Trying to replace the model with the same value");
                return;
            }

            T oldValue = this._model;
            INotifyPropertyChanged oldPattern = this._model as INotifyPropertyChanged;
            if (oldPattern != null)
            {
                oldPattern.PropertyChanged -= this.OnModelPropertyChanged;
            }

            this._model = model;
            this._forwardPropertyChanges = forwardPropertyChanges;
            INotifyPropertyChanged pattern = this._model as INotifyPropertyChanged;
            if (pattern != null)
            {
                pattern.PropertyChanged += this.OnModelPropertyChanged;
            }

            this.OnModelChanged(oldValue, this._model);
        }

        /// <summary>
        /// Override to resolve a property name that's property changed event is being
        /// forwarded. Use this if you need to have a property change event from the model fire
        /// for a property in the view model with a different name.
        /// </summary>
        /// <param name="propertyName">
        /// The original name of the property.
        /// </param>
        /// <returns>
        /// The resolved property name.
        /// </returns>
        protected virtual string ResolvePropertyNameOnChange(string propertyName)
        {
            return propertyName;
        }
        #endregion Methods
    } // RSG.Editor.View.ViewModelBase{T} {Class}
} // RSG.Editor.View {Namespace}
