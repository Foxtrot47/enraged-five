﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace RSG.Automation.Commands
{
    /// <summary>
    /// Provides static properties that give access to Automation Monitor related icons.
    /// </summary>
    public static class AutomationMonitorCommandIcons
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="DownloadJobOutput"/> property.
        /// </summary>
        private static BitmapSource _downloadJobOutput;

        /// <summary>
        /// The private field used for the <see cref="DownloadJobJog"/> property.
        /// </summary>
        private static BitmapSource _downloadJobJog;

        /// <summary>
        /// A generic object that provides thread safety access to the image sources.
        /// </summary>
        private static object _syncRoot = new object();
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets the icon that shows a zoom in icon.
        /// </summary>
        public static BitmapSource DownloadJobOutput
        {
            get
            {
                if (_downloadJobOutput == null)
                {
                    lock (_syncRoot)
                    {
                        if (_downloadJobOutput == null)
                        {
                            _downloadJobOutput = EnsureLoaded("Download.png");
                        }
                    }
                }

                return _downloadJobOutput;
            }
        }

        /// <summary>
        /// Gets the icon that shows a zoom out icon.
        /// </summary>
        public static BitmapSource DownloadJobJog
        {
            get
            {
                if (_downloadJobJog == null)
                {
                    lock (_syncRoot)
                    {
                        if (_downloadJobJog == null)
                        {
                            _downloadJobJog = EnsureLoaded("UniversalLog.png");
                        }
                    }
                }

                return _downloadJobJog;
            }
        }

        #endregion Properties

        #region Methods
        /// <summary>
        /// Ensures that the specified image source is loaded with the specified resource name.
        /// </summary>
        /// <param name="source">
        /// The image source that should be loaded.
        /// </param>
        /// <param name="resourceName">
        /// The name of the resource the loaded image source should set as its source.
        /// </param>
        private static BitmapSource EnsureLoaded(String resourceName)
        {
            String assemblyName = typeof(AutomationMonitorCommandIcons).Assembly.GetName().Name;
            String bitmapPath = String.Format(CultureInfo.InvariantCulture,
                "pack://application:,,,/{0};component/Resources/{1}", assemblyName, resourceName);
            Uri uri = new Uri(bitmapPath, UriKind.RelativeOrAbsolute);
            return new BitmapImage(uri);
        }
        #endregion Methods
    }
} // AutomationMonitorCommandIcons
