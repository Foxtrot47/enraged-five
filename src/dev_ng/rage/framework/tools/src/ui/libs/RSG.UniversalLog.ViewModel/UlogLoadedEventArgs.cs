﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.UniversalLog.ViewModel
{
    /// <summary>
    /// 
    /// </summary>
    public class UlogLoadedEventArgs : EventArgs
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="filepaths"></param>
        /// <param name="successful"></param>
        public UlogLoadedEventArgs(IList<String> filepaths)
        {
            Filepaths = filepaths;
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// List of files that were loaded.
        /// </summary>
        public IList<String> Filepaths { get; private set; }
        #endregion // Properties
    } // UlogLoadedEventArgs
}
