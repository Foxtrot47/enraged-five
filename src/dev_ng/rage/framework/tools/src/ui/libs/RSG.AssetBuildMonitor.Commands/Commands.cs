﻿//---------------------------------------------------------------------------------------------
// <copyright file="Commands.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.AssetBuildMonitor.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Navigation;
    using System.Windows.Shapes;
    using RSG.Editor;
    using RSG.Editor.Controls;
    using RSG.Editor.SharedCommands;

    /// <summary>
    /// Automation Monitor commands class.
    /// </summary>
    public static class Commands
    {
        #region Enumerations
        /// <summary>
        /// Defines all of the command identifiers for the different core Rockstar commands.
        /// </summary>
        private enum CommandId
        {
            Refresh,

            OpenLog,

            ClearHistory,

            UseUTC,

            ConnectToHost,

            ConnectToNewHost,

            /// <summary>
            /// Defines the number of commands defined as Automation Monitor commands.
            /// </summary>
            CommandCount
        }
        #endregion // Enumerations

        #region Properties
        /// <summary>
        /// Gets the global identifier used for the context menu for the individual items in
        /// the build item list.
        /// </summary>
        public static Guid BuildItemContextMenu
        {
            get
            {
                if (!_buildItemContextMenu.HasValue)
                {
                    lock (_syncRoot)
                    {
                        if (!_buildItemContextMenu.HasValue)
                        {
                            _buildItemContextMenu =
                                new Guid("1D14EFEE-4B77-4EFE-8373-87B228810902");
                        }
                    }
                }

                return _buildItemContextMenu.Value;
            }
        }

        /// <summary>
        /// Gets the value that represents the Refresh command.
        /// </summary>
        public static RockstarRoutedCommand Refresh
        {
            get { return EnsureCommandExists(CommandId.Refresh, CommonIcons.Refresh); }
        }

        /// <summary>
        /// Gets the value that represents the Connect to Host command.
        /// </summary>
        public static RockstarRoutedCommand ConnectToHost
        {
            get { return EnsureCommandExists(CommandId.ConnectToHost, CommandIcons.Connect); }
        }

        /// <summary>
        /// Gets the value that represents the Add Host command.
        /// </summary>
        public static RockstarRoutedCommand ConnectToNewHost
        {
            get { return EnsureCommandExists(CommandId.ConnectToNewHost, CommandIcons.ConnectNew); }
        }

        /// <summary>
        /// Gets the value that represents the Open Log command.
        /// </summary>
        public static RockstarRoutedCommand OpenLog
        {
            get { return EnsureCommandExists(CommandId.OpenLog, CommonIcons.OpenFile); }
        }

        /// <summary>
        /// Gets the value that represents the Clear History command.
        /// </summary>
        public static RockstarRoutedCommand ClearHistory
        {
            get { return EnsureCommandExists(CommandId.ClearHistory, CommonIcons.Clear); }
        }

        /// <summary>
        /// Gets the value that represents the Use UTC command.
        /// </summary>
        public static RockstarRoutedCommand UseUTC
        {
            get { return EnsureCommandExists(CommandId.UseUTC); }
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private static Guid? _buildItemContextMenu;

        /// <summary>
        /// A private instance to a generic object used to support thread access.
        /// </summary>
        private static Object _syncRoot = new Object();

        /// <summary>
        /// String table resource manager for getting at the command related strings.
        /// </summary>
        private static readonly CommandResourceManager _commandResourceManager =
            new CommandResourceManager(
                typeof(Commands).Namespace + ".Resources.CommandStringTable",
                typeof(Commands).Assembly);

        /// <summary>
        /// The private cache of System.Windows.Input.RoutedCommand objects.
        /// </summary>
        private static readonly RockstarRoutedCommand[] _internalCommands =
            new RockstarRoutedCommand[Enum.GetValues(typeof(CommandId)).Length];
        #endregion // Member Data

        #region Private Methods
        /// <summary>
        /// Creates a new System.Windows.Input.RoutedCommand object that is associated with the
        /// specified identifier.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command to create.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        /// <returns>
        /// The newly create System.Windows.Input.RoutedCommand object.
        /// </returns>
        private static RockstarRoutedCommand CreateCommand(String id, BitmapSource icon)
        {
            String name = _commandResourceManager.GetString(id, CommandStringCategory.Name);
            String category = _commandResourceManager.GetString(id, CommandStringCategory.Category);
            String description = _commandResourceManager.GetString(id, CommandStringCategory.Description);
            InputGestureCollection gestures = _commandResourceManager.GetGestures(id);
            return new RockstarRoutedCommand(
                name, typeof(Commands), gestures, category, description, icon);
        }

        /// <summary>
        /// Makes sure the internal value for the command associated with the specified
        /// identifier exists and returns it.
        /// </summary>
        /// <param name="commandId">
        /// The identifier for the command that should be created and returned.
        /// </param>
        /// <returns>
        /// The System.Windows.Input.RoutedCommand object associated with the specified
        /// identifier.
        /// </returns>
        private static RockstarRoutedCommand EnsureCommandExists(CommandId commandId, BitmapSource icon = null)
        {
            int commandIndex = (int)commandId;
            if (commandIndex < -1 || commandIndex >= _internalCommands.Length)
            {
                return null;
            }

            RockstarRoutedCommand command = _internalCommands[commandIndex];
            if (command == null)
            {
                lock (_internalCommands)
                {
                    if (command == null)
                    {
                        command = CreateCommand(commandId.ToString(), icon);
                        _internalCommands[commandIndex] = command;
                    }
                }
            }

            return command;
        }

        /// <summary>
        /// Initialises the array used to store the System.Windows.Input.RoutedCommand objects
        /// that make up the core commands for a Rockstar application.
        /// </summary>
        /// <returns>
        /// The array used to store the internal System.Windows.Input.RoutedCommand objects.
        /// </returns>
        private static RoutedCommand[] InitialiseInternalStorage()
        {
            return (new RoutedCommand[(int)CommandId.CommandCount]);
        }
        #endregion // Private Methods
    }

} // RSG.AssetBuildMonitor.Commands namespace
