﻿//---------------------------------------------------------------------------------------------
// <copyright file="UserCreateCommandToolbar.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Diagnostics;
    using System.Xml;
    using RSG.Editor.Resources;
    using RSG.Editor.SharedCommands;

    /// <summary>
    /// A class that represents a command toolbar that has been created by the user during
    /// customisation.
    /// </summary>
    internal sealed class UserCreateCommandToolbar : CommandToolBar, IUserCreatedCommandBarItem
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="UserCreateCommandToolbar"/> class..
        /// </summary>
        public UserCreateCommandToolbar()
            : base(0, 0, StringTable.NewToolbarName, Guid.NewGuid())
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="UserCreateCommandToolbar"/> class
        /// using the specified reader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The reader to use as a data provider.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        public UserCreateCommandToolbar(XmlReader reader, IFormatProvider provider)
            : base(0, 0, String.Empty, Guid.Empty)
        {
            this.DeserialiseCommand(reader, provider);
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Serialises this command to the end of the specified writer.
        /// </summary>
        /// <param name="writer">
        /// The xml writer that the command gets serialised out to.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        public void SerialiseCommand(XmlWriter writer, IFormatProvider provider)
        {
            writer.WriteAttributeString("id", this.Id.ToString("D").ToUpperInvariant());
        }

        /// <summary>
        /// Deserialises the command using the data contained within the specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the command data.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        private void DeserialiseCommand(XmlReader reader, IFormatProvider provider)
        {
            Guid guidResult = Guid.Empty;

            string id = reader.GetAttribute("id");
            if (id != null)
            {
                bool parsed = Guid.TryParseExact(id, "D", out guidResult);
                Debug.Assert(parsed, "Failed to parse a user created command parameter.");
                if (parsed)
                {
                    this.Id = guidResult;
                }
            }
        }
        #endregion Methods
    } // RSG.Editor.UserCreateCommandMenu {Class}
} // RSG.Editor {Namespace}
