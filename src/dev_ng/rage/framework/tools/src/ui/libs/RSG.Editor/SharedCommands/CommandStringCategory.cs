﻿//---------------------------------------------------------------------------------------------
// <copyright file="CommandStringCategory.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.SharedCommands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the different string values a command has that can be retrieved from the
    /// string table.
    /// </summary>
    public enum CommandStringCategory
    {
        /// <summary>
        /// Defines the name string value located in the string table as {id}Name.
        /// </summary>
        Name,

        /// <summary>
        /// Defines the description string value located in the string table as
        /// {id}KeyGestures.
        /// </summary>
        KeyGestures,

        /// <summary>
        /// Defines the category string value located in the string table as {id}Category.
        /// </summary>
        Category,

        /// <summary>
        /// Defines the description string value located in the string table as
        /// {id}Description.
        /// </summary>
        Description
    } // RSG.Editor.SharedCommands.CommandStringCategory {Class}
} // RSG.Editor.SharedCommands {Namespace}
