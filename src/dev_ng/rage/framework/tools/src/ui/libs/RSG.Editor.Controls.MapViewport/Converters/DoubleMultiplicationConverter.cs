﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor.Controls.Converters;

namespace RSG.Editor.Controls.MapViewport.Converters
{
    /// <summary>
    /// 
    /// </summary>
    public class DoubleMultiplicationConverter : MultiValueConverter<double, double>
    {
        #region Methods
        /// <summary>
        /// Converters the specified source values to a single target value.
        /// </summary>
        /// <param name="values">
        /// The pre-converted source values.
        /// </param>
        /// <param name="param">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A target value created by converting the two specified source values.
        /// </returns>
        protected override double Convert(double[] values, object param, CultureInfo culture)
        {
            double result = 1.0;

            foreach (double value in values)
            {
                result *= value;
            }

            return result;
        }
        #endregion Methods
    }
}
