﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Rag.Widgets;

namespace RSG.Rag.ViewModel.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    public class ComboInt32ViewModel : ComboViewModel<Int32>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        public ComboInt32ViewModel(WidgetCombo<Int32> widget)
            : base(widget)
        {
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public override int SelectedItem
        {
            get { return (Widget.SelectedItem - Widget.Offset); }
            set { Widget.SelectedItem = (value + Widget.Offset); }
        }

        /// <summary>
        /// 
        /// </summary>
        public override string DefaultValue
        {
            get { return String.Format("{0} (Index: {1})", Widget.Items[Widget.DefaultItem], Widget.DefaultItem); }
        }
        #endregion // Properties
    } // ComboInt32ViewModel
}
