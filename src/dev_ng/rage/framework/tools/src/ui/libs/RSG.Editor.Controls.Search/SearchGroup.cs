﻿//---------------------------------------------------------------------------------------------
// <copyright file="SearchGroup.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Search
{
    using System.Collections.ObjectModel;

    /// <summary>
    /// Represents a search group for the search builder window. This acts as a view model for
    /// the group.
    /// </summary>
    public class SearchGroup : NotifyPropertyChangedBase
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="FieldSearches"/> property.
        /// </summary>
        private ObservableCollection<SearcherViewModel> _fieldSearches;

        /// <summary>
        /// The private field used for the <see cref="Groups"/> property.
        /// </summary>
        private ObservableCollection<SearchGroup> _groups;

        /// <summary>
        /// The private field used for the <see cref="IsExpanded"/> property.
        /// </summary>
        private bool _isExpanded;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SearchGroup"/> class.
        /// </summary>
        public SearchGroup()
        {
            this._fieldSearches = new ObservableCollection<SearcherViewModel>();
            this._groups = new ObservableCollection<SearchGroup>();
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the collection containing the field searches defined for this group.
        /// </summary>
        public ObservableCollection<SearcherViewModel> FieldSearches
        {
            get { return this._fieldSearches; }
        }

        /// <summary>
        /// Gets the collection containing the child groups defined for this group.
        /// </summary>
        public ObservableCollection<SearchGroup> Groups
        {
            get { return this._groups; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the group is expanded in the user
        /// interface.
        /// </summary>
        public bool IsExpanded
        {
            get { return this._isExpanded; }
            set { this.SetProperty(ref this._isExpanded, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Removes the specified child from this group or if this child doesn't belong to the
        /// group forwards the removal to its child groups.
        /// </summary>
        /// <param name="child">
        /// The child group to remove.
        /// </param>
        public void RemoveChildGroup(SearchGroup child)
        {
            if (this._groups.Contains(child))
            {
                this._groups.Remove(child);
                return;
            }

            foreach (SearchGroup searchGroup in this._groups)
            {
                searchGroup.RemoveChildGroup(child);
            }
        }

        /// <summary>
        /// Removes the specified searcher from this group or if this searcher doesn't belong
        /// to the group forwards the removal to its child groups.
        /// </summary>
        /// <param name="searcher">
        /// The searcher to remove.
        /// </param>
        public void RemoveSearcher(SearcherViewModel searcher)
        {
            if (this._fieldSearches.Contains(searcher))
            {
                this._fieldSearches.Remove(searcher);
                return;
            }

            foreach (SearchGroup searchGroup in this._groups)
            {
                searchGroup.RemoveSearcher(searcher);
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.Search.SearchGroup {Class}
} // RSG.Editor.Controls.Search {Namespace}
