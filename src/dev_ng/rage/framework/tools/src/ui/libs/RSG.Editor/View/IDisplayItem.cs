﻿//---------------------------------------------------------------------------------------------
// <copyright file="IDisplayItem.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    using System.Windows;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// When implemented represents a view model that acts as a single item that the view can
    /// render.
    /// </summary>
    public interface IDisplayItem
    {
        #region Properties
        /// <summary>
        /// Gets the style that the font on this item should be using.
        /// </summary>
        FontStyle FontStyle { get; }

        /// <summary>
        /// Gets the weight or thickness that the font on this item should be using.
        /// </summary>
        FontWeight FontWeight { get; }

        /// <summary>
        /// Gets the bitmap source that is used to display the icon for this item.
        /// </summary>
        BitmapSource Icon { get; }

        /// <summary>
        /// Gets the text used to display this item.
        /// </summary>
        string Text { get; }

        /// <summary>
        /// Gets the text that is used in the tooltip for this item.
        /// </summary>
        string ToolTip { get; }
        #endregion Properties
    } // RSG.Editor.View.IDisplayItem {Interface}
} // RSG.Editor.View {Namespace}
