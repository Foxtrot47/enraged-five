﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace RSG.Editor.Controls.MapViewport.OverlayComponents
{
    /// <summary>
    /// Base class for all shape based map overlay components.
    /// </summary>
    public abstract class Shape : Component
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="FillBrush"/> property.
        /// </summary>
        private Brush _fillBrush;

        /// <summary>
        /// Private field for the <see cref="StrokeBrush"/> property.
        /// </summary>
        private Brush _strokeBrush;

        /// <summary>
        /// Private field for the <see cref="StrokeThickness"/> property.
        /// </summary>
        private double _strokeThickness;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Shape"/> class.
        /// </summary>
        protected Shape()
        {
            _strokeBrush = Brushes.Black;
            _fillBrush = Brushes.Transparent;
            _strokeThickness = 1.0;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the brush used to fill this shape.
        /// </summary>
        public Brush FillBrush
        {
            get { return _fillBrush; }
            set { SetProperty(ref _fillBrush, value); }
        }

        /// <summary>
        /// Gets or sets the brush used for this shape's stroke.
        /// </summary>
        public Brush StrokeBrush
        {
            get { return _strokeBrush; }
            set { SetProperty(ref _strokeBrush, value); }
        }

        /// <summary>
        /// Gets or sets the thickness of the shapes stroke.
        /// </summary>
        public double StrokeThickness
        {
            get { return _strokeThickness; }
            set { SetProperty(ref _strokeThickness, value); }
        }
        #endregion
    }
}
