﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Editor;
using RSG.Editor.View;
using XGE = RSG.Interop.Incredibuild.XGE;
using RSG.ManagedRage.ClipAnimation;
using Microsoft.Win32;
using System.Collections.Specialized;
using System.Configuration;
using RSG.SourceControl.Perforce;

namespace RSG.AnimationToolkit.ViewModel
{
    public class PreviewData
    {
        public string rootAssetPath;
        public string dictionaryName;

        public PreviewData(string assetPath, string dictName)
        {
            rootAssetPath = assetPath;
            dictionaryName = dictName;
        }
    }

    public class CompressionData
    {
        public int uncompressedDataSize;
        public int compressedDataSize;

        public CompressionData(int uncompressed, int compressed)
        {
            uncompressedDataSize = uncompressed;
            compressedDataSize = compressed;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class AnimationToolkitViewModel : NotifyPropertyChangedBase
    {
        public enum Tabs
        {
            ZipProcessingTab,
            RpfProcessingTab,
            ClipProcessingTab
        }

        #region Constants
        /// <summary>
        /// 
        /// </summary>
        static readonly string CONVERT_EXE = System.Environment.GetEnvironmentVariable("RS_TOOLSROOT") + @"\ironlib\lib\RSG.Pipeline.Convert.exe";

        static readonly string PATH_CLIP_METADATA_DICTIONARY = Environment.GetEnvironmentVariable("RS_BUILDBRANCH") + @"\common\data\anim\clip_dictionary_metadata\";

        #endregion

        #region Properties and Associated Member Data

        public ObservableCollection<CheckedListItem> IngameAssetFolderObjects
        {
            get { return _IngameAssetFolderObjects; }
        }

        public ObservableCollection<CheckedListItem> IngameRagePacketFolderObjects
        {
            get { return _IngameRagePacketFolderObjects; }
        }

        public ObservableCollection<CheckedListItem> IngameClipFileObjects
        {
            get { return _IngameClipFileObjects; }
        }

        public ObservableCollection<CheckedListItem> CutsceneAssetFolderObjects
        {
            get { return _CutsceneAssetFolderObjects; }
        }

        public ObservableCollection<CheckedListItem> CutsceneRagePacketFolderObjects
        {
            get { return _CutsceneRagePacketFolderObjects; }
        }

        public ObservableCollection<String> Branches
        {
            get { return _Branches; }
        }

        public ObservableCollection<String> Projects
        {
            get { return _Projects; }
        }

        public ObservableCollection<String> CompressionTemplates
        {
            get { return _CompressionTemplates; }
        }

        public String SelectedBranch
        {
            set
            {
                if (SetProperty(ref _SelectedBranch, value))
                {
                    if (SelectedModeTabIndex == 0)
                        SetPaths(false);
                    else
                        SetPaths(true);
                }
            }
            get
            {
                return _SelectedBranch;
            }
        }

        public String SelectedProject
        {
            set
            {
                if (SetProperty(ref _SelectedProject, value))
                {
                    SetBranch();
                }
            }
            get
            {
                return _SelectedProject;
            }
        }

        public String SelectedCompressionTemplate
        {
            get
            {
                return _SelectedCompressionTemplate;
            }
            set
            {
                _SelectedCompressionTemplate = value;
                NotifyPropertyChanged("SelectedCompressionTemplate");
            }
        }

        public String Title
        {
            get
            {
                return _Title.ToUpper();
            }
            set
            {
                _Title = value;
                NotifyPropertyChanged("Title");
            }
        }

        public int SelectedTabIndex
        {
            get
            {
                return _SelectedTabIndex;
            }
            set
            {
                _SelectedTabIndex = value;
                NotifyPropertyChanged("SelectedTabIndex");
            }
        }

        public int SelectedModeTabIndex
        {
            get
            {
                return _SelectedModeTabIndex;
            }
            set
            {
                _SelectedModeTabIndex = value;

                if (_SelectedModeTabIndex == 0)
                {
                    SetAppSetting("mode", "ig");
                    SetPaths(false);
                }
                else
                {
                    SetAppSetting("mode", "cs");
                    SetPaths(true);
                }

                NotifyPropertyChanged("SelectedModeTabIndex");
            }
        }

        public bool IngameZipProcessingEnabled
        {
            get { return _IngameZipProcessingEnabled; }
            set
            {
                _IngameZipProcessingEnabled = value;
                NotifyPropertyChanged("IngameZipProcessingEnabled");
            }
        }

        public bool IngameRpfProcessingEnabled
        {
            get { return _IngameRpfProcessingEnabled; }
            set
            {
                _IngameRpfProcessingEnabled = value;
                NotifyPropertyChanged("IngameRpfProcessingEnabled");
            }
        }

        public bool IngameClipProcessingEnabled
        {
            get { return _IngameClipProcessingEnabled; }
            set
            {
                _IngameClipProcessingEnabled = value;
                NotifyPropertyChanged("IngameClipProcessingEnabled");
            }
        }

        public bool CutsceneZipProcessingEnabled
        {
            get { return _CutsceneZipProcessingEnabled; }
            set
            {
                _CutsceneZipProcessingEnabled = value;
                NotifyPropertyChanged("CutsceneZipProcessingEnabled");
            }
        }

        public bool CutsceneRpfProcessingEnabled
        {
            get { return _CutsceneRpfProcessingEnabled; }
            set
            {
                _CutsceneRpfProcessingEnabled = value;
                NotifyPropertyChanged("CutsceneRpfProcessingEnabled");
            }
        }

        private ObservableCollection<CheckedListItem> _IngameAssetFolderObjects;
        private ObservableCollection<CheckedListItem> _IngameRagePacketFolderObjects;
        private ObservableCollection<CheckedListItem> _IngameClipFileObjects;
        private ObservableCollection<CheckedListItem> _CutsceneAssetFolderObjects;
        private ObservableCollection<CheckedListItem> _CutsceneRagePacketFolderObjects;
        private ObservableCollection<String> _Branches;
        private ObservableCollection<String> _Projects;
        private ObservableCollection<String> _CompressionTemplates;
        private String _SelectedBranch;
        private String _SelectedProject;
        private CheckedListItem _SelectedClip;
        private String _CurrentCompressionTemplate;
        private String _SelectedCompressionTemplate;

        private String _ZipDirectory = String.Empty;
        private String _ExportDirectory = String.Empty;
        private String _ArtDirectory = String.Empty;
        private String _ConcatListDirectory = String.Empty;
        private String _AssetDirectory = String.Empty;
        private String _CutsceneArtDirectory = String.Empty;
        private String _TempPath = String.Empty;
        private String _Title = String.Empty;
        private int _SelectedTabIndex;
        private int _SelectedModeTabIndex;

        private IBranch branch = null;

        private bool _IngameClipProcessingEnabled;
        private bool _IngameZipProcessingEnabled;
        private bool _IngameRpfProcessingEnabled;
        private bool _CutsceneZipProcessingEnabled;
        private bool _CutsceneRpfProcessingEnabled;

        #endregion // Properties and Associated Member Data

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="PackFileViewModel"/> class.
        /// </summary>
        public AnimationToolkitViewModel()
        {
            _IngameAssetFolderObjects = new ObservableCollection<CheckedListItem>();
            _IngameRagePacketFolderObjects = new ObservableCollection<CheckedListItem>();
            _IngameClipFileObjects = new ObservableCollection<CheckedListItem>();
            _CutsceneAssetFolderObjects = new ObservableCollection<CheckedListItem>();
            _CutsceneRagePacketFolderObjects = new ObservableCollection<CheckedListItem>();

            _IngameAssetFolderObjects.CollectionChanged += _IngameAssetFolderObjects_CollectionChanged;
            _IngameRagePacketFolderObjects.CollectionChanged += _IngameRagePacketFolderObjects_CollectionChanged;
            _IngameClipFileObjects.CollectionChanged += _IngameClipFileObjects_CollectionChanged;
            _CutsceneAssetFolderObjects.CollectionChanged += _CutsceneAssetFolderObjects_CollectionChanged;
            _CutsceneRagePacketFolderObjects.CollectionChanged += _CutsceneRagePacketFolderObjects_CollectionChanged;

            _Branches = new ObservableCollection<String>();
            _Projects = new ObservableCollection<String>();
            _CompressionTemplates = new ObservableCollection<String>();
            Initialise();
            MClip.Init();

            string value = GetAppSetting("mode");
            if (value != null)
            {
                if (value == "ig")
                    SelectedModeTabIndex = 0;
                else if (value == "cs")
                    SelectedModeTabIndex = 1;
            }
        }
        #endregion // Constructors

        #region Event Handlers

        public void _IngameAssetFolderObjects_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            ObservableCollection<CheckedListItem> collection = (ObservableCollection<CheckedListItem>)sender;
            IngameZipProcessingEnabled = (collection.Count > 0);
        }

        public void _IngameRagePacketFolderObjects_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            ObservableCollection<CheckedListItem> collection = (ObservableCollection<CheckedListItem>)sender;
            IngameRpfProcessingEnabled = (collection.Count > 0);
        }

        public void _IngameClipFileObjects_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            ObservableCollection<CheckedListItem> collection = (ObservableCollection<CheckedListItem>)sender;
            IngameClipProcessingEnabled = (collection.Count > 0);
        }

        public void _CutsceneAssetFolderObjects_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            ObservableCollection<CheckedListItem> collection = (ObservableCollection<CheckedListItem>)sender;
            CutsceneZipProcessingEnabled = (collection.Count > 0);
        }

        public void _CutsceneRagePacketFolderObjects_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            ObservableCollection<CheckedListItem> collection = (ObservableCollection<CheckedListItem>)sender;
            CutsceneRpfProcessingEnabled = (collection.Count > 0);
        }

        #endregion

        #region Public Methods

        public void PreviewCutsceneZip(object param)
        {
            BuildCutsceneZip(GetSelectedCutsceneAssetFolderObjects(), true);
        }

        public void CreateCutsceneZip(object param)
        {
            BuildCutsceneZip(GetSelectedCutsceneAssetFolderObjects(), false);
        }

        public void CreateCutsceneRPF(object param)
        {
            BuildRPFs(GetSelectedCutsceneRagePacketObjects(), true);
        }

        public void CreateBuildCutsceneZip(object param)
        {
            CreateBuildCutsceneZip(GetSelectedCutsceneAssetFolderObjects());
        }

        public void PreviewIngameZip(object param)
        {
            BuildZip(GetSelectedIngameAssetFolderObjects(), true);
        }

        public void CreateIngameZip(object param)
        {
            BuildZip(GetSelectedIngameAssetFolderObjects(), false);
        }

        public void PreviewIngameClipZip(object param)
        {
            BuildZip(GetSelectedClipObjects(), true);
        }

        public void CreateIngameClipZip(object param)
        {
            BuildZip(GetSelectedClipObjects(), false);
        }

        public void CreateIngameRPF(object param)
        {
            BuildRPFs(GetSelectedRagePacketObjects(), true);
        }

        public void CreateIngameClipRPF(object param)
        {
            BuildRPFs(GetSelectedClipObjects(), false);
        }

        public void ApplyIngameClipChange(object param)
        {
            BuildXGEClipXML(GetSelectedClipObjects());
        }

        public void SaveCSV(object param)
        {
            //List<string> lstClips = new List<string>();
            //foreach (CheckedListItem check in ClipFileObjects)
            //{
            //    lstClips.Add(check.Name);
            //}

            //SaveCompressionData(lstClips.ToArray());
        }

        public void Open(string[] directories, bool append)
        {
            if (SelectedModeTabIndex == 0)
            {
                if(!append)
                    _IngameAssetFolderObjects.Clear();

                foreach (string dir in directories)
                {
                    string[] dirs = Directory.GetDirectories(dir, "*.*", SearchOption.AllDirectories);

                    if (DirectoryContainsData(dir))
                        _IngameAssetFolderObjects.Add(new CheckedListItem(dir + "\\", false, String.Empty));
                    foreach (string subdir in dirs)
                    {
                        if (DirectoryContainsData(subdir))
                            _IngameAssetFolderObjects.Add(new CheckedListItem(subdir + "\\", false, String.Empty));
                    }
                }
            }
            else
            {
                if (!append)
                    _CutsceneAssetFolderObjects.Clear();

                foreach (string dir in directories)
                {
                    if (DirectoryContainsData(dir))
                        _CutsceneAssetFolderObjects.Add(new CheckedListItem(dir + "\\", false, String.Empty));
                }
            }
            
        }

        public void OpenRPFFolder(string[] directories, bool append)
        {
            if (SelectedModeTabIndex == 0)
            {
                if (!append)
                    _IngameRagePacketFolderObjects.Clear();

                foreach (string dir in directories)
                {
                    string[] dirs = Directory.GetDirectories(dir, "*.*", SearchOption.AllDirectories);

                    if(DirectoryContainsZips(dir + "\\"))
                        _IngameRagePacketFolderObjects.Add(new CheckedListItem(dir + "\\", false, String.Empty));
                    foreach (string subdir in dirs)
                    {
                        if (DirectoryContainsZips(subdir + "\\"))
                            _IngameRagePacketFolderObjects.Add(new CheckedListItem(subdir + "\\", false, String.Empty));
                    }
                }
            }
            else
            {
                if (!append)
                    _CutsceneRagePacketFolderObjects.Clear();

                foreach (string dir in directories)
                {
                    if (DirectoryContainsZips(dir + "\\"))
                        _CutsceneRagePacketFolderObjects.Add(new CheckedListItem(dir + "\\", false, String.Empty));
                }
            }
            
        }

        public void OpenClipFiles(string[] directories)
        {
            _IngameClipFileObjects.Clear();

            foreach (string dir in directories)
            {
                string[] files = Directory.GetFiles(dir, "*.clip", SearchOption.AllDirectories);

                foreach (string file in files)
                {
                    _IngameClipFileObjects.Add(new CheckedListItem(file, false, GetClipTemplate(file)));
                }
            }
        }

        public void OpenConcatListFile(string filename, bool append)
        {
            if (!append)
                _CutsceneAssetFolderObjects.Clear();

            XmlDocument xmlDoc = new  XmlDocument();
            xmlDoc.Load(filename);

            XmlNodeList nodes = xmlDoc.SelectNodes("/RexRageCutscenePartFile/parts/Item/filename");

            HashSet<string> uniqueDirectories = new HashSet<string>();
            foreach (XmlNode node in nodes)
            {
                string directory = Path.GetDirectoryName(node.InnerText);
                directory = branch.Environment.Subst(directory);

                using (P4 p4 = branch.Project.SCMConnect())
                {
                    p4.Run("sync", Path.Combine(directory, "..."));
                }

                if (!uniqueDirectories.Contains(directory) && DirectoryContainsData(directory))
                    _CutsceneAssetFolderObjects.Add(new CheckedListItem(directory + "\\", false, String.Empty));

                uniqueDirectories.Add(directory);
            }
        }

        public void SelectItems(bool bSelect)
        {
            if (SelectedTabIndex == Convert.ToInt32(RSG.AnimationToolkit.ViewModel.AnimationToolkitViewModel.Tabs.ZipProcessingTab))
            {
                if (SelectedModeTabIndex == 0)
                {
                    foreach (CheckedListItem item in IngameAssetFolderObjects)
                    {
                        item.IsChecked = bSelect;
                    }
                }
                else
                {
                    foreach (CheckedListItem item in CutsceneAssetFolderObjects)
                    {
                        item.IsChecked = bSelect;
                    }
                }
                
            }
            else if (SelectedTabIndex == Convert.ToInt32(RSG.AnimationToolkit.ViewModel.AnimationToolkitViewModel.Tabs.RpfProcessingTab))
            {
                if (SelectedModeTabIndex == 0)
                {
                    foreach (CheckedListItem item in IngameRagePacketFolderObjects)
                    {
                        item.IsChecked = bSelect;
                    }
                }
                else
                {
                    foreach (CheckedListItem item in CutsceneRagePacketFolderObjects)
                    {
                        item.IsChecked = bSelect;
                    }
                }
            }
            else if (SelectedTabIndex == Convert.ToInt32(RSG.AnimationToolkit.ViewModel.AnimationToolkitViewModel.Tabs.ClipProcessingTab))
            {
                if (SelectedModeTabIndex == 0)
                {
                    foreach (CheckedListItem item in IngameClipFileObjects)
                    {
                        item.IsChecked = bSelect;
                    }
                }
            }
        }

        public void SetBranch()
        {
            LongOption[] opts = new LongOption[] { };

            ArrayList lst = new ArrayList();

            lst.Add("--dlc");
            lst.Add(SelectedProject);

            CommandOptions options = new CommandOptions((string[])lst.ToArray(typeof(string)), opts);

            _Branches.Clear();

            foreach (KeyValuePair<String, IBranch> pair in options.Project.Branches)
            {
                _Branches.Add(pair.Key);
            }

            SelectedBranch = _Branches[0];
        }

        public void SetPaths(bool bCutscene)
        {
            if (SelectedProject == null || SelectedBranch == null) return;

            LongOption[] opts = new LongOption[] { };

            ArrayList lst = new ArrayList();

            lst.Add("--dlc");
            lst.Add(SelectedProject);
            lst.Add("--branch");
            lst.Add(SelectedBranch);

            CommandOptions options = new CommandOptions((string[])lst.ToArray(typeof(string)), opts);

            _TempPath = String.Format("{0}\\temp", options.Branch.Export);
            Title = options.CoreProject.Name;

            if (bCutscene)
            {
                _ZipDirectory = String.Format("{0}\\anim\\cutscene\\", options.Branch.Export);
                _ExportDirectory = String.Format("{0}\\cuts\\", options.Branch.Assets);
                _ConcatListDirectory = String.Format("{0}\\cuts\\!!Cutlists", options.Branch.Assets);
                _ArtDirectory = options.Branch.Art;
                _AssetDirectory = options.Branch.Assets;
                _CutsceneArtDirectory = String.Format("{0}\\animation\\cutscene\\!!scenes\\", options.Branch.Art);
            }
            else
            {
                _ZipDirectory = String.Format("{0}\\anim\\ingame\\", options.Branch.Export);
                _ExportDirectory = String.Format("{0}\\anim\\export_mb", options.Branch.Art);
                
            }

            branch = options.Branch;
        }

        private String GetClipTemplate(string file)
        {
            String currentTemplate = String.Empty;

            MClip clip = new MClip(MClip.ClipType.Normal);
            if (clip.Load(file).Equals(true))
            {
                MProperty compressionProperty = clip.FindProperty("Compressionfile_DO_NOT_RESOURCE");
                if (compressionProperty != null)
                {
                    MPropertyAttributeString compressionAttribute = (MPropertyAttributeString)compressionProperty.GetPropertyAttributes()[0];

                    if (compressionAttribute != null)
                        currentTemplate = compressionAttribute.GetString();
                }
            }

            clip.Dispose();

            return currentTemplate;
        }

        public string GetInitialSourcePath()
        {
            return _ZipDirectory;
        }

        public string GetInitialExportPath()
        {
            return _ExportDirectory;
        }

        public string GetInitialConcatListPath()
        {
            return _ConcatListDirectory;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// 
        /// </summary>
        private void Initialise()
        {
            LongOption[] opts = new LongOption[] { };
            string[] args = new string[] { };

            CommandOptions options = new CommandOptions(args, opts);

            Projects.Add("root");
            foreach (KeyValuePair<string, RSG.Base.Configuration.IProject> entry in options.Config.DLCProjects)
                Projects.Add(entry.Key);

            String compressionTemplateDirectory = String.Format("{0}\\etc\\config\\anim\\compression_templates", options.CoreProject.Config.ToolsRoot);
            if(Directory.Exists(compressionTemplateDirectory))
            {
                foreach (string file in Directory.GetFiles(compressionTemplateDirectory, "*.txt", SearchOption.TopDirectoryOnly))
                {
                    _CompressionTemplates.Add(Path.GetFileName(file));
                }

                SelectedCompressionTemplate = _CompressionTemplates[0];
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void CreateBuildCutsceneZip(string[] files)
        {
            BuildCutsceneZip(files, false);

            // Generate list of zips, pass into build rpf
            HashSet<string> zipFiles = new HashSet<string>();

            foreach (string file in files)
            {
                string strAnimationFolder = Path.GetDirectoryName(file) + "\\";
                string strDictionaryName = GenerateDictionaryName(strAnimationFolder);
                string strMission = GetMissionPath(strAnimationFolder);
                if (strDictionaryName == String.Empty) return;
                string previewDictionary = Path.Combine(_ZipDirectory, strMission, strDictionaryName + ".icd.zip");
                zipFiles.Add(previewDictionary.ToLower());
            }

            BuildRPFs(zipFiles.ToArray(), true);
        }

        /// <summary>
        /// 
        /// </summary>
        private void BuildCutsceneZip(string[] files, bool bPreview)
        {
            if (files.Length == 0) return;

            XmlDocument ap3Doc = BuildAP3XmlFile();
            List<string> previewDictionaries = new List<string>();
            Dictionary<string, PreviewData> previewDataDictionary = new Dictionary<string, PreviewData>();

            for (int i = 0; i < files.Length; ++i)
            {
                string strAnimationFolder = Path.GetDirectoryName((string)files[i]) + "\\";
                string strDictionaryName = GenerateDictionaryName(strAnimationFolder);
                string strMission = GetMissionPath(strAnimationFolder);
                if (strDictionaryName == String.Empty) return;
                string previewDictionary = Path.Combine(_ZipDirectory, strMission, strDictionaryName + ".icd.zip");

                //Check to see if we already have an entry for this animation folder.
                if (!previewDataDictionary.ContainsKey(strAnimationFolder))
                {
                    PreviewData newPreviewData = new PreviewData(strMission, strDictionaryName);
                    previewDataDictionary.Add(strAnimationFolder, newPreviewData);
                    previewDictionaries.Add(previewDictionary);
                }
            }

            foreach (KeyValuePair<string, PreviewData> kvp in previewDataDictionary)
            {
                string animationFolder = kvp.Key;
                XmlElement ap3Ele = AddDictionaryToAP3File(ap3Doc, animationFolder, kvp.Value.rootAssetPath, kvp.Value.dictionaryName);
                AddSubDictionariesToAP3File(ap3Ele, ap3Doc, animationFolder);
            }

            if (!Directory.Exists(_TempPath))
                Directory.CreateDirectory(_TempPath);

            string ap3AssetFilename = _ZipDirectory + "\\master_icd_list.xml";

            if (!Directory.Exists(_ZipDirectory))
                Directory.CreateDirectory(_ZipDirectory);

            ap3Doc.Save(ap3AssetFilename);

            bool success = BuildAP3Zip(ap3AssetFilename);

            if (success && bPreview)
            {
                BuildXGEPreview(previewDictionaries);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void BuildZip(string[] files, bool bPreview)
        {
            if (files.Length == 0) return;

            XmlDocument ap3Doc = BuildAP3XmlFile();
            List<string> previewDictionaries = new List<string>();
            Dictionary<string, PreviewData> previewDataDictionary = new Dictionary<string, PreviewData>();

            for (int i = 0; i < files.Length; ++i)
            {
                string strAnimationFolder = Path.GetDirectoryName((string)files[i]) + "\\";
                string strDictionaryName = GenerateDictionaryName(strAnimationFolder);
                if (strDictionaryName == String.Empty) return;
                string strRootAssetPath = GetRootAssetPath(strAnimationFolder);
                string previewDictionary = Path.Combine(_ZipDirectory, strRootAssetPath, strDictionaryName + ".icd.zip");

                 //Check to see if we already have an entry for this animation folder.
                if (!previewDataDictionary.ContainsKey(strAnimationFolder))
                {
                    PreviewData newPreviewData = new PreviewData(strRootAssetPath, strDictionaryName);
                    previewDataDictionary.Add(strAnimationFolder, newPreviewData);
                    previewDictionaries.Add(previewDictionary);
                }
            }

            foreach (KeyValuePair<string, PreviewData> kvp in previewDataDictionary)
            {
                string animationFolder = kvp.Key;
                AddDictionaryToAP3File(ap3Doc, animationFolder, kvp.Value.rootAssetPath, kvp.Value.dictionaryName);
            }

            if (!Directory.Exists(_TempPath))
                Directory.CreateDirectory(_TempPath);

            string ap3AssetFilename = _ZipDirectory + "\\master_icd_list.xml";

            if (!Directory.Exists(_ZipDirectory))
                Directory.CreateDirectory(_ZipDirectory);

            ap3Doc.Save(ap3AssetFilename);

            bool success = BuildAP3Zip(ap3AssetFilename);

            if (success && bPreview)
            {
                BuildXGEPreview(previewDictionaries);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void BuildRPFs(string[] files, bool qualifiedPaths)
        {
            if (files.Length == 0) return;

            List<string> zipFiles = new List<string>();

            for (int i = 0; i < files.Length; i++)
            {
                string strAnimationFolder = Path.GetDirectoryName((string)files[i]) + "\\";
                string strRootAssetPath = GetRootAssetPath(strAnimationFolder);
                string rpfDirectory = strAnimationFolder;
                if(!qualifiedPaths)
                    rpfDirectory = Path.Combine(_ZipDirectory, strRootAssetPath);

                if (!zipFiles.Contains(rpfDirectory))
                    zipFiles.Add(rpfDirectory);
            }

            BuildRPF(zipFiles);
        }

        /// <summary>
        /// 
        /// </summary>
        private void BuildXGEPreview(List<string> dictionaries)
        {
            string process = CONVERT_EXE;
            string cmdline = String.Format("--preview --branch {0} {1}", SelectedBranch, String.Join(" ", dictionaries));

            Process P = Process.Start(process, cmdline);
            P.WaitForExit();
        }

        /// <summary>
        /// 
        /// </summary>
        private bool BuildAP3Zip(string ap3Filename)
        {
            string process = CONVERT_EXE;
            string cmdline = String.Format("--rebuild --branch {0} {1}", SelectedBranch, ap3Filename);
            Process P = Process.Start(process, cmdline);
            P.WaitForExit();
            return P.ExitCode == 0;
        }

        /// <summary>
        /// 
        /// </summary>
        private bool BuildRPF(List<string> lstZips)
        {
            // process all the folders in one go
            string strArgs = "--branch " + SelectedBranch + " ";
            for (int i = 0; i < lstZips.Count; ++i)
            {
                //// Make sure we can update the clip file before actually building.
                //if (!CheckClipRPFFileForWriting(lstZips[i]))
                //    return false;
                strArgs += lstZips[i] + "\\ ";
            }

            string process = CONVERT_EXE;
            string cmdline = strArgs;
            Process P = Process.Start(process, cmdline);
            P.WaitForExit();
            return P.ExitCode == 0;
        }

        /// <summary>
        /// 
        /// </summary>
        private XmlDocument BuildAP3XmlFile()
        {
            XmlDocument ap3Document = new XmlDocument();
            XmlElement rootElement = ap3Document.CreateElement("Assets");
            ap3Document.AppendChild(rootElement);
            return ap3Document;
        }

        /// <summary>
        /// 
        /// </summary>
        private string GetRootAssetPath(string strAnimationPath)
        {
            string rootAssetPath = _ExportDirectory;
            string[] rootAssetPathTokens;
	        string[] outputPathTokens;

            rootAssetPathTokens = rootAssetPath.Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);
            outputPathTokens = strAnimationPath.Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);

            return outputPathTokens[rootAssetPathTokens.Length];
        }

        /// <summary>
        /// 
        /// </summary>
        private string GetMissionPath(string strAnimationPath)
        {
            string[] files = Directory.GetFiles(strAnimationPath, "*.clip", SearchOption.TopDirectoryOnly);
            foreach (string file in files)
            {
                String fbxFile = String.Empty;
                MClip clip = new MClip(MClip.ClipType.Normal);
                if (clip.Load(file).Equals(true))
                {
                    MProperty fbxProperty = clip.FindProperty("FBXFile_DO_NOT_RESOURCE");
                    if (fbxProperty != null)
                    {
                        MPropertyAttributeString fbxAttribute = (MPropertyAttributeString)fbxProperty.GetPropertyAttributes()[0];

                        if (fbxAttribute != null)
                            fbxFile = fbxAttribute.GetString();
                    }
                }

                String assetDir = _CutsceneArtDirectory.Replace("/", "\\").ToLower();
                fbxFile = fbxFile.Replace("/", "\\").Replace("$(art)", _ArtDirectory).ToLower();

                if (fbxFile.Contains(assetDir))
                {
                    fbxFile = fbxFile.Replace(assetDir, "");
                    string[] split = fbxFile.Split('\\');
                    if (split.Length > 0)
                    {
                        return split[0];
                    }
                }

                clip.Dispose();
            }

            return String.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        private string GenerateDictionaryName(string strAnimationPath)
        {
            string rootAssetPath = _ExportDirectory.ToLower();
	        string[] outputPathTokens;
            string cAnimRelativePath = "";

            strAnimationPath = strAnimationPath.ToLower();

            if (strAnimationPath.StartsWith(rootAssetPath,StringComparison.OrdinalIgnoreCase))
            {
                strAnimationPath = strAnimationPath.Replace(rootAssetPath, "");
            }
            else
            {
                String msg = String.Format("Output path '{0}' is not under expected path of '{1}'.", strAnimationPath, rootAssetPath);
                MessageBox.Show(msg, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return String.Empty;
            }

	        outputPathTokens = strAnimationPath.Split( '\\' );

            foreach (string entry in outputPathTokens)
            {
                cAnimRelativePath += entry;
            }

            return cAnimRelativePath;	
        }

        /// <summary>
        /// 
        /// </summary>
        private XmlElement AddDictionaryToAP3File(XmlDocument xmlDoc, string strDirectory, string strRootDir, string strDictionaryName)
        {
            XmlElement element = xmlDoc.CreateElement("ZipArchive");
            XmlAttribute pathAttribute = xmlDoc.CreateAttribute("path");

            string outputFile = Path.Combine(_ZipDirectory, strRootDir, strDictionaryName + ".icd.zip");
            pathAttribute.Value = outputFile;

            element.Attributes.Append(pathAttribute);

            XmlElement directoryElement = xmlDoc.CreateElement("Directory");
            XmlAttribute directoryPathAttribute = xmlDoc.CreateAttribute("path");
            directoryPathAttribute.Value = strDirectory;
            directoryElement.Attributes.Append(directoryPathAttribute);

            element.AppendChild(directoryElement);

            xmlDoc.DocumentElement.AppendChild(element);

            return element;
        }

        /// <summary>
        /// 
        /// </summary>
        private void AddSubDictionariesToAP3File(XmlElement xmlElement, XmlDocument xmlDoc, string strDirectory)
        {
            string[] directories = Directory.GetDirectories(strDirectory);

            foreach (string dir in directories)
            {
                if (Path.GetFileName(dir) == "toybox") continue;

                XmlElement directoryElement = xmlDoc.CreateElement("Directory");
                XmlAttribute directoryPathAttribute = xmlDoc.CreateAttribute("path");
                directoryPathAttribute.Value = dir;
                directoryElement.Attributes.Append(directoryPathAttribute);
                XmlAttribute directoryDestionationAttribute = xmlDoc.CreateAttribute("destination");
                directoryDestionationAttribute.Value = Path.GetFileName(dir);
                directoryElement.Attributes.Append(directoryDestionationAttribute);

                xmlElement.AppendChild(directoryElement);

                string[] subDirectories = Directory.GetDirectories(dir);

                foreach (string subDir in subDirectories)
                {
                    if (Path.GetFileName(subDir) == "toybox") continue;

                    directoryElement = xmlDoc.CreateElement("Directory");
                    directoryPathAttribute = xmlDoc.CreateAttribute("path");
                    directoryPathAttribute.Value = subDir;
                    directoryElement.Attributes.Append(directoryPathAttribute);
                    directoryDestionationAttribute = xmlDoc.CreateAttribute("destination");
                    directoryDestionationAttribute.Value = Path.Combine(Path.GetFileName(dir), Path.GetFileName(subDir));
                    directoryElement.Attributes.Append(directoryDestionationAttribute);

                    xmlElement.AppendChild(directoryElement);
                }
            } 
        }


        /// <summary>
        /// 
        /// </summary>
        private string[] GetSelectedIngameAssetFolderObjects()
        {
            List<string> selectedObjects = new List<string>();
            foreach (CheckedListItem check in _IngameAssetFolderObjects)
            {
                if (check.IsChecked)
                    selectedObjects.Add(check.Name);
            }

            return selectedObjects.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        private string[] GetSelectedCutsceneAssetFolderObjects()
        {
            List<string> selectedObjects = new List<string>();
            foreach (CheckedListItem check in _CutsceneAssetFolderObjects)
            {
                if (check.IsChecked)
                    selectedObjects.Add(check.Name);
            }

            return selectedObjects.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        private string[] GetSelectedRagePacketObjects()
        {
            List<string> selectedObjects = new List<string>();
            foreach (CheckedListItem check in _IngameRagePacketFolderObjects)
            {
                if (check.IsChecked)
                    selectedObjects.Add(check.Name);
            }

            return selectedObjects.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        private string[] GetSelectedCutsceneRagePacketObjects()
        {
            List<string> selectedObjects = new List<string>();
            foreach (CheckedListItem check in _CutsceneRagePacketFolderObjects)
            {
                if (check.IsChecked)
                    selectedObjects.Add(check.Name);
            }

            return selectedObjects.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        private string[] GetSelectedClipObjects()
        {
            List<string> selectedObjects = new List<string>();
            foreach (CheckedListItem check in _IngameClipFileObjects)
            {
                if (check.IsChecked)
                    selectedObjects.Add(check.Name);
            }

            return selectedObjects.ToArray();
        }

        private void SetCompressionTemplate(string template)
        {
            for(int i=0; i < _IngameClipFileObjects.Count; ++i)
            {
                if (_IngameClipFileObjects[i].IsChecked)
                    _IngameClipFileObjects[i].CompressionTemplate = template;
            }
        }

        private void BuildXGEClipXML(string[] clips)
        {
            //for (int i = 0; i < clips.Count; i++)
            //{
            //    if (!File.Exists(clips[i]) || IsFileLocked(clips[i]))
            //    {
            //        MessageBox.Show(string.Format("Cannot proceed with clip processing: {0} is read only.\nPlease check out from perforce.", clips[i]));
            //        return;
            //    }
            //}

            XGE.Environment clipEnv = new XGE.Environment("clip_env");
            XGE.Project proj = new XGE.Project("Clip Processing");
            proj.DefaultEnvironment = clipEnv;
            string toolArguments = "-clip=$(SourcePath) -out=$(SourcePath) -editproperty=Compressionfile_DO_NOT_RESOURCE,Compressionfile_DO_NOT_RESOURCE," + SelectedCompressionTemplate/*+ cmdboxCompression.SelectedItem.ToString()*/;
            XGE.Tool clipEditTool = new XGE.Tool("clipEdit", Path.Combine(Environment.GetEnvironmentVariable("RS_TOOLSROOT"), "bin\\anim\\clipedit.exe"), toolArguments);
            clipEnv.Tools.Add(clipEditTool);

            XGE.ITask[] taskArray = new XGE.ITask[clips.Length];

            for (int i = 0; i < clips.Length; i++)
            {
                string clipFilename = Path.GetFileNameWithoutExtension(clips[i]);
                XGE.TaskJob clipJob = new XGE.TaskJob(clipFilename, clips[i]);
                taskArray[i] = clipJob;
            }

            XGE.TaskGroup clipGroup = new XGE.TaskGroup("task_clipEdit", taskArray, null, clipEditTool);

            string xgeFilename = _TempPath + "XGEClipEdit.xml";
            string logFilename = _TempPath + "XGEClipLogFile.txt";

            proj.Tasks.Add(clipGroup);
            proj.Write(xgeFilename);

            if (XGE.XGE.Start("Update Clip Files", xgeFilename, logFilename))
            {
                SetCompressionTemplate(SelectedCompressionTemplate);
            }
        }

        private bool DirectoryContainsData(string directory)
        {
            return (Directory.Exists(directory) && Directory.GetFiles(directory, "*.clip").Length > 0) && (Directory.GetFiles(directory, "*.anim").Length > 0);
        }

        private bool DirectoryContainsZips(string directory)
        {
            return (Directory.Exists(directory) && Directory.GetFiles(directory, "*icd.zip").Length > 0);
        }

        public string GetAppSetting(string key)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(System.Reflection.Assembly.GetExecutingAssembly().Location);
            
            if (config.AppSettings.Settings.AllKeys.Contains(key))
                return config.AppSettings.Settings[key].Value;

            return null;
        }

        public void SetAppSetting(string key, string value)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(System.Reflection.Assembly.GetExecutingAssembly().Location);

            if (config.AppSettings.Settings[key] != null)
                config.AppSettings.Settings.Remove(key);
            
            config.AppSettings.Settings.Add(key, value);

            config.Save(ConfigurationSaveMode.Modified);
        }

        public void SavePreset(string filename)
        {
            if (_SelectedModeTabIndex == 1) // Only save in cutscene mode
            {
                using (XmlWriter writer = XmlWriter.Create(filename))
                {
                    writer.WriteStartDocument();

                    writer.WriteStartElement("Data");

                    // Zip View
                    writer.WriteStartElement("Source");
                    writer.WriteAttributeString("Mode", "cs");

                    foreach (CheckedListItem item in CutsceneAssetFolderObjects)
                    {
                        writer.WriteStartElement("Item");
                        writer.WriteAttributeString("Name", item.Name);
                        writer.WriteAttributeString("Checked", item.IsChecked.ToString());
                        writer.WriteEndElement();
                    }

                    writer.WriteEndElement();

                    // RPF View
                    writer.WriteStartElement("Zip");
                    writer.WriteAttributeString("Mode", "cs");

                    foreach (CheckedListItem item in CutsceneRagePacketFolderObjects)
                    {
                        writer.WriteStartElement("Item");
                        writer.WriteAttributeString("Name", item.Name);
                        writer.WriteAttributeString("Checked", item.IsChecked.ToString());
                        writer.WriteEndElement();
                    }

                    writer.WriteEndElement();

                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                }
            }
        }

        public void LoadPreset(string filename)
        {
            XmlDocument xml = new XmlDocument();
            xml.Load(filename);

            XmlNodeList nodes = xml.SelectNodes("/Data/Source");
            if (nodes.Count > 0)
            {
                CutsceneAssetFolderObjects.Clear();
                foreach (XmlNode n in nodes)
                {
                    if (n.Attributes["Mode"].Value == "cs")
                    {
                        foreach (XmlNode c in n.ChildNodes)
                        {
                            CutsceneAssetFolderObjects.Add(new CheckedListItem(c.Attributes["Name"].Value, (c.Attributes["Checked"].Value.ToLower() == "true"), String.Empty));
                        }
                    }
                }
            }

            nodes = xml.SelectNodes("/Data/Zip");
            if (nodes.Count > 0)
            {
                CutsceneRagePacketFolderObjects.Clear();
                foreach (XmlNode n in nodes)
                {
                    if (n.Attributes["Mode"].Value == "cs")
                    {
                        foreach (XmlNode c in n.ChildNodes)
                        {
                            CutsceneRagePacketFolderObjects.Add(new CheckedListItem(c.Attributes["Name"].Value, (c.Attributes["Checked"].Value.ToLower() == "true"), String.Empty));
                        }
                    }
                }
            }
        }

        //private string GetClipMetaDataFile(string clipPath)
        //{
        //    string strippedPath = clipPath.ToLower().Replace(_ExportDirectory, "");
        //    string[] clipPathSplit = strippedPath.Split('\\');
        //    string dictionaryName = clipPathSplit[0];
        //    string clipMetadataFile = Path.Combine(PATH_CLIP_METADATA_DICTIONARY, string.Format("clip_{0}.xml", dictionaryName));

        //    return clipMetadataFile;
        //}

        //private bool ParseMetadataFileForCompression(string clipPath, out int compressedSize, out int uncompressedSize)
        //{
        //    string metaDataFilename = GetClipMetaDataFile(clipPath);
        //    string clipDictionary = GenerateDictionaryName(clipPath);
        //    if (clipDictionary == String.Empty)
        //    {
        //        compressedSize = 0;
        //        uncompressedSize = 0;
        //        return false;
        //    }
        //    string clipName = clipDictionary + ".icd.zip";

        //    if (File.Exists(metaDataFilename))
        //    {
        //        XmlDocument xmlDoc = new XmlDocument();
        //        xmlDoc.Load(metaDataFilename);

        //        if (xmlDoc.DocumentElement.Name == "fwClipRpfBuildMetadata")
        //        {
        //            for (int i = 0; i < xmlDoc.DocumentElement.ChildNodes.Count; i++)
        //            {
        //                XmlNode node = xmlDoc.DocumentElement.ChildNodes[i];
        //                if (node.Name.ToLower() == "dictionaries")
        //                {
        //                    for (int clip = 0; clip < node.ChildNodes.Count; clip++)
        //                    {
        //                        XmlNode itemNode = node.ChildNodes[clip];
        //                        string dictionaryName = itemNode.Attributes["key"].Value;
        //                        if (dictionaryName.ToLower() == clipName.ToLower())
        //                        {
        //                            int uncompressedSizeValue = 0;
        //                            int compressedSizeValue = 0;

        //                            for (int sizes = 0; sizes < itemNode.ChildNodes.Count; sizes++)
        //                            {
        //                                XmlNode sizeNode = itemNode.ChildNodes[sizes];

        //                                if (sizeNode.Name == "sizeBefore")
        //                                    int.TryParse(sizeNode.Attributes["value"].Value, out uncompressedSizeValue);
        //                                else if (sizeNode.Name == "sizeAfter")
        //                                    int.TryParse(sizeNode.Attributes["value"].Value, out compressedSizeValue);
        //                            }

        //                            uncompressedSize = uncompressedSizeValue;
        //                            compressedSize = compressedSizeValue;

        //                            return true;
        //                        }
        //                    }
        //                }
        //            }
        //        }

        //        compressedSize = 0;
        //        uncompressedSize = 0;
        //    }
        //    else
        //    {
        //        compressedSize = 0;
        //        uncompressedSize = 0;
        //    }

        //    return true;
        //}

        //private void SaveCompressionData(string[] files)
        //{
        //    if (files.Length > 0)
        //    {
        //        SaveFileDialog sfd = new SaveFileDialog();
        //        sfd.DefaultExt = ".csv";
        //        sfd.AddExtension = true;

        //        if (sfd.ShowDialog() == true)
        //        {
        //            StreamWriter compressionFile = new StreamWriter(sfd.FileName);
        //            Dictionary<string, CompressionData> clipFileCompression = new Dictionary<string, CompressionData>();

        //            for (int i = 0; i < files.Length; i++)
        //            {
        //                int compressed = 0;
        //                int uncompressed = 0;

        //                string dictionaryName = GenerateDictionaryName(files[i].ToString());
        //                if (dictionaryName == String.Empty) return;

        //                // Already have an entry for this dictionary?
        //                if (!clipFileCompression.ContainsKey(dictionaryName))
        //                {
        //                    if (!ParseMetadataFileForCompression(files[i].ToString(), out compressed, out uncompressed)) return;
        //                    CompressionData compressionData = new CompressionData(uncompressed, compressed);
        //                    clipFileCompression.Add(dictionaryName, compressionData);
        //                }
        //            }

        //            foreach (KeyValuePair<string, CompressionData> kvp in clipFileCompression)
        //            {
        //                CompressionData data = kvp.Value;
        //                string dictionaryEntry = string.Format("{0}, {1}, {2}", kvp.Key, data.compressedDataSize, data.uncompressedDataSize);
        //                compressionFile.WriteLine(dictionaryEntry);
        //            }

        //            compressionFile.Close();
        //        }
        //    }
        //}

        #endregion // Controller Methods

    } // UniversalLogViewModel
}
