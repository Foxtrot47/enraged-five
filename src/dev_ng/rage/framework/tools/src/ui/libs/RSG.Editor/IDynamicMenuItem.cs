﻿//---------------------------------------------------------------------------------------------
// <copyright file="IDynamicMenuItem.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System.ComponentModel;

    /// <summary>
    /// Provides a base class to objects representing a item for a
    /// <see cref="DynamicMenuCommand"/> definition object.
    /// </summary>
    public interface IDynamicMenuItem : INotifyPropertyChanged
    {
        #region Properties
        /// <summary>
        /// Gets the parameter that is routed through the element tree with the command.
        /// </summary>
        object CommandParameter { get; }

        /// <summary>
        /// Gets the parent command definition that this is an item for.
        /// </summary>
        DynamicMenuCommand ParentDefinition { get; }

        /// <summary>
        /// Gets or sets the text that is used to display this item.
        /// </summary>
        string Text { get; set; }
        #endregion Properties
    } // RSG.Editor.IDynamicMenuItem {Interface}
} // RSG.Editor {Namespace}
