﻿//---------------------------------------------------------------------------------------------
// <copyright file="UpdateValueFromTextChangeMode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;

    /// <summary>
    /// Defines the different ways a spinner controls value property updates when the text
    /// changes.
    /// </summary>
    [Flags]
    public enum UpdateValueFromTextChangeMode
    {
        /// <summary>
        /// The value will not update though text change events, only when the user uses other
        /// means of changing the value (i.e. the spinner buttons).
        /// </summary>
        None = 0,

        /// <summary>
        /// The value updates as soon as the text changes.
        /// </summary>
        RealTime = 1,

        /// <summary>
        /// The value updates when the user presses enter while the spinner control has
        /// keyboard focus or loses the keyboard focus.
        /// </summary>
        OnEnterPressed = 2,

        /// <summary>
        /// The value updates only when the spinner control loses keyboard focus.
        /// </summary>
        OnLostFocus = 4,

        /// <summary>
        /// The default mode for a new spinner control.
        /// </summary>
        Default = RealTime
    } // RSG.Editor.Controls.UpdateValueFromTextChangeMode {Enum}
} // RSG.Editor.Controls {Namespace}
