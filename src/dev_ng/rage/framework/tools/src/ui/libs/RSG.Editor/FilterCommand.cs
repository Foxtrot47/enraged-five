﻿//---------------------------------------------------------------------------------------------
// <copyright file="FilterCommand.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    /// <summary>
    /// Provides a base class to a definition of a filter command.
    /// </summary>
    public abstract class FilterCommand : MultiCommand
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="FilterCommand"/> class.
        /// </summary>
        protected FilterCommand()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether the individual items are selectable.
        /// </summary>
        public override bool CanItemsBeSelected
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating the selection mode style adopted by this instance.
        /// </summary>
        public override MultiCommandSelectionMode SelectionMode
        {
            get { return MultiCommandSelectionMode.Multiple; }
        }
        #endregion Properties
    } // RSG.Editor.FilterCommand {Class}
} // RSG.Editor {Namespace}
