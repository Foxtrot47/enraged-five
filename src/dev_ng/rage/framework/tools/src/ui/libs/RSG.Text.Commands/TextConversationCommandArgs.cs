﻿//---------------------------------------------------------------------------------------------
// <copyright file="TextConversationCommandArgs.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Commands
{
    using System.Collections.Generic;
    using System.Windows.Controls;
    using RSG.Text.ViewModel;

    /// <summary>
    /// A struct that contains the parameters needed for the text conversation commands to
    /// execute.
    /// </summary>
    public struct TextConversationCommandArgs
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="DataGrid"/> property.
        /// </summary>
        private DataGrid _dataGrid;

        /// <summary>
        /// The private field used for the <see cref="Dialogue"/> property.
        /// </summary>
        private DialogueViewModel _dialogue;

        /// <summary>
        /// The private field used for the <see cref="Selected"/> property.
        /// </summary>
        private IEnumerable<ConversationViewModel> _selected;

        private string _audioPath;

        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets or sets the data grid control containing the conversations.
        /// </summary>
        public DataGrid DataGrid
        {
            get { return this._dataGrid; }
            set { this._dataGrid = value; }
        }

        /// <summary>
        /// Gets or sets the dialogue that is currently selected.
        /// </summary>
        public DialogueViewModel Dialogue
        {
            get { return this._dialogue; }
            set { this._dialogue = value; }
        }

        /// <summary>
        /// Gets or sets the iterator over the currently selected conversations.
        /// </summary>
        public IEnumerable<ConversationViewModel> Selected
        {
            get { return this._selected; }
            set { this._selected = value; }
        }

        /// <summary>
        /// Gets or sets the Audio path.
        /// </summary>
        public string AudioPath
        {
            get { return this._audioPath; }
            set { this._audioPath = value; }
        }

        #endregion Properties
    } // RSG.Text.Commands.TextConversationCommandArgs {Structure}
} // RSG.Text.Commands {Namespace}
