﻿//---------------------------------------------------------------------------------------------
// <copyright file="IDisplayItemWithState.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    using System.Windows.Media.Imaging;

    /// <summary>
    /// When implemented represents a view model that acts as a single item that the view can
    /// render that includes properties that can be also to render the state of the item.
    /// </summary>
    public interface IDisplayItemWithState : IDisplayItem
    {
        #region Properties
        /// <summary>
        /// Gets the bitmap source that is used to display the overlay icon for this item.
        /// </summary>
        BitmapSource OverlayIcon { get; }

        /// <summary>
        /// Gets the bitmap source that is used to display the state icon for this item.
        /// </summary>
        BitmapSource StateIcon { get; }

        /// <summary>
        /// Gets the text that is used to represent the state of this item.
        /// </summary>
        string StateToolTipText { get; }
        #endregion Properties
    } // RSG.Editor.View.IDisplayItemWithState {Interface}
} // RSG.Editor.View {Namespace}
