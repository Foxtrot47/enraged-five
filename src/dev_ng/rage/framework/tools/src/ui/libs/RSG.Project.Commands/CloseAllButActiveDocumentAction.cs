﻿//---------------------------------------------------------------------------------------------
// <copyright file="CloseAllButActiveDocumentAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Commands
{
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using RSG.Editor;
    using RSG.Editor.Controls.Dock;
    using RSG.Editor.Controls.Dock.ViewModel;

    /// <summary>
    /// Contains the logic for the <see cref="DockingCommands.CloseAllButItem"/> routed
    /// command. This class cannot be inherited.
    /// </summary>
    public sealed class CloseAllButActiveDocumentAction : ButtonAction<ProjectCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CloseAllButActiveDocumentAction"/>
        /// class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public CloseAllButActiveDocumentAction(ProjectCommandResolver resolver)
            : base(new ParameterResolverDelegate<ProjectCommandArgs>(resolver))
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ProjectCommandArgs args)
        {
            if (args == null)
            {
                return false;
            }

            if (!args.OpenedDocuments.Any())
            {
                return false;
            }

            if (args.ViewSite.ActiveDocument == null)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(ProjectCommandArgs args)
        {
            IDocumentManagerService docService = this.GetService<IDocumentManagerService>();
            if (docService == null)
            {
                Debug.Fail(
"Unable to close all documents but the active one due to the fact the service is missing.");
                return;
            }

            List<DocumentItem> documents = docService.OpenedDocuments.ToList();
            documents.Remove(docService.ActiveDocument);
            docService.CloseDocuments(documents, true);
        }
        #endregion Methods
    } // RSG.Project.Commands.CloseAllButActiveDocumentAction {Class}
} // RSG.Project.Commands {Namespace}
