﻿//---------------------------------------------------------------------------------------------
// <copyright file="CharacterMappingsViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.ViewModel
{
    using System;
    using System.ComponentModel;
    using System.Windows.Data;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Editor.View;
    using RSG.Text.Model;

    public class CharacterMappingsViewModel : ViewModelBase<CharacterMappings>
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Characters"/> property.
        /// </summary>
        private CharacterCollection _characters;

        private DialogueViewModel _dialogue;

        /// <summary>
        /// The private field used for the <see cref="Conversations"/> property.
        /// </summary>
        private MappingCollection _mappings;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dialogue"></param>
        public CharacterMappingsViewModel(DialogueViewModel dialogue)
            : base(dialogue.Model.CharacterMappings)
        {
            this._dialogue = dialogue;
            this._mappings =
                new MappingCollection(dialogue.Model.CharacterMappings.Mappings, this);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the collection of characters that can be used by the lines inside this
        /// dialogue with each character maintaining their used state.
        /// </summary>
        public CharacterCollection Characters
        {
            get
            {
                if (this._characters == null)
                {
                    DialogueConfigurations configurations = this.Model.Configurations;
                    if (configurations == null)
                    {
                        this._characters =
                            new CharacterCollection(new ModelCollection<DialogueCharacter>());
                    }
                    else
                    {
                        this._characters = new CharacterCollection(configurations.Characters);
                    }

                    DispatcherHelper.CheckBeginInvokeOnUI(new Action(() =>
                    {
                        ICollectionView view =
                            CollectionViewSource.GetDefaultView(this._characters);
                        if (view != null)
                        {
                            view.GroupDescriptions.Add(new PropertyGroupDescription("IsUsed"));
                            view.SortDescriptions.Add(
                                new SortDescription("IsUsed", ListSortDirection.Descending));
                            view.SortDescriptions.Add(
                                new SortDescription("Name", ListSortDirection.Ascending));
                            view.Filter = this.FilterMappedCharacters;
                        }
                    }));
                }

                return this._characters;
            }
        }

        /// <summary>
        /// Gets the collection of conversations belonging to this dialogue.
        /// </summary>
        public IReadOnlyViewModelCollection<CharacterMappingViewModel> Mappings
        {
            get { return this._mappings; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool FilterMappedCharacters(object item)
        {
            DialogueCharacterViewModel character = item as DialogueCharacterViewModel;
            if (character == null)
            {
                return true;
            }

            return !this._dialogue.Model.CharacterMappings.IsCharacterMapped(character.Id);
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// A collection class specifically for the use of the Character Mappings View Model
        /// for its mappings collection.
        /// </summary>
        public class MappingCollection
            : CollectionConverter<CharacterMapping, CharacterMappingViewModel>,
            IReadOnlyViewModelCollection<CharacterMappingViewModel>
        {
            #region Fields
            /// <summary>
            /// The mappings view model that owns this collection.
            /// </summary>
            private CharacterMappingsViewModel _owner;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="MappingCollection"/> class.
            /// </summary>
            /// <param name="source">
            /// The source model items for this collection.
            /// </param>
            public MappingCollection(
                ReadOnlyModelCollection<CharacterMapping> source,
                CharacterMappingsViewModel owner)
            {
                this._owner = owner;
                this.ResetSourceItems(source);
            }
            #endregion Constructors

            #region Methods
            /// <summary>
            /// Converts the single specified character mapping to a new instance of the
            /// character mapping view model class.
            /// </summary>
            /// <param name="item">
            /// The item to convert.
            /// </param>
            /// <returns>
            /// A new instance of the character mapping view model class created from the
            /// specified character mapping.
            /// </returns>
            protected override CharacterMappingViewModel ConvertItem(CharacterMapping item)
            {
                return new CharacterMappingViewModel(item, this._owner);
            }
            #endregion Methods
        } // CharacterMappingsViewModel.MappingCollection {Class}
        #endregion Classes
    }
}
