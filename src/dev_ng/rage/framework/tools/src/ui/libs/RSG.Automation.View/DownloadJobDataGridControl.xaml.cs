﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Automation.ViewModel;
using RSG.Editor;
using RSG.Editor.Controls;

namespace RSG.Automation.View
{
    /// <summary>
    /// Interaction logic for DownloadJobDataGridControl.xaml
    /// </summary>
    public partial class DownloadJobDataGridControl : RsUserControl
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<JobDownloadViewModel> SelectedItems
        {
            get
            {
                return (IEnumerable<JobDownloadViewModel>)this.GetValue(SelectedItemsProperty);
            }

            set
            {
                throw new Exception("An attempt to modify a Read-Only property was made.");
            }
        }

        /// <summary>
        /// Gets or sets the dependency property for the <see cref="SelectedItems"/> property.
        /// </summary>
        public static DependencyProperty SelectedItemsProperty
        {
            get { return _selectedItemsProperty; }
            set { _selectedItemsProperty = value; }
        }
        private static DependencyProperty _selectedItemsProperty;

        /// <summary>
        /// Gets or sets a collection used to generate the content of this control.
        /// </summary>
        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)this.GetValue(ItemsControl.ItemsSourceProperty); }
            set { this.SetValue(ItemsControl.ItemsSourceProperty, value); }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Static constructor.
        /// </summary>
        static DownloadJobDataGridControl()
        {
            SelectedItemsProperty = DependencyProperty.Register("SelectedItems",
                typeof(IEnumerable<JobDownloadViewModel>),
                typeof(DownloadJobDataGridControl),
                new FrameworkPropertyMetadata(Enumerable.Empty<JobDownloadViewModel>()));
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public DownloadJobDataGridControl()
        {
            InitializeComponent();
            AttachCommandBindings();
        }
        #endregion // Constructor(s)

        #region Private methods
        /// <summary>
        /// Represents the method that is used to retrieve a command parameter object for the
        /// view commands.
        /// </summary>
        /// <param name="command">
        /// The command whose command parameter needs to be resolved.
        /// </param>
        /// <returns>
        /// The command parameter object for the specified command.
        /// </returns>
        private DownloadJobDataGridControl ControlResolver(CommandData data)
        {
            return this;
        }

        /// <summary>
        /// Attaches all of the necessary command bindings to this instance.
        /// </summary>
        private void AttachCommandBindings()
        {
        }

        /// <summary>
        /// Event handler for Download Job DataGrid selection changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataGrid_SelectionChanged(Object sender, SelectionChangedEventArgs e)
        {
            Debug.Assert(this.DataContext is AutomationMonitorDataContext);
            this.SetValue(SelectedItemsProperty,
                this.DownloadJobDataGrid.SelectedItems.OfType<JobDownloadViewModel>());
        }
        #endregion // Private Methods
    }
}
