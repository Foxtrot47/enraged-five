﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor;
using RSG.Automation.ViewModel;

namespace RSG.Automation.Commands
{
    public class SkipJobAction : ButtonAction<AutomationMonitorDataContext>
    {
        #region Constructors(s)
        /// <summary>
        /// Download a job's universal log file action
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public SkipJobAction(ParameterResolverDelegate<AutomationMonitorDataContext> resolver)
            : base(resolver)
        {
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// Override to set logic against the can execute command handler.
        /// </summary>
        /// <param name="commandParameter"></param>
        /// <returns></returns>
        public override bool CanExecute(AutomationMonitorDataContext dc)
        {
            return dc.CanSkipSelectedJobs();
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="commandParameter">
        /// The command parameter that has been requested.
        /// </param>
        public override async void Execute(AutomationMonitorDataContext dc)
        {
            if (dc.CanSkipSelectedJobs())
            {
                await dc.OnSkipJobs();
            }
        }
        #endregion // Overrides
    } // SkipJobAction
}
