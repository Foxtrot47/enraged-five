﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsToolBar.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Commands
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Interop;
    using RSG.Editor.Controls.Helpers;

    /// <summary>
    /// Provides a container for a group of commands and controls.
    /// </summary>
    public class RsToolBar : ToolBar
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="CornerRadius"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty CornerRadiusProperty;

        /// <summary>
        /// Identifies the <see cref="ShowHeader"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ShowHeaderProperty;

        /// <summary>
        /// Identifies the <see cref="IsInsideTray"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsInsideTrayProperty;

        /// <summary>
        /// The private field used for the <see cref="BatchStyleKey"/> property.
        /// </summary>
        private static ResourceKey _batchStyleKey = CreateStyleKey("BatchStyleKey");

        /// <summary>
        /// The private field used for the <see cref="CommandButtonStyleKey"/> property.
        /// </summary>
        private static ResourceKey _buttonStyleKey = CreateStyleKey("ButtonStyleKey");

        /// <summary>
        /// The private field used for the <see cref="CommandComboBoxStyleKey"/> property.
        /// </summary>
        private static ResourceKey _comboBoxStyleKey = CreateStyleKey("ComboBoxStyleKey");

        /// <summary>
        /// The private field used for the <see cref="ControllerStyleKey"/> property.
        /// </summary>
        private static ResourceKey _controllerStyleKey = CreateStyleKey("ControllerStyleKey");

        /// <summary>
        /// The private field used for the <see cref="FilterComboStyleKey"/> property.
        /// </summary>
        private static ResourceKey _filterComboStyleKey =
            CreateStyleKey("FilterComboStyleKey");

        /// <summary>
        /// The private field used for the <see cref="CommandMenuStyleKey"/> property.
        /// </summary>
        private static ResourceKey _menuStyleKey = CreateStyleKey("MenuStyleKey");

        /// <summary>
        /// The private field used for the <see cref="SearchStyleKey"/> property.
        /// </summary>
        private static ResourceKey _searchStyleKey = CreateStyleKey("SearchStyleKey");

        /// <summary>
        /// The private field used for the <see cref="SeparatorStyleKey"/> property.
        /// </summary>
        private static ResourceKey _separatorStyleKey = CreateStyleKey("SeparatorStyleKey");

        /// <summary>
        /// The private field used for the <see cref="TextStyleKey"/> property.
        /// </summary>
        private static ResourceKey _textStyleKey = CreateStyleKey("TextStyleKey");

        /// <summary>
        /// The private field used for the <see cref="ToggleStyleKey"/> property.
        /// </summary>
        private static ResourceKey _toggleStyleKey = CreateStyleKey("ToggleStyleKey");
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsToolBar"/> class.
        /// </summary>
        static RsToolBar()
        {
            CornerRadiusProperty =
                DependencyProperty.Register(
                "CornerRadius",
                typeof(CornerRadius),
                typeof(RsToolBar),
                new FrameworkPropertyMetadata(new CornerRadius(0.0)));

            ShowHeaderProperty =
                DependencyProperty.Register(
                "ShowHeader",
                typeof(bool),
                typeof(RsToolBar),
                new FrameworkPropertyMetadata(true));

            IsInsideTrayProperty =
                DependencyProperty.Register(
                "IsInsideTray",
                typeof(bool),
                typeof(RsToolBar),
                new FrameworkPropertyMetadata(true));

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsToolBar),
                new FrameworkPropertyMetadata(typeof(RsToolBar)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsToolBar"/> class.
        /// </summary>
        public RsToolBar()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the resource key used to reference the style used on batch command items.
        /// </summary>
        public static ResourceKey BatchStyleKey
        {
            get { return _batchStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on button command items.
        /// </summary>
        public static ResourceKey CommandButtonStyleKey
        {
            get { return _buttonStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on combo box command items.
        /// </summary>
        public static ResourceKey CommandComboBoxStyleKey
        {
            get { return _comboBoxStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on controller command items.
        /// </summary>
        public static ResourceKey ControllerStyleKey
        {
            get { return _controllerStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on menu command items.
        /// </summary>
        public static ResourceKey CommandMenuStyleKey
        {
            get { return _menuStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on filter combo command
        /// items.
        /// </summary>
        public static ResourceKey FilterComboStyleKey
        {
            get { return _filterComboStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on search command items.
        /// </summary>
        public static ResourceKey SearchStyleKey
        {
            get { return _searchStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on separator command items.
        /// </summary>
        public static new ResourceKey SeparatorStyleKey
        {
            get { return _separatorStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on text command items.
        /// </summary>
        public static ResourceKey TextStyleKey
        {
            get { return _textStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on toggle command items.
        /// </summary>
        public static ResourceKey ToggleStyleKey
        {
            get { return _toggleStyleKey; }
        }

        /// <summary>
        /// Gets or sets the brush that describes the fill of the checkmark used for this
        /// control.
        /// </summary>
        public CornerRadius CornerRadius
        {
            get { return (CornerRadius)this.GetValue(CornerRadiusProperty); }
            set { this.SetValue(CornerRadiusProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the tool bar is positioned inside the tool
        /// bar tray. This controls whether the thumb and quick access menu is shown.
        /// </summary>
        public bool IsInsideTray
        {
            get { return (bool)this.GetValue(IsInsideTrayProperty); }
            set { this.SetValue(IsInsideTrayProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the header should be visible on the tool
        /// bar before any of the items appear.
        /// </summary>
        public bool ShowHeader
        {
            get { return (bool)this.GetValue(ShowHeaderProperty); }
            set { this.SetValue(ShowHeaderProperty, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates or identifies the element that is used to display items within this
        /// control.
        /// </summary>
        /// <returns>
        /// The element that is used to display items within this control.
        /// </returns>
        protected override DependencyObject GetContainerForItemOverride()
        {
            RsToolBarItem newItem = new RsToolBarItem();
            return newItem;
        }

        /// <summary>
        /// Prepares the specified element to display the specified item.
        /// </summary>
        /// <param name="element">
        /// Element used to display the specified item.
        /// </param>
        /// <param name="item">
        /// Specified item.
        /// </param>
        protected override void PrepareContainerForItemOverride(
            DependencyObject element, object item)
        {
            CommandBarItemViewModel viewmodel = item as CommandBarItemViewModel;
            if (viewmodel == null)
            {
                base.PrepareContainerForItemOverride(element, item);
                return;
            }

            FrameworkElement frameworkElement = element as FrameworkElement;
            ResourceKey resource = null;
            switch (viewmodel.DisplayType)
            {
                case CommandItemDisplayType.Button:
                    resource = RsToolBar.CommandButtonStyleKey;
                    break;
                case CommandItemDisplayType.Combo:
                    resource = RsToolBar.CommandComboBoxStyleKey;
                    break;
                case CommandItemDisplayType.Toggle:
                    resource = RsToolBar.ToggleStyleKey;
                    break;
                case CommandItemDisplayType.Separator:
                    resource = RsToolBar.SeparatorStyleKey;
                    break;
                case CommandItemDisplayType.FilterCombo:
                    resource = RsToolBar.FilterComboStyleKey;
                    break;
                case CommandItemDisplayType.Search:
                    resource = RsToolBar.SearchStyleKey;
                    break;
            }

            if (resource == null)
            {
                return;
            }

            frameworkElement.SetResourceReference(FrameworkElement.StyleProperty, resource);
        }

        /// <summary>
        /// Creates a new resource key that references a style with the specified name.
        /// </summary>
        /// <param name="styleName">
        /// The name that the resource key will be referencing.
        /// </param>
        /// <returns>
        /// A new resource key that references a style with the specified name.
        /// </returns>
        private static ResourceKey CreateStyleKey(string styleName)
        {
            return new StyleKey<RsToolBar>(styleName);
        }
        #endregion Methods
    } // RSG.Editor.Controls.Commands.RsToolBar {Class}
} // RSG.Editor.Controls.Commands {Namespace}
