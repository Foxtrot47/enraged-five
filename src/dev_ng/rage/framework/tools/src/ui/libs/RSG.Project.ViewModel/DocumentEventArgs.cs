﻿//---------------------------------------------------------------------------------------------
// <copyright file="DocumentEventArgs.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel
{
    using System;
    using RSG.Editor.Controls.Dock.ViewModel;

    /// <summary>
    /// Provides data for the event representing a document object being closed by the user.
    /// </summary>
    public class DocumentEventArgs : EventArgs
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="DocumentItem"/> property.
        /// </summary>
        private DocumentItem _documentItem;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DocumentEventArgs"/> class.
        /// </summary>
        /// <param name="documentItem">
        /// The document that is being closed.
        /// </param>
        public DocumentEventArgs(DocumentItem documentItem)
        {
            this._documentItem = documentItem;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the document item that is being closed.
        /// </summary>
        public DocumentItem DocumentItem
        {
            get { return this._documentItem; }
        }
        #endregion Properties
    } // RSG.Project.ViewModel.DocumentEventArgs {Class}
} // RSG.Project.ViewModel {Namespace}
