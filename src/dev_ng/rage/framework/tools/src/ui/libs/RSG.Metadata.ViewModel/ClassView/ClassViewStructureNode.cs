﻿//---------------------------------------------------------------------------------------------
// <copyright file="ClassViewStructureNode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.ClassView
{
    using System.Collections.Generic;
    using RSG.Metadata.Model.Definitions;
    using RSG.Metadata.ViewModel.Project;

    /// <summary>
    /// The node inside the class view that represents a standard metadata project. This class
    /// cannot be inherited.
    /// </summary>
    public sealed class ClassViewStructureNode : ClassViewNodeBase
    {
        #region Fields
        /// <summary>
        /// The structure that this node is representing.
        /// </summary>
        private IStructure _structure;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ClassViewStructureNode"/> class.
        /// </summary>
        /// <param name="structure">
        /// The structure object this node is representing.
        /// </param>
        /// <param name="parent">
        /// The parent node.
        /// </param>
        public ClassViewStructureNode(IStructure structure, ClassViewNodeBase parent)
            : base(parent)
        {
            this.IsExpandable = structure.BaseStructure != null;
            this._structure = structure;
            this.Icon = Icons.StructNodeIcon;
            this.Text = structure.ShortDataType;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the priority value for this object.
        /// </summary>
        public override int Priority
        {
            get { return 1; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates the child elements for this view model. This is only called once the item
        /// has been expanded in the user interface.
        /// </summary>
        /// <param name="collection">
        /// The collection to add the elements to.
        /// </param>
        protected override void CreateChildren(ICollection<object> collection)
        {
            ClassViewProjectNode project = this.ParentProjectNode;
            if (project == null)
            {
                return;
            }

            project.StandardProject.EnsureDefinitionsAreLoaded();
            IStructure baseStructure = this._structure.BaseStructure;
            if (baseStructure != null)
            {
                collection.Add(new ClassViewStructureNode(baseStructure, this));
            }
        }
        #endregion Methods
    } // RSG.Metadata.ViewModel.ClassView.ClassViewStructureNode {Class}
} // RSG.Metadata.ViewModel.ClassView {Namespace}
