﻿//---------------------------------------------------------------------------------------------
// <copyright file="GlowBitmapPart.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Media
{
    /// <summary>
    /// Defines the different parts of a glow bitmap object.
    /// </summary>
    internal enum GlowBitmapPart
    {
        /// <summary>
        /// Defines the top left corner of the bitmap.
        /// </summary>
        CornerTopLeft = 0,

        /// <summary>
        /// Defines the top right corner of the bitmap.
        /// </summary>
        CornerTopRight = 1,

        /// <summary>
        /// Defines the bottom left corner of the bitmap.
        /// </summary>
        CornerBottomLeft = 2,

        /// <summary>
        /// Defines the bottom right corner of the bitmap.
        /// </summary>
        CornerBottomRight = 3,

        /// <summary>
        /// Defines the top left of the bitmap.
        /// </summary>
        TopLeft = 4,

        /// <summary>
        /// Defines the top of the bitmap.
        /// </summary>
        Top = 5,

        /// <summary>
        /// Defines the top right of the bitmap.
        /// </summary>
        TopRight = 6,

        /// <summary>
        /// Defines the left top of the bitmap.
        /// </summary>
        LeftTop = 7,

        /// <summary>
        /// Defines the left of the bitmap.
        /// </summary>
        Left = 8,

        /// <summary>
        /// Defines the left bottom of the bitmap.
        /// </summary>
        LeftBottom = 9,

        /// <summary>
        /// Defines the bottom left of the bitmap.
        /// </summary>
        BottomLeft = 10,

        /// <summary>
        /// Defines the bottom of the bitmap.
        /// </summary>
        Bottom = 11,

        /// <summary>
        /// Defines the bottom right of the bitmap.
        /// </summary>
        BottomRight = 12,

        /// <summary>
        /// Defines the right top of the bitmap.
        /// </summary>
        RightTop = 13,

        /// <summary>
        /// Defines the right of the bitmap.
        /// </summary>
        Right = 14,

        /// <summary>
        /// Defines the right bottom of the bitmap.
        /// </summary>
        RightBottom = 15,

        /// <summary>
        /// The number of different parts that make up a glow bitmap.
        /// </summary>
        Count = 16
    } // RSG.Editor.Controls.Media.GlowBitmapPart {Enum}
} // RSG.Editor.Controls.Media {Namespace}
