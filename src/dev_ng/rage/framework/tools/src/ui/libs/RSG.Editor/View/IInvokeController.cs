﻿//---------------------------------------------------------------------------------------------
// <copyright file="IInvokeController.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    using System.Collections.Generic;
    using System.Windows;

    /// <summary>
    /// When implemented represents a class that can be used to invoke a specific action on
    /// a collection of objects from a input source.
    /// </summary>
    public interface IInvokeController
    {
        #region Methods
        /// <summary>
        /// Invokes a action on each item in the specified list.
        /// </summary>
        /// <param name="items">
        /// A list of items that should be invoked.
        /// </param>
        /// <param name="inputSource">
        /// The source of the input that has caused this invoke.
        /// </param>
        /// <param name="element">
        /// The input element that was the source for this invoke.
        /// </param>
        /// <returns>
        /// True if any of the specified items have been invoked successfully.
        /// </returns>
        bool Invoke(IEnumerable<object> items, InputSource inputSource, IInputElement element);
        #endregion Methods
    } // RSG.Editor.View.IInvokeController {Interface}
} // RSG.Editor.View {Namespace}
