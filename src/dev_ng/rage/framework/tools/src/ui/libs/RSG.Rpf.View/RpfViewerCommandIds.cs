﻿//---------------------------------------------------------------------------------------------
// <copyright file="RpfViewerCommandIds.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Rpf.View
{
    using System;

    /// <summary>
    /// Contains global identifiers that are used throughout the RPF viewer control.
    /// </summary>
    public static class RpfViewerCommandIds
    {
        #region Fields
        /// <summary>
        /// The private instance used for the <see cref="ContextMenu"/> property.
        /// </summary>
        private static Guid? _contextMenu;

        /// <summary>
        /// The private instance used for the <see cref="FilterMenu"/> property.
        /// </summary>
        private static Guid? _filterMenu;

        /// <summary>
        /// A private instance to a generic object used to support thread access.
        /// </summary>
        private static object _syncRoot = new object();
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets the global identifier used for the context menu for the individual items in
        /// the RPF viewer list.
        /// </summary>
        public static Guid ContextMenu
        {
            get
            {
                if (!_contextMenu.HasValue)
                {
                    lock (_syncRoot)
                    {
                        if (!_contextMenu.HasValue)
                        {
                            _contextMenu = new Guid("41A87EDB-53DC-4259-B763-5BE241982DEB");
                        }
                    }
                }

                return _contextMenu.Value;
            }
        }

        /// <summary>
        /// Gets the global identifier used for the filter menu on top of the items control in
        /// the RPF viewer control.
        /// </summary>
        public static Guid FilterMenu
        {
            get
            {
                if (!_filterMenu.HasValue)
                {
                    lock (_syncRoot)
                    {
                        if (!_filterMenu.HasValue)
                        {
                            _filterMenu = new Guid("E6572DC5-0AE9-4042-BCF9-483EE0801FAE");
                        }
                    }
                }

                return _filterMenu.Value;
            }
        }
        #endregion Properties
    } // RSG.Rpf.View.RpfViewerCommandIds {Class}
} // RSG.Rpf.View {Namespace}
