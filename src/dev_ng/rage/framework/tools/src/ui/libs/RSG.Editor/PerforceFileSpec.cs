﻿//---------------------------------------------------------------------------------------------
// <copyright file="PerforceFileSpec.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Contains a perforce file specification that can be represented as either a
    /// client, depot or local path.
    /// </summary>
    public class PerforceFileSpec : IPerforceFileSpec
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="ClientPath"/> property.
        /// </summary>
        private readonly string _clientPath;

        /// <summary>
        /// Private field for the <see cref="DepotPath"/> property.
        /// </summary>
        private readonly string _depotPath;

        /// <summary>
        /// Private field for the <see cref="LocalPath"/> property.
        /// </summary>
        private readonly string _localPath;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="PerforceFileSpec"/> class using
        /// the specified client, depot and local paths.
        /// </summary>
        /// <param name="clientPath"></param>
        /// <param name="depotPath"></param>
        /// <param name="localPath"></param>
        private PerforceFileSpec(string clientPath, string depotPath, string localPath)
        {
            _clientPath = clientPath;
            _depotPath = depotPath;
            _localPath = localPath;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the value that represents the files client path.
        /// </summary>
        public string ClientPath
        {
            get { return _clientPath; }
        }

        /// <summary>
        /// Gets the value that represents the files perforce depot path.
        /// </summary>
        public string DepotPath
        {
            get { return _depotPath; }
        }

        /// <summary>
        /// Gets the value that represents the files local path.
        /// </summary>
        public string LocalPath
        {
            get { return _localPath; }
        }
        #endregion

        #region Static Creation Methods
        /// <summary>
        /// Creates a list of <see cref="PerforceFileSpec"/> objects from a list of
        /// client paths.
        /// </summary>
        /// <param name="paths"></param>
        /// <returns></returns>
        public static IList<IPerforceFileSpec> ClientSpecList(params string[] paths)
        {
            return ClientSpecList((IEnumerable<string>)paths);
        }

        /// <summary>
        /// Creates a list of <see cref="PerforceFileSpec"/> objects from a list of
        /// client paths.
        /// </summary>
        /// <param name="paths"></param>
        /// <returns></returns>
        public static IList<IPerforceFileSpec> ClientSpecList(IEnumerable<string> paths)
        {
            IList<IPerforceFileSpec> fileSpecs = new List<IPerforceFileSpec>();
            foreach (string path in paths)
            {
                fileSpecs.Add(new PerforceFileSpec(path, null, null));
            }
            return fileSpecs;
        }

        /// <summary>
        /// Creates a list of <see cref="PerforceFileSpec"/> objects from a list of
        /// depot paths.
        /// </summary>
        /// <param name="paths"></param>
        /// <returns></returns>
        public static IList<IPerforceFileSpec> DepotSpecList(params string[] paths)
        {
            return DepotSpecList((IEnumerable<string>)paths);
        }
        
        /// <summary>
        /// Creates a list of <see cref="PerforceFileSpec"/> objects from a list of
        /// depot paths.
        /// </summary>
        /// <param name="paths"></param>
        /// <returns></returns>
        public static IList<IPerforceFileSpec> DepotSpecList(IEnumerable<string> paths)
        {
            IList<IPerforceFileSpec> fileSpecs = new List<IPerforceFileSpec>();
            foreach (string path in paths)
            {
                fileSpecs.Add(new PerforceFileSpec(null, path, null));
            }
            return fileSpecs;
        }
        
        /// <summary>
        /// Creates a list of <see cref="PerforceFileSpec"/> objects from a list of
        /// local paths.
        /// </summary>
        /// <param name="paths"></param>
        /// <returns></returns>
        public static IList<IPerforceFileSpec> LocalSpecList(params string[] paths)
        {
            return LocalSpecList((IEnumerable<string>)paths);
        }

        /// <summary>
        /// Creates a list of <see cref="PerforceFileSpec"/> objects from a list of
        /// local paths.
        /// </summary>
        /// <param name="paths"></param>
        /// <returns></returns>
        public static IList<IPerforceFileSpec> LocalSpecList(IEnumerable<string> paths)
        {
            IList<IPerforceFileSpec> fileSpecs = new List<IPerforceFileSpec>();
            foreach (string path in paths)
            {
                fileSpecs.Add(new PerforceFileSpec(null, null, path));
            }
            return fileSpecs;
        }
        #endregion
    }
}
