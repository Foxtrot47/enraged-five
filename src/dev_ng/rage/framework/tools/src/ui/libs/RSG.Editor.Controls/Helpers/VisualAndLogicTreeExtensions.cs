﻿//---------------------------------------------------------------------------------------------
// <copyright file="VisualAndLogicTreeExtensions.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Helpers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Media3D;

    /// <summary>
    /// Static class used to add extensions onto dependency objects to do with the visual tree.
    /// </summary>
    public static class VisualAndLogicTreeExtensions
    {
        #region Methods
        /// <summary>
        /// Finds the first occurrence of the qualifying type by looking up through the
        /// visual/logic tree.
        /// </summary>
        /// <typeparam name="T">
        /// The type of visual that you want to find  in the visual/logic tree.
        /// </typeparam>
        /// <param name="d">
        /// The dependency object to use as the starting root visual for the search.
        /// </param>
        /// <returns>
        /// A instance to the first occurrence of the qualifying type.
        /// </returns>
        public static T GetVisualOrLogicalAncestor<T>(this DependencyObject d)
            where T : DependencyObject
        {
            return d.GetVisualOrLogicalAncestors<T>(false).FirstOrDefault();
        }

        /// <summary>
        /// Finds the first occurrence of the qualifying type by looking up through the
        /// visual/logic tree.
        /// </summary>
        /// <typeparam name="T">
        /// The type of visual that you want to find  in the visual/logic tree.
        /// </typeparam>
        /// <param name="d">
        /// The dependency object to use as the starting root visual for the search.
        /// </param>
        /// <returns>
        /// A instance to the first occurrence of the qualifying type.
        /// </returns>
        public static T GetVisualOrLogicalAncestorOrSelf<T>(this DependencyObject d)
            where T : DependencyObject
        {
            return d.GetVisualOrLogicalAncestors<T>(true).FirstOrDefault();
        }

        /// <summary>
        /// Finds the first occurrence of a dependency object by looking up through the
        /// visual/logic tree.
        /// </summary>
        /// <param name="d">
        /// The dependency object to use as the starting root visual for the search.
        /// </param>
        /// <returns>
        /// A instance to the first occurrence.
        /// </returns>
        public static DependencyObject GetVisualOrLogicalAncestorOrSelf(
            this DependencyObject d)
        {
            return d.GetVisualOrLogicalAncestors(true).FirstOrDefault();
        }

        /// <summary>
        /// Creates an Enumerable that can be used to get all or loop through all instances of
        /// a specific type in the visual/logic tree above this dependency object.
        /// </summary>
        /// <typeparam name="T">
        /// The type of visual that you want to find in the visual/logic tree.
        /// </typeparam>
        /// <param name="d">
        /// The dependency object to use as the starting root visual for the search.
        /// </param>
        /// <param name="self">
        /// A value indicating whether the specified root should be included in the search.
        /// </param>
        /// <returns>
        /// Returns an Enumerable that can be used to get all or loop through all instances of
        /// a specific type in the visual/logic tree above this dependency object.
        /// </returns>
        public static IEnumerable<T> GetVisualOrLogicalAncestors<T>(
            this DependencyObject d, bool self) where T : DependencyObject
        {
            if (d == null)
            {
                yield break;
            }

            if (self)
            {
                if (d is T)
                {
                    yield return (T)d;
                }
            }

            DependencyObject parent = d.GetVisualOrLogicalAncestor();
            if (parent != null)
            {
                if (parent is T)
                {
                    yield return (T)parent;
                }

                foreach (T match in GetVisualOrLogicalAncestors<T>(parent, false))
                {
                    yield return match;
                }
            }

            yield break;
        }

        /// <summary>
        /// Creates an Enumerable that can be used to get all or loop through all dependency
        /// objects in the visual/logic tree above this dependency object.
        /// </summary>
        /// <param name="d">
        /// The dependency object to use as the starting root visual for the search.
        /// </param>
        /// <param name="self">
        /// A value indicating whether the specified root should be included in the search.
        /// </param>
        /// <returns>
        /// Returns an Enumerable that can be used to get all or loop through all dependency
        /// objects in the visual/logic tree above this dependency object.
        /// </returns>
        public static IEnumerable<DependencyObject> GetVisualOrLogicalAncestors(
            this DependencyObject d, bool self)
        {
            if (d == null)
            {
                yield break;
            }

            if (self)
            {
                yield return d;
            }

            DependencyObject parent = d.GetVisualOrLogicalAncestor();
            if (parent != null)
            {
                yield return parent;
                foreach (DependencyObject match in GetVisualOrLogicalAncestors(parent, false))
                {
                    yield return match;
                }
            }

            yield break;
        }

        /// <summary>
        /// Finds the first dependency object by looking up the visual tree and if nothing
        /// is found looks up the logical tree.
        /// </summary>
        /// <param name="d">
        /// The dependency object to use as the starting root visual for the search.
        /// </param>
        /// <returns>
        /// A instance to the first occurrence of a dependency object if found; otherwise null.
        /// </returns>
        public static DependencyObject GetVisualOrLogicalAncestor(this DependencyObject d)
        {
            if (d == null)
            {
                return null;
            }

            if (d is Visual || d is Visual3D)
            {
                DependencyObject parent = VisualTreeHelper.GetParent(d);
                if (parent != null)
                {
                    return parent;
                }
            }

            return LogicalTreeHelper.GetParent(d);
        }
        #endregion Methods
    } // RSG.Editor.Controls.Helpers.VisualAndLogicTreeExtensions {Class}
} // RSG.Editor.Controls.Helpers {Namespace}
