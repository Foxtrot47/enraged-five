﻿//---------------------------------------------------------------------------------------------
// <copyright file="LineViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.ViewModel
{
    using System;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Windows.Data;
    using RSG.Editor.Model;
    using RSG.Editor.View;
    using RSG.Text.Model;

    /// <summary>
    /// The view model the represents a <see cref="RSG.Text.Model.ILine"/> interface instance.
    /// </summary>
    public class LineViewModel : ValidatingViewModelBase<ILine>
    {
        #region Fields
        /// <summary>
        /// The reference to the parent conversation.
        /// </summary>
        private ConversationViewModel _conversation;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="LineViewModel"/> class.
        /// </summary>
        /// <param name="line">
        /// The <see cref="RSG.Text.Model.ILine"/> instance this view model is wrapping.
        /// </param>
        /// <param name="conversation">
        /// The conversation that owns this line.
        /// </param>
        public LineViewModel(ILine line, ConversationViewModel conversation)
            : base(line, true, true)
        {
            this._conversation = conversation;
            DialogueCharacterViewModel selectedCharacter = this.SelectedCharacter;
            if (selectedCharacter != null)
            {
                selectedCharacter.UsedCount++;
            }

            line.CharacterIdChanged += this.CharacterIdChanged;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether this line always gets subtitled even when
        /// they are turned off.
        /// </summary>
        public bool AlwaysSubtitled
        {
            get { return this.Model.AlwaysSubtitled; }
            set { this.Model.AlwaysSubtitled = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public ModelCollection<DialogueAudibility> Audibilities
        {
            get { return this.Model.Configurations.Audibilities; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DialogueAudibility Audibility
        {
            get
            {
                return this.Model.Configurations.GetAudibility(this.Model.Audibility);
            }

            set
            {
                if (value != null)
                {
                    this.Model.Audibility = value.Id;
                }
                else
                {
                    this.Model.Audibility = Guid.Empty;
                }
            }
        }

        /// <summary>
        /// Gets or sets the file path to the audio file containing the data for this line.
        /// </summary>
        public string AudioFilepath
        {
            get { return this.Model.AudioFilepath; }
            set { this.Model.AudioFilepath = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DialogueAudioType AudioType
        {
            get
            {
                return this.Model.Configurations.GetAudioType(this.Model.AudioType);
            }

            set
            {
                if (value != null)
                {
                    this.Model.AudioType = value.Id;
                }
                else
                {
                    this.Model.AudioType = Guid.Empty;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// 
        public ModelCollection<DialogueAudioType> AudioTypes
        {
            get { return this.Model.Configurations.AudioTypes; }
        }

        /// <summary>
        /// Gets the collection of characters that can be used by this line.
        /// </summary>
        public IReadOnlyViewModelCollection<DialogueCharacterViewModel> Characters
        {
            get { return this._conversation.Characters; }
        }

        /// <summary>
        /// Gets the parent conversation view model for this line.
        /// </summary>
        public ConversationViewModel Conversation
        {
            get { return this._conversation; }
        }

        /// <summary>
        /// Gets or sets the actual dialogue for this line.
        /// </summary>
        public string Dialogue
        {
            get { return this.Model.Dialogue; }
            set { this.Model.Dialogue = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not the audio for this line can be
        /// interrupted by the special ability.
        /// </summary>
        public bool DontInterruptForSpecialAbility
        {
            get { return this.Model.DontInterruptForSpecialAbility; }
            set { this.Model.DontInterruptForSpecialAbility = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not this line ducks the radio.
        /// </summary>
        public bool DucksRadio
        {
            get { return this.Model.DucksRadio; }
            set { this.Model.DucksRadio = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not this line ducks the score.
        /// </summary>
        public bool DucksScore
        {
            get { return this.Model.DucksScore; }
            set { this.Model.DucksScore = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not this line goes through the headset
        /// sub-mix.
        /// </summary>
        public bool HeadsetSubmix
        {
            get { return this.Model.HeadsetSubmix; }
            set { this.Model.HeadsetSubmix = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the audio for this line can be interrupt.
        /// </summary>
        public bool Interruptible
        {
            get { return this.Model.Interruptible; }
            set { this.Model.Interruptible = value; }
        }

        /// <summary>
        /// Gets the the date and time this line was last modified.
        /// </summary>
        public DateTime LastModifiedTime
        {
            get { return this.Model.LastModifiedTime; }
        }

        /// <summary>
        /// Gets or sets the index value of the listener of this line.
        /// </summary>
        public int Listener
        {
            get { return this.Model.Listener; }
            set { this.Model.Listener = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this line should be spoken out of the pad
        /// speaker.
        /// </summary>
        public bool PadSpeaker
        {
            get { return this.Model.PadSpeaker; }
            set { this.Model.PadSpeaker = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the filename for this line is read-only and
        /// needs auto generating or can be entered manually.
        /// </summary>
        public bool ReadOnlyFilename
        {
            get { return !this.Model.ManualFilename; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this line has been recorded.
        /// </summary>
        public bool Recorded
        {
            get { return this.Model.Recorded; }
            set { this.Model.Recorded = value; }
        }

        /// <summary>
        /// Gets or sets the character that is speaking this line.
        /// </summary>
        public DialogueCharacterViewModel SelectedCharacter
        {
            get
            {
                return this._conversation.Characters.GetCharacter(this.Model.CharacterId);
            }

            set
            {
                if (value != null)
                {
                    this.Model.CharacterId = value.Id;
                }
                else
                {
                    this.Model.CharacterId = Guid.Empty;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this line has been sent to be recorded.
        /// </summary>
        public bool SentForRecording
        {
            get { return this.Model.SentForRecording; }
            set { this.Model.SentForRecording = value; }
        }

        /// <summary>
        /// Gets the index value of the speaker for this line.
        /// </summary>
        public int Speaker
        {
            get { return this.Model.Speaker; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DialogueSpecial Special
        {
            get
            {
                return this.Model.Configurations.GetSpecial(this.Model.Special);
            }

            set
            {
                if (value != null)
                {
                    this.Model.Special = value.Id;
                }
                else
                {
                    this.Model.Special = Guid.Empty;
                }

                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ModelCollection<DialogueSpecial> Specials
        {
            get { return this.Model.Configurations.Specials; }
        }

        /// <summary>
        /// Gets the voice name that has been assigned to the line. This is the voice that is
        /// used while recording the line.
        /// </summary>
        public string VoiceName
        {
            get { return this.Model.VoiceName; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Makes sure that if anything error related to this line is updated in the owning
        /// conversation.
        /// </summary>
        /// <param name="propertyName">
        /// The name of the property whose errors have changed.
        /// </param>
        public override void OnErrorsChanged(string propertyName)
        {
            base.OnErrorsChanged(propertyName);
            if (this._conversation != null)
            {
                this._conversation.UpdateLineErrors();
            }
        }

        /// <summary>
        /// Makes sure that when requesting the error list for the specified property the
        /// correct name is past onto the model.
        /// </summary>
        /// <param name="propertyName">
        /// The original name of the property.
        /// </param>
        /// <returns>
        /// The resolved property name.
        /// </returns>
        protected override string ResolveGetErrorsPropertyName(string propertyName)
        {
            string result = base.ResolveGetErrorsPropertyName(propertyName);
            if (String.Equals(result, "SelectedCharacter"))
            {
                return "CharacterId";
            }

            return result;
        }

        /// <summary>
        /// Makes sure any forwarded changes from the manual filename property get pushed into
        /// the read-only filename property.
        /// </summary>
        /// <param name="propertyName">
        /// The original name of the property.
        /// </param>
        /// <returns>
        /// The resolved property name.
        /// </returns>
        protected override string ResolvePropertyNameOnChange(string propertyName)
        {
            string result = base.ResolvePropertyNameOnChange(propertyName);
            if (String.Equals(result, "ManualFilename"))
            {
                return "ReadOnlyFilename";
            }
            else if (String.Equals(result, "CharacterId"))
            {
                return "SelectedCharacter";
            }

            return result;
        }

        /// <summary>
        /// Determines whether the validation pipeline should be started due to the property
        /// with the specified name changing.
        /// </summary>
        /// <param name="propertyName">
        /// The property name to test.
        /// </param>
        /// <returns>
        /// True if the validation should be run due to the specified property changing;
        /// otherwise, false.
        /// </returns>
        protected override bool ShouldRunValidation(string propertyName)
        {
            if (String.Equals(propertyName, "SelectedCharacter"))
            {
                return true;
            }
            else if (String.Equals(propertyName, "AudioFilepath"))
            {
                return true;
            }
            else if (String.Equals(propertyName, "Speaker"))
            {
                return true;
            }
            else if (String.Equals(propertyName, "Listener"))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// The core method used to validate this class. By default this calls the models
        /// validate methods and uses that result.
        /// </summary>
        /// <param name="propertyName">
        /// The name of the property if this validation is associated with a change to a
        /// specific property; otherwise, null.
        /// </param>
        /// <returns>
        /// The result of the validation.
        /// </returns>
        protected override ValidationResult ValidateCore(string propertyName)
        {
            ValidationResult result = base.ValidateCore(propertyName);
            if (String.Equals(propertyName, "AudioFilepath"))
            {
                DialogueViewModel dialogue = this._conversation.Dialogue;
                foreach (ConversationViewModel conversation in dialogue.Conversations)
                {
                    foreach (LineViewModel line in conversation.Lines)
                    {
                        if (Object.ReferenceEquals(line, this))
                        {
                            continue;
                        }

                        if (line.SelectedCharacter != null)
                        {
                            if (line.SelectedCharacter.UsesManualFilenames)
                            {
                                continue;
                            }
                        }

                        line.Validate();
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Makes sure if the character id changes the used count on the character view model
        /// object is approiately updated.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The event arguments containing the event data include the old and new values for
        /// the character id.
        /// </param>
        private void CharacterIdChanged(object sender, Editor.ValueChangedEventArgs<Guid> e)
        {
            CharacterCollection characters = this._conversation.Characters;
            DialogueCharacterViewModel oldCharacter = characters.GetCharacter(e.OldValue);
            DialogueCharacterViewModel newCharacter = characters.GetCharacter(e.NewValue);

            if (oldCharacter != null)
            {
                if (oldCharacter.UsedCount > 0)
                {
                    oldCharacter.UsedCount--;
                }
            }

            if (newCharacter != null)
            {
                newCharacter.UsedCount++;
            }
        }
        #endregion Methods
    } // RSG.Text.ViewModel.LineViewModel {Class}
} // RSG.Text.ViewModel {Namespace}
