﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace RSG.Editor.Controls.CurveEditor
{
    /// <summary>
    /// 
    /// </summary>
    public class Ruler : Control
    {
        #region Fields
        /// <summary>
        /// Reference to the ruler element.
        /// </summary>
        private RulerTickBar _rulerElement;
        #endregion // Fields

        #region Properties
        /// <summary>
        /// Identifies the <see cref="InvertAxis"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty InvertAxisProperty =
            DependencyProperty.Register(
                "InvertAxis",
                typeof(bool),
                typeof(Ruler),
                new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.AffectsRender));

        /// <summary>
        /// Used to reverse the direction of the ruler.
        /// </summary>
        public bool InvertAxis
        {
            get { return (bool)GetValue(InvertAxisProperty); }
            set { SetValue(InvertAxisProperty, value); }
        }

        /// <summary>
        /// Identifies the <see cref="TickWidth"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty TickWidthProperty =
            DependencyProperty.Register(
                "TickWidth",
                typeof(double),
                typeof(Ruler),
                new FrameworkPropertyMetadata(10.0, FrameworkPropertyMetadataOptions.AffectsRender));

        /// <summary>
        /// Width in pixels between each tick.
        /// </summary>
        public double TickWidth
        {
            get { return (double)GetValue(TickWidthProperty); }
            set { SetValue(TickWidthProperty, value); }
        }

        /// <summary>
        /// Identifies the <see cref="TickIncrement"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty TickIncrementProperty =
            DependencyProperty.Register(
                "TickIncrement",
                typeof(double),
                typeof(Ruler),
                new FrameworkPropertyMetadata(1.0, FrameworkPropertyMetadataOptions.AffectsRender));

        /// <summary>
        /// Value differential between each tick.
        /// </summary>
        public double TickIncrement
        {
            get { return (double)GetValue(TickIncrementProperty); }
            set { SetValue(TickIncrementProperty, value); }
        }

        /// <summary>
        /// Identifies the <see cref="Placement"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty PlacementProperty =
            DependencyProperty.Register(
                "Placement",
                typeof(TickBarPlacement),
                typeof(Ruler),
                new FrameworkPropertyMetadata(TickBarPlacement.Top, FrameworkPropertyMetadataOptions.AffectsRender));

        /// <summary>
        /// Location where this ruler is placed.
        /// </summary>
        public TickBarPlacement Placement
        {
            get { return (TickBarPlacement)GetValue(PlacementProperty); }
            set { SetValue(PlacementProperty, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public static readonly DependencyProperty LabelPositionProperty =
            DependencyProperty.Register(
                "LabelPosition",
                typeof(TickBarLabelPosition),
                typeof(Ruler),
                new FrameworkPropertyMetadata(TickBarLabelPosition.On, FrameworkPropertyMetadataOptions.AffectsRender));

        /// <summary>
        /// Location where the label gets position relative to the ruler's tick.
        /// </summary>
        public TickBarLabelPosition LabelPosition
        {
            get { return (TickBarLabelPosition)GetValue(LabelPositionProperty); }
            set { SetValue(LabelPositionProperty, value); }
        }
        #endregion // Properties

        #region Ruler Limits Dependency Properties
        /// <summary>
        /// 
        /// </summary>
        public static readonly DependencyProperty StartProperty =
            DependencyProperty.Register(
                "Start",
                typeof(double),
                typeof(Ruler),
                new FrameworkPropertyMetadata(0.0));

        /// <summary>
        /// 
        /// </summary>
        public double Start
        {
            get { return (double)GetValue(StartProperty); }
            set { SetValue(StartProperty, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public static readonly DependencyProperty EndProperty =
            DependencyProperty.Register(
                "End",
                typeof(double),
                typeof(Ruler),
                new FrameworkPropertyMetadata(10.0));

        /// <summary>
        /// 
        /// </summary>
        public double End
        {
            get { return (double)GetValue(EndProperty); }
            set { SetValue(EndProperty, value); }
        }
        #endregion // Ruler Limits Dependency Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        static Ruler()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(Ruler),
                new FrameworkPropertyMetadata(typeof(Ruler)));
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            _rulerElement = GetTemplateChild("PART_RulerElement") as RulerTickBar;
        }
        #endregion // Overrides
    } // Ruler
}
