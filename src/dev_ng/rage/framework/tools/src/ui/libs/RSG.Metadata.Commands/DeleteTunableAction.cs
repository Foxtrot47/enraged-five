﻿//---------------------------------------------------------------------------------------------
// <copyright file="DeleteTunableAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Commands
{
    using System.Collections.Generic;
    using System.Linq;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Tunables;
    using RSG.Metadata.ViewModel.Tunables;

    /// <summary>
    /// Contains the logic for the RockstarCommands.Delete routed command. This class cannot be
    /// inherited.
    /// </summary>
    public sealed class DeleteTunableAction : MetadataCommandActionBase
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DeleteTunableAction"/> class.
        /// </summary>
        /// <param name="serviceProvider">
        /// The service provider that is used by this action to perform application specific
        /// operations, for example selecting a file to open.
        /// </param>
        public DeleteTunableAction(ICommandServiceProvider serviceProvider)
            : base(serviceProvider)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="DeleteTunableAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public DeleteTunableAction(MetadataCommandArgsResolver resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(MetadataCommandArgs args)
        {
            if (args.SelectedTunables == null || !args.SelectedTunables.Any())
            {
                return false;
            }

            foreach (ITunableViewModel viewModel in args.SelectedTunables)
            {
                if (viewModel == null || viewModel.Model == null)
                {
                    return false;
                }

                if (!(viewModel.Model.Parent is IDynamicTunableParent))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(MetadataCommandArgs args)
        {
            using (new UndoRedoBatch(args.UndoEngine))
            {
                IEnumerable<ITunable> items =
                    args.SelectedTunables.Select(t => { return t.Tunable; });
                List<ITunable> roots = this.CollectRoots(items);
                foreach (ITunable root in roots)
                {
                    IDynamicTunableParent parent = root.Parent as IDynamicTunableParent;
                    if (parent == null)
                    {
                        continue;
                    }

                    parent.RemoveItem(root);
                }
            }
        }

        /// <summary>
        /// Gets the root selected items so that we don't try to delete a child item from a
        /// branch that has already been deleted.
        /// </summary>
        /// <param name="items">
        /// The raw selected items.
        /// </param>
        /// <returns>
        /// The root selected items.
        /// </returns>
        private List<ITunable> CollectRoots(IEnumerable<ITunable> items)
        {
            List<ITunable> roots = new List<ITunable>(items);
            foreach (ITunable item in items)
            {
                IModel parent = item.Parent;
                while (parent != null)
                {
                    if (items.Contains(parent))
                    {
                        roots.Remove(item);
                        break;
                    }

                    parent = parent.Parent;
                }
            }

            return roots;
        }
        #endregion Methods
    } // RSG.Metadata.Commands.DeleteTunableAction {Class}
} // RSG.Metadata.Commands {Namespace}
