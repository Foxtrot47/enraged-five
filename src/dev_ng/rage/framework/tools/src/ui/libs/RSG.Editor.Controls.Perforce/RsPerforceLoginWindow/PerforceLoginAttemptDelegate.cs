﻿//---------------------------------------------------------------------------------------------
// <copyright file="PerforceLoginAttemptDelegate.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Perforce
{
    /// <summary>
    /// Represents the predicate method that determines whether the login is successful based
    /// on the current parameters.
    /// </summary>
    /// <param name="e">
    /// The object containing the data to use inside the predicate method to determine the user
    /// name and password the user has set.
    /// </param>
    /// <returns>
    /// A value indicating whether the login has been successful.
    /// </returns>
    public delegate bool PerforceLoginAttemptDelegate(PerforceLoginAttemptData e);
} // RSG.Editor.Controls.Perforce {Namespace}
