﻿//---------------------------------------------------------------------------------------------
// <copyright file="IPrioritisedComparable.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    using System;

    /// <summary>
    /// When implemented represents a object that contains a property that specifies a
    /// priority value for the object and a method that can be used to sort its instances.
    /// </summary>
    public interface IPrioritisedComparable : IComparable
    {
        #region Properties
        /// <summary>
        /// Gets the priority value for this object.
        /// </summary>
        int Priority { get; }
        #endregion Properties
    } // RSG.Editor.View.IPrioritisedComparable {Interface}
} // RSG.Editor.View
