﻿//---------------------------------------------------------------------------------------------
// <copyright file="PerforceService.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Perforce
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using global::Perforce.P4;
    using RSG.Base.Extensions;
    using RSG.Interop.Perforce;

    /// <summary>
    /// Provides a implementation of the <see cref="IPerforceService"/> that can be used be the
    /// application to give command actions the ability to perform perforce operations.
    /// </summary>
    public class PerforceService : IPerforceService
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="CommandTimeout"/> property.
        /// </summary>
        private TimeSpan _commandTimeout;

        /// <summary>
        /// The suppress exception scope count.
        /// </summary>
        private int _suppressExceptionCount;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="PerforceService"/> class.
        /// </summary>
        public PerforceService()
			: this(TimeSpan.FromSeconds(30))
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="PerforceService"/> class using
        /// the provided command timeout.
        /// </summary>
        /// <param name="commandTimeout"></param>
        public PerforceService(TimeSpan commandTimeout)
        {
            _commandTimeout = commandTimeout;
        }
        #endregion Constructors
        
        #region Properties
        /// <summary>
        /// Gets or sets the perforce command execution timeout.
        /// </summary>
        public TimeSpan CommandTimeout
        {
            get { return _commandTimeout; }
            set { _commandTimeout = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Adds the specified files and puts them into the default change list.
        /// </summary>
        /// <param name="fullPaths">
        ///  The full paths to the files that need adding.
        /// </param>
        public void AddFiles(IList<string> fullPaths)
        {
            using (P4 p4 = new P4(this.PasswordLogin, this._commandTimeout))
            {
                if (!this.EnsureConnection(p4))
                {
                    return;
                }

                IList<FileSpec> fileSpecifications = FileSpec.LocalSpecList(fullPaths);
                p4.Client.AddFiles(fileSpecifications, null);
            }
        }

        /// <summary>
        /// Adds the specified files and puts them into the change list with the specified id.
        /// </summary>
        /// <param name="fullPaths">
        ///  The full paths to the files that need adding.
        /// </param>
        /// <param name="changelistId">
        /// The id of the change list the files should be added to.
        /// </param>
        public void AddFiles(IList<string> fullPaths, int changelistId)
        {
            using (P4 p4 = new P4(this.PasswordLogin))
            {
                if (!this.EnsureConnection(p4))
                {
                    return;
                }

                Options options = new Options();
                Changelist newChangeList = p4.Repository.GetChangelist(changelistId);
                if (newChangeList == null)
                {
                    string msg = "Unable to find changelist with id {0}";
                    throw new PerforceException(msg.FormatInvariant(changelistId));
                }

                options.Clear();
                options.Add("-c", newChangeList.Id.ToString());
                IList<FileSpec> fileSpecifications = FileSpec.LocalSpecList(fullPaths);
                p4.Client.AddFiles(fileSpecifications, options);
            }
        }

        /// <summary>
        /// Returns which files are checked out locally from the list passed in.
        /// </summary>
        /// <param name="fullPaths">
        /// The collection containing the full paths of the files to check.
        /// </param>
        /// <param name="actions">
        /// The actions to test.
        /// </param>
        /// <returns>
        /// True if any of the given files have the actions; otherwise, false.
        /// </returns>
        public bool AnyFilesWithState(IList<String> fullPaths, params PerforceFileAction[] actions)
        {
            using (P4 p4 = new P4(this.PasswordLogin, this._commandTimeout))
            {
                this.EnsureConnection(p4, true);

                IList<FileSpec> fileSpecifications = FileSpec.LocalSpecList(fullPaths);
                IList<FileMetaData> metadataList = p4.Repository.GetFileMetaData(fileSpecifications, null);
                foreach (FileMetaData metadata in metadataList)
                {
                    foreach (PerforceFileAction action in actions)
                    {
                        if ((int)metadata.Action == (int)action)
                        {
                            return true;
                        }
                    }
                    
                }

                return false;
            }
        }

        /// <summary>
        /// Creates a new changelist using the specified description.
        /// </summary>
        /// <param name="description">
        /// The description of the change list we are creating.
        /// </param>
        /// <returns>
        /// The id of changelist that was created.
        /// </returns>
        public int CreateChangelist(string description)
        {
            using (P4 p4 = new P4(this.PasswordLogin, this._commandTimeout))
            {
                if (!this.EnsureConnection(p4))
                {
                    return 0;
                }

                Changelist newChangeList = new Changelist();
                newChangeList.Description = description;
                p4.InitialiseChangeList(newChangeList);
                newChangeList = p4.Repository.CreateChangelist(newChangeList);

                return newChangeList.Id;
            }
        }

        /// <summary>
        /// Checks out the specified files into a new change list with the specified
        /// description and optionally adds any file that isn't currently mapped.
        /// </summary>
        /// <param name="fullPaths">
        ///  The full paths to the files that need checking out.
        /// </param>
        /// <param name="description">
        /// The description of the change list the files should be checked out in.
        /// </param>
        /// <param name="add">
        /// A value indicating whether any files currently not mapped should be added.
        /// </param>
        public void EditFiles(IList<string> fullPaths, string description, bool add)
        {
            using (P4 p4 = new P4(this.PasswordLogin, this._commandTimeout))
            {
                if (!this.EnsureConnection(p4))
                {
                    return;
                }

                Options options = new Options();
                Changelist newChangeList = new Changelist();
                newChangeList.Description = description;
                p4.InitialiseChangeList(newChangeList);
                newChangeList = p4.Repository.CreateChangelist(newChangeList, options);

                options.Clear();
                options.Add("-c", newChangeList.Id.ToString());
                IList<FileSpec> fileSpecifications = FileSpec.LocalSpecList(fullPaths);
                p4.Client.EditFiles(fileSpecifications, options);
                p4.Client.ReopenFiles(fileSpecifications, options);
                if (add)
                {
                    p4.Client.AddFiles(fileSpecifications, options);
                }

                options.Clear();
                options.Add("-l", null);
                options.Add("-s", null);
                options.Add("pending", null);
                options.Add("-u", p4.Username);
                options.Add("-c", p4.ClientName);
                IList<Changelist> changelists = p4.Repository.GetChangelists(options);
                foreach (Changelist changeList in changelists)
                {
                    if (!String.Equals(changeList.Description.Trim(), description))
                    {
                        continue;
                    }

                    p4.Repository.DeleteChangelist(changeList, null);
                }
            }
        }

        /// <summary>
        /// Checks out the specified files into the change list with the specified id and
        /// optionally adds any file that isn't currently mapped.
        /// </summary>
        /// <param name="fullPaths">
        ///  The full paths to the files that need checking out.
        /// </param>
        /// <param name="changelistId">
        /// The id of the change list the files should be checked out in.
        /// </param>
        /// <param name="add">
        /// A value indicating whether any files currently not mapped should be added.
        /// </param>
        public void EditFiles(IList<string> fullPaths, int changelistId, bool add)
        {
            using (P4 p4 = new P4(this.PasswordLogin))
            {
                if (!this.EnsureConnection(p4))
                {
                    return;
                }

                Options options = new Options();
                Changelist newChangeList = p4.Repository.GetChangelist(changelistId);
                if (newChangeList == null)
                {
                    string msg = "Unable to find changelist with id {0}";
                    throw new PerforceException(msg.FormatInvariant(changelistId));
                }

                options.Clear();
                options.Add("-c", newChangeList.Id.ToString());
                IList<FileSpec> fileSpecifications = FileSpec.LocalSpecList(fullPaths);
                p4.Client.EditFiles(fileSpecifications, options);
                p4.Client.ReopenFiles(fileSpecifications, options);
                if (add)
                {
                    p4.Client.AddFiles(fileSpecifications, options);
                }

                options.Clear();
                options.Add("-l", null);
                options.Add("-s", "pending");
                options.Add("-u", p4.Username);
                options.Add("-c", p4.ClientName);
                IList<Changelist> changelists = p4.Repository.GetChangelists(options);
                string description = newChangeList.Description;
                foreach (Changelist changeList in changelists)
                {
                    if (!String.Equals(changeList.Description.Trim(), description))
                    {
                        continue;
                    }

                    if (changeList.Files.Count > 0 || changeList.Shelved)
                    {
                        continue;
                    }

                    p4.Repository.DeleteChangelist(changeList, null);
                }
            }
        }

        /// <summary>
        /// Checks out the specified files into the default change list.
        /// </summary>
        /// <param name="fullPaths">
        ///  The full paths to the files that need checking out.
        /// </param>
        public void EditFiles(IList<string> fullPaths)
        {
            using (P4 p4 = new P4(this.PasswordLogin, this._commandTimeout))
            {
                if (!this.EnsureConnection(p4))
                {
                    return;
                }

                IList<FileSpec> fileSpecifications = FileSpec.LocalSpecList(fullPaths);
                p4.Client.EditFiles(fileSpecifications, null);
            }
        }

        /// <summary>
        /// Returns which files are checked out locally from the list passed in.
        /// </summary>
        /// <param name="fullPaths"></param>
        /// <returns></returns>
        public IList<String> GetCheckedOutFiles(IList<String> fullPaths)
        {
            using (P4 p4 = new P4(this.PasswordLogin, this._commandTimeout))
            {
                this.EnsureConnection(p4, true);

                IList<FileSpec> fileSpecifications = FileSpec.LocalSpecList(fullPaths);
                IList<FileMetaData> metadataList = p4.Repository.GetFileMetaData(fileSpecifications, null);

                IList<String> checkedOutFiles = new List<String>();
                foreach (FileMetaData metadata in metadataList)
                {
                    if (metadata.Action == FileAction.Edit)
                    {
                        checkedOutFiles.Add(metadata.ClientPath.Path);
                    }
                }

                return checkedOutFiles;
            }
        }

        /// <summary>
        /// Returns a stream of the specified file's contents.
        /// </summary>
        /// <param name="fileSpecs">
        /// The file specifications for the files to get the contents for.
        /// </param>
        /// <returns>
        /// Mapping of file specifications to streams containing the file contents.
        /// </returns>
        /// <remarks>
        /// The calling code is in charge of disposing of the returned streams.
        /// </remarks>
        public IDictionary<IPerforceFileSpec, System.IO.Stream> GetFileContents(IList<IPerforceFileSpec> fileSpecs)
        {
            using (P4 p4 = new P4(this.PasswordLogin))
            {
                this.EnsureConnection(p4, true);

                IList<FileSpec> fileSpecifications = CreateFileSpecs(fileSpecs);
                IList<object> results =
                    p4.Repository.GetFileContentsEx(fileSpecifications, null);

                IDictionary<IPerforceFileSpec, System.IO.Stream> streams =
                    new Dictionary<IPerforceFileSpec, System.IO.Stream>(fileSpecifications.Count);

                IPerforceFileSpec currentFileSpec = null;

                foreach (object result in results)
                {
                    if (result is FileSpec)
                    {
                        FileSpec returnedFileSpec = (FileSpec)result;
                        if (returnedFileSpec.ClientPath != null)
                        {
                            currentFileSpec = fileSpecs.FirstOrDefault(item =>
                                item.ClientPath == returnedFileSpec.ClientPath.Path);
                        }
                        else if (returnedFileSpec.DepotPath != null)
                        {
                            currentFileSpec = fileSpecs.FirstOrDefault(item =>
                                item.DepotPath == returnedFileSpec.DepotPath.Path);
                        }
                        else if (returnedFileSpec.DepotPath != null)
                        {
                            currentFileSpec = fileSpecs.FirstOrDefault(item =>
                                item.LocalPath == returnedFileSpec.LocalPath.Path);
                        }
                        else
                        {
                            currentFileSpec = null;
                        }
                    }
                    else if (currentFileSpec != null)
                    {
                        System.IO.MemoryStream stream = new System.IO.MemoryStream();
                        if (result is byte[])
                        {
                            byte[] binaryOutput = (byte[])result;
                            stream.Write(binaryOutput, 0, binaryOutput.Length);
                        }
                        else if (result is string)
                        {
                            System.IO.StreamWriter writer = new System.IO.StreamWriter(stream);
                            writer.Write((string)result);
                        }
                        else
                        {
                            PerforceException exception = new PerforceException("Unsupported data returned from the GetFileContentsEx() method.");
                            exception.Data.Add("DataType", result.GetType().FullName);
                            throw exception;
                        }
                        stream.Position = 0;
                        streams.Add(currentFileSpec, stream);
                    }
                }

                return streams;
            }
        }

        /// <summary>
        /// Gets the metadata for the specified list of files. The metadata contains the
        /// results from the 'fstat' perforce command.
        /// </summary>
        /// <param name="fullPaths">
        /// The collection containing the full paths of the files to get.
        /// </param>
        /// <returns>
        /// A list containing the metadata for the specified files.
        /// </returns>
        public IList<IPerforceFileMetaData> GetFileMetaData(IList<string> fullPaths)
        {
            return GetFileMetaData(PerforceFileSpec.LocalSpecList(fullPaths));
        }

        /// <summary>
        /// Gets the metadata for the specified list of files. The metadata contains the
        /// results from the 'fstat' perforce command.
        /// </summary>
        /// <param name="fileSpecs">
        /// The file specifications for the files to get metadata for.
        /// </param>
        /// <returns>
        /// A list containing the metadata for the specified files.
        /// </returns>
        public IList<IPerforceFileMetaData> GetFileMetaData(IList<IPerforceFileSpec> fileSpecs)
        {
            using (P4 p4 = new P4(this.PasswordLogin))
            {
                this.EnsureConnection(p4, true);

                IList<FileSpec> fileSpecifications = CreateFileSpecs(fileSpecs);
                IList<FileMetaData> metadataList =
                    this.GetFileMetaData(p4.Repository, null, fileSpecifications.ToArray());

                IList<IPerforceFileMetaData> result =
                    new List<IPerforceFileMetaData>();
                if (metadataList != null)
                {
                    foreach (FileMetaData fileMetadata in metadataList)
                    {
                        result.Add(new PerforceFileMetadata(fileMetadata));
                    }
                }

                return result;
            }
        }

        /// <summary>
        /// Gets the head revision of the specified files.
        /// </summary>
        /// <param name="fullPaths">
        /// The full paths to the files that needs to be synced to the head revision.
        /// </param>
        public void GetLatest(IList<string> fullPaths)
        {
            GetLatest(PerforceFileSpec.LocalSpecList(fullPaths), false);
        }

        /// <summary>
        /// Gets the head revision of the specified files.
        /// </summary>
        /// <param name="fileSpecs">
        /// The file specifications for the files to get metadata for.
        /// </param>
        /// <param name="preview">
        /// Flag indicating whether we only wish to preview the results of the sync
        /// operation.
        /// </param>
        /// <returns>
        /// A list containing the metadata for the files that were/would be synced
        /// </returns>
        public IList<IPerforceFileMetaData> GetLatest(IList<IPerforceFileSpec> fileSpecs, bool preview = false)
        {
            using (P4 p4 = new P4(this.PasswordLogin, this._commandTimeout))
            {
                this.EnsureConnection(p4, true);

                SyncFilesCmdFlags syncFlags = SyncFilesCmdFlags.None;
                if (preview)
                {
                    syncFlags |= SyncFilesCmdFlags.Preview;
                }
                Options options = new Options(syncFlags, 0);
                IList<FileSpec> fileSpecifications = CreateFileSpecs(fileSpecs);

                IList<FileSpec> resultingSpecifications = p4.Client.SyncFiles(fileSpecifications, options);

                IList<IPerforceFileMetaData> result =
                    new List<IPerforceFileMetaData>();
                if (resultingSpecifications != null)
                {
                    foreach (FileMetaData fileMetadata in resultingSpecifications)
                    {
                        result.Add(new PerforceFileMetadata(fileMetadata));
                    }
                }

                return result;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="depotPaths"></param>
        /// <returns></returns>
        public IList<string> GetLocalPathsFromDepotPaths(IList<string> depotPaths)
        {
            IList<string> result = new List<string>();
            using (P4 p4 = new P4(this.PasswordLogin, this._commandTimeout))
            {
                if (!this.EnsureConnection(p4))
                {
                    return result;
                }

                string[] array = new string[depotPaths.Count];
                depotPaths.CopyTo(array, 0);

                IList<FileSpec> fileSpec = FileSpec.DepotSpecList(array);
                IList<FileMetaData> metaData = p4.Repository.GetFileMetaData(fileSpec, new Options());
                if (metaData != null)
                {
                    foreach (var m in metaData)
                    {
                        result.Add(m.LocalPath.Path);
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Marks the specified files for deletion.
        /// </summary>
        /// <param name="fullPaths">
        /// The full paths to the files that are to be mark for delete.
        /// </param>
        public void MarkForDelete(IList<string> fullPaths)
        {
            using (P4 p4 = new P4(this.PasswordLogin, this._commandTimeout))
            {
                if (!this.EnsureConnection(p4))
                {
                    return;
                }

                IList<FileSpec> fileSpecifications = FileSpec.LocalSpecList(fullPaths);
                p4.Client.DeleteFiles(fileSpecifications, new Options());
            }
        }
        
        /// <summary>
        /// Moves the specified files into the specified changelist.
        /// </summary>
        /// <param name="fullPaths">
        /// The full paths to the files that needs to be synced to the head revision.
        /// </param>
        /// <param name="changelistId">
        /// The id of the changelist to move the files into.
        /// </param>
        public void MoveFilesIntoChangelist(IList<string> fullPaths, int changelistId)
        {
            using (P4 p4 = new P4(this.PasswordLogin, this._commandTimeout))
            {
                if (!this.EnsureConnection(p4))
                {
                    return;
                }

                IList<FileSpec> fileSpecifications = FileSpec.LocalSpecList(fullPaths);
                Options options = new Options();
                options.Add("-c", changelistId.ToString());
                p4.Client.ReopenFiles(fileSpecifications, options);
            }
        }

        /// <summary>
        /// Reverts all of the specified files.
        /// </summary>
        /// <param name="fullPaths">
        /// The full paths to the files that are to be reverted.
        /// </param>
        public void RevertFiles(IList<string> fullPaths)
        {
            using (P4 p4 = new P4(this.PasswordLogin, this._commandTimeout))
            {
                if (!this.EnsureConnection(p4))
                {
                    return;
                }

                IList<FileSpec> fileSpecifications = FileSpec.LocalSpecList(fullPaths);
                p4.Client.RevertFiles(fileSpecifications, new Options());
            }
        }

        /// <summary>
        /// Reverts any of the specified files that are unchanged.
        /// </summary>
        /// <param name="fullPaths">
        /// The full paths to the files that needs to be synced to the head revision.
        /// </param>
        public void RevertUnchangedFiles(IList<string> fullPaths)
        {
            using (P4 p4 = new P4(this.PasswordLogin, this._commandTimeout))
            {
                if (!this.EnsureConnection(p4))
                {
                    return;
                }

                Options options = new Options();
                options.Add("-a", null);
                IList<FileSpec> fileSpecifications = FileSpec.LocalSpecList(fullPaths);
                p4.Client.RevertFiles(fileSpecifications, options);
            }
        }

        /// <summary>
        /// Submits the specified changelist.
        /// </summary>
        /// <param name="changelistId">
        /// The id of the changelist to move the files into.
        /// </param>
        public void SubmitChangelist(int changelistId)
        {
            using (P4 p4 = new P4(this.PasswordLogin, this._commandTimeout))
            {
                if (!this.EnsureConnection(p4))
                {
                    return;
                }

                Changelist changelist = p4.Repository.GetChangelist(changelistId);
                changelist.Submit(null);
            }
        }
        
        /// <summary>
        /// Creates a disposable object whose scope turns the perforce exceptions off.
        /// </summary>
        /// <returns></returns>
        public IDisposable SuspendExceptionsScope()
        {
            return new SuspendExceptions(this);
        }

        /// <summary>
        /// Attempts to delete the specified changelist.
        /// </summary>
        /// <param name="changelistId">
        /// The id of the changelist to attempt to delete.
        /// </param>
        public void TryToDeleteChangelist(int changelistId)
        {
            using (P4 p4 = new P4(this.PasswordLogin))
            {
                if (!this.EnsureConnection(p4))
                {
                    return;
                }

                Changelist changelist = p4.Repository.GetChangelist(changelistId);
                if (changelist == null)
                {
                    return;
                }

                if (changelist.Files.Count > 0 || changelist.Shelved)
                {
                    return;
                }

                p4.Repository.DeleteChangelist(changelist, null);
            }
        }

        /// <summary>
        /// Converts a list of file specifications from our internal format to the
        /// version that the perforce API requires.
        /// </summary>
        /// <param name="fileSpecs"></param>
        /// <returns></returns>
        private IList<FileSpec> CreateFileSpecs(IList<IPerforceFileSpec> fileSpecs)
        {
            if (fileSpecs == null || !fileSpecs.Any())
            {
                throw new PerforceException("No file specifications provided.");
            }

            IPerforceFileSpec firstFileSpec = fileSpecs[0];

            if (firstFileSpec.DepotPath != null)
            {
                return FileSpec.DepotSpecList(fileSpecs.Select(item => item.DepotPath).ToArray());
            }
            else if (firstFileSpec.LocalPath != null)
            {
                return FileSpec.LocalSpecList(fileSpecs.Select(item => item.LocalPath).ToArray());
            }
            else if (firstFileSpec.ClientPath != null)
            {
                return FileSpec.ClientSpecList(fileSpecs.Select(item => item.ClientPath).ToArray());
            }
            else
            {
                throw new PerforceException("Depot, local and client paths not set.");
            }
        }

        /// <summary>
        /// Ensures that a valid connection has been established with the specified perforce
        /// object.
        /// </summary>
        /// <param name="perforceObject">
        /// The perforce object to test for a connection.
        /// </param>
        /// <param name="rethrow">
        /// Flag indicating whether we should rethrow exceptions.
        /// </param>
        /// <returns>
        /// A value indicating whether a valid connection is present in the specified perforce
        /// object.
        /// </returns>
        private bool EnsureConnection(P4 perforceObject, bool rethrow=false)
        {
            bool connected = perforceObject.Connected;
            if (!connected)
            {
                try
                {
                    connected = perforceObject.Connect();
                }
                catch (PerforceException e)
                {
                    if (rethrow)
                    {
                        throw e;
                    }
                    return false;
                }
                catch (Exception e)
                {
                    if (rethrow)
                    {
                        throw e;
                    }
                    return false;
                }
            }

            return connected;
        }

        /// <summary>
        /// New version of the get file metadata method that doesn't return null if the method
        /// failed and instead returns the results that it can.
        /// </summary>
        /// <param name="repository">
        /// Target repository.
        /// </param>
        /// <param name="options">
        /// The options to use.
        /// </param>
        /// <param name="filespecs">
        /// The files whose metadata is to be retrieved.
        /// </param>
        /// <returns>
        /// The metadata for the specified files.
        /// </returns>
        private IList<FileMetaData> GetFileMetaData(
            Repository repository, Options options, params FileSpec[] filespecs)
        {
            string[] paths = FileSpec.ToEscapedStrings(filespecs);
            P4Command fstatCmd = new P4Command(repository, "fstat", true, paths);
            P4CommandResult r = fstatCmd.Run(options);
            if (r.Success != true)
            {
                foreach (P4ClientError current in r.ErrorList)
                {
                    if (current.SeverityLevel >= P4Exception.MinThrowLevel)
                    {
                        throw new P4Exception(r.ErrorList);
                    }
                }
            }

            if ((r.TaggedOutput == null) || (r.TaggedOutput.Count <= 0))
            {
                return null;
            }

            List<FileMetaData> value = new List<FileMetaData>();

            foreach (TaggedObject obj in r.TaggedOutput)
            {
                FileMetaData fmd = new FileMetaData();
                fmd.FromFstatCmdTaggedData(obj);
                value.Add(fmd);
            }

            return value;
        }

        /// <summary>
        /// Represents the predicate method that determines whether the login is successful
        /// based on the current parameters.
        /// </summary>
        /// <param name="e">
        /// The object containing the data to use inside the predicate method to determine the
        /// user name and password the user has set.
        /// </param>
        /// <returns>
        /// A value indicating whether the login has been successful.
        /// </returns>
        private bool LoginAttempt(PerforceLoginAttemptData e)
        {
            try
            {
                e.PerforceObject.Login(e.Password);
                P4CommandResult loginResult = e.PerforceObject.Run(P4CommandOp.Login, "-s");
                return loginResult.Success;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The delegate method that a perforce object calls to when a password is needed to
        /// login to the object.
        /// </summary>
        /// <param name="p4">
        /// The perforce object that requires a login.
        /// </param>
        /// <returns>
        /// A value indicating whether the login was successful or not.
        /// </returns>
        private bool PasswordLogin(P4 p4)
        {
            RsPerforceLoginWindow window = new RsPerforceLoginWindow();
            window.Port = p4.Port.Uri;
            window.Client = p4.ClientName;
            window.Username = p4.Username;
            window.LoginDelegate = this.LoginAttempt;
            window.PerforceObject = p4;
            return (bool)window.ShowDialog();
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// Contains the metadata for a file associated with the perforce server.
        /// </summary>
        private class PerforceFileMetadata : IPerforceFileMetaData
        {
            #region Fields
            /// <summary>
            /// The private reference to the perforce file metadata object.
            /// </summary>
            private FileMetaData _metadata;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="PerforceFileMetadata"/> class.
            /// </summary>
            /// <param name="metadata">
            /// The perforce object containing all of the metadata values.
            /// </param>
            public PerforceFileMetadata(FileMetaData metadata)
            {
                this._metadata = metadata;
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets the current users action on this file.
            /// </summary>
            public PerforceFileAction Action
            {
                get { return (PerforceFileAction)((long)this._metadata.Action); }
            }

            /// <summary>
            /// Gets the change list number that this file is inside in the current users
            /// pending change lists if applicable; otherwise, -1.
            /// </summary>
            public int Change
            {
                get { return this._metadata.Change; }
            }

            /// <summary>
            /// Gets the location of the file in the clients workspace.
            /// </summary>
            public string ClientPath
            {
                get { return this._metadata.ClientPath.Path; }
            }

            /// <summary>
            /// Gets the location of the file in the clients depot system.
            /// </summary>
            public string DepotPath
            {
                get { return this._metadata.DepotPath.Path; }
            }

            /// <summary>
            /// Gets the revision index the current user has of this file.
            /// </summary>
            public int HaveRevision
            {
                get { return this._metadata.HaveRev; }
            }

            /// <summary>
            /// Gets the action that was performed on the file at the head revision.
            /// </summary>
            public PerforceFileAction HeadAction
            {
                get { return (PerforceFileAction)((long)this._metadata.HeadAction); }
            }

            /// <summary>
            /// Gets the revision index the of this files head revision.
            /// </summary>
            public int HeadRevision
            {
                get { return this._metadata.HeadRev; }
            }

            /// <summary>
            /// Gets the date and time the head revision of the file was submitted.
            /// </summary>
            public DateTime HeadTime
            {
                get { return this._metadata.HeadTime; }
            }

            /// <summary>
            /// Gets a value indicating whether the file is inside the clients workspace.
            /// </summary>
            public bool IsInClient
            {
                get { return this._metadata.IsInClient; }
            }

            /// <summary>
            /// Gets a value indicating whether the file is inside the clients depot.
            /// </summary>
            public bool IsInDepot
            {
                get { return this._metadata.IsInDepot; }
            }

            /// <summary>
            /// Gets a value indicating whether the file is mapped in the clients workspace.
            /// </summary>
            public bool IsMapped
            {
                get { return this._metadata.IsMapped; }
            }

            /// <summary>
            /// Gets the location of the file in the client's file system.
            /// </summary>
            public string LocalPath
            {
                get { return this._metadata.LocalPath.Path; }
            }

            /// <summary>
            /// Gets the list containing all of the actions other users are currently
            /// performing on the file.
            /// </summary>
            public IList<PerforceFileAction> OtherActions
            {
                get
                {
                    IList<PerforceFileAction> acts = new List<PerforceFileAction>();
                    if (this._metadata.OtherActions == null)
                    {
                        return acts;
                    }

                    foreach (FileAction action in this._metadata.OtherActions)
                    {
                        acts.Add((PerforceFileAction)((long)action));
                    }

                    return acts;
                }
            }

            /// <summary>
            /// Gets a value indicating whether the file is locked by another user.
            /// </summary>
            public bool OtherLock
            {
                get { return this._metadata.OtherLock; }
            }
            #endregion Properties
        } // PerforceService.PerforceFileMetadata {Class}

        /// <summary>
        /// Provides a scoped object that sets the exception level of the perforce API to none.
        /// </summary>
        private class SuspendExceptions : DisposableObject
        {
            #region Fields
            /// <summary>
            /// The reference to the service this scope object was created from.
            /// </summary>
            private PerforceService _service;
            #endregion Fields

            #region Constuctors
            /// <summary>
            /// Initialises a new instance of the <see cref="SuspendExceptions"/> class.
            /// </summary>
            /// <param name="service">
            /// The service that contains the suppress exception count.
            /// </param>
            public SuspendExceptions(PerforceService service)
            {
                if (service._suppressExceptionCount == 0)
                {
                    P4Exception.MinThrowLevel = ErrorSeverity.E_NOEXC;
                }

                this._service = service;
                service._suppressExceptionCount++;
            }
            #endregion Constuctors

            #region Methods
            /// <summary>
            /// Resets the minimial throw level of the perforce exception object to failed.
            /// </summary>
            protected override void DisposeManagedResources()
            {
                base.DisposeManagedResources();
                this._service._suppressExceptionCount--;
                if (this._service._suppressExceptionCount == 0)
                {
                    P4Exception.MinThrowLevel = ErrorSeverity.E_FAILED;
                }
            }
            #endregion Methods
        } // PerforceService.SuppressExceptions {Class}
        #endregion
    } // RSG.Editor.Controls.Perforce.PerforceService {Class}
} // RSG.Editor.Controls.Perforce {Namespace}
