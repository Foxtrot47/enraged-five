﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace RSG.Editor.Controls.MapViewport
{
    /// <summary>
    /// Represents a map layer which positions its child elements based on their locations.
    /// </summary>
    public class MapLayer : ItemsControl
    {
        #region Dependency Properties
        /// <summary>
        /// Identifies the DisplayName dependency property.
        /// </summary>
        public readonly static DependencyProperty DisplayNameProperty =
            DependencyProperty.Register(
                "DisplayName",
                typeof(string),
                typeof(MapLayer),
                new FrameworkPropertyMetadata(null));

        /// <summary>
        /// Gets or sets the name for this map layer.
        /// </summary>
        public string DisplayName
        {
            get { return (string)GetValue(DisplayNameProperty); }
            set { SetValue(DisplayNameProperty, value); }
        }

        /// <summary>
        /// Identifies the StartsGroup dependency property.
        /// </summary>
        public readonly static DependencyProperty StartsGroupProperty =
            DependencyProperty.Register(
                "StartsGroup",
                typeof(bool),
                typeof(MapLayer),
                new FrameworkPropertyMetadata(false));

        /// <summary>
        /// Gets or sets the starts group flag.
        /// </summary>
        public bool StartsGroup
        {
            get { return (bool)GetValue(StartsGroupProperty); }
            set { SetValue(StartsGroupProperty, value); }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises the static members of the <see cref="MapLayer"/> class.
        /// </summary>
        static MapLayer()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(MapLayer),
                new FrameworkPropertyMetadata(typeof(MapLayer)));
        }
        #endregion
        
        #region Methods
        /// <summary>
        /// Creates or identifies the element used to display the child items.
        /// </summary>
        /// <returns>
        /// A new <see cref="RSG.Editor.Controls.MapViewport.MapLayer" />.
        /// </returns>
        protected override DependencyObject GetContainerForItemOverride()
        {
            return new MapLayerItem();
        }

        /// <summary>
        /// Determines whether the specified item is, or is eligible to be, its own item
        /// container.
        /// </summary>
        /// <param name="item">
        /// The item to check whether it is an item container.
        /// </param>
        /// <returns>
        /// True if the item is eligible to be its own item container; otherwise, false.
        /// </returns>
        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is MapLayerItem;
        }
        #endregion
    }
}
