﻿//---------------------------------------------------------------------------------------------
// <copyright file="BugstarLoginAttemptData.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Bugstar
{
    using RSG.Editor;
    using RSG.Editor.Controls;
    using RSG.Interop.Bugstar;

    /// <summary>
    /// Represents the object that is sent with the delegate determining whether the user has
    /// successfully logged in using the Bugstar login window.
    /// </summary>
    public class BugstarLoginAttemptData : LoginAttemptData
    {
        #region Properties
        /// <summary>
        /// Gets the BugstarConnection object that the login window is trying to establish a 
        /// connection to.
        /// </summary>
        public BugstarConnection BugstarConnection
        {
            get { return this._bugstarConnection; }
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Bugstar connection object.
        /// </summary>
        private BugstarConnection _bugstarConnection;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="BugstarLoginAttemptData"/> class.
        /// </summary>
        /// <param name="username">
        /// The submitted username.
        /// </param>
        /// <param name="password">
        /// The submitted password.
        /// </param>
        /// <param name="retry">
        /// A value indicating whether the user can attempt again if this attempt fails.
        /// </param>
        /// <param name="attempts">
        /// The number of attempts the user has had at logging in.
        /// </param>
        /// <param name="bugstarConnection">
        /// The BugstarConnection object that the login window is trying to establish a connection to.
        /// </param>
        public BugstarLoginAttemptData(
            string username, string password, bool retry, int attempts, BugstarConnection bugstarConnection)
            : base(username, password, retry, attempts)
        {
            if (null == bugstarConnection)
                throw (new SmartArgumentNullException(() => bugstarConnection));

            this._bugstarConnection = bugstarConnection;
        }
        #endregion Constructor(s)
    } // RSG.Editor.Controls.Perforce.PerforceLoginAttemptData {Class}
} // RSG.Editor.Controls.Perforce {Namespace}
