﻿//---------------------------------------------------------------------------------------------
// <copyright file="CollapseAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Commands
{
    using System.Linq;
    using RSG.Editor;
    using RSG.Metadata.ViewModel.Tunables;

    /// <summary>
    /// Contains the logic for the RockstarCommands.Collapse routed command. This class
    /// cannot be inherited.
    /// </summary>
    public sealed class CollapseAction : MetadataCommandActionBase
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CollapseAction"/> class.
        /// </summary>
        /// <param name="serviceProvider">
        /// The service provider that is used by this action to perform application specific
        /// operations, for example selecting a file to open.
        /// </param>
        public CollapseAction(ICommandServiceProvider serviceProvider)
            : base(serviceProvider)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="CollapseAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public CollapseAction(MetadataCommandArgsResolver resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(MetadataCommandArgs args)
        {
            ITunableViewModel selected = args.SelectedTunables.FirstOrDefault();
            if (selected == null)
            {
                return false;
            }

            return selected.IsExpandable;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(MetadataCommandArgs args)
        {
            ITunableViewModel selected = args.SelectedTunables.FirstOrDefault();
            if (selected == null)
            {
                return;
            }

            if (args.TunableCollapseDelegate != null)
            {
                args.TunableCollapseDelegate(selected, false);
            }
        }
        #endregion Methods
    } // RSG.Metadata.Commands.CollapseAction {Class}
} // RSG.Metadata.Commands {Namespace}
