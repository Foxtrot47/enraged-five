﻿//---------------------------------------------------------------------------------------------
// <copyright file="InitialisationResult.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    /// <summary>
    /// The different results the application manager can return after attempting to initialise
    /// the application.
    /// </summary>
    public enum InitialisationResult
    {
        /// <summary>
        /// The application was successfully initialised.
        /// </summary>
        Success,

        /// <summary>
        /// The application failed to parse the provided command line arguments. This is
        /// usually due to an exception being fired while parsing the arguments.
        /// </summary>
        SetArgumentsFailed,

        /// <summary>
        /// An existing instance of a single instance application is already running.
        /// </summary>
        SingleInstanceRunning
    }
}
