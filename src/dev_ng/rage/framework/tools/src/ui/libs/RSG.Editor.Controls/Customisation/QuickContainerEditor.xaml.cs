﻿//---------------------------------------------------------------------------------------------
// <copyright file="QuickContainerEditor.xaml.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Customisation
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for the quick container editor user control.
    /// </summary>
    internal partial class QuickContainerEditor : UserControl
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="QuickContainerEditor"/> class.
        /// </summary>
        public QuickContainerEditor()
        {
            this.InitializeComponent();
        }
        #endregion Constructors
    } // RSG.Editor.Controls.Customisation.QuickContainerEditor {Class}
} // RSG.Editor.Controls.Customisation {Namespace}
