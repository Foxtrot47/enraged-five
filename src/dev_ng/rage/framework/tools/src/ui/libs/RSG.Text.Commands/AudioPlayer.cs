﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace RSG.Text.Commands
{
    /// <summary>
    /// Static audio player class used by PlayLine and PlayConversation commands to play dialogue
    /// </summary>
    public static class AudioPlayer
    {
        private static MediaPlayer _audioPlayer;
        private static Queue<string> _playlist;
        private static bool _isPlaying;

        static AudioPlayer()
        {
            _audioPlayer = new MediaPlayer();
            _playlist = new Queue<string>();
        }

        /// <summary>
        /// Indicates wether the audio player is currently playing
        /// </summary>
        public static bool IsPlaying
        {
            get
            {
                return _isPlaying;
            }
        }

        /// <summary>
        /// Plays indiacted file. If something is currently playing it will be stopped. Clears the playlist.
        /// </summary>
        /// <param name="filePath"></param>
        public static void Play(string filePath)
        {
            Stop();
            PlayInternal(filePath);         
        }

        /// <summary>
        /// Adds file paths to playlist and starts playing first entry. If something is currently playing
        /// it will be stopped. Clears anything previously in the playlist.
        /// </summary>
        /// <param name="filePaths"></param>
        public static void Play(IList<string> filePaths)
        {
            Stop();
            foreach (string filePath in filePaths)
            {
                _playlist.Enqueue(filePath);
            }
            PlayInternal(_playlist.Dequeue());
        }

        private static void PlayInternal(string filePath)
        {      
            _audioPlayer.Open(new Uri(filePath));
            _audioPlayer.MediaEnded += AudioPlayer_MediaEnded;
            _audioPlayer.Play();
            _isPlaying = true;
        }

        /// <summary>
        /// Stops anything currently playing and clears the playlist
        /// </summary>
        public static void Stop()
        {
            _audioPlayer.Stop();
            _playlist.Clear();
            _isPlaying = false;
        }

        private static void AudioPlayer_MediaEnded(object sender, EventArgs e)
        {
            if (_playlist.Any())
            {
                string nextLine = _playlist.Dequeue();
                _audioPlayer.Open(new Uri(nextLine));
                _audioPlayer.Play();
            }
            else
            {
                _audioPlayer.MediaEnded -= AudioPlayer_MediaEnded;
                _isPlaying = false;
            }
        }
    }
}
