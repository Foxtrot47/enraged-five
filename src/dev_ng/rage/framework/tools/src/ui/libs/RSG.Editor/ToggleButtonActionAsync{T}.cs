﻿//---------------------------------------------------------------------------------------------
// <copyright file="ToggleButtonActionAsync{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Provides a abstract base class to any class that is used to implement a toggle button
    /// command.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the parameter pass into the can execute and execute methods.
    /// </typeparam>
    public abstract class ToggleButtonActionAsync<T> : CommandActionAsync<T>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ToggleButtonActionAsync{T}"/>
        /// class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        protected ToggleButtonActionAsync(ParameterResolverDelegate<T> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether state of the toggle command definition for the
        /// currently executed routed command is toggled or not.
        /// </summary>
        protected bool IsToggled
        {
            get
            {
                ToggleButtonCommand definition = this.CurrentDefinition as ToggleButtonCommand;
                if (definition != null)
                {
                    return definition.IsToggled;
                }

                throw new NotSupportedException(
                    "Only valid for actions that have been setup to use Toggle Definitions.");
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Create a new command definition for each command associated with this
        /// implementation.
        /// </summary>
        /// <param name="command">
        /// The command to create the definition for.
        /// </param>
        /// <returns>
        /// The new command definition created for the specified command.
        /// </returns>
        internal override CommandDefinition CreateDefinition(RockstarRoutedCommand command)
        {
            bool isToggled = this.GetInitialToggleState(command);
            ToggleButtonCommand definition = new ToggleButtonCommand(command, isToggled);
            definition.ToggledIcon = this.GetToggledIcon(command);
            return definition;
        }

        /// <summary>
        /// Determines what the initial toggle state for the definition to the specified
        /// command is.
        /// </summary>
        /// <param name="command">
        /// The command whose definitions toggle state is being set.
        /// </param>
        /// <returns>
        /// The initial toggle state for the definition to the specified command.
        /// </returns>
        protected virtual bool GetInitialToggleState(RockstarRoutedCommand command)
        {
            return false;
        }

        /// <summary>
        /// Retrieves the icon to use for the specified command in a toggled state if it's
        /// different to the icon used in the un-toggled state.
        /// </summary>
        /// <param name="command">
        /// The command whose definitions toggled icon is being set.
        /// </param>
        /// <returns>
        /// The icon to use for the specified command in a toggled state.
        /// </returns>
        protected virtual BitmapSource GetToggledIcon(RockstarRoutedCommand command)
        {
            return null;
        }
        #endregion Methods
    }  // RSG.Editor.ToggleButtonActionAsync{T} {Class}
} // RSG.Editor {Namespace}
