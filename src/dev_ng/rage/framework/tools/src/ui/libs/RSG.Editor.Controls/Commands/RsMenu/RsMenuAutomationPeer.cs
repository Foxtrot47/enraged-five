﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsMenuAutomationPeer.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Commands
{
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Automation.Peers;
    using System.Windows.Controls;

    /// <summary>
    /// Exposes <see cref="RSG.Editor.Controls.Commands.RsMenu"/> types to UI automation.
    /// </summary>
    public class RsMenuAutomationPeer : MenuAutomationPeer
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="RsMenuAutomationPeer"/> class.
        /// </summary>
        /// <param name="owner">
        /// The <see cref="RSG.Editor.Controls.Commands.RsMenu"/> object that is associated
        /// with this automation peer.
        /// </param>
        public RsMenuAutomationPeer(RsMenu owner)
            : base(owner)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Gets the collection of child elements of the parent element that is associated with
        /// this automation peer.
        /// </summary>
        /// <returns>
        /// A list of child automation peer elements.
        /// </returns>
        protected override List<AutomationPeer> GetChildrenCore()
        {
            return GetChildren(this.Owner as ItemsControl);
        }

        /// <summary>
        /// Gets the control type for the parent element that is associated with this
        /// automation peer.
        /// </summary>
        /// <returns>
        /// The System.Windows.Automation.Peers.AutomationControlType.Custom enumeration value.
        /// </returns>
        protected override AutomationControlType GetAutomationControlTypeCore()
        {
            return AutomationControlType.MenuBar;
        }

        /// <summary>
        /// Gets the collection of child elements of the specified parent element.
        /// </summary>
        /// <param name="parent">
        /// The parent element whose child element automation peers should be retrieved.
        /// </param>
        /// <returns>
        /// A list of child automation peer elements.
        /// </returns>
        private static List<AutomationPeer> GetChildren(ItemsControl parent)
        {
            List<AutomationPeer> children = null;
            ItemCollection items = parent.Items;
            ItemContainerGenerator generator = parent.ItemContainerGenerator;
            if (items.Count <= 0 || generator == null)
            {
                return children;
            }

            children = new List<AutomationPeer>(items.Count);
            for (int i = 0; i < items.Count; i++)
            {
                UIElement element = generator.ContainerFromIndex(i) as UIElement;
                if (element == null || element.IsVisible == false)
                {
                    continue;
                }

                AutomationPeer automationPeer = UIElementAutomationPeer.FromElement(element);
                if (automationPeer == null)
                {
                    automationPeer = UIElementAutomationPeer.CreatePeerForElement(element);
                }

                children.Add(automationPeer);
            }

            return children;
        }
        #endregion Methods
    } // RSG.Editor.Controls.Commands.RsMenuAutomationPeer {Class}
} // RSG.Editor.Controls.Commands {Namespace}
