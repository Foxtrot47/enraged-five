﻿//---------------------------------------------------------------------------------------------
// <copyright file="IFilteredItemsSource.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    using RSG.Base.Collections;

    /// <summary>
    /// Represents the items source for a filtered collection.
    /// </summary>
    public interface IFilteredItemsSource : IReadOnlyObservableSet
    {
        #region Methods
        /// <summary>
        /// Retrieves a value indicating the behaviour of the filter for the specified item.
        /// </summary>
        /// <param name="item">
        /// The item to get the filter behaviour for.
        /// </param>
        /// <returns>
        /// The filter behaviour for the specified item.
        /// </returns>
        FilterDescendantBehaviour GetFilterDescendantsBehavior(object item);
        #endregion Methods
    } // RSG.Editor.View.IFilteredItemsSource {Interface}
} // RSG.Editor.View {Namespace}
