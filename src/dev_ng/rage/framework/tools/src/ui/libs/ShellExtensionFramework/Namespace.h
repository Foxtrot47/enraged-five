#ifndef SEF_NAMESPACE_H
#define SEF_NAMESPACE_H

#if defined _MSC_VER && _MSC_VER >= 1300
#pragma once
#endif

#include "ShellExtensionFramework.h"

// Windows headers
#include <atlbase.h>
#include <atlcom.h>
#include <shlobj.h>
#include <comdef.h>
#include <Windows.h>

BEGIN_SHELL_EXTENSION_FRAMEWORK_NS

/**
	\brief Abstract Shell namespace base class.
 */
class CNamespace
{
public:
	CNamespace( ) { }
	virtual ~CNamespace( ) { }

	
};

END_SHELL_EXTENSION_FRAMEWORK_NS

#endif // SEF_NAMESPACE_H
