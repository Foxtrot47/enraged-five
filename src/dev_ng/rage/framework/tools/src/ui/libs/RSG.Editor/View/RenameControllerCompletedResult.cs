﻿//---------------------------------------------------------------------------------------------
// <copyright file="RenameControllerCompletedResult.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    using System;

    /// <summary>
    /// Defines the different completion results for a rename controller.
    /// </summary>
    public enum RenameControllerCompletedResult
    {
        /// <summary>
        /// The controller completed due to the fact the item was renamed.
        /// </summary>
        Successful,

        /// <summary>
        /// The controller completed due to the fact the validation failed causing no renaming
        /// to occur.
        /// </summary>
        ValidationFailed,

        /// <summary>
        /// The controller completed due to the user cancelling the operation causing no
        /// renaming to occur.
        /// </summary>
        Cancelled,
    } // RSG.Editor.View.RenameControllerCompletedResult {Enum}
} // RSG.Editor.View {Namespace}
