﻿//---------------------------------------------------------------------------------------------
// <copyright file="ClassViewNodeBase.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.ClassView
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Media.Imaging;
    using RSG.Editor.View;

    /// <summary>
    /// Provides a base class for a node inside the class view.
    /// </summary>
    public abstract class ClassViewNodeBase :
        ThreadedHierarchicalBase,
        ITreeDisplayItem,
        IPrioritisedComparable
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="ExpandedIcon"/> property.
        /// </summary>
        private BitmapSource _expandedIcon;

        /// <summary>
        /// The private field used for the <see cref="FontStyle"/> property.
        /// </summary>
        private FontStyle _fontStyle;

        /// <summary>
        /// The private field used for the <see cref="FontWeight"/> property.
        /// </summary>
        private FontWeight _fontWeight;

        /// <summary>
        /// The private field used for the <see cref="Icon"/> property.
        /// </summary>
        private BitmapSource _icon;

        /// <summary>
        /// The private reference to the parent node.
        /// </summary>
        private ClassViewNodeBase _parent;

        /// <summary>
        /// The private field used for the <see cref="Text"/> property.
        /// </summary>
        private string _text;

        /// <summary>
        /// The private field used for the <see cref="ToolTip"/> property.
        /// </summary>
        private string _tooltip;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ClassViewNodeBase"/> class.
        /// </summary>
        /// <param name="parent">
        /// The parent node for this object.
        /// </param>
        public ClassViewNodeBase(ClassViewNodeBase parent)
        {
            this.IsExpandable = false;
            this._parent = parent;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the bitmap source that is used to display the icon for this item when
        /// it has been expanded.
        /// </summary>
        public BitmapSource ExpandedIcon
        {
            get { return this._expandedIcon; }
            protected set { this.SetProperty(ref this._expandedIcon, value); }
        }

        /// <summary>
        /// Gets or sets the style that the font on this item should be using.
        /// </summary>
        public FontStyle FontStyle
        {
            get { return this._fontStyle; }
            protected set { this.SetProperty(ref this._fontStyle, value); }
        }

        /// <summary>
        /// Gets or sets the weight or thickness that the font on this item should be using.
        /// </summary>
        public FontWeight FontWeight
        {
            get { return this._fontWeight; }
            protected set { this.SetProperty(ref this._fontWeight, value); }
        }

        /// <summary>
        /// Gets or sets the bitmap source that is used to display the icon for this item.
        /// </summary>
        public virtual BitmapSource Icon
        {
            get { return this._icon; }
            protected set { this.SetProperty(ref this._icon, value); }
        }

        /// <summary>
        /// Gets the bitmap source that is used to display the overlay icon for this item.
        /// </summary>
        public virtual BitmapSource OverlayIcon
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the parent project node for this node.
        /// </summary>
        public ClassViewProjectNode ParentProjectNode
        {
            get
            {
                ClassViewProjectNode project = this as ClassViewProjectNode;
                if (project != null)
                {
                    return project;
                }

                if (this._parent == null)
                {
                    return null;
                }

                return this._parent.ParentProjectNode;
            }
        }

        /// <summary>
        /// Gets the priority value for this object.
        /// </summary>
        public virtual int Priority
        {
            get { return 0; }
        }

        /// <summary>
        /// Gets the bitmap source that is used to display the state icon for this item.
        /// </summary>
        public BitmapSource StateIcon
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the text that is used to represent the state of this item.
        /// </summary>
        public string StateToolTipText
        {
            get { return null; }
        }

        /// <summary>
        /// Gets or sets the text used to display this item.
        /// </summary>
        public string Text
        {
            get { return this._text; }
            set { this.SetProperty(ref this._text, value); }
        }

        /// <summary>
        /// Gets or sets the text that is used in the tooltip for this item.
        /// </summary>
        public string ToolTip
        {
            get { return this._tooltip ?? this._text; }
            protected set { this.SetProperty(ref this._tooltip, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Compares this object to another object, returning an integer that indicates the
        /// relationship.
        /// </summary>
        /// <param name="obj">
        /// The object to compare.
        /// </param>
        /// <returns>
        /// Less than zero if this is less than object, zero if this is equal to object, or a
        /// value greater than zero if this is greater than object.
        /// </returns>
        public int CompareTo(object obj)
        {
            IDisplayItem item = obj as IDisplayItem;
            if (item != null)
            {
                return Comparer.Default.Compare(this.Text, item.Text);
            }

            return 0;
        }

        /// <summary>
        /// Creates the child elements for this view model. This is only called once the item
        /// has been expanded in the user interface.
        /// </summary>
        /// <param name="collection">
        /// The collection to add the elements to.
        /// </param>
        protected override void CreateChildren(ICollection<object> collection)
        {
        }
        #endregion Methods
    } // RSG.Metadata.ViewModel.ClassView.ClassViewNodeBase {Class}
} // RSG.Metadata.ViewModel.ClassView {Namespace}
