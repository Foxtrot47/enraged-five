﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace RSG.Editor.Controls.ZoomCanvas.Commands
{
    /// <summary>
    /// The action logic that is responsible for zooming the zoomable canvas based on
    /// the specified extents mode.
    /// </summary>
    public class ZoomExtentsAction : ButtonAction<ZoomableCanvas, ZoomExtentsMode>
    {
        #region Constants
        /// <summary>
        /// Number of pixels (in viewport space) to pad the zoom by.
        /// </summary>
        private const double _zoomPadding = 10.0;
        #endregion
    
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="EmailImplementer"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public ZoomExtentsAction(ParameterResolverDelegate<ZoomableCanvas> resolver)
            : base(resolver)
        {
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="control">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="mode">
        /// The extents mode we should use for zooming.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ZoomableCanvas control, ZoomExtentsMode mode)
        {
            // Check whether the control has any contents.
            if (control.Children.Count == 0)
            {
                return false;
            }

            // Make sure the requested zoom extents mode is supported by the canvas scale mode.
            if (control.ScaleMode == ScaleMode.Uniform &&
                (mode == ZoomExtentsMode.Horizontal || mode == ZoomExtentsMode.Vertical))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="control">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="mode">
        /// The extents mode we should use for zooming.
        /// </param>
        public override void Execute(ZoomableCanvas control, ZoomExtentsMode mode)
        {
            if (mode == ZoomExtentsMode.Horizontal ||
                mode == ZoomExtentsMode.Vertical ||
                mode == ZoomExtentsMode.HorizontalAndVertical)
            {
                bool scaleHorizontally = (mode == ZoomExtentsMode.Horizontal || mode == ZoomExtentsMode.HorizontalAndVertical);
                bool scaleVertically = (mode == ZoomExtentsMode.Vertical || mode == ZoomExtentsMode.HorizontalAndVertical);

                ZoomToRectangle(control, control.Extent, scaleHorizontally, scaleVertically);
            }
            else if (mode == ZoomExtentsMode.Inner ||
                mode == ZoomExtentsMode.Outer)
            {
                PerformUniformScale(control, mode);
            }
        }
        #endregion

        #region Util Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="control"></param>
        /// <param name="mode"></param>
        private void PerformUniformScale(ZoomableCanvas control, ZoomExtentsMode mode)
        {
            Size adjustedRenderSize = new Size(
                control.RenderSize.Width - 2.0 * _zoomPadding,
                control.RenderSize.Height - 2.0 * _zoomPadding);

            Vector newScale = control.Scale;

            if ((mode == ZoomExtentsMode.Inner && control.Extent.Width > control.Extent.Height) ||
                (mode == ZoomExtentsMode.Outer && control.Extent.Height > control.Extent.Width))
            {
                newScale.X = adjustedRenderSize.Height / control.Extent.Height;
                newScale.Y = adjustedRenderSize.Height / control.Extent.Height;
            }
            else
            {
                newScale.X = adjustedRenderSize.Width / control.Extent.Width;
                newScale.Y = adjustedRenderSize.Width / control.Extent.Width;
            }

            Point newOffset = control.Offset;
            newOffset.X = (control.Extent.X + control.Extent.Width / 2) * newScale.X;
            newOffset.X -= adjustedRenderSize.Width / 2 + _zoomPadding;
            newOffset.Y = (control.Extent.Y + control.Extent.Height / 2) * newScale.Y;
            newOffset.Y -= adjustedRenderSize.Height / 2 + _zoomPadding;

            control.Scale = newScale;
            control.Offset = newOffset;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="control">
        /// Zoom canvas control to adjust.
        /// </param>
        /// <param name="contentRect">
        /// Rectangle in content space to zoom to.
        /// </param>
        /// <param name="scaleHorizontally">
        /// Whether we should apply horizontal scaling.
        /// </param>
        /// <param name="scaleVertically">
        /// Whether we should apply vertical scaling.
        /// </param>
        public void ZoomToRectangle(ZoomableCanvas control, Rect contentRect, bool scaleHorizontally, bool scaleVertically)
        {
            // Adjust the render size by the padding amount.
            Size adjustedRenderSize = new Size(
                control.RenderSize.Width - 2.0 * _zoomPadding,
                control.RenderSize.Height - 2.0 * _zoomPadding);

            // Determine the new scale/offset.
            Vector newScale = control.Scale;
            Point newOffset = control.Offset;

            if (scaleHorizontally)
            {
                newScale.X = adjustedRenderSize.Width / contentRect.Width;
                newOffset.X = (contentRect.X + contentRect.Width / 2) * newScale.X;
                newOffset.X -= adjustedRenderSize.Width / 2 + _zoomPadding;
            }
            if (scaleVertically)
            {
                newScale.Y = adjustedRenderSize.Height / contentRect.Height;
                newOffset.Y = (contentRect.Y + contentRect.Height / 2) * newScale.Y;
                newOffset.Y -= adjustedRenderSize.Height / 2 + _zoomPadding;
            }

            // Set the new values.
            control.Scale = newScale;
            control.Offset = newOffset;
        }
        #endregion
    }
}
