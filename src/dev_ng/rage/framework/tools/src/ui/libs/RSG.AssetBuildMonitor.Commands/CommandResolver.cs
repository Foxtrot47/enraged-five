﻿//---------------------------------------------------------------------------------------------
// <copyright file="CommandResolver.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.AssetBuildMonitor.Commands
{
    using RSG.Editor;

    /// <summary>
    /// Represents the method that is used to retrieve a command parameter object of the
    /// specified type for the specified command.
    /// </summary>
    /// <param name="commandData">
    /// The data that was sent with the command containing among other things the command
    /// whose command parameter needs to be resolved.
    /// </param>
    /// <returns>
    /// The command parameter object of the specified type for the specified command.
    /// </returns>
    public delegate CommandArgs CommandResolver(CommandData commandData);

} // RSG.AssetBuildMonitor.Commands namespace
