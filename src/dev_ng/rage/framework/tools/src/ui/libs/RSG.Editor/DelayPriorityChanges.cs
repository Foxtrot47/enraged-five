﻿//---------------------------------------------------------------------------------------------
// <copyright file="DelayPriorityChanges.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    /// <summary>
    /// Defines a class that is used to delay the notifications about any priority changes
    /// seen inside the command system.
    /// </summary>
    public class DelayPriorityChanges : DisposableObject
    {
        #region Fields
        /// <summary>
        /// The private count value of the current number of instances alive of this object.
        /// When this reaches zero the notifications will be fired.
        /// </summary>
        private static int _count;

        /// <summary>
        /// A generic object used to sync access to the count parameter across multiple
        /// threads.
        /// </summary>
        private static object _syncRoot = new object();
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DelayPriorityChanges"/> class.
        /// </summary>
        public DelayPriorityChanges()
        {
            lock (_syncRoot)
            {
                _count++;
                if (_count == 1)
                {
                    RockstarCommandManager.DelayingPriorityChanges = true;
                }
            }
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Disposes of the managed resources and if the delay count parameter is 0 commits
        /// the changes.
        /// </summary>
        protected override void DisposeManagedResources()
        {
            bool handleCommit = false;
            lock (_syncRoot)
            {
                _count--;
                if (_count == 0)
                {
                    handleCommit = true;
                }
            }

            if (handleCommit)
            {
                RockstarCommandManager.DelayingPriorityChanges = false;
            }
        }
        #endregion Methods
    } // RSG.Editor.DelayPriorityChanges {Class}
} // RSG.Editor {Namespace}
