﻿//---------------------------------------------------------------------------------------------
// <copyright file="CharacterMappingViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.ViewModel
{
    using System;
    using RSG.Editor.View;
    using RSG.Text.Model;

    /// <summary>
    /// 
    /// </summary>
    public class CharacterMappingViewModel : ViewModelBase<CharacterMapping>
    {
        #region Fields
        /// <summary>
        /// 
        /// </summary>
        private CharacterMappingsViewModel _characterMappings;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mapping">
        /// 
        /// </param>
        /// <param name="parent">
        /// 
        /// </param>
        public CharacterMappingViewModel(CharacterMapping mapping, CharacterMappingsViewModel parent)
            : base(mapping, true)
        {
            this._characterMappings = parent;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public Guid CharacterId
        {
            get { return this.Model.CharacterId; }
            set { this.Model.CharacterId = value; }
        }

        /// <summary>
        /// Gets the collection of characters that can be used by this line.
        /// </summary>
        public IReadOnlyViewModelCollection<DialogueCharacterViewModel> Characters
        {
            get { return this._characterMappings.Characters; }
        }

        /// <summary>
        /// Gets or sets the character that is used in this mapping.
        /// </summary>
        public DialogueCharacterViewModel SelectedCharacter
        {
            get
            {
                return this._characterMappings.Characters.GetCharacter(this.Model.CharacterId);
            }

            set
            {
                if (value != null)
                {
                    this.Model.CharacterId = value.Id;
                }
                else
                {
                    this.Model.CharacterId = Guid.Empty;
                }

                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int SpeakerId
        {
            get { return this.Model.SpeakerId; }
            set { this.Model.SpeakerId = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string VoiceName
        {
            get { return this.Model.VoiceName; }
            set { this.Model.VoiceName = value; }
        }
        #endregion Properties
    }
}
