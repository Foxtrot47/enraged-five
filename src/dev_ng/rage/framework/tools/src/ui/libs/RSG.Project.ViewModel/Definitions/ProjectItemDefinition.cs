﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProjectItemDefinition.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel.Definitions
{
    using System;
    using System.IO;
    using System.Windows.Media.Imaging;
    using RSG.Project.Model;

    /// <summary>
    /// Represents the definition for a single project item type.
    /// </summary>
    public abstract class ProjectItemDefinition : IDefinition
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ProjectItemDefinition"/> class.
        /// </summary>
        protected ProjectItemDefinition()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the name that is used as the default value for a new instance of this
        /// definition.
        /// </summary>
        public abstract string DefaultName { get; }

        /// <summary>
        /// Gets the description for this item definition to be displayed in the add dialog.
        /// </summary>
        public abstract string Description { get; }

        /// <summary>
        /// Gets the extension used for this defined item.
        /// </summary>
        public abstract string Extension { get; }

        /// <summary>
        /// Gets the icon that is used to display this project item.
        /// </summary>
        public abstract BitmapSource Icon { get; }

        /// <summary>
        /// Gets the name for this defined project.
        /// </summary>
        public abstract string Name { get; }

        /// <summary>
        /// Gets a byte array containing the data that is used to create a new instance of the
        /// defined project.
        /// </summary>
        public abstract byte[] Template { get; }

        /// <summary>
        /// Gets the type of this item to be displayed in the add dialog.
        /// </summary>
        public abstract string Type { get; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Gives the definition a opportunity to run a wizard before the item is created so
        /// that the user can set certain properties into the template before the file gets
        /// saved and replace all other variables.
        /// </summary>
        /// <param name="template">
        /// The template that is being used to create the project.
        /// </param>
        /// <param name="fullPath">
        /// The fullPath the new item is being saved to. So that relative paths can be
        /// determined.
        /// </param>
        /// <returns>
        /// A value indicating whether the wizard finished successfully. If false is returned
        /// the creation process is cancelled.
        /// </returns>
        /// <remarks>
        /// This method should modify the specified template with any additional properties
        /// from the wizard and replaced variables.
        /// </remarks>
        public virtual bool ReplaceVariables(ref byte[] template, string fullPath)
        {
            return true;
        }
        #endregion Methods
    } // RSG.Project.ViewModel.Definitions.ProjectItemDefinition {Class}
} // RSG.Project.ViewModel.Definitions {Namespace}
