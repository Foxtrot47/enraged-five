﻿//---------------------------------------------------------------------------------------------
// <copyright file="AddNewItemAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Windows;
    using RSG.Base.Extensions;
    using RSG.Editor;
    using RSG.Interop.Microsoft.Windows;
    using RSG.Project.Commands.Resources;
    using RSG.Project.Model;
    using RSG.Project.ViewModel;
    using RSG.Project.ViewModel.Definitions;

    /// <summary>
    /// Contains the logic for the <see cref="ProjectCommands.AddNewItem"/> routed command.
    /// This class cannot be inherited.
    /// </summary>
    public sealed class AddNewItemAction : ButtonAction<ProjectCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="AddNewItemAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public AddNewItemAction(ProjectCommandResolver resolver)
            : base(new ParameterResolverDelegate<ProjectCommandArgs>(resolver))
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ProjectCommandArgs args)
        {
            if (args == null)
            {
                return false;
            }

            List<ProjectNode> projectSelection = this.ProjectSelection(args.SelectedNodes);
            if (projectSelection.Count != 1 || !projectSelection[0].CanHaveItemsAdded)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(ProjectCommandArgs args)
        {
            IMessageBoxService msgService = this.GetService<IMessageBoxService>();
            IProjectAddViewService addViewService = this.GetService<IProjectAddViewService>();
            if (msgService == null || addViewService == null)
            {
                Debug.Assert(false, "Unable to add new item as services are missing.");
                return;
            }

            List<ProjectNode> projectSelection = this.ProjectSelection(args.SelectedNodes);
            ProjectNode project = projectSelection.FirstOrDefault();
            if (projectSelection.Count != 1 || !project.CanHaveItemsAdded)
            {
                return;
            }

            IHierarchyNode parent = this.GetNewItemsParent(args.SelectedNodes) ?? project;
            if (parent == null)
            {
                Debug.Assert(parent != null, "Parent sanity check failed.");
                return;
            }

            IProjectItemScope scope = project.Model as IProjectItemScope;
            if (scope == null)
            {
                Debug.Assert(false, "Unable to add new item, no scope can be found.");
                return;
            }

            ProjectItemDefinition selectedDefinition = null;
            string fullPath = null;
            string directory = scope.DirectoryPath;
            string initialDirectory = scope.DirectoryPath;
            string initialName = null;
            while (true)
            {
                addViewService.ShowNewItemSelector(
                    project,
                    initialDirectory,
                    initialName,
                    out selectedDefinition,
                    out fullPath);
                if (selectedDefinition == null || String.IsNullOrWhiteSpace(fullPath))
                {
                    throw new OperationCanceledException();
                }

                FileInfo info = new FileInfo(fullPath);
                if (info.Exists)
                {
                    string name = Path.GetFileName(fullPath);
                    IHierarchyNode existingNode = project.FindNodeWithFullPath(fullPath);
                    if (existingNode != null)
                    {
                        msgService.ShowStandardErrorBox(
                            StringTable.AddExistingOpenedFileMsg.FormatInvariant(name),
                            null);

                        initialDirectory = Path.GetDirectoryName(fullPath);
                        initialName = Path.GetFileName(fullPath);
                        continue;
                    }

                    MessageBoxResult result = msgService.Show(
                        StringTable.AddExistingFileWarningMsg.FormatInvariant(name),
                        null,
                        MessageBoxButton.YesNoCancel,
                        MessageBoxImage.Warning,
                        MessageBoxResult.Yes);

                    if (result == MessageBoxResult.No)
                    {
                        msgService.ShowStandardErrorBox(
                            StringTable.AddExistingFileErrorMsg.FormatInvariant(name),
                            null);

                        initialDirectory = Path.GetDirectoryName(fullPath);
                        initialName = Path.GetFileName(fullPath);
                        continue;
                    }
                    else if (result == MessageBoxResult.Yes)
                    {
                        info.IsReadOnly = false;
                        break;
                    }
                    else if (result == MessageBoxResult.Cancel)
                    {
                        throw new OperationCanceledException();
                    }
                }

                break;
            }

            string relativePath = Shlwapi.MakeRelativeFromDirectoryToFile(directory, fullPath);
            if (relativePath == String.Empty)
            {
                relativePath = fullPath;
            }

            if (scope.Find(relativePath) != null)
            {
                return;
            }

            byte[] template = selectedDefinition.Template;
            if (!selectedDefinition.ReplaceVariables(ref template, fullPath))
            {
                throw new OperationCanceledException();
            }

            if (!Directory.Exists(Path.GetDirectoryName(fullPath)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(fullPath));
            }

            using (FileStream stream = new FileStream(fullPath, FileMode.OpenOrCreate))
            {
                stream.SetLength(0);
                using (BinaryWriter writer = new BinaryWriter(stream))
                {
                    if (template != null)
                    {
                        writer.Write(template);
                    }

                    writer.Flush();
                    writer.Close();
                }
            }

            IHierarchyNode newNode = project.AddNewFile(parent, relativePath);
            if (args.NodeSelectionDelegate != null)
            {
                args.NodeSelectionDelegate(newNode);
            }
        }

        /// <summary>
        /// Retrieves the project selection from the specified selection collection by
        /// collecting all of the selected nodes parent project and removing duplicates.
        /// </summary>
        /// <param name="selection">
        /// The collection containing all of the selected nodes.
        /// </param>
        /// <returns>
        /// The project selection from the specified selection collection.
        /// </returns>
        private List<ProjectNode> ProjectSelection(IEnumerable<IHierarchyNode> selection)
        {
            List<ProjectNode> projectSelection = new List<ProjectNode>();
            if (selection == null)
            {
                return projectSelection;
            }

            foreach (IHierarchyNode selectedNode in selection)
            {
                ProjectNode parentProject = selectedNode.ParentProject;
                if (parentProject != null && !projectSelection.Contains(parentProject))
                {
                    projectSelection.Add(parentProject);
                }
            }

            return projectSelection;
        }

        /// <summary>
        /// Determines which item should be the parent to a new added item from the specified
        /// collection of items.
        /// </summary>
        /// <param name="items">
        /// The collection of items that could be the parent.
        /// </param>
        /// <returns>
        /// The first item if all of them are valid; otherwise, null.
        /// </returns>
        private IHierarchyNode GetNewItemsParent(IEnumerable<IHierarchyNode> items)
        {
            IHierarchyNode parent = null;
            foreach (IHierarchyNode selectedNode in items)
            {
                if (selectedNode == null)
                {
                    continue;
                }

                if (!selectedNode.CanHaveItemsAdded)
                {
                    return null;
                }
                else if (parent == null)
                {
                    parent = selectedNode;
                }
            }

            return parent;
        }
        #endregion Methods
    } // RSG.Project.Commands.AddNewItemAction {Class}
} // RSG.Project.Commands {Namespace}
