﻿//---------------------------------------------------------------------------------------------
// <copyright file="FileWatcherManager.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    /// <summary>
    /// Provides a central class to listen to external changes made to a file on the disk and
    /// the read-only attribute.
    /// </summary>
    public class FileWatcherManager
    {
        #region Fields
        /// <summary>
        /// The private collection of current file system watchers running.
        /// </summary>
        private Dictionary<Key, FileSystemWatcher> _cache;

        /// <summary>
        /// The private dictionary containing the current values for a watched file.
        /// </summary>
        private Dictionary<string, CurrentValues> _currentValues;

        /// <summary>
        /// The private set of full paths whose change event have been suspended.
        /// </summary>
        private HashSet<string> _suspendedChangePaths;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="FileWatcherManager"/> class.
        /// </summary>
        public FileWatcherManager()
        {
            this._cache = new Dictionary<Key, FileSystemWatcher>();
            this._currentValues = new Dictionary<string, CurrentValues>();
            this._suspendedChangePaths = new HashSet<string>();
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs whenever a watched file has its last write time changed.
        /// </summary>
        public event EventHandler<ModifiedTimeChangedEventArgs> ModifiedTimeChanged;

        /// <summary>
        /// Occurs whenever a watched file has its read-only flag changed.
        /// </summary>
        public event EventHandler<ReadOnlyChangedEventArgs> ReadOnlyFlagChanged;
        #endregion Events

        #region Methods
        /// <summary>
        /// Registers the specified path to be watched for changes.
        /// </summary>
        /// <param name="fullPath">
        /// The path to the file that needs watching.
        /// </param>
        public void RegisterFile(string fullPath)
        {
            string directory = Path.GetDirectoryName(fullPath);
            string filter = Path.GetFileName(fullPath);

            Key cacheKey = new Key(directory, filter);
            FileSystemWatcher watcher = null;
            if (!this._cache.TryGetValue(cacheKey, out watcher))
            {
                watcher = new FileSystemWatcher(directory, filter);
                watcher.IncludeSubdirectories = false;
                watcher.NotifyFilter = NotifyFilters.Attributes | NotifyFilters.LastWrite;
                watcher.Changed += this.OnWatcherChanged;
                watcher.EnableRaisingEvents = true;

                this._cache.Add(cacheKey, watcher);

                FileInfo info = new FileInfo(fullPath);
                CurrentValues currentValues = new CurrentValues();
                if (info.Exists)
                {
                    currentValues.IsReadOnly = info.IsReadOnly;
                    currentValues.LastWriteTimeUtc = info.LastWriteTimeUtc.Ticks;
                }

                this._currentValues.Add(fullPath, currentValues);
            }
        }

        /// <summary>
        /// Suspend the change event for the specified full path. This method returns a
        /// disposable scope object that when disposed of will remove the suspension.
        /// </summary>
        /// <param name="fullPath">
        /// The full path to the file whose change event will be suspended.
        /// </param>
        /// <returns>
        /// A disposable scope object that when disposed of will remove the suspension.
        /// </returns>
        public DisposableObject SuspendChangeEvent(string fullPath)
        {
            return new SuspendChangeEventScope(fullPath, this);
        }

        /// <summary>
        /// Unregisters the specified file as a watched item.
        /// </summary>
        /// <param name="fullPath">
        /// The path to the file that doesn't need watching anymore.
        /// </param>
        public void UnregisterFile(string fullPath)
        {
            string directory = Path.GetDirectoryName(fullPath);
            string filter = Path.GetFileName(fullPath);

            Key cacheKey = new Key(directory, filter);
            FileSystemWatcher watcher = null;
            if (this._cache.TryGetValue(cacheKey, out watcher))
            {
                using (watcher)
                {
                    watcher.Changed -= this.OnWatcherChanged;
                    watcher.EnableRaisingEvents = false;
                }

                this._cache.Remove(cacheKey);
            }

            if (this._currentValues.ContainsKey(fullPath))
            {
                this._currentValues.Remove(fullPath);
            }
        }

        /// <summary>
        /// Called whenever a attribute is changed on a watched file or the last write time is
        /// modified.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.IO.FileSystemEventArgs containing the event data.
        /// </param>
        private void OnWatcherChanged(object sender, FileSystemEventArgs e)
        {
            bool readOnlyChanged = false;
            bool lastWriteTimeChanged = false;
            bool suspended = false;
            bool isReadOnly = false;
            long timeDifference = 0;
            if (this._suspendedChangePaths.Contains(Path.GetFullPath(e.FullPath)))
            {
                suspended = true;
            }

            CurrentValues values = new CurrentValues();
            if (!this._currentValues.TryGetValue(e.FullPath, out values))
            {
                readOnlyChanged = true;
                lastWriteTimeChanged = true;
            }
            else
            {
                FileInfo info = new FileInfo(e.FullPath);
                if (values.IsReadOnly != info.IsReadOnly)
                {
                    readOnlyChanged = true;
                }

                timeDifference = info.LastWriteTimeUtc.Ticks - values.LastWriteTimeUtc;
                long ellipse = TimeSpan.FromMilliseconds(250).Ticks;
                if (timeDifference > ellipse)
                {
                    lastWriteTimeChanged = true;
                }

                if (info.Exists)
                {
                    isReadOnly = info.IsReadOnly;
                    values.IsReadOnly = info.IsReadOnly;
                    values.LastWriteTimeUtc = info.LastWriteTimeUtc.Ticks;
                }
            }

            this._currentValues[e.FullPath] = values;
            if (!lastWriteTimeChanged && !readOnlyChanged)
            {
                return;
            }

            if (lastWriteTimeChanged)
            {
                if (!suspended)
                {
                    EventHandler<ModifiedTimeChangedEventArgs> handler =
                        this.ModifiedTimeChanged;
                    if (handler != null)
                    {
                        handler(this, new ModifiedTimeChangedEventArgs(e.FullPath));
                    }
                }
            }

            if (readOnlyChanged)
            {
                EventHandler<ReadOnlyChangedEventArgs> handler = this.ReadOnlyFlagChanged;
                if (handler != null)
                {
                    handler(
                        this,
                        new ReadOnlyChangedEventArgs(!isReadOnly, isReadOnly, e.FullPath));
                }
            }
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// Contains the previous read-only value and previous write time for a file so that
        /// it can be checked whether these values have changed.
        /// </summary>
        private class CurrentValues
        {
            #region Fields
            /// <summary>
            /// The private field used for the <see cref="IsReadOnly"/> property.
            /// </summary>
            private bool _isReadOnly;

            /// <summary>
            /// The private field used for the <see cref="LastWriteTimeUtc"/> property.
            /// </summary>
            private long _lastWriteTimeUtc;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="CurrentValues"/> class.
            /// </summary>
            public CurrentValues()
            {
                this._isReadOnly = false;
                this._lastWriteTimeUtc = DateTime.UtcNow.Ticks;
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets or sets a value indicating whether the associated file is read-only or
            /// not.
            /// </summary>
            public bool IsReadOnly
            {
                get { return this._isReadOnly; }
                set { this._isReadOnly = value; }
            }

            /// <summary>
            /// Gets or sets the last write time for the associated file.
            /// </summary>
            public long LastWriteTimeUtc
            {
                get { return this._lastWriteTimeUtc; }
                set { this._lastWriteTimeUtc = value; }
            }
            #endregion Properties
        } // FileWatcherManager.CurrentValues {Class}

        /// <summary>
        /// The class that is used to index the file watcher cache.
        /// </summary>
        private class Key : Tuple<string, string>
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="Key"/> class.
            /// </summary>
            /// <param name="directory">
            /// The directory to monitor.
            /// </param>
            /// <param name="filter">
            /// The type of files to watch.
            /// </param>
            public Key(string directory, string filter)
                : base(directory, filter)
            {
            }
            #endregion Constructors
        } // FileWatcherManager.Key {Class}

        /// <summary>
        /// Provides functionality so that the file watchers change event can be suspended
        /// for a specific file.
        /// </summary>
        private class SuspendChangeEventScope : DisposableObject
        {
            #region Fields
            /// <summary>
            /// The full path to the file whose change event should be suspended.
            /// </summary>
            private string _fullPath;

            /// <summary>
            /// The private reference of the file watcher manager that this is suspending.
            /// </summary>
            private FileWatcherManager _manager;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="SuspendChangeEventScope"/> class.
            /// </summary>
            /// <param name="fullPath">
            /// The full path to the file that change event should be suspended.
            /// </param>
            /// <param name="manager">
            /// The file watcher manager that is being suspended.
            /// </param>
            public SuspendChangeEventScope(string fullPath, FileWatcherManager manager)
            {
                if (manager == null)
                {
                    throw new SmartArgumentNullException(() => manager);
                }

                if (fullPath == null)
                {
                    return;
                }

                this._fullPath = Path.GetFullPath(fullPath);
                this._manager = manager;
                this._manager._suspendedChangePaths.Add(this._fullPath);
            }
            #endregion Constructors

            #region Methods
            /// <summary>
            /// Finishes the suspended state that this object represents.
            /// </summary>
            protected override void DisposeManagedResources()
            {
                if (this._manager == null)
                {
                    return;
                }

                FileInfo info = new FileInfo(this._fullPath);
                CurrentValues currentValues = new CurrentValues();
                currentValues.LastWriteTimeUtc = DateTime.UtcNow.Ticks;
                if (info.Exists)
                {
                    currentValues.IsReadOnly = info.IsReadOnly;
                }
                else
                {
                    currentValues.IsReadOnly = false;
                }

                this._manager._suspendedChangePaths.Remove(this._fullPath);
                this._manager._currentValues[this._fullPath] = currentValues;
            }
            #endregion Methods
        } // FileWatcherManager.SuspendChangeEventScope {Class}
        #endregion
    }  // RSG.Editor.FileWatcherManager {Class}
} // RSG.Editor {Namespace}
