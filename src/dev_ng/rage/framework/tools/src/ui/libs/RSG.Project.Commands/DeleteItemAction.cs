﻿//---------------------------------------------------------------------------------------------
// <copyright file="DeleteItemAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Windows;
    using RSG.Base.Extensions;
    using RSG.Editor;
    using RSG.Editor.Controls.Dock.ViewModel;
    using RSG.Editor.SharedCommands;
    using RSG.Project.Commands.Resources;
    using RSG.Project.Model;
    using RSG.Project.ViewModel;

    /// <summary>
    /// Contains the logic for the <see cref="RockstarCommands.Delete"/> routed command. This
    /// class cannot be inherited.
    /// </summary>
    public sealed class DeleteItemAction : ButtonAction<ProjectCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DeleteItemAction"/> class.
        /// </summary>
        /// <param name="serviceProvider">
        /// The service provider that is used by this action to perform application specific
        /// operations, for example selecting a file to open.
        /// </param>
        public DeleteItemAction(ICommandServiceProvider serviceProvider)
            : base(serviceProvider)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="DeleteItemAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public DeleteItemAction(ProjectCommandResolver resolver)
            : base(new ParameterResolverDelegate<ProjectCommandArgs>(resolver))
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ProjectCommandArgs args)
        {
            if (args == null)
            {
                return false;
            }

            List<IHierarchyNode> rootSelection = this.RootSelection(args.SelectedNodes);
            if (rootSelection.Count == 0)
            {
                return false;
            }

            foreach (IHierarchyNode node in rootSelection)
            {
                if (node.CanBeRemoved == false)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(ProjectCommandArgs args)
        {
            IDocumentManagerService docService = this.GetService<IDocumentManagerService>();
            if (docService == null)
            {
                Debug.Fail(
                    "Unable to save document as due to the fact the service is missing.");
                return;
            }

            IMessageBoxService msgService = this.GetService<IMessageBoxService>();
            IProjectAddViewService addViewService = this.GetService<IProjectAddViewService>();
            if (msgService == null || addViewService == null)
            {
                Debug.Assert(false, "Unable to delete item as service is missing.");
                return;
            }

            List<IHierarchyNode> rootSelection = this.RootSelection(args.SelectedNodes);
            if (rootSelection.Count == 0)
            {
                return;
            }

            List<ProjectNode> removedProjects = new List<ProjectNode>();
            List<FileNode> removedFiles = new List<FileNode>();
            foreach (IHierarchyNode node in rootSelection)
            {
                if (node.CanBeRemoved == false)
                {
                    return;
                }

                ProjectNode projectNode = node as ProjectNode;
                if (projectNode != null)
                {
                    removedProjects.Add(projectNode);
                }
                else
                {
                    FileNode fileNode = node as FileNode;
                    if (fileNode != null)
                    {
                        removedFiles.Add(fileNode);
                    }
                }

                foreach (IHierarchyNode child in node.GetChildren(true))
                {
                    ProjectNode childProject = child as ProjectNode;
                    if (childProject != null)
                    {
                        removedProjects.Add(childProject);
                    }
                }
            }

            bool deleting = false;
            if (!this.Confirm(rootSelection, msgService, out deleting))
            {
                throw new OperationCanceledException();
            }

            if (removedProjects.Count > 0)
            {
                docService.UnloadProjects(removedProjects, true);
            }
            else
            {

                List<DocumentItem> documentsToClose = new List<DocumentItem>();
                foreach (DocumentItem document in args.OpenedDocuments)
                {
                    ProjectDocumentItem projectDocument = document as ProjectDocumentItem;
                    if (projectDocument == null || projectDocument.FileNode == null)
                    {
                        continue;
                    }

                    if (removedProjects.Contains(projectDocument.FileNode.ParentProject))
                    {
                        documentsToClose.Add(document);
                    }

                    if (removedFiles.Contains(projectDocument.FileNode))
                    {
                        documentsToClose.Add(document);
                    }
                }

                docService.CloseDocuments(documentsToClose, true);
            }

            foreach (IHierarchyNode node in rootSelection)
            {
                ProjectItem item = node.ProjectItem;
                IHierarchyNode parent = node.Parent;
                ProjectNode projectNode = node as ProjectNode;
                if (projectNode != null)
                {
                    projectNode.Unload();
                }

                if (parent != null && parent.RemoveChild(node))
                {
                    node.ModifiedScopeNode.IsModified = true;
                    if (item != null)
                    {
                        IProjectItemScope scope = item.Scope;
                        if (scope != null)
                        {
                            scope.RemoveProjectItem(node.ProjectItem);
                        }
                    }
                }
            }

            if (deleting)
            {
                foreach (IHierarchyNode node in rootSelection)
                {
                    if (node.ProjectItem == null)
                    {
                        continue;
                    }

                    string fullPath = node.ProjectItem.GetMetadata("FullPath");
                    try
                    {
                        if (File.Exists(fullPath))
                        {
                            File.Delete(fullPath);
                        }
                    }
                    catch (Exception ex)
                    {
                        msgService.ShowStandardErrorBox(
                            StringTable.DeleteFailedMsg.FormatInvariant(
                            node.Text, ex.Message),
                            null);
                    }
                }
            }
        }

        /// <summary>
        /// Makes sure the user wishes to continue with the operation and determines whether
        /// the selected items should be deleted or just removed.
        /// </summary>
        /// <param name="items">
        /// The collection of items that are to be deleted/removed.
        /// </param>
        /// <param name="msgService">
        /// The message service to use to display message boxes to the user.
        /// </param>
        /// <param name="deleting">
        /// Set to a value indicating whether the user has selected to delete the items or not.
        /// </param>
        /// <returns>
        /// True if the user has confirmed they want to continue with the operation; otherwise,
        /// false.
        /// </returns>
        private bool Confirm(
            List<IHierarchyNode> items, IMessageBoxService msgService, out bool deleting)
        {
            bool canDelete = true;
            foreach (IHierarchyNode node in items)
            {
                ProjectNode projectNode = node as ProjectNode;
                if (projectNode == null)
                {
                    FileNode fileNode = node as FileNode;
                    if (fileNode == null)
                    {
                        canDelete = false;
                    }
                }
                else
                {
                    canDelete = false;
                }
            }

            deleting = canDelete;
            if (canDelete)
            {
                IMessageBox messageBox = msgService.CreateMessageBox();
                messageBox.AddButton(StringTable.RemoveButtonContent, 1, true, false);
                messageBox.AddButton(StringTable.DeleteButtonContent, 2, false, false);
                messageBox.AddButton(StringTable.CancelButtonContent, 3, false, true);
                if (items.Count > 1)
                {
                    messageBox.Text = StringTable.DeleteRemoveMultipleOptionMsg;
                }
                else
                {
                    string itemName = items[0].Text;
                    messageBox.Text =
                        StringTable.DeleteRemoveSingleOptionMsg.FormatInvariant(itemName);
                }

                long result = messageBox.ShowMessageBox();
                if (result == 1)
                {
                    deleting = false;
                }
                else if (result == 2)
                {
                    deleting = true;
                }
                else if (result == 3)
                {
                    return false;
                }
            }
            else
            {
                string messageText = null;
                if (items.Count > 1)
                {
                    messageText = StringTable.RemoveMultipleMsg;
                }
                else if (items[0] is ProjectFolderNode)
                {
                    string itemName = items[0].Text;
                    messageText = StringTable.RemoveSingleBranchMsg.FormatInvariant(itemName);
                }
                else
                {
                    string itemName = items[0].Text;
                    messageText = StringTable.RemoveSingleMsg.FormatInvariant(itemName);
                }

                MessageBoxResult result = msgService.Show(
                    messageText,
                    null,
                    MessageBoxButton.OKCancel,
                    MessageBoxImage.Warning);

                if (result == MessageBoxResult.Cancel)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Retrieves the root selection from the specified selection collection by removing
        /// any item that has a parent/grandparent etc that's also selected.
        /// </summary>
        /// <param name="selection">
        /// The collection containing all of the selected nodes.
        /// </param>
        /// <returns>
        /// The root selection from the specified selection collection.
        /// </returns>
        private List<IHierarchyNode> RootSelection(IEnumerable<IHierarchyNode> selection)
        {
            List<IHierarchyNode> rootSelection = new List<IHierarchyNode>();
            if (selection == null)
            {
                return rootSelection;
            }

            foreach (IHierarchyNode selectedNode in selection)
            {
                IHierarchyNode parent = selectedNode;
                do
                {
                    parent = parent.Parent;
                    if (parent == null)
                    {
                        rootSelection.Add(selectedNode);
                        break;
                    }

                    if (selection.Contains(parent))
                    {
                        break;
                    }
                }
                while (parent != null);
            }

            return rootSelection;
        }
        #endregion Methods
    } // RSG.Project.Commands.DeleteItemAction {Class}
} // RSG.Project.Commands {Namespace}
