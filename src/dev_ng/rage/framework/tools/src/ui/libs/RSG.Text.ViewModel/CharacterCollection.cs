﻿//---------------------------------------------------------------------------------------------
// <copyright file="CharacterCollection.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.ViewModel
{
    using System;
    using RSG.Editor.Model;
    using RSG.Editor.View;
    using RSG.Text.Model;

    /// <summary>
    /// Provides a collection class that contains <see cref="DialogueCharacterViewModel"/>
    /// objects that are kept in sync with a collection of <see cref="DialogueCharacter"/>
    /// objects.
    /// </summary>
    public class CharacterCollection
        : CollectionConverter<DialogueCharacter, DialogueCharacterViewModel>,
        IReadOnlyViewModelCollection<DialogueCharacterViewModel>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CharacterCollection"/> class.
        /// </summary>
        /// <param name="source">
        /// The model collection containing the source <see cref="DialogueCharacter"/> objects
        /// for this collection.
        /// </param>
        public CharacterCollection(ModelCollection<DialogueCharacter> source)
            : base(source)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Retrieves the dialogue character whose identifier is equal to the specified id if
        /// found; otherwise, null.
        /// </summary>
        /// <param name="id">
        /// The identifier of the character to retrieve.
        /// </param>
        /// <returns>
        /// The dialogue character whose identifier is equal to the specified id.
        /// </returns>
        public DialogueCharacterViewModel GetCharacter(Guid id)
        {
            foreach (DialogueCharacterViewModel item in this)
            {
                if (item.Id == id)
                {
                    return item;
                }
            }

            return null;
        }

        /// <summary>
        /// Converts a single <see cref="DialogueCharacter"/> object to a new
        /// <see cref="DialogueCharacterViewModel"/> object.
        /// </summary>
        /// <param name="item">
        /// The item to convert.
        /// </param>
        /// <returns>
        /// A new <see cref="DialogueCharacterViewModel"/> object from the given
        /// <see cref="DialogueCharacter"/> object.
        /// </returns>
        protected override DialogueCharacterViewModel ConvertItem(DialogueCharacter item)
        {
            DialogueCharacterViewModel newViewModel = new DialogueCharacterViewModel(item);
            return newViewModel;
        }
        #endregion Methods
    } // RSG.Text.ViewModel.CharacterCollection {Class}
} // RSG.Text.ViewModel {Namespace}
