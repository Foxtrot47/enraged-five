﻿//---------------------------------------------------------------------------------------------
// <copyright file="AddCommandWindowDataContext.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Customisation
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Represents the data context that is used for a <see cref="AddCommandWindow"/> instance.
    /// </summary>
    internal class AddCommandWindowDataContext : NotifyPropertyChangedBase
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="SelectedCategory"/> property.
        /// </summary>
        private string _selectedCategory;

        /// <summary>
        /// The private field used for the <see cref="SelectedDefinition"/> property.
        /// </summary>
        private CommandDefinition _selectedDefinition;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="AddCommandWindowDataContext"/> class.
        /// </summary>
        public AddCommandWindowDataContext()
        {
            this.SelectedCategory = this.Categories.FirstOrDefault();
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the collection of categories to show in the user interface.
        /// </summary>
        public IEnumerable<string> Categories
        {
            get { return RockstarCommandManager.AllCategories; }
        }

        /// <summary>
        /// Gets the collection of definitions that should be shown in the user interface based
        /// on the selected category.
        /// </summary>
        public IEnumerable<CommandDefinition> Definitions
        {
            get { return RockstarCommandManager.GetDefinitions(this.SelectedCategory); }
        }

        /// <summary>
        /// Gets or sets the category that is currently selected.
        /// </summary>
        public string SelectedCategory
        {
            get
            {
                return this._selectedCategory;
            }

            set
            {
                if (this.SetProperty(ref this._selectedCategory, value))
                {
                    this.NotifyPropertyChanged("Definitions");
                    this.SelectedDefinition = this.Definitions.FirstOrDefault();
                }
            }
        }

        /// <summary>
        /// Gets or sets the currently selected command definition.
        /// </summary>
        public CommandDefinition SelectedDefinition
        {
            get { return this._selectedDefinition; }
            set { this.SetProperty(ref this._selectedDefinition, value); }
        }
        #endregion Properties
    } // RSG.Editor.Controls.Customisation.AddCommandWindowDataContext {Class}
} // RSG.Editor.Controls.Customisation {Namespace}
