﻿//---------------------------------------------------------------------------------------------
// <copyright file="KeyGestureComparer.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System.Collections.Generic;
    using System.Windows.Input;

    /// <summary>
    /// Defines methods to support the comparison of two instances of the
    /// System.Windows.Input.KeyGesture class.
    /// </summary>
    internal class KeyGestureComparer : IEqualityComparer<KeyGesture>
    {
        #region Methods
        /// <summary>
        /// Determines whether the specified objects are equal.
        /// </summary>
        /// <param name="x">
        /// The first object of type T to compare.
        /// </param>
        /// <param name="y">
        /// The second object of type T to compare.
        /// </param>
        /// <returns>
        /// True if the specified objects are equal; otherwise, false.
        /// </returns>
        public bool Equals(KeyGesture x, KeyGesture y)
        {
            if (x != null && y != null)
            {
                if (x.Key != y.Key)
                {
                    return false;
                }

                if (x.Modifiers != y.Modifiers)
                {
                    return false;
                }

                return true;
            }

            return x == null && y == null;
        }

        /// <summary>
        /// Returns a hash code for the specified object.
        /// </summary>
        /// <param name="obj">
        /// The System.Object for which a hash code is to be returned.
        /// </param>
        /// <returns>
        /// A hash code for the specified object.
        /// </returns>
        public int GetHashCode(KeyGesture obj)
        {
            if (obj == null)
            {
                throw new SmartArgumentNullException(() => obj);
            }

            return obj.GetHashCode();
        }
        #endregion Methods
    } // RSG.Editor.KeyGestureComparer {Class}
} // RSG.Editor {Namespace}
