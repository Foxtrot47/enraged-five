﻿//---------------------------------------------------------------------------------------------
// <copyright file="ConfirmAddEventArgs.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.View
{
    using System.ComponentModel;
    using RSG.Project.ViewModel.Definitions;

    /// <summary>
    /// Provides data for the confirm add event.
    /// </summary>
    public class ConfirmAddEventArgs : CancelEventArgs
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Definition"/> property.
        /// </summary>
        private IDefinition _definition;

        /// <summary>
        /// The private field used for the <see cref="Location"/> property.
        /// </summary>
        private string _location;

        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private string _name;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ConfirmAddEventArgs"/> class.
        /// </summary>
        /// <param name="name">
        /// The name that has been entered by the user.
        /// </param>
        /// <param name="location">
        /// The location that has been entered by the user.
        /// </param>
        /// <param name="definition">
        /// The definition that has been selected by the user.
        /// </param>
        public ConfirmAddEventArgs(string name, string location, IDefinition definition)
        {
            this._name = name;
            this._location = location;
            this._definition = definition;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the definition that has been selected by the user.
        /// </summary>
        public IDefinition Definition
        {
            get { return this._definition; }
        }

        /// <summary>
        /// Gets the location that has been entered by the user.
        /// </summary>
        public string Location
        {
            get { return this._location; }
        }

        /// <summary>
        /// Gets the name that has been entered by the user.
        /// </summary>
        public string Name
        {
            get { return this._name; }
        }
        #endregion Properties
    } // RSG.Project.View.ConfirmAddEventArgs {Class}
} // RSG.Project.View {Namespace}
