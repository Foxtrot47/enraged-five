﻿//---------------------------------------------------------------------------------------------
// <copyright file="DefaultStateAdorner.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------
namespace RSG.Metadata.View
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Documents;
    using System.Windows.Media;

    /// <summary>
    /// A adorner used to decorate a element with a validation error visual.
    /// </summary>
    public sealed class DefaultStateAdorner : Adorner
    {
        #region Fields
        /// <summary>
        /// The reference to the placeholder object for this adorned that is used during
        /// measuring.
        /// </summary>
        private DefaultStateAdornedPlaceholder _adornedPlaceholder;

        /// <summary>
        /// The one and only child for this adorner that has a template set based on the
        /// elements attached manual validation template property.
        /// </summary>
        private Control _child;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DefaultStateAdorner"/> class.
        /// </summary>
        /// <param name="element">
        /// The element that adorner will be for.
        /// </param>
        /// <param name="template">
        /// The template to use within the control for this adorner.
        /// </param>
        public DefaultStateAdorner(UIElement element, ControlTemplate template)
            : base(element)
        {
            this._child = new Control();
            this._child.IsTabStop = false;
            this._child.Template = template;
            this.AddVisualChild(this._child);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the reference to the placeholder object for this adorned that is used
        /// during measuring.
        /// </summary>
        public DefaultStateAdornedPlaceholder AdornedPlaceholder
        {
            get { return this._adornedPlaceholder; }
            set { this._adornedPlaceholder = value; }
        }

        /// <summary>
        /// Gets the number of visual children elements within this element.
        /// </summary>
        protected override int VisualChildrenCount
        {
            get { return this._child == null ? 0 : 1; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Clears the visual child for this element.
        /// </summary>
        public void ClearChild()
        {
            this.RemoveVisualChild(this._child);
            this._child = null;
        }

        /// <summary>
        /// Get the transform for this adorner based on the placeholder.
        /// </summary>
        /// <param name="transform">
        /// The currently transform.
        /// </param>
        /// <returns>
        /// The transform for this adorner.
        /// </returns>
        public override GeneralTransform GetDesiredTransform(GeneralTransform transform)
        {
            if (this.AdornedPlaceholder == null)
            {
                return transform;
            }

            GeneralTransformGroup group = new GeneralTransformGroup();
            group.Children.Add(transform);
            GeneralTransform child = this.TransformToDescendant(this.AdornedPlaceholder);
            if (child != null)
            {
                group.Children.Add(child);
            }

            return group;
        }

        /// <summary>
        /// Positions the child elements and determines the size needed for this element.
        /// </summary>
        /// <param name="size">
        /// The size of the area this element should use to arrange itself and its children.
        /// </param>
        /// <returns>
        /// The size that this element is actually going to use for rendering.
        /// </returns>
        protected override Size ArrangeOverride(Size size)
        {
            Size size2 = base.ArrangeOverride(size);
            if (this._child != null)
            {
                this._child.Arrange(new Rect(default(Point), size2));
            }

            return size2;
        }

        /// <summary>
        /// Gets the child at the specified index for this element. Only valid for index 0.
        /// </summary>
        /// <param name="index">
        /// The zero-based index of the requested child element.
        /// </param>
        /// <returns>
        /// The child at the specified index.
        /// </returns>
        protected override Visual GetVisualChild(int index)
        {
            if (this._child == null || index != 0)
            {
                return null;
            }

            return this._child;
        }

        /// <summary>
        /// Measures the adorner and the visual children to get the desired size.
        /// </summary>
        /// <param name="constraint">
        /// The size to constrain the adorner to.
        /// </param>
        /// <returns>
        /// Desired Size of the control, given available size passed as parameter.
        /// </returns>
        protected override Size MeasureOverride(Size constraint)
        {
            base.MeasureOverride(constraint);
            if (this.AdornedElement != null && this.AdornedElement.IsMeasureValid)
            {
                if (this.AdornedPlaceholder != null)
                {
                    this.AdornedPlaceholder.InvalidateMeasure();
                }
            }

            this._child.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
            return this._child.DesiredSize;
        }
        #endregion Methods
    } // RSG.Metadata.View.DefaultState {Class}
} // RSG.Metadata.View {Namespace}
