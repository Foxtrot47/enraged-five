﻿//---------------------------------------------------------------------------------------------
// <copyright file="RelayCommand{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Windows.Input;

    /// <summary>
    /// Provides a way to bind command attributes to methods within the binding source.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the command parameter that should be passed into the delegated can execute
    /// and execute methods.
    /// </typeparam>
    public class RelayCommand<T> : ICommand
    {
        #region Fields
        /// <summary>
        /// The method to call when this command executes.
        /// </summary>
        private Action<T> _action;

        /// <summary>
        /// The method to call when determining if this command can be executed.
        /// </summary>
        private Predicate<T> _prediction;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="RelayCommand{T}"/> class, using the
        /// specified prediction and action methods as command delegates.
        /// </summary>
        /// <param name="action">
        /// The method to call when this command executes. This only gets called if the
        /// specified prediction method returns true.
        /// </param>
        /// <param name="prediction">
        /// The method to call when determining if this command can be executed or not. Can
        /// be null.
        /// </param>
        public RelayCommand(Action<T> action, Predicate<T> prediction)
        {
            if (action == null)
            {
                throw new SmartArgumentNullException(() => action);
            }

            this._action = action;
            this._prediction = prediction;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RelayCommand{T}"/> class, using the
        /// specified action method as a command delegate.
        /// </summary>
        /// <param name="action">
        /// The method to call when this command executes.
        /// </param>
        public RelayCommand(Action<T> action)
            : this(action, null)
        {
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs when changes occur that affect whether or not the command can be executed by
        /// the user.
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
        #endregion Events

        #region Methods
        /// <summary>
        /// Gets called when the manager is deciding if this command can be executed. This
        /// returns true be default if no prediction method was specified at creation time.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter that has been setup in the binding to this command.
        /// </param>
        /// <returns>
        /// True if the command can be executed; otherwise, false.
        /// </returns>
        public bool CanExecute(object parameter)
        {
            if (this._prediction != null)
            {
                return this._prediction((T)parameter);
            }

            return true;
        }

        /// <summary>
        /// Gets called when the command is executed. This just relays the execution to the
        /// delegate function.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter that has been setup in the binding to this command.
        /// </param>
        public void Execute(object parameter)
        {
            if (this._action == null)
            {
                return;
            }

            this._action((T)parameter);
        }
        #endregion Methods
    } // RSG.Editor.RelayCommand{T} {Class}
} // RSG.Editor {Namespace}
