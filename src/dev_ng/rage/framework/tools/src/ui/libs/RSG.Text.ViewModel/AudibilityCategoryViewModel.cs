﻿//---------------------------------------------------------------------------------------------
// <copyright file="AudibilityCategoryViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.ViewModel
{
    using System.Collections;
    using System.Collections.Generic;
    using RSG.Text.Model;
    using RSG.Text.ViewModel.Resources;

    /// <summary>
    /// Represents the dialogue configuration editor category for the audibility items in the
    /// configuration. This class cannot be inherited.
    /// </summary>
    public sealed class AudibilityCategoryViewModel : CategoryViewModel
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Items"/> property.
        /// </summary>
        private AudibilityCollection _audibilities;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="AudibilityCategoryViewModel"/> class
        /// for the specified configurations.
        /// </summary>
        /// <param name="configurations">
        /// The configurations whose audibility items will be shown as part of this category.
        /// </param>
        public AudibilityCategoryViewModel(DialogueConfigurations configurations)
            : base(StringTable.AudibilityCategoryHeader, configurations)
        {
            this._audibilities = new AudibilityCollection(configurations.Audibilities);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the list of audibility values this category contains.
        /// </summary>
        public AudibilityCollection Items
        {
            get { return this._audibilities; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a composite view model object containing the
        /// <see cref="DialogueAudibilityViewModel"/> objects inside the specified list.
        /// </summary>
        /// <param name="list">
        /// The collection that contains the individual items that should be grouped inside the
        /// resulting composite object.
        /// </param>
        /// <returns>
        /// A composite view model object that contains the
        /// <see cref="DialogueAudibilityViewModel"/> objects inside the specified list.
        /// </returns>
        public override CompositeViewModel CreateComposite(IList list)
        {
            List<DialogueAudibilityViewModel> items = new List<DialogueAudibilityViewModel>();
            foreach (object item in list)
            {
                DialogueAudibilityViewModel audibility = item as DialogueAudibilityViewModel;
                if (audibility == null)
                {
                    continue;
                }

                items.Add(audibility);
            }

            return new AudibilityCompositeViewModel(this.Model, items);
        }
        #endregion Methods
    } // RSG.Text.ViewModel.AudibilityCategoryViewModel {Class}
} // RSG.Text.ViewModel {Namespace}
