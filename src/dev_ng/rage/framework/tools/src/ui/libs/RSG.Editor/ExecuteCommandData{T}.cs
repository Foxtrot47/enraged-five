﻿//---------------------------------------------------------------------------------------------
// <copyright file="ExecuteCommandData{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System.Windows.Input;

    /// <summary>
    /// Provides data for a method that implements a command based on a parameter of the
    /// specified type.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the parameter used during the execution of the command.
    /// </typeparam>
    public class ExecuteCommandData<T> : ExecuteCommandData
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="OriginalParameterWasNull"/> property.
        /// </summary>
        private bool _originalParameterWasNull;

        /// <summary>
        /// The private field used for the <see cref="Parameter"/> property.
        /// </summary>
        private T _parameter;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ExecuteCommandData{T}"/> class based
        /// on the specified command being invoked with the specified parameter.
        /// </summary>
        /// <param name="command">
        /// The command that was invoked.
        /// </param>
        /// <param name="parameter">
        /// The data parameter of the command.
        /// </param>
        internal ExecuteCommandData(ICommand command, T parameter)
            : base(command)
        {
            this._parameter = parameter;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether the parameter passed in from the source event data
        /// was set to null or not.
        /// </summary>
        public bool OriginalParameterWasNull
        {
            get { return this._originalParameterWasNull; }
            internal set { this._originalParameterWasNull = value; }
        }

        /// <summary>
        /// Gets the data parameter that was sent with the invoked command.
        /// </summary>
        public T Parameter
        {
            get { return this._parameter; }
        }
        #endregion Properties
    } // RSG.Editor.ExecuteCommandData<T> {Class}
} // RSG.Editor {Namespace}
