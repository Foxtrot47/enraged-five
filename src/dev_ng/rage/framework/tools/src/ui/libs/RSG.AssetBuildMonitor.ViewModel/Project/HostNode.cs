﻿//---------------------------------------------------------------------------------------------
// <copyright file="HostNode.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.AssetBuildMonitor.ViewModel.Project
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Windows;
    using RSG.AssetBuildMonitor.ViewModel;
    using RSG.Editor.Controls.Dock.ViewModel;
    using RSG.Editor.View;
    using RSG.Project.Model;
    using RSG.Project.ViewModel;

    /// <summary>
    /// A Host node object that represents a Host document inside the Host Explorer. 
    /// </summary>
    public class HostNode : 
        FileNode,
        ICreatesDocumentContent,
       // ICreatesProjectDocument,
        IInvokable
    {
        #region Properties
        /// <summary>
        /// Gets a value indicating whether this hierarchy nodes data can be saved.
        /// </summary>
        public override bool CanBeSaved
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether this hierarchy node can be removed from the parent
        /// items scope it belongs to.
        /// </summary>
        public override bool CanBeRemoved
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether this item is expanded when the user double clicks
        /// on it.
        /// </summary>
        public override bool ExpandOnDoubleClick
        {
            get { return false; }
        }

        /// <summary>
        /// Gets the priority value for this object.
        /// </summary>
        public override int Priority
        {
            get { return int.MaxValue; }
        }

        /// <summary>
        /// Gets the Bugstar users enumerable (for displaying user-information).
        /// </summary>
        public IEnumerable<RSG.Interop.Bugstar.Organisation.User> BugstarUsers
        {
            get { return _bugstarUsers; }
            private set { this.SetProperty(ref _bugstarUsers, value); }
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Enumerable of Bugstar users (for displaying user-information).
        /// </summary>
        private IEnumerable<RSG.Interop.Bugstar.Organisation.User> _bugstarUsers;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="HostNode"/> class.
        /// </summary>
        /// <param name="parent">
        /// The parent hierarchy node that this node will be placed under.
        /// </param>
        /// <param name="model">
        /// The project item for this node that will act as its model.
        /// </param>
        public HostNode(IHierarchyNode parent, ProjectItem model,
            IEnumerable<RSG.Interop.Bugstar.Organisation.User> bugstarUsers)
            : base(parent, model)
        {
            this.IsExpandable = false;
            this.Icon = Icons.HostNodeIcon;
            int index = model.Include.LastIndexOf('\\') + 1;
            this.Text = model.Include.Substring(index);
            this.BugstarUsers = bugstarUsers;
        }
        #endregion // Constructor(s)

        #region Methods
        /// <summary>
        /// Called whenever a property inside the model changes.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The data for this event including the name of the property that changed.
        /// </param>
        protected override void OnModelPropertyChanged(Object s, PropertyChangedEventArgs e)
        {
            if (String.Equals(e.PropertyName, "Include"))
            {
                int index = this.Model.Include.LastIndexOf('\\') + 1;
                this.Text = this.Model.Include.Substring(index);
            }
        }

        /// <summary>
        /// Creates the document content for this node optionally using the specified undo
        /// engine.
        /// </summary>
        /// <param name="undoEngine">
        /// The undo engine that can be optionally used in the construction of the document
        /// content.
        /// </param>
        /// <returns>
        /// The document content for this node.
        /// </returns>
        public Object CreateDocumentContent(RSG.Editor.Model.UndoEngine undoEngine)
        {
            // DHM FIX ME: err... more specialisation in HostNode?
            String fullPath = this.ProjectItem.GetMetadata("FullPath");
            String hostname = System.IO.Path.GetFileNameWithoutExtension(fullPath);
            return (new AssetBuildHistoryViewModel(RSG.Base.Logging.LogFactory.ApplicationLog, 
                hostname, this._bugstarUsers));
        }
        #endregion // Methods
    }

} // RSG.AssetBuildMonitor.ViewModel.Project namespace
