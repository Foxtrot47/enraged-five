﻿//---------------------------------------------------------------------------------------------
// <copyright file="PerforceLoginAttemptData.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Perforce
{
    using RSG.Interop.Perforce;

    /// <summary>
    /// Represents the object that is sent with the delegate determining whether the user has
    /// successfully logged in using the perforce login window.
    /// </summary>
    public class PerforceLoginAttemptData : LoginAttemptData
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="PerforceObject"/> property.
        /// </summary>
        private P4 _perforceObject;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="PerforceLoginAttemptData"/> class.
        /// </summary>
        /// <param name="username">
        /// The submitted username.
        /// </param>
        /// <param name="password">
        /// The submitted password.
        /// </param>
        /// <param name="retry">
        /// A value indicating whether the user can attempt again if this attempt fails.
        /// </param>
        /// <param name="attempts">
        /// The number of attempts the user has had at logging in.
        /// </param>
        /// <param name="perforceObject">
        /// The perforce object that the login window is trying to establish a connection to.
        /// </param>
        public PerforceLoginAttemptData(
            string username, string password, bool retry, int attempts, P4 perforceObject)
            : base(username, password, retry, attempts)
        {
            if (perforceObject == null)
            {
                throw new SmartArgumentNullException(() => perforceObject);
            }

            this._perforceObject = perforceObject;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the perforce object that the login window is trying to establish a connection
        /// to.
        /// </summary>
        public P4 PerforceObject
        {
            get { return this._perforceObject; }
        }
        #endregion Properties
    } // RSG.Editor.Controls.Perforce.PerforceLoginAttemptData {Class}
} // RSG.Editor.Controls.Perforce {Namespace}
