﻿//---------------------------------------------------------------------------------------------
// <copyright file="VoiceCategoryViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.ViewModel
{
    using System.Collections;
    using System.Collections.Generic;
    using RSG.Text.Model;
    using RSG.Text.ViewModel.Resources;

    /// <summary>
    /// Represents the dialogue configuration editor category for the voice items in the
    /// configuration. This class cannot be inherited.
    /// </summary>
    public sealed class VoiceCategoryViewModel : CategoryViewModel
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Items"/> property.
        /// </summary>
        private VoiceCollection _voices;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="VoiceCategoryViewModel"/> class
        /// for the specified configurations.
        /// </summary>
        /// <param name="configurations">
        /// The configurations whose voice items will be shown as part of this category.
        /// </param>
        public VoiceCategoryViewModel(DialogueConfigurations configurations)
            : base(StringTable.VoiceCategoryHeader, configurations)
        {
            this._voices = new VoiceCollection(configurations.Voices);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the list of audibility values this category contains.
        /// </summary>
        public VoiceCollection Items
        {
            get { return this._voices; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a composite view model object containing the
        /// <see cref="DialogueVoiceViewModel"/> objects inside the specified list.
        /// </summary>
        /// <param name="list">
        /// The collection that contains the individual items that should be grouped inside the
        /// resulting composite object.
        /// </param>
        /// <returns>
        /// A composite view model object that contains the
        /// <see cref="DialogueVoiceViewModel"/> objects inside the specified list.
        /// </returns>
        public override CompositeViewModel CreateComposite(IList list)
        {
            List<DialogueVoiceViewModel> items = new List<DialogueVoiceViewModel>();
            foreach (object item in list)
            {
                DialogueVoiceViewModel voice = item as DialogueVoiceViewModel;
                if (voice == null)
                {
                    continue;
                }

                items.Add(voice);
            }

            return new VoiceCompositeViewModel(this.Model, items);
        }
        #endregion Methods
    } // RSG.Text.ViewModel.VoiceCategoryViewModel {Class}
} // RSG.Text.ViewModel {Namespace}
