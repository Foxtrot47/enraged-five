﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Text.Commands
{
    /// <summary>
    /// Enum used for filtering conversations
    /// </summary>
    public enum ConversationFilter
    {
        /// <summary>
        /// Show placeholder
        /// </summary>
        ShowPlaceholder,
        /// <summary>
        /// Show not placeholder
        /// </summary>
        ShowNotPlaceholder,
    }
}
