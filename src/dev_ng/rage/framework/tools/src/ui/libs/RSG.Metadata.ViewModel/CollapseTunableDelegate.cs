﻿//---------------------------------------------------------------------------------------------
// <copyright file="CollapseTunableDelegate.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel
{
    using RSG.Metadata.ViewModel.Tunables;

    /// <summary>
    /// Represents the method that can be used to collapse a single tunable view model when
    /// needed in the view while inside a command action.
    /// </summary>
    /// <param name="tunable">
    /// The tunable that needs to be collapsed.
    /// </param>
    /// <param name="recursively">
    /// A value indicating whether the collapse is recursive through the whole tree.
    /// </param>
    public delegate void CollapseTunableDelegate(ITunableViewModel tunable, bool recursively);
} // RSG.Metadata.ViewModel {Namespace}
