﻿//---------------------------------------------------------------------------------------------
// <copyright file="OpenContainingFolderAction.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Commands
{
    using System;
    using System.Diagnostics;
    using RSG.Base.Extensions;
    using RSG.Editor;
    using RSG.Project.ViewModel;

    /// <summary>
    /// Contains the logic for the <see cref="ProjectCommands.OpenContainingFolder"/>
    /// routed command. This class cannot be inherited.
    /// </summary>
    public sealed class OpenContainingFolderAction : ButtonAction<ProjectCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="OpenCollectionAction"/> class.
        /// </summary>
        /// <param name="serviceProvider">
        /// The service provider that is used by this action to perform application specific
        /// operations, for example selecting a file to open.
        /// </param>
        public OpenContainingFolderAction(ICommandServiceProvider serviceProvider)
            : base(serviceProvider)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="OpenCollectionAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public OpenContainingFolderAction(ProjectCommandResolver resolver)
            : base(new ParameterResolverDelegate<ProjectCommandArgs>(resolver))
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ProjectCommandArgs args)
        {
            // If nothing selected then we cannot open containing folder.
            if (args.SelectedNodes.None())
                return (false);

            // In order to determine if we are enabled for multiple-selection we need to
            // ensure all selected nodes have the same folder.
            bool allHaveSameContainingFolder = true;
            String containingFolder = String.Empty;
            foreach (IHierarchyNode node in args.SelectedNodes)
            {
                if (node is FileNode)
                {
                    String fullPath = ((FileNode)node).Model.GetMetadata("FullPath");
                    String directory = System.IO.Path.GetDirectoryName(fullPath);
                    if (String.IsNullOrEmpty(containingFolder))
                        containingFolder = directory;
                    else if (containingFolder.Equals(directory, StringComparison.OrdinalIgnoreCase))
                        allHaveSameContainingFolder = false;
                } 
                else if (node is FolderNode)
                {
                    String directory = ((FolderNode)node).Model.GetMetadata("FullPath");
                    if (String.IsNullOrEmpty(containingFolder))
                        containingFolder = directory;
                    else if (containingFolder.Equals(directory, StringComparison.OrdinalIgnoreCase))
                        allHaveSameContainingFolder = false;
                }

                if (!allHaveSameContainingFolder)
                    break;
            }
            
            return (allHaveSameContainingFolder);
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(ProjectCommandArgs args)
        {
            // In order to determine if we are enabled for multiple-selection we need to
            // ensure all selected nodes have the same folder.
            bool allHaveSameContainingFolder = true;
            String containingFolder = String.Empty;
            foreach (IHierarchyNode node in args.SelectedNodes)
            {
                if (node is FileNode)
                {
                    String fullPath = ((FileNode)node).Model.GetMetadata("FullPath");
                    String directory = System.IO.Path.GetDirectoryName(fullPath);
                    if (String.IsNullOrEmpty(containingFolder))
                        containingFolder = directory;
                    else if (containingFolder.Equals(directory, StringComparison.OrdinalIgnoreCase))
                        allHaveSameContainingFolder = false;
                }
                else if (node is FolderNode)
                {
                    String directory = ((FolderNode)node).Model.GetMetadata("FullPath");
                    if (String.IsNullOrEmpty(containingFolder))
                        containingFolder = directory;
                    else if (containingFolder.Equals(directory, StringComparison.OrdinalIgnoreCase))
                        allHaveSameContainingFolder = false;
                }

                if (!allHaveSameContainingFolder)
                    break;
            }

            Debug.Assert(allHaveSameContainingFolder);
            String arguments = String.Format("/select,\"{0}\"", containingFolder);
            Process.Start("explorer.exe", arguments);
        }
        #endregion Methods
    } // RSG.Project.Commands.OpenCollectionAction {Class}
} // RSG.Project.Commands {Namespace}
