﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProjectAddViewService.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.View
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Windows;
    using RSG.Editor;
    using RSG.Editor.Controls;
    using RSG.Editor.Controls.Dock;
    using RSG.Project.View.Resources;
    using RSG.Project.ViewModel;
    using RSG.Project.ViewModel.Definitions;

    /// <summary>
    /// Provides functionality to show the views specific to the project add commands.
    /// </summary>
    public class ProjectAddViewService : IProjectAddViewService
    {
        #region Methods
        /// <summary>
        /// Shows a modal window which displays the progress of the update for a set of files.
        /// </summary>
        /// <param name="project">
        /// The project that the updated files will be added to.
        /// </param>
        /// <param name="filesToUpdate">
        /// A list of full paths to the files that need updating.
        /// </param>
        /// <returns>
        /// The full path to all of the updated files.
        /// </returns>
        public virtual IReadOnlyList<string> ShowFileUpdatingProgressWindow(
            ProjectNode project, List<string> filesToUpdate)
        {
            UpdateProgressWindow window = new UpdateProgressWindow(project, filesToUpdate);
            window.Owner = Application.Current.MainWindow;
            window.ShowDialog();
            return window.FilesUpdated;
        }

        /// <summary>
        /// Shows the new item selector dialog window.
        /// </summary>
        /// <param name="project">
        /// The project whose item definitions should be available to select.
        /// </param>
        /// <param name="initialLocation">
        /// The initial location for the new item.
        /// </param>
        /// <param name="initialName">
        /// The initial name for the new item.
        /// </param>
        /// <param name="selected">
        /// The item that the user selected, or null if the user has chosen to cancel.
        /// </param>
        /// <param name="fullPath">
        /// The full path where the user has chosen the new item to be located.
        /// </param>
        public virtual void ShowNewItemSelector(
            ProjectNode project,
            string initialLocation,
            string initialName,
            out ProjectItemDefinition selected,
            out string fullPath)
        {
            ItemAddSelectorWindow window = new ItemAddSelectorWindow();
            window.Owner = Application.Current.MainWindow;
            window.SetDefinition(project);
            window.LocationValueTextBox.Text = initialLocation;
            if (initialName != null)
            {
                window.NameValueTextBox.Text = initialName;
            }

            if (window.ShowDialog() != true)
            {
                selected = null;
                fullPath = null;
                return;
            }

            selected = window.TemplateListBox.SelectedItem as ProjectItemDefinition;
            if (selected == null)
            {
                fullPath = null;
                return;
            }

            string name = window.NameValueTextBox.Text;
            if (!name.EndsWith(selected.Extension))
            {
                name += selected.Extension;
            }

            fullPath = Path.Combine(window.LocationValueTextBox.Text, name);
        }

        /// <summary>
        /// Shows the new project selector dialog window.
        /// </summary>
        /// <param name="collection">
        /// The project collection whose project definitions should be available to select.
        /// </param>
        /// <param name="initialLocation">
        /// The initial location for the new project.
        /// </param>
        /// <param name="selected">
        /// The project that the user selected, or null if the user has chosen to cancel.
        /// </param>
        /// <param name="fullPath">
        /// The full path where the user has chosen the new project to be located.
        /// </param>
        public virtual void ShowNewProjectSelector(
            ProjectCollectionNode collection,
            string initialLocation,
            out ProjectDefinition selected,
            out string fullPath)
        {
            ProjectAddSelectorWindow window = new ProjectAddSelectorWindow();
            window.Owner = Application.Current.MainWindow;
            window.SetDefinition(collection);
            window.LocationValueTextBox.Text = initialLocation;
            window.MakeCollectionOptionsInvisible();
            if (window.ShowDialog() != true)
            {
                selected = null;
                fullPath = null;
                return;
            }

            selected = window.TemplateListBox.SelectedItem as ProjectDefinition;
            if (selected == null)
            {
                fullPath = null;
                return;
            }

            string name = window.NameValueTextBox.Text;
            if (!name.EndsWith(selected.Extension))
            {
                name += selected.Extension;
            }

            fullPath = Path.Combine(window.LocationValueTextBox.Text, name);
        }

        /// <summary>
        /// Shows the new project selector dialog window.
        /// </summary>
        /// <param name="collection">
        /// The project collection whose project definitions should be available to select.
        /// </param>
        /// <param name="addToCurrentCollection">
        /// A value indicating whether the new project is being added to the current collection
        /// or a new collection should be created.
        /// </param>
        /// <param name="selected">
        /// The project that the user selected, or null if the user has chosen to cancel.
        /// </param>
        /// <param name="fullPath">
        /// The full path where the user has chosen the new project to be located.
        /// </param>
        public virtual void ShowNewProjectSelector(
            ProjectCollectionNode collection,
            out bool addToCurrentCollection,
            out ProjectDefinition selected,
            out string fullPath)
        {
            ProjectAddSelectorWindow window = new ProjectAddSelectorWindow();
            window.ConfirmAdd += this.OnConfirmAddForNewProject;
            window.Owner = Application.Current.MainWindow;
            window.SetDefinition(collection);
            window.Title = "New Project";
            if (collection.Loaded && !String.IsNullOrWhiteSpace(collection.Filename))
            {
                window.LocationValueTextBox.Text = Path.GetDirectoryName(collection.Filename);
            }

            if (collection.Loaded)
            {
                window.CollectionOptionComboBox.IsEnabled = true;
                window.CollectionOptionComboBox.SelectedIndex = 1;
            }
            else
            {
                window.CollectionOptionComboBox.IsEnabled = false;
                window.CollectionOptionComboBox.SelectedIndex = 0;
            }

            if (window.ShowDialog() != true)
            {
                selected = null;
                fullPath = null;
                addToCurrentCollection = true;
                return;
            }

            selected = window.TemplateListBox.SelectedItem as ProjectDefinition;
            if (selected == null)
            {
                fullPath = null;
                addToCurrentCollection = true;
                return;
            }

            string name = window.NameValueTextBox.Text;
            fullPath = this.CreateFullPath(name, window.LocationValueTextBox.Text, selected);
            if (collection.Loaded)
            {
                addToCurrentCollection = window.CollectionOptionComboBox.SelectedIndex == 1;
            }
            else
            {
                addToCurrentCollection = false;
            }
        }

        /// <summary>
        /// Shows the add dynamic folder dialog window.
        /// </summary>
        /// <param name="dlgService">
        /// The common dialog service.
        /// </param>
        /// <param name="path">
        /// The full path to the selected folder.
        /// </param>
        /// <param name="filters">
        /// The list of filters the user has selected.
        /// </param>
        /// <param name="recursive">
        /// A value indicating whether this is going to be recursive.
        /// </param>
        public bool ShowAddNewDynamicFolder(
            ICommonDialogService dlgService,
            out string path,
            out string filters,
            out bool recursive)
        {
            AddDynamicFolderWindow window = new AddDynamicFolderWindow();
            window.Owner = Application.Current.MainWindow;
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            if (window.ShowDialog() != true)
            {
                filters = String.Empty;
                recursive = false;
                path = null;
                return false;
            }

            path = window.LocationValueTextBox.Text;
            recursive = window.RecursiveValueCheckBox.IsChecked == true;
            filters = window.FiltersValueTextBox.Text;
            return true;
        }

        /// <summary>
        /// Creates the full path from the individual entered values.
        /// </summary>
        /// <param name="name">
        /// The name that has been entered by the user.
        /// </param>
        /// <param name="location">
        /// The location that has been entered by the user.
        /// </param>
        /// <param name="definition">
        /// The definition that has been selected by the user.
        /// </param>
        /// <returns>
        /// The full path from the individual entered values or null if invalid.
        /// </returns>
        private string CreateFullPath(string name, string location, IDefinition definition)
        {
            if (definition == null)
            {
                return null;
            }

            if (!name.EndsWith(definition.Extension))
            {
                name += definition.Extension;
            }

            string fullPath = Path.Combine(location, name);
            return Path.GetFullPath(fullPath);
        }

        /// <summary>
        /// Called when the add project dialog needs to confirm it can proceed with the entered
        /// data.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The RSG.Project.View.ConfirmAddEventArgs containing the event data.
        /// </param>
        private void OnConfirmAddForNewProject(object sender, ConfirmAddEventArgs e)
        {
            string fullPath = this.CreateFullPath(e.Name, e.Location, e.Definition);
            FileInfo info = new FileInfo(fullPath);
            if (info.Exists)
            {
                RsMessageBox.Show(
                    StringTable.ProjectExistsErrorMsg,
                    null,
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                e.Cancel = true;
            }
        }
        #endregion Methods
    } // RSG.Project.View.ProjectAddViewService {Class}
} // RSG.Project.View {Namespace}
