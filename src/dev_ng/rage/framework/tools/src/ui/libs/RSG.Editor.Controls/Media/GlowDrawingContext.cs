﻿//---------------------------------------------------------------------------------------------
// <copyright file="GlowDrawingContext.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Media
{
    using System;
    using RSG.Interop.Microsoft.Windows;

    /// <summary>
    /// Represents the drawing context to use for a glow bitmap on a glow window.
    /// </summary>
    internal sealed class GlowDrawingContext : DisposableObject
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="BackgroundDC"/> property.
        /// </summary>
        private IntPtr _backgroundDC;

        /// <summary>
        /// The private field used for the <see cref="Blend"/> property.
        /// </summary>
        private BLENDFUNCTION _blend;

        /// <summary>
        /// The private field used for the <see cref="ScreenDC"/> property.
        /// </summary>
        private IntPtr _screenDC;

        /// <summary>
        /// A private reference to the glow bitmap that is to be drawn by the context.
        /// </summary>
        private GlowBitmap _windowBitmap;

        /// <summary>
        /// The private field used for the <see cref="WindowDC"/> property.
        /// </summary>
        private IntPtr _windowDC;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="GlowDrawingContext"/> class that will
        /// be for a
        /// glow bitmap with a specified width and height.
        /// </summary>
        /// <param name="width">
        /// The width of the glow bitmap this drawing context is for.
        /// </param>
        /// <param name="height">
        /// The height of the glow bitmap this drawing context is for.
        /// </param>
        public GlowDrawingContext(int width, int height)
        {
            this._screenDC = User32.GetDC(IntPtr.Zero);
            if (this._screenDC == IntPtr.Zero)
            {
                return;
            }

            this._windowDC = Gdi32.CreateCompatibleDC(this._screenDC);
            if (this._windowDC == IntPtr.Zero)
            {
                return;
            }

            this._backgroundDC = Gdi32.CreateCompatibleDC(this._screenDC);
            if (this._backgroundDC == IntPtr.Zero)
            {
                return;
            }

            this._blend.BlendOp = 0;
            this._blend.BlendFlags = 0;
            this._blend.SourceConstantAlpha = 255;
            this._blend.AlphaFormat = 1;

            this._windowBitmap = new GlowBitmap(this.ScreenDC, width, height);
            Gdi32.SelectObject(this._windowDC, this._windowBitmap.Handle);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the background device context for the glow bitmap.
        /// </summary>
        public IntPtr BackgroundDC
        {
            get { return this._backgroundDC; }
        }

        /// <summary>
        /// Gets the blend function used with this drawing context.
        /// </summary>
        public BLENDFUNCTION Blend
        {
            get { return this._blend; }
        }

        /// <summary>
        /// Gets the height of the glow bitmap that this drawing context is for.
        /// </summary>
        public int Height
        {
            get { return this._windowBitmap.Height; }
        }

        /// <summary>
        /// Gets the screen device context for the glow bitmap.
        /// </summary>
        public IntPtr ScreenDC
        {
            get { return this._screenDC; }
        }

        /// <summary>
        /// Gets the width of the glow bitmap that this drawing context is for.
        /// </summary>
        public int Width
        {
            get { return this._windowBitmap.Width; }
        }

        /// <summary>
        /// Gets the window device context for the glow bitmap.
        /// </summary>
        public IntPtr WindowDC
        {
            get { return this._windowDC; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Disposes of all the managed resources contained inside this drawing context.
        /// </summary>
        protected override void DisposeManagedResources()
        {
            this._windowBitmap.Dispose();
        }

        /// <summary>
        /// Disposes of all the native resources contained inside this drawing context.
        /// </summary>
        protected override void DisposeNativeResources()
        {
            if (this._screenDC != IntPtr.Zero)
            {
                User32.ReleaseDC(IntPtr.Zero, this._screenDC);
            }

            if (this._windowDC != IntPtr.Zero)
            {
                Gdi32.DeleteDC(this._windowDC);
            }

            if (this._backgroundDC != IntPtr.Zero)
            {
                Gdi32.DeleteDC(this._backgroundDC);
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.Media.GlowDrawingContext {Class}
} // RSG.Editor.Controls.Media {Namespace}
