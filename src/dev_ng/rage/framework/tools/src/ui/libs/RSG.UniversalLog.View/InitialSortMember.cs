﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.UniversalLog.View
{
    /// <summary>
    /// Defines the difference members of the Universal Log Viewers data grid that can be initially sorted
    /// before the control is loaded.
    /// </summary>
    public enum InitialSortMember
    {
        LogLevel,

        Timestamp,

        Ticks,

        SystemContext,

        MessageContext,

        FileContext,

        Message
    } // InitialSortMember
}
