﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsTechArtWindow.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.TechArt
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows;
    using RSG.Base.Extensions;
    using System.Diagnostics;
    using RSG.Services.Statistics.Consumers;
    using System.Threading.Tasks;
    using RSG.Base.Logging;

    /// <summary>
    /// Represents a window that tech art can use that merges their own styles and template
    /// overrides into the resource packets so that it looks and works like tech art want but
    /// uses our logic as the base.
    /// </summary>
    public class RsTechArtWindow : RsWindow
    {
        #region Fields
        /// <summary>
        /// Unique guid for the usage of this tech art tool.
        /// </summary>
        private readonly Guid _toolSessionGuid = Guid.NewGuid();

        /// <summary>
        /// A value indicating whether the tool usage was tracked.
        /// </summary>
        private bool _usageTracked = false;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsTechArtWindow"/> class.
        /// </summary>
        static RsTechArtWindow()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsTechArtWindow),
                new FrameworkPropertyMetadata(typeof(RsTechArtWindow)));

            Window.TitleProperty.OverrideMetadata(
                typeof(RsTechArtWindow),
                new FrameworkPropertyMetadata(null, CoerceTitle));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsWindow"/> class.
        /// </summary>
        public RsTechArtWindow()
        {
            Collection<ResourceDictionary> resources = this.Resources.MergedDictionaries;
            if (resources == null)
            {
                return;
            }

            string theme = "Themes/override.techart.xaml";
            string path = "pack://application:,,,/RSG.Editor.Controls.TechArt;component/{0}";
            string standardTheme = path.FormatInvariant(theme);
            Uri overrideSource = new Uri(standardTheme, System.UriKind.RelativeOrAbsolute);

            try
            {
                ResourceDictionary dictionary = new ResourceDictionary();
                dictionary.Source = overrideSource;
                resources.Add(dictionary);
            }
            catch (Exception)
            {
                return;
            }

            Loaded += RsTechArtWindow_Loaded;
            Closed += RsTechArtWindow_Closed;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the name of this tool.
        /// </summary>
        public string LaunchLocation { get; set; }

        /// <summary>
        /// Gets the name of this tool.
        /// </summary>
        public string ToolName { get; set; }

        /// <summary>
        /// Gets the version of this tool.
        /// </summary>
        public string ToolVersion { get; set; }

        /// <summary>
        /// Gets a flag indicating whether we wish to track the tool usage session on
        /// the statistics server.
        /// </summary>
        public bool TrackUsageStatistics { get; set; }

        /// <summary>
        /// The wiki help link, Just the end bit.
        /// </summary>
        public string Wiki { get; set; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Called whenever the window receives the WM_SYSCOMMAND message with the CONTEXTHELP
        /// identifier set in the word parameter.
        /// </summary>
        protected override void HandleContextHelp()
        {
            Process.Start("https://devstar.rockstargames.com/wiki/index.php/" + Wiki);
        }

        /// <summary>
        /// Called whenever the Title dependency property needs to be
        /// re-evaluated.
        /// </summary>
        /// <param name="s">
        /// The <see cref="RsMainWindow"/> instance whose dependency property needs evaluated.
        /// </param>
        /// <param name="baseValue">
        /// The new value of the property, prior to any coercion attempt.
        /// </param>
        /// <returns>
        /// The coerced value.
        /// </returns>
        private static object CoerceTitle(DependencyObject s, object baseValue)
        {
            RsTechArtWindow window = s as RsTechArtWindow;
            if (window == null)
            {
                Debug.Assert(false, "Incorrect cast type in property coerce");
                return baseValue;
            }

            if (!window.SupportCoerceTitle)
            {
                return baseValue;
            }

            string value = baseValue as string;
            if (String.IsNullOrEmpty(window.ToolName) == null ||
                String.IsNullOrEmpty(window.ToolVersion))
            {
                return baseValue;
            }

            if (String.IsNullOrEmpty(value))
            {
                return "{0} - v{1}".FormatInvariant(window.ToolName, window.ToolVersion);
            }
            else
            {
                return "{0} - {1} - v{2}".FormatInvariant(value, window.ToolName, window.ToolVersion);
            }
        }

        /// <summary>
        /// Event handler for when the window is closed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RsTechArtWindow_Closed(object sender, EventArgs e)
        {
#if !DEBUG
            if (TrackUsageStatistics && !Debugger.IsAttached)
            {
                TrackWindowClosed();
            }
#endif
        }

        /// <summary>
        /// Event handler for when the window is loaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RsTechArtWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if (TrackUsageStatistics)
            {
                if (String.IsNullOrEmpty(ToolName) || String.IsNullOrEmpty(ToolVersion))
                {
                    throw new ArgumentNullException("ToolName and ToolVersion need to be set if you wish to track RsTechArtWindow usage statistics.");
                }
#if !DEBUG
                // Only track stats for Release builds that aren't attached to the debugger.
                if (!Debugger.IsAttached)
                {
                    // Kick off a task to track the application session stat.
                    Task.Factory.StartNew(() => TrackUsageStats());
                }
#endif
            }
        }

        /// <summary>
        /// Sends the tool usage information to the statistics server.
        /// </summary>
        private void TrackUsageStats()
        {
            RsApplication application = Application.Current as RsApplication;
            if (application == null)
            {
                return;
            }

            LogFactory.ApplicationLog.Message(
                "Tracking tech art tool usage session {0}.", this._toolSessionGuid);
            try
            {
                ApplicationSessionConsumer consumer =
                    new ApplicationSessionConsumer(application.StatisticsServer);
                consumer.TrackApplicationSession(
                    this._toolSessionGuid,
                    application.ApplicationSessionGuid,
                    this.ToolName,
                    this.ToolVersion,
                    this.LaunchLocation,
                    DateTime.UtcNow,
                    application.CommandOptions.Project);
                this._usageTracked = true;
            }
            catch (Exception e)
            {
                LogFactory.ApplicationLog.ToolException(
                    e,
                    "Unable to track tech art tool usage stat due to an unhandled exception.");
                this._usageTracked = false;
            }
        }

        /// <summary>
        /// Sends the tech art window exit information to the statistics server.
        /// </summary>
        private void TrackWindowClosed()
        {
            if (!this._usageTracked)
            {
                // We didn't track the startup, so don't bother tracking the exit.
                return;
            }

            RsApplication application = Application.Current as RsApplication;
            if (application == null)
            {
                return;
            }

            LogFactory.ApplicationLog.Message("Tracking tech art tool exit info.");
            try
            {
                ApplicationSessionConsumer consumer =
                    new ApplicationSessionConsumer(application.StatisticsServer);
                consumer.TrackApplicationExit(
                    this._toolSessionGuid,
                    0,
                    DateTime.UtcNow);
            }
            catch (Exception e)
            {
                LogFactory.ApplicationLog.ToolException(
                    e,
                    "Unable to track tech art tool exit info due to an unhandled exception.");
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.TechArt.RsTechArtWindow {Class}
} // RSG.Editor.Controls.TechArt {Namespace}
