﻿//---------------------------------------------------------------------------------------------
// <copyright file="LogicalOrConverter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Converters
{
    using System.Globalization;

    /// <summary>
    /// Converters n-number of boolean values to a single boolean value by the logical or
    /// operator.
    /// </summary>
    public class LogicalOrConverter : MultiValueConverter<bool, bool>
    {
        #region Methods
        /// <summary>
        /// Converters the specified source values to a single target value.
        /// </summary>
        /// <param name="values">
        /// The pre-converted source values.
        /// </param>
        /// <param name="param">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A target value created by converting the two specified source values.
        /// </returns>
        protected override bool Convert(bool[] values, object param, CultureInfo culture)
        {
            foreach (bool value in values)
            {
                if (value == true)
                {
                    return true;
                }
            }

            return false;
        }
        #endregion Methods
    } // RSG.Editor.Controls.Converters.LogicalOrConverter {Class}
} // RSG.Editor.Controls.Converters {Namespace}
