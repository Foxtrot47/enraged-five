﻿//---------------------------------------------------------------------------------------------
// <copyright file="LogicTreeExtensions.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Helpers
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    ///  Static class used to add extensions onto dependency objects to do with the logic tree.
    /// </summary>
    public static class LogicTreeExtensions
    {
        #region Methods
        /// <summary>
        /// Finds the first occurrence of the qualifying type by looking up through the
        /// logic tree.
        /// </summary>
        /// <typeparam name="T">
        /// The type of object that you want to find in the logic tree.
        /// </typeparam>
        /// <param name="d">
        /// The dependency object to use as the starting root for the search.
        /// </param>
        /// <param name="self">
        /// A value indicating whether the root should also be returned if valid.
        /// </param>
        /// <returns>
        /// A instance to the first occurrence of the qualifying type.
        /// </returns>
        public static T GetLogicalAncestor<T>(this DependencyObject d, bool self)
            where T : DependencyObject
        {
            return d.GetLogicalAncestors<T>(self).FirstOrDefault();
        }

        /// <summary>
        /// Creates an Enumerable that can be used to get all or loop through all instances of
        /// a specific type in the logic tree above this dependency object.
        /// </summary>
        /// <typeparam name="T">
        /// The type of dependency object that you want to find in the logic tree.
        /// </typeparam>
        /// <param name="d">
        /// The dependency object to use as the starting root for the search.
        /// </param>
        /// <param name="self">
        /// A value indicating whether the root should also be returned if valid.
        /// </param>
        /// <returns>
        /// Returns an Enumerable that can be used to get all or loop through all instances of
        /// a specific type in the logic tree above this dependency object.
        /// </returns>
        public static IEnumerable<T> GetLogicalAncestors<T>(this DependencyObject d, bool self)
            where T : DependencyObject
        {
            if (d == null)
            {
                yield break;
            }

            if (self && d is T)
            {
                yield return (T)d;
            }

            DependencyObject parent = LogicalTreeHelper.GetParent(d);
            if (parent != null)
            {
                if (parent is T)
                {
                    yield return (T)parent;
                }

                foreach (T match in GetLogicalAncestors<T>(parent, false))
                {
                    yield return match;
                }
            }

            yield break;
        }

        /// <summary>
        /// Finds the first occurrence of the qualifying type by looking down through the
        /// logic tree.
        /// </summary>
        /// <typeparam name="T">
        /// The type of object that you want to find in the logic tree.
        /// </typeparam>
        /// <param name="d">
        /// The dependency object to use as the starting root for the search.
        /// </param>
        /// <returns>
        /// A instance to the first occurrence of the qualifying type.
        /// </returns>
        public static T GetLogicalDescendent<T>(this DependencyObject d)
            where T : DependencyObject
        {
            return d.GetLogicalDescendents<T>().FirstOrDefault();
        }

        /// <summary>
        /// Creates an Enumerable that can be used to get all or loop through all instances of
        /// a specific type in the logic tree below this dependency object.
        /// </summary>
        /// <typeparam name="T">
        /// The type of dependency object that you want to find in the logic tree.
        /// </typeparam>
        /// <param name="d">
        /// The dependency object to use as the starting root for the search.
        /// </param>
        /// <returns>
        /// Returns an Enumerable that can be used to get all or loop through all instances of
        /// a specific type in the logic tree below this dependency object.
        /// </returns>
        public static IEnumerable<T> GetLogicalDescendents<T>(this DependencyObject d)
            where T : DependencyObject
        {
            if (d == null)
            {
                yield break;
            }

            IEnumerable children = LogicalTreeHelper.GetChildren(d);
            foreach (object child in children)
            {
                if (child is T)
                {
                    yield return (T)child;
                }

                if (child is DependencyObject)
                {
                    foreach (T match in GetLogicalDescendents<T>(child as DependencyObject))
                    {
                        yield return match;
                    }
                }
            }

            yield break;
        }

        /// <summary>
        /// Retrieves the logical or visual parent for this dependency object.
        /// </summary>
        /// <param name="d">
        /// The dependency object to use as the starting root for the search.
        /// </param>
        /// <returns>
        /// The logical or visual parent for this dependency object.
        /// </returns>
        public static DependencyObject GetLogicalOrVisualParent(this DependencyObject d)
        {
            if (d == null)
            {
                return null;
            }

            if (d is Visual)
            {
                return VisualTreeHelper.GetParent(d) ?? LogicalTreeHelper.GetParent(d);
            }

            return LogicalTreeHelper.GetParent(d);
        }

        /// <summary>
        /// Determines whether this dependency object is a direct ancestor of the other
        /// specified dependency object.
        /// </summary>
        /// <param name="d">
        /// The dependency object that is determined to be a ancestor of the other one or not.
        /// </param>
        /// <param name="other">
        /// The dependency object to use as the starting root for the search.
        /// </param>
        /// <returns>
        /// True if this dependency object is a ancestor of the other specified dependency
        /// property.
        /// </returns>
        public static bool IsLogicalAncestorOf(this DependencyObject d, DependencyObject other)
        {
            if (d == null || other == null)
            {
                return false;
            }

            DependencyObject parent = GetLogicalOrVisualParent(other);
            while (parent != null)
            {
                if (parent == d)
                {
                    return true;
                }

                parent = GetLogicalOrVisualParent(parent);
            }

            return false;
        }

        /// <summary>
        /// Determines whether this dependency object is a direct descendant of the other
        /// specified dependency object.
        /// </summary>
        /// <param name="d">
        /// The dependency object that is determined to be a descendant of the other one or
        /// not.
        /// </param>
        /// <param name="other">
        /// The dependency object to use as the starting root for the search.
        /// </param>
        /// <returns>
        /// True if this dependency object is a descendant of the other specified dependency
        /// property.
        /// </returns>
        public static bool IsLogicalDescendant(this DependencyObject d, DependencyObject other)
        {
            if (d == null || other == null)
            {
                return false;
            }

            DependencyObject parent = GetLogicalOrVisualParent(other);
            while (parent != null)
            {
                if (parent == d)
                {
                    return true;
                }

                parent = GetLogicalOrVisualParent(parent);
            }

            return false;
        }
        #endregion Methods
    } // RSG.Editor.Controls.Helpers.LogicTreeExtensions {Class}
} // RSG.Editor.Controls.Helpers {Namespace}
