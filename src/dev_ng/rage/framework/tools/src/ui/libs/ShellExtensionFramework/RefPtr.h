#ifndef SEF_REFPTR_H
#define SEF_REFPTR_H

#if defined _MSC_VER && _MSC_VER >= 1300
#pragma once
#endif

#include "ShellExtensionFramework.h"

BEGIN_SHELL_EXTENSION_FRAMEWORK_NS

/**
	\brief Reference-counted pointer.
 */
template<typename T>
class CRefPtr
{
public:
	CRefPtr() : m_p(NULL)
	{
	}

	CRefPtr(T* p)
	{
		m_p = p;
		if( m_p != NULL ) m_p->AddRef();
	}

	CRefPtr(const CRefPtr<T>& p)
	{
		m_p = p.m_p;
		if( m_p != NULL ) m_p->AddRef();
	}

	~CRefPtr()
	{
		Release();
	}

	operator T*() const
	{
		return m_p;
	}

	T& operator*() const
	{
		ATLENSURE(m_p!=NULL);
		return *m_p;
	}

	T** operator&()
	{
		ATLASSERT(m_p==NULL);
		return &m_p;
	}

	T* operator=(T* p)
	{
		if( m_p == p ) return *this;
		if( m_p != NULL ) m_p->Release();
		m_p = p;
		if( m_p != NULL ) m_p->AddRef();
		return *this;
	}

	T* operator=(CRefPtr<T>& p)
	{
		return operator=(p.m_p);
	}

	T* operator->() const
	{
		ATLASSERT(m_p!=NULL);
		return m_p;
	}

	bool operator!() const
	{
		return m_p == NULL;
	}

	bool operator==(T* p) const
	{
		return m_p == p;
	}

	void Release()
	{
		T* pTemp = m_p;
		if( pTemp != NULL ) {
			m_p = NULL;
			pTemp->Release();
		}
	}

private:
	T* m_p;
};

END_SHELL_EXTENSION_FRAMEWORK_NS

#endif // SEF_REFPTR_H
