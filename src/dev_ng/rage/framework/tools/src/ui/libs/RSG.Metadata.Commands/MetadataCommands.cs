﻿//---------------------------------------------------------------------------------------------
// <copyright file="MetadataCommands.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Commands
{
    using System;
    using System.Windows.Input;
    using System.Windows.Media.Imaging;
    using RSG.Editor;
    using RSG.Editor.SharedCommands;

    /// <summary>
    /// Defines core commands for the metadata tree structure control. This class cannot be
    /// inherited.
    /// </summary>
    public sealed class MetadataCommands
    {
        #region Fields
        /// <summary>
        /// The private cache of command objects.
        /// </summary>
        private static RockstarRoutedCommand[] _internalCommands = InitialiseInternalStorage();

        /// <summary>
        /// The private string table resource manager used for getting the command related
        /// strings and objects.
        /// </summary>
        private static CommandResourceManager _resourceManager = InitialiseResourceManager();

        /// <summary>
        /// The generic object that is used to sync the creation of the commands.
        /// </summary>
        private static object _syncRoot = new object();
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Prevents a default instance of the <see cref="MetadataCommands"/> class from being
        /// created.
        /// </summary>
        private MetadataCommands()
        {
        }
        #endregion Constructors

        #region Enumerations
        /// <summary>
        /// Defines all of the command identifiers for the different core Rockstar commands.
        /// </summary>
        private enum Id : byte
        {
            /// <summary>
            /// Used to identifier the <see cref="MetadataCommands.AddNewMetadataItem"/> routed
            /// command.
            /// </summary>
            AddNewMetadataItem,

            /// <summary>
            /// Used to identifier the <see cref="MetadataCommands.CopyTunableName"/> routed
            /// command.
            /// </summary>
            CopyTunableName,

            /// <summary>
            /// Used to identifier the <see cref="MetadataCommands.CopyTunableType"/> routed
            /// command.
            /// </summary>
            CopyTunableType,

            /// <summary>
            /// Used to identifier the
            /// <see cref="MetadataCommands.InsertNewMetadataItemAbove"/> routed command.
            /// </summary>
            InsertNewMetadataItemAbove,

            /// <summary>
            /// Used to identifier the
            /// <see cref="MetadataCommands.InsertNewMetadataItemBelow"/> routed command.
            /// </summary>
            InsertNewMetadataItemBelow,

            /// <summary>
            /// Used to identifier the <see cref="MetadataCommands.MoveMetadataItemUp"/> routed
            /// command.
            /// </summary>
            MoveMetadataItemUp,

            /// <summary>
            /// Used to identifier the <see cref="MetadataCommands.MoveMetadataItemDown"/>
            /// routed command.
            /// </summary>
            MoveMetadataItemDown,

            /// <summary>
            /// Used to identifier the <see cref="MetadataCommands.ChangePointerType"/> routed
            /// command.
            /// </summary>
            ChangePointerType,

            /// <summary>
            /// Used to identifier the <see cref="MetadataCommands.ResetToDefault"/> routed
            /// command.
            /// </summary>
            ResetToDefault,

            /// <summary>
            /// Used to identify the <see cref="MetadataCommands.ResourceFile"/> routed
            /// command.
            /// </summary>
            ResourceFile,

            /// <summary>
            /// Used to identify the <see cref="MetadataCommands.SaveAndResourceFile"/> routed
            /// command.
            /// </summary>
            SaveAndResourceFile,

            /// <summary>
            /// Used to identifier the
            /// <see cref="MetadataCommands.ValidateProjectDefinitions"/> routed command.
            /// </summary>
            ValidateProjectDefinitions,
        } // MetadataCommands.Id {Enum}
        #endregion Enumerations

        #region Properties
        /// <summary>
        /// Gets the value that represents the Add New Item command.
        /// </summary>
        public static RockstarRoutedCommand AddNewMetadataItem
        {
            get { return EnsureCommandExists(Id.AddNewMetadataItem, null); }
        }

        /// <summary>
        /// Gets the value that represents the Change Pointer Type command.
        /// </summary>
        public static RockstarRoutedCommand ChangePointerType
        {
            get { return EnsureCommandExists(Id.ChangePointerType, null); }
        }

        /// <summary>
        /// Gets the value that represents the Copy Tunable Name command.
        /// </summary>
        public static RockstarRoutedCommand CopyTunableName
        {
            get { return EnsureCommandExists(Id.CopyTunableName, CommonIcons.Copy); }
        }

        /// <summary>
        /// Gets the value that represents the Copy Tunable Type command.
        /// </summary>
        public static RockstarRoutedCommand CopyTunableType
        {
            get { return EnsureCommandExists(Id.CopyTunableType, CommonIcons.Copy); }
        }

        /// <summary>
        /// Gets the value that represents the Insert New Item Above command.
        /// </summary>
        public static RockstarRoutedCommand InsertNewMetadataItemAbove
        {
            get { return EnsureCommandExists(Id.InsertNewMetadataItemAbove, null); }
        }

        /// <summary>
        /// Gets the value that represents the Insert New Item Below command.
        /// </summary>
        public static RockstarRoutedCommand InsertNewMetadataItemBelow
        {
            get { return EnsureCommandExists(Id.InsertNewMetadataItemBelow, null); }
        }

        /// <summary>
        /// Gets the value that represents the Move Metadata Item Down command.
        /// </summary>
        public static RockstarRoutedCommand MoveMetadataItemDown
        {
            get { return EnsureCommandExists(Id.MoveMetadataItemDown, CommonIcons.ArrowDown); }
        }

        /// <summary>
        /// Gets the value that represents the Move Metadata Item Up command.
        /// </summary>
        public static RockstarRoutedCommand MoveMetadataItemUp
        {
            get { return EnsureCommandExists(Id.MoveMetadataItemUp, CommonIcons.ArrowUp); }
        }

        /// <summary>
        /// Gets the value that represents the Reset Tunable To Default command.
        /// </summary>
        public static RockstarRoutedCommand ResetToDefault
        {
            get { return EnsureCommandExists(Id.ResetToDefault, null); }
        }

        /// <summary>
        /// Gets the value that represents the Resource File command.
        /// </summary>
        public static RockstarRoutedCommand ResourceFile
        {
            get { return EnsureCommandExists(Id.ResourceFile, null); }
        }

        /// <summary>
        /// Gets the value that represents the Save And Resource File command.
        /// </summary>
        public static RockstarRoutedCommand SaveAndResourceFile
        {
            get { return EnsureCommandExists(Id.SaveAndResourceFile, null); }
        }

        /// <summary>
        /// Gets the value that represents the Validate Project Definitions command.
        /// </summary>
        public static RockstarRoutedCommand ValidateProjectDefinitions
        {
            get { return EnsureCommandExists(Id.ValidateProjectDefinitions, null); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="RSG.Editor.RockstarRoutedCommand"/> object that is
        /// associated with the specified identifier.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command to create.
        /// </param>
        /// <param name="icon">
        /// The icon that the created command should be using.
        /// </param>
        /// <returns>
        /// The newly created <see cref="RSG.Editor.RockstarRoutedCommand"/> object.
        /// </returns>
        private static RockstarRoutedCommand CreateCommand(string id, BitmapSource icon)
        {
            string name = _resourceManager.GetName(id);
            InputGestureCollection gestures = _resourceManager.GetGestures(id);
            string category = _resourceManager.GetCategory(id);
            string description = _resourceManager.GetDescription(id);

            return new RockstarRoutedCommand(
                name, typeof(MetadataCommands), gestures, category, description, icon);
        }

        /// <summary>
        /// Makes sure the internal value for the command associated with the specified
        /// identifier exists and returns it.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command that should be created and returned.
        /// </param>
        /// <returns>
        /// The <see cref="RSG.Editor.RockstarRoutedCommand"/> object associated with the
        /// specified identifier.
        /// </returns>
        private static RockstarRoutedCommand EnsureCommandExists(Id id)
        {
            return EnsureCommandExists(id, null);
        }

        /// <summary>
        /// Makes sure the internal value for the command associated with the specified
        /// identifier exists and returns it.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command that should be created and returned.
        /// </param>
        /// <param name="icon">
        /// The icon that the command should be using.
        /// </param>
        /// <returns>
        /// The <see cref="RSG.Editor.RockstarRoutedCommand"/> object associated with the
        /// specified identifier.
        /// </returns>
        private static RockstarRoutedCommand EnsureCommandExists(Id id, BitmapSource icon)
        {
            int commandIndex = (int)id;
            if (commandIndex < -1 || commandIndex >= _internalCommands.Length)
            {
                return null;
            }

            RockstarRoutedCommand command = _internalCommands[commandIndex];
            if (command == null)
            {
                lock (_internalCommands)
                {
                    if (command == null)
                    {
                        command = CreateCommand(id.ToString(), icon);
                        _internalCommands[commandIndex] = command;
                    }
                }
            }

            return command;
        }

        /// <summary>
        /// Initialises the array used to store the
        /// <see cref="RSG.Editor.RockstarRoutedCommand"/> objects that make up the core
        /// commands for a Rockstar application.
        /// </summary>
        /// <returns>
        /// The array used to store the internal command objects.
        /// </returns>
        private static RockstarRoutedCommand[] InitialiseInternalStorage()
        {
            return new RockstarRoutedCommand[Enum.GetValues(typeof(Id)).Length];
        }

        /// <summary>
        /// Initialises the resource manager to use to retrieve command related strings and
        /// objects from a resource data file.
        /// </summary>
        /// <returns>
        /// The resource manager to use with this class.
        /// </returns>
        private static CommandResourceManager InitialiseResourceManager()
        {
            Type type = typeof(MetadataCommands);
            string baseName = type.Namespace + ".Resources.CommandStringTable";
            return new CommandResourceManager(baseName, type.Assembly);
        }
        #endregion Methods
    } // RSG.Metadata.Commands.MetadataCommands {Class}
} // RSG.Metadata.Commands {Namespace}
