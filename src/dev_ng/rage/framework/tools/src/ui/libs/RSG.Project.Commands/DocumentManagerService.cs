﻿//---------------------------------------------------------------------------------------------
// <copyright file="DocumentManagerService.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using RSG.Base.Extensions;
    using RSG.Editor;
    using RSG.Editor.Controls.Dock;
    using RSG.Editor.Controls.Dock.ViewModel;
    using RSG.Editor.Model;
    using RSG.Project.Commands.Resources;
    using RSG.Project.Model;
    using RSG.Project.ViewModel;

    /// <summary>
    /// When implemented represents a service that contains methods that operate on documents
    /// inside a docking manager and project explorer.
    /// </summary>
    public class DocumentManagerService : IDocumentManagerService
    {
        #region Fields
        /// <summary>
        /// The reference to the file watcher the documents opened using this manager will be
        /// associated with.
        /// </summary>
        private FileWatcherManager _fileWatcher;

        /// <summary>
        /// The reference to the backup manager the documents opened using this manager will be
        /// associated with.
        /// </summary>
        private FileBackupManager _backupManager;

        /// <summary>
        /// A command service provider that can be used to get the command dialog service and
        /// the message service.
        /// </summary>
        private ICommandServiceProvider _serviceProvider;

        /// <summary>
        /// The reference to the view site that the documents are opened against.
        /// </summary>
        private ViewSite _viewSite;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DocumentManagerService"/> class.
        /// </summary>
        /// <param name="serviceProvider">
        /// A command service provider that can be used to get the command dialog service and
        /// the message service.
        /// </param>
        public DocumentManagerService(ICommandServiceProvider serviceProvider)
        {
            this._serviceProvider = serviceProvider;
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs whenever a collection is about to be closed.
        /// </summary>
        public event EventHandler<CollectionClosedEventArgs> CollectionClosing;

        /// <summary>
        /// Occurs whenever a document is about to be closed.
        /// </summary>
        public event EventHandler<DocumentClosingEventArgs> DocumentClosing;

        /// <summary>
        /// Occurs whenever a document is opened.
        /// </summary>
        public event EventHandler<DocumentEventArgs> DocumentOpened;
        #endregion Events

        #region Properties
        /// <summary>
        /// Gets the currently active document if there is one; otherwise, null.
        /// </summary>
        public virtual DocumentItem ActiveDocument
        {
            get { return this._viewSite.ActiveDocument; }
        }

        /// <summary>
        /// Gets the file backup manager that has beens et for this service.
        /// </summary>
        public FileBackupManager BackupManager
        {
            get { return this._backupManager; }
        }

        /// <summary>
        /// Gets the file watcher manager that has beens et for this service.
        /// </summary>
        public FileWatcherManager FileWatcher
        {
            get { return this._fileWatcher; }
        }

        /// <summary>
        /// Gets a iterator around all of the currently opened documents.
        /// </summary>
        public virtual IEnumerable<DocumentItem> OpenedDocuments
        {
            get { return this._viewSite.AllDocuments; }
        }

        /// <summary>
        /// Gets the service to use to show the common file dialog windows to the user.
        /// </summary>
        protected ICommonDialogService DialogService
        {
            get { return this._serviceProvider.GetService<ICommonDialogService>(); }
        }

        /// <summary>
        /// Gets the service to use to show message boxes to the user.
        /// </summary>
        protected IMessageBoxService MessageService
        {
            get { return this._serviceProvider.GetService<IMessageBoxService>(); }
        }

        /// <summary>
        /// Gets the service provider to use for this document manager.
        /// </summary>
        protected ICommandServiceProvider ServiceProvider
        {
            get { return this._serviceProvider; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Determines whether the specified node when opened needs to be attached to a backup
        /// manager. By default this returns false.
        /// </summary>
        /// <param name="node">
        /// The node to test.
        /// </param>
        /// <returns>
        /// True if the specified node when opened needs to be attached to a backup manager;
        /// otherwise, false.
        /// </returns>
        public virtual bool AttachToBackupManager(HierarchyNode node)
        {
            return false;
        }

        /// <summary>
        /// Determines whether the specified node when opened needs to be attached to a file
        /// watcher manager. By default this returns false.
        /// </summary>
        /// <param name="node">
        /// The node to test.
        /// </param>
        /// <returns>
        /// True if the specified node when opened needs to be attached to a file watcher;
        /// otherwise, false.
        /// </returns>
        public virtual bool AttachToFileWatcher(HierarchyNode node)
        {
            return false;
        }

        /// <summary>
        /// Determines whether the specified node can be opened.
        /// </summary>
        /// <param name="node">
        /// The hierarchy node to test.
        /// </param>
        /// <returns>
        /// True if the specified node can be opened; otherwise, false.
        /// </returns>
        public virtual bool CanOpenHierarchyNode(HierarchyNode node)
        {
            return false;
        }

        /// <summary>
        /// Closes the active document optionally giving the user a chance to save if
        /// modifications are present.
        /// </summary>
        /// <param name="checkModifications">
        /// A value indicating whether the user should be given a chance to save if there are
        /// modifications.
        /// </param>
        public virtual void CloseActiveDocument(bool checkModifications)
        {
            this.CloseDocument(this.ActiveDocument, checkModifications);
        }

        /// <summary>
        /// Closes the specified collection optionally giving the user a chance to save if
        /// modifications are present.
        /// </summary>
        /// <param name="collection">
        /// The collection to close.
        /// </param>
        /// <param name="checkModifications">
        /// A value indicating whether the user should be given a chance to save if there are
        /// modifications.
        /// </param>
        public virtual void CloseCollection(
            ProjectCollectionNode collection, bool checkModifications)
        {
            if (checkModifications)
            {
                if (!this.CheckModifications(collection))
                {
                    throw new OperationCanceledException();
                }
            }

            EventHandler<CollectionClosedEventArgs> handler = this.CollectionClosing;
            if (handler != null)
            {
                handler(this, new CollectionClosedEventArgs(collection, collection.FullPath));
            }

            this.CloseOpenedDocuments(false);
            collection.Unload();
        }

        /// <summary>
        /// Closes the specified document  optionally giving the user a chance to save if
        /// modifications are present.
        /// </summary>
        /// <param name="document">
        /// The document to close.
        /// </param>
        /// <param name="checkModifications">
        /// A value indicating whether the user should be given a chance to save if there are
        /// modifications.
        /// </param>
        public virtual void CloseDocument(DocumentItem document, bool checkModifications)
        {
            if (checkModifications)
            {
                if (!this.CheckModifications(document))
                {
                    throw new OperationCanceledException();
                }
            }

            EventHandler<DocumentClosingEventArgs> handler = this.DocumentClosing;
            if (handler != null)
            {
                handler(this, new DocumentClosingEventArgs(document));
            }

            this._viewSite.HandleDocumentClosing(document);
            DockingPane pane = document.ParentPane;
            if (pane == null)
            {
                return;
            }

            pane.RemoveItem(document);
            ProjectDocumentItem projectDocument = document as ProjectDocumentItem;
            if (projectDocument != null)
            {
                ProjectNode projectNode = projectDocument.FileNode as ProjectNode;
                if (projectNode != null)
                {
                    return;
                }
            }

            this._fileWatcher.UnregisterFile(document.FullPath);
            this._backupManager.UnregisterFile(document);
        }

        /// <summary>
        /// Closes all of the specified documents optionally giving the user a chance to save
        /// if modifications are present.
        /// </summary>
        /// <param name="documents">
        /// The documents to close.
        /// </param>
        /// <param name="checkModifications">
        /// A value indicating whether the user should be given a chance to save if there are
        /// modifications.
        /// </param>
        public virtual void CloseDocuments(
            IList<DocumentItem> documents, bool checkModifications)
        {
            if (checkModifications)
            {
                if (!this.CheckModifications(documents))
                {
                    throw new OperationCanceledException();
                }
            }

            foreach (DocumentItem document in documents)
            {
                this.CloseDocument(document, false);
            }
        }

        /// <summary>
        /// Closes all opened documents optionally giving the user a chance to save if
        /// modifications are present.
        /// </summary>
        /// <param name="checkModifications">
        /// A value indicating whether the user should be given a chance to save if there are
        /// modifications.
        /// </param>
        public virtual void CloseOpenedDocuments(bool checkModifications)
        {
            this.CloseDocuments(this.OpenedDocuments.ToList(), checkModifications);
        }

        /// <summary>
        /// Attempts to find the opened document that is associated with the specified node.
        /// </summary>
        /// <param name="node">
        /// The node whose associated document should be returned.
        /// </param>
        /// <returns>
        /// The document that is associated with the specified node; otherwise, null.
        /// </returns>
        public virtual DocumentItem FindDocument(IHierarchyNode node)
        {
            return null;
        }

        /// <summary>
        /// Fires the <see cref="DocumentOpened"/> event.
        /// </summary>
        /// <param name="item">
        /// The document item that has been opened.
        /// </param>
        public void FireDocumentOpenedEvent(DocumentItem item)
        {
            EventHandler<DocumentEventArgs> handler = this.DocumentOpened;
            if (handler == null)
            {
                return;
            }

            handler(this, new DocumentEventArgs(item));
        }

        /// <summary>
        /// Retrieves a value indicating whether the specified document is modified.
        /// </summary>
        /// <param name="document">
        /// The document to test.
        /// </param>
        /// <returns>
        /// True if the specified document is modified; otherwise, false.
        /// </returns>
        public virtual bool IsDocumentModified(DocumentItem document)
        {
            return document.IsModified;
        }

        /// <summary>
        /// Retrieves a value indicating whether the specified project is modified.
        /// </summary>
        /// <param name="project">
        /// The project to test.
        /// </param>
        /// <returns>
        /// True if the specified project is modified; otherwise, false.
        /// </returns>
        public virtual bool IsProjectModified(ProjectNode project)
        {
            return project.IsModified;
        }

        /// <summary>
        /// Retrieves a value indicating whether the specified collection is modified.
        /// </summary>
        /// <param name="collection">
        /// The collection to test.
        /// </param>
        /// <returns>
        /// True if the specified collection is modified; otherwise, false.
        /// </returns>
        public virtual bool IsProjectCollectionModified(ProjectCollectionNode collection)
        {
            return collection.IsModified;
        }

        /// <summary>
        /// Opens the specified hierarchy node.
        /// </summary>
        /// <param name="node">
        /// The node to open.
        /// </param>
        public virtual void OpenHierarchyNode(IHierarchyNode node)
        {
        }

        /// <summary>
        /// Saves the currently active document.
        /// </summary>
        /// <param name="forceSaveAs">
        /// A value indicating whether this should force the user to select a location for the
        /// saved document in windows explorer.
        /// </param>
        public virtual void SaveActiveDocument(bool forceSaveAs)
        {
            this.SaveDocuments(new List<DocumentItem>() { this.ActiveDocument }, forceSaveAs);
        }

        /// <summary>
        /// Saves the currently active document forcing the user to select a location for the
        /// saved document in windows explorer.
        /// </summary>
        public virtual void SaveActiveDocumentAs()
        {
            this.SaveActiveDocument(true);
        }

        /// <summary>
        /// Saves the specified collection.
        /// </summary>
        /// <param name="collection">
        /// The collection that will be saved.
        /// </param>
        /// <param name="forceSaveAs">
        /// A value indicating whether this should force the user to select a location for the
        /// saved collection in windows explorer.
        /// </param>
        public virtual void SaveCollection(ProjectCollectionNode collection, bool forceSaveAs)
        {
            if (collection == null || !collection.CanBeSaved)
            {
                return;
            }

            IMessageBoxService messageService = this.MessageService;
            ICommonDialogService dlgService = this.DialogService;
            if (messageService == null || dlgService == null)
            {
                Debug.Assert(false, "Unable to save as services are missing.");
                return;
            }

            string fullPath = collection.FullPath;
            using (this._fileWatcher.SuspendChangeEvent(fullPath))
            {
                if (collection.IsTemporaryFile || forceSaveAs)
                {
                    string filter = collection.Definition.Filter;
                    string name = collection.Name;
                    if (!dlgService.ShowSaveFile(null, name, filter, 0, out fullPath))
                    {
                        throw new OperationCanceledException();
                    }
                }

                if (String.IsNullOrWhiteSpace(fullPath))
                {
                    messageService.ShowStandardErrorBox(
                        StringTable.PathCreationFailedMsg,
                        StringTable.PathCreationFailedCaption);

                    return;
                }

                collection.IsTemporaryFile = false;
                collection.FullPath = fullPath;

                foreach (MetadataLevel level in Enum.GetValues(typeof(MetadataLevel)))
                {
                    string extension = MetadataLevelUtil.GetExtensionAttribute(level);
                    if (extension == null)
                    {
                        continue;
                    }

                    string filename = fullPath + extension;
                    bool cancelled = false;
                    if (!dlgService.HandleSavingOfReadOnlyFile(ref filename, ref cancelled))
                    {
                        collection.IsReadOnly = true;
                        return;
                    }
                    else
                    {
                        collection.IsReadOnly = false;
                    }

                    if (cancelled)
                    {
                        throw new OperationCanceledException();
                    }

                    using (MemoryStream stream = new MemoryStream())
                    {
                        if (collection.Save(stream, level))
                        {
                            FileMode mode = FileMode.Create;
                            FileAccess access = FileAccess.Write;
                            using (FileStream file = new FileStream(filename, mode, access))
                            {
                                stream.WriteTo(file);
                                file.Close();
                                file.Dispose();
                            }

                            stream.Close();
                            stream.Dispose();
                        }
                        else
                        {
                            string name = Path.GetFileName(filename);
                            messageService.ShowStandardErrorBox(
                                StringTable.SaveFailedMsg.FormatInvariant(name),
                                StringTable.SaveFailedCaption);
                            return;
                        }
                    }
                }

                collection.IsModified = false;
            }
        }

        /// <summary>
        /// Saves the specified document.
        /// </summary>
        /// <param name="document">
        /// The document that will be saved.
        /// </param>
        /// <param name="forceSaveAs">
        /// A value indicating whether this should force the user to select a location for the
        /// saved document in windows explorer.
        /// </param>
        public virtual void SaveDocument(DocumentItem document, bool forceSaveAs)
        {
            ISaveableDocument saveableItem = document as ISaveableDocument;
            if (saveableItem == null)
            {
                return;
            }

            if (document == null || !document.CanSave)
            {
                return;
            }

            string fullPath = saveableItem.FullPath;
            ProjectDocumentItem projectDocument = document as ProjectDocumentItem;
            if (projectDocument != null)
            {
                ProjectNode projectNode = projectDocument.FileNode as ProjectNode;
                if (projectNode != null)
                {
                    IProjectPropertyContent content =
                        projectDocument.Content as IProjectPropertyContent;
                    if (content != null)
                    {
                        using (this._fileWatcher.SuspendChangeEvent(fullPath))
                        {
                            content.ApplyProperties();
                            this.SaveProject(projectNode, forceSaveAs);

                            if (saveableItem != null)
                            {
                                saveableItem.FullPath = projectNode.FullPath;
                                UndoEngine undoEngine = saveableItem.UndoEngine;
                                if (undoEngine != null)
                                {
                                    undoEngine.PushCleanDirtyState();
                                }
                            }
                        }

                        return;
                    }
                }
            }

            IMessageBoxService messageService = this.MessageService;
            ICommonDialogService dlgService = this.DialogService;
            if (messageService == null || dlgService == null)
            {
                Debug.Fail("Unable to save as services are missing.");
                return;
            }

            using (this._fileWatcher.SuspendChangeEvent(fullPath))
            {
                if (saveableItem.IsTemporaryFile || forceSaveAs)
                {
                    if (!dlgService.ShowSaveFile(null, saveableItem.Filename, out fullPath))
                    {
                        throw new OperationCanceledException();
                    }
                }

                if (String.IsNullOrWhiteSpace(fullPath))
                {
                    messageService.ShowStandardErrorBox(
                        StringTable.PathCreationFailedMsg,
                        StringTable.PathCreationFailedCaption);

                    return;
                }

                bool cancelled = false;
                if (!dlgService.HandleSavingOfReadOnlyFile(ref fullPath, ref cancelled))
                {
                    saveableItem.IsReadOnly = true;
                    return;
                }
                else
                {
                    saveableItem.IsReadOnly = false;
                }

                if (cancelled)
                {
                    throw new OperationCanceledException();
                }

                using (MemoryStream stream = new MemoryStream())
                {
                    if (saveableItem.Save(stream))
                    {
                        FileMode mode = FileMode.Create;
                        FileAccess access = FileAccess.Write;
                        using (FileStream file = new FileStream(fullPath, mode, access))
                        {
                            stream.WriteTo(file);
                            file.Close();
                            file.Dispose();
                        }

                        stream.Close();
                        stream.Dispose();

                        saveableItem.FullPath = fullPath;
                        UndoEngine undoEngine = saveableItem.UndoEngine;
                        if (undoEngine != null)
                        {
                            undoEngine.PushCleanDirtyState();
                        }

                        saveableItem.RaiseSavedEvent();
                    }
                    else
                    {
                        string filename = Path.GetFileName(fullPath);
                        messageService.ShowStandardErrorBox(
                            StringTable.SaveFailedMsg.FormatInvariant(filename),
                            StringTable.SaveFailedCaption);
                    }
                }
            }
        }

        /// <summary>
        /// Saves all of the specified documents.
        /// </summary>
        /// <param name="documents">
        /// The documents to save.
        /// </param>
        /// <param name="forceSaveAs">
        /// A value indicating whether this should force the user to select a location for the
        /// saved documents in windows explorer.
        /// </param>
        public virtual void SaveDocuments(IList<DocumentItem> documents, bool forceSaveAs)
        {
            foreach (DocumentItem document in documents)
            {
                this.SaveDocument(document, forceSaveAs);
            }
        }

        /// <summary>
        /// Saves the specified item.
        /// </summary>
        /// <param name="item">
        /// The item to save.
        /// </param>
        /// <param name="forceSaveAs">
        /// A value indicating whether this should force the user to select a location for the
        /// saved item in windows explorer.
        /// </param>
        public virtual void SaveItem(IHierarchyNode item, bool forceSaveAs)
        {
            DocumentItem document = item as DocumentItem;
            if (document != null)
            {
                this.SaveDocuments(new List<DocumentItem>() { document }, forceSaveAs);
            }

            ProjectNode project = item as ProjectNode;
            if (project != null)
            {
                this.SaveProject(project, forceSaveAs);
            }

            ProjectCollectionNode collection = item as ProjectCollectionNode;
            if (collection != null)
            {
                this.SaveCollection(collection, forceSaveAs);
            }
        }

        /// <summary>
        /// Saves all of the specified items.
        /// </summary>
        /// <param name="items">
        /// The items to save.
        /// </param>
        /// <param name="forceSaveAs">
        /// A value indicating whether this should force the user to select a location for the
        /// saved items in windows explorer.
        /// </param>
        public virtual void SaveItems(IEnumerable<IHierarchyNode> items, bool forceSaveAs)
        {
            foreach (IHierarchyNode item in items)
            {
                this.SaveItem(item, forceSaveAs);
            }
        }

        /// <summary>
        /// Saves the specified project.
        /// </summary>
        /// <param name="project">
        /// The project that will be saved.
        /// </param>
        /// <param name="forceSaveAs">
        /// A value indicating whether this should force the user to select a location for the
        /// saved project in windows explorer.
        /// </param>
        public virtual void SaveProject(ProjectNode project, bool forceSaveAs)
        {
            if (project == null || !project.CanBeSaved)
            {
                return;
            }

            IMessageBoxService messageService = this.MessageService;
            ICommonDialogService dlg = this.DialogService;
            if (messageService == null || dlg == null)
            {
                Debug.Assert(false, "Unable to save as services are missing.");
                return;
            }

            string fullPath = project.FullPath;
            using (this._fileWatcher.SuspendChangeEvent(fullPath))
            {
                if (project.IsTemporaryFile || forceSaveAs)
                {
                    string filter = project.Definition.Filter;
                    if (!dlg.ShowSaveFile(null, project.Filename, filter, 0, out fullPath))
                    {
                        throw new OperationCanceledException();
                    }
                }

                if (String.IsNullOrWhiteSpace(fullPath))
                {
                    messageService.ShowStandardErrorBox(
                        StringTable.PathCreationFailedMsg,
                        StringTable.PathCreationFailedCaption);

                    return;
                }

                foreach (MetadataLevel level in Enum.GetValues(typeof(MetadataLevel)))
                {
                    string extension = MetadataLevelUtil.GetExtensionAttribute(level);
                    if (extension == null)
                    {
                        continue;
                    }

                    string filename = fullPath + extension;
                    bool cancelled = false;
                    if (!dlg.HandleSavingOfReadOnlyFile(ref filename, ref cancelled))
                    {
                        project.IsReadOnly = true;
                        return;
                    }
                    else
                    {
                        project.IsReadOnly = false;
                    }

                    if (cancelled)
                    {
                        throw new OperationCanceledException();
                    }

                    using (MemoryStream stream = new MemoryStream())
                    {
                        if (project.Save(stream, level))
                        {
                            FileMode mode = FileMode.Create;
                            FileAccess access = FileAccess.Write;
                            using (FileStream file = new FileStream(filename, mode, access))
                            {
                                stream.WriteTo(file);
                                file.Close();
                                file.Dispose();
                            }

                            stream.Close();
                            stream.Dispose();
                        }
                        else
                        {
                            string name = Path.GetFileName(filename);
                            messageService.ShowStandardErrorBox(
                                StringTable.SaveFailedMsg.FormatInvariant(name),
                                StringTable.SaveFailedCaption);
                            return;
                        }
                    }
                }

                project.FullPath = fullPath;
                project.IsModified = false;
            }
        }

        /// <summary>
        /// Sets the backup manager to use for the documents created using this manager.
        /// </summary>
        /// <param name="backupManager">
        /// The instance of the backup manager to use.
        /// </param>
        public virtual void SetBackupManager(FileBackupManager backupManager)
        {
            if (this._backupManager != null)
            {
                throw new NotSupportedException(
                    "Cannot reset the backup manager for this manager");
            }

            this._backupManager = backupManager;
        }

        /// <summary>
        /// Sets the file watcher to use for the documents created using this manager.
        /// </summary>
        /// <param name="fileWatcher">
        /// The instance of the file watcher to use.
        /// </param>
        public virtual void SetFileWatcher(FileWatcherManager fileWatcher)
        {
            if (this._fileWatcher != null)
            {
                throw new NotSupportedException(
                    "Cannot reset the file watcher for this manager");
            }

            this._fileWatcher = fileWatcher;
        }

        /// <summary>
        /// Sets the view site to use for the documents created using this manager.
        /// </summary>
        /// <param name="viewSite">
        /// The instance of the viewSite site to use.
        /// </param>
        public virtual void SetViewSite(ViewSite viewSite)
        {
            if (this._viewSite != null)
            {
                throw new NotSupportedException(
                    "Cannot reset the view site for this manager");
            }

            this._viewSite = viewSite;
        }

        /// <summary>
        /// Unloaded the specified project making sure that all files from the project are
        /// closed as well.
        /// </summary>
        /// <param name="project">
        /// The project to unload.
        /// </param>
        /// <param name="checkModifications">
        /// A value indicating whether the user should be given a chance to save if there are
        /// modifications.
        /// </param>
        public virtual void UnloadProject(ProjectNode project, bool checkModifications)
        {
            List<DocumentItem> documentsToClose = new List<DocumentItem>();
            foreach (DocumentItem document in this.OpenedDocuments)
            {
                ProjectDocumentItem projectDocument = document as ProjectDocumentItem;
                if (projectDocument == null)
                {
                    continue;
                }

                ProjectNode parentProject = projectDocument.FileNode.ParentProject;
                if (!Object.ReferenceEquals(project, parentProject))
                {
                    continue;
                }

                documentsToClose.Add(document);
            }

            if (checkModifications)
            {
                if (!this.CheckModifications(project, documentsToClose))
                {
                    throw new OperationCanceledException();
                }
            }

            this.CloseDocuments(documentsToClose, false);
            this._fileWatcher.UnregisterFile(project.FullPath);
            project.Unload();
        }

        /// <summary>
        /// Unloaded the specified project making sure that all files from the project are
        /// closed as well.
        /// </summary>
        /// <param name="projects">
        /// The projects to unload.
        /// </param>
        /// <param name="checkModifications">
        /// A value indicating whether the user should be given a chance to save if there are
        /// modifications.
        /// </param>
        public virtual void UnloadProjects(
            IList<ProjectNode> projects, bool checkModifications)
        {
            List<DocumentItem> documentsToClose = new List<DocumentItem>();
            foreach (ProjectNode project in projects)
            {
                foreach (DocumentItem document in this.OpenedDocuments)
                {
                    ProjectDocumentItem projectDocument = document as ProjectDocumentItem;
                    if (projectDocument == null)
                    {
                        continue;
                    }

                    ProjectNode parentProject = projectDocument.FileNode.ParentProject;
                    if (!Object.ReferenceEquals(project, parentProject))
                    {
                        continue;
                    }

                    documentsToClose.Add(document);
                }
            }

            if (checkModifications)
            {
                if (!this.CheckModifications(projects, documentsToClose))
                {
                    throw new OperationCanceledException();
                }
            }

            foreach (ProjectNode project in projects)
            {
                this.UnloadProject(project, false);
            }
        }

        /// <summary>
        /// Checks whether the specified document is modified and if so asks the user if they
        /// want to save. If the user presses yes this also saves the document.
        /// </summary>
        /// <param name="document">
        /// The document to check for modifications.
        /// </param>
        /// <returns>
        /// False if the user has cancelled the operation; otherwise, true.
        /// </returns>
        private bool CheckModifications(DocumentItem document)
        {
            List<CloseModifiedItemViewModel> modified = new List<CloseModifiedItemViewModel>();
            if (this.IsDocumentModified(document))
            {
                string friendlySavePath = document.FriendlySavePath;
                modified.Add(new CloseModifiedItemViewModel(friendlySavePath, true, document));
            }

            if (modified.Count == 0)
            {
                return true;
            }

            RsCloseModifiedWindow window = new RsCloseModifiedWindow();
            window.CloseModifiedItems = modified;
            bool? windowResult = window.ShowDialog();
            if (windowResult != true)
            {
                return false;
            }

            if (window.MarkForSave(document))
            {
                this.SaveDocuments(new List<DocumentItem>() { document }, false);
            }

            return true;
        }

        /// <summary>
        /// Checks whether any of the the specified documents are modified and if so asks the
        /// user if they want to save. If the user presses yes this also saves the documents.
        /// </summary>
        /// <param name="documents">
        /// The documents to check for modifications.
        /// </param>
        /// <returns>
        /// False if the user has cancelled the operation; otherwise, true.
        /// </returns>
        private bool CheckModifications(IList<DocumentItem> documents)
        {
            List<CloseModifiedItemViewModel> modified = new List<CloseModifiedItemViewModel>();
            foreach (DocumentItem document in documents)
            {
                if (!this.IsDocumentModified(document))
                {
                    continue;
                }

                string friendlySavePath = document.FriendlySavePath;
                modified.Add(new CloseModifiedItemViewModel(friendlySavePath, true, document));
            }

            if (modified.Count == 0)
            {
                return true;
            }

            RsCloseModifiedWindow window = new RsCloseModifiedWindow();
            window.CloseModifiedItems = modified;
            bool? windowResult = window.ShowDialog();
            if (windowResult != true)
            {
                return false;
            }

            List<DocumentItem> documentsToSave = new List<DocumentItem>();
            foreach (DocumentItem document in documents)
            {
                if (window.MarkForSave(document))
                {
                    documentsToSave.Add(document);
                }
            }

            this.SaveDocuments(documentsToSave, false);
            return true;
        }

        /// <summary>
        /// Checks whether any of the the specified documents or project are modified and if so
        /// asks the user if they want to save. If the user presses yes this also saves the
        /// documents/project.
        /// </summary>
        /// <param name="project">
        /// The project to check for modifications.
        /// </param>
        /// <param name="documents">
        /// The documents to check for modifications.
        /// </param>
        /// <returns>
        /// False if the user has cancelled the operation; otherwise, true.
        /// </returns>
        private bool CheckModifications(ProjectNode project, IList<DocumentItem> documents)
        {
            List<CloseModifiedItemViewModel> modified = new List<CloseModifiedItemViewModel>();
            if (this.IsProjectModified(project))
            {
                string name = project.FriendlySavePath;
                modified.Add(new CloseModifiedItemViewModel(name, true, project));
            }

            foreach (DocumentItem document in documents)
            {
                if (!this.IsDocumentModified(document))
                {
                    continue;
                }

                string name = document.FriendlySavePath;
                modified.Add(new CloseModifiedItemViewModel(name, true, document));
            }

            if (modified.Count == 0)
            {
                return true;
            }

            RsCloseModifiedWindow window = new RsCloseModifiedWindow();
            window.CloseModifiedItems = modified;
            bool? windowResult = window.ShowDialog();
            if (windowResult != true)
            {
                return false;
            }

            List<DocumentItem> documentsToSave = new List<DocumentItem>();
            foreach (DocumentItem document in documents)
            {
                if (window.MarkForSave(document))
                {
                    documentsToSave.Add(document);
                }
            }

            this.SaveDocuments(documentsToSave, false);
            if (window.MarkForSave(project))
            {
                this.SaveProject(project, false);
            }

            return true;
        }

        /// <summary>
        /// Checks whether any of the the specified documents or projects are modified and if
        /// so asks the user if they want to save. If the user presses yes this also saves the
        /// documents/projects.
        /// </summary>
        /// <param name="projects">
        /// The projects to check for modifications.
        /// </param>
        /// <param name="documents">
        /// The documents to check for modifications.
        /// </param>
        /// <returns>
        /// False if the user has cancelled the operation; otherwise, true.
        /// </returns>
        private bool CheckModifications(
            IList<ProjectNode> projects, IList<DocumentItem> documents)
        {
            List<CloseModifiedItemViewModel> modified = new List<CloseModifiedItemViewModel>();
            foreach (ProjectNode project in projects)
            {
                if (!this.IsProjectModified(project))
                {
                    continue;
                }

                string name = project.FriendlySavePath;
                modified.Add(new CloseModifiedItemViewModel(name, true, project));
            }

            foreach (DocumentItem document in documents)
            {
                if (!this.IsDocumentModified(document))
                {
                    continue;
                }

                string name = document.FriendlySavePath;
                modified.Add(new CloseModifiedItemViewModel(name, true, document));
            }

            if (modified.Count == 0)
            {
                return true;
            }

            RsCloseModifiedWindow window = new RsCloseModifiedWindow();
            window.CloseModifiedItems = modified;
            bool? windowResult = window.ShowDialog();
            if (windowResult != true)
            {
                return false;
            }

            List<DocumentItem> documentsToSave = new List<DocumentItem>();
            foreach (DocumentItem document in documents)
            {
                if (window.MarkForSave(document))
                {
                    documentsToSave.Add(document);
                }
            }

            this.SaveDocuments(documentsToSave, false);
            foreach (ProjectNode project in projects)
            {
                if (window.MarkForSave(project))
                {
                    this.SaveProject(project, false);
                }
            }

            return true;
        }

        /// <summary>
        /// Checks whether the specified collection is modified and if so asks the user if they
        /// want to save. If the user presses yes this also saves the collection.
        /// </summary>
        /// <param name="collection">
        /// The collection to check for modifications.
        /// </param>
        /// <returns>
        /// False if the user has cancelled the operation; otherwise, true.
        /// </returns>
        private bool CheckModifications(ProjectCollectionNode collection)
        {
            List<CloseModifiedItemViewModel> modified = new List<CloseModifiedItemViewModel>();
            foreach (DocumentItem document in this.OpenedDocuments)
            {
                if (!this.IsDocumentModified(document))
                {
                    continue;
                }

                string name = document.FriendlySavePath;
                modified.Add(new CloseModifiedItemViewModel(name, true, document));
            }

            foreach (ProjectNode project in collection.GetProjects())
            {
                if (!this.IsProjectModified(project))
                {
                    continue;
                }

                string name = project.FriendlySavePath;
                modified.Add(new CloseModifiedItemViewModel(name, true, project));
            }

            if (this.IsProjectCollectionModified(collection))
            {
                string name = collection.FriendlySavePath;
                modified.Add(new CloseModifiedItemViewModel(name, true, collection));
            }

            if (modified.Count == 0)
            {
                return true;
            }

            RsCloseModifiedWindow window = new RsCloseModifiedWindow();
            window.CloseModifiedItems = modified;
            bool? windowResult = window.ShowDialog();
            if (windowResult != true)
            {
                return false;
            }

            List<DocumentItem> documentsToSave = new List<DocumentItem>();
            foreach (DocumentItem document in this.OpenedDocuments)
            {
                if (window.MarkForSave(document))
                {
                    documentsToSave.Add(document);
                }
            }

            this.SaveDocuments(documentsToSave, false);
            foreach (ProjectNode project in collection.GetProjects())
            {
                if (window.MarkForSave(project))
                {
                    this.SaveProject(project, false);
                }
            }

            if (window.MarkForSave(collection))
            {
                this.SaveCollection(collection, false);
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public virtual System.Windows.FrameworkElement GetControlForDocument(object content)
        {
            return null;
        }
        #endregion Methods
    } // RSG.Project.Commands.DocumentManagerService {Class}
} // RSG.Project.Commands {Namespace}
