﻿//---------------------------------------------------------------------------------------------
// <copyright file="FilterByCategoryAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Rpf.View.Commands
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Media.Imaging;
    using RSG.Editor;
    using RSG.Rpf.ViewModel;
    using ArgType = System.Collections.Generic.IEnumerable<RpfViewControl>;
    using MultiArgType = RSG.Editor.MultiCommandParameter<RSG.Rpf.ViewModel.PackEntryCategory>;

    /// <summary>
    /// Implements the <see cref="RSG.Rpf.Commands.RpfViewerCommands.FilterRpfByCategory"/>
    /// command.
    /// </summary>
    public class FilterByCategoryAction : FilterAction<ArgType, PackEntryCategory>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="FilterByCategoryAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public FilterByCategoryAction(ParameterResolverDelegate<ArgType> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="secondaryParameter">
        /// The secondary command parameter that has been sent with the command.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ArgType parameter, MultiArgType secondaryParameter)
        {
            if (parameter == null)
            {
                throw new SmartArgumentNullException(() => parameter);
            }

            PackEntryCategory category = secondaryParameter.ItemParameter;
            foreach (RpfViewControl control in parameter)
            {
                if (this.ContainsItems(control, category))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="secondaryParameter">
        /// The secondary command parameter that has been sent with the command.
        /// </param>
        public override void Execute(ArgType parameter, MultiArgType secondaryParameter)
        {
            if (parameter == null)
            {
                throw new SmartArgumentNullException(() => parameter);
            }

            PackEntryCategory category = secondaryParameter.ItemParameter;
            foreach (RpfViewControl control in parameter)
            {
                if (secondaryParameter.IsToggled)
                {
                    if (control.AllowedCategories.Contains(category))
                    {
                        return;
                    }

                    var categories = new List<PackEntryCategory>(control.AllowedCategories);
                    categories.Add(category);
                    control.AllowedCategories = categories;
                }
                else
                {
                    if (!control.AllowedCategories.Contains(category))
                    {
                        return;
                    }

                    var categories = new List<PackEntryCategory>(control.AllowedCategories);
                    categories.Remove(category);
                    control.AllowedCategories = categories;
                }
            }
        }

        /// <summary>
        /// Provide the specified definition with the items to show in the filter.
        /// </summary>
        /// <param name="definition">
        /// The definition that the created items will be used for.
        /// </param>
        /// <returns>
        /// The items to show in the filter.
        /// </returns>
        protected override ObservableCollection<IMultiCommandItem> GetItems(
            MultiCommand definition)
        {
            var items = new ObservableCollection<IMultiCommandItem>();
            var item1 = new FilterByCategoryItem(definition, PackEntryCategory.File, "Files");

            var item2 = new FilterByCategoryItem(
                definition, PackEntryCategory.Resource, "Resources");

            var item3 = new FilterByCategoryItem(
                definition, PackEntryCategory.Unknown, "Unknowns");

            item1.IsToggled = true;
            item2.IsToggled = true;

            item1.SetIcon(CommonIcons.BlankFile);
            item2.SetIcon(CommonIcons.Resource);
            item3.SetIcon(CommonIcons.UnknownFile);

            items.Add(item1);
            items.Add(item2);
            items.Add(item3);

            return items;
        }

        /// <summary>
        /// Determines whether the specified controls items source contains a item in it that
        /// uses the specified category.
        /// </summary>
        /// <param name="control">
        /// The control to test.
        /// </param>
        /// <param name="category">
        /// The category to locate.
        /// </param>
        /// <returns>
        /// True if this controls items source contains a item using the specified category;
        /// otherwise, false.
        /// </returns>
        private bool ContainsItems(RpfViewControl control, PackEntryCategory category)
        {
            if (control.ItemsSource == null)
            {
                return false;
            }

            foreach (object item in control.ItemsSource)
            {
                PackEntryViewModel viewModel = item as PackEntryViewModel;
                if (viewModel != null && viewModel.Category == category)
                {
                    return true;
                }
            }

            return false;
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// Defines the item used by the created command definition for this implementer.
        /// </summary>
        private class FilterByCategoryItem : MultiCommandItem<PackEntryCategory>
        {
            #region Fields
            /// <summary>
            /// The private field used for the <see cref="Icon"/> property.
            /// </summary>
            private BitmapSource _icon;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="FilterByCategoryItem"/> class.
            /// </summary>
            /// <param name="definition">
            /// The multi command definition that owns this item.
            /// </param>
            /// <param name="parameter">
            /// The parameter that is sent when this item is executed.
            /// </param>
            /// <param name="text">
            /// The text that is used to display this item.
            /// </param>
            public FilterByCategoryItem(
                MultiCommand definition,
                PackEntryCategory parameter,
                string text)
                : base(definition, text, parameter)
            {
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets the description for this item. This is also what is shown as a tooltip
            /// along with the key gesture if one is set if it is being rendered as a
            /// toggle control.
            /// </summary>
            public override string Description
            {
                get { return this.Text; }
            }

            /// <summary>
            /// Gets the System.Windows.Media.Imaging.BitmapSource object that represents the
            /// icon that is used for this item if it is being rendered as a toggle control.
            /// </summary>
            public override BitmapSource Icon
            {
                get { return this._icon; }
            }
            #endregion Properties

            #region Methods
            /// <summary>
            /// Sets the bitmap source used as the icon for this item.
            /// </summary>
            /// <param name="icon">
            /// The icon that should be used for this item.
            /// </param>
            internal void SetIcon(BitmapSource icon)
            {
                this._icon = icon;
            }
            #endregion Methods
        } // FilterByCategoryAction.FilterByCategoryItem {Class}
        #endregion Classes
    } // RSG.Rpf.View.Commands.FilterByCategoryAction {Class}
} // RSG.Rpf.View.Commands {Namespace}
