﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Rag.Widgets;

namespace RSG.Rag.ViewModel.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    public class ToggleUInt32ViewModel : ToggleViewModel<UInt32>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        public ToggleUInt32ViewModel(WidgetToggle<UInt32> widget)
            : base(widget)
        {
        }
        #endregion // Constructor(s)
    } // ToggleUInt32ViewModel
}
