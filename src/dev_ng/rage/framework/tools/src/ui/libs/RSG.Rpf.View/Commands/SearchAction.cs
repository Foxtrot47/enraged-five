﻿//---------------------------------------------------------------------------------------------
// <copyright file="SearchAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Rpf.View.Commands
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using System.Windows.Data;
    using RSG.Editor;
    using RSG.Editor.Controls;
    using RSG.Editor.SharedCommands;
    using RSG.Rpf.ViewModel;
    using Controls = System.Collections.Generic.IEnumerable<RpfViewControl>;

    /// <summary>
    /// Implements the Rockstar search commands.
    /// </summary>
    public class SearchAction : SearchAction<Controls>
    {
        #region Fields
        /// <summary>
        /// The current match count within the currently selected item.
        /// </summary>
        private static int _currentMatchCount;

        /// <summary>
        /// The current match index within the currently selected item.
        /// </summary>
        private static int _currentMatchIndex;

        /// <summary>
        /// The current regular expression.
        /// </summary>
        private static Regex _currentRegex;

        /// <summary>
        /// A value indicating whether the last search produced any results.
        /// </summary>
        private static bool _foundResults;

        /// <summary>
        /// The current result which is selected.
        /// </summary>
        private static PackEntryViewModel _searchResult;

        /// <summary>
        /// The previously selected item in the data grid.
        /// </summary>
        private static object _previousSelection;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SearchAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public SearchAction(ParameterResolverDelegate<Controls> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute handler for the
        /// <see cref="RockstarCommands.Find"/> command. By default this returns true.
        /// </summary>
        /// <param name="controls">
        /// The collection of controls resolved for this implementer.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        protected override bool CanSearch(Controls controls)
        {
            if (controls == null)
            {
                return false;
            }

            foreach (RpfViewControl control in controls)
            {
                foreach (object item in this.GetItemsSource(control))
                {
                    PackEntryViewModel viewModel = item as PackEntryViewModel;
                    if (viewModel != null)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Override to set logic against the execute command handler for the
        /// <see cref="RockstarCommands.Find"/> command when the search data indicates the
        /// search should be cleared.
        /// </summary>
        /// <param name="controls">
        /// The collection of controls resolved for this implementer.
        /// </param>
        protected override void ClearSearch(Controls controls)
        {
            _currentMatchCount = 0;
            _currentMatchIndex = 0;
            if (_searchResult != null)
            {
                _searchResult.SearchHighlightIndex = -1;
            }

            foreach (RpfViewControl control in controls)
            {
                this.SetSearchProperties(control, null, false);
            }
        }

        /// <summary>
        /// Override to set logic against the execute command handler for the
        /// <see cref="RockstarCommands.Find"/> command.
        /// </summary>
        /// <param name="controls">
        /// The collection of controls resolved for this implementer.
        /// </param>
        /// <param name="data">
        /// The search data that has been sent with the command.
        /// </param>
        /// <returns>
        /// A task that represents the work done by the search handler.
        /// </returns>
        protected override async Task<bool> Search(Controls controls, SearchData data)
        {
            bool result = false;
            data.ThrowIfCancellationRequested();
            result = await Task.Factory.StartNew(
                delegate
                {
                    if (data.Cleared)
                    {
                        this.ClearSearch(controls);
                        return true;
                    }

                    return this.SearchDelegate(controls, data);
                });

            return result;
        }

        /// <summary>
        /// Override to set logic against the execute command handler for the
        /// <see cref="RockstarCommands.FindNext"/> command.
        /// </summary>
        /// <param name="controls">
        /// The collection of controls resolved for this implementer.
        /// </param>
        /// <param name="data">
        /// The search data that has been sent with the command.
        /// </param>
        /// <returns>
        /// True if the next search result was navigated to successfully; otherwise, false.
        /// </returns>
        protected override bool SearchNext(Controls controls, SearchData data)
        {
            if (_foundResults == false)
            {
                return false;
            }

            return this.GoToResult(controls.First(), false);
        }

        /// <summary>
        /// Override to set logic against the execute command handler for the
        /// <see cref="RockstarCommands.FindPrevious"/> command.
        /// </summary>
        /// <param name="controls">
        /// The collection of controls resolved for this implementer.
        /// </param>
        /// <param name="data">
        /// The search data that has been sent with the command.
        /// </param>
        /// <returns>
        /// True if the next search result was navigated to successfully; otherwise, false.
        /// </returns>
        protected override bool SearchPrevious(Controls controls, SearchData data)
        {
            if (_foundResults == false)
            {
                return false;
            }

            return this.GoToResult(controls.First(), true);
        }

        /// <summary>
        /// Gets the list of items currently attached to the items source of the specified
        /// control. Making sure the logic is done on the correct thread.
        /// </summary>
        /// <param name="control">
        /// The control whose items need to be retrieved.
        /// </param>
        /// <returns>
        /// A list of items currently attached to the specified control.
        /// </returns>
        private List<object> GetItemsSource(RpfViewControl control)
        {
            if (!control.Dispatcher.CheckAccess())
            {
                return control.Dispatcher.Invoke<List<object>>(
                    new Func<List<object>>(
                        delegate
                        {
                            return this.GetItemsSource(control);
                        }));
            }

            if (control.ItemsSource == null)
            {
                return new List<object>();
            }

            return new List<object>(control.ItemsSource.Cast<object>());
        }

        /// <summary>
        /// Gets the number of search matches in the specified item for the specified regular
        /// expression search object.
        /// </summary>
        /// <param name="regex">
        /// The search object to test the specified item against.
        /// </param>
        /// <param name="item">
        /// The item to test.
        /// </param>
        /// <returns>
        /// The number of search matches in the specified item.
        /// </returns>
        private int GetMatchCount(Regex regex, object item)
        {
            if (regex == null || item == null)
            {
                return 0;
            }

            PackEntryViewModel viewModel = item as PackEntryViewModel;
            if (viewModel == null)
            {
                return 0;
            }

            return regex.Matches(viewModel.Name).Count;
        }

        /// <summary>
        /// Gets the items currently shown on the specified control in the order they need to
        /// be searched in.
        /// </summary>
        /// <param name="view">
        /// The control whose items will be retrieved.
        /// </param>
        /// <param name="reverse">
        /// A value indicating whether the items should be reversed before ordering them.
        /// </param>
        /// <param name="selectionFirst">
        /// A value indicating whether the selected item (if set) should be ordered as the
        /// first item to search or the last.
        /// </param>
        /// <returns>
        /// The items currently shown on the specified control in the order they need to
        /// be searched in.
        /// </returns>
        private List<object> OrderItems(RpfViewControl view, bool reverse, bool selectionFirst)
        {
            ICollectionView items = CollectionViewSource.GetDefaultView(view.DataGrid.Items);
            if (items == null)
            {
                return null;
            }

            IEnumerable<object> query = items.Cast<object>();
            if (reverse)
            {
                query = query.Reverse();
            }

            bool foundSelection = view.DataGrid.SelectedItem == null ? true : false;
            List<object> first = new List<object>();
            List<object> second = new List<object>();
            foreach (object item in query)
            {
                if (foundSelection)
                {
                    first.Add(item);
                    continue;
                }

                if (Object.ReferenceEquals(item, view.DataGrid.SelectedItem))
                {
                    foundSelection = true;
                }
                else
                {
                    second.Add(item);
                }
            }

            List<object> list = first.Concat(second).ToList();
            if (view.DataGrid.SelectedItem != null)
            {
                int insertIndex = selectionFirst ? 0 : list.Count;
                list.Insert(insertIndex, view.DataGrid.SelectedItem);
            }

            return list;
        }

        /// <summary>
        /// Goes to the next result in the specified control, either going forwards from the
        /// current selection or backwards.
        /// </summary>
        /// <param name="view">
        /// The control to navigator within.
        /// </param>
        /// <param name="reverse">
        /// A value indicating whether to navigator forwards to the next result or backwards.
        /// </param>
        /// <returns>
        /// True if the next result was found; otherwise, false.
        /// </returns>
        private bool GoToResult(RpfViewControl view, bool reverse)
        {
            if (_searchResult != null)
            {
                _searchResult.SearchHighlightIndex = -1;
            }

            bool selectionFirst = false;
            if (!Object.ReferenceEquals(view.DataGrid.SelectedItem, _previousSelection))
            {
                selectionFirst = !reverse;
                _currentMatchCount = 0;
                _currentMatchIndex = 0;
                if (_searchResult != null)
                {
                    _searchResult.SearchHighlightIndex = -1;
                }

                _searchResult = null;
                _previousSelection = null;
            }
            else
            {
                if (this.HandleMatchIndex(reverse))
                {
                    if (view.DataGrid.SelectedItems.Count > 1)
                    {
                        view.DataGrid.SelectedItems.Clear();
                        view.DataGrid.SelectedItem = _previousSelection;
                    }

                    return true;
                }
            }

            IEnumerable<object> items = this.OrderItems(view, reverse, selectionFirst);
            if (items == null)
            {
                return false;
            }

            foreach (object item in items)
            {
                int matchCount = this.GetMatchCount(_currentRegex, item);
                if (matchCount <= 0)
                {
                    continue;
                }

                int newMatchIndex = reverse == false ? 0 : matchCount - 1;
                _searchResult = item as PackEntryViewModel;
                _currentMatchCount = matchCount;
                _currentMatchIndex = newMatchIndex;
                if (_searchResult != null)
                {
                    _searchResult.SearchHighlightIndex = _currentMatchIndex;
                }

                view.DataGrid.SelectedItem = item;
                _previousSelection = item;
                view.DataGrid.ScrollIntoView(item);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Handles the match index within the currently selected item to see whether or not
        /// the next search result is still contained within said item.
        /// </summary>
        /// <param name="reverse">
        /// A value indicating whether we are looking for the next result by going backwards
        /// or forwards.
        /// </param>
        /// <returns>
        /// True if the navigation was handled; otherwise, false.
        /// </returns>
        private bool HandleMatchIndex(bool reverse)
        {
            if (_currentMatchCount <= 1)
            {
                return false;
            }

            if (reverse && _currentMatchIndex != 0)
            {
                _currentMatchIndex--;
                if (_searchResult != null)
                {
                    _searchResult.SearchHighlightIndex = _currentMatchIndex;
                }

                return true;
            }
            else if (!reverse && _currentMatchIndex != _currentMatchCount - 1)
            {
                _currentMatchIndex++;
                if (_searchResult != null)
                {
                    _searchResult.SearchHighlightIndex = _currentMatchIndex;
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// Performs the search described in the specified search data.
        /// </summary>
        /// <param name="controls">
        /// The collection of controls to search in.
        /// </param>
        /// <param name="data">
        /// The data that contains the search parameters.
        /// </param>
        /// <returns>
        /// A value indicating whether the search produced any results.
        /// </returns>
        private bool SearchDelegate(Controls controls, SearchData data)
        {
            _foundResults = false;
            data.ThrowIfCancellationRequested();
            if (!this.TryCreateSearchExpression(data, out _currentRegex))
            {
                return false;
            }

            data.ThrowIfCancellationRequested();
            foreach (RpfViewControl control in controls)
            {
                this.SetSearchProperties(control, _currentRegex, data.FilteringResults);
                data.ThrowIfCancellationRequested();
                if (_foundResults)
                {
                    continue;
                }

                foreach (object item in this.GetItemsSource(control))
                {
                    data.ThrowIfCancellationRequested();
                    if (this.GetMatchCount(_currentRegex, item) > 0)
                    {
                        _foundResults = true;
                        break;
                    }
                }
            }

            data.ThrowIfCancellationRequested();
            _previousSelection = null;
            _currentMatchCount = 0;
            _currentMatchIndex = 0;
            if (_searchResult != null)
            {
                _searchResult.SearchHighlightIndex = -1;
                _searchResult = null;
            }

            return _foundResults;
        }

        /// <summary>
        /// Sets the search properties on the specified control making sure that the logic is
        /// done on the correct thread.
        /// </summary>
        /// <param name="control">
        /// The instance of the control to set the properties on.
        /// </param>
        /// <param name="regex">
        /// The regular expression value to set on the specified control.
        /// </param>
        /// <param name="filter">
        /// The filter search items value to set on the specified control.
        /// </param>
        private void SetSearchProperties(RpfViewControl control, Regex regex, bool filter)
        {
            if (!control.Dispatcher.CheckAccess())
            {
                control.Dispatcher.BeginInvoke(
                    new Action(
                        delegate
                        {
                            this.SetSearchProperties(control, regex, filter);
                        }));

                return;
            }

            control.SetValue(RsHighlightTextBlock.HighlightExpressionProperty, regex);
            control.SearchExpression = regex;
            control.FilterSearchItems = filter;
        }

        /// <summary>
        /// Creates a regular expression object that is needed to perform the search defined
        /// in the specified data.
        /// </summary>
        /// <param name="data">
        /// The data to create the regular expression from.
        /// </param>
        /// <param name="expression">
        /// When this method returns contains the expression object needed to perform the
        /// search defined in the specified data.
        /// </param>
        /// <returns>
        /// True if the creation was successful; otherwise, false.
        /// </returns>
        private bool TryCreateSearchExpression(SearchData data, out Regex expression)
        {
            string pattern = data.SearchText;
            RegexOptions options = RegexOptions.None;
            if (!data.RegularExpressions)
            {
                pattern = Regex.Escape(pattern);
            }

            if (!data.MatchCase)
            {
                options |= RegexOptions.IgnoreCase;
            }

            if (data.MatchWord)
            {
                // Cannot use word boundary as the (.) isn't handled correctly.
                // pattern = String.Format(@"\b{0}\b", pattern);
                pattern = String.Format(@"^{0}$", pattern);
            }

            try
            {
                expression = new Regex(pattern, options);
            }
            catch (ArgumentException ex)
            {
                Debug.Assert(false, "Unable to create regular expression.", "{0}", ex.Message);
                data.RegisterParseError(ex.Message);
                expression = null;
                return false;
            }

            return true;
        }
        #endregion Methods
    }  // RSG.Rpf.View.Commands.SearchAction {Class}
} // RSG.Rpf.View.Commands {Namespace}
