﻿//---------------------------------------------------------------------------------------------
// <copyright file="MapItemKeyTemplateSelector.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.View
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using RSG.Editor.Controls;
    using RSG.Editor.Controls.Helpers;
    using RSG.Metadata.Model.Definitions.Members;
    using RSG.Metadata.Model.Tunables;
    using RSG.Metadata.ViewModel.Tunables;

    /// <summary>
    /// Represents the cell template that is associated with the type column in the tunable
    /// structure control.
    /// </summary>
    public class MapItemKeyTemplateSelector : DataTemplateSelector
    {
        #region Fields
        /// <summary>
        /// The private data template that's used by this selector for the normal tunable view
        /// model objects.
        /// </summary>
        private static DataTemplate _enumTemplate;

        /// <summary>
        /// The private data template that's used by this selector for the normal tunable view
        /// model objects.
        /// </summary>
        private static DataTemplate _numericalTemplate;

        /// <summary>
        /// The private data template that's used by this selector for the normal tunable view
        /// model objects.
        /// </summary>
        private static DataTemplate _stringTemplate;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="MapItemKeyTemplateSelector" /> class.
        /// </summary>
        static MapItemKeyTemplateSelector()
        {
            _enumTemplate = CreateEnumTemplate();
            _numericalTemplate = CreateNumericalTemplate();
            _stringTemplate = CreateStringTemplate();
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Returns the data template to use for the specified item inside the type column for
        /// the tunable structure control.
        /// </summary>
        /// <param name="item">
        /// The data content.
        /// </param>
        /// <param name="container">
        /// The element to which the template will be applied.
        /// </param>
        /// <returns>
        /// The data template to use for the specified item inside the type column for the
        /// tunable structure control.
        /// </returns>
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            MapKeyViewModel mapKeyViewModel = item as MapKeyViewModel;
            if (mapKeyViewModel == null || mapKeyViewModel.MapTunable == null)
            {
                return new DataTemplate();
            }

            MapMember mapMember = mapKeyViewModel.MapTunable.MapMember;
            switch (mapMember.Key)
            {
                case KeyType.AtHashString:
                case KeyType.AtHashValue:
                    return _stringTemplate;

                case KeyType.Signed32:
                case KeyType.Signed16:
                case KeyType.Signed8:
                case KeyType.Unsigned32:
                case KeyType.Unsigned16:
                case KeyType.Unsigned8:
                    return _numericalTemplate;

                case KeyType.Enum:
                    return _enumTemplate;

                default:
                    return new DataTemplate();
            }
        }

        /// <summary>
        /// Creates a new data template that normal tunable view models can use for the type
        /// column in the metadata tree structure.
        /// </summary>
        /// <returns>
        /// A new data template that normal tunable view models can use.
        /// </returns>
        private static DataTemplate CreateEnumTemplate()
        {
            FrameworkElementFactory factory = new FrameworkElementFactory(typeof(ComboBox));
            factory.SetValue(TextBox.MarginProperty, new Thickness(3.0, 2.0, 0.0, 0.0));
            factory.SetValue(TextBox.WidthProperty, 180.0d);

            Binding itemsSourceBinding = new Binding("Items");
            factory.SetBinding(ComboBox.ItemsSourceProperty, itemsSourceBinding);

            Binding valueBinding = new Binding("Value");
            valueBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            valueBinding.ValidatesOnNotifyDataErrors = true;
            valueBinding.Mode = BindingMode.TwoWay;
            factory.SetBinding(ComboBox.SelectedItemProperty, valueBinding);

            DataTemplate itemTemplate = new DataTemplate();
            itemTemplate.VisualTree = new FrameworkElementFactory(typeof(TextBlock));
            itemTemplate.VisualTree.SetBinding(TextBlock.TextProperty, new Binding("Name"));

            factory.SetValue(ComboBox.ItemTemplateProperty, itemTemplate);
            DataTemplate template = new DataTemplate();
            template.VisualTree = factory;
            return template;
        }

        /// <summary>
        /// Creates a new data template that normal tunable view models can use for the type
        /// column in the metadata tree structure.
        /// </summary>
        /// <returns>
        /// A new data template that normal tunable view models can use.
        /// </returns>
        private static DataTemplate CreateNumericalTemplate()
        {
            FrameworkElementFactory factory =
                new FrameworkElementFactory(typeof(RsIntegerSpinner));
            factory.SetValue(TextBox.MarginProperty, new Thickness(3.0, 2.0, 0.0, 0.0));
            factory.SetValue(TextBox.WidthProperty, 180.0d);

            factory.SetBinding(RsIntegerSpinner.MaximumProperty, new Binding("Maximum"));
            factory.SetBinding(RsIntegerSpinner.MinimumProperty, new Binding("Minimum"));

            Binding valueBinding = new Binding("Value");
            valueBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            valueBinding.ValidatesOnNotifyDataErrors = true;
            valueBinding.Mode = BindingMode.TwoWay;
            factory.SetBinding(RsIntegerSpinner.ValueProperty, valueBinding);

            DataTemplate template = new DataTemplate();
            template.VisualTree = factory;
            return template;
        }

        /// <summary>
        /// Creates a new data template that normal tunable view models can use for the type
        /// column in the metadata tree structure.
        /// </summary>
        /// <returns>
        /// A new data template that normal tunable view models can use.
        /// </returns>
        private static DataTemplate CreateStringTemplate()
        {
            FrameworkElementFactory factory = new FrameworkElementFactory(typeof(TextBox));
            factory.SetValue(TextBox.MarginProperty, new Thickness(3.0, 2.0, 0.0, 0.0));
            factory.SetValue(TextBox.WidthProperty, 180.0d);

            Binding textBinding = new Binding("Value");
            textBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            textBinding.ValidatesOnNotifyDataErrors = true;
            textBinding.Mode = BindingMode.TwoWay;
            factory.SetBinding(TextBox.TextProperty, textBinding);

            DataTemplate template = new DataTemplate();
            template.VisualTree = factory;
            return template;
        }
        #endregion Methods
    } // RSG.Metadata.View.MapItemKeyTemplateSelector {Class}
} // RSG.Metadata.View {Namespace}
