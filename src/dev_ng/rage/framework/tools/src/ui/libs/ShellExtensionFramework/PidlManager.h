#ifndef SEF_PIDLMANAGER_H
#define SEF_PIDLMANAGER_H

#if defined _MSC_VER && _MSC_VER >= 1300
#pragma once
#endif

#include "ShellExtensionFramework.h"
#include "ShellAlloctor.h"
#include "Pidl.h"

// Windows headers
#include "Windows.h"
#include "ShTypes.h"
#include "atlstr.h"

BEGIN_SHELL_EXTENSION_FRAMEWORK_NS

/**
	\brief Windows Shell Extension PIDL Manager implementation.
 */
class CPidlManager
{
public:
	CPidlManager()
	{
		HRESULT hr = SHGetMalloc(&m_MallocPtr);
		ATLASSERT(SUCCEEDED(hr));
	}

	LPITEMIDLIST Create( CPidl& Data)
	{
		// Total size of the PIDL, including SHITEMID
		UINT TotalSize = sizeof(ITEMIDLIST) + Data.GetSize();

		// Also allocate memory for the final null SHITEMID.
		LPITEMIDLIST pidlNew = (LPITEMIDLIST) m_MallocPtr->Alloc(TotalSize + sizeof(ITEMIDLIST));
		if (pidlNew)
		{
			LPITEMIDLIST pidlTemp = pidlNew;

			// Prepares the PIDL to be filled with actual data
			pidlTemp->mkid.cb = TotalSize;

			// Fill the PIDL
			Data.CopyTo((void*)pidlTemp->mkid.abID);

			// Set an empty PIDL at the end
			pidlTemp = GetNextItem(pidlTemp);
			pidlTemp->mkid.cb = 0;
			pidlTemp->mkid.abID[0] = 0;
		}

		return pidlNew;
	}

	void Delete(LPITEMIDLIST pidl)
	{
		if (pidl)
			m_MallocPtr->Free(pidl);
	}

	LPITEMIDLIST GetNextItem(LPCITEMIDLIST pidl)
	{
		ATLASSERT(pidl != NULL);
		if (!pidl)
			return NULL;

		return LPITEMIDLIST(LPBYTE(pidl) + pidl->mkid.cb);
	}

	LPITEMIDLIST GetLastItem(LPCITEMIDLIST pidl)
	{
		LPITEMIDLIST pidlLast = NULL;

		//get the PIDL of the last item in the list
		while (pidl && pidl->mkid.cb)
		{
			pidlLast = (LPITEMIDLIST)pidl;
			pidl = GetNextItem(pidl);
		}

		return pidlLast;
	}

	LPITEMIDLIST Copy(LPCITEMIDLIST pidlSrc)
	{
		LPITEMIDLIST pidlTarget = NULL;
		UINT Size = 0;

		if (pidlSrc == NULL)
			return NULL;

		// Allocate memory for the new PIDL.
		Size = GetSize(pidlSrc);
		pidlTarget = (LPITEMIDLIST) m_MallocPtr->Alloc(Size);

		if (pidlTarget == NULL)
			return NULL;

		// Copy the source PIDL to the target PIDL.
		CopyMemory(pidlTarget, pidlSrc, Size);

		return pidlTarget;
	}

	UINT GetSize(LPCITEMIDLIST pidl)
	{
		UINT Size = 0;
		LPITEMIDLIST pidlTemp = (LPITEMIDLIST) pidl;

		ATLASSERT(pidl != NULL);
		if (!pidl)
			return 0;

		while (pidlTemp->mkid.cb != 0)
		{
			Size += pidlTemp->mkid.cb;
			pidlTemp = GetNextItem(pidlTemp);
		}  

		// add the size of the NULL terminating ITEMIDLIST
		Size += sizeof(ITEMIDLIST);

		return Size;
	}

	bool IsSingle(LPCITEMIDLIST pidl)
	{
		LPITEMIDLIST pidlTemp = GetNextItem(pidl);
		return pidlTemp->mkid.cb == 0;
	}

	CString StrRetToCString(STRRET *pStrRet, LPCITEMIDLIST pidl)
	{
		int Length = 0;
		bool Unicode = false;
		LPCSTR StringA = NULL;
		LPCWSTR StringW = NULL;

		switch (pStrRet->uType)
		{
		case STRRET_CSTR:
			StringA = pStrRet->cStr;
			Unicode = false;
			Length = strlen(StringA);
			break;

		case STRRET_OFFSET:
			StringA = (char*)pidl+pStrRet->uOffset;
			Unicode = false;
			Length = strlen(StringA);
			break;

		case STRRET_WSTR:
			StringW = pStrRet->pOleStr;
			Unicode = true;
			Length = wcslen(StringW);
			break;
		}

		if (Length==0)
			return CString();

		CString Target;
		LPTSTR pTarget = Target.GetBuffer(Length);
		if (Unicode)
		{
#ifdef _UNICODE
			wcscpy(pTarget, StringW);
#else
			wcstombs(pTarget, StringW, Length+1);
#endif
		}
		else
		{
#ifdef _UNICODE
			mbstowcs(pTarget, StringA, Length+1);
#else
			strcpy(pTarget, StringA);
#endif
		}
		Target.ReleaseBuffer();

		// Release the OLESTR
		if (pStrRet->uType == STRRET_WSTR)
		{
			m_MallocPtr->Free(pStrRet->pOleStr);
		}

		return Target;
	}

protected:
	CComPtr<IMalloc> m_MallocPtr;

};

END_SHELL_EXTENSION_FRAMEWORK_NS

#endif // SEF_PIDLMANAGER_H