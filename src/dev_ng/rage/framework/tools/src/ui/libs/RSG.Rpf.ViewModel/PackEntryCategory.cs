﻿//---------------------------------------------------------------------------------------------
// <copyright file="PackEntryCategory.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Rpf.ViewModel
{
    /// <summary>
    /// Defines the different categories a pack entry can have.
    /// </summary>
    public enum PackEntryCategory
    {
        /// <summary>
        /// Specifies that the category is unknown.
        /// </summary>
        Unknown,

        /// <summary>
        /// Specifies that the pack entry is representing a directory.
        /// </summary>
        Directory,

        /// <summary>
        /// Specifies that the pack entry is representing a file.
        /// </summary>
        File,

        /// <summary>
        /// Specifies that the pack entry is representing a resource type.
        /// </summary>
        Resource,
    } // RSG.Rpf.ViewModel.PackEntryCategory {Enum}
} // RSG.Rpf.ViewModel {Namespace}
