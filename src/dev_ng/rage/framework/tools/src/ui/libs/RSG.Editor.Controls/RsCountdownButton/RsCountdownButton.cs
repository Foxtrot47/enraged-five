﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsCountdownButton.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Threading;
    using RSG.Base.Extensions;

    /// <summary>
    /// Represents a button control that will automatically fire the click event once a certain
    /// about of time has past if the user doesn't click it before.
    /// </summary>
    public class RsCountdownButton : Button
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="Active"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ActiveProperty;

        /// <summary>
        /// Identifies the <see cref="CountdownLabel"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty CountdownLabelProperty;

        /// <summary>
        /// Identifies the <see cref="Duration"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty DurationProperty;

        /// <summary>
        /// Identifies the <see cref="TickFrequency"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty TickFrequencyProperty;

        /// <summary>
        /// Identifies the <see cref="CountdownLabel"/> read-only dependency property.
        /// </summary>
        private static readonly DependencyPropertyKey _countdownLabelKey;

        /// <summary>
        /// Amount of timer that has elapsed since the timer started.
        /// </summary>
        private int _elapsedDuration = 0;

        /// <summary>
        /// Timer for the countdown.
        /// </summary>
        private DispatcherTimer _timer;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsCountdownButton"/> class.
        /// </summary>
        static RsCountdownButton()
        {
            DurationProperty =
                DependencyProperty.Register(
                "Duration",
                typeof(int),
                typeof(RsCountdownButton),
                new FrameworkPropertyMetadata(10000));

            TickFrequencyProperty =
                DependencyProperty.Register(
                "TickFrequency",
                typeof(int),
                typeof(RsCountdownButton),
                new FrameworkPropertyMetadata(1000, OnTickFrequencyChanged));

            ActiveProperty =
                DependencyProperty.Register(
                "Active",
                typeof(bool),
                typeof(RsCountdownButton),
                new FrameworkPropertyMetadata(true, OnActiveChanged));

            _countdownLabelKey =
                DependencyProperty.RegisterReadOnly(
                "CountdownLabel",
                typeof(String),
                typeof(RsCountdownButton),
                new FrameworkPropertyMetadata("(1)"));

            CountdownLabelProperty = _countdownLabelKey.DependencyProperty;

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsCountdownButton),
                new FrameworkPropertyMetadata(typeof(RsCountdownButton)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsCountdownButton"/> class.
        /// </summary>
        public RsCountdownButton()
        {
            this._timer = new DispatcherTimer();
            this.IsEnabledChanged += this.OnIsEnabledChanged;
            this.Unloaded += this.OnUnloaded;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether the countdown timer is currently active.
        /// </summary>
        public bool Active
        {
            get { return (bool)this.GetValue(ActiveProperty); }
            set { this.SetValue(ActiveProperty, value); }
        }

        /// <summary>
        /// Gets the label to show next to the content.
        /// </summary>
        public String CountdownLabel
        {
            get { return (String)this.GetValue(CountdownLabelProperty); }
            private set { this.SetValue(_countdownLabelKey, value); }
        }

        /// <summary>
        /// Gets or sets the duration of the countdown prior to the click event being fired.
        /// </summary>
        public int Duration
        {
            get { return (int)this.GetValue(DurationProperty); }
            set { this.SetValue(DurationProperty, value); }
        }

        /// <summary>
        /// Gets or sets the value for how often the dispatch timer should tick.
        /// </summary>
        public int TickFrequency
        {
            get { return (int)this.GetValue(TickFrequencyProperty); }
            set { this.SetValue(TickFrequencyProperty, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Start the timer once the control has been initialized.
        /// </summary>
        /// <param name="e">
        /// The event arguments. Always empty.
        /// </param>
        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
            this.UpdateCountdownLabel();

            // Start the timer.
            this._timer.Interval = TimeSpan.FromMilliseconds(this.TickFrequency);
            this._timer.Tick += this.OnTick;
            this._timer.IsEnabled = this.Active && this.IsEnabled;
            this._timer.Start();
        }

        /// <summary>
        /// Event handler for when the countdown active dependency property changes.
        /// </summary>
        /// <param name="d">
        /// The dependency object on which the dependency property has changed.
        /// </param>
        /// <param name="e">
        /// The event args containing the old and new values of the dependency property.
        /// </param>
        private static void OnActiveChanged(
            DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RsCountdownButton button = d as RsCountdownButton;
            if (button == null)
            {
                Debug.Assert(button != null, "Wrong type cast in property changed handler");
                return;
            }

            button._timer.IsEnabled = button.Active && button.IsEnabled;
        }

        /// <summary>
        /// Event handler for when the tick frequency dependency property changes.
        /// </summary>
        /// <param name="d">
        /// The dependency object on which the dependency property has changed.
        /// </param>
        /// <param name="e">
        /// The event args containing the old and new values of the dependency property.
        /// </param>
        private static void OnTickFrequencyChanged(
            DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RsCountdownButton button = d as RsCountdownButton;
            if (button == null)
            {
                Debug.Assert(button != null, "Wrong type cast in property changed handler");
                return;
            }

            button._timer.Interval = TimeSpan.FromMilliseconds(button.TickFrequency);
        }

        /// <summary>
        /// Event handler for listening to changes to the IsEnabled property.
        /// </summary>
        /// <param name="sender">
        /// The object whose IsEnabled property changed.
        /// </param>
        /// <param name="e">
        /// The event args containing the old and new values of the dependency property.
        /// </param>
        private void OnIsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            this._timer.IsEnabled = this.Active && this.IsEnabled;
        }

        /// <summary>
        /// Event handler for the dispatch timer's tick method.
        /// </summary>
        /// <param name="sender">
        /// The instance of the timer that has fired the Tick event.
        /// </param>
        /// <param name="e">
        /// The event arguments. Always empty.
        /// </param>
        private void OnTick(object sender, EventArgs e)
        {
            this._elapsedDuration += this.TickFrequency;
            this.UpdateCountdownLabel();

            // Has the duration we were waiting for elapsed?
            if (this._elapsedDuration >= this.Duration)
            {
                // It's elapsed so fire off the click event.
                this.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
                this._timer.Stop();
            }
        }

        /// <summary>
        /// Called when this control has been unloaded from the user interface. Used to stop
        /// the countdown timer.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs instance containing the event data.
        /// </param>
        private void OnUnloaded(object sender, RoutedEventArgs e)
        {
            this._timer.Stop();
        }

        /// <summary>
        /// Helper method for updating the countdown label.
        /// </summary>
        private void UpdateCountdownLabel()
        {
            int remaindingMilliseconds = this.Duration - this._elapsedDuration;
            TimeSpan milliseconds = TimeSpan.FromMilliseconds(remaindingMilliseconds);
            string label = milliseconds.TotalSeconds.ToStringInvariant();

            this.CountdownLabel = "({0})".FormatCurrent(label);
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsCountdownButton {Class}
} // RSG.Editor.Controls {Namespace}
