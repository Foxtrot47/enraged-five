﻿//---------------------------------------------------------------------------------------------
// <copyright file="ValidatingViewModelBase{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Threading.Tasks;
    using RSG.Editor.Model;

    /// <summary>
    /// Provides a abstract base class that can be inherited by classes that wish to implement
    /// the <see cref="RSG.Editor.View.IViewModel{T}"/> interface and support data validation.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the model that is being wrapped by this view model.
    /// </typeparam>
    public abstract class ValidatingViewModelBase<T>
        : ViewModelBase<T>, INotifyDataErrorInfo, IValidating where T : IModel
    {
        #region Fields
        /// <summary>
        /// A generic object used to lock the errors dictionary for thread safety.
        /// </summary>
        private object _errorLock;

        /// <summary>
        /// The private field used for the <see cref="CurrentValidation"/> property.
        /// </summary>
        private ValidationResult _currentValidation;

        /// <summary>
        /// The private count showing the number of times the validation for this object has
        /// been suspended.
        /// </summary>
        private int _suspendedStateCount;

        /// <summary>
        /// The private field used for the <see cref="ValidateOnPropertyChange"/> property.
        /// </summary>
        private bool _validateOnPropertyChange;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ValidatingViewModelBase{T}"/> class.
        /// </summary>
        /// <param name="model">
        /// The model that this view model will be wrapping.
        /// </param>
        /// <param name="forwardPropertyChanges">
        /// A value indicating whether the property changed events from the specified model are
        /// forwarded on through this object.
        /// </param>
        /// <param name="validateOnPropertyChange">
        /// A value indicating whether validation takes place asynchronously whenever a
        /// property's value changes.
        /// </param>
        protected ValidatingViewModelBase(
            T model, bool forwardPropertyChanges, bool validateOnPropertyChange)
            : base(model, forwardPropertyChanges)
        {
            this._errorLock = new object();
            this._currentValidation = ValidationResult.Empty;
            this._validateOnPropertyChange = validateOnPropertyChange;
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs when the validation errors have changed for a property or for the entire
        /// entity.
        /// </summary>
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;
        #endregion Events

        #region Properties
        /// <summary>
        /// Gets the current validation result container obtained from the previous validation.
        /// </summary>
        public ValidationResult CurrentValidation
        {
            get { return this._currentValidation; }
        }

        /// <summary>
        /// Gets the lock object that is used to access the current validation object.
        /// </summary>
        public object ErrorLock
        {
            get { return this._errorLock; }
        }

        /// <summary>
        /// Gets a value indicating whether the entity has validation errors.
        /// </summary>
        public virtual bool HasErrors
        {
            get
            {
                lock (this._errorLock)
                {
                    if (this._currentValidation == null)
                    {
                        return false;
                    }

                    return this._currentValidation.HasErrors;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the validation should takes place
        /// asynchronously whenever a property's value changes.
        /// </summary>
        public bool ValidateOnPropertyChange
        {
            get
            {
                return this._validateOnPropertyChange;
            }

            set
            {
                if (this._validateOnPropertyChange == value)
                {
                    return;
                }

                this._validateOnPropertyChange = value;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Gets the validation errors for a specified property or for the entire entity.
        /// </summary>
        /// <param name="propertyName">
        /// The name of the property to retrieve validation errors for; or null or Empty, to
        /// retrieve entity-level errors.
        /// </param>
        /// <returns>
        /// The validation errors for the property or entity.
        /// </returns>
        public virtual IEnumerable GetErrors(string propertyName)
        {
            lock (this._errorLock)
            {
                if (this._currentValidation == null)
                {
                    return null;
                }

                string resolvedPropertyName = this.ResolveGetErrorsPropertyName(propertyName);
                return this._currentValidation.GetErrors(resolvedPropertyName);
            }
        }

        /// <summary>
        /// Fires the errors changed event that the user interface is listening to through the
        /// binding engine.
        /// </summary>
        /// <param name="propertyName">
        /// The name of the property whose errors have changed.
        /// </param>
        public virtual void OnErrorsChanged(string propertyName)
        {
            EventHandler<DataErrorsChangedEventArgs> handler = this.ErrorsChanged;
            if (handler == null)
            {
                return;
            }

            handler(this, new DataErrorsChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Retrieves a disposable object that is used to create a region of code where the
        /// validation due to property changes has been suspended.
        /// </summary>
        /// <returns>
        /// A disposable object that started the suspension of the validation.
        /// </returns>
        public IDisposable SuspendValidation()
        {
            return new SuspendValidationScope<T>(this);
        }

        /// <summary>
        /// Validates this object using the INotifyDataErrorInfo interface.
        /// </summary>
        public void Validate()
        {
            this.Validate(null);
        }

        /// <summary>
        /// Validates this object asynchronously using the INotifyDataErrorInfo interface.
        /// </summary>
        /// <returns>
        /// A task that represents the work done by the validation logic.
        /// </returns>
        public Task ValidateAsync()
        {
            return this.ValidateAsync(null);
        }

        /// <summary>
        /// Runs the validation pipeline for this object as long as the property being changed
        /// in the model has been registered and the validation on property change property
        /// boolean is set to true.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.ComponentModel.PropertyChangedEventArgs containing the event data.
        /// </param>
        protected override void OnModelPropertyChanged(object s, PropertyChangedEventArgs e)
        {
            base.OnModelPropertyChanged(s, e);
            if (this._validateOnPropertyChange && this.ShouldRunValidation(e.PropertyName))
            {
                this.ValidateAsync(e.PropertyName);
            }
        }

        /// <summary>
        /// Called whenever the validation has finished running due to a change to the property
        /// with the specified name.
        /// </summary>
        /// <param name="propertyName">
        /// The name of the property that was changed to cause the validation to run.
        /// </param>
        /// <remarks>
        /// This doesn't get called if there wasn't a property name attached to the validation.
        /// </remarks>
        protected virtual void OnValidationRun(string propertyName)
        {
        }

        /// <summary>
        /// Override to resolve a property name whose error list has been requested by the
        /// validation system. Use this in the case where the bounded to property's name is
        /// different then the model property's name.
        /// </summary>
        /// <param name="propertyName">
        /// The original name of the property.
        /// </param>
        /// <returns>
        /// The resolved property name.
        /// </returns>
        protected virtual string ResolveGetErrorsPropertyName(string propertyName)
        {
            return propertyName;
        }

        /// <summary>
        /// Determines whether the validation pipeline should be started due to the property
        /// with the specified name changing.
        /// </summary>
        /// <param name="propertyName">
        /// The property name to test.
        /// </param>
        /// <returns>
        /// True if the validation should be run due to the specified property changing;
        /// otherwise, false.
        /// </returns>
        protected virtual bool ShouldRunValidation(string propertyName)
        {
            return false;
        }

        /// <summary>
        /// The core method used to validate this class. By default this calls the models
        /// validate methods and uses that result.
        /// </summary>
        /// <param name="propertyName">
        /// The name of the property if this validation is associated with a change to a
        /// specific property; otherwise, null.
        /// </param>
        /// <returns>
        /// The result of the validation.
        /// </returns>
        protected virtual ValidationResult ValidateCore(string propertyName)
        {
            if (this.Model == null)
            {
                return ValidationResult.Empty;
            }

            return this.Model.Validate(false);
        }

        /// <summary>
        /// Validates this object using the INotifyDataErrorInfo interface.
        /// </summary>
        /// <param name="propertyName">
        /// The name of the property if this validation is associated with a change to a
        /// specific property; otherwise, null.
        /// </param>
        private void Validate(string propertyName)
        {
            if (this._suspendedStateCount != 0)
            {
                return;
            }

            ValidationResult result = this.ValidateCore(propertyName);
            lock (this._errorLock)
            {
                ValidationResult old = this._currentValidation;
                this._currentValidation = result;

                HashSet<string> changedPropertyNames = new HashSet<string>();
                if (old != null)
                {
                    changedPropertyNames.UnionWith(old.PropertyNames);
                }

                changedPropertyNames.UnionWith(result.PropertyNames);
                foreach (string changedPropertyName in changedPropertyNames)
                {
                    this.OnErrorsChanged(changedPropertyName);
                }
            }

            if (propertyName != null)
            {
                this.OnValidationRun(propertyName);
            }
        }

        /// <summary>
        /// Validates this object asynchronously using the INotifyDataErrorInfo interface.
        /// </summary>
        /// <param name="propertyName">
        /// The name of the property if this validation is associated with a change to a
        /// specific property; otherwise, null.
        /// </param>
        /// <returns>
        /// A task that represents the work done by the validation logic.
        /// </returns>
        private Task ValidateAsync(string propertyName)
        {
            if (this._suspendedStateCount != 0)
            {
                return EmptyTask.CompletedTask;
            }

            return Task.Run(() => this.Validate(propertyName));
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// Provides functionality so that the validation based on property changes can be
        /// suspended for a specific view model.
        /// </summary>
        /// <typeparam name="J">
        /// The type of the model that the suspended view model is wrapping.
        /// </typeparam>
        private class SuspendValidationScope<J> : DisposableObject where J : IModel
        {
            #region Fields
            /// <summary>
            /// Gets the view model instance that this suspension was created for.
            /// </summary>
            private ValidatingViewModelBase<J> _viewModel;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="SuspendValidation"/> class.
            /// </summary>
            /// <param name="viewModel">
            /// A instance of the view model whose validation will be suspended.
            /// </param>
            public SuspendValidationScope(ValidatingViewModelBase<J> viewModel)
            {
                this._viewModel = viewModel;
                if (this._viewModel != null)
                {
                    this._viewModel._suspendedStateCount++;
                }
            }
            #endregion Constructors

            #region Methods
            /// <summary>
            /// Finishes the suspended state that this object represents on the associated
            /// view model.
            /// </summary>
            protected override void DisposeManagedResources()
            {
                if (this._viewModel == null)
                {
                    return;
                }

                this._viewModel._suspendedStateCount--;
            }
            #endregion Methods
        } // ValidatingViewModelBase{T}.SuspendValidationScope{J} {Class}
        #endregion Classes
    } // RSG.Editor.View.ValidatingViewModelBase{T} {Class}
} // RSG.Editor.View {Namespace}
