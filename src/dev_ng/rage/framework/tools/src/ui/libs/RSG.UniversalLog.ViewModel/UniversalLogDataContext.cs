﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Xml.Linq;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Editor;

namespace RSG.UniversalLog.ViewModel
{
    /// <summary>
    /// Data context for the universal log view control which wraps the logic for having multiple
    /// universal log files open simultaneously.
    /// </summary>
    public class UniversalLogDataContext : NotifyPropertyChangedBase
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MainWindowDataContext"/> class.
        /// </summary>
        public UniversalLogDataContext()
        {
            _loadedFilesViewModels = new ObservableCollection<UniversalLogFileViewModel>();
            _allMessages = new List<UniversalLogMessageViewModel>();

            // Initialise the list of log level counts.
            _logLevelCounts = new List<LogLevelCountViewModel>();
            foreach (LogLevel level in Enum.GetValues(typeof(LogLevel)))
            {
                BrowsableAttribute att = level.GetAttributeOfType<BrowsableAttribute>();
                if (att == null || att.Browsable == true)
                {
                    _logLevelCounts.Add(new LogLevelCountViewModel(level));
                }
            }
        }
        #endregion // Constructors

        #region Events
        /// <summary>
        /// Occurs after an attempt has been made to load one or more files.
        /// </summary>
        public event EventHandler<UlogLoadedEventArgs> LoadedFiles;

        /// <summary>
        /// Occurs after the currently loaded file(s) has been unloaded.
        /// </summary>
        public event EventHandler UnloadedFiles;
        #endregion Events

        #region Properties and associated member data
        /// <summary>
        /// List of all the loaded universal log file view models.
        /// </summary>
        public ObservableCollection<UniversalLogFileViewModel> LoadedFileViewModels
        {
            get { return _loadedFilesViewModels; }
        }
        private ObservableCollection<UniversalLogFileViewModel> _loadedFilesViewModels;

        /// <summary>
        /// Flag which is used to determine if any files are currently open.
        /// </summary>
        public bool AnyFilesLoaded
        {
            get { return _loadedFilesViewModels.Any(); }
        }

        /// <summary>
        /// Create a separate collection for all the message view models.
        /// This is required because CompositeCollections don't support sorting >:[.
        /// </summary>
        public List<UniversalLogMessageViewModel> AllMessages
        {
            get { return _allMessages; }
            private set
            {
                if (SetProperty(ref _allMessages, value))
                {
                    UpdateTotalCounts();
                }
            }
        }
        private List<UniversalLogMessageViewModel> _allMessages;

        /// <summary>
        /// List of items that have been selected in the view.
        /// </summary>
        public IEnumerable<UniversalLogMessageViewModel> SelectedItems
        {
            get { return this._selectedItems; }
            set { this._selectedItems = value; }
        }
        private IEnumerable<UniversalLogMessageViewModel> _selectedItems;

        /// <summary>
        /// 
        /// </summary>
        public IList<LogLevelCountViewModel> LogLevelCounts
        {
            get { return _logLevelCounts; }
        }
        private IList<LogLevelCountViewModel> _logLevelCounts;
        #endregion // Properties and associated member data

        #region Methods
        /// <summary>
        /// Loads the file located at the specified file path.
        /// </summary>
        /// <param name="filepaths">List of files to open.</param>
        /// <param name="merge">Flag indicating whether we wish to view the contents
        /// of the file merged with the files that are already open.</param>
        public void OpenFiles(IEnumerable<String> filepaths, bool merge)
        {
            // Check whether we need to clear out the files that are currently open.
            List<UniversalLogMessageViewModel> newMessages = new List<UniversalLogMessageViewModel>();

            if (!merge)
            {
                _loadedFilesViewModels.Clear();
            }
            else
            {
                newMessages.AddRange(_allMessages);
            }

            // Make sure we only load the files once.
            IList<String> filesToLoad =
                filepaths.Except(_loadedFilesViewModels.Select(item => item.Filepath)).ToList();

            // Load all the required files.
            foreach (String filepath in filesToLoad)
            {
                UniversalLogFileViewModel vm = new UniversalLogFileViewModel(filepath);
                vm.OpenFile();
                _loadedFilesViewModels.Add(vm);
                newMessages.AddRange(vm.Messages);
            }

            AllMessages = newMessages;

            // Inform the UI of any changes to the loaded files.
            if (LoadedFiles != null)
            {
                LoadedFiles(this, new UlogLoadedEventArgs(filesToLoad));
            }

            NotifyPropertyChanged("AnyFilesLoaded");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public void RefreshFiles()
        {
            // Clear the list of all messages and readd them after the files have been read from disk again.
            List<UniversalLogMessageViewModel> newMessages = new List<UniversalLogMessageViewModel>();
            foreach (UniversalLogFileViewModel vm in LoadedFileViewModels)
            {
                vm.RefreshFile();
                newMessages.AddRange(vm.Messages);
            }
            AllMessages = newMessages;
        }

        /// <summary>
        /// Saves the loaded files out into a single merged file.
        /// </summary>
        /// <returns></returns>
        public bool SaveFile(String filepath)
        {
            // Get the context based list of buffered messages.
            IDictionary<String, IList<UniversalLogFileBufferedMessage>> messages =
                new Dictionary<String, IList<UniversalLogFileBufferedMessage>>();

            foreach (UniversalLogMessageViewModel messageVm in AllMessages)
            {
                // If the system context is the default one then replace it with it's filename (minus any file extensions).
                String systemContext = messageVm.SystemContext;
                if (systemContext == UniversalLogFile.DEFAULT_CONTEXT)
                {
                    systemContext = Path.GetFileNameWithoutExtension(messageVm.FilePath);
                }

                // Add the buffered message to the appropriate context.
                if (!messages.ContainsKey(systemContext))
                {
                    messages[systemContext] = new List<UniversalLogFileBufferedMessage>();
                }
                messages[systemContext].Add(messageVm.BufferedMessage);
            }

            // Try and save the file out.
            try
            {
                XDocument xDoc = UniversalLogFile.TransformToXDocument(messages);
                xDoc.Save(filepath as String);
                return true;
            }
            catch (System.Exception)
            {
            	return false;
            }
        }

        /// <summary>
        /// Closes all opened files.
        /// </summary>
        public void CloseAllFiles()
        {
            _loadedFilesViewModels.Clear();
            _allMessages.Clear();
            if (UnloadedFiles != null)
            {
                UnloadedFiles(this, EventArgs.Empty);
            }
            NotifyPropertyChanged("AnyFilesLoaded");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public bool ContainsItemsWithLogLevel(LogLevel level)
        {
            return AllMessages.Any(item => item.Level == level);
        }

        /// <summary>
        /// 
        /// </summary>
        private void UpdateTotalCounts()
        {
            foreach (LogLevelCountViewModel countVm in _logLevelCounts)
            {
                int sourceCount = 0;
                foreach (UniversalLogMessageViewModel messageVm in AllMessages)
                {
                    if (messageVm.Level == countVm.Level ||
                        (messageVm.Level == LogLevel.ToolError && countVm.Level == LogLevel.Error) ||
                        (messageVm.Level == LogLevel.ProfileEnd && countVm.Level == LogLevel.Profile))
                    {
                        sourceCount++;
                    }
                }

                countVm.TotalCount = sourceCount;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void UpdateVisibleCounts(IEnumerable vms)
        {
            foreach (LogLevelCountViewModel countVm in _logLevelCounts)
            {
                int visibleCount = 0;
                foreach (UniversalLogMessageViewModel messageVm in vms.OfType<UniversalLogMessageViewModel>())
                {
                    if (messageVm.Level == countVm.Level ||
                        (messageVm.Level == LogLevel.ToolError && countVm.Level == LogLevel.Error) ||
                        (messageVm.Level == LogLevel.ProfileEnd && countVm.Level == LogLevel.Profile))
                    {
                        visibleCount++;
                    }
                }

                countVm.VisibleCount = visibleCount;
            }
        }
        #endregion // Methods
    } // UniversalLogDataContext
}
