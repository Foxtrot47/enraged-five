﻿//---------------------------------------------------------------------------------------------
// <copyright file="AddNewMetadataItemDefinition.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Commands
{
    /// <summary>
    /// The command definition for the dynamic add new item menu that lists the valid items
    /// that can be added to the active item. This class cannot be inherited.
    /// </summary>
    public sealed class AddNewMetadataItemDefinition : NewMetadataItemDefinitionBase
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="AddNewMetadataItemDefinition"/> class.
        /// </summary>
        public AddNewMetadataItemDefinition()
            : base(MetadataCommands.AddNewMetadataItem)
        {
        }
        #endregion Constructors
    } // RSG.Metadata.Commands.AddNewMetadataItemDefinition {Class}
} // RSG.Metadata.Commands {Namespace}
