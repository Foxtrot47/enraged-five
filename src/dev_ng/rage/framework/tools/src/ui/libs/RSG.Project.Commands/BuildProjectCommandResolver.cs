﻿//---------------------------------------------------------------------------------------------
// <copyright file="BuildProjectCommandResolver.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Commands
{
    using RSG.Editor;

    /// <summary>
    /// Represents the method that is used to retrieve a build project command parameter object
    /// to use with the project build commands.
    /// </summary>
    /// <param name="cd">
    /// The data that was sent with the command containing among other things the command
    /// whose command parameter needs to be resolved.
    /// </param>
    /// <returns>
    /// The build project command parameter object.
    /// </returns>
    public delegate BuildProjectCommandArgs BuildProjectCommandResolver(CommandData cd);
} // RSG.Project.Commands {Namespace}
