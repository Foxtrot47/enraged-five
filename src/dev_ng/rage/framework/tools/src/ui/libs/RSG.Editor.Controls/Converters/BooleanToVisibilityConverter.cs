﻿//---------------------------------------------------------------------------------------------
// <copyright file="BooleanToVisibilityConverter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Converters
{
    using System.Globalization;
    using System.Windows;

    /// <summary>
    /// Converts a boolean value into a System.Windows.Visibility enum value.
    /// </summary>
    public class BooleanToVisibilityConverter : ValueConverter<bool, Visibility>
    {
        #region Methods
        /// <summary>
        /// Converters the given value and returns the result.
        /// </summary>
        /// <param name="value">
        /// The original value to convert.
        /// </param>
        /// <param name="param">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A converted value.
        /// </returns>
        protected override Visibility Convert(bool value, object param, CultureInfo culture)
        {
            Visibility parameter = Visibility.Collapsed;
            if (param is Visibility)
            {
                parameter = (Visibility)param;
            }

            return value == false ? parameter : Visibility.Visible;
        }

        /// <summary>
        /// Converts a already converted value back to its original value and returns the
        /// result.
        /// </summary>
        /// <param name="value">
        /// The target value to convert back to its source value.
        /// </param>
        /// <param name="param">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// The source value for the already converted specified value.
        /// </returns>
        protected override bool ConvertBack(
            Visibility value, object param, CultureInfo culture)
        {
            return value == Visibility.Visible ? true : false;
        }
        #endregion Methods
    } // IntegerToVisibilityConverter.BooleanToVisibilityConverter {Class}
} // RSG.Editor.Controls.Converters {Namespace}
