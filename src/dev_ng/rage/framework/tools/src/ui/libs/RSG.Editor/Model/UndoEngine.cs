﻿//---------------------------------------------------------------------------------------------
// <copyright file="UndoEngine.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Model
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;

    /// <summary>
    /// Provides undo / redo functionality to the Editor Framework. A instance of this class
    /// can be past to the <see cref="RSG.Editor.Model.ModelBase"/> class to undo / redo
    /// property changes.
    /// </summary>
    public class UndoEngine
    {
        #region Fields
        /// <summary>
        /// A reference to the undo event that is being used to batch events into the same
        /// command event.
        /// </summary>
        private UndoEvent _currentEvent;

        /// <summary>
        /// A count of the number of items this undo engine has been put into a suspended state
        /// in a row.
        /// </summary>
        private int _dirtySuspendedStateCount;

        /// <summary>
        /// A count of the number of items this undo engine has been put into a suspended state
        /// in a row.
        /// </summary>
        private int _eventSuspendedStateCount;

        /// <summary>
        /// The private field used for the <see cref="PerformingEvent"/> property.
        /// </summary>
        private bool _performingEvent;

        /// <summary>
        /// The count specifying the redo offset since the engine was clean.
        /// </summary>
        private int _redoDirtyCount;

        /// <summary>
        /// The private stack containing all of the undo events that have been executed and
        /// can be redone.
        /// </summary>
        private Stack<UndoEvent> _redoStack;

        /// <summary>
        /// A count of the number of times the <see cref="StartBatch"/> method is called
        /// against the number of times the <see cref="FinishBatch"/> method is called. This
        /// supports hierarchical calls to the <see cref="StartBatch"/> and
        /// <see cref="FinishBatch"/> methods.
        /// </summary>
        private int _startCommandCount;

        /// <summary>
        /// The count specifying the undo offset since the engine was clean.
        /// </summary>
        private int _undoDirtyCount;

        /// <summary>
        /// The private stack that contains all of the undo events that have occurred
        /// that are being monitored by this engine.
        /// </summary>
        private Stack<UndoEvent> _undoStack;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="UndoEngine"/> class.
        /// </summary>
        public UndoEngine()
        {
            this._undoStack = new Stack<UndoEvent>();
            this._redoStack = new Stack<UndoEvent>();
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs whenever the dirty state that is being tracked by this undo engine has been
        /// changed.
        /// </summary>
        public event EventHandler DirtyStateChanged;
        #endregion Events

        #region Properties
        /// <summary>
        /// Gets a value indicating whether there are any events present that can be redone.
        /// </summary>
        public bool CanRedo
        {
            get
            {
                if (this._redoStack != null && this._redoStack.Count > 0)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether there are any undo events present that
        /// can be undone.
        /// </summary>
        public bool CanUndo
        {
            get
            {
                if (this._undoStack != null && this._undoStack.Count > 0)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this undo engine is currently dirty or not.
        /// </summary>
        public bool DirtyState
        {
            get
            {
                if (this._redoDirtyCount == 0 && this._undoDirtyCount == 0)
                {
                    return false;
                }

                return true;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the engine is currently busy performing
        /// a undo or redo event.
        /// </summary>
        public bool PerformingEvent
        {
            get { return this._performingEvent; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Pushes a clean dirty state value into the undo engine. This is used when the
        /// underlying data has been saved.
        /// </summary>
        public void PushCleanDirtyState()
        {
            bool dirtyState = this.DirtyState;
            this._undoDirtyCount = 0;
            this._redoDirtyCount = 0;
            if (dirtyState != this.DirtyState)
            {
                this.FireDirtyStateChangedEvent();
            }
        }

        /// <summary>
        /// Redoes the action that was last undone by the engine, by popping from the
        /// redo stack and pushing it to the undo stack.
        /// </summary>
        public void Redo()
        {
            if (!this.CanRedo || this._redoStack == null || this._undoStack == null)
            {
                return;
            }

            UndoEvent redoEvent = this._redoStack.Pop();
            bool dirtyState = this.DirtyState;
            if (redoEvent == null)
            {
                return;
            }

            this._undoStack.Push(redoEvent);
            this._undoDirtyCount++;
            this._redoDirtyCount--;
            try
            {
                this._performingEvent = true;
                redoEvent.Redo();
            }
            catch (Exception ex)
            {
                throw new RedoException(ex);
            }
            finally
            {
                this._performingEvent = false;
                if (dirtyState != this.DirtyState)
                {
                    this.FireDirtyStateChangedEvent();
                }
            }
        }

        /// <summary>
        /// Clears all of the stacks within this undo engine so no undo or redo event are
        /// available and the dirty state is set to false.
        /// </summary>
        public void ResetAllStacks()
        {
            this._undoStack.Clear();
            this._redoStack.Clear();
            this._undoDirtyCount = 0;
            this._redoDirtyCount = 0;
        }

        /// <summary>
        /// Undoes the last added undo event by popping it from the stack and pushing it
        /// to the redo stack.
        /// </summary>
        public void Undo()
        {
            if (!this.CanUndo || this._redoStack == null || this._undoStack == null)
            {
                return;
            }

            UndoEvent undoEvent = this._undoStack.Pop();
            bool dirtyState = this.DirtyState;
            if (undoEvent == null)
            {
                return;
            }

            this._redoStack.Push(undoEvent);
            this._undoDirtyCount--;
            this._redoDirtyCount++;
            try
            {
                this._performingEvent = true;
                undoEvent.Undo();
            }
            catch (Exception ex)
            {
                throw new UndoException(ex);
            }
            finally
            {
                this._performingEvent = false;
                if (dirtyState != this.DirtyState)
                {
                    this.FireDirtyStateChangedEvent();
                }
            }
        }

        /// <summary>
        /// Creates a new undo event and adds it to the undo stack.
        /// </summary>
        /// <param name="change">
        /// The <see cref="RSG.Editor.Model.IChange"/> the describes the undo event.
        /// </param>
        internal void CreateUndoEvent(IChange change)
        {
            bool dirtyState = this.DirtyState;
            if (this._eventSuspendedStateCount == 0)
            {
                if (this._startCommandCount > 0)
                {
                    if (this._currentEvent == null)
                    {
                        this._currentEvent = new UndoEvent();
                    }

                    this._currentEvent.AddChange(change);
                    return;
                }

                UndoEvent undoEvent = new UndoEvent();
                undoEvent.AddChange(change);
                this._undoStack.Push(undoEvent);
                this._redoStack.Clear();
            }

            if (this._dirtySuspendedStateCount == 0)
            {
                this._undoDirtyCount++;
                this._redoDirtyCount--;
            }

            if (dirtyState != this.DirtyState)
            {
                this.FireDirtyStateChangedEvent();
            }
        }

        /// <summary>
        /// Finishes the current command.
        /// </summary>
        internal void FinishBatch()
        {
            this._startCommandCount--;
            if (this._startCommandCount != 0 || this._currentEvent.ChangeCount == 0)
            {
                return;
            }

            if (this._eventSuspendedStateCount > 0)
            {
                this._currentEvent = null;
                return;
            }

            this._undoStack.Push(this._currentEvent);
            bool dirtyState = this.DirtyState;
            this._undoDirtyCount++;
            this._redoDirtyCount--;
            this._redoStack.Clear();
            this._currentEvent = null;
            if (dirtyState != true)
            {
                this.FireDirtyStateChangedEvent();
            }
        }

        /// <summary>
        /// Finishes the current suspended state region.
        /// </summary>
        /// <param name="mode">
        /// The mode in which the undo engine was suspended under.
        /// </param>
        internal void FinishSuspendedState(SuspendUndoEngineMode mode)
        {
            if ((mode & SuspendUndoEngineMode.UndoEvents) == SuspendUndoEngineMode.UndoEvents)
            {
                this._eventSuspendedStateCount--;
                if (this._eventSuspendedStateCount < 0)
                {
                    Debug.Assert(false, "Suspended state finished more times than started.");
                    this._eventSuspendedStateCount = 0;
                }
            }

            if ((mode & SuspendUndoEngineMode.DirtyState) == SuspendUndoEngineMode.DirtyState)
            {
                this._dirtySuspendedStateCount--;
                if (this._dirtySuspendedStateCount < 0)
                {
                    Debug.Assert(false, "Suspended state finished more times than started.");
                    this._dirtySuspendedStateCount = 0;
                }
            }
        }

        /// <summary>
        /// Starts a new command so that all future events are batched into a single undo
        /// event until the <see cref="FinishBatch"/> method is called.
        /// </summary>
        internal void StartBatch()
        {
            this._startCommandCount++;
            if (this._currentEvent == null)
            {
                this._currentEvent = new UndoEvent();
            }
        }

        /// <summary>
        /// States a new suspended state region.
        /// </summary>
        /// <param name="mode">
        /// The mode in which the undo engine should be suspended.
        /// </param>
        internal void StartSuspendedState(SuspendUndoEngineMode mode)
        {
            if ((mode & SuspendUndoEngineMode.UndoEvents) == SuspendUndoEngineMode.UndoEvents)
            {
                this._eventSuspendedStateCount++;
            }

            if ((mode & SuspendUndoEngineMode.DirtyState) == SuspendUndoEngineMode.DirtyState)
            {
                this._dirtySuspendedStateCount++;
            }
        }

        /// <summary>
        /// Fires the <see cref="DirtyStateChanged"/> event for this undo engine.
        /// </summary>
        private void FireDirtyStateChangedEvent()
        {
            EventHandler handler = this.DirtyStateChanged;
            if (handler == null)
            {
                return;
            }

            handler(this, EventArgs.Empty);
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// A object used to track the dirty state of a undo engine even though suspended
        /// states.
        /// </summary>
        private class DirtyValueTrackItem
        {
            #region Fields
            /// <summary>
            /// The private field used for the <see cref="AssociatedWithEvent"/> property.
            /// </summary>
            private bool _associatedWithEvent;

            /// <summary>
            /// The private field used for the <see cref="IsDirty"/> property.
            /// </summary>
            private bool _isDirty;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="DirtyValueTrackItem"/> class.
            /// </summary>
            /// <param name="dirtyState">
            /// The dirty state for this item to track.
            /// </param>
            /// <param name="associatedWithEvent">
            /// A value indicating whether this track item was produced with an event.
            /// </param>
            public DirtyValueTrackItem(bool dirtyState, bool associatedWithEvent)
            {
                this._isDirty = dirtyState;
                this._associatedWithEvent = associatedWithEvent;
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets a value indicating whether this track item is directly linked with an
            /// event in the undo engine or whether this was created during a suspended state
            /// frame.
            /// </summary>
            public bool AssociatedWithEvent
            {
                get { return this._isDirty; }
            }

            /// <summary>
            /// Gets a value indicating whether the dirty state of this track item is set or
            /// not..
            /// </summary>
            public bool IsDirty
            {
                get { return this._isDirty; }
            }
            #endregion Properties
        } // UndoEngine.DirtyTrackValue {Class}
        #endregion Classes
    } // RSG.Editor.Model.UndoEngine {Class}
} // RSG.Editor.Model {Namespace}
