﻿//---------------------------------------------------------------------------------------------
// <copyright file="PlayLineAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Commands
{
    using RSG.Editor;
    using RSG.Text.ViewModel;
    using RSG.Text.Model;
    using System.Diagnostics;
    using System.IO;
    using System.Windows.Media;
    using System.Collections.Generic;
    using System.Linq;
    using System;
    using System.Windows;

    /// <summary>
    /// Implements the <see cref="TextCommands.PlayConversation"/> command.
    /// </summary>
    public class PlayConversationAction : ButtonAction<TextConversationCommandArgs>
    {
        private MediaPlayer _audioPlayer = new MediaPlayer();

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="PlayConversationAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public PlayConversationAction(ParameterResolverDelegate<TextConversationCommandArgs> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(TextConversationCommandArgs args)
        {
            if (args.Selected == null)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(TextConversationCommandArgs args)
        {
            ConversationViewModel item = args.Selected.FirstOrDefault();

            string audioBranch = args.AudioPath;

            List<string> wavDepotFilepaths = new List<string>();
            List<string> wavLocalFilepaths = new List<string>();
            List<string> notFoundFilePaths = new List<string>();
            if (item != null)
            {
                bool isRandom = item.IsRandom;

                foreach (var line in item.Lines)
                {
                    if (!line.AudioFilepath.StartsWith("SFX_PAUSE"))
                    {
                        string characterName = line.SelectedCharacter.Name;
                        wavDepotFilepaths.Add(PlayLineHelpers.GetWavDepotFilepathForLine(line.Model, isRandom, characterName, audioBranch));
                    }
                }
            }

            IPerforceService service = this.GetService<IPerforceService>();
            if (service == null)
            {
                Debug.Assert(false, "Unable to check out item due to missing service");
                return;
            }

            try
            {
                foreach (var path in wavDepotFilepaths)
                {
                    IList<string> result = service.GetLocalPathsFromDepotPaths(new List<string>() { path });
                    bool found = false;
                    if(result.Count > 0)
                    {
                        string localPath = result.First();
                        service.GetLatest(new List<string>() { localPath });
                        if (File.Exists(localPath))
                        {
                            wavLocalFilepaths.Add(result.First());
                            found = true;
                        }
                    }
                    if(!found)
                    {
                        notFoundFilePaths.Add(path);
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return;
            }

            if (wavLocalFilepaths.Count > 0)
            {
                bool play = true;
                if (notFoundFilePaths.Count > 0)
                {
                    String unresolvedLines = String.Join("\r\n", notFoundFilePaths);

                    IMessageBoxService mesageBoxService = this.GetService<IMessageBoxService>();
                    MessageBoxResult continueResult = mesageBoxService.Show("Unable to find the wav file for the following lines:\r\n\r\n" +
                        unresolvedLines +
                        "\r\n\r\nDo you wish to play those that were correctly resolved?", "Unresolved wav filepaths.",
                        MessageBoxButton.YesNo, MessageBoxImage.Question);

                    if (continueResult == MessageBoxResult.No)
                    {
                        play = false;
                    }
                }

                if (play)
                {
                    AudioPlayer.Play(wavLocalFilepaths);
                }            
            }
            else
            {
                String unresolvedLines = String.Join("\r\n", notFoundFilePaths);

                IMessageBoxService mesageBoxService = this.GetService<IMessageBoxService>();
                mesageBoxService.Show("Unable to find the wav file for any of the lines in the selected conversation.\r\n\r\n" +
                    unresolvedLines,
                    "Unresolved wav filepaths",
                    MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        #endregion Methods
    } // RSG.Text.Commands.PlayConversationAction {Class}
} // RSG.Text.Commands {Namespace}