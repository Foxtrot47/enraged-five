﻿//---------------------------------------------------------------------------------------------
// <copyright file="ValidateProjectDefinitionsAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Commands
{
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Base.Logging.Universal;
    using RSG.Editor;
    using RSG.Metadata.ViewModel.Project;
    using RSG.Project.Commands;
    using RSG.Project.ViewModel;

    /// <summary>
    /// Contains the logic for the <see cref="MetadataCommands.ValidateProjectDefinitions"/>
    /// routed command. This class cannot be inherited.
    /// </summary>
    public sealed class ValidateProjectDefinitionsAction : ButtonAction<ProjectCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ValidateProjectDefinitionsAction"/>
        /// class.
        /// </summary>
        /// <param name="serviceProvider">
        /// The service provider that is used by this action to perform application specific
        /// operations, for example selecting a file to open.
        /// </param>
        public ValidateProjectDefinitionsAction(ICommandServiceProvider serviceProvider)
            : base(serviceProvider)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ValidateProjectDefinitionsAction"/>
        /// class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public ValidateProjectDefinitionsAction(
            ParameterResolverDelegate<ProjectCommandArgs> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ProjectCommandArgs args)
        {
            if (args == null)
            {
                return false;
            }

            if (args.SelectedNodes != null && args.SelectionCount == 1)
            {
                ProjectNode node = args.SelectedNodes.FirstOrDefault() as ProjectNode;
                if (node != null)
                {
                    return node.Loaded;
                }
            }

            return false;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(ProjectCommandArgs args)
        {
            IHierarchyNode selectedNode = args.SelectedNodes.FirstOrDefault();
            StandardProjectNode project = selectedNode as StandardProjectNode;
            if (project == null)
            {
                return;
            }

            ////project.EnsureDefinitionsAreLoaded();
            ////ILog log = new Log("Metadata Validation");
            ////log.LogError += this.OnLogError;
            ////log.LogWarning += this.OnLogWarning;
            ////project.MetadataDefinitions.Validate(log, true);

            project.EnsureDefinitionsAreLoaded();
            LogFactory.Initialize();
            IUniversalLog log = LogFactory.CreateUniversalLog("Metadata Validation");
            string filename = Path.Combine(Path.GetTempPath(), "metadata_validation");
            filename = Path.ChangeExtension(filename, UniversalLogFile.EXTENSION);
            UniversalLogFile target =
                new UniversalLogFile(filename, FileMode.Create, FlushMode.Never, log);
            project.MetadataDefinitions.Validate(log, true);
            target.Flush();

            Process.Start(filename);
        }

        /// <summary>
        /// Called whenever the metadata validation logs a error message.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The RSG.Base.Logging.LogMessageEventArgs containing the event data.
        /// </param>
        private void OnLogError(object sender, LogMessageEventArgs e)
        {
            Debug.WriteLine("Error: {0}".FormatCurrent(e.Message));
        }

        /// <summary>
        /// Called whenever the metadata validation logs a warning message.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The RSG.Base.Logging.LogMessageEventArgs containing the event data.
        /// </param>
        private void OnLogWarning(object sender, LogMessageEventArgs e)
        {
            Debug.WriteLine("Warning: {0}".FormatCurrent(e.Message));
        }
        #endregion Methods
    } // RSG.Metadata.Commands.ValidateProjectDefinitionsAction {Class}
} // RSG.Metadata.Commands {Namespace}
