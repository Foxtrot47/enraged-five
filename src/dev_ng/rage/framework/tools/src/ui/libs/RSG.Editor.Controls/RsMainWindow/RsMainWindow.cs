﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsMainWindow.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Xml;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Editor.Controls.Commands;
    using RSG.Editor.Controls.Customisation;
    using RSG.Editor.Controls.Helpers;
    using RSG.Editor.Controls.Resources;
    using RSG.Editor.Controls.Themes;
    using RSG.Editor.SharedCommands;
    using RSG.Interop.Bugstar;
    using RSG.Interop.Bugstar.Application;
    using RSG.Interop.Microsoft.Windows;

    /// <summary>
    /// Represents the main window control for a application. This is a extension of
    /// <see cref="RsWindow"/> which adds support for a menu, tool bar try and a status bar.
    /// </summary>
    public abstract class RsMainWindow : RsWindow
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="IsInMenuMode"/> property.
        /// </summary>
        public static readonly DependencyProperty IsInMenuModeProperty;

        /// <summary>
        /// Identifies the <see cref="IsShowingDebugOptions"/> property.
        /// </summary>
        public static readonly DependencyProperty IsShowingDebugOptionsProperty;

        /// <summary>
        /// Identifies the <see cref="ShowMainMenu"/> property.
        /// </summary>
        public static readonly DependencyProperty ShowMainMenuProperty;

        /// <summary>
        /// Identifies the <see cref="ShowToolBarTray"/> property.
        /// </summary>
        public static readonly DependencyProperty ShowToolBarTrayProperty;

        /// <summary>
        /// Identifies the <see cref="ShowProgressBar"/> property.
        /// </summary>
        public static readonly DependencyProperty ShowProgressBarProperty;

        /// <summary>
        /// Identifies the <see cref="ShowStatusBar"/> property.
        /// </summary>
        public static readonly DependencyProperty ShowStatusBarProperty;

        /// <summary>
        /// Identifies the <see cref="ShowStatusText"/> property.
        /// </summary>
        public static readonly DependencyProperty ShowStatusTextProperty;

        /// <summary>
        /// Identifies the <see cref="StatusContent"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty StatusContentProperty;

        /// <summary>
        /// Identifies the <see cref="StatusText"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty StatusTextProperty;

        /// <summary>
        /// Identifies the <see cref="IsInMenuMode"/> dependency property.
        /// </summary>
        private static readonly DependencyPropertyKey _isInMenuModePropertyKey;

        /// <summary>
        /// Identifies the <see cref="IsShowingDebugOptions"/> dependency property.
        /// </summary>
        private static readonly DependencyPropertyKey _isShowingDebugOptionsPropertyKey;

        /// <summary>
        /// The string that contains the data to use when loading the layout for the toolbars.
        /// </summary>
        private string _toolBarLayout;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsMainWindow"/> class.
        /// </summary>
        static RsMainWindow()
        {
            StatusContentProperty =
                DependencyProperty.Register(
                "StatusContent",
                typeof(object),
                typeof(RsMainWindow));

            StatusContentProperty =
                DependencyProperty.Register(
                "StatusText",
                typeof(string),
                typeof(RsMainWindow),
                new PropertyMetadata(null, null, CoerceStatusText));

            ShowMainMenuProperty =
                DependencyProperty.Register(
                "ShowMainMenu",
                typeof(bool),
                typeof(RsMainWindow),
                new PropertyMetadata(true));

            ShowToolBarTrayProperty =
                DependencyProperty.Register(
                "ShowToolBarTray",
                typeof(bool),
                typeof(RsMainWindow),
                new PropertyMetadata(true));

            ShowStatusBarProperty =
                DependencyProperty.Register(
                "ShowStatusBar",
                typeof(bool),
                typeof(RsMainWindow),
                new PropertyMetadata(true));

            ShowStatusTextProperty =
                DependencyProperty.Register(
                "ShowStatusText",
                typeof(bool),
                typeof(RsMainWindow),
                new PropertyMetadata(false));

            ShowProgressBarProperty =
                DependencyProperty.Register(
                "ShowProgressBar",
                typeof(bool),
                typeof(RsMainWindow),
                new PropertyMetadata(false));

            _isShowingDebugOptionsPropertyKey =
                DependencyProperty.RegisterAttachedReadOnly(
                "IsShowingDebugOptions",
                typeof(bool),
                typeof(RsMainWindow),
                new FrameworkPropertyMetadata(Debugger.IsAttached));

            _isInMenuModePropertyKey =
                DependencyProperty.RegisterAttachedReadOnly(
                "IsInMenuMode",
                typeof(bool),
                typeof(RsMainWindow),
                new FrameworkPropertyMetadata(false));

            IsShowingDebugOptionsProperty =
                _isShowingDebugOptionsPropertyKey.DependencyProperty;

            IsInMenuModeProperty =
                _isInMenuModePropertyKey.DependencyProperty;

            ProgressBar.ValueProperty.AddOwner(typeof(RsMainWindow));

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsMainWindow),
                new FrameworkPropertyMetadata(typeof(RsMainWindow)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsMainWindow"/> class.
        /// </summary>
        protected RsMainWindow()
        {
            this.SupportCoerceTitle = true;
            this.CoerceValue(StatusContentProperty);

            RockstarCommandManager.AddBinding(
                this, RockstarCommands.ViewHelp, this.OnViewHelp);

            RockstarCommandManager.AddBinding(
                this, RockstarCommands.ReportBug, this.OnReportBug);

            RockstarCommandManager.AddBinding(
                this, RockstarCommands.ViewAbout, this.OnViewAbout);

            InputManager.Current.EnterMenuMode += this.OnEnterMenuMode;
            InputManager.Current.LeaveMenuMode += this.OnLeaveMenuMode;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether the current input manager used for this main window
        /// is set in menu mode.
        /// </summary>
        public bool IsInMenuMode
        {
            get { return (bool)this.GetValue(IsInMenuModeProperty); }
            private set { this.SetValue(_isInMenuModePropertyKey, value); }
        }

        /// <summary>
        /// Gets a value indicating whether the debug options are shown on the main menu on the
        /// right hand side.
        /// </summary>
        public bool IsShowingDebugOptions
        {
            get { return (bool)this.GetValue(IsShowingDebugOptionsProperty); }
            private set { this.SetValue(_isShowingDebugOptionsPropertyKey, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the main menu is visible.
        /// </summary>
        public bool ShowMainMenu
        {
            get { return (bool)this.GetValue(ShowMainMenuProperty); }
            set { this.SetValue(ShowMainMenuProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the toolbar tray is visible.
        /// </summary>
        public bool ShowToolBarTray
        {
            get { return (bool)this.GetValue(ShowToolBarTrayProperty); }
            set { this.SetValue(ShowToolBarTrayProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the progress bar in the status bar is
        /// visible.
        /// </summary>
        public bool ShowProgressBar
        {
            get { return (bool)this.GetValue(ShowProgressBarProperty); }
            set { this.SetValue(ShowProgressBarProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the status bar is visible.
        /// </summary>
        public bool ShowStatusBar
        {
            get { return (bool)this.GetValue(ShowStatusBarProperty); }
            set { this.SetValue(ShowStatusBarProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the status text in the status bar is
        /// visible.
        /// </summary>
        public bool ShowStatusText
        {
            get { return (bool)this.GetValue(ShowStatusTextProperty); }
            set { this.SetValue(ShowStatusTextProperty, value); }
        }

        /// <summary>
        /// Gets or sets the content that is displayed on the status bar.
        /// </summary>
        public object StatusContent
        {
            get { return (object)this.GetValue(StatusContentProperty); }
            set { this.SetValue(StatusContentProperty, value); }
        }

        /// <summary>
        /// Gets or sets the content that is displayed on the status bar.
        /// </summary>
        public string StatusText
        {
            get { return (string)this.GetValue(StatusTextProperty); }
            set { this.SetValue(StatusTextProperty, value); }
        }

        /// <summary>
        /// Gets or sets the progress bar value shown in the status bar.
        /// </summary>
        public double Value
        {
            get { return (double)this.GetValue(ProgressBar.ValueProperty); }
            set { this.SetValue(ProgressBar.ValueProperty, value); }
        }

        /// <summary>
        /// Gets the url string that is shown when the user chooses to view help.
        /// </summary>
        protected virtual string HelpUrl
        {
            get
            {
                Type type = this.GetType();
                Assembly assembly = type.Assembly;
                return AssemblyInfo.GetHelpLink(assembly);
            }
        }

        /// <summary>
        /// Gets the collection containing all of the commands that should be shown on the main
        /// menu command bar.
        /// </summary>
        private CommandBarItemViewModelCollection RootMenuCommands
        {
            get { return CommandBarItemViewModel.Create(RockstarCommandManager.MainMenuId); }
        }

        /// <summary>
        /// Gets the collection containing all of the commands that should be shown on the tool
        /// bar tray.
        /// </summary>
        private CommandBarItemViewModelCollection ToolBarTrayMenuCommands
        {
            get
            {
                return CommandBarItemViewModel.Create(RockstarCommandManager.ToolBarTrayId);
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Deserialises the layout for this control using the data contained within the
        /// specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the layout data to apply to this window.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        public override void DeserialiseLayout(XmlReader reader, IFormatProvider provider)
        {
            base.DeserialiseLayout(reader, provider);
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                if (!reader.IsStartElement())
                {
                    reader.Read();
                    continue;
                }

                if (String.CompareOrdinal("ToolBars", reader.Name) != 0)
                {
                    reader.Skip();
                    continue;
                }

                this._toolBarLayout = reader.ReadOuterXml();
                break;
            }
        }

        /// <summary>
        /// Called whenever application code or framework processes call
        /// System.Windows.FrameworkElement.ApplyTemplate.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            RsApplication app = Application.Current as RsApplication;
            if (app == null)
            {
                return;
            }

            if (Debugger.IsAttached)
            {
                ApplySkinPickerTemplate(app);
            }
            ApplyDebugChromeTemplate(app);
        }

        /// <summary>
        /// Serialises the layout of this control into the specified xml writer.
        /// </summary>
        /// <param name="writer">
        /// The xml writer that the layout gets serialised out to.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        public override void SerialiseLayout(XmlWriter writer, IFormatProvider provider)
        {
            base.SerialiseLayout(writer, provider);
            writer.WriteStartElement("ToolBars");

            RsToolBarTray toolBarTray =
                this.GetTemplateChild("PART_ToolBarTray") as RsToolBarTray;
            if (toolBarTray != null)
            {
                foreach (ToolBar toolbar in toolBarTray.ToolBars)
                {
                    CommandBarItemViewModel dc =
                        toolbar.DataContext as CommandBarItemViewModel;
                    if (dc == null)
                    {
                        continue;
                    }

                    writer.WriteStartElement("ToolBar");

                    string id = dc.CommandBarItem.Id.ToString("D", provider);
                    writer.WriteAttributeString("id", id);
                    writer.WriteAttributeString("visible", dc.IsVisible.ToString(provider));
                    writer.WriteAttributeString("band", dc.Band.ToString(provider));
                    writer.WriteAttributeString("bandindex", dc.BandIndex.ToString(provider));

                    writer.WriteEndElement();
                }
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Called whenever the window receives the WM_SYSCOMMAND message with the CONTEXTHELP
        /// identifier set in the word parameter.
        /// </summary>
        protected override void HandleContextHelp()
        {
            Process.Start(this.HelpUrl);
        }

        /// <summary>
        /// Gets called once the content for this window has been rendered. This hooks the
        /// binding for the main menu and toolbar tray.
        /// </summary>
        /// <param name="e">
        /// The event data.
        /// </param>
        protected override void OnContentRendered(EventArgs e)
        {
            base.OnContentRendered(e);
            RsMenu mainMenu = this.GetTemplateChild("PART_MainMenu") as RsMenu;
            if (mainMenu != null)
            {
                mainMenu.Items.Clear();
                Binding menuBinding = new Binding();
                menuBinding.Source = this.RootMenuCommands;
                menuBinding.BindsDirectlyToSource = true;
                menuBinding.Mode = BindingMode.OneWay;
                menuBinding.UpdateSourceTrigger = UpdateSourceTrigger.Explicit;
                mainMenu.SetBinding(RsMenu.ItemsSourceProperty, menuBinding);

                ContextMenu contextMenu = mainMenu.ContextMenu;
                if (contextMenu != null)
                {
                    CompositeCollection collection = new CompositeCollection();
                    CollectionContainer container = new CollectionContainer();
                    container.Collection = from i in this.ToolBarTrayMenuCommands
                                           orderby i.Text
                                           select i;

                    collection.Add(container);

                    collection.Add(new Separator());
                    RsMenuItem customiseItem = new RsMenuItem();
                    customiseItem.Header = "Customise...";
                    customiseItem.Click += this.OnToolbarCommandCustomiseItemClicked;
                    collection.Add(customiseItem);

                    contextMenu.ItemsSource = collection;
                }
            }

            RsToolBarTray toolBarTray =
                this.GetTemplateChild("PART_ToolBarTray") as RsToolBarTray;
            if (toolBarTray != null)
            {
                toolBarTray.ToolBars.Clear();
                Binding toolBarTrayBinding = new Binding();
                toolBarTrayBinding.Source = this.ToolBarTrayMenuCommands;
                toolBarTrayBinding.BindsDirectlyToSource = true;
                toolBarTrayBinding.Mode = BindingMode.OneWay;
                toolBarTrayBinding.UpdateSourceTrigger = UpdateSourceTrigger.Explicit;
                toolBarTray.SetBinding(ItemsControl.ItemsSourceProperty, toolBarTrayBinding);

                ContextMenu contextMenu = toolBarTray.ContextMenu;
                if (contextMenu != null)
                {
                    CompositeCollection collection = new CompositeCollection();
                    CollectionContainer container = new CollectionContainer();
                    container.Collection = from i in this.ToolBarTrayMenuCommands
                                           orderby i.Text
                                           select i;

                    collection.Add(container);

                    collection.Add(new Separator());
                    RsMenuItem customiseItem = new RsMenuItem();
                    customiseItem.Header = "Customise...";
                    customiseItem.Click += this.OnToolbarCommandCustomiseItemClicked;
                    collection.Add(customiseItem);

                    contextMenu.ItemsSource = collection;
                }
            }

            this.LoadToolBarLayout();
        }

        /// <summary>
        /// Called whenever the <see cref="RockstarCommands.ViewAbout"/> command is fired and
        /// needs handling at this instance level.
        /// </summary>
        /// <param name="data">
        /// The execute data that has been sent with the command.
        /// </param>
        protected virtual void OnViewAbout(ExecuteCommandData data)
        {
            Type type = this.GetType();
            Assembly assembly = type.Assembly;

            RsAboutWindow window = new RsAboutWindow(assembly);
            window.Owner = this;
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            window.ShowDialog();
        }

        /// <summary>
        /// Called whenever the <see cref="StatusContent"/> dependency property needs to be
        /// re-evaluated.
        /// </summary>
        /// <param name="s">
        /// The <see cref="RsMainWindow"/> instance whose dependency property needs evaluated.
        /// </param>
        /// <param name="baseValue">
        /// The new value of the property, prior to any coercion attempt.
        /// </param>
        /// <returns>
        /// The coerced value.
        /// </returns>
        private static object CoerceStatusText(DependencyObject s, object baseValue)
        {
            string newValue = baseValue as string;
            if (newValue == null)
            {
                return "Ready";
            }

            return newValue;
        }

        /// <summary>
        /// Applies the template to the SkinPicker combobox.
        /// </summary>
        /// <param name="app">Reference to the application object.</param>
        private void ApplySkinPickerTemplate(RsApplication app)
        {
            ComboBox skinPicker = this.GetTemplateChild("PART_SkinPicker") as ComboBox;
            if (skinPicker != null)
            {
                skinPicker.ItemsSource = RsApplication.AvailableThemes;
                skinPicker.SelectedItem = app.CurrentTheme;
                skinPicker.SelectionChanged += new SelectionChangedEventHandler(
                    delegate
                    {
                        app.SetTheme((ApplicationTheme)skinPicker.SelectedItem);
                    });
            }
        }

        /// <summary>
        /// Applies the default template to the debug chrome checkbox.
        /// </summary>
        /// <param name="app">Reference to the application object.</param>
        [Conditional("DEBUG")]
        private void ApplyDebugChromeTemplate(RsApplication app)
        {
            CheckBox debugChrome = this.GetTemplateChild("PART_DebugChrome") as CheckBox;
            if (debugChrome != null)
            {
                debugChrome.IsChecked = !Debugger.IsAttached;
                debugChrome.Click += new RoutedEventHandler(
                    delegate
                    {
                        SetDebugChrome(app, debugChrome.IsChecked.Value);
                    });

                if (!Debugger.IsAttached)
                {
                    SetDebugChrome(app, true);
                }
            }
        }

        /// <summary>
        /// Loads and applies the layout settings for the tool bars.
        /// </summary>
        private void LoadToolBarLayout()
        {
            if (this._toolBarLayout == null)
            {
                return;
            }

            TextReader textReader = new StringReader(this._toolBarLayout);
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.CloseInput = true;
            using (XmlReader reader = XmlReader.Create(textReader, settings))
            {
                while (reader.MoveToContent() != XmlNodeType.None)
                {
                    if (!reader.IsStartElement())
                    {
                        reader.Read();
                        continue;
                    }

                    if (String.CompareOrdinal("ToolBar", reader.Name) != 0)
                    {
                        reader.Read();
                        continue;
                    }

                    Guid id;
                    if (!Guid.TryParseExact(reader.GetAttribute("id"), "D", out id))
                    {
                        reader.Skip();
                        continue;
                    }

                    foreach (CommandBarItemViewModel vm in this.ToolBarTrayMenuCommands)
                    {
                        if (vm.CommandBarItem == null || vm.CommandBarItem.Id != id)
                        {
                            continue;
                        }

                        bool visible;
                        if (bool.TryParse(reader.GetAttribute("visible"), out visible))
                        {
                            vm.IsVisible = visible;
                        }

                        int band;
                        if (int.TryParse(reader.GetAttribute("band"), out band))
                        {
                            vm.Band = band;
                        }

                        int bandindex;
                        if (int.TryParse(reader.GetAttribute("bandindex"), out bandindex))
                        {
                            vm.BandIndex = bandindex;
                        }

                        break;
                    }

                    reader.Skip();
                }
            }

            this._toolBarLayout = null;
        }

        /// <summary>
        /// Called whenever the current input manager enters menu mode.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The event data.
        /// </param>
        private void OnEnterMenuMode(object s, EventArgs e)
        {
            this.IsInMenuMode = true;
        }

        /// <summary>
        /// Called whenever the current input manager leaves menu mode.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The event data.
        /// </param>
        private void OnLeaveMenuMode(object s, EventArgs e)
        {
            this.IsInMenuMode = false;
        }

        /// <summary>
        /// Called whenever the <see cref="RockstarCommands.ReportBug"/> command is fired and
        /// needs handling at this instance level.
        /// </summary>
        /// <param name="data">
        /// The execute data that has been sent with the command.
        /// </param>
        private void OnReportBug(ExecuteCommandData data)
        {
            // Get a reference to the object that will provide us with bugstar information.
            IBugstarInfoProvider bugstarInfoProvider = Application.Current as IBugstarInfoProvider;
            if (bugstarInfoProvider == null)
            {
                return;
            }

            BugstarApplication app = new BugstarApplication();
            if (!app.IsBugstarRunning())
            {
                RsMessageBox.Show(
                    WindowResources.BugstarNotRunningMessage,
                    WindowResources.BugstarNotRunningCaption);
                return;
            }

            try
            {
                PendingBug bug = new PendingBug();
                bug.Project = bugstarInfoProvider.BugstarProjectName;

                bug.Summary = String.Format("{0} - ", bugstarInfoProvider.ProductName);
                bug.Tags.Add("Editor Framework Generated Bug");

                bug.Owner = bugstarInfoProvider.DefaultBugOwner;
                bug.Component = bugstarInfoProvider.BugstarComponent;

                RSG.Base.Logging.LogFactory.FlushApplicationLog();
                bug.FileAttachments.Add(LogFactory.ApplicationLogFilename);

                // Attempt to capture a screen shot of the main window.
                MemoryStream screenshotStream = null;
                try
                {
                    BitmapSource screenshotSource = CaptureScreenshot();

                    screenshotStream = new MemoryStream();
                    PngBitmapEncoder png = new PngBitmapEncoder();
                    png.Frames.Add(BitmapFrame.Create(screenshotSource));
                    png.Save(screenshotStream);

                    screenshotStream.Position = 0;
                    bug.AddStreamedAttachment("MainWindowScreenshot.png", screenshotStream);
                }
                catch (Exception e)
                {
                    // Ignore any exceptions that might happen while trying to capture
                    // the screen shot.
                    LogFactory.ApplicationLog.ToolException(e,
                        "An exception occurred while attempting to capture a screen shot to attach to the bug.");
                }

                // Create the bug itself.
                app.CreateBug(bug);

                // Clean up after ourselves.
                if (screenshotStream != null)
                {
                    screenshotStream.Dispose();
                }
            }
            catch (Exception)
            {
                RsMessageBox.Show(WindowResources.CreateBugFailedMsg);
            }
        }

        /// <summary>
        /// Called whenever the command customisation button is pressed for the toolbars.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs data for this event.
        /// </param>
        private void OnToolbarCommandCustomiseItemClicked(object sender, RoutedEventArgs e)
        {
            RsCustomiseWindow window = new RsCustomiseWindow();
            window.Owner = this;
            window.ShowDialog();
        }

        /// <summary>
        /// Called whenever the <see cref="RockstarCommands.ViewHelp"/> command is fired and
        /// needs handling at this instance level.
        /// </summary>
        /// <param name="data">
        /// The execute data that has been sent with the command.
        /// </param>
        private void OnViewHelp(ExecuteCommandData data)
        {
            IntPtr code = new IntPtr((int)SystemCommandType.CONTEXTHELP);
            User32.SendMessage(this.Handle, WindowMessage.SYSCOMMAND, code, IntPtr.Zero);
        }

        /// <summary>
        /// Toggles the debug chrome on or off.
        /// </summary>
        /// <param name="app">Reference to the application object.</param>
        /// <param name="enabled">Whether we wish to enable or disable debug chrome.</param>
        private void SetDebugChrome(RsApplication app, bool enabled)
        {
            object key = ThemeBrushes.WindowBorderKey;
            object key1 = ThemeBrushes.WindowStatusTextKey;

            bool found = false;
            foreach (object resourceKey in app.Resources.Keys)
            {
                if (Object.ReferenceEquals(resourceKey, key))
                {
                    found = true;
                    break;
                }
            }

            if (!found && enabled)
            {
                app.Resources.Add(key, Colors.Pink);
                app.Resources.Add(key1, Brushes.Black);
            }
            else if (found && !enabled)
            {
                app.Resources.Remove(key);
                app.Resources.Remove(key1);
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsMainWindow {Class}
} // RSG.Editor.Controls {Namespace}
