﻿//---------------------------------------------------------------------------------------------
// <copyright file="CommandViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Customisation
{
    using System;
    using System.ComponentModel;
    using System.Windows;
    using System.Windows.Media.Imaging;
    using RSG.Editor.View;

    /// <summary>
    /// Represents a view model that wraps a collection of commands together underneath a
    /// single container.
    /// </summary>
    public class CommandViewModel : NotifyPropertyChangedBase, ITreeDisplayItem
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="CommandBarItem"/> property.
        /// </summary>
        private CommandBarItem _commandBarItem;

        /// <summary>
        /// The private field used for the <see cref="Container"/> property.
        /// </summary>
        private CommandContainerViewModel _container;

        /// <summary>
        /// The private field used for the <see cref="Id"/> property.
        /// </summary>
        private Guid _id;

        /// <summary>
        /// The private field used for the <see cref="IsVisible"/> property.
        /// </summary>
        private bool _isVisible;

        /// <summary>
        /// The private field used for the <see cref="Text"/> property.
        /// </summary>
        private string _text;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CommandViewModel"/> class.
        /// </summary>
        /// <param name="text">
        /// The text used for this new command view model.
        /// </param>
        /// <param name="id">
        /// The id used for this new command view model.
        /// </param>
        public CommandViewModel(string text, Guid id)
        {
            this._text = text;
            this._id = id;
            this._container = null;
            this._isVisible = true;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="CommandViewModel"/> class.
        /// </summary>
        /// <param name="item">
        /// The item that this view model is wrapping.
        /// </param>
        /// <param name="container">
        /// The container that this view model is currently inside.
        /// </param>
        public CommandViewModel(CommandBarItem item, CommandContainerViewModel container)
        {
            this._commandBarItem = item;
            this._id = item.Id;
            this._container = container;

            PropertyChangedEventManager.AddHandler(item, this.ItemChanged, String.Empty);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the command bar item this view model is wrapping.
        /// </summary>
        public CommandBarItem CommandBarItem
        {
            get { return this._commandBarItem; }
        }

        /// <summary>
        /// Gets the container this command view model is directly underneath.
        /// </summary>
        public CommandContainerViewModel Container
        {
            get { return this._container; }
        }

        /// <summary>
        /// Gets the bitmap source that is used to display the icon for this item when it has
        /// been expanded.
        /// </summary>
        public BitmapSource ExpandedIcon
        {
            get { return CommonIcons.Paste; }
        }

        /// <summary>
        /// Gets a value indicating whether this item is currently modified.
        /// </summary>
        public bool IsModified
        {
            get { return false; }
        }

        /// <summary>
        /// Gets the style that the font on this item should be using.
        /// </summary>
        public FontStyle FontStyle
        {
            get { return FontStyles.Normal; }
        }

        /// <summary>
        /// Gets the weight or thickness that the font on this item should be using.
        /// </summary>
        public FontWeight FontWeight
        {
            get { return FontWeights.Normal; }
        }

        /// <summary>
        /// Gets the bitmap source that is used to display the icon for this item.
        /// </summary>
        public BitmapSource Icon
        {
            get { return CommonIcons.ClosedFolder; }
        }

        /// <summary>
        /// Gets the identifier for this command.
        /// </summary>
        public Guid Id
        {
            get { return this._id; }
        }

        /// <summary>
        /// Gets a value indicating whether this item can be expanded in the view.
        /// </summary>
        public virtual bool IsExpandable
        {
            get { return false; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the command bar item this view model is
        /// wrapping is currently visible to the user in the user interface.
        /// </summary>
        public bool IsVisible
        {
            get
            {
                if (this._commandBarItem == null)
                {
                    return this._isVisible;
                }
                else
                {
                    return this._commandBarItem.IsVisible;
                }
            }

            set
            {
                if (this._commandBarItem != null)
                {
                    if (this._commandBarItem.IsVisible == value)
                    {
                        return;
                    }

                    string oldValue = this._commandBarItem.IsVisible.ToString();
                    this._commandBarItem.IsVisible = value;
                    this.RegisterModification(ModificationField.Visibility, oldValue);
                }
                else
                {
                    this.SetProperty(ref this._isVisible, value);
                }
            }
        }

        /// <summary>
        /// Gets the bitmap source that is used to display the overlay icon for this item.
        /// </summary>
        public BitmapSource OverlayIcon
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the priority of the command bar item that determines the order it is placed
        /// amongst its siblings.
        /// </summary>
        public ushort Priority
        {
            get { return this._commandBarItem.Priority; }
        }

        /// <summary>
        /// Gets the bitmap source that is used to display the state icon for this item.
        /// </summary>
        public BitmapSource StateIcon
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the text that is used to represent the state of this item.
        /// </summary>
        public string StateToolTipText
        {
            get { return null; }
        }

        /// <summary>
        /// Gets or sets the text for this command.
        /// </summary>
        public string Text
        {
            get
            {
                if (this._commandBarItem == null)
                {
                    return this._text;
                }
                else
                {
                    return this._commandBarItem.Text;
                }
            }

            set
            {
                if (this._commandBarItem != null)
                {
                    if (this._commandBarItem.Text == value)
                    {
                        return;
                    }

                    string oldValue = this._commandBarItem.Text;
                    this._commandBarItem.Text = value;
                    this.RegisterModification(ModificationField.Text, oldValue);
                }
                else
                {
                    this.SetProperty(ref this._text, value);
                }
            }
        }

        /// <summary>
        /// Gets the text that is used in the tooltip for this item.
        /// </summary>
        public string ToolTip
        {
            get { return null; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Registers a modification to the command bar item to the specified field with the
        /// command manager.
        /// </summary>
        /// <param name="field">
        /// The field that was modified.
        /// </param>
        /// <param name="oldValue">
        /// The value as a string before the modification occurred.
        /// </param>
        protected void RegisterModification(ModificationField field, string oldValue)
        {
            RockstarCommandManager.RegisterModification(this._commandBarItem, field, oldValue);
        }

        /// <summary>
        /// Gets called whenever a property changed event occurs from the associated command
        /// bar item.
        /// </summary>
        /// <param name="sender">
        /// The command bar item that fired the event.
        /// </param>
        /// <param name="e">
        /// The event data including the name of the property that changed.
        /// </param>
        private void ItemChanged(object sender, PropertyChangedEventArgs e)
        {
            this.NotifyPropertyChanged(e.PropertyName);
        }
        #endregion Methods
    } // RSG.Editor.Controls.Customisation.CommandViewModel {Class}
} // RSG.Editor.Controls.Customisation {Namespace}
