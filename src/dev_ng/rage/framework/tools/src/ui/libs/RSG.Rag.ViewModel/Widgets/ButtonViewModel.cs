﻿using RSG.Rag.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor;

namespace RSG.Rag.ViewModel.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    public class ButtonViewModel : WidgetViewModel<WidgetButton>
    {
        #region Fields
        /// <summary>
        /// 
        /// </summary>
        private IAsyncCommand _activateCommand;
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        public ButtonViewModel(WidgetButton widget)
            : base(widget)
        {
        }
        #endregion // Constructor(s)

        #region Commands
        /// <summary>
        /// 
        /// </summary>
        public IAsyncCommand ActivateCommand
        {
            get
            {
                if (_activateCommand == null)
                {
                    _activateCommand = new RelayCommandAsync(param => Activate(param), param => CanActivate(param));
                }
                return _activateCommand;
            }
        }
        #endregion // Commands

        #region Command Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool CanActivate(Object parameter)
        {
            return !ReadOnly;
        }

        /// <summary>
        /// Called to press the button.
        /// </summary>
        public async Task Activate(Object parameter)
        {
            await Widget.Activate();
        }
        #endregion // Command Methods
    } // ButtonViewModel
}
