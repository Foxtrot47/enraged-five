﻿//---------------------------------------------------------------------------------------------
// <copyright file="StandardProjectNode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Project
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml;
    using RSG.Base;
    using RSG.Editor;
    using RSG.Metadata.Model.Definitions;
    using RSG.Project.Model;
    using RSG.Project.ViewModel;
    using RSG.Project.ViewModel.Definitions;

    /// <summary>
    /// The node inside the project explorer that represents a standard metadata project. This
    /// class cannot be inherited.
    /// </summary>
    public sealed class StandardProjectNode : ProjectNode
    {
        #region Fields
        /// <summary>
        /// The private list of all of the item definition categories that have been created
        /// for this project using the associated definitions.
        /// </summary>
        private List<ItemDefinitionCategory> _categories;

        /// <summary>
        /// The private object used for the load synchronisation across multiple threads.
        /// </summary>
        private object _loadSyncObject = new object();

        /// <summary>
        /// The private field used for the <see cref="MetadataDefinitions"/> property.
        /// </summary>
        private IDefinitionDictionary _metadataDefinitions;

        /// <summary>
        /// A private field indicating whether the metadata definitions have been loaded.
        /// </summary>
        private bool _metadataDefinitionsLoaded;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="StandardProjectNode"/> class.
        /// </summary>
        /// <param name="definition">
        /// The project definition that was used to create this project node instance.
        /// </param>
        public StandardProjectNode(ProjectDefinition definition)
            : base(definition)
        {
            this._metadataDefinitions = new DefinitionDictionary();
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the definition dictionary containing the metadata definitions that have been
        /// tied to this project.
        /// </summary>
        public IDefinitionDictionary MetadataDefinitions
        {
            get { return this._metadataDefinitions; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new metadata file node object that will be added to the specified parent
        /// and using the specified item as a model.
        /// </summary>
        /// <param name="parent">
        /// The parent node that the new file node will be added to.
        /// </param>
        /// <param name="item">
        /// The project item model that the new file node will be representing.
        /// </param>
        /// <returns>
        /// A new file node object that represents the specified project item.
        /// </returns>
        public override FileNode CreateFileNode(IHierarchyNode parent, ProjectItem item)
        {
            return new MetadataFileNode(parent, item);
        }

        /// <summary>
        /// Ensures that the definitions for this project are loaded.
        /// </summary>
        public void EnsureDefinitionsAreLoaded()
        {
            lock (this._loadSyncObject)
            {
                if (!this._metadataDefinitionsLoaded)
                {
                    this.LoadMetadataDefinitions();
                }
            }
        }

        /// <summary>
        /// Gets all of the metadata constants for this project that have been loaded from the
        /// file with the specified full path.
        /// </summary>
        /// <param name="fullPath">
        /// The full path of the file whose constants should be retrieved.
        /// </param>
        /// <returns>
        /// A read-only list of metadata constants that have been loaded from the file with the
        /// specified full path.
        /// </returns>
        public IReadOnlyList<IConstant> GetConstants(string fullPath)
        {
            return this._metadataDefinitions.GetConstantsFromFile(fullPath);
        }

        /// <summary>
        /// Retrieves the item definition categories that have been setup for this project.
        /// </summary>
        /// <returns>
        /// The item definition categories that have been setup for this project.
        /// </returns>
        public override IReadOnlyList<ItemDefinitionCategory> GetDefinitionCategories()
        {
            this.EnsureDefinitionsAreLoaded();
            if (this._categories != null)
            {
                return this._categories;
            }

            this._categories = new List<ItemDefinitionCategory>();
            Dictionary<string, List<IStructure>> structures =
                new Dictionary<string, List<IStructure>>();
            foreach (IStructure structure in this._metadataDefinitions.Structures)
            {
                string category = "Miscellaneous";
                List<IStructure> currentStructures = null;
                if (!structures.TryGetValue(category, out currentStructures))
                {
                    currentStructures = new List<IStructure>();
                    structures.Add(category, currentStructures);
                }

                currentStructures.Add(structure);
            }

            foreach (KeyValuePair<string, List<IStructure>> kvp in structures)
            {
                ItemDefinitionCategory category = new ItemDefinitionCategory(kvp.Key);
                foreach (IStructure structure in kvp.Value)
                {
                    string fileExtensions = structure.Filter;
                    if (String.IsNullOrWhiteSpace(fileExtensions))
                    {
                        category.AddItemDefinition(
                            new StructureProjectItemDefinition(structure, ".meta"));
                        category.AddItemDefinition(
                            new StructureProjectItemDefinition(structure, ".pso.meta"));
                    }
                    else
                    {
                        string[] parts = fileExtensions.Split(new char[] { '|' });
                        for (int i = 1; i < parts.Length; i += 2)
                        {
                            string[] patterns = parts[i].Split(new char[] { ';' });
                            foreach (string pattern in patterns)
                            {
                                int startIndex = pattern.IndexOf(".");
                                if (startIndex == -1)
                                {
                                    startIndex = 0;
                                }

                                string extension = pattern.Substring(startIndex);
                                category.AddItemDefinition(
                                    new StructureProjectItemDefinition(structure, extension));
                            }
                        }
                    }
                }

                this._categories.Add(category);
            }

            return this._categories;
        }

        /// <summary>
        /// Gets all of the metadata enumerations for this project that have been loaded from
        /// the file with the specified full path.
        /// </summary>
        /// <param name="fullPath">
        /// The full path of the file whose enumerations should be retrieved.
        /// </param>
        /// <returns>
        /// A read-only list of metadata enumerations that have been loaded from the file with
        /// the specified full path.
        /// </returns>
        public IReadOnlyList<IEnumeration> GetEnumerations(string fullPath)
        {
            return this._metadataDefinitions.GetEnumerationsFromFile(fullPath);
        }

        /// <summary>
        /// Gets all of the metadata structures for this project that have been loaded from the
        /// file with the specified full path.
        /// </summary>
        /// <param name="fullPath">
        /// The full path of the file whose structures should be retrieved.
        /// </param>
        /// <returns>
        /// A read-only list of metadata structures that have been loaded from the file with
        /// the specified full path.
        /// </returns>
        public IReadOnlyList<IStructure> GetStructures(string fullPath)
        {
            return this._metadataDefinitions.GetStructuresFromFile(fullPath);
        }

        /// <summary>
        /// Creates the core "Definitions" folder node that can be expanded to show the .psc
        /// files that are being linked into this project.
        /// </summary>
        /// <returns>
        /// A list of core folder nodes created by this project.
        /// </returns>
        protected override IList<CoreFolderNode> CreateCoreFolders()
        {
            return new List<CoreFolderNode>()
            {
                new LinkedDefinitionsNode(this),
            };
        }

        /// <summary>
        /// Performs all post load actions. Loads the metadata definitions from the root path
        /// that has been set for this project.
        /// </summary>
        protected override void PostLoad()
        {
            base.PostLoad();
            Task.Factory.StartNew(
                new Action(() =>
                {
                    lock (this._loadSyncObject)
                    {
                        if (!this._metadataDefinitionsLoaded)
                        {
                            this.LoadMetadataDefinitions();
                        }
                    }
                }));
        }

        /// <summary>
        /// Loads the metadata definitions for this project.
        /// </summary>
        private void LoadMetadataDefinitions()
        {
            string definitionRoot = this.GetProperty("DefinitionRoot");
            string directoryName = Path.GetDirectoryName(this.FullPath);
            definitionRoot = Path.Combine(directoryName, definitionRoot);
            definitionRoot = Path.GetFullPath(definitionRoot);

            if (Directory.Exists(definitionRoot))
            {
                string value = this.GetProperty("IncludeSubDirectories");
                bool includeSubDirectories = false;
                if (!bool.TryParse(value, out includeSubDirectories))
                {
                    includeSubDirectories = false;
                }

                SearchOption options = SearchOption.TopDirectoryOnly;
                if (includeSubDirectories)
                {
                    options = SearchOption.AllDirectories;
                }

                string[] files = Directory.GetFiles(definitionRoot, "*.psc", options);
                foreach (string file in files)
                {
                    this._metadataDefinitions.LoadFile(file);
                }
            }

            this._metadataDefinitionsLoaded = true;
        }
        #endregion Methods
    } // RSG.Metadata.ViewModel.Project.StandardProjectNode {Class}
} // RSG.Metadata.ViewModel.Project {Namespace}
