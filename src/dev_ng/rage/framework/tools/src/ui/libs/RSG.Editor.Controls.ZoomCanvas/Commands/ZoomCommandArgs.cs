﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace RSG.Editor.Controls.ZoomCanvas.Commands
{
    /// <summary>
    /// Command arguments for the <see cref="ZoomCanvasCommands.Zoom"/> routed command.
    /// </summary>
    public class ZoomCommandArgs
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Amount"/> property.
        /// </summary>
        private readonly Vector _amount;

        /// <summary>
        /// Private field for the <see cref="ZoomAboutCenter"/> property.
        /// </summary>
        private readonly bool _zoomAboutCenter;

        /// <summary>
        /// Private field for the <see cref="ZoomAboutPoint"/> property.
        /// </summary>
        private readonly Point _zoomAboutPoint;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ZoomCommandArgs"/> class using
        /// the specified amount and zooming about the controls center point.
        /// </summary>
        /// <param name="amount"></param>
        public ZoomCommandArgs(Vector amount)
        {
            _amount = amount;
            _zoomAboutCenter = true;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ZoomCommandArgs"/> class using
        /// the specified amount and point to zoom about.
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="zoomAboutPoint"></param>
        public ZoomCommandArgs(Vector amount, Point zoomAboutPoint)
        {
            _amount = amount;
            _zoomAboutCenter = false;
            _zoomAboutPoint = zoomAboutPoint;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Amount to zoom by.
        /// </summary>
        public Vector Amount
        {
            get { return _amount; }
        }

        /// <summary>
        /// Flag indicating that we should zoom about the controls central point.
        /// </summary>
        public bool ZoomAboutCenter
        {
            get { return _zoomAboutCenter; }
        }

        /// <summary>
        /// Point to perform the zoom operation around.
        /// </summary>
        public Point ZoomAboutPoint
        {
            get { return _zoomAboutPoint; }
        }
        #endregion
    }
}
