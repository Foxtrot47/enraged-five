﻿//---------------------------------------------------------------------------------------------
// <copyright file="ItemDefinitionCategory.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel.Definitions
{
    using System.Collections.Generic;
    using System.Windows.Media.Imaging;
    using RSG.Project.Model;

    /// <summary>
    /// Defines a category of item definitions that define the different items that can be
    /// added to a project.
    /// </summary>
    public class ItemDefinitionCategory
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="ItemDefinitions"/> property.
        /// </summary>
        private List<ProjectItemDefinition> _itemDefinitions;

        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private string _name;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ItemDefinitionCategory"/> class.
        /// </summary>
        /// <param name="name">
        /// The name for this category.
        /// </param>
        public ItemDefinitionCategory(string name)
        {
            this._name = name;
            this._itemDefinitions = new List<ProjectItemDefinition>();
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the read-only list of all of the item definitions defined for this project.
        /// </summary>
        public IReadOnlyList<ProjectItemDefinition> ItemDefinitions
        {
            get { return this._itemDefinitions; }
        }

        /// <summary>
        /// Gets the name of this item definition category.
        /// </summary>
        public string Name
        {
            get { return this._name; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Adds a new item definition into this project definition.
        /// </summary>
        /// <param name="definition">
        /// The item definition to add.
        /// </param>
        public void AddItemDefinition(ProjectItemDefinition definition)
        {
            this._itemDefinitions.Add(definition);
        }
        #endregion Methods
    } // RSG.Project.ViewModel.Definitions.ItemDefinitionCategory {Class}
} // RSG.Project.ViewModel.Definitions {Namespace}
