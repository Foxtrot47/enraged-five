﻿//---------------------------------------------------------------------------------------------
// <copyright file="CommandInstance.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Windows.Input;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Provides a abstract base class to all of the objects that will be placed within the
    /// command bar hierarchy.
    /// </summary>
    public class CommandInstance : CommandBarItem
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Command"/> parameter.
        /// </summary>
        private RockstarRoutedCommand _command;

        /// <summary>
        /// The private field used for the <see cref="DisplayStyle"/> property.
        /// </summary>
        private CommandItemDisplayStyle _displayStyle;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CommandInstance"/> class.
        /// </summary>
        /// <param name="command">
        /// The command that this command bar item is instancing.
        /// </param>
        /// <param name="priority">
        /// The initial priority that this command bar item should be given.
        /// </param>
        /// <param name="startsGroup">
        /// A value indicating whether this command bar item should start a new group. (I.e.
        /// Placed directly after a separator).
        /// </param>
        /// <param name="text">
        /// The text for this command bar item.
        /// </param>
        /// <param name="id">
        /// The unique identifier that is used to reference this item.
        /// </param>
        /// <param name="parentId">
        /// The unique identifier for the parent of this item inside the command bar hierarchy.
        /// </param>
        public CommandInstance(
            RockstarRoutedCommand command,
            ushort priority,
            bool startsGroup,
            string text,
            Guid id,
            Guid parentId)
            : base(priority, startsGroup, text, id, parentId)
        {
            if (command == null)
            {
                throw new SmartArgumentNullException(() => command);
            }

            this._command = command;
            this._displayStyle = CommandItemDisplayStyle.Default;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="CommandInstance"/> class.
        /// </summary>
        /// <param name="priority">
        /// The initial priority that this command bar item should be given.
        /// </param>
        /// <param name="startsGroup">
        /// A value indicating whether this command bar item should start a new group. (I.e.
        /// Placed directly after a separator).
        /// </param>
        /// <param name="text">
        /// The text for this command bar item.
        /// </param>
        /// <param name="id">
        /// The unique identifier that is used to reference this item.
        /// </param>
        /// <param name="parentId">
        /// The unique identifier for the parent of this item inside the command bar hierarchy.
        /// </param>
        internal CommandInstance(
            ushort priority,
            bool startsGroup,
            string text,
            Guid id,
            Guid parentId)
            : base(priority, startsGroup, text, id, parentId)
        {
            this._displayStyle = CommandItemDisplayStyle.Default;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the command that this command bar item is instancing. This ties the
        /// definition and instance together.
        /// </summary>
        public RockstarRoutedCommand Command
        {
            get { return this._command; }
            protected internal set { this.SetProperty(ref this._command, value); }
        }

        /// <summary>
        /// Gets or sets a value that can be used to change how the control that is used to
        /// display this item gets styled.
        /// </summary>
        public virtual CommandItemDisplayStyle DisplayStyle
        {
            get { return this._displayStyle; }
            set { this.SetProperty(ref this._displayStyle, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Updates the text making sure the original text is kept as a fall back for when this
        /// instance is disabled.
        /// </summary>
        /// <param name="updatedText">
        /// The updated text that should replace the existing text.
        /// </param>
        internal void UpdateTextValue(string updatedText)
        {
            this.Text = updatedText;
        }
        #endregion Methods
    }  // RSG.Editor.CommandInstance {Class}
} // RSG.Editor {Namespace}
