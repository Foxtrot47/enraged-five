﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Pipeline.Automation.Common.Client;
using RSG.Pipeline.Automation.Common;

namespace RSG.Automation.ViewModel
{

    /// <summary>
    /// Client View-Model.
    /// </summary>
    public sealed class ClientViewModel : ViewModelBase
    {
        #region Properties
        /// <summary>
        /// Client unique identifier.
        /// </summary>
        public Guid Id
        {
            get { return (this._client.ID); }
        }

        /// <summary>
        /// Client state.
        /// </summary>
        public WorkerState State
        {
            get { return (this._client.State); }
        }

        /// <summary>
        /// Client hostname.
        /// </summary>
        public Uri ServiceConnection
        {
            get { return (this._client.ServiceConnection); }
        }

        /// <summary>
        /// Currently assigned-job (if any).
        /// </summary>
        public Guid AssignedJob
        {
            get { return (this._client.AssignedJob); }
        }

        /// <summary>
        /// Client capability.
        /// </summary>
        public Capability Capability
        {
            get { return (this._client.Capability); }
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Model worker status object.
        /// </summary>
        private WorkerStatus _client;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="client"></param>
        public ClientViewModel(WorkerStatus client)
        {
            this._client = client;
        }
        #endregion // Constructor(s)
    }

} // RSG.Automation.ViewModel namespace
