﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsSearchControlItem.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;

    /// <summary>
    /// Represents a selectable control inside a <see cref="RsSearchControl"/> control.
    /// </summary>
    public class RsSearchControlItem : ComboBoxItem
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="MruItemSelected"/> routed event.
        /// </summary>
        public static readonly RoutedEvent MruItemSelectedEvent;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsSearchControlItem" /> class.
        /// </summary>
        static RsSearchControlItem()
        {
            MruItemSelectedEvent =
                EventManager.RegisterRoutedEvent(
                "MRUItemSelectedEvent",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(RsSearchControlItem));

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsSearchControlItem),
                new FrameworkPropertyMetadata(typeof(RsSearchControlItem)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsSearchControlItem" /> class.
        /// </summary>
        public RsSearchControlItem()
        {
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Adds or removes handlers to the event that is fired when this item is selected.
        /// </summary>
        public event RoutedEventHandler MruItemSelected
        {
            add { this.AddHandler(MruItemSelectedEvent, value); }
            remove { this.RemoveHandler(MruItemSelectedEvent, value); }
        }
        #endregion Events

        #region Methods
        /// <summary>
        /// Invoked when an unhandled System.Windows.Input.Keyboard.KeyDown attached event
        /// reaches this element.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.KeyEventArgs that contains the event data.
        /// </param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            if (e.Key == Key.Return || e.Key == Key.Space)
            {
                this.RaiseEvent(new RoutedEventArgs(MruItemSelectedEvent));
                e.Handled = true;
            }
        }

        /// <summary>
        /// Invoked when an unhandled System.Windows.Input.Mouse.MouseDown attached event
        /// reaches this element.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.MouseButtonEventArgs that contains the event data.
        /// </param>
        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            this.RaiseEvent(new RoutedEventArgs(MruItemSelectedEvent));
            e.Handled = true;
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsSearchControlItem {Class}
} // RSG.Editor.Controls {Namespace}
