﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsSplitterResizePreviewWindow.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Interop;
    using RSG.Editor.Controls.Helpers;
    using RSG.Interop.Microsoft.Windows;

    /// <summary>
    /// A window that is used to show the splitter grip preview element to the user while
    /// it is being dragged.
    /// </summary>
    public class RsSplitterResizePreviewWindow : Control
    {
        #region Fields
        /// <summary>
        /// The private reference to the HWND source to the actual win32 window.
        /// </summary>
        private HwndSource _hwndSource;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsSplitterResizePreviewWindow"/>
        /// class.
        /// </summary>
        static RsSplitterResizePreviewWindow()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsSplitterResizePreviewWindow),
                new FrameworkPropertyMetadata(typeof(RsSplitterResizePreviewWindow)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsSplitterResizePreviewWindow" />
        /// class.
        /// </summary>
        public RsSplitterResizePreviewWindow()
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Hides this preview window from the user by destroying it.
        /// </summary>
        public void Hide()
        {
            if (this._hwndSource == null)
            {
                return;
            }

            using (this._hwndSource)
            {
                this._hwndSource = null;
            }
        }

        /// <summary>
        /// Moves the preview window to the specified device location.
        /// </summary>
        /// <param name="left">
        /// The device location of the left position of the window.
        /// </param>
        /// <param name="top">
        /// The device location of the top position of the window.
        /// </param>
        public void Move(double left, double top)
        {
            if (this._hwndSource == null)
            {
                return;
            }

            IntPtr handle = this._hwndSource.Handle;

            WindowPosFlags flags =
                WindowPosFlags.NOSIZE |
                WindowPosFlags.NOZORDER |
                WindowPosFlags.NOACTIVATE |
                WindowPosFlags.SHOWWINDOW;

            User32.SetWindowPos(handle, (int)left, (int)top, flags);
        }

        /// <summary>
        /// Shows the window to the user with the specified parent user interface element.
        /// </summary>
        /// <param name="parentElement">
        /// The parent user interface element the window should have.
        /// </param>
        public void Show(UIElement parentElement)
        {
            HwndSource hwndSource = PresentationSource.FromVisual(parentElement) as HwndSource;
            IntPtr owner = (hwndSource == null) ? IntPtr.Zero : hwndSource.Handle;
            this.EnsureWindow(owner);
            Point point = parentElement.PointToScreen(new Point(0.0, 0.0));
            Size size = parentElement.RenderSize.LogicalToDeviceUnits();

            WindowPosFlags flags =
                WindowPosFlags.NOZORDER |
                WindowPosFlags.NOACTIVATE |
                WindowPosFlags.SHOWWINDOW;

            IntPtr handle = this._hwndSource.Handle;
            User32.SetWindowPos(handle, point, size, flags);
            HwndSource source = HwndSource.FromHwnd(handle);
        }

        /// <summary>
        /// Ensures that the window is created with the specified owner.
        /// </summary>
        /// <param name="owner">
        /// The owner the preview window should have.
        /// </param>
        private void EnsureWindow(IntPtr owner)
        {
            if (this._hwndSource != null)
            {
                return;
            }

            HwndSourceParameters parameters =
                new HwndSourceParameters("RsSplitterResizePreviewWindow");
            int windowStyle = -2013265880;
            parameters.Width = 0;
            parameters.Height = 0;
            parameters.PositionX = 0;
            parameters.PositionY = 0;
            parameters.WindowStyle = windowStyle;
            parameters.UsesPerPixelOpacity = true;
            parameters.ParentWindow = owner;
            this._hwndSource = new HwndSource(parameters);
            this._hwndSource.SizeToContent = SizeToContent.Manual;
            this._hwndSource.RootVisual = this;
        }
        #endregion Methods
    } // RSG.Editor.Controls.Dock.RsSplitterResizePreviewWindow {Class}
} // RSG.Editor.Controls.Dock {Namespace}
