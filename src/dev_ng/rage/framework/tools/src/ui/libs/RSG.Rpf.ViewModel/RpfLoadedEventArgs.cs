﻿//---------------------------------------------------------------------------------------------
// <copyright file="RpfLoadedEventArgs.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Rpf.ViewModel
{
    using System;

    /// <summary>
    /// Provides data for the event that is raised when a <see cref="RpfViewerDataContext"/>
    /// has finished attempting to load a file.
    /// </summary>
    public class RpfLoadedEventArgs : EventArgs
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Path"/> property.
        /// </summary>
        private string _path;

        /// <summary>
        /// The private field used for the <see cref="Successful"/> property.
        /// </summary>
        private bool _successful;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="RpfLoadedEventArgs"/> class.
        /// </summary>
        /// <param name="path">
        /// The location of the file loaded.
        /// </param>
        /// <param name="successful">
        /// A value indicating whether the load has been successful or not.
        /// </param>
        public RpfLoadedEventArgs(string path, bool successful)
        {
            this._path = path;
            this._successful = successful;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the location of the file loaded.
        /// </summary>
        public string Path
        {
            get { return this._path; }
        }

        /// <summary>
        /// Gets a value indicating whether the load has been successful or not.
        /// </summary>
        public bool Successful
        {
            get { return this._successful; }
        }
        #endregion Properties
    } // RSG.Rpf.ViewModel.RpfLoadedEventArgs {Class}
} // RSG.Rpf.ViewModel {Namespace}
