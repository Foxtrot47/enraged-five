﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Logging;
using RSG.Configuration;
using RSG.Configuration.Bugstar;
using RSG.Interop.Bugstar;
using RSG.Interop.Bugstar.Organisation;

namespace RSG.Editor.Controls.Bugstar
{
    /// <summary>
    /// Provides a implementation of the <see cref="IBugstarService"/> interface that can
    /// be used be the application to give command actions the ability to perform bugstar
    /// operations.
    /// </summary>
    public class BugstarService : IBugstarService
    {
        #region Fields
        /// <summary>
        /// Bugstar configuration object.
        /// </summary>
        private readonly IBugstarConfig _bugstarConfig;

        /// <summary>
        /// Bugstar connection object.
        /// </summary>
        private readonly BugstarConnection _bugstarConnection;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BugstarService"/> class using
        /// the specified log and project.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="project"></param>
        public BugstarService(ILog log, IProject project)
        {
            _bugstarConfig = ConfigFactory.CreateBugstarConfig(log, project);

            // Don't login to bugstar until it's required.
            _bugstarConnection = new BugstarConnection(_bugstarConfig.RESTService, _bugstarConfig.AttachmentService);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Retrieves the bugstar project id for the currently installed tools project.
        /// </summary>
        public uint ProjectId
        {
            get { return _bugstarConfig.ProjectId; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Gets the list of available bugstar projects.
        /// </summary>
        public IEnumerable<Project> GetAvailableProjects()
        {
            if (!ValidateBugstarConnection())
            {
                return Enumerable.Empty<Project>();
            }

            return Project.GetProjects(_bugstarConnection);
        }

        /// <summary>
        /// Gets the bugstar project for the currently installed tools project.
        /// </summary>
        /// <returns></returns>
        public Project GetActiveProject()
        {
            return GetProjectById(_bugstarConfig.ProjectId);
        }

        /// <summary>
        /// Gets a particular project by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Project GetProjectById(uint id)
        {
            if (!ValidateBugstarConnection())
            {
                return null;
            }

            return Project.GetProjectById(_bugstarConnection, id);
        }

        /// <summary>
        /// Attempts to create a valid bugstar connection that can be used to run
        /// requests against the bugstar REST service.
        /// </summary>
        /// <param name="rethrow"></param>
        /// <returns></returns>
        private bool ValidateBugstarConnection(bool rethrow = false)
        {
            // Only prompt for a login if the connection isn't valid.
            if (!_bugstarConnection.IsValidConnection())
            {
                try
                {
                    PasswordLogin(_bugstarConnection);
                }
                catch
                {
                    if (rethrow)
                    {
                        throw;
                    }
                }
            }

            return _bugstarConnection.IsValidConnection();
        }

        /// <summary>
        /// Represents the predicate method that determines whether the login is successful
        /// based on the current parameters.
        /// </summary>
        /// <param name="e">
        /// The object containing the data to use inside the predicate method to determine the
        /// user name and password the user has set.
        /// </param>
        /// <returns>
        /// A value indicating whether the login has been successful.
        /// </returns>
        private bool LoginAttempt(BugstarLoginAttemptData e)
        {
            try
            {
                e.BugstarConnection.Login(e.Username, e.Password, _bugstarConfig.UserDomain);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The delegate method that can be used to log into bugstar.
        /// </summary>
        /// <param name="p4">
        /// The perforce object that requires a login.
        /// </param>
        /// <returns>
        /// A value indicating whether the login was successful or not.
        /// </returns>
        private bool PasswordLogin(BugstarConnection connection)
        {
            RsBugstarLoginWindow window = new RsBugstarLoginWindow();
            window.Username = Environment.UserName;
            window.LoginDelegate = LoginAttempt;
            window.BugstarConnection = connection;
            return (bool)window.ShowDialog();
        }
        #endregion
    }
}
