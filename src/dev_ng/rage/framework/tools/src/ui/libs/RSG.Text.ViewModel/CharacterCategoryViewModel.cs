﻿//---------------------------------------------------------------------------------------------
// <copyright file="CharacterCategoryViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.ViewModel
{
    using System.Collections;
    using System.Collections.Generic;
    using RSG.Text.Model;
    using RSG.Text.ViewModel.Resources;

    /// <summary>
    /// Represents the dialogue configuration editor category for the character items in the
    /// configuration. This class cannot be inherited.
    /// </summary>
    public sealed class CharacterCategoryViewModel : CategoryViewModel
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Items"/> property.
        /// </summary>
        private CharacterCollection _characters;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CharacterCategoryViewModel"/> class
        /// for the specified configurations.
        /// </summary>
        /// <param name="configurations">
        /// The configurations whose character items will be shown as part of this category.
        /// </param>
        public CharacterCategoryViewModel(DialogueConfigurations configurations)
            : base(StringTable.CharacterCategoryHeader, configurations)
        {
            this._characters = new CharacterCollection(configurations.Characters);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the list of audibility values this category contains.
        /// </summary>
        public CharacterCollection Items
        {
            get { return this._characters; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a composite view model object containing the
        /// <see cref="DialogueCharacterViewModel"/> objects inside the specified list.
        /// </summary>
        /// <param name="list">
        /// The collection that contains the individual items that should be grouped inside the
        /// resulting composite object.
        /// </param>
        /// <returns>
        /// A composite view model object that contains the
        /// <see cref="DialogueCharacterViewModel"/> objects inside the specified list.
        /// </returns>
        public override CompositeViewModel CreateComposite(IList list)
        {
            List<DialogueCharacterViewModel> items = new List<DialogueCharacterViewModel>();
            foreach (object item in list)
            {
                DialogueCharacterViewModel character = item as DialogueCharacterViewModel;
                if (character == null)
                {
                    continue;
                }

                items.Add(character);
            }

            return new CharacterCompositeViewModel(this.Model, items);
        }
        #endregion Methods
    } // RSG.Text.ViewModel.CharacterCategoryViewModel {Class}
} // RSG.Text.ViewModel {Namespace}
