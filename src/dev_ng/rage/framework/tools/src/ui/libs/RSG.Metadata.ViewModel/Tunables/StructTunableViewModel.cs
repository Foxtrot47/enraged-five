﻿//---------------------------------------------------------------------------------------------
// <copyright file="StructTunableViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Tunables
{
    using System.ComponentModel;
using RSG.Editor.View;
using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// The view model for a <see cref="StructTunable"/> model object.
    /// </summary>
    public class StructTunableViewModel
        : TunableViewModelBase<StructTunable>, ITunableViewModelParent
    {
        #region Fields
        /// <summary>
        /// A private value indicating whether the is expandable value has been initialised.
        /// </summary>
        private bool _initialised;

        /// <summary>
        /// The private field used for the <see cref="StructureKeyValue"/> property.
        /// </summary>
        private string _structureKeyValue;

        /// <summary>
        /// The private field used for the <see cref="Tunables"/> property.
        /// </summary>
        private TunableCollection _tunables;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="StructTunableViewModel"/> class.
        /// </summary>
        /// <param name="tunable">
        /// The struct tunable that this view model is representing.
        /// </param>
        /// <param name="parent">
        /// The parent tunable view model for this view model.
        /// </param>
        public StructTunableViewModel(StructTunable tunable, ITunableViewModel parent)
            : base(tunable, parent)
        {
            this.Initialise();
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether this tunable can be expanded.
        /// </summary>
        public override bool IsExpandable
        {
            get { return this.Model.Tunables.Count > 0; }
        }

        /// <summary>
        /// Gets the value of the structure key for this tunable.
        /// </summary>
        public string StructureKeyValue
        {
            get { return this._structureKeyValue; }
            private set { this.SetProperty(ref this._structureKeyValue, value); }
        }

        /// <summary>
        /// Gets the collection of child tunables for this structure.
        /// </summary>
        public IReadOnlyViewModelCollection<ITunableViewModel> Tunables
        {
            get
            {
                if (this._tunables == null)
                {
                    this._tunables = new TunableCollection(this.Model.Tunables, this);
                }

                return this._tunables;
            }
        }

        /// <summary>
        /// Gets the value of the type for this tunable view model that should be shown to the
        /// user.
        /// </summary>
        public override string TypeValue
        {
            get { return this.Model.StructMember.ReferencedStructure.ShortDataType; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Finds the tunable object whose value is being presented as the key to the
        /// structure.
        /// </summary>
        /// <returns>
        /// The tunable object whose value is being presented as the key to the structure.
        /// </returns>
        private ITunable FindStructureKeyTunable()
        {
            foreach (ITunable tunable in this.Model.Tunables)
            {
                if (tunable.Member.IsStructureKey)
                {
                    return tunable;
                }
            }

            return null;
        }

        /// <summary>
        /// Initialises the is expandable value for the first time.
        /// </summary>
        private void Initialise()
        {
            if (this._initialised)
            {
                return;
            }

            this._initialised = true;
            ITunable key = this.FindStructureKeyTunable();
            if (key != null)
            {
                this.UpdateStructureKey(key, new PropertyChangedEventArgs(null));
                key.PropertyChanged += this.UpdateStructureKey;
            }
        }

        /// <summary>
        /// Updates the structure key value due to a change in the tunables data.
        /// </summary>
        /// <param name="s">
        /// The tunable whose property value changed.
        /// </param>
        /// <param name="e">
        /// The System.ComponentModel.PropertyChangedEventArgs containing the event data.
        /// </param>
        private void UpdateStructureKey(object s, PropertyChangedEventArgs e)
        {
            ITunable tunable = (ITunable)s;
            StringTunable stringTunable = tunable as StringTunable;
            if (stringTunable == null)
            {
                return;
            }

            this.StructureKeyValue = stringTunable.Value;
        }
        #endregion Methods
    } // RSG.Metadata.ViewModel.Tunables.StructTunableViewModel {Class}
} // RSG.Metadata.ViewModel.Tunables {Namespace}
