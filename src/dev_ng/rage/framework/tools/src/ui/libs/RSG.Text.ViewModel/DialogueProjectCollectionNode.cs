﻿//---------------------------------------------------------------------------------------------
// <copyright file="DialogueProjectCollectionNode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.ViewModel
{
    using RSG.Project.Model;
    using RSG.Project.ViewModel;

    public class DialogueProjectCollectionNode : ProjectCollectionNode
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DialogueProjectCollectionNode"/>
        /// class.
        /// </summary>
        public DialogueProjectCollectionNode(ProjectCollection collection)
            : base(collection)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        protected override ProjectNode CreateProject(IHierarchyNode parent, ProjectItem item)
        {
            return new DialogueProjectNode(parent, item);
        }
        #endregion Methods
    } // RSG.Text.ViewModel.DialogueProjectCollectionNode {Class}
} // RSG.Text.ViewModel {Namespace}
