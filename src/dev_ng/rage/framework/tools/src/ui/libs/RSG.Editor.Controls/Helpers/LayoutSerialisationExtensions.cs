﻿//---------------------------------------------------------------------------------------------
// <copyright file="LayoutSerialisationExtensions.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Controls;
    using System.Xml;
    using RSG.Base.Extensions;
    using RSG.Editor.Controls.AttachedDependencyProperties;

    /// <summary>
    /// Static class used to add extensions onto view controls that are used to serialise the
    /// instance to a xml writer.
    /// </summary>
    public static class LayoutSerialisationExtensions
    {
        #region Methods
        /// <summary>
        /// Deserialises the layout for the specified object using the data contained within
        /// the specified xml reader.
        /// </summary>
        /// <param name="obj">
        /// The instance to apply the deserialised layout data to.
        /// </param>
        /// <param name="reader">
        /// The xml reader that contains the layout data to apply to the specified instance.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        public static void DeserialiseLayout(
            this DependencyObject obj, XmlReader reader, IFormatProvider provider)
        {
            DataGrid dataGrid = obj as DataGrid;
            if (dataGrid != null)
            {
                dataGrid.DeserialiseLayout(reader, provider);
                return;
            }

            RsWindow window = obj as RsWindow;
            if (window != null)
            {
                window.DeserialiseLayout(reader, provider);
                return;
            }
        }

        /// <summary>
        /// Serialises the layout of the specified object out to the specified xml writer.
        /// </summary>
        /// <param name="obj">
        /// The instance to serialise.
        /// </param>
        /// <param name="writer">
        /// The xml writer that the layout gets serialised out to.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        public static void SerialiseLayout(
            this DependencyObject obj, XmlWriter writer, IFormatProvider provider)
        {
            DataGrid dataGrid = obj as DataGrid;
            if (dataGrid != null)
            {
                dataGrid.SerialiseLayout(writer, provider);
                return;
            }

            RsWindow window = obj as RsWindow;
            if (window != null)
            {
                window.SerialiseLayout(writer, provider);
                return;
            }
        }

        /// <summary>
        /// Serialises the layout of the specified data grid out to the specified xml writer.
        /// </summary>
        /// <param name="dataGrid">
        /// The instance to serialise.
        /// </param>
        /// <param name="writer">
        /// The xml writer that the layout gets serialised out to.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        /// <param name="includeVisibility">
        /// A value indicating whether the visibility is to be serialised as well.
        /// </param>
        public static void SerialiseLayout(
            this DataGrid dataGrid,
            XmlWriter writer,
            IFormatProvider provider,
            bool includeVisibility)
        {
            HashSet<string> uids = new HashSet<string>();
            DataGridLengthConverter widthConverter = new DataGridLengthConverter();
            foreach (DataGridColumn c in dataGrid.Columns)
            {
                string uid = GetUidFromColumn(dataGrid, c);
                bool serialiseWidth = SerialisationProperties.GetSerialiseWidth(c);
                Debug.Assert(!uids.Contains(uid), "Unique identifier repeated.");
                uids.Add(uid);

                writer.WriteStartElement(uid);
                writer.WriteAttributeString("display", c.DisplayIndex.ToString(provider));

                if (serialiseWidth)
                {
                    if (c.Width.IsAbsolute)
                    {
                        writer.WriteAttributeString("width", c.ActualWidth.ToString(provider));
                    }
                    else
                    {
                        writer.WriteAttributeString(
                            "width", widthConverter.ConvertToString(c.Width));
                    }
                }

                if (includeVisibility)
                {
                    writer.WriteAttributeString("visibility", c.Visibility.ToString("G"));
                }

                writer.WriteAttributeString("sorted", c.SortDirection.ToString());
                writer.WriteEndElement();
            }
        }

        /// <summary>
        /// Deserialises the layout for the specified data grid using the data contained within
        /// the specified xml reader.
        /// </summary>
        /// <param name="dataGrid">
        ///  The instance to apply the deserialised layout data to.
        /// </param>
        /// <param name="reader">
        /// The xml reader that contains the layout data to apply to the specified data grid.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        private static void DeserialiseLayout(
            this DataGrid dataGrid, XmlReader reader, IFormatProvider provider)
        {
            var displayMap = new Dictionary<string, int>();
            foreach (DataGridColumn column in dataGrid.Columns)
            {
                string uid = GetUidFromColumn(dataGrid, column);
                if (uid != null)
                {
                    displayMap[uid] = column.DisplayIndex;
                }
            }

            DataGridLengthConverter widthConverter = new DataGridLengthConverter();
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                displayMap.Remove(reader.Name);
                DataGridColumn column = GetColumnFromUid(dataGrid, reader.Name);
                if (column == null)
                {
                    reader.Read();
                    continue;
                }

                string displayString = reader.GetAttribute("display");
                int displayIndex = 0;
                if (int.TryParse(displayString, out displayIndex))
                {
                    column.DisplayIndex = displayIndex;
                }

                string width = reader.GetAttribute("width");
                if (width != null)
                {
                    object converted = widthConverter.ConvertFromInvariantString(width);
                    if (converted is DataGridLength)
                    {
                        column.Width = (DataGridLength)converted;
                    }
                }

                string visibilityString = reader.GetAttribute("visibility");
                if (visibilityString != null)
                {
                    Visibility visibility = Visibility.Visible;
                    if (Enum.TryParse<Visibility>(visibilityString, out visibility))
                    {
                        column.Visibility = visibility;
                    }
                }

                string sortedString = reader.GetAttribute("sorted");
                ListSortDirection direction = ListSortDirection.Ascending;
                if (Enum.TryParse<ListSortDirection>(sortedString, out direction))
                {
                    column.SortDirection = direction;
                }
                else
                {
                    column.SortDirection = null;
                }

                reader.Read();
            }

            foreach (KeyValuePair<string, int> displayPair in displayMap)
            {
                DataGridColumn column = GetColumnFromUid(dataGrid, displayPair.Key);
                if (column == null)
                {
                    continue;
                }

                column.DisplayIndex = displayPair.Value;
                DataGridLength length = column.Width;
                if (length.UnitType == DataGridLengthUnitType.Pixel)
                {
                    if (column.ActualWidth != column.Width.Value)
                    {
                        column.Width = new DataGridLength(column.Width.Value);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the column inside the specified data grid that uses the specified unique
        /// serialisation identifier.
        /// </summary>
        /// <param name="dataGrid">
        /// The data grid that contains the column to search through.
        /// </param>
        /// <param name="uid">
        /// The unique serialisation identifier that should be located on the data grid
        /// columns.
        /// </param>
        /// <returns>
        /// The column inside the specified data grid that uses the specified unique
        /// serialisation identifier if found; otherwise, null.
        /// </returns>
        private static DataGridColumn GetColumnFromUid(DataGrid dataGrid, string uid)
        {
            foreach (DataGridColumn c in dataGrid.Columns)
            {
                string columnUid = SerialisationProperties.GetSerialiseUid(c);
                if (String.CompareOrdinal(uid, columnUid) == 0)
                {
                    return c;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the unique serialisation identifier for the specified column inside the
        /// specified data grid.
        /// </summary>
        /// <param name="dataGrid">
        /// The data grid the specified column is a member of.
        /// </param>
        /// <param name="column">
        /// The column to find the unique serialisation identifier for.
        /// </param>
        /// <returns>
        /// The unique serialisation identifier for the specified column.
        /// </returns>
        private static string GetUidFromColumn(DataGrid dataGrid, DataGridColumn column)
        {
            string uid = SerialisationProperties.GetSerialiseUid(column);
            if (uid == null)
            {
                if (dataGrid.AutoGenerateColumns)
                {
                    uid = column.Header as string;
                }
                else
                {
                    Debug.Assert(
                        uid != null,
                        "Uid not defined on the data grid column, falling back to the index.");
                }

                if (uid == null)
                {
                    uid = dataGrid.Columns.IndexOf(column).ToStringInvariant();
                }
            }

            return uid;
        }

        /// <summary>
        /// Serialises the layout of the specified data grid out to the specified xml writer.
        /// </summary>
        /// <param name="dataGrid">
        /// The instance to serialise.
        /// </param>
        /// <param name="writer">
        /// The xml writer that the layout gets serialised out to.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        private static void SerialiseLayout(
            this DataGrid dataGrid, XmlWriter writer, IFormatProvider provider)
        {
            HashSet<string> uids = new HashSet<string>();
            DataGridLengthConverter widthConverter = new DataGridLengthConverter();
            foreach (DataGridColumn c in dataGrid.Columns)
            {
                string uid = GetUidFromColumn(dataGrid, c);
                bool serialiseWidth = SerialisationProperties.GetSerialiseWidth(c);
                Debug.Assert(!uids.Contains(uid), "Unique identifier repeated.");
                uids.Add(uid);

                writer.WriteStartElement(uid);
                writer.WriteAttributeString("display", c.DisplayIndex.ToString(provider));

                if (serialiseWidth)
                {
                    if (c.Width.IsAbsolute)
                    {
                        writer.WriteAttributeString("width", c.ActualWidth.ToString(provider));
                    }
                    else
                    {
                        writer.WriteAttributeString(
                            "width", widthConverter.ConvertToString(c.Width));
                    }
                }

                writer.WriteAttributeString("visibility", c.Visibility.ToString("G"));
                writer.WriteAttributeString("sorted", c.SortDirection.ToString());
                writer.WriteEndElement();
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.Helpers.LayoutSerialisationExtensions {Class}
} // RSG.Editor.Controls.Helpers [Namespace}
