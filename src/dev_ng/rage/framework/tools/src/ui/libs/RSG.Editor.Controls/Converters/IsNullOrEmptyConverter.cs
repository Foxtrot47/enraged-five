﻿//---------------------------------------------------------------------------------------------
// <copyright file="IsNullOrEmptyConverter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Converters
{
    using System;
    using System.Globalization;

    /// <summary>
    /// Converters a single boolean value to its logical negative.
    /// </summary>
    public class IsNullOrEmptyConverter : ValueConverter<string, bool>
    {
        #region Methods
        /// <summary>
        /// Converters the given value and returns the result.
        /// </summary>
        /// <param name="value">
        /// The original value to convert.
        /// </param>
        /// <param name="param">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A converted value.
        /// </returns>
        protected override bool Convert(string value, object param, CultureInfo culture)
        {
            return String.IsNullOrEmpty(value);
        }
        #endregion Methods
    } // RSG.Editor.Controls.Converters.IsNullOrEmptyConverter {Class}
} // RSG.Editor.Controls.Converters {Namespace}
