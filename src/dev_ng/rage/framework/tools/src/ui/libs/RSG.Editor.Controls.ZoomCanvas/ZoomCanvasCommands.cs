﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using RSG.Editor.SharedCommands;

namespace RSG.Editor.Controls.ZoomCanvas
{
    /// <summary>
    /// Contains the routed commands that can be used with the <see cref="ZoomableCanvas"/> control.
    /// </summary>
    public static class ZoomCanvasCommands
    {
        #region Fields
        /// <summary>
        /// String table resource manager for getting at the command related strings.
        /// </summary>
        private static readonly CommandResourceManager _commandResourceManager =
            new CommandResourceManager(
                "RSG.Editor.Controls.ZoomCanvas.Resources.CommandStringTable",
                typeof(ZoomCanvasCommands).Assembly);

        /// <summary>
        /// The private cache of System.Windows.Input.RoutedCommand objects.
        /// </summary>
        private static readonly RockstarRoutedCommand[] _internalCommands =
            new RockstarRoutedCommand[Enum.GetValues(typeof(CommandId)).Length];
        #endregion

        #region Enumerations
        /// <summary>
        /// Defines all of the command identifiers for the different core Rockstar commands.
        /// </summary>
        private enum CommandId
        {
            /// <summary>
            /// Used to identifier the <see cref="ZoomCanvasCommands.Zoom"/>
            /// routed command.
            /// </summary>
            Zoom,

            /// <summary>
            /// Used to identifier the <see cref="ZoomCanvasCommands.ZoomExtents"/>
            /// routed command.
            /// </summary>
            ZoomExtents,

            /// <summary>
            /// Used to identifier the <see cref="ZoomCanvasCommands.Pan"/>
            /// routed command.
            /// </summary>
            Pan
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the value that represents the zoom command.
        /// </summary>
        public static RockstarRoutedCommand Zoom
        {
            get { return EnsureCommandExists(CommandId.Zoom); }
        }

        /// <summary>
        /// Gets the command that is used to zoom to the contents extents.
        /// </summary>
        public static RockstarRoutedCommand ZoomExtents
        {
            get { return EnsureCommandExists(CommandId.ZoomExtents, ZoomCanvasIcons.ZoomExtents); }
        }

        /// <summary>
        /// Gets the value that represents the pan command.
        /// </summary>
        public static RockstarRoutedCommand Pan
        {
            get { return EnsureCommandExists(CommandId.Pan); }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Creates a new System.Windows.Input.RoutedCommand object that is associated with the
        /// specified identifier.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command to create.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        /// <returns>
        /// The newly create System.Windows.Input.RoutedCommand object.
        /// </returns>
        private static RockstarRoutedCommand CreateCommand(String id, BitmapSource icon)
        {
            string name = _commandResourceManager.GetName(id);
            string category = _commandResourceManager.GetCategory(id);
            string description = _commandResourceManager.GetDescription(id);
            InputGestureCollection gestures = _commandResourceManager.GetGestures(id);

            return new RockstarRoutedCommand(
                name, typeof(ZoomCanvasCommands), gestures, category, description, icon);
        }

        /// <summary>
        /// Makes sure the internal value for the command associated with the specified
        /// identifier exists and returns it.
        /// </summary>
        /// <param name="commandId">
        /// The identifier for the command that should be created and returned.
        /// </param>
        /// <param name="icon">
        /// The icon that the command should be using.
        /// </param>
        /// <returns>
        /// The <see cref="RSG.Editor.RockstarRoutedCommand"/> object associated with the
        /// specified identifier.
        /// </returns>
        private static RockstarRoutedCommand EnsureCommandExists(CommandId commandId, BitmapSource icon = null)
        {
            int commandIndex = (int)commandId;
            if (commandIndex < -1 || commandIndex >= _internalCommands.Length)
            {
                return null;
            }

            RockstarRoutedCommand command = _internalCommands[commandIndex];
            if (command == null)
            {
                lock (_internalCommands)
                {
                    if (command == null)
                    {
                        command = CreateCommand(commandId.ToString(), icon);
                        _internalCommands[commandIndex] = command;
                    }
                }
            }

            return command;
        }
        #endregion
    }
}
