﻿//---------------------------------------------------------------------------------------------
// <copyright file="OpenCollectionAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Commands
{
    using System;
    using System.Diagnostics;
    using System.Threading.Tasks;
    using RSG.Editor;
    using RSG.Project.ViewModel;

    /// <summary>
    /// Contains the logic for the <see cref="ProjectCommands.OpenProjectCollectionFile"/>
    /// routed command. This class cannot be inherited.
    /// </summary>
    public sealed class OpenCollectionAction : ButtonAction<ProjectCommandArgs, string>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="OpenCollectionAction"/> class.
        /// </summary>
        /// <param name="serviceProvider">
        /// The service provider that is used by this action to perform application specific
        /// operations, for example selecting a file to open.
        /// </param>
        public OpenCollectionAction(ICommandServiceProvider serviceProvider)
            : base(serviceProvider)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="OpenCollectionAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public OpenCollectionAction(ProjectCommandResolver resolver)
            : base(new ParameterResolverDelegate<ProjectCommandArgs>(resolver))
        {
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs when a collection is loaded from using this action.
        /// </summary>
        public event EventHandler<CollectionOpenedEventArgs> CollectionOpened;
        #endregion Events

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="target">
        /// A override target parameter for the file path.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ProjectCommandArgs args, string target)
        {
            if (args == null || args.CollectionNode == null)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="target">
        /// A override target parameter for the file path.
        /// </param>
        public override void Execute(ProjectCommandArgs args, string target)
        {
            ICommonDialogService dlgService = this.GetService<ICommonDialogService>();
            if (dlgService == null)
            {
                Debug.Assert(false, "Unable to open collection file as service is missing.");
                return;
            }

            string fullPath = target;
            if (fullPath == null)
            {
                if (!dlgService.ShowOpenFile(
                    null, null, args.CollectionNode.Definition.Filter, 0, out fullPath))
                {
                    throw new OperationCanceledException();
                }
            }

            if (args.CollectionNode.Loaded)
            {
                if (String.Equals(args.CollectionNode.FullPath, fullPath))
                {
                    this.CollectionOpened(
                        this,
                        new CollectionOpenedEventArgs(args.CollectionNode, fullPath, true));
                    return;
                }
            }

            CloseCollectionAction close = new CloseCollectionAction(this.ServiceProvider);
            close.Execute(args);
            args.CollectionNode.Load(fullPath);
            if (this.CollectionOpened != null)
            {
                this.CollectionOpened(
                    this, new CollectionOpenedEventArgs(args.CollectionNode, fullPath, false));
            }
        }
        #endregion Methods
    } // RSG.Project.Commands.OpenCollectionAction {Class}
} // RSG.Project.Commands {Namespace}
