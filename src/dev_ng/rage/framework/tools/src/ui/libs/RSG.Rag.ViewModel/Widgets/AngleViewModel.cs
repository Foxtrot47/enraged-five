﻿using RSG.Rag.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.ViewModel.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    public class AngleViewModel : WidgetViewModel<WidgetAngle>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        public AngleViewModel(WidgetAngle widget)
            : base(widget)
        {
        }
        #endregion // Constructor(s)
    } // AngleViewModel
}
