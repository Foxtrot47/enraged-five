﻿//---------------------------------------------------------------------------------------------
// <copyright file="CustomCommandStyle.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Customisation
{
    using System;
    using System.Reflection;
    using RSG.Base.Attributes;

    /// <summary>
    /// A class that represents a single <see cref="CommandItemDisplayStyle"/> value to be used
    /// while customising the commands.
    /// </summary>
    public class CustomCommandStyle
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="DisplayName"/> property.
        /// </summary>
        private string _displayName;

        /// <summary>
        /// The private field used for the <see cref="Style"/> property.
        /// </summary>
        private CommandItemDisplayStyle _style;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CustomCommandStyle"/> class.
        /// </summary>
        /// <param name="style">
        /// The style that this class represents.
        /// </param>
        public CustomCommandStyle(CommandItemDisplayStyle style)
        {
            this._style = style;

            Type type = typeof(CommandItemDisplayStyle);
            MemberInfo element = type.GetField(Enum.GetName(type, style));

            Type attributeType = typeof(FieldDisplayNameAttribute);
            Attribute attribute = Attribute.GetCustomAttribute(element, attributeType);
            this._displayName = ((FieldDisplayNameAttribute)attribute).DisplayName;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the string that is used to display this custom style with in the user
        /// interface.
        /// </summary>
        public string DisplayName
        {
            get { return this._displayName; }
        }

        /// <summary>
        /// Gets the style that this custom class represents.
        /// </summary>
        public CommandItemDisplayStyle Style
        {
            get { return this._style; }
        }
        #endregion Properties
    } // RSG.Editor.Controls.Customisation.CustomCommandStyle {Class}
} // RSG.Editor.Controls.Customisation {Namespace}
