﻿//---------------------------------------------------------------------------------------------
// <copyright file="ColourUpdateMode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    /// <summary>
    /// Defines the different ways the colour can be updated through the different colour
    /// controls.
    /// </summary>
    public enum ColourUpdateMode
    {
        /// <summary>
        /// Using this means that the colour will never update. This can be used to temporarily
        /// stop the update for some external reason.
        /// </summary>
        None,

        /// <summary>
        /// Using this means that the colour will be constantly updating during a dragging
        /// event. This will result in a lot of individual property changed events.
        /// </summary>
        Realtime,

        /// <summary>
        /// Using this means that the colour will be updated when it is stable and hasn't
        /// changed in a certain amount of time (default of 500 milliseconds).
        /// </summary>
        Delay,

        /// <summary>
        /// Using this means that the colour will be updating whenever the drag operation has
        /// finished.
        /// </summary>
        OnDragFinished,

        /// <summary>
        /// The default mode for the dolour controls.
        /// </summary>
        Default = Realtime,
    } // RSG.Editor.Controls.ColourUpdateMode {Enum}
} // RSG.Editor.Controls {Namespace}
