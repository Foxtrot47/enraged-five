﻿//---------------------------------------------------------------------------------------------
// <copyright file="Screen.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Windows;
    using RSG.Editor.Controls.Helpers;
    using RSG.Interop.Microsoft.Windows;

    /// <summary>
    /// Calculates the on screen position of a window rectangle expressed
    /// in logical virtual-screen coordinates.
    /// </summary>
    internal sealed class Screen
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="OnScreenPosition"/> property.
        /// </summary>
        private Rect _screenPosition;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Screen"/> class using the specified
        /// window and point as the test subject.
        /// </summary>
        /// <param name="windowRect">
        /// The windows area rectangle to test.
        /// </param>
        /// <param name="point">
        /// The fall-back point to test if there is a error and the associated window is
        /// determined to not be displayed on a monitor.
        /// </param>
        public Screen(Rect windowRect, Point point)
        {
            this._screenPosition = windowRect;
            windowRect = DpiHelper.LogicalToDeviceUnits(windowRect);

            Rect monitorRect = FindMaximumSingleMonitorRectangle(windowRect);

            if (!windowRect.IntersectsWith(monitorRect))
            {
                monitorRect = FindMonitorRectsFromPoint(point);
                if (this._screenPosition.Width > monitorRect.Width)
                {
                    this._screenPosition.Width = monitorRect.Width;
                }

                if (this._screenPosition.Height > monitorRect.Height)
                {
                    this._screenPosition.Height = monitorRect.Height;
                }

                if (monitorRect.Right <= this._screenPosition.X)
                {
                    this._screenPosition.X = monitorRect.Right - this._screenPosition.Width;
                }

                if (monitorRect.Left > this._screenPosition.X + this._screenPosition.Width)
                {
                    this._screenPosition.X = monitorRect.Left;
                }

                if (monitorRect.Bottom <= this._screenPosition.Y)
                {
                    this._screenPosition.Y = monitorRect.Bottom - this._screenPosition.Height;
                }

                if (monitorRect.Top > this._screenPosition.Y + this._screenPosition.Height)
                {
                    this._screenPosition.Y = monitorRect.Top;
                }
            }
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the on screen position of the associated window.
        /// </summary>
        public Rect OnScreenPosition
        {
            get { return this._screenPosition; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Gets the working area for the monitor that the majority of the window is
        /// being displayed on.
        /// </summary>
        /// <param name="windowRect">
        /// The area of the window to test.
        /// </param>
        /// <returns>
        /// A rectangle expressed in virtual-screen coordinates of the working area for the
        /// monitor that the majority of the window is being displayed on.
        /// </returns>
        private static Rect FindMaximumSingleMonitorRectangle(Rect windowRect)
        {
            Rect[] monitorsAreas = User32.GetDisplayMonitorAreas();
            long greatestArea = 0;
            Rect result = new Rect();
            foreach (Rect monitorRect in monitorsAreas)
            {
                Rect intersect = User32.IntersectRect(monitorRect, windowRect);
                long area = (long)(intersect.Width * intersect.Height);
                if (area > greatestArea)
                {
                    result = monitorRect;
                    greatestArea = area;
                }
            }

            return result;
        }

        /// <summary>
        /// Gets the display rectangle of the monitor that encapsulates the given point.
        /// </summary>
        /// <param name="point">
        /// The point to test.
        /// </param>
        /// <returns>
        /// A rectangle expressed in virtual-screen coordinates of the monitor that
        /// encapsulates the given point.
        /// </returns>
        private static Rect FindMonitorRectsFromPoint(Point point)
        {
            IntPtr monitor = User32.GetMonitorFromPoint(new POINT(point));

            Rect monitorRect = new Rect(0.0, 0.0, 0.0, 0.0);
            if (monitor != IntPtr.Zero)
            {
                MONITORINFO monitorInfo = User32.GetMonitorInfo(monitor);
                monitorRect = new Rect(monitorInfo.Monitor.Position, monitorInfo.Monitor.Size);
            }

            return DpiHelper.DeviceToLogicalUnits(monitorRect);
        }
        #endregion Methods
    } // RSG.Editor.Controls.Screen {Class}
} // RSG.Editor.Controls {Namespace}
