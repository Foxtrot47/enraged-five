﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsGameViewportHost.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Runtime.InteropServices;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Interop;
    using System.Windows.Media;
    using RSG.Interop.Microsoft.Windows;

    /// <summary>
    /// Control for hosting an externally rendered to HWND.
    /// </summary>
    public class RsGameViewportHost : HwndHost
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="FillColour"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty FillColourProperty;

        /// <summary>
        /// Class name for the window that will host the game viewport.
        /// </summary>
        private const string GameViewportHostClassName = "GameViewportHost";

        /// <summary>
        /// Class name for the child window that the game creates.
        /// </summary>
        private const string GameViewportWindowName = "grcWindow";

        /// <summary>
        /// Stores a reference to the windows procedure handler method to prevent it
        /// from being garbage collected.
        /// </summary>
        private static readonly WndProcDelegate _defaultWndProc;

        /// <summary>
        /// Window class atom obtained from registering a new window class.
        /// </summary>
        private static readonly IntPtr _windowClassAtom;

        /// <summary>
        /// Window handle for this control.
        /// </summary>
        private readonly IntPtr _hwnd = IntPtr.Zero;

        /// <summary>
        /// Parent window's handle. Used to see whether we need to re-parent the window.
        /// </summary>
        private IntPtr _parentHwnd = IntPtr.Zero;

        /// <summary>
        /// Flag indicating whether we've changed the hosted window's style to a CHILD
        /// window (from a POPUP window).
        /// </summary>
        private bool _styleChanged = false;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsGameViewportHost" /> class.
        /// </summary>
        static RsGameViewportHost()
        {
            FillColourProperty = DependencyProperty.Register(
                "FillColour",
                typeof(Color),
                typeof(RsGameViewportHost),
                new FrameworkPropertyMetadata(Colors.Black));

            _defaultWndProc = new WndProcDelegate(User32.DefWindowProc);
            _windowClassAtom = RegisterWindowClass();
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsGameViewportHost" /> class.
        /// </summary>
        public RsGameViewportHost()
        {
            WindowStyles style = WindowStyles.POPUP;
            this._hwnd = User32.CreateWindow(0, _windowClassAtom, style, IntPtr.Zero);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the colour that the non client area gets filled with.
        /// </summary>
        public Color FillColour
        {
            get { return (Color)this.GetValue(FillColourProperty); }
            set { this.SetValue(FillColourProperty, value); }
        }

        /// <summary>
        /// Gets the handle to the instance of the game that is rendering into this viewport.
        /// IntPtr.Zero if nothing is currently rendering.
        /// </summary>
        public IntPtr GameHandle
        {
            get
            {
                return User32.FindWindowEx(
                    this.Handle, IntPtr.Zero, GameViewportWindowName, null);
            }
        }

        /// <summary>
        /// Gets the handle of the host window (Note that this is available prior to the Handle
        /// being returned from the BuildWindowCore so can be used prior to the control
        /// being displayed on screen.
        /// </summary>
        public IntPtr HostHandle
        {
            get { return this._hwnd; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Used to simulate a key press in the hosted window.  Note that this only works
        /// if the external app isn't reading the key state directly from the keyboard
        /// device.
        /// </summary>
        /// <param name="key">
        /// The key to simulate.
        /// </param>
        public void SimulateKeyPress(VirtualKey key)
        {
            // Ignore if the game isn't running.
            IntPtr gameHwnd = this.GameHandle;
            if (gameHwnd == IntPtr.Zero)
            {
                return;
            }

            int keyScanCode =
                User32.MapVirtualKey((uint)key, MapVirtualKeyMapTypes.MAPVK_VK_TO_VSC);

            IntPtr longParam = (IntPtr)key;
            IntPtr wordParam = (IntPtr)(keyScanCode << 16);

            User32.PostMessage(gameHwnd, WindowMessage.KEYDOWN, longParam, wordParam);
            User32.PostMessage(gameHwnd, WindowMessage.CHAR, longParam, wordParam);

            // We need to put this artificial pause in here to give the game a chance to
            // progress one update cycle so that things that use the input have a chance
            // to react to it.
            System.Threading.Thread.Sleep(50);
            User32.PostMessage(gameHwnd, WindowMessage.KEYUP, longParam, wordParam);
        }

        /// <summary>
        /// Returns the window that will be hosted.
        /// </summary>
        /// <param name="hwndParent">
        /// The parent window handle for the child window.
        /// </param>
        /// <returns>
        /// The window handle to the child window that was created.
        /// </returns>
        protected override HandleRef BuildWindowCore(HandleRef hwndParent)
        {
            if (!this._styleChanged)
            {
                WindowStyles style =
                    WindowStyles.CHILD | WindowStyles.CLIPCHILDREN | WindowStyles.CLIPSIBLINGS;
                bool typeChanged =
                    User32.SetWindowLong(this._hwnd, WindowLongFlag.STYLE, (int)style);
            }

            //// Check whether the parent has changed and re-parent the hosted window if
            //// required.
            if (this._parentHwnd != hwndParent.Handle)
            {
                User32.SetParent(this._hwnd, hwndParent.Handle);
                this._parentHwnd = hwndParent.Handle;
            }

            return new HandleRef(this, this._hwnd);
        }

        /// <summary>
        /// Destroys the hosted window.
        /// </summary>
        /// <param name="hwnd">
        /// The handle to the window to destroy.
        /// </param>
        protected override void DestroyWindowCore(HandleRef hwnd)
        {
            User32.DestroyWindow(hwnd.Handle);
        }

        /// <summary>
        /// Override to handle the windows key down message.
        /// </summary>
        /// <param name="key">
        /// The key that was pressed down.
        /// </param>
        protected virtual void OnWindowsKeyDownMessage(Key key)
        {
        }

        /// <summary>
        /// Override to handle the windows key up message.
        /// </summary>
        /// <param name="key">
        /// The key that was released.
        /// </param>
        protected virtual void OnWindowsKeyUpMessage(Key key)
        {
        }

        /// <summary>
        /// Override to handle the windows mouse down message.
        /// </summary>
        /// <param name="pos">
        /// The position of the cursor relative to the upper-left  corner of the client area.
        /// </param>
        /// <param name="button">
        /// The mouse button that was pressed down.
        /// </param>
        protected virtual void OnWindowsMouseDownMessage(Point pos, MouseButton button)
        {
        }

        /// <summary>
        /// Override to handle the windows mouse move message.
        /// </summary>
        /// <param name="pos">
        /// The position of the cursor relative to the upper-left  corner of the client area.
        /// </param>
        protected virtual void OnWindowsMouseMoveMessage(Point pos)
        {
        }

        /// <summary>
        /// Override to handle the windows mouse up message.
        /// </summary>
        /// <param name="pos">
        /// The position of the cursor relative to the upper-left  corner of the client area.
        /// </param>
        /// <param name="button">
        /// The mouse button that was released.
        /// </param>
        protected virtual void OnWindowsMouseUpMessage(Point pos, MouseButton button)
        {
        }

        /// <summary>
        /// Override to handle the windows mouse wheel message.
        /// </summary>
        /// <param name="pos">
        /// The position of the cursor relative to the upper-left  corner of the client area.
        /// </param>
        /// <param name="delta">
        /// The distance the wheel is rotated. A positive value indicates that the wheel was
        /// rotated forward, away from the user; a negative value indicates that the wheel was
        /// rotated backward, toward the user.
        /// </param>
        protected virtual void OnWindowsMouseWheelMessage(Point pos, int delta)
        {
        }

        /// <summary>
        /// Give focus to the hosted window when using the keyboard to TAB into the control.
        /// </summary>
        /// <param name="request">
        /// Specifies whether focus should be set to the first tab stop or the last.
        /// </param>
        /// <returns>
        /// Always true.
        /// </returns>
        protected override bool TabIntoCore(TraversalRequest request)
        {
            User32.SetFocus(this._hwnd);
            return true;
        }

        /// <summary>
        /// This is supposed to allow us to tab out of the window, but the game appears to be
        /// consuming that event and we never receive it.
        /// </summary>
        /// <param name="msg">
        /// The message and the associated data.
        /// </param>
        /// <param name="modifiers">
        /// Modifier keys.
        /// </param>
        /// <returns>
        /// True if the focus has been shifted to another component; otherwise, false.
        /// </returns>
        protected override bool TranslateAcceleratorCore(ref MSG msg, ModifierKeys modifiers)
        {
            if (msg.message == (int)WindowMessage.KEYDOWN &&
                msg.wParam == (IntPtr)VirtualKey.TAB)
            {
                FocusNavigationDirection direction = FocusNavigationDirection.Next;
                if ((User32.GetKeyState(VirtualKey.SHIFT) & 0x8000) != 0)
                {
                    direction = FocusNavigationDirection.Previous;
                }

                return (this as IKeyboardInputSink).KeyboardInputSite.OnNoMoreTabStops(
                    new TraversalRequest(direction));
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// This is the WindowProc of the hosted Win32 window. The window proc allows you to
        /// handle messages, sent to the window.
        /// </summary>
        /// <param name="hwnd">
        /// Win32 handle to the hosted Win32 window.
        /// </param>
        /// <param name="message">
        /// Win32 message sent to the hosted Win32 window.
        /// </param>
        /// <param name="wordParam">
        /// The message's additional word parameter value.
        /// </param>
        /// <param name="longParam">
        /// The message's additional long parameter value.
        /// </param>
        /// <param name="handled">
        /// Set to true if the message is handled, false otherwise.
        /// </param>
        /// <returns>
        /// The appropriate return value depends on the particular message.
        /// </returns>
        protected override IntPtr WndProc(
            IntPtr hwnd, int message, IntPtr wordParam, IntPtr longParam, ref bool handled)
        {
            switch ((WindowMessage)message)
            {
                case WindowMessage.ERASEBKGND:
                    handled = true;
                    return IntPtr.Zero;

                case WindowMessage.PAINT:
                    // Start painting.
                    PAINTSTRUCT ps = new PAINTSTRUCT();
                    User32.BeginPaint(hwnd, out ps);

                    Rect rect = User32.GetClientRect(hwnd);

                    Color fillColor = this.FillColour;
                    int colour = (int)fillColor.B << 16;
                    colour |= (int)fillColor.G << 8;
                    colour |= (int)fillColor.R;
                    IntPtr colorPtr = Gdi32.CreateSolidBrush(colour);

                    // Do the actual painting
                    User32.FillRect(ps.Hdc, ref rect, colorPtr);

                    // Finish painting.
                    User32.EndPaint(hwnd, ref ps);
                    Gdi32.DeleteObject(colorPtr);

                    handled = true;
                    return IntPtr.Zero;

                case WindowMessage.MOUSEACTIVATE:
                    User32.SetFocus(this._hwnd);
                    break;

                case WindowMessage.KEYDOWN:
                    this.OnWindowsKeyDownMessage(
                        KeyInterop.KeyFromVirtualKey(wordParam.ToInt32()));
                    handled = true;
                    return IntPtr.Zero;

                case WindowMessage.KEYUP:
                    this.OnWindowsKeyUpMessage(
                        KeyInterop.KeyFromVirtualKey(wordParam.ToInt32()));
                    handled = true;
                    return IntPtr.Zero;

                case WindowMessage.MOUSEMOVE:
                    this.OnWindowsMouseMoveMessage(this.ConvertToPoint(longParam));
                    handled = true;
                    return IntPtr.Zero;

                case WindowMessage.MOUSEWHEEL:
                    this.OnWindowsMouseWheelMessage(
                        this.ConvertToPoint(longParam),
                        (int)this.GetWheelDeltaWParam(wordParam));
                    handled = true;
                    return IntPtr.Zero;

                case WindowMessage.LBUTTONDOWN:
                    User32.SetFocus(this._hwnd);
                    this.OnWindowsMouseDownMessage(
                        this.ConvertToPoint(longParam), MouseButton.Left);
                    handled = true;
                    return IntPtr.Zero;

                case WindowMessage.LBUTTONUP:
                    User32.SetFocus(this._hwnd);
                    this.OnWindowsMouseUpMessage(
                        this.ConvertToPoint(longParam), MouseButton.Left);
                    handled = true;
                    return IntPtr.Zero;

                case WindowMessage.RBUTTONDOWN:
                    User32.SetFocus(this._hwnd);
                    this.OnWindowsMouseDownMessage(
                        this.ConvertToPoint(longParam), MouseButton.Right);
                    handled = true;
                    return IntPtr.Zero;

                case WindowMessage.RBUTTONUP:
                    User32.SetFocus(this._hwnd);
                    this.OnWindowsMouseUpMessage(
                        this.ConvertToPoint(longParam), MouseButton.Right);
                    handled = true;
                    return IntPtr.Zero;

                case WindowMessage.MBUTTONDOWN:
                    User32.SetFocus(this._hwnd);
                    this.OnWindowsMouseDownMessage(
                        this.ConvertToPoint(longParam), MouseButton.Middle);
                    handled = true;
                    return IntPtr.Zero;

                case WindowMessage.MBUTTONUP:
                    User32.SetFocus(this._hwnd);
                    this.OnWindowsMouseUpMessage(
                        this.ConvertToPoint(longParam), MouseButton.Middle);
                    handled = true;
                    return IntPtr.Zero;
            }

            return base.WndProc(hwnd, message, wordParam, longParam, ref handled);
        }

        /// <summary>
        /// Registers a new window class for our hosted HWND.
        /// </summary>
        /// <returns>
        /// If the function succeeds, the return value is a class atom that uniquely identifies
        /// the class being registered. If the function fails, the return value is zero.
        /// </returns>
        private static IntPtr RegisterWindowClass()
        {
            WNDCLASS windowClass = default(WNDCLASS);
            windowClass.ClassExtraBytes = 0;
            windowClass.WindowExtraBytes = 0;
            windowClass.Background = new IntPtr(17);
            windowClass.Cursor = IntPtr.Zero;
            windowClass.Icon = IntPtr.Zero;
            windowClass.WndProc = _defaultWndProc;
            windowClass.ClassName = GameViewportHostClassName;
            windowClass.MenuName = null;
            windowClass.Style = 0u;

            return (IntPtr)User32.RegisterClass(ref windowClass);
        }

        /// <summary>
        /// Helper method for obtaining the x/y positions from an IntPtr.
        /// </summary>
        /// <param name="value">
        /// The value to convert to a point.
        /// </param>
        /// <returns>
        /// The converted point.
        /// </returns>
        private Point ConvertToPoint(IntPtr value)
        {
            int x = value.ToInt32() & 0x0000FFFF;
            int y = (int)((value.ToInt32() & 0xFFFF0000) >> 16);
            return new Point(x, y);
        }

        /// <summary>
        /// Helper method for extracting the HIWORD from a double word.
        /// </summary>
        /// <param name="value">
        /// The value whose hi word should be extracted.
        /// </param>
        /// <returns>
        /// The extracted hi word from the specified value.
        /// </returns>
        private ushort GetHiword(IntPtr value)
        {
            return (ushort)((long)value >> 16 & 0xFFFF);
        }

        /// <summary>
        /// Helper method for extracting the LOWORD from a double word.
        /// </summary>
        /// <param name="value">
        /// The value whose lo word should be extracted.
        /// </param>
        /// <returns>
        /// The extracted lo word from the specified value.
        /// </returns>
        private ushort GetLoword(IntPtr value)
        {
            return (ushort)((long)value & 0xFFFF);
        }

        /// <summary>
        /// Retrieves the mouse wheel delta value from the specified word parameter by
        /// extracting the hi word from it.
        /// </summary>
        /// <param name="wordParam">
        /// The parameter to extract the mouse wheel delta value from.
        /// </param>
        /// <returns>
        /// The mouse wheel delta value from the specified word parameter.
        /// </returns>
        private short GetWheelDeltaWParam(IntPtr wordParam)
        {
            return (short)this.GetHiword(wordParam);
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsGameViewportHost {Class}
} // RSG.Editor.Controls {Namespace}
