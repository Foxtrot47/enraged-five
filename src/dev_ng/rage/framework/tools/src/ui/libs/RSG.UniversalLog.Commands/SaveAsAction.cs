﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor;
using RSG.UniversalLog.ViewModel;

namespace RSG.UniversalLog.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public class SaveAsAction : ButtonAction<UniversalLogDataContext>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="OpenFileImplementer"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public SaveAsAction(ParameterResolverDelegate<UniversalLogDataContext> resolver)
            : base(resolver)
        {
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// Override to set logic against the can execute command handler.
        /// </summary>
        /// <param name="commandParameter"></param>
        /// <returns></returns>
        public override bool CanExecute(UniversalLogDataContext dc)
        {
            return dc.AnyFilesLoaded;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="commandParameter">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(UniversalLogDataContext dc)
        {
            ICommonDialogService dlgService = this.GetService<ICommonDialogService>();
            IMessageBoxService msgService = this.GetService<IMessageBoxService>();
            if (dlgService == null || msgService == null)
            {
                Debug.Assert(false, "Unable to save file as service is missing.");
                return;
            }

            String filter = "Universal Log Files (*.ulog)|*.ulog|All Files (*.*)|*.*";
            String destination;
            if (!dlgService.ShowSaveFile(null, null, filter, 0, out destination))
            {
                return;
            }

            // Save the file
            if (!dc.SaveFile(destination))
            {
                msgService.Show(
                    "An unexpected error occurred while attempting to save the file out.",
                    String.Empty,
                    System.Windows.MessageBoxButton.OK,
                    System.Windows.MessageBoxImage.Error);
            }
            else
            {
                msgService.Show(
                    String.Format("File successfully saved to '{0}'.", destination),
                    String.Empty,
                    System.Windows.MessageBoxButton.OK,
                    System.Windows.MessageBoxImage.Information);
            }
        }
        #endregion // Overrides
    } // SaveAsAction
}
