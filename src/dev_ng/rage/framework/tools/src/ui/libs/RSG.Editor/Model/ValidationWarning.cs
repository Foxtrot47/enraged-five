﻿//---------------------------------------------------------------------------------------------
// <copyright file="ValidationWarning.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Model
{
    /// <summary>
    /// Represents a warning that has occurred during validation.
    /// </summary>
    public class ValidationWarning : IValidationWarning
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Location"/> property.
        /// </summary>
        private FileLocation _location;

        /// <summary>
        /// The private field used for the <see cref="Message"/> property.
        /// </summary>
        private string _message;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ValidationWarning"/> class.
        /// </summary>
        /// <param name="message">
        /// The message that describes the warning.
        /// </param>
        public ValidationWarning(string message)
        {
            this._message = message;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ValidationWarning"/> class.
        /// </summary>
        /// <param name="message">
        /// The message that describes the warning.
        /// </param>
        /// <param name="location">
        /// The file location for the warning if applicable.
        /// </param>
        public ValidationWarning(string message, FileLocation location)
        {
            this._location = location;
            this._message = message;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether this object can provide information about the
        /// location of the warning inside a file.
        /// </summary>
        public bool HasLocationInformation
        {
            get { return this._location != null; }
        }

        /// <summary>
        /// Gets the file location that this warning is associated with.
        /// </summary>
        public FileLocation Location
        {
            get { return this._location ?? FileLocation.NotApplicable; }
        }

        /// <summary>
        /// Gets the message that describes the warning.
        /// </summary>
        public string Message
        {
            get { return this._message; }
        }
        #endregion Properties
    } // RSG.Editor.Model.ValidationWarning {Class}
} // RSG.Editor.Model {Namespace}
