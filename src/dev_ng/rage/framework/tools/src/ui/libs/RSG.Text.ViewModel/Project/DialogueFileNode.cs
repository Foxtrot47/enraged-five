﻿//---------------------------------------------------------------------------------------------
// <copyright file="DialogueFileNode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.ViewModel.Project
{
    using System;
    using System.IO;
    using System.Xml;
    using RSG.Base.Logging;
    using RSG.Editor.Model;
    using RSG.Project.Model;
    using RSG.Project.ViewModel;
    using RSG.Text.Model;
    using RSG.Text.ViewModel.Project.Controllers;

    /// <summary>
    /// 
    /// </summary>
    public class DialogueFileNode : FileNode
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DialogueFileNode"/> class.
        /// </summary>
        /// <param name="model">
        /// The model that this view model will be wrapping.
        /// </param>
        public DialogueFileNode(IHierarchyNode parent, ProjectItem model)
            : base(parent, model)
        {
            this.Icon = DialogueProjectIcons.DialogueFile;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the creates document content controller that can be used to create a content
        /// object for this object inside a document tab.
        /// </summary>
        public override ICreateDocumentContentController CreateDocumentContentController
        {
            get { return new CreateDocumentContentController(); }
        }
        #endregion Properties
    } // RSG.Text.ViewModel.Project.DialogueFileNode {Class}
} // RSG.Text.ViewModel.Project {Namespace}
