﻿//---------------------------------------------------------------------------------------------
// <copyright file="DocumentPane.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock.ViewModel
{
    /// <summary>
    /// Represents a group of document items that are grouped together inside a single pane.
    /// </summary>
    public class DocumentPane : DockingPane
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DocumentPane"/> class.
        /// </summary>
        /// <param name="viewSite">
        /// The view site that this pane is a element of.
        /// </param>
        public DocumentPane(ViewSite viewSite)
            : base(viewSite)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Inserts the specified child into this panes collection at the specified index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index where the item should be placed.
        /// </param>
        /// <param name="item">
        /// The item that should be added to this document pane.
        /// </param>
        public void InsertChild(int index, DocumentItem item)
        {
            base.InsertChild(index, item);
        }
        #endregion Methods
    } // RSG.Editor.Controls.Dock.ViewModel.DocumentPane {Class}
} // RSG.Editor.Controls.Dock.ViewModel {Namespace}
