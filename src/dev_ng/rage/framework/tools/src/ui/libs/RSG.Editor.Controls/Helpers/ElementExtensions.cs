﻿//---------------------------------------------------------------------------------------------
// <copyright file="ElementExtensions.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Helpers
{
    using System.Globalization;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;

    /// <summary>
    /// Static class used to add extensions onto UI elements to do with their current state.
    /// </summary>
    public static class ElementExtensions
    {
        #region Methods
        /// <summary>
        /// Determines whether the content inside the specified element is currently being
        /// trimmed.
        /// </summary>
        /// <param name="element">
        /// The text block to test.
        /// </param>
        /// <returns>
        /// True if the specified element is displaying its content trimmed; otherwise, false.
        /// </returns>
        public static bool IsTrimmed(this UIElement element)
        {
            TextBlock textBlock = element as TextBlock;
            return textBlock != null && textBlock.IsTextTrimmed();
        }

        /// <summary>
        /// Determines whether the text inside the specified text block is currently being
        /// trimmed.
        /// </summary>
        /// <param name="textBlock">
        /// The text block to test.
        /// </param>
        /// <returns>
        /// True if the specified text block is displaying its text trimmed; otherwise, false.
        /// </returns>
        public static bool IsTextTrimmed(this TextBlock textBlock)
        {
            if (textBlock.TextTrimming == TextTrimming.None)
            {
                return false;
            }

            TextFormattingMode mode = TextOptions.GetTextFormattingMode(textBlock);

            Typeface typeface =
                new Typeface(
                    textBlock.FontFamily,
                    textBlock.FontStyle,
                    textBlock.FontWeight,
                    textBlock.FontStretch);

            FormattedText formattedText =
                new FormattedText(
                    textBlock.Text,
                    CultureInfo.CurrentCulture,
                    textBlock.FlowDirection,
                    typeface,
                    textBlock.FontSize,
                    textBlock.Foreground,
                    new NumberSubstitution(),
                    mode);

            return formattedText.Width > textBlock.ActualWidth;
        }

        /// <summary>
        /// Determines whether the specified element is currently being clipped by it parent
        /// scroll viewer.
        /// </summary>
        /// <param name="element">
        /// The element to test.
        /// </param>
        /// <returns>
        /// True if the specified element is being clipped by its parent scroll viewer;
        /// otherwise, false.
        /// </returns>
        public static bool IsClipped(this UIElement element)
        {
            var scroll = element.GetVisualOrLogicalAncestor<ScrollContentPresenter>();
            if (scroll == null)
            {
                return false;
            }

            GeneralTransform transform = element.TransformToAncestor(scroll);
            Rect bounds = new Rect(0, 0, element.RenderSize.Width, element.RenderSize.Height);

            Rect rect = transform.TransformBounds(bounds);
            Rect rect2 = new Rect(0.0, 0.0, scroll.RenderSize.Width, scroll.RenderSize.Height);
            rect2.Intersect(rect);
            return !AreClose(rect, rect2);
        }

        /// <summary>
        /// Determines whether the two specified rectangles are close together. Which would
        /// mean that their top, left values were close together and their width and height
        /// were close together as well.
        /// </summary>
        /// <param name="rect1">
        /// The first rectangle to test.
        /// </param>
        /// <param name="rect2">
        /// The second rectangle to test.
        /// </param>
        /// <returns>
        /// True if the two specified rectangles are close together or equal; otherwise, false.
        /// </returns>
        private static bool AreClose(Rect rect1, Rect rect2)
        {
            if (!AreClose(rect1.Left, rect2.Left))
            {
                return false;
            }

            if (!AreClose(rect1.Top, rect2.Top))
            {
                return false;
            }

            if (!AreClose(rect1.Width, rect2.Width))
            {
                return false;
            }

            if (!AreClose(rect1.Height, rect2.Height))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Determines whether the two specified doubles are close together. Which would mean
        /// that their are equal or no more than a small (1.53E-06) value apart.
        /// </summary>
        /// <param name="value1">
        /// The first value to test.
        /// </param>
        /// <param name="value2">
        /// The second value to test.
        /// </param>
        /// <returns>
        /// True if the two specified values are close together or equal; otherwise, false.
        /// </returns>
        private static bool AreClose(double value1, double value2)
        {
            if (value1.IsNonreal() || value2.IsNonreal())
            {
                return value1.CompareTo(value2) == 0;
            }

            if (value1 == value2)
            {
                return true;
            }

            double difference = value1 - value2;
            return difference < 1.53E-06 && difference > -1.53E-06;
        }

        /// <summary>
        /// Determines whether the specified double has a non-real value or not.
        /// </summary>
        /// <param name="value">
        /// The value to test.
        /// </param>
        /// <returns>
        /// True if the specified double is non-real; otherwise, true.
        /// </returns>
        private static bool IsNonreal(this double value)
        {
            return double.IsNaN(value) || double.IsInfinity(value);
        }
        #endregion Methods
    } // RSG.Editor.Controls.Helpers.ElementExtensions {Class}
} // RSG.Editor.Controls.Helpers {Namespace}
