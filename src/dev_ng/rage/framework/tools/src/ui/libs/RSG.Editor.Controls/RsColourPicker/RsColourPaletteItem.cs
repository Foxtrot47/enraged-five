﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsColourPaletteItem.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;

    /// <summary>
    /// Represents a selectable item inside the <see cref="RsColourPalette"/> control.
    /// </summary>
    public class RsColourPaletteItem : ListBoxItem
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="Brush" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty BrushProperty;

        /// <summary>
        /// Identifies the <see cref="Colour" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty ColourProperty;

        /// <summary>
        /// Identifies the <see cref="Brush" /> dependency property.
        /// </summary>
        private static DependencyPropertyKey _brushPropertyKey;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsColourPaletteItem" /> class.
        /// </summary>
        static RsColourPaletteItem()
        {
            ColourProperty =
                DependencyProperty.Register(
                "Colour",
                typeof(Color),
                typeof(RsColourPaletteItem),
                new FrameworkPropertyMetadata(Colors.Transparent, OnColourChanged));

            _brushPropertyKey =
                DependencyProperty.RegisterReadOnly(
                "Brush",
                typeof(Brush),
                typeof(RsColourPaletteItem),
                new PropertyMetadata(Brushes.Transparent));

            BrushProperty = _brushPropertyKey.DependencyProperty;

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsColourPaletteItem),
                new FrameworkPropertyMetadata(typeof(RsColourPaletteItem)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsColourPaletteItem" /> class.
        /// </summary>
        public RsColourPaletteItem()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the brush that this palette item is currently using to display its value. This
        /// is separate to the colour value to support showing alpha.
        /// </summary>
        public Brush Brush
        {
            get { return (Brush)this.GetValue(BrushProperty); }
            private set { this.SetValue(_brushPropertyKey, value); }
        }

        /// <summary>
        /// Gets or sets the colour that is associated with this palette item. This is used to
        /// construct the brush.
        /// </summary>
        public Color Colour
        {
            get { return (Color)this.GetValue(ColourProperty); }
            set { this.SetValue(ColourProperty, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Called whenever the <see cref="Colour"/> dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The object whose <see cref="Colour"/> dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnColourChanged(object s, DependencyPropertyChangedEventArgs e)
        {
            RsColourPaletteItem item = s as RsColourPaletteItem;
            if (item == null)
            {
                Debug.Assert(false, "Incorrect dependency property changed cast.");
                return;
            }

            if (e.NewValue is Color)
            {
                Color colour = (Color)e.NewValue;
                Brush newBrush = null;
                if (colour.A != 255)
                {
                    Color startColour = Color.FromRgb(colour.R, colour.G, colour.B);
                    LinearGradientBrush linearBrush = new LinearGradientBrush();
                    linearBrush.StartPoint = new Point(0.0d, 0.0d);
                    linearBrush.EndPoint = new Point(0.0d, 1.0d);
                    linearBrush.GradientStops.Add(new GradientStop(startColour, 0.0d));
                    linearBrush.GradientStops.Add(new GradientStop(colour, 0.75f));
                    linearBrush.GradientStops.Add(new GradientStop(colour, 1.0f));
                    newBrush = linearBrush;
                }
                else
                {
                    newBrush = new SolidColorBrush(colour);
                }

                item.Brush = newBrush;
            }
            else
            {
                item.Brush = Brushes.Transparent;
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsColourPaletteItem {Class}
} // RSG.Editor.Controls {Namespace}
