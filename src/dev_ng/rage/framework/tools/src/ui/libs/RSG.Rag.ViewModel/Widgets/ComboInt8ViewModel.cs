﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Rag.Widgets;

namespace RSG.Rag.ViewModel.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    public class ComboInt8ViewModel : ComboViewModel<SByte>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        public ComboInt8ViewModel(WidgetCombo<SByte> widget)
            : base(widget)
        {
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public override int SelectedItem
        {
            get { return (int)(Widget.SelectedItem - (SByte)Widget.Offset); }
            set { Widget.SelectedItem = (SByte)(value + Widget.Offset); }
        }

        /// <summary>
        /// 
        /// </summary>
        public override string DefaultValue
        {
            get { return String.Format("{0} (Index: {1})", Widget.Items[Widget.DefaultItem], Widget.DefaultItem); }
        }
        #endregion // Properties
    } // ComboInt8ViewModel
}
