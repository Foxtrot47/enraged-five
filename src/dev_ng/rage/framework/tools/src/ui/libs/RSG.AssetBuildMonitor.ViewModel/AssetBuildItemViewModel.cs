﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssetBuildItem.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.AssetBuildMonitor.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using RSG.Configuration;
    using RSG.Editor;
    using RSG.Interop.Bugstar.Organisation;
    using RSG.Pipeline.Services.Engine;

    /// <summary>
    /// Asset Build Item View-Model.
    /// </summary>
    public class AssetBuildItemViewModel : 
        AssetBuildMonitorViewModelBase
    {
        #region Properties
        /// <summary>
        /// Gets the build item.
        /// </summary>
        public AssetBuildItem BuildItem
        {
            get { return _buildItem; }
            private set
            {
                this.SetProperty(ref _buildItem, value);
            }
        }

        /// <summary>
        /// Gets or sets the selection state of this item.
        /// </summary>
        public bool IsSelected
        {
            get { return _isSelected; }
            set { this.SetProperty(ref _isSelected, value); }
        }

        /// <summary>
        /// Gets the current build progress.
        /// </summary>
        public float Progress
        {
            get { return _progress; }
            private set { this.SetProperty(ref _progress, value); }
        }

        /// <summary>
        /// Bugstar User who owns the job.
        /// </summary>
        public User User
        {
            get { return _user; }
            private set
            {
                SetProperty(ref _user, value);
            }
        }

        /// <summary>
        /// User mailto: URI.
        /// </summary>
        public String MailToUri
        {
            get { return _mailToUri; }
            private set
            {
                SetProperty(ref _mailToUri, value, "MailToUri");
            }
        }

        /// <summary>
        /// Whether to display in UTC or local time.
        /// </summary>
        public bool UseUTC
        {
            get { return _useUTC; }
            set { SetProperty(ref _useUTC, value); }
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Associated build item model.
        /// </summary>
        private AssetBuildItem _buildItem;

        /// <summary>
        /// Selection state of this item.
        /// </summary>
        private bool _isSelected;

        /// <summary>
        /// Current build progress.
        /// </summary>
        private float _progress;

        /// <summary>
        /// Bugstar user object.
        /// </summary>
        private User _user;

        /// <summary>
        /// Mailto link support.
        /// </summary>
        private String _mailToUri;

        /// <summary>
        /// Use UTC.
        /// </summary>
        private bool _useUTC;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="buildItem"></param>
        public AssetBuildItemViewModel(AssetBuildItem buildItem, IEnumerable<User> users)
        {
            this.BuildItem = buildItem;
            this.Progress = 0.0f;

            switch (buildItem.State)
            {
                case AssetBuildState.Aborted:
                case AssetBuildState.Complete:
                case AssetBuildState.Failed:
                    this.Progress = 1.0f;
                    break;
            }

            if (!String.IsNullOrEmpty(buildItem.Username))
            {
                this.User = users.FirstOrDefault(u => u.UserName.Equals(buildItem.Username));
                if (null != this.User)
                {
                    String subject = String.Format("Asset Build");
                    this.MailToUri = String.Format("mailto:{0}?subject={1}", this.User.Email, subject);
                }
            }
        }
        #endregion // Constructor(s)

        #region AssetBuildMonitorViewModelBase Abstract Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void ViewModelValidation()
        {

        }
        #endregion // AssetBuildMonitorViewModelBase Abstract Methods
    }

} // RSG.AssetBuildMonitor.ViewModel namespace
