﻿//---------------------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.AssetBuildMonitor.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media.Imaging;


    /// <summary>
    /// Provides static properties that give access to Build Monitor related icons.
    /// </summary>
    public class CommandIcons
    {
        #region Member Data
        /// <summary>
        /// The private field used for the <see cref="Connect"/> property.
        /// </summary>
        private static BitmapSource _connect;

        /// <summary>
        /// The private field used for the <see cref="ConnectNew"/> property.
        /// </summary>
        private static BitmapSource _connectNew;
        
        /// <summary>
        /// A generic object that provides thread safety access to the image sources.
        /// </summary>
        private static Object _syncRoot = new Object();
        #endregion // Member Data

        #region Properties
        /// <summary>
        /// Gets the icon that shows a Connect icon.
        /// </summary>
        public static BitmapSource Connect
        {
            get
            {
                if (_connect == null)
                {
                    lock (_syncRoot)
                    {
                        if (_connect == null)
                        {
                            _connect = EnsureLoaded("Connect.png");
                        }
                    }
                }

                return _connect;
            }
        }

        /// <summary>
        /// Gets the icon that shows a Connect-New icon.
        /// </summary>
        public static BitmapSource ConnectNew
        {
            get
            {
                if (_connectNew == null)
                {
                    lock (_syncRoot)
                    {
                        if (_connectNew == null)
                        {
                            _connectNew = EnsureLoaded("ConnectNew.png");
                        }
                    }
                }

                return _connectNew;
            }
        }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Ensures that the specified image source is loaded with the specified resource name.
        /// </summary>
        /// <param name="source">
        /// The image source that should be loaded.
        /// </param>
        /// <param name="resourceName">
        /// The name of the resource the loaded image source should set as its source.
        /// </param>
        private static BitmapSource EnsureLoaded(String resourceName)
        {
            String assemblyName = typeof(CommandIcons).Assembly.GetName().Name;
            String bitmapPath = String.Format(CultureInfo.InvariantCulture,
                "pack://application:,,,/{0};component/Resources/{1}", assemblyName, resourceName);
            Uri uri = new Uri(bitmapPath, UriKind.RelativeOrAbsolute);
            return new BitmapImage(uri);
        }
        #endregion // Methods
    }

} // RSG.AssetBuildMonitor.Commands namespace
