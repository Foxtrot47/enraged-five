﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Rag.Widgets;

namespace RSG.Rag.ViewModel.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    public class ToggleBoolViewModel : ToggleViewModel<Boolean>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        public ToggleBoolViewModel(WidgetToggle<Boolean> widget)
            : base(widget)
        {
        }
        #endregion // Constructor(s)
    } // ToggleBoolViewModel
}
