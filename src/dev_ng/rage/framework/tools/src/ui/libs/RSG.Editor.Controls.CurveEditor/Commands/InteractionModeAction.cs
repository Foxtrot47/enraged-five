﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using RSG.Base.Extensions;

namespace RSG.Editor.Controls.CurveEditor.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public class InteractionModeAction : ComboAction<CurveEditor, InteractionMode>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="InteractionModeImplementer"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public InteractionModeAction(ParameterResolverDelegate<CurveEditor> resolver)
            : base(resolver)
        {
        }
        #endregion // Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="control">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="secondaryParameter">
        /// The secondary command parameter that has been sent with the command.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(CurveEditor control, MultiCommandParameter<InteractionMode> secondaryParameter)
        {
            if (control == null)
            {
                throw new SmartArgumentNullException(() => control);
            }

            return true;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="control">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="secondaryParameter">
        /// The secondary command parameter that has been sent with the command.
        /// </param>
        public override void Execute(CurveEditor control, MultiCommandParameter<InteractionMode> secondaryParameter)
        {
            if (control == null)
            {
                throw new SmartArgumentNullException(() => control);
            }

            control.InteractionMode = secondaryParameter.ItemParameter;
        }

        /// <summary>
        /// Provide the specified definition with the items to show in the filter.
        /// </summary>
        /// <param name="definition">
        /// The definition that the created items will be used for.
        /// </param>
        /// <returns>
        /// The items to show in the filter.
        /// </returns>
        protected override ObservableCollection<IMultiCommandItem> GetItems(
            MultiCommand definition)
        {
            // Add a filter item for all the interaction modes.
            ObservableCollection<IMultiCommandItem> items = new ObservableCollection<IMultiCommandItem>();
            foreach (InteractionMode mode in Enum.GetValues(typeof(InteractionMode)))
            {
                items.Add(new InteractionModeItem(definition, mode));
            }

            return items;
        }
        #endregion // Methods

        #region Classes
        /// <summary>
        /// Defines the item published for the items of the
        /// <see cref="FilterByLogLevelItem"/> command definition.
        /// </summary>
        private class InteractionModeItem : MultiCommandItem<InteractionMode>
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="FilterByLogLevelItem"/> class.
            /// </summary>
            /// <param name="definition">
            /// The multi command definition that owns this item.
            /// </param>
            /// <param name="level">
            /// The parameter that is sent when this item is executed.
            /// </param>
            /// <param name="dataGrid">
            /// The data grid this item is linked to.
            /// </param>
            public InteractionModeItem(MultiCommand definition, InteractionMode mode)
                : base(definition, mode.GetDisplayName(), mode)
            {
                if (mode == default(InteractionMode))
                {
                    IsToggled = true;
                }
            }
            #endregion // Constructors

            #region Properties
            /// <summary>
            /// Gets the System.Windows.Media.Imaging.BitmapSource object that represents the
            /// icon that is used for this item if it is being rendered as a toggle control.
            /// </summary>
            public override BitmapSource Icon
            {
                get { return CommandParameter.GetDisplayIcon(); }
            }

            /// <summary>
            /// 
            /// </summary>
            public override string Description
            {
                get { return CommandParameter.GetDisplayName(); }
            }
            #endregion Properties
        } // FilterByLogLevelItem
        #endregion // Classes
    } // InteractionModeAction
}
