﻿//---------------------------------------------------------------------------------------------
// <copyright file="SuspendUndoEngineMode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Model
{
    using System;

    /// <summary>
    /// Defines the different modes that can be undo engine can be suspended under.
    /// </summary>
    [Flags]
    public enum SuspendUndoEngineMode
    {
        /// <summary>
        /// Defines that the undo engine shouldn't produce any undo events.
        /// </summary>
        UndoEvents = 1 << 0,

        /// <summary>
        /// Defines that the undo engine shouldn't change its dirty state.
        /// </summary>
        DirtyState = 1 << 1,

        /// <summary>
        /// Defines that the undo engine shouldn't produce any undo events or change its dirty
        /// state.
        /// </summary>
        Both = SuspendUndoEngineMode.UndoEvents | SuspendUndoEngineMode.DirtyState,
    } // RSG.Editor.Model.SuspendUndoEngineMode {Enum}
} // RSG.Editor.Model {Namespace}
