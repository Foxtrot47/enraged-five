﻿//---------------------------------------------------------------------------------------------
// <copyright file="TextLineCommandArgs.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Commands
{
    using System.Collections.Generic;
    using System.Windows.Controls;
    using RSG.Text.ViewModel;

    /// <summary>
    /// A struct that contains the parameters needed for the text line commands to execute.
    /// </summary>
    public struct TextLineCommandArgs
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Selected"/> property.
        /// </summary>
        private IEnumerable<LineViewModel> _selected;

        /// <summary>
        /// The private field used for the <see cref="Conversation"/> property.
        /// </summary>
        private ConversationViewModel _conversation;

        /// <summary>
        /// The private field used for the <see cref="DataGrid"/> property.
        /// </summary>
        private DataGrid _dataGrid;

        private string _audioPath;
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets or sets the iterator over the currently selected lines.
        /// </summary>
        public IEnumerable<LineViewModel> Selected
        {
            get { return this._selected; }
            set { this._selected = value; }
        }

        /// <summary>
        /// Gets or sets the conversation that is currently selected.
        /// </summary>
        public ConversationViewModel Conversation
        {
            get { return this._conversation; }
            set { this._conversation = value; }
        }

        /// <summary>
        /// Gets or sets the data grid control containing the lines.
        /// </summary>
        public DataGrid DataGrid
        {
            get { return this._dataGrid; }
            set { this._dataGrid = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string AudioPath
        {
            get { return this._audioPath; }
            set { this._audioPath = value; }
        }

        #endregion Properties
    } // RSG.Text.Commands.TextLineCommandArgs {Structure}
} // RSG.Text.Commands {Namespace}
