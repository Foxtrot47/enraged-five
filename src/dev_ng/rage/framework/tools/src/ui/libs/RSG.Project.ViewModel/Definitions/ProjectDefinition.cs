﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProjectDefinition.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel.Definitions
{
    using System.Collections.Generic;
    using System.Windows.Media.Imaging;
    using RSG.Project.Model;

    /// <summary>
    /// Represents the definition for a single project type that includes all of the
    /// definitions for any valid item for the project.
    /// </summary>
    public abstract class ProjectDefinition : IDefinition
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ProjectDefinition"/> class.
        /// </summary>
        protected ProjectDefinition()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the description for this defined project.
        /// </summary>
        public abstract string Description { get; }

        /// <summary>
        /// Gets the name that is used as the default value for a new instance of this
        /// definition.
        /// </summary>
        public abstract string DefaultName { get; }

        /// <summary>
        /// Gets the extension used for this defined project.
        /// </summary>
        public abstract string Extension { get; }

        /// <summary>
        /// Gets the string used as the filter in a open or save dialog for this defined
        /// project.
        /// </summary>
        public abstract string Filter { get; }

        /// <summary>
        /// Gets the icon that is used to display this project.
        /// </summary>
        public abstract BitmapSource Icon { get; }

        /// <summary>
        /// Gets the name for this defined project.
        /// </summary>
        public abstract string Name { get; }

        /// <summary>
        /// Gets the filter to use in the Add Existing Item dialog window for this project.
        /// </summary>
        public abstract string NewItemFilter { get; }

        /// <summary>
        /// Gets a byte array containing the data that is used to create a new instance of the
        /// defined project.
        /// </summary>
        public abstract byte[] Template { get; }

        /// <summary>
        /// Gets the type of this item to be displayed in the add dialog.
        /// </summary>
        public abstract string Type { get; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Create the project node that will represent this project inside the project
        /// explorer.
        /// </summary>
        /// <returns>
        /// The project node to use in the explorer.
        /// </returns>
        public virtual ProjectNode CreateProjectNode()
        {
            return new ProjectNode(this);
        }

        /// <summary>
        /// Attempts to get the item definition for the specified extension.
        /// </summary>
        /// <param name="extension">
        /// The extension whose associated item definition should be retrieved.
        /// </param>
        /// <returns>
        /// The item definition that's associated with the specified extension.
        /// </returns>
        public virtual ProjectItemDefinition GetItemDefinition(string extension)
        {
            return null;
        }

        /// <summary>
        /// Gives the definition a opportunity to run a wizard before the project is created so
        /// that the user can set certain properties into the template before the file gets
        /// saved and replace all other variables.
        /// </summary>
        /// <param name="template">
        /// The template that is being used to create the project.
        /// </param>
        /// <param name="fullPath">
        /// The fullPath the new project is being saved to. So that relative paths can be
        /// determined.
        /// </param>
        /// <returns>
        /// A value indicating whether the wizard finished successfully. If false is returned
        /// the creation process is cancelled.
        /// </returns>
        /// <remarks>
        /// This method should modify the specified template with any additional properties
        /// from the wizard and replaced variables.
        /// </remarks>
        public virtual bool ReplaceVariables(ref byte[] template, string fullPath)
        {
            return true;
        }
        #endregion Methods
    } // RSG.Project.ViewModel.Definitions.ProjectDefinition {Class}
} // RSG.Project.ViewModel.Definitions {Namespace}
