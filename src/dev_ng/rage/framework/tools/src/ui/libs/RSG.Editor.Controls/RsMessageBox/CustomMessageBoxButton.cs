﻿//---------------------------------------------------------------------------------------------
// <copyright file="CustomMessageBoxButton.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    /// <summary>
    /// Represents a single button that has be defined for a <see cref="RsMessageBox"/>
    /// instance.
    /// </summary>
    internal class CustomMessageBoxButton
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Content"/> property.
        /// </summary>
        private string _content;

        /// <summary>
        /// The private field used for the <see cref="IsCancel"/> property.
        /// </summary>
        private bool _isCancel;

        /// <summary>
        /// The private field used for the <see cref="IsDefault"/> property.
        /// </summary>
        private bool _isDefault;

        /// <summary>
        /// The private field used for the <see cref="Value"/> property.
        /// </summary>
        private long _value;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CustomMessageBoxButton"/> class.
        /// </summary>
        /// <param name="content">
        /// The content for the button.
        /// </param>
        /// <param name="value">
        /// The value for the button.
        /// </param>
        public CustomMessageBoxButton(string content, long value)
        {
            this._content = content;
            this._value = value;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the content for the button.
        /// </summary>
        public string Content
        {
            get { return this._content; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this is the cancel button for the
        /// window and the button that will be executed when the user presses escape.
        /// </summary>
        public bool IsCancel
        {
            get { return this._isCancel; }
            set { this._isCancel = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this is the default button for the user
        /// to press.
        /// </summary>
        public bool IsDefault
        {
            get { return this._isDefault; }
            set { this._isDefault = value; }
        }

        /// <summary>
        /// Gets the value for the button.
        /// </summary>
        public long Value
        {
            get { return this._value; }
        }
        #endregion Properties
    } // RSG.Editor.Controls.CustomMessageBoxButton {Class}
} // RSG.Editor.Controls {Namespace}
