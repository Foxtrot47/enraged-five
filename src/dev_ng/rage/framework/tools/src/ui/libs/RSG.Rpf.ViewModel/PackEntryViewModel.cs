﻿//---------------------------------------------------------------------------------------------
// <copyright file="PackEntryViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Rpf.ViewModel
{
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Windows.Media.Imaging;
    using RSG.Editor;
    using RSG.ManagedRage;

    /// <summary>
    /// The view model that contains the data retrieved from a
    /// <see cref="RSG.ManagedRage.PackEntry"/> object.
    /// </summary>
    public class PackEntryViewModel : NotifyPropertyChangedBase
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Category"/> property.
        /// </summary>
        private PackEntryCategory _category;

        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private PackEntry _entry;

        /// <summary>
        /// The private field used for the <see cref="IncludedInFilter"/> property.
        /// </summary>
        private bool _includedInFilter;

        /// <summary>
        /// The private reference to the parent pack file view model.
        /// </summary>
        private PackFileViewModel _parent;

        /// <summary>
        /// The private field used for the <see cref="SearchHighlightIndex"/> property.
        /// </summary>
        private int _searchHighlightIndex;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="PackEntryViewModel"/> class.
        /// </summary>
        /// <param name="entry">
        /// The <see cref="RSG.ManagedRage.PackEntry"/> object that this view model is
        /// wrapping.
        /// </param>
        /// <param name="parent">
        /// The parent <see cref="PackFileViewModel"/> object which this view model belongs to.
        /// </param>
        public PackEntryViewModel(PackEntry entry, PackFileViewModel parent)
        {
            if (entry == null)
            {
                throw new SmartArgumentNullException(() => entry);
            }

            this._entry = entry;
            this._parent = parent;
            this._includedInFilter = true;
            this._searchHighlightIndex = -1;

            if (this._entry.IsResource)
            {
                this._category = PackEntryCategory.Resource;
            }
            else if (this._entry.IsFile)
            {
                this._category = PackEntryCategory.File;
            }
            else if (this._entry.IsDirectory)
            {
                this._category = PackEntryCategory.Directory;
            }
            else
            {
                this._category = PackEntryCategory.Unknown;
            }
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the category this entry represents.
        /// </summary>
        public PackEntryCategory Category
        {
            get { return this._category; }
        }

        /// <summary>
        /// Gets the name of the category this entry represents.
        /// </summary>
        public string CategoryName
        {
            get { return PackFileViewModel.CategoryNames[this._category]; }
        }

        /// <summary>
        /// Gets the icon bitmap object that represents the entry category.
        /// </summary>
        public BitmapSource IconBitmap
        {
            get
            {
                if (this._entry == null)
                {
                    return CommonIcons.UnknownFile;
                }

                if (this._entry.IsResource)
                {
                    return CommonIcons.Resource;
                }
                else if (this._entry.IsFile)
                {
                    return CommonIcons.BlankFile;
                }
                else if (this._entry.IsDirectory)
                {
                    return CommonIcons.ClosedFolder;
                }

                return CommonIcons.UnknownFile;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this object is currently included in a
        /// filtering or not.
        /// </summary>
        public bool IncludedInFilter
        {
            get { return this._includedInFilter; }
            set { this._includedInFilter = value; }
        }

        /// <summary>
        /// Gets the index of this entry.
        /// </summary>
        public uint Index
        {
            get
            {
                if (this._entry == null)
                {
                    return 0;
                }

                return this._entry.Index;
            }
        }

        /// <summary>
        /// Gets the name of this entry.
        /// </summary>
        public string Name
        {
            get
            {
                if (this._entry == null)
                {
                    return null;
                }

                return this._entry.Name;
            }
        }

        /// <summary>
        /// Gets the offset of this entry.
        /// </summary>
        public virtual uint Offset
        {
            get
            {
                if (this._entry == null)
                {
                    return 0;
                }

                return this._entry.FileOffset;
            }
        }

        /// <summary>
        /// Gets the compressed size in bytes of this entry.
        /// </summary>
        public uint Packed
        {
            get
            {
                if (this._entry == null)
                {
                    return 0;
                }

                return this._entry.ConsumedSize;
            }
        }

        /// <summary>
        /// Gets the parent file view model this entry is apart of.
        /// </summary>
        public PackFileViewModel Parent
        {
            get { return this._parent; }
        }

        /// <summary>
        /// Gets the path of this entry.
        /// </summary>
        public string Path
        {
            get
            {
                if (this._entry == null)
                {
                    return null;
                }

                return this._entry.Path;
            }
        }

        /// <summary>
        /// Gets the number of physical chunks this entry uses.
        /// </summary>
        public ulong PhysicalChunkCount
        {
            get
            {
                if (this._entry == null)
                {
                    return 0;
                }

                return this._entry.PhysicalChunkCount;
            }
        }

        /// <summary>
        /// Gets the number of physical chunks this entry uses as a string.
        /// </summary>
        public string PhysicalChunkCountString
        {
            get
            {
                if (this._entry == null)
                {
                    return null;
                }

                if (!this._entry.IsResource)
                {
                    return null;
                }

                return this._entry.PhysicalChunkCount.ToString(
                    "N0", CultureInfo.CurrentUICulture);
            }
        }

        /// <summary>
        /// Gets the physical chunk size in bytes of this entry.
        /// </summary>
        public ulong PhysicalChunkSize
        {
            get
            {
                if (this._entry == null)
                {
                    return 0;
                }

                return this._entry.PhysicalChunkSize;
            }
        }

        /// <summary>
        /// Gets the physical chunk size in bytes of this entry as a string.
        /// </summary>
        public string PhysicalChunkSizeString
        {
            get
            {
                if (this._entry == null)
                {
                    return null;
                }

                if (!this._entry.IsResource)
                {
                    return null;
                }

                return this._entry.PhysicalChunkSize.ToString(
                    "N0", CultureInfo.CurrentUICulture);
            }
        }

        /// <summary>
        /// Gets the physical size in bytes of this entry.
        /// </summary>
        public ulong PhysicalSize
        {
            get
            {
                if (this._entry == null)
                {
                    return 0;
                }

                return this._entry.PhysicalSize;
            }
        }

        /// <summary>
        /// Gets the physical size in bytes of this entry as a string.
        /// </summary>
        public string PhysicalSizeString
        {
            get
            {
                if (this._entry == null)
                {
                    return null;
                }

                if (!this._entry.IsResource)
                {
                    return null;
                }

                return this._entry.PhysicalSize.ToString("N0", CultureInfo.CurrentUICulture);
            }
        }

        /// <summary>
        /// Gets the ratio between the compressed size and the uncompressed size.
        /// </summary>
        public virtual float Ratio
        {
            get
            {
                float size = (float)this.Size;
                float packed = (float)this.Packed;
                if (size == 0.0f || packed == 0.0f)
                {
                    return 0.0f;
                }

                return (packed / size) * 100.0f;
            }
        }

        /// <summary>
        /// Gets or sets the search highlight index for this view model, used to show the
        /// navigation of the search results.
        /// </summary>
        public int SearchHighlightIndex
        {
            get { return this._searchHighlightIndex; }
            set { this.SetProperty(ref this._searchHighlightIndex, value); }
        }

        /// <summary>
        /// Gets the uncompressed size in bytes of this entry.
        /// </summary>
        public uint Size
        {
            get
            {
                if (this._entry == null)
                {
                    return 0;
                }

                return this._entry.UncompressedSize;
            }
        }

        /// <summary>
        /// Gets the version number of this entry.
        /// </summary>
        public int Version
        {
            get
            {
                if (this._entry == null)
                {
                    return 0;
                }

                return this._entry.Version;
            }
        }

        /// <summary>
        /// Gets the version number of this entry as a string.
        /// </summary>
        public string VersionString
        {
            get
            {
                if (this._entry == null)
                {
                    return null;
                }

                if (!this._entry.IsResource)
                {
                    return null;
                }

                return this._entry.Version.ToString("N0", CultureInfo.CurrentUICulture);
            }
        }

        /// <summary>
        /// Gets the number of virtual chunks this entry uses.
        /// </summary>
        public ulong VirtualChunkCount
        {
            get
            {
                if (this._entry == null)
                {
                    return 0;
                }

                return this._entry.VirtualChunkCount;
            }
        }

        /// <summary>
        /// Gets the number of virtual chunks this entry uses as a string.
        /// </summary>
        public string VirtualChunkCountString
        {
            get
            {
                if (this._entry == null)
                {
                    return null;
                }

                if (!this._entry.IsResource)
                {
                    return null;
                }

                return this._entry.VirtualChunkCount.ToString(
                    "N0", CultureInfo.CurrentUICulture);
            }
        }

        /// <summary>
        /// Gets the virtual chunk size in bytes of this entry.
        /// </summary>
        public ulong VirtualChunkSize
        {
            get
            {
                if (this._entry == null)
                {
                    return 0;
                }

                return this._entry.VirtualChunkSize;
            }
        }

        /// <summary>
        /// Gets the virtual chunk size in bytes of this entry as a string.
        /// </summary>
        public string VirtualChunkSizeString
        {
            get
            {
                if (this._entry == null)
                {
                    return null;
                }

                if (!this._entry.IsResource)
                {
                    return null;
                }

                return this._entry.VirtualChunkSize.ToString(
                    "N0", CultureInfo.CurrentUICulture);
            }
        }

        /// <summary>
        /// Gets the virtual size in bytes of this entry.
        /// </summary>
        public ulong VirtualSize
        {
            get
            {
                if (this._entry == null)
                {
                    return 0;
                }

                return this._entry.VirtualSize;
            }
        }

        /// <summary>
        /// Gets the virtual size in bytes of this entry as a string.
        /// </summary>
        public string VirtualSizeString
        {
            get
            {
                if (this._entry == null)
                {
                    return null;
                }

                if (!this._entry.IsResource)
                {
                    return null;
                }

                return this._entry.VirtualSize.ToString("N0", CultureInfo.CurrentUICulture);
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Extracts this entry to the specified destination.
        /// </summary>
        /// <param name="destination">
        /// The full path to where this entry should be extracted to.
        /// </param>
        /// <returns>
        /// True if the extraction was successful; otherwise, false.
        /// </returns>
        internal bool Extract(string destination)
        {
            using (MemoryStream memoryStream = this._parent.ExtractEntry(this))
            {
                if (memoryStream != null && memoryStream.Length != 0)
                {
                    using (FileStream fileStream = File.OpenWrite(destination))
                    {
                        memoryStream.WriteTo(fileStream);
                    }

                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Unloads this pack entry by disposing of the unmanaged resources.
        /// </summary>
        internal void UnloadPackEntry()
        {
            if (this._entry == null)
            {
                return;
            }

            using (this._entry)
            {
            }

            this._entry = null;
        }

        /// <summary>
        /// Extracts this entry to a temporary location and runs it using the shell execute.
        /// </summary>
        /// <returns>
        /// True if the extraction was successful; otherwise, false.
        /// </returns>
        internal bool View()
        {
            string destination = this.ExtractToTempLocation();
            if (destination == null)
            {
                return false;
            }

            if (!System.IO.File.Exists(destination))
            {
                return false;
            }

            Process.Start(destination);
            return true;
        }

        /// <summary>
        /// Extracts this entry to a temporary location and returns the full file path.
        /// </summary>
        /// <returns>
        /// The full file path to the extracted item.
        /// </returns>
        private string ExtractToTempLocation()
        {
            string directory = System.IO.Path.GetTempPath();
            string destination = System.IO.Path.Combine(directory, "tmp_" + this.Name);
            using (MemoryStream memoryStream = this._parent.ExtractEntry(this))
            {
                if (memoryStream != null && memoryStream.Length != 0)
                {
                    using (FileStream fileStream = File.OpenWrite(destination))
                    {
                        memoryStream.WriteTo(fileStream);
                    }

                    return destination;
                }
                else
                {
                    return null;
                }
            }
        }
        #endregion Methods
    } // RSG.Rpf.ViewModel.PackEntryViewModel {Class}
} // RSG.Rpf.ViewModel {Namespace}
