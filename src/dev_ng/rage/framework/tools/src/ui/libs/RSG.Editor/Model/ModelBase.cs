﻿//---------------------------------------------------------------------------------------------
// <copyright file="ModelBase.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Model
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using RSG.Base.Logging;

    /// <summary>
    /// Provides a abstract class that can be inherited by classes that wish to implement the
    /// <see cref="RSG.Editor.Model.IModel"/> interface.
    /// </summary>
    public abstract class ModelBase : NotifyPropertyChangedBase, IModel
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Parent"/> property.
        /// </summary>
        private IModel _parent;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ModelBase"/> class.
        /// </summary>
        protected ModelBase()
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ModelBase"/> class.
        /// </summary>
        /// <param name="parent">
        /// A reference to the parent model used for this model.
        /// </param>
        protected ModelBase(IModel parent)
        {
            this._parent = parent;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether this model can be reloaded from the source that
        /// initialised it.
        /// </summary>
        public virtual bool CanReload
        {
            get { return false; }
        }

        /// <summary>
        /// Gets a reference to this instances parent model object inside a model hierarchy if
        /// appropriate so that the undo engine can be set once on the root.
        /// </summary>
        public IModel Parent
        {
            get { return this._parent; }
        }

        /// <summary>
        /// Gets the top most model in the hierarchy this this model belongs to.
        /// </summary>
        public IModel RootParent
        {
            get
            {
                IModel currentParent = this;
                IModel root = null;
                while (currentParent != null)
                {
                    root = currentParent;
                    currentParent = currentParent.Parent;
                }

                return root;
            }
        }

        /// <summary>
        /// Gets the instance of the <see cref="RSG.Editor.Model.UndoEngine"/> class that this
        /// object is using as its undo engine.
        /// </summary>
        public virtual UndoEngine UndoEngine
        {
            get
            {
                if (this.Parent != null)
                {
                    return this.Parent.UndoEngine;
                }

                return null;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Changes the parent for this object to the one specified.
        /// </summary>
        /// <param name="newParent">
        /// The new parent for this object.
        /// </param>
        public void ChangeParent(IModel newParent)
        {
            bool updating = !Object.ReferenceEquals(newParent, this._parent);
            if (!updating)
            {
                Debug.Assert(updating, "Specified new parent is the same as the current one");
            }

            IModel oldValue = this._parent;
            this._parent = newParent;

            this.OnParentChanged(oldValue, this._parent);
        }

        /// <summary>
        /// Creates a new object that is a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A new object that is a deep copy of this instance.
        /// </returns>
        public object Clone()
        {
            return this.DeepClone();
        }

        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public virtual object DeepClone()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">
        /// An object to compare with this object.
        /// </param>
        /// <returns>
        /// True if the current object is equal to the other parameter; otherwise, false.
        /// </returns>
        public bool Equals(IModel other)
        {
            if (Object.ReferenceEquals(other, null))
            {
                return false;
            }

            if (Object.ReferenceEquals(other, this))
            {
                return true;
            }

            if (this.GetType() != other.GetType())
            {
                return false;
            }

            return this.EqualsCore(other);
        }

        /// <summary>
        /// Reloads the model from whichever source it was initialised with.
        /// </summary>
        public void ReloadModel()
        {
            if (!this.CanReload)
            {
                return;
            }

            this.ReloadModelCore();
        }

        /// <summary>
        /// Validates this entity and creates a validation result object containing the errors
        /// and warnings.
        /// </summary>
        /// <param name="recursive">
        /// A value indicating whether any child models to this one should also be validated.
        /// </param>
        /// <returns>
        /// A validation result object containing all of the errors and warnings associated
        /// with this validation pass.
        /// </returns>
        public virtual ValidationResult Validate(bool recursive)
        {
            return ValidationResult.Empty;
        }

        /// <summary>
        /// Validates this entity and adds the errors and warnings to the specified log object.
        /// </summary>
        /// <param name="log">
        /// The log object the errors and warnings for this entity should be added.
        /// </param>
        /// <param name="recursive">
        /// A value indicating whether any child models to this one should also be validated.
        /// </param>
        public void Validate(ILog log, bool recursive)
        {
            if (log == null)
            {
                Debug.Assert(log != null, "Unable to validate into a null log");
                return;
            }

            ValidationResult result = this.Validate(recursive);
            if (result == null)
            {
                Debug.Assert(result != null, "Unable to validate from a null result object.");
                return;
            }

            this.AddValidationResultToLog(result, log);
        }

        /// <summary>
        /// Creates a shallow copy of the current instance.
        /// </summary>
        /// <returns>
        /// A shallow copy of the current System.Object.
        /// </returns>
        public virtual object ShallowClone()
        {
            return this.MemberwiseClone();
        }

        /// <summary>
        /// Adds the errors and warnings that are currently inside the specified results object
        /// into the specified log object.
        /// </summary>
        /// <param name="result">
        /// The validation results object containing all of the errors and warnings.
        /// </param>
        /// <param name="log">
        /// The log object to add the errors and warning to.
        /// </param>
        protected virtual void AddValidationResultToLog(ValidationResult result, ILog log)
        {
        }

        /// <summary>
        /// Indicates whether the current object is equal to the specified object. When this
        /// method is called it has already been determined that the specified object is not
        /// equal to the current object or null through reference equality.
        /// </summary>
        /// <param name="other">
        /// An object to compare with this object.
        /// </param>
        /// <returns>
        /// True if the current object is equal to the other parameter; otherwise, false.
        /// </returns>
        protected virtual bool EqualsCore(IModel other)
        {
            return false;
        }

        /// <summary>
        /// Override to handle the logic whenever the parent for this object has been changed
        /// after initialisation.
        /// </summary>
        /// <param name="oldValue">
        /// The old parent object.
        /// </param>
        /// <param name="newValue">
        /// The new parent object.
        /// </param>
        protected virtual void OnParentChanged(IModel oldValue, IModel newValue)
        {
        }

        /// <summary>
        /// Reloads the model from whichever source it was initialised with. This method is
        /// only called if the <see cref="CanReload"/> property returns true.
        /// </summary>
        protected virtual void ReloadModelCore()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Sets the given field to the given value making sure the correct property changed
        /// event is fired.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the field that is getting set.
        /// </typeparam>
        /// <param name="field">
        /// A reference to the field that is getting set.
        /// </param>
        /// <param name="value">
        /// The value to set the field to.
        /// </param>
        /// <param name="name">
        /// The name of the property that has been changed.
        /// </param>
        /// <returns>
        /// True if the property was changed; otherwise, false.
        /// </returns>
        /// <remarks>
        /// This only works if the name of the field is in the format "_{property_name}".
        /// </remarks>
        protected bool SetField<T>(ref T field, T value, [CallerMemberName]string name = "")
        {
            return this.SetField(ref field, value, Enumerable.Repeat(name, 1));
        }

        /// <summary>
        /// Sets the given field to the given value making sure the correct property changed
        /// event is fired.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the field that is getting set.
        /// </typeparam>
        /// <param name="field">
        /// A reference to the field that is getting set.
        /// </param>
        /// <param name="value">
        /// The value to set the field to.
        /// </param>
        /// <param name="comparer">
        /// The equality comparer to use to determine whether the value has changed.
        /// </param>
        /// <param name="names">
        /// A iterator around all of property names that are changed due to this one change.
        /// </param>
        /// <returns>
        /// True if the property was changed; otherwise, false.
        /// </returns>
        protected override bool SetProperty<T>(
            ref T field, T value, IEqualityComparer<T> comparer, IEnumerable<string> names)
        {
            T oldValue = field;
            if (!base.SetProperty<T>(ref field, value, comparer, names))
            {
                return false;
            }

            UndoEngine undoEngine = this.UndoEngine;
            if (undoEngine != null && undoEngine.PerformingEvent == false)
            {
                string name = names.FirstOrDefault();
                PropertyChange<T> change = new PropertyChange<T>(oldValue, value, this, name);
                undoEngine.CreateUndoEvent(change);
            }

            return true;
        }

        /// <summary>
        /// Sets the given field to the given value making sure the correct property changed
        /// event is fired.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the field that is getting set.
        /// </typeparam>
        /// <param name="field">
        /// A reference to the field that is getting set.
        /// </param>
        /// <param name="value">
        /// The value to set the field to.
        /// </param>
        /// <param name="names">
        /// A iterator around all of property names that are changed due to this one change.
        /// </param>
        /// <returns>
        /// True if the field was changed; otherwise, false.
        /// </returns>
        /// <remarks>
        /// This only works if the name of the field is in the format "_{property_name}".
        /// </remarks>
        private bool SetField<T>(ref T field, T value, IEnumerable<string> names)
        {
            T oldValue = field;
            if (!base.SetProperty<T>(ref field, value, EqualityComparer<T>.Default, names))
            {
                return false;
            }

            UndoEngine undoEngine = this.UndoEngine;
            if (undoEngine != null && undoEngine.PerformingEvent == false)
            {
#if DEBUG
                string fieldName = names.FirstOrDefault();
                fieldName = "_" + fieldName.ToLower();
                BindingFlags flags =
                    BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
                if (this.GetType().GetField(fieldName, flags) == null)
                {
                    Debug.Assert(false, "Undo/redo will not work as field doesn't exist");
                }
#endif

                string name = names.FirstOrDefault();
                FieldChange<T> change = new FieldChange<T>(oldValue, value, this, name);
                undoEngine.CreateUndoEvent(change);
            }

            return true;
        }
        #endregion Methods
    } // RSG.Editor.Model.ModelBase {Class}
} // RSG.Editor.Model {Namespace}
