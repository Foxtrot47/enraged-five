﻿//---------------------------------------------------------------------------------------------
// <copyright file="RockstarRoutedCommand.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Windows.Input;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Defines an ICommand that is routed through the element tree and contains a icon,
    /// description, and category property.
    /// </summary>
    public class RockstarRoutedCommand : RoutedCommand
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Category"/> property.
        /// </summary>
        private string _category;

        /// <summary>
        /// The private field used for the <see cref="Description"/> property.
        /// </summary>
        private string _description;

        /// <summary>
        /// The private field used for the <see cref="Icon"/> property.
        /// </summary>
        private BitmapSource _icon;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="RockstarRoutedCommand" /> class.
        /// </summary>
        /// <param name="name">
        /// Declared name for serialization.
        /// </param>
        /// <param name="ownerType">
        /// The type that is registering the command.
        /// </param>
        /// <param name="inputGestures">
        /// Default input gestures associated with this command.
        /// </param>
        /// <param name="category">
        /// The name of the category this command belongs to.
        /// </param>
        /// <param name="description">
        /// The description for this command.
        /// </param>
        /// <param name="icon">
        /// The icon this command should use.
        /// </param>
        public RockstarRoutedCommand(
            string name,
            Type ownerType,
            InputGestureCollection inputGestures,
            string category,
            string description,
            BitmapSource icon)
            : base(name, ownerType, inputGestures)
        {
            this._category = category;
            this._description = description;
            this._icon = icon;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the category name this command belongs to.
        /// </summary>
        public string Category
        {
            get { return this._category; }
        }

        /// <summary>
        /// Gets the description for this command.
        /// </summary>
        public string Description
        {
            get { return this._description; }
        }

        /// <summary>
        /// Gets the icon for this command.
        /// </summary>
        public BitmapSource Icon
        {
            get { return this._icon; }
        }
        #endregion Properties
    }  // RSG.Editor.RockstarRoutedCommand {Class}
} // RSG.Editor {Namespace}
