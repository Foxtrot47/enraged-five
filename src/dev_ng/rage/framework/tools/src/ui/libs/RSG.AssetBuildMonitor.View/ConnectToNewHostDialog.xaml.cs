﻿//---------------------------------------------------------------------------------------------
// <copyright file="ConnectToNewHostDialog.xaml.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.AssetBuildMonitor.ViewModel
{
    using System;
    using System.Windows;
    using RSG.Editor.Controls;

    /// <summary>
    /// Interaction logic for ConnectToNewHostDialog.xaml
    /// </summary>
    public partial class ConnectToNewHostDialog : RsWindow
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ConnectToNewHostDialog()
        {
            InitializeComponent();
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OkButtonClick(Object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
        #endregion // Private Methods
    }

} // RSG.AssetBuildMonitor.ViewModel namespace
