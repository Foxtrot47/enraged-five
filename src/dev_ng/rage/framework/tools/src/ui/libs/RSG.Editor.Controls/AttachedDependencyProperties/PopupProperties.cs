﻿//---------------------------------------------------------------------------------------------
// <copyright file="PopupProperties.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.AttachedDependencyProperties
{
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;

    /// <summary>
    /// Contains attached properties that are related to the popup control or controls inside
    /// a popup.
    /// </summary>
    public static class PopupProperties
    {
        #region Fields
        /// <summary>
        /// Identifies the SupportValidationInPopup dependency property.
        /// </summary>
        public static readonly DependencyProperty SupportValidationInPopupProperty;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="PopupProperties"/> class.
        /// </summary>
        static PopupProperties()
        {
            SupportValidationInPopupProperty =
                DependencyProperty.RegisterAttached(
                "SupportValidationInPopup",
                typeof(bool),
                typeof(PopupProperties),
                new PropertyMetadata(false, OnSupportValidationInPopupChanged));
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Retrieves the SupportValidationInPopup dependency property value attached to the
        /// specified object.
        /// </summary>
        /// <param name="element">
        /// The object whose attached SupportValidationInPopup dependency property value will
        /// be returned.
        /// </param>
        /// <returns>
        /// The SupportValidationInPopup dependency property value attached to the specified
        /// object.
        /// </returns>
        public static bool GetSupportValidationInPopup(DependencyObject element)
        {
            return (bool)element.GetValue(SupportValidationInPopupProperty);
        }

        /// <summary>
        /// Sets the SupportValidationInPopup dependency property value on the specified object
        /// to the specified value.
        /// </summary>
        /// <param name="element">
        /// The object whose attached SupportValidationInPopup dependency property value will
        /// be set.
        /// </param>
        /// <param name="value">
        /// The value to set the attached SupportValidationInPopup dependency property to on
        /// the specified object.
        /// </param>
        public static void SetSupportValidationInPopup(DependencyObject element, bool value)
        {
            element.SetValue(SupportValidationInPopupProperty, value);
        }

        /// <summary>
        /// Called when a framework element object loads so that the error template can be
        /// reinitialised.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// Always EventArgs.Empty.
        /// </param>
        private static void OnLoaded(object sender, EventArgs e)
        {
            FrameworkElement element = sender as FrameworkElement;
            if (element == null || !Validation.GetHasError(element))
            {
                return;
            }

            ControlTemplate errorTemplate = Validation.GetErrorTemplate(element);
            Validation.SetErrorTemplate(element, null);
            Validation.SetErrorTemplate(element, errorTemplate);
        }

        /// <summary>
        /// Called whenever the SupportValidationInPopup dependency property changes.
        /// </summary>
        /// <param name="d">
        /// The object whose SupportValidationInPopup dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnSupportValidationInPopupChanged(
            DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FrameworkElement element = d as FrameworkElement;
            if (element == null)
            {
                throw new NotSupportedException(
                    "SupportValidationInPopup only valid on Framework Elements");
            }

            if ((bool)e.NewValue)
            {
                element.Loaded += OnLoaded;
            }
            else
            {
                element.Loaded -= OnLoaded;
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.AttachedDependencyProperties.PopupProperties {Class}
} // RSG.Editor.Controls.AttachedDependencyProperties {Namespace}
