﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace RSG.Editor.Controls.ZoomCanvas.Commands
{
    /// <summary>
    /// The action logic that is responsible for zooming the zoomable canvas by a
    /// predetermined amount in the specified direction.
    /// </summary>
    public class ZoomAction : ButtonAction<ZoomableCanvas, ZoomCommandArgs>
    {
        #region Constants
        /// <summary>
        /// Increment to zoom in.
        /// </summary>
        private static readonly double _zoomInAmount = Math.Pow(2, (1.0 / 3));

        /// <summary>
        /// Amount to zoom out.
        /// </summary>
        private static readonly double _zoomOutAmount = Math.Pow(2, -(1.0 / 3));

        /// <summary>
        /// Increment to zoom in.
        /// </summary>
        public static readonly ZoomCommandArgs ZoomInAboutCenterParameters =
            new ZoomCommandArgs(new Vector(_zoomInAmount, _zoomInAmount));

        /// <summary>
        /// Amount to zoom out.
        /// </summary>
        public static readonly ZoomCommandArgs ZoomOutAboutCenterParameters =
            new ZoomCommandArgs(new Vector(_zoomOutAmount, _zoomOutAmount));
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="ZoomAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public ZoomAction(ParameterResolverDelegate<ZoomableCanvas> resolver)
            : base(resolver)
        {
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="canvas">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="args">
        /// Zoom arguments.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ZoomableCanvas canvas, ZoomCommandArgs args)
        {
            bool atMinX = canvas.Scale.X == canvas.MinScale.X;
            bool atMaxX = canvas.Scale.X == canvas.MaxScale.X;
            bool atMinY = canvas.Scale.Y == canvas.MinScale.Y;
            bool atMaxY = canvas.Scale.Y == canvas.MaxScale.Y;

            // Not at one of the limits or we're at a limit and scale the right way.
            bool canZoomX = !(atMinX || atMaxX) ||
                (atMinX && args.Amount.X >= 1.0) ||
                (atMaxX && args.Amount.X <= 1.0);
            bool canZoomY = !(atMinY || atMaxY) ||
                (atMinY && args.Amount.Y >= 1.0) ||
                (atMaxY && args.Amount.Y <= 1.0);

            return canZoomX || canZoomY;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="control">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="args">
        /// Zoom arguments
        /// </param>
        public override void Execute(ZoomableCanvas control, ZoomCommandArgs args)
        {
            Vector zoomPoint = (Vector)args.ZoomAboutPoint;
            if (args.ZoomAboutCenter)
            {
                zoomPoint = new Vector(
                   control.ActualViewbox.Width * control.Scale.X / 2.0,
                   control.ActualViewbox.Height * control.Scale.Y / 2.0);
            }

            // Update the controls scale.
            Vector oldScale = control.Scale;
            Vector newScale = oldScale;
            newScale.X *= args.Amount.X;
            newScale.Y *= args.Amount.Y;
            control.Scale = newScale;

            // Check how much the scale actually changed (might be limited by min/max values).
            Vector actualAmount = new Vector(control.Scale.X / oldScale.X, control.Scale.Y / oldScale.Y);

            // Update the controls offset.
            Vector newOffset = (Vector)(control.Offset + zoomPoint);
            newOffset.X *= actualAmount.X;
            newOffset.Y *= actualAmount.Y;
            newOffset -= zoomPoint;
            control.Offset = (Point)newOffset;
        }
        #endregion
    }
}
