﻿//---------------------------------------------------------------------------------------------
// <copyright file="CommandBarItem.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;

    /// <summary>
    /// Provides a abstract base class to all of the objects that will be placed within the
    /// command bar hierarchy.
    /// </summary>
    public abstract class CommandBarItem : NotifyPropertyChangedBase
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="AreMultiItemsShown"/> property.
        /// </summary>
        private bool _areMultiItemsShown;

        /// <summary>
        /// The private field used for the <see cref="EachItemStartsGroup"/> property.
        /// </summary>
        private bool _eachItemStartsGroup;

        /// <summary>
        /// The private field used for the <see cref="Id"/> property.
        /// </summary>
        private Guid _id;

        /// <summary>
        /// The private field used for the <see cref="IsVisible"/> property.
        /// </summary>
        private bool _isVisible;

        /// <summary>
        /// The private field used for the <see cref="ParentId"/> property.
        /// </summary>
        private Guid _parentId;

        /// <summary>
        /// The private field used for the <see cref="Priority"/> property.
        /// </summary>
        private ushort _priority;

        /// <summary>
        /// The private field used for the <see cref="ShowFilterCount"/> property.
        /// </summary>
        private bool _showFilterCount;

        /// <summary>
        /// The private field used for the <see cref="StartsGroup"/> property.
        /// </summary>
        private bool _startsGroup;

        /// <summary>
        /// The private field used for the <see cref="Text"/> property.
        /// </summary>
        private string _text;

        /// <summary>
        /// The private field used for the <see cref="Width"/> property.
        /// </summary>
        private int _width;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CommandBarItem"/> class.
        /// </summary>
        /// <param name="priority">
        /// The initial priority that this command bar item should be given.
        /// </param>
        /// <param name="startsGroup">
        /// A value indicating whether this command bar item should start a new group. (I.e.
        /// Placed directly after a separator).
        /// </param>
        /// <param name="text">
        /// The text for this command bar item.
        /// </param>
        /// <param name="id">
        /// The unique identifier that is used to reference this item.
        /// </param>
        /// <param name="parentId">
        /// The unique identifier for the parent of this item inside the command bar hierarchy.
        /// </param>
        protected CommandBarItem(
            ushort priority, bool startsGroup, string text, Guid id, Guid parentId)
        {
            this._id = id;
            this._parentId = parentId;
            this._priority = priority;
            this._startsGroup = startsGroup;
            this._showFilterCount = false;
            this._text = text;
            this._isVisible = true;
            this._width = 160;
            this._eachItemStartsGroup = false;
            this._areMultiItemsShown = false;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether the individual items are shown to the user
        /// for this instance.
        /// </summary>
        public virtual bool AreMultiItemsShown
        {
            get { return this._areMultiItemsShown; }
            set { this.SetProperty(ref this._areMultiItemsShown, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the individual items start a group each or
        /// not. Only applicable when instancing a multi command with
        /// <see cref="AreMultiItemsShown"/> set to true.
        /// </summary>
        public virtual bool EachItemStartsGroup
        {
            get { return this._eachItemStartsGroup; }
            set { this.SetProperty(ref this._eachItemStartsGroup, value); }
        }

        /// <summary>
        /// Gets or sets the global unique identifier that is used to reference this item.
        /// </summary>
        public Guid Id
        {
            get { return this._id; }
            protected set { this._id = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this command bar item is visible to the
        /// user.
        /// </summary>
        public virtual bool IsVisible
        {
            get { return this._isVisible; }
            set { this.SetProperty(ref this._isVisible, value); }
        }

        /// <summary>
        /// Gets or sets the global identifier for the parent of this item inside the command
        /// bar hierarchy.
        /// </summary>
        public Guid ParentId
        {
            get { return this._parentId; }
            protected set { this._parentId = value; }
        }

        /// <summary>
        /// Gets or sets the priority of the item which indicates the position of it relative
        /// to the other items under the same parent.
        /// </summary>
        public virtual ushort Priority
        {
            get { return this._priority; }
            set { this.SetProperty(ref this._priority, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the filter check count is shown to the
        /// user in the user interface.
        /// </summary>
        public bool ShowFilterCount
        {
            get { return this._showFilterCount; }
            set { this.SetProperty(ref this._showFilterCount, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this item is proceeded by a separator to
        /// indicate the start of a new group inside the command bar hierarchy.
        /// </summary>
        public virtual bool StartsGroup
        {
            get { return this._startsGroup; }
            set { this.SetProperty(ref this._startsGroup, value); }
        }

        /// <summary>
        /// Gets or sets the text for this command bar item. This appears on the menu item in
        /// the hierarchy.
        /// </summary>
        public virtual string Text
        {
            get { return this._text; }
            set { this.SetProperty(ref this._text, value); }
        }

        /// <summary>
        /// Gets or sets the width of the control used for this command instance.
        /// </summary>
        public int Width
        {
            get { return this._width; }
            set { this.SetProperty(ref this._width, value); }
        }
        #endregion Properties
    }  // RSG.Editor.CommandBarItem {Class}
} // RSG.Editor {Namespace}
