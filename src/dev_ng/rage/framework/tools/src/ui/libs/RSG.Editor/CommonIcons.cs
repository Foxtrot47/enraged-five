﻿//---------------------------------------------------------------------------------------------
// <copyright file="CommonIcons.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Globalization;
    using System.Windows.Media.Imaging;
    using RSG.Base.Extensions;

    /// <summary>
    /// Provides static properties that give access to common icons that can be used throughout
    /// a application.
    /// </summary>
    public static class CommonIcons
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="ActionAdd"/> property.
        /// </summary>
        private static BitmapSource _actionAdd;

        /// <summary>
        /// The private field used for the <see cref="ArrowDown"/> property.
        /// </summary>
        private static BitmapSource _arrowDown;

        /// <summary>
        /// The private field used for the <see cref="ArrowUp"/> property.
        /// </summary>
        private static BitmapSource _arrowUp;

        /// <summary>
        /// The private field used for the <see cref="BlankFile"/> property.
        /// </summary>
        private static BitmapSource _blankFile;

        /// <summary>
        /// The private field used for the <see cref="Build"/> property.
        /// </summary>
        private static BitmapSource _build;

        /// <summary>
        /// The private field used for the <see cref="Clear"/> property.
        /// </summary>
        private static BitmapSource _clear;

        /// <summary>
        /// The private field used for the <see cref="Close"/> property.
        /// </summary>
        private static BitmapSource _close;

        /// <summary>
        /// The private field used for the <see cref="ClosedFolder"/> property.
        /// </summary>
        private static BitmapSource _closedFolder;

        /// <summary>
        /// The private field used for the <see cref="Collapse"/> property.
        /// </summary>
        private static BitmapSource _collapse;

        /// <summary>
        /// The private field used for the <see cref="CollapseAll"/> property.
        /// </summary>
        private static BitmapSource _collapseAll;

        /// <summary>
        /// The private field used for the <see cref="Copy"/> property.
        /// </summary>
        private static BitmapSource _copy;

        /// <summary>
        /// The private field used for the <see cref="Cut"/> property.
        /// </summary>
        private static BitmapSource _cut;

        /// <summary>
        /// The private field used for the <see cref="Delete"/> property.
        /// </summary>
        private static BitmapSource _delete;

        /// <summary>
        /// The private field used for the <see cref="Error"/> property.
        /// </summary>
        private static BitmapSource _error;

        /// <summary>
        /// The private field used for the <see cref="Expand"/> property.
        /// </summary>
        private static BitmapSource _expand;

        /// <summary>
        /// The private field used for the <see cref="ExpandAll"/> property.
        /// </summary>
        private static BitmapSource _expandAll;

        /// <summary>
        /// The private field used for the <see cref="FindInFiles"/> property.
        /// </summary>
        private static BitmapSource _findInFiles;

        /// <summary>
        /// The private field used for the <see cref="FindNext"/> property.
        /// </summary>
        private static BitmapSource _findNext;

        /// <summary>
        /// The private field used for the <see cref="FindPrevious"/> property.
        /// </summary>
        private static BitmapSource _findPrevious;

        /// <summary>
        /// The private field used for the <see cref="Gears"/> property.
        /// </summary>
        private static BitmapSource _gears;

        /// <summary>
        /// The private field used for the <see cref="Help"/> property.
        /// </summary>
        private static BitmapSource _help;

        /// <summary>
        /// The private field used for the <see cref="Lock"/> property.
        /// </summary>
        private static BitmapSource _lock;

        /// <summary>
        /// The private field used for the <see cref="NewFile"/> property.
        /// </summary>
        private static BitmapSource _newFile;

        /// <summary>
        /// The private field used for the <see cref="OpenedFolder"/> property.
        /// </summary>
        private static BitmapSource _openedFolder;

        /// <summary>
        /// The private field used for the <see cref="OpenFile"/> property.
        /// </summary>
        private static BitmapSource _openFile;

        /// <summary>
        /// The private field used for the <see cref="Paste"/> property.
        /// </summary>
        private static BitmapSource _paste;

        /// <summary>
        /// The private field used for the <see cref="Playstation"/> property.
        /// </summary>
        private static BitmapSource _playstation;

        /// <summary>
        /// The private field used for the <see cref="QuickFind"/> property.
        /// </summary>
        private static BitmapSource _quickFind;

        /// <summary>
        /// The private field used for the <see cref="QuickReplace"/> property.
        /// </summary>
        private static BitmapSource _quickReplace;

        /// <summary>
        /// The private field used for the <see cref="Redo"/> property.
        /// </summary>
        private static BitmapSource _redo;

        /// <summary>
        /// The private field used for the <see cref="Refresh"/> property.
        /// </summary>
        private static BitmapSource _refresh;

        /// <summary>
        /// The private field used for the <see cref="Rename"/> property.
        /// </summary>
        private static BitmapSource _rename;

        /// <summary>
        /// The private field used for the <see cref="ReplaceInFiles"/> property.
        /// </summary>
        private static BitmapSource _replaceInFiles;

        /// <summary>
        /// The private field used for the <see cref="ReportBug"/> property.
        /// </summary>
        private static BitmapSource _reportBug;

        /// <summary>
        /// The private field used for the <see cref="Resource"/> property.
        /// </summary>
        private static BitmapSource _resource;

        /// <summary>
        /// The private field used for the <see cref="Restart"/> property.
        /// </summary>
        private static BitmapSource _restart;

        /// <summary>
        /// The private field used for the <see cref="Save"/> property.
        /// </summary>
        private static BitmapSource _save;

        /// <summary>
        /// The private field used for the <see cref="SaveAll"/> property.
        /// </summary>
        private static BitmapSource _saveAll;

        /// <summary>
        /// The private field used for the <see cref="Search"/> property.
        /// </summary>
        private static BitmapSource _search;

        /// <summary>
        /// The private field used for the <see cref="SelectAll"/> property.
        /// </summary>
        private static BitmapSource _selectAll;

        /// <summary>
        /// A generic object that provides thread safety access to the image sources.
        /// </summary>
        private static object _syncRoot = new object();

        /// <summary>
        /// The private field used for the <see cref="Undo"/> property.
        /// </summary>
        private static BitmapSource _undo;

        /// <summary>
        /// The private field used for the <see cref="UnknownFile"/> property.
        /// </summary>
        private static BitmapSource _unknownFile;

        /// <summary>
        /// The private field used for the <see cref="ViewDocument"/> property.
        /// </summary>
        private static BitmapSource _viewDocument;

        /// <summary>
        /// The private field used for the <see cref="Warning"/> property.
        /// </summary>
        private static BitmapSource _warning;

        /// <summary>
        /// The private field used for the <see cref="Windows"/> property.
        /// </summary>
        private static BitmapSource _windows;

        /// <summary>
        /// The private field used for the <see cref="XBox"/> property.
        /// </summary>
        private static BitmapSource _xbox;

        /// <summary>
        /// The private field used for the <see cref="Filter"/> property.
        /// </summary>
        private static BitmapSource _filter;

        /// <summary>
        /// The private field used for the <see cref="MatchCase"/> property.
        /// </summary>
        private static BitmapSource _matchCase;

        /// <summary>
        /// The private field used for the <see cref="MatchWord"/> property.
        /// </summary>
        private static BitmapSource _matchWord;

        /// <summary>
        /// The private field used for the <see cref="RegularExpressions"/> property.
        /// </summary>
        private static BitmapSource _regularExpressions;
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets the icon that shows a add action icon.
        /// </summary>
        public static BitmapSource ActionAdd
        {
            get
            {
                if (_actionAdd == null)
                {
                    lock (_syncRoot)
                    {
                        if (_actionAdd == null)
                        {
                            EnsureLoaded(ref _actionAdd, "actionAdd16");
                        }
                    }
                }

                return _actionAdd;
            }
        }

        /// <summary>
        /// Gets the icon that shows a arrow pointing down icon.
        /// </summary>
        public static BitmapSource ArrowDown
        {
            get
            {
                if (_arrowDown == null)
                {
                    lock (_syncRoot)
                    {
                        if (_arrowDown == null)
                        {
                            EnsureLoaded(ref _arrowDown, "arrowDown16");
                        }
                    }
                }

                return _arrowDown;
            }
        }

        /// <summary>
        /// Gets the icon that shows a arrow pointing up icon.
        /// </summary>
        public static BitmapSource ArrowUp
        {
            get
            {
                if (_arrowUp == null)
                {
                    lock (_syncRoot)
                    {
                        if (_arrowUp == null)
                        {
                            EnsureLoaded(ref _arrowUp, "arrowUp16");
                        }
                    }
                }

                return _arrowUp;
            }
        }

        /// <summary>
        /// Gets the icon that shows a blank file icon.
        /// </summary>
        public static BitmapSource BlankFile
        {
            get
            {
                if (_blankFile == null)
                {
                    lock (_syncRoot)
                    {
                        if (_blankFile == null)
                        {
                            EnsureLoaded(ref _blankFile, "blankFile16");
                        }
                    }
                }

                return _blankFile;
            }
        }

        /// <summary>
        /// Gets the icon that shows a build icon.
        /// </summary>
        public static BitmapSource Build
        {
            get
            {
                if (_build == null)
                {
                    lock (_syncRoot)
                    {
                        if (_build == null)
                        {
                            EnsureLoaded(ref _build, "build16");
                        }
                    }
                }

                return _build;
            }
        }

        /// <summary>
        /// Gets the icon that shows a clear icon.
        /// </summary>
        public static BitmapSource Clear
        {
            get
            {
                if (_clear == null)
                {
                    lock (_syncRoot)
                    {
                        if (_clear == null)
                        {
                            EnsureLoaded(ref _clear, "clear16");
                        }
                    }
                }

                return _clear;
            }
        }

        /// <summary>
        /// Gets the icon that shows a close icon.
        /// </summary>
        public static BitmapSource Close
        {
            get
            {
                if (_close == null)
                {
                    lock (_syncRoot)
                    {
                        if (_close == null)
                        {
                            EnsureLoaded(ref _close, "close16");
                        }
                    }
                }

                return _close;
            }
        }

        /// <summary>
        /// Gets the icon that shows a closed folder icon.
        /// </summary>
        public static BitmapSource ClosedFolder
        {
            get
            {
                if (_closedFolder == null)
                {
                    lock (_syncRoot)
                    {
                        if (_closedFolder == null)
                        {
                            EnsureLoaded(ref _closedFolder, "closedFolder16");
                        }
                    }
                }

                return _closedFolder;
            }
        }

        /// <summary>
        /// Gets the icon that shows a collapse icon.
        /// </summary>
        public static BitmapSource Collapse
        {
            get
            {
                if (_collapse == null)
                {
                    lock (_syncRoot)
                    {
                        if (_collapse == null)
                        {
                            EnsureLoaded(ref _collapse, "collapse16");
                        }
                    }
                }

                return _collapse;
            }
        }

        /// <summary>
        /// Gets the icon that shows a collapse all icon.
        /// </summary>
        public static BitmapSource CollapseAll
        {
            get
            {
                if (_collapseAll == null)
                {
                    lock (_syncRoot)
                    {
                        if (_collapseAll == null)
                        {
                            EnsureLoaded(ref _collapseAll, "collapseAll16");
                        }
                    }
                }

                return _collapseAll;
            }
        }

        /// <summary>
        /// Gets the icon that shows a copy icon.
        /// </summary>
        public static BitmapSource Copy
        {
            get
            {
                if (_copy == null)
                {
                    lock (_syncRoot)
                    {
                        if (_copy == null)
                        {
                            EnsureLoaded(ref _copy, "copy16");
                        }
                    }
                }

                return _copy;
            }
        }

        /// <summary>
        /// Gets the icon that shows a cut icon.
        /// </summary>
        public static BitmapSource Cut
        {
            get
            {
                if (_cut == null)
                {
                    lock (_syncRoot)
                    {
                        if (_cut == null)
                        {
                            EnsureLoaded(ref _cut, "cut16");
                        }
                    }
                }

                return _cut;
            }
        }

        /// <summary>
        /// Gets the icon that shows a delete icon.
        /// </summary>
        public static BitmapSource Delete
        {
            get
            {
                if (_delete == null)
                {
                    lock (_syncRoot)
                    {
                        if (_delete == null)
                        {
                            EnsureLoaded(ref _delete, "delete16");
                        }
                    }
                }

                return _delete;
            }
        }

        /// <summary>
        /// Gets the icon that shows a error icon.
        /// </summary>
        public static BitmapSource Error
        {
            get
            {
                if (_error == null)
                {
                    lock (_syncRoot)
                    {
                        if (_error == null)
                        {
                            EnsureLoaded(ref _error, "error16");
                        }
                    }
                }

                return _error;
            }
        }

        /// <summary>
        /// Gets the icon that shows a expand icon.
        /// </summary>
        public static BitmapSource Expand
        {
            get
            {
                if (_expand == null)
                {
                    lock (_syncRoot)
                    {
                        if (_expand == null)
                        {
                            EnsureLoaded(ref _expand, "expand16");
                        }
                    }
                }

                return _expand;
            }
        }

        /// <summary>
        /// Gets the icon that shows a expand all icon.
        /// </summary>
        public static BitmapSource ExpandAll
        {
            get
            {
                if (_expandAll == null)
                {
                    lock (_syncRoot)
                    {
                        if (_expandAll == null)
                        {
                            EnsureLoaded(ref _expandAll, "expandAll16");
                        }
                    }
                }

                return _expandAll;
            }
        }

        /// <summary>
        /// Gets the icon that shows a find in files icon.
        /// </summary>
        public static BitmapSource FindInFiles
        {
            get
            {
                if (_findInFiles == null)
                {
                    lock (_syncRoot)
                    {
                        if (_findInFiles == null)
                        {
                            EnsureLoaded(ref _findInFiles, "findInFiles16");
                        }
                    }
                }

                return _findInFiles;
            }
        }

        /// <summary>
        /// Gets the icon that shows a find next icon.
        /// </summary>
        public static BitmapSource FindNext
        {
            get
            {
                if (_findNext == null)
                {
                    lock (_syncRoot)
                    {
                        if (_findNext == null)
                        {
                            EnsureLoaded(ref _findNext, "findNext16");
                        }
                    }
                }

                return _findNext;
            }
        }

        /// <summary>
        /// Gets the icon that shows a find previous icon.
        /// </summary>
        public static BitmapSource FindPrevious
        {
            get
            {
                if (_findPrevious == null)
                {
                    lock (_syncRoot)
                    {
                        if (_findPrevious == null)
                        {
                            EnsureLoaded(ref _findPrevious, "findPrevious16");
                        }
                    }
                }

                return _findPrevious;
            }
        }

        /// <summary>
        /// Gets the icon that shows a gears icon.
        /// </summary>
        public static BitmapSource Gears
        {
            get
            {
                if (_gears == null)
                {
                    lock (_syncRoot)
                    {
                        if (_gears == null)
                        {
                            EnsureLoaded(ref _gears, "gears16");
                        }
                    }
                }

                return _gears;
            }
        }

        /// <summary>
        /// Gets the icon that shows a help icon.
        /// </summary>
        public static BitmapSource Help
        {
            get
            {
                if (_help == null)
                {
                    lock (_syncRoot)
                    {
                        if (_help == null)
                        {
                            EnsureLoaded(ref _help, "help16");
                        }
                    }
                }

                return _help;
            }
        }

        /// <summary>
        /// Gets the icon that shows a lock icon.
        /// </summary>
        public static BitmapSource Lock
        {
            get
            {
                if (_lock == null)
                {
                    lock (_syncRoot)
                    {
                        if (_lock == null)
                        {
                            EnsureLoaded(ref _lock, "lock16");
                        }
                    }
                }

                return _lock;
            }
        }

        /// <summary>
        /// Gets the icon that shows a new file icon.
        /// </summary>
        public static BitmapSource NewFile
        {
            get
            {
                if (_newFile == null)
                {
                    lock (_syncRoot)
                    {
                        if (_newFile == null)
                        {
                            EnsureLoaded(ref _newFile, "newFile16");
                        }
                    }
                }

                return _newFile;
            }
        }

        /// <summary>
        /// Gets the icon that shows a opened folder icon.
        /// </summary>
        public static BitmapSource OpenedFolder
        {
            get
            {
                if (_openedFolder == null)
                {
                    lock (_syncRoot)
                    {
                        if (_openedFolder == null)
                        {
                            EnsureLoaded(ref _openedFolder, "openedFolder16");
                        }
                    }
                }

                return _openedFolder;
            }
        }

        /// <summary>
        /// Gets the icon that shows a open file icon.
        /// </summary>
        public static BitmapSource OpenFile
        {
            get
            {
                if (_openFile == null)
                {
                    lock (_syncRoot)
                    {
                        if (_openFile == null)
                        {
                            EnsureLoaded(ref _openFile, "openFile16");
                        }
                    }
                }

                return _openFile;
            }
        }

        /// <summary>
        /// Gets the icon that shows a open paste icon.
        /// </summary>
        public static BitmapSource Paste
        {
            get
            {
                if (_paste == null)
                {
                    lock (_syncRoot)
                    {
                        if (_paste == null)
                        {
                            EnsureLoaded(ref _paste, "paste16");
                        }
                    }
                }

                return _paste;
            }
        }

        /// <summary>
        /// Gets the icon that shows a undo icon.
        /// </summary>
        public static BitmapSource Playstation
        {
            get
            {
                if (_playstation == null)
                {
                    lock (_syncRoot)
                    {
                        if (_playstation == null)
                        {
                            EnsureLoaded(ref _playstation, "playstation16");
                        }
                    }
                }

                return _playstation;
            }
        }

        /// <summary>
        /// Gets the icon that shows a quick find icon.
        /// </summary>
        public static BitmapSource QuickFind
        {
            get
            {
                if (_quickFind == null)
                {
                    lock (_syncRoot)
                    {
                        if (_quickFind == null)
                        {
                            EnsureLoaded(ref _quickFind, "quickFind16");
                        }
                    }
                }

                return _quickFind;
            }
        }

        /// <summary>
        /// Gets the icon that shows a quick replace icon.
        /// </summary>
        public static BitmapSource QuickReplace
        {
            get
            {
                if (_quickReplace == null)
                {
                    lock (_syncRoot)
                    {
                        if (_quickReplace == null)
                        {
                            EnsureLoaded(ref _quickReplace, "quickReplace16");
                        }
                    }
                }

                return _quickReplace;
            }
        }

        /// <summary>
        /// Gets the icon that shows a redo icon.
        /// </summary>
        public static BitmapSource Redo
        {
            get
            {
                if (_redo == null)
                {
                    lock (_syncRoot)
                    {
                        if (_redo == null)
                        {
                            EnsureLoaded(ref _redo, "redo16");
                        }
                    }
                }

                return _redo;
            }
        }

        /// <summary>
        /// Gets the icon that shows a refresh icon.
        /// </summary>
        public static BitmapSource Refresh
        {
            get
            {
                if (_refresh == null)
                {
                    lock (_syncRoot)
                    {
                        if (_refresh == null)
                        {
                            EnsureLoaded(ref _refresh, "refresh16");
                        }
                    }
                }

                return _refresh;
            }
        }

        /// <summary>
        /// Gets the icon that shows a rename icon.
        /// </summary>
        public static BitmapSource Rename
        {
            get
            {
                if (_rename == null)
                {
                    lock (_syncRoot)
                    {
                        if (_rename == null)
                        {
                            EnsureLoaded(ref _rename, "rename16");
                        }
                    }
                }

                return _rename;
            }
        }

        /// <summary>
        /// Gets the icon that shows a replace in files icon.
        /// </summary>
        public static BitmapSource ReplaceInFiles
        {
            get
            {
                if (_replaceInFiles == null)
                {
                    lock (_syncRoot)
                    {
                        if (_replaceInFiles == null)
                        {
                            EnsureLoaded(ref _replaceInFiles, "replaceInFiles16");
                        }
                    }
                }

                return _replaceInFiles;
            }
        }

        /// <summary>
        /// Gets the icon that shows a replace in files icon.
        /// </summary>
        public static BitmapSource ReportBug
        {
            get
            {
                if (_reportBug == null)
                {
                    lock (_syncRoot)
                    {
                        if (_reportBug == null)
                        {
                            EnsureLoaded(ref _reportBug, "reportBug16");
                        }
                    }
                }

                return _reportBug;
            }
        }

        /// <summary>
        /// Gets the icon that shows a resource icon.
        /// </summary>
        public static BitmapSource Resource
        {
            get
            {
                if (_resource == null)
                {
                    lock (_syncRoot)
                    {
                        if (_resource == null)
                        {
                            EnsureLoaded(ref _resource, "resource16");
                        }
                    }
                }

                return _resource;
            }
        }

        /// <summary>
        /// Gets the icon that shows a restart icon.
        /// </summary>
        public static BitmapSource Restart
        {
            get
            {
                if (_restart == null)
                {
                    lock (_syncRoot)
                    {
                        if (_restart == null)
                        {
                            EnsureLoaded(ref _restart, "restart16");
                        }
                    }
                }

                return _restart;
            }
        }

        /// <summary>
        /// Gets the icon that shows a save to disk icon.
        /// </summary>
        public static BitmapSource Save
        {
            get
            {
                if (_save == null)
                {
                    lock (_syncRoot)
                    {
                        if (_save == null)
                        {
                            EnsureLoaded(ref _save, "save16");
                        }
                    }
                }

                return _save;
            }
        }

        /// <summary>
        /// Gets the icon that shows a save all icon.
        /// </summary>
        public static BitmapSource SaveAll
        {
            get
            {
                if (_saveAll == null)
                {
                    lock (_syncRoot)
                    {
                        if (_saveAll == null)
                        {
                            EnsureLoaded(ref _saveAll, "saveall16");
                        }
                    }
                }

                return _saveAll;
            }
        }

        /// <summary>
        /// Gets the icon that shows a search icon.
        /// </summary>
        public static BitmapSource Search
        {
            get
            {
                if (_search == null)
                {
                    lock (_syncRoot)
                    {
                        if (_search == null)
                        {
                            EnsureLoaded(ref _search, "search16");
                        }
                    }
                }

                return _search;
            }
        }

        /// <summary>
        /// Gets the icon that shows a select all icon.
        /// </summary>
        public static BitmapSource SelectAll
        {
            get
            {
                if (_selectAll == null)
                {
                    lock (_syncRoot)
                    {
                        if (_selectAll == null)
                        {
                            EnsureLoaded(ref _selectAll, "selectAll16");
                        }
                    }
                }

                return _selectAll;
            }
        }

        /// <summary>
        /// Gets the icon that shows a undo icon.
        /// </summary>
        public static BitmapSource Undo
        {
            get
            {
                if (_undo == null)
                {
                    lock (_syncRoot)
                    {
                        if (_undo == null)
                        {
                            EnsureLoaded(ref _undo, "undo16");
                        }
                    }
                }

                return _undo;
            }
        }

        /// <summary>
        /// Gets the icon that shows a unknown file icon.
        /// </summary>
        public static BitmapSource UnknownFile
        {
            get
            {
                if (_unknownFile == null)
                {
                    lock (_syncRoot)
                    {
                        if (_unknownFile == null)
                        {
                            EnsureLoaded(ref _unknownFile, "unknownFile16");
                        }
                    }
                }

                return _unknownFile;
            }
        }

        /// <summary>
        /// Gets the icon that shows a View Document icon.
        /// </summary>
        public static BitmapSource ViewDocument
        {
            get
            {
                if (_viewDocument == null)
                {
                    lock (_syncRoot)
                    {
                        if (_viewDocument == null)
                        {
                            EnsureLoaded(ref _viewDocument, "viewDocument16");
                        }
                    }
                }

                return _viewDocument;
            }
        }

        /// <summary>
        /// Gets the icon that shows a warning icon.
        /// </summary>
        public static BitmapSource Warning
        {
            get
            {
                if (_warning == null)
                {
                    lock (_syncRoot)
                    {
                        if (_warning == null)
                        {
                            EnsureLoaded(ref _warning, "warning16");
                        }
                    }
                }

                return _warning;
            }
        }

        /// <summary>
        /// Gets the icon that shows a save all icon.
        /// </summary>
        public static BitmapSource Windows
        {
            get
            {
                if (_windows == null)
                {
                    lock (_syncRoot)
                    {
                        if (_windows == null)
                        {
                            EnsureLoaded(ref _windows, "windows16");
                        }
                    }
                }

                return _windows;
            }
        }

        /// <summary>
        /// Gets the icon that shows a select all icon.
        /// </summary>
        public static BitmapSource XBox
        {
            get
            {
                if (_xbox == null)
                {
                    lock (_syncRoot)
                    {
                        if (_xbox == null)
                        {
                            EnsureLoaded(ref _xbox, "xbox16");
                        }
                    }
                }

                return _xbox;
            }
        }

        /// <summary>
        /// Gets the icon that shows a filter items icon.
        /// </summary>
        public static BitmapSource Filter
        {
            get
            {
                if (_filter == null)
                {
                    lock (_syncRoot)
                    {
                        if (_filter == null)
                        {
                            EnsureLoaded(ref _filter, "filter16");
                        }
                    }
                }

                return _filter;
            }
        }

        /// <summary>
        /// Gets the icon that shows a match case icon.
        /// </summary>
        public static BitmapSource MatchCase
        {
            get
            {
                if (_matchCase == null)
                {
                    lock (_syncRoot)
                    {
                        if (_matchCase == null)
                        {
                            EnsureLoaded(ref _matchCase, "matchCase16");
                        }
                    }
                }

                return _matchCase;
            }
        }

        /// <summary>
        /// Gets the icon that shows a match whole word icon.
        /// </summary>
        public static BitmapSource MatchWord
        {
            get
            {
                if (_matchWord == null)
                {
                    lock (_syncRoot)
                    {
                        if (_matchWord == null)
                        {
                            EnsureLoaded(ref _matchWord, "matchWord16");
                        }
                    }
                }

                return _matchWord;
            }
        }

        /// <summary>
        /// Gets the icon that shows a regular expressions icon.
        /// </summary>
        public static BitmapSource RegularExpressions
        {
            get
            {
                if (_regularExpressions == null)
                {
                    lock (_syncRoot)
                    {
                        if (_regularExpressions == null)
                        {
                            EnsureLoaded(ref _regularExpressions, "regularExpressions16");
                        }
                    }
                }

                return _regularExpressions;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Ensures that the specified image source is loaded with the specified resource name.
        /// </summary>
        /// <param name="source">
        /// The image source that should be loaded.
        /// </param>
        /// <param name="resourceName">
        /// The name of the resource the loaded image source should set as its source.
        /// </param>
        private static void EnsureLoaded(ref BitmapSource source, string resourceName)
        {
            if (source != null)
            {
                return;
            }

            string assemblyName = typeof(CommonIcons).Assembly.GetName().Name;
            string path = "Resources/" + resourceName + ".png";
            string bitmapPath = "pack://application:,,,/{0};component/{1}";
            bitmapPath = bitmapPath.FormatInvariant(assemblyName, path);
            Uri uri = new Uri(bitmapPath, UriKind.RelativeOrAbsolute);

            source = new BitmapImage(uri);
            source.Freeze();
        }
        #endregion Methods
    } // RSG.Editor.CommonIcons {Class}
} // RSG.Editor.Commands {Namespace}
