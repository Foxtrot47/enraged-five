﻿//---------------------------------------------------------------------------------------------
// <copyright file="AddDynamicFolderWindow.xaml.cs" company="Rockstar">
//     Copyright © Rockstar Games 2015. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.View
{
    using System;
    using System.IO;
    using System.Text.RegularExpressions;
    using System.Windows;
    using RSG.Editor;
    using RSG.Editor.Controls;

    /// <summary>
    /// Interaction logic for the AddDynamicFolderWindow.
    /// </summary>
    public partial class AddDynamicFolderWindow : RsWindow
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="AddDynamicFolderWindow"/> class.
        /// </summary>
        public AddDynamicFolderWindow()
        {
            this.InitializeComponent();
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the
        /// <see cref="RSG.Editor.SharedCommands.RockstarViewCommands.ConfirmDialog"/> command
        /// can be executed at this level.
        /// </summary>
        /// <param name="e">
        /// The command data that is past from the command system.
        /// </param>
        /// <returns>
        /// True if the command can be executed here; otherwise, false.
        /// </returns>
        protected override bool CanConfirmDialog(CanExecuteCommandData e)
        {
            if (String.IsNullOrWhiteSpace(this.LocationValueTextBox.Text))
            {
                return false;
            }

            var pattern = "\\A[a-zA-Z]:\\\\[^{0}]*\\z";
            pattern = String.Format(pattern, new String(Path.GetInvalidPathChars()));
            var regex = new Regex(pattern);
            if (!regex.IsMatch(this.LocationValueTextBox.Text))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Called when the browse button is pressed. Shows the select folder windows dialog
        /// for the user to select.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private void OnBrowseButtonClicked(object sender, RoutedEventArgs e)
        {
            string directory = null;
            Guid id = new Guid("341B1E2B-74D7-41B6-A613-395F54821C19");

            RsSelectFolderDialog dlg = new RsSelectFolderDialog(id);
            dlg.Title = "Select Directory";

            bool? result = dlg.ShowDialog();
            if (result == true)
            {
                directory = dlg.FileName;
            }
            else
            {
                return;
            }

            this.LocationValueTextBox.Text = Path.GetFullPath(directory);
        }
        #endregion Methods
    } // RSG.Project.View.AddDynamicFolderWindow {Class}
} // RSG.Project.View {Namespace}
