﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsToolBarTray.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Commands
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;

    /// <summary>
    /// Represents the container that handles the layout of a
    /// <see cref="RSG.Editor.Controls.Commands.RsToolBar"/>.
    /// </summary>
    public class RsToolBarTray : ToolBarTray
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="ToolBarStyleKey"/> property.
        /// </summary>
        private static ResourceKey _toolBarStyleKey = CreateStyleKey("ToolBarStyleKey");
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsToolBarTray"/> class.
        /// </summary>
        static RsToolBarTray()
        {
            ItemsControl.ItemsSourceProperty.AddOwner(
                typeof(RsToolBarTray),
                new FrameworkPropertyMetadata(
                    new PropertyChangedCallback(OnItemsSourceChanged)));

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsToolBarTray),
                new FrameworkPropertyMetadata(typeof(RsToolBarTray)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsToolBarTray"/> class.
        /// </summary>
        public RsToolBarTray()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the resource key used to reference the style used on tool bar command items.
        /// </summary>
        public static ResourceKey ToolBarStyleKey
        {
            get { return _toolBarStyleKey; }
        }

        /// <summary>
        /// Gets or sets a collection used to generate the tool bars for this tool bar tray.
        /// </summary>
        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)this.GetValue(ItemsControl.ItemsSourceProperty); }
            set { this.SetValue(ItemsControl.ItemsSourceProperty, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new resource key that references a style with the specified name.
        /// </summary>
        /// <param name="styleName">
        /// The name that the resource key will be referencing.
        /// </param>
        /// <returns>
        /// A new resource key that references a style with the specified name.
        /// </returns>
        private static ResourceKey CreateStyleKey(string styleName)
        {
            return new StyleKey<RsToolBarTray>(styleName);
        }

        /// <summary>
        /// Gets called whenever the <see cref="ItemsSource"/> dependency property
        /// changes on a <see cref="RsToolBarTray"/> instance.
        /// </summary>
        /// <param name="s">
        /// The <see cref="RsToolBarTray"/> instance whose dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data used for this event.
        /// </param>
        private static void OnItemsSourceChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            RsToolBarTray toolBarTray = s as RsToolBarTray;
            if (toolBarTray == null)
            {
                return;
            }

            if (e.OldValue != null)
            {
                INotifyCollectionChanged collection = e.OldValue as INotifyCollectionChanged;
                if (collection != null)
                {
                    CollectionChangedEventManager.RemoveHandler(
                        collection, toolBarTray.OnCollectionChanged);
                }

                toolBarTray.ToolBars.Clear();
            }

            if (e.NewValue != null)
            {
                INotifyCollectionChanged collection = e.NewValue as INotifyCollectionChanged;
                if (collection != null)
                {
                    CollectionChangedEventManager.AddHandler(
                        collection, toolBarTray.OnCollectionChanged);
                }

                toolBarTray.ProcessToolBarViewModels((IEnumerable)e.NewValue, 0);
            }
        }

        /// <summary>
        /// Called whenever the collection bound to the <see cref="ItemsSource"/> property
        /// changes.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to, in this case the collection that changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Specialized.NotifyCollectionChangedEventArgs data describing the
        /// change.
        /// </param>
        private void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                if (e.OldItems != null)
                {
                    foreach (object item in e.OldItems)
                    {
                        CommandBarItemViewModel viewModel = item as CommandBarItemViewModel;
                        ToolBar remove = null;
                        foreach (ToolBar toolBar in this.ToolBars)
                        {
                            if (!Object.ReferenceEquals(toolBar.DataContext, viewModel))
                            {
                                continue;
                            }

                            remove = toolBar;
                            break;
                        }

                        this.ToolBars.Remove(remove);
                    }
                }

                return;
            }

            List<ToolBar> toolBars = new List<ToolBar>(this.ToolBars);
            this.ToolBars.Clear();
            foreach (ToolBar toolBar in toolBars)
            {
                toolBar.DataContext = null;
            }

            IEnumerable collection = sender as ICollection;
            if (collection != null)
            {
                this.ProcessToolBarViewModels(collection, 0);
            }
        }

        /// <summary>
        /// Process the specified view models and creates a tool bar for each one.
        /// </summary>
        /// <param name="collection">
        /// A iterator over the view models that need tool bars created for them.
        /// </param>
        /// <param name="index">
        /// The index where the new tool bars should be inserted.
        /// </param>
        private void ProcessToolBarViewModels(IEnumerable collection, int index)
        {
            if (collection == null)
            {
                return;
            }

            foreach (object item in collection)
            {
                RsToolBar newToolBar = new RsToolBar();
                newToolBar.DataContext = item;
                this.ToolBars.Add(newToolBar);

                CommandBarItemViewModel viewModel = item as CommandBarItemViewModel;
                if (viewModel == null)
                {
                    continue;
                }

                ResourceKey resource = null;
                switch (viewModel.DisplayType)
                {
                    case CommandItemDisplayType.ToolBar:
                        resource = RsToolBarTray.ToolBarStyleKey;
                        break;
                }

                if (resource == null)
                {
                    return;
                }

                newToolBar.SetResourceReference(FrameworkElement.StyleProperty, resource);
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.Commands.RsToolBarTray {Class}
} // RSG.Editor.Controls.Commands {Namespace}
