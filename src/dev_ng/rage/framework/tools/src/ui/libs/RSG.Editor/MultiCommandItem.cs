﻿//---------------------------------------------------------------------------------------------
// <copyright file="MultiCommandItem.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Windows.Input;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Provides a base class to objects representing a item for a
    /// <see cref="RSG.Editor.MultiCommand"/> definition object.
    /// </summary>
    /// <typeparam name="T">
    /// The type that the actual command parameter is for this multi command item.
    /// </typeparam>
    public abstract class MultiCommandItem<T> : NotifyPropertyChangedBase, IMultiCommandItem
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="IsToggled"/> property.
        /// </summary>
        private bool _isToggled;

        /// <summary>
        /// The private field used for the <see cref="KeyGesture"/> property.
        /// </summary>
        private KeyGesture _keyGesture;

        /// <summary>
        /// A reference to the multi item command definition.
        /// </summary>
        private MultiCommand _parentDefinition;

        /// <summary>
        /// The private field used for the <see cref="Text"/> property.
        /// </summary>
        private string _text;

        /// <summary>
        /// The private field used for the <see cref="CommandParameter"/> property.
        /// </summary>
        private T _commandParameter;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MultiCommandItem{T}"/> class.
        /// </summary>
        /// <param name="definition">
        /// The multi command definition that owns this item.
        /// </param>
        /// <param name="text">
        /// The text that is used to display this item inside the combo control.
        /// </param>
        /// <param name="commandParameter">
        /// The item parameter that is sent with the command if this item is selected in the
        /// combo control or toggled in the filter control.
        /// </param>
        protected MultiCommandItem(MultiCommand definition, string text, T commandParameter)
        {
            if (definition == null)
            {
                throw new SmartArgumentNullException(() => definition);
            }

            this._parentDefinition = definition;
            this._text = text;
            this._commandParameter = commandParameter;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the generic object that is sent with the command if this item is the
        /// selected item in the combo control or toggled in the filter control.
        /// </summary>
        public T CommandParameter
        {
            get { return this._commandParameter; }
            protected set { this._commandParameter = value; }
        }

        /// <summary>
        /// Gets the description for this item. This is also what is shown as a tooltip along
        /// with the key gesture if one is set if it is being rendered as a toggle control.
        /// </summary>
        public abstract string Description { get; }

        /// <summary>
        /// Gets the System.Windows.Media.Imaging.BitmapSource object that represents the icon
        /// that is used for this item if it is being rendered as a toggle control.
        /// </summary>
        public virtual BitmapSource Icon
        {
            get { return null; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this item is currently the selected item on
        /// the owning command definition.
        /// </summary>
        public bool IsToggled
        {
            get
            {
                return this._isToggled;
            }

            set
            {
                if (this._parentDefinition.SelectionMode == MultiCommandSelectionMode.Single)
                {
                    if (!value)
                    {
                        // Has come in through a view binding which we cannot allow.
                        this._parentDefinition.IgnoreNextExecution = true;
                        return;
                    }
                }

                if (this.SetProperty(ref this._isToggled, value))
                {
                    this.NotifyPropertyChanged("CommandParameter");
                    if (value)
                    {
                        this._parentDefinition.SelectedItem = this;
                    }
                    else
                    {
                        this._parentDefinition.SelectedItem = null;
                    }
                }

                this.NotifyPropertyChanged("CommandParameter");
                return;
            }
        }

        /// <summary>
        /// Gets or sets the System.Windows.Input.KeyGesture object that is associated with
        /// this combo command item.
        /// </summary>
        public KeyGesture KeyGesture
        {
            get
            {
                return this._keyGesture;
            }

            set
            {
                this._parentDefinition.ReplaceKeyGesture(this._keyGesture, value);
                this._keyGesture = value;
            }
        }

        /// <summary>
        /// Gets the parent command definition that this is an item for.
        /// </summary>
        public MultiCommand ParentDefinition
        {
            get { return this._parentDefinition; }
        }

        /// <summary>
        /// Gets or sets the text that is used to display this item inside the combo control.
        /// </summary>
        public string Text
        {
            get { return this._text; }
            set { this.SetProperty(ref this._text, value); }
        }

        /// <summary>
        /// Gets the text that contains the key gesture that is attached to this combo command
        /// item.
        /// </summary>
        private string KeyGestureText
        {
            get
            {
                KeyGesture keyGesture = this.KeyGesture;
                if (keyGesture != null)
                {
                    KeyGestureConverter converter = new KeyGestureConverter();
                    return converter.ConvertToInvariantString(keyGesture);
                }

                return null;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Gets the command parameter used for this multi command item.
        /// </summary>
        /// <returns>
        /// The command parameter used for this multi command item.
        /// </returns>
        public IMultiCommandParameter GetCommandParameter()
        {
            T itemParameter = this.CommandParameter;
            var commandParameter = new MultiCommandParameter<T>(itemParameter);
            commandParameter.IsToggled = this.IsToggled;
            return commandParameter;
        }

        /// <summary>
        /// Sets the <see cref="IsToggled"/> property from an internal operation.
        /// </summary>
        /// <param name="isToggled">
        /// A value to set the <see cref="IsToggled"/> property to.
        /// </param>
        /// <param name="handleSelectionOnParent">
        /// A value indicating whether the toggle change should result in it being handled in
        /// the parent command definition.
        /// </param>
        public void SetIsToggled(bool isToggled, bool handleSelectionOnParent)
        {
            if (!handleSelectionOnParent)
            {
                this._isToggled = isToggled;
                this.NotifyPropertyChanged("IsToggled");
            }
            else
            {
                this.IsToggled = isToggled;
            }
        }

        /// <summary>
        /// Sets the <see cref="IsToggled"/> property from an internal operation.
        /// </summary>
        /// <param name="isToggled">
        /// A value to set the <see cref="IsToggled"/> property to.
        /// </param>
        internal void SetIsToggled(bool isToggled)
        {
            this._isToggled = isToggled;
            this.NotifyPropertyChanged("IsToggled");
        }
        #endregion Methods
    } // RSG.Editor.MultiCommandItem {Class}
} // RSG.Editor {Namespace}
