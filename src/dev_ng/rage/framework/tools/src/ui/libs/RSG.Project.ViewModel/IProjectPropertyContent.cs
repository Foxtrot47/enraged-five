﻿//---------------------------------------------------------------------------------------------
// <copyright file="IProjectPropertyContent.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel
{
    /// <summary>
    /// When implemented represents the document content that can go into a projects property
    /// document.
    /// </summary>
    public interface IProjectPropertyContent
    {
        #region Methods
        /// <summary>
        /// Applies the current properties that the user has set.
        /// </summary>
        void ApplyProperties();
        #endregion Methods
    } // RSG.Project.ViewModel.IProjectPropertyContent {Interface}
} // RSG.Project.ViewModel {Namespace}
