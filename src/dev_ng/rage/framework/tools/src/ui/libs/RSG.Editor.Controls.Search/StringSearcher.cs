﻿//---------------------------------------------------------------------------------------------
// <copyright file="StringSearcher.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Search
{
    using System;

    /// <summary>
    /// Represents a searcher used to find a specific string value.
    /// </summary>
    public class StringSearcher : Searcher
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="SearchValue"/> property.
        /// </summary>
        private string _searchValue;

        /// <summary>
        /// The private field used for the <see cref="CaseSensitive"/> property.
        /// </summary>
        private bool _caseSensitive;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="StringSearcher"/> class.
        /// </summary>
        public StringSearcher()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the value that is being searched for.
        /// </summary>
        public string SearchValue
        {
            get { return this._searchValue ?? String.Empty; }
            set { this.SetProperty(ref this._searchValue, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the search should be case sensitive or not.
        /// </summary>
        public bool CaseSensitive
        {
            get { return this._caseSensitive; }
            set { this.SetProperty(ref this._caseSensitive, value); }
        }
        #endregion Properties
    } // RSG.Editor.Controls.Search.StringSearcher {Class}
} // RSG.Editor.Controls.Search {Namespace}
