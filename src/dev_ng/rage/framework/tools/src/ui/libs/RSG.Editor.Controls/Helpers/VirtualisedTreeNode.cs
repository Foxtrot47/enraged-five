﻿//---------------------------------------------------------------------------------------------
// <copyright file="VirtualisedTreeNode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Helpers
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Threading;
    using RSG.Editor.View;

    /// <summary>
    /// Represents a single tree node inside the virtualised tree view control. This acts as a
    /// sort of internal view model for the bounded items.
    /// </summary>
    public class VirtualisedTreeNode : DependencyObject, ISupportInitialize, IWeakEventListener
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="IsExpanded"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsExpandedProperty;

        /// <summary>
        /// Identifies the <see cref="IsExpandingAsynchronously"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsExpandingAsynchronouslyProperty;

        /// <summary>
        /// Identifies the <see cref="IsRefreshingAsynchronously"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsRefreshingAsynchronouslyProperty;

        /// <summary>
        /// Identifies the <see cref="IsSpinAnimationVisible"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsSpinAnimationVisibleProperty;

        /// <summary>
        /// Identifies the <see cref="ItemsSource"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ItemsSourceProperty;

        /// <summary>
        /// Identifies the <see cref="IsExpandingAsynchronously"/> dependency property.
        /// </summary>
        private static readonly DependencyPropertyKey _isExpandingAsynchronouslyPropertyKey;

        /// <summary>
        /// Identifies the <see cref="IsRefreshingAsynchronously"/> dependency property.
        /// </summary>
        private static readonly DependencyPropertyKey _isRefreshingAsynchronouslyPropertyKey;

        /// <summary>
        /// Identifies the <see cref="IsSpinAnimationVisible"/> dependency property.
        /// </summary>
        private static readonly DependencyPropertyKey _isSpinAnimationVisiblePropertyKey;

        /// <summary>
        /// The private field used for the <see cref="AreChildNodesInitialised"/> property.
        /// </summary>
        private bool _areChildNodesInitialised;

        /// <summary>
        /// The private field used for the <see cref="AreItemsPlaceholders"/> property.
        /// </summary>
        private bool _areItemsPlaceholders;

        /// <summary>
        /// The private list containing the cached child nodes that have been created to used
        /// as the children for this node.
        /// </summary>
        private List<VirtualisedTreeNode> _cachedChildNodes;

        /// <summary>
        /// The private field used for the <see cref="Depth"/> property.
        /// </summary>
        private int _depth;

        /// <summary>
        /// The private field used for the <see cref="FilterChildCount"/> property.
        /// </summary>
        private int _filterChildCount;

        /// <summary>
        /// A private value indicating whether this node has been initialised.
        /// </summary>
        private bool _initialised;

        /// <summary>
        /// A private value indicating whether this node is currently being initialised.
        /// </summary>
        private bool _initialising;

        /// <summary>
        /// The private field used for the <see cref="IsExplicitlyIncludedInFilter"/> property.
        /// </summary>
        private bool _isExplicitlyIncludedInFilter;

        /// <summary>
        /// The private field used for the <see cref="IsFilterActive"/> property.
        /// </summary>
        private bool _isFilterActive;

        /// <summary>
        /// The private field used for the <see cref="IsIncludedInFilter"/> property.
        /// </summary>
        private bool _isIncludedInFilter;

        /// <summary>
        /// Private value indicating the number of times the <see cref="CoerceIsExpanded"/>
        /// method has been called in the stack hierarchy.
        /// </summary>
        private int _isInCoerceIsExpandedCount;

        /// <summary>
        /// The private field used for the <see cref="IsVisible"/> property.
        /// </summary>
        private bool _isVisible;

        /// <summary>
        /// The private field used for the <see cref="Item"/> property.
        /// </summary>
        private object _item;

        /// <summary>
        /// The private collection view that wraps around the items source for the content of
        /// this tree node.
        /// </summary>
        private CollectionView _itemsSourceView;

        /// <summary>
        /// The private field used for the <see cref="Parent"/> property.
        /// </summary>
        private VirtualisedTreeNode _parent;

        /// <summary>
        /// The private field used for the <see cref="ProcessingBeforeExpand"/> property.
        /// </summary>
        private bool _processingBeforeExpand;

        /// <summary>
        /// The private field used for the <see cref="TreeItem"/> property.
        /// </summary>
        private RsVirtualisedTreeViewItem _treeItem;

        /// <summary>
        /// The private field used for the <see cref="TreeView"/> property.
        /// </summary>
        private RsVirtualisedTreeView _treeView;

        /// <summary>
        /// The private reference to the timer used to control the visibility of the wait
        /// animation during a asynchronous operation.
        /// </summary>
        private DispatcherTimer _waitTimer;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="VirtualisedTreeNode"/> class.
        /// </summary>
        static VirtualisedTreeNode()
        {
            IsExpandedProperty =
                DependencyProperty.Register(
                "IsExpanded",
                typeof(bool),
                typeof(VirtualisedTreeNode),
                new PropertyMetadata(
                    false,
                    new PropertyChangedCallback(OnIsExpandedChanged),
                    new CoerceValueCallback(CoerceIsExpanded)));

            ItemsSourceProperty =
                DependencyProperty.Register(
                "ItemsSource",
                typeof(IEnumerable),
                typeof(VirtualisedTreeNode),
                new PropertyMetadata(
                    new PropertyChangedCallback(OnItemsSourceChanged)));

            _isExpandingAsynchronouslyPropertyKey =
                DependencyProperty.RegisterReadOnly(
                "IsExpandingAsynchronously",
                typeof(bool),
                typeof(VirtualisedTreeNode),
                new PropertyMetadata(
                    false,
                    new PropertyChangedCallback(OnAsynchronousTriggerPropertyChanged)));

            _isRefreshingAsynchronouslyPropertyKey =
                DependencyProperty.RegisterReadOnly(
                "IsRefreshingAsynchronously",
                typeof(bool),
                typeof(VirtualisedTreeNode),
                new PropertyMetadata(
                    false,
                    new PropertyChangedCallback(OnAsynchronousTriggerPropertyChanged)));

            _isSpinAnimationVisiblePropertyKey =
                DependencyProperty.RegisterReadOnly(
                "IsSpinAnimationVisible",
                typeof(bool),
                typeof(VirtualisedTreeNode),
                new PropertyMetadata(false));

            IsExpandingAsynchronouslyProperty =
                _isExpandingAsynchronouslyPropertyKey.DependencyProperty;

            IsRefreshingAsynchronouslyProperty =
                _isRefreshingAsynchronouslyPropertyKey.DependencyProperty;

            IsSpinAnimationVisibleProperty =
                _isSpinAnimationVisiblePropertyKey.DependencyProperty;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="VirtualisedTreeNode"/> class for the
        /// specified parent tree view.
        /// </summary>
        /// <param name="treeView">
        /// The parent tree view this tree node belongs to.
        /// </param>
        public VirtualisedTreeNode(RsVirtualisedTreeView treeView)
        {
            this._cachedChildNodes = new List<VirtualisedTreeNode>();
            this._treeView = treeView;
            this._depth = -1;
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs after this tree node item has been collapsed in its parents tree view.
        /// </summary>
        public event EventHandler Collapsed;

        /// <summary>
        /// Occurs after this tree node item has been expanded in its parents tree view.
        /// </summary>
        public event EventHandler Expanded;

        /// <summary>
        /// Occurs once the expanded items for this node have been loaded, this happens either
        /// after expansion or after the items source collection has sent its initialised
        /// event.
        /// </summary>
        public event EventHandler ExpandedItemsLoaded;

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion Events

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether the child nodes for this node have been
        /// initialised.
        /// </summary>
        public bool AreChildNodesInitialised
        {
            get { return this._areChildNodesInitialised; }
            set { this._areChildNodesInitialised = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this node currently has placeholder items
        /// inside its items collection.
        /// </summary>
        public bool AreItemsPlaceholders
        {
            get { return this._areItemsPlaceholders; }
            set { this._areItemsPlaceholders = value; }
        }

        /// <summary>
        /// Gets a iterator around all of the nodes that are directly underneath this node in
        /// the tree view hierarchy.
        /// </summary>
        public IEnumerable<VirtualisedTreeNode> ChildNodes
        {
            get
            {
                foreach (VirtualisedTreeNode node in this._cachedChildNodes)
                {
                    yield return node;
                }
            }
        }

        /// <summary>
        /// Gets the virtualised tree view container used for this tree node on the parent
        /// tree view.
        /// </summary>
        public UIElement Container
        {
            get
            {
                RsVirtualisedTreeView treeView = this.TreeView;
                if (treeView == null)
                {
                    return null;
                }

                return treeView.ItemContainerGenerator.ContainerFromItem(this) as UIElement;
            }
        }

        /// <summary>
        /// Gets or sets the depth of this tree node within the tree view hierarchy.
        /// </summary>
        public int Depth
        {
            get { return this._depth; }
            set { this._depth = value; }
        }

        /// <summary>
        /// Gets a iterator around all of the nodes that are underneath this node in the tree
        /// view hierarchy.
        /// </summary>
        public IEnumerable<VirtualisedTreeNode> DescendantNodes
        {
            get
            {
                foreach (VirtualisedTreeNode node in this._cachedChildNodes)
                {
                    yield return node;
                    foreach (VirtualisedTreeNode descendantNode in node.DescendantNodes)
                    {
                        yield return descendantNode;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the number of items this node has as immediate descendants that are currently
        /// filtered.
        /// </summary>
        public int FilterChildCount
        {
            get
            {
                return this._filterChildCount;
            }

            private set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException("value");
                }

                bool previouslyHadFilteredItems = this._filterChildCount != 0;
                this._filterChildCount = value;
                bool hasFilteredItems = this._filterChildCount != 0;
                if (previouslyHadFilteredItems != hasFilteredItems)
                {
                    this.UpdateIsIncludedInFilter();
                    this.UpdateIsFilterActive();
                    if (this.IsFilterActive)
                    {
                        this.RaisePropertyChanged("IsExpandable");
                    }
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether this tree node is currently inside the hierarchy of
        /// a valid tree view.
        /// </summary>
        public bool IsConnected
        {
            get { return this.TreeView != null; }
        }

        /// <summary>
        /// Gets a value indicating whether this tree node can be expanded.
        /// </summary>
        public bool IsExpandable
        {
            get
            {
                if (this.IsPermanentlyExpandedRoot)
                {
                    return true;
                }

                IProvidesExpandableState pattern = this.Item as IProvidesExpandableState;
                if (pattern != null)
                {
                    return pattern.IsExpandable;
                }

                return true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this tree node is currently expanded.
        /// </summary>
        public bool IsExpanded
        {
            get { return (bool)this.GetValue(IsExpandedProperty); }
            set { this.SetValue(IsExpandedProperty, value); }
        }

        /// <summary>
        /// Gets a value indicating whether this tree node should be expanded on creation.
        /// </summary>
        public bool IsExpandedByDefault
        {
            get
            {
                if (this.IsPermanentlyExpandedRoot)
                {
                    return true;
                }

                ISupportsExpansionState pattern = this.Item as ISupportsExpansionState;
                if (pattern == null)
                {
                    return false;
                }

                return pattern.IsExpandedByDefault;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this tree node is currently expanding
        /// asynchronously.
        /// </summary>
        public bool IsExpandingAsynchronously
        {
            get { return (bool)this.GetValue(IsExpandingAsynchronouslyProperty); }
            private set { this.SetValue(_isExpandingAsynchronouslyPropertyKey, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this node is currently explicitly included
        /// in the filtered item count for its parent.
        /// </summary>
        public bool IsExplicitlyIncludedInFilter
        {
            get
            {
                return this._isExplicitlyIncludedInFilter;
            }

            set
            {
                if (this._isExplicitlyIncludedInFilter == value)
                {
                    return;
                }

                this._isExplicitlyIncludedInFilter = value;
                this.UpdateIsIncludedInFilter();
                if (!this.TreeView.AutoExpandFilteredItems || !this.IsFilterActive)
                {
                    return;
                }

                if (this.IsConnected)
                {
                    RsVirtualisedTreeView.EnsureExpanded(this);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the filtering is currently active.
        /// </summary>
        public bool IsFilterActive
        {
            get
            {
                return this._isFilterActive;
            }

            set
            {
                if (this._isFilterActive == value)
                {
                    return;
                }

                if (value)
                {
                    this.BeginFiltering();
                }
                else
                {
                    this.EndFiltering();
                }

                this.RaisePropertyChanged("IsExpandable");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this node is currently included in the
        /// filtered item count for its parent.
        /// </summary>
        public bool IsIncludedInFilter
        {
            get
            {
                return this._isIncludedInFilter;
            }

            set
            {
                if (this._isIncludedInFilter == value)
                {
                    return;
                }

                this._isIncludedInFilter = value;
                if (this.Parent == null)
                {
                    return;
                }

                if (value)
                {
                    this.Parent.OnFilteredChildAdded(this);
                }
                else
                {
                    this.Parent.OnFilteredChildRemoved(this);
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether this tree node is currently refreshing
        /// asynchronously.
        /// </summary>
        public bool IsRefreshingAsynchronously
        {
            get { return (bool)this.GetValue(IsRefreshingAsynchronouslyProperty); }
            private set { this.SetValue(_isRefreshingAsynchronouslyPropertyKey, value); }
        }

        /// <summary>
        /// Gets a value indicating whether this tree node should be selected on creation.
        /// </summary>
        public bool IsSelectedByDefault
        {
            get
            {
                ISupportsSelectionState pattern = this.Item as ISupportsSelectionState;
                if (pattern == null)
                {
                    return false;
                }

                return pattern.IsSelectedByDefault;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the asynchronous wait animation is currently
        /// visible.
        /// </summary>
        public bool IsSpinAnimationVisible
        {
            get { return (bool)this.GetValue(IsSpinAnimationVisibleProperty); }
            private set { this.SetValue(_isSpinAnimationVisiblePropertyKey, value); }
        }

        /// <summary>
        /// Gets a value indicating whether this tree node is the virtual, invisible, tree node
        /// that acts as the root of tree view hierarchy.
        /// </summary>
        public bool IsVirtualRoot
        {
            get { return this._parent == null; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this tree node is currently visible.
        /// </summary>
        public bool IsVisible
        {
            get { return this._isVisible; }
            set { this._isVisible = value; }
        }

        /// <summary>
        /// Gets or sets the object that is linked to this tree node.
        /// </summary>
        public object Item
        {
            get
            {
                return this._item;
            }

            set
            {
                IProvidesExpandableState oldItem = value as IProvidesExpandableState;
                if (oldItem != null)
                {
                    PropertyChangedEventManager.RemoveHandler(
                        oldItem, this.OnIsExpandableChanged, "IsExpandable");
                }

                this._item = value;
                IProvidesExpandableState newItem = value as IProvidesExpandableState;
                if (newItem != null)
                {
                    PropertyChangedEventManager.AddHandler(
                        newItem, this.OnIsExpandableChanged, "IsExpandable");
                }
            }
        }

        /// <summary>
        /// Gets or sets a collection used to generate the child items for this tree node.
        /// </summary>
        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)this.GetValue(ItemsSourceProperty); }
            set { this.SetValue(ItemsSourceProperty, value); }
        }

        /// <summary>
        /// Gets or sets the reference to the tree node object that is the parent of this.
        /// </summary>
        public VirtualisedTreeNode Parent
        {
            get
            {
                return this._parent;
            }

            set
            {
                if (this._parent != null && this.IsIncludedInFilter)
                {
                    this._parent.FilterChildCount--;
                }

                this._parent = value;
                if (this._parent != null)
                {
                    this.Depth = this._parent._depth + 1;
                    this.UpdateIsFilterActive();
                    if (this.IsIncludedInFilter)
                    {
                        this._parent.FilterChildCount++;
                    }
                }
                else
                {
                    this._depth = -1;
                }
            }
        }

        /// <summary>
        /// Gets a list that contains the placeholder children for this node.
        /// </summary>
        public IList<object> PlaceholderChildItems
        {
            get
            {
                if (this.AreItemsPlaceholders)
                {
                    return (IList<object>)this.ItemsSource;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the index of this tree node in the full collection of tree nodes currently
        /// visible on the parent tree view.
        /// </summary>
        public int TreeIndex
        {
            get
            {
                if (this.TreeView != null)
                {
                    return this.TreeView.Items.IndexOf(this);
                }

                return -1;
            }
        }

        /// <summary>
        /// Gets the reference to the virtualised tree view item control that this tree node
        /// belongs to.
        /// </summary>
        public RsVirtualisedTreeViewItem TreeItem
        {
            get
            {
                return this._treeItem;
            }

            internal set
            {
                if (this._treeItem != null)
                {
                    value.Selected -= this.OnItemSelected;
                    value.Unselected -= this.OnItemUnselected;
                }

                this._treeItem = value;
                if (value != null)
                {
                    value.Selected += this.OnItemSelected;
                    value.Unselected += this.OnItemUnselected;
                }
            }
        }

        /// <summary>
        /// Gets the reference to the virtualised tree view control that this tree node
        /// belongs to.
        /// </summary>
        public RsVirtualisedTreeView TreeView
        {
            get { return this._treeView; }
        }

        /// <summary>
        /// Gets a value indicating whether the expansion for this item is currently visible.
        /// </summary>
        private bool IsExpansionVisible
        {
            get
            {
                if (this.ProcessingBeforeExpand)
                {
                    return false;
                }

                return this.IsVisible && this.IsExpanded && this._initialised;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this tree node is permanently expanded.
        /// </summary>
        private bool IsPermanentlyExpandedRoot
        {
            get
            {
                if (this.IsVirtualRoot)
                {
                    return true;
                }

                if (this._parent == null || this._treeView == null)
                {
                    return false;
                }

                if (this._parent.IsVirtualRoot && !this._treeView.ShowRootExpander)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this item is currently performing a
        /// operation while this item is expanding.
        /// </summary>
        private bool ProcessingBeforeExpand
        {
            get { return this._processingBeforeExpand; }
            set { this._processingBeforeExpand = value; }
        }

        /// <summary>
        /// Gets the time span structure that contains the amount of time that should pass
        /// before the wait spin animation is played.
        /// </summary>
        private TimeSpan WaitTimerDelay
        {
            get
            {
                if (this.IsRefreshingAsynchronously)
                {
                    return TimeSpan.FromMilliseconds(500.0);
                }

                return TimeSpan.FromMilliseconds(100.0);
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Signals the object that initialisation is starting.
        /// </summary>
        public void BeginInit()
        {
            if (this._initialising || this._initialised)
            {
                throw new InvalidOperationException("Can't call BeginInit multiple times");
            }

            this._initialising = true;
        }

        /// <summary>
        /// Adds a handler onto the item to track its disposal notifications if available.
        /// </summary>
        public void BeginLifetimeTracking()
        {
            ISupportsDisposalState pattern = this.Item as ISupportsDisposalState;
            if (pattern == null)
            {
                return;
            }

            EventHandler<PropertyChangedEventArgs> handler = this.OnItemDisposed;
            PropertyChangedEventManager.AddHandler(pattern, handler, "IsDisposed");
        }

        /// <summary>
        /// Disconnects this tree node from is parent tree view.
        /// </summary>
        public void Disconnect()
        {
            this.EndLifetimeTracking();
            var pattern = this.ItemsSource as ISupportInitializeNotification;
            if (pattern != null)
            {
                pattern.Initialized -= this.OnExpandedItemsInitialised;
            }

            BindingOperations.ClearBinding(this, VirtualisedTreeNode.ItemsSourceProperty);
            BindingOperations.ClearBinding(this, VirtualisedTreeNode.IsExpandedProperty);
            this._treeView = null;
            this.Parent = null;
        }

        /// <summary>
        /// Signals the object that initialization is complete.
        /// </summary>
        public void EndInit()
        {
            if (!this._initialising)
            {
                throw new InvalidOperationException("Can't call EndInit without BeginInit");
            }

            if (this._initialised)
            {
                throw new InvalidOperationException("Can't call EndInit multiple times");
            }

            this._initialising = false;
            this._initialised = true;
        }

        /// <summary>
        /// Finds the nearest tree node from this one that is visible. This first look down the
        /// hierarchy and then back up.
        /// </summary>
        /// <returns>
        /// The nearest tree node from this one that is visible.
        /// </returns>
        public VirtualisedTreeNode FindNearestVisibleNode()
        {
            if (this.IsVisible)
            {
                return this;
            }

            if (this.Parent == null)
            {
                return null;
            }

            if (this.Parent._cachedChildNodes != null)
            {
                int index = this.Parent._cachedChildNodes.IndexOf(this);
                if (index >= 0)
                {
                    for (int i = index + 1; i < this.Parent._cachedChildNodes.Count; i++)
                    {
                        VirtualisedTreeNode node = this.Parent._cachedChildNodes[i];
                        if (node.IsVisible)
                        {
                            return node;
                        }
                    }

                    for (int j = index - 1; j >= 0; j--)
                    {
                        VirtualisedTreeNode node = this.Parent._cachedChildNodes[j];
                        if (node.IsVisible)
                        {
                            return node.GetLastVisibleDescendantOrSelf();
                        }
                    }
                }
            }

            return this.Parent.FindNearestVisibleNode();
        }

        /// <summary>
        /// Initialises the child nodes for this node by setting the <see cref="ItemsSource"/>
        /// property to the correct value based on the items being placeholder or not.
        /// </summary>
        public void InitialiseChildNodes()
        {
            if (!this.AreChildNodesInitialised)
            {
                this.InitialiseChildNodesCore();
            }
        }

        /// <summary>
        /// Receives events from the centralized event manager.
        /// </summary>
        /// <param name="managerType">
        /// The type of the System.Windows.WeakEventManager calling this method.
        /// </param>
        /// <param name="sender">
        /// Object that originated the event.
        /// </param>
        /// <param name="e">
        /// Event data.
        /// </param>
        /// <returns>
        /// True if the listener handled the event; otherwise, false.
        /// </returns>
        public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
        {
            if (managerType == typeof(CollectionChangedEventManager))
            {
                this.HandleCollectionChanged((NotifyCollectionChangedEventArgs)e);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Refreshes the child nodes for this node and starts the spinning animation if
        /// required.
        /// </summary>
        /// <returns>
        /// A task that represents the work to do to refresh the child nodes.
        /// </returns>
        public async Task RefreshAsync()
        {
            if (this.IsExpandingAsynchronously || this.IsRefreshingAsynchronously)
            {
                return;
            }

            ISupportsRefresh pattern = this.Item as ISupportsRefresh;
            if (pattern != null)
            {
                this.IsRefreshingAsynchronously = true;
                await pattern.RefreshAsync();
                this.IsRefreshingAsynchronously = false;
            }
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>
        /// A string that represents the current object.
        /// </returns>
        public override string ToString()
        {
            if (this.Item != null)
            {
                return this.Item.ToString();
            }

            return base.ToString();
        }

        /// <summary>
        /// Scrolls this tree node item into view using the parents tree view scroll viewer.
        /// </summary>
        internal void ScrollExpansionIntoView()
        {
            if (!this.IsExpansionVisible)
            {
                return;
            }

            ScrollViewer scrollViewer = this.TreeView.TemplateScrollViewer;
            if (scrollViewer == null || !scrollViewer.CanContentScroll)
            {
                return;
            }

            int treeIndex = this.TreeIndex;
            int num = this.GetLastVisibleDescendantOrSelf().TreeIndex + 1;
            int num2 = num - treeIndex;
            if ((double)num2 > scrollViewer.ViewportHeight)
            {
                scrollViewer.ScrollToVerticalOffset((double)treeIndex);
                return;
            }

            if ((double)num > scrollViewer.VerticalOffset + scrollViewer.ViewportHeight)
            {
                scrollViewer.ScrollToVerticalOffset((double)num - scrollViewer.ViewportHeight);
            }
        }

        /// <summary>
        /// Called whenever the <see cref="IsExpanded"/> dependency property needs to be
        /// re-evaluated.
        /// </summary>
        /// <param name="s">
        /// The <see cref="VirtualisedTreeNode"/> instance whose dependency property needs
        /// evaluated.
        /// </param>
        /// <param name="baseValue">
        /// The new value of the property, prior to any coercion attempt.
        /// </param>
        /// <returns>
        /// The coerced value.
        /// </returns>
        private static object CoerceIsExpanded(DependencyObject s, object baseValue)
        {
            VirtualisedTreeNode node = s as VirtualisedTreeNode;
            if (node == null)
            {
                Debug.Assert(node != null, "Coerce handler attached to wrong object");
                return baseValue;
            }

            object result = baseValue;
            using (new CoerceIsExpandedScopeCounter(node))
            {
                bool flag = (bool)baseValue;
                if (flag && !node.IsExpandable)
                {
                    result = false;
                }
                else
                {
                    if (!flag && node.IsPermanentlyExpandedRoot)
                    {
                        result = true;
                    }
                    else
                    {
                        result = baseValue;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Gets called whenever a asynchronous dependency property changes on a
        /// <see cref="VirtualisedTreeNode"/> instance.
        /// </summary>
        /// <param name="s">
        /// The <see cref="VirtualisedTreeNode"/> instance whose dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data used for this event.
        /// </param>
        private static void OnAsynchronousTriggerPropertyChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            VirtualisedTreeNode node = s as VirtualisedTreeNode;
            if (node == null)
            {
                Debug.Assert(node != null, "Change handler attached to wrong object");
                return;
            }

            if (node.IsExpandingAsynchronously || node.IsRefreshingAsynchronously)
            {
                node.StartSpinWaitTimer();
                return;
            }

            node.StopSpinAnimation();
        }

        /// <summary>
        /// Gets called whenever the <see cref="IsExpanded"/> dependency property changes on a
        /// <see cref="VirtualisedTreeNode"/> instance.
        /// </summary>
        /// <param name="s">
        /// The <see cref="VirtualisedTreeNode"/> instance whose dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data used for this event.
        /// </param>
        private static void OnIsExpandedChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            VirtualisedTreeNode node = s as VirtualisedTreeNode;
            if (node == null)
            {
                Debug.Assert(node != null, "Change handler attached to wrong object");
                return;
            }

            bool flag = (bool)e.NewValue;
            bool oldValue = (bool)e.OldValue;
            if (node._initialised && node.IsVisible)
            {
                if (flag)
                {
                    node.ExpandInternal();
                }
                else
                {
                    node.CollapseInternal();
                }
            }

            if (node.IsConnected)
            {
                if (flag)
                {
                    node.NotifyAfterExpand();
                }
                else
                {
                    node.NotifyAfterCollapse();
                }
            }
        }

        /// <summary>
        /// Gets called whenever the <see cref="ItemsSource"/> dependency property changes on a
        /// <see cref="VirtualisedTreeNode"/> instance.
        /// </summary>
        /// <param name="s">
        /// The <see cref="VirtualisedTreeNode"/> instance whose dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data used for this event.
        /// </param>
        private static void OnItemsSourceChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            VirtualisedTreeNode node = s as VirtualisedTreeNode;
            if (node == null)
            {
                Debug.Assert(node != null, "Change handler attached to wrong object");
                return;
            }

            IEnumerable enumerable = e.NewValue as IEnumerable;
            if (enumerable == null)
            {
                node.ClearItemsSource();
                return;
            }

            node.SetItemsSource(enumerable);
        }

        /// <summary>
        /// Begins filtering of the child nodes.
        /// </summary>
        private void BeginFiltering()
        {
            this.RemoveFilteredNodes();
            this._isFilterActive = true;
            if (this.IsConnected && this.TreeView.AutoExpandFilteredItems)
            {
                if (this.IsExpanded == false)
                {
                    this.IsExpanded = true;
                }
            }

            foreach (VirtualisedTreeNode node in this._cachedChildNodes)
            {
                node.UpdateIsFilterActive();
            }
        }

        /// <summary>
        /// Clears all of the nodes from the parents internal node list.
        /// </summary>
        private void ClearAllNodes()
        {
            foreach (VirtualisedTreeNode node in this.TreeView.InternalItems)
            {
                node.IsVisible = false;
            }

            this.TreeView.InternalItems.Clear();
        }

        /// <summary>
        /// Clears the items that have been created using the current items source. This
        /// includes removing the child items if the expansion is visible and unhooking events
        /// attached to the current collection view.
        /// </summary>
        private void ClearItemsSource()
        {
            if (this._itemsSourceView == null)
            {
                return;
            }

            if (this.IsExpansionVisible)
            {
                this.RemoveChildItems();
            }

            if (this.IsConnected)
            {
                this.SyncChildNodes(null);
            }

            this.UnhookCollectionView(this._itemsSourceView);
            this._itemsSourceView = null;
            this._cachedChildNodes.Clear();
        }

        /// <summary>
        /// Collapses this node and removes all of its children from the parent tree view.
        /// </summary>
        private void CollapseInternal()
        {
            this.RemoveChildItems();
            ISupportsExpansionEvents pattern = this.Item as ISupportsExpansionEvents;
            if (pattern != null)
            {
                try
                {
                    pattern.AfterCollapse();
                }
                catch (Exception)
                {
                    Debug.Assert(false, "Exception caught in \"AfterCollapse\" method.");
                }
            }
        }

        /// <summary>
        /// Converts the specified index of a child node into the zero-based index into the
        /// parents internal list.
        /// </summary>
        /// <param name="collectionIndex">
        /// The zero-based index to convert.
        /// </param>
        /// <returns>
        /// The index in the parent tree views internal list that corresponds to the specified
        /// index in the child nodes.
        /// </returns>
        private int ConvertCollectionIndexToTreeIndex(int collectionIndex)
        {
            for (int i = collectionIndex; i < this._cachedChildNodes.Count; i++)
            {
                if (this._cachedChildNodes[i].IsVisible)
                {
                    return this._cachedChildNodes[i].TreeIndex;
                }
            }

            return this.GetLastVisibleDescendantOrSelf().TreeIndex + 1;
        }

        /// <summary>
        /// Ends filtering of the child nodes.
        /// </summary>
        private void EndFiltering()
        {
            if (!this.AreItemsPlaceholders)
            {
                if (this.IsExpansionVisible)
                {
                    for (int i = this._cachedChildNodes.Count - 1; i >= 0; i--)
                    {
                        if (!this._cachedChildNodes[i].IsVisible)
                        {
                            int index = this.ConvertCollectionIndexToTreeIndex(i);
                            this.InsertNodeAndDescendants(index, this._cachedChildNodes[i]);
                        }
                    }
                }

                this._isFilterActive = false;
                foreach (VirtualisedTreeNode node in this._cachedChildNodes)
                {
                    node.UpdateIsFilterActive();
                }

                return;
            }

            IList<VirtualisedTreeNode> list = null;
            if (this.IsExpanded && this.IsConnected && !this.TreeView.AutoExpandFilteredItems)
            {
                list = new List<VirtualisedTreeNode>(this._cachedChildNodes);
            }

            this._isFilterActive = false;
            this.ItemsSource = null;
            this.AreChildNodesInitialised = false;
            this.AreItemsPlaceholders = false;
            this.IsExpanded = false;
            if (list == null)
            {
                return;
            }

            this.RestorePreviousExpansion(list);
        }

        /// <summary>
        /// Removes the handlers on the item used to track its disposal notifications if
        /// present.
        /// </summary>
        private void EndLifetimeTracking()
        {
            ISupportsDisposalState pattern = this.Item as ISupportsDisposalState;
            if (pattern == null)
            {
                return;
            }

            EventHandler<PropertyChangedEventArgs> handler = this.OnItemDisposed;
            PropertyChangedEventManager.RemoveHandler(pattern, handler, "IsDisposed");
        }

        /// <summary>
        /// Expands this node and makes sure the child items are initialised and shown.
        /// </summary>
        private void ExpandInternal()
        {
            ISupportsExpansionEvents pattern = this.Item as ISupportsExpansionEvents;
            if (pattern != null)
            {
                try
                {
                    this.ProcessingBeforeExpand = true;
                    pattern.BeforeExpand();
                }
                catch (Exception)
                {
                    Debug.Assert(false, "Exception caught in \"BeforeExpand\" method.");
                }
                finally
                {
                    this.ProcessingBeforeExpand = false;
                }
            }

            if (!this.IsExpansionVisible)
            {
                return;
            }

            if (!this.AreChildNodesInitialised)
            {
                this.InitialiseChildNodesCore();
                return;
            }

            if (this._itemsSourceView != null)
            {
                this.InsertChildItems(this._itemsSourceView);
            }
        }

        /// <summary>
        /// Retrieves the last visible descendant of this node by recursively looking into the
        /// child nodes or its self.
        /// </summary>
        /// <returns>
        /// The last visible descendant of this node or if not found itself.
        /// </returns>
        private VirtualisedTreeNode GetLastVisibleDescendantOrSelf()
        {
            VirtualisedTreeNode node = null;
            for (int i = this._cachedChildNodes.Count - 1; i >= 0; i--)
            {
                if (this._cachedChildNodes[i].IsVisible)
                {
                    node = this._cachedChildNodes[i];
                    break;
                }
            }

            if (node == null)
            {
                return this;
            }

            if (node.IsExpanded)
            {
                return node.GetLastVisibleDescendantOrSelf();
            }

            return node;
        }

        /// <summary>
        /// Called whenever the default collection view wrapping the current items source for
        /// this node changes.
        /// </summary>
        /// <param name="e">
        /// The System.Collections.Specialized.NotifyCollectionChangedEventArgs data containing
        /// the change arguments.
        /// </param>
        private void HandleCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (!this.IsConnected)
            {
                return;
            }

            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    this.InsertChangedItems(e.NewStartingIndex, e.NewItems);
                    return;
                case NotifyCollectionChangedAction.Remove:
                    this.RemoveChangedItems(e.OldStartingIndex, e.OldItems, true);
                    return;
                case NotifyCollectionChangedAction.Replace:
                    this.ReplaceChangedItems(e.OldStartingIndex, e.OldItems, e.NewItems);
                    break;
                case NotifyCollectionChangedAction.Move:
                    this.MoveChangedItems(e.OldStartingIndex, e.NewStartingIndex, e.NewItems);
                    return;
                case NotifyCollectionChangedAction.Reset:
                    this.ResetChangedItems();
                    return;
                default:
                    return;
            }
        }

        /// <summary>
        /// Adds a weak event listener to the collection changed event for the specified
        /// collection.
        /// </summary>
        /// <param name="view">
        /// The view collection that the listener should be hooked up to.
        /// </param>
        private void HookCollectionView(ICollectionView view)
        {
            CollectionChangedEventManager.AddListener(view, this);
        }

        /// <summary>
        /// Initialises the child nodes for this node by setting the <see cref="ItemsSource"/>
        /// property to the correct value based on the items being placeholder or not.
        /// </summary>
        private void InitialiseChildNodesCore()
        {
            if (this.AreItemsPlaceholders)
            {
                var collection = new ObservableCollection<object>();
                ICollectionView view = CollectionViewSource.GetDefaultView(collection);
                ListCollectionView listCollectionView = view as ListCollectionView;
                if (this._treeView == null || this._treeView.DisableSorting == false)
                {
                    listCollectionView.CustomSort = PrioritisedComparer.Instance;
                }

                this.ItemsSource = collection;
            }
            else
            {
                Binding binding = new Binding(this.TreeView.ItemsPath);
                binding.Source = this.Item;
                binding.Converter = this.TreeView.ItemsConverter;
                BindingOperations.SetBinding(
                    this, VirtualisedTreeNode.ItemsSourceProperty, binding);
            }

            this.AreChildNodesInitialised = true;
        }

        /// <summary>
        /// Called whenever items are added to the current items source and inserts them in
        /// both the parents internal list and the child cache.
        /// </summary>
        /// <param name="childIndex">
        /// The starting index for the added items.
        /// </param>
        /// <param name="items">
        /// The items that have been added.
        /// </param>
        private void InsertChangedItems(int childIndex, IList items)
        {
            if (this.IsExpansionVisible)
            {
                int index = this.ConvertCollectionIndexToTreeIndex(childIndex);
                this.InsertItems(index, items);
            }

            foreach (object item in items)
            {
                VirtualisedTreeNode node = this.TreeView.GetOrCreateNode(item, this);
                this._cachedChildNodes.Insert(childIndex, node);
                childIndex++;
            }
        }

        /// <summary>
        /// Inserts the items specified in the given enumerable into this nodes parent internal
        /// list of items at the correct index to be child items of this node.
        /// </summary>
        /// <param name="childItems">
        /// A iterator around the items to insert.
        /// </param>
        private void InsertChildItems(IEnumerable childItems)
        {
            int index = 0;
            if (!this.IsVirtualRoot)
            {
                foreach (object item in this.TreeView.InternalItems)
                {
                    if (Object.ReferenceEquals(item, this))
                    {
                        index += 1;
                        break;
                    }

                    index++;
                }
            }

            this.InsertItems(index, childItems);
        }

        /// <summary>
        /// Inserts the items specified in the given enumerable into this nodes parent internal
        /// list at the specified index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index to insert the items at.
        /// </param>
        /// <param name="items">
        /// A iterator around the items to insert.
        /// </param>
        private void InsertItems(int index, IEnumerable items)
        {
            List<VirtualisedTreeNode> list = new List<VirtualisedTreeNode>();
            foreach (object item in items)
            {
                VirtualisedTreeNode node = this.TreeView.GetOrCreateNode(item, this);
                if (!this.IsFilterActive || node.IsIncludedInFilter)
                {
                    this.InsertNode(index, node);
                    if (node.IsExpanded)
                    {
                        list.Add(node);
                    }

                    index++;
                }
            }

            foreach (VirtualisedTreeNode node in list)
            {
                node.ExpandInternal();
            }
        }

        /// <summary>
        /// Inserts the specified node into this nodes parent internal list at the specified
        /// index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index to insert the items at.
        /// </param>
        /// <param name="node">
        /// The node to insert.
        /// </param>
        private void InsertNode(int index, VirtualisedTreeNode node)
        {
            node.IsVisible = true;
            this.TreeView.InternalItems.Insert(index, node);
        }

        /// <summary>
        /// Inserts the specified node into this nodes parent internal list at the specified
        /// index and its children.
        /// </summary>
        /// <param name="index">
        /// The zero-based index to insert the items at.
        /// </param>
        /// <param name="node">
        /// The node to insert.
        /// </param>
        private void InsertNodeAndDescendants(int index, VirtualisedTreeNode node)
        {
            this.InsertNode(index, node);
            if (node.IsExpanded)
            {
                node.ExpandInternal();
            }
        }

        /// <summary>
        /// Called whenever items are moved inside the current items source and moves them in
        /// both the parents internal list and the child cache.
        /// </summary>
        /// <param name="oldIndex">
        /// The index where the items were moved from.
        /// </param>
        /// <param name="newIndex">
        /// The index where the items were moved to.
        /// </param>
        /// <param name="newItems">
        /// The items that were moved.
        /// </param>
        private void MoveChangedItems(int oldIndex, int newIndex, IList newItems)
        {
            this.RemoveChangedItems(oldIndex, newItems, false);
            this.InsertChangedItems(newIndex, newItems);
        }

        /// <summary>
        /// Performs the logic needed after this node has been collapsed.
        /// </summary>
        private void NotifyAfterCollapse()
        {
            var pattern = this.ItemsSource as ISupportInitializeNotification;
            if (pattern != null && !pattern.IsInitialized)
            {
                this.IsExpandingAsynchronously = false;
                pattern.Initialized -= this.OnExpandedItemsInitialised;
            }

            EventHandler handler = this.Collapsed;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Performs the logic needed after this node has been expanded.
        /// </summary>
        private void NotifyAfterExpand()
        {
            this.InitialiseChildNodes();
            EventHandler handler = this.Expanded;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }

            var pattern = this.ItemsSource as ISupportInitializeNotification;
            if (pattern == null || pattern.IsInitialized)
            {
                this.OnExpandedItemsLoaded();
                return;
            }

            this.IsExpandingAsynchronously = true;
            pattern.Initialized += this.OnExpandedItemsInitialised;
        }

        /// <summary>
        /// Called when the collection source of the child items have been initialised through
        /// the System.ComponentModel.ISupportInitializeNotification pattern.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.EventArgs data for the event.
        /// </param>
        private void OnExpandedItemsInitialised(object s, EventArgs e)
        {
            ISupportInitializeNotification pattern = s as ISupportInitializeNotification;
            if (pattern == null)
            {
                Debug.Assert(pattern != null, "Handler attached to the incorrect object type");
                return;
            }

            pattern.Initialized -= this.OnExpandedItemsInitialised;
            this.IsExpandingAsynchronously = false;
            this.OnExpandedItemsLoaded();
        }

        /// <summary>
        /// Called whenever the child items have been loaded.
        /// </summary>
        private void OnExpandedItemsLoaded()
        {
            EventHandler handler = this.ExpandedItemsLoaded;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Called whenever a child node registers itself as being included in the current
        /// filter.
        /// </summary>
        /// <param name="node">
        /// The node that has been included in the filter.
        /// </param>
        private void OnFilteredChildAdded(VirtualisedTreeNode node)
        {
            this.FilterChildCount++;
            if (this.IsFilterActive && this.IsExpansionVisible && !node.IsVisible)
            {
                int index = this._cachedChildNodes.IndexOf(node);
                int treeIndex = this.ConvertCollectionIndexToTreeIndex(index);
                this.InsertNodeAndDescendants(treeIndex, node);
            }
        }

        /// <summary>
        /// Called whenever a child node unregisters itself as being included in the current
        /// filter.
        /// </summary>
        /// <param name="childNode">
        /// The node that has been removed from the filter.
        /// </param>
        private void OnFilteredChildRemoved(VirtualisedTreeNode childNode)
        {
            this.FilterChildCount--;
            if (!this.IsConnected)
            {
                return;
            }

            if (this.IsFilterActive && this.IsExpansionVisible && childNode.IsVisible)
            {
                this.RemoveNodeAndDescendants(childNode);
            }

            if (this.AreItemsPlaceholders)
            {
                this.PlaceholderChildItems.Remove(childNode.Item);
                this.TreeView.RemoveNodeCore(childNode);
            }
        }

        /// <summary>
        /// Called whenever the item updates its own IsExpandable value.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.ComponentModel.PropertyChangedEventArgs event data.
        /// </param>
        private void OnIsExpandableChanged(object s, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "IsExpandable")
            {
                return;
            }

            DispatcherHelper.CheckBeginInvokeOnUI(
                new Action(
                delegate
                {
                    if (this._treeItem != null)
                    {
                        BindingExpression expression = BindingOperations.GetBindingExpression(
                            this._treeItem, RsVirtualisedTreeViewItem.IsExpandableProperty);
                        if (expression != null)
                        {
                            expression.UpdateTarget();
                        }
                    }

                    this.RaisePropertyChanged("IsExpandable");
                    if (this._isInCoerceIsExpandedCount == 0)
                    {
                        if (this.IsExpandable)
                        {
                            this.IsExpanded = this.IsExpandedByDefault;
                            return;
                        }

                        this.IsExpanded = false;
                    }
                }));
        }

        /// <summary>
        /// Called once the placeholder item has been disposed of.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.ComponentModel.PropertyChangedEventArgs event data.
        /// </param>
        private void OnItemDisposed(object s, PropertyChangedEventArgs e)
        {
            ISupportsDisposalState pattern = s as ISupportsDisposalState;
            if (pattern.IsDisposed && this.Parent != null)
            {
                this.IsIncludedInFilter = false;
            }
        }

        /// <summary>
        /// Called whenever the tree view item is selected so that view models conforming to
        /// the selection event pattern can be notified.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private void OnItemSelected(object sender, RoutedEventArgs e)
        {
            ISupportsSelectionEvents pattern = this.Item as ISupportsSelectionEvents;
            if (pattern != null)
            {
                try
                {
                    pattern.BeforeSelected();
                }
                catch (Exception)
                {
                    Debug.Assert(false, "Exception caught in \"BeforeSelected\" method.");
                }
            }
        }

        /// <summary>
        /// Called whenever the tree view item is unselected so that view models conforming to
        /// the selection event pattern can be notified.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private void OnItemUnselected(object sender, RoutedEventArgs e)
        {
            if (!Object.ReferenceEquals(this._treeItem.Content, this))
            {
                return;
            }

            ISupportsSelectionEvents pattern = this.Item as ISupportsSelectionEvents;
            if (pattern != null)
            {
                try
                {
                    pattern.AfterUnselected();
                }
                catch (Exception)
                {
                    Debug.Assert(false, "Exception caught in \"AfterUnselected\" method.");
                }
            }
        }

        /// <summary>
        /// Called whenever the spin wait timer finishes.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.EventArgs data for the event.
        /// </param>
        private void OnStartSpin(object sender, EventArgs e)
        {
            this.IsSpinAnimationVisible = true;
            this.StopSpinWaitTimer();
        }

        /// <summary>
        /// Raises the <see cref="PropertyChanged"/> event for this object for the specified
        /// property name.
        /// </summary>
        /// <param name="propertyName">
        /// The name of the property that has changed.
        /// </param>
        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler == null)
            {
                return;
            }

            handler(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Called whenever items are removed from the current items source and removes them in
        /// both the parents internal list and the child cache.
        /// </summary>
        /// <param name="childIndex">
        /// The index where the items were removed from.
        /// </param>
        /// <param name="items">
        /// The items that were removed.
        /// </param>
        /// <param name="removeFromParent">
        /// A value indicating where the items should be removed from the parents internal
        /// list.
        /// </param>
        private void RemoveChangedItems(int childIndex, IList items, bool removeFromParent)
        {
            bool itemSelected = this.TreeView.SelectedItems.Count > 0;
            if (this.IsExpansionVisible)
            {
                int num = childIndex + items.Count;
                for (int i = childIndex; i < num; i++)
                {
                    VirtualisedTreeNode treeNode = this._cachedChildNodes[i];
                    if (treeNode.IsVisible)
                    {
                        this.RemoveNodeAndDescendants(treeNode);
                    }
                }
            }

            var range = this._cachedChildNodes.GetRange(childIndex, items.Count);
            this._cachedChildNodes.RemoveRange(childIndex, items.Count);
            if (removeFromParent)
            {
                foreach (VirtualisedTreeNode current in range)
                {
                    this.TreeView.RemoveTreeNode(current);
                }
            }

            if (!itemSelected || this.TreeView.SelectedItems.Count != 0)
            {
                return;
            }

            if (childIndex >= this._cachedChildNodes.Count)
            {
                childIndex = this._cachedChildNodes.Count - 1;
            }

            VirtualisedTreeNode target = null;
            if (this._cachedChildNodes.Count > 0)
            {
                target = this._cachedChildNodes[childIndex];
            }
            else if (!this.IsVirtualRoot)
            {
                target = this;
            }

            if (target == null)
            {
                return;
            }

            this.TreeView.ChangeSelection(target, SelectionAction.SingleSelection, false);
        }

        /// <summary>
        /// Removes all of this nodes children from the internal list of items on its parent.
        /// </summary>
        private void RemoveChildItems()
        {
            if (this.TreeView == null)
            {
                return;
            }

            if (this.IsVirtualRoot)
            {
                this.ClearAllNodes();
                return;
            }

            int num = this.TreeView.InternalItems.IndexOf(this);
            int treeIndex = this.GetLastVisibleDescendantOrSelf().TreeIndex;
            int num2 = treeIndex;
            bool itemSelected = this.TreeView.SelectedItems.Count > 0;
            while (num2 > num && this.TreeView.SelectedItems.Count > 0)
            {
                this.TreeView.SelectedItems.Remove(this.TreeView.InternalItems[num2]);
                num2--;
            }

            if (itemSelected && this.TreeView.SelectedItems.Count == 0)
            {
                this.TreeView.ChangeSelection(this, SelectionAction.SingleSelection, true);
            }

            if (this.IsVisible)
            {
                for (int i = treeIndex; i > num; i--)
                {
                    this.RemoveNodeAt(i);
                }
            }
        }

        /// <summary>
        /// Removes all of the child nodes that are being shown because they are included in
        /// the current filter.
        /// </summary>
        private void RemoveFilteredNodes()
        {
            if (!this.IsExpansionVisible)
            {
                return;
            }

            foreach (VirtualisedTreeNode node in this._cachedChildNodes)
            {
                if (!node.IsIncludedInFilter)
                {
                    this.RemoveNodeAndDescendants(node);
                }
            }
        }

        /// <summary>
        /// Removes the specified node and all of its descendants.
        /// </summary>
        /// <param name="node">
        /// The node to remove.
        /// </param>
        private void RemoveNodeAndDescendants(VirtualisedTreeNode node)
        {
            node.RemoveChildItems();
            if (node.IsVisible)
            {
                this.RemoveNodeAt(node.TreeIndex);
            }
        }

        /// <summary>
        /// Removes the node in the parents internal list at the specified index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index of the node to remove.
        /// </param>
        private void RemoveNodeAt(int index)
        {
            object item = this.TreeView.InternalItems[index];
            VirtualisedTreeNode node = item as VirtualisedTreeNode;
            if (node == null)
            {
                Debug.Assert(node != null, "Incorrect type added to tree!");
            }
            else
            {
                node.IsVisible = false;
            }

            this.TreeView.InternalItems.RemoveAt(index);
            ISupportsExpansionState pattern = this.Item as ISupportsExpansionState;
            if (pattern != null && pattern.DestroyNodesOnCollapse)
            {
                this.TreeView.RemoveNodeCore(node);
            }
        }

        /// <summary>
        /// Called whenever items inside the current items source are replaced and makes sure
        /// the child nodes and the tree views internal list are in sync.
        /// </summary>
        /// <param name="index">
        /// The zero-based index where the replacement occurred.
        /// </param>
        /// <param name="oldItems">
        /// The items that were removed.
        /// </param>
        /// <param name="newItems">
        /// The items that were added.
        /// </param>
        private void ReplaceChangedItems(int index, IList oldItems, IList newItems)
        {
            this.RemoveChangedItems(index, oldItems, true);
            this.InsertChangedItems(index, newItems);
        }

        /// <summary>
        /// Called whenever the items inside the current items source are reset and makes sure
        /// the child nodes and the tree views internal list are in sync.
        /// </summary>
        private void ResetChangedItems()
        {
            if (this.IsExpansionVisible)
            {
                this.RemoveChildItems();
            }

            this.SyncChildNodes(this._itemsSourceView);
            if (this.IsExpansionVisible)
            {
                this.InsertChildItems(this._itemsSourceView);
            }
        }

        /// <summary>
        /// Restores the previous expansion after the current filter has ended. This is so that
        /// during a filter the filtered items can be automatically expanded and on clear the
        /// tree view can go make to its original state.
        /// </summary>
        /// <param name="previousNodes">
        /// A iterator around the previous nodes that were visible before the filtering.
        /// </param>
        private void RestorePreviousExpansion(IEnumerable<VirtualisedTreeNode> previousNodes)
        {
            this.IsExpanded = true;
            var map = new Dictionary<object, VirtualisedTreeNode>();
            HashSet<VirtualisedTreeNode> expanded = new HashSet<VirtualisedTreeNode>();
            foreach (VirtualisedTreeNode node in previousNodes)
            {
                if (!node.IsExpanded)
                {
                    continue;
                }

                expanded.Add(node);
            }

            foreach (VirtualisedTreeNode node in expanded)
            {
                object key = this.TreeView.GetKeyFromItem(node.Item);
                map[key] = node;
            }

            foreach (VirtualisedTreeNode node in this._cachedChildNodes)
            {
                VirtualisedTreeNode mappedNode;
                object key = this.TreeView.GetKeyFromItem(node.Item);
                if (map.TryGetValue(key, out mappedNode))
                {
                    node.RestorePreviousExpansion(mappedNode._cachedChildNodes);
                }
                else
                {
                    node.IsExpanded = false;
                }
            }
        }

        /// <summary>
        /// Sets the current items source to the specified iterator and syncs the child nodes
        /// with them.
        /// </summary>
        /// <param name="items">
        /// The iterator over the items this nodes item source should be set to.
        /// </param>
        private void SetItemsSource(IEnumerable items)
        {
            this.ClearItemsSource();
            ICollectionView collection = CollectionViewSource.GetDefaultView(items);
            this._itemsSourceView = collection as CollectionView;
            ListCollectionView listCollectionView = collection as ListCollectionView;
            if (listCollectionView != null)
            {
                if (this._treeView == null || this._treeView.DisableSorting == false)
                {
                    listCollectionView.CustomSort = PrioritisedComparer.Instance;
                }
            }

            if (this.IsConnected)
            {
                this.SyncChildNodes(this._itemsSourceView);
            }

            this.HookCollectionView(this._itemsSourceView);
            if (this.IsExpansionVisible)
            {
                this.InsertChildItems(this._itemsSourceView);
            }
        }

        /// <summary>
        /// Starts the spin wait timer.
        /// </summary>
        private void StartSpinWaitTimer()
        {
            this._waitTimer = new DispatcherTimer(
                this.WaitTimerDelay,
                DispatcherPriority.Normal,
                this.OnStartSpin,
                this.Dispatcher);

            this._waitTimer.Start();
        }

        /// <summary>
        /// Stops the spin animation that is shown during asynchronous methods.
        /// </summary>
        private void StopSpinAnimation()
        {
            this.IsSpinAnimationVisible = false;
            this.StopSpinWaitTimer();
        }

        /// <summary>
        /// Stops the spin wait timer.
        /// </summary>
        private void StopSpinWaitTimer()
        {
            if (this._waitTimer == null)
            {
                return;
            }

            this._waitTimer.Stop();
            this._waitTimer = null;
        }

        /// <summary>
        /// Syncs the child nodes to the specified enumerable of object, making sure there is
        /// a one-one mapping.
        /// </summary>
        /// <param name="source">
        /// The source that the child nodes should be synced to.
        /// </param>
        private void SyncChildNodes(IEnumerable source)
        {
            var cache = new HashSet<VirtualisedTreeNode>(this._cachedChildNodes);
            this._cachedChildNodes.Clear();
            if (source != null)
            {
                while (true)
                {
                    using (var enumerable = new StableEnumerable(source))
                    {
                        foreach (object current in enumerable)
                        {
                            var node = this.TreeView.GetOrCreateNode(current, this);
                            this._cachedChildNodes.Add(node);
                            cache.Add(node);
                            if (enumerable.DetectedChange)
                            {
                                break;
                            }
                        }

                        if (enumerable.DetectedChange)
                        {
                            this._cachedChildNodes.Clear();
                            continue;
                        }
                    }

                    break;
                }

                foreach (VirtualisedTreeNode node in this._cachedChildNodes)
                {
                    cache.Remove(node);
                }
            }

            foreach (VirtualisedTreeNode node in cache)
            {
                this.TreeView.RemoveTreeNode(node);
            }
        }

        /// <summary>
        /// Removes the weak event listener from the collection changed event for the specified
        /// collection.
        /// </summary>
        /// <param name="view">
        /// The view collection that the listener should be unhooked from.
        /// </param>
        private void UnhookCollectionView(ICollectionView view)
        {
            CollectionChangedEventManager.RemoveListener(view, this);
        }

        /// <summary>
        /// Updates whether or not a current filter is currently active.
        /// </summary>
        private void UpdateIsFilterActive()
        {
            if (this.Parent == null)
            {
                return;
            }

            if (!this.Parent.IsFilterActive || this.TreeView.FilteredItemsSource == null)
            {
                this.IsFilterActive = false;
                return;
            }

            if (this.FilterChildCount > 0)
            {
                this.IsFilterActive = true;
                return;
            }

            FilterDescendantBehaviour behaviour = this.TreeView.GetFilterBehaviour(this);
            if (behaviour == FilterDescendantBehaviour.ExcludeDescendantsByDefault)
            {
                this.IsFilterActive = true;
            }
            else
            {
                this.IsFilterActive = false;
            }
        }

        /// <summary>
        /// Updates whether this node is currently included in the current filter.
        /// </summary>
        private void UpdateIsIncludedInFilter()
        {
            if (this.IsVirtualRoot)
            {
                this.IsIncludedInFilter = true;
                return;
            }

            if (this.IsExplicitlyIncludedInFilter || this.FilterChildCount > 0)
            {
                this.IsIncludedInFilter = true;
                return;
            }

            this.IsIncludedInFilter = false;
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// Provides a way of keeping track of the number of times the
        /// <see cref="CoerceIsExpanded"/> method has been entered.
        /// </summary>
        private class CoerceIsExpandedScopeCounter : DisposableObject
        {
            #region Fields
            /// <summary>
            /// The private reference to the tree node whose coerce is expanded counter is
            /// being tracked.
            /// </summary>
            private VirtualisedTreeNode _scope;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="CoerceIsExpandedScopeCounter"/>
            /// class.
            /// </summary>
            /// <param name="scope">
            /// The tree node whose coerce is expanded counter is being tracked.
            /// </param>
            public CoerceIsExpandedScopeCounter(VirtualisedTreeNode scope)
            {
                if (scope == null)
                {
                    throw new SmartArgumentNullException(() => scope);
                }

                this._scope = scope;
                this._scope._isInCoerceIsExpandedCount++;
            }
            #endregion Constructors

            #region Methods
            /// <summary>
            /// When overridden disposes of the managed resources.
            /// </summary>
            protected override void DisposeManagedResources()
            {
                this._scope._isInCoerceIsExpandedCount--;
            }
            #endregion Methods
        } // VirtualisedTreeNode.CoerceIsExpandedScopeCounter {Class}

        /// <summary>
        /// Compares two objects based on their priority from the
        /// <see cref="IPrioritisedComparable"/> interface.
        /// </summary>
        private class PrioritisedComparer : IComparer
        {
            #region Fields
            /// <summary>
            /// The private field used for the <see cref="Instance"/> property.
            /// </summary>
            private static IComparer _instance;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Prevents a default instance of the <see cref="PrioritisedComparer"/> class from
            /// being created.
            /// </summary>
            private PrioritisedComparer()
            {
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets the singleton instance for this class type.
            /// </summary>
            public static IComparer Instance
            {
                get
                {
                    IComparer singleton;
                    if ((singleton = _instance) == null)
                    {
                        singleton = _instance = new PrioritisedComparer();
                    }

                    return singleton;
                }
            }
            #endregion Properties

            #region Methods
            /// <summary>
            /// Compares two objects and returns a value indicating whether one is less than,
            /// equal to, or greater than the other.
            /// </summary>
            /// <param name="x">
            /// The first object to compare.
            /// </param>
            /// <param name="y">
            /// The second object to compare.
            /// </param>
            /// <returns>
            /// A signed integer that indicates the relative values of x and y. Less than zero
            /// x is less than y. Zero x equals y. Greater than zero x is greater than y.
            /// </returns>
            public int Compare(object x, object y)
            {
                IPrioritisedComparable comparable1 = x as IPrioritisedComparable;
                IPrioritisedComparable comparable2 = y as IPrioritisedComparable;
                if (comparable1 == null || comparable2 == null)
                {
                    IDisplayItem displayItem1 = x as IDisplayItem;
                    IDisplayItem displayItem2 = y as IDisplayItem;
                    if (displayItem1 != null && displayItem2 != null)
                    {
                        return Comparer.Default.Compare(displayItem1.Text, displayItem2.Text);
                    }

                    if (x is IComparable || y is IComparable)
                    {
                        return Comparer.Default.Compare(x, y);
                    }

                    return 0;
                }

                int num = comparable1.Priority.CompareTo(comparable2.Priority);
                if (num == 0)
                {
                    num = comparable1.CompareTo(comparable2);
                }

                return num;
            }
            #endregion Methods
        } // VirtualisedTreeNode.PrioritisedComparer {Class}
        #endregion
    } // RSG.Editor.Controls.Helpers.VirtualisedTreeNode {Class}
} // RSG.Editor.Controls.Helpers {Namespace}
