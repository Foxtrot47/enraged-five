﻿//---------------------------------------------------------------------------------------------
// <copyright file="CommandFocusManager.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Input;
    using System.Windows.Interop;
    using System.Windows.Threading;
    using RSG.Editor.Controls.AttachedDependencyProperties;
    using RSG.Editor.Controls.Commands;
    using RSG.Editor.Controls.Helpers;
    using RSG.Interop.Microsoft.Windows;

    /// <summary>
    /// A manager class handling all of the focus related situations for the command framework.
    /// </summary>
    internal static class CommandFocusManager
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="CurrentMenuModeSource"/> property.
        /// </summary>
        private static PresentationSource _currentMenuModeSource;

        /// <summary>
        /// A helper object used to restore focus to a previously focused element.
        /// </summary>
        private static CommandRestoreFocusScope _restoreFocusScope;
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets or sets the presentation source object that is acting as the current root of
        /// the menu mode push.
        /// </summary>
        private static PresentationSource CurrentMenuModeSource
        {
            get
            {
                return _currentMenuModeSource;
            }

            set
            {
                if (_currentMenuModeSource == value)
                {
                    return;
                }

                PresentationSource presentationSource = _currentMenuModeSource;
                _currentMenuModeSource = value;
                try
                {
                    if (_currentMenuModeSource != null)
                    {
                        InputManager.Current.PushMenuMode(_currentMenuModeSource);
                    }
                }
                finally
                {
                    if (presentationSource != null)
                    {
                        InputManager.Current.PopMenuMode(presentationSource);
                    }
                }
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Initialises this static class by attaching event handlers onto the command controls
        /// and set some global properties.
        /// </summary>
        public static void Initialise()
        {
            HwndSource.DefaultAcquireHwndFocusInMenuMode = false;
            RegisterClassHandlers(typeof(RsMenu));
            RegisterClassHandlers(typeof(RsContextMenu));
            RegisterClassHandlers(typeof(RsToolBar));
            EventManager.RegisterClassHandler(
                typeof(ContextMenu),
                ContextMenu.ClosedEvent,
                new RoutedEventHandler(OnContextMenuClosed));

            EventManager.RegisterClassHandler(
                typeof(FrameworkElement),
                FrameworkElement.ContextMenuOpeningEvent,
                new RoutedEventHandler(OnContextMenuOpening));

            EventManager.RegisterClassHandler(
                typeof(FrameworkElement),
                FrameworkElement.ContextMenuClosingEvent,
                new RoutedEventHandler(OnContextMenuClosing));

            InputManager.Current.LeaveMenuMode += delegate(object param0, EventArgs param1)
            {
                CorrectDetachedHwndFocus();
            };
        }

        /// <summary>
        /// Determines whether the specified element is a registered command focus control.
        /// </summary>
        /// <param name="element">
        /// The element to test.
        /// </param>
        /// <returns>
        /// True if the specified element is a type of registered command control; otherwise,
        /// false.
        /// </returns>
        public static bool IsRegisteredCommandFocusElement(DependencyObject element)
        {
            return element is RsMenu || element is RsContextMenu || element is RsToolBar;
        }

        /// <summary>
        /// Determines whether the specified element is apart of a registered command focus
        /// controls visual tree.
        /// </summary>
        /// <param name="element">
        /// The element to test.
        /// </param>
        /// <returns>
        /// True if the specified element is inside the visual tree of a command control;
        /// otherwise, false.
        /// </returns>
        internal static bool IsInsideCommandContainer(IInputElement element)
        {
            DependencyObject obj = element as DependencyObject;
            if (obj == null)
            {
                return false;
            }

            foreach (DependencyObject ancestor in obj.GetVisualOrLogicalAncestors(true))
            {
                if (IsRegisteredCommandFocusElement(ancestor))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Corrects the focus after a element has left menu mode.
        /// </summary>
        private static void CorrectDetachedHwndFocus()
        {
            DependencyObject obj = Keyboard.FocusedElement as DependencyObject;
            if (obj == null)
            {
                return;
            }

            HwndSource hwndSource = PresentationSource.FromDependencyObject(obj) as HwndSource;
            if (hwndSource == null)
            {
                return;
            }

            IntPtr previousFocus = User32.GetFocus();
            if (previousFocus != hwndSource.Handle)
            {
                IntPtr result = User32.SetFocus(hwndSource.Handle);
                Debug.Assert(result != IntPtr.Zero, "Failed to set focus");
            }
        }

        /// <summary>
        /// Retrieves the parent focus scope of the specified focus scope.
        /// </summary>
        /// <param name="focusScope">
        /// The focus scope whose parent will be retrieved.
        /// </param>
        /// <returns>
        /// The parent focus scope of the specified focus scope.
        /// </returns>
        private static DependencyObject GetParentFocusScope(DependencyObject focusScope)
        {
            if (focusScope == null)
            {
                return null;
            }

            DependencyObject parent = focusScope.GetVisualOrLogicalAncestor();
            if (parent != null)
            {
                return FocusManager.GetFocusScope(parent);
            }

            return null;
        }

        /// <summary>
        /// Determines whether a command container is gaining focus.
        /// </summary>
        /// <param name="oldFocus">
        /// The old element that focus is moving away from.
        /// </param>
        /// <param name="newFocus">
        /// The new element that focus is moving on.
        /// </param>
        /// <returns>
        /// True if the element that is about to get focus is inside a command container;
        /// otherwise, false.
        /// </returns>
        private static bool IsCommandContainerGainingFocus(
            IInputElement oldFocus, IInputElement newFocus)
        {
            if (oldFocus != null)
            {
                if (IsInsideCommandContainer(oldFocus))
                {
                    return false;
                }
            }

            return IsInsideCommandContainer(newFocus);
        }

        /// <summary>
        /// Determines whether a command container is losing focus.
        /// </summary>
        /// <param name="oldFocus">
        /// The old element that focus is moving away from.
        /// </param>
        /// <param name="newFocus">
        /// The new element that focus is moving on.
        /// </param>
        /// <returns>
        /// True if the element that is losing focus is inside a command container; otherwise,
        /// false.
        /// </returns>
        private static bool IsCommandContainerLosingFocus(
            IInputElement oldFocus, IInputElement newFocus)
        {
            return IsCommandContainerGainingFocus(newFocus, oldFocus);
        }

        /// <summary>
        /// Determines whether the current thread is the main UI thread for the application.
        /// </summary>
        /// <param name="fallbackObject">
        /// The object that is used if there doesn't seem to be a current application. This
        /// might happen when the window has been opened inside a Win32 application.
        /// </param>
        /// <returns>
        /// True if the current thread is the main UI thread for the application; otherwise,
        /// false.
        /// </returns>
        private static bool IsCurrentThreadMainUIThread(DispatcherObject fallbackObject)
        {
            Application app = Application.Current;
            if (app != null)
            {
                return app.Dispatcher.CheckAccess();
            }

            if (fallbackObject != null)
            {
                return fallbackObject.Dispatcher.CheckAccess();
            }

            return false;
        }

        /// <summary>
        /// Called whenever a context menu closes.
        /// </summary>
        /// <param name="sender">
        /// The context menu this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs data for the event.
        /// </param>
        private static void OnContextMenuClosed(object sender, RoutedEventArgs e)
        {
            if (!IsCurrentThreadMainUIThread(sender as DispatcherObject))
            {
                return;
            }

            if (_restoreFocusScope == null)
            {
                return;
            }

            IInputElement source = e.Source as IInputElement;
            if (source == null)
            {
                return;
            }

            if (!IsCommandContainerLosingFocus(source, Keyboard.FocusedElement))
            {
                return;
            }

            CurrentMenuModeSource = null;
            CommandRestoreFocusScope restoreFocusScope = _restoreFocusScope;
            _restoreFocusScope = null;
            restoreFocusScope.PerformRestoration();
        }

        /// <summary>
        /// Called whenever a context menu opens.
        /// </summary>
        /// <param name="sender">
        /// The context menu this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs data for the event.
        /// </param>
        private static void OnContextMenuClosing(object sender, RoutedEventArgs e)
        {
            if (!Object.ReferenceEquals(e.OriginalSource, sender))
            {
                return;
            }

            FrameworkElement element = sender as FrameworkElement;
            if (element == null)
            {
                return;
            }

            ItemsControl itemsControl = ItemsControl.ItemsControlFromItemContainer(element);
            if (itemsControl == null)
            {
                itemsControl = element as ItemsControl;
                if (itemsControl == null)
                {
                    return;
                }
            }
            else if (itemsControl is DataGridCellsPresenter)
            {
                itemsControl = itemsControl.GetVisualAncestor<ItemsControl>();
                if (itemsControl == null)
                {
                    return;
                }
            }

            foreach (object item in itemsControl.ItemContainerGenerator.Items)
            {
                var container = itemsControl.ItemContainerGenerator.ContainerFromItem(item);
                FrameworkElement itemElement = container as FrameworkElement;
                if (itemElement == null)
                {
                    continue;
                }

                if (itemElement.ContextMenu != null && itemElement.ContextMenu.IsOpen)
                {
                    return;
                }
            }

            ContextMenuProperties.SetIsContextMenuOpen(itemsControl, false);
            if (itemsControl is DataGrid)
            {
                IEnumerable<DataGridCellsPresenter> cellPresenters =
                    itemsControl.GetVisualDescendents<DataGridCellsPresenter>();

                foreach (DataGridCellsPresenter child in cellPresenters)
                {
                    ContextMenuProperties.SetIsContextMenuOpen(child, false);
                }
            }
        }

        /// <summary>
        /// Called whenever a context menu opens.
        /// </summary>
        /// <param name="sender">
        /// The context menu this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs data for the event.
        /// </param>
        private static void OnContextMenuOpening(object sender, RoutedEventArgs e)
        {
            FrameworkElement element = sender as FrameworkElement;
            if (element == null || !Object.ReferenceEquals(sender, e.Source))
            {
                return;
            }

            ItemsControl itemsControl = ItemsControl.ItemsControlFromItemContainer(element);
            if (itemsControl == null)
            {
                itemsControl = element as ItemsControl;
                if (itemsControl == null)
                {
                    return;
                }
            }
            else if (itemsControl is DataGridCellsPresenter)
            {
                itemsControl = itemsControl.GetVisualAncestor<ItemsControl>();
                if (itemsControl == null)
                {
                    return;
                }
            }

            if (!itemsControl.IsKeyboardFocusWithin)
            {
                foreach (FrameworkElement obj in
                    element.GetVisualAncestors<FrameworkElement>(false))
                {
                    ItemsControl parent = ItemsControl.ItemsControlFromItemContainer(obj);
                    if (Object.ReferenceEquals(parent, itemsControl))
                    {
                        obj.MoveFocus(new TraversalRequest(FocusNavigationDirection.First));
                        break;
                    }

                    if (Object.ReferenceEquals(obj, itemsControl))
                    {
                        break;
                    }
                }
            }

            ContextMenuProperties.SetIsContextMenuOpen(itemsControl, true);
            if (itemsControl is DataGrid)
            {
                IEnumerable<DataGridCellsPresenter> cellPresenters =
                    itemsControl.GetVisualDescendents<DataGridCellsPresenter>();

                foreach (DataGridCellsPresenter child in cellPresenters)
                {
                    ContextMenuProperties.SetIsContextMenuOpen(child, true);
                }
            }
        }

        /// <summary>
        /// Called when a registered command control gets the keyboard input provider acquired
        /// focus event so that it can push menu mode.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.KeyboardInputProviderAcquireFocusEventArgs data for the
        /// event.
        /// </param>
        private static void OnKeyboardInputProviderAcquireFocus(
            object sender, KeyboardInputProviderAcquireFocusEventArgs e)
        {
            if (!IsCurrentThreadMainUIThread(sender as DispatcherObject))
            {
                return;
            }

            UIElement element = (UIElement)sender;
            if (!IsRegisteredCommandFocusElement(element))
            {
                return;
            }

            if (element.IsKeyboardFocusWithin)
            {
                return;
            }

            if (e.RoutedEvent == Keyboard.PreviewKeyboardInputProviderAcquireFocusEvent)
            {
                CurrentMenuModeSource = PresentationSource.FromVisual(element);
            }
            else
            {
                if (!e.FocusAcquired)
                {
                    CurrentMenuModeSource = null;
                }
            }
        }

        /// <summary>
        /// Called when a registered command control loses keyboard focus so that it can pop
        /// menu mode.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.KeyboardFocusChangedEventArgs data for the event.
        /// </param>
        private static void OnLostKeyboardFocus(object s, KeyboardFocusChangedEventArgs e)
        {
            if (!IsCurrentThreadMainUIThread(s as DispatcherObject))
            {
                return;
            }

            UIElement element = s as UIElement;
            if (!IsRegisteredCommandFocusElement(element))
            {
                return;
            }

            if (!IsCommandContainerLosingFocus(e.OldFocus, e.NewFocus))
            {
                return;
            }

            CurrentMenuModeSource = null;
            _restoreFocusScope = null;
        }

        /// <summary>
        /// Called when a registered command control is about to receive keyboard focus.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.KeyboardFocusChangedEventArgs data for the event.
        /// </param>
        private static void OnPreviewGotKeyboardFocus(
            object s, KeyboardFocusChangedEventArgs e)
        {
            if (e.OldFocus == null || !IsCurrentThreadMainUIThread(s as DispatcherObject))
            {
                return;
            }

            UIElement element = s as UIElement;
            if (!IsRegisteredCommandFocusElement(element))
            {
                return;
            }

            if (_restoreFocusScope != null)
            {
                return;
            }

            if (!IsCommandContainerGainingFocus(e.OldFocus, e.NewFocus))
            {
                return;
            }

            CurrentMenuModeSource = PresentationSource.FromVisual(element);
            _restoreFocusScope = new CommandRestoreFocusScope(e.OldFocus);
        }

        /// <summary>
        /// Called when a registered command control is about to loss keyboard focus.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.KeyboardFocusChangedEventArgs data for the event.
        /// </param>
        private static void OnPreviewLostKeyboardFocus(
            object s, KeyboardFocusChangedEventArgs e)
        {
            if (!IsCurrentThreadMainUIThread(s as DispatcherObject))
            {
                return;
            }

            UIElement element = s as UIElement;
            if (!IsRegisteredCommandFocusElement(element))
            {
                return;
            }

            if (_restoreFocusScope == null)
            {
                return;
            }

            if (!IsCommandContainerLosingFocus(e.OldFocus, e.NewFocus))
            {
                return;
            }

            CommandRestoreFocusScope restoreFocusScope = _restoreFocusScope;
            _restoreFocusScope = null;
            restoreFocusScope.PerformRestoration();
            e.Handled = true;
        }

        /// <summary>
        /// Registers the command class handlers for the specified type.
        /// </summary>
        /// <param name="type">
        /// The type to attach the command handlers to.
        /// </param>
        private static void RegisterClassHandlers(Type type)
        {
            EventManager.RegisterClassHandler(
                type,
                Keyboard.PreviewKeyboardInputProviderAcquireFocusEvent,
                new KeyboardInputProviderAcquireFocusEventHandler(
                    OnKeyboardInputProviderAcquireFocus),
                true);

            EventManager.RegisterClassHandler(
                type,
                Keyboard.KeyboardInputProviderAcquireFocusEvent,
                new KeyboardInputProviderAcquireFocusEventHandler(
                    OnKeyboardInputProviderAcquireFocus),
                true);

            EventManager.RegisterClassHandler(
                type,
                Keyboard.PreviewGotKeyboardFocusEvent,
                new KeyboardFocusChangedEventHandler(OnPreviewGotKeyboardFocus),
                true);

            EventManager.RegisterClassHandler(
                type,
                Keyboard.PreviewLostKeyboardFocusEvent,
                new KeyboardFocusChangedEventHandler(OnPreviewLostKeyboardFocus),
                true);

            EventManager.RegisterClassHandler(
                type,
                Keyboard.LostKeyboardFocusEvent,
                new KeyboardFocusChangedEventHandler(OnLostKeyboardFocus),
                true);
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// A helper class used to restore the focus to a specific IInputElement instance.
        /// </summary>
        private class CommandRestoreFocusScope
        {
            #region Fields
            /// <summary>
            /// The private collection containing the focusing ancestors to the restore focus
            /// element as well as the restore focus element its self.
            /// </summary>
            private List<DependencyObject> _focusAncestors;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="CommandRestoreFocusScope"/> class
            /// using the specified element as a restore point.
            /// </summary>
            /// <param name="restoreFocus">
            /// The element that should be focus on the restore.
            /// </param>
            public CommandRestoreFocusScope(IInputElement restoreFocus)
            {
                this.RestoreFocus = restoreFocus;
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets or sets the actual element that should be focus during the restore.
            /// </summary>
            private IInputElement RestoreFocus
            {
                get
                {
                    IInputElement result = null;
                    if (this._focusAncestors != null)
                    {
                        foreach (DependencyObject ancestor in this._focusAncestors)
                        {
                            if (ancestor.IsConnectedToPresentationSource())
                            {
                                result = ancestor as IInputElement;
                                break;
                            }
                        }
                    }

                    return result;
                }

                set
                {
                    this._focusAncestors = new List<DependencyObject>();
                    DependencyObject parent = value as DependencyObject;
                    while (parent != null)
                    {
                        UIElement element = parent as UIElement;
                        if (element != null && element.Focusable)
                        {
                            this._focusAncestors.Add(parent);
                        }

                        parent = parent.GetVisualOrLogicalAncestor();
                    }
                }
            }
            #endregion Properties

            #region Methods
            /// <summary>
            /// Restores the focus to the element that was past into the constructor.
            /// </summary>
            public void PerformRestoration()
            {
                if (this.RestoreFocus != null)
                {
                    Application app = Application.Current;
                    if (app == null || app.Dispatcher.HasShutdownStarted)
                    {
                        Keyboard.Focus(this.RestoreFocus);
                        return;
                    }

                    Application.Current.Dispatcher.BeginInvoke(
                        new Action(
                            delegate
                            {
                                Keyboard.Focus(this.RestoreFocus);
                            }));
                }
                else
                {
                    Keyboard.ClearFocus();
                }
            }
            #endregion Methods
        } // RSG.Editor.Controls.CommandFocusManager.CommandRestoreFocusScope {Class}
        #endregion Classes
    } // RSG.Editor.Controls.CommandFocusManager {Class}
} // RSG.Editor.Controls {Namespace}
