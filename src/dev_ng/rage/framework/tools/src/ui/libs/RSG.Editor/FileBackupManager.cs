﻿//---------------------------------------------------------------------------------------------
// <copyright file="FileBackupManager.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;
    using System.Timers;

    /// <summary>
    /// Provides a central class used to backup and recover changes to a document.
    /// </summary>
    public class FileBackupManager : DisposableObject
    {
        #region Fields
        /// <summary>
        /// The full path to the directory that contains the backup files.
        /// </summary>
        private string _backupDirectory;

        /// <summary>
        /// The private field used for the <see cref="Interval"/> property.
        /// </summary>
        private TimeSpan _interval;

        /// <summary>
        /// The weak reference collection of registered items.
        /// </summary>
        private List<WeakReference<ISaveableDocument>> _registeredItems;

        /// <summary>
        /// The generic sync object to support multithreading.
        /// </summary>
        private object _syncObject;

        /// <summary>
        /// The timer object used to time the interval between backups.
        /// </summary>
        private Timer _timer;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="FileBackupManager"/> class.
        /// </summary>
        /// <param name="interval">
        /// The time interval between backup saves.
        /// </param>
        /// <param name="backupDirectory">
        /// The root directory the backup saves should be kept.
        /// </param>
        public FileBackupManager(TimeSpan interval, string backupDirectory)
        {
            this._syncObject = new object();
            this._registeredItems = new List<WeakReference<ISaveableDocument>>();
            this._interval = interval;
            this._backupDirectory = backupDirectory;
            this._timer = new Timer(interval.TotalMilliseconds);
            this._timer.AutoReset = true;
            this._timer.Elapsed += this.StartBackup;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the backup directory.
        /// </summary>
        public string BackupDirectory
        {
            get
            {
                return this._backupDirectory;
            }

            set
            {
                lock (this._syncObject)
                {
                    if (this._registeredItems.Count > 0)
                    {
                        throw new NotSupportedException(
                            "Cannot change backup directory while items are registered.");
                    }

                    this._backupDirectory = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the time span between the backup saves.
        /// </summary>
        public TimeSpan Interval
        {
            get { return this._interval; }
            set { this._interval = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Registers the specified item to be backed up.
        /// </summary>
        /// <param name="saveableItem">
        /// The item to register.
        /// </param>
        public void RegisterFile(ISaveableDocument saveableItem)
        {
            if (saveableItem == null)
            {
                return;
            }

            lock (this._syncObject)
            {
                this._registeredItems.Add(new WeakReference<ISaveableDocument>(saveableItem));
                saveableItem.Saved += this.OnRegisteredItemSaved;
                if (this._registeredItems.Count == 1)
                {
                    this._timer.Start();
                }
            }
        }

        /// <summary>
        /// Unregisters the specified item.
        /// </summary>
        /// <param name="saveableItem">
        /// The item to unregister.
        /// </param>
        public void UnregisterFile(ISaveableDocument saveableItem)
        {
            if (saveableItem == null)
            {
                return;
            }

            lock (this._syncObject)
            {
                foreach (WeakReference<ISaveableDocument> item in this._registeredItems)
                {
                    ISaveableDocument target = null;
                    if (item.TryGetTarget(out target))
                    {
                        if (Object.ReferenceEquals(saveableItem, target))
                        {
                            saveableItem.Saved -= this.OnRegisteredItemSaved;
                            this._registeredItems.Remove(item);

                            string fullPath = target.FullPath;
                            string filename = "~Backup." + Path.GetFileName(fullPath);
                            fullPath = Path.Combine(this._backupDirectory, filename);
                            FileInfo info = new FileInfo(fullPath);
                            if (info.Exists)
                            {
                                File.Delete(fullPath);
                                if (Directory.GetFiles(this._backupDirectory).Length == 0)
                                {
                                    try
                                    {
                                        Directory.Delete(this._backupDirectory);
                                    }
                                    catch
                                    {
                                    }
                                }
                            }

                            break;
                        }
                    }
                }

                if (this._registeredItems.Count == 0)
                {
                    this._timer.Stop();
                }
            }
        }

        /// <summary>
        /// When overridden disposes of the managed resources.
        /// </summary>
        protected override void DisposeManagedResources()
        {
            if (this._timer != null)
            {
                using (this._timer)
                {
                }
            }
        }

        /// <summary>
        /// Starts the backup logic by kicking off a background thread to go through and save
        /// all modified registered items.
        /// </summary>
        /// <param name="sender">
        /// The System.Timers.ElapsedEventArgs containing the event data.
        /// </param>
        /// <param name="e">
        /// The timer this handler is attached to.
        /// </param>
        private void StartBackup(object sender, ElapsedEventArgs e)
        {
            try
            {
                this._timer.Stop();
                List<WeakReference<ISaveableDocument>> items;
                string directory = null;
                lock (this._syncObject)
                {
                    items = new List<WeakReference<ISaveableDocument>>(this._registeredItems);
                    directory = this.BackupDirectory;
                }

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                foreach (WeakReference<ISaveableDocument> item in items)
                {
                    ISaveableDocument target = null;
                    if (!item.TryGetTarget(out target) || !target.IsModified)
                    {
                        continue;
                    }

                    string fullPath = target.FullPath;
                    string filename = "~Backup." + Path.GetFileName(fullPath);
                    fullPath = Path.Combine(directory, filename);
                    FileInfo info = new FileInfo(fullPath);
                    if (info.Exists && info.IsReadOnly)
                    {
                        info.IsReadOnly = false;
                    }

                    using (MemoryStream stream = new MemoryStream())
                    {
                        if (!target.Save(stream))
                        {
                            continue;
                        }

                        FileMode mode = FileMode.Create;
                        FileAccess access = FileAccess.Write;
                        using (FileStream file = new FileStream(fullPath, mode, access))
                        {
                            stream.WriteTo(file);
                        }
                    }
                }
            }
            catch
            {
                // Shouldn't stop the application because a auto save failed.
            }
            finally
            {
                this._timer.Start();
            }
        }

        /// <summary>
        /// Called whenever a item that has been registered is saved successfully. When this
        /// occurs we no longer need the backup file and can delete it.
        /// </summary>
        /// <param name="sender">
        /// The registered file that was saved.
        /// </param>
        /// <param name="e">
        /// Always EventArgs.Empty.
        /// </param>
        private void OnRegisteredItemSaved(object sender, EventArgs e)
        {
            ISaveableDocument item = sender as ISaveableDocument;
            if (item == null)
            {
                return;
            }

            string fullPath = item.FullPath;
            string filename = "~Backup." + Path.GetFileName(fullPath);
            fullPath = Path.Combine(this._backupDirectory, filename);
            FileInfo info = new FileInfo(fullPath);
            if (info.Exists)
            {
                File.Delete(fullPath);
                if (Directory.GetFiles(this._backupDirectory).Length == 0)
                {
                    try
                    {
                        Directory.Delete(this._backupDirectory);
                    }
                    catch
                    {
                    }
                }
            }
        }
        #endregion Methods
    }  // RSG.Editor.FileBackupManager {Class}
} // RSG.Editor {Namespace}
