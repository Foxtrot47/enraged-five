﻿//---------------------------------------------------------------------------------------------
// <copyright file="InsertNewMetadataItemAboveDefinition.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Commands
{
    /// <summary>
    /// The command definition for the dynamic add new item menu that lists the valid items
    /// that can be added to the active item. This class cannot be inherited.
    /// </summary>
    public sealed class InsertNewMetadataItemAboveDefinition : NewMetadataItemDefinitionBase
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="InsertNewMetadataItemAboveDefinition"/> class.
        /// </summary>
        public InsertNewMetadataItemAboveDefinition()
            : base(MetadataCommands.InsertNewMetadataItemAbove)
        {
        }
        #endregion Constructors
    } // RSG.Metadata.Commands.InsertNewMetadataItemAboveDefinition {Class}
} // RSG.Metadata.Commands {Namespace}
