﻿//---------------------------------------------------------------------------------------------
// <copyright file="ConfigurationProjectNode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.ViewModel.Project
{
    using RSG.Editor;
    using RSG.Project.Model;
    using RSG.Project.ViewModel;
    
    /// <summary>
    /// 
    /// </summary>
    public class ConfigurationProjectNode : ProjectFolderNode
    {
        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public ConfigurationProjectNode(IHierarchyNode parent, ProjectItem item)
            : base(parent, item)
        {
            this.Icon = CommonIcons.Gears;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether this hierarchy node can be removed from the parent
        /// items scope it belongs to.
        /// </summary>
        public override bool CanBeRemoved
        {
            get { return false; }
        }


        public override int Priority
        {
            get { return int.MinValue; }
        }
        #endregion Properties
    } // RSG.Text.ViewModel.Project.ConfigurationProjectNode {Class}
} // RSG.Text.ViewModel.Project {Namespace}
