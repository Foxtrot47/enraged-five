﻿//---------------------------------------------------------------------------------------------
// <copyright file="UndoRedoBatch.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Model
{
    using System;

    /// <summary>
    /// Provides functionality so that multiple changes can be grouped together to form a
    /// single undo event.
    /// </summary>
    public class UndoRedoBatch : IDisposable
    {
        #region Fields
        /// <summary>
        /// The private event handler that is used for the <see cref="Disposed"/> event.
        /// </summary>
        private EventHandler _disposed;

        /// <summary>
        /// The private event handler that is used for the <see cref="Disposing"/> event.
        /// </summary>
        private EventHandler _disposing;

        /// <summary>
        /// Gets the <see cref="RSG.Editor.Model.UndoEngine"/> instance that this batch was
        /// created for.
        /// </summary>
        private UndoEngine _engine;

        /// <summary>
        /// The private field used for the <see cref="IsDisposed"/> property.
        /// </summary>
        private bool _isDisposed;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="UndoRedoBatch"/> class.
        /// </summary>
        /// <param name="engine">
        /// A instance of the <see cref="RSG.Editor.Model.UndoEngine"/> that this batch of
        /// changes will be added to as a single undo event.
        /// </param>
        public UndoRedoBatch(UndoEngine engine)
        {
            this._engine = engine;
            if (this._engine != null)
            {
                this._engine.StartBatch();
            }
        }

        /// <summary>
        /// Finalises an instance of the <see cref="RSG.Editor.Model.UndoRedoBatch"/> class.
        /// </summary>
        ~UndoRedoBatch()
        {
            this.Dispose(false);
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs when this instance has been disposed by the garage collector or by a user.
        /// </summary>
        public event EventHandler Disposed
        {
            add
            {
                this._disposed = (EventHandler)Delegate.Combine(this._disposed, value);
            }

            remove
            {
                this._disposed = (EventHandler)Delegate.Remove(this._disposed, value);
            }
        }

        /// <summary>
        /// Occurs when this instance is being disposed by the garage collector or by a user.
        /// </summary>
        public event EventHandler Disposing
        {
            add
            {
                this.ThrowIfDisposed();
                this._disposing = (EventHandler)Delegate.Combine(this._disposing, value);
            }

            remove
            {
                this.ThrowIfDisposed();
                this._disposing = (EventHandler)Delegate.Remove(this._disposing, value);
            }
        }
        #endregion Events

        #region Properties
        /// <summary>
        /// Gets a value indicating whether this instance has been disposed of.
        /// </summary>
        public bool IsDisposed
        {
            get { return this._isDisposed; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or
        /// resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or
        /// resetting unmanaged resources.
        /// </summary>
        /// <param name="disposeManaged">
        /// If true the managed resources for this instance also get disposed of as well as
        /// the unmanaged resources.
        /// </param>
        private void Dispose(bool disposeManaged)
        {
            if (!this.IsDisposed)
            {
                try
                {
                    EventHandler handler = this._disposing;
                    if (handler != null)
                    {
                        handler(this, EventArgs.Empty);
                    }

                    this._disposing = null;
                    if (disposeManaged)
                    {
                        this.DisposeManagedResources();
                    }
                }
                finally
                {
                    this._isDisposed = true;
                    EventHandler handler = this._disposed;
                    if (handler != null)
                    {
                        handler(this, EventArgs.Empty);
                    }
                }
            }
        }

        /// <summary>
        /// When overridden disposes of the managed resources.
        /// </summary>
        private void DisposeManagedResources()
        {
            if (this._engine == null)
            {
                return;
            }

            this._engine.FinishBatch();
        }

        /// <summary>
        /// Throws a System.ObjectDisposedException exception if this instance has been
        /// already disposed of.
        /// </summary>
        private void ThrowIfDisposed()
        {
            if (!this.IsDisposed)
            {
                return;
            }

            throw new ObjectDisposedException(this.GetType().Name);
        }
        #endregion Methods
    } // RSG.Editor.Model.UndoRedoBatch {Class}
} // RSG.Editor.Model {Namespace}
