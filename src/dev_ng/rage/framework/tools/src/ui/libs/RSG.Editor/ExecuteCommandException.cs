﻿//---------------------------------------------------------------------------------------------
// <copyright file="ExecuteCommandException.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Runtime.Serialization;
    using RSG.Editor.Resources;

    /// <summary>
    /// Represents errors that occur during the execution of a command.
    /// </summary>
    [Serializable]
    public class ExecuteCommandException : Exception
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ExecuteCommandException"/> class using
        /// the default message.
        /// </summary>
        public ExecuteCommandException()
            : base(ErrorStringTable.GetString("ExecuteCommandExceptionMessage"))
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ExecuteCommandException"/> class with
        /// the specified message.
        /// </summary>
        /// <param name="message">
        /// The message that describes the error.
        /// </param>
        public ExecuteCommandException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ExecuteCommandException"/> class with
        /// the specified inner exception and the default message.
        /// </summary>
        /// <param name="innerException">
        /// The exception that is the cause of this current exception, or a null reference if
        /// no inner exception is specified.
        /// </param>
        public ExecuteCommandException(Exception innerException)
            : base(
            ErrorStringTable.GetString("CanExecuteCommandExceptionMessage"), innerException)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ExecuteCommandException"/> class with
        /// the specified message and inner exception.
        /// </summary>
        /// <param name="message">
        /// The message that describes the error.
        /// </param>
        /// <param name="innerException">
        /// The exception that is the cause of this current exception, or a null reference if
        /// no inner exception is specified.
        /// </param>
        public ExecuteCommandException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ExecuteCommandException"/> class with
        /// serialised data.
        /// </summary>
        /// <param name="info">
        /// The System.Runtime.Serialization.SerializationInfo that holds the serialised
        /// object data about the exception being thrown.
        /// </param>
        /// <param name="context">
        /// The System.Runtime.Serialization.StreamingContext that contains contextual
        /// information about the source or destination.
        /// </param>
        protected ExecuteCommandException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
        #endregion Constructors
    } // RSG.Editor.ExecuteCommandException {Class}
} // RSG.Editor {Namespace}
