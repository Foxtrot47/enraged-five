﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using RSG.Editor.Controls.MapViewport.Annotations;

namespace RSG.Editor.Controls.MapViewport.Commands
{
    /// <summary>
    /// The action logic that is responsible for deleting a comment from an annotation.
    /// </summary>
    public class AddAnnotationCommentAction : ButtonAction<AnnotationControl, Annotation>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="AddAnnotationCommentAction"/> class
        /// using the specified resolver.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public AddAnnotationCommentAction(ParameterResolverDelegate<AnnotationControl> resolver)
            : base(resolver)
        {
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="annotationControl">
        /// The resolved command parameter that has been requested.
        /// </param>
        /// <param name="annotation">
        /// The original command parameter.
        /// </param>
        public override void Execute(AnnotationControl annotationControl, Annotation annotation)
        {
            TextBox commentTextBox = annotationControl.AddCommentTextBox;

            annotation.AddComment(commentTextBox.Text);
            commentTextBox.Text = "";

            annotationControl.CommentsScrollViewer.ScrollToBottom();
        }
        #endregion
    }
}
