﻿//---------------------------------------------------------------------------------------------
// <copyright file="PasteLineAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Commands
{
    using System.Collections.Generic;
    using System.Linq;
    using RSG.Editor;
    using RSG.Text.Model;
    using RSG.Text.ViewModel;

    /// <summary>
    /// Implements the RockstarCommands.Paste command.
    /// </summary>
    public class PasteLineAction : CopyAndPasteLineAction
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="PasteLineAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public PasteLineAction(ParameterResolverDelegate<TextLineCommandArgs> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(TextLineCommandArgs args)
        {
            return this.ClipboardContainsLineData();
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(TextLineCommandArgs args)
        {
            int index = args.DataGrid.SelectedIndex + 1;
            List<ILine> data = this.GetDataFromClipboard(args.Conversation.Model, index);
        }
        #endregion Methods
    } // RSG.Text.Commands.PasteLineAction {Class}
} // RSG.Text.Commands {Namespace}
