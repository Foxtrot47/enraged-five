﻿//---------------------------------------------------------------------------------------------
// <copyright file="CustomPresenterBase.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Presenters
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Input;
    using System.Windows.Media;
    using RSG.Editor.Controls.Helpers;

    /// <summary>
    /// Displays the content of a specific item in the user interface.
    /// </summary>
    public abstract class CustomPresenterBase : FrameworkElement, IToolTipVisibilityController
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="Background"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty BackgroundProperty;

        /// <summary>
        /// Identifies the <see cref="BorderBrush"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty BorderBrushProperty;

        /// <summary>
        /// Identifies the <see cref="ToolTipContent"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ToolTipContentProperty;

        /// <summary>
        /// Identifies the <see cref="ToolTipText"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ToolTipTextProperty;

        /// <summary>
        /// The private field used for the <see cref="BorderPen"/> property.
        /// </summary>
        private Pen _borderPen;

        /// <summary>
        /// The private field used for the <see cref="Children"/> property.
        /// </summary>
        private UIElementCollection _children;

        /// <summary>
        /// The tooltip visibility manager for this presenter.
        /// </summary>
        private ToolTipVisibilityManager _toolTipVisibilityManager;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="CustomPresenterBase"/> class.
        /// </summary>
        static CustomPresenterBase()
        {
            CustomPresenterBase.BackgroundProperty =
                Border.BackgroundProperty.AddOwner(
                typeof(CustomPresenterBase),
                new FrameworkPropertyMetadata(
                    null, FrameworkPropertyMetadataOptions.AffectsRender));

            CustomPresenterBase.BorderBrushProperty =
                Border.BorderBrushProperty.AddOwner(
                typeof(CustomPresenterBase),
                new FrameworkPropertyMetadata(
                    null,
                    FrameworkPropertyMetadataOptions.AffectsRender,
                    new PropertyChangedCallback(OnBorderBrushChanged)));

            ToolTipContentProperty =
                DependencyProperty.Register(
                "ToolTipContent",
                typeof(object),
                typeof(CustomPresenterBase),
                new PropertyMetadata(OnToolTipContentChanged));

            ToolTipTextProperty =
                DependencyProperty.Register(
                "ToolTipText",
                typeof(string),
                typeof(CustomPresenterBase),
                new PropertyMetadata(null));

            IsEnabledProperty.OverrideMetadata(
                typeof(CustomPresenterBase),
                new FrameworkPropertyMetadata(
                    true, FrameworkPropertyMetadataOptions.AffectsRender));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="CustomPresenterBase"/> class.
        /// </summary>
        protected CustomPresenterBase()
        {
            this._toolTipVisibilityManager = new ToolTipVisibilityManager(this, this);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the brush to use for the background of this presenter.
        /// </summary>
        public Brush Background
        {
            get { return (Brush)this.GetValue(BackgroundProperty); }
            set { this.SetValue(BackgroundProperty, value); }
        }

        /// <summary>
        /// Gets or sets the brush to use for the border of this presenter.
        /// </summary>
        public Brush BorderBrush
        {
            get { return (Brush)this.GetValue(BorderBrushProperty); }
            set { this.SetValue(BorderBrushProperty, value); }
        }

        /// <summary>
        /// Gets the collection containing the child UI elements for this presenter.
        /// </summary>
        public UIElementCollection Children
        {
            get
            {
                if (this._children == null)
                {
                    this._children = new UIElementCollection(this, null);
                }

                return this._children;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the keyboard focus is within this tree view item.
        /// </summary>
        public bool IsSelectionActive
        {
            get { return Selector.GetIsSelectionActive(this); }
        }

        /// <summary>
        /// Gets or sets the content object that is used as the this presenters tooltip.
        /// </summary>
        public object ToolTipContent
        {
            get { return this.GetValue(ToolTipContentProperty); }
            set { this.SetValue(ToolTipContentProperty, value); }
        }

        /// <summary>
        /// Gets or sets a string that is displayed inside the presenters tooltip.
        /// </summary>
        public virtual string ToolTipText
        {
            get
            {
                string text = (string)this.GetValue(ToolTipTextProperty);
                if (string.IsNullOrEmpty(text))
                {
                    return this.ResolveEmptyToolTipText();
                }

                return text;
            }

            set
            {
                this.SetValue(ToolTipTextProperty, value);
            }
        }

        /// <summary>
        /// Gets the height of the content for this presenter by taking the height of the part
        /// that has the maximum size.
        /// </summary>
        internal double ContentHeight
        {
            get
            {
                return this.PartsAffectingSize.Max((PresenterPart part) => part.Height);
            }
        }

        /// <summary>
        /// Gets a value indicating whether this presenter has access to content that is used
        /// for its own tooltip.
        /// </summary>
        internal bool HasContentToolTip
        {
            get { return this.ToolTipContent != null; }
        }

        /// <summary>
        /// Gets a iterator around all the parts that have been initialised.
        /// </summary>
        protected abstract IEnumerable<PresenterPart> InitialisedParts { get; }

        /// <summary>
        /// Gets a iterator around all the parts that affect the render size of this presenter.
        /// </summary>
        protected abstract IEnumerable<PresenterPart> PartsAffectingSize { get; }

        /// <summary>
        /// Gets the number of visual child elements within this presenter.
        /// </summary>
        protected override int VisualChildrenCount
        {
            get { return this.Children.Count; }
        }

        /// <summary>
        /// Gets the pen to use when drawing the border.
        /// </summary>
        private Pen BorderPen
        {
            get
            {
                if (this._borderPen == null)
                {
                    Brush brush = this.BorderBrush;
                    if (brush == null)
                    {
                        return null;
                    }

                    this._borderPen = new Pen(brush, 1.0);
                    if (brush.IsFrozen)
                    {
                        this._borderPen.Freeze();
                    }
                }

                return this._borderPen;
            }
        }

        /// <summary>
        /// Gets a iterator around all the parts that are currently connected to this
        /// presenter.
        /// </summary>
        private IEnumerable<PresenterPart> ConnectedParts
        {
            get
            {
                return from part in this.InitialisedParts
                       where part.IsConnected
                       select part;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Determines whether the tooltip for this object can be shown based on the relative
        /// position the tooltip wants to appear.
        /// </summary>
        /// <param name="position">
        /// The relative position to where the tooltip wants to be shown.
        /// </param>
        /// <returns>
        /// True if the tooltip can be shown; otherwise, false.
        /// </returns>
        public abstract bool CanShowToolTip(Point position);

        /// <summary>
        /// Retrieves the point to click during a automation operation.
        /// </summary>
        /// <returns>
        /// The clickable point during a automation operation.
        /// </returns>
        public abstract Point GetAutomationClickablePoint();

        /// <summary>
        /// Retrieves the content that should be shown inside the tooltip presenter.
        /// </summary>
        /// <returns>
        /// The content that should be shown inside the tooltip presenter.
        /// </returns>
        public abstract object GetToolTipContent();

        /// <summary>
        /// Resolves the tooltip text when the ToolTipTextProperty is empty or null.
        /// </summary>
        /// <returns>
        /// The text to use as a tooltip when the ToolTipTextProperty is empty or null.
        /// </returns>
        public abstract string ResolveEmptyToolTipText();

        /// <summary>
        /// Adds a new child UI element to this presenter and inserts it into the child
        /// collection based on the z-index.
        /// </summary>
        /// <param name="child">
        /// The UI element to add to this presenter.
        /// </param>
        internal void InsertChildSorted(UIElement child)
        {
            UIElementCollection children = this.Children;
            int zindex = Panel.GetZIndex(child);
            for (int i = 0; i < children.Count; i++)
            {
                UIElement element = children[i];
                if (element == null)
                {
                    continue;
                }

                if (Panel.GetZIndex(element) > zindex)
                {
                    children.Insert(i, child);
                    return;
                }
            }

            children.Add(child);
        }

        /// <summary>
        /// Removes the specified UI element from this presenters children.
        /// </summary>
        /// <param name="child">
        /// The UI element to remove.
        /// </param>
        internal void RemoveChild(UIElement child)
        {
            this.Children.Remove(child);
        }

        /// <summary>
        /// Arranges the content.
        /// </summary>
        /// <param name="finalSize">
        /// The System.Windows.Size this element uses to arrange its child content.
        /// </param>
        /// <returns>
        /// The System.Windows.Size that represents the arranged size of this element and
        /// its child.
        /// </returns>
        protected override Size ArrangeOverride(Size finalSize)
        {
            foreach (PresenterPart part in this.InitialisedParts)
            {
                part.InvalidateArrange();
            }

            foreach (PresenterPart part in this.ConnectedParts)
            {
                UIElement element = part.Element;
                if (element == null)
                {
                    continue;
                }

                element.Arrange(part.Bounds);
            }

            return finalSize;
        }

        /// <summary>
        /// Gets a child at the specified index from a collection of child elements.
        /// </summary>
        /// <param name="index">
        /// The zero-based index of the requested child element in the collection.
        /// </param>
        /// <returns>
        /// The requested child element.
        /// </returns>
        protected override Visual GetVisualChild(int index)
        {
            return this.Children[index];
        }

        /// <summary>
        /// Measures the child element of this element to prepare for arranging it during the
        /// ArrangeOverride pass.
        /// </summary>
        /// <param name="availableSize">
        /// An upper limit System.Windows.Size that should not be exceeded.
        /// </param>
        /// <returns>
        /// The target System.Windows.Size of the element.
        /// </returns>
        protected override Size MeasureOverride(Size availableSize)
        {
            foreach (PresenterPart part in this.InitialisedParts)
            {
                part.InvalidateMeasure();
                part.InvalidateArrange();
                if (part.IsConnected)
                {
                    UIElement element = part.Element;
                    if (element != null)
                    {
                        element.Measure(availableSize);
                    }
                }
            }

            double width = 0.0;
            double height = 0.0;
            foreach (PresenterPart part in this.PartsAffectingSize)
            {
                width = Math.Max(width, part.Left + part.Width);
                height = Math.Max(height, part.Top + part.Height);
            }

            return new Size(width + 1.0, height + 1.0);
        }

        /// <summary>
        /// Called whenever the mouse enters this presenter.
        /// </summary>
        /// <param name="e">
        /// The event data used to retrieve the mouse position and containing the mouse state.
        /// </param>
        protected override void OnMouseEnter(MouseEventArgs e)
        {
            base.OnMouseEnter(e);
            this.UpdateMouseOver(e, false);
            this.InvalidateVisual();
        }

        /// <summary>
        /// Called whenever the mouse leaves this presenter.
        /// </summary>
        /// <param name="e">
        /// The event data used to retrieve the mouse position and containing the mouse state.
        /// </param>
        protected override void OnMouseLeave(MouseEventArgs e)
        {
            base.OnMouseLeave(e);
            this.UpdateMouseOver(e, true);
            this.InvalidateVisual();
        }

        /// <summary>
        /// Called whenever the mouse moves while over this presenter.
        /// </summary>
        /// <param name="e">
        /// The event data used to retrieve the mouse position and containing the mouse state.
        /// </param>
        protected override void OnMouseMove(MouseEventArgs e)
        {
            this.UpdateMouseOver(e, false);
            base.OnMouseMove(e);
        }

        /// <summary>
        /// Renders this element using the specified drawing context.
        /// </summary>
        /// <param name="drawingContext">
        /// The drawing instructions for a specific element. This context is provided to the
        /// layout system.
        /// </param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            Pen pen = this.BorderPen;
            double borderThickness = (pen == null) ? 0.0 : pen.Thickness;
            double halfThickness = borderThickness * 0.5;
            Rect bounds = new Rect(new Point(halfThickness, halfThickness), this.RenderSize);
            double width = Math.Max(0.0, bounds.Width - borderThickness);
            double height = Math.Max(0.0, bounds.Height - borderThickness);

            Rect borderArea = new Rect(bounds.Left, bounds.Top, width, height);
            drawingContext.DrawRectangle(this.Background, pen, borderArea);

            foreach (PresenterPart current in this.InitialisedParts)
            {
                current.Render(drawingContext);
            }
        }

        /// <summary>
        /// Called when the <see cref="BorderBrush"/> dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The dependency object whose <see cref="BorderBrush"/> dependency property
        /// changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data used for the event.
        /// </param>
        private static void OnBorderBrushChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            CustomPresenterBase presenter = s as CustomPresenterBase;
            if (presenter == null)
            {
                Debug.Assert(presenter != null, "Handler attached to unexpected object");
                return;
            }

            presenter._borderPen = null;
        }

        /// <summary>
        /// Called when the <see cref="ToolTipContent"/> dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The dependency object whose <see cref="ToolTipContent"/> dependency property
        /// changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data used for the event.
        /// </param>
        private static void OnToolTipContentChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            CustomPresenterBase presenter = s as CustomPresenterBase;
            if (presenter == null)
            {
                Debug.Assert(presenter != null, "Handler attached to wrong object type");
            }

            presenter.UpdateToolTipPlacement();
        }

        /// <summary>
        /// Updates the IsMouseOver property on each initialised presenter part.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.MouseEventArgs data that can be used to retrieve the mouse
        /// position related to this presenter.
        /// </param>
        /// <param name="mouseLeft">
        /// A value indicating whether the mouse has actually left this control.
        /// </param>
        private void UpdateMouseOver(MouseEventArgs e, bool mouseLeft)
        {
            Point position = e.GetPosition(this);
            foreach (PresenterPart part in this.InitialisedParts)
            {
                if (part.IsConnected)
                {
                    if (mouseLeft)
                    {
                        part.IsMouseOver = false;
                    }
                    else
                    {
                        part.IsMouseOver = part.Bounds.Contains(position);
                    }
                }
            }
        }

        /// <summary>
        /// Updates the tooltip placement whenever the content for the tooltip changes.
        /// </summary>
        private void UpdateToolTipPlacement()
        {
            this._toolTipVisibilityManager.SetToolTipPlacement(PlacementMode.MousePoint);
            this._toolTipVisibilityManager.SetToolTipOffsets(0.0, 3.0);
            foreach (PresenterPart current in this.InitialisedParts)
            {
                current.UpdateToolTipPlacement();
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.Presenters.CustomPresenterBase {Class}
} // RSG.Editor.Controls.Presenters {Namespace}
