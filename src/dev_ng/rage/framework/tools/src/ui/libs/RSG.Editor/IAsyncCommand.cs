﻿//---------------------------------------------------------------------------------------------
// <copyright file="IAsyncCommand.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Threading.Tasks;

    /// <summary>
    /// When implemented represents a asynchronous version of a command.
    /// </summary>
    public interface IAsyncCommand
    {
        #region Methods
        /// <summary>
        /// Gets called when the manager is deciding if this command can be executed. This
        /// returns true be default.
        /// </summary>
        /// <param name="parameter">
        /// The parameter that has been sent with the command.
        /// </param>
        /// <returns>
        /// True if the command can be executed; otherwise, false.
        /// </returns>
        bool CanExecute(Object parameter);

        /// <summary>
        /// Asynchronously executes the command.
        /// </summary>
        /// <param name="parameter">
        /// The parameter that has been sent with the command.
        /// </param>
        /// <returns>
        /// A task that is executing the command logic.
        /// </returns>
        Task ExecuteAsync(Object parameter);
        #endregion Methods
    } // RSG.Editor.IAsyncCommand {Interface}
} // RSG.Editor {Namespace}
