﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProjectOpenedEventArgs.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel
{
    using System;

    /// <summary>
    /// Provides data for the event representing a project node being opened by the user.
    /// </summary>
    public class ProjectOpenedEventArgs : EventArgs
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="FullPath"/> property.
        /// </summary>
        private string _fullPath;

        /// <summary>
        /// The private field used for the <see cref="Project"/> property.
        /// </summary>
        private ProjectNode _project;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ProjectOpenedEventArgs"/> class.
        /// </summary>
        /// <param name="project">
        /// The project whose load state has changed.
        /// </param>
        /// <param name="fullPath">
        /// The full path to the project that was opened.
        /// </param>
        public ProjectOpenedEventArgs(ProjectNode project, string fullPath)
        {
            this._fullPath = fullPath;
            this._project = project;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the full path to the location the project was opened from.
        /// </summary>
        public string FullPath
        {
            get { return this._fullPath; }
        }

        /// <summary>
        /// Gets the value before the changed occurred.
        /// </summary>
        public ProjectNode Project
        {
            get { return this._project; }
        }
        #endregion Properties
    } // RSG.Project.ViewModel.ProjectOpenedEventArgs {Class}
} // RSG.Project.ViewModel {Namespace}
