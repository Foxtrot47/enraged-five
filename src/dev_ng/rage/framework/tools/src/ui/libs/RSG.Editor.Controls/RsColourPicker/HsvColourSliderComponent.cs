﻿//---------------------------------------------------------------------------------------------
// <copyright file="HsvColourSliderComponent.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    /// <summary>
    /// Defines the different colour components that can be manipulated using a HSV colour
    /// slider control.
    /// </summary>
    public enum HsvColourSliderComponent
    {
        /// <summary>
        /// The control wont manipulate anything and show as a blank control.
        /// </summary>
        None,

        /// <summary>
        /// The control will manipulate the hue of the base colour.
        /// </summary>
        Hue,

        /// <summary>
        /// The control will manipulate the saturation of the base colour.
        /// </summary>
        Saturation,

        /// <summary>
        /// The control will manipulate the value of the base colour in its HSV format.
        /// </summary>
        Value,
    } // RSG.Editor.Controls.HsvColourSliderComponent {Enum}
} // RSG.Editor.Controls {Namespace}
