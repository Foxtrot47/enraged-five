﻿//---------------------------------------------------------------------------------------------
// <copyright file="MetadataInheritanceHierarchyViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Inheritance
{
    using System.Collections.Generic;
    using System.Linq;
    using RSG.Editor.View;
    using RSG.Metadata.Model.Inheritance;
    using RSG.Metadata.Model.Tunables;
    using RSG.Metadata.ViewModel.Tunables;

    /// <summary>
    /// 
    /// </summary>
    public class MetadataInheritanceHierarchyViewModel
        : ViewModelBase<MetadataInheritanceHierarchy>
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Layers"/> property.
        /// </summary>
        private TunableStructureCollection _layers;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="MetadataInheritanceHierarchyViewModel"/> class.
        /// </summary>
        /// <param name="model">
        /// The model this viewmodel is wrapping.
        /// </param>
        public MetadataInheritanceHierarchyViewModel(MetadataInheritanceHierarchy model)
            : base(model)
        {
            this._layers = new TunableStructureCollection(model.Layers);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a list containing the individual layers in the inheritance hierarchy.
        /// </summary>
        public IReadOnlyViewModelCollection<TunableStructureViewModel> Layers
        {
            get { return this._layers; }
        }

        /// <summary>
        /// Gets the leaf tunable structure. The bottom of the hierarchy.
        /// </summary>
        public TunableStructureViewModel Leaf
        {
            get
            {
                if (this._layers.Count > 0)
                {
                    return this._layers[this._layers.Count - 1];
                }

                return null;
            }
        }
        #endregion Properties

        #region Classes
        /// <summary>
        /// A collection class used by hierarchical tunable view models to construct its child
        /// collection.
        /// </summary>
        private class TunableStructureCollection
            : CollectionConverter<TunableStructure, TunableStructureViewModel>,
            IReadOnlyViewModelCollection<TunableStructureViewModel>
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="TunableStructureCollection"/>
            /// class.
            /// </summary>
            /// <param name="source">
            /// The source model items for this collection.
            /// </param>
            public TunableStructureCollection(IEnumerable<TunableStructure> source)
                : base(Enumerable.Empty<object>(), null)
            {
                this.ResetSourceItems(source);
            }
            #endregion Constructors

            #region Methods
            /// <summary>
            /// Creates a tunable view model from the specified tunable model object.
            /// </summary>
            /// <param name="ts">
            /// The tunable structure used to construct the view model.
            /// </param>
            /// <returns>
            /// A new tunable structure view model constructed from the specified tunable model
            /// object.
            /// </returns>
            protected override TunableStructureViewModel ConvertItem(TunableStructure ts)
            {
                return new TunableStructureViewModel(ts);
            }
            #endregion Methods
        } // RSG.Metadata.ViewModel.Tunables.TunableCollection {Class}
        #endregion Classes
    } // RSG.Metadata.ViewModel.Inheritance.MetadataInheritanceHierarchyViewModel {Class}
} // RSG.Metadata.ViewModel.Inheritance {Namespace}
