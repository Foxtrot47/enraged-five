﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsMessageBoxWindow.xaml.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Input;
    using System.Windows.Interop;
    using RSG.Editor.Controls.Helpers;
    using RSG.Editor.SharedCommands;

    /// <summary>
    /// The code behind logic for the <see cref="RsMessageBoxWindow"/> class.
    /// </summary>
    internal partial class RsMessageBoxWindow : RsWindow
    {
        #region Fields
        /// <summary>
        /// The private iterator of custom buttons that are displayed to the user.
        /// </summary>
        private RsMessageBox _messageBox;

        /// <summary>
        /// The reference to the button that was pressed to close this message box.
        /// </summary>
        private CustomMessageBoxButton _pressedButton;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="RsMessageBoxWindow"/> class.
        /// </summary>
        /// <param name="owner">
        /// A System.Windows.Window that represents the owner window of the message box.
        /// </param>
        /// <param name="messageBox">
        /// The message box that contains all of the properties used to display this window.
        /// </param>
        internal RsMessageBoxWindow(Window owner, RsMessageBox messageBox)
        {
            if (messageBox == null)
            {
                throw new SmartArgumentNullException(() => messageBox);
            }

            if (messageBox.Buttons == null || messageBox.Buttons.Count == 0)
            {
                throw new NotSupportedException("Cannot have a message box without buttons");
            }

            this._messageBox = messageBox;
            if (owner != null)
            {
                IntPtr windowHandle = new WindowInteropHelper(owner).Handle;
                this.ChangeOwner(windowHandle);
            }
            else
            {
                this.ChangeOwner(IntPtr.Zero);
            }

            if (messageBox.Caption == null)
            {
                this.Title = "error";
            }
            else
            {
                this.Title = messageBox.Caption;
            }

            this.ShowHelpButton = !string.IsNullOrWhiteSpace(messageBox.HelpUrl);
            this.CanMaximiseWindow = false;
            this.CanMinimiseWindow = false;
            this.SupportCoerceTitle = true;
            this.ShowInTaskbar = false;

            if (this._messageBox.Buttons.Count > 1)
            {
                bool enableClose = false;
                foreach (CustomMessageBoxButton button in this._messageBox.Buttons)
                {
                    if (button.IsCancel)
                    {
                        enableClose = true;
                        break;
                    }
                }

                this.CanCloseWindowWithButton = enableClose;
            }

            this.InitializeComponent();
            if (this.Owner == null)
            {
                this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            }
            else
            {
                this.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            }

            this.CommandBindings.Add(new CommandBinding(RockstarCommands.Copy, this.OnCopy));
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Overridden to always throws a System.NotSupportedException exception.
        /// </summary>
        public new void Show()
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Overridden to always throws a System.NotSupportedException exception.
        /// </summary>
        /// <returns>
        /// Nothing as exception is thrown.
        /// </returns>
        public new bool? ShowDialog()
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Opens a window and returns only when the newly opened window is closed.
        /// </summary>
        /// <returns>
        /// The button that was pressed to close the window if one was pressed; otherwise,
        /// null.
        /// </returns>
        public CustomMessageBoxButton ShowMessageBox()
        {
            if (this._messageBox.Buttons.Count == 0)
            {
                throw new NotSupportedException("Cannot show a message box without buttons");
            }

            this.ButtonList.ItemsSource = this._messageBox.Buttons;
            this.Title = this._messageBox.Caption;
            this.TextMessage.Text = this._messageBox.Text;
            this.MessageIcon.Source = this._messageBox.ImageSource;

            if (this.MessageIcon.Source == null)
            {
                this.MessageIcon.Visibility = Visibility.Collapsed;
            }

            bool? buttonPressed = base.ShowDialog();
            CustomMessageBoxButton result = null;
            if (buttonPressed == true && this._pressedButton != null)
            {
                result = this._pressedButton;
            }
            else
            {
                if (this.ButtonList.Items.Count == 1)
                {
                    result = this.ButtonList.Items[0] as CustomMessageBoxButton;
                }
                else
                {
                    foreach (CustomMessageBoxButton button in this._messageBox.Buttons)
                    {
                        if (button.IsCancel)
                        {
                            result = button;
                            break;
                        }
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Called whenever one of the buttons is loaded so that the focus can be moved to
        /// the default item.
        /// </summary>
        /// <param name="sender">
        /// The button that fired this event.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs data used for this event.
        /// </param>
        private void OnButtonLoaded(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            if (button == null)
            {
                System.Diagnostics.Debug.Assert(false, "Handler attached to wrong object.");
                return;
            }

            CustomMessageBoxButton dataContext = button.DataContext as CustomMessageBoxButton;
            if (dataContext == null)
            {
                return;
            }

            if (!dataContext.IsDefault)
            {
                return;
            }

            FocusManager.SetFocusedElement(this, button);
        }

        /// <summary>
        /// Called whenever one of the buttons displayed to the user is pressed.
        /// </summary>
        /// <param name="sender">
        /// The button that fired this event.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs data used for this event.
        /// </param>
        private void OnButtonPressed(object sender, RoutedEventArgs e)
        {
            FrameworkElement element = sender as FrameworkElement;
            if (element != null)
            {
                this._pressedButton = element.DataContext as CustomMessageBoxButton;
            }

            if (this._pressedButton == null)
            {
                throw new InvalidOperationException("Button had incorrect data context");
            }

            this.DialogResult = true;
        }

        /// <summary>
        /// Called whenever the copy command is fired and caught at this location.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.ExecutedRoutedEventArgs containing the event data.
        /// </param>
        private void OnCopy(object s, ExecutedRoutedEventArgs e)
        {
            Clipboard.SetText(this.TextMessage.Text);
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsMessageBoxWindow {Class}
} // RSG.Editor.Controls {Namespace}
