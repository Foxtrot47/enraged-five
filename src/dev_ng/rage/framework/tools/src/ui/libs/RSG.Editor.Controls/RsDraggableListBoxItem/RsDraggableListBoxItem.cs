﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsDraggableListBoxItem.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using RSG.Editor.Controls.Helpers;

    /// <summary>
    /// Represents a selectable control inside a <see cref="RsDraggableListBox"/> control.
    /// </summary>
    public class RsDraggableListBoxItem : ListBoxItem
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="DragCompletedEvent"/> routed event.
        /// </summary>
        public static readonly RoutedEvent DragCompletedEvent;

        /// <summary>
        /// Identifies the <see cref="DragDeltaEvent"/> routed event.
        /// </summary>
        public static readonly RoutedEvent DragDeltaEvent;

        /// <summary>
        /// Identifies the <see cref="DragStartedEvent"/> routed event.
        /// </summary>
        public static readonly RoutedEvent DragStartedEvent;

        /// <summary>
        /// Identifies the <see cref="IsDragEnabled"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsDragEnabledProperty;

        /// <summary>
        /// Identifies the <see cref="IsDragging"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsDraggingProperty;

        /// <summary>
        /// Identifies the <see cref="IsDragging"/> dependency property key.
        /// </summary>
        private static readonly DependencyPropertyKey _isDraggingPropertyKey;

        /// <summary>
        /// The point in screen space of where the drag operation ended.
        /// </summary>
        private Point _lastScreenPoint;

        /// <summary>
        /// A value indicating whether the mouse moved during the drag operation.
        /// </summary>
        private bool _movedDuringDrag;

        /// <summary>
        /// The point in screen space of where the drag operation started.
        /// </summary>
        private Point _originalScreenPoint;

        /// <summary>
        /// A value indicating whether the drag started event has been raised.
        /// </summary>
        private bool _raisedDragStarted;

        /// <summary>
        /// A value indicating whether a selection needs to be made when the mouse button is
        /// released. This happens when the drag operation is started on a mouse click.
        /// </summary>
        private bool _shouldChangeSelectionOnMouseUp;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsDraggableListBoxItem" /> class.
        /// </summary>
        static RsDraggableListBoxItem()
        {
            DragStartedEvent =
                EventManager.RegisterRoutedEvent(
                "DragStarted",
                RoutingStrategy.Bubble,
                typeof(EventHandler<RsDragEventArgs>),
                typeof(RsDraggableListBoxItem));

            DragDeltaEvent =
                EventManager.RegisterRoutedEvent(
                "DragDelta",
                RoutingStrategy.Bubble,
                typeof(EventHandler<RsDragEventArgs>),
                typeof(RsDraggableListBoxItem));

            DragCompletedEvent =
                EventManager.RegisterRoutedEvent(
                "DragCompleted",
                RoutingStrategy.Bubble,
                typeof(EventHandler<RsDragEventArgs>),
                typeof(RsDraggableListBoxItem));

            IsDragEnabledProperty =
                DependencyProperty.Register(
                "IsDragEnabled",
                typeof(bool),
                typeof(RsDraggableListBoxItem),
                new FrameworkPropertyMetadata(true));

            _isDraggingPropertyKey =
                DependencyProperty.RegisterReadOnly(
                "IsDragging",
                typeof(bool),
                typeof(RsDraggableListBoxItem),
                new FrameworkPropertyMetadata(false));

            IsDraggingProperty = _isDraggingPropertyKey.DependencyProperty;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsDraggableListBoxItem"/> class.
        /// </summary>
        public RsDraggableListBoxItem()
        {
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs when a drag is completed.
        /// </summary>
        public event EventHandler<RsDragEventArgs> DragCompleted
        {
            add { this.AddHandler(RsDraggableListBoxItem.DragCompletedEvent, value); }
            remove { this.RemoveHandler(RsDraggableListBoxItem.DragCompletedEvent, value); }
        }

        /// <summary>
        /// Occurs when a drag is in progress.
        /// </summary>
        public event EventHandler<RsDragEventArgs> DragDelta
        {
            add { this.AddHandler(RsDraggableListBoxItem.DragDeltaEvent, value); }
            remove { this.RemoveHandler(RsDraggableListBoxItem.DragDeltaEvent, value); }
        }

        /// <summary>
        /// Occurs when a drag is started.
        /// </summary>
        public event EventHandler<RsDragEventArgs> DragStarted
        {
            add { this.AddHandler(RsDraggableListBoxItem.DragStartedEvent, value); }
            remove { this.RemoveHandler(RsDraggableListBoxItem.DragStartedEvent, value); }
        }
        #endregion Events

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether the dragging of this control is enabled.
        /// </summary>
        public bool IsDragEnabled
        {
            get { return (bool)this.GetValue(IsDragEnabledProperty); }
            set { this.SetValue(IsDragEnabledProperty, value); }
        }

        /// <summary>
        /// Gets a value indicating whether this control is currently being dragged.
        /// </summary>
        public bool IsDragging
        {
            get { return (bool)this.GetValue(IsDraggingProperty); }
            private set { this.SetValue(_isDraggingPropertyKey, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Cancels the current dragging operation.
        /// </summary>
        public void CancelDrag()
        {
            if (!this.IsDragging)
            {
                return;
            }

            this.ReleaseCapture();
            this.OnDragCompleted(this._lastScreenPoint, true);
        }

        /// <summary>
        /// Ensures that the specified item is the item that is initially selected when the
        /// selection mode is extended.
        /// </summary>
        /// <param name="item">
        /// The item that should be made to be the anchored item.
        /// </param>
        protected virtual void MakeAnchorItem(object item)
        {
            ItemsControl control = ItemsControl.ItemsControlFromItemContainer(this);
            RsDraggableListBox listBox = control as RsDraggableListBox;
            if (listBox == null)
            {
                Debug.Assert(listBox != null, "Unable to set anchor item.");
                return;
            }

            listBox.SetAnchorItem(item);
        }

        /// <summary>
        /// Raises the <see cref="DragCompletedEvent"/> routed event.
        /// </summary>
        /// <param name="point">
        /// The screen position of the mouse when the drag operation finished.
        /// </param>
        /// <param name="cancelled">
        /// A value indicating whether the drag operation was cancelled or not.
        /// </param>
        protected virtual void OnDragCompleted(Point point, bool cancelled)
        {
            DragState state = DragState.NotFinished;
            if (cancelled == true)
            {
                state = DragState.Cancelled;
            }
            else
            {
                if (this._movedDuringDrag)
                {
                    state = DragState.FinishedWithMovement;
                }
                else
                {
                    state = DragState.FinishedWithoutMovement;
                }
            }

            Vector totalDelta = point - this._originalScreenPoint;
            this.RaiseEvent(
                new RsDragEventArgs(
                    DragCompletedEvent, point, state, totalDelta.X, totalDelta.Y));
        }

        /// <summary>
        /// Raises the <see cref="DragDeltaEvent"/> routed event.
        /// </summary>
        /// <param name="previousPoint">
        /// The previous screen position of the mouse.
        /// </param>
        /// <param name="newPoint">
        /// The current screen position of the mouse.
        /// </param>
        protected virtual void OnDragDelta(Point previousPoint, Point newPoint)
        {
            DragState state = DragState.NotFinished;
            Vector delta = newPoint - previousPoint;
            this.RaiseEvent(
                new RsDragEventArgs(DragDeltaEvent, newPoint, state, delta.X, delta.Y));
        }

        /// <summary>
        /// Raises the <see cref="DragStartedEvent"/> routed event.
        /// </summary>
        /// <param name="point">
        /// The original point where the drag operation started from.
        /// </param>
        protected virtual void OnDragStarted(Point point)
        {
            DragState state = DragState.NotFinished;
            this.RaiseEvent(new RsDragEventArgs(DragStartedEvent, point, state));
        }

        /// <summary>
        /// Called if the mouse starts or stops capturing this item and makes sure to cancel
        /// the drag operation if the mouse capture is set to a different control.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data used for this event.
        /// </param>
        protected override void OnIsMouseCapturedChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnIsMouseCapturedChanged(e);
            if (!this.IsMouseCaptured)
            {
                this.CancelDrag();
            }
        }

        /// <summary>
        /// Responds to the System.Windows.UIElement.MouseLeftButtonDown event.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.MouseButtonEventArgs data used for the event.
        /// </param>
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            if (Keyboard.Modifiers == ModifierKeys.None ||
                Keyboard.Modifiers == ModifierKeys.Control)
            {
                if (this.IsSelected)
                {
                    this._shouldChangeSelectionOnMouseUp = true;
                    this.Focus();
                    e.Handled = true;
                }
                else
                {
                    this._shouldChangeSelectionOnMouseUp = false;
                }
            }
            else
            {
                this._shouldChangeSelectionOnMouseUp = false;
            }

            base.OnMouseLeftButtonDown(e);
            if (Mouse.LeftButton != MouseButtonState.Pressed || !this.IsDragEnabled)
            {
                return;
            }

            if (!(this as DependencyObject).IsConnectedToPresentationSource())
            {
                return;
            }

            Point screenPoint = this.PointToScreen(e.GetPosition(this));
            this.BeginDragging(screenPoint);
        }

        /// <summary>
        /// Responds to the System.Windows.UIElement.MouseLeftButtonUp event.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.MouseButtonEventArgs data used for the event.
        /// </param>
        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            bool connected = PresentationSource.FromVisual(this) != null;
            if (this.IsMouseCaptured && this.IsDragging && connected)
            {
                this._lastScreenPoint = this.PointToScreen(e.GetPosition(this));
                this.CompleteDrag();
            }

            if (this._shouldChangeSelectionOnMouseUp)
            {
                switch (Keyboard.Modifiers)
                {
                    case ModifierKeys.None:
                        if (this.IsSelected)
                        {
                            this.MakeSingleSelection();
                        }

                        break;
                    case ModifierKeys.Control:
                        if (this._shouldChangeSelectionOnMouseUp)
                        {
                            this.MakeToggleSelection();
                        }

                        break;
                }
            }

            base.OnMouseLeftButtonUp(e);
        }

        /// <summary>
        /// Responds to the System.Windows.UIElement.MouseMove event.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.MouseButtonEventArgs data used for the event.
        /// </param>
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            bool connected = PresentationSource.FromVisual(this) != null;
            if (!this.IsMouseCaptured || !this.IsDragging || !connected)
            {
                return;
            }

            this._movedDuringDrag = true;
            Point point = this.PointToScreen(e.GetPosition(this));
            if (this._raisedDragStarted)
            {
                this.OnDragDelta(this._lastScreenPoint, point);
            }

            if (this.IsOutsideSensitivity(point) && !this._raisedDragStarted)
            {
                this._shouldChangeSelectionOnMouseUp = false;
                this._raisedDragStarted = true;
                this.OnDragStarted(this._originalScreenPoint);
                this.OnDragDelta(this._originalScreenPoint, point);
            }

            this._lastScreenPoint = point;
        }

        /// <summary>
        /// Handles the event fired when the right mouse button is pressed while over this
        /// control.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.MouseButtonEventArgs data used for this event.
        /// </param>
        protected override void OnPreviewMouseRightButtonDown(MouseButtonEventArgs e)
        {
            if (!this.IsKeyboardFocusWithin)
            {
                this.Focus();
            }

            base.OnPreviewMouseRightButtonDown(e);
        }

        /// <summary>
        /// Beings the dragging operation.
        /// </summary>
        /// <param name="screenPoint">
        /// The screen pont at which the drag operation has been started.
        /// </param>
        private void BeginDragging(Point screenPoint)
        {
            if (!this.CaptureMouse())
            {
                return;
            }

            this.IsDragging = true;
            this._originalScreenPoint = screenPoint;
            this._lastScreenPoint = screenPoint;
            this._movedDuringDrag = false;
            this._raisedDragStarted = false;
        }

        /// <summary>
        /// Completes the current drag operation.
        /// </summary>
        private void CompleteDrag()
        {
            if (!this.IsDragging)
            {
                return;
            }

            this.ReleaseCapture();
            this.OnDragCompleted(this._lastScreenPoint, false);
        }

        /// <summary>
        /// Determines whether the original starting point and the current point are a
        /// significant distance between each other to say the dragged item has been moved.
        /// </summary>
        /// <param name="point">
        /// The point to test.
        /// </param>
        /// <returns>
        /// True if the difference is significant between the two points.
        /// </returns>
        private bool IsOutsideSensitivity(Point point)
        {
            point.Offset(-this._originalScreenPoint.X, -this._originalScreenPoint.Y);
            point.X = Math.Abs(point.X);
            point.Y = Math.Abs(point.Y);
            double minimumDeltaX = SystemParameters.MinimumHorizontalDragDistance;
            double minimumDeltaY = SystemParameters.MinimumVerticalDragDistance;
            return point.X > minimumDeltaX || point.Y > minimumDeltaY;
        }

        /// <summary>
        /// Makes a single selection on the items control for this control.
        /// </summary>
        private void MakeSingleSelection()
        {
            ListBox listBox = ItemsControl.ItemsControlFromItemContainer(this) as ListBox;
            if (listBox == null)
            {
                return;
            }

            object item = listBox.ItemContainerGenerator.ItemFromContainer(this);
            if (item == null)
            {
                return;
            }

            if (listBox.SelectedItems.Count != 1 || listBox.SelectedItems[0] != item)
            {
                listBox.SelectedItems.Clear();
                listBox.SelectedItem = item;
                this.MakeAnchorItem(item);
            }

            if (listBox.IsKeyboardFocusWithin)
            {
                this.Focus();
            }
        }

        /// <summary>
        /// Makes a toggle selection on the items control for this control.
        /// </summary>
        private void MakeToggleSelection()
        {
            ListBox listBox = ItemsControl.ItemsControlFromItemContainer(this) as ListBox;
            if (listBox == null)
            {
                return;
            }

            object item = listBox.ItemContainerGenerator.ItemFromContainer(this);
            if (item == null)
            {
                return;
            }

            if (listBox.SelectedItems.Contains(item))
            {
                listBox.SelectedItems.Remove(item);
            }
            else
            {
                listBox.SelectedItems.Add(item);
            }

            if (listBox.IsKeyboardFocusWithin)
            {
                this.Focus();
            }
        }

        /// <summary>
        /// Releases the mouse capture of this control if it had it.
        /// </summary>
        private void ReleaseCapture()
        {
            if (!this.IsDragging)
            {
                return;
            }

            this.ClearValue(_isDraggingPropertyKey);
            if (this.IsMouseCaptured)
            {
                this.ReleaseMouseCapture();
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsDraggableListBoxItem {Class}
} // RSG.Editor.Controls {Namespace}
