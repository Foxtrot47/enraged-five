﻿//---------------------------------------------------------------------------------------------
// <copyright file="DialogueProjectPropertiesControl.xaml.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.View
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using Microsoft.Win32;
    using RSG.Editor.Controls;
    using RSG.Interop.Microsoft.Windows;
    using RSG.Text.ViewModel;
    using RSG.Text.ViewModel.Project;
    using RSG.Editor;

    /// <summary>
    /// Interaction logic for the DialogueProjectPropertiesControl class.
    /// </summary>
    public partial class DialogueProjectPropertiesControl : UserControl
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DialogueProjectPropertiesControl"/>
        /// class.
        /// </summary>
        public DialogueProjectPropertiesControl()
        {
            this.InitializeComponent();
        }
        #endregion Constructors

        #region Methods
        private void BrowseAudioPath(object sender, RoutedEventArgs e)
        {
            DialogueProjectProperties dc = this.DataContext as DialogueProjectProperties;
            if (dc == null || dc.Project == null)
            {
                return;
            }

            Guid persistenceId = new Guid("D4DF05D8-D4BC-4988-9BA7-1D60006CE11A");
            RsSelectFolderDialog dlg = new RsSelectFolderDialog(persistenceId);
            if (dlg.ShowDialog() != true)
            {
                return;
            }

            string directory = dc.Project.DirectoryPath;
            string fullPath = dlg.FileName;
            string relativePath = Shlwapi.MakeRelativeFromDirectoryToFile(directory, fullPath);
            if (relativePath == String.Empty)
            {
                relativePath = fullPath;
            }

            this.AudioPath.Text = relativePath;
        }

        /// <summary>
        /// Called when the user selects to browse for a new configuration path. Shows the user
        /// the standard open file dialogue filtering on xml files.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Window.RoutedEventArgs containing the event data.
        /// </param>
        private void BrowseConfigurationPath(object sender, RoutedEventArgs e)
        {
            DialogueProjectProperties dc = this.DataContext as DialogueProjectProperties;
            if (dc == null || dc.Project == null)
            {
                return;
            }

            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Dialogue Star Config Files (*.dstarconfig)|*.dstarconfig";
            dlg.Title = "Select configuration file";
            dlg.FilterIndex = 0;
            if (dlg.ShowDialog() != true)
            {
                return;
            }

            string directory = dc.Project.DirectoryPath;
            string fullPath = dlg.FileName;
            string relativePath = Shlwapi.MakeRelativeFromDirectoryToFile(directory, fullPath);
            if (relativePath == String.Empty)
            {
                relativePath = fullPath;
            }

            this.ConfigurationPath.Text = relativePath;
        }

        /// <summary>
        /// Called when the user selects to browse for a new edl timings directory path. Shows
        /// the user the rockstar select folder dialogue.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Window.RoutedEventArgs containing the event data.
        /// </param>
        private void BrowseEdlTimingsPath(object sender, RoutedEventArgs e)
        {
            DialogueProjectProperties dc = this.DataContext as DialogueProjectProperties;
            if (dc == null || dc.Project == null)
            {
                return;
            }

            Guid persistenceId = new Guid("A95042DA-01F4-4A2B-B2B7-FD707E22E594");
            RsSelectFolderDialog dlg = new RsSelectFolderDialog(persistenceId);
            if (dlg.ShowDialog() != true)
            {
                return;
            }

            string directory = dc.Project.DirectoryPath;
            string fullPath = dlg.FileName;
            string relativePath = Shlwapi.MakeRelativeFromDirectoryToFile(directory, fullPath);
            if (relativePath == String.Empty)
            {
                relativePath = fullPath;
            }

            this.EdlTimingsDirectory.Text = relativePath;
        }

        /// <summary>
        /// Called when the user selects to browse for a new output path. Shows the user
        /// the rockstar select folder dialogue.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Window.RoutedEventArgs containing the event data.
        /// </param>
        private void BrowseOutputPath(object sender, RoutedEventArgs e)
        {
            DialogueProjectProperties dc = this.DataContext as DialogueProjectProperties;
            if (dc == null || dc.Project == null)
            {
                return;
            }

            Guid persistenceId = new Guid("DC6CF25F-D165-46C1-9F21-3BEFCBFE434D");
            RsSelectFolderDialog dlg = new RsSelectFolderDialog(persistenceId);
            if (dlg.ShowDialog() != true)
            {
                return;
            }

            string directory = dc.Project.DirectoryPath;
            string fullPath = dlg.FileName;
            string relativePath = Shlwapi.MakeRelativeFromDirectoryToFile(directory, fullPath);
            if (relativePath == String.Empty)
            {
                relativePath = fullPath;
            }

            this.OutputPath.Text = relativePath;
        }

        /// <summary>
        /// Called when the user selects to browse for a new configuration path. Shows the user
        /// the standard open file dialogue filtering on xml files.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Window.RoutedEventArgs containing the event data.
        /// </param>
        private void BrowsePsoDataPath(object sender, RoutedEventArgs e)
        {
            DialogueProjectProperties dc = this.DataContext as DialogueProjectProperties;
            if (dc == null || dc.Project == null)
            {
                return;
            }

            Guid persistenceId = new Guid("68746977-AFF3-4DAA-9B00-4E409F8E53D6");
            RsSelectFolderDialog dlg = new RsSelectFolderDialog(persistenceId);
            if (dlg.ShowDialog() != true)
            {
                return;
            }

            string directory = dc.Project.DirectoryPath;
            string fullPath = dlg.FileName;
            string relativePath = Shlwapi.MakeRelativeFromDirectoryToFile(directory, fullPath);
            if (relativePath == String.Empty)
            {
                relativePath = fullPath;
            }

            this.PsoDataOutputPath.Text = relativePath;
        }
        #endregion Methods
    } // RSG.Text.View.DialogueProjectPropertiesControl {Class}
} // RSG.Text.View {Namespace}
