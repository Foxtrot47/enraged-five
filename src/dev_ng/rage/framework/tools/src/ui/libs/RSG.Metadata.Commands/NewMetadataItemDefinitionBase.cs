﻿//---------------------------------------------------------------------------------------------
// <copyright file="NewMetadataItemDefinitionBase.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Commands
{
    using System.Collections.Generic;
    using RSG.Editor;
    using RSG.Metadata.Model.Definitions;
    using RSG.Metadata.Model.Definitions.Members;
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// The command definition for the dynamic add new item menu that lists the valid items
    /// that can be added to the active item.
    /// </summary>
    public abstract class NewMetadataItemDefinitionBase : DynamicMenuCommand
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="NewMetadataItemDefinitionBase"/>
        /// class.
        /// </summary>
        /// <param name="command">
        /// The metadata command this definition is representing.
        /// </param>
        protected NewMetadataItemDefinitionBase(RockstarRoutedCommand command)
            : base(command)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Creates this definition items using the specified dynamic parent as a base. This
        /// adds items to the definition based on the valid items from the specified object.
        /// </summary>
        /// <param name="dynamicParent">
        /// The dynamic parent instance that the items should be created from.
        /// </param>
        public void CreateItems(IDynamicTunableParent dynamicParent)
        {
            if (this.Items.Count > 0)
            {
                this.Items.Clear();
            }

            if (dynamicParent == null)
            {
                return;
            }

            IMember elementType = dynamicParent.ElementType;
            IStructure structure = null;
            if (elementType is PointerMember)
            {
                PointerMember pointerMember = (PointerMember)elementType;
                structure = pointerMember.ReferencedStructure;
                if (pointerMember.CanBeDerivedType)
                {
                    if (structure != null)
                    {
                        foreach (IStructure descendant in GetSelfAndAllDescendents(structure))
                        {
                            this.Items.Add(new DefinitionItem(this, descendant));
                        }
                    }
                }
                else
                {
                    this.Items.Add(new DefinitionItem(this, structure));
                }
            }
            else if (elementType is StructMember)
            {
                structure = ((StructMember)elementType).ReferencedStructure;
                if (structure != null)
                {
                    this.Items.Add(new DefinitionItem(this, structure));
                }
            }
            else
            {
                this.Items.Add(new DefinitionItem(this, elementType.TypeName));
            }
        }

        /// <summary>
        /// Iterators through the specified structure root object and all of its descendants,
        /// immediate and children.
        /// </summary>
        /// <param name="root">
        /// The structure to start the iterator from.
        /// </param>
        /// <returns>
        /// A iterator through the specified structure root object and all of its descendants.
        /// </returns>
        private static IEnumerable<IStructure> GetSelfAndAllDescendents(IStructure root)
        {
            if (root == null)
            {
                yield break;
            }

            yield return root;
            foreach (IStructure descendant in root.ImmediateDescendants)
            {
                foreach (IStructure childDescendant in GetSelfAndAllDescendents(descendant))
                {
                    yield return childDescendant;
                }
            }
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// The private class that is used for the items inside the new metadata item
        /// definitions.
        /// </summary>
        private class DefinitionItem : NotifyPropertyChangedBase, IDynamicMenuItem
        {
            #region Fields
            /// <summary>
            /// The private field used for the <see cref="CommandParameter"/> property.
            /// </summary>
            private IStructure _parameter;

            /// <summary>
            /// The private field used for the <see cref="ParentDefinition"/> property.
            /// </summary>
            private DynamicMenuCommand _parent;

            /// <summary>
            /// The private field used for the <see cref="Text"/> property.
            /// </summary>
            private string _text;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="DefinitionItem"/> class.
            /// </summary>
            /// <param name="parent">
            /// The parent dynamic menu command this item belongs to.
            /// </param>
            /// <param name="text">
            /// The text that is used to display this item.
            /// </param>
            public DefinitionItem(DynamicMenuCommand parent, string text)
            {
                this._parent = parent;
                this._text = text;
            }

            /// <summary>
            /// Initialises a new instance of the <see cref="DefinitionItem"/> class.
            /// </summary>
            /// <param name="parent">
            /// The parent dynamic menu command this item belongs to.
            /// </param>
            /// <param name="parameter">
            /// The command parameter this item represents. This is also used to retrieve the
            /// text used to display this item.
            /// </param>
            public DefinitionItem(DynamicMenuCommand parent, IStructure parameter)
            {
                this._parameter = parameter;
                this._parent = parent;
                this._text = parameter.ShortDataType;
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets the parameter that is routed through the element tree with the command.
            /// </summary>
            public object CommandParameter
            {
                get { return this._parameter; }
            }

            /// <summary>
            /// Gets the parent command definition that this is an item for.
            /// </summary>
            public DynamicMenuCommand ParentDefinition
            {
                get { return this._parent; }
            }

            /// <summary>
            /// Gets or sets the text that is used to display this item.
            /// </summary>
            public string Text
            {
                get { return this._text; }
                set { this.SetProperty(ref this._text, value); }
            }
            #endregion Properties
        } // NewMetadataItemDefinitionBase.DefinitionItem {Class}
        #endregion Classes
    } // RSG.Metadata.Commands.NewMetadataItemDefinitionBase {Class}
} // RSG.Metadata.Commands {Namespace}
