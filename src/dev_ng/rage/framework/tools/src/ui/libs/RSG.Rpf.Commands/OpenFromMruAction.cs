﻿//---------------------------------------------------------------------------------------------
// <copyright file="OpenFromMruAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Rpf.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using RSG.Editor;
    using RSG.Editor.SharedCommands;
    using RSG.Rpf.ViewModel;

    /// <summary>
    /// Implements the <see cref="RockstarCommands.OpenFileFromMru"/> command.
    /// </summary>
    public class OpenFromMruAction : MruCommandActionAsync<IEnumerable<RpfViewerDataContext>>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="OpenFromMruAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public OpenFromMruAction(
            ParameterResolverDelegate<IEnumerable<RpfViewerDataContext>> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="viewModels">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="path">
        /// The path sent with the MRU command.
        /// </param>
        /// <param name="additional">
        /// The additional information for the path property sent with the command.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(
            IEnumerable<RpfViewerDataContext> viewModels, string path, string[] additional)
        {
            if (viewModels == null)
            {
                return false;
            }

            foreach (RpfViewerDataContext viewModel in viewModels)
            {
                if (viewModel != null)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="viewModels">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="path">
        /// The path sent with the MRU command.
        /// </param>
        /// <param name="additional">
        /// The additional information for the path property sent with the command.
        /// </param>
        /// <returns>
        /// A task that represents the work done by the command handler.
        /// </returns>
        public override async Task Execute(
            IEnumerable<RpfViewerDataContext> viewModels, string path, string[] additional)
        {
            if (String.IsNullOrWhiteSpace(path) || viewModels == null)
            {
                return;
            }

            foreach (RpfViewerDataContext viewModel in viewModels)
            {
                await viewModel.LoadAsync(path);
            }
        }
        #endregion Methods
    } // RSG.Rpf.Commands.OpenFromMruAction {Class}
} // RSG.Rpf.Commands {Namespace}
