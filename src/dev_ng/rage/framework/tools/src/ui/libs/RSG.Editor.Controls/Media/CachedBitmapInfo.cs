﻿//---------------------------------------------------------------------------------------------
// <copyright file="CachedBitmapInfo.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Media
{
    /// <summary>
    /// Describes a bitmap image that has been cached for future use.
    /// </summary>
    internal sealed class CachedBitmapInfo
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Width"/> property.
        /// </summary>
        private readonly int _width;

        /// <summary>
        /// The private field used for the <see cref="Height"/> property.
        /// </summary>
        private readonly int _height;

        /// <summary>
        /// The private field used for the <see cref="Data"/> property.
        /// </summary>
        private readonly byte[] _data;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CachedBitmapInfo"/> class.
        /// </summary>
        /// <param name="data">
        /// The byte array the make sup the data of the bitmap.
        /// </param>
        /// <param name="width">
        /// The width of the bitmap.
        /// </param>
        /// <param name="height">
        /// The height of the bitmap.
        /// </param>
        public CachedBitmapInfo(byte[] data, int width, int height)
        {
            this._width = width;
            this._height = height;
            this._data = data;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the width in pixels of the cached bitmap.
        /// </summary>
        public int Width
        {
            get { return this._width; }
        }

        /// <summary>
        /// Gets the height in pixels of the cached bitmap.
        /// </summary>
        public int Height
        {
            get { return this._height; }
        }

        /// <summary>
        /// Gets the cached array of bytes that make up the data for the bitmap.
        /// </summary>
        public byte[] Data
        {
            get { return this._data; }
        }
        #endregion Properties
    } // RSG.Editor.Controls.Media.CachedBitmapInfo {Class}
} // RSG.Editor.Controls.Media {Namespace}
