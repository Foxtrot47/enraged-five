﻿//---------------------------------------------------------------------------------------------
// <copyright file="AddNewLineAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Commands
{
    using RSG.Editor;
    using RSG.Text.ViewModel;

    /// <summary>
    /// Implements the <see cref="TextCommands.AddNewLine"/> command.
    /// </summary>
    public class AddNewLineAction : ButtonAction<TextLineCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="AddNewLineAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public AddNewLineAction(ParameterResolverDelegate<TextLineCommandArgs> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(TextLineCommandArgs args)
        {
            return args.Conversation != null;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(TextLineCommandArgs args)
        {
            args.Conversation.Model.AddNewLine();
            LineViewModel newitem = args.Conversation.Lines[args.Conversation.Lines.Count - 1];
        }
        #endregion Methods
    } // RSG.Text.Commands.AddNewLineAction {Class}
} // RSG.Text.Commands {Namespace}
