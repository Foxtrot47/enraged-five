﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using RSG.Interop.Microsoft.Windows;

namespace RSG.Editor.Controls
{
    /// <summary>
    /// Extension methods for use by the <see cref="RsTaskbarIcon"/> class.
    /// </summary>
    public static class RsTaskbarIconUtil
    {
        #region IsDesignMode
        /// <summary>
        /// Private field for the <see cref="IsDesignMode"/> property.
        /// </summary>
        private static readonly bool _isDesignMode;

        /// <summary>
        /// Checks whether the application is currently in design mode.
        /// </summary>
        public static bool IsDesignMode
        {
            get { return _isDesignMode; }
        }
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Static constructor for the <see cref="RsTaskbarIconUtil"/> class.
        /// </summary>
        static RsTaskbarIconUtil()
        {
            _isDesignMode =
                (bool)DependencyPropertyDescriptor.FromProperty(
                    DesignerProperties.IsInDesignModeProperty,
                    typeof (FrameworkElement)).Metadata.DefaultValue;
        }
        #endregion

        #region ICommand Helper
        /// <summary>
        /// Executes a given command if its <see cref="ICommand.CanExecute"/> method
        /// indicates it can run.
        /// </summary>
        /// <param name="command">The command to be executed, or a null reference.</param>
        /// <param name="commandParameter">An optional parameter that is associated with
        /// the command.</param>
        /// <param name="target">The target element on which to raise the command.</param>
        public static void ExecuteIfEnabled(this ICommand command, object commandParameter, IInputElement target)
        {
            if (command == null)
            {
                return;
            }

            RoutedCommand rc = command as RoutedCommand;
            if (rc != null)
            {
                //routed commands work on a target
                if (rc.CanExecute(commandParameter, target)) rc.Execute(commandParameter, target);
            }
            else if (command.CanExecute(commandParameter))
            {
                command.Execute(commandParameter);
            }
        }
        #endregion

        #region WriteIconData
        /// <summary>
        /// Thread synchronisation object.
        /// </summary>
        private static readonly object SyncRoot = new object();

        /// <summary>
        /// Updates the taskbar icons with data provided by a given
        /// <see cref="NOTIFYICONDATA"/> instance.
        /// </summary>
        /// <param name="data">Configuration settings for the NotifyIcon.</param>
        /// <param name="command">Operation on the icon (e.g. delete the icon).</param>
        /// <returns>True if the data was successfully written.</returns>
        /// <remarks>See Shell_NotifyIcon documentation on MSDN for details.</remarks>
        public static bool WriteIconData(ref NOTIFYICONDATA data, NotifyIconMessage command)
        {
            return WriteIconData(ref data, command, data.uFlags);
        }

        /// <summary>
        /// Updates the taskbar icons with data provided by a given
        /// <see cref="NOTIFYICONDATA"/> instance.
        /// </summary>
        /// <param name="data">Configuration settings for the NotifyIcon.</param>
        /// <param name="command">Operation on the icon (e.g. delete the icon).</param>
        /// <param name="flags">Defines which members of the <paramref name="data"/>
        /// structure are set.</param>
        /// <returns>True if the data was successfully written.</returns>
        /// <remarks>See Shell_NotifyIcon documentation on MSDN for details.</remarks>
        public static bool WriteIconData(ref NOTIFYICONDATA data, NotifyIconMessage command, NotifyIconFlags flags)
        {
            //do nothing if in design mode
            if (IsDesignMode)
            {
                return true;
            }

            data.uFlags = flags;
            lock (SyncRoot)
            {
                return Shell32.NotifyIcon(command, ref data);
            }
        }
        #endregion

        #region GetBalloonFlag
        /// <summary>
        /// Gets a <see cref="NotifyIconBalloonFlags"/> enum value that
        /// matches a given <see cref="BalloonIcon"/>.
        /// </summary>
        public static NotifyIconBalloonFlags GetBalloonFlag(this BalloonIcon icon)
        {
            switch (icon)
            {
                case BalloonIcon.None:
                    return NotifyIconBalloonFlags.None;
                case BalloonIcon.Info:
                    return NotifyIconBalloonFlags.Info;
                case BalloonIcon.Warning:
                    return NotifyIconBalloonFlags.Warning;
                case BalloonIcon.Error:
                    return NotifyIconBalloonFlags.Error;
                default:
                    throw new ArgumentOutOfRangeException("icon");
            }
        }
        #endregion
    }
}
