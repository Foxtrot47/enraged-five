﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsWindow.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Interop;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Threading;
    using System.Xml;
    using RSG.Base.Extensions;
    using RSG.Editor.Controls.Helpers;
    using RSG.Editor.Controls.Resources;
    using RSG.Editor.SharedCommands;
    using RSG.Interop.Microsoft.Windows;

    /// <summary>
    /// Provides the ability to create, configure, show, and manage the lifetime of windows
    /// and dialog boxes within a Rockstar application.
    /// </summary>
    public class RsWindow : Window
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="ActiveGlowColour"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ActiveGlowColourProperty;

        /// <summary>
        /// Identifies the <see cref="CanCloseWindowWithButton"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty CanCloseWindowWithButtonProperty;

        /// <summary>
        /// Identifies the <see cref="CanMaximiseWindow"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty CanMaximiseWindowProperty;

        /// <summary>
        /// Identifies the <see cref="CanMinimiseWindow"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty CanMinimiseWindowProperty;

        /// <summary>
        /// Identifies the <see cref="InactiveGlowColour"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty InactiveGlowColourProperty;

        /// <summary>
        /// Identifies the <see cref="NonClientFillColour"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty NonClientFillColourProperty;

        /// <summary>
        /// Identifies the <see cref="ShowHelpButton"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ShowHelpButtonProperty;

        /// <summary>
        /// Identifies the <see cref="ShowOptionsButton"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ShowOptionsButtonProperty;

        /// <summary>
        /// The command value for the Close system menu item.
        /// </summary>
        private const uint CloseMenuItem = 0xF060;

        /// <summary>
        /// The command value for the Maximise system menu item.
        /// </summary>
        private const uint MaximiseMenuItem = 0xF030;

        /// <summary>
        /// The command value for the Minimise system menu item.
        /// </summary>
        private const uint MinimiseMenuItem = 0xF020;

        /// <summary>
        /// A private constant value representing the minimum number of milliseconds the glow
        /// animation should be shown for.
        /// </summary>
        private const int MinimizeAnimationDurationMilliseconds = 200;

        /// <summary>
        /// The command value for the Move system menu item.
        /// </summary>
        private const uint MoveMenuItem = 0xF010;

        /// <summary>
        /// The command value for the Restore system menu item.
        /// </summary>
        private const uint RestoreMenuItem = 0xF120;

        /// <summary>
        /// The command value for the Size system menu item.
        /// </summary>
        private const uint SizeMenuItem = 0xF000;

        /// <summary>
        /// The private field used for the <see cref="NotifyOwnerActivateCode"/> property.
        /// </summary>
        private static int _notifyOwnerActivateCode;

        /// <summary>
        /// The private list of the glow windows that are shown surrounding this window.
        /// </summary>
        private readonly GlowWindow[] _glowWindows;

        /// <summary>
        /// A value indicating whether the content to this window has been rendered.
        /// </summary>
        private bool _contentRendered;

        /// <summary>
        /// The private field used for the <see cref="DeferGlowChangesCount"/> property.
        /// </summary>
        private int _deferGlowChangesCount;

        /// <summary>
        /// The private field used for the <see cref="DontDelayGlow"/> property.
        /// </summary>
        private bool _dontDelayGlow;

        /// <summary>
        /// The private field used for the <see cref="Handle"/> property.
        /// </summary>
        private IntPtr _handle;

        /// <summary>
        /// The private field used for the <see cref="IsGlowVisible"/> property.
        /// </summary>
        private bool _isGlowVisible;

        /// <summary>
        /// A value indicating whether the non client strip at the bottom of the window is
        /// currently showing.
        /// </summary>
        private bool _isNonClientStripVisible;

        /// <summary>
        /// A cached value representing the last window placement.
        /// </summary>
        private WindowShowState _lastWindowShowState;

        /// <summary>
        /// A dispatcher timer that is used to control the animation to make the glow visible.
        /// </summary>
        private DispatcherTimer _makeGlowVisibleTimer;

        /// <summary>
        /// The private field used for the <see cref="ShowingSystemIconMenu"/> property.
        /// </summary>
        private bool _showingSystemIconMenu;

        /// <summary>
        /// The private field used for the <see cref="SupportCoerceTitle"/> property.
        /// </summary>
        private bool _supportCoerceTitle;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsWindow"/> class.
        /// </summary>
        static RsWindow()
        {
            ActiveGlowColourProperty =
                DependencyProperty.Register(
                "ActiveGlowColour",
                typeof(Color),
                typeof(RsWindow),
                new FrameworkPropertyMetadata(
                    Colors.Transparent,
                    new PropertyChangedCallback(OnGlowColourChanged)));

            InactiveGlowColourProperty =
                DependencyProperty.Register(
                "InactiveGlowColour",
                typeof(Color),
                typeof(RsWindow),
                new FrameworkPropertyMetadata(
                    Colors.Transparent,
                    new PropertyChangedCallback(OnGlowColourChanged)));

            NonClientFillColourProperty =
                DependencyProperty.Register(
                "NonClientFillColor",
                typeof(Color),
                typeof(RsWindow),
                new FrameworkPropertyMetadata(Colors.Black));

            ShowHelpButtonProperty =
                DependencyProperty.Register(
                "ShowHelpButton",
                typeof(bool),
                typeof(RsWindow),
                new PropertyMetadata(false));

            ShowOptionsButtonProperty =
                DependencyProperty.Register(
                "ShowOptionsButton",
                typeof(bool),
                typeof(RsWindow),
                new PropertyMetadata(false));

            CanCloseWindowWithButtonProperty =
                DependencyProperty.Register(
                "CanCloseWindowWithButton",
                typeof(bool),
                typeof(RsWindow),
                new PropertyMetadata(true));

            CanMinimiseWindowProperty =
                DependencyProperty.Register(
                "CanMinimiseWindow",
                typeof(bool),
                typeof(RsWindow),
                new PropertyMetadata(true));

            CanMaximiseWindowProperty =
                DependencyProperty.Register(
                "CanMaximiseWindow",
                typeof(bool),
                typeof(RsWindow),
                new PropertyMetadata(true));

            Window.ResizeModeProperty.OverrideMetadata(
                typeof(RsWindow),
                new FrameworkPropertyMetadata(
                    new PropertyChangedCallback(OnResizeModeChanged)));

            Window.TitleProperty.OverrideMetadata(
                typeof(RsWindow),
                new FrameworkPropertyMetadata(null, CoerceTitle));

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsWindow),
                new FrameworkPropertyMetadata(typeof(RsWindow)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsWindow"/> class.
        /// </summary>
        protected RsWindow()
        {
            this._glowWindows = new GlowWindow[4];
            this.SetValue(FocusManager.IsFocusScopeProperty, true);
            this.SetValue(Window.UseLayoutRoundingProperty, true);
            this.SetValue(TextOptions.TextFormattingModeProperty, TextFormattingMode.Display);

            RockstarCommandManager.AddBinding(
                this, RockstarViewCommands.ShowHelpFromTitleBar, this.ShowHelpFromTitleBar);

            RockstarCommandManager.AddBinding(
                this,
                RockstarViewCommands.ShowOptionsFromTitleBar,
                this.ShowOptionsFromTitleBar);

            RockstarCommandManager.AddBinding(
                this,
                RockstarViewCommands.MinimiseWindow,
                this.MinimiseWindow,
                this.DetermineMinimiseWindow);

            RockstarCommandManager.AddBinding(
                this,
                RockstarViewCommands.MaximiseWindow,
                this.MaximiseWindow,
                this.DetermineMaximiseWindow);

            RockstarCommandManager.AddBinding(
                this, RockstarViewCommands.RestoreWindow, this.RestoreWindow);

            RockstarCommandManager.AddBinding(
                this, RockstarViewCommands.CloseWindow, this.CloseWindow);

            RockstarCommandManager.AddBinding(
                this,
                RockstarViewCommands.CancelDialog,
                this.CancelDialog,
                this.CanCancelDialog);

            RockstarCommandManager.AddBinding(
                this,
                RockstarViewCommands.ConfirmDialog,
                this.ConfirmDialog,
                this.CanConfirmDialog);

            RockstarCommandManager.AddBinding(
                this, RockstarCommands.Exit, this.OnExit);

            RockstarCommandManager.AddBinding<string>(
                this,
                RockstarCommands.ShellExecute,
                this.OnShellExecute,
                this.CanShellExecute);

            RsApplication app = Application.Current as RsApplication;
            if (app == null)
            {
                this.HandleDisconnectedApplication();
            }
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs whenever the native message hook detects a key down message.
        /// </summary>
        public event EventHandler<NativeKeyDownEventArgs> NativeKeyDown;
        #endregion Events

        #region Enum
        /// <summary>
        /// Defines the different region change types that can occur.
        /// </summary>
        private enum RegionChangeType
        {
            /// <summary>
            /// Means that no change has actually occurred.
            /// </summary>
            None,

            /// <summary>
            /// Means that the clip region has changed due to the window size changing.
            /// </summary>
            FromSize,

            /// <summary>
            /// Means that the clip region has changed due to the window position changing.
            /// </summary>
            FromPosition,

            /// <summary>
            /// Means that the clip region has changed due to a property changing.
            /// </summary>
            FromPropertyChange
        } // RSG.Editor.Controls.RsWindow.ClipRegionChangeType {Enum}
        #endregion Enum

        #region Properties
        /// <summary>
        /// Gets or sets the colour of the glow when this window is in a active state.
        /// </summary>
        public Color ActiveGlowColour
        {
            get { return (Color)this.GetValue(ActiveGlowColourProperty); }
            set { this.SetValue(ActiveGlowColourProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the window can be closed using the close
        /// button in the top-right.
        /// </summary>
        public bool CanCloseWindowWithButton
        {
            get { return (bool)this.GetValue(CanCloseWindowWithButtonProperty); }
            set { this.SetValue(CanCloseWindowWithButtonProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the window can be maximise.
        /// </summary>
        public bool CanMaximiseWindow
        {
            get { return (bool)this.GetValue(CanMaximiseWindowProperty); }
            set { this.SetValue(CanMaximiseWindowProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the window can be minimised.
        /// </summary>
        public bool CanMinimiseWindow
        {
            get { return (bool)this.GetValue(CanMinimiseWindowProperty); }
            set { this.SetValue(CanMinimiseWindowProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the windows glow border should be delayed
        /// before being shown to the user.
        /// </summary>
        public bool DontDelayGlow
        {
            get { return this._dontDelayGlow; }
            set { this._dontDelayGlow = value; }
        }

        /// <summary>
        /// Gets or sets the colour of the glow when this window is in a inactive state.
        /// </summary>
        public Color InactiveGlowColour
        {
            get { return (Color)this.GetValue(InactiveGlowColourProperty); }
            set { this.SetValue(InactiveGlowColourProperty, value); }
        }

        /// <summary>
        /// Gets or sets the colour that the non client area gets filled with.
        /// </summary>
        public Color NonClientFillColour
        {
            get { return (Color)this.GetValue(NonClientFillColourProperty); }
            set { this.SetValue(NonClientFillColourProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the help button is shown in the system
        /// menu on the title bar.
        /// </summary>
        public bool ShowHelpButton
        {
            get { return (bool)this.GetValue(ShowHelpButtonProperty); }
            set { this.SetValue(ShowHelpButtonProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the options button is shown in the system
        /// menu on the title bar.
        /// </summary>
        public bool ShowOptionsButton
        {
            get { return (bool)this.GetValue(ShowOptionsButtonProperty); }
            set { this.SetValue(ShowOptionsButtonProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the title for this window is coerce with
        /// the product name.
        /// </summary>
        public bool SupportCoerceTitle
        {
            get
            {
                return this._supportCoerceTitle;
            }

            set
            {
                if (this._supportCoerceTitle == value)
                {
                    return;
                }

                this._supportCoerceTitle = value;
                this.CoerceValue(RsWindow.TitleProperty);
            }
        }

        /// <summary>
        /// Gets the number of changes to the glow properties that have currently been deferred
        /// for the next commit.
        /// </summary>
        internal int DeferGlowChangesCount
        {
            get { return this._deferGlowChangesCount; }
            private set { this._deferGlowChangesCount = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the system icon context menu is currently
        /// being shown for this window.
        /// </summary>
        internal bool ShowingSystemIconMenu
        {
            get { return this._showingSystemIconMenu; }
            set { this._showingSystemIconMenu = value; }
        }

        /// <summary>
        /// Gets the unique message code for the custom Notify Owner Activate window message.
        /// </summary>
        protected static int NotifyOwnerActivateCode
        {
            get
            {
                if (_notifyOwnerActivateCode != 0)
                {
                    return _notifyOwnerActivateCode;
                }

                _notifyOwnerActivateCode =
                    User32.RegisterWindowMessage(WindowResources.NotifyOwnerActivate);
                return _notifyOwnerActivateCode;
            }
        }

        /// <summary>
        /// Gets the windows handle object for this window, guaranteed to not change during the
        /// lift time of the window.
        /// </summary>
        protected IntPtr Handle
        {
            get { return this._handle; }
        }

        /// <summary>
        /// Gets the product name this window should be using to coerce the title.
        /// </summary>
        protected virtual string ProductName
        {
            get
            {
                RsApplication app = Application.Current as RsApplication;
                if (app == null)
                {
                    return null;
                }

                return app.ProductName;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the glow should be shown to the user.
        /// </summary>
        protected virtual bool ShouldShowGlow
        {
            get
            {
                IntPtr handle = new WindowInteropHelper(this).Handle;
                if (!User32.IsWindowVisible(handle))
                {
                    return false;
                }

                if (User32.IsZoomed(handle) || User32.IsIconic(handle))
                {
                    return false;
                }

                return true;
            }
        }

        /// <summary>
        /// Gets a int value representing what mouse buttons are currently pressed if any.
        /// </summary>
        private static int PressedMouseButtons
        {
            get
            {
                int num = 0;
                if (User32.IsKeyPressed(VirtualKey.LBUTTON))
                {
                    num |= 1;
                }

                if (User32.IsKeyPressed(VirtualKey.RBUTTON))
                {
                    num |= 2;
                }

                if (User32.IsKeyPressed(VirtualKey.MBUTTON))
                {
                    num |= 16;
                }

                if (User32.IsKeyPressed(VirtualKey.XBUTTON1))
                {
                    num |= 32;
                }

                if (User32.IsKeyPressed(VirtualKey.XBUTTON2))
                {
                    num |= 64;
                }

                return num;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the glow is currently visible to the user.
        /// </summary>
        private bool IsGlowVisible
        {
            get
            {
                return this._isGlowVisible;
            }

            set
            {
                if (this._isGlowVisible != value)
                {
                    this._isGlowVisible = value;
                    for (int i = 0; i < this._glowWindows.Length; i++)
                    {
                        this.GetOrCreateGlowWindow(i).IsVisible = value;
                    }
                }
            }
        }

        /// <summary>
        /// Gets a iterator over the glow windows that are currently loaded for this window.
        /// </summary>
        private IEnumerable<GlowWindow> LoadedGlowWindows
        {
            get
            {
                foreach (GlowWindow glowWindow in this._glowWindows)
                {
                    if (glowWindow == null)
                    {
                        continue;
                    }

                    yield return glowWindow;
                }
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new IntPtr from the specified point.
        /// </summary>
        /// <param name="point">
        /// The point to convert into a IntPtr.
        /// </param>
        /// <returns>
        /// A new IntPtr that has the specified point embedded into it.
        /// </returns>
        public static IntPtr MakeParam(POINT point)
        {
            return new IntPtr((point.X & 65535) | point.Y << 16);
        }

        /// <summary>
        /// Changes the windows owner to the window with the specified handle.
        /// </summary>
        /// <param name="newOwner">
        /// The handle to the new owner.
        /// </param>
        public void ChangeOwner(IntPtr newOwner)
        {
            WindowInteropHelper windowInteropHelper = new WindowInteropHelper(this);
            windowInteropHelper.Owner = newOwner;
            foreach (GlowWindow current in this.LoadedGlowWindows)
            {
                current.ChangeOwner(newOwner);
            }

            this.UpdateDepthOrderOfThisAndOwner();
        }

        /// <summary>
        /// Deserialises the layout for this control using the data contained within the
        /// specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the layout data to apply to this window.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        public virtual void DeserialiseLayout(XmlReader reader, IFormatProvider provider)
        {
            double width;
            if (double.TryParse(reader.GetAttribute("width"), out width))
            {
                this.Width = width;
            }

            double height;
            if (double.TryParse(reader.GetAttribute("height"), out height))
            {
                this.Height = height;
            }

            double top;
            if (double.TryParse(reader.GetAttribute("top"), out top))
            {
                POINT pt = new POINT(0, (int)top);
                IntPtr monitor = User32.GetMonitorFromPoint(pt, GetMonitorFlag.DEFAULTTONULL);
                if (monitor != IntPtr.Zero)
                {
                    this.Top = top;
                }
                else
                {
                    this.Top = 8.0;
                }
            }

            double left;
            if (double.TryParse(reader.GetAttribute("left"), out left))
            {
                POINT pt = new POINT((int)left, 0);
                IntPtr monitor = User32.GetMonitorFromPoint(pt, GetMonitorFlag.DEFAULTTONULL);
                if (monitor != IntPtr.Zero)
                {
                    this.Left = left;
                }
                else
                {
                    this.Left = 8.0;
                }
            }

            bool maximised;
            if (bool.TryParse(reader.GetAttribute("max"), out maximised))
            {
                if (maximised)
                {
                    this.WindowState = WindowState.Maximized;
                }
                else
                {
                    this.WindowState = WindowState.Normal;
                }
            }

            reader.ReadStartElement();
        }

        /// <summary>
        /// Serialises the layout of this control into the specified xml writer.
        /// </summary>
        /// <param name="writer">
        /// The xml writer that the layout gets serialised out to.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        public virtual void SerialiseLayout(XmlWriter writer, IFormatProvider provider)
        {
            writer.WriteAttributeString("width", this.Width.ToString(provider));
            writer.WriteAttributeString("height", this.Height.ToString(provider));
            writer.WriteAttributeString("top", this.Top.ToString(provider));
            writer.WriteAttributeString("left", this.Left.ToString(provider));

            bool maximised = this.WindowState == WindowState.Maximized;
            writer.WriteAttributeString("max", maximised.ToString(provider));
        }

        /// <summary>
        /// Shows the system context menu for this window.
        /// </summary>
        /// <param name="element">
        /// The element on which the menu should be shown.
        /// </param>
        /// <param name="elementPoint">
        /// The point in the elements coordinates the menu should be shown at.
        /// </param>
        /// <param name="elementSize">
        /// The render size of the element that the menu should be shown on.
        /// </param>
        public void ShowWindowMenu(Visual element, Point elementPoint, Size elementSize)
        {
            if (element == null || elementPoint.X < 0.0 || elementPoint.Y < 0.0)
            {
                return;
            }

            if (elementPoint.X > elementSize.Width || elementPoint.Y > elementSize.Height)
            {
                return;
            }

            Point screenPoint = element.PointToScreen(elementPoint);
            IntPtr window = this._handle;

            bool modifiedVisibility = this.ModifyStyle(WindowStyles.VISIBLE, 0);
            IntPtr menu = User32.GetSystemMenu(this._handle);
            WINDOWPLACEMENT windowPlacement = User32.GetWindowPlacement(window);
            if (windowPlacement.ShowCmd == WindowShowState.SHOWNORMAL)
            {
                User32.GreyOutMenuItem(menu, RestoreMenuItem);
                User32.EnableMenuItem(menu, MoveMenuItem);
                if (this.ResizeMode != ResizeMode.NoResize)
                {
                    if (this.ShowHelpButton)
                    {
                        User32.EnableMenuItem(menu, SizeMenuItem);
                        User32.GreyOutMenuItem(menu, MaximiseMenuItem);
                        User32.GreyOutMenuItem(menu, MinimiseMenuItem);
                    }
                    else
                    {
                        if (this.CanMaximiseWindow)
                        {
                            User32.EnableMenuItem(menu, MaximiseMenuItem);
                        }
                        else
                        {
                            User32.GreyOutMenuItem(menu, MaximiseMenuItem);
                        }

                        if (this.CanMinimiseWindow)
                        {
                            User32.EnableMenuItem(menu, MinimiseMenuItem);
                        }
                        else
                        {
                            User32.GreyOutMenuItem(menu, MinimiseMenuItem);
                        }

                        User32.EnableMenuItem(menu, SizeMenuItem);
                    }
                }
                else
                {
                    User32.GreyOutMenuItem(menu, MinimiseMenuItem);
                    User32.GreyOutMenuItem(menu, SizeMenuItem);
                    User32.GreyOutMenuItem(menu, MaximiseMenuItem);
                }
            }
            else if (windowPlacement.ShowCmd == WindowShowState.SHOWMAXIMIZED)
            {
                User32.GreyOutMenuItem(menu, MoveMenuItem);
                User32.GreyOutMenuItem(menu, SizeMenuItem);
                User32.GreyOutMenuItem(menu, MaximiseMenuItem);
                if (this.ShowHelpButton)
                {
                    User32.EnableMenuItem(menu, RestoreMenuItem);
                    User32.GreyOutMenuItem(menu, MinimiseMenuItem);
                }
                else
                {
                    if (this.CanMaximiseWindow)
                    {
                        User32.EnableMenuItem(menu, RestoreMenuItem);
                        User32.EnableMenuItem(menu, MaximiseMenuItem);
                    }
                    else
                    {
                        User32.GreyOutMenuItem(menu, RestoreMenuItem);
                        User32.GreyOutMenuItem(menu, MaximiseMenuItem);
                    }

                    if (this.CanMinimiseWindow)
                    {
                        User32.EnableMenuItem(menu, MinimiseMenuItem);
                    }
                    else
                    {
                        User32.GreyOutMenuItem(menu, MinimiseMenuItem);
                    }
                }
            }

            if (this.CanCloseWindowWithButton)
            {
                User32.EnableMenuItem(menu, CloseMenuItem);
            }
            else
            {
                User32.GreyOutMenuItem(menu, CloseMenuItem);
            }

            if (modifiedVisibility)
            {
                this.ModifyStyle(0, WindowStyles.VISIBLE);
            }

            int dropAlignment = User32.GetSystemMetrics(SystemMetric.MENUDROPALIGNMENT);
            TrackPopupMenuFlags flags = TrackPopupMenuFlags.BASE;
            if (dropAlignment == 0)
            {
                flags |= TrackPopupMenuFlags.LEFTALIGN;
            }
            else
            {
                flags |= TrackPopupMenuFlags.RIGHTALIGN;
            }

            int x = (int)screenPoint.X;
            int y = (int)screenPoint.Y;
            int num = User32.TrackPopupMenuEx(menu, flags, x, y, window);
            if (num == 0)
            {
                return;
            }

            User32.PostMessage(window, WindowMessage.SYSCOMMAND, new IntPtr(num), IntPtr.Zero);
        }

        /// <summary>
        /// Captures a screenshot of the main.
        /// </summary>
        /// <returns>
        /// Bitmap source containing a screenshot of the window.
        /// </returns>
        public BitmapSource CaptureScreenshot()
        {
            return ScreenCapture.CaptureWindow(this._handle);
        }

        /// <summary>
        /// Determines whether the
        /// <see cref="RSG.Editor.SharedCommands.RockstarViewCommands.CancelDialog"/> command
        /// can be executed at this level.
        /// </summary>
        /// <param name="e">
        /// The command data that is past from the command system.
        /// </param>
        /// <returns>
        /// True if the command can be executed here; otherwise, false.
        /// </returns>
        protected virtual bool CanCancelDialog(CanExecuteCommandData e)
        {
            return true;
        }

        /// <summary>
        /// Handles the
        /// <see cref="RSG.Editor.SharedCommands.RockstarViewCommands.CancelDialog"/> command
        /// after it gets caught here.
        /// </summary>
        /// <param name="e">
        /// The command data that is past from the command system.
        /// </param>
        protected virtual void CancelDialog(ExecuteCommandData e)
        {
            this.DialogResult = false;
        }

        /// <summary>
        /// Determines whether the
        /// <see cref="RSG.Editor.SharedCommands.RockstarViewCommands.ConfirmDialog"/> command
        /// can be executed at this level.
        /// </summary>
        /// <param name="e">
        /// The command data that is past from the command system.
        /// </param>
        /// <returns>
        /// True if the command can be executed here; otherwise, false.
        /// </returns>
        protected virtual bool CanConfirmDialog(CanExecuteCommandData e)
        {
            return true;
        }

        /// <summary>
        /// Handles the
        /// <see cref="RSG.Editor.SharedCommands.RockstarViewCommands.ConfirmDialog"/> command
        /// after it gets caught here.
        /// </summary>
        /// <param name="e">
        /// The command data that is past from the command system.
        /// </param>
        protected virtual void ConfirmDialog(ExecuteCommandData e)
        {
            this.DialogResult = true;
        }

        /// <summary>
        /// Called whenever the window receives the WM_SYSCOMMAND message with the CONTEXTHELP
        /// identifier set in the word parameter.
        /// </summary>
        protected virtual void HandleContextHelp()
        {
        }

        /// <summary>
        /// Handles window messages for this window.
        /// </summary>
        /// <param name="window">
        /// The window handle.
        /// </param>
        /// <param name="msg">
        /// The message ID.
        /// </param>
        /// <param name="wordParam">
        /// The message's additional word parameter value.
        /// </param>
        /// <param name="longParam">
        /// The message's additional long parameter value.
        /// </param>
        /// <param name="handled">
        /// A value that indicates whether the message was handled.
        /// </param>
        /// <returns>
        /// The appropriate return value depends on the particular message.
        /// </returns>
        protected virtual IntPtr MessageHook(
            IntPtr window, int msg, IntPtr wordParam, IntPtr longParam, ref bool handled)
        {
            WindowMessage message = (WindowMessage)msg;

            Key key = Key.None;
            if (message == WindowMessage.KEYDOWN || message == WindowMessage.SYSKEYDOWN)
            {
                key = KeyInterop.KeyFromVirtualKey(wordParam.ToInt32());
            }
            else if (message == WindowMessage.KEYUP)
            {
                key = KeyInterop.KeyFromVirtualKey(wordParam.ToInt32());
                if (key != Key.Escape)
                {
                    key = Key.None;
                }
            }

            if (key != Key.None)
            {
                EventHandler<NativeKeyDownEventArgs> handler = this.NativeKeyDown;
                if (handler != null)
                {
                    handler(this, new NativeKeyDownEventArgs(key));
                }
            }

            if (message == WindowMessage.SYSCOMMAND)
            {
                if (wordParam.ToInt32() == (int)SystemCommandType.CONTEXTHELP)
                {
                    this.HandleContextHelp();
                    handled = true;
                    return IntPtr.Zero;
                }
            }

            if (msg <= 71)
            {
                if (msg == 6)
                {
                    this.WmActivate(wordParam, longParam);
                    return IntPtr.Zero;
                }

                if (msg != 12)
                {
                    switch (msg)
                    {
                        case 70:
                            this.WmWindowPosChanging(longParam);
                            return IntPtr.Zero;
                        case 71:
                            if (this._contentRendered || this.Content == null)
                            {
                                this.WmWindowPosChanged(longParam);
                            }

                            return IntPtr.Zero;
                        default:
                            return IntPtr.Zero;
                    }
                }

                handled = true;
                return this.CallDefWindowProc(window, message, wordParam, longParam);
            }

            switch ((WindowMessage)msg)
            {
                case WindowMessage.SETICON:
                    break;
                case WindowMessage.NCCREATE:
                case WindowMessage.NCDESTROY:
                    return IntPtr.Zero;
                case WindowMessage.NCCALCSIZE:
                    handled = true;
                    this.WmNcCalcSize(window, wordParam, longParam);
                    return IntPtr.Zero;
                case WindowMessage.NCHITTEST:
                    return this.WmNcHitTest(longParam, ref handled);
                case WindowMessage.NCPAINT:
                    handled = true;
                    this.WmNcPaint(wordParam, longParam);
                    return IntPtr.Zero;
                case WindowMessage.NCACTIVATE:
                    return WmNcActivate(window, wordParam, ref handled);
                case WindowMessage.NCRBUTTONDOWN:
                case WindowMessage.NCRBUTTONUP:
                case WindowMessage.NCRBUTTONDBLCLK:
                    this.ReRaiseMouseMessage(msg, longParam);
                    handled = true;
                    return IntPtr.Zero;
                case WindowMessage.NCUAHDRAWCAPTION:
                case WindowMessage.NCUAHDRAWFRAME:
                    handled = true;
                    return IntPtr.Zero;
                default:
                    return IntPtr.Zero;
            }

            handled = true;
            return this.CallDefWindowProc(window, message, wordParam, longParam);
        }

        /// <summary>
        /// Gets called whenever the state for this window gets switched to active and updates
        /// the glow properties accordingly.
        /// </summary>
        /// <param name="e">
        /// The System.EventArgs for this event.
        /// </param>
        protected override void OnActivated(EventArgs e)
        {
            this.UpdateGlowActiveState();
            base.OnActivated(e);
        }

        /// <summary>
        /// Gets called when this window has been closed so that the glow windows can
        /// get destroyed.
        /// </summary>
        /// <param name="e">
        /// The System.EventArgs for this event.
        /// </param>
        protected override void OnClosed(EventArgs e)
        {
            this.StopTimer();
            this.DestroyGlowWindows();
            base.OnClosed(e);
        }

        /// <summary>
        /// Gets called when this window is about to be closed and gives it the opportunity to
        /// cancel.
        /// </summary>
        /// <param name="e">
        /// The System.ComponentModel.CancelEventArgs for this event.
        /// </param>
        protected sealed override void OnClosing(CancelEventArgs e)
        {
            Application baseApp = Application.Current;
            if (baseApp == null)
            {
                return;
            }

            bool exiting = Application.Current.MainWindow == this;
            if (exiting)
            {
                exiting = Application.Current.ShutdownMode == ShutdownMode.OnMainWindowClose;
            }

            RsApplication app = Application.Current as RsApplication;
            if (app != null && !app.Exiting)
            {
                ISet<Window> windows;
                if (exiting)
                {
                    windows = new HashSet<Window>(app.Windows.OfType<Window>());
                }
                else
                {
                    windows = new HashSet<Window>(Enumerable.Repeat(this, 1));
                }

                bool interaction = !app.CanCloseWithoutInteraction(windows);
                if (interaction)
                {
                    CancelEventArgs cancelArgs = new CancelEventArgs();
                    app.HandleWindowsClosing(windows, cancelArgs);
                    if (cancelArgs.Cancel == true)
                    {
                        e.Cancel = true;
                        return;
                    }
                }

                if (exiting)
                {
                    app.HandleOnExiting();
                }
            }
        }

        /// <summary>
        /// Called when the content to this window is rendered for the first time.
        /// </summary>
        /// <param name="e">
        /// The System.EventArgs data used for this event.
        /// </param>
        protected override void OnContentRendered(EventArgs e)
        {
            this._contentRendered = true;
            base.OnContentRendered(e);
            this.UpdateGlowVisibility(false);
            this.UpdateGlowWindowPositions(true);
            this.UpdateDepthOrderOfThisAndOwner();
            this.UpdateClipRegion(RegionChangeType.FromSize);
        }

        /// <summary>
        /// Gets called whenever the state for this window gets switched to not active and
        /// updates the glow properties accordingly.
        /// </summary>
        /// <param name="e">
        /// The System.EventArgs for this event.
        /// </param>
        protected override void OnDeactivated(EventArgs e)
        {
            this.UpdateGlowActiveState();
            base.OnDeactivated(e);
        }

        /// <summary>
        /// Invoked when an unhandled System.Windows.Input.Keyboard.PreviewKeyDown attached
        /// event reaches an element in its route that is derived from this class.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.KeyEventArgs that contains the event data.
        /// </param>
        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            if (!this.CanCloseWindowWithButton)
            {
                if (Keyboard.IsKeyDown(Key.LeftAlt) || Keyboard.IsKeyDown(Key.RightAlt))
                {
                    if (e.SystemKey == Key.F4)
                    {
                        e.Handled = true;
                        return;
                    }
                }
            }

            base.OnPreviewKeyDown(e);
        }

        /// <summary>
        /// Gets called when the source for this window has been initialised so that the
        /// message pump hook can be added to handle window messages for this window.
        /// </summary>
        /// <param name="e">
        /// The System.EventArgs for this event.
        /// </param>
        protected override void OnSourceInitialized(EventArgs e)
        {
            this._handle = new WindowInteropHelper(this).Handle;
            if (this.SizeToContent == SizeToContent.WidthAndHeight)
            {
                Rect windowRect = User32.GetClientRect(this._handle);
                this.SizeToContent = System.Windows.SizeToContent.Manual;
                this.Width = windowRect.Width;
                this.Height = windowRect.Height;
                this.UpdateLayout();
                this.InvalidateVisual();
            }

            HwndSource hwndSource = HwndSource.FromHwnd(this._handle);
            hwndSource.AddHook(new HwndSourceHook(this.MessageHook));
            base.OnSourceInitialized(e);

            this.UpdateClipRegion(RegionChangeType.FromPropertyChange);
            IntPtr menu = User32.GetSystemMenu(this._handle);
            User32.SetMenuDefaultItem(menu, CloseMenuItem);
            User32.DrawMenuBar(this._handle);

            if (this.CanMaximiseWindow)
            {
                this.ModifyStyle(0, WindowStyles.MAXIMIZEBOX);
            }
            else
            {
                this.ModifyStyle(WindowStyles.MAXIMIZEBOX, 0);
            }

            if (this.CanMinimiseWindow)
            {
                this.ModifyStyle(0, WindowStyles.MINIMIZEBOX);
            }
            else
            {
                this.ModifyStyle(WindowStyles.MINIMIZEBOX, 0);
            }

            if (this.ShowHelpButton)
            {
                this.ModifyStyle(WindowStyles.MINIMIZEBOX | WindowStyles.MAXIMIZEBOX, 0);
                this.ModifyStyle(0, WindowExStyles.CONTEXTHELP);

                IntPtr type = new IntPtr((int)SystemCommandType.RESTORE);
                User32.SendMessage(this.Handle, WindowMessage.SYSCOMMAND, type, IntPtr.Zero);
            }
            else
            {
                this.ModifyStyle(WindowExStyles.CONTEXTHELP, 0);
                WindowStyles style = WindowStyles.NONE;
                if (this.CanMaximiseWindow)
                {
                    style |= WindowStyles.MAXIMIZEBOX;
                }

                if (this.CanMinimiseWindow)
                {
                    style |= WindowStyles.MINIMIZEBOX;
                }

                this.ModifyStyle(0, style);
            }

            User32.SetWindowPos(
                this.Handle,
                WindowPosFlags.FRAMECHANGED | WindowPosFlags.NOSIZE | WindowPosFlags.NOMOVE);
        }

        /// <summary>
        /// Override to handle when the user clicks on the options button on the titlebar.
        /// </summary>
        protected virtual void ShowOptionsFromTitleBar()
        {
        
        }

        /// <summary>
        /// Called whenever the Title dependency property needs to be
        /// re-evaluated.
        /// </summary>
        /// <param name="s">
        /// The <see cref="RsMainWindow"/> instance whose dependency property needs evaluated.
        /// </param>
        /// <param name="baseValue">
        /// The new value of the property, prior to any coercion attempt.
        /// </param>
        /// <returns>
        /// The coerced value.
        /// </returns>
        private static object CoerceTitle(DependencyObject s, object baseValue)
        {
            RsWindow window = s as RsWindow;
            if (window == null)
            {
                Debug.Assert(false, "Incorrect cast type in property coerce");
                return baseValue;
            }

            if (!window.SupportCoerceTitle)
            {
                return baseValue;
            }

            string value = baseValue as string;
            RsApplication app = Application.Current as RsApplication;
            if (app == null)
            {
                return baseValue;
            }

            string productName = app.ProductName;
            if (productName == null)
            {
                return baseValue;
            }

            if (String.IsNullOrEmpty(value))
            {
                return productName;
            }
            else
            {
                return "{0} - {1}".FormatInvariant(value, productName);
            }
        }

        /// <summary>
        /// Creates a new rounded rectangular region with the size and shape determined by
        /// the given parameters.
        /// </summary>
        /// <param name="width">
        /// The width of the region to create.
        /// </param>
        /// <param name="height">
        /// The height to the region to create.
        /// </param>
        /// <returns>
        /// A handle to the newly created rounded region.
        /// </returns>
        private static IntPtr CreateRegion(int width, int height)
        {
            int right = width + 1;
            int bottom = height + 1;

            return Gdi32.CreateRectRgn(0, 0, right, bottom);
        }

        /// <summary>
        /// Retrieves the low-order word for the given value.
        /// </summary>
        /// <param name="value">
        /// The value to return the low-order word from.
        /// </param>
        /// <returns>
        /// The low-order word for the given value.
        /// </returns>
        private static int GetHiWord(int value)
        {
            return (int)((short)(value >> 16));
        }

        /// <summary>
        /// Retrieves the low-order word for the given value.
        /// </summary>
        /// <param name="value">
        /// The value to return the low-order word from.
        /// </param>
        /// <returns>
        /// The low-order word for the given value.
        /// </returns>
        private static int GetLowWord(int value)
        {
            return (int)((short)(value & 65535));
        }

        /// <summary>
        /// Gets called whenever the <see cref="ActiveGlowColour"/> dependency property or the
        /// <see cref="InactiveGlowColour"/> dependency property changes on a
        /// <see cref="RsWindow"/> instance.
        /// </summary>
        /// <param name="s">
        /// The <see cref="RsWindow"/> instance whose dependency property changed.
        /// </param>
        /// <param name="args">
        /// The System.Windows.DependencyPropertyChangedEventArgs data used for this event.
        /// </param>
        private static void OnGlowColourChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs args)
        {
            RsWindow window = s as RsWindow;
            if (window == null)
            {
                return;
            }

            window.UpdateGlowColors();
        }

        /// <summary>
        /// Called whenever the ResizeMode dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The <see cref="RsWindow"/> instance whose dependency property changed.
        /// </param>
        /// <param name="args">
        /// The System.Windows.DependencyPropertyChangedEventArgs data used for this event.
        /// </param>
        private static void OnResizeModeChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs args)
        {
            RsWindow window = s as RsWindow;
            if (window == null)
            {
                return;
            }

            bool canBeUsedToResize = true;
            if (window.ResizeMode == ResizeMode.NoResize)
            {
                canBeUsedToResize = false;
            }

            foreach (GlowWindow current in window.LoadedGlowWindows)
            {
                current.CanBeUsedToResize = canBeUsedToResize;
            }
        }

        /// <summary>
        /// Updates the Z order for the owner window of this window.
        /// </summary>
        /// <param name="owner">
        /// The handle to the owner window to update.
        /// </param>
        private static void UpdateDepthOrderOfOwner(IntPtr owner)
        {
            IntPtr lastOwnedWindow = IntPtr.Zero;
            foreach (IntPtr window in User32.CurrentThreadWindows())
            {
                if (User32.GetWindow(window, GetWindowFlag.OWNER) == owner)
                {
                    lastOwnedWindow = window;
                }
            }

            IntPtr aboveWindow = User32.GetWindow(owner, GetWindowFlag.HWNDPREV);
            if (lastOwnedWindow != IntPtr.Zero && aboveWindow != lastOwnedWindow)
            {
                User32.SetWindowPos(owner, lastOwnedWindow, WindowPosFlags.NOACTION);
            }
        }

        /// <summary>
        /// Gets called when this window receives the WM_NCACTIVATE message.
        /// </summary>
        /// <param name="window">
        /// A handle to the window.
        /// </param>
        /// <param name="wordParam">
        /// The word additional message information.
        /// </param>
        /// <param name="handled">
        /// A value that indicates whether this method has handled the message.
        /// </param>
        /// <returns>
        /// A value returned by the default windows message procedural method.
        /// </returns>
        private static IntPtr WmNcActivate(IntPtr window, IntPtr wordParam, ref bool handled)
        {
            handled = true;
            WindowMessage messageCode = WindowMessage.NCACTIVATE;
            return User32.DefaultWindowProc(window, messageCode, wordParam, new IntPtr(-1));
        }

        /// <summary>
        /// Handles window messages for this window in the default way.
        /// </summary>
        /// <param name="window">
        /// The window handle.
        /// </param>
        /// <param name="msg">
        /// The message ID.
        /// </param>
        /// <param name="wordParam">
        /// The message's additional word parameter value.
        /// </param>
        /// <param name="longParam">
        /// The message's additional long parameter value.
        /// </param>
        /// <returns>
        /// The appropriate return value depends on the particular message.
        /// </returns>
        private IntPtr CallDefWindowProc(
            IntPtr window, WindowMessage msg, IntPtr wordParam, IntPtr longParam)
        {
            bool visible = this.ModifyStyle(WindowStyles.VISIBLE, WindowStyles.NONE);
            IntPtr result = User32.DefaultWindowProc(window, msg, wordParam, longParam);
            if (visible)
            {
                this.ModifyStyle(WindowStyles.NONE, WindowStyles.VISIBLE);
            }

            return result;
        }

        /// <summary>
        /// Determines whether the <see cref="RockstarCommands.ShellExecute"/> command can be
        /// executed based on the current state of the application.
        /// </summary>
        /// <param name="data">
        /// The can execute data that has been sent with the command.
        /// </param>
        /// <returns>
        /// True if the command can be executed; otherwise, false.
        /// </returns>
        private bool CanShellExecute(CanExecuteCommandData<string> data)
        {
            return !String.IsNullOrWhiteSpace(data.Parameter);
        }

        /// <summary>
        /// Called whenever the <see cref="RockstarViewCommands.CloseWindow"/> routed
        /// command is executed on this control.
        /// </summary>
        /// <param name="e">
        /// The command execute data that has been sent with the event.
        /// </param>
        private void CloseWindow(ExecuteCommandData e)
        {
            IntPtr type = new IntPtr((int)SystemCommandType.CLOSE);
            User32.SendMessage(this.Handle, WindowMessage.SYSCOMMAND, type, IntPtr.Zero);
        }

        /// <summary>
        /// Starts a defer glow changing scope.
        /// </summary>
        /// <returns>
        /// The System.IDisposable object that is responsible for maintaining the scope of
        /// changes before a commit is performed.
        /// </returns>
        private IDisposable DeferGlowChanges()
        {
            return new DeferChangeScope(this);
        }

        /// <summary>
        /// Destroys all of the currently cached glow windows.
        /// </summary>
        private void DestroyGlowWindows()
        {
            for (int i = 0; i < this._glowWindows.Length; i++)
            {
                GlowWindow glowWindow = this._glowWindows[i];
                if (glowWindow == null)
                {
                    continue;
                }

                using (glowWindow)
                {
                }

                glowWindow = null;
            }
        }

        /// <summary>
        /// Determines whether the <see cref="RockstarViewCommands.MaximiseWindow"/> routed
        /// command can be executed on this control.
        /// </summary>
        /// <param name="e">
        /// The command can execute data that has been sent with the event.
        /// </param>
        /// <returns>
        /// True if the command can be executed; otherwise, false.
        /// </returns>
        private bool DetermineMaximiseWindow(CanExecuteCommandData e)
        {
            return this.CanMaximiseWindow;
        }

        /// <summary>
        /// Determines whether the <see cref="RockstarViewCommands.MaximiseWindow"/> routed
        /// command can be executed on this control.
        /// </summary>
        /// <param name="e">
        /// The command can execute data that has been sent with the event.
        /// </param>
        /// <returns>
        /// True if the command can be executed; otherwise, false.
        /// </returns>
        private bool DetermineMinimiseWindow(CanExecuteCommandData e)
        {
            return this.CanMinimiseWindow;
        }

        /// <summary>
        /// Commits all of the changes made to the glow windows.
        /// </summary>
        private void EndDeferGlowChanges()
        {
            foreach (GlowWindow current in this.LoadedGlowWindows)
            {
                current.CommitChanges();
            }
        }

        /// <summary>
        /// Gets a cached glow window or creates a new one for the specified orientation.
        /// </summary>
        /// <param name="orientation">
        /// The docking orientation of the glow window to return.
        /// </param>
        /// <returns>
        /// Returns a glow window positioned with the specified orientation.
        /// </returns>
        private GlowWindow GetOrCreateGlowWindow(int orientation)
        {
            if (this._glowWindows[orientation] != null)
            {
                return this._glowWindows[orientation];
            }

            bool canBeUsedToResize = this.ResizeMode == ResizeMode.NoResize ? false : true;
            this._glowWindows[orientation] =
                new GlowWindow(this, (Dock)orientation, canBeUsedToResize);

            WindowInteropHelper windowInteropHelper = new WindowInteropHelper(this);
            this._glowWindows[orientation].ChangeOwner(windowInteropHelper.Owner);
            return this._glowWindows[orientation];
        }

        /// <summary>
        /// Adds the standard control themes and the colour theme files to this instance of the
        /// window so that it appears correct within the editor framework even when the
        /// application object is missing.
        /// </summary>
        private void HandleDisconnectedApplication()
        {
            RsApplication app = Application.Current as RsApplication;
            if (app != null)
            {
                throw new NotSupportedException(
                    "Only add themes to individual windows when no app is present.");
            }

            Collection<ResourceDictionary> resources = this.Resources.MergedDictionaries;
            if (resources == null)
            {
                return;
            }

            string theme = "Themes/theme.standard.xaml";
            string themePath = "pack://application:,,,/RSG.Editor.Controls;component/{0}";
            themePath = themePath.FormatInvariant(theme);
            Uri source = new Uri(themePath, System.UriKind.RelativeOrAbsolute);

            ResourceDictionary newDictionary = new ResourceDictionary();
            try
            {
                newDictionary.Source = source;
            }
            catch (Exception)
            {
                Debug.Assert(newDictionary != null, "Unable to create the standard themes.");
                return;
            }

            resources.Add(newDictionary);

            string path = RsApplication.GetEditorSettingsPath(false);
            ApplicationTheme appTheme = ApplicationTheme.Light;
            if (File.Exists(path))
            {
                try
                {
                    using (XmlReader reader = XmlReader.Create(path))
                    {
                        while (reader.MoveToContent() != XmlNodeType.None)
                        {
                            if (!reader.IsStartElement())
                            {
                                reader.Read();
                                continue;
                            }

                            if (String.CompareOrdinal("Theme", reader.Name) == 0)
                            {
                                reader.Read();
                                if (reader.NodeType == XmlNodeType.Text)
                                {
                                    string value = reader.Value;
                                    if (!Enum.TryParse<ApplicationTheme>(value, out appTheme))
                                    {
                                        appTheme = ApplicationTheme.Light;
                                    }
                                }

                                continue;
                            }

                            reader.Read();
                        }
                    }
                }
                catch (Exception)
                {
                    // Ignore any exceptions that are thrown while parsing the settings file.
                }
            }

            string sourcePath;
            if (!RsApplication.AvailableThemeLookup.TryGetValue(appTheme, out sourcePath))
            {
                Debug.Fail("Unable to find the given theme");
                return;
            }

            Uri appThemeSource = new Uri(sourcePath, System.UriKind.RelativeOrAbsolute);
            ResourceDictionary appThemeDictionary = new ResourceDictionary();
            try
            {
                appThemeDictionary.Source = appThemeSource;
            }
            catch (Exception)
            {
                return;
            }

            resources.Add(appThemeDictionary);
        }

        /// <summary>
        /// Runs a filter on the specified target to make sure it is a
        /// System.Windows.Media.Visual instance.
        /// </summary>
        /// <param name="potentialTarget">
        /// The dependency object that is being filtered.
        /// </param>
        /// <returns>
        /// A <see cref="System.Windows.Media.HitTestFilterBehavior"/> that describes how to
        /// filter the specified potential target.
        /// </returns>
        private HitTestFilterBehavior HitTestFilter(DependencyObject potentialTarget)
        {
            if (!(potentialTarget is Visual))
            {
                return HitTestFilterBehavior.ContinueSkipSelfAndChildren;
            }

            UIElement element = potentialTarget as UIElement;
            if (element == null || element.IsVisible)
            {
                return HitTestFilterBehavior.Continue;
            }

            return HitTestFilterBehavior.ContinueSkipSelfAndChildren;
        }

        /// <summary>
        /// Initiates a hit test on this window for the specified hit point.
        /// </summary>
        /// <param name="callback">
        /// A call-back method that will run for every visual tested.
        /// </param>
        /// <param name="point">
        /// The point to test.
        /// </param>
        private void HitTestVisibleElements(HitTestResultCallback callback, Point point)
        {
            Point screenPoint = this.PointFromScreen(point);
            PointHitTestParameters parameter = new PointHitTestParameters(screenPoint);
            HitTestFilterCallback filter = new HitTestFilterCallback(this.HitTestFilter);
            VisualTreeHelper.HitTest(this, filter, callback, parameter);
        }

        /// <summary>
        /// Called whenever the <see cref="RockstarViewCommands.MaximiseWindow"/> routed
        /// command is executed on this control.
        /// </summary>
        /// <param name="e">
        /// The command execute data that has been sent with the event.
        /// </param>
        private void MaximiseWindow(ExecuteCommandData e)
        {
            IntPtr type = new IntPtr((int)SystemCommandType.MAXIMIZE);
            User32.SendMessage(this.Handle, WindowMessage.SYSCOMMAND, type, IntPtr.Zero);
        }

        /// <summary>
        /// Called whenever the <see cref="RockstarViewCommands.MinimiseWindow"/> routed
        /// command is executed on this control.
        /// </summary>
        /// <param name="e">
        /// The command execute data that has been sent with the event.
        /// </param>
        private void MinimiseWindow(ExecuteCommandData e)
        {
            IntPtr type = new IntPtr((int)SystemCommandType.MINIMIZE);
            User32.SendMessage(this.Handle, WindowMessage.SYSCOMMAND, type, IntPtr.Zero);
        }

        /// <summary>
        /// Modifies the style of this window by remove and adding properties.
        /// </summary>
        /// <param name="styleToRemove">
        /// One or more style fields to remove for the current style.
        /// </param>
        /// <param name="styleToAdd">
        /// One or more style fields to add to the current style.
        /// </param>
        /// <returns>
        /// A value indicating whether or not the style needed changing.
        /// </returns>
        private bool ModifyStyle(WindowStyles styleToRemove, WindowStyles styleToAdd)
        {
            WindowStyles currentStyle = User32.GetWindowStyle(this._handle);
            WindowStyles newStyle = (currentStyle & ~styleToRemove) | styleToAdd;
            if (newStyle == currentStyle)
            {
                return false;
            }

            User32.SetWindowStyle(this._handle, newStyle);
            return true;
        }

        /// <summary>
        /// Modifies the style of this window by remove and adding properties.
        /// </summary>
        /// <param name="styleToRemove">
        /// One or more style fields to remove for the current style.
        /// </param>
        /// <param name="styleToAdd">
        /// One or more style fields to add to the current style.
        /// </param>
        /// <returns>
        /// A value indicating whether or not the style needed changing.
        /// </returns>
        private bool ModifyStyle(WindowExStyles styleToRemove, WindowExStyles styleToAdd)
        {
            WindowExStyles currentStyle = User32.GetWindowExStyle(this._handle);
            WindowExStyles newStyle = (currentStyle & ~styleToRemove) | styleToAdd;
            if (newStyle == currentStyle)
            {
                return false;
            }

            User32.SetWindowExStyle(this._handle, newStyle);
            return true;
        }

        /// <summary>
        /// Gets the information for the monitor showing the majority of the window.
        /// </summary>
        /// <returns>
        /// The monitor information for the monitor that the majority of the window
        /// is shown on.
        /// </returns>
        private MONITORINFO MonitorInfo()
        {
            IntPtr monitor = User32.GetMonitorFromWindow(this._handle);
            return User32.GetMonitorInfo(monitor);
        }

        /// <summary>
        /// Gets called after the glow visibility delay timer has run down and we can show
        /// the glow windows.
        /// </summary>
        /// <param name="sender">
        /// The object that this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.EventArgs for this event.
        /// </param>
        private void OnDelayedVisibilityTimerTick(object sender, EventArgs e)
        {
            this.StopTimer();
            this.UpdateGlowWindowPositions(false);
        }

        /// <summary>
        /// Called whenever the <see cref="RockstarCommands.Exit"/> command is fired.
        /// </summary>
        /// <param name="data">
        /// The execute data that has been sent with the command.
        /// </param>
        private void OnExit(ExecuteCommandData data)
        {
            RsApplication application = Application.Current as RsApplication;
            if (application == null)
            {
                return;
            }

            application.OnExit(data);
        }

        /// <summary>
        /// Called whenever the <see cref="RockstarCommands.ShellExecute"/> command is fired.
        /// </summary>
        /// <param name="data">
        /// The execute data that has been sent with the command.
        /// </param>
        private void OnShellExecute(ExecuteCommandData<string> data)
        {
            Process.Start(new ProcessStartInfo(data.Parameter));
        }

        /// <summary>
        /// Re raises the specified non-client mouse message again as a client message.
        /// WM_NCRBUTTONDOWN => WM_RBUTTONDOWN etc.
        /// </summary>
        /// <param name="msg">
        /// The mouse message.
        /// </param>
        /// <param name="longParam">
        /// The long parameter containing the point the mouse button was pressed.
        /// </param>
        private void ReRaiseMouseMessage(int msg, IntPtr longParam)
        {
            POINT point = new POINT
            {
                X = GetLowWord(longParam.ToInt32()),
                Y = GetHiWord(longParam.ToInt32()),
            };

            msg = msg + 513 - 161;
            User32.ScreenToClient(this._handle, ref point);
            User32.SendMessage(
                this._handle,
                (WindowMessage)msg,
                new IntPtr(PressedMouseButtons),
                MakeParam(point));
        }

        /// <summary>
        /// Called whenever the <see cref="RockstarViewCommands.RestoreWindow"/> routed
        /// command is executed on this control.
        /// </summary>
        /// <param name="e">
        /// The command execute data that has been sent with the event.
        /// </param>
        private void RestoreWindow(ExecuteCommandData e)
        {
            IntPtr type = new IntPtr((int)SystemCommandType.RESTORE);
            User32.SendMessage(this.Handle, WindowMessage.SYSCOMMAND, type, IntPtr.Zero);
        }

        /// <summary>
        /// Called whenever the <see cref="RockstarViewCommands.ShowHelpFromTitleBar"/> routed
        /// command is executed on this control.
        /// </summary>
        /// <param name="e">
        /// The command execute data that has been sent with the event.
        /// </param>
        private void ShowHelpFromTitleBar(ExecuteCommandData e)
        {
            IntPtr type = new IntPtr((int)SystemCommandType.CONTEXTHELP);
            User32.SendMessage(this.Handle, WindowMessage.SYSCOMMAND, type, IntPtr.Zero);
        }

         /// <summary>
        /// Called whenever the <see cref="RockstarViewCommands.ShowOptionsFromTitleBar"/>
        /// routed command is executed on this control.
        /// </summary>
        /// <param name="e">
        /// The command execute data that has been sent with the event.
        /// </param>
        private void ShowOptionsFromTitleBar(ExecuteCommandData e)
        {
            this.ShowOptionsFromTitleBar();
        }

        /// <summary>
        /// Stops the timer responsible for making the glow windows visible after a
        /// delayed period of time.
        /// </summary>
        private void StopTimer()
        {
            if (this._makeGlowVisibleTimer == null)
            {
                return;
            }

            this._makeGlowVisibleTimer.Stop();
            this._makeGlowVisibleTimer.Tick -= this.OnDelayedVisibilityTimerTick;
            this._makeGlowVisibleTimer = null;
        }

        /// <summary>
        /// Updates the clip region for the window.
        /// </summary>
        /// <param name="regionChangeType">
        /// The type of change that occurred that resulted in the clip region needing updating.
        /// </param>
        private void UpdateClipRegion(RegionChangeType regionChangeType)
        {
            if (this._handle == IntPtr.Zero)
            {
                return;
            }

            RECT bounds = User32.GetWindowNativeRect(this._handle);
            this.UpdateClipRegion(regionChangeType, bounds);
        }

        /// <summary>
        /// Updates the clip region for the window.
        /// </summary>
        /// <param name="changeType">
        /// The type of change that occurred that resulted in the clip region needing updating.
        /// </param>
        /// <param name="bounds">
        /// The current bounds of the window position.
        /// </param>
        private void UpdateClipRegion(RegionChangeType changeType, RECT bounds)
        {
            if (this._handle == IntPtr.Zero)
            {
                return;
            }

            WINDOWPLACEMENT placement = User32.GetWindowPlacement(this._handle);
            WindowShowState showCmd = placement.ShowCmd;

            this.UpdateClipRegion(showCmd, changeType, bounds);
            this._lastWindowShowState = showCmd;
        }

        /// <summary>
        /// Updates the clip region for the window.
        /// </summary>
        /// <param name="showCmd">
        /// The current show state of the window.
        /// </param>
        /// <param name="changeType">
        /// The type of change that occurred that resulted in the clip region needing updating.
        /// </param>
        /// <param name="bounds">
        /// The current bounds of the window position.
        /// </param>
        private void UpdateClipRegion(
            WindowShowState showCmd, RegionChangeType changeType, RECT bounds)
        {
            if (this._handle == IntPtr.Zero)
            {
                return;
            }

            if (showCmd == WindowShowState.MAXIMIZE)
            {
                this.UpdateMaximisedClipRegion();
                return;
            }

            bool validChange = false;
            if (changeType == RegionChangeType.FromSize)
            {
                validChange = true;
            }
            else if (changeType == RegionChangeType.FromPropertyChange)
            {
                validChange = true;
            }
            else if (this._lastWindowShowState != showCmd)
            {
                validChange = true;
            }

            if (!validChange)
            {
                return;
            }

            Int32Rect integerBounds = bounds.ToInt32Rect();
            IntPtr rgn = CreateRegion(integerBounds.Width, integerBounds.Height);
            bool redraw = User32.IsWindowVisible(this._handle);
            int setWindowRgnResult = User32.SetWindowRgn(this._handle, rgn, redraw);
            Debug.Assert(setWindowRgnResult != 0, "Failed to set the region");
        }

        /// <summary>
        /// Updates the Z order for this window, its glow windows and the windows owner.
        /// </summary>
        private void UpdateDepthOrderOfThisAndOwner()
        {
            WindowInteropHelper windowInteropHelper = new WindowInteropHelper(this);
            IntPtr handle = this._handle;
            foreach (GlowWindow current in this.LoadedGlowWindows)
            {
                IntPtr window = User32.GetWindow(current.Handle, GetWindowFlag.HWNDPREV);
                if (window != handle)
                {
                    User32.SetWindowPos(current.Handle, handle, WindowPosFlags.NOACTION);
                }

                handle = current.Handle;
            }

            IntPtr owner = windowInteropHelper.Owner;
            if (owner != IntPtr.Zero)
            {
                UpdateDepthOrderOfOwner(owner);
            }
        }

        /// <summary>
        /// Updates all of the currently loaded glow windows active states.
        /// </summary>
        private void UpdateGlowActiveState()
        {
            using (this.DeferGlowChanges())
            {
                foreach (GlowWindow current in this.LoadedGlowWindows)
                {
                    current.IsActive = this.IsActive;
                }
            }
        }

        /// <summary>
        /// Updates the colour of the glow window, both the active colour and inactive colour.
        /// </summary>
        private void UpdateGlowColors()
        {
            using (this.DeferGlowChanges())
            {
                foreach (GlowWindow current in this.LoadedGlowWindows)
                {
                    current.ActiveGlowColor = this.ActiveGlowColour;
                    current.InactiveGlowColor = this.InactiveGlowColour;
                }
            }
        }

        /// <summary>
        /// Updates the visibility of the glow windows with either a delay or straight away.
        /// </summary>
        /// <param name="delayIfNecessary">
        /// If true specifies that a delay is required before the glow windows are visible.
        /// </param>
        private void UpdateGlowVisibility(bool delayIfNecessary)
        {
            bool shouldShowGlow = this.ShouldShowGlow;
            if (shouldShowGlow == this.IsGlowVisible)
            {
                return;
            }

            if (SystemParameters.MinimizeAnimation && shouldShowGlow && delayIfNecessary)
            {
                if (this._makeGlowVisibleTimer != null)
                {
                    this._makeGlowVisibleTimer.Stop();
                }
                else
                {
                    this._makeGlowVisibleTimer = new DispatcherTimer();
                    this._makeGlowVisibleTimer.Interval = TimeSpan.FromMilliseconds(200.0);
                    this._makeGlowVisibleTimer.Tick += this.OnDelayedVisibilityTimerTick;
                }

                this._makeGlowVisibleTimer.Start();
                return;
            }

            this.StopTimer();
            this.IsGlowVisible = shouldShowGlow;
        }

        /// <summary>
        /// Updates the position of all the glow window based on the position of this window.
        /// </summary>
        /// <param name="delayIfNecessary">
        /// If true specifies that a delay is required before the glow windows are moved.
        /// </param>
        private void UpdateGlowWindowPositions(bool delayIfNecessary)
        {
            using (this.DeferGlowChanges())
            {
                this.UpdateGlowVisibility(delayIfNecessary);
                foreach (GlowWindow current in this.LoadedGlowWindows)
                {
                    current.UpdateWindowPos();
                }
            }
        }

        /// <summary>
        /// Updates the clip region for this window based on the fact that it is current
        /// being shown as a maximised window.
        /// </summary>
        private void UpdateMaximisedClipRegion()
        {
            Rect windowRect = User32.GetWindowRect(this._handle);
            Rect clientRect = User32.GetClientRect(this._handle);
            POINT screen = User32.ClientToScreen(this._handle, new POINT(0, 0));

            clientRect.Offset(screen.X - windowRect.Left, screen.Y - windowRect.Top);
            RECT rect = new RECT(clientRect);
            if (this._isNonClientStripVisible)
            {
                rect.Bottom++;
            }

            bool redraw = User32.IsWindowVisible(this._handle);
            IntPtr region = Gdi32.CreateRectRgnIndirect(ref rect);
            int setWindowRgnResult = User32.SetWindowRgn(this._handle, region, redraw);
            Debug.Assert(setWindowRgnResult != 0, "Failed to set the region");
        }

        /// <summary>
        /// Gets called when this window receives the WM_ACTIVATE message.
        /// </summary>
        /// <param name="wordParam">
        /// The word additional message information.
        /// </param>
        /// <param name="longParam">
        /// The long additional message information.
        /// </param>
        private void WmActivate(IntPtr wordParam, IntPtr longParam)
        {
            WindowInteropHelper windowInteropHelper = new WindowInteropHelper(this);
            if (windowInteropHelper.Owner == IntPtr.Zero)
            {
                return;
            }

            IntPtr owner = windowInteropHelper.Owner;
            User32.SendMessage(owner, NotifyOwnerActivateCode, wordParam, longParam);
        }

        /// <summary>
        /// Gets called when this window receives the WM_NCCALCSIZE message.
        /// </summary>
        /// <param name="window">
        /// A handle to the window.
        /// </param>
        /// <param name="wordParam">
        /// The word additional message information.
        /// </param>
        /// <param name="longParam">
        /// The long additional message information.
        /// </param>
        private void WmNcCalcSize(IntPtr window, IntPtr wordParam, IntPtr longParam)
        {
            this._isNonClientStripVisible = false;
            WINDOWPLACEMENT windowPlacement = User32.GetWindowPlacement(window);
            bool flag = windowPlacement.ShowCmd == WindowShowState.SHOWMAXIMIZED;
            if (!flag)
            {
                return;
            }

            RECT rect = (RECT)Marshal.PtrToStructure(longParam, typeof(RECT));
            User32.DefaultWindowProc(window, WindowMessage.NCCALCSIZE, wordParam, longParam);
            RECT rect2 = (RECT)Marshal.PtrToStructure(longParam, typeof(RECT));
            IntPtr monitor = User32.GetMonitorFromWindow(window);
            MONITORINFO monitorInfo = User32.GetMonitorInfo(monitor);
            if (monitorInfo.Monitor.Height == monitorInfo.Work.Height)
            {
                if (monitorInfo.Monitor.Width == monitorInfo.Work.Width)
                {
                    this._isNonClientStripVisible = true;
                    rect2.Bottom--;
                }
            }

            rect2.Top = rect.Top + (int)User32.GetWindowInfo(window).BorderHeight;
            Marshal.StructureToPtr(rect2, longParam, true);
        }

        /// <summary>
        /// Handles the WM_NCHITTEST window message.
        /// </summary>
        /// <param name="pointParameter">
        /// The addition parameter containing the x- y- coordinates of the test point.
        /// </param>
        /// <param name="handled">
        /// A value that indicates whether this method has handled the message.
        /// </param>
        /// <returns>
        /// A value indicating the system hot spot that has been hit by the test point.
        /// </returns>
        private IntPtr WmNcHitTest(IntPtr pointParameter, ref bool handled)
        {
            if (PresentationSource.FromDependencyObject(this) == null)
            {
                return new IntPtr(0);
            }

            double x = (double)GetLowWord(pointParameter.ToInt32());
            double y = (double)GetHiWord(pointParameter.ToInt32());
            Point point = new Point(x, y);
            DependencyObject visualHit = null;

            this.HitTestVisibleElements(
                delegate(HitTestResult target)
                {
                    visualHit = target.VisualHit;
                    return HitTestResultBehavior.Stop;
                },
                point);

            NcHitTestResult hitResult = NcHitTestResult.NOWHERE;
            while (visualHit != null)
            {
                INonClientArea nonClientArea = visualHit as INonClientArea;
                if (nonClientArea != null)
                {
                    hitResult = nonClientArea.HitTest(point);
                    if (hitResult != NcHitTestResult.NOWHERE)
                    {
                        break;
                    }
                }

                visualHit = visualHit.GetVisualOrLogicalAncestor();
            }

            if (hitResult == NcHitTestResult.NOWHERE)
            {
                hitResult = NcHitTestResult.CLIENT;
            }

            handled = true;
            return new IntPtr((int)hitResult);
        }

        /// <summary>
        /// Gets called when this window receives the WM_NCPAINT message.
        /// </summary>
        /// <param name="window">
        /// A handle to the window.
        /// </param>
        /// <param name="wordParam">
        /// The word additional message information.
        /// </param>
        private void WmNcPaint(IntPtr window, IntPtr wordParam)
        {
            if (!this._isNonClientStripVisible)
            {
                return;
            }

            IntPtr hrgnClip = (wordParam == new IntPtr(1)) ? IntPtr.Zero : wordParam;

            DeviceCreationFlags flags = DeviceCreationFlags.WINDOW;
            flags |= DeviceCreationFlags.CACHE;
            flags |= DeviceCreationFlags.CLIPCHILDREN;
            flags |= DeviceCreationFlags.CLIPSIBLINGS;
            flags |= DeviceCreationFlags.INTERSECTRGN;
            IntPtr dc = User32.GetDCEx(window, hrgnClip, flags);
            if (dc != IntPtr.Zero)
            {
                try
                {
                    Color nonClientFillColor = this.NonClientFillColour;
                    int colour = (int)nonClientFillColor.B << 16;
                    colour |= (int)nonClientFillColor.G << 8;
                    colour |= (int)nonClientFillColor.R;
                    IntPtr intPtr = Gdi32.CreateSolidBrush(colour);
                    try
                    {
                        Rect rect = User32.GetWindowRect(window);
                        User32.FillRect(dc, ref rect, intPtr);
                    }
                    finally
                    {
                        Gdi32.DeleteObject(intPtr);
                    }
                }
                finally
                {
                    User32.ReleaseDC(window, dc);
                }
            }
        }

        /// <summary>
        /// Gets called when this window receives the WM_WINDOWPOSCHANGED message.
        /// </summary>
        /// <param name="newPosition">
        /// The additional parameter data from the window message that represents the new
        /// position for the window.
        /// </param>
        private void WmWindowPosChanged(IntPtr newPosition)
        {
            object marshalled = Marshal.PtrToStructure(newPosition, typeof(WINDOWPOS));
            if (!(marshalled is WINDOWPOS))
            {
                return;
            }

            WINDOWPOS pos = (WINDOWPOS)marshalled;
            RegionChangeType changeType = RegionChangeType.None;
            if ((pos.Flags & WindowPosFlags.NOSIZE) != WindowPosFlags.NOSIZE)
            {
                changeType = RegionChangeType.FromSize;
            }
            else if ((pos.Flags & WindowPosFlags.NOMOVE) != WindowPosFlags.NOMOVE)
            {
                changeType = RegionChangeType.FromPosition;
            }

            if (changeType != RegionChangeType.None)
            {
                RECT bounds = new RECT(pos);
                this.UpdateClipRegion(changeType, bounds);
            }

            bool delayGlow = (pos.Flags & WindowPosFlags.SHOWWINDOW) == 0;
            this.UpdateGlowWindowPositions(delayGlow);
            this.UpdateDepthOrderOfThisAndOwner();
        }

        /// <summary>
        /// Gets called when this window receives the WM_WINDOWPOSCHANGING message.
        /// </summary>
        /// <param name="newPosition">
        /// The additional parameter data from the window message that represents the new
        /// position for the window.
        /// </param>
        private void WmWindowPosChanging(IntPtr newPosition)
        {
            object marshalled = Marshal.PtrToStructure(newPosition, typeof(WINDOWPOS));
            if (!(marshalled is WINDOWPOS))
            {
                return;
            }

            WINDOWPOS pos = (WINDOWPOS)marshalled;
            if ((pos.Flags & WindowPosFlags.SHOWWINDOW) == WindowPosFlags.SHOWWINDOW)
            {
                if (this.DontDelayGlow)
                {
                    this.IsGlowVisible = true;
                }
            }

            if ((pos.Flags & WindowPosFlags.NOSIZE) != 0)
            {
                return;
            }

            if ((pos.Flags & WindowPosFlags.NOMOVE) != 0)
            {
                return;
            }

            if (pos.Width <= 0 || pos.Height <= 0)
            {
                return;
            }

            Rect rect = new Rect(pos.Left, pos.Top, pos.Width, pos.Height);
            rect = rect.DeviceToLogicalUnits();

            Screen screen = new Screen(rect, User32.GetCursorPos());
            Rect logicalRect = screen.OnScreenPosition;

            logicalRect = logicalRect.LogicalToDeviceUnits();
            pos.Left = (int)logicalRect.X;
            pos.Top = (int)logicalRect.Y;
            Marshal.StructureToPtr(pos, newPosition, true);
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// Defines a change scope that uses the IDisposable pattern to batch changes to the
        /// glow windows together before committing them.
        /// </summary>
        private class DeferChangeScope : DisposableObject
        {
            #region Fields
            /// <summary>
            /// The private reference to the window that is associated with this change scope.
            /// </summary>
            private readonly RsWindow _window;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="DeferChangeScope"/> class.
            /// </summary>
            /// <param name="window">
            /// The <see cref="RsWindow"/> that is associated with this change scope.
            /// </param>
            public DeferChangeScope(RsWindow window)
            {
                this._window = window;
                this._window.DeferGlowChangesCount++;
            }
            #endregion Constructors

            #region Methods
            /// <summary>
            /// Disposes of the managed resources and if the scope reference count is 0 commits
            /// the changes to the glow windows.
            /// </summary>
            protected override void DisposeManagedResources()
            {
                this._window.DeferGlowChangesCount--;
                if (this._window.DeferGlowChangesCount == 0)
                {
                    this._window.EndDeferGlowChanges();
                }
            }
            #endregion Methods
        } // RSG.Editor.Controls.DeferChangeScope {Class}
        #endregion Classes
    } // RSG.Editor.Controls.RsWindow {Class}
} // RSG.Editor.Controls {Namespace}
