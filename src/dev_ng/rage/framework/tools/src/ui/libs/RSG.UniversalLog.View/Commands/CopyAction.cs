﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using RSG.Editor;
using RSG.UniversalLog.ViewModel;

namespace RSG.UniversalLog.View.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public class CopyAction : ButtonAction<IEnumerable<UniversalLogMessageViewModel>>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="RefreshImplementer"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public CopyAction(ParameterResolverDelegate<IEnumerable<UniversalLogMessageViewModel>> resolver)
            : base(resolver)
        {
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// Override to set logic against the can execute command handler.
        /// </summary>
        /// <param name="commandParameter"></param>
        /// <returns></returns>
        public override bool CanExecute(IEnumerable<UniversalLogMessageViewModel> messages)
        {
            return messages.Any();
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="commandParameter">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(IEnumerable<UniversalLogMessageViewModel> messages)
        {
            Clipboard.SetText(String.Join("\n", messages.Select(item => item.CopyText)));
        }
        #endregion // Overrides
    } // CopyAction
}
