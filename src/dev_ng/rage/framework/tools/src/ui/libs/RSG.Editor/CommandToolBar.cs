﻿//---------------------------------------------------------------------------------------------
// <copyright file="CommandToolBar.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;

    /// <summary>
    /// Creates a object inside the command hierarchy that represents a single toolbar that
    /// shows a subset of child command bar items.
    /// </summary>
    internal class CommandToolBar : CommandBarItem
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Band"/> property.
        /// </summary>
        private int _band;

        /// <summary>
        /// The private field used for the <see cref="BandIndex"/> property.
        /// </summary>
        private int _bandIndex;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CommandToolBar"/> class.
        /// </summary>
        /// <param name="band">
        /// Where the toolbar should be located in the ToolBarTray.
        /// </param>
        /// <param name="bandIndex">
        /// The position of the toolbar on the band.
        /// </param>
        /// <param name="text">
        /// The text for this command bar item.
        /// </param>
        /// <param name="id">
        /// The unique identifier that is used to reference this item.
        /// </param>
        public CommandToolBar(int band, int bandIndex, string text, Guid id)
            : base(0, false, text, id, RockstarCommandManager.ToolBarTrayId)
        {
            this._band = band;
            this._bandIndex = bandIndex;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets a value that indicates where the toolbar should be located in the
        /// ToolBarTray.
        /// </summary>
        public int Band
        {
            get { return this._band; }
            set { this.SetProperty(ref this._band, value); }
        }

        /// <summary>
        /// Gets or sets the band index number that indicates the position of the toolbar on
        /// the band.
        /// </summary>
        public int BandIndex
        {
            get { return this._bandIndex; }
            set { this.SetProperty(ref this._bandIndex, value); }
        }
        #endregion Properties
    }  // RSG.Editor.CommandToolBar {Class}
} // RSG.Editor {Namespace}
