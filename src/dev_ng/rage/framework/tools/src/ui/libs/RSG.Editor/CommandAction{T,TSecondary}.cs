﻿//---------------------------------------------------------------------------------------------
// <copyright file="CommandAction{T,TSecondary}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Windows;

    /// <summary>
    /// Provides a abstract base class to any class that is used to implement a command.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the parameter passed into the can execute and execute methods.
    /// </typeparam>
    /// <typeparam name="TSecondary">
    /// The type of the secondary parameter passed into the can execute and execute methods
    /// that has been send with the command.
    /// </typeparam>
    public abstract class CommandAction<T, TSecondary> : ICommandAction
    {
        #region Fields
        /// <summary>
        /// The private resolver that this class uses to obtain the command parameter from.
        /// </summary>
        private ParameterResolverDelegate<T> _resolver;

        /// <summary>
        /// The private field used for the <see cref="ServiceProvider"/> property.
        /// </summary>
        private ICommandServiceProvider _serviceProvider;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CommandAction{T,TSecondary}"/> class.
        /// </summary>
        /// <param name="serviceProvider">
        /// The service provider that is used by this action to perform application specific
        /// operations, for example selecting a file to open.
        /// </param>
        protected CommandAction(ICommandServiceProvider serviceProvider)
        {
            this._serviceProvider = serviceProvider;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="CommandAction{T,TSecondary}"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        protected CommandAction(ParameterResolverDelegate<T> resolver)
        {
            this._resolver = resolver;
            this._serviceProvider = Application.Current as ICommandServiceProvider;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Sets the service provider that can be used in the Execute methods.
        /// </summary>
        public ICommandServiceProvider ServiceProvider
        {
            get
            {
                return this._serviceProvider;
            }

            set
            {
                if (value == null)
                {
                    throw new SmartArgumentNullException(() => value);
                }

                this._serviceProvider = value;
            }
        }

        /// <summary>
        /// Gets the command definition that has been created for this implementer for the
        /// specified routed command.
        /// </summary>
        /// <param name="command">
        /// The command whose definition should be returned.
        /// </param>
        /// <returns>
        /// The command definition that has been created for this implementer for the specified
        /// routed command.
        /// </returns>
        public CommandDefinition this[RockstarRoutedCommand command]
        {
            get
            {
                CommandDefinition definition = RockstarCommandManager.GetDefinition(command);
                if (definition == null)
                {
                    definition = this.CreateDefinition(command);
                    if (definition != null)
                    {
                        RockstarCommandManager.AddCommandDefinition(definition);
                    }
                }

                return definition;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Binds this implementation to the specified command and the specified user interface
        /// element.
        /// </summary>
        /// <param name="command">
        /// The command to bind to this implementation.
        /// </param>
        /// <param name="element">
        /// The element that this implementation will be bound to.
        /// </param>
        public virtual void AddBinding(RockstarRoutedCommand command, UIElement element)
        {
            if (element == null)
            {
                throw new SmartArgumentNullException(() => element);
            }

            if (command == null)
            {
                throw new SmartArgumentNullException(() => command);
            }

            CommandDefinition definition = RockstarCommandManager.GetDefinition(command);
            if (definition == null)
            {
                definition = this.CreateDefinition(command);
                if (definition != null)
                {
                    RockstarCommandManager.AddCommandDefinition(definition);
                }
            }

            RockstarCommandManager.AddBinding<TSecondary>(
                element, command, this.Execute, this.CanExecute);
        }

        /// <summary>
        /// Binds this implementation to the specified command for the specified type.
        /// </summary>
        /// <param name="command">
        /// The command to bind to this implementation.
        /// </param>
        /// <param name="type">
        /// The class with which to register the command binding to.
        /// </param>
        public virtual void AddBinding(RockstarRoutedCommand command, Type type)
        {
            if (type == null)
            {
                throw new SmartArgumentNullException(() => type);
            }

            if (command == null)
            {
                throw new SmartArgumentNullException(() => command);
            }

            CommandDefinition definition = RockstarCommandManager.GetDefinition(command);
            if (definition == null)
            {
                definition = this.CreateDefinition(command);
                if (definition != null)
                {
                    RockstarCommandManager.AddCommandDefinition(definition);
                }
            }

            RockstarCommandManager.AddBinding<TSecondary>(
                type, command, this.Execute, this.CanExecute);
        }

        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="commandParameter">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="secondaryParameter">
        /// The secondary command parameter that has been sent with the command.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public virtual bool CanExecute(T commandParameter, TSecondary secondaryParameter)
        {
            return true;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="commandParameter">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="secondaryParameter">
        /// The secondary command parameter that has been sent with the command.
        /// </param>
        public abstract void Execute(T commandParameter, TSecondary secondaryParameter);

        /// <summary>
        /// Create a new command definition for each command associated with this
        /// implementation.
        /// </summary>
        /// <param name="command">
        /// The command to create the definition for.
        /// </param>
        /// <returns>
        /// The new command definition created for the specified command.
        /// </returns>
        protected abstract CommandDefinition CreateDefinition(RockstarRoutedCommand command);

        /// <summary>
        /// Gets the service object of the specified type.
        /// </summary>
        /// <typeparam name="TService">
        /// The type of service object to get.
        /// </typeparam>
        /// <returns>
        /// A service object of the specified type or null if there is no service object of
        /// that type.
        /// </returns>
        protected TService GetService<TService>() where TService : class
        {
            TService service = null;
            if (this._serviceProvider != null)
            {
                service = this._serviceProvider.GetService<TService>();
            }

            if (service != null)
            {
                return service;
            }

            throw new ExecuteCommandException(
                "Unable to perform command as service is missing");
        }

        /// <summary>
        /// Determines whether the associated command can be executed based on the current
        /// state of the application.
        /// </summary>
        /// <param name="data">
        /// The can execute data that has been sent with the command.
        /// </param>
        /// <returns>
        /// True if the command can be executed; otherwise, false.
        /// </returns>
        private bool CanExecute(CanExecuteCommandData<TSecondary> data)
        {
            T commandParameter = default(T);
            if (this._resolver != null)
            {
                commandParameter = this._resolver(data);
            }

            return this.CanExecute(commandParameter, data.Parameter);
        }

        /// <summary>
        /// Called whenever the associated command is fired and needs handling at this instance
        /// level.
        /// </summary>
        /// <param name="data">
        /// The execute data that has been sent with the command.
        /// </param>
        private void Execute(ExecuteCommandData<TSecondary> data)
        {
            T commandParameter = default(T);
            if (this._resolver != null)
            {
                commandParameter = this._resolver(data);
            }

            try
            {
                this.Execute(commandParameter, data.Parameter);
            }
            catch (OperationCanceledException)
            {
                //// The operation has been cancelled by the user so we can just ignore this.
            }
        }
        #endregion Methods
    } // RSG.Editor.CommandAction{T,TSecondary} {Class}
} // RSG.Editor {Namespace}
