﻿//---------------------------------------------------------------------------------------------
// <copyright file="RegisteredSearchTerm.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Search
{
    using System;
    using RSG.Base.Extensions;

    /// <summary>
    /// Represents a registered search term that can be searched for through a external model.
    /// </summary>
    public class RegisteredSearchTerm
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private string _name;

        /// <summary>
        /// The private field used for the <see cref="Type"/> property.
        /// </summary>
        private SearchFieldType _type;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="RegisteredSearchTerm"/> class.
        /// </summary>
        /// <param name="name">
        /// The name for this search term.
        /// </param>
        /// <param name="type">
        /// The data type this search term is associated with.
        /// </param>
        public RegisteredSearchTerm(string name, SearchFieldType type)
        {
            this._name = name;
            this._type = type;
            switch (type)
            {
                case SearchFieldType.Boolean:
                case SearchFieldType.Numeric:
                case SearchFieldType.String:
                    return;

                default:
                    string msg = "The 'SearchFieldType.{0}' type isn't supported";
                    throw new NotSupportedException(msg.FormatInvariant(type.ToString()));
            }
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the name of this search term.
        /// </summary>
        public string Name
        {
            get { return this._name; }
        }

        /// <summary>
        /// Gets the data type that this search term is associated with.
        /// </summary>
        public SearchFieldType Type
        {
            get { return this._type; }
        }
        #endregion Properties
    } // RSG.Editor.Controls.Search.RegisteredSearchTerm {Class}
} // RSG.Editor.Controls.Search {Namespace}
