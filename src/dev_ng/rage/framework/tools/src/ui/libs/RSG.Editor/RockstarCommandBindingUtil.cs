﻿//---------------------------------------------------------------------------------------------
// <copyright file="RockstarCommandBindingUtil.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Input;

    /// <summary>
    /// Provides a utility class that can be used be the command binding objects to support
    /// key gestures.
    /// </summary>
    internal static class RockstarCommandBindingUtil
    {
        #region Fields
        /// <summary>
        /// The private reference to the keyboard helper class that is used to determine the
        /// last pressed key.
        /// </summary>
        private static KeyboardHelper _keyboardHelper;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RockstarCommandBindingUtil"/> class.
        /// </summary>
        static RockstarCommandBindingUtil()
        {
            _keyboardHelper = new KeyboardHelper();
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Initialises the static properties within this static class.
        /// </summary>
        public static void Initialise()
        {
        }

        /// <summary>
        /// Determines whether the specified routed event arguments have been created due to a
        /// key gesture being pressed and returns that key gesture.
        /// </summary>
        /// <param name="e">
        /// The routed event arguments used to determine if there is a key gesture associated
        /// with it.
        /// </param>
        /// <returns>
        /// The key gesture that was pressed to created the specified event arguments.
        /// </returns>
        public static KeyGesture GetExecutedKeyGesture(ExecutedRoutedEventArgs e)
        {
            KeyGesture gesture =
                RockstarCommandBindingUtil.GetExecutedKeyGesture(e.Command, e.Parameter);
            _keyboardHelper.ResetKey();
            return gesture;
        }

        /// <summary>
        /// Determines whether the specified routed event arguments have been created due to a
        /// key gesture being pressed and returns that key gesture.
        /// </summary>
        /// <param name="e">
        /// The routed event arguments used to determine if there is a key gesture associated
        /// with it.
        /// </param>
        /// <returns>
        /// The key gesture that was pressed to create the specified event arguments.
        /// </returns>
        public static KeyGesture GetExecutedKeyGesture(CanExecuteRoutedEventArgs e)
        {
            KeyGesture gesture =
                RockstarCommandBindingUtil.GetExecutedKeyGesture(e.Command, e.Parameter);
            return gesture;
        }

        /// <summary>
        /// Handles the aftermath of a command executing using a keyboard gesture.
        /// </summary>
        /// <param name="definition">
        /// The definition of the command that was fired that was fired.
        /// </param>
        /// <param name="gesture">
        /// The gesture that was used to execute this command.
        /// </param>
        /// <param name="parameter">
        /// The parameter that was sent with the command.
        /// </param>
        public static void HandleExection(
            CommandDefinition definition, KeyGesture gesture, object parameter)
        {
            if (definition == null)
            {
                return;
            }

            ToggleButtonCommand toggleDefinition = definition as ToggleButtonCommand;
            if (toggleDefinition != null)
            {
                bool? newValue = null;
                if (parameter is bool)
                {
                    newValue = parameter as bool?;
                }
                else
                {
                    Tuple<bool, object> tupleParameter = parameter as Tuple<bool, object>;
                    if (tupleParameter != null)
                    {
                        newValue = tupleParameter.Item1;
                    }
                }

                if (newValue == null)
                {
                    return;
                }

                toggleDefinition.IsToggled = (bool)newValue;
                return;
            }

            MultiCommand multiItemCommand = definition as MultiCommand;
            if (multiItemCommand != null)
            {
                KeyGestureComparer comparer = new KeyGestureComparer();
                foreach (IMultiCommandItem item in multiItemCommand.Items)
                {
                    if (gesture != null)
                    {
                        if (!comparer.Equals(item.KeyGesture, gesture))
                        {
                            continue;
                        }
                    }
                    else
                    {
                        if (!object.Equals(item.GetCommandParameter(), parameter))
                        {
                            continue;
                        }
                    }

                    if (!multiItemCommand.CanItemsBeSelected)
                    {
                        continue;
                    }

                    if (multiItemCommand.SelectionMode == MultiCommandSelectionMode.Multiple)
                    {
                        item.IsToggled = !item.IsToggled;
                        continue;
                    }

                    multiItemCommand.SelectedItem = item;
                }

                return;
            }
        }

        /// <summary>
        /// Determines whether the specified routed event arguments have been created due to a
        /// key gesture being pressed and returns that key gesture.
        /// </summary>
        /// <param name="command">
        /// The command that was past into the event handler as data.
        /// </param>
        /// <param name="parameter">
        /// The parameter that was past into the event handler as data.
        /// </param>
        /// <returns>
        /// The key gesture that was pressed to create the specified event arguments.
        /// </returns>
        private static KeyGesture GetExecutedKeyGesture(ICommand command, object parameter)
        {
            if (parameter != null)
            {
                return null;
            }

            RoutedCommand routedCommand = command as RoutedCommand;
            InputGestureCollection gestureCollection = null;
            if (routedCommand != null)
            {
                gestureCollection = routedCommand.InputGestures;
            }

            if (gestureCollection != null && Keyboard.PrimaryDevice.ActiveSource != null)
            {
                KeyEventArgs keyArgs = new KeyEventArgs(
                    Keyboard.PrimaryDevice,
                    Keyboard.PrimaryDevice.ActiveSource,
                    0,
                    _keyboardHelper.PreviousKeyDownEventKey);

                foreach (InputGesture inputGesture in gestureCollection)
                {
                    if (inputGesture.Matches(null, keyArgs))
                    {
                        return inputGesture as KeyGesture;
                    }
                }
            }

            return null;
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// A helper class that is used to capture the last key down event the keyboard fires.
        /// </summary>
        private class KeyboardHelper
        {
            #region Fields
            /// <summary>
            /// The private field used for the <see cref="PreviousKeyDownEventKey"/> property.
            /// </summary>
            private Key _previousKeyDownEventKey;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="KeyboardHelper"/> class.
            /// </summary>
            public KeyboardHelper()
            {
                InputManager.Current.PostProcessInput += this.OnPostProcessInput;
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets the key that was last past into the key down keyboard event.
            /// </summary>
            public Key PreviousKeyDownEventKey
            {
                get { return this._previousKeyDownEventKey; }
            }
            #endregion Properties

            #region Methods
            /// <summary>
            /// Resets the <see cref="PreviousKeyDownEventKey"/> property back to the Key.None
            /// value.
            /// </summary>
            public void ResetKey()
            {
                this._previousKeyDownEventKey = Key.None;
            }

            /// <summary>
            /// Handles the post process input event on the current input manager. This is used
            /// to pull out the key event args for the next keyboard event.
            /// </summary>
            /// <param name="sender">
            /// The object that this handler is attached to.
            /// </param>
            /// <param name="e">
            /// The event data.
            /// </param>
            private void OnPostProcessInput(object sender, ProcessInputEventArgs e)
            {
                if (e.StagingItem.Input.RoutedEvent != Keyboard.PreviewKeyDownEvent)
                {
                    return;
                }

                KeyEventArgs keyEventArgs = e.StagingItem.Input as KeyEventArgs;
                if (keyEventArgs == null)
                {
                    return;
                }

                this._previousKeyDownEventKey = keyEventArgs.Key;
            }
            #endregion Methods
        } // RSG.Editor.RockstarCommandBindingUtil.KeyboardHelper {Class}
        #endregion Classes
    } // RSG.Editor.RockstarCommandBindingUtil {Class}
} // RSG.Editor {Namespace}
