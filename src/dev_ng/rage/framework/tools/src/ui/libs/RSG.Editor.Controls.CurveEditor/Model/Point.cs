﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using RSG.Base.Math;
using RSG.Editor.Model;

namespace RSG.Editor.Controls.CurveEditor.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class Point : ModelBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public double PositionX
        {
            get { return _positionX; }
            set { SetProperty(ref _positionX, value); }
        }
        private double _positionX;

        /// <summary>
        /// 
        /// </summary>
        public double PositionY
        {
            get { return _positionY; }
            set { SetProperty(ref _positionY, value); }
        }
        private double _positionY;

        /// <summary>
        /// 
        /// </summary>
        public Tangent InTangent
        {
            get { return _inTangent; }
            set { SetProperty(ref _inTangent, value); }
        }
        private Tangent _inTangent;

        /// <summary>
        /// 
        /// </summary>
        public Tangent OutTangent
        {
            get { return _outTangent; }
            set { SetProperty(ref _outTangent, value); }
        }
        private Tangent _outTangent;

        /// <summary>
        /// 
        /// </summary>
        public bool TangentsUnified
        {
            get { return _tangentsUnified; }
            set { SetProperty(ref _tangentsUnified, value); }
        }
        private bool _tangentsUnified;
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlElem"></param>
        public Point(Curve parent, XElement xmlElem)
            : base(parent)
        {
            // Parse the key's position.
            XElement xmlPositionElem = xmlElem.Element("Position");
            Debug.Assert(xmlPositionElem != null, "Point XElement is missing the Position element.");
            if (xmlPositionElem != null)
            {
                //Position = new Vector2d(xmlPositionElem);

                XAttribute xAtt = xmlPositionElem.Attribute("x");
                Debug.Assert(xAtt != null, "Vector2d missing x-attribute.");
                if (xAtt != null)
                {
                    this._positionX = Double.Parse(xAtt.Value);
                }

                XAttribute yAtt = xmlPositionElem.Attribute("y");
                Debug.Assert(yAtt != null, "Vector2d missing y-attribute.");
                if (yAtt != null)
                {
                    this._positionY = Double.Parse(yAtt.Value);
                }
            }

            // Parse the tangents.
            XElement xmlInTangentElem = xmlElem.Element("InTangent");
            if (xmlInTangentElem != null)
            {
                InTangent = new Tangent(xmlInTangentElem);
            }

            XElement xmlOutTangentElem = xmlElem.Element("OutTangent");
            if (xmlOutTangentElem != null)
            {
                OutTangent = new Tangent(xmlOutTangentElem);
            }

            // Tangents unified.
            XElement xmlTangentsUnifiedElem = xmlElem.Element("TangentsUnified");
            Debug.Assert(xmlTangentsUnifiedElem != null, "Point XElement is missing the TangentsUnified element.");
            if (xmlTangentsUnifiedElem != null)
            {
                TangentsUnified = Boolean.Parse(xmlTangentsUnifiedElem.Value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="position"></param>
        public Point(Curve parent, System.Windows.Point position)
            : base(parent)
        {
            this._positionX = position.X;
            this._positionY = position.Y;
            this._inTangent = new Tangent(TangentMode.Linear);
            this._outTangent = new Tangent(TangentMode.Linear);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Point(IModel parent, float x, float y)
        {
            this._positionX = x;
            this._positionY = y;
            this._inTangent = new Tangent(TangentMode.Linear);
            this._outTangent = new Tangent(TangentMode.Linear);
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        internal XElement ToXElement(XName name)
        {
            return
                new XElement(name,
                    new XElement("Position", new XAttribute("x", PositionX), new XAttribute("y", PositionY)),
                    (InTangent == null ? null : InTangent.ToXElement("InTangent")),
                    (OutTangent == null ? null : OutTangent.ToXElement("OutTangent")),
                    new XElement("TangentsUnified", TangentsUnified)
                );
        }
        #endregion // Public Methods

        #region Overrides
        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Indicates whether the current object is equal to the specified object. When this
        /// method is called it has already been determined that the specified object is not
        /// equal to the current object or null through reference equality.
        /// </summary>
        /// <param name="other">
        /// An object to compare with this object.
        /// </param>
        /// <returns>
        /// True if the current object is equal to the other parameter; otherwise, false.
        /// </returns>
        protected override bool EqualsCore(IModel other)
        {
            Point otherPoint = (Point)other;
            return (PositionX == otherPoint.PositionX &&
                PositionY == otherPoint.PositionY &&
                InTangent == otherPoint.InTangent &&
                OutTangent == otherPoint.OutTangent);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return String.Format("({0},{1})", PositionX, PositionY);
        }
        #endregion // Overrides
    } // PointModel
}
