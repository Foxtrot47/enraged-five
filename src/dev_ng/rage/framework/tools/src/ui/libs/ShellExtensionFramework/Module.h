#ifndef SEF_MODULE_H
#define SEF_MODULE_H

#if defined _MSC_VER && _MSC_VER >= 1300
#pragma once
#endif

#include "ShellExtensionFramework.h"

/**
	\brief Base extension class to define the extension module (one per extension).
 */
class CShellExtensionModule
{
public:
	virtual ~CShellExtensionModule( ) { };

	virtual BOOL DllMain( DWORD dwReason, LPVOID lpReserved ) = 0;
};

#endif // SEF_MODULE_H