﻿//---------------------------------------------------------------------------------------------
// <copyright file="ExceptionCommands.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Exceptions
{
    using System.Windows.Input;

    /// <summary>
    /// Contains the routed commands that are used within the exception window.
    /// </summary>
    public sealed class ExceptionCommands
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Continue"/> property.
        /// </summary>
        private static RoutedCommand _continueCommand;

        /// <summary>
        /// The private field used for the <see cref="LogBug"/> property.
        /// </summary>
        private static RoutedCommand _logBugCommand;

        /// <summary>
        /// The private field used for the <see cref="Quit"/> property.
        /// </summary>
        private static RoutedCommand _quitCommand;

        /// <summary>
        /// The private field used for the <see cref="SendEmail"/> property.
        /// </summary>
        private static RoutedCommand _sendEmailCommand;

        /// <summary>
        /// The generic object that is used to sync the creation of the commands.
        /// </summary>
        private static object _syncRoot = new object();
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets the command that is used to close the exception window.
        /// </summary>
        public static RoutedCommand Continue
        {
            get
            {
                if (_continueCommand == null)
                {
                    lock (_syncRoot)
                    {
                        if (_continueCommand == null)
                        {
                            _continueCommand = new RoutedCommand(
                                "ContinueCommand", typeof(ExceptionCommands));
                        }
                    }
                }

                return _continueCommand;
            }
        }

        /// <summary>
        /// Gets the command that is used to close the exception window.
        /// </summary>
        public static RoutedCommand LogBug
        {
            get
            {
                if (_logBugCommand == null)
                {
                    lock (_syncRoot)
                    {
                        if (_logBugCommand == null)
                        {
                            _logBugCommand = new RoutedCommand(
                                "LogBugCommand", typeof(ExceptionCommands));
                        }
                    }
                }

                return _logBugCommand;
            }
        }

        /// <summary>
        /// Gets the command that is used to quit the application.
        /// </summary>
        public static RoutedCommand Quit
        {
            get
            {
                if (_quitCommand == null)
                {
                    lock (_syncRoot)
                    {
                        if (_quitCommand == null)
                        {
                            _quitCommand = new RoutedCommand(
                                "QuitCommand", typeof(ExceptionCommands));
                        }
                    }
                }

                return _quitCommand;
            }
        }

        /// <summary>
        /// Gets the command that is used to close the exception window.
        /// </summary>
        public static RoutedCommand SendEmail
        {
            get
            {
                if (_sendEmailCommand == null)
                {
                    lock (_syncRoot)
                    {
                        if (_sendEmailCommand == null)
                        {
                            _sendEmailCommand = new RoutedCommand(
                                "SendEmailCommand", typeof(ExceptionCommands));
                        }
                    }
                }

                return _sendEmailCommand;
            }
        }
        #endregion Properties
    } // RSG.Editor.Controls.Exceptions.ExceptionCommands {Class}
} // RSG.Editor.Controls.Exceptions {Namespace}
