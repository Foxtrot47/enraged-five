﻿//---------------------------------------------------------------------------------------------
// <copyright file="ExportCharacterSelectWindow.xaml.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.View
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using RSG.Editor;
    using RSG.Editor.Controls;
    using RSG.Text.Model;

    /// <summary>
    /// Interaction logic for the <see cref="ExportCharacterSelectWindow"/> class.
    /// </summary>
    public partial class ExportCharacterSelectWindow : RsWindow
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="CharacterWrapperItems"/> property.
        /// </summary>
        private ObservableCollection<ChararcterWrapper> _characterWrapperItems;

        /// <summary>
        /// A private value indicating whether the select all modifier is currently being
        /// processed.
        /// </summary>
        private bool _selectingAllModifying;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ExportCharacterSelectWindow"/> class.
        /// </summary>
        public ExportCharacterSelectWindow()
        {
            this._characterWrapperItems = new ObservableCollection<ChararcterWrapper>();
            this.InitializeComponent();
            Application application = Application.Current;
            if (application != null)
            {
                Window mainWindow = application.MainWindow;
                if (mainWindow != null)
                {
                    this.Owner = mainWindow;
                }
            }

            Binding binding = new Binding();
            binding.Source = this._characterWrapperItems;
            binding.BindsDirectlyToSource = true;
            this.CharacterItemsControl.SetBinding(ItemsControl.ItemsSourceProperty, binding);

            this.SelectAllCheckBox.Checked += this.SelectAllStateChanged;
            this.SelectAllCheckBox.Unchecked += this.SelectAllStateChanged;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the list of characters that should be displayed to the user.
        /// </summary>
        public IEnumerable<DialogueCharacter> CharacterItems
        {
            set
            {
                foreach (DialogueCharacter character in value)
                {
                    this._characterWrapperItems.Add(new ChararcterWrapper(character));
                }

                if (this._characterWrapperItems.Count > 0)
                {
                    this.SelectAllCheckBox.IsChecked = true;
                }
                else
                {
                    this.SelectAllCheckBox.IsChecked = false;
                }
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Retrieves values indicating whether the specified character should be exported and
        /// what that characters name is.
        /// </summary>
        /// <param name="characterId">
        /// The id of the character to test.
        /// </param>
        /// <returns>
        /// A tuple containing the character name and a value indicating whether that character
        /// should be exported.
        /// </returns>
        public Tuple<string, bool> ExportCharacter(Guid characterId)
        {
            foreach (ChararcterWrapper character in this._characterWrapperItems)
            {
                if (character.Id == characterId)
                {
                    return Tuple.Create(character.Name, character.Export);
                }
            }

            return Tuple.Create("", false);
        }

        /// <summary>
        /// Sets the dialog result to true after the continue button is pressed.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private void ExportButtonClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        /// <summary>
        /// Sets the dialog result to false after the cancel button is pressed.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private void CancelButtonClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        /// <summary>
        /// Called whenever the master select all check box is either checked or unchecked.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private void SelectAllStateChanged(object sender, RoutedEventArgs e)
        {
            if (this._characterWrapperItems == null)
            {
                return;
            }

            try
            {
                this._selectingAllModifying = true;
                if (this._characterWrapperItems != null)
                {
                    foreach (ChararcterWrapper vm in this._characterWrapperItems)
                    {
                        if (vm == null)
                        {
                            continue;
                        }

                        vm.Export = (bool)this.SelectAllCheckBox.IsChecked;
                    }
                }
            }
            finally
            {
                this._selectingAllModifying = false;
            }
        }

        /// <summary>
        /// Called whenever a individual items check box is either checked or unchecked.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private void CheckBoxStateChanged(object sender, RoutedEventArgs e)
        {
            if (this._selectingAllModifying)
            {
                return;
            }

            bool containsFalse = false;
            bool containsTrue = false;
            foreach (ChararcterWrapper vm in this._characterWrapperItems)
            {
                if (vm == null)
                {
                    continue;
                }

                if (vm.Export == true && containsFalse)
                {
                    this.SelectAllCheckBox.IsChecked = null;
                    return;
                }

                if (!vm.Export == true && containsTrue)
                {
                    this.SelectAllCheckBox.IsChecked = null;
                    return;
                }

                if (!vm.Export)
                {
                    containsFalse = true;
                }
                else
                {
                    containsTrue = true;
                }
            }

            this.SelectAllCheckBox.IsChecked = !containsFalse;
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// Represents a view model wrapper class for a dialogue character allowing the user to
        /// specify if the export should be exported.
        /// </summary>
        private class ChararcterWrapper : NotifyPropertyChangedBase
        {
            #region Fields
            /// <summary>
            /// The private field used for the <see cref="Export"/> property.
            /// </summary>
            private bool _export;

            /// <summary>
            /// The private field used for the <see cref="Id"/> property.
            /// </summary>
            private Guid _id;

            /// <summary>
            /// The private field used for the <see cref="Name"/> property.
            /// </summary>
            private string _name;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Intialises a new instance of the <see cref="ChararcterWrapper"/> class.
            /// </summary>
            /// <param name="character">
            /// The dialogue character it is wrapping.
            /// </param>
            public ChararcterWrapper(DialogueCharacter character)
            {
                this._name = character.Name;
                this._export = true;
                this._id = character.Id;
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets or sets a value indicating whether this character is going to be exported.
            /// </summary>
            public bool Export
            {
                get { return this._export; }
                set { this.SetProperty(ref this._export, value); }
            }

            /// <summary>
            /// Gets the unique identifier for this character.
            /// </summary>
            public Guid Id
            {
                get { return this._id; }
            }

            /// <summary>
            /// Gets the name for this character.
            /// </summary>
            public string Name
            {
                get { return this._name; }
            }
            #endregion Properties
        } // ExportCharacterSelectWindow.ChararcterWrapper {Class}
        #endregion
    } // RSG.Text.View.ExportCharacterSelectWindow {Class}
} // RSG.Text.View {Namespace}
