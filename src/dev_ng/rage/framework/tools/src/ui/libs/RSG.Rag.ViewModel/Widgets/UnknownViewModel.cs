﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Rag.Widgets;

namespace RSG.Rag.ViewModel.Widgets
{
    /// <summary>
    /// View model for the <see cref="WidgetUnknown"/> class.
    /// </summary>
    public class UnknownViewModel : WidgetViewModel<WidgetUnknown>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="UnknownViewModel"/> class.
        /// </summary>
        /// <param name="widget">Widget this view model is for.</param>
        public UnknownViewModel(WidgetUnknown widget)
            : base(widget)
        {
        }
        #endregion // Constructor(s)
    } // UnknownWidgetViewModel
}
