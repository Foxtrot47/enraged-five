﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsTreeListViewItem.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System.Windows;

    /// <summary>
    /// Represents a selectable item inside a <see cref="RsTreeListView"/> control.
    /// </summary>
    public class RsTreeListViewItem : RsVirtualisedTreeViewItem
    {
        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsTreeListViewItem" /> class.
        /// </summary>
        static RsTreeListViewItem()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsTreeListViewItem),
                new FrameworkPropertyMetadata(typeof(RsTreeListViewItem)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsTreeListViewItem"/> class.
        /// </summary>
        public RsTreeListViewItem()
        {
        }
        #endregion Constructors
    } // RSG.Editor.Controls.RsTreeListViewItem {Class}
} // RSG.Editor.Controls {Namespace}
