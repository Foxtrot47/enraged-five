﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace RSG.Editor.Controls.MapViewport
{
    /// <summary>
    /// Control for displaying the current map scale as a distance.
    /// </summary>
    public class MapScaleInfo : Control
    {
        #region Constants
        /// <summary>
        /// List of distances that are supported for display in this control.
        /// TODO: Get rid of this and use an algorithm instead.  I couldn't be bothered to figure the
        /// maths out, but here is a link that would provide a good starting point:
        /// http://stackoverflow.com/questions/264080/how-do-i-calculate-the-closest-power-of-2-or-10-a-number-is
        /// </summary>
        private static readonly double[] _distances =
            new double[] { 10000, 5000, 2000, 1000, 500, 200, 100, 50, 20, 10, 5, 2, 1 };
        #endregion

        #region Dependency Properties
        /// <summary>
        /// Identifies the Zoom dependency property.
        /// </summary>
        public static readonly DependencyProperty ZoomProperty =
            DependencyProperty.Register(
                "Zoom",
                typeof(double),
                typeof(MapScaleInfo),
                new FrameworkPropertyMetadata(1.0, OnZoomChanged));

        /// <summary>
        /// 
        /// </summary>
        public double Zoom
        {
            get { return (double)GetValue(ZoomProperty); }
            set { SetValue(ZoomProperty, value); }
        }

        /// <summary>
        /// Handles the event that occurs when the value of the <see cref="Zoom"/> dependency property has changed.
        /// </summary>
        /// <param name="d">The dependency object on which the dependency property has changed.</param>
        /// <param name="e">The event args containing the old and new values of the dependency property.</param>
        private static void OnZoomChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MapScaleInfo control = d as MapScaleInfo;
            if (control != null)
            {
                // Determine the best distance to use.
                int closestIndex = 0;
                double closestWidth = 0.0;
                double closestDiff = Double.MaxValue;

                for (int i = 0; i < _distances.Length; ++i)
                {
                    double pixels = _distances[i] * (double)e.NewValue;
                    double diff = Math.Abs(75.0 - pixels);

                    if (diff < closestDiff)
                    {
                        closestIndex = i;
                        closestDiff = diff;
                        closestWidth = pixels;
                    }
                }

                control.ScaleWidth = closestWidth;
                control.ActualDistance = _distances[closestIndex];
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static readonly DependencyPropertyKey ScaleWidthPropertyKey =
            DependencyProperty.RegisterReadOnly(
                "ScaleWidth",
                typeof(double),
                typeof(MapScaleInfo),
                new FrameworkPropertyMetadata(100.0));

        /// <summary>
        /// 
        /// </summary>
        public static readonly DependencyProperty ScaleWidthProperty =
            ScaleWidthPropertyKey.DependencyProperty;

        /// <summary>
        /// 
        /// </summary>
        public double ScaleWidth
        {
            get { return (double)GetValue(ScaleWidthProperty); }
            private set { SetValue(ScaleWidthPropertyKey, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public static readonly DependencyPropertyKey ActualDistancePropertyKey =
            DependencyProperty.RegisterReadOnly(
                "ActualDistance",
                typeof(double),
                typeof(MapScaleInfo),
                new FrameworkPropertyMetadata(100.0));

        /// <summary>
        /// 
        /// </summary>
        public static readonly DependencyProperty ActualDistanceProperty =
            ActualDistancePropertyKey.DependencyProperty;

        /// <summary>
        /// 
        /// </summary>
        public double ActualDistance
        {
            get { return (double)GetValue(ActualDistanceProperty); }
            private set { SetValue(ActualDistancePropertyKey, value); }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises the static fields of the <see cref="MapScaleInfo"/> control.
        /// </summary>
        static MapScaleInfo()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(MapScaleInfo),
                new FrameworkPropertyMetadata(typeof(MapScaleInfo)));
        }
        #endregion
    }
}
