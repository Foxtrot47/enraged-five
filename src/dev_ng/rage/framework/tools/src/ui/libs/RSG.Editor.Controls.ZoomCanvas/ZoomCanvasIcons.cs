﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace RSG.Editor.Controls.ZoomCanvas
{
    /// <summary>
    /// Provides static properties that give access to ZoomCanvas related icons.
    /// </summary>
    public static class ZoomCanvasIcons
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="ZoomIn"/> property.
        /// </summary>
        private static BitmapSource _zoomIn;

        /// <summary>
        /// The private field used for the <see cref="ZoomOut"/> property.
        /// </summary>
        private static BitmapSource _zoomOut;

        /// <summary>
        /// The private field used for the <see cref="ZoomExtents"/> property.
        /// </summary>
        private static BitmapSource _zoomExtents;

        /// <summary>
        /// The private field used for the <see cref="ZoomHorizontalExtents"/> property.
        /// </summary>
        private static BitmapSource _zoomHorizontalExtents;

        /// <summary>
        /// The private field used for the <see cref="ZoomVerticalExtents"/> property.
        /// </summary>
        private static BitmapSource _zoomVerticalExtents;

        /// <summary>
        /// The private field used for the <see cref="ZoomToFit"/> property.
        /// </summary>
        private static BitmapSource _zoomToFit;

        /// <summary>
        /// A generic object that provides thread safety access to the image sources.
        /// </summary>
        private static object _syncRoot = new object();
        #endregion

        #region Properties
        /// <summary>
        /// Gets the icon that shows a zoom in icon.
        /// </summary>
        public static BitmapSource ZoomIn
        {
            get
            {
                if (_zoomIn == null)
                {
                    lock (_syncRoot)
                    {
                        if (_zoomIn == null)
                        {
                            _zoomIn = EnsureLoaded("zoomIn16.png");
                        }
                    }
                }

                return _zoomIn;
            }
        }

        /// <summary>
        /// Gets the icon that shows a zoom out icon.
        /// </summary>
        public static BitmapSource ZoomOut
        {
            get
            {
                if (_zoomOut == null)
                {
                    lock (_syncRoot)
                    {
                        if (_zoomOut == null)
                        {
                            _zoomOut = EnsureLoaded("zoomOut16.png");
                        }
                    }
                }

                return _zoomOut;
            }
        }

        /// <summary>
        /// Gets the icon that shows a zoom extents icon.
        /// </summary>
        public static BitmapSource ZoomExtents
        {
            get
            {
                if (_zoomExtents == null)
                {
                    lock (_syncRoot)
                    {
                        if (_zoomExtents == null)
                        {
                            _zoomExtents = EnsureLoaded("zoomExtents16.png");
                        }
                    }
                }

                return _zoomExtents;
            }
        }

        /// <summary>
        /// Gets the icon that shows a zoom horizontal extents icon.
        /// </summary>
        public static BitmapSource ZoomHorizontalExtents
        {
            get
            {
                if (_zoomHorizontalExtents == null)
                {
                    lock (_syncRoot)
                    {
                        if (_zoomHorizontalExtents == null)
                        {
                            _zoomHorizontalExtents = EnsureLoaded("zoomHorizontalExtents16.png");
                        }
                    }
                }

                return _zoomHorizontalExtents;
            }
        }

        /// <summary>
        /// Gets the icon that shows a zoom vertical extents icon.
        /// </summary>
        public static BitmapSource ZoomVerticalExtents
        {
            get
            {
                if (_zoomVerticalExtents == null)
                {
                    lock (_syncRoot)
                    {
                        if (_zoomVerticalExtents == null)
                        {
                            _zoomVerticalExtents = EnsureLoaded("zoomVerticalExtents16.png");
                        }
                    }
                }

                return _zoomVerticalExtents;
            }
        }

        /// <summary>
        /// Gets the icon that shows a zoom to fit icon.
        /// </summary>
        public static BitmapSource ZoomToFit
        {
            get
            {
                if (_zoomToFit == null)
                {
                    lock (_syncRoot)
                    {
                        if (_zoomToFit == null)
                        {
                            _zoomToFit = EnsureLoaded("zoomToFit16.png");
                        }
                    }
                }

                return _zoomToFit;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Ensures that the specified image source is loaded with the specified resource name.
        /// </summary>
        /// <param name="source">
        /// The image source that should be loaded.
        /// </param>
        /// <param name="resourceName">
        /// The name of the resource the loaded image source should set as its source.
        /// </param>
        private static BitmapSource EnsureLoaded(String resourceName)
        {
            String assemblyName = typeof(ZoomCanvasIcons).Assembly.GetName().Name;
            String bitmapPath = String.Format(CultureInfo.InvariantCulture,
                "pack://application:,,,/{0};component/Resources/{1}", assemblyName, resourceName);
            Uri uri = new Uri(bitmapPath, UriKind.RelativeOrAbsolute);
            return new BitmapImage(uri);
        }
        #endregion
    }
}
