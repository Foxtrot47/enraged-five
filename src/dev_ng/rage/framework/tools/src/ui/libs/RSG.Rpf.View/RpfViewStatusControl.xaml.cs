﻿//---------------------------------------------------------------------------------------------
// <copyright file="RpfViewStatusControl.xaml.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Rpf.View
{
    using System.Windows.Controls;

    /// <summary>
    /// Defines the control that can be used int the status bar while a
    /// <see cref="RpfViewControl"/> has the focus.
    /// </summary>
    public partial class RpfViewStatusControl : UserControl
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="RpfViewStatusControl"/> class.
        /// </summary>
        public RpfViewStatusControl()
        {
            this.InitializeComponent();
        }
        #endregion Constructors
    } // RSG.Rpf.View.RpfViewerStatusControl {Class}
} // RSG.Rpf.View {Namespace}
