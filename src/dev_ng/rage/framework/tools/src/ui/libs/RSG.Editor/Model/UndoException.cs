﻿//---------------------------------------------------------------------------------------------
// <copyright file="UndoException.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Model
{
    using System;
    using System.Runtime.Serialization;
    using RSG.Editor.Resources;

    /// <summary>
    /// The exception that is thrown when a unhandled exception is caught when trying to undo
    /// a single undo event.
    /// </summary>
    [Serializable]
    public class UndoException : Exception
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="UndoException"/> class using the
        /// default message.
        /// </summary>
        public UndoException()
            : base(ErrorStringTable.GetString("UndoExceptionMessage"))
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="UndoException"/> class with the
        /// specified message.
        /// </summary>
        /// <param name="message">
        /// The message that describes the error.
        /// </param>
        public UndoException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="UndoException"/> class with the
        /// specified inner exception and the default message.
        /// </summary>
        /// <param name="innerException">
        /// The exception that is the cause of this current exception, or a null reference if
        /// no inner exception is specified.
        /// </param>
        public UndoException(Exception innerException)
            : base(ErrorStringTable.GetString("UndoExceptionMessage"), innerException)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="UndoException"/> class with the
        /// specified message and inner exception.
        /// </summary>
        /// <param name="message">
        /// The message that describes the error.
        /// </param>
        /// <param name="innerException">
        /// The exception that is the cause of this current exception, or a null reference if
        /// no inner exception is specified.
        /// </param>
        public UndoException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="UndoException"/> class with serialised
        /// data.
        /// </summary>
        /// <param name="info">
        /// The System.Runtime.Serialization.SerializationInfo that holds the serialised
        /// object data about the exception being thrown.
        /// </param>
        /// <param name="context">
        /// The System.Runtime.Serialization.StreamingContext that contains contextual
        /// information about the source or destination.
        /// </param>
        protected UndoException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
        #endregion Constructors
    } // RSG.Editor.Model.UndoException {Class}
} // RSG.Editor.Model {Namespace}
