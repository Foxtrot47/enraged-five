﻿//---------------------------------------------------------------------------------------------
// <copyright file="ITunableViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Tunables
{
    using RSG.Editor.View;
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// When implemented represents a view model that is used for view models wrapping a
    /// tunable class type.
    /// </summary>
    public interface ITunableViewModel : IViewModel, ITreeDisplayItem
    {
        #region Properties
        /// <summary>
        /// Gets a value indicating whether or not the value for this tunable can be inherited.
        /// </summary>
        bool CanInheritValue { get; }

        /// <summary>
        /// Gets a value indicating whether this tunable can be expanded.
        /// </summary>
        bool IsExpandable { get; }

        /// <summary>
        /// Gets or sets a value indicating whether this tunable is currently locked from being
        /// edited.
        /// </summary>
        bool IsLocked { get; set; }

        /// <summary>
        /// Gets or sets the map key view model object for this tunable.
        /// </summary>
        MapKeyViewModel MapKey { get; set; }

        /// <summary>
        /// Gets the parent tunable view model for this view model.
        /// </summary>
        ITunableViewModel Parent { get; }

        /// <summary>
        /// Gets the tunable model this view model is wrapping.
        /// </summary>
        ITunable Tunable { get; }

        /// <summary>
        /// Gets the value of the type for this tunable view model that should be shown to the
        /// user.
        /// </summary>
        string TypeValue { get; }

        /// <summary>
        /// Gets the tunable at the specified item from within this tunables item collection if
        /// applicable.
        /// </summary>
        /// <param name="index">
        /// The zero-based index of the tunable to get.
        /// </param>
        /// <returns>
        /// The tunable at the specified index if applicable; otherwise, null.
        /// </returns>
        ITunableViewModel this[int index] { get; }

        /// <summary>
        /// Gets the tunable view model that belongs to this tunable that is currently wrapping
        /// the specified model.
        /// </summary>
        /// <param name="model">
        /// The model of the tunable view model to get.
        /// </param>
        /// <returns>
        /// The tunable view model that is wrapping the specified model if applicable;
        /// otherwise, null.
        /// </returns>
        ITunableViewModel this[ITunable model] { get; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Resets this tunable to have its default value. (i.e no source data).
        /// </summary>
        void ResetValueToDefault();
        #endregion Methods
    } // RSG.Metadata.ViewModel.Tunables.ITunableViewModel {Interface}
} // RSG.Metadata.ViewModel.Tunables {Namespace}
