﻿//---------------------------------------------------------------------------------------------
// <copyright file="Icons.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel
{
    using System;
    using System.Windows.Media.Imaging;
    using RSG.Base.Extensions;

    /// <summary>
    /// Provides static properties that gives access to the icons that are used for the
    /// metadata types inside a tree view.
    /// </summary>
    internal static class Icons
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="ConstantNodeIcon"/> property.
        /// </summary>
        private static BitmapSource _constantNodeIcon;

        /// <summary>
        /// The private field used for the <see cref="DefinitionFileNodeIcon"/> property.
        /// </summary>
        private static BitmapSource _definitionFileNodeIcon;

        /// <summary>
        /// The private field used for the <see cref="DefinitionsNodeIcon"/> property.
        /// </summary>
        private static BitmapSource _definitionsNodeIcon;

        /// <summary>
        /// The private field used for the <see cref="EnumMemberNodeIcon"/> property.
        /// </summary>
        private static BitmapSource _enumMemberNodeIcon;

        /// <summary>
        /// The private field used for the <see cref="EnumNodeIcon"/> property.
        /// </summary>
        private static BitmapSource _enumNodeIcon;

        /// <summary>
        /// The private field used for the <see cref="NamespaceNodeIcon"/> property.
        /// </summary>
        private static BitmapSource _namespaceNodeIcon;

        /// <summary>
        /// The private field used for the <see cref="PscFileNode"/> property.
        /// </summary>
        private static BitmapSource _pscFileNodeIcon;

        /// <summary>
        /// The private field used for the <see cref="StructMemberNodeIcon"/> property.
        /// </summary>
        private static BitmapSource _structMemberNodeIcon;

        /// <summary>
        /// The private field used for the <see cref="StructNodeIcon"/> property.
        /// </summary>
        private static BitmapSource _structNodeIcon;

        /// <summary>
        /// A generic object that provides thread safety access to the image sources.
        /// </summary>
        private static object _syncRoot = new object();
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets the icon that is used for a node representing a constant inside the project
        /// tree view.
        /// </summary>
        public static BitmapSource ConstantNodeIcon
        {
            get
            {
                if (_constantNodeIcon == null)
                {
                    lock (_syncRoot)
                    {
                        if (_constantNodeIcon == null)
                        {
                            EnsureLoaded(ref _constantNodeIcon, "constantNode");
                        }
                    }
                }

                return _constantNodeIcon;
            }
        }

        /// <summary>
        /// Gets the icon that is used for a definition file node inside the project tree view.
        /// </summary>
        public static BitmapSource DefinitionFileNodeIcon
        {
            get
            {
                if (_definitionFileNodeIcon == null)
                {
                    lock (_syncRoot)
                    {
                        if (_definitionFileNodeIcon == null)
                        {
                            EnsureLoaded(ref _definitionFileNodeIcon, "definitionFileNode");
                        }
                    }
                }

                return _definitionFileNodeIcon;
            }
        }

        /// <summary>
        /// Gets the icon that is used for the linked definitions node inside the project
        /// tree view.
        /// </summary>
        public static BitmapSource DefinitionsNodeIcon
        {
            get
            {
                if (_definitionsNodeIcon == null)
                {
                    lock (_syncRoot)
                    {
                        if (_definitionsNodeIcon == null)
                        {
                            EnsureLoaded(ref _definitionsNodeIcon, "definitionsNode");
                        }
                    }
                }

                return _definitionsNodeIcon;
            }
        }

        /// <summary>
        /// Gets the icon that is used for a node representing a enumeration member inside the
        /// project tree view.
        /// </summary>
        public static BitmapSource EnumMemberNodeIcon
        {
            get
            {
                if (_enumMemberNodeIcon == null)
                {
                    lock (_syncRoot)
                    {
                        if (_enumMemberNodeIcon == null)
                        {
                            EnsureLoaded(ref _enumMemberNodeIcon, "enumMemberNode");
                        }
                    }
                }

                return _enumMemberNodeIcon;
            }
        }

        /// <summary>
        /// Gets the icon that is used for a node representing a enumeration inside the project
        /// tree view.
        /// </summary>
        public static BitmapSource EnumNodeIcon
        {
            get
            {
                if (_enumNodeIcon == null)
                {
                    lock (_syncRoot)
                    {
                        if (_enumNodeIcon == null)
                        {
                            EnsureLoaded(ref _enumNodeIcon, "enumNode");
                        }
                    }
                }

                return _enumNodeIcon;
            }
        }

        /// <summary>
        /// Gets the icon that is used for a node representing a namespace inside the class
        /// view tool.
        /// </summary>
        public static BitmapSource NamespaceNodeIcon
        {
            get
            {
                if (_namespaceNodeIcon == null)
                {
                    lock (_syncRoot)
                    {
                        if (_namespaceNodeIcon == null)
                        {
                            EnsureLoaded(ref _namespaceNodeIcon, "namespaceNode");
                        }
                    }
                }

                return _namespaceNodeIcon;
            }
        }

        /// <summary>
        /// Gets the icon that is used for a node representing a psc file inside the project
        /// tree view.
        /// </summary>
        public static BitmapSource PscFileNode
        {
            get
            {
                if (_pscFileNodeIcon == null)
                {
                    lock (_syncRoot)
                    {
                        if (_pscFileNodeIcon == null)
                        {
                            EnsureLoaded(ref _pscFileNodeIcon, "pscFileNode");
                        }
                    }
                }

                return _pscFileNodeIcon;
            }
        }

        /// <summary>
        /// Gets the icon that is used for a node representing a structure member inside the
        /// project tree view.
        /// </summary>
        public static BitmapSource StructMemberNodeIcon
        {
            get
            {
                if (_structMemberNodeIcon == null)
                {
                    lock (_syncRoot)
                    {
                        if (_structMemberNodeIcon == null)
                        {
                            EnsureLoaded(ref _structMemberNodeIcon, "structMemberNode");
                        }
                    }
                }

                return _structMemberNodeIcon;
            }
        }

        /// <summary>
        /// Gets the icon that is used for a node representing a structure inside the project
        /// tree view.
        /// </summary>
        public static BitmapSource StructNodeIcon
        {
            get
            {
                if (_structNodeIcon == null)
                {
                    lock (_syncRoot)
                    {
                        if (_structNodeIcon == null)
                        {
                            EnsureLoaded(ref _structNodeIcon, "structNode");
                        }
                    }
                }

                return _structNodeIcon;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Ensures that the specified image source is loaded with the specified resource name.
        /// </summary>
        /// <param name="source">
        /// The image source that should be loaded.
        /// </param>
        /// <param name="resourceName">
        /// The name of the resource the loaded image source should set as its source.
        /// </param>
        private static void EnsureLoaded(ref BitmapSource source, string resourceName)
        {
            if (source != null)
            {
                return;
            }

            string assemblyName = typeof(Icons).Assembly.GetName().Name;
            string path = "Resources/" + resourceName + ".png";
            string bitmapPath = "pack://application:,,,/{0};component/{1}";
            bitmapPath = bitmapPath.FormatInvariant(assemblyName, path);
            Uri uri = new Uri(bitmapPath, UriKind.RelativeOrAbsolute);

            source = new BitmapImage(uri);
            source.Freeze();
        }
        #endregion Methods
    } // RSG.Metadata.ViewModel.Icons {Class}
} // RSG.Metadata.ViewModel {Namespace}
