﻿//---------------------------------------------------------------------------------------------
// <copyright file="ColourUtilities.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Media;

    /// <summary>
    /// Contains static methods that can be used to manipulate colour values.
    /// </summary>
    public static class ColourUtilities
    {
        #region Methods
        /// <summary>
        /// Converts an HSV colour to an RGB colour.
        /// </summary>
        /// <param name="hsvColour">
        /// The HSV colour to convert.
        /// </param>
        /// <returns>
        /// The RGB colour representation of the specified HSV values.
        /// </returns>
        public static Color ConvertHsvToRgb(HsvColour hsvColour)
        {
            return ConvertHsvToRgb(hsvColour.H, hsvColour.S, hsvColour.V);
        }

        /// <summary>
        /// Converts an HSV colour to an RGB colour.
        /// </summary>
        /// <param name="h">
        /// The hue component of the colour to convert.
        /// </param>
        /// <param name="s">
        /// The saturation component of the colour to convert.
        /// </param>
        /// <param name="v">
        /// The value component of the colour to convert.
        /// </param>
        /// <returns>
        /// The RGB colour representation of the specified HSV values.
        /// </returns>
        public static Color ConvertHsvToRgb(double h, double s, double v)
        {
            double c = v * (s / 100.0);
            double x = c * (1 - Math.Abs(h / 60));
            double m = v - c;

            double r = 0.0;
            double g = 0.0;
            double b = 0.0;

            if (0 <= h && h < 60)
            {
                r = c;
                g = x;
                b = 0.0;
            }
            else if (60 <= h && h < 120)
            {
                r = x;
                g = c;
                b = 0.0;
            }
            else if (120 <= h && h < 180)
            {
                r = 0.0;
                g = c;
                b = x;
            }
            else if (180 <= h && h < 240)
            {
                r = 0.0;
                g = x;
                b = c;
            }
            else if (240 <= h && h < 300)
            {
                r = x;
                g = 0.0;
                b = c;
            }
            else if (300 <= h && h < 360)
            {
                r = c;
                g = 0.0;
                b = x;
            }

            if (s == 0)
            {
                r = v;
                g = v;
                b = v;
            }
            else
            {
                int i;
                double f, p, q, t;

                if (h == 360)
                {
                    h = 0;
                }
                else
                {
                    h = h / 60;
                }

                i = (int)Math.Truncate(h);
                f = h - i;

                p = v * (1.0 - s);
                q = v * (1.0 - (s * f));
                t = v * (1.0 - (s * (1.0 - f)));

                switch (i)
                {
                    case 0:
                        {
                            r = v;
                            g = t;
                            b = p;
                        }

                        break;

                    case 1:
                        {
                            r = q;
                            g = v;
                            b = p;
                        }

                        break;

                    case 2:
                        {
                            r = p;
                            g = v;
                            b = t;
                        }

                        break;

                    case 3:
                        {
                            r = p;
                            g = q;
                            b = v;
                        }

                        break;

                    case 4:
                        {
                            r = t;
                            g = p;
                            b = v;
                        }

                        break;

                    default:
                        {
                            r = v;
                            g = p;
                            b = q;
                        }

                        break;
                }
            }

            byte red = (byte)(r * 255);
            byte green = (byte)(g * 255);
            byte blue = (byte)(b * 255);

            return Color.FromRgb(red, green, blue);
        }

        /// <summary>
        /// Converts an RGB colour to an HSV colour.
        /// </summary>
        /// <param name="colour">
        /// The RSG colour to convert.
        /// </param>
        /// <returns>
        /// The RGB colour representation of the specified HSV values.
        /// </returns>
        public static HsvColour ConvertRgbToHsv(Color colour)
        {
            return ConvertRgbToHsv(colour.R, colour.G, colour.B);
        }

        /// <summary>
        /// Converts an RGB colour to an HSV colour.
        /// </summary>
        /// <param name="r">
        /// The red component of the colour to convert.
        /// </param>
        /// <param name="g">
        /// The green component of the colour to convert.
        /// </param>
        /// <param name="b">
        /// The blue component of the colour to convert.
        /// </param>
        /// <returns>
        /// The HSV colour representation of the specified RGB values.
        /// </returns>
        public static HsvColour ConvertRgbToHsv(byte r, byte g, byte b)
        {
            double h = 0, s;

            double min = Math.Min(Math.Min(r, g), b);
            double max = Math.Max(Math.Max(r, g), b);
            double delta = max - min;

            if (max == 0.0)
            {
                s = 0;
            }
            else
            {
                s = delta / max;
            }

            if (s == 0)
            {
                h = 0.0;
            }
            else
            {
                if (r == max)
                {
                    h = (g - b) / delta;
                }
                else if (g == max)
                {
                    h = 2 + ((b - r) / delta);
                }
                else if (b == max)
                {
                    h = 4 + ((r - g) / delta);
                }

                h *= 60;
                if (h < 0.0)
                {
                    h = h + 360;
                }
            }

            return new HsvColour(h, s, max / 255);
        }

        /// <summary>
        /// Generates a list of colours with hues ranging from 0 to 360 with a saturation and
        /// value of 1.
        /// </summary>
        /// <returns>
        /// A list of colours with hues ranging from 0 to 360.
        /// </returns>
        public static List<Color> GenerateHsvSpectrum()
        {
            List<Color> colourList = new List<Color>(8);
            for (int i = 0; i < 29; i++)
            {
                colourList.Add(ConvertHsvToRgb(i * 12, 1, 1));
            }

            colourList.Add(ConvertHsvToRgb(0, 1, 1));
            return colourList;
        }

        /// <summary>
        /// Generates a list of colours with hues with a saturation and value of 1.
        /// </summary>
        /// <param name="startHue">
        /// The start of the hue spectrum to retrieve.
        /// </param>
        /// <param name="endHue">
        /// The end of the hue spectrum to retrieve.
        /// </param>
        /// <returns>
        /// A list of colours with hues ranging from 0 to 360.
        /// </returns>
        public static List<Color> GenerateHsvSpectrum(double startHue, double endHue)
        {
            List<Color> colourList = new List<Color>(8);
            int start = (int)(startHue * 36);
            int end = (int)(endHue * 36);

            for (int i = start; i <= end; ++i)
            {
                colourList.Add(ConvertHsvToRgb(i * 10, 1, 1));
            }

            return colourList;
        }
        #endregion Methods
    } // RSG.Editor.Controls.Helpers.ColourUtilities {Class}
} // RSG.Editor.Controls.Helpers {Namespace}
