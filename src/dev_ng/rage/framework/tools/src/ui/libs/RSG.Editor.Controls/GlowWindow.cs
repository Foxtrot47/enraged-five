﻿//---------------------------------------------------------------------------------------------
// <copyright file="GlowWindow.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Interop;
    using System.Windows.Media;
    using RSG.Editor.Controls.Media;
    using RSG.Interop.Microsoft.Windows;

    /// <summary>
    /// Represents a window that has a native glow bitmap drawn onto it and sits at a specified
    /// orientation to a target element.
    /// </summary>
    internal class GlowWindow : HwndWrapper
    {
        #region Fields
        /// <summary>
        /// The thickness of the area the user can grip on to with the mouse.
        /// </summary>
        private const int CornerGripThickness = 18;

        /// <summary>
        /// The amount of depth the glow should have.
        /// </summary>
        private const int GlowDepth = 9;

        /// <summary>
        /// The class name that will be used to register the class atom for all windows
        /// of this type.
        /// </summary>
        private const string GlowWindowClassName = "RockstarGlowWindow";

        /// <summary>
        /// The private field used for the <see cref="SharedWindowClassAtom"/> property.
        /// </summary>
        private static ushort _sharedWindowClassAtom;

        /// <summary>
        /// A reference to the window procedural call-back delegate that is shared across
        /// all glow windows.
        /// </summary>
        private static WndProcDelegate _sharedWndProc;

        /// <summary>
        /// The cache of glow bitmaps that are drawn when this window is in a active state.
        /// </summary>
        private readonly GlowBitmap[] _activeGlowBitmaps;

        /// <summary>
        /// The cache of glow bitmaps that are drawn when this window is in a inactive state.
        /// </summary>
        private readonly GlowBitmap[] _inactiveGlowBitmaps;

        /// <summary>
        /// A value specifying the docking orientation of this window to its target window.
        /// </summary>
        private readonly Dock _orientation;

        /// <summary>
        /// This windows target window, and the window that it positions itself along side
        /// based on its orientation property.
        /// </summary>
        private readonly RsWindow _targetWindow;

        /// <summary>
        /// The private field used for the <see cref="ActiveGlowColor"/> property.
        /// </summary>
        private Color _activeGlowColor;

        /// <summary>
        /// The private field used for the <see cref="CanBeUsedToResize"/> property.
        /// </summary>
        private bool _canBeUsedToResize;

        /// <summary>
        /// The private field used for the <see cref="Height"/> property.
        /// </summary>
        private int _height;

        /// <summary>
        /// The private field used for the <see cref="InactiveGlowColor"/> property.
        /// </summary>
        private Color _inactiveGlowColor;

        /// <summary>
        /// A flag that contains all of the field types that have changed since the last commit
        /// and are therefore on longer valid.
        /// </summary>
        private InvalidFields _invalidFields;

        /// <summary>
        /// The private field used for the <see cref="IsActive"/> property.
        /// </summary>
        private bool _isActive;

        /// <summary>
        /// The private field used for the <see cref="IsVisible"/> property.
        /// </summary>
        private bool _isVisible;

        /// <summary>
        /// The private field used for the <see cref="Left"/> property.
        /// </summary>
        private int _left;

        /// <summary>
        /// A value indicating whether there is currently a pending rendering pass
        /// to process.
        /// </summary>
        private bool _pendingDelayRender;

        /// <summary>
        /// The private field used for the <see cref="Top"/> property.
        /// </summary>
        private int _top;

        /// <summary>
        /// The private field used for the <see cref="Width"/> property.
        /// </summary>
        private int _width;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="GlowWindow"/> class to be used on the
        /// specified target window at the specified orientation.
        /// </summary>
        /// <param name="target">
        /// The target window for the glow window.
        /// </param>
        /// <param name="orientation">
        /// The orientation of this glow window to this target window.
        /// </param>
        public GlowWindow(RsWindow target, Dock orientation)
        {
            if (target == null)
            {
                throw new SmartArgumentNullException(() => target);
            }

            this._activeGlowBitmaps = new GlowBitmap[(int)GlowBitmapPart.Count];
            this._inactiveGlowBitmaps = new GlowBitmap[(int)GlowBitmapPart.Count];
            this._activeGlowColor = Colors.Transparent;
            this._inactiveGlowColor = Colors.Transparent;

            this._targetWindow = target;
            this._orientation = orientation;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="GlowWindow"/> class to be used on the
        /// specified target window at the specified orientation.
        /// </summary>
        /// <param name="target">
        /// The target window for the glow window.
        /// </param>
        /// <param name="orientation">
        /// The orientation of this glow window to this target window.
        /// </param>
        /// <param name="resizable">
        /// A value indicating whether this glow window can be used to resize the target
        /// window.
        /// </param>
        public GlowWindow(RsWindow target, Dock orientation, bool resizable)
        {
            if (target == null)
            {
                throw new SmartArgumentNullException(() => target);
            }

            this._activeGlowBitmaps = new GlowBitmap[(int)GlowBitmapPart.Count];
            this._inactiveGlowBitmaps = new GlowBitmap[(int)GlowBitmapPart.Count];
            this._activeGlowColor = target.ActiveGlowColour;
            this._inactiveGlowColor = target.InactiveGlowColour;
            this._canBeUsedToResize = resizable;
            this._isActive = target.IsActive;

            this._targetWindow = target;
            this._orientation = orientation;
        }
        #endregion Constructors

        #region Enum
        /// <summary>
        /// Defines the different invalid states a glow window can have for its fields.
        /// </summary>
        private enum InvalidFields
        {
            /// <summary>
            /// Represents no field is invalid.
            /// </summary>
            None = 0,

            /// <summary>
            /// Represents the fields that are associated with the windows location are
            /// invalid.
            /// </summary>
            Location = 1,

            /// <summary>
            /// Represents the fields that are associated with the windows size are invalid.
            /// </summary>
            Size = 2,

            /// <summary>
            /// Represents that the windows active colour is invalid.
            /// </summary>
            ActiveColor = 4,

            /// <summary>
            /// Represents that the windows inactive colour is invalid.
            /// </summary>
            InactiveColor = 8,

            /// <summary>
            /// Represents the fields that are associated with the way the window is rendered
            /// are invalid.
            /// </summary>
            Render = 16,

            /// <summary>
            /// Represents that the windows visibility state is invalid.
            /// </summary>
            Visibility = 32
        }  // RSG.Editor.Controls.InvalidFields {Enum}
        #endregion Enum

        #region Properties
        /// <summary>
        /// Gets or sets the colour of the glow when the <see cref="IsActive"/> property
        /// is set to true.
        /// </summary>
        public Color ActiveGlowColor
        {
            get
            {
                return this._activeGlowColor;
            }

            set
            {
                InvalidFields invalid = InvalidFields.ActiveColor | InvalidFields.Render;
                this.UpdateProperty(ref this._activeGlowColor, value, invalid);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this glow window can be used to resize the
        /// window that it is associated to.
        /// </summary>
        public bool CanBeUsedToResize
        {
            get { return this._canBeUsedToResize; }
            set { this._canBeUsedToResize = value; }
        }

        /// <summary>
        /// Gets or sets the width of the window.
        /// </summary>
        public override int Height
        {
            get
            {
                return this._height;
            }

            set
            {
                InvalidFields invalid = InvalidFields.Size | InvalidFields.Render;
                this.UpdateProperty(ref this._height, value, invalid);
            }
        }

        /// <summary>
        /// Gets or sets the colour of the glow when the <see cref="IsActive"/> property
        /// is set to false.
        /// </summary>
        public Color InactiveGlowColor
        {
            get
            {
                return this._inactiveGlowColor;
            }

            set
            {
                InvalidFields invalid = InvalidFields.InactiveColor | InvalidFields.Render;
                this.UpdateProperty(ref this._inactiveGlowColor, value, invalid);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this window is currently in a active
        /// state. This determine which glow colour will be rendered.
        /// </summary>
        public bool IsActive
        {
            get
            {
                return this._isActive;
            }

            set
            {
                InvalidFields invalid = InvalidFields.Render;
                this.UpdateProperty(ref this._isActive, value, invalid);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this window is current visible.
        /// </summary>
        public bool IsVisible
        {
            get
            {
                return this._isVisible;
            }

            set
            {
                InvalidFields invalid = InvalidFields.Render | InvalidFields.Visibility;
                this.UpdateProperty(ref this._isVisible, value, invalid);
            }
        }

        /// <summary>
        /// Gets or sets the position of the window's left edge, in relation to the desktop.
        /// </summary>
        public override int Left
        {
            get
            {
                return this._left;
            }

            set
            {
                InvalidFields invalid = InvalidFields.Location;
                this.UpdateProperty(ref this._left, value, invalid);
            }
        }

        /// <summary>
        /// Gets or sets the position of the window's top edge, in relation to the desktop.
        /// </summary>
        public override int Top
        {
            get
            {
                return this._top;
            }

            set
            {
                InvalidFields invalid = InvalidFields.Location;
                this.UpdateProperty(ref this._top, value, invalid);
            }
        }

        /// <summary>
        /// Gets or sets the width of the window.
        /// </summary>
        public override int Width
        {
            get
            {
                return this._width;
            }

            set
            {
                InvalidFields invalid = InvalidFields.Size | InvalidFields.Render;
                this.UpdateProperty(ref this._width, value, invalid);
            }
        }

        /// <summary>
        /// Gets a value indicating whether this class is a subclass of the
        /// <see cref="HwndWrapper"/> class.
        /// </summary>
        protected override bool IsWindowSubclassed
        {
            get { return true; }
        }

        /// <summary>
        /// Gets the shared window atom that is registered for this type of window.
        /// </summary>
        private static ushort SharedWindowClassAtom
        {
            get
            {
                if (_sharedWindowClassAtom == 0)
                {
                    _sharedWndProc = new WndProcDelegate(User32.DefWindowProc);

                    WNDCLASS windowClass = default(WNDCLASS);
                    windowClass.ClassExtraBytes = 0;
                    windowClass.WindowExtraBytes = 0;
                    windowClass.Background = IntPtr.Zero;
                    windowClass.Cursor = IntPtr.Zero;
                    windowClass.Icon = IntPtr.Zero;
                    windowClass.WndProc = _sharedWndProc;
                    windowClass.ClassName = GlowWindowClassName;
                    windowClass.MenuName = null;
                    windowClass.Style = 0u;

                    _sharedWindowClassAtom = User32.RegisterClass(ref windowClass);
                }

                return _sharedWindowClassAtom;
            }
        }

        /// <summary>
        /// Gets a value indicating whether any changes should be deferred or committed
        /// straight away.
        /// </summary>
        private bool IsDeferringChanges
        {
            get { return this._targetWindow.DeferGlowChangesCount > 0; }
        }

        /// <summary>
        /// Gets a value indicating whether the position is currently valid or whether a
        /// change has been made that is yet to be committed.
        /// </summary>
        private bool IsPositionValid
        {
            get
            {
                InvalidFields test =
                    InvalidFields.Location | InvalidFields.Size | InvalidFields.Visibility;
                return (this._invalidFields & test) == InvalidFields.None;
            }
        }

        /// <summary>
        /// Gets the handle to the window that is the target to this glow window.
        /// </summary>
        private IntPtr TargetWindowHandle
        {
            get { return new WindowInteropHelper(this._targetWindow).Handle; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Changes the owner of this window.
        /// </summary>
        /// <param name="newOwner">
        /// The handle to the window that will this windows owner.
        /// </param>
        public void ChangeOwner(IntPtr newOwner)
        {
            User32.SetWindowLong(this.Handle, WindowLongFlag.HWNDPARENT, newOwner);
        }

        /// <summary>
        /// Commits on any changes that have been made since the last time this function
        /// was called.
        /// </summary>
        public void CommitChanges()
        {
            this.InvalidateCachedBitmaps();
            this.UpdateWindowPosCore();
            this.UpdateLayeredWindowCore();

            this._invalidFields = InvalidFields.None;
        }

        /// <summary>
        /// Updates the window position based on the target window position.
        /// </summary>
        public void UpdateWindowPos()
        {
            IntPtr targetWindowHandle = this.TargetWindowHandle;
            Rect rect = User32.GetWindowRect(targetWindowHandle);

            if (this.IsVisible)
            {
                switch (this._orientation)
                {
                    case Dock.Left:
                        this.Left = (int)rect.Left - 9;
                        this.Top = (int)rect.Top - 9;
                        this.Width = 9;
                        this.Height = (int)rect.Height + 18;
                        return;
                    case Dock.Top:
                        this.Left = (int)rect.Left - 9;
                        this.Top = (int)rect.Top - 9;
                        this.Width = (int)rect.Width + 18;
                        this.Height = 9;
                        return;
                    case Dock.Right:
                        this.Left = (int)rect.Right;
                        this.Top = (int)rect.Top - 9;
                        this.Width = 9;
                        this.Height = (int)rect.Height + 18;
                        return;
                    default:
                        this.Left = (int)rect.Left - 9;
                        this.Top = (int)rect.Bottom;
                        this.Width = (int)rect.Width + 18;
                        this.Height = 9;
                        break;
                }
            }
        }

        /// <summary>
        /// Creates a unique identifier for the class that was registered for the
        /// wrapped window.
        /// </summary>
        /// <returns>
        /// A atom for the wrapped class.
        /// </returns>
        protected override ushort CreateWindowClassCore()
        {
            return SharedWindowClassAtom;
        }

        /// <summary>
        /// Creates the core window and returns the handle to the newly created window.
        /// </summary>
        /// <returns>
        /// A handle to the newly created window.
        /// </returns>
        protected override IntPtr CreateWindowCore()
        {
            WindowExStyles extendedStyle = WindowExStyles.LAYERED | WindowExStyles.TOOLWINDOW;
            WindowStyles style =
                WindowStyles.POPUP | WindowStyles.CLIPCHILDREN | WindowStyles.CLIPSIBLINGS;

            IntPtr atom = new IntPtr((int)this.WindowClassAtom);
            IntPtr parent = new WindowInteropHelper(this._targetWindow).Owner;
            return User32.CreateWindow(extendedStyle, atom, style, parent);
        }

        /// <summary>
        /// Destroys the core components of the wrapped window.
        /// </summary>
        protected override void DestroyWindowClassCore()
        {
        }

        /// <summary>
        /// Disposes of the managed resources the belong to this glow window.
        /// </summary>
        protected override void DisposeManagedResources()
        {
            ClearCache(this._activeGlowBitmaps);
            ClearCache(this._inactiveGlowBitmaps);
        }

        /// <summary>
        /// Processes the specified message on the specified window handle.
        /// </summary>
        /// <param name="hwnd">
        /// A handle to the window procedure that received the message.
        /// </param>
        /// <param name="msg">
        /// The message.
        /// </param>
        /// <param name="wordParameter">
        /// First additional message information. The content of this parameter depends on
        /// the value of the <paramref name="msg"/> parameter.
        /// </param>
        /// <param name="longParameter">
        /// Second additional message information. The content of this parameter depends on
        /// the value of the <paramref name="msg"/> parameter.
        /// </param>
        /// <returns>
        /// The return value is the result of the message processing and depends on
        /// the message.
        /// </returns>
        protected override IntPtr WndProc(
            IntPtr hwnd, WindowMessage msg, IntPtr wordParameter, IntPtr longParameter)
        {
            switch (msg)
            {
                case WindowMessage.ACTIVATE:
                    return IntPtr.Zero;

                case WindowMessage.WINDOWPOSCHANGING:
                    object obj = Marshal.PtrToStructure(longParameter, typeof(WINDOWPOS));
                    WINDOWPOS windowPos = (WINDOWPOS)obj;
                    windowPos.Flags |= WindowPosFlags.NOACTIVATE;
                    Marshal.StructureToPtr(windowPos, longParameter, true);
                    break;

                case WindowMessage.NCHITTEST:
                    return new IntPtr(this.WindowMessageNcHitTest(longParameter));

                case WindowMessage.NCLBUTTONDOWN:
                case WindowMessage.NCLBUTTONDBLCLK:
                case WindowMessage.NCRBUTTONDOWN:
                case WindowMessage.NCRBUTTONDBLCLK:
                case WindowMessage.NCMBUTTONDOWN:
                case WindowMessage.NCMBUTTONDBLCLK:
                case WindowMessage.NCXBUTTONDOWN:
                case WindowMessage.NCXBUTTONDBLCLK:
                    IntPtr targetHandle = this.TargetWindowHandle;
                    User32.SendMessage(
                        targetHandle, WindowMessage.ACTIVATE, new IntPtr(2), IntPtr.Zero);
                    User32.SendMessage(targetHandle, msg, wordParameter, IntPtr.Zero);
                    return IntPtr.Zero;
            }

            return base.WndProc(hwnd, msg, wordParameter, longParameter);
        }

        /// <summary>
        /// Clears the bitmap cache of the given bitmaps.
        /// </summary>
        /// <param name="bitmaps">
        /// The bitmaps that are no longer valid and need disposing and setting to null.
        /// </param>
        private static void ClearCache(GlowBitmap[] bitmaps)
        {
            for (int i = 0; i < bitmaps.Length; i++)
            {
                using (bitmaps[i])
                {
                    bitmaps[i] = null;
                }
            }
        }

        /// <summary>
        /// Draws a single bitmap to the window assuming that this is either docked to the
        /// top or bottom of the target window.
        /// </summary>
        /// <param name="dc">
        /// The drawing context to use.
        /// </param>
        /// <param name="bitmap">
        /// The bitmap that needs drawing.
        /// </param>
        /// <param name="topLeft">
        /// The top left position of the bitmap.
        /// </param>
        /// <param name="destWidth">
        /// The width of the bitmap to draw.
        /// </param>
        private static void DrawPolarBitmap(
            GlowDrawingContext dc, GlowBitmap bitmap, int topLeft, int destWidth)
        {
            int w = bitmap.Width;
            int h = bitmap.Height;
            IntPtr wdc = dc.WindowDC;
            IntPtr bdc = dc.BackgroundDC;
            BLENDFUNCTION blend = dc.Blend;

            Gdi32.SelectObject(bdc, bitmap.Handle);
            Gdi32.AlphaBlend(wdc, topLeft, 0, destWidth, h, bdc, 0, 0, w, h, blend);
        }

        /// <summary>
        /// Draws a single bitmap to the window assuming that this is either docked to the
        /// left or right of the target window.
        /// </summary>
        /// <param name="dc">
        /// The drawing context to use.
        /// </param>
        /// <param name="bitmap">
        /// The bitmap that needs drawing.
        /// </param>
        /// <param name="topLeft">
        /// The top left position of the bitmap.
        /// </param>
        /// <param name="destHeight">
        /// The height of the bitmap to draw.
        /// </param>
        private static void DrawSideBitmap(
            GlowDrawingContext dc, GlowBitmap bitmap, int topLeft, int destHeight)
        {
            int w = bitmap.Width;
            int h = bitmap.Height;
            IntPtr wdc = dc.WindowDC;
            IntPtr bdc = dc.BackgroundDC;
            BLENDFUNCTION blend = dc.Blend;

            Gdi32.SelectObject(bdc, bitmap.Handle);
            Gdi32.AlphaBlend(wdc, 0, topLeft, w, destHeight, bdc, 0, 0, w, h, blend);
        }

        /// <summary>
        /// Retrieves the low-order word for the given value.
        /// </summary>
        /// <param name="value">
        /// The value to return the low-order word from.
        /// </param>
        /// <returns>
        /// The low-order word for the given value.
        /// </returns>
        private static int GetHiWord(int value)
        {
            return (int)((short)(value >> 16));
        }

        /// <summary>
        /// Retrieves the low-order word for the given value.
        /// </summary>
        /// <param name="value">
        /// The value to return the low-order word from.
        /// </param>
        /// <returns>
        /// The low-order word for the given value.
        /// </returns>
        private static int GetLowWord(int value)
        {
            return (int)((short)(value & 65535));
        }

        /// <summary>
        /// Sets the internal state for the window with pending changes and attaches a
        /// event handler to handle the next time this window is rendered.
        /// </summary>
        private void BeginDelayedRender()
        {
            if (this._pendingDelayRender)
            {
                return;
            }

            this._pendingDelayRender = true;
            CompositionTarget.Rendering += this.CommitDelayedRender;
        }

        /// <summary>
        /// Cancels the pending delayed changes, so that nothing will change the next time
        /// this window is rendered, essentially undoing the commit changes.
        /// </summary>
        private void CancelDelayedRender()
        {
            if (!this._pendingDelayRender)
            {
                return;
            }

            this._pendingDelayRender = false;
            CompositionTarget.Rendering -= this.CommitDelayedRender;
        }

        /// <summary>
        /// Commits to the delayed rendered and re-renders this window based on the
        /// current properties.
        /// </summary>
        /// <param name="sender">
        /// The object that this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.EventArgs data for this event.
        /// </param>
        private void CommitDelayedRender(object sender, EventArgs e)
        {
            this.CancelDelayedRender();
            if (this.IsVisible)
            {
                this.RenderLayeredWindow();
            }
        }

        /// <summary>
        /// Draws this window based on the fact that it is currently docked either above or
        /// below the target window.
        /// </summary>
        /// <param name="dc">
        /// The drawing context to use while drawing.
        /// </param>
        /// <param name="top">
        /// A valid indicating whether to draw the top or bottom plane.
        /// </param>
        private void DrawPolar(GlowDrawingContext dc, bool top)
        {
            //// GD     x                        y
            //// --------------------------------------
            ////   | L  |         Middle         | R  |
            ////   |    |                        |    |
            //// --------------------------------------

            GlowBitmap left = null;
            GlowBitmap middle = null;
            GlowBitmap right = null;

            if (top)
            {
                left = this.GetGlowBitmap(dc, GlowBitmapPart.TopLeft);
                middle = this.GetGlowBitmap(dc, GlowBitmapPart.Top);
                right = this.GetGlowBitmap(dc, GlowBitmapPart.TopRight);
            }
            else
            {
                left = this.GetGlowBitmap(dc, GlowBitmapPart.BottomLeft);
                middle = this.GetGlowBitmap(dc, GlowBitmapPart.Bottom);
                right = this.GetGlowBitmap(dc, GlowBitmapPart.BottomRight);
            }

            int x = GlowDepth + left.Width;
            int y = dc.Width - GlowDepth - right.Width;
            int middleWidth = y - x;

            DrawPolarBitmap(dc, left, GlowDepth, left.Width);

            if (middleWidth > 0)
            {
                DrawPolarBitmap(dc, middle, x, middleWidth);
            }

            DrawPolarBitmap(dc, right, y, right.Width);
        }

        /// <summary>
        /// Draws this window based on the fact that it is currently docked to either the left
        /// side or right side of the target window.
        /// </summary>
        /// <param name="dc">
        /// The drawing context to use while drawing.
        /// </param>
        /// <param name="left">
        /// A valid indicating whether to draw the left or right side.
        /// </param>
        private void DrawSide(GlowDrawingContext dc, bool left)
        {
            //// 0 ------
            ////   |    |       Top Corner
            ////   |    |
            //// x ------
            ////   |    |       Top
            ////   |    |
            //// y ------
            ////   |    |
            ////   |    |
            ////   |    |
            ////   |    |       Middle
            ////   |    |
            ////   |    |
            ////   |    |
            //// z ------
            ////   |    |
            ////   |    |       Bottom
            //// a ------
            ////   |    |
            ////   |    |       Bottom Corner
            ////   ------

            GlowBitmap topCorner = null;
            GlowBitmap top = null;
            GlowBitmap middle = null;
            GlowBitmap bottom = null;
            GlowBitmap bottomCorner = null;

            if (left)
            {
                topCorner = this.GetGlowBitmap(dc, GlowBitmapPart.CornerTopLeft);
                top = this.GetGlowBitmap(dc, GlowBitmapPart.LeftTop);
                middle = this.GetGlowBitmap(dc, GlowBitmapPart.Left);
                bottom = this.GetGlowBitmap(dc, GlowBitmapPart.LeftBottom);
                bottomCorner = this.GetGlowBitmap(dc, GlowBitmapPart.CornerBottomLeft);
            }
            else
            {
                topCorner = this.GetGlowBitmap(dc, GlowBitmapPart.CornerTopRight);
                top = this.GetGlowBitmap(dc, GlowBitmapPart.RightTop);
                middle = this.GetGlowBitmap(dc, GlowBitmapPart.Right);
                bottom = this.GetGlowBitmap(dc, GlowBitmapPart.RightBottom);
                bottomCorner = this.GetGlowBitmap(dc, GlowBitmapPart.CornerBottomRight);
            }

            int x = topCorner.Height;
            int y = x + top.Height;
            int z = dc.Height - bottom.Height - bottomCorner.Height;
            int a = z + bottom.Height;
            int middleHeight = z - y;

            DrawSideBitmap(dc, topCorner, 0, topCorner.Height);
            DrawSideBitmap(dc, top, x, top.Height);

            if (middleHeight > 0)
            {
                DrawSideBitmap(dc, middle, y, middleHeight);
            }

            DrawSideBitmap(dc, bottom, z, bottom.Height);
            DrawSideBitmap(dc, bottomCorner, a, bottomCorner.Height);
        }

        /// <summary>
        /// Gets a glow bitmap associated with the specified drawing context and for the
        /// specified part.
        /// </summary>
        /// <param name="dc">
        /// The glow drawing context to use on the glow bitmap.
        /// </param>
        /// <param name="bitmapPart">
        /// The bitmap part that should be returned.
        /// </param>
        /// <returns>
        /// A glow bitmap using the specified drawing context that's for the specified
        /// bitmap part.
        /// </returns>
        private GlowBitmap GetGlowBitmap(GlowDrawingContext dc, GlowBitmapPart bitmapPart)
        {
            GlowBitmap[] array;
            Color color;
            if (this.IsActive)
            {
                array = this._activeGlowBitmaps;
                color = this.ActiveGlowColor;
            }
            else
            {
                array = this._inactiveGlowBitmaps;
                color = this.InactiveGlowColor;
            }

            if (array[(int)bitmapPart] == null)
            {
                array[(int)bitmapPart] = GlowBitmap.Create(dc, bitmapPart, color);
            }

            return array[(int)bitmapPart];
        }

        /// <summary>
        /// Clears the bitmap cache if the entries have become invalid.
        /// </summary>
        private void InvalidateCachedBitmaps()
        {
            if (this._invalidFields.HasFlag(InvalidFields.ActiveColor))
            {
                ClearCache(this._activeGlowBitmaps);
            }

            if (this._invalidFields.HasFlag(InvalidFields.InactiveColor))
            {
                ClearCache(this._inactiveGlowBitmaps);
            }
        }

        /// <summary>
        /// Rendered this window as a layered window based on the current orientation.
        /// </summary>
        private void RenderLayeredWindow()
        {
            using (GlowDrawingContext dc = new GlowDrawingContext(this.Width, this.Height))
            {
                switch (this._orientation)
                {
                    case Dock.Left:
                        this.DrawSide(dc, true);
                        break;
                    case Dock.Right:
                        this.DrawSide(dc, false);
                        break;
                    case Dock.Top:
                        this.DrawPolar(dc, true);
                        break;
                    default:
                        this.DrawPolar(dc, false);
                        break;
                }

                User32.UpdateLayeredWindow(this, dc.ScreenDC, dc.WindowDC, dc.Blend);
            }
        }

        /// <summary>
        /// Updated the window by setting up the delay render or rendering the window with
        /// the current parameters.
        /// </summary>
        private void UpdateLayeredWindowCore()
        {
            if (!this.IsVisible)
            {
                return;
            }

            if (!this._invalidFields.HasFlag(InvalidFields.Render))
            {
                return;
            }

            if (this.IsPositionValid)
            {
                this.BeginDelayedRender();
                return;
            }

            this.CancelDelayedRender();
            this.RenderLayeredWindow();
        }

        /// <summary>
        /// Sets the given field to the given value and making sure the correct commit
        /// logic is followed.
        /// </summary>
        /// <typeparam name="T">
        /// The type the property that is getting set is.
        /// </typeparam>
        /// <param name="field">
        /// A reference to the property that is getting set.
        /// </param>
        /// <param name="value">
        /// The value to set the property to.
        /// </param>
        /// <param name="invalidFields">
        /// The field types that have become invalid due to this change.
        /// </param>
        private void UpdateProperty<T>(ref T field, T value, InvalidFields invalidFields)
        {
            if (EqualityComparer<T>.Default.Equals(field, value))
            {
                return;
            }

            field = value;
            this._invalidFields |= invalidFields;

            if (!this.IsDeferringChanges)
            {
                this.CommitChanges();
            }
        }

        /// <summary>
        /// Updates the windows position, z-order, size and position flags based on the
        /// current properties and invalid fields value.
        /// </summary>
        private void UpdateWindowPosCore()
        {
            WindowPosFlags flags = WindowPosFlags.NOACTIVATE;
            flags |= WindowPosFlags.NOOWNERZORDER;
            flags |= WindowPosFlags.NOZORDER;

            bool changeMade = false;
            if (this._invalidFields.HasFlag(InvalidFields.Visibility))
            {
                changeMade = true;
                if (this.IsVisible)
                {
                    flags |= WindowPosFlags.SHOWWINDOW;
                }
                else
                {
                    flags |= WindowPosFlags.HIDEWINDOW;
                    flags |= WindowPosFlags.NOSIZE;
                    flags |= WindowPosFlags.NOMOVE;
                }
            }

            if (!this._invalidFields.HasFlag(InvalidFields.Location))
            {
                changeMade = true;
                flags |= WindowPosFlags.NOMOVE;
            }

            if (!this._invalidFields.HasFlag(InvalidFields.Size))
            {
                changeMade = true;
                flags |= WindowPosFlags.NOSIZE;
            }

            if (changeMade == false)
            {
                return;
            }

            User32.SetWindowPos(this, flags);
        }

        /// <summary>
        /// Performs a hit test from a windows message and returns a value based on the out
        /// come.
        /// </summary>
        /// <param name="longParam">
        /// The window message long additional data.
        /// </param>
        /// <returns>
        /// Returns a value based on what part of the window the mouse has hit.
        /// </returns>
        private int WindowMessageNcHitTest(IntPtr longParam)
        {
            if (this.CanBeUsedToResize == false)
            {
                return 0;
            }

            int mouseX = GetLowWord(longParam.ToInt32());
            int mouseY = GetHiWord(longParam.ToInt32());
            Rect rect = User32.GetWindowRect(this.Handle);

            switch (this._orientation)
            {
                case Dock.Left:
                    if (mouseY - CornerGripThickness < rect.Top)
                    {
                        return 13;
                    }

                    if (mouseY + CornerGripThickness > rect.Bottom)
                    {
                        return 16;
                    }

                    return 10;
                case Dock.Top:
                    if (mouseX - CornerGripThickness < rect.Left)
                    {
                        return 13;
                    }

                    if (mouseX + CornerGripThickness > rect.Right)
                    {
                        return 14;
                    }

                    return 12;
                case Dock.Right:
                    if (mouseY - CornerGripThickness < rect.Top)
                    {
                        return 14;
                    }

                    if (mouseY + CornerGripThickness > rect.Bottom)
                    {
                        return 17;
                    }

                    return 11;
                default:
                    if (mouseX - CornerGripThickness < rect.Left)
                    {
                        return 16;
                    }

                    if (mouseX + CornerGripThickness > rect.Right)
                    {
                        return 17;
                    }

                    return 15;
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.GlowWindow {Class}
} // RSG.Editor.Controls {Namespace}
