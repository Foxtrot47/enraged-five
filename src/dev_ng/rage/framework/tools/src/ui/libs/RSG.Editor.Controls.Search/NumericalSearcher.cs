﻿//---------------------------------------------------------------------------------------------
// <copyright file="NumericalSearcher.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Search
{
    /// <summary>
    /// Represents a searcher used to find a numerical value.
    /// </summary>
    public class NumericalSearcher : Searcher
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="SearchValue"/> property.
        /// </summary>
        private double _searchValue;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="NumericalSearcher"/> class.
        /// </summary>
        public NumericalSearcher()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the value that is being searched for.
        /// </summary>
        public double SearchValue
        {
            get { return this._searchValue; }
            set { this.SetProperty(ref this._searchValue, value); }
        }
        #endregion Properties
    } // RSG.Editor.Controls.Search.NumericalSearcher {Class}
} // RSG.Editor.Controls.Search {Namespace}
