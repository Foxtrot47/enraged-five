﻿//---------------------------------------------------------------------------------------------
// <copyright file="FileNodeInvokeController.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel.Controllers
{
    using System.Collections.Generic;
    using System.Windows;
    using RSG.Editor.View;

    /// <summary>
    /// A implementation of a invoke controller that can be added to a file node.
    /// </summary>
    public sealed class FileNodeInvokeController : IInvokeController
    {
        #region Methods
        /// <summary>
        /// Invokes a action on each item in the specified list.
        /// </summary>
        /// <param name="items">
        /// A list of items that should be invoked.
        /// </param>
        /// <param name="inputSource">
        /// The source of the input that has caused this invoke.
        /// </param>
        /// <param name="element">
        /// The input element that was the source for this invoke.
        /// </param>
        /// <returns>
        /// True if any of the specified items have been invoked successfully.
        /// </returns>
        public bool Invoke(
            IEnumerable<object> items, InputSource inputSource, IInputElement element)
        {
            if (ProjectCommands.OpenFileFromProject.CanExecute(null, element))
            {
                ProjectCommands.OpenFileFromProject.Execute(null, element);
            }

            return true;
        }
        #endregion Methods
    } // RSG.Project.ViewModel.Controllers.FileNodeInvokeController {Interface}
} // RSG.Project.ViewModel.Controllers {Namespace}
