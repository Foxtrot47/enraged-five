﻿//---------------------------------------------------------------------------------------------
// <copyright file="VisualTreeExtensions.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Helpers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using System.Windows.Media;
    using RSG.Editor.Controls.AttachedDependencyProperties;

    /// <summary>
    /// Static class used to add extensions onto dependency objects to do with the visual tree.
    /// </summary>
    public static class VisualTreeExtensions
    {
        #region Methods
        /// <summary>
        /// Gets the first <see cref="IInitialFocusStealer"/> object that's a descendent of the
        /// specified dependency object.
        /// </summary>
        /// <param name="d">
        /// The dependency object which is the root of the search.
        /// </param>
        /// <returns>
        /// The first <see cref="IInitialFocusStealer"/> object that's a descendent of the
        /// specified dependency object.
        /// </returns>
        public static IInitialFocusStealer GetFocusStealer(this DependencyObject d)
        {
            foreach (DependencyObject descendent in d.GetVisualDescendents<DependencyObject>())
            {
                IInitialFocusStealer stealer = descendent as IInitialFocusStealer;
                if (stealer != null)
                {
                    return stealer;
                }
            }

            return null;
        }

        /// <summary>
        /// Creates an Enumerable that can be used to get all or loop through all instances of
        /// a specific type in the visual tree below this dependency object that are
        /// serialisable.
        /// </summary>
        /// <param name="d">
        /// The dependency object to use as the starting root visual for the search.
        /// </param>
        /// <returns>
        /// Returns an Enumerable that can be used to get all or loop through all instances of
        /// a specific type in the visual tree below this dependency object that are
        /// serialisable.
        /// </returns>
        public static IEnumerable<DependencyObject> GetSerialisableDescendents(
            this DependencyObject d)
        {
            if (d == null)
            {
                yield break;
            }

            int childCount = VisualTreeHelper.GetChildrenCount(d);
            for (int i = 0; i < childCount; i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(d, i);
                if (SerialisationProperties.GetSerialise(child))
                {
                    yield return child;
                    continue;
                }

                foreach (DependencyObject match in GetSerialisableDescendents(child))
                {
                    yield return match;
                }
            }

            yield break;
        }

        /// <summary>
        /// Finds the first occurrence of the qualifying type by looking up through the visual
        /// tree.
        /// </summary>
        /// <typeparam name="T">
        /// The type of visual that you want to find  in the visual tree.
        /// </typeparam>
        /// <param name="d">
        /// The dependency object to use as the starting root visual for the search.
        /// </param>
        /// <returns>
        /// A instance to the first occurrence of the qualifying type.
        /// </returns>
        public static T GetVisualAncestor<T>(this DependencyObject d)
            where T : DependencyObject
        {
            return d.GetVisualAncestors<T>(false).FirstOrDefault();
        }

        /// <summary>
        /// Finds the first occurrence of the qualifying type by looking up through the visual
        /// tree.
        /// </summary>
        /// <typeparam name="T">
        /// The type of visual that you want to find  in the visual tree.
        /// </typeparam>
        /// <param name="d">
        /// The dependency object to use as the starting root visual for the search.
        /// </param>
        /// <returns>
        /// A instance to the first occurrence of the qualifying type.
        /// </returns>
        public static T GetVisualAncestorOrSelf<T>(this DependencyObject d)
            where T : DependencyObject
        {
            return d.GetVisualAncestors<T>(true).FirstOrDefault();
        }

        /// <summary>
        /// Creates an Enumerable that can be used to get all or loop through all instances of
        /// a specific type in the visual tree above this dependency object.
        /// </summary>
        /// <typeparam name="T">
        /// The type of visual that you want to find in the visual tree.
        /// </typeparam>
        /// <param name="d">
        /// The dependency object to use as the starting root visual for the search.
        /// </param>
        /// <param name="self">
        /// A value indicating whether the specified root should be included in the search.
        /// </param>
        /// <returns>
        /// Returns an Enumerable that can be used to get all or loop through all instances of
        /// a specific type in the visual tree above this dependency object.
        /// </returns>
        public static IEnumerable<T> GetVisualAncestors<T>(this DependencyObject d, bool self)
            where T : DependencyObject
        {
            if (d == null)
            {
                yield break;
            }

            if (self)
            {
                if (d is T)
                {
                    yield return (T)d;
                }
            }

            DependencyObject parent = VisualTreeHelper.GetParent(d);
            if (parent != null)
            {
                if (parent is T)
                {
                    yield return (T)parent;
                }

                foreach (T match in GetVisualAncestors<T>(parent, false))
                {
                    yield return match;
                }
            }

            yield break;
        }

        /// <summary>
        /// Finds the first occurrence of the qualifying type by looking down through the
        /// visual tree.
        /// </summary>
        /// <typeparam name="T">
        /// The type of visual that you want to find  in the visual tree.
        /// </typeparam>
        /// <param name="d">
        /// The dependency object to use as the starting root visual for the search.
        /// </param>
        /// <returns>
        /// A instance to the first occurrence of the qualifying type.
        /// </returns>
        public static T GetVisualDescendent<T>(this DependencyObject d)
            where T : DependencyObject
        {
            return d.GetVisualDescendents<T>().FirstOrDefault();
        }

        /// <summary>
        /// Finds the first occurrence of the qualifying type by looking down through the
        /// visual tree.
        /// </summary>
        /// <typeparam name="T">
        /// The type of visual that you want to find  in the visual tree.
        /// </typeparam>
        /// <param name="d">
        /// The dependency object to use as the starting root visual for the search.
        /// </param>
        /// <returns>
        /// A instance to the first occurrence of the qualifying type.
        /// </returns>
        public static T GetVisualDescendentOrSelf<T>(this DependencyObject d)
            where T : DependencyObject
        {
            return d.GetVisualDescendents<T>(true).FirstOrDefault();
        }

        /// <summary>
        /// Creates an Enumerable that can be used to get all or loop through all instances of
        /// a specific type in the visual tree below this dependency object.
        /// </summary>
        /// <typeparam name="T">
        /// The type of visual that you want to find in the visual tree.
        /// </typeparam>
        /// <param name="d">
        /// The dependency object to use as the starting root visual for the search.
        /// </param>
        /// <returns>
        /// Returns an Enumerable that can be used to get all or loop through all instances of
        /// a specific type in the visual tree below this dependency object.
        /// </returns>
        public static IEnumerable<T> GetVisualDescendents<T>(this DependencyObject d)
            where T : DependencyObject
        {
            return d.GetVisualDescendents<T>(false);
        }

        /// <summary>
        /// Creates an Enumerable that can be used to get all or loop through all instances of
        /// a specific type in the visual tree below this dependency object.
        /// </summary>
        /// <typeparam name="T">
        /// The type of visual that you want to find in the visual tree.
        /// </typeparam>
        /// <param name="d">
        /// The dependency object to use as the starting root visual for the search.
        /// </param>
        /// <param name="self">
        /// A value indicating whether this should return the specified root object is of type
        /// T as well.
        /// </param>
        /// <returns>
        /// Returns an Enumerable that can be used to get all or loop through all instances of
        /// a specific type in the visual tree below this dependency object.
        /// </returns>
        public static IEnumerable<T> GetVisualDescendents<T>(
            this DependencyObject d, bool self) where T : DependencyObject
        {
            if (d == null)
            {
                yield break;
            }

            if (self)
            {
                if (d is T)
                {
                    yield return (T)d;
                }
            }

            int childCount = VisualTreeHelper.GetChildrenCount(d);
            for (int i = 0; i < childCount; i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(d, i);
                if (child is T)
                {
                    yield return (T)child;
                }

                foreach (T match in GetVisualDescendents<T>(child))
                {
                    yield return match;
                }
            }

            yield break;
        }

        /// <summary>
        /// Determines whether the dependency object is connect to a presentation source.
        /// </summary>
        /// <param name="d">
        /// The dependency object to test.
        /// </param>
        /// <returns>
        /// True if the dependency object is connect to a presentation source; otherwise,
        /// false.
        /// </returns>
        public static bool IsConnectedToPresentationSource(this DependencyObject d)
        {
            return PresentationSource.FromDependencyObject(d) != null;
        }

        /// <summary>
        /// Determines whether the specified node is a descendant of this dependency object.
        /// </summary>
        /// <param name="d">
        /// The dependency object to use as the starting root visual for the search.
        /// </param>
        /// <param name="node">
        /// The node to determine whether its a descendant or not.
        /// </param>
        /// <returns>
        /// True if the specified node is a descendant of this dependency object; otherwise,
        /// false.
        /// </returns>
        public static bool HasDescendant(this DependencyObject d, DependencyObject node)
        {
            DependencyObject current = node;
            while (current != null)
            {
                if (object.ReferenceEquals(current, d))
                {
                    return true;
                }

                current = current.GetLogicalOrVisualParent();
            }

            return false;
        }
        #endregion Methods
    } // RSG.Editor.Controls.Helpers.ValueConverter {Class}
} // RSG.Editor.Controls.Helpers {Namespace}
