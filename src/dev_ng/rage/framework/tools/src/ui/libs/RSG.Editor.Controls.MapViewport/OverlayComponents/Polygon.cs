﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace RSG.Editor.Controls.MapViewport.OverlayComponents
{
    /// <summary>
    /// 
    /// </summary>
    public class Polygon : Shape
    {
        #region Fields
        /// <summary>
        /// 
        /// </summary>
        private Rect _worldExtents;

        /// <summary>
        /// Private field for the <see cref="Points"/> property.
        /// </summary>
        private PointCollection _points;
        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="points"></param>
        public Polygon(IEnumerable<Point> points)
        {
            _points = new PointCollection(points.Select(item => new Point(item.X, item.Y * -1.0)));

            _worldExtents = Rect.Empty;
            foreach (Point point in points)
            {
                _worldExtents.Union(point);
            }
        }
        #endregion

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public PointCollection Points
        {
            get { return _points; }
        }

        /// <summary>
        /// Position of this component in world space.
        /// </summary>
        public override Point Position
        {
            get { return _worldExtents.BottomLeft; }
            set { throw new NotSupportedException(); }
        }

        /// <summary>
        /// Where the position relates to in terms of the component's bounds.
        /// </summary>
        public override PositionOrigin PositionOrigin
        {
            get { return PositionOrigin.TopLeft; }
            set { throw new NotSupportedException(); }
        }

        /// <summary>
        /// The extents of the image (in world coordinates).
        /// </summary>
        public Rect WorldExtents
        {
            get { return _worldExtents; }
            set { SetProperty(ref _worldExtents, value, "WorldExtents", "Position"); }
        }
        #endregion
    }
}
