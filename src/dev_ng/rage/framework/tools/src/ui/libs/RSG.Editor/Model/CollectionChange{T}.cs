﻿//---------------------------------------------------------------------------------------------
// <copyright file="CollectionChange{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Model
{
    using System.Collections.Generic;
    using System.Collections.Specialized;

    /// <summary>
    /// Implements the <see cref="RSG.Editor.Model.IChange"/> interface for a single collection
    /// change.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the item that is contained in the collection that has changed.
    /// </typeparam>
    internal class CollectionChange<T> : ChangeBase
    {
        #region Fields
        /// <summary>
        /// The action that was performed to change the collection.
        /// </summary>
        private NotifyCollectionChangedAction _action;

        /// <summary>
        /// A private reference to the collection that has changed.
        /// </summary>
        private ModelCollection<T> _collection;

        /// <summary>
        /// The starting index in the collection that any new items were added at.
        /// </summary>
        private int _newIndex;

        /// <summary>
        /// A list of items that were added to the collection during the change.
        /// </summary>
        private List<T> _newItems;

        /// <summary>
        /// The starting index in the collection that any items that were removed started at.
        /// </summary>
        private int _oldIndex;

        /// <summary>
        /// A list of items that were removed from the collection during the change.
        /// </summary>
        private List<T> _oldItems;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CollectionChange{T}"/> class with the
        /// specified action on the specified items at the specified index.
        /// </summary>
        /// <param name="collection">
        /// The reference to the collection that this change is on.
        /// </param>
        /// <param name="args">
        /// A instance of the argument class that is used to describe the change that has
        /// occurred.
        /// </param>
        public CollectionChange(
            ModelCollection<T> collection, NotifyCollectionChangedEventArgs args)
        {
            if (args == null)
            {
                throw new SmartArgumentNullException(() => args);
            }

            this._collection = collection;
            this._action = args.Action;
            this._newIndex = args.NewStartingIndex;
            this._oldIndex = args.OldStartingIndex;
            this._newItems = new List<T>();
            this._oldItems = new List<T>();

            if (args.NewItems != null)
            {
                foreach (object item in args.NewItems)
                {
                    if (item is T)
                    {
                        this._newItems.Add((T)item);
                    }
                }
            }

            if (args.OldItems != null)
            {
                foreach (object item in args.OldItems)
                {
                    if (item is T)
                    {
                        this._oldItems.Add((T)item);
                    }
                }
            }
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Redoes this single change.
        /// </summary>
        public override void Redo()
        {
            switch (this._action)
            {
                case NotifyCollectionChangedAction.Add:
                    if (this._newIndex == -1)
                    {
                        return;
                    }
                    else
                    {
                        for (int i = 0; i < this._newItems.Count; i++)
                        {
                            this._collection.Insert(this._newIndex + i, this._newItems[i]);
                        }
                    }

                    break;
                case NotifyCollectionChangedAction.Move:
                    for (int i = 0; i < this._newItems.Count; i++)
                    {
                        this._collection.RemoveAt(this._oldIndex + i);
                    }

                    for (int i = 0; i < this._newItems.Count; i++)
                    {
                        this._collection.Insert(this._newIndex + i, this._newItems[i]);
                    }

                    break;
                case NotifyCollectionChangedAction.Remove:
                    if (this._oldIndex == -1)
                    {
                        return;
                    }
                    else
                    {
                        for (int i = 0; i < this._oldItems.Count; i++)
                        {
                            this._collection.RemoveAt(this._oldIndex);
                        }
                    }

                    break;
                case NotifyCollectionChangedAction.Replace:
                    this._collection[this._newIndex] = this._newItems[0];
                    break;
                case NotifyCollectionChangedAction.Reset:
                    break;
            }
        }

        /// <summary>
        /// Undoes this single change.
        /// </summary>
        public override void Undo()
        {
            switch (this._action)
            {
                case NotifyCollectionChangedAction.Add:
                    if (this._newIndex == -1)
                    {
                        return;
                    }
                    else
                    {
                        for (int i = 0; i < this._newItems.Count; i++)
                        {
                            this._collection.RemoveAt(this._newIndex);
                        }
                    }

                    break;
                case NotifyCollectionChangedAction.Move:
                    for (int i = 0; i < this._newItems.Count; i++)
                    {
                        this._collection.RemoveAt(this._newIndex + i);
                    }

                    for (int i = 0; i < this._newItems.Count; i++)
                    {
                        this._collection.Insert(this._oldIndex + i, this._newItems[i]);
                    }

                    break;
                case NotifyCollectionChangedAction.Remove:
                    if (this._oldIndex == -1)
                    {
                        return;
                    }
                    else
                    {
                        for (int i = 0; i < this._oldItems.Count; i++)
                        {
                            this._collection.Insert(this._oldIndex + i, this._oldItems[i]);
                        }
                    }

                    break;
                case NotifyCollectionChangedAction.Replace:
                    this._collection[this._newIndex] = this._oldItems[0];
                    break;
                case NotifyCollectionChangedAction.Reset:
                    break;
            }
        }
        #endregion Methods
    } // RSG.Editor.Model.CollectionChange {Class}
} // RSG.Editor.Model {Namespace}
