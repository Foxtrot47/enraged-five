﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsSpinAnimationControl.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Documents;
    using System.Windows.Media;
    using System.Windows.Media.Animation;
    using System.Windows.Threading;
    using RSG.Editor.Controls.Helpers;

    /// <summary>
    /// Represents a spinner control that animations.
    /// </summary>
    public sealed class RsSpinAnimationControl : FrameworkElement
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="ControlPointCount" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty ControlPointCountProperty;

        /// <summary>
        /// Identifies the <see cref="ControlPointRadius" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty ControlPointRadiusProperty;

        /// <summary>
        /// Identifies the <see cref="IsClockwise" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsClockwiseProperty;

        /// <summary>
        /// Identifies the <see cref="IsSpinning" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsSpinningProperty;

        /// <summary>
        /// Identifies the <see cref="RotationPeriod" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty RotationPeriodProperty;

        /// <summary>
        /// The arranged size for this control.
        /// </summary>
        private Size _arrangedSize;

        /// <summary>
        /// The main dispatcher to use to invoke commands on a background thread.
        /// </summary>
        private Dispatcher _backgroundDispatcher;

        /// <summary>
        /// The presentation source wrapper used for the contain element.
        /// </summary>
        private VisualTargetPresentationSource _backgroundPresentationSource;

        /// <summary>
        /// The private field used for the <see cref="Child"/> property.
        /// </summary>
        private Visual _child;

        /// <summary>
        /// The root element contained within this control.
        /// </summary>
        private UIElement _containedElement;

        /// <summary>
        /// A value indicating whether the background dispatcher and contained element have
        /// been initialised.
        /// </summary>
        private bool _initialised;

        /// <summary>
        /// The measured size for this control.
        /// </summary>
        private Size _measuredSize;

        /// <summary>
        /// A dictionary of cached property values that should be passed to the contain
        /// element whenever it is created.
        /// </summary>
        private Dictionary<DependencyProperty, object> _propertyCache;

        /// <summary>
        /// A generic object used to lock collections to supporting threading.
        /// </summary>
        private object _syncObject;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsSpinAnimationControl"/> class.
        /// </summary>
        static RsSpinAnimationControl()
        {
            ControlPointCountProperty =
                DependencyProperty.Register(
                "ControlPointCount",
                typeof(int),
                typeof(RsSpinAnimationControl),
                new FrameworkPropertyMetadata(25));

            ControlPointRadiusProperty =
                DependencyProperty.Register(
                "ControlPointRadius",
                typeof(double),
                typeof(RsSpinAnimationControl),
                new FrameworkPropertyMetadata(1.0));

            IsClockwiseProperty =
                DependencyProperty.Register(
                "IsClockwise",
                typeof(bool),
                typeof(RsSpinAnimationControl),
                new FrameworkPropertyMetadata(true));

            RotationPeriodProperty =
                DependencyProperty.Register(
                "RotationPeriod",
                typeof(TimeSpan),
                typeof(RsSpinAnimationControl),
                new FrameworkPropertyMetadata(TimeSpan.FromMilliseconds(750.0)));

            IsSpinningProperty =
                DependencyProperty.Register(
                "IsSpinning",
                typeof(bool),
                typeof(RsSpinAnimationControl),
                new FrameworkPropertyMetadata(false));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsSpinAnimationControl"/> class.
        /// </summary>
        public RsSpinAnimationControl()
        {
            this.Child = new HostVisual();
            PresentationSource.AddSourceChangedHandler(
                this, new SourceChangedEventHandler(this.OnPresentationSourceChanged));

            this._syncObject = new object();
            this._measuredSize = Size.Empty;
            this._arrangedSize = Size.Empty;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the visual child underneath this framework element.
        /// </summary>
        public Visual Child
        {
            get
            {
                return this._child;
            }

            set
            {
                if (this._child == value)
                {
                    return;
                }

                if (this._child != null)
                {
                    this.RemoveVisualChild(this._child);
                }

                this._child = value;
                if (this._child != null)
                {
                    this.AddVisualChild(this._child);
                }
            }
        }

        /// <summary>
        /// Gets or sets the number of unique points the animation stops at.
        /// </summary>
        public int ControlPointCount
        {
            get { return (int)this.GetValue(ControlPointCountProperty); }
            set { this.SetValue(ControlPointCountProperty, value); }
        }

        /// <summary>
        /// Gets or sets the radius for the spinner.
        /// </summary>
        public double ControlPointRadius
        {
            get { return (double)this.GetValue(ControlPointRadiusProperty); }
            set { this.SetValue(ControlPointRadiusProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the spinning animation spins clockwise or
        /// anti-clockwise.
        /// </summary>
        public bool IsClockwise
        {
            get { return (bool)this.GetValue(IsClockwiseProperty); }
            set { this.SetValue(IsClockwiseProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this spinning control is currently
        /// spinning.
        /// </summary>
        public bool IsSpinning
        {
            get { return (bool)this.GetValue(IsSpinningProperty); }
            set { this.SetValue(IsSpinningProperty, value); }
        }

        /// <summary>
        /// Gets or sets a time span that defines the duration of the animation.
        /// </summary>
        public TimeSpan RotationPeriod
        {
            get { return (TimeSpan)this.GetValue(RotationPeriodProperty); }
            set { this.SetValue(RotationPeriodProperty, value); }
        }

        /// <summary>
        /// Gets the number of visual child elements within this element.
        /// </summary>
        protected override int VisualChildrenCount
        {
            get
            {
                if (this._child == null)
                {
                    return 0;
                }

                return 1;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Arranges the content.
        /// </summary>
        /// <param name="finalSize">
        /// The System.Windows.Size this element uses to arrange its child content.
        /// </param>
        /// <returns>
        /// The System.Windows.Size that represents the arranged size of this element and
        /// its child.
        /// </returns>
        protected override Size ArrangeOverride(Size finalSize)
        {
            lock (this._syncObject)
            {
                if (this._containedElement == null)
                {
                    this._arrangedSize = finalSize;
                    return finalSize;
                }
            }

            this._containedElement.Dispatcher.BeginInvoke(
                new Action<Size>(this.InnerArrange),
                DispatcherPriority.Render,
                new object[1] { finalSize });

            return finalSize;
        }

        /// <summary>
        /// Gets a child at the specified index from a collection of child elements.
        /// </summary>
        /// <param name="index">
        /// The zero-based index of the requested child element in the collection.
        /// </param>
        /// <returns>
        /// The requested child element.
        /// </returns>
        protected override Visual GetVisualChild(int index)
        {
            if (this._child == null || index != 0)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            return this._child;
        }

        /// <summary>
        /// Supply base element hit testing behaviour.
        /// </summary>
        /// <param name="hitTestParameters">
        /// Describes the hit test to perform, including the initial hit point.
        /// </param>
        /// <returns>
        /// Results of the test, including the evaluated point.
        /// </returns>
        protected override HitTestResult HitTestCore(PointHitTestParameters hitTestParameters)
        {
            return new PointHitTestResult(this.Child, hitTestParameters.HitPoint);
        }

        /// <summary>
        /// Measures the child element of this element to prepare for arranging it during the
        /// ArrangeOverride pass.
        /// </summary>
        /// <param name="availableSize">
        /// An upper limit System.Windows.Size that should not be exceeded.
        /// </param>
        /// <returns>
        /// The target System.Windows.Size of the element.
        /// </returns>
        protected override Size MeasureOverride(Size availableSize)
        {
            lock (this._syncObject)
            {
                if (this._containedElement == null)
                {
                    this._measuredSize = availableSize;
                    return Size.Empty;
                }
            }

            this._containedElement.Dispatcher.BeginInvoke(
                new Action<Size>(this.InnerMeasure),
                DispatcherPriority.Render,
                new object[1] { availableSize });

            return this._containedElement.DesiredSize;
        }

        /// <summary>
        /// Invoked whenever the effective value of any dependency property on this control
        /// has changed.
        /// </summary>
        /// <param name="e">
        /// The event data that describes the property that changed, as well as old and new
        /// values.
        /// </param>
        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
            if (this.ShouldForwardPropertyChange(e))
            {
                this.ForwardPropertyChange(e.Property, e.NewValue);
            }
        }

        /// <summary>
        /// Connects the contained element and child visual to a new presentation source.
        /// </summary>
        private void ConnectHostedPresentationSource()
        {
            this._backgroundPresentationSource =
                new VisualTargetPresentationSource((HostVisual)this.Child);
            this._backgroundPresentationSource.RootVisual = this._containedElement;
        }

        /// <summary>
        /// Connects the contained element and child visual to a new presentation source.
        /// </summary>
        private void ConnectHostedVisualToSourceWorker()
        {
            this._backgroundDispatcher.VerifyAccess();
            this.ConnectHostedPresentationSource();
        }

        /// <summary>
        /// Creates a new hosted visual, forwards the animation properties to it and connects
        /// it to its presentation source.
        /// </summary>
        private void CreateHostedVisualWorker()
        {
            this._backgroundDispatcher.VerifyAccess();
            UIElement newRootElement = this.CreateRootUIElement();
            lock (this._syncObject)
            {
                this._containedElement = newRootElement;
                if (!this._measuredSize.IsEmpty)
                {
                    this.InnerMeasure(this._measuredSize);
                }

                if (!this._arrangedSize.IsEmpty)
                {
                    this.InnerArrange(this._arrangedSize);
                }

                this.ForwardCachedProperties(this._containedElement);
            }

            this.ConnectHostedPresentationSource();
        }

        /// <summary>
        /// Creates the root UI Element for the spin animation control.
        /// </summary>
        /// <returns>
        /// The root UI Element for this control.
        /// </returns>
        private UIElement CreateRootUIElement()
        {
            return new SpinAnimationRenderer();
        }

        /// <summary>
        /// Disconnects the contained element and child visual for this control from its
        /// presentation source by destroying the presentation source.
        /// </summary>
        private void DisconnectHostedVisualFromSourceWorker()
        {
            if (this._backgroundDispatcher != null)
            {
                this._backgroundDispatcher.VerifyAccess();
            }

            if (this._backgroundPresentationSource != null)
            {
                using (this._backgroundPresentationSource)
                {
                    this._backgroundPresentationSource = null;
                }
            }
        }

        /// <summary>
        /// Forwards the cached animation values for the dependency properties to the specified
        /// visual.
        /// </summary>
        /// <param name="visual">
        /// The visual to forward the cached properties onto.
        /// </param>
        private void ForwardCachedProperties(Visual visual)
        {
            if (this._propertyCache == null)
            {
                return;
            }

            foreach (KeyValuePair<DependencyProperty, object> current in this._propertyCache)
            {
                visual.SetValue(current.Key, current.Value);
            }

            this._propertyCache.Clear();
            this._propertyCache = null;
        }

        /// <summary>
        /// Forwards the property changed event to the contained element by setting its value
        /// to the changed new value.
        /// </summary>
        /// <param name="dp">
        /// The dependency property that changed.
        /// </param>
        /// <param name="newValue">
        /// The value the dependency property should be changed to.
        /// </param>
        private void ForwardPropertyChange(DependencyProperty dp, object newValue)
        {
            lock (this._syncObject)
            {
                if (this._containedElement == null)
                {
                    if (this._propertyCache == null)
                    {
                        this._propertyCache = new Dictionary<DependencyProperty, object>();
                    }

                    if (newValue == DependencyProperty.UnsetValue)
                    {
                        this._propertyCache.Remove(dp);
                    }
                    else
                    {
                        this._propertyCache[dp] = newValue;
                    }
                }
                else
                {
                    this._containedElement.Dispatcher.BeginInvoke(
                        new Action<DependencyProperty, object>(
                            this._containedElement.SetValue),
                        DispatcherPriority.DataBind,
                        new object[2] { dp, newValue });
                }
            }
        }

        /// <summary>
        /// Arranges the content.
        /// </summary>
        /// <param name="finalSize">
        /// The System.Windows.Size this element uses to arrange its child content.
        /// </param>
        private void InnerArrange(Size finalSize)
        {
            Size renderSize = this._containedElement.RenderSize;
            this._containedElement.Arrange(new Rect(finalSize));
            if (renderSize != this._containedElement.RenderSize)
            {
                this.Dispatcher.BeginInvoke(
                    new Action(this.InvalidateVisual),
                    DispatcherPriority.Render,
                    new object[0]);
            }
        }

        /// <summary>
        /// Measures the child element of this element to prepare for arranging it during the
        /// ArrangeOverride pass.
        /// </summary>
        /// <param name="availableSize">
        /// An upper limit System.Windows.Size that should not be exceeded.
        /// </param>
        private void InnerMeasure(Size availableSize)
        {
            Size desiredSize = this._containedElement.DesiredSize;
            this._containedElement.Measure(availableSize);
            if (desiredSize != this._containedElement.DesiredSize)
            {
                this.Dispatcher.BeginInvoke(
                    new Action(this.InvalidateMeasure),
                    DispatcherPriority.Render,
                    new object[0]);
            }
        }

        /// <summary>
        /// Called whenever this controls presentation source changes.
        /// </summary>
        /// <param name="sender">
        /// The object that this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.SourceChangedEventArgs data used for this event.
        /// </param>
        private void OnPresentationSourceChanged(object sender, SourceChangedEventArgs e)
        {
            if (e.OldSource != null && e.NewSource != null)
            {
                return;
            }

            if (e.OldSource != null)
            {
                if (this._backgroundDispatcher != null)
                {
                    this._backgroundDispatcher.BeginInvoke(
                        new Action(this.DisconnectHostedVisualFromSourceWorker),
                        new object[0]);

                    return;
                }
            }
            else
            {
                if (e.NewSource != null)
                {
                    if (!this._initialised)
                    {
                        this._initialised = true;
                        if (this._backgroundDispatcher == null)
                        {
                            this._backgroundDispatcher =
                                BackgroundDispatcher.GetBackgroundDispatcher(
                                "SpinAnimationRenderer", 0);
                        }

                        this._backgroundDispatcher.BeginInvoke(
                            new Action(this.CreateHostedVisualWorker), new object[0]);
                        return;
                    }

                    this._backgroundDispatcher.BeginInvoke(
                        new Action(this.ConnectHostedVisualToSourceWorker), new object[0]);
                }
            }
        }

        /// <summary>
        /// Determines whether a changed property should be passed forward to the contained
        /// element.
        /// </summary>
        /// <param name="e">
        /// The event data that describes the property that changed, as well as old and new
        /// values.
        /// </param>
        /// <returns>
        /// True if the property change should be passed forward; otherwise, false.
        /// </returns>
        private bool ShouldForwardPropertyChange(DependencyPropertyChangedEventArgs e)
        {
            bool forward = e.Property == RsSpinAnimationControl.ControlPointCountProperty ||
                e.Property == RsSpinAnimationControl.ControlPointRadiusProperty ||
                e.Property == RsSpinAnimationControl.IsClockwiseProperty ||
                e.Property == RsSpinAnimationControl.RotationPeriodProperty ||
                e.Property == RsSpinAnimationControl.IsSpinningProperty;

            if (!forward)
            {
                if (e.Property.ReadOnly)
                {
                    return false;
                }

                FrameworkPropertyMetadata frameworkPropertyMetadata =
                    e.Property.GetMetadata(
                    typeof(FrameworkElement)) as FrameworkPropertyMetadata;
                return frameworkPropertyMetadata != null && frameworkPropertyMetadata.Inherits;
            }

            return true;
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// Defines a presentation wrapper around a visual target.
        /// </summary>
        public class VisualTargetPresentationSource : PresentationSource, IDisposable
        {
            #region Fields
            /// <summary>
            /// The private field used for the <see cref="RootVisual"/> property.
            /// </summary>
            private VisualTarget _visualTarget;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="VisualTargetPresentationSource" />
            /// class.
            /// </summary>
            /// <param name="hostVisual">
            /// The target for rendering the visual.
            /// </param>
            public VisualTargetPresentationSource(HostVisual hostVisual)
            {
                if (hostVisual == null)
                {
                    throw new SmartArgumentNullException(() => hostVisual);
                }

                this._visualTarget = new VisualTarget(hostVisual);
            }

            /// <summary>
            /// Finalises an instance of the <see cref="VisualTargetPresentationSource" />
            /// class.
            /// </summary>
            ~VisualTargetPresentationSource()
            {
                this.Dispose(false);
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets a value indicating whether this instance has been disposed of.
            /// </summary>
            public override bool IsDisposed
            {
                get { return this._visualTarget == null; }
            }

            /// <summary>
            /// Gets or sets the root visual being presented in this source.
            /// </summary>
            public override Visual RootVisual
            {
                get
                {
                    return this._visualTarget.RootVisual;
                }

                set
                {
                    Visual rootVisual = this._visualTarget.RootVisual;
                    if (rootVisual != value)
                    {
                        this._visualTarget.RootVisual = value;
                        this.RootChanged(rootVisual, value);
                    }
                }
            }
            #endregion Properties

            #region Methods
            /// <summary>
            /// Performs application-defined tasks associated with freeing, releasing, or
            /// resetting unmanaged resources.
            /// </summary>
            public void Dispose()
            {
                this.Dispose(true);
                GC.SuppressFinalize(this);
            }

            /// <summary>
            /// Returns a visual target for the given source.
            /// </summary>
            /// <returns>
            /// The target for rendering the visual.
            /// </returns>
            protected override CompositionTarget GetCompositionTargetCore()
            {
                return this._visualTarget;
            }

            /// <summary>
            /// Performs application-defined tasks associated with freeing, releasing, or
            /// resetting unmanaged resources.
            /// </summary>
            /// <param name="disposeManaged">
            /// If true the managed resources for this instance also get disposed of as well as
            /// the unmanaged resources.
            /// </param>
            private void Dispose(bool disposeManaged)
            {
                if (disposeManaged)
                {
                    using (this._visualTarget)
                    {
                        this._visualTarget = null;
                    }
                }
            }
            #endregion Methods
        } // SpinAnimationControl.VisualTargetPresentationSource {Class}

        /// <summary>
        /// Defines the framework element that represents the spin ellipse animation control.
        /// </summary>
        private class SpinAnimationRenderer : FrameworkElement
        {
            #region Fields
            /// <summary>
            /// Identifies the <see cref="ControlPointCount" /> dependency property.
            /// </summary>
            public static readonly DependencyProperty ControlPointCountProperty;

            /// <summary>
            /// Identifies the <see cref="ControlPointRadius" /> dependency property.
            /// </summary>
            public static readonly DependencyProperty ControlPointRadiusProperty;

            /// <summary>
            /// Identifies the <see cref="IsClockwise" /> dependency property.
            /// </summary>
            public static readonly DependencyProperty IsClockwiseProperty;

            /// <summary>
            /// Identifies the <see cref="IsSpinning" /> dependency property.
            /// </summary>
            public static readonly DependencyProperty IsSpinningProperty;

            /// <summary>
            /// Identifies the <see cref="RotationPeriod" /> dependency property.
            /// </summary>
            public static readonly DependencyProperty RotationPeriodProperty;

            /// <summary>
            /// Identifies the <see cref="AnimationIndex" /> dependency property.
            /// </summary>
            private static readonly DependencyProperty _animationIndexProperty;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises static members of the <see cref="SpinAnimationRenderer"/> class.
            /// </summary>
            static SpinAnimationRenderer()
            {
                ControlPointCountProperty =
                    RsSpinAnimationControl.ControlPointCountProperty.AddOwner(
                    typeof(SpinAnimationRenderer),
                    new FrameworkPropertyMetadata(
                        25,
                        FrameworkPropertyMetadataOptions.AffectsRender,
                        new PropertyChangedCallback(
                            SpinAnimationRenderer.OnAnimationPropertyChanged)));

                ControlPointRadiusProperty =
                    RsSpinAnimationControl.ControlPointRadiusProperty.AddOwner(
                    typeof(SpinAnimationRenderer),
                    new FrameworkPropertyMetadata(
                        1.0, FrameworkPropertyMetadataOptions.AffectsRender));

                IsClockwiseProperty =
                    RsSpinAnimationControl.IsClockwiseProperty.AddOwner(
                    typeof(SpinAnimationRenderer),
                    new FrameworkPropertyMetadata(
                        true, FrameworkPropertyMetadataOptions.AffectsRender));

                RotationPeriodProperty =
                    RsSpinAnimationControl.RotationPeriodProperty.AddOwner(
                    typeof(SpinAnimationRenderer),
                    new FrameworkPropertyMetadata(
                        TimeSpan.FromMilliseconds(750.0),
                        FrameworkPropertyMetadataOptions.AffectsRender,
                        new PropertyChangedCallback(
                            SpinAnimationRenderer.OnAnimationPropertyChanged)));

                IsSpinningProperty =
                    RsSpinAnimationControl.IsSpinningProperty.AddOwner(
                    typeof(SpinAnimationRenderer),
                    new FrameworkPropertyMetadata(
                        false,
                        new PropertyChangedCallback(OnIsSpinningChanged)));

                _animationIndexProperty =
                    DependencyProperty.Register(
                    "AnimationIndex",
                    typeof(int),
                    typeof(SpinAnimationRenderer),
                    new FrameworkPropertyMetadata(
                        0, FrameworkPropertyMetadataOptions.AffectsRender));
            }

            /// <summary>
            /// Initialises a new instance of the <see cref="SpinAnimationRenderer"/> class.
            /// </summary>
            public SpinAnimationRenderer()
            {
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets or sets the number of unique points the animation stops at.
            /// </summary>
            public int ControlPointCount
            {
                get { return (int)this.GetValue(ControlPointCountProperty); }
                set { this.SetValue(ControlPointCountProperty, value); }
            }

            /// <summary>
            /// Gets or sets the radius for the spinner.
            /// </summary>
            public double ControlPointRadius
            {
                get { return (double)this.GetValue(ControlPointRadiusProperty); }
                set { this.SetValue(ControlPointRadiusProperty, value); }
            }

            /// <summary>
            /// Gets or sets a value indicating whether spinning animation spins clockwise or
            /// anti-clockwise.
            /// </summary>
            public bool IsClockwise
            {
                get { return (bool)this.GetValue(IsClockwiseProperty); }
                set { this.SetValue(IsClockwiseProperty, value); }
            }

            /// <summary>
            /// Gets or sets a value indicating whether the spinner is currently spinning.
            /// </summary>
            public bool IsSpinning
            {
                get { return (bool)this.GetValue(IsSpinningProperty); }
                set { this.SetValue(IsSpinningProperty, value); }
            }

            /// <summary>
            /// Gets or sets a time span that defines the duration of the animation.
            /// </summary>
            public TimeSpan RotationPeriod
            {
                get { return (TimeSpan)this.GetValue(RotationPeriodProperty); }
                set { this.SetValue(RotationPeriodProperty, value); }
            }

            /// <summary>
            /// Gets or sets the current process of the animation.
            /// </summary>
            private int AnimationIndex
            {
                get { return (int)this.GetValue(_animationIndexProperty); }
                set { this.SetValue(_animationIndexProperty, value); }
            }
            #endregion Properties

            #region Methods
            /// <summary>
            /// Defines the rendering operation for this framework element.
            /// </summary>
            /// <param name="drawingContext">
            /// The drawing instructions for a specific element.
            /// </param>
            protected override void OnRender(DrawingContext drawingContext)
            {
                double actualRadius = Math.Min(this.ActualWidth, this.ActualHeight) / 2.0;
                double num2 = actualRadius - this.ControlPointRadius;
                double clockwiseFactor = this.IsClockwise ? -1 : 1;
                Brush foreground = TextElement.GetForeground(this);
                int uniquePoints = this.ControlPointCount;
                for (int i = 0; i < uniquePoints; i++)
                {
                    double num4 =
                        (double)((i + uniquePoints - this.AnimationIndex) % uniquePoints);
                    double opacity;
                    if (this.ControlPointCount > 1)
                    {
                        opacity = num4 / (double)(this.ControlPointCount - 1);
                    }
                    else
                    {
                        opacity = 1.0;
                    }

                    double num5 = (double)(clockwiseFactor * i) / (double)uniquePoints;
                    double num6 = (num5 + 0.25) * 2.0 * Math.PI;
                    double num7 = Math.Cos(num6) * num2;
                    double num8 = Math.Sin(num6) * num2;
                    drawingContext.PushOpacity(opacity);
                    try
                    {
                        double centreX = actualRadius + num7;
                        double centreY = actualRadius - num8;

                        drawingContext.DrawEllipse(
                            foreground,
                            null,
                            new Point(centreX, centreY),
                            this.ControlPointRadius,
                            this.ControlPointRadius);
                    }
                    finally
                    {
                        drawingContext.Pop();
                    }
                }
            }

            /// <summary>
            /// Gets called when a animation property changes that will cause the animation to
            /// have to be restarted.
            /// </summary>
            /// <param name="s">
            /// The object whose dependency property changed.
            /// </param>
            /// <param name="e">
            /// The System.Windows.DependencyPropertyChangedEventArgs data used for this event.
            /// </param>
            private static void OnAnimationPropertyChanged(
                DependencyObject s, DependencyPropertyChangedEventArgs e)
            {
                SpinAnimationRenderer renderer = s as SpinAnimationRenderer;
                if (renderer == null)
                {
                    return;
                }

                if (renderer.IsSpinning)
                {
                    renderer.StartSpinAnimation();
                }
            }

            /// <summary>
            /// Gets called when the <see cref="IsSpinning"/> dependency property changes.
            /// </summary>
            /// <param name="s">
            /// The object whose dependency property changed.
            /// </param>
            /// <param name="e">
            /// The System.Windows.DependencyPropertyChangedEventArgs data used for this event.
            /// </param>
            private static void OnIsSpinningChanged(
                DependencyObject s, DependencyPropertyChangedEventArgs e)
            {
                SpinAnimationRenderer renderer = s as SpinAnimationRenderer;
                if (renderer == null)
                {
                    return;
                }

                if (renderer.IsSpinning)
                {
                    renderer.StartSpinAnimation();
                    return;
                }

                renderer.StopSpinAnimation();
            }

            /// <summary>
            /// Starts the spinning animation.
            /// </summary>
            private void StartSpinAnimation()
            {
                int fromValue = 0;
                int toValue = this.ControlPointCount;
                Duration duration = new Duration(this.RotationPeriod);
                Int32Animation animation = new Int32Animation(fromValue, toValue, duration)
                {
                    RepeatBehavior = RepeatBehavior.Forever
                };

                this.BeginAnimation(_animationIndexProperty, animation);
            }

            /// <summary>
            /// Stops the spinning animation.
            /// </summary>
            private void StopSpinAnimation()
            {
                this.BeginAnimation(_animationIndexProperty, null);
            }
            #endregion Methods
        } // SpinAnimationControl.SpinAnimationRenderer {Class}
        #endregion Classes
    } // RSG.Editor.Controls.RsSpinAnimationControl {Class}
} // RSG.Editor.Controls {Namespace}
