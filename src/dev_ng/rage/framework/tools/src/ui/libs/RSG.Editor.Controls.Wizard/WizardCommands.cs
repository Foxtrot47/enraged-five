﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace RSG.Editor.Controls.Wizard
{

    /// <summary>
    /// 
    /// </summary>
    public sealed class WizardCommands
    {
        public static RoutedUICommand Next
        {
            get;
            private set;
        }

        public static RoutedUICommand Previous
        {
            get;
            private set;
        }

        public static RoutedUICommand Finish
        {
            get;
            private set;
        }

        public static RoutedUICommand Cancel
        {
            get;
            private set;
        }

        static WizardCommands()
        {
            Next = new RoutedUICommand("Next", "Next", typeof(WizardCommands));
            Previous = new RoutedUICommand("Previous", "Previous", typeof(WizardCommands));
            Finish = new RoutedUICommand("Finish", "Finish", typeof(WizardCommands));
            Cancel = new RoutedUICommand("Cancel", "Cancel", typeof(WizardCommands));
        }
    }

} // RSG.Editor.Controls.Wizard namespace
