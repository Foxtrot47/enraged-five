﻿//---------------------------------------------------------------------------------------------
// <copyright file="SimpleFunctionDelegate.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    /// <summary>
    /// Encapsulates a method that has no parameters and returns a generic object.
    /// </summary>
    /// <returns>
    /// The return value of the method that this delegate encapsulates.
    /// </returns>
    public delegate object SimpleFunctionDelegate();
} // RSG.Editor {Namespace}
