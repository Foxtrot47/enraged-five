﻿//---------------------------------------------------------------------------------------------
// <copyright file="InputBindingProperties.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.AttachedDependencyProperties
{
    using System.Windows;
    using System.Windows.Data;
    using System.Windows.Input;

    /// <summary>
    /// Contains an attached property to use with textboxes allowing the enter key to update
    /// the binding value.
    /// </summary>
    public static class InputBindingProperties
    {
        #region Fields
        /// <summary>
        /// Identifies the UpdateBindingOnEnter attached property.
        /// </summary>
        public static readonly DependencyProperty UpdateBindingOnEnterProperty =
            DependencyProperty.RegisterAttached(
                "UpdateBindingOnEnter",
                typeof(DependencyProperty),
                typeof(InputBindingProperties),
                new PropertyMetadata(null, OnUpdateBindingOnEnterPropertyChanged));
        #endregion // Fields

        #region Methods
        /// <summary>
        /// Retrieves the UpdateBindingOnEnter dependency property value attached to the
        /// specified object.
        /// </summary>
        /// <param name="obj">
        /// The object whose attached UpdateBindingOnEnter dependency property value will be
        /// returned.
        /// </param>
        /// <returns>
        /// The UpdateBindingOnEnter dependency property value attached to the specified
        /// object.
        /// </returns>
        public static DependencyProperty GetUpdateBindingOnEnter(DependencyObject obj)
        {
            if (obj == null)
            {
                throw new SmartArgumentNullException(() => obj);
            }

            return (DependencyProperty)obj.GetValue(UpdateBindingOnEnterProperty);
        }

        /// <summary>
        /// Sets the UpdateBindingOnEnter attached property value on the specified object to
        /// the specified value.
        /// </summary>
        /// <param name="obj">
        /// The object whose attached UpdateBindingOnEnter attached property value will be set.
        /// </param>
        /// <param name="value">
        /// The value to set the attached UpdateBindingOnEnter attached property to on the
        /// specified object.
        /// </param>
        public static void SetUpdateBindingOnEnter(
            DependencyObject obj, DependencyProperty value)
        {
            if (obj == null)
            {
                throw new SmartArgumentNullException(() => obj);
            }

            obj.SetValue(UpdateBindingOnEnterProperty, value);
        }

        /// <summary>
        /// Helper method for forcing the binding to update it's source value.
        /// </summary>
        /// <param name="source">
        /// The source object.
        /// </param>
        private static void DoUpdateSource(object source)
        {
            DependencyProperty property = GetUpdateBindingOnEnter(source as DependencyObject);
            UIElement element = source as UIElement;
            if (property == null || element == null)
            {
                return;
            }

            BindingExpression binding =
                BindingOperations.GetBindingExpression(element, property);
            if (binding != null)
            {
                binding.UpdateSource();
            }
        }

        /// <summary>
        /// Event handler for previewing a key down event.
        /// </summary>
        /// <param name="sender">
        /// Sender of the event.
        /// </param>
        /// <param name="e">
        /// Event args containing information about the key that was pressed.
        /// </param>
        private static void HandlePreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                DoUpdateSource(e.Source);
            }
        }

        /// <summary>
        /// Call-back that fires whenever the UpdateBindingOnEnter attached property changes
        /// value.
        /// </summary>
        /// <param name="obj">
        /// The object whose attached UpdateBindingOnEnter attached property value has
        /// changed.
        /// </param>
        /// <param name="e">
        /// Event args containing information about the attached property that has changed.
        /// </param>
        private static void OnUpdateBindingOnEnterPropertyChanged(
            DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            UIElement element = obj as UIElement;
            if (element == null)
            {
                return;
            }

            if (e.OldValue != null)
            {
                element.PreviewKeyDown -= HandlePreviewKeyDown;
            }

            if (e.NewValue != null)
            {
                element.PreviewKeyDown += new KeyEventHandler(HandlePreviewKeyDown);
            }
        }
        #endregion // Methods
    } // RSG.Editor.Controls.AttachedDependencyProperties.InputBindingProperties {Class}
} // RSG.Editor.Controls.AttachedDependencyProperties {Namespace}
