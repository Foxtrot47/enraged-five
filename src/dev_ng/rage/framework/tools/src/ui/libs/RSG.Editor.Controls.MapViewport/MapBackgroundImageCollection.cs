﻿using System.Collections.ObjectModel;
using System.Xml.Linq;

namespace RSG.Editor.Controls.MapViewport
{
    /// <summary>
    /// Represents a collection of <see cref="MapBackgroundImage"/> objects.
    /// </summary>
    public class MapBackgroundImageCollection : ObservableCollection<MapBackgroundImage>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MapBackgroundImageCollection"/>
        /// class.
        /// </summary>
        public MapBackgroundImageCollection()
            : base()
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="MapBackgroundImageCollection"/>
        /// class using the provided xml element.
        /// </summary>
        public MapBackgroundImageCollection(XElement elem)
            : base()
        {
            foreach (XElement mapElem in elem.Elements("map"))
            {
                Add(new MapBackgroundImage(mapElem));
            }
        }
        #endregion
    }
}
