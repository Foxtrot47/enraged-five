﻿//---------------------------------------------------------------------------------------------
// <copyright file="SearchFinishedEventArgs.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;

    /// <summary>
    /// Represents the class containing the data for the event when a search finishes.
    /// </summary>
    internal class SearchFinishedEventArgs : EventArgs
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="FoundEntries"/> property.
        /// </summary>
        private bool _foundEntries;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SearchFinishedEventArgs"/> class.
        /// </summary>
        public SearchFinishedEventArgs()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether any entries have been found during the
        /// search.
        /// </summary>
        public bool FoundEntries
        {
            get { return this._foundEntries; }
            set { this._foundEntries = value; }
        }
        #endregion Properties
    } // RSG.Editor.SearchFinishedEventArgs {Class}
} // RSG.Editor {Namespaace}
