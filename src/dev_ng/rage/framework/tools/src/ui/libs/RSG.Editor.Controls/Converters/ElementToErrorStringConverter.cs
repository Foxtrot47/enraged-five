﻿//---------------------------------------------------------------------------------------------
// <copyright file="ElementToErrorStringConverter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Converters
{
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Converters a user interface element to a error string using the validation properties.
    /// </summary>
    public class ElementToErrorStringConverter : ValueConverter<UIElement, int, string>
    {
        #region Methods
        /// <summary>
        /// Converters the given value and returns the result.
        /// </summary>
        /// <param name="element">
        /// The element to retrieve the error string from.
        /// </param>
        /// <param name="count">
        /// The current number of errors attached to the element.
        /// </param>
        /// <param name="param">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A converted value.
        /// </returns>
        protected override string Convert(
            UIElement element, int count, object param, CultureInfo culture)
        {
            if (element == null)
            {
                return null;
            }

            ReadOnlyObservableCollection<ValidationError> errors =
                Validation.GetErrors(element);
            if (errors.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                int i = 1;
                foreach (ValidationError error in errors)
                {
                    sb.Append(error.ErrorContent.ToString());
                    if (i != errors.Count)
                    {
                        sb.AppendLine();
                    }

                    i++;
                }

                return sb.ToString();
            }

            return null;
        }
        #endregion Methods
    } // RSG.Editor.Controls.Converters.ElementToErrorStringConverter {Class}
} // RSG.Editor.Controls.Converters {Namespace}
