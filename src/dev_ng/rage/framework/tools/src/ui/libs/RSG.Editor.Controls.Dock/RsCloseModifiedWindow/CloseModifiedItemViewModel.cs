﻿//---------------------------------------------------------------------------------------------
// <copyright file="CloseModifiedItemViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock
{
    using RSG.Editor.Controls.Dock.ViewModel;

    /// <summary>
    /// The view model that represents a item that is being closed but has been modified.
    /// </summary>
    public class CloseModifiedItemViewModel : NotifyPropertyChangedBase
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private string _name;

        /// <summary>
        /// The private field used for the <see cref="Save"/> property.
        /// </summary>
        private bool _save;

        /// <summary>
        /// The private field used for the <see cref="SaveableItem"/> property.
        /// </summary>
        private ISaveableDocument _saveableItem;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CloseModifiedItemViewModel"/> class
        /// with the specified name and initial save state.
        /// </summary>
        /// <param name="name">
        /// The name of this item.
        /// </param>
        /// <param name="save">
        /// A value indicating whether this item is initial set to be saved before closing.
        /// </param>
        /// <param name="item">
        /// The saveable item that this view model is representing.
        /// </param>
        public CloseModifiedItemViewModel(string name, bool save, ISaveableDocument item)
        {
            this._name = name;
            this._save = save;
            this._saveableItem = item;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the name for this item.
        /// </summary>
        public string Name
        {
            get { return this._name; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this item has been selected to be saved
        /// before it is closed.
        /// </summary>
        public bool Save
        {
            get { return this._save; }
            set { this.SetProperty(ref this._save, value); }
        }

        /// <summary>
        /// Gets the saveable item this view model is representing.
        /// </summary>
        public ISaveableDocument SaveableItem
        {
            get { return this._saveableItem; }
        }
        #endregion Properties
    } // RSG.Editor.Controls.Dock.CloseModifiedItemViewModel {Class}
} // RSG.Editor.Controls.Dock {Namespace}
