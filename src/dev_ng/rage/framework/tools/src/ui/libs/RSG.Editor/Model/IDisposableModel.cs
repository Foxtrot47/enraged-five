﻿//---------------------------------------------------------------------------------------------
// <copyright file="IDisposableModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Model
{
    using System;
    using System.ComponentModel;

    /// <summary>
    ///  When implemented represents a model (data source) object in the Editor Framework that
    ///  can be disposed of.
    /// </summary>
    public interface IDisposableModel : IModel, IDisposable
    {
    } // RSG.Editor.Model.IDisposableModel {Interface}
} // RSG.Editor.Model {Namespace}
