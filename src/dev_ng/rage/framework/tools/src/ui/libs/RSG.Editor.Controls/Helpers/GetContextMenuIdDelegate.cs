﻿//---------------------------------------------------------------------------------------------
// <copyright file="GetContextMenuIdDelegate.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Helpers
{
    using System;

    /// <summary>
    /// Represents the method that retrieves a command id that can be used to retrieve the
    /// child commands from the command manager for the specified item. This is used on item
    /// controls that want to publically support a extensible
    /// context menu.
    /// </summary>
    /// <param name="item">
    /// The item whose child command id needs to be determined.
    /// </param>
    /// <returns>
    /// The identifier that the command system should use to retrieve the commands for the
    /// specified items context menu.
    /// </returns>
    public delegate Guid GetContextMenuIdDelegate(object item);
} // RSG.Editor.Controls.Helpers {Namespace}
