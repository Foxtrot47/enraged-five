﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Editor.SharedCommands;

namespace RSG.Editor.Controls.MapViewport.Annotations
{
    /// <summary>
    /// Interaction logic for AnnotationCoordinatesWindow.xaml
    /// </summary>
    public partial class AnnotationCoordinatesWindow : RsWindow
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="AnnotationCoordinatesWindow"/>
        /// class.
        /// </summary>
        public AnnotationCoordinatesWindow()
        {
            InitializeComponent();

            this.CommandBindings.Add(
                new CommandBinding(
                    RockstarViewCommands.CancelDialog,
                    this.OnCancelExecuted));

            this.CommandBindings.Add(
                new CommandBinding(
                    RockstarViewCommands.ConfirmDialog,
                    this.OnConfirmExecuted));
        }
        #endregion

        #region Methods
        /// <summary>
        /// Called whenever the <see cref="RockstarViewCommands.CancelDialog"/> command
        /// is fired and needs handling at this instance level.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The execute data that has been sent with the command.
        /// </param>
        private void OnCancelExecuted(object s, ExecutedRoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        /// <summary>
        /// Called whenever the <see cref="RockstarViewCommands.ConfirmDialog"/> command is
        /// fired and needs handling at this instance level.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The execute data that has been sent with the command.
        /// </param>
        private void OnConfirmExecuted(object s, ExecutedRoutedEventArgs e)
        {
            this.DialogResult = true;
        }
        #endregion
    }
}
