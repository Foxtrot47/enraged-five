﻿//---------------------------------------------------------------------------------------------
// <copyright file="IProvidesExpandableState.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    using System.ComponentModel;

    /// <summary>
    /// When implemented represents a item that contains a default expansion state that can be
    /// used by the user interface.
    /// </summary>
    public interface IProvidesExpandableState : INotifyPropertyChanged
    {
        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether this item is expanded by default in the user
        /// interface when by rendered inside a hierarchical control.
        /// </summary>
        bool IsExpandable { get; set; }
        #endregion Properties
    } // RSG.Editor.View.IProvidesExpandableState {Interface}
} // RSG.Editor.View {Namespace}
