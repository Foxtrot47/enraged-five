﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProjectNode.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.AssetBuildMonitor.ViewModel.Project
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using RSG.Project.Model;
    using RSG.Project.ViewModel;
    using RSG.Project.ViewModel.Definitions;

    /// <summary>
    /// 
    /// </summary>
    public sealed class ProjectNode : 
        RSG.Project.ViewModel.ProjectNode
    {
        #region Member Data
        /// <summary>
        /// Enumerable of Bugstar users (for displaying user-information).
        /// </summary>
        private IEnumerable<RSG.Interop.Bugstar.Organisation.User> _bugstarUsers;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="ProjectNode"/> class.
        /// </summary>
        /// <param name="definition">
        /// The project definition that was used to create this project node instance.
        /// </param>
        public ProjectNode(ProjectDefinition definition, 
            IEnumerable<RSG.Interop.Bugstar.Organisation.User> bugstarUsers)
            : base(definition)
        {
            this._bugstarUsers = bugstarUsers;
        }
        #endregion // Constructor(s)
        
        #region ProjectNode Overridden Methods
        /// <summary>
        /// Creates a new project item object for the file with the specified relative path
        /// and adds a new hierarchy node to the specified parent that represents it.
        /// </summary>
        /// <param name="parent">
        /// The parent node to the new item.
        /// </param>
        /// <param name="relativePath">
        /// The relative path for the new file from this project.
        /// </param>
        /// <returns>
        /// The new hierarchy node that was created to represent the file.
        /// </returns>
        public override IHierarchyNode AddNewFile(IHierarchyNode parent, String relativePath)
        {
            IProjectItemScope scope = this.Model as IProjectItemScope;
            if (scope == null)
            {
                return null;
            }

            ProjectItem newItem = scope.AddNewProjectItem("Host", relativePath);
            if (parent is FolderNode)
            {
                newItem.SetMetadata("Filter", parent.Include, MetadataLevel.Filter);
            }

            IHierarchyNode newNode = new HostNode(parent, newItem, this._bugstarUsers);
            parent.AddChild(newNode);
            this.IsModified = true;
            return newNode;
        }

        /// <summary>
        /// Override to create additional nodes in your project that are defined in your
        /// project file.
        /// </summary>
        protected override void ProcessCustom()
        {
            foreach (RSG.Project.Model.ProjectItem file in this["Host"])
            {
                string filter = file.GetMetadata("Filter");
                if (filter != null)
                {
                    RSG.Project.ViewModel.FolderNode node = CreateFolderNodes(filter.TrimEnd('\\') + "\\");
                    if (node != null)
                    {
                        node.AddChild(new HostNode(node, file, this._bugstarUsers));
                        continue;
                    }

                    Debug.Fail("Failed to create parent for file.");
                }

                this.AddChild(new HostNode(this, file, this._bugstarUsers));
            }
        }
        #endregion // ProjectNode Overridden Methods
    }

} // RSG.AssetBuildMonitor.ViewModel.Project namespace
