﻿//---------------------------------------------------------------------------------------------
// <copyright file="InvertSelectionAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Rpf.View.Commands
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Input;
    using RSG.Editor;
    using RSG.Editor.SharedCommands;
    using RSG.Rpf.ViewModel;

    /// <summary>
    /// Implements the <see cref="RockstarCommands.InvertSelection"/> command.
    /// </summary>
    public class InvertSelectionAction : ButtonAction<IEnumerable<RpfViewControl>>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="InvertSelectionAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public InvertSelectionAction(
            ParameterResolverDelegate<IEnumerable<RpfViewControl>> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="controls">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(IEnumerable<RpfViewControl> controls)
        {
            if (controls == null)
            {
                return false;
            }

            foreach (RpfViewControl control in controls)
            {
                if (control.ItemsSource.Cast<object>().Any())
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="controls">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(IEnumerable<RpfViewControl> controls)
        {
            if (controls == null)
            {
                return;
            }

            foreach (RpfViewControl control in controls)
            {
                if (control.DataGrid == null || control.DataGrid.Items.Count == 0)
                {
                    continue;
                }

                List<PackEntryViewModel> selectedItems = new List<PackEntryViewModel>();
                foreach (PackEntryViewModel item in control.DataGrid.SelectedItems)
                {
                    selectedItems.Add(item);
                }

                control.DataGrid.SelectAll();
                foreach (PackEntryViewModel selectedItem in selectedItems)
                {
                    control.DataGrid.SelectedItems.Remove(selectedItem);
                }

                if (!control.IsKeyboardFocusWithin)
                {
                    FocusNavigationDirection direction = FocusNavigationDirection.First;
                    control.MoveFocus(new TraversalRequest(direction));
                }
            }
        }
        #endregion Methods
    } // RSG.Rpf.View.Commands.InvertSelectionAction {Class}
} // RSG.Rpf.View.Commands {Namespace}
