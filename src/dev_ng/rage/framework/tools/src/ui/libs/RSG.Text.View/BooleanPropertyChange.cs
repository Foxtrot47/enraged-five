﻿//---------------------------------------------------------------------------------------------
// <copyright file="BooleanPropertyChange.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.View
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows.Controls;
    using System.Windows.Data;
    using RSG.Editor.Controls;
    using RSG.Text.ViewModel;

    /// <summary>
    /// A composite change object that represents a change to a single property on n-number of
    /// <see cref="LineViewModel"/> objects where that property is a boolean value.
    /// </summary>
    public abstract class BooleanPropertyChange : CompositePropertyChange, INotifyDataErrorInfo
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Value"/> property.
        /// </summary>
        private bool? _value;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BooleanPropertyChange"/> class.
        /// </summary>
        /// <param name="header">
        /// The header value for this property, can be thought of as the friendly name of the
        /// property.
        /// </param>
        /// <param name="binding">
        /// The binding path to the property through a line view model object.
        /// </param>
        public BooleanPropertyChange(string header, string binding)
            : base(header, binding)
        {
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs whenever the has error property or the error messages for this entity or
        /// one of its properties changes.
        /// </summary>
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;
        #endregion Events

        #region Properties
        /// <summary>
        /// Gets a data grid column object that shows the property this change object
        /// represents through a line view model object.
        /// </summary>
        public override DataGridColumn Column
        {
            get
            {
                RsDataGridCheckBoxColumn column = new RsDataGridCheckBoxColumn();
                column.Width = 150;
                column.MinWidth = 150;
                column.IsReadOnly = true;
                column.Binding = new Binding(this.Binding) { Mode = BindingMode.OneWay };
                column.Header = this.Header;
                return column;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this entity or any of its properties currently have
        /// an error associated with them.
        /// </summary>
        public override bool HasErrors
        {
            get { return this._value == null; }
        }

        /// <summary>
        /// Gets or sets the value that will be applied to the line view models.
        /// </summary>
        public bool? Value
        {
            get
            {
                return this._value;
            }

            set
            {
                this._value = value;
                EventHandler<DataErrorsChangedEventArgs> handler = this.ErrorsChanged;
                if (handler == null)
                {
                    return;
                }

                DataErrorsChangedEventArgs args = new DataErrorsChangedEventArgs("Value");
                handler(this, args);
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Gets the error messages associated with the property with the specified name.
        /// </summary>
        /// <param name="propertyName">
        /// The name of the property whose associated errors should be retrieved.
        /// </param>
        /// <returns>
        /// The error messages associated with the property with the specified name.
        /// </returns>
        public IEnumerable GetErrors(string propertyName)
        {
            List<string> errors = new List<string>();
            if (this._value == null && propertyName == "Value")
            {
                errors.Add("Value needs to be set.");
            }

            return errors;
        }

        /// <summary>
        /// Sets the items that the property can be set to.
        /// </summary>
        /// <param name="lineViewModels">
        /// The line view models that will have the property value applied to.
        /// </param>
        public override void Initialise(IEnumerable<LineViewModel> lineViewModels)
        {
            this._value = null;
        }
        #endregion Methods
    } // RSG.Text.View.BooleanPropertyChange {Class}
} // RSG.Text.View {Namespace}
