﻿//---------------------------------------------------------------------------------------------
// <copyright file="MoveLineDownAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Commands
{
    using System.Linq;
    using System.Windows.Input;
    using RSG.Editor;
    using RSG.Text.ViewModel;

    /// <summary>
    /// Implements the <see cref="TextCommands.MoveLineDown"/> routed command.
    /// </summary>
    public class MoveLineDownAction : ButtonAction<TextLineCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MoveLineDownAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public MoveLineDownAction(ParameterResolverDelegate<TextLineCommandArgs> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(TextLineCommandArgs args)
        {
            if (args.Selected == null || args.Selected.Count() != 1)
            {
                return false;
            }

            return args.DataGrid.SelectedIndex != args.DataGrid.Items.Count - 1;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(TextLineCommandArgs args)
        {
            int index = args.DataGrid.SelectedIndex;
            foreach (LineViewModel lineViewModel in args.Selected)
            {
                args.Conversation.Model.MoveLine(lineViewModel.Model, index + 1);
            }

            FocusNavigationDirection direction = FocusNavigationDirection.Next;
            args.DataGrid.MoveFocus(new TraversalRequest(direction));
        }
        #endregion Methods
    } // RSG.Text.Commands.MoveLineDownAction {Class}
} // RSG.Text.Commands {Namespace}
