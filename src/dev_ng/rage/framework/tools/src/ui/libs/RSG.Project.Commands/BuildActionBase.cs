﻿//---------------------------------------------------------------------------------------------
// <copyright file="BuildActionBase.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Windows;
    using RSG.Base.Logging;
    using RSG.Base.Logging.Universal;
    using RSG.Editor;
    using RSG.Project.Model;

    /// <summary>
    /// Provides a base class to all project actions that implement project build logic.
    /// </summary>
    public abstract class BuildActionBase : ButtonAction<BuildProjectCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BuildActionBase"/> class.
        /// </summary>
        /// <param name="serviceProvider">
        /// The service provider that is used by this action to perform application specific
        /// operations, for example selecting a file to open.
        /// </param>
        protected BuildActionBase(ICommandServiceProvider serviceProvider)
            : base(serviceProvider)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="BuildActionBase"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        protected BuildActionBase(BuildProjectCommandResolver resolver)
            : base(new ParameterResolverDelegate<BuildProjectCommandArgs>(resolver))
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Builds the specified projects using the specified argument object.
        /// </summary>
        /// <param name="projects">
        /// The projects to build.
        /// </param>
        /// <param name="args">
        /// The argument object used by the build process.
        /// </param>
        protected virtual void Build(List<Project> projects, BuildProjectCommandArgs args)
        {
            IPerforceService perforceService = this.GetService<IPerforceService>();
            IProjectBuilder builder = args.ProjectBuilder;
            if (builder == null)
            {
                Debug.Assert(false, "No project builder has been specified.");
                return;
            }

            SaveAllAction saveAction = new SaveAllAction(this.ServiceProvider);
            saveAction.Execute(args);

            builder.SetProjects(projects);
            int changelist = 0;

            IUniversalLog log = LogFactory.CreateUniversalLog("Build");
            string filename = Path.Combine(Path.GetTempPath(), "project_build");
            filename = Path.ChangeExtension(filename, UniversalLogFile.EXTENSION);
            UniversalLogFile target =
                new UniversalLogFile(filename, FileMode.Create, FlushMode.Never, log);

            if (perforceService != null)
            {
                try
                {
                    List<string> syncFiles = new List<string>();
                    syncFiles.AddRange(builder.SourceFiles);
                    syncFiles.AddRange(builder.TargetFiles);

                    perforceService.GetLatest(syncFiles);
                    changelist = perforceService.CreateChangelist("Automatic Build Checkout");
                    perforceService.EditFiles(builder.TargetFiles, changelist, true);
                }
                catch (Exception ex)
                {
                    log.ToolException(ex, "Exception caught while get latest and checking out the build files.");
                }
            }

            builder.Build(log);

            bool changed = true;
            if (perforceService != null)
            {
                try
                {
                    IList<string> targetFiles = builder.FilesWrittenToDuringBuild;
                    perforceService.AddFiles(targetFiles, changelist);
                    perforceService.RevertUnchangedFiles(targetFiles);
                    perforceService.TryToDeleteChangelist(changelist);

                    if (!perforceService.AnyFilesWithState(targetFiles, PerforceFileAction.Add, PerforceFileAction.Edit))
                    {
                        changed = false;
                    }
                }
                catch (Exception ex)
                {
                    log.ToolException(ex, "Exception caught while adding and reverting written to build files.");
                }
            }

            target.Flush();
            if (log.HasErrors || log.HasWarnings)
            {
                Process.Start(filename);
            }
            
            if (!log.HasErrors)
            {
                IMessageBoxService service = this.GetService<IMessageBoxService>();
                if (service != null)
                {
                    if (changed)
                    {
                        service.Show("Build Succeeded.");
                    }
                    else
                    {
                        service.Show("Build Succeeded. No changes were detected.");
                    }
                }
            }
        }
        #endregion Methods
    } // RSG.Project.Commands.BuildActionBase {Class}
} // RSG.Project.Commands {Namespace}
