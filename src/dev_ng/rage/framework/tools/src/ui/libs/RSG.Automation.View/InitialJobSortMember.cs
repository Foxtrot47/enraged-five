﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Automation.View
{

    /// <summary>
    /// Defines the different members of the Job view data grid that can be
    /// initially sorted.
    /// </summary>
    public enum InitialJobSortMember
    {
        Id,
        State,
        Priority,
        Changelist,
        Username,
        SubmittedAt,
        ProcessedAt,
        DownloadSize,
        DownloadProgress,
        ProcessingHost
    }

} // RSG.Automation.View namespace
