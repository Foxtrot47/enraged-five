﻿<rsg:RsUserControl x:Class="RSG.AssetBuildMonitor.View.AssetBuildListControl"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:rsg="http://schemas.rockstargames.com/2013/xaml/editor"
             xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" 
             xmlns:d="http://schemas.microsoft.com/expression/blend/2008" 
             xmlns:model="clr-namespace:RSG.Pipeline.Services.Engine;assembly=RSG.Pipeline.Services.Engine"
             xmlns:commands="clr-namespace:RSG.AssetBuildMonitor.Commands;assembly=RSG.AssetBuildMonitor.Commands"
             Foreground="{rsg:ThemeResource ContentTextKey}"
             mc:Ignorable="d" 
             d:DesignHeight="300" 
             d:DesignWidth="300">
    
    <UserControl.Resources>
        <rsg:VisibilityToBooleanConverter x:Key="VisibilityToBooleanConverter" />
        <rsg:BooleanToVisibilityConverter x:Key="BooleanToVisibilityConverter" />
        <rsg:StringCollectionToStringConverter x:Key="StringCollectionToStringConverter" />
        <rsg:StringCollectionToSingleLineStringConverter x:Key="StringCollectionToSingleLineStringConverter" />
        <rsg:InvertedBooleanToVisibilityConverter x:Key="InvertedBooleanToVisibilityConverter" />

        <BitmapImage x:Key="BuildUnknown" UriSource="Resources/BuildCancelled.png" />
        <BitmapImage x:Key="BuildError" UriSource="Resources/BuildError.png" />
        <BitmapImage x:Key="BuildSuccess" UriSource="Resources/BuildSuccess.png" />
        <BitmapImage x:Key="BuildWarning" UriSource="Resources/BuildWarning.png" />
        <BitmapImage x:Key="DefaultUserImage" UriSource="Resources/DefaultUser.png" />
        
        <!-- Header Context Menu -->
        <rsg:RsContextMenu x:Key="DataGridColumnHeaderContextMenu"
                           ItemsSource="{Binding RelativeSource={RelativeSource AncestorType={x:Type DataGrid}},
                                                 Path=Columns, ValidatesOnNotifyDataErrors=True}">
            <rsg:RsContextMenu.ItemContainerStyle>
                <Style BasedOn="{StaticResource {x:Type rsg:RsMenuItem}}"
                       TargetType="rsg:RsMenuItem">
                    <Setter Property="IsCheckable"
                            Value="True" />
                    <Setter Property="Header"
                            Value="{Binding Header}" />
                    <Setter Property="IsChecked"
                            Value="{Binding Visibility,
                                            Converter={StaticResource VisibilityToBooleanConverter}}" />
                    <Style.Triggers>
                        <DataTrigger Binding="{Binding Header}"
                                     Value="{x:Null}">
                            <Setter Property="Header"
                                    Value="{Binding Path=(ToolTipService.ToolTip)}" />
                        </DataTrigger>
                        <DataTrigger Binding="{Binding Header}"
                                     Value="Name">
                            <Setter Property="IsEnabled"
                                    Value="False" />
                        </DataTrigger>
                    </Style.Triggers>
                </Style>
            </rsg:RsContextMenu.ItemContainerStyle>
        </rsg:RsContextMenu>

        <Style BasedOn="{StaticResource {x:Type DataGridColumnHeader}}"
               TargetType="{x:Type DataGridColumnHeader}">
            <Setter Property="ContextMenu"
                    Value="{StaticResource DataGridColumnHeaderContextMenu}" />
        </Style>
        
        <!-- Build Item Context Menu -->
        <Style BasedOn="{StaticResource {x:Type DataGridRow}}"
               TargetType="{x:Type DataGridRow}">
            <Setter Property="rsg:ContextMenuProperties.ContextMenuIdentifier"
                    Value="{Binding Source={x:Static commands:Commands.BuildItemContextMenu}}" />
        </Style>
    </UserControl.Resources>
    
    <!-- Asset Build Item DataGrid -->
    <DataGrid Name="BuildListGrid"
              AutoGenerateColumns="False"
              CanUserReorderColumns="True"
              CanUserResizeRows="True"
              HeadersVisibility="Column"
              IsTextSearchEnabled="True"
              ItemsSource="{Binding BuildItems}"
              SelectionMode="Extended"
              SelectionUnit="FullRow"
              AlternationCount="1"
              Visibility="{Binding IsConnected, Converter={StaticResource BooleanToVisibilityConverter}}"
              VirtualizingStackPanel.IsVirtualizing="True"
              VirtualizingStackPanel.ScrollUnit="Item"
              VirtualizingStackPanel.VirtualizationMode="Recycling"
              rsg:ContextMenuProperties.ContextCommandParameter="{Binding}">
        <DataGrid.Resources>
            <Style TargetType="DataGridRow">
                <Setter Property="IsSelected" Value="{Binding IsSelected}"/>
            </Style>
        </DataGrid.Resources>
        <DataGrid.Columns>
            <DataGridTemplateColumn x:Name="StatusImageColumn"
                                    Width="20"
                                    CanUserResize="False"
                                    Header=""
                                    IsReadOnly="True"
                                    ToolTipService.ToolTip="Build status">
                <DataGridTemplateColumn.CellTemplate>
                    <DataTemplate>
                        <Image>
                            <Image.Style>
                                <Style TargetType="{x:Type Image}">
                                    <Style.Triggers>
                                        <DataTrigger Binding="{Binding Path=BuildItem.State}" Value="{x:Static model:AssetBuildState.Pending}">
                                            <Setter Property="Source" Value="{StaticResource BuildUnknown}" />
                                        </DataTrigger>
                                        <DataTrigger Binding="{Binding Path=BuildItem.State}" Value="{x:Static model:AssetBuildState.Building}">
                                            <Setter Property="Source" Value="{StaticResource BuildWarning}" />
                                        </DataTrigger>
                                        <DataTrigger Binding="{Binding Path=BuildItem.State}" Value="{x:Static model:AssetBuildState.Complete}">
                                            <Setter Property="Source" Value="{StaticResource BuildSuccess}" />
                                        </DataTrigger>
                                        <DataTrigger Binding="{Binding Path=BuildItem.State}" Value="{x:Static model:AssetBuildState.Failed}">
                                            <Setter Property="Source" Value="{StaticResource BuildError}" />
                                        </DataTrigger>
                                        <DataTrigger Binding="{Binding Path=BuildItem.State}" Value="{x:Static model:AssetBuildState.Aborted}">
                                            <Setter Property="Source" Value="{StaticResource BuildUnknown}" />
                                        </DataTrigger>
                                    </Style.Triggers>
                                </Style>
                            </Image.Style>
                        </Image>
                    </DataTemplate>
                </DataGridTemplateColumn.CellTemplate>
            </DataGridTemplateColumn>
            <DataGridTextColumn x:Name="ProjectNameColumn"
                                Width="100"
                                Header="Project"
                                IsReadOnly="True"
                                SortMemberPath="Name"
                                ToolTipService.ToolTip="Project."
                                Binding="{Binding BuildItem.ProjectName}" />
            <DataGridTextColumn x:Name="BranchNameColumn"
                                Width="100"
                                Header="Branch"
                                IsReadOnly="True"
                                SortMemberPath="Name"
                                ToolTipService.ToolTip="Branch."
                                Binding="{Binding BuildItem.BranchName}" />
            <DataGridTemplateColumn x:Name="UsernameColumn"
                                    Width="100"
                                    Header="Username"
                                    IsReadOnly="True"
                                    SortMemberPath="BuildItem.Username"
                                    ToolTipService.ToolTip="Build initiating username.">
                <DataGridTemplateColumn.CellTemplate>
                    <DataTemplate>
                        <TextBlock>
                                <Hyperlink Command="{x:Static rsg:RockstarCommands.ShellExecute}"
                                           CommandParameter="{Binding MailToUri}">
                                    <TextBlock Text="{Binding BuildItem.Username}">
                                        <!--  Username Tooltip: username image and email link.  -->
                                        <TextBlock.ToolTip>
                                            <ToolTip>
                                                <StackPanel Orientation="Horizontal" Margin="5">
                                                    <Image Source="{Binding User.Image, FallbackValue={StaticResource DefaultUserImage}, TargetNullValue={StaticResource DefaultUserImage}}" Width="120" Height="160" />
                                                    <Separator Width="5" Height="0" />
                                                    <StackPanel Orientation="Vertical">
                                                        <TextBlock Text="{Binding Trigger.Username, StringFormat='Username: {0}'}" />
                                                        <TextBlock Text="{Binding User.Email, StringFormat='Email Address: {0}'}" />
                                                        <TextBlock Text="{Binding User.JobTitle, StringFormat='Title: {0}'}" />
                                                    </StackPanel>
                                                </StackPanel>
                                            </ToolTip>
                                        </TextBlock.ToolTip>
                                    </TextBlock>
                                </Hyperlink>
                            </TextBlock>
                        </DataTemplate>
                </DataGridTemplateColumn.CellTemplate>
            </DataGridTemplateColumn>
            <DataGridTextColumn x:Name="TypeColumn"
                                Width="100"
                                Header="Type"
                                IsReadOnly="True"
                                SortMemberPath="BuildType"
                                ToolTipService.ToolTip="Branch."
                                Binding="{Binding BuildItem.BuildType}" />
            <DataGridTextColumn x:Name="HostColumn"
                                Width="150"
                                Header="Host"
                                IsReadOnly="True"
                                SortMemberPath="Host"
                                ToolTipService.ToolTip="Build host."
                                Binding="{Binding BuildItem.Host}" />
            <DataGridTextColumn x:Name="BuildStateColumn"
                                Width="150"
                                Header="State"
                                IsReadOnly="True"
                                SortMemberPath="State"
                                ToolTipService.ToolTip="Build State."
                                Binding="{Binding BuildItem.State}" />
            <DataGridTextColumn x:Name="StartTimeColumn"
                                Width="150"
                                Header="Start Time"
                                IsReadOnly="True"
                                SortMemberPath="StartTime"
                                ToolTipService.ToolTip="Build start time."
                                Binding="{Binding BuildItem.StartTime}" />
            <DataGridTextColumn x:Name="DurationColumn"
                                Width="150"
                                Header="Duration"
                                IsReadOnly="True"
                                SortMemberPath="Duration"
                                ToolTipService.ToolTip="Build duration."
                                Binding="{Binding BuildItem.Duration, StringFormat=hh\\:mm\\:ss}" />
            <DataGridTemplateColumn x:Name="SourceFilesColumn" 
                                    Width="100"
                                    Header="Source Files"
                                    IsReadOnly="True"
                                    SortMemberPath="Name"
                                    ToolTipService.ToolTip="All source files.">
                <DataGridTemplateColumn.CellTemplate>
                    <DataTemplate>
                        <TextBlock Text="{Binding BuildItem.SourceFilenames, Converter={StaticResource StringCollectionToSingleLineStringConverter}}">
                            <TextBlock.ToolTip>
                                <ToolTip>
                                    <TextBlock Text="{Binding BuildItem.SourceFilenames, Converter={StaticResource StringCollectionToStringConverter}}" />
                                </ToolTip>
                            </TextBlock.ToolTip>
                        </TextBlock>
                    </DataTemplate>
                </DataGridTemplateColumn.CellTemplate>
            </DataGridTemplateColumn>
            <DataGridTemplateColumn x:Name="OutputFilesColumn"
                                    Width="100"
                                    Header="Output Files"
                                    IsReadOnly="True"
                                    SortMemberPath="Name"
                                    ToolTipService.ToolTip="All output files.">
                <DataGridTemplateColumn.CellTemplate>
                    <DataTemplate>
                        <TextBlock Text="{Binding BuildItem.OutputFilenames, Converter={StaticResource StringCollectionToSingleLineStringConverter}}">
                            <TextBlock.ToolTip>
                                <ToolTip>
                                    <TextBlock Text="{Binding BuildItem.OutputFilenames, Converter={StaticResource StringCollectionToStringConverter}}" />
                                </ToolTip>
                            </TextBlock.ToolTip>
                        </TextBlock>
                    </DataTemplate>
                </DataGridTemplateColumn.CellTemplate>
            </DataGridTemplateColumn>
        </DataGrid.Columns>
    </DataGrid>
</rsg:RsUserControl>
