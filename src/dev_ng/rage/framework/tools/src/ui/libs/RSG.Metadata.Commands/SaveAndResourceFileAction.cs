﻿//---------------------------------------------------------------------------------------------
// <copyright file="SaveAndResourceFileAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Commands
{
    using System.Diagnostics;
    using System.IO;
    using System.Text;
    using RSG.Editor;
    using RSG.Editor.Controls;
    using RSG.Project.Commands;
    using RSG.Project.ViewModel;

    /// <summary>
    /// Contains the logic for the RockstarCommands.ExpandAll routed command. This class cannot
    /// be inherited.
    /// </summary>
    public sealed class SaveAndResourceFileAction : ButtonAction<ProjectCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SaveAndResourceFileAction"/> class.
        /// </summary>
        /// <param name="serviceProvider">
        /// The service provider that is used by this action to perform application specific
        /// operations, for example selecting a file to open.
        /// </param>
        public SaveAndResourceFileAction(ICommandServiceProvider serviceProvider)
            : base(serviceProvider)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="SaveAndResourceFileAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public SaveAndResourceFileAction(ProjectCommandResolver resolver)
            : base(new ParameterResolverDelegate<ProjectCommandArgs>(resolver))
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ProjectCommandArgs args)
        {
            string fallbackText = "Save and Resource Selected Item";
            if (args == null || args.ViewSite == null || args.ViewSite.ActiveDocument == null)
            {
                RockstarCommandManager.UpdateItemText(MetadataCommands.SaveAndResourceFile, fallbackText);
                return false;
            }

            string updatedText = "Save and Resource " + args.ViewSite.ActiveDocument.FriendlySavePath;
            RockstarCommandManager.UpdateItemText(MetadataCommands.SaveAndResourceFile, updatedText);
            return true;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(ProjectCommandArgs args)
        {
            IDocumentManagerService docService = this.GetService<IDocumentManagerService>();
            IMessageBoxService msgService = this.GetService<IMessageBoxService>();
            if (docService == null || msgService == null)
            {
                Debug.Fail("Unable to save and resource due to the fact services is missing.");
                return;
            }

            docService.SaveActiveDocument(false);

            ProjectDocumentItem doc = docService.ActiveDocument as ProjectDocumentItem;
            if (doc == null || doc.FileNode == null || doc.FileNode.ParentProject == null)
            {
                Debug.Fail("Unable to resource a file that doesn't belong to a project.");
                return;
            }

            string path = (doc.FileNode.ParentProject.Model as RSG.Project.Model.Project).GetPropertyAsFullPath("ResourceConverter");
            FileInfo pathInfo = new FileInfo(path);
            if (!pathInfo.Exists)
            {
                msgService.ShowStandardErrorBox("Unable to resource as the specified converter doesn't exist {0}", null);
                return;
            }

            StringBuilder sb = new StringBuilder();
            sb.Append(" --rebuild");
            sb.Append(" --no-content");
            sb.AppendFormat(" {0}", Path.GetFullPath(doc.FullPath));
            Process.Start(new ProcessStartInfo(path, sb.ToString()));
        }
        #endregion Methods
    } // RSG.Metadata.Commands.SaveAndResourceFileAction {Class}
} // RSG.Metadata.Commands {Namespace}
