﻿using RSG.Editor.Controls;
using RSG.Text.Model;
using RSG.Text.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RSG.Text.View
{
    /// <summary>
    /// Interaction logic for CategoryEditorControl.xaml
    /// </summary>
    public partial class CategoryEditorControl : UserControl
    {
        public CategoryEditorControl()
        {
            InitializeComponent();

            this.NameSelector.DataContextChanged += NameSelector_DataContextChanged;
        }

        void NameSelector_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            EventHandler eventHandler = null;
            eventHandler = new EventHandler(delegate
            {
                if (this.NameSelector.ItemContainerGenerator.Status == GeneratorStatus.ContainersGenerated)
                {
                    this.NameSelector.SelectedIndex = 0;
                    this.NameSelector.ItemContainerGenerator.StatusChanged -= eventHandler;
                }
            });
            this.NameSelector.ItemContainerGenerator.StatusChanged += eventHandler;
        }

        private void NameSelector_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            CategoryViewModel dc = this.DataContext as CategoryViewModel;
            if (dc == null)
            {
                return;
            }

            this.CompositeContent.Content = dc.CreateComposite((sender as ListBox).SelectedItems);
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            int selectedIndex = this.NameSelector.SelectedIndex;

            if (selectedIndex == -1)
            {
                return;
            }

            CompositeViewModel composite = this.CompositeContent.Content as CompositeViewModel;
            if (composite == null)
            {
                return;
            }

            MessageBoxResult result = RsMessageBox.Show("Are you sure you want to delete the selection?", "Delete selection", MessageBoxButton.OKCancel, MessageBoxImage.Warning);

            if (result == MessageBoxResult.OK)
            {
                composite.Delete();
            }

            if (selectedIndex >= this.NameSelector.Items.Count)
            {
                selectedIndex = this.NameSelector.Items.Count - 1;
            }

            this.NameSelector.SelectedIndex = selectedIndex;
        }

        private void Rename_Click(object sender, RoutedEventArgs e)
        {
            if (this.NameSelector.SelectedItems.Count == 1)
            {
                DialogueCharacterViewModel character = this.NameSelector.SelectedItems[0] as DialogueCharacterViewModel;
                if (character != null)
                {
                    RenameWindow renameWindow = new RenameWindow(character.Name);
                    Window parentWindow = Window.GetWindow(this);
                    renameWindow.Owner = parentWindow;
                    bool? result = renameWindow.ShowDialog();
                    if (result.Value == true)
                    {
                        character.Name = renameWindow.ResultTextBox.Text;   
                    }
                }
                DialogueVoiceViewModel voice = this.NameSelector.SelectedItems[0] as DialogueVoiceViewModel;
                if (voice != null)
                {
                    RenameWindow renameWindow = new RenameWindow(voice.Name);
                    Window parentWindow = Window.GetWindow(this);
                    renameWindow.Owner = parentWindow;
                    bool? result = renameWindow.ShowDialog();
                    if (result.Value == true)
                    {
                        voice.Name = renameWindow.ResultTextBox.Text;
                    }
                }
                DialogueSpecialViewModel special = this.NameSelector.SelectedItems[0] as DialogueSpecialViewModel;
                if (special != null)
                {
                    RenameWindow renameWindow = new RenameWindow(special.Name);
                    Window parentWindow = Window.GetWindow(this);
                    renameWindow.Owner = parentWindow;
                    bool? result = renameWindow.ShowDialog();
                    if (result.Value == true)
                    {
                        special.Name = renameWindow.ResultTextBox.Text;
                    }
                }
                DialogueAudioTypeViewModel audioType = this.NameSelector.SelectedItems[0] as DialogueAudioTypeViewModel;
                if (audioType != null)
                {
                    RenameWindow renameWindow = new RenameWindow(audioType.Name);
                    Window parentWindow = Window.GetWindow(this);
                    renameWindow.Owner = parentWindow;
                    bool? result = renameWindow.ShowDialog();
                    if (result.Value == true)
                    {
                        audioType.Name = renameWindow.ResultTextBox.Text;
                    }
                }
                DialogueAudibilityViewModel audiobility = this.NameSelector.SelectedItems[0] as DialogueAudibilityViewModel;
                if (audiobility != null)
                {
                    RenameWindow renameWindow = new RenameWindow(audiobility.Name);
                    Window parentWindow = Window.GetWindow(this);
                    renameWindow.Owner = parentWindow;
                    bool? result = renameWindow.ShowDialog();
                    if (result.Value == true)
                    {
                        audiobility.Name = renameWindow.ResultTextBox.Text;
                    }
                }
            }
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            CategoryViewModel dc = this.DataContext as CategoryViewModel;

            switch (dc.Text)
            {
                case "Characters":
                    {
                        string name = "NewCharacter";
                        List<int> taken = new List<int>();
                        taken.Add(0);
                        bool first = true;

                        foreach (object item in this.NameSelector.Items)
                        {
                            DialogueCharacterViewModel vm = item as DialogueCharacterViewModel;
                            if (vm == null)
                            {
                                continue;
                            }

                            string objectName = vm.Name;

                            if (objectName.StartsWith(name))
                            {
                                first = false;
                                string suffix = objectName.Substring(name.Length);
                                int number;
                                if (int.TryParse(suffix, out number))
                                {
                                    taken.Add(number);
                                }
                            }
                        }

                        if (!first)
                        {
                            int? firstAvailable = Enumerable.Range(0, int.MaxValue).Except(taken).FirstOrDefault();

                            if (firstAvailable.HasValue)
                            {
                                name += firstAvailable.Value;
                            }
                        }

                        Model.DialogueCharacter character = new Model.DialogueCharacter(Guid.NewGuid(), name, Guid.Empty);
                        dc.Model.Characters.Add(character);

                        foreach (object item in this.NameSelector.Items)
                        {
                            DialogueCharacterViewModel vm = item as DialogueCharacterViewModel;
                            if (vm == null)
                            {
                                continue;
                            }

                            if (Object.ReferenceEquals(vm.Model, character))
                            {
                                this.NameSelector.SelectedItem = vm;
                                this.NameSelector.ScrollIntoView(vm);
                                break;
                            }
                        }
                        break;
                    }

                case "Voices":
                    {
                        string name = "NewVoice";
                        List<int> taken = new List<int>();
                        taken.Add(0);
                        bool first = true;

                        foreach (object item in this.NameSelector.Items)
                        {
                            DialogueVoiceViewModel vm = item as DialogueVoiceViewModel;
                            if (vm == null)
                            {
                                continue;
                            }

                            string objectName = vm.Name;

                            if (objectName.StartsWith(name))
                            {
                                first = false;
                                string suffix = objectName.Substring(name.Length);
                                int number;
                                if (int.TryParse(suffix, out number))
                                {
                                    taken.Add(number);
                                }
                            }
                        }

                        if (!first)
                        {
                            int? firstAvailable = Enumerable.Range(0, int.MaxValue).Except(taken).FirstOrDefault();

                            if (firstAvailable.HasValue)
                            {
                                name += firstAvailable.Value;
                            }
                        }

                        Model.DialogueVoice voice = new Model.DialogueVoice(Guid.NewGuid(), name);
                        dc.Model.Voices.Add(voice);

                        foreach (object item in this.NameSelector.Items)
                        {
                            DialogueVoiceViewModel vm = item as DialogueVoiceViewModel;
                            if (vm == null)
                            {
                                continue;
                            }

                            if (Object.ReferenceEquals(vm.Model, voice))
                            {
                                this.NameSelector.SelectedItem = vm;
                                this.NameSelector.ScrollIntoView(vm);
                                break;
                            }
                        }
                        break;
                    }

                case "Specials":
                    {
                        string name = "NewSpecial";
                        List<int> taken = new List<int>();
                        taken.Add(0);
                        bool first = true;

                        foreach (object item in this.NameSelector.Items)
                        {
                            DialogueSpecialViewModel vm = item as DialogueSpecialViewModel;
                            if (vm == null)
                            {
                                continue;
                            }

                            string objectName = vm.Name;

                            if (objectName.StartsWith(name))
                            {
                                first = false;
                                string suffix = objectName.Substring(name.Length);
                                int number;
                                if (int.TryParse(suffix, out number))
                                {
                                    taken.Add(number);
                                }
                            }
                        }

                        if (!first)
                        {
                            int? firstAvailable = Enumerable.Range(0, int.MaxValue).Except(taken).FirstOrDefault();

                            if (firstAvailable.HasValue)
                            {
                                name += firstAvailable.Value;
                            }
                        }

                        Model.DialogueSpecial special = new Model.DialogueSpecial(Guid.NewGuid(), name);
                        dc.Model.Specials.Add(special);

                        foreach (object item in this.NameSelector.Items)
                        {
                            DialogueSpecialViewModel vm = item as DialogueSpecialViewModel;
                            if (vm == null)
                            {
                                continue;
                            }

                            if (Object.ReferenceEquals(vm.Model, special))
                            {
                                this.NameSelector.SelectedItem = vm;
                                this.NameSelector.ScrollIntoView(vm);
                                break;
                            }
                        }
                        break;
                    }

                case "Audio Types":
                    {
                        string name = "NewAudioType";
                        List<int> taken = new List<int>();
                        taken.Add(0);
                        bool first = true;

                        foreach (object item in this.NameSelector.Items)
                        {
                            DialogueAudioTypeViewModel vm = item as DialogueAudioTypeViewModel;
                            if (vm == null)
                            {
                                continue;
                            }

                            string objectName = vm.Name;

                            if (objectName.StartsWith(name))
                            {
                                first = false;
                                string suffix = objectName.Substring(name.Length);
                                int number;
                                if (int.TryParse(suffix, out number))
                                {
                                    taken.Add(number);
                                }
                            }
                        }

                        if (!first)
                        {
                            int? firstAvailable = Enumerable.Range(0, int.MaxValue).Except(taken).FirstOrDefault();

                            if (firstAvailable.HasValue)
                            {
                                name += firstAvailable.Value;
                            }
                        }

                        Model.DialogueAudioType audioType =
                            new Model.DialogueAudioType(Guid.NewGuid(), name, 0, null);
                        dc.Model.AudioTypes.Add(audioType);

                        foreach (object item in this.NameSelector.Items)
                        {
                            DialogueAudioTypeViewModel vm = item as DialogueAudioTypeViewModel;
                            if (vm == null)
                            {
                                continue;
                            }

                            if (Object.ReferenceEquals(vm.Model, audioType))
                            {
                                this.NameSelector.SelectedItem = vm;
                                this.NameSelector.ScrollIntoView(vm);
                                break;
                            }
                        }
                        break;
                    }

                case "Audibilities":
                    {
                        string name = "NewAudibility";
                        List<int> taken = new List<int>();
                        taken.Add(0);
                        bool first = true;

                        foreach (object item in this.NameSelector.Items)
                        {
                            DialogueAudibilityViewModel vm = item as DialogueAudibilityViewModel;
                            if (vm == null)
                            {
                                continue;
                            }

                            string objectName = vm.Name;

                            if (objectName.StartsWith(name))
                            {
                                first = false;
                                string suffix = objectName.Substring(name.Length);
                                int number;
                                if (int.TryParse(suffix, out number))
                                {
                                    taken.Add(number);
                                }
                            }
                        }

                        if (!first)
                        {
                            int? firstAvailable = Enumerable.Range(0, int.MaxValue).Except(taken).FirstOrDefault();

                            if (firstAvailable.HasValue)
                            {
                                name += firstAvailable.Value;
                            }
                        }

                        Model.DialogueAudibility audibility =
                            new Model.DialogueAudibility(Guid.NewGuid(), name, 0, null);
                        dc.Model.Audibilities.Add(audibility);

                        foreach (object item in this.NameSelector.Items)
                        {
                            DialogueAudibilityViewModel vm = item as DialogueAudibilityViewModel;
                            if (vm == null)
                            {
                                continue;
                            }

                            if (Object.ReferenceEquals(vm.Model, audibility))
                            {
                                this.NameSelector.SelectedItem = vm;
                                this.NameSelector.ScrollIntoView(vm);
                                break;
                            }
                        }
                        break;
                    }
            }

        }
    }
}
