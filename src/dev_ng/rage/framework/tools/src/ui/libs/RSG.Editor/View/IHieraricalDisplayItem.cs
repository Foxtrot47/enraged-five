﻿//---------------------------------------------------------------------------------------------
// <copyright file="IHieraricalDisplayItem.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    using System.Windows.Media.Imaging;

    /// <summary>
    /// When implemented represents a a view model that acts as a single display item that the
    /// view can render that also contains properties that are specific to a item instance a
    /// hierarchy.
    /// </summary>
    public interface IHieraricalDisplayItem : IDisplayItem
    {
        #region Properties
        /// <summary>
        /// Gets the bitmap source that is used to display the icon for this item when this
        /// item is in a expanded state.
        /// </summary>
        BitmapSource ExpandedIcon { get; }

        /// <summary>
        /// Gets a value indicating whether this display item can be expanded.
        /// </summary>
        bool IsExpandable { get; }
        #endregion Properties
    } // RSG.Editor.View.IHieraricalDisplayItem {Interface}
} // RSG.Editor.View {Namespace}
