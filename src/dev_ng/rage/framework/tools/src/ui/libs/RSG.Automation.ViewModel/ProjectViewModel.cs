﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Configuration;
using RSG.Base.Configuration.Automation;
using RSG.Base.Extensions;

namespace RSG.Automation.ViewModel
{

    /// <summary>
    /// (Core) Project View-Model.
    /// </summary>
    public class ProjectViewModel : ViewModelBase
    {
        #region Properties
        /// <summary>
        /// Project friendly-name.
        /// </summary>
        public String FriendlyName
        {
            get { return (this._project.FriendlyName); }
        }

        /// <summary>
        /// Studios.
        /// </summary>
        public ObservableCollection<StudioViewModel> Studios
        {
            get { return _studios; }
            private set
            {
                SetProperty(ref _studios, value, "Studios");
            }
        }
        private ObservableCollection<StudioViewModel> _studios;

        /// <summary>
        /// Whether this is the current user's project.
        /// </summary>
        public bool IsCurrentProject
        {
            get { return _isCurrentProject; }
            private set
            {
                SetProperty(ref _isCurrentProject, value, "IsCurrentProject");
            }
        }
        private bool _isCurrentProject;

        /// <summary>
        /// If the tree view for this project is expanded.
        /// </summary>
        public bool IsExpanded
        {
            get {return _isExpanded; }
            private set
            {
                SetProperty(ref _isExpanded, value, "IsExpanded");
            }
        }
        private bool _isExpanded;
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Project object.
        /// </summary>
        private IProject _project;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="project"></param>
        /// <param name="currentproject"></param>
        /// <param name="services"></param>
        public ProjectViewModel(IProject project, bool currentproject, 
            IEnumerable<IAutomationServiceConfig> services)
        {
            this._project = project;
            this._isCurrentProject = currentproject;
            this.Studios = new ObservableCollection<StudioViewModel>();
            this.Studios.Clear();
            this.IsExpanded = true;
            foreach (IStudio studio in this._project.Config.Studios)
            {
                bool localStudio = (this._project.Config.Studios.ThisStudio.Name.Equals(studio.Name));
                IEnumerable<IAutomationServiceConfig> studioServices =
                    services.Where(s => s.Studio.Name.Equals(studio.Name));
                this.Studios.Add(new StudioViewModel(studio, localStudio, studioServices));
            }
        }
        #endregion // Constructor(s)
    }

} // RSG.Automation.ViewModel namespace
