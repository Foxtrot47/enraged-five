﻿//---------------------------------------------------------------------------------------------
// <copyright file="DispatcherHelper.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Windows.Threading;

    /// <summary>
    /// A helper class for dispatcher operations on the UI thread. This class can be used
    /// inside a view model assembly to push actions onto the main UI thread without the need
    /// to constantly check the current application.
    /// </summary>
    public static class DispatcherHelper
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="UIDispatcher"/> property.
        /// </summary>
        private static Dispatcher _uiDispatcher;
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets a reference to the UI threads dispatcher object.
        /// </summary>
        public static Dispatcher UIDispatcher
        {
            get { return _uiDispatcher; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Executes an action on the UI thread. If this method is called from the UI thread,
        /// the action is executed immediately. If the method is called from another thread,
        /// the action will be enqueued on the UI thread's dispatcher and executed
        /// asynchronously.
        /// </summary>
        /// <param name="action">
        /// The action that will be executed on the UI thread.
        /// </param>
        /// <param name="priority">
        /// The priority, relative to the other pending operations in the event queue, the
        /// specified method is invoked.
        /// </param>
        public static void BeginInvokeOnUI(Action action, DispatcherPriority priority)
        {
            if (action == null)
            {
                return;
            }

            CheckDispatcher();
            UIDispatcher.BeginInvoke(action, priority);
        }

        /// <summary>
        /// Executes an action on the UI thread. If this method is called from the UI thread,
        /// the action is executed immediately. If the method is called from another thread,
        /// the action will be enqueued on the UI thread's dispatcher and executed
        /// asynchronously.
        /// </summary>
        /// <param name="action">
        /// The action that will be executed on the UI thread.
        /// </param>
        public static void CheckBeginInvokeOnUI(Action action)
        {
            if (action == null)
            {
                return;
            }

            CheckDispatcher();
            if (UIDispatcher.CheckAccess())
            {
                action();
            }
            else
            {
                UIDispatcher.BeginInvoke(action);
            }
        }

        /// <summary>
        /// This method should be called once on the UI thread to ensure that the
        /// <see cref="UIDispatcher"/> property is initialised. Call this method on the static
        /// App() constructor.
        /// </summary>
        public static void Initialize()
        {
            if (UIDispatcher != null && UIDispatcher.Thread.IsAlive)
            {
                return;
            }

            _uiDispatcher = Dispatcher.CurrentDispatcher;
        }

        /// <summary>
        /// Executes an action on the UI thread.
        /// </summary>
        /// <param name="action">
        /// The action that will be executed on the UI thread.
        /// </param>
        public static void InvokeOnUI(Action action)
        {
            if (action == null)
            {
                return;
            }

            CheckDispatcher();
            UIDispatcher.Invoke(action);
        }

        /// <summary>
        /// Executes an action on the UI thread.
        /// </summary>
        /// <param name="action">
        /// The action that will be executed on the UI thread.
        /// </param>
        /// <param name="priority">
        /// The priority, relative to the other pending operations in the event queue, the
        /// specified method is invoked.
        /// </param>
        public static void InvokeOnUI(Action action, DispatcherPriority priority)
        {
            if (action == null)
            {
                return;
            }

            CheckDispatcher();
            UIDispatcher.Invoke(action, priority);
        }

        /// <summary>
        /// Invokes an action asynchronously on the UI thread.
        /// </summary>
        /// <param name="action">
        /// The action that is executed.
        /// </param>
        /// <returns>
        /// An object that can be used to interact with the delegate as it is pending execution
        /// in the event queue.
        /// </returns>
        public static DispatcherOperation RunAsync(Action action)
        {
            CheckDispatcher();
            return UIDispatcher.BeginInvoke(action);
        }

        /// <summary>
        /// Checks to make sure the <see cref="Initialize"/> method has been called.
        /// </summary>
        private static void CheckDispatcher()
        {
            if (UIDispatcher != null)
            {
                return;
            }

            string msg =
                "The DispatcherHelper is not initialised. Call DispatcherHelper.Initialise() "
              + "in the static App constructor.";
            throw new InvalidOperationException(msg);
        }
        #endregion Methods
    } // RSG.Editor.DispatcherHelper {Class}
} // RSG.Editor {Namespace}
