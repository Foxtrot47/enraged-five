﻿//---------------------------------------------------------------------------------------------
// <copyright file="BuildSelectedProjectsAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Commands
{
    using System.Collections.Generic;
    using RSG.Editor;
    using RSG.Project.Model;
    using RSG.Project.ViewModel;

    /// <summary>
    /// Contains the logic for the <see cref="ProjectCommands.BuildSelectedProjects"/> routed
    /// command. This class cannot be inherited.
    /// </summary>
    public sealed class BuildSelectedProjectsAction : BuildActionBase
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BuildSelectedProjectsAction"/> class.
        /// </summary>
        /// <param name="serviceProvider">
        /// The service provider that is used by this action to perform application specific
        /// operations, for example selecting a file to open.
        /// </param>
        public BuildSelectedProjectsAction(ICommandServiceProvider serviceProvider)
            : base(serviceProvider)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="BuildSelectedProjectsAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public BuildSelectedProjectsAction(BuildProjectCommandResolver resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(BuildProjectCommandArgs args)
        {
            if (args == null || args.ProjectBuilder == null)
            {
                return false;
            }

            if (this.ProjectSelection(args.SelectedNodes).Count == 0)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(BuildProjectCommandArgs args)
        {
            this.Build(this.ProjectSelection(args.SelectedNodes), args);
        }

        /// <summary>
        /// Retrieves the project selection from the specified selection collection by
        /// collecting all of the selected nodes parent project and removing duplicates.
        /// </summary>
        /// <param name="selection">
        /// The collection containing all of the selected nodes.
        /// </param>
        /// <returns>
        /// The project selection from the specified selection collection.
        /// </returns>
        private List<Project> ProjectSelection(IEnumerable<IHierarchyNode> selection)
        {
            List<Project> projectSelection = new List<Project>();
            if (selection == null)
            {
                return projectSelection;
            }

            foreach (IHierarchyNode selectedNode in selection)
            {
                ProjectNode parentProject = selectedNode.ParentProject;
                if (parentProject != null)
                {
                    Project project = parentProject.Model as Project;
                    if (project != null && !projectSelection.Contains(project))
                    {
                        projectSelection.Add(project);
                    }
                }
            }

            return projectSelection;
        }
        #endregion Methods
    } // RSG.Project.Commands.BuildSelectedProjectsAction {Class}
} // RSG.Project.Commands {Namespace}
