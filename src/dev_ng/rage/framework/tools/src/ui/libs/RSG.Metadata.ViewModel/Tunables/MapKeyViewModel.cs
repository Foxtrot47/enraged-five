﻿//---------------------------------------------------------------------------------------------
// <copyright file="MapKeyViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Tunables
{
    using System;
    using System.Collections.Generic;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Editor.View;
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// Provides a base class to the view models that represent the map keys for tunables that
    /// belong to a map tunable.
    /// </summary>
    public abstract class MapKeyViewModel : ValidatingViewModelBase<KeyLookupItem>
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="MapItem"/> property.
        /// </summary>
        private ITunable _mapItem;

        /// <summary>
        /// The private field used for the <see cref="MapTunable"/> property.
        /// </summary>
        private MapTunableViewModel _mapTunableViewModel;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MapKeyViewModel"/> class.
        /// </summary>
        /// <param name="map">
        /// The map tunable whose key lookup is tied to this view model class.
        /// </param>
        /// <param name="item">
        /// The tunable item whose map key this view model represents.
        /// </param>
        /// <param name="key">
        /// The mapping model between the map item tunable and the map key.
        /// </param>
        protected MapKeyViewModel(MapTunableViewModel map, ITunable item, KeyLookupItem key)
            : base(key, true, true)
        {
            this._mapItem = item;
            this._mapTunableViewModel = map;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the tunable item whose key this view model represents.
        /// </summary>
        public ITunable MapItem
        {
            get { return this._mapItem; }
        }

        /// <summary>
        /// Gets the map tunable whose key lookup is tied to this view model class.
        /// </summary>
        public MapTunable MapTunable
        {
            get { return this._mapTunableViewModel.Model; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Makes sure that sibling key view models get validated on value change.
        /// </summary>
        /// <param name="propertyName">
        /// The name of the property whose errors have changed.
        /// </param>
        protected override void OnValidationRun(string propertyName)
        {
            if (!String.Equals(propertyName, "Value"))
            {
                return;
            }

            IList<ITunableViewModel> items =
                new List<ITunableViewModel>(this._mapTunableViewModel.Tunables);
            foreach (ITunableViewModel item in items)
            {
                MapKeyViewModel key = item.MapKey;
                if (key == null || Object.ReferenceEquals(key, this))
                {
                    continue;
                }

                key.Validate();
            }
        }

        /// <summary>
        /// Determines whether the validation pipeline should be started due to the property
        /// with the specified name changing.
        /// </summary>
        /// <param name="propertyName">
        /// The property name to test.
        /// </param>
        /// <returns>
        /// True if the validation should be run due to the specified property changing;
        /// otherwise, false.
        /// </returns>
        protected override bool ShouldRunValidation(string propertyName)
        {
            return String.Equals(propertyName, "Value");
        }
        #endregion Methods
    } // RSG.Metadata.ViewModel.Tunables.MapKeyViewModel {Class}
} // RSG.Metadata.ViewModel.Tunables {Namespace}
