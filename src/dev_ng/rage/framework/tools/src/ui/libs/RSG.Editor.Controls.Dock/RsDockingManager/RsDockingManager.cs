﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsDockingManager.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Markup;

    /// <summary>
    /// Represents the control used to house all of the docking elements.
    /// </summary>
    [ContentProperty("RootDockingGroup")]
    public class RsDockingManager : Control
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="RootDockingGroup"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty RootDockingGroupProperty;

        /// <summary>
        /// The private field used for the <see cref="DirtyIndicatorTemplate"/> property.
        /// </summary>
        private static ResourceKey _dirtyIndicatorTemplate;

        /// <summary>
        /// The private field used for the <see cref="DocumentHeaderTemplate"/> property.
        /// </summary>
        private static ResourceKey _documentHeaderTemplate;

        /// <summary>
        /// The private field used for the <see cref="ToolWindowHeaderTemplate"/> property.
        /// </summary>
        private static ResourceKey _toolWindowHeaderTemplate;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsDockingManager"/> class.
        /// </summary>
        static RsDockingManager()
        {
            _dirtyIndicatorTemplate = CreateTemplateKey("DirtyIndicatorTemplate");
            _documentHeaderTemplate = CreateTemplateKey("DocumentHeaderTemplate");
            _toolWindowHeaderTemplate = CreateTemplateKey("ToolWindowHeaderTemplate");

            RootDockingGroupProperty =
                DependencyProperty.Register(
                "RootDockingGroup",
                typeof(object),
                typeof(RsDockingManager),
                new PropertyMetadata(null));

            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsDockingManager),
                new FrameworkPropertyMetadata(typeof(RsDockingManager)));
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the resource key used to reference the template used on the dirty indicator
        /// for a document.
        /// </summary>
        public static ResourceKey DirtyIndicatorTemplate
        {
            get { return _dirtyIndicatorTemplate; }
        }

        /// <summary>
        /// Gets the resource key used to reference the data template used on the document
        /// headers.
        /// </summary>
        public static ResourceKey DocumentHeaderTemplate
        {
            get { return _documentHeaderTemplate; }
        }

        /// <summary>
        /// Gets the resource key used to reference the data template used on the tool window
        /// headers.
        /// </summary>
        public static ResourceKey ToolWindowHeaderTemplate
        {
            get { return _toolWindowHeaderTemplate; }
        }

        /// <summary>
        /// Gets or sets the System.Window.UIElement that is used to display the docking root.
        /// </summary>
        public object RootDockingGroup
        {
            get { return (object)this.GetValue(RootDockingGroupProperty); }
            set { this.SetValue(RootDockingGroupProperty, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new resource key that references a template with the specified name.
        /// </summary>
        /// <param name="styleName">
        /// The name that the resource key will be referencing.
        /// </param>
        /// <returns>
        /// A new resource key that references a template with the specified name.
        /// </returns>
        private static ResourceKey CreateTemplateKey(string styleName)
        {
            return new StyleKey<RsDockingManager>(styleName);
        }
        #endregion Methods
    } // RSG.Editor.Controls.Dock.RsDockingManager {Class}
} // RSG.Editor.Controls.Dock {Namespace}
