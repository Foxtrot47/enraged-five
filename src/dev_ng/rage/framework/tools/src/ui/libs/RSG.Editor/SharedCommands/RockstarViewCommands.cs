﻿//---------------------------------------------------------------------------------------------
// <copyright file="RockstarViewCommands.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.SharedCommands
{
    using System;
    using System.Globalization;
    using System.Windows.Input;
    using RSG.Base.Extensions;
    using RSG.Editor.Resources;

    /// <summary>
    /// Defines core view commands for any Rockstar C#/WPF application. This class cannot be
    /// inherited.
    /// </summary>
    public sealed class RockstarViewCommands
    {
        #region Fields
        /// <summary>
        /// The private cache of System.Windows.Input.RoutedCommand objects.
        /// </summary>
        private static RoutedCommand[] _internalCommands = InitialiseInternalStorage();
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Prevents a default instance of the <see cref="RockstarViewCommands"/> class from
        /// being created.
        /// </summary>
        private RockstarViewCommands()
        {
        }
        #endregion Constructors

        #region Enumerations
        /// <summary>
        /// Defines all of the command identifiers for the different core Rockstar commands.
        /// </summary>
        private enum CommandId : byte
        {
            /// <summary>
            /// Used to identifier the <see cref="RockstarViewCommands.CancelDialog"/> routed
            /// command.
            /// </summary>
            CancelDialog,

            /// <summary>
            /// Used to identifier the <see cref="RockstarViewCommands.CloseWindow"/> routed
            /// command.
            /// </summary>
            CloseWindow,

            /// <summary>
            /// Used to identifier the <see cref="RockstarViewCommands.ConfirmDialog"/> routed
            /// command.
            /// </summary>
            ConfirmDialog,

            /// <summary>
            /// Used to identifier the <see cref="RockstarViewCommands.MinimiseWindow"/> routed
            /// command.
            /// </summary>
            MinimiseWindow,

            /// <summary>
            /// Used to identifier the <see cref="RockstarViewCommands.MaximiseWindow"/> routed
            /// command.
            /// </summary>
            MaximiseWindow,

            /// <summary>
            /// Used to identifier the <see cref="RockstarViewCommands.RestoreWindow"/> routed
            /// command.
            /// </summary>
            RestoreWindow,

            /// <summary>
            /// Used to identifier the <see cref="RockstarViewCommands.ShowHelpFromTitleBar"/>
            /// routed command.
            /// </summary>
            ShowHelpFromTitleBar,

            /// <summary>
            /// Used to identifier the
            /// <see cref="RockstarViewCommands.ShowOptionsFromTitleBar"/> routed command.
            /// </summary>
            ShowOptionsFromTitleBar,

            /// <summary>
            /// Defines the number of commands defined as view commands for a Rockstar
            /// application.
            /// </summary>
            CommandCount
        }
        #endregion Enumerations

        #region Properties
        /// <summary>
        /// Gets the value that represents the Cancel Dialog command.
        /// </summary>
        public static RoutedCommand CancelDialog
        {
            get { return EnsureCommandExists(CommandId.CancelDialog); }
        }

        /// <summary>
        /// Gets the value that represents the Close Window command.
        /// </summary>
        public static RoutedCommand CloseWindow
        {
            get { return EnsureCommandExists(CommandId.CloseWindow); }
        }

        /// <summary>
        /// Gets the value that represents the Confirm Dialog command.
        /// </summary>
        public static RoutedCommand ConfirmDialog
        {
            get { return EnsureCommandExists(CommandId.ConfirmDialog); }
        }

        /// <summary>
        /// Gets the value that represents the Maximize Window command.
        /// </summary>
        public static RoutedCommand MaximiseWindow
        {
            get { return EnsureCommandExists(CommandId.MaximiseWindow); }
        }

        /// <summary>
        /// Gets the value that represents the Minimize Window command.
        /// </summary>
        public static RoutedCommand MinimiseWindow
        {
            get { return EnsureCommandExists(CommandId.MinimiseWindow); }
        }

        /// <summary>
        /// Gets the value that represents the Restore Window command.
        /// </summary>
        public static RoutedCommand RestoreWindow
        {
            get { return EnsureCommandExists(CommandId.RestoreWindow); }
        }

        /// <summary>
        /// Gets the value that represents the Show Help From TitleBar command.
        /// </summary>
        public static RoutedCommand ShowHelpFromTitleBar
        {
            get { return EnsureCommandExists(CommandId.ShowHelpFromTitleBar); }
        }

        /// <summary>
        /// Gets the value that represents the Show Options From TitleBar command.
        /// </summary>
        public static RoutedCommand ShowOptionsFromTitleBar
        {
            get { return EnsureCommandExists(CommandId.ShowOptionsFromTitleBar); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new System.Windows.Input.RoutedCommand object that is associated with the
        /// specified identifier.
        /// </summary>
        /// <param name="commandId">
        /// The identifier for the command to create.
        /// </param>
        /// <returns>
        /// The newly create System.Windows.Input.RoutedCommand object.
        /// </returns>
        private static RoutedCommand CreateCommand(CommandId commandId)
        {
            string name = GetName(commandId);
            return new RoutedCommand(name, typeof(RockstarViewCommands));
        }

        /// <summary>
        /// Makes sure the internal value for the command associated with the specified
        /// identifier exists and returns it.
        /// </summary>
        /// <param name="commandId">
        /// The identifier for the command that should be created and returned.
        /// </param>
        /// <returns>
        /// The System.Windows.Input.RoutedCommand object associated with the specified
        /// identifier.
        /// </returns>
        private static RoutedCommand EnsureCommandExists(CommandId commandId)
        {
            int commandIndex = (int)commandId;
            if (commandIndex < -1 || commandIndex >= _internalCommands.Length)
            {
                return null;
            }

            RoutedCommand command = _internalCommands[commandIndex];
            if (command == null)
            {
                lock (_internalCommands)
                {
                    if (command == null)
                    {
                        command = CreateCommand(commandId);
                        _internalCommands[commandIndex] = command;
                    }
                }
            }

            return command;
        }

        /// <summary>
        /// Get the name that corresponds to the specified command id.
        /// </summary>
        /// <param name="commandId">
        /// The command id whose name should be retrieved.
        /// </param>
        /// <returns>
        /// The corresponding name for the specified command id.
        /// </returns>
        private static string GetName(CommandId commandId)
        {
            string result = String.Empty;
            switch (commandId)
            {
                case CommandId.CancelDialog:
                    result = "Cancel Dialog";
                    break;
                case CommandId.CloseWindow:
                    result = "Close Window";
                    break;
                case CommandId.ConfirmDialog:
                    result = "Confirm Dialog";
                    break;
                case CommandId.MaximiseWindow:
                    result = "Maximise Window";
                    break;
                case CommandId.MinimiseWindow:
                    result = "Minimise Window";
                    break;
                case CommandId.RestoreWindow:
                    result = "Restore Window";
                    break;
                case CommandId.ShowHelpFromTitleBar:
                    result = "Show Help From Titlebar";
                    break;
                case CommandId.ShowOptionsFromTitleBar:
                    result = "Show Options From Titlebar";
                    break;
            }

            return result;
        }

        /// <summary>
        /// Initialises the array used to store the System.Windows.Input.RoutedCommand objects
        /// that make up the core commands for a Rockstar application.
        /// </summary>
        /// <returns>
        /// The array used to store the internal System.Windows.Input.RoutedCommand objects.
        /// </returns>
        private static RoutedCommand[] InitialiseInternalStorage()
        {
            return new RoutedCommand[(int)CommandId.CommandCount];
        }
        #endregion Methods
    } // RSG.Editor.SharedCommands.RockstarCommands {Class}
} // RSG.Editor.SharedCommands {Namespace}
