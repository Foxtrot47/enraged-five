﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using RSG.Editor.SharedCommands;

namespace RSG.Editor.Controls.MapViewport
{
    /// <summary>
    /// Contains the routed commands that can be used by the <see cref="MapViewport"/> control.
    /// </summary>
    public static class MapViewportCommands
    {
        #region Fields
        /// <summary>
        /// String table resource manager for getting at the command related strings.
        /// </summary>
        private static readonly CommandResourceManager _commandResourceManager =
            new CommandResourceManager(
                "RSG.Editor.Controls.MapViewport.Resources.CommandStringTable",
                typeof(MapViewportCommands).Assembly);

        /// <summary>
        /// The private cache of System.Windows.Input.RoutedCommand objects.
        /// </summary>
        private static readonly RockstarRoutedCommand[] _internalCommands =
            new RockstarRoutedCommand[Enum.GetValues(typeof(CommandId)).Length];
        #endregion

        #region Enumerations
        /// <summary>
        /// Defines all of the command identifiers for the different map viewer commands.
        /// </summary>
        private enum CommandId
        {
            /// <summary>
            /// Used to identifier the <see cref="MapViewportCommands.AddAnnotation"/>
            /// routed command.
            /// </summary>
            AddAnnotation,

            /// <summary>
            /// Used to identifier the <see cref="MapViewportCommands.AddAnnotationComment"/>
            /// routed command.
            /// </summary>
            AddAnnotationComment,

            /// <summary>
            /// Used to identifier the <see cref="MapViewportCommands.DeleteAnnotation"/>
            /// routed command.
            /// </summary>
            DeleteAnnotation,

            /// <summary>
            /// Used to identifier the <see cref="MapViewportCommands.DeleteAnnotationComment"/>
            /// routed command.
            /// </summary>
            DeleteAnnotationComment,

            /// <summary>
            /// Used to identifier the <see cref="MapViewportCommands.SaveToImage"/>
            /// routed command.
            /// </summary>
            SaveToImage
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the value that represents the add annotation command.
        /// </summary>
        public static RockstarRoutedCommand AddAnnotation
        {
            get { return EnsureCommandExists(CommandId.AddAnnotation, MapViewportIcons.AddAnnotation); }
        }

        /// <summary>
        /// Gets the value that represents the add annotation comment command.
        /// </summary>
        public static RockstarRoutedCommand AddAnnotationComment
        {
            get { return EnsureCommandExists(CommandId.AddAnnotationComment); }
        }

        /// <summary>
        /// Gets the value that represents the delete annotation command.
        /// </summary>
        public static RockstarRoutedCommand DeleteAnnotation
        {
            get { return EnsureCommandExists(CommandId.DeleteAnnotation, MapViewportIcons.DeleteAnnotation); }
        }

        /// <summary>
        /// Gets the value that represents the delete annotation comment command.
        /// </summary>
        public static RockstarRoutedCommand DeleteAnnotationComment
        {
            get { return EnsureCommandExists(CommandId.DeleteAnnotationComment, MapViewportIcons.DeleteAnnotation); }
        }

        /// <summary>
        /// Gets the value that represents the save to image command.
        /// </summary>
        public static RockstarRoutedCommand SaveToImage
        {
            get { return EnsureCommandExists(CommandId.SaveToImage, MapViewportIcons.SaveToImage); }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Creates a new System.Windows.Input.RoutedCommand object that is associated with the
        /// specified identifier.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command to create.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        /// <returns>
        /// The newly create System.Windows.Input.RoutedCommand object.
        /// </returns>
        private static RockstarRoutedCommand CreateCommand(String id, BitmapSource icon)
        {
            string name = _commandResourceManager.GetName(id);
            string category = _commandResourceManager.GetCategory(id);
            string description = _commandResourceManager.GetDescription(id);
            InputGestureCollection gestures = _commandResourceManager.GetGestures(id);

            return new RockstarRoutedCommand(
                name, typeof(MapViewportCommands), gestures, category, description, icon);
        }

        /// <summary>
        /// Makes sure the internal value for the command associated with the specified
        /// identifier exists and returns it.
        /// </summary>
        /// <param name="commandId">
        /// The identifier for the command that should be created and returned.
        /// </param>
        /// <param name="icon">
        /// The icon that the command should be using.
        /// </param>
        /// <returns>
        /// The <see cref="RSG.Editor.RockstarRoutedCommand"/> object associated with the
        /// specified identifier.
        /// </returns>
        private static RockstarRoutedCommand EnsureCommandExists(CommandId commandId, BitmapSource icon = null)
        {
            int commandIndex = (int)commandId;
            if (commandIndex < -1 || commandIndex >= _internalCommands.Length)
            {
                return null;
            }

            RockstarRoutedCommand command = _internalCommands[commandIndex];
            if (command == null)
            {
                lock (_internalCommands)
                {
                    if (command == null)
                    {
                        command = CreateCommand(commandId.ToString(), icon);
                        _internalCommands[commandIndex] = command;
                    }
                }
            }

            return command;
        }
        #endregion
    }
}
