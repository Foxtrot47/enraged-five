﻿//---------------------------------------------------------------------------------------------
// <copyright file="CollectionConverterExtensions.cs" company="Rockstar">
//     Copyright © Rockstar Games 2012. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// A static class containing extension methods for the
    /// <see cref="CollectionConverter{TSource,TTarget}"/> class.
    /// </summary>
    public static class CollectionConverterExtensions
    {
        #region Methods
        /// <summary>
        /// Helper extension method for creating a collection converter for a particular
        /// collection.
        /// </summary>
        /// <typeparam name="TSource">
        /// The type of elements in the source collection.
        /// </typeparam>
        /// <typeparam name="TTarget">
        /// The type of elements in the collection.
        /// </typeparam>
        /// <param name="sourceCollection">
        /// The source collection to convert.
        /// </param>
        /// <param name="converter">
        /// The converter that will be used once per source item before adding them to this
        /// collection.
        /// </param>
        /// <returns>
        /// A new collection converter class that has converted the items in the source
        /// enumerable to the target type.
        /// </returns>
        public static CollectionConverter<TSource, TTarget> Convert<TSource, TTarget>(
            this IEnumerable<TSource> sourceCollection, Func<TSource, TTarget> converter)
        {
            return new CollectionConverter<TSource, TTarget>(sourceCollection, converter);
        }
        #endregion Methods
    } // RSG.Editor.View.CollectionConverterExtensions {Class}
} // RSG.Editor.View {Namespace}
