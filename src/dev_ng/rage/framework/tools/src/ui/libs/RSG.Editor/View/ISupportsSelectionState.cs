﻿//---------------------------------------------------------------------------------------------
// <copyright file="ISupportsSelectionState.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    /// <summary>
    /// When implemented represents a item that contains a default expansion state that can be
    /// used by the user interface.
    /// </summary>
    public interface ISupportsSelectionState
    {
        #region Properties
        /// <summary>
        /// Gets a value indicating whether this item is selected by default in the user
        /// interface.
        /// </summary>
        bool IsSelectedByDefault { get; }
        #endregion Properties
    } // RSG.Editor.View.ISupportsSelectionState {Interface}
} // RSG.Editor.View {Namespace}
