﻿//---------------------------------------------------------------------------------------------
// <copyright file="TreeItemPresenter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Presenters
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Text.RegularExpressions;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Data;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using RSG.Base.Extensions;
    using RSG.Editor.Controls.AttachedDependencyProperties;
    using RSG.Editor.Controls.Chromes;
    using RSG.Editor.Controls.Converters;
    using RSG.Editor.Controls.Helpers;
    using RSG.Editor.Controls.Resources;
    using RSG.Editor.View;

    /// <summary>
    /// Displays the contents of a virtualised tree view item.
    /// </summary>
    public class TreeItemPresenter : CustomPresenterBase
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="Depth"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty DepthProperty;

        /// <summary>
        /// Identifies the <see cref="DisplayItem"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty DisplayItemProperty;

        /// <summary>
        /// Identifies the <see cref="DragOverArea"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty DragOverAreaProperty;

        /// <summary>
        /// Identifies the <see cref="ExpandedIcon"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ExpandedIconProperty;

        /// <summary>
        /// Identifies the <see cref="GlyphBrush"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty GlyphBrushProperty;

        /// <summary>
        /// Identifies the <see cref="GlyphMouseOverBrush"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty GlyphMouseOverBrushProperty;

        /// <summary>
        /// Identifies the <see cref="Icon"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IconProperty;

        /// <summary>
        /// Identifies the <see cref="IsCut"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsCutProperty;

        /// <summary>
        /// Identifies the <see cref="IsExpandable"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsExpandableProperty;

        /// <summary>
        /// Identifies the <see cref="IsExpanded"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsExpandedProperty;

        /// <summary>
        /// Identifies the <see cref="IsModified"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsModifiedProperty;

        /// <summary>
        /// Identifies the <see cref="IsSelected"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsSelectedProperty;

        /// <summary>
        /// Identifies the <see cref="IsSpinAnimationVisible"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsSpinAnimationVisibleProperty;

        /// <summary>
        /// Identifies the <see cref="ModifiedMarkTemplate"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ModifiedMarkTemplateProperty;

        /// <summary>
        /// Identifies the <see cref="OverlayIcon"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty OverlayIconProperty;

        /// <summary>
        /// Identifies the <see cref="ShowRootExpander"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ShowRootExpanderProperty;

        /// <summary>
        /// Identifies the <see cref="StateIcon"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty StateIconProperty;

        /// <summary>
        /// Identifies the <see cref="Text"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty TextProperty;

        /// <summary>
        /// The private field used for the <see cref="EditLabelPart"/> property.
        /// </summary>
        private EditLabelPresenterPart _editLabelPart;

        /// <summary>
        /// The private field used for the <see cref="DirectionalDropAdornerPart"/> property.
        /// </summary>
        private DirectionalDropAdornerPresenterPart _directionalDropPart;

        /// <summary>
        /// The private field used for the <see cref="DirtyMarkPart"/> property.
        /// </summary>
        private DirtyMarkPresenterPart _dirtyMarkPart;

        /// <summary>
        /// The private field used for the <see cref="ExpanderPart"/> property.
        /// </summary>
        private ExpanderPresenterPart _expanderPart;

        /// <summary>
        /// The private field used for the <see cref="IconPart"/> property.
        /// </summary>
        private IconPresenterPart _iconPart;

        /// <summary>
        /// The private field used for the <see cref="LabelPart"/> property.
        /// </summary>
        private LabelPresenterPart _labelPart;

        /// <summary>
        /// The private field used for the <see cref="SpinAnimationPart"/> property.
        /// </summary>
        private SpinAnimationPresenterPart _spinAnimationPart;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="TreeItemPresenter"/> class.
        /// </summary>
        static TreeItemPresenter()
        {
            ContentPresenter.RecognizesAccessKeyProperty.AddOwner(
                typeof(TreeItemPresenter),
                new FrameworkPropertyMetadata(
                    false, OnRecognizesAccessKeyChanged));

            DisplayItemProperty =
                DependencyProperty.Register(
                "DisplayItem",
                typeof(ITreeDisplayItem),
                typeof(TreeItemPresenter));

            IsSelectedProperty =
                DependencyProperty.Register(
                "IsSelected",
                typeof(bool),
                typeof(TreeItemPresenter),
                new FrameworkPropertyMetadata(
                    false, FrameworkPropertyMetadataOptions.AffectsRender));

            IsExpandedProperty =
                DependencyProperty.Register(
                "IsExpanded",
                typeof(bool),
                typeof(TreeItemPresenter),
                new FrameworkPropertyMetadata(
                    false, FrameworkPropertyMetadataOptions.AffectsRender));

            IsExpandableProperty =
                DependencyProperty.Register(
                "IsExpandable",
                typeof(bool),
                typeof(TreeItemPresenter),
                new FrameworkPropertyMetadata(
                    false, FrameworkPropertyMetadataOptions.AffectsRender));

            IsModifiedProperty =
                DependencyProperty.Register(
                "IsModified",
                typeof(bool),
                typeof(TreeItemPresenter),
                new FrameworkPropertyMetadata(
                    false, FrameworkPropertyMetadataOptions.AffectsRender));

            ModifiedMarkTemplateProperty =
                DependencyProperty.Register(
                "ModifiedMarkTemplate",
                typeof(ControlTemplate),
                typeof(TreeItemPresenter),
                new FrameworkPropertyMetadata(
                    null,
                    FrameworkPropertyMetadataOptions.AffectsRender,
                    OnModifiedMarkTemplateChanged));

            IsSpinAnimationVisibleProperty =
                DependencyProperty.Register(
                "IsSpinAnimationVisible",
                typeof(bool),
                typeof(TreeItemPresenter),
                new FrameworkPropertyMetadata(
                    false,
                    FrameworkPropertyMetadataOptions.AffectsRender,
                    OnIsSpinAnimationVisibleChanged));

            FrameworkPropertyMetadataOptions options =
                FrameworkPropertyMetadataOptions.AffectsMeasure |
                FrameworkPropertyMetadataOptions.AffectsArrange |
                FrameworkPropertyMetadataOptions.AffectsRender;

            DepthProperty =
                DependencyProperty.Register(
                "Depth",
                typeof(int),
                typeof(TreeItemPresenter),
                new FrameworkPropertyMetadata(0, options));

            ShowRootExpanderProperty =
                DependencyProperty.Register(
                "ShowRootExpander",
                typeof(bool),
                typeof(TreeItemPresenter),
                new FrameworkPropertyMetadata(true, options));

            TextProperty =
                DependencyProperty.Register(
                "Text",
                typeof(string),
                typeof(TreeItemPresenter),
                new FrameworkPropertyMetadata(null, CoerceText));

            IconProperty =
                DependencyProperty.Register(
                "Icon",
                typeof(BitmapSource),
                typeof(TreeItemPresenter),
                new FrameworkPropertyMetadata(
                    null, FrameworkPropertyMetadataOptions.AffectsRender));

            ExpandedIconProperty =
                DependencyProperty.Register(
                "ExpandedIcon",
                typeof(BitmapSource),
                typeof(TreeItemPresenter),
                new FrameworkPropertyMetadata(
                    null, FrameworkPropertyMetadataOptions.AffectsRender));

            OverlayIconProperty =
                DependencyProperty.Register(
                "OverlayIcon",
                typeof(BitmapSource),
                typeof(TreeItemPresenter),
                new FrameworkPropertyMetadata(
                    null, FrameworkPropertyMetadataOptions.AffectsRender));

            StateIconProperty =
                DependencyProperty.Register(
                "StateIcon",
                typeof(BitmapSource),
                typeof(TreeItemPresenter),
                new FrameworkPropertyMetadata(
                    null, FrameworkPropertyMetadataOptions.AffectsRender));

            IsCutProperty =
                DependencyProperty.Register(
                "IsCut",
                typeof(bool),
                typeof(TreeItemPresenter),
                new FrameworkPropertyMetadata(
                    false, FrameworkPropertyMetadataOptions.AffectsRender));

            DragOverAreaProperty =
                DependencyProperty.Register(
                "DragOverArea",
                typeof(DirectionalDropArea),
                typeof(TreeItemPresenter),
                new FrameworkPropertyMetadata(
                    DirectionalDropArea.None,
                    FrameworkPropertyMetadataOptions.AffectsRender,
                    OnDragOverAreaChanged));

            GlyphBrushProperty =
                DependencyProperty.Register(
                "GlyphBrush",
                typeof(Brush),
                typeof(TreeItemPresenter),
                new FrameworkPropertyMetadata(
                    null, FrameworkPropertyMetadataOptions.AffectsRender));

            GlyphMouseOverBrushProperty =
                DependencyProperty.Register(
                "GlyphMouseOverBrush",
                typeof(Brush),
                typeof(TreeItemPresenter),
                new FrameworkPropertyMetadata(
                    null, FrameworkPropertyMetadataOptions.AffectsRender));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="TreeItemPresenter"/> class.
        /// </summary>
        public TreeItemPresenter()
        {
            this.LabelPart.Connect();
            this.DirtyMarkPart.Connect();
            this.ExpanderPart.Connect();
            this.IconPart.Connect();

            this.Loaded += this.OnLoaded;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the depth index of this tree view item (the number of parents it has
        /// in the hierarchy).
        /// </summary>
        public int Depth
        {
            get { return (int)this.GetValue(DepthProperty); }
            set { this.SetValue(DepthProperty, value); }
        }

        /// <summary>
        /// Gets or sets the item that is being displayed for this tree view item.
        /// </summary>
        public ITreeDisplayItem DisplayItem
        {
            get { return (ITreeDisplayItem)this.GetValue(DisplayItemProperty); }
            set { this.SetValue(DisplayItemProperty, value); }
        }

        /// <summary>
        /// Gets or sets the drag over area value for this item.
        /// </summary>
        public DirectionalDropArea DragOverArea
        {
            get { return (DirectionalDropArea)this.GetValue(DragOverAreaProperty); }
            set { this.SetValue(DragOverAreaProperty, value); }
        }

        /// <summary>
        /// Gets or sets the icon that will be displayed for this tree view item when it is
        /// expanded.
        /// </summary>
        public BitmapSource ExpandedIcon
        {
            get { return (BitmapSource)this.GetValue(ExpandedIconProperty); }
            set { this.SetValue(ExpandedIconProperty, value); }
        }

        /// <summary>
        /// Gets or sets the brush to use for the expansion glyph.
        /// </summary>
        public Brush GlyphBrush
        {
            get { return (Brush)this.GetValue(GlyphBrushProperty); }
            set { this.SetValue(GlyphBrushProperty, value); }
        }

        /// <summary>
        /// Gets or sets the brush to use for the expansion glyph when the mouse is over it.
        /// </summary>
        public Brush GlyphMouseOverBrush
        {
            get { return (Brush)this.GetValue(GlyphMouseOverBrushProperty); }
            set { this.SetValue(GlyphMouseOverBrushProperty, value); }
        }

        /// <summary>
        /// Gets or sets the icon that will be displayed for this tree view item.
        /// </summary>
        public BitmapSource Icon
        {
            get { return (BitmapSource)this.GetValue(IconProperty); }
            set { this.SetValue(IconProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this item is currently in a cut state.
        /// </summary>
        public bool IsCut
        {
            get { return (bool)this.GetValue(IsCutProperty); }
            set { this.SetValue(IsCutProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this tree view item can be expanded.
        /// </summary>
        public bool IsExpandable
        {
            get { return (bool)this.GetValue(IsExpandableProperty); }
            set { this.SetValue(IsExpandableProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this tree view item is currently expanded.
        /// </summary>
        public bool IsExpanded
        {
            get { return (bool)this.GetValue(IsExpandedProperty); }
            set { this.SetValue(IsExpandedProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this tree view item is showing a item that
        /// is currently modified. This controls whether the dirty mark is shown.
        /// </summary>
        public bool IsModified
        {
            get { return (bool)this.GetValue(IsModifiedProperty); }
            set { this.SetValue(IsModifiedProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this tree item is selected.
        /// </summary>
        public bool IsSelected
        {
            get { return (bool)this.GetValue(IsSelectedProperty); }
            set { this.SetValue(IsSelectedProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this tree item presenter should be
        /// showing its spin animation.
        /// </summary>
        public bool IsSpinAnimationVisible
        {
            get { return (bool)this.GetValue(IsSpinAnimationVisibleProperty); }
            set { this.SetValue(IsSpinAnimationVisibleProperty, value); }
        }

        /// <summary>
        /// Gets or sets the overlay icon for this item.
        /// </summary>
        public BitmapSource OverlayIcon
        {
            get { return (BitmapSource)this.GetValue(OverlayIconProperty); }
            set { this.SetValue(OverlayIconProperty, value); }
        }

        /// <summary>
        /// Gets or sets the depth index of this tree view item (the number of parents it has
        /// in the hierarchy).
        /// </summary>
        public ControlTemplate ModifiedMarkTemplate
        {
            get { return (ControlTemplate)this.GetValue(ModifiedMarkTemplateProperty); }
            set { this.SetValue(ModifiedMarkTemplateProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the first underscore in the text should be
        /// ignored.
        /// </summary>
        public bool RecognizesAccessKey
        {
            get { return (bool)this.GetValue(ContentPresenter.RecognizesAccessKeyProperty); }
            set { this.SetValue(ContentPresenter.RecognizesAccessKeyProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the expanders for the root items should be
        /// shown or not.
        /// </summary>
        public bool ShowRootExpander
        {
            get { return (bool)this.GetValue(ShowRootExpanderProperty); }
            set { this.SetValue(ShowRootExpanderProperty, value); }
        }

        /// <summary>
        /// Gets or sets the state icon for this item.
        /// </summary>
        public BitmapSource StateIcon
        {
            get { return (BitmapSource)this.GetValue(StateIconProperty); }
            set { this.SetValue(StateIconProperty, value); }
        }

        /// <summary>
        /// Gets or sets the text that is displayed for the tree view item.
        /// </summary>
        public string Text
        {
            get { return (string)this.GetValue(TextProperty); }
            set { this.SetValue(TextProperty, value); }
        }

        /// <summary>
        /// Gets or sets a string that is displayed inside the presenters tooltip.
        /// </summary>
        public override string ToolTipText
        {
            get
            {
                string text = (string)this.GetValue(ToolTipTextProperty);
                if (string.IsNullOrEmpty(text))
                {
                    text = this.ResolveEmptyToolTipText();
                }

                if (this.IsModified)
                {
                    text = StringTable.TreeViewItemModifiedToolTip.FormatCurrent(text);
                }

                return text;
            }

            set
            {
                this.SetValue(TreeItemPresenter.ToolTipTextProperty, value);
            }
        }

        /// <summary>
        /// Gets a iterator around all the parts that have been initialised.
        /// </summary>
        protected override IEnumerable<PresenterPart> InitialisedParts
        {
            get
            {
                if (this._labelPart != null)
                {
                    yield return this._labelPart;
                }

                if (this._dirtyMarkPart != null)
                {
                    yield return this._dirtyMarkPart;
                }

                if (this._editLabelPart != null)
                {
                    yield return this._editLabelPart;
                }

                if (this._expanderPart != null)
                {
                    yield return this._expanderPart;
                }

                if (this._iconPart != null)
                {
                    yield return this._iconPart;
                }

                if (this._directionalDropPart != null)
                {
                    yield return this._directionalDropPart;
                }

                if (this._spinAnimationPart != null)
                {
                    yield return this._spinAnimationPart;
                }

                yield break;
            }
        }

        /// <summary>
        /// Gets a iterator around all the parts that affect the render size of this presenter.
        /// </summary>
        protected override IEnumerable<PresenterPart> PartsAffectingSize
        {
            get
            {
                yield return this.ExpanderPart;
                yield return this.LabelPart;
                yield return this.DirtyMarkPart;
                yield return this.IconPart;
                yield break;
            }
        }

        /// <summary>
        /// Gets the part of the presenter which is used to display a edit text control that
        /// can be used to rename the item.
        /// </summary>
        private EditLabelPresenterPart EditLabelPart
        {
            get
            {
                if (this._editLabelPart == null)
                {
                    this._editLabelPart = new EditLabelPresenterPart(this);
                }

                return this._editLabelPart;
            }
        }

        /// <summary>
        /// Gets the part of the presenter which is used to display the drop adorner for the
        /// tree view item.
        /// </summary>
        private DirectionalDropAdornerPresenterPart DirectionalDropAdornerPart
        {
            get
            {
                if (this._directionalDropPart == null)
                {
                    this._directionalDropPart = new DirectionalDropAdornerPresenterPart(this);
                }

                return this._directionalDropPart;
            }
        }

        /// <summary>
        /// Gets part of the presenter which is used to display the dirty mark for the tree
        /// view item.
        /// </summary>
        private DirtyMarkPresenterPart DirtyMarkPart
        {
            get
            {
                if (this._dirtyMarkPart == null)
                {
                    this._dirtyMarkPart = new DirtyMarkPresenterPart(this);
                }

                return this._dirtyMarkPart;
            }
        }

        /// <summary>
        /// Gets part of the presenter which is used to display the expander control for the
        /// tree view item.
        /// </summary>
        private ExpanderPresenterPart ExpanderPart
        {
            get
            {
                if (this._expanderPart == null)
                {
                    this._expanderPart = new ExpanderPresenterPart(this);
                }

                return this._expanderPart;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this item has a valid state tooltip.
        /// </summary>
        private bool HasStateToolTip
        {
            get { return !string.IsNullOrEmpty(this.GetDisplayItemStateToolTipText()); }
        }

        /// <summary>
        /// Gets the part of the presenter which is used to display the icon for the tree view
        /// item.
        /// </summary>
        private IconPresenterPart IconPart
        {
            get
            {
                if (this._iconPart == null)
                {
                    this._iconPart = new IconPresenterPart(this);
                }

                return this._iconPart;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this item has been disconnected with the rest of
        /// the user interface.
        /// </summary>
        private bool IsDisconnected
        {
            get
            {
                if (!this.IsConnectedToPresentationSource())
                {
                    return true;
                }

                ISupportsDisposalState pattern = this.DisplayItem as ISupportsDisposalState;
                return pattern != null && pattern.IsDisposed;
            }
        }

        /// <summary>
        /// Gets part of the presenter which is used to display the label control for the tree
        /// view item.
        /// </summary>
        private LabelPresenterPart LabelPart
        {
            get
            {
                if (this._labelPart == null)
                {
                    this._labelPart = new LabelPresenterPart(this);
                }

                return this._labelPart;
            }
        }

        /// <summary>
        /// Gets the part of the presenter which is used to display the spin animation control
        /// for the tree view item.
        /// </summary>
        private SpinAnimationPresenterPart SpinAnimationPart
        {
            get
            {
                if (this._spinAnimationPart == null)
                {
                    this._spinAnimationPart = new SpinAnimationPresenterPart(this);
                }

                return this._spinAnimationPart;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Determines whether the tooltip for this object can be shown based on the relative
        /// position the tooltip wants to appear.
        /// </summary>
        /// <param name="position">
        /// The relative position to where the tooltip wants to be shown.
        /// </param>
        /// <returns>
        /// True if the tooltip can be shown; otherwise, false.
        /// </returns>
        public override bool CanShowToolTip(Point position)
        {
            return this.HasStateToolTip && this.IconPart.Bounds.Contains(position);
        }

        /// <summary>
        /// Retrieves the point to click during a automation operation.
        /// </summary>
        /// <returns>
        /// The clickable point during a automation operation.
        /// </returns>
        public override Point GetAutomationClickablePoint()
        {
            bool connectedToSource = PresentationSource.FromDependencyObject(this) != null;
            if (!connectedToSource)
            {
                return new Point(double.NaN, double.NaN);
            }

            Rect bounds = new Rect(0.0, 0.0, this.ActualWidth, this.ActualHeight);
            ScrollContentPresenter scrollPresenter =
                this.GetVisualAncestor<ScrollContentPresenter>();
            if (scrollPresenter != null)
            {
                double width = scrollPresenter.ActualWidth;
                double height = scrollPresenter.ActualHeight;
                GeneralTransform transform = scrollPresenter.TransformToDescendant(this);
                Rect scrollBounds = new Rect(0.0, 0.0, width, height);
                Rect transformBounds = transform.TransformBounds(scrollBounds);
                bounds.Intersect(transformBounds);
            }

            if (bounds.IsEmpty)
            {
                return new Point(double.NaN, double.NaN);
            }

            if (!this.ExcludeRect(ref bounds, this.ExpanderPart.Bounds))
            {
                return new Point(double.NaN, double.NaN);
            }

            double centreX = Math.Floor(bounds.Left + (bounds.Width * 0.5));
            double centreY = Math.Floor(bounds.Top + (bounds.Height * 0.5));
            return this.PointToScreen(new Point(centreX, centreY));
        }

        /// <summary>
        /// Retrieves the content that should be shown inside the tooltip presenter.
        /// </summary>
        /// <returns>
        /// The content that should be shown inside the tooltip presenter.
        /// </returns>
        public override object GetToolTipContent()
        {
            return this.GetDisplayItemStateToolTipText();
        }

        /// <summary>
        /// Resolves the tooltip text when the ToolTipTextProperty is empty or null.
        /// </summary>
        /// <returns>
        /// The text to use as a tooltip when the ToolTipTextProperty is empty or null.
        /// </returns>
        public override string ResolveEmptyToolTipText()
        {
            return this.Text;
        }

        /// <summary>
        /// Enters edit mode that shows the text edit control and moves focus inside it.
        /// </summary>
        /// <param name="renameController">
        /// The rename controller that specifies the rename behaviour to use.
        /// </param>
        internal void EnterEditMode(IRenameController renameController)
        {
            this.EditLabelPart.EnterEditMode(renameController);
        }

        /// <summary>
        /// Exits edit mode which hides the edit text control.
        /// </summary>
        internal void ExitEditMode()
        {
            this.EditLabelPart.ExitEditMode();
        }

        /// <summary>
        /// Called whenever the mouse is pressed down over this presenter.
        /// </summary>
        /// <param name="e">
        /// The event data used to retrieve the mouse position and containing the mouse state.
        /// </param>
        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            if (e.ChangedButton != MouseButton.Left)
            {
                base.OnMouseDown(e);
                return;
            }

            if (this.ExpanderPart.IsVisible && this.ExpanderPart.IsMouseOver)
            {
                this.IsExpanded = !this.IsExpanded;
                if (this.IsExpanded && this.IsConnectedToPresentationSource())
                {
                    VirtualisedTreeNode node = this.DataContext as VirtualisedTreeNode;
                    if (node != null)
                    {
                        node.ScrollExpansionIntoView();
                    }
                }

                RsVirtualisedTreeViewItem item
                    = this.GetVisualAncestor<RsVirtualisedTreeViewItem>();
                if (item != null && !item.IsKeyboardFocusWithin)
                {
                    item.Focus();
                }

                e.Handled = true;
            }

            base.OnMouseDown(e);
        }

        /// <summary>
        /// Called whenever the <see cref="Text"/> dependency property needs to be
        /// re-evaluated.
        /// </summary>
        /// <param name="s">
        /// The <see cref="TreeItemPresenter"/> instance whose dependency property needs
        /// evaluated.
        /// </param>
        /// <param name="baseValue">
        /// The new value of the property, prior to any coercion attempt.
        /// </param>
        /// <returns>
        /// The coerced value.
        /// </returns>
        private static object CoerceText(DependencyObject s, object baseValue)
        {
            TreeItemPresenter presenter = s as TreeItemPresenter;
            if (presenter == null)
            {
                Debug.Assert(presenter != null, "Handler attached to unexpected object");
                return baseValue;
            }

            string result = baseValue as string;
            if (result == null)
            {
                return baseValue;
            }

            if (presenter.RecognizesAccessKey)
            {
                Regex regex = new Regex(Regex.Escape("_"));
                result = regex.Replace(result, String.Empty, 1);
            }

            return result;
        }

        /// <summary>
        /// Called when the <see cref="DragOverArea"/> dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The dependency object whose <see cref="DragOverArea"/> dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data used for the event.
        /// </param>
        private static void OnDragOverAreaChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            TreeItemPresenter presenter = s as TreeItemPresenter;
            if (presenter == null)
            {
                Debug.Assert(presenter != null, "Handler attached to unexpected object");
                return;
            }

            switch ((DirectionalDropArea)e.NewValue)
            {
                case DirectionalDropArea.None:
                case DirectionalDropArea.On:
                    presenter.DirectionalDropAdornerPart.Disconnect();
                    return;
                default:
                    presenter.DirectionalDropAdornerPart.Connect();
                    break;
            }
        }

        /// <summary>
        /// Called when the <see cref="IsSpinAnimationVisible"/> dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The dependency object whose <see cref="IsSpinAnimationVisible"/> dependency
        /// property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data used for the event.
        /// </param>
        private static void OnIsSpinAnimationVisibleChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            TreeItemPresenter presenter = s as TreeItemPresenter;
            if (presenter == null)
            {
                Debug.Assert(presenter != null, "Handler attached to unexpected object");
                return;
            }

            if (presenter.IsSpinAnimationVisible)
            {
                presenter.SpinAnimationPart.Connect();
            }
            else
            {
                presenter.SpinAnimationPart.Disconnect();
            }
        }

        /// <summary>
        /// Called when the <see cref="ModifiedMarkTemplate"/> dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The dependency object whose <see cref="ModifiedMarkTemplate"/> dependency property
        /// changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data used for the event.
        /// </param>
        private static void OnModifiedMarkTemplateChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            TreeItemPresenter presenter = s as TreeItemPresenter;
            if (presenter == null)
            {
                Debug.Fail("Handler attached to unexpected object");
                return;
            }

            presenter.DirtyMarkPart.UpdateTemplate();
        }

        /// <summary>
        /// Called when the <see cref="RecognizesAccessKey"/> dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The dependency object whose <see cref="RecognizesAccessKey"/> dependency property
        /// changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data used for the event.
        /// </param>
        private static void OnRecognizesAccessKeyChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            TreeItemPresenter presenter = s as TreeItemPresenter;
            if (presenter == null)
            {
                Debug.Fail("Handler attached to unexpected object");
                return;
            }

            presenter.CoerceValue(TreeItemPresenter.TextProperty);
        }

        /// <summary>
        /// Removes the intersection between a bounds rectangle and a specified exclude
        /// rectangle and puts the resulting bounds into the initial bounds.
        /// </summary>
        /// <param name="bounds">
        /// The rectangle that contains the initial bounds and when this method returns
        /// contains the initial bounds with the specified bounds to exclude taken away.
        /// </param>
        /// <param name="boundsToExclude">
        /// The rectangle that contains the bounds to exclude.
        /// </param>
        /// <returns>
        /// True if the resulting bounds rectangle is valid; otherwise, false.
        /// </returns>
        private bool ExcludeRect(ref Rect bounds, Rect boundsToExclude)
        {
            if (!bounds.IntersectsWith(boundsToExclude))
            {
                return true;
            }

            if (boundsToExclude.Left > bounds.Left)
            {
                bounds.Width = boundsToExclude.Left - bounds.Left;
            }
            else
            {
                if (boundsToExclude.Right >= bounds.Right)
                {
                    return false;
                }

                double right = boundsToExclude.Right;
                double width = bounds.Right - boundsToExclude.Right;
                bounds.X = right;
                bounds.Width = width;
            }

            return true;
        }

        /// <summary>
        /// Gets the state tool tip text from the associated display item is possible.
        /// </summary>
        /// <returns>
        /// The state tool tip text from the associated display item.
        /// </returns>
        private string GetDisplayItemStateToolTipText()
        {
            if (this.DisplayItem == null || this.IsDisconnected)
            {
                return String.Empty;
            }

            return this.DisplayItem.StateToolTipText;
        }

        /// <summary>
        /// Called when this element is laid out, rendered and ready for interaction.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs data for this event.
        /// </param>
        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            FrameworkElement element = sender as FrameworkElement;
            if (element != null)
            {
                element.Loaded -= this.OnLoaded;
            }

            RelativeSource source = new RelativeSource(
                RelativeSourceMode.FindAncestor, typeof(RsVirtualisedTreeView), 1);

            Binding binding = new Binding("RecognizesAccessKey");
            binding.RelativeSource = source;
            this.SetBinding(ContentPresenter.RecognizesAccessKeyProperty, binding);

            binding = new Binding("ShowRootExpander");
            binding.RelativeSource = source;
            this.SetBinding(TreeItemPresenter.ShowRootExpanderProperty, binding);
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// Represents a presenter part that contains a directional drop adorner control.
        /// </summary>
        private class DirectionalDropAdornerPresenterPart : PresenterPart
        {
            #region Fields
            /// <summary>
            /// The actual UI control that is drawn for this part.
            /// </summary>
            private DirectionalDropAdorner _adorner;

            /// <summary>
            /// The tree item presenter that this part belongs to.
            /// </summary>
            private TreeItemPresenter _owner;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the
            /// <see cref="DirectionalDropAdornerPresenterPart"/> class.
            /// </summary>
            /// <param name="owner">
            /// The presenter that this part belongs to.
            /// </param>
            public DirectionalDropAdornerPresenterPart(TreeItemPresenter owner)
                : base(owner)
            {
                this._owner = owner;
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets the UIElement that represents this layout part.
            /// </summary>
            public override UIElement Element
            {
                get { return this._adorner; }
            }
            #endregion Properties

            #region Methods
            /// <summary>
            /// Override this method to calculate the position of this layout part and return
            /// the top-left corner.
            /// </summary>
            /// <returns>
            /// The point that defines the left-top corner of this layout part.
            /// </returns>
            protected override Point Arrange()
            {
                double y;
                if (this._owner.DragOverArea == DirectionalDropArea.Above)
                {
                    y = -2.0;
                }
                else
                {
                    y = this.Owner.ActualHeight - 3.0;
                }

                ExpanderPresenterPart expanderPath = this._owner.ExpanderPart;
                return new Point(expanderPath.Left + expanderPath.Width, y);
            }

            /// <summary>
            /// Override this method to measure the UI control that represents this layout part
            /// and return the size.
            /// </summary>
            /// <returns>
            /// The size that defines the width and height of this layout part.
            /// </returns>
            protected override Size Measure()
            {
                double actualWidth = this._owner.ActualWidth;
                double num = this._owner.ExpanderPart.Left + this._owner.ExpanderPart.Width;
                double width = Math.Max(0.0, actualWidth - num - 1.0);
                return new Size(width, 5.0);
            }

            /// <summary>
            /// Override to handle when this layout part gets connected to the parent tree view
            /// presenter.
            /// </summary>
            protected override void OnConnected()
            {
                this._adorner = new DirectionalDropAdorner();
                this.Owner.InsertChildSorted(this._adorner);
            }

            /// <summary>
            /// Override to handle when this layout part gets disconnected to the parent tree
            /// view presenter.
            /// </summary>
            protected override void OnDisconnected()
            {
                this.Owner.Children.Remove(this._adorner);
                this._adorner = null;
            }
            #endregion Methods
        } // TreeItemPresenter.DirectionalDropAdornerPresenterPart {Class}

        /// <summary>
        /// Represents a presenter part that contains the dirty mark.
        /// </summary>
        private class DirtyMarkPresenterPart : PresenterPart
        {
            #region Fields
            /// <summary>
            /// The actual UI control that is drawn for this part.
            /// </summary>
            private Control _control;

            /// <summary>
            /// The tree item presenter that this part belongs to.
            /// </summary>
            private TreeItemPresenter _owner;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="DirtyMarkPresenterPart"/> class
            /// for the specified presenter.
            /// </summary>
            /// <param name="owner">
            /// The presenter that this part belongs to.
            /// </param>
            public DirtyMarkPresenterPart(TreeItemPresenter owner)
                : base(owner)
            {
                this._owner = owner;
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets the UIElement that represents this layout part.
            /// </summary>
            public override UIElement Element
            {
                get { return this._control; }
            }
            #endregion Properties

            #region Methods
            /// <summary>
            /// Resets the controls template to the owners modified mark template.
            /// </summary>
            public void UpdateTemplate()
            {
                this.EnsureTextControl();
                this._control.Template = this._owner.ModifiedMarkTemplate;
            }

            /// <summary>
            /// Override this method to calculate the position of this layout part and return
            /// the top-left corner.
            /// </summary>
            /// <returns>
            /// The point that defines the left-top corner of this layout part.
            /// </returns>
            protected override Point Arrange()
            {
                PresenterPart labelPart = this._owner.LabelPart;
                double edge = this.GetCentredEdge(this.Owner.ContentHeight, this.Height);

                return new Point(labelPart.Left + labelPart.Width, edge);
            }

            /// <summary>
            /// Override this method to measure the UI control that represents this layout part
            /// and return the size.
            /// </summary>
            /// <returns>
            /// The size that defines the width and height of this layout part.
            /// </returns>
            protected override Size Measure()
            {
                Size size = new Size(0.0, 0.0);
                if (this._control != null)
                {
                    size = this._control.DesiredSize;
                }

                return new Size(size.Width, size.Height);
            }

            /// <summary>
            /// Override to handle when this layout part gets connected to the parent tree view
            /// presenter.
            /// </summary>
            protected override void OnConnected()
            {
                this.EnsureTextControl();
                this.Owner.InsertChildSorted(this._control);
            }

            /// <summary>
            /// Override to handle when this layout part gets disconnected to the parent tree
            /// view presenter.
            /// </summary>
            protected override void OnDisconnected()
            {
                this.Owner.Children.Remove(this._control);
            }

            /// <summary>
            /// Ensures that the text control has been created and initialised.
            /// </summary>
            private void EnsureTextControl()
            {
                if (this._control != null)
                {
                    return;
                }

                this._control = new Control();
                this._control.Template = this._owner.ModifiedMarkTemplate;
                this.UpdateVisibilityBinding();
                Panel.SetZIndex(this._control, 0);
            }

            /// <summary>
            /// Updates the text binding for the text control displayed in this part.
            /// </summary>
            private void UpdateVisibilityBinding()
            {
                Binding binding = new Binding();
                binding.Source = this.Owner;
                binding.Path = new PropertyPath(TreeItemPresenter.IsModifiedProperty);
                binding.Converter = new System.Windows.Controls.BooleanToVisibilityConverter();
                this._control.SetBinding(RsHighlightTextBlock.VisibilityProperty, binding);
            }
            #endregion Methods
        } // TreeItemPresenter.DirtyMarkPresenterPart {Class}

        /// <summary>
        /// Represents a presenter part that contains a text box control.
        /// </summary>
        private class EditLabelPresenterPart : PresenterPart
        {
            #region Fields
            /// <summary>
            /// The tree item presenter that this part belongs to.
            /// </summary>
            private TreeItemPresenter _owner;

            /// <summary>
            /// The actual UI control that is drawn for this part.
            /// </summary>
            private TextBox _textControl;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="EditLabelPresenterPart"/> class
            /// for the specified presenter.
            /// </summary>
            /// <param name="owner">
            /// The presenter that this part belongs to.
            /// </param>
            public EditLabelPresenterPart(TreeItemPresenter owner)
                : base(owner)
            {
                this._owner = owner;
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets the UIElement that represents this layout part.
            /// </summary>
            public override UIElement Element
            {
                get { return this._textControl; }
            }
            #endregion Properties

            #region Methods
            /// <summary>
            /// Connects the text control and sets up the text box as well as disconnect the
            /// label control.
            /// </summary>
            /// <param name="renameController">
            /// The rename controller that specifies the rename behaviour to use.
            /// </param>
            public void EnterEditMode(IRenameController renameController)
            {
                if (this.IsConnected)
                {
                    return;
                }

                this._textControl = new EditTextBox(renameController);

                Panel.SetZIndex(this._textControl, 2);
                this._textControl.Loaded += this.TextControlLoaded;
                this._owner.LabelPart.Disconnect();
                this.Connect();
            }

            /// <summary>
            /// Disconnects the text control and reconnects the label control.
            /// </summary>
            public void ExitEditMode()
            {
                if (this.IsConnected)
                {
                    this.Disconnect();
                    this._owner.LabelPart.Connect();
                    this._textControl = null;
                }
            }

            /// <summary>
            /// Override this method to calculate the position of this layout part and return
            /// the top-left corner.
            /// </summary>
            /// <returns>
            /// The point that defines the left-top corner of this layout part.
            /// </returns>
            protected override Point Arrange()
            {
                PresenterPart iconPart = this._owner.IconPart;
                double width = iconPart.Width;
                if (this._owner.IsSpinAnimationVisible)
                {
                    width = this._owner.SpinAnimationPart.Width;
                }

                double x = iconPart.Left + width + 2.0;
                double y = 1.0 + this.GetCentredEdge(this.Owner.ContentHeight, this.Height);
                return new Point(x, y);
            }

            /// <summary>
            /// Override this method to measure the UI control that represents this layout part
            /// and return the size.
            /// </summary>
            /// <returns>
            /// The size that defines the width and height of this layout part.
            /// </returns>
            protected override Size Measure()
            {
                Size size = new Size(0.0, 0.0);
                if (this._textControl != null)
                {
                    size = this._textControl.DesiredSize;
                }

                return new Size(size.Width, size.Height);
            }

            /// <summary>
            /// Override to handle when this layout part gets connected to the parent tree view
            /// presenter.
            /// </summary>
            protected override void OnConnected()
            {
                this.Owner.InsertChildSorted(this._textControl);
            }

            /// <summary>
            /// Override to handle when this layout part gets disconnected to the parent tree
            /// view presenter.
            /// </summary>
            protected override void OnDisconnected()
            {
                this.Owner.Children.Remove(this._textControl);
            }

            /// <summary>
            /// Called whenever the text control is loaded so we can move focus inside the
            /// control and the right text is automatically selected.
            /// </summary>
            /// <param name="sender">
            /// The text control that loaded {this._textControl}.
            /// </param>
            /// <param name="e">
            /// The event data.
            /// </param>
            private void TextControlLoaded(object sender, RoutedEventArgs e)
            {
                this._textControl.SelectAll();
                this._textControl.Focus();
            }
            #endregion Methods

            #region Classes
            /// <summary>
            /// The text box control that is used for this edit label presenter.
            /// </summary>
            private class EditTextBox : TextBox
            {
                #region Fields
                /// <summary>
                /// The rename controller that specifies the rename behaviour to use.
                /// </summary>
                private IRenameController _renameController;
                #endregion Fields

                #region Constructors
                /// <summary>
                /// Initialises a new instance of the <see cref="EditTextBox"/> class.
                /// </summary>
                /// <param name="renameController">
                /// The rename controller that specifies the rename behaviour to use.
                /// </param>
                public EditTextBox(IRenameController renameController)
                {
                    this._renameController = renameController;
                    this.Cursor = Cursors.IBeam;
                    this.Padding = new Thickness(0.0, 0.0, 0.0, 0.0);
                    Type type = typeof(TextBox);
                    Style baseStyle = (Style)Application.Current.FindResource(type);

                    Style style = new Style(type, baseStyle);
                    style.Seal();
                    this.Style = style;

                    Binding textBinding = new Binding("Text");
                    textBinding.Source = renameController;
                    textBinding.Mode = BindingMode.TwoWay;
                    textBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                    this.SetBinding(TextBox.TextProperty, textBinding);
                }
                #endregion Constructors

                #region Methods
                /// <summary>
                /// Need to set the padding in here or the <see cref="OnLostKeyboardFocus"/>
                /// method will be fired when hitting the background.
                /// </summary>
                /// <param name="constraint">
                /// Specifies the constraints of the size of the text box.
                /// </param>
                /// <returns>
                /// The measured size of this text box.
                /// </returns>
                protected override Size MeasureOverride(Size constraint)
                {
                    Size result = base.MeasureOverride(constraint);
                    result.Width += 10.0;
                    return result;
                }

                /// <summary>
                /// Makes sure that the appropriate action is taken if the user hits enter or
                /// escaped. If enter is pressed the renaming is commit, and on escape the
                /// renaming is cancelled.
                /// </summary>
                /// <param name="e">
                /// The System.Windows.Input.KeyEventArgs containing the event data.
                /// </param>
                protected override void OnKeyDown(KeyEventArgs e)
                {
                    if (this._renameController.IsIncomplete)
                    {
                        Key key = e.Key;
                        if (key == Key.Return)
                        {
                            this._renameController.Commit(true);
                            e.Handled = true;
                        }
                        else if (key == Key.Escape)
                        {
                            this._renameController.Cancel(true);
                            e.Handled = true;
                        }
                    }

                    base.OnKeyDown(e);
                }

                /// <summary>
                /// On lost keyboard focus as long as the context menu isn't opened we need to
                /// commit or cancel depending on where the focus has gone.
                /// </summary>
                /// <param name="e">
                /// The System.Windows.Input.KeyboardFocusChangedEventArgs containing the event
                /// data.
                /// </param>
                protected override void OnLostKeyboardFocus(KeyboardFocusChangedEventArgs e)
                {
                    Debug.WriteLine("Lost Focus");
                    if (this._renameController.IsIncomplete && this.IsLoaded)
                    {
                        if (InputManager.Current.IsInMenuMode)
                        {
                            ContextMenu contextMenu = e.NewFocus as ContextMenu;
                            if (contextMenu == null || contextMenu.PlacementTarget != this)
                            {
                                this._renameController.Cancel(false);
                            }
                        }
                        else
                        {
                            this._renameController.Commit(false);
                        }
                    }

                    base.OnLostKeyboardFocus(e);
                }
                #endregion Methods
            } // EditLabelPresenterPart.EditTextBox {Class}
            #endregion Classes
        } // TreeItemPresenter.EditLabelPresenterPart {Class}

        /// <summary>
        /// Represents a presenter part that contains a expander control.
        /// </summary>
        private class ExpanderPresenterPart : PresenterPart
        {
            #region Fields
            /// <summary>
            /// The height in pixels of the box the geometry path is draw in.
            /// </summary>
            private const double GlyphBoxHeight = 16.0;

            /// <summary>
            /// The width in pixels of the box the geometry path is draw in.
            /// </summary>
            private const double GlyphBoxWidth = 16.0;

            /// <summary>
            /// The number of pixels each level of the tree view is indented by.
            /// </summary>
            private const double IndentationPerLevel = 18.0;

            /// <summary>
            /// The path geometry used to draw the expander when the tree view item is
            /// collapsed.
            /// </summary>
            private static readonly Geometry _collapsedGeometry;

            /// <summary>
            /// The path geometry used to draw the expander when the tree view item is
            /// expanded.
            /// </summary>
            private static readonly Geometry _expandedGeometry;

            /// <summary>
            /// The tree item presenter that this part belongs to.
            /// </summary>
            private TreeItemPresenter _owner;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises static members of the <see cref="ExpanderPresenterPart"/> class.
            /// </summary>
            static ExpanderPresenterPart()
            {
                _expandedGeometry = Geometry.Parse("M 5 0 L 0 5 L 5 5 Z");
                _collapsedGeometry = Geometry.Parse("M 0 0 L 4 4 L 0 8 Z");
            }

            /// <summary>
            /// Initialises a new instance of the <see cref="ExpanderPresenterPart"/> class.
            /// </summary>
            /// <param name="owner">
            /// The presenter that this part belongs to.
            /// </param>
            public ExpanderPresenterPart(TreeItemPresenter owner)
                : base(owner)
            {
                this._owner = owner;
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets a value indicating whether the expander is visible.
            /// </summary>
            public bool IsVisible
            {
                get
                {
                    if (!this._owner.IsExpandable)
                    {
                        return false;
                    }

                    if (this._owner.Depth == 0 && !this._owner.ShowRootExpander)
                    {
                        return false;
                    }

                    return true;
                }
            }

            /// <summary>
            /// Gets a value indicating whether the expander glyph should be filled or not.
            /// </summary>
            private bool IsGlyphFilled
            {
                get
                {
                    if (this.IsOwnerActiveAndSelected && this.IsMouseOver)
                    {
                        return !this._owner.IsExpanded;
                    }

                    return this._owner.IsExpanded;
                }
            }

            /// <summary>
            /// Gets a value indicating whether the owning tree item presenter is both selected
            /// and has keyboard focus within it.
            /// </summary>
            private bool IsOwnerActiveAndSelected
            {
                get { return this._owner.IsSelected && this._owner.IsSelectionActive; }
            }
            #endregion Properties

            #region Methods
            /// <summary>
            /// Use the specified drawing context to render this layout part is the layout part
            /// doesn't have a specific UI element.
            /// </summary>
            /// <param name="context">
            /// The drawing context to use to render this layout part.
            /// </param>
            public override void Render(DrawingContext context)
            {
                if (!this.IsVisible)
                {
                    return;
                }

                Geometry geometry = _collapsedGeometry;
                if (this._owner.IsExpanded)
                {
                    geometry = _expandedGeometry;
                }

                Rect bounds = geometry.Bounds;
                Brush glyphBrush = this.GetGlyphBrush();
                Pen pen = new Pen(glyphBrush, 1.0);
                double penOffset = pen.Thickness * 0.5;
                double widthOffset = Math.Ceiling((this.Width - bounds.Width) * 0.5);
                double heightOffset = Math.Ceiling((this.Height - bounds.Height) * 0.5);
                double offsetX = this.Left + widthOffset - penOffset;
                double offsetY = this.Top + heightOffset - penOffset;

                Brush fillBrush = null;
                if (this.IsGlyphFilled)
                {
                    fillBrush = glyphBrush;
                }

                context.PushTransform(new TranslateTransform(offsetX, offsetY));
                try
                {
                    context.DrawGeometry(fillBrush, pen, geometry);
                }
                finally
                {
                    context.Pop();
                }
            }

            /// <summary>
            /// Calculates the position of this layout part and return the top-left corner.
            /// </summary>
            /// <returns>
            /// The point that defines the left-top corner of this layout part.
            /// </returns>
            protected override Point Arrange()
            {
                double num = 0.0;
                if (!this._owner.ShowRootExpander)
                {
                    num = this.Width;
                }

                double x = 4.0 + (this._owner.Depth * IndentationPerLevel) - num;
                double y = 2.0 + this.GetCentredEdge(this._owner.ContentHeight, this.Height);
                return new Point(x, y);
            }

            /// <summary>
            /// Measures the UI control that represents this layout part and return the size.
            /// </summary>
            /// <returns>
            /// The size that defines the width and height of this layout part.
            /// </returns>
            protected override Size Measure()
            {
                return new Size(GlyphBoxWidth, GlyphBoxHeight);
            }

            /// <summary>
            /// Invalidates the visual whenever the IsMouseOver property changes as the
            /// <see cref="IsGlyphFilled"/> could have changed.
            /// </summary>
            protected override void OnIsMouseOverChanged()
            {
                this._owner.InvalidateVisual();
            }

            /// <summary>
            /// Retrieves the brush to use to draw the expander.
            /// </summary>
            /// <returns>
            /// The brush to use to draw the expander.
            /// </returns>
            private Brush GetGlyphBrush()
            {
                if (!this._owner.IsEnabled)
                {
                    return this._owner.GlyphBrush;
                }

                if (this.IsMouseOver)
                {
                    return this._owner.GlyphMouseOverBrush;
                }

                return this._owner.GlyphBrush;
            }
            #endregion Methods
        } // TreeItemPresenter.ExpanderPresenterPart {Class}

        /// <summary>
        /// Represents a presenter part that contains a icon.
        /// </summary>
        private class IconPresenterPart : PresenterPart
        {
            #region Fields
            /// <summary>
            /// The height in pixels the icon is rendered.
            /// </summary>
            private const int IconHeight = 16;

            /// <summary>
            /// The width in pixels the icon is rendered.
            /// </summary>
            private const int IconWidth = 16;

            /// <summary>
            /// The width in pixels the state icon is rendered.
            /// </summary>
            public const int StateIconWidth = 7;

            /// <summary>
            /// The tree item presenter that this part belongs to.
            /// </summary>
            private TreeItemPresenter _owner;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="IconPresenterPart"/> class.
            /// </summary>
            /// <param name="owner">
            /// The presenter that this part belongs to.
            /// </param>
            public IconPresenterPart(TreeItemPresenter owner)
                : base(owner)
            {
                this._owner = owner;
            }
            #endregion Constructors

            #region Methods
            /// <summary>
            /// Use the specified drawing context to render this layout part is the layout part
            /// doesn't have a specific UI element.
            /// </summary>
            /// <param name="context">
            /// The drawing context to use to render this layout part.
            /// </param>
            public override void Render(DrawingContext context)
            {
                if (this._owner.IsSpinAnimationVisible)
                {
                    return;
                }

                BitmapSource icon = this._owner.Icon;
                BitmapSource stateIcon = this._owner.StateIcon;
                BitmapSource overlayIcon = this._owner.OverlayIcon;
                if (this._owner.IsExpanded && this._owner.ExpandedIcon != null)
                {
                    icon = this._owner.ExpandedIcon;
                }

                Point point = new Point(this.Left, this.Top).LogicalToDeviceUnits();
                double x = point.X + StateIconWidth;
                double y = point.Y;

                this.DrawIcon(context, icon, x, y, IconWidth, IconHeight);
                this.DrawIcon(context, overlayIcon, x, y, IconWidth, IconHeight);

                x = point.X;
                this.DrawIcon(context, stateIcon, x, y, StateIconWidth, IconHeight);
            }

            /// <summary>
            /// Calculates the position of this layout part and return the top-left corner.
            /// </summary>
            /// <returns>
            /// The point that defines the left-top corner of this layout part.
            /// </returns>
            protected override Point Arrange()
            {
                double expanderWidth = 0;
                expanderWidth = this._owner.ExpanderPart.Width;

                double x = this._owner.ExpanderPart.Left + expanderWidth + 1.0;
                double y = 2.0 + this.GetCentredEdge(this.Owner.ContentHeight, this.Height);
                return new Point(x, y);
            }

            /// <summary>
            /// Measures the UI control that represents this layout part and return the size.
            /// </summary>
            /// <returns>
            /// The size that defines the width and height of this layout part.
            /// </returns>
            protected override Size Measure()
            {
                BitmapSource icon = this._owner.Icon;
                BitmapSource stateIcon = this._owner.StateIcon;
                if (this._owner.IsExpanded && this._owner.ExpandedIcon != null)
                {
                    icon = this._owner.ExpandedIcon;
                }

                if (icon != null)
                {
                    return new Size(IconWidth + StateIconWidth, IconHeight).DeviceToLogicalUnits();
                }

                return new Size(0.0, 0.0);
            }

            /// <summary>
            /// Draws the specified image source using the specified drawing context.
            /// </summary>
            /// <param name="context">
            /// The drawing context to use to render the icon.
            /// </param>
            /// <param name="icon">
            /// The image source of the icon to render.
            /// </param>
            /// <param name="x">
            /// The left-most position of the icon.
            /// </param>
            /// <param name="y">
            /// The top-most position of the icon.
            /// </param>
            /// <param name="width">
            /// The width of the icon to render.
            /// </param>
            /// <param name="height">
            /// The height of the icon to render.
            /// </param>
            private void DrawIcon(
                DrawingContext context,
                BitmapSource icon,
                double x,
                double y,
                double width,
                double height)
            {
                if (icon != null)
                {
                    Brush background = ThemeProperties.GetImageBackgroundColour(this.Owner);
                    BrushToColourConverter converter = new BrushToColourConverter();
                    ThemeBitmapArguments args = new ThemeBitmapArguments(icon);
                    args.Background = converter.Convert(background);
                    args.Enabled = this._owner.IsEnabled;
                    if (this._owner.IsEnabled == false)
                    {
                        args.Bias = Color.FromArgb(127, 255, 255, 255);
                    }
                    else
                    {
                        args.Bias = Colors.Transparent;
                    }

                    icon = ThemeHelper.GetOrCreateThemedBitmapSource(args);

                    TransformGroup transformGroup = new TransformGroup();
                    transformGroup.Children.Add(new TranslateTransform(x, y));
                    transformGroup.Children.Add(DpiHelper.TransformFromDevice);
                    context.PushTransform(transformGroup);

                    try
                    {
                        Rect rectangle = new Rect(0.0, 0.0, width, height);
                        context.DrawImage(icon, rectangle);
                    }
                    finally
                    {
                        context.Pop();
                    }
                }
            }
            #endregion
        } // TreeItemPresenter.IconPresenterPart {Class}

        /// <summary>
        /// Represents a presenter part that contains a highlighting text block control.
        /// </summary>
        private class LabelPresenterPart : PresenterPart, IToolTipVisibilityController
        {
            #region Fields
            /// <summary>
            /// The tree item presenter that this part belongs to.
            /// </summary>
            private TreeItemPresenter _owner;

            /// <summary>
            /// The actual UI control that is drawn for this part.
            /// </summary>
            private RsHighlightTextBlock _textControl;

            /// <summary>
            /// The tooltip visibility manager that this part uses.
            /// </summary>
            private ToolTipVisibilityManager _toolTipVisibilityManager;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="LabelPresenterPart"/> class for
            /// the specified presenter.
            /// </summary>
            /// <param name="owner">
            /// The presenter that this part belongs to.
            /// </param>
            public LabelPresenterPart(TreeItemPresenter owner)
                : base(owner)
            {
                this._owner = owner;
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets the UIElement that represents this layout part.
            /// </summary>
            public override UIElement Element
            {
                get { return this._textControl; }
            }
            #endregion Properties

            #region Methods
            /// <summary>
            /// Determines whether the tooltip for this object can be shown based on the
            /// relative position the tooltip wants to appear.
            /// </summary>
            /// <param name="position">
            /// The relative position to where the tooltip wants to be shown.
            /// </param>
            /// <returns>
            /// True if the tooltip can be shown; otherwise, false.
            /// </returns>
            public bool CanShowToolTip(Point position)
            {
                bool trimmed = this._textControl.IsTrimmed();
                bool clipped = this._textControl.IsClipped();
                return this.Owner.HasContentToolTip || clipped || trimmed;
            }

            /// <summary>
            /// Retrieves the content that should be shown inside the tooltip presenter.
            /// </summary>
            /// <returns>
            /// The content that should be shown inside the tooltip presenter.
            /// </returns>
            public object GetToolTipContent()
            {
                if (this.Owner.HasContentToolTip)
                {
                    return this.Owner.ToolTipContent;
                }

                return this._owner.ToolTipText;
            }

            /// <summary>
            /// Updates the tooltip placement whenever the content for the tooltip changes.
            /// </summary>
            public override void UpdateToolTipPlacement()
            {
                if (this._toolTipVisibilityManager == null)
                {
                    return;
                }

                if (this.Owner.HasContentToolTip)
                {
                    this._toolTipVisibilityManager.SetToolTipPlacement(PlacementMode.Bottom);
                    this._toolTipVisibilityManager.SetToolTipOffsets(0.0, 3.0);
                    return;
                }

                this._toolTipVisibilityManager.SetToolTipPlacement(PlacementMode.Relative);
                this._toolTipVisibilityManager.SetToolTipOffsets(-7.0, -5.0);
            }

            /// <summary>
            /// Override this method to calculate the position of this layout part and return
            /// the top-left corner.
            /// </summary>
            /// <returns>
            /// The point that defines the left-top corner of this layout part.
            /// </returns>
            protected override Point Arrange()
            {
                PresenterPart iconPart = this._owner.IconPart;
                double x = iconPart.Left + iconPart.Width + 5.0;
                double y = 2.0 + this.GetCentredEdge(this.Owner.ContentHeight, this.Height);
                return new Point(x, y);
            }

            /// <summary>
            /// Override this method to measure the UI control that represents this layout part
            /// and return the size.
            /// </summary>
            /// <returns>
            /// The size that defines the width and height of this layout part.
            /// </returns>
            protected override Size Measure()
            {
                Size size = new Size(0.0, 0.0);
                if (this._textControl != null)
                {
                    size = this._textControl.DesiredSize;
                }

                return new Size(size.Width, size.Height);
            }

            /// <summary>
            /// Override to handle when this layout part gets connected to the parent tree view
            /// presenter.
            /// </summary>
            protected override void OnConnected()
            {
                this.EnsureTextControl();
                this.Owner.InsertChildSorted(this._textControl);
            }

            /// <summary>
            /// Override to handle when this layout part gets disconnected to the parent tree
            /// view presenter.
            /// </summary>
            protected override void OnDisconnected()
            {
                this.Owner.Children.Remove(this._textControl);
            }

            /// <summary>
            /// Ensures that the text control has been created and initialised.
            /// </summary>
            private void EnsureTextControl()
            {
                if (this._textControl != null)
                {
                    return;
                }

                this._textControl = new RsHighlightTextBlock();
                this.UpdateTextBinding();
                Panel.SetZIndex(this._textControl, 0);
                this._toolTipVisibilityManager =
                    new ToolTipVisibilityManager(this._textControl, this);
                this.UpdateToolTipPlacement();
            }

            /// <summary>
            /// Updates the text binding for the text control displayed in this part.
            /// </summary>
            private void UpdateTextBinding()
            {
                Binding binding = new Binding();
                binding.Source = this.Owner;
                binding.Path = new PropertyPath(TreeItemPresenter.TextProperty);
                this._textControl.SetBinding(RsHighlightTextBlock.TextProperty, binding);
            }
            #endregion Methods
        } // TreeItemPresenter.LabelPresenterPart {Class}

        /// <summary>
        /// Represents a presenter part that contains a circler spinning control.
        /// </summary>
        private class SpinAnimationPresenterPart : PresenterPart
        {
            #region Fields
            /// <summary>
            /// The pixel width and height of the spin animation control.
            /// </summary>
            private const int SpinIconSize = 14;

            /// <summary>
            /// The tree item presenter that this part belongs to.
            /// </summary>
            private TreeItemPresenter _owner;

            /// <summary>
            /// The private reference to the spin animation control.
            /// </summary>
            private RsSpinAnimationControl _spinAnimation;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="SpinAnimationPresenterPart"/>
            /// class.
            /// </summary>
            /// <param name="owner">
            /// The presenter that this part belongs to.
            /// </param>
            public SpinAnimationPresenterPart(TreeItemPresenter owner)
                : base(owner)
            {
                this._owner = owner;
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets the UIElement that represents this layout part.
            /// </summary>
            public override UIElement Element
            {
                get { return this._spinAnimation; }
            }
            #endregion Properties

            #region Methods
            /// <summary>
            /// Calculates the position of this layout part and return the top-left corner.
            /// </summary>
            /// <returns>
            /// The point that defines the left-top corner of this layout part.
            /// </returns>
            protected override Point Arrange()
            {
                return new Point(this._owner.IconPart.Left + IconPresenterPart.StateIconWidth + 1, this._owner.IconPart.Top + 1);
            }

            /// <summary>
            /// Measures the UI control that represents this layout part and return the size.
            /// </summary>
            /// <returns>
            /// The size that defines the width and height of this layout part.
            /// </returns>
            protected override Size Measure()
            {
                return new Size(SpinIconSize, SpinIconSize);
            }

            /// <summary>
            /// Connects this layout part to the parent tree item presenter.
            /// </summary>
            protected override void OnConnected()
            {
                this._spinAnimation = new RsSpinAnimationControl
                {
                    Width = SpinIconSize,
                    Height = SpinIconSize
                };

                this._spinAnimation.IsSpinning = true;
                this.Owner.InsertChildSorted(this._spinAnimation);
            }

            /// <summary>
            /// Disconnects this layout part to the parent tree item presenter.
            /// </summary>
            protected override void OnDisconnected()
            {
                this.Owner.RemoveChild(this._spinAnimation);
                this._spinAnimation.IsSpinning = false;
                this._spinAnimation = null;
            }
            #endregion Methods
        } // TreeItemPresenter.SpinAnimationPresenterPart {Class}
        #endregion
    } // RSG.Editor.Controls.Presenters.TreeItemPresenter {Class}
} // RSG.Editor.Controls.Presenters {Namespace}
