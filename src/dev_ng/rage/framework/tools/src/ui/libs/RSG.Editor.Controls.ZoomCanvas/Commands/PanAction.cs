﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace RSG.Editor.Controls.ZoomCanvas.Commands
{
    /// <summary>
    /// The action logic that is responsible for panning the zoomable canvas by a
    /// specified amount.
    /// </summary>
    public class PanAction : ButtonAction<ZoomableCanvas, Vector>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="PanAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public PanAction(ParameterResolverDelegate<ZoomableCanvas> resolver)
            : base(resolver)
        {
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="control">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="amount">
        /// The amount we wish to pan by.
        /// </param>
        public override void Execute(ZoomableCanvas control, Vector amount)
        {
            control.Offset += amount;
        }
        #endregion
    }
}
