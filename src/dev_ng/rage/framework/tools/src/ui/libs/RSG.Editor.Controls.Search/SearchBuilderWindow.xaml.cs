﻿//---------------------------------------------------------------------------------------------
// <copyright file="SearchBuilderWindow.xaml.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Search
{
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Input;

    /// <summary>
    /// Interaction logic for SearchBuilder Window XAML file.
    /// </summary>
    public partial class SearchBuilderWindow : RsWindow
    {
        #region Fields
        /// <summary>
        /// Declares the routed command used to remove a search group.
        /// </summary>
        private static RoutedCommand _removeSearchGroup;

        /// <summary>
        /// Declares the routed command used to remove a search parameter.
        /// </summary>
        private static RoutedCommand _removeSearchParameter;

        /// <summary>
        /// Declares the routed command used to add a search group.
        /// </summary>
        private static RoutedCommand _addSearchGroup;

        /// <summary>
        /// Declares the routed command used to add a search parameter.
        /// </summary>
        private static RoutedCommand _addSearchParameter;

        /// <summary>
        /// The private collection containing the root search groups shown in this window.
        /// </summary>
        private ObservableCollection<SearchGroup> _groups;

        /// <summary>
        /// The private field used for the <see cref="RegisteredSearchTerms"/> property.
        /// </summary>
        private ObservableCollection<RegisteredSearchTerm> _registeredSearchTerms;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="SearchBuilderWindow"/> class.
        /// </summary>
        static SearchBuilderWindow()
        {
            _removeSearchGroup =
                new RoutedCommand("RemoveSearchGroup", typeof(SearchBuilderWindow));
            _removeSearchParameter =
                new RoutedCommand("RemoveSearchParameter", typeof(SearchBuilderWindow));
            _addSearchGroup =
                new RoutedCommand("AddSearchGroup", typeof(SearchBuilderWindow));
            _addSearchParameter =
                new RoutedCommand("AddSearchParameter", typeof(SearchBuilderWindow));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="SearchBuilderWindow"/> class.
        /// </summary>
        public SearchBuilderWindow()
        {
            this._registeredSearchTerms = new ObservableCollection<RegisteredSearchTerm>();
            this._groups = new ObservableCollection<SearchGroup>();
            this.InitializeComponent();

            Binding binding = new Binding();
            binding.Source = this._groups;
            binding.BindsDirectlyToSource = true;
            this.SearchGroups.SetBinding(ItemsControl.ItemsSourceProperty, binding);

            this.CommandBindings.Add(
                new CommandBinding(RemoveSearchGroup, this.ExecuteRemoveSearchGroup));

            this.CommandBindings.Add(
                new CommandBinding(AddSearchGroup, this.ExecuteAddSearchGroup));

            this.CommandBindings.Add(
                new CommandBinding(AddSearchParameter, this.ExecuteAddSearchParameter));

            this.CommandBindings.Add(
                new CommandBinding(RemoveSearchParameter, this.ExecuteRemoveSearchParameter));
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the routed command used to remove a search group.
        /// </summary>
        public static RoutedCommand RemoveSearchGroup
        {
            get { return _removeSearchGroup; }
        }

        /// <summary>
        /// Gets the routed command used to remove a search parameter.
        /// </summary>
        public static RoutedCommand RemoveSearchParameter
        {
            get { return _removeSearchParameter; }
        }

        /// <summary>
        /// Gets the routed command used to add a search group.
        /// </summary>
        public static RoutedCommand AddSearchGroup
        {
            get { return _addSearchGroup; }
        }

        /// <summary>
        /// Gets the routed command used to add a search parameter.
        /// </summary>
        public static RoutedCommand AddSearchParameter
        {
            get { return _addSearchParameter; }
        }

        /// <summary>
        /// Gets the collection containing the search terms that can be used in this window.
        /// </summary>
        public ObservableCollection<RegisteredSearchTerm> RegisteredSearchTerms
        {
            get { return this._registeredSearchTerms; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Registers a search term with this window.
        /// </summary>
        /// <param name="name">
        /// The name of the search term.
        /// </param>
        /// <param name="type">
        /// The data type the search term is associated with.
        /// </param>
        public void RegisterSearchTerm(string name, SearchFieldType type)
        {
            this._registeredSearchTerms.Add(new RegisteredSearchTerm(name, type));
        }

        /// <summary>
        /// Called when the <see cref="RemoveSearchGroup"/> routed command is executed.
        /// </summary>
        /// <param name="sender">
        /// The sender of the command. (This window).
        /// </param>
        /// <param name="e">
        /// The command data including the search group that should be removed.
        /// </param>
        public void ExecuteRemoveSearchGroup(object sender, ExecutedRoutedEventArgs e)
        {
            SearchGroup parameter = (SearchGroup)e.Parameter;
            if (this._groups.Contains(parameter))
            {
                this._groups.Remove(parameter);
                return;
            }

            foreach (SearchGroup searchGroup in this._groups)
            {
                searchGroup.RemoveChildGroup(parameter);
            }
        }

        /// <summary>
        /// Called when the <see cref="AddSearchGroup"/> routed command is executed.
        /// </summary>
        /// <param name="sender">
        /// The sender of the command. (This window).
        /// </param>
        /// <param name="e">
        /// The command data including the scope the new search group should be added to.
        /// </param>
        public void ExecuteAddSearchGroup(object sender, ExecutedRoutedEventArgs e)
        {
            SearchGroup newGroup = new SearchGroup();
            newGroup.IsExpanded = true;
            if (e.Parameter == null)
            {
                this._groups.Add(newGroup);
                return;
            }

            SearchGroup parameter = (SearchGroup)e.Parameter;
            parameter.Groups.Add(newGroup);
        }

        /// <summary>
        /// Called when the <see cref="AddSearchParameter"/> routed command is executed.
        /// </summary>
        /// <param name="sender">
        /// The sender of the command. (This window).
        /// </param>
        /// <param name="e">
        /// The command data including the scope the new search parameter should be added to.
        /// </param>
        public void ExecuteAddSearchParameter(object sender, ExecutedRoutedEventArgs e)
        {
            SearcherViewModel newSearcher = new SearcherViewModel(new Searcher());
            newSearcher.SelectedSearchTerm = this.RegisteredSearchTerms.FirstOrDefault();

            SearchGroup parameter = (SearchGroup)e.Parameter;
            parameter.FieldSearches.Add(newSearcher);
        }

        /// <summary>
        /// Called when the <see cref="RemoveSearchParameter"/> routed command is executed.
        /// </summary>
        /// <param name="sender">
        /// The sender of the command. (This window).
        /// </param>
        /// <param name="e">
        /// The command data including the search parameter that should be removed.
        /// </param>
        public void ExecuteRemoveSearchParameter(object sender, ExecutedRoutedEventArgs e)
        {
            SearcherViewModel parameter = (SearcherViewModel)e.Parameter;
            foreach (SearchGroup searchGroup in this._groups)
            {
                searchGroup.RemoveSearcher(parameter);
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.Search.SearchBuilderWindow {Class}
} // RSG.Editor.Controls.Search {Namespace}
