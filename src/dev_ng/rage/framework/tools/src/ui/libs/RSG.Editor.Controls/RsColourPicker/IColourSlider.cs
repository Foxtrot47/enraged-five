﻿//---------------------------------------------------------------------------------------------
// <copyright file="IColourSlider.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Windows.Data;

    /// <summary>
    /// When implemented represents a colour slider control that can have bindings attached to
    /// its colour property and supports notifying a listener when a drag event has finished.
    /// </summary>
    public interface IColourSlider
    {
        #region Events
        /// <summary>
        /// Occurs as soon as the drag operation has finished.
        /// </summary>
        event EventHandler DragFinished;
        #endregion Events

        #region Methods
        /// <summary>
        /// Clears the binding on the colour dependency property.
        /// </summary>
        void ClearColourBinding();

        /// <summary>
        /// Set the colour dependency property to the specified binding object.
        /// </summary>
        /// <param name="binding">
        /// The binding object to set on the colour dependency property.
        /// </param>
        void SetColourBinding(Binding binding);
        #endregion Methods
    } // RSG.Editor.Controls.RsRgbaColourSlider {Class}
} // RSG.Editor.Controls {Namespace}
