﻿//---------------------------------------------------------------------------------------------
// <copyright file="HostDocumentItem.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.AssetBuildMonitor.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using RSG.Editor.Controls.Dock.ViewModel;

    /// <summary>
    /// 
    /// </summary>
    public sealed class HostDocumentItem : DocumentItem
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="HostDocumentItem"/> class.
        /// </summary>
        /// <param name="parentPane">
        /// The parent docking pane.
        /// </param>
        /// <param name="path">
        /// The full path to the document.
        /// </param>
        /// <param name="fileNode">
        /// The file node that is associated with this document.
        /// </param>
        public HostDocumentItem(DockingPane parentPane, String hostname)
            : base(parentPane, hostname)
        {
        }
        #endregion // Constructor(s)
    
        #region Methods
        /// <summary>
        /// Override to provide core save logic to the data that this document represents.
        /// </summary>
        /// <param name="stream">
        /// The io stream that data for this document should be written to.
        /// </param>
        /// <returns>
        /// A value indicating whether the save operation was successful.
        /// </returns>
        protected override bool SaveCore(System.IO.Stream stream)
        {
            return (true);
        }
        #endregion // Methods
    }

} // RSG.AssetBuildMonitor.ViewModel namespace
