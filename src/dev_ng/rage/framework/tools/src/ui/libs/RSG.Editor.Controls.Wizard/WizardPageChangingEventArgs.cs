﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Editor.Controls.Wizard
{

    /// <summary>
    /// Event argument class for WizardPageChanging event; specifies the page
    /// user is navigating from and allows the programmer to cancel the navigation.
    /// </summary>
    public class WizardPageChangingEventArgs : EventArgs
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public WizardPage FromPage
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public WizardPage ToPage
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Cancel
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="from"></param>
        public WizardPageChangingEventArgs(WizardPage from, WizardPage to)
        {
            this.FromPage = from;
            this.ToPage = to;
            this.Cancel = false;
        }
        #endregion // Constructor(s)
    }

} // RSG.Editor.Controls.Wizard namespace
