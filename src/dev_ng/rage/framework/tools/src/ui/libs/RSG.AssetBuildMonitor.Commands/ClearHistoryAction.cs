﻿//---------------------------------------------------------------------------------------------
// <copyright file="ClearHistoryAction.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.AssetBuildMonitor.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Windows;
    using RSG.AssetBuildMonitor.ViewModel;
    using RSG.AssetBuildMonitor.ViewModel.Project;
    using RSG.Editor;
    using RSG.Editor.Controls.Dock.ViewModel;

    /// <summary>
    /// Contains the logic for the <see cref="Commands.ClearHistory"/> routed
    /// command. This class cannot be inherited.
    /// </summary>
    public sealed class ClearHistoryAction : ButtonAction<CommandArgs>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="ClearHistoryAction"/> class.
        /// </summary>
        /// <param name="serviceProvider">
        /// The service provider that is used by this action to perform application specific
        /// operations, for example selecting a file to open.
        /// </param>
        public ClearHistoryAction(ICommandServiceProvider serviceProvider)
            : base(serviceProvider)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ClearHistoryAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public ClearHistoryAction(CommandResolver resolver)
            : base(new ParameterResolverDelegate<CommandArgs>(resolver))
        {
        }
        #endregion // Constructor(s)
        
        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(CommandArgs args)
        {
            return (null != args.ActiveDocument);
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(CommandArgs args)
        {
            IMessageBoxService messageService = this.GetService<IMessageBoxService>();
            if (messageService == null)
            {
                Debug.Assert(false, "Unable to open as services are missing.");
                return;
            }
            
            String title = ((RSG.Editor.Controls.RsApplication)Application.Current).UniqueName;
            AssetBuildHistoryViewModel vm = args.GetActiveDocumentViewModel();
            IMessageBox messageConfirmBox = messageService.CreateMessageBox();
            messageConfirmBox.Caption = title;
            messageConfirmBox.SetSystemIcon(MessageBoxImage.Question);
            messageConfirmBox.AddButton("Yes", 0, true, false);
            messageConfirmBox.AddButton("No", 1, false, true);
            messageConfirmBox.Text = String.Format(Resources.CommandStringTable.ClearHistoryMessageText, vm.Hostname);
            switch (messageConfirmBox.ShowMessageBox())
            {
                case 1: // No
                    return; // Exit from action.
            }

            if (!vm.ClearBuildHistory())
            {
                String message = String.Format(Resources.CommandStringTable.ClearHistoryErrorText, vm.Hostname);
                messageService.ShowStandardErrorBox(message, title);
            }
        }
        #endregion Methods
    }

} // RSG.AssetBuildMonitor.Commands namespace
