﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsAboutBox.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Diagnostics;
    using System.Globalization;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Controls;
    using RSG.Editor.Controls.Helpers;
    using RSG.Editor.Controls.Resources;

    /// <summary>
    /// Represents a control that displays the attribute information for a single assembly
    /// (.dll) file.
    /// </summary>
    public class RsAboutBox : ContentControl
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="Assembly"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty AssemblyProperty;

        /// <summary>
        /// Identifies the <see cref="BuildTime"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty BuildTimeProperty;

        /// <summary>
        /// Identifies the <see cref="ConfigurationName"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ConfigurationNameProperty;

        /// <summary>
        /// Identifies the <see cref="Copyright"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty CopyrightProperty;

        /// <summary>
        /// Identifies the <see cref="Description"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty DescriptionProperty;

        /// <summary>
        /// Identifies the <see cref="Edition"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty EditionProperty;

        /// <summary>
        /// Identifies the <see cref="HelpLink"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty HelpLinkProperty;

        /// <summary>
        /// Identifies the <see cref="ProductName"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ProductNameProperty;

        /// <summary>
        /// Identifies the <see cref="VersionNumber"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty VersionNumberProperty;

        /// <summary>
        /// The time zone code to use to retrieve the assemblies build time.
        /// </summary>
        private const string TimeZoneCode = "GMT Standard Time";

        /// <summary>
        /// Identifies the <see cref="BuildTime"/> dependency property.
        /// </summary>
        private static readonly DependencyPropertyKey _buildTimePropertyKey;

        /// <summary>
        /// Identifies the <see cref="ConfigurationName"/> dependency property.
        /// </summary>
        private static readonly DependencyPropertyKey _configurationNamePropertyKey;

        /// <summary>
        /// Identifies the <see cref="Copyright"/> dependency property.
        /// </summary>
        private static readonly DependencyPropertyKey _copyrightPropertyKey;

        /// <summary>
        /// Identifies the <see cref="Description"/> dependency property.
        /// </summary>
        private static readonly DependencyPropertyKey _descriptionPropertyKey;

        /// <summary>
        /// Identifies the <see cref="Edition"/> dependency property.
        /// </summary>
        private static readonly DependencyPropertyKey _editionPropertyKey;

        /// <summary>
        /// Identifies the <see cref="HelpLink"/> dependency property.
        /// </summary>
        private static readonly DependencyPropertyKey _helpLinkPropertyKey;

        /// <summary>
        /// Identifies the <see cref="ProductName"/> dependency property.
        /// </summary>
        private static readonly DependencyPropertyKey _productNamePropertyKey;

        /// <summary>
        /// Identifies the <see cref="VersionNumber"/> dependency property.
        /// </summary>
        private static readonly DependencyPropertyKey _versionNumberPropertyKey;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsAboutBox"/> class.
        /// </summary>
        static RsAboutBox()
        {
            _buildTimePropertyKey =
                DependencyProperty.RegisterReadOnly(
                "BuildTime",
                typeof(string),
                typeof(RsAboutBox),
                new FrameworkPropertyMetadata(AboutControlResources.DefaultBuildTime));

            _configurationNamePropertyKey =
                DependencyProperty.RegisterReadOnly(
                "ConfigurationName",
                typeof(string),
                typeof(RsAboutBox),
                new FrameworkPropertyMetadata(AboutControlResources.DefaultConfiguration));

            _copyrightPropertyKey =
                DependencyProperty.RegisterReadOnly(
                "Copyright",
                typeof(string),
                typeof(RsAboutBox),
                new FrameworkPropertyMetadata("© 2013 Rockstar Games."));

            _descriptionPropertyKey =
                DependencyProperty.RegisterReadOnly(
                "Description",
                typeof(string),
                typeof(RsAboutBox),
                new FrameworkPropertyMetadata(String.Empty));

            _editionPropertyKey =
                DependencyProperty.RegisterReadOnly(
                "Edition",
                typeof(string),
                typeof(RsAboutBox),
                new FrameworkPropertyMetadata(AboutControlResources.DefaultEdition));

            _helpLinkPropertyKey =
                DependencyProperty.RegisterReadOnly(
                "HelpLink",
                typeof(string),
                typeof(RsAboutBox),
                new FrameworkPropertyMetadata(null));

            _productNamePropertyKey =
                DependencyProperty.RegisterReadOnly(
                "ProductName",
                typeof(string),
                typeof(RsAboutBox),
                new FrameworkPropertyMetadata(AboutControlResources.DefaultProductName));

            _versionNumberPropertyKey =
                DependencyProperty.RegisterReadOnly(
                "VersionNumber",
                typeof(string),
                typeof(RsAboutBox),
                new FrameworkPropertyMetadata(AboutControlResources.DefaultVersion));

            BuildTimeProperty = _buildTimePropertyKey.DependencyProperty;
            ConfigurationNameProperty = _configurationNamePropertyKey.DependencyProperty;
            CopyrightProperty = _copyrightPropertyKey.DependencyProperty;
            DescriptionProperty = _descriptionPropertyKey.DependencyProperty;
            EditionProperty = _editionPropertyKey.DependencyProperty;
            HelpLinkProperty = _helpLinkPropertyKey.DependencyProperty;
            ProductNameProperty = _productNamePropertyKey.DependencyProperty;
            VersionNumberProperty = _versionNumberPropertyKey.DependencyProperty;

            AssemblyProperty =
                DependencyProperty.Register(
                "Assembly",
                typeof(Assembly),
                typeof(RsAboutBox),
                new FrameworkPropertyMetadata(null, OnAssemblyChanged));

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsAboutBox),
                new FrameworkPropertyMetadata(typeof(RsAboutBox)));
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the string used to display the build time of the associated assembly.
        /// </summary>
        public Assembly Assembly
        {
            get { return (Assembly)this.GetValue(AssemblyProperty); }
            private set { this.SetValue(AssemblyProperty, value); }
        }

        /// <summary>
        /// Gets the string used to display the build time of the associated assembly.
        /// </summary>
        public string BuildTime
        {
            get { return (string)this.GetValue(BuildTimeProperty); }
            private set { this.SetValue(_buildTimePropertyKey, value); }
        }

        /// <summary>
        /// Gets the string used to display the configuration of the associated assembly.
        /// </summary>
        public string ConfigurationName
        {
            get { return (string)this.GetValue(ConfigurationNameProperty); }
            private set { this.SetValue(_configurationNamePropertyKey, value); }
        }

        /// <summary>
        /// Gets the string used to display the copyright text for the associated assembly.
        /// </summary>
        public string Copyright
        {
            get { return (string)this.GetValue(CopyrightProperty); }
            private set { this.SetValue(_copyrightPropertyKey, value); }
        }

        /// <summary>
        /// Gets the string used to display the description defined in the associated assembly.
        /// </summary>
        public string Description
        {
            get { return (string)this.GetValue(DescriptionProperty); }
            private set { this.SetValue(_descriptionPropertyKey, value); }
        }

        /// <summary>
        /// Gets the string used to display the edition of the associated assembly.
        /// </summary>
        public string Edition
        {
            get { return (string)this.GetValue(EditionProperty); }
            private set { this.SetValue(_editionPropertyKey, value); }
        }

        /// <summary>
        /// Gets the string used to display the edition of the associated assembly.
        /// </summary>
        public string HelpLink
        {
            get { return (string)this.GetValue(HelpLinkProperty); }
            private set { this.SetValue(_helpLinkPropertyKey, value); }
        }

        /// <summary>
        /// Gets the string used to display the product name defined in the associated
        /// assembly.
        /// </summary>
        public string ProductName
        {
            get { return (string)this.GetValue(ProductNameProperty); }
            private set { this.SetValue(_productNamePropertyKey, value); }
        }

        /// <summary>
        /// Gets the string used to display the version number of the associated assembly.
        /// </summary>
        public string VersionNumber
        {
            get { return (string)this.GetValue(VersionNumberProperty); }
            private set { this.SetValue(_versionNumberPropertyKey, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Called whenever the <see cref="Assembly"/> dependency property changes on a
        /// instance of the <see cref="RsAboutBox"/> class.
        /// </summary>
        /// <param name="s">
        /// The <see cref="RsAboutBox"/> instance whose dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data used for this event.
        /// </param>
        private static void OnAssemblyChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            RsAboutBox aboutBox = s as RsAboutBox;
            if (aboutBox == null)
            {
                return;
            }

            Assembly value = e.NewValue as Assembly;
            if (value == null)
            {
                aboutBox.BuildTime = AboutControlResources.DefaultBuildTime;
                aboutBox.ConfigurationName = AboutControlResources.DefaultConfiguration;
                aboutBox.Copyright = "© 2013 Rockstar Games.";
                aboutBox.Description = String.Empty;
                aboutBox.Edition = AboutControlResources.DefaultEdition;
                aboutBox.ProductName = AboutControlResources.DefaultProductName;
                aboutBox.VersionNumber = AboutControlResources.DefaultVersion;
                aboutBox.HelpLink = null;
                return;
            }

            DateTime buildTime;
            try
            {
                buildTime = AssemblyInfo.GetBuildTime(value, TimeZoneCode);
            }
            catch (InvalidTimeZoneException)
            {
                Debug.Assert(false, "Unable to retrieve the GMT version of the build time.");
                buildTime = AssemblyInfo.GetUtcBuildTime(value);
            }

            CultureInfo cultureInfo = CultureInfo.CurrentCulture;
            aboutBox.BuildTime = "Build Time " + buildTime.ToString(cultureInfo) + " (GMT)";
            if (AssemblyInfo.IsAssemblyDebugBuild(value))
            {
                aboutBox.ConfigurationName = AboutControlResources.DevelopmentVersion;
            }
            else
            {
                aboutBox.ConfigurationName = AboutControlResources.ReleaseVersion;
            }

            aboutBox.Copyright = AssemblyInfo.GetCopyright(value, "© 2013 Rockstar Games.");
            aboutBox.Description = AssemblyInfo.GetDescription(value, String.Empty);
            aboutBox.Edition = AssemblyInfo.GetEdition(value);
            aboutBox.ProductName =
                AssemblyInfo.GetProductName(value, AboutControlResources.DefaultProductName);
            aboutBox.VersionNumber = AssemblyInfo.GetVersion(value).ToString();
            aboutBox.HelpLink = AssemblyInfo.GetHelpLink(value);
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsAboutBox {Class}
} // RSG.Editor.Controls {Namespace}
