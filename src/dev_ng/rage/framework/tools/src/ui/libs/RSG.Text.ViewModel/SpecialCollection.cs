﻿//---------------------------------------------------------------------------------------------
// <copyright file="SpecialCollection.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.ViewModel
{
    using RSG.Editor.Model;
    using RSG.Editor.View;
    using RSG.Text.Model;

    /// <summary>
    /// Provides a collection class that contains <see cref="DialogueSpecialViewModel"/>
    /// objects that are kept in sync with a collection of <see cref="DialogueSpecial"/>
    /// objects.
    /// </summary>
    public class SpecialCollection
        : CollectionConverter<DialogueSpecial, DialogueSpecialViewModel>,
        IReadOnlyViewModelCollection<DialogueSpecialViewModel>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SpecialCollection"/> class.
        /// </summary>
        /// <param name="source">
        /// The model collection containing the source <see cref="DialogueSpecial"/> objects
        /// for this collection.
        /// </param>
        public SpecialCollection(ModelCollection<DialogueSpecial> source)
            : base(source)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Converts a single <see cref="DialogueSpecial"/> object to a new
        /// <see cref="DialogueSpecialViewModel"/> object.
        /// </summary>
        /// <param name="item">
        /// The item to convert.
        /// </param>
        /// <returns>
        /// A new <see cref="DialogueSpecialViewModel"/> object from the given
        /// <see cref="DialogueSpecial"/> object.
        /// </returns>
        protected override DialogueSpecialViewModel ConvertItem(DialogueSpecial item)
        {
            DialogueSpecialViewModel newViewModel = new DialogueSpecialViewModel(item);
            return newViewModel;
        }
        #endregion Methods
    } // RSG.Text.ViewModel.SpecialCollection {Class}
} // RSG.Text.ViewModel {Namespace}
