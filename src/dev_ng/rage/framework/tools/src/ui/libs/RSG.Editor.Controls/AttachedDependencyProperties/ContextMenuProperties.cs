﻿//---------------------------------------------------------------------------------------------
// <copyright file="ContextMenuProperties.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.AttachedDependencyProperties
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using RSG.Editor.Controls.Commands;

    /// <summary>
    /// Contains attached properties that are related to the commands located inside context
    /// menus.
    /// </summary>
    public static class ContextMenuProperties
    {
        #region Fields
        /// <summary>
        /// Identifies the ContextCommandParameter dependency property.
        /// </summary>
        public static DependencyProperty ContextCommandParameterProperty;

        /// <summary>
        /// Identifies the ContextMenuIdentifier dependency property.
        /// </summary>
        public static DependencyProperty ContextMenuIdentifierProperty;

        /// <summary>
        /// Identifies the IsContextMenuOpen dependency property.
        /// </summary>
        public static DependencyProperty IsContextMenuOpenProperty;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="ContextMenuProperties"/> class.
        /// </summary>
        static ContextMenuProperties()
        {
            ContextCommandParameterProperty =
                DependencyProperty.RegisterAttached(
                "ContextCommandParameter",
                typeof(object),
                typeof(ContextMenuProperties),
                new FrameworkPropertyMetadata(
                    null, FrameworkPropertyMetadataOptions.Inherits));

            ContextMenuIdentifierProperty =
                DependencyProperty.RegisterAttached(
                "ContextMenuIdentifier",
                typeof(Guid),
                typeof(ContextMenuProperties),
                new FrameworkPropertyMetadata(Guid.Empty, OnContextMenuIdentifierChanged));

            IsContextMenuOpenProperty =
                DependencyProperty.RegisterAttached(
                "IsContextMenuOpen",
                typeof(bool),
                typeof(ContextMenuProperties),
                new FrameworkPropertyMetadata(
                    false, FrameworkPropertyMetadataOptions.Inherits));
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Retrieves the ContextCommandParameter dependency property value attached to the
        /// specified object.
        /// </summary>
        /// <param name="obj">
        /// The object whose attached ContextCommandParameter dependency property value will be
        /// returned.
        /// </param>
        /// <returns>
        /// The ContextCommandParameter dependency property value attached to the specified
        /// object.
        /// </returns>
        public static object GetContextCommandParameter(Control obj)
        {
            if (obj == null)
            {
                throw new SmartArgumentNullException(() => obj);
            }

            return obj.GetValue(ContextCommandParameterProperty);
        }

        /// <summary>
        /// Retrieves the ContextMenuIdentifier dependency property value attached to the
        /// specified object.
        /// </summary>
        /// <param name="obj">
        /// The object whose attached ContextMenuIdentifier dependency property value will be
        /// returned.
        /// </param>
        /// <returns>
        /// The ContextMenuIdentifier dependency property value attached to the specified
        /// object.
        /// </returns>
        public static Guid GetContextMenuIdentifier(Control obj)
        {
            if (obj == null)
            {
                throw new SmartArgumentNullException(() => obj);
            }

            return (Guid)obj.GetValue(ContextMenuIdentifierProperty);
        }

        /// <summary>
        /// Retrieves the IsContextMenuOpen dependency property value attached to the specified
        /// object.
        /// </summary>
        /// <param name="obj">
        /// The object whose attached IsContextMenuOpen dependency property value will be
        /// returned.
        /// </param>
        /// <returns>
        /// The IsContextMenuOpen dependency property value attached to the specified object.
        /// </returns>
        public static object GetIsContextMenuOpen(FrameworkElement obj)
        {
            if (obj == null)
            {
                throw new SmartArgumentNullException(() => obj);
            }

            return obj.GetValue(IsContextMenuOpenProperty);
        }

        /// <summary>
        /// Sets the ContextCommandParameter dependency property value on the specified object
        /// to the specified value.
        /// </summary>
        /// <param name="obj">
        /// The object whose attached ContextCommandParameter dependency property value will be
        /// set.
        /// </param>
        /// <param name="value">
        /// The value to set the attached ContextCommandParameter dependency property to on the
        /// specified object.
        /// </param>
        public static void SetContextCommandParameter(Control obj, object value)
        {
            if (obj == null)
            {
                throw new SmartArgumentNullException(() => obj);
            }

            obj.SetValue(ContextCommandParameterProperty, value);
        }

        /// <summary>
        /// Sets the ContextMenuIdentifier dependency property value on the specified object
        /// to the specified value.
        /// </summary>
        /// <param name="obj">
        /// The object whose attached ContextMenuIdentifier dependency property value will be
        /// set.
        /// </param>
        /// <param name="value">
        /// The value to set the attached ContextMenuIdentifier dependency property to on the
        /// specified object.
        /// </param>
        public static void SetContextMenuIdentifier(Control obj, Guid value)
        {
            if (obj == null)
            {
                throw new SmartArgumentNullException(() => obj);
            }

            obj.SetValue(ContextMenuIdentifierProperty, value);
        }

        /// <summary>
        /// Sets the IsContextMenuOpen dependency property value on the specified object to the
        /// specified value.
        /// </summary>
        /// <param name="obj">
        /// The object whose attached IsContextMenuOpen dependency property value will be set.
        /// </param>
        /// <param name="value">
        /// The value to set the attached IsContextMenuOpen dependency property to on the
        /// specified object.
        /// </param>
        public static void SetIsContextMenuOpen(FrameworkElement obj, bool value)
        {
            if (obj == null)
            {
                throw new SmartArgumentNullException(() => obj);
            }

            obj.SetValue(IsContextMenuOpenProperty, value);
        }

        /// <summary>
        /// Called whenever the ContextMenuIdentifier dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The object whose ContextMenuIdentifier dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnContextMenuIdentifierChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            Control control = s as Control;
            if (control == null)
            {
                return;
            }

            if ((Guid)e.NewValue != Guid.Empty)
            {
                control.ContextMenu = new RsContextMenu();
                control.ContextMenuOpening += OnContextMenuOpening;
            }
            else
            {
                control.ContextMenuOpening -= OnContextMenuOpening;
            }
        }

        /// <summary>
        /// Called whenever the context menu is about to be opened on the attached object.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Controls.ContextMenuEventArgs data for this event.
        /// </param>
        private static void OnContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            Control control = sender as Control;
            if (control == null)
            {
                return;
            }

            RsContextMenu menu = new RsContextMenu();
            Guid id = GetContextMenuIdentifier(control);
            if (id != Guid.Empty)
            {
                menu.ItemsSource = CommandBarItemViewModel.Create(id);
            }

            if (menu.Items.Count == 0)
            {
                control.ContextMenu = null;
            }
            else
            {
                control.ContextMenu = menu;
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.AttachedDependencyProperties.ContextMenuProperties {Class}
} // RSG.Editor.Controls.AttachedDependencyProperties {Namespace}
