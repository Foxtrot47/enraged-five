﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsCustomiseWindow.xaml.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Customisation
{
    using System.Windows.Input;

    /// <summary>
    /// Interaction logic for the command customisation window.
    /// </summary>
    internal partial class RsCustomiseWindow : RsWindow
    {
        #region Fields
        /// <summary>
        /// The private reference to the data context of this window.
        /// </summary>
        private RsCustomiseWindowDataContext _dataContext;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="RsCustomiseWindow"/> class.
        /// </summary>
        public RsCustomiseWindow()
        {
            this.InitializeComponent();

            this._dataContext = new RsCustomiseWindowDataContext();
            this.DataContext = this._dataContext;

            this.CommandBindings.Add(
                new CommandBinding(
                    CustomViewCommands.ResetAll,
                    this.OnResetAllExecuted,
                    CanResetAll));
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the <see cref="CustomViewCommands.ResetAll"/> command can be
        /// executed based on the current state of the application.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The can execute data that has been sent with the command.
        /// </param>
        private static void CanResetAll(object s, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = RockstarCommandManager.HasModifications;
        }

        /// <summary>
        /// Called whenever the <see cref="CustomViewCommands.ResetAll"/> command is fired and
        /// needs handling at this instance level.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The execute data that has been sent with the command.
        /// </param>
        private void OnResetAllExecuted(object s, ExecutedRoutedEventArgs e)
        {
            RockstarCommandManager.ClearModifications(true);
            this._dataContext.ResetItems();
        }
        #endregion Methods
    } // RSG.Editor.Controls.Customisation.RsCustomiseWindow {Class}
} // RSG.Editor.Controls.Customisation {Namespace}
