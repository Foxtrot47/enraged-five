﻿//---------------------------------------------------------------------------------------------
// <copyright file="DynamicValueSolver{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.ComponentModel;
    using System.Reflection;
    using RSG.Base.Extensions;

    /// <summary>
    /// Represents a value solver that can forward changes from one source to an additional
    /// listener.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the target value.
    /// </typeparam>
    public class DynamicValueSolver<T> where T : class
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="CurrentValue"/> property.
        /// </summary>
        private T _currentValue;

        /// <summary>
        /// The private field used for the <see cref="FallbackValue"/> property.
        /// </summary>
        private T _fallbackValue;

        /// <summary>
        /// The private field used for the <see cref="StringFormat"/> property.
        /// </summary>
        private string _stringFormat;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DynamicValueSolver{T}"/> class.
        /// </summary>
        /// <param name="source">
        /// The source object that original raises the property changed event.
        /// </param>
        /// <param name="propertyName">
        /// The name of the property on the source object that should be listened to.
        /// </param>
        public DynamicValueSolver(INotifyPropertyChanged source, string propertyName)
        {
            this.UpdateCurrentValue(source, propertyName);
            PropertyChangedEventManager.AddHandler(
                source, this.OnPropertyChanged, propertyName);
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs when the value this class is handling changes.
        /// </summary>
        public event EventHandler ValueChanged;
        #endregion Events

        #region Properties
        /// <summary>
        /// Gets the currently solved value.
        /// </summary>
        public T CurrentValue
        {
            get { return this._currentValue; }
        }

        /// <summary>
        /// Gets or sets the fall back value to use if the current value is null.
        /// </summary>
        public T FallbackValue
        {
            get { return this._fallbackValue; }
            set { this._fallbackValue = value; }
        }

        /// <summary>
        /// Gets or sets the format string used to convert the data to type String.
        /// </summary>
        /// <remarks>
        /// This property is object used when the target type is a String.
        /// </remarks>
        public string StringFormat
        {
            get { return this._stringFormat; }
            set { this._stringFormat = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Occurs when the listened to property has changed.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.ComponentModel.PropertyChangedEventArgs containing the event data.
        /// </param>
        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            EventHandler handler = this.ValueChanged;
            if (handler == null)
            {
                return;
            }

            this.UpdateCurrentValue(sender, e.PropertyName);
            handler(this, EventArgs.Empty);
        }

        /// <summary>
        /// Updates the currently value by reflecting into the specified source object and
        /// looking for the value of the property with the specified name.
        /// </summary>
        /// <param name="source">
        /// The source for the value.
        /// </param>
        /// <param name="propertyName">
        /// The name of the property the value should be retrieved from.
        /// </param>
        private void UpdateCurrentValue(object source, string propertyName)
        {
            PropertyInfo propertyInfo = source.GetType().GetProperty(propertyName);
            if (propertyInfo != null)
            {
                T propertyValue = propertyInfo.GetValue(source) as T;
                if (propertyValue == null)
                {
                    propertyValue = this.FallbackValue;
                }

                if (propertyValue != null && typeof(T) == typeof(string))
                {
                    if (this.StringFormat != null)
                    {
                        propertyValue = this.StringFormat.FormatCurrent(propertyValue) as T;
                    }
                }

                this._currentValue = propertyValue;
            }
        }
        #endregion Methods
    } // RSG.Editor.DynamicValueSolver{T} {Class}
} // RSG.Editor {Namespace}
