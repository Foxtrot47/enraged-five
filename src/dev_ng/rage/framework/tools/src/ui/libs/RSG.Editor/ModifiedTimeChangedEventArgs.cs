﻿//---------------------------------------------------------------------------------------------
// <copyright file="ModifiedTimeChangedEventArgs.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;

    /// <summary>
    /// Provides data for a event that is representing a last write time change to a file.
    /// </summary>
    public class ModifiedTimeChangedEventArgs : EventArgs
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="FullPath"/> property.
        /// </summary>
        private string _fullPath;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ModifiedTimeChangedEventArgs"/> class.
        /// </summary>
        /// <param name="fullpath">
        /// The full path to the file whose read-only state has changed.
        /// </param>
        public ModifiedTimeChangedEventArgs(string fullpath)
        {
            this._fullPath = fullpath;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the full path to the file whose read-only state has changed.
        /// </summary>
        public string FullPath
        {
            get { return this._fullPath; }
        }
        #endregion Properties
    } // RSG.Editor.ModifiedTimeChangedEventArgs {Class}
} // RSG.Editor {Namespace}
