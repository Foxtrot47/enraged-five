﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsSplitterItemsControl.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Represents a control that can be used to present a collection of items in it with
    /// each item being a <see cref="RsSplitterItem"/> with a resize grip between them.
    /// </summary>
    public class RsSplitterItemsControl : ItemsControl
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="Orientation"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty OrientationProperty;

        /// <summary>
        /// Identifies the SplitterGripSize attached dependency property.
        /// </summary>
        public static readonly DependencyProperty SplitterGripSizeProperty;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsSplitterItemsControl"/> class.
        /// </summary>
        static RsSplitterItemsControl()
        {
            OrientationProperty =
                DependencyProperty.Register(
                "Orientation",
                typeof(Orientation),
                typeof(RsSplitterItemsControl),
                new FrameworkPropertyMetadata(
                    Orientation.Vertical,
                    FrameworkPropertyMetadataOptions.AffectsMeasure,
                    new PropertyChangedCallback(OnOrientationChanged)));

            SplitterGripSizeProperty =
                DependencyProperty.RegisterAttached(
                "SplitterGripSize",
                typeof(double),
                typeof(RsSplitterItemsControl),
                new FrameworkPropertyMetadata(6.0, FrameworkPropertyMetadataOptions.Inherits));

            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsSplitterItemsControl),
                new FrameworkPropertyMetadata(typeof(RsSplitterItemsControl)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsSplitterItemsControl"/> class.
        /// </summary>
        public RsSplitterItemsControl()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the orientation that the child elements for this control are
        /// arranged in.
        /// </summary>
        public Orientation Orientation
        {
            get { return (Orientation)this.GetValue(OrientationProperty); }
            set { this.SetValue(OrientationProperty, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Gets the SplitterGripSize attached dependency property value for the
        /// specified element.
        /// </summary>
        /// <param name="element">
        /// The element to get the SplitterGripSize attached dependency property
        /// value for.
        /// </param>
        /// <returns>
        /// The value of the SplitterGripSize attached dependency property on the
        /// specified element.
        /// </returns>
        /// <exception cref="RSG.Editor.SmartArgumentNullException">
        /// Thrown if the specified <paramref name="element"/> is null.
        /// </exception>
        public static double GetSplitterGripSize(DependencyObject element)
        {
            if (element == null)
            {
                throw new SmartArgumentNullException(() => element);
            }

            return (double)element.GetValue(SplitterGripSizeProperty);
        }

        /// <summary>
        /// Sets the SplitterGripSize attached dependency property on the specified
        /// element to the specified value.
        /// </summary>
        /// <param name="element">
        /// The element to set the SplitterGripSize attached dependency property on.
        /// </param>
        /// <param name="value">
        /// The value to set the SplitterGripSize attached dependency property to on the
        /// specified element.
        /// </param>
        /// <exception cref="RSG.Editor.SmartArgumentNullException">
        /// Thrown if the specified <paramref name="element"/> is null.
        /// </exception>
        public static void SetSplitterGripSize(DependencyObject element, double value)
        {
            if (element == null)
            {
                throw new SmartArgumentNullException(() => element);
            }

            element.SetValue(SplitterGripSizeProperty, value);
        }

        /// <summary>
        /// Creates or identifies the element that is used to display the given item.
        /// </summary>
        /// <returns>
        /// The element that is used to display the given item.
        /// </returns>
        protected override DependencyObject GetContainerForItemOverride()
        {
            return new RsSplitterItem();
        }

        /// <summary>
        /// Determines if the specified item is (or is eligible to be) its own container.
        /// </summary>
        /// <param name="item">
        /// The item to check.
        /// </param>
        /// <returns>
        /// True if the item is (or is eligible to be) its own container; otherwise, false.
        /// </returns>
        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is RsSplitterItem;
        }

        /// <summary>
        /// Gives derived types a chance to handle the situation where the orientation changes.
        /// </summary>
        protected virtual void OnOrientationChanged()
        {
        }

        /// <summary>
        /// Gets called whenever the <see cref="Orientation"/> dependency property for the
        /// specified dependency object changes.
        /// </summary>
        /// <param name="sender">
        /// The dependency object whose <see cref="Orientation"/> dependency property changed.
        /// </param>
        /// <param name="args">
        /// The System.Windows.DependencyPropertyChangedEventArgs data for the event.
        /// </param>
        private static void OnOrientationChanged(
            DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            RsSplitterItemsControl splitterItemsControl = sender as RsSplitterItemsControl;
            if (splitterItemsControl == null)
            {
                return;
            }

            splitterItemsControl.OnOrientationChanged();
        }
        #endregion Methods
    } // RSG.Editor.Controls.Dock.RsSplitterItemsControl {Class}
} // RSG.Editor.Controls.Dock {Namespace}
