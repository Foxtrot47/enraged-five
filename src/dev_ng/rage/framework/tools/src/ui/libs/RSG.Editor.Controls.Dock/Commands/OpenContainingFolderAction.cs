﻿//---------------------------------------------------------------------------------------------
// <copyright file="OpenContainingFolderAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock.Commands
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Windows;
    using RSG.Editor.Controls.Dock.ViewModel;

    /// <summary>
    /// The action logic that is responsible for executing the open containing folder command
    /// on the active document.
    /// </summary>
    public class OpenContainingFolderAction : ButtonAction<DocumentItem>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="OpenContainingFolderAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public OpenContainingFolderAction(
            ParameterResolverDelegate<DocumentItem> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(DocumentItem parameter)
        {
            if (parameter == null)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(DocumentItem parameter)
        {
            string filePath = parameter.FullPath;
            FileInfo info = new FileInfo(filePath);
            if (!info.Exists)
            {
                return;
            }

            string argument = String.Format("/select, \"{0}\"", filePath);
            Process.Start("explorer.exe", argument);
        }
        #endregion Methods
    } // RSG.Editor.Controls.Dock.Commands.OpenContainingFolderAction {Class}
} // RSG.Editor.Controls.Dock.Commands {Namespace}
