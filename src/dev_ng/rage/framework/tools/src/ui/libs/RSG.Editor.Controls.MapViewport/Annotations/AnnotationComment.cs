﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Xml.Linq;
using RSG.Editor.Model;

namespace RSG.Editor.Controls.MapViewport.Annotations
{
    /// <summary>
    /// Represents a single comment that makes up part of an annotation.
    /// </summary>
    public class AnnotationComment : UndoablePropertyChangedBase
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="AcceptEditCommand"/> property.
        /// </summary>
        private readonly ICommand _acceptEditCommand;

        /// <summary>
        /// Private field for the <see cref="BeginEditCommand"/> property.
        /// </summary>
        private readonly ICommand _beginEditCommand;

        /// <summary>
        /// Private field for the <see cref="CancelEditCommand"/> property.
        /// </summary>
        private readonly ICommand _cancelEditCommand;

        /// <summary>
        /// Private field for the <see cref="CreatedTimestamp"/> property.
        /// </summary>
        private readonly DateTime _createdTimestamp;

        /// <summary>
        /// Private field for the <see cref="IsEditing"/> property.
        /// </summary>
        private bool _isEditing;

        /// <summary>
        /// Private field for the <see cref="Message"/> property.
        /// </summary>
        private string _message;

        /// <summary>
        /// Private field for the <see cref="OriginalMessage"/> property.
        /// </summary>
        private string _originalMessage;

        /// <summary>
        /// Private field for the <see cref="ModifiedTimestamp"/> property.
        /// </summary>
        private DateTime? _modifiedTimestamp;

        /// <summary>
        /// Private field for the <see cref="Username"/> property.
        /// </summary>
        private readonly string _username;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="AnnotationComment"/> class
        /// using the specified message.
        /// </summary>
        /// <param name="message"></param>
        public AnnotationComment(string message, IUndoEngineProvider undoEngineProvider)
            : base(undoEngineProvider)
        {
            _acceptEditCommand = new RelayCommand(OnAcceptEdit, CanAcceptEdit);
            _beginEditCommand = new RelayCommand(OnBeginEdit);
            _cancelEditCommand = new RelayCommand(OnCancelEdit);
            _createdTimestamp = DateTime.UtcNow;
            _isEditing = false;
            _message = message;
            _originalMessage = message;
            _username = Environment.UserName;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="AnnotationComment"/> class
        /// using the specified <see cref="XElement"/>.
        /// </summary>
        /// <param name="elem"></param>
        /// <param name="undoEngineProvider"></param>
        public AnnotationComment(XElement elem, IUndoEngineProvider undoEngineProvider)
            : base(undoEngineProvider)
        {
            XElement createdElem = elem.Element("CreatedTimestamp");
            XElement messageElem = elem.Element("Message");
            XElement modifiedElem = elem.Element("ModifiedTimestamp");
            XElement usernameElem = elem.Element("Username");

            long createdTicks = Int64.Parse(createdElem.Value);
            _createdTimestamp = new DateTime(createdTicks, DateTimeKind.Utc);

            if (modifiedElem != null && !String.IsNullOrEmpty(modifiedElem.Value))
            {
                long modifiedTicks = Int64.Parse(modifiedElem.Value);
                _modifiedTimestamp = new DateTime(modifiedTicks, DateTimeKind.Utc);
            }

            _acceptEditCommand = new RelayCommand(OnAcceptEdit, CanAcceptEdit);
            _beginEditCommand = new RelayCommand(OnBeginEdit);
            _cancelEditCommand = new RelayCommand(OnCancelEdit);
            _isEditing = false;
            _message = messageElem.Value;
            _originalMessage = _message;
            _username = usernameElem.Value;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the command for accepting an edit.
        /// </summary>
        public ICommand AcceptEditCommand
        {
            get { return _acceptEditCommand; }
        }

        /// <summary>
        /// Gets the command for entering into edit mode.
        /// </summary>
        public ICommand BeginEditCommand
        {
            get { return _beginEditCommand; }
        }

        /// <summary>
        /// Gets the command for cancelling an edit.
        /// </summary>
        public ICommand CancelEditCommand
        {
            get { return _cancelEditCommand; }
        }

        /// <summary>
        /// Datetime when this comment was originally added.
        /// </summary>
        public DateTime CreatedTimestamp
        {
            get { return _createdTimestamp.ToLocalTime(); }
        }

        /// <summary>
        /// Gets a value indicating whether this comment can be edited.
        /// </summary>
        public bool Editable
        {
            get { return _username == Environment.UserName; }
        }

        /// <summary>
        /// Gets a value indicating whether this comment has been edited.
        /// </summary>
        public bool HasBeenEdited
        {
            get { return _modifiedTimestamp.HasValue; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the comment is currently in edit mode.
        /// </summary>
        public bool IsEditing
        {
            get { return _isEditing; }
            set { SetProperty(ref _isEditing, value); }
        }

        /// <summary>
        /// The actual comment.
        /// </summary>
        public string Message
        {
            get { return _message; }
            set { SetProperty(ref _message, value); }
        }

        /// <summary>
        /// Datetime when this comment was last edited.
        /// </summary>
        public DateTime? ModifiedTimestamp
        {
            get { return (_modifiedTimestamp.HasValue ? _modifiedTimestamp.Value.ToLocalTime() : (DateTime?)null); }
            set { SetUndoableProperty(ref _modifiedTimestamp, value, "ModifiedTimestamp", "HasBeenEdited"); }
        }

        /// <summary>
        /// The original message before it was edited.
        /// </summary>
        public string OriginalMessage
        {
            get { return _originalMessage; }
            set
            {
                bool performingEvent = UndoEngine.PerformingEvent;
                using (new UndoRedoBatch(UndoEngine))
                {
                    if (SetUndoableProperty(ref _originalMessage, value))
                    {
                        Message = OriginalMessage;
                        if (!performingEvent)
                        {
                            ModifiedTimestamp = DateTime.UtcNow;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// User that added this comment.
        /// </summary>
        public string Username
        {
            get { return _username; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Converts the annotation comment to an XElement.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public XElement ToXElement(string name)
        {
            return new XElement(name,
                new XElement("CreatedTimestamp", _createdTimestamp.Ticks),
                new XElement("Message", _originalMessage),
                new XElement("ModifiedTimestamp", _modifiedTimestamp.HasValue ? (long?)_modifiedTimestamp.Value.Ticks : null),
                new XElement("Username", _username)
                );
        }

        /// <summary>
        /// Called to determine whether the accept edit command logic can be run.
        /// </summary>
        /// <param name="commandParameter"></param>
        /// <returns></returns>
        private bool CanAcceptEdit(object commandParameter)
        {
            return Message != OriginalMessage;
        }

        /// <summary>
        /// Executed when the accept edit command is invoked.
        /// </summary>
        /// <param name="commandParameter"></param>
        private void OnAcceptEdit(object commandParameter)
        {
            OriginalMessage = Message;
            IsEditing = false;
        }

        /// <summary>
        /// Executed when the begin edit command is invoked.
        /// </summary>
        /// <param name="commandParameter"></param>
        private void OnBeginEdit(object commandParameter)
        {
            IsEditing = true;
        }

        /// <summary>
        /// Executed when the cancel edit command is invoked.
        /// </summary>
        /// <param name="commandParameter"></param>
        private void OnCancelEdit(object commandParameter)
        {
            Message = OriginalMessage;
            IsEditing = false;
        }
        #endregion
    }
}
