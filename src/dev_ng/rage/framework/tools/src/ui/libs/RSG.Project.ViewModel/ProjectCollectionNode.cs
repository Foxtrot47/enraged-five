﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProjectCollectionNode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Xml;
    using RSG.Base.Extensions;
    using RSG.Editor;
    using RSG.Editor.Controls.Perforce;
    using RSG.Editor.Model;
    using RSG.Editor.View;
    using RSG.Project.Model;
    using RSG.Project.ViewModel.Definitions;

    /// <summary>
    /// Contains the root of a project collection hierarchy that supports nested projects.
    /// </summary>
    public class ProjectCollectionNode :
        ViewModelBase<ProjectCollection>,
        ITreeDisplayItem,
        IHierarchyNode,
        ISaveableDocument
    {
        #region Fields
        /// <summary>
        /// The private reference to the collection definition associated with this node.
        /// </summary>
        private ProjectCollectionDefinition _definition;

        /// <summary>
        /// The private field used for the <see cref="IsExpandable"/> property.
        /// </summary>
        private bool _isExpandable;

        /// <summary>
        /// The private field used for the <see cref="isReadOnly"/> property.
        /// </summary>
        private bool _isReadOnly;

        /// <summary>
        /// The private field used for the <see cref="Items"/> property.
        /// </summary>
        private ObservableCollection<IHierarchyNode> _items;

        /// <summary>
        /// The number of project objects currently loaded.
        /// </summary>
        private int _projectCount;

        /// <summary>
        /// The private field used for the <see cref="OverlayIcon"/> property.
        /// </summary>
        private BitmapSource _overlayIcon;

        /// <summary>
        /// The private field used for the <see cref="StateIcon"/> property.
        /// </summary>
        private BitmapSource _stateIcon;

        /// <summary>
        /// The private field used for the <see cref="PerforceService"/> property.
        /// </summary>
        private IPerforceService _perforceService;

        /// <summary>
        /// The private field used for the <see cref="IsModified"/> property.
        /// </summary>
        private bool _isModified;

        /// <summary>
        /// The private field used for the <see cref="Loaded"/> property.
        /// </summary>
        private bool _loaded;

        /// <summary>
        /// The private field used for the <see cref="IsTemporaryFile"/> property.
        /// </summary>
        private bool _isTemporaryFile;

        /// <summary>
        /// The private field used for the <see cref="SelectedItems"/> property.
        /// </summary>
        private List<IHierarchyNode> _selectedItems;

        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private string _name;

        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, string> _documentStates;

        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, int> _previouslyOpenedDocuments;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ProjectCollectionNode"/> class.
        /// </summary>
        public ProjectCollectionNode(ProjectCollectionDefinition definition)
            : base(new ProjectCollection())
        {
            this._documentStates = new Dictionary<string, string>();
            this._previouslyOpenedDocuments = new Dictionary<string, int>();
            this._definition = definition;
            this._items = new ObservableCollection<IHierarchyNode>();
            this._projectCount = 0;
            this.Icon = ProjectIcons.GenericProjectCollection;
            this._isExpandable = true;
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs whenever the loaded state for this collection is changed.
        /// </summary>
        public event EventHandler<ValueChangedEventArgs<bool>> LoadedStateChanged;

        /// <summary>
        /// Occurs whenever a project within this collection is loaded.
        /// </summary>
        public event EventHandler<ProjectLoadedStateChangedEventArgs> ProjectLoaded;

        /// <summary>
        /// Occurs whenever a project within this collection is unloaded or removed.
        /// </summary>
        public event EventHandler<ProjectLoadedStateChangedEventArgs> ProjectUnloaded;

        /// <summary>
        /// Occurs whenever a project within this collection has failed to load.
        /// </summary>
        public event EventHandler<ProjectLoadFailedEventArgs> ProjectFailed;

        /// <summary>
        /// Occurs whenever a project within this collection is saved.
        /// </summary>
        public event EventHandler<ProjectLoadedStateChangedEventArgs> ProjectSaved;

        /// <summary>
        /// Occurs whenever this project collection is loaded.
        /// </summary>
        public event EventHandler<EventArgs> ProjectCollectionLoaded;

        /// <summary>
        /// Occurs whenever this project collection is saved.
        /// </summary>
        public event EventHandler<EventArgs> ProjectCollectionSaved;

        /// <summary>
        /// Occurs just after this object has been saved successfully.
        /// </summary>
        public event EventHandler Saved;
        #endregion Events

        #region Properties
        /// <summary>
        /// Gets a value indicating whether this hierarchy node can be removed from the parent
        /// items scope it belongs to.
        /// </summary>
        public bool CanBeRemoved
        {
            get { return false; }
        }

        /// <summary>
        /// Gets the bitmap source that is used to display the state icon for this item.
        /// </summary>
        public BitmapSource OverlayIcon
        {
            get { return this._overlayIcon; }
            protected set { this.SetProperty(ref this._overlayIcon, value); }
        }

        /// <summary>
        /// Gets the bitmap source that is used to display the state icon for this item.
        /// </summary>
        public BitmapSource StateIcon
        {
            get { return this._stateIcon; }
            protected set { this.SetProperty(ref this._stateIcon, value); }
        }

        /// <summary>
        /// Gets the text that is used to represent the state of this item.
        /// </summary>
        public virtual string StateToolTipText
        {
            get { return null; }
        }

        /// <summary>
        /// Gets a value indicating whether a new or existing collection folder can be added to
        /// this hierarchy node.
        /// </summary>
        public bool CanHaveCollectionFoldersAdded
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether a new or existing project folder can be added to
        /// this hierarchy node.
        /// </summary>
        public bool CanHaveProjectFoldersAdded
        {
            get { return false; }
        }

        /// <summary>
        /// Gets the definition that was used to create this project collection node.
        /// </summary>
        public ProjectCollectionDefinition Definition
        {
            get { return this._definition; }
        }

        /// <summary>
        /// Gets or sets the filename to use to display this document.
        /// </summary>
        public string Filename
        {
            get { return this.Text; }
            set { this.Text = value; }
        }

        /// <summary>
        /// Gets or sets the full file path to the location of this file.
        /// </summary>
        public string FullPath
        {
            get
            {
                if (this.Model != null)
                {
                    return this.Model.FullPath;
                }

                return String.Empty;
            }

            set
            {
                if (string.Equals(this.Model.FullPath, value))
                {
                    return;
                }

                this.Model.FullPath = value;
                this.UpdateText();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this node can be expanded.
        /// </summary>
        public bool IsExpandable
        {
            get { return this._isExpandable; }
            set { this.SetProperty(ref this._isExpandable, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this document is currently locked inside
        /// the windows explorer.
        /// </summary>
        public bool IsReadOnly
        {
            get { return this._isReadOnly; }
            set { this.SetProperty(ref this._isReadOnly, value); }
        }

        /// <summary>
        /// Gets a value indicating whether this document represents a temporary document. A
        /// temporary file cannot be saved to the same location and needs to be "Saved As...".
        /// </summary>
        public bool IsTemporaryFile
        {
            get
            {
                if (this._isTemporaryFile || this.FullPath.StartsWith(Path.GetTempPath()))
                {
                    return true;
                }

                return false;
            }

            set
            {
                this._isTemporaryFile = value;
            }
        }

        /// <summary>
        /// Gets or sets the perforce service this project collection can use to setup source
        /// control states for the physical files in it.
        /// </summary>
        public IPerforceService PerforceService
        {
            get { return this._perforceService; }
            set { this._perforceService = value; }
        }

        /// <summary>
        /// Gets the previously opened documents for this collection in the opened index order.
        /// </summary>
        public IEnumerable<string> PreviouslyOpenedDocuments
        {
            get
            {
                return from kvp in _previouslyOpenedDocuments
                       orderby kvp.Value
                       select kvp.Key;
            }
        }

        /// <summary>
        /// Gets the undo engine associated with this document.
        /// </summary>
        public UndoEngine UndoEngine
        {
            get { return null; }
        }

        /// <summary>
        /// Gets a value indicating whether this hierarchy nodes data can be saved.
        /// </summary>
        public bool CanBeSaved
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether a new or existing project can be added to this
        /// hierarchy node.
        /// </summary>
        public bool CanHaveProjectsAdded
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether a new or existing items can be added to this
        /// hierarchy node.
        /// </summary>
        public bool CanHaveItemsAdded
        {
            get { return false; }
        }

        /// <summary>
        /// Gets the friendly save path that this item pushes into the command system to
        /// display on menu items.
        /// </summary>
        public string FriendlySavePath
        {
            get
            {
                if (this.Model.FullPath == null)
                {
                    return this.Name + this.Definition.Extension;
                }

                return Path.GetFileName(this.Model.FullPath);
            }
        }

        /// <summary>
        /// Gets a value indicating whether a new or existing folder can be added to this
        /// hierarchy node.
        /// </summary>
        public bool CanHaveFoldersAdded
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether this item is expanded when the user double clicks
        /// on it.
        /// </summary>
        public bool ExpandOnDoubleClick
        {
            get { return true; }
        }

        /// <summary>
        /// Gets the include string for the project item this hierarchy node 
        /// </summary>
        public string Include
        {
            get { return null; }
        }

        /// <summary>
        /// Gets a value indicating whether this node is the only node that is currently
        /// selected.
        /// </summary>
        public bool IsSelected
        {
            get
            {
                if (this._selectedItems == null || this._selectedItems.Count != 1)
                {
                    return false;
                }

                return Object.ReferenceEquals(this._selectedItems[0], this);
            }
        }

        /// <summary>
        /// Gets a value indicating whether this node is representing a virtual project item or
        /// a project item that is located on the users disk drive.
        /// </summary>
        public bool IsVirtual
        {
            get { return false; }
        }

        /// <summary>
        /// Gets a value indicating whether this document item currently contains modified
        /// unsaved data.
        /// </summary>
        public bool IsModified
        {
            get { return this._isModified; }
            set { this.SetProperty(ref this._isModified, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this project collection is currently
        /// loaded.
        /// </summary>
        public bool Loaded
        {
            get { return this._loaded; }
            set
            {
                if (!this.SetProperty(ref this._loaded, value))
                {
                    return;
                }

                EventHandler<ValueChangedEventArgs<bool>> handler = this.LoadedStateChanged;
                if (handler != null)
                {
                    handler(this, new ValueChangedEventArgs<bool>(!value, value));
                }
            }
        }

        /// <summary>
        /// Gets the collection of items belonging to this node.
        /// </summary>
        public ObservableCollection<IHierarchyNode> Items
        {
            get { return this._items; }
        }

        /// <summary>
        /// Gets or sets the currently selected items in this project collection.
        /// </summary>
        public IEnumerable<IHierarchyNode> SelectedItems
        {
            get { return this._selectedItems; } 
            set { this._selectedItems = new List<IHierarchyNode>(value); }
        }

        /// <summary>
        /// Gets or sets the currently selected items in this project collection that do not
        /// have parents that are also selected.
        /// </summary>
        public IEnumerable<IHierarchyNode> RootSelectedItems
        {
            get
            {
                if (this.SelectedItems == null)
                {
                    return null;
                }

                List<IHierarchyNode> rootSet = new List<IHierarchyNode>(this.SelectedItems);
                foreach (IHierarchyNode node in this.SelectedItems)
                {
                    if (node == null)
                    {
                        rootSet.Remove(node);
                        continue;
                    }

                    foreach (IHierarchyNode child in node.GetChildren(true))
                    {
                        rootSet.Remove(child);
                    }
                }

                return rootSet;
            }
        }

        /// <summary>
        /// Gets the active project node for this collection by looking into the currently
        /// selected nodes.
        /// </summary>
        public ProjectNode ActiveProjectNode
        {
            get
            {
                if (this._selectedItems == null || this._selectedItems.Count == 0)
                {
                    return null;
                }

                if (this._selectedItems.Count == 1)
                {
                    ProjectNode project = this._selectedItems[0] as ProjectNode;
                    if (project != null)
                    {
                        return project;
                    }

                    return this._selectedItems[0].ParentProject;
                }
                else
                {
                    ProjectNode project = this._selectedItems[0].ParentProject;
                    foreach (IHierarchyNode node in this._selectedItems.Skip(1))
                    {
                        ProjectNode parentProject = node.ParentProject;
                        if (!Object.ReferenceEquals(parentProject, project))
                        {
                            return null;
                        }
                    }

                    return project;
                }
            }
        }

        /// <summary>
        /// Gets the active project nodes for this collection by looking into the currently
        /// selected nodes.
        /// </summary>
        public IEnumerable<ProjectNode> ActiveProjectNodes
        {
            get
            {
                if (this._selectedItems == null || this._selectedItems.Count == 0)
                {
                    return Enumerable.Empty<ProjectNode>();
                }

                List<ProjectNode> activeProjects = new List<ProjectNode>();
                foreach (IHierarchyNode node in this._selectedItems)
                {
                    ProjectNode project = node as ProjectNode;
                    if (project != null)
                    {
                        activeProjects.Add(project);
                    }
                    else
                    {
                        project = node.ParentProject;
                    }

                    if (project == null || activeProjects.Contains(project))
                    {
                        continue;
                    }

                    activeProjects.Add(project);
                }

                return activeProjects;
            }
        }

        /// <summary>
        /// Gets the currently slected project node in this collection if one is selected;
        /// otherwise, null.
        /// </summary>
        public ProjectNode SelectedProjectNode
        {
            get
            {
                if (this._selectedItems == null || this._selectedItems.Count != 1)
                {
                    return null;
                }

                ProjectNode project = this._selectedItems[0] as ProjectNode;
                if (project != null)
                {
                    return project;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the currently slected folder node in this collection if one is selected;
        /// otherwise, null.
        /// </summary>
        public FolderNode SelectedFolderNode
        {
            get
            {
                if (this._selectedItems == null || this._selectedItems.Count != 1)
                {
                    return null;
                }

                FolderNode folder = this._selectedItems[0] as FolderNode;
                if (folder != null)
                {
                    return folder;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the currently selected node in this collection if only one is selected;
        /// otherwise, null.
        /// </summary>
        public IHierarchyNode SelectedNode
        {
            get
            {
                if (this._selectedItems == null || this._selectedItems.Count != 1)
                {
                    return null;
                }

                return this._selectedItems[0];
            }
        }

        /// <summary>
        /// Gets the parent node for this node.
        /// </summary>
        public IHierarchyNode Parent
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the parent collection node for this node.
        /// </summary>
        public ProjectCollectionNode ParentCollection
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the first project node above this node if found; otherwise, null.
        /// </summary>
        public ProjectNode ParentProject
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the project item that this hierarachy node is representing.
        /// </summary>
        public ProjectItem ProjectItem
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the project scope for the item that this node is representing.
        /// </summary>
        public IProjectItemScope ProjectItemScope
        {
            get { return this.Model as IProjectItemScope; }
        }

        /// <summary>
        /// Gets the parent node who is currently representing this nodes scope item.
        /// </summary>
        public IHierarchyNode ModifiedScopeNode
        {
            get { return null; }
        }

        /// <summary>
        /// Gets or sets the name that is used for this collection.
        /// </summary>
        public string Name
        {
            get
            {
                if (this.Model.Name != null)
                {
                    return this.Model.Name;
                }

                return this._name;
            }

            set
            {
                if (this.SetProperty(ref this._name, value))
                {
                    this.UpdateText();
                }
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        private ProjectNode[] ProcessProjects()
        {
            List<ProjectNode> projectNodes = new List<ProjectNode>();
            foreach (ProjectItem project in this.Model["Project"])
            {
                ProjectNode projectNode = null;
                string filter = project.GetMetadata("Filter");
                if (filter != null)
                {
                    FolderNode node = CreateFolderNodes(filter.TrimEnd('\\') + "\\");
                    if (node != null)
                    {
                        projectNode = this.CreateProject(node, project);
                        node.AddChild(projectNode);
                        projectNodes.Add(projectNode);
                        continue;
                    }

                    Debug.Assert(false, "Failed to create parent for project.");
                }

                projectNode = this.CreateProject(this, project);
                this.AddChild(projectNode);
                projectNodes.Add(projectNode);
            }

            return projectNodes.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        protected ProjectNode CreateProject(IHierarchyNode parent, ProjectItem item)
        {
            string extension = Path.GetExtension(item.GetMetadata("Fullpath"));
            StringComparison comparisonType = StringComparison.OrdinalIgnoreCase;
            ProjectDefinition selectedDefinition = null;
            foreach (ProjectDefinition definition in this.Definition.ProjectDefinitions)
            {
                if (String.Equals(definition.Extension, extension, comparisonType))
                {
                    selectedDefinition = definition;
                    break;
                }
            }

            if (selectedDefinition == null)
            {
                throw new InvalidOperationException();
            }

            ProjectNode projectNode = selectedDefinition.CreateProjectNode();
            projectNode.SetParentAndModel(parent, item);
            return projectNode;
        }

        /// <summary>
        /// Loads folders from the project file into the hierarchy.
        /// </summary>
        private FolderNode[] ProcessFolders()
        {
            List<FolderNode> projectNodes = new List<FolderNode>();
            foreach (ProjectItem folder in this.Model["Folder"])
            {
                this.CreateFolderNodes(folder.Include);
            }

            return projectNodes.ToArray();
        }

        /// <summary>
        /// Walks the subpaths of a project relative path and checks if the folder nodes
        /// hierarchy is already there, if not creates it.
        /// </summary>
        /// <param name="path">
        /// Path of the folder.
        /// </param>
        private FolderNode CreateFolderNodes(string path)
        {
            if (String.IsNullOrEmpty(path))
            {
                throw new SmartArgumentNullException(() => path);
            }

            string[] parts = path.Split(Path.DirectorySeparatorChar);
            FolderNode currentParent = null;
            path = String.Empty;

            for (int i = 0; i < parts.Length; i++)
            {
                if (parts[i].Length > 0)
                {
                    path += parts[i] + "\\";
                    currentParent = VerifySubFolderExists(path, currentParent);
                }
            }

            return currentParent;
        }

        /// <summary>
        /// To support virtual folders, override this method to return your own folder nodes
        /// </summary>
        /// <param name="path">Path to store for this folder</param>
        /// <param name="element">Element corresponding to the folder</param>
        /// <returns>A FolderNode that can then be added to the hierarchy</returns>
        private FolderNode CreateFolderNode(IHierarchyNode parent, ProjectItem item)
        {
            return new CollectionFolderNode(parent, item);
        }

        /// <summary>
        /// Adds the specified item to the end of this nodes child collection.
        /// </summary>
        /// <param name="item">
        /// The item to add.
        /// </param>
        /// <returns>
        /// True if the item has been successfully added to the collection; otherwise, false.
        /// </returns>
        public bool AddChild(IHierarchyNode item)
        {
            this.Items.Add(item);
            return true;
        }

        /// <summary>
        /// Retrieves a iterator around the children underneath this node that share the same
        /// scope as this node.
        /// </summary>
        /// <param name="recursive">
        /// A value indicating whether this method retrieves just the children of this node or
        /// recurses into them as well.
        /// </param>
        /// <returns>
        /// A iterator around the children underneath this node that share the same scope.
        /// </returns>
        public IEnumerable<IHierarchyNode> GetChildrenInSameScope(bool recursive)
        {
            IProjectItemScope scope = this.Model;
            List<IHierarchyNode> nodes = new List<IHierarchyNode>();
            foreach (IHierarchyNode child in this.Items)
            {
                if (child == null)
                {
                    continue;
                }

                IProjectItemScope childScope = child.ProjectItem.Scope;
                if (object.ReferenceEquals(childScope, scope))
                {
                    nodes.Add(child);
                    if (recursive)
                    {
                        nodes.AddRange(child.GetChildrenInSameScope(true));
                    }
                }
            }

            return nodes;
        }

        /// <summary>
        /// Retrieves a iterator around the children underneath this node.
        /// </summary>
        /// <param name="recursive">
        /// A value indicating whether this method retrieves just the children of this node or
        /// recurses into them as well.
        /// </param>
        /// <returns>
        /// A iterator around the children underneath this node.
        /// </returns>
        public IEnumerable<IHierarchyNode> GetChildren(bool recursive)
        {
            List<IHierarchyNode> nodes = new List<IHierarchyNode>();
            foreach (IHierarchyNode child in this.Items)
            {
                if (child == null)
                {
                    continue;
                }

                nodes.Add(child);
                if (recursive)
                {
                    nodes.AddRange(child.GetChildren(true));
                }
            }

            return nodes;
        }

        /// <summary>
        /// Retrieves a iterator around the projects underneath this node.
        /// </summary>
        /// <returns>
        /// A iterator around the projects underneath this node.
        /// </returns>
        public IEnumerable<ProjectNode> GetProjects()
        {
            List<ProjectNode> projectNodes = new List<ProjectNode>();
            foreach (IHierarchyNode node in this.GetChildren(true))
            {
                ProjectNode projectNode = node as ProjectNode;
                if (projectNode == null)
                {
                    continue;
                }

                projectNodes.Add(projectNode);
            }

            return projectNodes;
        }

        /// <summary>
        /// Raises the saved event on this object.
        /// </summary>
        public void RaiseSavedEvent()
        {
            EventHandler handler = this.Saved;
            if (handler == null)
            {
                return;
            }

            handler(this, EventArgs.Empty);
        }

        /// <summary>
        /// Removes the first occurrence of the specified item from this nodes child
        /// collection.
        /// </summary>
        /// <param name="item">
        /// The item to remove.
        /// </param>
        /// <returns>
        /// True if the item was present and has been successfully removed; otherwise, false.
        /// </returns>
        public bool RemoveChild(IHierarchyNode item)
        {
            if (!this.Items.Contains(item))
            {
                return false;
            }

            this.Items.Remove(item);
            return true;
        }

        /// <summary>
        /// Clears the item collection for this node.
        /// </summary>
        public void ClearChildren()
        {
            this.Items.Clear();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="fullPath"></param>
        /// <returns></returns>
        public IHierarchyNode FindNodeWithFullPath(string fullPath)
        {
            if (String.Equals(this.FullPath, fullPath))
            {
                return this;
            }

            foreach (IHierarchyNode node in this.Items)
            {
                IHierarchyNode found = node.FindNodeWithFullPath(fullPath);
                if (found != null)
                {
                    return found;
                }
            }

            return null;
        }

        /// <summary>
        /// Adds a new folder underneath this node and returns the instance to the new folder.
        /// </summary>
        /// <returns>
        /// The new folder that was created.
        /// </returns>
        public IHierarchyNode AddNewFolder()
        {
            string include = "{0}".FormatInvariant("NewFolder");
            include = this.Model.GetUniqueInclude(include);
            ProjectItem item = this.Model.AddNewProjectItem("Folder", include);
            item.SetMetadata("UniqueIdentifier", Guid.NewGuid().ToString("D"), MetadataLevel.Filter);
            HierarchyNode node = this.CreateFolderNode(this, item);
            node.IsExpandable = false;
            this.AddChild(node);
            return node;
        }

        /// <summary>
        /// Saves the data that is associated with this item to the specified file.
        /// </summary>
        /// <param name="stream">
        /// The io stream that data for this object should be written to.
        /// </param>
        /// <returns>
        /// A value indicating whether the save operation was successful.
        /// </returns>
        public bool Save(Stream stream)
        {
            return Save(stream, MetadataLevel.Core);
        }

        /// <summary>
        /// Saves the current state of this instances specified metadata level to the specified
        /// stream.
        /// </summary>
        /// <param name="stream">
        /// The stream to write this instances data to.
        /// </param>
        /// <param name="metadataLevel">
        /// The metadata level that needs to be serialised into the specified stream.
        /// </param>
        public bool Save(Stream stream, MetadataLevel metadataLevel)
        {
            if (this.Model == null)
            {
                return false;
            }

            bool success = this.Model.Save(stream, metadataLevel);

            if(success)
            {
                if (this.ProjectCollectionSaved != null)
                {
                    this.ProjectCollectionSaved(this, new EventArgs());
                }
            }

            return success;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documents">
        /// 
        /// </param>
        public void SetOpenedDocuments(Dictionary<string, int> documents)
        {
            string docStatePath = this.FullPath + ".docstates";
            FileInfo info = new FileInfo(docStatePath);
            if (info.Exists)
            {
                FileAttributes attributes = File.GetAttributes(docStatePath);
                info.IsReadOnly = false;
                attributes &= ~FileAttributes.Hidden;
                File.SetAttributes(docStatePath, attributes);
            }

            using (XmlWriter writer = XmlWriter.Create(docStatePath, new XmlWriterSettings() { Indent = true }))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("DocStates");

                foreach (var kvp in documents)
                {
                    writer.WriteStartElement("OpenedDocument");
                    writer.WriteAttributeString("index", kvp.Value.ToStringInvariant());
                    writer.WriteAttributeString("path", kvp.Key);

                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }

            {
                FileAttributes attributes = File.GetAttributes(docStatePath);
                File.SetAttributes(docStatePath, attributes | FileAttributes.Hidden);
            }
        }

        /// <summary>
        /// Takes a path and verifies that we have a node with that name.
        /// </summary>
        /// <param name="path">
        /// Full path to the subfolder we want to verify.
        /// </param>
        /// <param name="parent">
        /// The parent node where to add the subfolder if it does not exist.
        /// </param>
        /// <returns>
        /// The foldernode corresponding to the path.
        /// </returns>
        private FolderNode VerifySubFolderExists(string path, FolderNode parent)
        {
            path = path.TrimEnd('\\');
            FolderNode folderNode = null;
            IHierarchyNode parentNode = parent;
            if (parentNode == null)
            {
                parentNode = this;
            }

            foreach (HierarchyNode node in parentNode.GetChildren(false))
            {
                string nodeInclude = node.Model.Include.TrimEnd('\\');
                if (String.Equals(nodeInclude, path, StringComparison.OrdinalIgnoreCase))
                {
                    Debug.Assert(node is FolderNode, "Not a FolderNode");
                    folderNode = node as FolderNode;
                    break;
                }
            }

            if (folderNode == null && path != null)
            {
                ProjectItem item = null;
                foreach (ProjectItem folder in this.Model["Folder"])
                {
                    string folderInclude = folder.Include.TrimEnd('\\');
                    if (String.Equals(folderInclude, path, StringComparison.OrdinalIgnoreCase))
                    {
                        item = folder;
                        break;
                    }
                }

                if (item == null)
                {
                    item = this.Model.AddNewProjectItem("Folder", path.TrimEnd('\\'));
                }

                folderNode = this.CreateFolderNode(parentNode, item);
                parentNode.AddChild(folderNode);
            }

            return folderNode;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fullpath"></param>
        /// <returns></returns>
        public void Load(string fullpath)
        {
            this.Model.Load(fullpath);

            // Load the document states
            string docStatePath = fullpath + ".docstates";
            try
            {
                if (File.Exists(docStatePath))
                {
                    using (XmlReader reader = XmlReader.Create(docStatePath))
                    {
                        while (reader.MoveToContent() != XmlNodeType.None)
                        {
                            if (reader.NodeType == XmlNodeType.Element)
                            {
                                if (String.Equals("OpenedDocument", reader.Name))
                                {
                                    int index = 0;
                                    if (!int.TryParse(reader.GetAttribute("index"), out index))
                                    {
                                        index = 0;
                                    }

                                    string path = reader.GetAttribute("path");
                                    if (!String.IsNullOrWhiteSpace(path))
                                    {
                                        this._previouslyOpenedDocuments[path] = index;
                                    }
                                }
                            }

                            reader.Read();
                        }
                    }
                }
            }
            catch
            {
                Debug.Fail("Failed to load document states for the collection");
            }
            
            this.Reload();
            this.Loaded = true;
            if (ProjectCollectionLoaded != null)
            {
                ProjectCollectionLoaded(this, new EventArgs());
            }

            Task.Run(new Action(async () => await this.UpdatePerforceStateAsync(true)));
        }

        /// <summary>
        /// Unloads the currently loaded collection, resetting all of the properties back to
        /// default.
        /// </summary>
        public async void Unload()
        {
            this.Model.Unload();
            await this.ReloadAsync();
            this.Loaded = false;
            this.IsModified = false;

            // Save the document states if we can
        }

        /// <summary>
        /// Reload project from project file
        /// </summary>
        protected void Reload()
        {
            this._items.Clear();
            ProjectNode[] projectNodes = this.ProcessProjects();
            this._projectCount = 0;
            this.UpdateText();
            this.ProcessFolders();

            foreach (ProjectNode p in projectNodes)
            {
                p.Load();
            }
        }

        /// <summary>
        /// Reload project from project file
        /// </summary>
        protected async virtual Task ReloadAsync()
        {
            this._items.Clear();
            ProjectNode[] projectNodes = this.ProcessProjects();
            this._projectCount = 0;
            this.UpdateText();
            this.ProcessFolders();

            await Task.WhenAll(from p in projectNodes select p.LoadAsync());
        }

        internal virtual void OnProjectLoaded(ProjectNode project)
        {
            EventHandler<ProjectLoadedStateChangedEventArgs> handler = this.ProjectLoaded;
            if (handler != null)
            {
                handler(this, new ProjectLoadedStateChangedEventArgs(project));
            }

            ++this._projectCount;
            this.UpdateText();
        }

        internal virtual void OnProjectUnloaded(ProjectNode project)
        {
            EventHandler<ProjectLoadedStateChangedEventArgs> handler = this.ProjectUnloaded;
            if (handler != null)
            {
                handler(this, new ProjectLoadedStateChangedEventArgs(project));
            }

            --this._projectCount;
            this.UpdateText();
        }

        internal virtual void OnProjectFailed(
            ProjectNode project, Exception exception, bool reloaded)
        {
            EventHandler<ProjectLoadFailedEventArgs> handler = this.ProjectFailed;
            if (handler != null)
            {
                handler(this, new ProjectLoadFailedEventArgs(project, exception, reloaded));
            }
        }

        internal virtual void OnProjectSaved(ProjectNode project)
        {
            EventHandler<ProjectLoadedStateChangedEventArgs> handler = this.ProjectSaved;
            if (handler != null)
            {
                handler(this, new ProjectLoadedStateChangedEventArgs(project));
            }
        }

        internal virtual void OnProjectReloaded(ProjectNode project)
        {
        }

        private void UpdateText()
        {
            string format = "Collection '{0}' ({1} projects)";
            this.Text = format.FormatCurrent(this.Name, this._projectCount);
        }

        private async Task UpdatePerforceStateAsync(bool children)
        {
            List<FileNode> fileNodes = new List<FileNode>();
            foreach (IHierarchyNode child in this.GetChildren(false))
            {
                FileNode childFile = child as FileNode;
                if (childFile == null)
                {
                    continue;
                }

                fileNodes.Add(childFile);
            }

            IList<IPerforceFileMetaData> metadataList = await this.GetChildMetadataAsync();
            StringComparison comparisonType = StringComparison.OrdinalIgnoreCase;
            foreach (FileNode fileNode in fileNodes)
            {
                IPerforceFileMetaData metadata = null;
                if (metadataList != null)
                {
                    string filePath = fileNode.FullPath;
                    foreach (IPerforceFileMetaData fileMetadata in metadataList)
                    {
                        string localPath = fileMetadata.LocalPath;
                        if (String.Equals(localPath, filePath, comparisonType))
                        {
                            metadata = fileMetadata;
                            break;
                        }
                    }
                }

                fileNode.UpdatePerforceState(metadata);
            }

            foreach (IPerforceFileMetaData fileMetadata in metadataList)
            {
                if (String.Equals(this.FullPath, fileMetadata.LocalPath, comparisonType))
                {
                    this.UpdatePerforceState(fileMetadata);
                    break;
                }
            }
        }

        /// <summary>
        /// Gets the source control metadata for the child nodes.
        /// </summary>
        /// <returns>
        /// The source control metadata for the child nodes.
        /// </returns>
        private async Task<IList<IPerforceFileMetaData>> GetChildMetadataAsync()
        {
            IPerforceService ps = this.PerforceService;
            if (ps == null)
            {
                return null;
            }

            List<string> filenames = new List<string>();
            List<FileNode> fileNodes = new List<FileNode>();
            foreach (IHierarchyNode child in this.GetChildren(false))
            {
                FileNode childFile = child as FileNode;
                if (childFile == null)
                {
                    continue;
                }

                filenames.Add(childFile.FullPath);
                fileNodes.Add(childFile);
            }

            filenames.Add(this.FullPath);

            var action = new Func<IList<IPerforceFileMetaData>>(
                delegate
                {
                    using (ps.SuspendExceptionsScope())
                    {
                        return ps.GetFileMetaData(filenames);
                    }
                });

            return await Task.Factory.StartNew(action);
        }

        /// <summary>
        /// Updates the state icons and tooltip based on the specified source control metadata.
        /// </summary>
        /// <param name="metadata">
        /// The source control metadata for this file.
        /// </param>
        public void UpdatePerforceState(IPerforceFileMetaData metadata)
        {
            DispatcherHelper.CheckBeginInvokeOnUI(new Action(() =>
            {
                if (metadata != null && metadata.IsMapped)
                {
                    if (metadata.HeadRevision != -1)
                    {
                        if (metadata.HeadRevision == metadata.HaveRevision)
                        {
                            this.OverlayIcon = PerforceIcons.GotLatestOverlay;
                        }
                        else
                        {
                            this.OverlayIcon = PerforceIcons.NotLatestOverlay;
                        }
                    }
                    else
                    {
                        this.OverlayIcon = null;
                    }

                    int width = 7;
                    int height = 16;
                    PixelFormat format = PixelFormats.Bgra32;
                    int stride = width * (format.BitsPerPixel / 8);
                    double dpi = 96.0;
                    byte[] pixels = new byte[stride * height];
                    if (metadata.Action == PerforceFileAction.Edit)
                    {
                        PerforceIcons.CheckedOutState.CopyPixels(pixels, stride, 0);
                    }
                    else if (metadata.Action == PerforceFileAction.Add)
                    {
                        PerforceIcons.AddState.CopyPixels(pixels, stride, 0);
                    }

                    this.StateIcon = BitmapSource.Create(
                        width, height, dpi, dpi, PixelFormats.Bgra32, null, pixels, stride);
                }
                else
                {
                    this.StateIcon = null;
                    this.OverlayIcon = null;
                }
            }));
        }
        #endregion Methods

        public BitmapSource ExpandedIcon
        {
            get { return null; }
        }
    } // RSG.Project.ViewModel.ProjectCollectionNode {Class}
} // RSG.Project.ViewModel {Namespace}
