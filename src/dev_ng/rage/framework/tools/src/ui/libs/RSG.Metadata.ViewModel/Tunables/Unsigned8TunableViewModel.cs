﻿//---------------------------------------------------------------------------------------------
// <copyright file="Unsigned8TunableViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Tunables
{
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// The view model for a <see cref="Unsigned8Tunable"/> model object.
    /// </summary>
    public class Unsigned8TunableViewModel : TunableViewModelBase<Unsigned8Tunable>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Unsigned8TunableViewModel"/> class.
        /// </summary>
        /// <param name="tunable">
        /// The unsigned 8-bit integer tunable that this view model is representing.
        /// </param>
        /// <param name="parent">
        /// The parent tunable view model for this view model.
        /// </param>
        public Unsigned8TunableViewModel(Unsigned8Tunable tunable, ITunableViewModel parent)
            : base(tunable, parent)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the value currently assigned to the unsigned 8-bit integer tunable.
        /// </summary>
        public byte Value
        {
            get { return this.Model.Value; }
            set { this.Model.Value = value; }
        }
        #endregion Properties
    } // RSG.Metadata.ViewModel.Tunables.Unsigned8TunableViewModel {Class}
} // RSG.Metadata.ViewModel.Tunables {Namespace}
