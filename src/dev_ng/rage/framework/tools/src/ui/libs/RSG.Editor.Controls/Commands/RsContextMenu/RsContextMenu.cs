﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsContextMenu.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Commands
{
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Represents a pop-up menu that enables a control to expose functionality that is
    /// specific to the context of the control.
    /// </summary>
    public class RsContextMenu : ContextMenu
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="BatchStyleKey"/> property.
        /// </summary>
        private static ResourceKey _batchStyleKey = CreateStyleKey("BatchStyleKey");

        /// <summary>
        /// The private field used for the <see cref="ButtonStyleKey"/> property.
        /// </summary>
        private static ResourceKey _buttonStyleKey = CreateStyleKey("ButtonStyleKey");

        /// <summary>
        /// The private field used for the <see cref="ComboBoxStyleKey"/> property.
        /// </summary>
        private static ResourceKey _comboBoxStyleKey = CreateStyleKey("ComboBoxStyleKey");

        /// <summary>
        /// The private field used for the <see cref="ControllerStyleKey"/> property.
        /// </summary>
        private static ResourceKey _controllerStyleKey = CreateStyleKey("ControllerStyleKey");

        /// <summary>
        /// The private field used for the <see cref="DynamicMenuStyleKey"/> property.
        /// </summary>
        private static ResourceKey _dynamicMenuStyleKey = CreateStyleKey("DynamicMenuStyleKey");

        /// <summary>
        /// The private field used for the <see cref="GridCheckStyleKey"/> property.
        /// </summary>
        private static ResourceKey _gridCheckStyleKey = CreateStyleKey("GridCheckStyleKey");

        /// <summary>
        /// The private field used for the <see cref="GridComboStyleKey"/> property.
        /// </summary>
        private static ResourceKey _gridComboStyleKey =
            CreateStyleKey("GridComboStyleKey");

        /// <summary>
        /// The private field used for the <see cref="GridSpinnerStyleKey"/> property.
        /// </summary>
        private static ResourceKey _gridSpinnerStyleKey =
            CreateStyleKey("GridSpinnerStyleKey");

        /// <summary>
        /// The private field used for the <see cref="MenuStyleKey"/> property.
        /// </summary>
        private static ResourceKey _menuStyleKey = CreateStyleKey("MenuStyleKey");

        /// <summary>
        /// The private field used for the <see cref="SeparatorStyleKey"/> property.
        /// </summary>
        private static ResourceKey _separatorStyleKey = CreateStyleKey("SeparatorStyleKey");

        /// <summary>
        /// The private field used for the <see cref="TextStyleKey"/> property.
        /// </summary>
        private static ResourceKey _textStyleKey = CreateStyleKey("TextStyleKey");

        /// <summary>
        /// The private field used for the <see cref="ToggleStyleKey"/> property.
        /// </summary>
        private static ResourceKey _toggleStyleKey = CreateStyleKey("ToggleStyleKey");

        /// <summary>
        /// The private field used for the <see cref="ToolBarStyleKey"/> property.
        /// </summary>
        private static ResourceKey _toolBarStyleKey = CreateStyleKey("ToolBarStyleKey");
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsContextMenu"/> class.
        /// </summary>
        static RsContextMenu()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsContextMenu),
                new FrameworkPropertyMetadata(typeof(RsContextMenu)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsContextMenu"/> class.
        /// </summary>
        public RsContextMenu()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the resource key used to reference the style used on batch command items.
        /// </summary>
        public static ResourceKey BatchStyleKey
        {
            get { return _batchStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on button command items.
        /// </summary>
        public static ResourceKey ButtonStyleKey
        {
            get { return _buttonStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on combo box command items.
        /// </summary>
        public static ResourceKey ComboBoxStyleKey
        {
            get { return _comboBoxStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on controller command items.
        /// </summary>
        public static ResourceKey ControllerStyleKey
        {
            get { return _controllerStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on dynamic menu command
        /// items.
        /// </summary>
        public static ResourceKey DynamicMenuStyleKey
        {
            get { return _dynamicMenuStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used for items representing a
        /// check box data grid column.
        /// </summary>
        public static ResourceKey GridCheckStyleKey
        {
            get { return _gridCheckStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used for items representing a
        /// combo box data grid column.
        /// </summary>
        public static ResourceKey GridComboStyleKey
        {
            get { return _gridComboStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used for items representing a
        /// integer spinner data grid column.
        /// </summary>
        public static ResourceKey GridSpinnerStyleKey
        {
            get { return _gridSpinnerStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on menu command items.
        /// </summary>
        public static ResourceKey MenuStyleKey
        {
            get { return _menuStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on separator command items.
        /// </summary>
        public static ResourceKey SeparatorStyleKey
        {
            get { return _separatorStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on text command items.
        /// </summary>
        public static ResourceKey TextStyleKey
        {
            get { return _textStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on toggle command items.
        /// </summary>
        public static ResourceKey ToggleStyleKey
        {
            get { return _toggleStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on a toolbar command items.
        /// </summary>
        public static ResourceKey ToolBarStyleKey
        {
            get { return _toolBarStyleKey; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates or identifies the element used to display the child items.
        /// </summary>
        /// <returns>
        /// A new <see cref="RSG.Editor.Controls.Commands.RsMenuItem"/>.
        /// </returns>
        protected override DependencyObject GetContainerForItemOverride()
        {
            RsMenuItem newItem = new RsMenuItem();
            newItem.IsPlacedInContextMenu = true;
            return newItem;
        }

        /// <summary>
        /// Determines whether the specified item is, or is eligible to be, its own item
        /// container.
        /// </summary>
        /// <param name="item">
        /// The item to check whether it is an item container.
        /// </param>
        /// <returns>
        /// True if the item is eligible to be its own item container; otherwise, false.
        /// </returns>
        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            if (item is MenuItem || item is Separator)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Prepares the specified element to display the specified item.
        /// </summary>
        /// <param name="element">
        /// Element used to display the specified item.
        /// </param>
        /// <param name="item">
        /// Specified item.
        /// </param>
        protected override void PrepareContainerForItemOverride(
            DependencyObject element, object item)
        {
            FrameworkElement frameworkElement = element as FrameworkElement;
            CommandBarItemViewModel viewmodel = item as CommandBarItemViewModel;
            if (viewmodel == null)
            {
                base.PrepareContainerForItemOverride(element, item);
                return;
            }

            ResourceKey resource = null;
            switch (viewmodel.DisplayType)
            {
                case CommandItemDisplayType.ToolBar:
                    resource = RsContextMenu.ToolBarStyleKey;
                    break;
                case CommandItemDisplayType.Button:
                    resource = RsContextMenu.ButtonStyleKey;
                    break;
                case CommandItemDisplayType.Combo:
                    resource = RsContextMenu.ComboBoxStyleKey;
                    break;
                case CommandItemDisplayType.Menu:
                    resource = RsContextMenu.MenuStyleKey;
                    break;
                case CommandItemDisplayType.Separator:
                    resource = RsContextMenu.SeparatorStyleKey;
                    break;
                case CommandItemDisplayType.Toggle:
                    resource = RsContextMenu.ToggleStyleKey;
                    break;
                case CommandItemDisplayType.DynamicMenu:
                    resource = RsContextMenu.DynamicMenuStyleKey;
                    break;
            }

            if (resource == null)
            {
                return;
            }

            frameworkElement.SetResourceReference(FrameworkElement.StyleProperty, resource);
        }

        /// <summary>
        /// Creates a new resource key that references a style with the specified name.
        /// </summary>
        /// <param name="styleName">
        /// The name that the resource key will be referencing.
        /// </param>
        /// <returns>
        /// A new resource key that references a style with the specified name.
        /// </returns>
        private static ResourceKey CreateStyleKey(string styleName)
        {
            return new StyleKey<RsContextMenu>(styleName);
        }
        #endregion Methods
    } // RSG.Editor.Controls.Commands.RsContextMenu {Class}
} // RSG.Editor.Controls.Commands {Namespace}
