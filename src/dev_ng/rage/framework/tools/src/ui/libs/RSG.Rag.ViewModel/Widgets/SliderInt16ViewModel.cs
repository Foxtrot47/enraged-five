﻿using RSG.Rag.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.ViewModel.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    public class SliderInt16ViewModel : SliderViewModel<Int16>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        public SliderInt16ViewModel(WidgetSlider<Int16> widget)
            : base(widget)
        {
        }
        #endregion // Constructor(s)
    } // SliderInt16ViewModel
}
