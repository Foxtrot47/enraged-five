﻿//---------------------------------------------------------------------------------------------
// <copyright file="MruCommandActionAsync{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Threading.Tasks;

    /// <summary>
    /// Provides a abstract base class to any class that is used to implement a MRU button
    /// command.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the parameter pass into the can execute and execute methods.
    /// </typeparam>
    public abstract class MruCommandActionAsync<T> :
        ButtonActionAsync<T, MultiCommandParameter<Tuple<string, string[]>>>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MruCommandActionAsync{T}"/>
        /// class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public MruCommandActionAsync(ParameterResolverDelegate<T> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="commandParameter">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="path">
        /// The path sent with the MRU command.
        /// </param>
        /// <param name="additional">
        /// The additional information for the path property sent with the command.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public virtual bool CanExecute(T commandParameter, string path, string[] additional)
        {
            return true;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="commandParameter">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="path">
        /// The path sent with the MRU command.
        /// </param>
        /// <param name="additional">
        /// The additional information for the path property sent with the command.
        /// </param>
        /// <returns>
        /// A task that represents the work done by the command handler.
        /// </returns>
        public abstract Task Execute(T commandParameter, string path, string[] additional);

        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="commandParameter">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="parameter">
        /// The secondary command parameter that has been sent with the command.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override sealed bool CanExecute(
            T commandParameter,
            MultiCommandParameter<Tuple<string, string[]>> parameter)
        {
            if (parameter == null || parameter.ItemParameter == null)
            {
                return false;
            }

            Tuple<string, string[]> tuple = parameter.ItemParameter;
            return this.CanExecute(commandParameter, tuple.Item1, tuple.Item2);
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="commandParameter">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="parameter">
        /// The secondary command parameter that has been sent with the command.
        /// </param>
        /// <returns>
        /// A task that represents the work done by the command handler.
        /// </returns>
        public override sealed async Task Execute(
            T commandParameter,
            MultiCommandParameter<Tuple<string, string[]>> parameter)
        {
            if (parameter == null || parameter.ItemParameter == null)
            {
                return;
            }

            Tuple<string, string[]> tuple = parameter.ItemParameter;
            await this.Execute(commandParameter, tuple.Item1, tuple.Item2);
        }
        #endregion Methods
    } // RSG.Editor.MruCommandActionAsync{T} {Class}
} // RSG.Editor {Namespace}
