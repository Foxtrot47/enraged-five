﻿//---------------------------------------------------------------------------------------------
// <copyright file="TextViewCommandIds.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.View
{
    using System;

    /// <summary>
    /// Contains global identifiers that are used throughout the RPF viewer control.
    /// </summary>
    public static class TextViewCommandIds
    {
        #region Fields
        /// <summary>
        /// The private instance used for the <see cref="ConversationContextMenu"/> property.
        /// </summary>
        private static Guid? _conversationContextMenu;

        /// <summary>
        /// The private instance used for the <see cref="LineContextMenu"/> property.
        /// </summary>
        private static Guid? _lineContextMenu;

        /// <summary>
        /// A private instance to a generic object used to support thread access.
        /// </summary>
        private static object _syncRoot = new object();
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets the global identifier used to retrieve the commands that should be displayed
        /// on the context menu for the DialogueConversation control.
        /// </summary>
        public static Guid ConversationContextMenu
        {
            get
            {
                if (!_conversationContextMenu.HasValue)
                {
                    lock (_syncRoot)
                    {
                        if (!_conversationContextMenu.HasValue)
                        {
                            _conversationContextMenu =
                                new Guid("C0D9B1D8-083F-468D-B03F-B7730B4F8524");
                        }
                    }
                }

                return _conversationContextMenu.Value;
            }
        }

        /// <summary>
        /// Gets the global identifier used to retrieve the commands that should be displayed
        /// on the context menu for the DialogueLine control.
        /// </summary>
        public static Guid LineContextMenu
        {
            get
            {
                if (!_lineContextMenu.HasValue)
                {
                    lock (_syncRoot)
                    {
                        if (!_lineContextMenu.HasValue)
                        {
                            _lineContextMenu =
                                new Guid("89323A11-8DFD-4A1F-9434-99B17C15DD7F");
                        }
                    }
                }

                return _lineContextMenu.Value;
            }
        }
        #endregion Properties
    } // RSG.Text.View.TextViewCommandIds {Class}
} // RSG.Text.View {Namespace}
