﻿//---------------------------------------------------------------------------------------------
// <copyright file="CommandContainerViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Customisation
{
    using System;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.Text.RegularExpressions;
    using RSG.Editor.SharedCommands;

    /// <summary>
    /// Represents a view model that wraps a collection of commands together underneath a
    /// single container.
    /// </summary>
    public abstract class CommandContainerViewModel : CommandViewModel
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="ChildContainers"/> property.
        /// </summary>
        private ObservableCollection<CommandContainerViewModel> _childContainers;

        /// <summary>
        /// The private field used for the <see cref="Children"/> property.
        /// </summary>
        private ObservableCollection<CommandViewModel> _children;

        /// <summary>
        /// The private field used for the <see cref="IsExpandable"/> property.
        /// </summary>
        private bool _isExpandable;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CommandContainerViewModel"/> class.
        /// </summary>
        /// <param name="text">
        /// The text used for this new container view model.
        /// </param>
        /// <param name="id">
        /// The id that this new container view model uses to retrieve its children from the
        /// Rockstar command manager.
        /// </param>
        public CommandContainerViewModel(string text, Guid id)
            : base(text, id)
        {
            this._isExpandable = true;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="CommandContainerViewModel"/> class.
        /// </summary>
        /// <param name="item">
        /// The item that this view model is wrapping.
        /// </param>
        /// <param name="container">
        /// The container that this view model is currently inside.
        /// </param>
        public CommandContainerViewModel(
            CommandBarItem item, CommandContainerViewModel container)
            : base(item, container)
        {
            this._isExpandable = true;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the containers that belong to this container.
        /// </summary>
        public ObservableCollection<CommandContainerViewModel> ChildContainers
        {
            get
            {
                if (this._childContainers != null)
                {
                    return this._childContainers;
                }

                this._children = new ObservableCollection<CommandViewModel>();
                this._childContainers = new ObservableCollection<CommandContainerViewModel>();
                this.CreateChildren();
                return this._childContainers;
            }
        }

        /// <summary>
        /// Gets all of the children, both containers and non-containers that belong to this
        /// container.
        /// </summary>
        public ObservableCollection<CommandViewModel> Children
        {
            get
            {
                if (this._children != null)
                {
                    return this._children;
                }

                this._children = new ObservableCollection<CommandViewModel>();
                this._childContainers = new ObservableCollection<CommandContainerViewModel>();
                this.CreateChildren();
                return this._children;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this item can be expanded in the view.
        /// </summary>
        public override bool IsExpandable
        {
            get { return this._isExpandable; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Re-initialises the items in this container. This can be called once the commands
        /// have been reset to make sure the items shown to the user are correctly synced up
        /// against the model.
        /// </summary>
        public void ResetItems()
        {
            this.Children.Clear();
            this.ChildContainers.Clear();
            this.CreateChildren();
        }

        /// <summary>
        /// Creates the children, both the containers and non containers for this container.
        /// </summary>
        private void CreateChildren()
        {
            foreach (CommandBarItem child in RockstarCommandManager.GetCommands(this.Id))
            {
                CommandToolBar toolbar = child as CommandToolBar;
                if (toolbar != null)
                {
                    var viewModel = CommandToolBarViewModel.CreateChild(toolbar, this);
                    this.Children.Add(viewModel);
                    this.ChildContainers.Add(viewModel);
                }
                else
                {
                    CommandMenu menu = child as CommandMenu;
                    if (menu != null)
                    {
                        var viewModel = CommandMenuViewModel.CreateChild(menu, this);
                        this.Children.Add(viewModel);
                        this.ChildContainers.Add(viewModel);
                    }
                    else
                    {
                        CommandInstance instance = child as CommandInstance;
                        if (instance != null)
                        {
                            var viewModel = new CommandInstanceViewModel(instance, this);
                            this.Children.Add(viewModel);
                        }
                    }
                }
            }

            this.SetProperty(ref this._isExpandable, this.Children.Count > 0, "IsExpandable");
        }
        #endregion Methods
    } // RSG.Editor.Controls.Customisation.CommandContainerViewModel {Class}
} // RSG.Editor.Controls.Customisation {Namespace}
