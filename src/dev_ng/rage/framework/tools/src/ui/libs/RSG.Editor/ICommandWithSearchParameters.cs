﻿//---------------------------------------------------------------------------------------------
// <copyright file="ICommandWithSearchParameters.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    /// <summary>
    /// When implemented represents a command that contains parameters used for searching.
    /// </summary>
    public interface ICommandWithSearchParameters
    {
        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether the user can filter the view to just show
        /// the search results.
        /// </summary>
        bool CanFilterResults { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user can search with case sensitivity.
        /// </summary>
        bool CanUseMatchCase { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user can search for whole words.
        /// </summary>
        bool CanUseMatchWholeWord { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user can search using regular
        /// expressions.
        /// </summary>
        bool CanUseRegularExpressions { get; set; }
        #endregion Properties
    }  // RSG.Editor.ICommandWithSearchParameters {Interface}
} // RSG.Editor {Namespace}
