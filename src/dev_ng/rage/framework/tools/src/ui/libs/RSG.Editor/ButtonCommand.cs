﻿//---------------------------------------------------------------------------------------------
// <copyright file="ButtonCommand.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    /// <summary>
    /// Provides the ability to define a rockstar command as a button command.
    /// </summary>
    public class ButtonCommand : CommandDefinition, ICommandWithParameter
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Command"/> property.
        /// </summary>
        private RockstarRoutedCommand _command;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ButtonCommand"/> class.
        /// </summary>
        /// <param name="command">
        /// The <see cref="RockstarRoutedCommand"/> that this definition is
        /// representing.
        /// </param>
        public ButtonCommand(RockstarRoutedCommand command)
        {
            this._command = command;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the <see cref="RockstarRoutedCommand"/> that this definition is wrapping.
        /// </summary>
        public override RockstarRoutedCommand Command
        {
            get { return this._command; }
        }

        /// <summary>
        /// Gets the parameter that is routed through the element tree with the command.
        /// </summary>
        public virtual object CommandParameter
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the parameter that is routed through the element tree with the command.
        /// </summary>
        object ICommandWithParameter.CommandParameter
        {
            get { return this.CommandParameter; }
        }
        #endregion Properties
    }  // RSG.Editor.ButtonCommand {Class}
} // RSG.Editor {Namespace}
