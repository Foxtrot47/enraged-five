//---------------------------------------------------------------------------------------------
// <copyright file="RsApplication.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Runtime;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Markup;
    using System.Windows.Media;
    using System.Windows.Threading;
    using System.Xml;
    using Microsoft.Win32;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Base.Logging.Universal;
    using RSG.Base.OS;
    using RSG.Configuration;
    using RSG.Configuration.Bugstar;
    using RSG.Editor.Controls.Exceptions;
    using RSG.Editor.Controls.Helpers;
    using RSG.Editor.Controls.Resources;
    using RSG.Editor.Controls.Themes;
    using RSG.Editor.SharedCommands;
    using RSG.Interop.Microsoft.Windows;
    using RSG.Services.Configuration;
    using RSG.Services.Configuration.Statistics;
    using RSG.Services.Statistics.Consumers;

    /// <summary>
    /// Provides a abstract base class for a WPF application that supports different modes,
    /// themes and the new Rockstar command system.
    /// </summary>
    public abstract class RsApplication :
        Application,
        ICommandServiceProvider,
        IApplicationInfoProvider,
        IBugstarInfoProvider
    {
        #region Fields
        /// <summary>
        /// Reference to the application log.
        /// </summary>
        protected readonly IUniversalLog Log;

        /// <summary>
        /// The private field used for the <see cref="AvailableThemes"/> property.
        /// </summary>
        private static Dictionary<ApplicationTheme, string> _availableThemes;

        /// <summary>
        /// The private field used for the <see cref="ApplicationInitialised"/> property.
        /// </summary>
        private bool _applicationInitialised = false;

        /// <summary>
        /// The private field used for the <see cref="ApplicationSessionGuid"/> property.
        /// </summary>
        private readonly Guid _applicationSessionGuid = Guid.NewGuid();

        /// <summary>
        /// A value indicating whether the application session was tracked.
        /// </summary>
        private bool _applicationSessionTracked = false;

        /// <summary>
        /// A private array containing the command-line options that will be parsed by the
        /// start up method.
        /// </summary>
        private readonly IList<LongOption> _options = new List<LongOption>();

        /// <summary>
        /// The time zone code to use to retrieve the assemblies build time.
        /// </summary>
        private const string AssemblyBuildTimeZoneCode = "GMT Standard Time";

        /// <summary>
        /// The private field used for the <see cref="BugstarComponent"/> property.
        /// </summary>
        private string _bugstarComponent;

        /// <summary>
        /// The private field used for the <see cref="BugstarProjectName"/> property.
        /// </summary>
        private string _bugstarProjectName;

        /// <summary>
        /// The private field used for the <see cref="CommandLineOptions"/> property.
        /// </summary>
        private Getopt _commandLineOptions;

        /// <summary>
        /// The private field used for the <see cref="CommandOptions"/> property.
        /// </summary>
        private CommandOptions _commandOptions;

        /// <summary>
        /// The private field used for the <see cref="CurrentTheme"/> property.
        /// </summary>
        private ApplicationTheme _currentTheme;

        /// <summary>
        /// A private reference to the dictionary that has been included in with this
        /// applications resources as the theme dictionary.
        /// </summary>
        private ResourceDictionary _currentThemeDictionary;

        /// <summary>
        /// The private field used for the <see cref="DefaultBugOwner"/> property.
        /// </summary>
        private string _defaultBugOwner;

        /// <summary>
        /// The private field used for the <see cref="Exiting"/> property.
        /// </summary>
        private bool _exiting;

        /// <summary>
        /// The private field used for the <see cref="Mode"/> property.
        /// </summary>
        private ApplicationMode _mode = ApplicationMode.Multiple;

        /// <summary>
        /// The private dictionary containing all of the registered Mru lists indexed by their
        /// name.
        /// </summary>
        private Dictionary<string, MruListDefinition> _registeredMruLists;

        /// <summary>
        /// The private field used for the <see cref="StatisticsServer"/> property.
        /// </summary>
        private IServer _statisticsServer;

        /// <summary>
        /// A private collection containing weak references to all of the user controls that
        /// have been loaded in the application so that serialisation can be performed on exit.
        /// </summary>
        private List<WeakReference<RsUserControl>> _userControls;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsApplication"/> class.
        /// </summary>
        static RsApplication()
        {
            LogFactory.Initialize();

            string themePath = "pack://application:,,,/RSG.Editor.Controls;component/{0}";
            _availableThemes = new Dictionary<ApplicationTheme, string>();

            Type enumType = typeof(ApplicationTheme);
            foreach (ApplicationTheme applicationTheme in Enum.GetValues(enumType))
            {
                var att = applicationTheme.GetAttributeOfType<ThemeRelativePathAttribute>();
                if (att == null)
                {
                    continue;
                }

                string location = themePath.FormatInvariant(att.RelativePath);
                _availableThemes.Add(applicationTheme, location);
            }

            CommandFocusManager.Initialise();
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsApplication"/> class.
        /// </summary>
        protected RsApplication()
        {
            this.DispatcherUnhandledException += this.OnUnhandledException;
            AppDomain.CurrentDomain.UnhandledException += this.OnUnhandledException;

            this.Log = LogFactory.ApplicationLog;
            DispatcherHelper.Initialize();

            if (String.IsNullOrWhiteSpace(this.UniqueName))
            {
                throw new FormatException(
                    "Unable to start application with a empty or null unique name!");
            }

            this.RegisterCommandlineArg(
                "statistics-server",
                LongOption.ArgType.Required,
                "Name of the statistics server to connect to.");

            this._userControls = new List<WeakReference<RsUserControl>>();
            this._registeredMruLists = new Dictionary<string, MruListDefinition>();
            string localData = Environment.GetFolderPath(
                Environment.SpecialFolder.LocalApplicationData);
            string root = Path.Combine(localData, "Rockstar Profile Root", this.UniqueName);
            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }

            ProfileOptimization.SetProfileRoot(root);
            ProfileOptimization.StartProfile("Startup.Profile");

            this.LoadEditorValues();

            Collection<ResourceDictionary> resources = this.Resources.MergedDictionaries;
            if (resources == null)
            {
                return;
            }

            string theme = "Themes/theme.standard.xaml";
            string themePath = "pack://application:,,,/RSG.Editor.Controls;component/{0}";
            themePath = themePath.FormatInvariant(theme);
            Uri source = new Uri(themePath, System.UriKind.RelativeOrAbsolute);

            ResourceDictionary newDictionary = new ResourceDictionary();
            try
            {
                newDictionary.Source = source;
            }
            catch (Exception)
            {
                Debug.Assert(newDictionary != null, "Unable to create the standard themes.");
                return;
            }

            resources.Add(newDictionary);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a iterator around the themes that are available to this application.
        /// </summary>
        public static IEnumerable<ApplicationTheme> AvailableThemes
        {
            get
            {
                foreach (ApplicationTheme theme in _availableThemes.Keys)
                {
                    yield return theme;
                }
            }
        }

        /// <summary>
        /// Gets or sets a flag indicating whether the application has been successfully
        /// initialised.
        /// </summary>
        public bool ApplicationInitialised
        {
            get { return _applicationInitialised; }
            protected set { _applicationInitialised = value; }
        }

        /// <summary>
        /// Gets the unique application session identifier used for stats tracking purposes.
        /// </summary>
        public Guid ApplicationSessionGuid
        {
            get { return this._applicationSessionGuid; }
        }

        /// <summary>
        /// Gets a semi-colon separate list of the application authors email addresses. These
        /// are the addresses which are used by the unhandled exception dialog to determine
        /// where the mail should be sent by default.
        /// e.g.
        /// *tools@rockstarnorth.com
        /// michael.taschler@rockstarnorth.com;dave.evans@rockstarnorth.com.
        /// </summary>
        public virtual string AuthorEmailAddress
        {
            get { return String.Join(";", CommandOptions.Project.ToolsEmailAddresses); }
        }

        /// <summary>
        /// Gets the name of the bug star component that bugs should have their component
        /// set to.
        /// </summary>
        public string BugstarComponent
        {
            get { return this._bugstarComponent; }
        }

        /// <summary>
        /// Gets the name of the bug star project that bugs should be added to.
        /// </summary>
        public string BugstarProjectName
        {
            get { return this._bugstarProjectName; }
        }

        /// <summary>
        /// Gets the command line options that are passed into this instance of the
        /// application on start-up.
        /// </summary>
        [Obsolete("Use CommandOptions Instead")]
        public Getopt CommandLineOptions
        {
            get
            {
                if (this._commandLineOptions == null)
                {
                    Debug.Fail(
                        "Attempting to access command line options prior to the application " +
                        "being initialised");

                    throw new ArgumentNullException(
                        "Attempting to access command line options prior to the application " +
                        "being initialised");
                }

                return this._commandLineOptions;
            }
        }

        /// <summary>
        /// Gets the command line options that are passed into this instance of the
        /// application on start-up.
        /// </summary>
        public CommandOptions CommandOptions
        {
            get
            {
                if (this._commandOptions == null)
                {
                    Debug.Fail(
                        "Attempting to access command line options prior to the application " +
                        "being initialised");

                    throw new ArgumentNullException(
                        "Attempting to access command line options prior to the application " +
                        "being initialised");
                }

                return this._commandOptions;
            }
        }

        /// <summary>
        /// Gets the theme that is currently set.
        /// </summary>
        public ApplicationTheme CurrentTheme
        {
            get { return this._currentTheme; }
        }

        /// <summary>
        /// Gets the name of the default owner to assign bugs to.
        /// </summary>
        public string DefaultBugOwner
        {
            get { return this._defaultBugOwner; }
        }

        /// <summary>
        /// Gets a value indicating whether the application is current exiting.
        /// </summary>
        public bool Exiting
        {
            get { return this._exiting; }
        }

        /// <summary>
        /// Gets the product name in the assembly information of the entry assembly. This value
        /// is used as a friendly name display on the different dialogues and splash screen.
        /// </summary>
        public string ProductName
        {
            get
            {
                Type type = this.GetType();
                string identifier = type.GUID.ToString("B");
                Assembly assembly = type.Assembly;

                string defaultValue = "Unknown Product - {0}".FormatInvariant(identifier);
                return AssemblyInfo.GetProductName(assembly, defaultValue);
            }
        }

        /// <summary>
        /// Gets the statistics server configuration object.
        /// </summary>
        public IServer StatisticsServer
        {
            get { return this._statisticsServer; }
        }

        /// <summary>
        /// Gets a value indicating whether we should throw an exception when an unregistered
        /// command line argument is found while parsing the arguments.
        /// </summary>
        public virtual bool ThrowOnUnsupportedCommandLineArgs
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether we wish to track the application session on the
        /// statistics server.
        /// </summary>
        public virtual bool TrackSessionStatistics
        {
            get { return true; }
        }

        /// <summary>
        /// Gets the trailing command line arguments that are passed into this instance of the
        /// application on start-up.
        /// </summary>
        public IEnumerable<string> TrailingArguments
        {
            get
            {
                if (this._commandLineOptions == null)
                {
                    Debug.Fail(
                        "Attempting to access command line options prior to the application " +
                        "being initialised");

                    throw new ArgumentNullException(
                        "Attempting to access command line options prior to the application " +
                        "being initialised");
                }

                return this._commandLineOptions.TrailingOptions;
            }
        }

        /// <summary>
        /// Gets the unique name for this application that is used for mutex creation, and
        /// folder organisation inside the local app data folder and the registry.
        /// </summary>
        public virtual string UniqueName
        {
            get { return this.ProductName; }
        }

        /// <summary>
        /// Gets the application version.
        /// </summary>
        public string Version
        {
            get
            {
                Type type = this.GetType();
                Assembly assembly = type.Assembly;
                return AssemblyInfo.GetVersion(assembly).ToString();
            }
        }

        /// <summary>
        /// Gets a lookup of the available application themes to retrieve their url source.
        /// </summary>
        internal static Dictionary<ApplicationTheme, string> AvailableThemeLookup
        {
            get { return _availableThemes; }
        }

        /// <summary>
        /// Gets the application mode this application was started in.
        /// </summary>
        internal ApplicationMode Mode
        {
            get { return this._mode; }
        }

        /// <summary>
        /// Gets the name of the mutex to use if the application mode is set to Single.
        /// </summary>
        public string MutexName
        {
            get { return String.Format("{0}:{1}", this.UniqueName, Environment.UserName); }
        }

        /// <summary>
        /// Gets the directory where application specific settings can be saved to.
        /// </summary>
        protected string ApplicationSettingsDirectory
        {
            get
            {
                Environment.SpecialFolder folder = Environment.SpecialFolder.LocalApplicationData;
                string appData = Environment.GetFolderPath(folder);
                return Path.Combine(appData, "Rockstar Games", this.UniqueName);
            }
        }

        /// <summary>
        /// Gets the theme that is used if the theme entry in the registry is missing or
        /// invalid.
        /// </summary>
        protected virtual ApplicationTheme DefaultTheme
        {
            get { return ApplicationTheme.Light; }
        }

        /// <summary>
        /// Gets a flag containing the buttons to display in the unhandled exception window
        /// dialog.
        /// </summary>
        protected virtual ExceptionWindowButtons ExceptionWindowButtons
        {
            get { return ExceptionWindowButtons.Default; }
        }

        /// <summary>
        /// Gets a value indicating whether the layout for the main window gets serialised on
        /// exit and deserialised during loading.
        /// </summary>
        protected virtual bool MakeLayoutPersistent
        {
            get { return false; }
        }

        /// <summary>
        /// Gets the read-only list of supported command-line options.
        /// </summary>
        protected IReadOnlyList<LongOption> Options
        {
            get { return (IReadOnlyList<LongOption>)this._options; }
        }

        /// <summary>
        /// Gets a value indicating whether the editor settings should be saved once the
        /// application has been destroyed.
        /// </summary>
        protected virtual bool SaveEditorSettings
        {
            get { return false; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Adds the specified value to the Mru list with the specified name. The list has to
        /// have been first registered.
        /// </summary>
        /// <param name="name">
        /// The name of the Mru list to add the new item to.
        /// </param>
        /// <param name="value">
        /// The parameter of the item to add to the specified Mru list as well as the text to
        /// show the user in the user interface.
        /// </param>
        /// <param name="additionalData">
        /// An array of additional data that is sent with the fired command when the item is
        /// executed.
        /// </param>
        public void AddToMruList(string name, string value, string[] additionalData)
        {
            MruListDefinition definition = null;
            if (name == null || !this._registeredMruLists.TryGetValue(name, out definition))
            {
                string msg = "Unable to add item to a unregistered Mru list";
                Debug.Assert(this._registeredMruLists.ContainsKey(name), msg);
                return;
            }

            if (!this.Dispatcher.CheckAccess())
            {
                this.Dispatcher.BeginInvoke(
                    new Action(
                        delegate
                        {
                            this.AddToMruList(name, value, additionalData);
                        }));

                return;
            }

            definition.AddItemToList(value, additionalData);
        }

        /// <summary>
        /// Determines whether the specified windows can close without the need for user
        /// interaction. For example unsaved data.
        /// </summary>
        /// <param name="windows">
        /// The windows that are to be tested.
        /// </param>
        /// <returns>
        /// True if the specified windows can be closed without user interaction; otherwise,
        /// false.
        /// </returns>
        public virtual bool CanCloseWithoutInteraction(ISet<Window> windows)
        {
            return true;
        }

        /// <summary>
        /// Gets the service object of the specified type.
        /// </summary>
        /// <param name="serviceType">
        /// An object that specifies the type of service object to get.
        /// </param>
        /// <returns>
        /// The service of the specified type if found; otherwise, null.
        /// </returns>
        public virtual object GetService(Type serviceType)
        {
            if (serviceType == null)
            {
                throw new ArgumentNullException("serviceType");
            }

            if (serviceType == typeof(IMessageBoxService))
            {
                return new MessageBoxService();
            }

            if (serviceType == typeof(ICommonDialogService))
            {
                return new CommonDialogService(this);
            }

            if (serviceType == typeof(IProductInfoService) ||
                serviceType == typeof(IApplicationInfoProvider) ||
                serviceType == typeof(IBugstarInfoProvider))
            {
                return this;
            }

            Debug.Assert(false, "Called for service not present", "{0}\n", serviceType.Name);
            return null;
        }

        /// <summary>
        /// Gets the service object of the specified type.
        /// </summary>
        /// <typeparam name="T">
        /// The type of service object to get.
        /// </typeparam>
        /// <returns>
        /// A service object of the specified type or null if there is no service object of
        /// that type.
        /// </returns>
        public T GetService<T>() where T : class
        {
            return this.GetService(typeof(T)) as T;
        }

        /// <summary>
        /// Handles the specified windows being closed. This is called after the method
        /// <see cref="CanCloseWithoutInteraction"/> returns false.
        /// </summary>
        /// <param name="windows">
        /// The windows to handle.
        /// </param>
        /// <param name="e">
        /// A instance of CancelEventArgs that can be used to cancel the closing of the
        /// specified windows.
        /// </param>
        public virtual void HandleWindowsClosing(ISet<Window> windows, CancelEventArgs e)
        {
            if (e.Cancel)
            {
                return;
            }
        }

        /// <summary>
        /// Registers a new Mru list for this application that can optionally be shown to the
        /// user inside the file menu.
        /// </summary>
        /// <param name="name">
        /// The name of the list. This has to be unique as it is used inside a dictionary as
        /// well as inside the registry.
        /// </param>
        /// <param name="showInFileMenu">
        /// A value indicating whether the list is displayed to the user inside the file menu.
        /// </param>
        /// <param name="command">
        /// The command that is fired if the user selects one of the values in the list. This
        /// can be null if and only if <paramref name="showInFileMenu"/> parameter is false.
        /// </param>
        /// <param name="menuId">
        /// The command id to use for the menu item.
        /// </param>
        /// <param name="listId">
        /// The command id to use for the actual list instance.
        /// </param>
        /// <param name="clearId">
        /// The command id to use for the clear menu item.
        /// </param>
        public void RegisterMruList(
            string name,
            bool showInFileMenu,
            RockstarRoutedCommand command,
            Guid menuId,
            Guid listId,
            Guid clearId)
        {
            if (name == null || this._registeredMruLists.ContainsKey(name))
            {
                string msg = "Unable to register a Mru list with a non-unique name";
                Debug.Assert(!this._registeredMruLists.ContainsKey(name), msg);
                return;
            }

            MruListDefinition definition = new MruListDefinition(name, command, 8);
            this._registeredMruLists.Add(name, definition);
            if (!showInFileMenu)
            {
                return;
            }

            ushort priority = ushort.MaxValue - 2;
            bool startsGroup = true;
            if (this._registeredMruLists.Count > 1)
            {
                priority = ushort.MaxValue - 1;
                startsGroup = false;
            }

            MruListMenu menu = new MruListMenu(priority, name, menuId, startsGroup);
            RockstarCommandManager.AddCommandBarItem(menu);

            RockstarCommandManager.AddCommandDefinition(definition);
            RockstarCommandManager.AddCommandInstance(
                command,
                ushort.MinValue,
                false,
                name + " " + StringTable.MruListText,
                listId,
                menuId,
                new CommandInstanceCreationArgs()
                {
                    AreMultiItemsShown = true
                });

            RockstarRoutedCommand cmd =
                new RockstarRoutedCommand(
                    "Clear Recent " + name + " List",
                    typeof(RsApplication),
                    null,
                    "File",
                    null,
                    CommonIcons.Clear);

            RockstarCommandManager.AddCommandDefinition(new ClearMruListDefinition(cmd, name));
            RockstarCommandManager.AddCommandInstance(
                cmd,
                ushort.MaxValue,
                true,
                StringTable.ClearMruListText.FormatCurrent(name),
                clearId,
                menuId);

            RockstarCommandManager.AddBinding<string>(
                typeof(UIElement),
                cmd,
                this.OnClearFileMruList,
                this.CanClearFileMruList);
        }

        /// <summary>
        /// Saves the editor settings from the registry.
        /// </summary>
        public void SaveEditorValues()
        {
            string path = GetEditorSettingsPath(true);
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            IFormatProvider provider = CultureInfo.InvariantCulture;
            using (XmlWriter writer = XmlWriter.Create(path, settings))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Settings");
                writer.WriteElementString("Theme", this.CurrentTheme.ToString() ?? "Light");

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }

        /// <summary>
        /// Sets the theme to the theme with the specified name.
        /// </summary>
        /// <param name="theme">
        /// The theme you wish to set.
        /// </param>
        /// <returns>
        /// True if the theme was changed successfully; otherwise, false.
        /// </returns>
        public bool SetTheme(ApplicationTheme theme)
        {
            string sourcePath;
            if (!_availableThemes.TryGetValue(theme, out sourcePath))
            {
                Debug.Assert(
                    _availableThemes.ContainsKey(theme), "Unable to find the given theme");
                return false;
            }

            Uri source = new Uri(sourcePath, System.UriKind.RelativeOrAbsolute);
            if (this.SetTheme(source))
            {
                this._currentTheme = theme;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Gets the full path to the xml file containing the settings for the editor as well
        /// as making sure the file is writable if need be.
        /// </summary>
        /// <param name="writable">
        /// A value indicating whether the settings file should also be checked to make sure it
        /// has the current readonly property on it.
        /// </param>
        /// <returns>
        /// The full path to the editor settings xml file.
        /// </returns>
        internal static string GetEditorSettingsPath(bool writable)
        {
            Environment.SpecialFolder folder = Environment.SpecialFolder.LocalApplicationData;
            string appData = Environment.GetFolderPath(folder);
            string persistentDirectory = Path.Combine(appData, "Rockstar Games", "Editor");
            if (!Directory.Exists(persistentDirectory))
            {
                Directory.CreateDirectory(persistentDirectory);
            }

            string path = Path.Combine(persistentDirectory, "settings.xml");
            if (!writable)
            {
                return path;
            }

            FileInfo fileInfo = new FileInfo(path);
            if (fileInfo.Exists && fileInfo.IsReadOnly)
            {
                fileInfo.IsReadOnly = false;
            }

            return path;
        }

        /// <summary>
        /// Called when the application is activated from an external source.
        /// </summary>
        internal void ActivationCallback()
        {
            if (this.MainWindow == null)
            {
                return;
            }

            if (this.MainWindow.WindowState == WindowState.Minimized)
            {
                this.MainWindow.WindowState = WindowState.Normal;
            }

            this.MainWindow.Activate();
        }

        /// <summary>
        /// Forces a "clean" exit of the application.
        /// </summary>
        internal protected void ForceExit()
        {
            if (this._exiting)
            {
                return;
            }

            this.HandleOnExiting();

            this._exiting = true;
            switch (this.ShutdownMode)
            {
                case ShutdownMode.OnMainWindowClose:
                    Window mainWindow = this.MainWindow;
                    if (mainWindow != null)
                    {
                        mainWindow.Close();
                    }

                    break;

                case ShutdownMode.OnLastWindowClose:
                    ISet<Window> windows = new HashSet<Window>(this.Windows.OfType<Window>());
                    foreach (Window window in windows)
                    {
                        window.Close();
                    }

                    break;

                case ShutdownMode.OnExplicitShutdown:
                    this.Shutdown();
                    break;
            }

            this._exiting = false;
        }

        /// <summary>
        /// Override to handle the command line arguments passed into the application from an
        /// external source after start-up.
        /// </summary>
        /// <param name="args">
        /// A list of command line arguments.
        /// </param>
        /// <returns>
        /// True if the command line arguments have been handled correctly; otherwise, false.
        /// </returns>
        internal bool HandleExternalArguments(string[] args)
        {
            return this.HandleExternalArgumentsCore(args ?? new string[] { });
        }

        /// <summary>
        /// Performs all tasks related to the application being shutdown.
        /// </summary>
        internal void HandleOnExiting()
        {
            if (!Directory.Exists(this.ApplicationSettingsDirectory))
            {
                try
                {
                    Directory.CreateDirectory(this.ApplicationSettingsDirectory);
                }
                catch (Exception ex)
                {
                    Debug.Fail("Unable to create app data folder");
                    this.Log.ToolException(
                        ex,
                        "Unable to create app data folder {0}.",
                        this.ApplicationSettingsDirectory);
                    return;
                }
            }

            this.HandleOnExiting(this.ApplicationSettingsDirectory);
        }

        /// <summary>
        /// Registers the loading of the specified user control to this application so that
        /// serialisation can take place on exit.
        /// </summary>
        /// <param name="control">
        /// The user control to register.
        /// </param>
        internal void RegisterRsUserControl(RsUserControl control)
        {
            bool loaded = control.IsLoaded;
            Debug.Assert(loaded, "Unable to register a control that hasn't been loaded.");
            if (!loaded)
            {
                return;
            }

            this._userControls.Add(new WeakReference<RsUserControl>(control));
        }

        /// <summary>
        /// Sets the command line arguments that are passed into this instance of the
        /// application on start-up.
        /// </summary>
        /// <param name="arguments">
        /// The start-up arguments that have been passed into this instance.
        /// </param>
        internal bool SetStartArguments(string[] arguments)
        {
            return this.ParseStatupArguments(arguments);
        }

        /// <summary>
        /// Called whenever the <see cref="RockstarCommands.Exit"/> command is fired.
        /// </summary>
        /// <param name="data">
        /// The execute data that has been sent with the command.
        /// </param>
        protected internal void OnExit(ExecuteCommandData data)
        {
            if (this._exiting)
            {
                return;
            }

            ISet<Window> windows = new HashSet<Window>(this.Windows.OfType<Window>());
            bool interaction = !this.CanCloseWithoutInteraction(windows);
            if (interaction)
            {
                CancelEventArgs cancelArgs = new CancelEventArgs();
                this.HandleWindowsClosing(windows, cancelArgs);
                if (cancelArgs.Cancel == true)
                {
                    return;
                }
            }

            this.ForceExit();
        }

        /// <summary>
        /// Call this method as soon as the application enters the Main method.
        /// </summary>
        /// <param name="instance">
        /// The one and only instance of the application class that called this method from
        /// inside its static main method.
        /// </param>
        /// <param name="mode">
        /// The application mode that this application has to adheres to.
        /// </param>
        protected static void OnEntry(RsApplication instance, ApplicationMode mode)
        {
            Thread currentThread = Thread.CurrentThread;
            currentThread.Name = "Main Application Entry Thread";
            if (instance == null)
            {
                return;
            }

            instance.Main(mode);
        }

        /// <summary>
        /// Override to create the main window for the application.
        /// </summary>
        /// <returns>
        /// The System.Windows.Window that will be the main window for this application.
        /// </returns>
        protected abstract Window CreateMainWindow();

        /// <summary>
        /// Override to handle the command line arguments passed into the application from an
        /// external source after start-up.
        /// </summary>
        /// <param name="args">
        /// A list of command line arguments.
        /// </param>
        /// <returns>
        /// True if the command line arguments have been handled correctly; otherwise, false.
        /// </returns>
        protected virtual bool HandleExternalArgumentsCore(string[] args)
        {
            return (args.Length <= 1);
        }

        /// <summary>
        /// Performs all tasks related to the application being shutdown.
        /// </summary>
        /// <param name="persistentDirectory">
        /// The path to the directory all persistent files should be saved.
        /// </param>
        protected virtual void HandleOnExiting(string persistentDirectory)
        {
            RsWindow mainWindow = this.MainWindow as RsWindow;
            if (mainWindow != null)
            {
                string filename = Path.Combine(persistentDirectory, "layout_and_commands.xml");
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;
                IFormatProvider provider = CultureInfo.InvariantCulture;
                try
                {
                    using (XmlWriter writer = XmlWriter.Create(filename, settings))
                    {
                        writer.WriteStartDocument();
                        writer.WriteStartElement("Data");
                        if (this.MakeLayoutPersistent)
                        {
                            writer.WriteStartElement("Layout");
                            mainWindow.SerialiseLayout(writer, provider);
                            writer.WriteEndElement();
                        }

                        writer.WriteStartElement("Commands");
                        RockstarCommandManager.SerialiseCommands(writer, provider);
                        writer.WriteEndElement();

                        writer.WriteEndElement();
                        writer.WriteEndDocument();
                    }
                }
                catch (Exception)
                {
                    Debug.Assert(
                        false,
                        "Cannot let this bubble up, usually used to catch IO exceptions.");
                }
            }

            foreach (WeakReference<RsUserControl> weakReference in this._userControls)
            {
                RsUserControl control = null;
                if (weakReference.TryGetTarget(out control) && control != null)
                {
                    try
                    {
                        control.SerialisePersistentData();
                    }
                    catch (Exception)
                    {
                        Debug.Assert(
                            false,
                            "Cannot let this bubble up, usually used to catch IO exceptions.");
                    }
                }
            }
        }

        /// <summary>
        /// Called when the application is about to exit.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.ExitEventArgs data for this event.
        /// </param>
        protected sealed override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);

#if !DEBUG
            if (this.TrackSessionStatistics && !Debugger.IsAttached)
            {
                TrackApplicationExit();
            }
#endif
        }

        /// <summary>
        /// Called immediately after the main windows Show method has been called.
        /// </summary>
        /// <param name="mainWindow">
        /// A reference to the main window that was shown.
        /// </param>
        /// <returns>
        /// A task that represents the work done by this method.
        /// </returns>
        protected async virtual Task OnMainWindowShown(Window mainWindow)
        {
            await EmptyTask.CompletedTask;
        }

        /// <summary>
        /// Sets up the unhandled exception handlers, creates and shows the main window.
        /// </summary>
        /// <param name="e">
        /// The event data.
        /// </param>
        protected override void OnStartup(StartupEventArgs e)
        {
            if (!this._applicationInitialised)
            {
                return;
            }

            this.StartupCore(e);
        }

        /// <summary>
        /// Override this method instead of the <see cref="OnStartup"/> method. This works as
        /// the core logic for the start up and is only called if there have been no problems
        /// pre start up.
        /// </summary>
        /// <param name="e">
        /// The start up event data passed from the <see cref="OnStartup"/> method.
        /// </param>
        protected virtual void StartupCore(StartupEventArgs e)
        {
#if !DEBUG
            // Only track stats for Release builds that aren't attached to the debugger.
            if (this.TrackSessionStatistics && !Debugger.IsAttached)
            {
                // Kick off a task to track the application session stat.
                Task.Factory.StartNew(() => this.TrackApplicationSession());
            }
#endif

            base.OnStartup(e);

            // Force the correct culture.
            FrameworkElement.LanguageProperty.OverrideMetadata(
                typeof(FrameworkElement),
                new FrameworkPropertyMetadata(
                    XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.Name)));

            // Restore the application's layout and command data.
            string filename = Path.Combine(this.ApplicationSettingsDirectory, "layout_and_commands.xml");
            string layoutData = null;
            if (File.Exists(filename))
            {
                try
                {
                    IFormatProvider provider = CultureInfo.InvariantCulture;
                    using (XmlReader reader = XmlReader.Create(filename))
                    {
                        while (reader.MoveToContent() != XmlNodeType.None)
                        {
                            if (!reader.IsStartElement())
                            {
                                reader.Read();
                                continue;
                            }

                            if (String.Equals("Data", reader.Name))
                            {
                                reader.Read();
                                continue;
                            }

                            if (String.Equals("Layout", reader.Name))
                            {
                                layoutData = reader.ReadOuterXml();
                                continue;
                            }

                            if (String.Equals("Commands", reader.Name))
                            {
                                RockstarCommandManager.DeserialiseCommands(reader, provider);
                                continue;
                            }

                            reader.Skip();
                        }
                    }
                }
                catch (Exception)
                {
                    // Ignore any exceptions that are thrown while parsing the layout file.
                }
            }

            Window mainWindow = this.CreateMainWindow();
            this.MainWindow = mainWindow;
            if (this.MainWindow == null)
            {
                return;
            }

            this.MainWindow.ContentRendered += this.MainWindowRendered;
            this.MainWindow.ShowActivated = true;

            if (layoutData != null)
            {
                IFormatProvider provider = CultureInfo.InvariantCulture;
                TextReader textReader = new StringReader(layoutData);
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.CloseInput = true;
                using (XmlReader reader = XmlReader.Create(textReader, settings))
                {
                    reader.Read();
                    this.MainWindow.WindowStartupLocation = WindowStartupLocation.Manual;
                    this.MainWindow.DeserialiseLayout(reader, provider);
                }
            }

            this.MainWindow.Show();
        }

        /// <summary>
        /// Gets called when an exception is fired from either the application or the
        /// dispatcher thread and it goes unhandled. By default this shows the exception
        /// window showing the thrown exception.
        /// </summary>
        /// <param name="exception">
        /// The exception that was thrown and not handled.
        /// </param>
        protected virtual void OnUnhandledException(Exception exception)
        {
            RsExceptionWindow window =
                new RsExceptionWindow(
                    exception,
                    this,
					this,
                    this.ExceptionWindowButtons);

            if (this.MainWindow.IsLoaded)
            {
                window.Owner = this.MainWindow;
            }
            else
            {
                window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                window.Owner = null;
            }

            window.ShowDialog();
        }

        /// <summary>
        /// Method for registering command line arguments. Note that arguments must be
        /// registered prior to initialising the application.
        /// </summary>
        /// <param name="option">
        /// The long option string for the command-line argument.
        /// </param>
        /// <param name="type">
        /// The flag specifying the <see cref="LongOption.ArgType"/> for the argument.
        /// </param>
        /// <param name="description">
        /// A optional description string for the command-line argument.
        /// </param>
        protected void RegisterCommandlineArg(
            string option, LongOption.ArgType type, string description)
        {
            if (this._commandLineOptions != null)
            {
                throw new NotSupportedException(
                    "It is not possible to add additional command line args once the " +
                    "application has been initialised.  Add options in either the " +
                    "application's constructor or prior to application initialisation.");
            }

            this._options.Add(new LongOption(option, type, description));
        }

        /// <summary>
        /// Determines whether the associated clear MRU list command can be executed based on
        /// the current state of the application.
        /// </summary>
        /// <param name="data">
        /// The can execute data that has been sent with the command.
        /// </param>
        /// <returns>
        /// True if the command can be executed; otherwise, false.
        /// </returns>
        private bool CanClearFileMruList(CanExecuteCommandData<string> data)
        {
            string name = data.Parameter;
            MruListDefinition definition = null;
            if (name == null || !this._registeredMruLists.TryGetValue(name, out definition))
            {
                string msg = "Unable to clear a Mru list that hasn't been registered";
                Debug.Assert(this._registeredMruLists.ContainsKey(name), msg);
                return false;
            }

            return definition.Items.Count > 0;
        }

        /// <summary>
        /// Loads the editor settings from the registry.
        /// </summary>
        private void LoadEditorValues()
        {
            string path = GetEditorSettingsPath(false);
            ApplicationTheme theme = this.DefaultTheme;
            if (File.Exists(path))
            {
                try
                {
                    using (XmlReader reader = XmlReader.Create(path))
                    {
                        while (reader.MoveToContent() != XmlNodeType.None)
                        {
                            if (!reader.IsStartElement())
                            {
                                reader.Read();
                                continue;
                            }

                            if (String.CompareOrdinal("Theme", reader.Name) == 0)
                            {
                                reader.Read();
                                if (reader.NodeType == XmlNodeType.Text)
                                {
                                    string value = reader.Value;
                                    if (!Enum.TryParse<ApplicationTheme>(value, out theme))
                                    {
                                        theme = this.DefaultTheme;
                                    }
                                }

                                continue;
                            }

                            reader.Read();
                        }
                    }
                }
                catch (Exception)
                {
                    // Ignore any exceptions that are thrown while parsing the settings file.
                }
            }

            string sourcePath;
            if (!_availableThemes.TryGetValue(theme, out sourcePath))
            {
                Debug.Assert(
                    _availableThemes.ContainsKey(theme), "Unable to find the given theme");
                return;
            }

            Uri source = new Uri(sourcePath, System.UriKind.RelativeOrAbsolute);
            if (this.SetTheme(source))
            {
                this._currentTheme = theme;
            }
        }

        /// <summary>
        /// Utility method for outputting some basic application information to the log on
        /// start up.
        /// </summary>
        private void LogApplicationInformation()
        {
            Type type = this.GetType();
            Assembly assembly = type.Assembly;

            DateTime buildTime;
            try
            {
                buildTime = AssemblyInfo.GetBuildTime(assembly, AssemblyBuildTimeZoneCode);
            }
            catch (InvalidTimeZoneException)
            {
                Debug.Assert(false, "Unable to retrieve the GMT version of the build time.");
                buildTime = AssemblyInfo.GetUtcBuildTime(assembly);
            }

            string commandline = String.Join(
                " ", ApplicationManager<RsApplication>.GetCommandLineArgs(this.UniqueName));

            this.Log.Message("Commandline {0}", commandline);
            this.Log.Message("Current Directory {0}", Directory.GetCurrentDirectory());
            this.Log.Message(AssemblyInfo.GetProductName(assembly, "Unknown Product"));
            this.Log.Message("{0} Version - {1}",
                AssemblyInfo.GetConfiguration(assembly, "Unknown"),
                AssemblyInfo.GetVersion(assembly));
            this.Log.Message("Build Time {0} (GMT)", buildTime);
            this.Log.Message(AssemblyInfo.GetEdition(assembly));
        }

        /// <summary>
        /// The main method that should be called inside the static main entry method. This
        /// method decides whether the application should continue to run or pass the command
        /// line arguments to a already running instance of the application.
        /// </summary>
        /// <param name="appMode">
        /// The application mode that this application has to adheres to.
        /// </param>
        private void Main(ApplicationMode appMode)
        {
            this._mode = appMode;
            InitialisationResult result =
                ApplicationManager<RsApplication>.InitialiseApplication(this, this.UniqueName);
			this.LogApplicationInformation();
            if (result == InitialisationResult.Success)
            {
                this._applicationInitialised = true;
                this.Run();

                ApplicationManager<RsApplication>.Cleanup();
            }
            else if (result == InitialisationResult.SetArgumentsFailed)
            {
                Environment.ExitCode = ExitCode.Failure;
            }
            else if (result == InitialisationResult.SingleInstanceRunning)
            {
                Environment.ExitCode = ExitCode.SingleInstanceRunning;
            }
            else
            {
                Debug.Fail("Unsupported Initialisation Result.");
            }

            // Last thing to do is shutdown the log factory.  No logging should be
            // performed after this point.
            this.Log.Debug("Application exited with exit code {0}", Environment.ExitCode);
            LogFactory.ApplicationShutdown();
        }

        /// <summary>
        /// Occurs when the content of the main window has been rendered.
        /// </summary>
        /// <param name="sender">
        /// A reference to the window that published the ContentRendered event.
        /// </param>
        /// <param name="e">
        /// The System.EventArgs data for the event.
        /// </param>
        private async void MainWindowRendered(object sender, EventArgs e)
        {
            await this.OnMainWindowShown(this.MainWindow);

            DateTime startTime = Process.GetCurrentProcess().StartTime;
            TimeSpan span = DateTime.Now - startTime;
            string msg = "Application took {0} milliseconds to show the main window.\n";
            CultureInfo provider = CultureInfo.InvariantCulture;
            Trace.Write(msg.FormatInvariant(span.Milliseconds.ToString(provider)));

            Window window = sender as Window;
            if (window == null)
            {
                return;
            }

            window.ContentRendered -= this.MainWindowRendered;
        }

        /// <summary>
        /// Called whenever the associated clear MRU list command is fired and needs handling
        /// at this instance level.
        /// </summary>
        /// <param name="data">
        /// The execute data that has been sent with the command.
        /// </param>
        private void OnClearFileMruList(ExecuteCommandData<string> data)
        {
            string name = data.Parameter;
            MruListDefinition definition = null;
            if (name == null || !this._registeredMruLists.TryGetValue(name, out definition))
            {
                string msg = "Unable to clear a Mru list that hasn't been registered";
                Debug.Assert(this._registeredMruLists.ContainsKey(name), msg);
                return;
            }

            definition.ClearMruList();
        }

        /// <summary>
        /// Occurs when a exception is thrown by the application but not handled in either the
        /// application or the dispatcher exception handler.
        /// </summary>
        /// <param name="s">
        /// The source of the unhandled exception.
        /// </param>
        /// <param name="e">
        /// The System.UnhandledExceptionEventArgs data for this event.
        /// </param>
        private void OnUnhandledException(object s, UnhandledExceptionEventArgs e)
        {
            Exception exception = e.ExceptionObject as Exception;
            this.OnUnhandledException(exception);
        }

        /// <summary>
        /// Occurs when a exception is thrown by a dispatcher thread but not handled.
        /// </summary>
        /// <param name="s">
        /// The dispatcher object that threw the exception.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Threading.DispatcherUnhandledExceptionEventArgs data for
        /// this event.
        /// </param>
        private void OnUnhandledException(object s, DispatcherUnhandledExceptionEventArgs e)
        {
            Exception exception = e.Exception;
            this.OnUnhandledException(exception);
            e.Handled = true;
        }

        /// <summary>
        /// Parses the start-up arguments to create the options list and trailing argument
        /// list.
        /// </summary>
        /// <param name="arguments">
        /// The arguments that are to be parsed to produce the command line options and
        /// trailing arguments.
        /// </param>
        private bool ParseStatupArguments(string[] arguments)
        {
            try
            {
                IList<LongOption> oldOptions = CommandOptions.DefaultArguments.ToList();
                oldOptions.AddRange(this._options);

                this._commandLineOptions = new Getopt(arguments, oldOptions.ToArray(), ThrowOnUnsupportedCommandLineArgs);
                this._commandOptions = new CommandOptions(arguments, this._options.ToArray(), ThrowOnUnsupportedCommandLineArgs);

                // Set up the bugstar configuration data.
                IBugstarConfig bugstarConfig =
                    ConfigFactory.CreateBugstarConfig(this.Log, this._commandOptions.Project);
                this._bugstarProjectName = bugstarConfig.ProjectName;
                this._defaultBugOwner = bugstarConfig.DefaultBugOwner;
                bugstarConfig.ApplicationComponents.TryGetValue(
                    this.ProductName, out this._bugstarComponent);

                // Set up the statistics configuration.
                StatisticsConfig statisticsConfig = null;
                try
                {
                    statisticsConfig = new StatisticsConfig(this._commandOptions.Config);
                }
                catch (Exception ex)
                {
                    // The statistic server cannot be initialised, in this case we log a warning
                    // and carry on. Just because the statistics config cannot be created
                    // shouldn't stop the application from starting.
                    this.Log.ToolException(ex, StringTable.StatisticsConfigFailure);
                }

                if (statisticsConfig != null)
                {
                    IServer serverConfig = statisticsConfig.DefaultServer;
                    if (this._commandOptions.ContainsOption("statistics-server"))
                    {
                        string serverName = (string)this._commandOptions["statistics-server"];
                        serverConfig = statisticsConfig.GetServerByName(serverName);
                        if (serverConfig == null)
                        {
                            this.Log.Warning(StringTable.ServerNotFoundWarning, serverName);
                            serverConfig = statisticsConfig.DefaultServer;
                        }
                    }

                    this._statisticsServer = serverConfig;
                }

                return true;
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Exception caught before startup.");

                string msg = StringTable.ApplicationStartupFailed;
                RsMessageBox.Show(msg);
                return false;
            }
        }

        /// <summary>
        /// Sets the current theme to a resource dictionary that uses the specified uri object
        /// as its source.
        /// </summary>
        /// <param name="source">
        /// The uniform resource identifier to load the theme file from.
        /// </param>
        /// <returns>
        /// True if the theme was changed successfully; otherwise, false.
        /// </returns>
        private bool SetTheme(Uri source)
        {
            if (this._currentThemeDictionary != null)
            {
                if (this._currentThemeDictionary.Source == source)
                {
                    return true;
                }
            }

            Collection<ResourceDictionary> resources = this.Resources.MergedDictionaries;
            if (resources == null)
            {
                return false;
            }

            ResourceDictionary newDictionary = new ResourceDictionary();
            try
            {
                newDictionary.Source = source;
            }
            catch (Exception)
            {
                return false;
            }

            ThemeHelper.ClearCache();

            resources.Add(newDictionary);
            if (this._currentThemeDictionary != null)
            {
                int i = 0;
                for (i = 0; i < resources.Count; i++)
                {
                    ResourceDictionary dictionary = resources[i];
                    if (dictionary.Source != this._currentThemeDictionary.Source)
                    {
                        continue;
                    }

                    break;
                }

                Debug.Assert(i != resources.Count, "Unable to remove the previous theme.");
                resources.RemoveAt(i);
            }

            this._currentThemeDictionary = newDictionary;
            return true;
        }

        /// <summary>
        /// Sends the application session information to the statistics server.
        /// </summary>
        private void TrackApplicationSession()
        {
            if (this._statisticsServer == null)
            {
                // The statistics config failed to initialise and a tool exception message has
                // already been logged.
                return;
            }

            this.Log.Message(
                "Tracking application session {0}.", this._applicationSessionGuid);
            try
            {
                Process currentProcess = Process.GetCurrentProcess();
                ProcessModule mainModule = currentProcess.MainModule;

                ApplicationSessionConsumer consumer =
                    new ApplicationSessionConsumer(this._statisticsServer);
                consumer.TrackApplicationSession(
                    this._applicationSessionGuid,
                    null,
                    this.ProductName,
                    this.Version,
                    mainModule.FileName,
                    currentProcess.StartTime.ToUniversalTime(),
                    CommandOptions.Project);
                this._applicationSessionTracked = true;
            }
            catch (Exception e)
            {
                this._applicationSessionTracked = false;
                this.Log.ToolException(
                    e,
                    "Unable to track application session stat due to an unhandled exception.");
            }
        }

        /// <summary>
        /// Sends the application session exit information to the statistics server.
        /// </summary>
        private void TrackApplicationExit()
        {
            if (!this._applicationSessionTracked)
            {
                // We didn't track the startup, so don't bother tracking the exit.
                return;
            }

            this.Log.Message("Tracking application session exit info.");
            try
            {
                ApplicationSessionConsumer consumer =
                    new ApplicationSessionConsumer(this._statisticsServer);
                consumer.TrackApplicationExit(
                    this._applicationSessionGuid,
                    Environment.ExitCode,
                    DateTime.UtcNow);
            }
            catch (Exception e)
            {
                this.Log.ToolException(
                    e,
                    "Unable to track application exit stat due to an unhandled exception.");
            }
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// The definition for a custom command used to clear a specific MRU list.
        /// </summary>
        private sealed class ClearMruListDefinition : ButtonCommand
        {
            #region Fields
            /// <summary>
            /// The private field used for the <see cref="CommandParameter"/> property.
            /// </summary>
            private string _commandParameter;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="ClearMruListDefinition"/> class.
            /// </summary>
            /// <param name="command">
            /// The routed command this definition is wrapping.
            /// </param>
            /// <param name="parameter">
            /// The command parameter to use for this definition.
            /// </param>
            public ClearMruListDefinition(RockstarRoutedCommand command, string parameter)
                : base(command)
            {
                if (command == null)
                {
                    throw new SmartArgumentNullException(() => command);
                }

                this._commandParameter = parameter;
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets the parameter that is routed through the element tree with the command.
            /// </summary>
            public override object CommandParameter
            {
                get { return this._commandParameter; }
            }
            #endregion Properties
        } // RsApplication.ClearMruListDefinition {Class}

        /// <summary>
        /// Represents the common dialog service this application uses.
        /// </summary>
        private sealed class CommonDialogService : ICommonDialogService
        {
            #region Fields
            /// <summary>
            /// Reference to the product info service.
            /// </summary>
            private readonly IProductInfoService _productInfoService;
            #endregion

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="CommonDialogService"/> class
            /// using the specified product info service.
            /// </summary>
            /// <param name="productInfoService"></param>
            public CommonDialogService(IProductInfoService productInfoService)
            {
                _productInfoService = productInfoService;
            }
            #endregion

            #region Methods
            /// <summary>
            /// Handles the situation of trying to save to a read-only file.
            /// </summary>
            /// <param name="fullPath">
            /// The full path to the file being saved.
            /// </param>
            /// <param name="cancelled">
            /// A value that is set to true if during the handling the user chooses to cancel
            /// the operation.
            /// </param>
            /// <returns>
            /// True if the specified full path is need writable; otherwise, false.
            /// </returns>
            public bool HandleSavingOfReadOnlyFile(ref string fullPath, ref bool cancelled)
            {
                IMessageBoxService messageService = new MessageBoxService();

                string filename = Path.GetFileName(fullPath);
                FileInfo fileInfo = new FileInfo(fullPath);
                if (fileInfo.Exists && fileInfo.IsReadOnly)
                {
                    IMessageBox messageBox = messageService.CreateMessageBox();
                    messageBox.SetIconToExclamation();
                    messageBox.Caption = StringTable.WriteProtectedCaption;
                    messageBox.Text = StringTable.WriteProtectedMsg.FormatCurrent(
                        filename, this._productInfoService.ProductName);
                    messageBox.AddButton(StringTable.SaveAsButtonText, 1, true, false);
                    messageBox.AddButton(StringTable.OverwriteButtonText, 2, false, false);
                    messageBox.AddButton(StringTable.CancelButtonText, 3, false, true);

                UserChoice:
                    switch (messageBox.ShowMessageBox())
                    {
                        case 1:
                            string directory = Path.GetDirectoryName(fullPath);
                            if (!this.ShowSaveFile(directory, filename, out fullPath))
                            {
                                cancelled = true;
                                return false;
                            }

                            break;
                        case 2:
                            try
                            {
                                fileInfo.IsReadOnly = false;
                            }
                            catch
                            {
                                messageService.Show(
                                    StringTable.OverwriteFailedMsg.FormatCurrent(filename),
                                    String.Empty,
                                    System.Windows.MessageBoxButton.OK,
                                    System.Windows.MessageBoxImage.Error);

                                goto UserChoice;
                            }

                            break;
                        case 3:
                            cancelled = true;
                            return false;
                        default:
                            Debug.Assert(false, "Unexpected result from message box");
                            cancelled = true;
                            return false;
                    }
                }

                return true;
            }

            /// <summary>
            /// Shows the open file dialog window to the user so that they can select a single
            /// file to read from.
            /// </summary>
            /// <param name="directory">
            /// The initial directory that is displayed by the file dialog.
            /// </param>
            /// <param name="filename">
            /// A string containing the initial full path of the file selected in the file
            /// dialog.
            /// </param>
            /// <param name="filter">
            /// The filter that determines what types of files are displayed in the file
            /// dialog.
            /// </param>
            /// <param name="filterIndex">
            /// The index of the filter currently selected in the file dialog.
            /// </param>
            /// <param name="location">
            /// When this method returns contains the full path to the file the user selected
            /// if valid.
            /// </param>
            /// <returns>
            ///  True if the user clicks the OK button; otherwise, false.
            /// </returns>
            public bool ShowOpenFile(
                string directory,
                string filename,
                string filter,
                int filterIndex,
                out string location)
            {
                OpenFileDialog dlg = new OpenFileDialog();
                if (directory != null)
                {
                    dlg.InitialDirectory = directory;
                }

                if (filename != null)
                {
                    dlg.FileName = filename;
                }

                if (filter != null)
                {
                    dlg.Filter = filter;
                }

                dlg.FilterIndex = filterIndex;

                bool? result = dlg.ShowDialog();
                if (result != true)
                {
                    location = null;
                }
                else
                {
                    location = dlg.FileName;
                }

                return (bool)result;
            }

            /// <summary>
            /// Shows the open file dialog window to the user so that they can select a single
            /// file to read from.
            /// </summary>
            /// <param name="directory">
            /// The initial directory that is displayed by the file dialog.
            /// </param>
            /// <param name="filename">
            /// A string containing the initial full path of the file selected in the file
            /// dialog.
            /// </param>
            /// <param name="filter">
            /// The filter that determines what types of files are displayed in the file
            /// dialog.
            /// </param>
            /// <param name="filterIndex">
            /// The index of the filter currently selected in the file dialog.
            /// </param>
            /// <param name="filepaths">
            /// When this method returns contains the full path to the files the user selected
            /// if valid.
            /// </param>
            /// <returns>
            ///  True if the user clicks the OK button; otherwise, false.
            /// </returns>
            public bool ShowOpenFile(
                string directory,
                string filename,
                string filter,
                int filterIndex,
                out string[] filepaths)
            {
                OpenFileDialog dlg = new OpenFileDialog();
                dlg.InitialDirectory = directory;
                dlg.FileName = filename;
                dlg.Filter = filter;
                dlg.FilterIndex = filterIndex;
                dlg.Multiselect = true;

                bool? result = dlg.ShowDialog();
                if (result != true)
                {
                    filepaths = null;
                }
                else
                {
                    filepaths = dlg.FileNames;
                }

                return (bool)result;
            }

            /// <summary>
            /// Shows the save file dialog window to the user so that they can select a single
            /// file to write to.
            /// </summary>
            /// <param name="directory">
            /// The initial directory that is displayed by the file dialog.
            /// </param>
            /// <param name="filename">
            /// A string containing the initial full path of the file selected in the file
            /// dialog.
            /// </param>
            /// <param name="destination">
            /// When this method returns contains the full path to the file the user selected
            /// if valid.
            /// </param>
            /// <returns>
            ///  True if the user clicks the OK button; otherwise, false.
            /// </returns>
            public bool ShowSaveFile(string directory, string filename, out string destination)
            {
                return this.ShowSaveFile(directory, filename, null, 0, out destination);
            }

            /// <summary>
            /// Shows the save file dialog window to the user to that they can select a single
            /// file to write to.
            /// </summary>
            /// <param name="directory">
            /// The initial directory that is displayed by the file dialog.
            /// </param>
            /// <param name="filename">
            /// A string containing the initial full path of the file selected in the file
            /// dialog.
            /// </param>
            /// <param name="filter">
            /// The filter that determines what types of files are displayed in the file.
            /// </param>
            /// <param name="filterIndex">
            /// The index of the filter currently selected in the file dialog.
            /// </param>
            /// <param name="destination">
            /// When this method returns contains the full path to the file the user selected
            /// if valid.
            /// </param>
            /// <returns>
            ///  True if the user clicks the OK button; otherwise, false.
            /// </returns>
            public bool ShowSaveFile(
                string directory,
                string filename,
                string filter,
                int filterIndex,
                out string destination)
            {
                SaveFileDialog dlg = new SaveFileDialog();
                dlg.InitialDirectory = directory;
                dlg.FileName = filename;
                dlg.Filter = filter;
                dlg.FilterIndex = filterIndex;

                bool? result = dlg.ShowDialog();
                if (result != true)
                {
                    destination = null;
                }
                else
                {
                    destination = dlg.FileName;
                }

                return (bool)result;
            }

            /// <summary>
            /// Shows the selected folder dialog to the user so that they can select a folder
            /// and returns the result back in the <paramref name="directory"/> parameter.
            /// </summary>
            /// <param name="id">
            /// The identifier to use to control the persistence of this dialog.
            /// </param>
            /// <param name="title">
            /// The title to show on the window.
            /// </param>
            /// <param name="directory">
            /// When this method returns contains the full path to the directory the user
            /// selected if valid.
            /// </param>
            /// <returns>
            /// True if the user selected a folder; otherwise, false.
            /// </returns>
            public bool ShowSelectFolder(Guid id, string title, out string directory)
            {
                RsSelectFolderDialog dlg = new RsSelectFolderDialog(id);
                dlg.Title = title;

                bool? result = dlg.ShowDialog();
                if (result != true)
                {
                    directory = null;
                }
                else
                {
                    directory = dlg.FileName;
                }

                return (bool)result;
            }

            /// <summary>
            /// Shows the selected folder dialog to the user so that they can select a folder
            /// and returns the result back in the <paramref name="directory"/> parameter.
            /// </summary>
            /// <param name="id">
            /// The identifier to use to control the persistence of this dialog.
            /// </param>
            /// <param name="initialDirectory">
            /// The initial directory that is displayed by the folder dialog.
            /// </param>
            /// <param name="title">
            /// The title to show on the window.
            /// </param>
            /// <param name="directory">
            /// When this method returns contains the full path to the directory the user
            /// selected if valid.
            /// </param>
            /// <returns>
            /// True if the user selected a folder; otherwise, false.
            /// </returns>
            public bool ShowSelectFolder(
                Guid id, string initialDirectory, string title, out string directory)
            {
                RsSelectFolderDialog dlg = new RsSelectFolderDialog(id);
                dlg.Title = title;
                dlg.InitialDirectory = initialDirectory;

                bool? result = dlg.ShowDialog();
                if (result != true)
                {
                    directory = null;
                }
                else
                {
                    directory = dlg.FileName;
                }

                return (bool)result;
            }

            /// <summary>
            /// Shows the selected folder dialog to the user so that they can select folders
            /// and returns the result back in the <paramref name="directories"/> parameter.
            /// </summary>
            /// <param name="id">
            /// The identifier to use to control the persistence of this dialog.
            /// </param>
            /// <param name="title">
            /// The title to show on the window.
            /// </param>
            /// <param name="directories">
            /// When this method returns contains an array of full paths to the directories the user
            /// selected if valid.
            /// </param>
            /// <returns>
            /// True if the user selected a folder; otherwise, false.
            /// </returns>
            public bool ShowMultiSelectFolder(Guid id, string title, out string[] directories)
            {
                RsSelectFolderDialog dlg = new RsSelectFolderDialog(id);
                dlg.Title = title;
                dlg.MultiSelect = true;

                bool? result = dlg.ShowDialog();
                if (result != true)
                {
                    directories = null;
                }
                else
                {
                    directories = dlg.FileNames;
                }

                return (bool)result;
            }

            /// <summary>
            /// Shows the selected folder dialog to the user so that they can select folders
            /// and returns the result back in the <paramref name="directories"/> parameter.
            /// </summary>
            /// <param name="id">
            /// The identifier to use to control the persistence of this dialog.
            /// </param>
            /// <param name="initialDirectory">
            /// The initial directory that is displayed by the folder dialog.
            /// </param>
            /// <param name="title">
            /// The title to show on the window.
            /// </param>
            /// <param name="directories">
            /// When this method returns contains an array of full paths to the directories the user
            /// selected if valid.
            /// </param>
            /// <returns>
            /// True if the user selected a folder; otherwise, false.
            /// </returns>
            public bool ShowMultiSelectFolder(
                Guid id, string initialDirectory, string title, out string[] directories)
            {
                RsSelectFolderDialog dlg = new RsSelectFolderDialog(id);
                dlg.Title = title;
                dlg.InitialDirectory = initialDirectory;
                dlg.MultiSelect = true;

                bool? result = dlg.ShowDialog();
                if (result != true)
                {
                    directories = null;
                }
                else
                {
                    directories = dlg.FileNames;
                }

                return (bool)result;
            }
            #endregion Methods
        } // RsApplication.CommonDialogService {Class}

        /// <summary>
        /// Represents the message box service this application uses.
        /// </summary>
        private sealed class MessageBoxService : IMessageBoxService
        {
            #region Methods
            /// <summary>
            /// Creates a new message box that can be modified and shown to the user.
            /// </summary>
            /// <returns>
            /// A newly created message box object.
            /// </returns>
            public IMessageBox CreateMessageBox()
            {
                Window owner = null;
                if (Application.Current != null)
                {
                    owner = Application.Current.MainWindow;
                }

                RsMessageBox messageBox = new RsMessageBox(owner);
                return messageBox as IMessageBox;
            }

            /// <summary>
            /// Displays a message box that has a message and that returns a result.
            /// </summary>
            /// <param name="messageBoxText">
            /// A System.String that specifies the text to display.
            /// </param>
            /// <returns>
            /// A System.Windows.MessageBoxResult value that specifies which message box button
            /// is clicked by the user.
            /// </returns>
            public MessageBoxResult Show(string messageBoxText)
            {
                Window owner = null;
                if (Application.Current != null)
                {
                    owner = Application.Current.MainWindow;
                }

                return RsMessageBox.Show(owner, messageBoxText);
            }

            /// <summary>
            /// Displays a message box in front of the specified window that has a message and
            /// that returns a result.
            /// </summary>
            /// <param name="owner">
            /// A System.Windows.Window that represents the owner window of the message box.
            /// </param>
            /// <param name="messageBoxText">
            /// A System.String that specifies the text to display.
            /// </param>
            /// <returns>
            /// A System.Windows.MessageBoxResult value that specifies which message box button
            /// is clicked by the user.
            /// </returns>
            public MessageBoxResult Show(Window owner, string messageBoxText)
            {
                return RsMessageBox.Show(owner, messageBoxText);
            }

            /// <summary>
            /// Displays a message box that has a message and title bar caption; and that
            /// returns a result.
            /// </summary>
            /// <param name="messageBoxText">
            /// A System.String that specifies the text to display.
            /// </param>
            /// <param name="caption">
            /// A System.String that specifies the title bar caption to display.
            /// </param>
            /// <returns>
            /// A System.Windows.MessageBoxResult value that specifies which message box button
            /// is clicked by the user.
            /// </returns>
            public MessageBoxResult Show(string messageBoxText, string caption)
            {
                Window owner = null;
                if (Application.Current != null)
                {
                    owner = Application.Current.MainWindow;
                }

                return RsMessageBox.Show(owner, messageBoxText, caption);
            }

            /// <summary>
            /// Displays a message box in front of the specified window that has a message and
            /// title bar caption; and that returns a result.
            /// </summary>
            /// <param name="owner">
            /// A System.Windows.Window that represents the owner window of the message box.
            /// </param>
            /// <param name="messageBoxText">
            /// A System.String that specifies the text to display.
            /// </param>
            /// <param name="caption">
            /// A System.String that specifies the title bar caption to display.
            /// </param>
            /// <returns>
            /// A System.Windows.MessageBoxResult value that specifies which message box button
            /// is clicked by the user.
            /// </returns>
            public MessageBoxResult Show(Window owner, string messageBoxText, string caption)
            {
                return RsMessageBox.Show(owner, messageBoxText, caption);
            }

            /// <summary>
            /// Displays a message box that has a message, title bar caption, and button; and
            /// that returns a result.
            /// </summary>
            /// <param name="messageBoxText">
            /// A System.String that specifies the text to display.
            /// </param>
            /// <param name="caption">
            /// A System.String that specifies the title bar caption to display.
            /// </param>
            /// <param name="button">
            /// A System.Windows.MessageBoxButton value that specifies which button or buttons
            /// to display.
            /// </param>
            /// <returns>
            /// A System.Windows.MessageBoxResult value that specifies which message box button
            /// is clicked by the user.
            /// </returns>
            public MessageBoxResult Show(
                string messageBoxText, string caption, MessageBoxButton button)
            {
                Window owner = null;
                if (Application.Current != null)
                {
                    owner = Application.Current.MainWindow;
                }

                return RsMessageBox.Show(owner, messageBoxText, caption, button);
            }

            /// <summary>
            /// Displays a message box in front of the specified window that has a message,
            /// title bar caption, and button; and that returns a result.
            /// </summary>
            /// <param name="owner">
            /// A System.Windows.Window that represents the owner window of the message box.
            /// </param>
            /// <param name="messageBoxText">
            /// A System.String that specifies the text to display.
            /// </param>
            /// <param name="caption">
            /// A System.String that specifies the title bar caption to display.
            /// </param>
            /// <param name="button">
            /// A System.Windows.MessageBoxButton value that specifies which button or buttons
            /// to display.
            /// </param>
            /// <returns>
            /// A System.Windows.MessageBoxResult value that specifies which message box button
            /// is clicked by the user.
            /// </returns>
            public MessageBoxResult Show(
                Window owner, string messageBoxText, string caption, MessageBoxButton button)
            {
                return RsMessageBox.Show(owner, messageBoxText, caption, button);
            }

            /// <summary>
            /// Displays a message box that has a message, title bar caption, button, and icon;
            /// and that returns a result.
            /// </summary>
            /// <param name="messageBoxText">
            /// A System.String that specifies the text to display.
            /// </param>
            /// <param name="caption">
            /// A System.String that specifies the title bar caption to display.
            /// </param>
            /// <param name="button">
            /// A System.Windows.MessageBoxButton value that specifies which button or buttons
            /// to  display.
            /// </param>
            /// <param name="icon">
            /// A System.Windows.MessageBoxImage value that specifies the icon to display.
            /// </param>
            /// <returns>
            /// A System.Windows.MessageBoxResult value that specifies which message box button
            /// is clicked by the user.
            /// </returns>
            public MessageBoxResult Show(
                string messageBoxText,
                string caption,
                MessageBoxButton button,
                MessageBoxImage icon)
            {
                Window owner = null;
                if (Application.Current != null)
                {
                    owner = Application.Current.MainWindow;
                }

                return RsMessageBox.Show(owner, messageBoxText, caption, button, icon);
            }

            /// <summary>
            /// Displays a message box in front of the specified window that has a message,
            /// title bar caption, button, and icon; and that returns a result.
            /// </summary>
            /// <param name="owner">
            /// A System.Windows.Window that represents the owner window of the message box.
            /// </param>
            /// <param name="messageBoxText">
            /// A System.String that specifies the text to display.
            /// </param>
            /// <param name="caption">
            /// A System.String that specifies the title bar caption to display.
            /// </param>
            /// <param name="button">
            /// A System.Windows.MessageBoxButton value that specifies which button or buttons
            /// to display.
            /// </param>
            /// <param name="icon">
            /// A System.Windows.MessageBoxImage value that specifies the icon to display.
            /// </param>
            /// <returns>
            /// A System.Windows.MessageBoxResult value that specifies which message box button
            /// is clicked by the user.
            /// </returns>
            public MessageBoxResult Show(
                Window owner,
                string messageBoxText,
                string caption,
                MessageBoxButton button,
                MessageBoxImage icon)
            {
                return RsMessageBox.Show(owner, messageBoxText, caption, button, icon);
            }

            /// <summary>
            /// Displays a message box that has a message, title bar caption, button, and icon;
            /// and that returns a result.
            /// </summary>
            /// <param name="messageBoxText">
            /// A System.String that specifies the text to display.
            /// </param>
            /// <param name="caption">
            /// A System.String that specifies the title bar caption to display.
            /// </param>
            /// <param name="button">
            /// A System.Windows.MessageBoxButton value that specifies which button or buttons
            /// to  display.
            /// </param>
            /// <param name="icon">
            /// A System.Windows.MessageBoxImage value that specifies the icon to display.
            /// </param>
            /// <param name="defaultResult">
            /// A System.Windows.MessageBoxResult value that specifies the default result of
            /// the message box.
            /// </param>
            /// <returns>
            /// A System.Windows.MessageBoxResult value that specifies which message box button
            /// is clicked by the user.
            /// </returns>
            public MessageBoxResult Show(
                string messageBoxText,
                string caption,
                MessageBoxButton button,
                MessageBoxImage icon,
                MessageBoxResult defaultResult)
            {
                Window owner = null;
                if (Application.Current != null)
                {
                    owner = Application.Current.MainWindow;
                }

                return RsMessageBox.Show(
                    owner, messageBoxText, caption, button, icon, defaultResult);
            }

            /// <summary>
            /// Displays a message box in front of the specified window that has a message,
            /// title bar caption, button, and icon; and that returns a result.
            /// </summary>
            /// <param name="owner">
            /// A System.Windows.Window that represents the owner window of the message box.
            /// </param>
            /// <param name="messageBoxText">
            /// A System.String that specifies the text to display.
            /// </param>
            /// <param name="caption">
            /// A System.String that specifies the title bar caption to display.
            /// </param>
            /// <param name="button">
            /// A System.Windows.MessageBoxButton value that specifies which button or buttons
            /// to display.
            /// </param>
            /// <param name="icon">
            /// A System.Windows.MessageBoxImage value that specifies the icon to display.
            /// </param>
            /// <param name="defaultResult">
            /// A System.Windows.MessageBoxResult value that specifies the default result of
            /// the message box.
            /// </param>
            /// <returns>
            /// A System.Windows.MessageBoxResult value that specifies which message box button
            /// is clicked by the user.
            /// </returns>
            public MessageBoxResult Show(
                Window owner,
                string messageBoxText,
                string caption,
                MessageBoxButton button,
                MessageBoxImage icon,
                MessageBoxResult defaultResult)
            {
                return RsMessageBox.Show(
                    owner, messageBoxText, caption, button, icon, defaultResult);
            }

            /// <summary>
            /// Shows a standard error message box that has a message, and title. This message
            /// box displays an okay button and the error icon.
            /// </summary>
            /// <param name="messageBoxText">
            /// A System.String that specifies the text to display.
            /// </param>
            /// <param name="caption">
            /// A System.String that specifies the title bar caption to display.
            /// </param>
            public void ShowStandardErrorBox(string messageBoxText, string caption)
            {
                this.Show(messageBoxText, caption, MessageBoxButton.OK, MessageBoxImage.Error);
            }
            #endregion Methods
        } // RsApplication.MessageBoxService {Class}

        /// <summary>
        /// The definition wrapper for a multi command inside a Mru list.
        /// </summary>
        private class MruListDefinition : MultiCommand
        {
            #region Fields
            /// <summary>
            /// The private field used for the <see cref="Command"/> property.
            /// </summary>
            private RockstarRoutedCommand _command;

            /// <summary>
            /// Flag indicating whether we experienced some IO issues when loading the file
            /// from disk.
            /// </summary>
            private bool _ioExceptionThrownOnLoad = false;

            /// <summary>
            /// The private field used for the <see cref="ListName"/> property.
            /// </summary>
            private string _listName;

            /// <summary>
            /// The maximum length this list can be.
            /// </summary>
            private int _maxLength;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="MruListDefinition"/> class.
            /// </summary>
            /// <param name="listName">
            /// The name of the Mru list.
            /// </param>
            /// <param name="command">
            /// The command that is fired when any of the items inside this multi command are
            /// executed.
            /// </param>
            /// <param name="maxLength">
            /// The maximum length this Mru list can be.
            /// </param>
            public MruListDefinition(
                string listName, RockstarRoutedCommand command, int maxLength)
            {
                this._command = command;
                this._listName = listName;
                this._maxLength = maxLength;
                string path = this.GetSettingsPath(false);
                if (!File.Exists(path))
                {
                    return;
                }

                try
                {
                    using (XmlReader reader = XmlReader.Create(path))
                    {
                        while (reader.MoveToContent() != XmlNodeType.None)
                        {
                            if (!reader.IsStartElement())
                            {
                                reader.Read();
                                continue;
                            }

                            if (String.CompareOrdinal("Items", reader.Name) == 0)
                            {
                                reader.Read();
                                continue;
                            }

                            if (String.CompareOrdinal("Item", reader.Name) != 0)
                            {
                                reader.Skip();
                                continue;
                            }

                            Tuple<string, string[]> parameter = this.CreateParameter(reader);
                            this.Items.Add(new RecentListItem(this, parameter));
                        }
                    }
                }
                catch (IOException ex)
                {
                    string msg = ErrorStringTable.GetString("LoadingMruListError", path);
                    Debug.Assert(false, msg, ex.Message);
                    this._ioExceptionThrownOnLoad = true;
                }
                catch (Exception ex)
                {
                    string msg = ErrorStringTable.GetString("LoadingMruListError", path);
                    Debug.Assert(false, msg, ex.Message);
                }
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets the System.Windows.Input.RoutedCommand that this definition is wrapping.
            /// </summary>
            public override RockstarRoutedCommand Command
            {
                get { return this._command; }
            }

            /// <summary>
            /// Gets the name that has been assigned to this Mru list and which is used inside
            /// the registry and on the user interface.
            /// </summary>
            public string ListName
            {
                get { return this._listName; }
            }

            /// <summary>
            /// Gets the name of this command definition.
            /// </summary>
            public override string Name
            {
                get { return "Recent " + this._listName + " List"; }
            }
            #endregion Properties

            #region Methods
            /// <summary>
            /// Adds the specified recent file to the list at index 0.
            /// </summary>
            /// <param name="value">
            /// The parameter of the item to add to the specified Mru list as well as the text
            /// to show the user in the user interface.
            /// </param>
            /// <param name="additionalData">
            /// An array of additional data that is sent with the fired command when the item
            /// is executed.
            /// </param>
            public void AddItemToList(string value, string[] additionalData)
            {
                IMultiCommandItem existing = this.Items.FirstOrDefault(i => HasText(i, value));
                while (existing != null)
                {
                    this.Items.Remove(existing);
                    existing = this.Items.FirstOrDefault(i => HasText(i, value));
                }

                if (additionalData == null)
                {
                    additionalData = new string[0];
                }

                var parameter = new Tuple<string, string[]>(value, additionalData);
                this.Items.Insert(0, new RecentListItem(this, parameter));
                while (this.Items.Count > this._maxLength)
                {
                    this.Items.RemoveAt(this._maxLength);
                }

                this.SaveListValues();
            }

            /// <summary>
            /// Clears the list of recently opened files.
            /// </summary>
            public void ClearMruList()
            {
                this.Items.Clear();
                this.SaveListValues();
            }

            /// <summary>
            /// Determines whether the specified item has the given text.
            /// </summary>
            /// <param name="item">
            /// The item to test.
            /// </param>
            /// <param name="text">
            /// The text that should be checked against the specified item.
            /// </param>
            /// <returns>
            /// True if the specified items text property is equal to the specified text;
            /// otherwise, false.
            /// </returns>
            private static bool HasText(IMultiCommandItem item, string text)
            {
                StringComparison comparison = StringComparison.InvariantCultureIgnoreCase;

                string itemText = item.Text;
                int index = itemText.IndexOf("_");
                if (index != -1)
                {
                    itemText = itemText.Remove(index, 1);
                }

                return String.Equals(itemText, text, comparison);
            }

            /// <summary>
            /// Creates the valid parameter of a tuple that includes the main parameter and any
            /// additional data by parsing data inside the specified XmlReader.
            /// </summary>
            /// <param name="reader">
            /// The XmlReader containing the data to parse to create the valid parameter.
            /// </param>
            /// <returns>
            /// The valid parameter that represents the data contained within the specified
            /// string.
            /// </returns>
            private Tuple<string, string[]> CreateParameter(XmlReader reader)
            {
                string path = reader.GetAttribute("path");
                List<string> additionalData = new List<string>();
                if (!reader.IsEmptyElement)
                {
                    reader.ReadStartElement();
                    while (reader.MoveToContent() == XmlNodeType.Element)
                    {
                        if (!reader.IsStartElement())
                        {
                            reader.Read();
                            continue;
                        }

                        if (String.CompareOrdinal("Addition", reader.Name) != 0)
                        {
                            reader.Skip();
                            continue;
                        }

                        if (reader.IsEmptyElement)
                        {
                            reader.Skip();
                            continue;
                        }

                        reader.ReadStartElement();
                        if (reader.NodeType == XmlNodeType.Text)
                        {
                            additionalData.Add(reader.Value);
                            reader.Read();
                        }

                        reader.ReadEndElement();
                    }

                    reader.ReadEndElement();
                }
                else
                {
                    reader.Skip();
                }

                return new Tuple<string, string[]>(path, additionalData.ToArray());
            }

            /// <summary>
            /// Gets the full path to the xml file containing the settings for the editor as
            /// well as making sure the file is writable if need be.
            /// </summary>
            /// <param name="writable">
            /// A value indicating whether the settings file should also be checked to make
            /// sure it has the current readonly property on it.
            /// </param>
            /// <returns>
            /// The full path to the editor settings xml file.
            /// </returns>
            private string GetSettingsPath(bool writable)
            {
                Environment.SpecialFolder folder =
                    Environment.SpecialFolder.LocalApplicationData;
                string appData = Environment.GetFolderPath(folder);
                string persistentDirectory = Path.Combine(appData, "Rockstar Games");
                RsApplication app = Application.Current as RsApplication;
                persistentDirectory = Path.Combine(persistentDirectory, app.UniqueName);
                persistentDirectory = Path.Combine(persistentDirectory, "Mru Lists");
                if (!Directory.Exists(persistentDirectory))
                {
                    Directory.CreateDirectory(persistentDirectory);
                }

                string path = Path.Combine(persistentDirectory, this._listName + ".xml");
                if (!writable)
                {
                    return path;
                }

                FileInfo fileInfo = new FileInfo(path);
                if (fileInfo.Exists && fileInfo.IsReadOnly)
                {
                    fileInfo.IsReadOnly = false;
                }

                return path;
            }

            /// <summary>
            /// Saves the current list to the registry.
            /// </summary>
            private void SaveListValues()
            {
                // Don't save out the file if we experienced a problem loading it up.
                if (this._ioExceptionThrownOnLoad)
                {
                    return;
                }

                string path = this.GetSettingsPath(true);
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;
                IFormatProvider provider = CultureInfo.InvariantCulture;
                try
                {
                    using (XmlWriter writer = XmlWriter.Create(path, settings))
                    {
                        writer.WriteStartDocument();
                        writer.WriteStartElement("Items");
                        for (int i = 0; i < this.Items.Count; i++)
                        {
                            var item =
                                this.Items[i] as MultiCommandItem<Tuple<string, string[]>>;
                            if (item == null)
                            {
                                continue;
                            }

                            Tuple<string, string[]> parameter = item.CommandParameter;
                            if (parameter == null)
                            {
                                continue;
                            }

                            writer.WriteStartElement("Item");
                            writer.WriteAttributeString("path", parameter.Item1);
                            foreach (string additional in parameter.Item2)
                            {
                                writer.WriteStartElement("Addition");
                                writer.WriteString(additional);
                                writer.WriteEndElement();
                            }

                            writer.WriteEndElement();
                        }

                        writer.WriteEndElement();
                        writer.WriteEndDocument();
                    }
                }
                catch (Exception ex)
                {
                    string msg = ErrorStringTable.GetString("SavingMruListError", ex.Message);
                    Debug.Assert(false, msg, ex.Message);
                }
            }
            #endregion Methods

            #region Classes
            /// <summary>
            /// Defines the item published for the items of the <see cref="MruListDefinition"/>
            /// command definition.
            /// </summary>
            private class RecentListItem : MultiCommandItem<Tuple<string, string[]>>
            {
                #region Constructors
                /// <summary>
                /// Initialises a new instance of the <see cref="RecentListItem"/> class.
                /// </summary>
                /// <param name="definition">
                /// The multi command definition that owns this item.
                /// </param>
                /// <param name="parameter">
                /// The parameter that is sent when this item is executed.
                /// </param>
                public RecentListItem(
                    MultiCommand definition, Tuple<string, string[]> parameter)
                    : base(definition, RecentListItem.GetText(parameter.Item1), parameter)
                {
                }
                #endregion Constructors

                #region Properties
                /// <summary>
                /// Gets the description for this item. This is also what is shown as a tooltip
                /// along with the key gesture if one is set if it is being rendered as a
                /// toggle control.
                /// </summary>
                public override string Description
                {
                    get { return null; }
                }
                #endregion Properties

                #region Methods
                /// <summary>
                /// Creates the text property for this item, making sure the first '_'
                /// character is handled correctly.
                /// </summary>
                /// <param name="rawString">
                /// The raw text for this item.
                /// </param>
                /// <returns>
                /// The text to use to display this item.
                /// </returns>
                private static string GetText(string rawString)
                {
                    int index = rawString.IndexOf('_');
                    if (index == -1)
                    {
                        return rawString;
                    }

                    return rawString.Insert(index, "_");
                }
                #endregion Methods
            } // RSG.Editor.Controls.RsApplication.MruListDefinition.RecentListItem {Class}
            #endregion Classes
        } // RsApplication.MruListDefinition {Class}

        /// <summary>
        /// Defines a command bar menu item that represents a menu for a Mru list.
        /// </summary>
        private class MruListMenu : CommandMenu
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="MruListMenu"/> class.
            /// </summary>
            /// <param name="priority">
            /// The initial priority that this command bar item should be given.
            /// </param>
            /// <param name="listName">
            /// The name of the Mru list that this menu is being created for.
            /// </param>
            /// <param name="id">
            /// The global identifier for this command bar item.
            /// </param>
            /// <param name="startsGroup">
            /// A value indicating whether this command bar item should start a new group.
            /// (I.e. Placed directly after a separator).
            /// </param>
            public MruListMenu(ushort priority, string listName, Guid id, bool startsGroup)
                : base(
                priority,
                startsGroup,
                StringTable.MruMenuText + listName,
                id,
                RockstarCommandManager.FileMenuId)
            {
            }
            #endregion Constructors
        } // RsApplication.MruListMenu {Class}
        #endregion
    } // RSG.Editor.Controls.RsApplication {Class}
} // RSG.Editor.Controls {Namespace}
