﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using RSG.Editor;
using RSG.Automation.ViewModel;

namespace RSG.Automation.Commands
{
    public class DownloadJobOutputAction : ButtonAction<AutomationMonitorDataContext>
    {
        #region Constructors(s)
        /// <summary>
        /// Download a job's universal log file action
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public DownloadJobOutputAction(ParameterResolverDelegate<AutomationMonitorDataContext> resolver)
            : base(resolver)
        {
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// Override to set logic against the can execute command handler.
        /// </summary>
        /// <param name="commandParameter"></param>
        /// <returns></returns>
        public override bool CanExecute(AutomationMonitorDataContext dc)
        {
            return dc.CanDownloadSelectedJobsOutput();
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="commandParameter">
        /// The command parameter that has been requested.
        /// </param>
        public override async void Execute(AutomationMonitorDataContext dc)
        {
            if (dc.CanDownloadSelectedJobsOutput())
            {
                IMessageBoxService msgBoxService = this.GetService<IMessageBoxService>();
                await dc.OnDownloadJobOutput(msgBoxService);
            }
        }
        #endregion // Overrides
    } // DownloadJobOutputAction
}
