﻿//---------------------------------------------------------------------------------------------
// <copyright file="SortedObservableCollection{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Linq;

    /// <summary>
    /// Represents a observable collection which sorts it's contents based on the default
    /// comparer for type T.
    /// </summary>
    /// <typeparam name="T">
    /// The type of elements in the collection.
    /// </typeparam>
    public class SortedObservableCollection<T>
        : IList<T>, IReadOnlyList<T>, INotifyCollectionChanged, INotifyPropertyChanged
    {
        #region Fields
        /// <summary>
        /// The comparer to use when inserting items.
        /// </summary>
        private readonly IComparer<T> _comparer;

        /// <summary>
        /// The internal sorted list of items.
        /// </summary>
        private readonly List<T> _internalList;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SortedObservableCollection{T}"/> class
        /// with a optional comparer.
        /// </summary>
        /// <param name="comparer">
        /// Optional comparer to use for sorting the collection.
        /// </param>
        public SortedObservableCollection(IComparer<T> comparer = null)
            : this(Enumerable.Empty<T>(), comparer)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="SortedObservableCollection{T}"/> class
        /// based on a pre-existing list of items and optional taking a custom comparer.
        /// </summary>
        /// <param name="items">
        /// The items to add to the sorted list.
        /// </param>
        /// <param name="comparer">
        /// Optional comparer to use for sorting the collection.
        /// </param>
        public SortedObservableCollection(IEnumerable<T> items, IComparer<T> comparer = null)
        {
            if (comparer == null)
            {
                comparer = Comparer<T>.Default;
            }

            this._comparer = comparer;

            this._internalList = items.ToList();
            this._internalList.Sort(this._comparer);
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Triggered whenever the internal list of items changes.
        /// </summary>
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        /// <summary>
        /// Triggered when the <see cref="Count"/> property changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion Events

        #region Properties
        /// <summary>
        /// Gets the number of elements contained in the collection.
        /// </summary>
        public int Count
        {
            get { return this._internalList.Count; }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="SortedObservableCollection{T}"/> is
        /// read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <remarks>
        /// This is not supported by the <see cref="SortedObservableCollection{T}"/> class.
        /// </remarks>
        /// <param name="index">
        /// The zero-based index of the element to get or set.
        /// </param>
        /// <returns>
        /// The element at the specified index.
        /// </returns>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// Index is not a valid index in the <see cref="SortedObservableCollection{T}"/>.
        /// </exception>
        /// <exception cref="System.InvalidOperationException">
        /// Trying to set a item via the setter.
        /// </exception>
        public T this[int index]
        {
            get
            {
                return this._internalList[index];
            }

            set
            {
                throw new InvalidOperationException(
                    "Can't set an item via index for a sorted observable collection.");
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Adds an item to the <see cref="SortedObservableCollection{T}"/>.
        /// </summary>
        /// <param name="item">
        /// The object to add to the <see cref="SortedObservableCollection{T}"/>.
        /// </param>
        public void Add(T item)
        {
            // Try to determine the index of where the item will end up.
            int idx = this._internalList.BinarySearch(item, this._comparer);
            if (idx < 0)
            {
                // Item that is comparable wasn't found.
                idx = ~idx;
            }
            else
            {
                // Use the index + 1 of the last item that matches.
                do
                {
                    idx++;
                }
                while (
                    idx < this._internalList.Count &&
                    this._comparer.Compare(this._internalList[idx], item) == 0);
            }

            this._internalList.Insert(idx, item);
            this.OnItemAdded(item, idx);
            this.OnPropertyChanged("Count");
        }

        /// <summary>
        /// Removes all items from the <see cref="SortedObservableCollection{T}"/>.
        /// </summary>
        public void Clear()
        {
            this._internalList.Clear();
            this.OnCollectionReset();
            this.OnPropertyChanged("Count");
        }

        /// <summary>
        /// Determines whether the <see cref="SortedObservableCollection{T}"/> contains a
        /// specific value.
        /// </summary>
        /// <param name="item">
        /// The object to locate in the <see cref="SortedObservableCollection{T}"/>.
        /// </param>
        /// <returns>
        /// True if item is found in the <see cref="SortedObservableCollection{T}"/>;
        /// otherwise, false.
        /// </returns>
        public bool Contains(T item)
        {
            return this.IndexOf(item) >= 0;
        }

        /// <summary>
        /// Copies the elements of the <see cref="SortedObservableCollection{T}"/> to an
        /// System.Array, starting at a particular System.Array index.
        /// </summary>
        /// <param name="array">
        /// The one-dimensional System.Array that is the destination of the elements
        /// copied from <see cref="SortedObservableCollection{T}"/>. The System.Array must
        /// have zero-based indexing.
        /// </param>
        /// <param name="arrayIndex">
        /// The zero-based index in array at which copying begins.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// <paramref name="array"/> is null.
        /// </exception>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// <paramref name="arrayIndex"/> is less than 0.
        /// </exception>
        /// <exception cref="System.ArgumentException">
        /// The number of elements in the source <see cref="SortedObservableCollection{T}"/>
        /// is greater than the available space from arrayIndex to the end of the destination
        /// array.
        /// </exception>
        public void CopyTo(T[] array, int arrayIndex)
        {
            this._internalList.CopyTo(array, arrayIndex);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="IEnumerator"/> that can be used to iterate through
        /// the collection.
        /// </returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this._internalList.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="IEnumerator{T}"/> object that can be used to iterate through
        /// the collection.
        /// </returns>
        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return this._internalList.GetEnumerator();
        }

        /// <summary>
        /// Determines the index of a specific item in the
        /// <see cref="SortedObservableCollection{T}"/>.
        /// </summary>
        /// <param name="item">
        /// The object to locate in the <see cref="SortedObservableCollection{T}"/>.
        /// </param>
        /// <returns>
        /// The index of item if found in the list; otherwise, -1.
        /// </returns>
        public int IndexOf(T item)
        {
            int index = this._internalList.BinarySearch(item, this._comparer);
            if (index < 0)
            {
                // Item wasn't found in the list.
                return -1;
            }

            if (Object.ReferenceEquals(this._internalList[index], item))
            {
                // This is the exact item we were looking for.
                return index;
            }

            // There might be multiple items that match the item based on the comparer,
            // so search up and down from the idx that we found.
            for (int upIndex = index + 1;
                upIndex < this._internalList.Count &&
                this._comparer.Compare(this._internalList[upIndex], item) == 0;
                ++upIndex)
            {
                if (Object.ReferenceEquals(this._internalList[upIndex], item))
                {
                    return upIndex;
                }
            }

            for (int downIndex = index - 1;
                downIndex >= 0 &&
                this._comparer.Compare(this._internalList[downIndex], item) == 0;
                --downIndex)
            {
                if (Object.ReferenceEquals(this._internalList[downIndex], item))
                {
                    return downIndex;
                }
            }

            // We didn't find the exact item we were after.
            return -1;
        }

        /// <summary>
        /// Inserts an item to the <see cref="SortedObservableCollection{T}"/> at the specified
        /// index.
        /// </summary>
        /// <remarks>
        /// This is not supported by the <see cref="SortedObservableCollection{T}"/> class.
        /// </remarks>
        /// <param name="index">
        /// The zero-based index at which item should be inserted.
        /// </param>
        /// <param name="item">
        /// The object to insert into the <see cref="SortedObservableCollection{T}"/>.
        /// </param>
        /// <exception cref="InvalidOperationException">
        /// This method isn't supported by the <see cref="SortedObservableCollection{T}"/>
        /// class.
        /// </exception>
        public void Insert(int index, T item)
        {
            throw new InvalidOperationException(
                "Can't insert an item via index for a sorted observable collection.");
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the
        /// <see cref="SortedObservableCollection{T}"/>.
        /// </summary>
        /// <param name="item">
        /// The object to remove from the <see cref="SortedObservableCollection{T}"/>.
        /// </param>
        /// <returns>
        /// True if item was successfully removed from the
        /// <see cref="SortedObservableCollection{T}"/>; otherwise, false. This method also
        /// returns false if item is not found in the original
        /// <see cref="SortedObservableCollection{T}"/>.
        /// </returns>
        public bool Remove(T item)
        {
            int i = this.IndexOf(item);
            if (i < 0)
            {
                return false;
            }

            this._internalList.RemoveAt(i);
            this.OnItemRemoved(item, i);
            this.OnPropertyChanged("Count");
            return true;
        }

        /// <summary>
        /// Removes the item at the specified index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index of the item to remove.
        /// </param>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// <paramref name="index"/> is not a valid index in the
        /// <see cref="SortedObservableCollection{T}"/>.
        /// </exception>
        public void RemoveAt(int index)
        {
            T item = this._internalList[index];
            this._internalList.RemoveAt(index);
            this.OnItemRemoved(item, index);
            this.OnPropertyChanged("Count");
        }

        /// <summary>
        /// Called to notify listeners that the <see cref="SortedObservableCollection{T}"/> has
        /// been reset.
        /// </summary>
        private void OnCollectionReset()
        {
            if (this.CollectionChanged != null)
            {
                this.CollectionChanged(
                    this,
                    new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            }
        }

        /// <summary>
        /// Called to notify listeners that a new item has been added to the
        /// <see cref="SortedObservableCollection{T}"/>.
        /// </summary>
        /// <param name="item">
        /// The item that was added.
        /// </param>
        /// <param name="index">
        /// The index at which it was added to.
        /// </param>
        private void OnItemAdded(T item, int index)
        {
            if (this.CollectionChanged != null)
            {
                this.CollectionChanged(
                    this,
                    new NotifyCollectionChangedEventArgs(
                        NotifyCollectionChangedAction.Add, item, index));
            }
        }

        /// <summary>
        /// Called to notify listeners that an item has been removed from the
        /// <see cref="SortedObservableCollection{T}"/>.
        /// </summary>
        /// <param name="item">
        /// The item that was removed.
        /// </param>
        /// <param name="index">
        /// The index where the item was removed from.
        /// </param>
        private void OnItemRemoved(T item, int index)
        {
            if (this.CollectionChanged != null)
            {
                this.CollectionChanged(
                    this,
                    new NotifyCollectionChangedEventArgs(
                        NotifyCollectionChangedAction.Remove, item, index));
            }
        }

        /// <summary>
        /// Called to notify listeners that a property of the
        /// <see cref="SortedObservableCollection{T}"/> has changed.
        /// </summary>
        /// <param name="name">
        /// Name of the property that has changed.
        /// </param>
        private void OnPropertyChanged(String name)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
        #endregion Methods
    } // RSG.Editor.View.SortedObservableCollection{T} {Class}
} // RSG.Editor.View {Namespace}
