﻿//---------------------------------------------------------------------------------------------
// <copyright file="UndoDocumentAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock.Commands
{
    using RSG.Editor.Controls.Dock.ViewModel;

    /// <summary>
    /// The action logic that is responsible for executing the undo command on the active
    /// document.
    /// </summary>
    public class UndoDocumentAction : ButtonAction<DocumentItem>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="UndoDocumentAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public UndoDocumentAction(
            ParameterResolverDelegate<DocumentItem> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(DocumentItem parameter)
        {
            if (parameter == null)
            {
                return false;
            }

            return parameter.UndoEngine != null && parameter.UndoEngine.CanUndo;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(DocumentItem parameter)
        {
            parameter.UndoEngine.Undo();
        }
        #endregion Methods
    } // RSG.Editor.Controls.Dock.Commands.UndoDocumentAction {Class}
} // RSG.Editor.Controls.Dock.Commands {Namespace}
