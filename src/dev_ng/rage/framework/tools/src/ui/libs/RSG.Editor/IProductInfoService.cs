﻿//---------------------------------------------------------------------------------------------
// <copyright file="IProductInfoService.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    /// <summary>
    /// When implemented represents a service that can be used to get the product information.
    /// </summary>
    public interface IProductInfoService
    {
        #region Properties
        /// <summary>
        /// Gets the name of the product.
        /// </summary>
        string ProductName { get; }
        #endregion Properties
    } // RSG.Editor.IProductInfoService {Class}
} // RSG.Editor {Namespace}
