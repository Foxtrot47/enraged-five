﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsUnsignedIntegerSpinner.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using RSG.Base.Extensions;
    using RSG.Editor.Controls.Helpers;
    using RSG.Editor.Controls.Resources;

    /// <summary>
    /// Represents a control that can be used to edit a integer numerical value by inputting
    /// the text value or manipulating the value with increment and decrement buttons.
    /// </summary>
    public class RsUnsignedIntegerSpinner : Control
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="CycleValueOnRangeBreach"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty CycleValueOnRangeBreachProperty;

        /// <summary>
        /// Identifies the <see cref="DecrementCommand"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty DecrementCommandProperty;

        /// <summary>
        /// Identifies the <see cref="IncrementCommand"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IncrementCommandProperty;

        /// <summary>
        /// Identifies the <see cref="IsReadOnly"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsReadOnlyProperty;

        /// <summary>
        /// Identifies the <see cref="IsEditable"/> dependency property.
        /// </summary>
        public static DependencyProperty IsEditableProperty;

        /// <summary>
        /// Identifies the <see cref="Maximum"/> dependency property.
        /// </summary>
        public static DependencyProperty MaximumProperty;

        /// <summary>
        /// Identifies the <see cref="Minimum"/> dependency property.
        /// </summary>
        public static DependencyProperty MinimumProperty;

        /// <summary>
        /// Identifies the <see cref="SelectAllOnGotFocus"/> dependency property.
        /// </summary>
        public static DependencyProperty SelectAllOnGotFocusProperty;

        /// <summary>
        /// Identifies the <see cref="SelectAllOnGotFocus"/> dependency property.
        /// </summary>
        public static DependencyProperty StepProperty;

        /// <summary>
        /// Identifies the <see cref="TextAlignment"/> dependency property.
        /// </summary>
        public static DependencyProperty TextAlignmentProperty;

        /// <summary>
        /// Identifies the <see cref="Text"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty TextProperty;

        /// <summary>
        /// Identifies the <see cref="Value"/> dependency property.
        /// </summary>
        public static DependencyProperty ValueProperty;

        /// <summary>
        /// Identifies the <see cref="DecrementCommand"/> dependency property.
        /// </summary>
        private static readonly DependencyPropertyKey _decrementCommandPropertyKey;

        /// <summary>
        /// Identifies the <see cref="IncrementCommand"/> dependency property.
        /// </summary>
        private static readonly DependencyPropertyKey _incrementCommandPropertyKey;

        /// <summary>
        /// Identifies the <see cref="Text"/> dependency property.
        /// </summary>
        private static readonly DependencyPropertyKey _textPropertyKey;

        /// <summary>
        /// A private field indicating whether there are any text changes to this control that
        /// need to be resolved when the enter key is pressed or the control loses focus.
        /// </summary>
        private bool _delayedTextChanges;

        /// <summary>
        /// A private value indicating whether this object should respond to a change in the
        /// text by syncing the value property with it.
        /// </summary>
        private bool _dontResponsedToTextChange;

        /// <summary>
        /// A private value indicating whether this object should respond to a change in the
        /// value by syncing the text property with it.
        /// </summary>
        private bool _dontResponsedToValueChange;

        /// <summary>
        /// The private reference to the text box that has been defined inside the control
        /// template.
        /// </summary>
        private TextBox _textBox;

        /// <summary>
        /// The private field used for the <see cref="UpdateValueFromTextMode"/>
        /// property.
        /// </summary>
        private UpdateValueFromTextChangeMode _updateValueFromTextMode;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsUnsignedIntegerSpinner"/> class.
        /// </summary>
        static RsUnsignedIntegerSpinner()
        {
            CycleValueOnRangeBreachProperty =
                DependencyProperty.Register(
                "CycleValueOnRangeBreach",
                typeof(bool),
                typeof(RsUnsignedIntegerSpinner),
                new PropertyMetadata(false));

            MinimumProperty =
                DependencyProperty.Register(
                "Minimum",
                typeof(ulong),
                typeof(RsUnsignedIntegerSpinner),
                new PropertyMetadata(ulong.MinValue, OnRangeChanged));

            MaximumProperty =
                DependencyProperty.Register(
                "Maximum",
                typeof(ulong),
                typeof(RsUnsignedIntegerSpinner),
                new PropertyMetadata(ulong.MaxValue, OnRangeChanged));

            ValueProperty =
                DependencyProperty.Register(
                "Value",
                typeof(ulong),
                typeof(RsUnsignedIntegerSpinner),
                new FrameworkPropertyMetadata(
                    default(ulong),
                    FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                    OnValueChanged,
                    CoerceValue));

            SelectAllOnGotFocusProperty =
                DependencyProperty.Register(
                "SelectAllOnGotFocus",
                typeof(bool),
                typeof(RsUnsignedIntegerSpinner),
                new PropertyMetadata(false));

            StepProperty =
                DependencyProperty.Register(
                "Step",
                typeof(ulong),
                typeof(RsUnsignedIntegerSpinner),
                new PropertyMetadata(1UL));

            IsReadOnlyProperty =
                DependencyProperty.Register(
                "IsReadOnly",
                typeof(bool),
                typeof(RsUnsignedIntegerSpinner),
                new PropertyMetadata(false));

            IsEditableProperty =
                DependencyProperty.Register(
                "IsEditable",
                typeof(bool),
                typeof(RsUnsignedIntegerSpinner),
                new PropertyMetadata(true));

            TextAlignmentProperty =
                DependencyProperty.Register(
                "TextAlignment",
                typeof(TextAlignment),
                typeof(RsUnsignedIntegerSpinner),
                new PropertyMetadata(TextAlignment.Left));

            _incrementCommandPropertyKey =
                DependencyProperty.RegisterReadOnly(
                "IncrementCommand",
                typeof(ICommand),
                typeof(RsUnsignedIntegerSpinner),
                new PropertyMetadata(null));

            IncrementCommandProperty = _incrementCommandPropertyKey.DependencyProperty;

            _decrementCommandPropertyKey =
                DependencyProperty.RegisterReadOnly(
                "DecrementCommand",
                typeof(ICommand),
                typeof(RsUnsignedIntegerSpinner),
                new PropertyMetadata(null));

            DecrementCommandProperty = _decrementCommandPropertyKey.DependencyProperty;

            _textPropertyKey =
                DependencyProperty.RegisterReadOnly(
                "Text",
                typeof(string),
                typeof(RsUnsignedIntegerSpinner),
                new PropertyMetadata(default(ulong).ToString()));

            TextProperty = _textPropertyKey.DependencyProperty;

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsUnsignedIntegerSpinner),
                new FrameworkPropertyMetadata(typeof(RsUnsignedIntegerSpinner)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsUnsignedIntegerSpinner"/> class.
        /// </summary>
        public RsUnsignedIntegerSpinner()
        {
            this._updateValueFromTextMode = UpdateValueFromTextChangeMode.Default;

            this.SetValue(
                _incrementCommandPropertyKey,
                new RelayCommand<ulong?>(this.Increment, this.CanIncrement));

            this.SetValue(
                _decrementCommandPropertyKey,
                new RelayCommand<ulong?>(this.Decrement, this.CanDecrement));
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs whenever the integer value displayed in this control changes.
        /// </summary>
        public event EventHandler<SpinnerValueChangedEventArgs<ulong>> ValueChanged;
        #endregion Events

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether the value remains unchanged when going
        /// beyond the range or cycles to the opposite range.
        /// </summary>
        public bool CycleValueOnRangeBreach
        {
            get { return (bool)this.GetValue(CycleValueOnRangeBreachProperty); }
            set { this.SetValue(CycleValueOnRangeBreachProperty, value); }
        }

        /// <summary>
        /// Gets the command that is fired whenever the decrement spinner button is pressed.
        /// </summary>
        public ICommand DecrementCommand
        {
            get { return (ICommand)this.GetValue(DecrementCommandProperty); }
            private set { this.SetValue(_decrementCommandPropertyKey, value); }
        }

        /// <summary>
        /// Gets the command that is fired whenever the increment spinner button is pressed.
        /// </summary>
        public ICommand IncrementCommand
        {
            get { return (ICommand)this.GetValue(IncrementCommandProperty); }
            private set { this.SetValue(_incrementCommandPropertyKey, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the text label for this control can be
        /// edited directly by the user.
        /// </summary>
        public bool IsEditable
        {
            get { return (bool)this.GetValue(IsEditableProperty); }
            set { this.SetValue(IsEditableProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the user can change the value for this
        /// spinner.
        /// </summary>
        public bool IsReadOnly
        {
            get { return (bool)this.GetValue(IsReadOnlyProperty); }
            set { this.SetValue(IsReadOnlyProperty, value); }
        }

        /// <summary>
        /// Gets or sets the maximum value the numerical value for this control can have.
        /// </summary>
        public ulong Maximum
        {
            get { return (ulong)this.GetValue(MaximumProperty); }
            set { this.SetValue(MaximumProperty, value); }
        }

        /// <summary>
        /// Gets or sets the minimum value the numerical value for this control can have.
        /// </summary>
        public ulong Minimum
        {
            get { return (ulong)this.GetValue(MinimumProperty); }
            set { this.SetValue(MinimumProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether all of the text is selected and highlighted
        /// automatically when this control receives keyboard input.
        /// </summary>
        public bool SelectAllOnGotFocus
        {
            get { return (bool)this.GetValue(SelectAllOnGotFocusProperty); }
            set { this.SetValue(SelectAllOnGotFocusProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value defining the amount the numerical value of this control
        /// increases or decreases when the spinner buttons are pressed.
        /// </summary>
        public ulong Step
        {
            get { return (ulong)this.GetValue(StepProperty); }
            set { this.SetValue(StepProperty, value); }
        }

        /// <summary>
        /// Gets the text that is displayed in the string input component of this control.
        /// </summary>
        public string Text
        {
            get { return (string)this.GetValue(TextProperty); }
            private set { this.SetValue(_textPropertyKey, value); }
        }

        /// <summary>
        /// Gets or sets a value representing the alignment of the text inside the control.
        /// </summary>
        public TextAlignment TextAlignment
        {
            get { return (TextAlignment)this.GetValue(TextAlignmentProperty); }
            set { this.SetValue(TextAlignmentProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating how and when the value updates based on a change to
        /// the displayed text property.
        /// </summary>
        public UpdateValueFromTextChangeMode UpdateValueFromTextMode
        {
            get { return this._updateValueFromTextMode; }
            set { this._updateValueFromTextMode = value; }
        }

        /// <summary>
        /// Gets or sets the numerical value.
        /// </summary>
        public ulong Value
        {
            get { return (ulong)this.GetValue(ValueProperty); }
            set { this.SetValue(ValueProperty, value); }
        }

        /// <summary>
        /// Gets a value indicating whether the value property for this spinner needs to be
        /// updated when the enter key is pressed.
        /// </summary>
        private bool UpdateValueOnEnter
        {
            get
            {
                if (!this._delayedTextChanges)
                {
                    return false;
                }

                UpdateValueFromTextChangeMode mode =
                    UpdateValueFromTextChangeMode.OnEnterPressed;
                if (this.UpdateValueFromTextMode.HasFlag(mode))
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the value property for this spinner needs to be
        /// updated when the keyboard focus is lost.
        /// </summary>
        private bool UpdateValueOnLostFocus
        {
            get
            {
                if (!this._delayedTextChanges)
                {
                    return false;
                }

                UpdateValueFromTextChangeMode mode = UpdateValueFromTextChangeMode.OnLostFocus;
                if (this.UpdateValueFromTextMode.HasFlag(mode))
                {
                    return true;
                }

                mode = UpdateValueFromTextChangeMode.OnEnterPressed;
                if (this.UpdateValueFromTextMode.HasFlag(mode))
                {
                    return true;
                }

                return false;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Called whenever the control template for this instance is applied.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            if (this._textBox != null)
            {
                this._textBox.TextChanged -= this.OnTextChanged;
            }

            this._textBox = this.GetTemplateChild("PART_EditableTextBox") as TextBox;
            if (this._textBox != null)
            {
                this._textBox.Text = this.Value.ToString();
                this._textBox.TextChanged += this.OnTextChanged;
            }
        }

        /// <summary>
        /// Called whenever a mouse wheel event needs handling here.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.MouseWheelEventArgs containing the event data.
        /// </param>
        protected override void OnMouseWheel(MouseWheelEventArgs e)
        {
            base.OnMouseWheel(e);
            if (this.IsReadOnly)
            {
                return;
            }

            if (e.Handled || e.Delta == 0 || !this.IsKeyboardFocusWithin)
            {
                return;
            }

            int wheelDelta = e.Delta;
            int oneLine = Mouse.MouseWheelDeltaForOneLine;
            if (oneLine == 0)
            {
                return;
            }

            int incrementDelta = wheelDelta / oneLine;
            if (incrementDelta > 0)
            {
                for (int i = 0; i < incrementDelta; i++)
                {
                    if (this.IncrementCommand.CanExecute(this.Value))
                    {
                        this.IncrementCommand.Execute(this.Value);
                    }
                }
            }
            else if (incrementDelta < 0)
            {
                for (int i = 0; i > incrementDelta; i--)
                {
                    if (this.DecrementCommand.CanExecute(this.Value))
                    {
                        this.DecrementCommand.Execute(this.Value);
                    }
                }
            }

            decimal tempDecimal = new Decimal(this.Value) + new Decimal(incrementDelta);
            this.Value = (ulong)tempDecimal;
            e.Handled = true;
        }

        /// <summary>
        /// Called whenever the PreviewKeyDown routed event reaches this element.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.KeyEventArgs containing the event data.
        /// </param>
        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            if (e.Key == Key.Up && !this.IsReadOnly)
            {
                if (this.IncrementCommand.CanExecute(this.Value))
                {
                    this.IncrementCommand.Execute(this.Value);
                }

                e.Handled = true;
            }
            else if (e.Key == Key.Down && !this.IsReadOnly)
            {
                if (this.DecrementCommand.CanExecute(this.Value))
                {
                    this.DecrementCommand.Execute(this.Value);
                }

                e.Handled = true;
            }
            else if (e.Key == Key.Enter && !this.IsReadOnly)
            {
                if (this.UpdateValueOnEnter)
                {
                    this.UpdateValueOnTextChange();
                }
            }

            base.OnPreviewKeyDown(e);
        }

        /// <summary>
        /// Called whenever the lost keyboard focus event gets to this element. We use this
        /// event to 'straighten out' the text so that everything is valid and synced.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.KeyboardFocusChangedEventArgs containing the event data.
        /// </param>
        protected override void OnPreviewLostKeyboardFocus(KeyboardFocusChangedEventArgs e)
        {
            base.OnPreviewLostKeyboardFocus(e);
            if (this.UpdateValueOnLostFocus)
            {
                this.UpdateValueOnTextChange();
            }

            this._textBox.Text = this.Value.ToString();
            ManualValidation.HideManualValidationAdorner(this);
        }

        /// <summary>
        /// Determines the correct value for the <see cref="Value"/> dependency property based
        /// on the specified value.
        /// </summary>
        /// <param name="s">
        /// The dependency object whose <see cref="Value"/> dependency property is going to be
        /// set.
        /// </param>
        /// <param name="value">
        /// The value that the <see cref="Value"/> dependency property will be set to.
        /// </param>
        /// <returns>
        /// The value that the <see cref="Value"/> dependency property should be set to.
        /// </returns>
        private static object CoerceValue(DependencyObject s, object value)
        {
            RsUnsignedIntegerSpinner spinner = s as RsUnsignedIntegerSpinner;
            if (spinner == null)
            {
                return value;
            }

            ulong result = (ulong)value;
            if (result > spinner.Maximum)
            {
                if (spinner.CycleValueOnRangeBreach)
                {
                    result = spinner.Minimum;
                }
                else
                {
                    result = spinner.Maximum;
                }
            }

            if (result < spinner.Minimum)
            {
                if (spinner.CycleValueOnRangeBreach)
                {
                    result = spinner.Maximum;
                }
                else
                {
                    result = spinner.Minimum;
                }
            }

            return result;
        }

        /// <summary>
        /// Called whenever the <see cref="Maximum"/> or <see cref="Minimum"/> dependency
        /// properties on a dependency object changes.
        /// </summary>
        /// <param name="s">
        /// The reference to the dependency object whose <see cref="Maximum"/> or
        /// <see cref="Minimum"/> property has been changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data used for this event.
        /// </param>
        private static void OnRangeChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            RsUnsignedIntegerSpinner spinner = s as RsUnsignedIntegerSpinner;
            if (spinner == null || spinner._textBox == null)
            {
                return;
            }

            if (spinner.Value < spinner.Minimum)
            {
                spinner.Value = spinner.Minimum;
            }

            if (spinner.Value > spinner.Maximum)
            {
                spinner.Value = spinner.Maximum;
            }
        }

        /// <summary>
        /// Called whenever the <see cref="Value"/> dependency property on a dependency object
        /// changes.
        /// </summary>
        /// <param name="s">
        /// The reference to the dependency object whose <see cref="Value"/> property has been
        /// changed.
        /// </param>
        /// <param name="e">
        /// Data containing the old and new values of the <see cref="Value"/> property.
        /// </param>
        private static void OnValueChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            RsUnsignedIntegerSpinner spinner = s as RsUnsignedIntegerSpinner;
            if (spinner == null || spinner._textBox == null)
            {
                return;
            }

            spinner.OnValueChanged(e);
        }

        /// <summary>
        /// Determines whether the <see cref="DecrementCommand"/> command can be executed by
        /// the user.
        /// </summary>
        /// <param name="value">
        /// The value the spinner currently has when the command was fired.
        /// </param>
        /// <returns>
        /// True if the <see cref="DecrementCommand"/> command can be executed; otherwise,
        /// false.
        /// </returns>
        private bool CanDecrement(ulong? value)
        {
            if (this.IsReadOnly)
            {
                return false;
            }

            return this.Value > this.Minimum || this.CycleValueOnRangeBreach;
        }

        /// <summary>
        /// Determines whether the <see cref="IncrementCommand"/> command can be executed by
        /// the user.
        /// </summary>
        /// <param name="value">
        /// The value the spinner currently has when the command was fired.
        /// </param>
        /// <returns>
        /// True if the <see cref="IncrementCommand"/> command can be executed; otherwise,
        /// false.
        /// </returns>
        private bool CanIncrement(ulong? value)
        {
            if (this.IsReadOnly)
            {
                return false;
            }

            return this.Value < this.Maximum || this.CycleValueOnRangeBreach;
        }

        /// <summary>
        /// Gets called whenever the <see cref="DecrementCommand"/> command is executed.
        /// </summary>
        /// <param name="value">
        /// The value the spinner currently has when the command was fired.
        /// </param>
        private void Decrement(ulong? value)
        {
            decimal tempDecimal = new Decimal(this.Value) - new Decimal(this.Step);
            this.Value = (ulong)tempDecimal;
        }

        /// <summary>
        /// Fires the ValueChanged event using the specified values in the events argument
        /// data.
        /// </summary>
        /// <param name="oldValue">
        /// The old value for the spinner before the change.
        /// </param>
        /// <param name="newValue">
        /// The new value for the spinner after the change.
        /// </param>
        private void FireValueChangedEvent(ulong oldValue, ulong newValue)
        {
            EventHandler<SpinnerValueChangedEventArgs<ulong>> handler = this.ValueChanged;
            if (handler == null)
            {
                return;
            }

            handler(this, new SpinnerValueChangedEventArgs<ulong>(oldValue, newValue));
        }

        /// <summary>
        /// Gets called whenever the <see cref="IncrementCommand"/> command is executed.
        /// </summary>
        /// <param name="value">
        /// The value the spinner currently has when the command was fired.
        /// </param>
        private void Increment(ulong? value)
        {
            decimal tempDecimal = new Decimal(this.Value) + new Decimal(this.Step);
            this.Value = (ulong)tempDecimal;
        }

        /// <summary>
        /// Called whenever the text inside the control template defined text box changes.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Controls.TextChangedEventArgs data used for this event.
        /// </param>
        private void OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (this._dontResponsedToTextChange)
            {
                return;
            }

            if (!this.UpdateValueFromTextMode.HasFlag(UpdateValueFromTextChangeMode.RealTime))
            {
                this._delayedTextChanges = true;
                return;
            }

            this.UpdateValueOnTextChange();
        }

        /// <summary>
        /// Called whenever the <see cref="Value"/> dependency property changes.
        /// </summary>
        /// <param name="e">
        /// Data containing the old and new values of the <see cref="Value"/> property.
        /// </param>
        private void OnValueChanged(DependencyPropertyChangedEventArgs e)
        {
            if (this._dontResponsedToValueChange)
            {
                return;
            }

            if (this._textBox != null)
            {
                this._dontResponsedToTextChange = true;
                int caretIndex = this._textBox.CaretIndex;
                this._textBox.Text = ((ulong)e.NewValue).ToString();
                this._textBox.CaretIndex = Math.Min(caretIndex, this._textBox.Text.Length);
                this._dontResponsedToTextChange = false;
            }

            ManualValidation.HideManualValidationAdorner(this);
            this.FireValueChangedEvent((ulong)e.OldValue, (ulong)e.NewValue);
        }

        /// <summary>
        /// Updates the value for this spinner based on the currently entered text.
        /// </summary>
        private void UpdateValueOnTextChange()
        {
            this._delayedTextChanges = false;
            ulong newValue;
            ulong oldValue = this.Value;
            if (ulong.TryParse(this._textBox.Text, out newValue))
            {
                if (newValue > this.Maximum || newValue < this.Minimum)
                {
                    string message = StringTable.SpinnerRangeErrMsg.FormatInvariant(
                        this.Minimum.ToStringInvariant(),
                        this.Maximum.ToStringInvariant(),
                        this.Value.ToStringInvariant());
                    ManualValidation.ShowManualValidationAdorner(this, message);
                }
                else
                {
                    this._dontResponsedToValueChange = true;
                    this.Value = newValue;
                    this._dontResponsedToValueChange = false;

                    this.Text = this._textBox.Text;
                    ManualValidation.HideManualValidationAdorner(this);
                    this.FireValueChangedEvent(oldValue, newValue);
                }
            }
            else
            {
                string message = StringTable.IntegerSpinnerErrMsg.FormatInvariant(
                    this._textBox.Text, this.Value.ToStringInvariant());

                ManualValidation.ShowManualValidationAdorner(this, message);
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsUnsignedIntegerSpinner {Class}
} // RSG.Editor.Controls {Namespace}
