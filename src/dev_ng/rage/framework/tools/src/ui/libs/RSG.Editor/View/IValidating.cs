﻿//---------------------------------------------------------------------------------------------
// <copyright file="IValidating.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    using System.ComponentModel;
    using System.Threading.Tasks;
    using RSG.Editor.Model;

    /// <summary>
    /// When implemented represents a object that can be validated and have validation
    /// dependencies.
    /// </summary>
    public interface IValidating : INotifyDataErrorInfo
    {
        #region Properties
        /// <summary>
        /// Gets the current validation result container obtained from the previous validation.
        /// </summary>
        ValidationResult CurrentValidation { get; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Validates this object using the INotifyDataErrorInfo interface.
        /// </summary>
        void Validate();

        /// <summary>
        /// Validates this object asynchronously using the INotifyDataErrorInfo interface.
        /// </summary>
        /// <returns>
        /// A task that represents the work done by the validation logic.
        /// </returns>
        Task ValidateAsync();
        #endregion Methods
    } // RSG.Editor.View.IValidating {Interface}
} // RSG.Editor.View {Namespace}
