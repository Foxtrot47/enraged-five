﻿//---------------------------------------------------------------------------------------------
// <copyright file="ToolWindowItem.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock.ViewModel
{
    using System.ComponentModel;

    /// <summary>
    /// Represents a single item inside the <see cref="ToolWindowPane"/> class.
    /// </summary>
    public class ToolWindowItem : IDockingPaneItem
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="CanHide"/> property.
        /// </summary>
        private bool _canHide;

        /// <summary>
        /// The private field used for the <see cref="PaneIndex"/> property.
        /// </summary>
        private int _paneIndex;

        /// <summary>
        /// The private field used for the <see cref="ParentPane"/> property.
        /// </summary>
        private DockingPane _parentPane;

        /// <summary>
        /// The private field used for the <see cref="Content"/> property.
        /// </summary>
        private object _content;

        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private string _name;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ToolWindowItem"/> class.
        /// </summary>
        /// <param name="parentPane">
        /// The parent docking pane.
        /// </param>
        /// <param name="name">
        /// The name for this tool window item.
        /// </param>
        public ToolWindowItem(DockingPane parentPane, string name)
        {
            this._parentPane = parentPane;
            this._name = name;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets a valid indicating whether this tool window item can be hidden from
        /// the user.
        /// </summary>
        public bool CanHide
        {
            get { return this._canHide; }
            set { this._canHide = value; }
        }

        /// <summary>
        /// Gets or sets the index for this item inside its parent pane.
        /// </summary>
        public int PaneIndex
        {
            get { return this._paneIndex; }
            set { this._paneIndex = value; }
        }

        /// <summary>
        /// Gets the <see cref="DockingPane"/> that is a parent to this item.
        /// </summary>
        public DockingPane ParentPane
        {
            get { return this._parentPane; }
        }

        /// <summary>
        /// Gets or sets the content used for this tool window. This usually is the root view
        /// model for the data contained within the window.
        /// </summary>
        public object Content
        {
            get { return this._content; }
            set { this._content = value; }
        }

        /// <summary>
        /// Gets the name assigned to this tool window.
        /// </summary>
        public string Name
        {
            get { return this._name; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Called whenever this item is about to be closed and gives it the opportunity to
        /// cancel.
        /// </summary>
        /// <param name="e">
        /// The System.ComponentModel.CancelEventArgs giving this method the chance to cancel
        /// the close operation.
        /// </param>
        public void OnClosing(CancelEventArgs e)
        {
        }
        #endregion Methods
    } // RSG.Editor.Controls.Dock.ViewModel.ToolWindowItem {Class}
} // RSG.Editor.Controls.Dock.ViewModel {Namespace}
