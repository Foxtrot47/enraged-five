﻿//---------------------------------------------------------------------------------------------
// <copyright file="ThemeProperties.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.AttachedDependencyProperties
{
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Contains attached properties that are related to the theming of the user-interface.
    /// </summary>
    public static class ThemeProperties
    {
        #region Fields
        /// <summary>
        /// Identifies the ImageBackgroundColour dependency property.
        /// </summary>
        public static readonly DependencyProperty ImageBackgroundColourProperty;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="ThemeProperties"/> class.
        /// </summary>
        static ThemeProperties()
        {
            ImageBackgroundColourProperty =
                DependencyProperty.RegisterAttached(
                "ImageBackgroundColour",
                typeof(Brush),
                typeof(ThemeProperties),
                new FrameworkPropertyMetadata(
                    Brushes.Transparent, FrameworkPropertyMetadataOptions.Inherits));
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Retrieves the ImageBackgroundColour dependency property value attached to the
        /// specified object.
        /// </summary>
        /// <param name="obj">
        /// The object whose attached ImageBackgroundColour dependency property value will be
        /// returned.
        /// </param>
        /// <returns>
        /// The ImageBackgroundColour dependency property value attached to the specified
        /// object.
        /// </returns>
        public static Brush GetImageBackgroundColour(DependencyObject obj)
        {
            if (obj == null)
            {
                throw new SmartArgumentNullException(() => obj);
            }

            return (Brush)obj.GetValue(ImageBackgroundColourProperty);
        }

        /// <summary>
        /// Sets the ImageBackgroundColour dependency property value on the specified object to
        /// the specified value.
        /// </summary>
        /// <param name="obj">
        /// The object whose attached ImageBackgroundColour dependency property value will be
        /// set.
        /// </param>
        /// <param name="value">
        /// The value to set the attached ImageBackgroundColour dependency property to on the
        /// specified object.
        /// </param>
        public static void SetImageBackgroundColour(DependencyObject obj, Brush value)
        {
            if (obj == null)
            {
                throw new SmartArgumentNullException(() => obj);
            }

            obj.SetValue(ImageBackgroundColourProperty, value);
        }
        #endregion Methods
    } // RSG.Editor.Controls.AttachedDependencyProperties.ThemeProperties {Class}
} // RSG.Editor.Controls.AttachedDependencyProperties {Namespace}
