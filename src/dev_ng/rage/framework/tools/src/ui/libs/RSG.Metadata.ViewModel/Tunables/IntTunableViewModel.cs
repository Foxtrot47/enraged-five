﻿//---------------------------------------------------------------------------------------------
// <copyright file="IntTunableViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Tunables
{
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// The view model for a <see cref="IntTunable"/> model object.
    /// </summary>
    public class IntTunableViewModel : TunableViewModelBase<IntTunable>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="IntTunableViewModel"/> class.
        /// </summary>
        /// <param name="tunable">
        /// The 32-bit integer tunable that this view model is representing.
        /// </param>
        /// <param name="parent">
        /// The parent tunable view model for this view model.
        /// </param>
        public IntTunableViewModel(IntTunable tunable, ITunableViewModel parent)
            : base(tunable, parent)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the value currently assigned to the 32-bit integer tunable.
        /// </summary>
        public int Value
        {
            get { return this.Model.Value; }
            set { this.Model.Value = value; }
        }
        #endregion Properties
    } // RSG.Metadata.ViewModel.Tunables.IntTunableViewModel {Class}
} // RSG.Metadata.ViewModel.Tunables {Namespace}
