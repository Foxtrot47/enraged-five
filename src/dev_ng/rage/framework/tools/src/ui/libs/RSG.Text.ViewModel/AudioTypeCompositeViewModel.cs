﻿//---------------------------------------------------------------------------------------------
// <copyright file="AudioTypeCompositeViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.ViewModel
{
    using System.Collections.Generic;
    using System.Linq;
    using RSG.Text.Model;

    /// <summary>
    /// A composite class containing n-number of <see cref="DialogueAudioTypeViewModel"/>
    /// objects. This class cannot be inherited.
    /// </summary>
    public sealed class AudioTypeCompositeViewModel : CompositeViewModel
    {
        #region Fields
        /// <summary>
        /// The private collection containing the <see cref="DialogueAudioTypeViewModel"/>
        /// objects that are currently managed by this composite class.
        /// </summary>
        private List<DialogueAudioTypeViewModel> _items;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="AudioTypeCompositeViewModel"/> class.
        /// </summary>
        /// <param name="configurations">
        /// The <see cref="DialogueConfigurations"/> object that the configuration values have
        /// come from.
        /// </param>
        /// <param name="items">
        /// The collection of <see cref="DialogueAudioTypeViewModel"/> objects that this
        /// composite object should manage.
        /// </param>
        public AudioTypeCompositeViewModel(
            DialogueConfigurations configurations,
            IEnumerable<DialogueAudioTypeViewModel> items)
            : base(configurations)
        {
            this._items = new List<DialogueAudioTypeViewModel>(items);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the collective export index for all of the dialogue audio type objects
        /// managed by this class.
        /// </summary>
        public int? ExportIndex
        {
            get
            {
                if (this._items == null || this._items.Count == 0)
                {
                    return null;
                }

                IEnumerable<int> indices = this._items.Select(x => x.ExportIndex).Distinct();
                if (indices.Count() == 1)
                {
                    return indices.First();
                }

                return null;
            }

            set
            {
                if (this._items == null)
                {
                    return;
                }

                foreach (DialogueAudioTypeViewModel item in this._items)
                {
                    item.ExportIndex = value.Value;
                }
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Deletes all of the values managed by this composite collection from the dialogue
        /// configuration object.
        /// </summary>
        public override void Delete()
        {
            List<DialogueAudioType> delete = new List<DialogueAudioType>();
            foreach (DialogueAudioTypeViewModel viewModel in this._items)
            {
                foreach (DialogueAudioType audioType in this.Model.AudioTypes)
                {
                    if (viewModel.Id == audioType.Id)
                    {
                        delete.Add(audioType);
                    }
                }
            }

            foreach (DialogueAudioType audioType in delete)
            {
                this.Model.AudioTypes.Remove(audioType);
            }
        }
        #endregion Methods
    } // RSG.Text.ViewModel.AudioTypeCompositeViewModel {Class}
} // RSG.Text.ViewModel {Namespace}
