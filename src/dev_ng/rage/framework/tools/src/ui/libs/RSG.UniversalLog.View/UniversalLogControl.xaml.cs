﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Editor;
using RSG.Editor.Controls;
using RSG.Editor.Controls.Helpers;
using RSG.Editor.SharedCommands;
using RSG.UniversalLog.Commands;
using RSG.UniversalLog.View.Commands;
using RSG.UniversalLog.ViewModel;

namespace RSG.UniversalLog.View
{
    /// <summary>
    /// Interaction logic for UniversalLogControl.xaml
    /// </summary>
    public partial class UniversalLogControl : RsUserControl
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="SelectedItemsProperty"/> property.
        /// </summary>
        private static DependencyProperty SelectedItemsProperty =
            DependencyProperty.Register(
                "SelectedItems",
                typeof(IEnumerable<UniversalLogMessageViewModel>),
                typeof(UniversalLogControl),
                new FrameworkPropertyMetadata(Enumerable.Empty<UniversalLogMessageViewModel>()));

        /// <summary>
        /// The private field used for the <see cref="SearchExpressionProperty"/> property.
        /// </summary>
        private static DependencyProperty SearchExpressionProperty =
            DependencyProperty.Register(
                "SearchExpression",
                typeof(Regex),
                typeof(UniversalLogControl),
                new FrameworkPropertyMetadata(null));

        /// <summary>
        /// The private field used for the <see cref="InitialSortMember"/> property.
        /// </summary>
        private InitialSortMember _initialSortMember = InitialSortMember.Timestamp;

        /// <summary>
        /// Flag indicating whether we are currently trying to scroll an item into the view.
        /// </summary>
        private bool _attemptingScrollIntoView = false;

        /// <summary>
        /// The item we are currently trying to scroll into view.
        /// </summary>
        private UniversalLogMessageViewModel _scrollIntoViewItem = null;

        /// <summary>
        /// The number of attempts we've made at scrolling the item into view.
        /// </summary>
        private int _scrollIntoViewAttempts = 0;

        /// <summary>
        /// Log level categories that are currently allowed to be shown.
        /// </summary>
        private ISet<LogLevel> _allowedLevels;

        /// <summary>
        /// List of files that are being filtered out and not shown in the UI.
        /// </summary>
        private ISet<String> _filteredFiles;

        /// <summary>
        /// Flag indicating whether search filtering is currently enabled.
        /// </summary>
        private bool _searchFilteringEnabled;
		#endregion // Fields

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public UniversalLogControl()
        {
            _allowedLevels = new HashSet<LogLevel>();
            _allowedLevels.AddRange((LogLevel[])Enum.GetValues(typeof(LogLevel)));

            _filteredFiles = new HashSet<String>();

            InitializeComponent();

            ICollectionView items = CollectionViewSource.GetDefaultView(this.DataGrid.Items);
            if (items != null)
            {
                items.CollectionChanged += this.OnVisibleItemsChanged;
            }


            this.DataContextChanged += this.OnDataContextChanged;
            this.DataGrid.SelectionChanged += this.OnSelectionChanged;

            Binding binding = new Binding("ItemsSource");
            binding.Source = this;
            this.DataGrid.SetBinding(DataGrid.ItemsSourceProperty, binding);

            this.AttachCommandBindings();
        }

        /// <summary>
        /// 
        /// </summary>
        static UniversalLogControl()
        {
            ItemsControl.ItemsSourceProperty.AddOwner(
                typeof(UniversalLogControl),
                new PropertyMetadata(null, OnItemsSourceChanged));
        }
        #endregion // Constructors

        #region Properties
        /// <summary>
        /// Sets the member which is initially sorted ascending.
        /// </summary>
        public InitialSortMember InitialSortMember
        {
            set { this._initialSortMember = value; }
        }

        /// <summary>
        /// Gets or sets a collection used to generate the content of this control.
        /// </summary>
        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)this.GetValue(ItemsControl.ItemsSourceProperty); }
            set { this.SetValue(ItemsControl.ItemsSourceProperty, value); }
        }

        /// <summary>
        /// Gets or sets the search expression to use during filtering of the items.
        /// </summary>
        public Regex SearchExpression
        {
            get { return (Regex)this.GetValue(SearchExpressionProperty); }
            set { this.SetValue(SearchExpressionProperty, value); }
        }

        /// <summary>
        /// Gets or sets a iterator around the currently selected items in the data grid.
        /// </summary>
        public IEnumerable<UniversalLogMessageViewModel> SelectedItems
        {
            get { return (IEnumerable<UniversalLogMessageViewModel>)this.GetValue(SelectedItemsProperty); }
            set { throw new Exception("An attempt to modify a Read-Only property was made."); }
        }

        /// <summary>
        /// Gets a value indicating whether the layout for this control get serialised on
        /// exit and deserialised after loading.
        /// </summary>
        protected override bool MakeLayoutPersistent
        {
            get { return true; }
        }

        /// <summary>
        /// Reference to the filter by file action.
        /// </summary>
        public FilterByFileAction FilterByFileAction
        {
            get { return _filterByFileAction; }
        }
        private FilterByFileAction _filterByFileAction;

        /// <summary>
        /// Filter by log level action.
        /// </summary>
        public FilterByLogLevelAction FilterByLogLevelAction
        {
            get { return _filterByLogLevelAction; }
        }
        private FilterByLogLevelAction _filterByLogLevelAction;

        /// <summary>
        /// Copy action.
        /// </summary>
        public CopyAction CopyAction
        {
            get { return _copyAction; }
        }
        private CopyAction _copyAction;

        /// <summary>
        /// Reference to the search action.
        /// </summary>
        public SearchAction SearchAction
        {
            get { return _searchAction; }
        }
        private SearchAction _searchAction;
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Called whenever the <see cref="ItemsSource"/> dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The object whose <see cref="ItemsSource"/> dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnItemsSourceChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            UniversalLogControl logViewer = s as UniversalLogControl;
            if (logViewer == null)
            {
                return;
            }

            if (e.OldValue == null)
            {
                string propertyName = String.Empty;
                switch (logViewer._initialSortMember)
                {
                    case InitialSortMember.LogLevel:
                        propertyName = "Level";
                        break;
                    case InitialSortMember.Timestamp:
                        propertyName = "LocalTimestamp";
                        break;
                    case InitialSortMember.Ticks:
                        propertyName = "Ticks";
                        break;
                    case InitialSortMember.SystemContext:
                        propertyName = "SystemContext";
                        break;
                    case InitialSortMember.MessageContext:
                        propertyName = "MessageContext";
                        break;
                    case InitialSortMember.FileContext:
                        propertyName = "FileContext";
                        break;
                    case InitialSortMember.Message:
                        propertyName = "Message";
                        break;
                    default:
                        return;
                }

                DataGridColumn sortColumn = null;
                foreach (DataGridColumn column in logViewer.DataGrid.Columns)
                {
                    if (column.SortDirection != null)
                    {
                        return;
                    }

                    if (String.CompareOrdinal(column.SortMemberPath, propertyName) == 0)
                    {
                        sortColumn = column;
                        break;
                    }
                }

                if (sortColumn == null)
                {
                    return;
                }

                sortColumn.SortDirection = ListSortDirection.Ascending;
                logViewer.DataGrid.Items.SortDescriptions.Clear();
                SortDescription description =
                    new SortDescription(propertyName, ListSortDirection.Ascending);
                logViewer.DataGrid.Items.SortDescriptions.Add(description);
            }
            else
            {
                ListSortDirection direction = ListSortDirection.Ascending;
                DataGridColumn sortColumn = null;
                foreach (DataGridColumn column in logViewer.DataGrid.Columns)
                {
                    if (column.SortDirection == null)
                    {
                        continue;
                    }

                    direction = (ListSortDirection)column.SortDirection;
                    sortColumn = column;
                    break;
                }

                if (sortColumn == null)
                {
                    return;
                }

                logViewer.Dispatcher.BeginInvoke(
                    new Action(
                    delegate
                    {
                        sortColumn.SortDirection = direction;
                        logViewer.DataGrid.Items.SortDescriptions.Clear();
                        SortDescription description =
                            new SortDescription(sortColumn.SortMemberPath, direction);
                        logViewer.DataGrid.Items.SortDescriptions.Add(description);

                        logViewer.UpdateSelectedItems();
                    }),
                    System.Windows.Threading.DispatcherPriority.Render);
            }
        }

        /// <summary>
        /// Called whenever the selection on the data grid changes.
        /// </summary>
        /// <param name="sender">
        /// The object that this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Controls.SelectionChangedEventArgs data for this event.
        /// </param>
        private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateSelectedItems();
        }

        /// <summary>
        /// 
        /// </summary>
        private void UpdateSelectedItems()
        {
            this.SetValue(SelectedItemsProperty,
                this.DataGrid.SelectedItems.OfType<UniversalLogMessageViewModel>());
        }

        /// <summary>
        /// Called whenever the items currently in the data grid are changed.
        /// </summary>
        /// <param name="sender">
        /// The object that this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The collection changed arguments.
        /// </param>
        private void OnVisibleItemsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            ItemCollection collection = sender as ItemCollection;
            if (collection == null)
            {
                Debug.Assert(collection != null, "Incorrect cast in collection arguments.");
                return;
            }

            UniversalLogDataContext dc =
                this.Dispatcher.Invoke(new Func<UniversalLogDataContext>(
                    delegate
                    {
                        return this.DataContext as UniversalLogDataContext;
                    }));

            if (dc == null || !dc.AnyFilesLoaded)
            {
                Debug.Assert(collection != null, "Incorrect data context cast in universal log viewer.");
                return;
            }

            //
            dc.UpdateVisibleCounts(collection);
        }
        #endregion // Methods

        #region Command Implementations
        /// <summary>
        /// Attaches all of the necessary command bindings to this instance.
        /// </summary>
        private void AttachCommandBindings()
        {
            _copyAction = new CopyAction(this.SelectedItemsResolver);
            _copyAction.AddBinding(RockstarCommands.Copy, this);

            _filterByLogLevelAction = new FilterByLogLevelAction(this.ControlResolver);
            _filterByLogLevelAction.AddBinding(UniversalLogCommands.FilterByLogLevel, this);

            _filterByFileAction = new FilterByFileAction(this.ControlResolver, this.DataContext as UniversalLogDataContext);
            _filterByFileAction.AddBinding(UniversalLogCommands.FilterByFile, this);
            
            _searchAction = new SearchAction(this.ControlResolver);
            _searchAction.AddBinding(this);
        }

        /// <summary>
        /// Updates the filter by file action's item list when the data context changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue != null)
            {
                _filterByFileAction.UpdateDataContext(e.OldValue as UniversalLogDataContext, e.NewValue as UniversalLogDataContext);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public IEnumerable<UniversalLogMessageViewModel> SelectedItemsResolver(CommandData data)
        {
            // Get the list of items ordered by how the view is currently displaying them.
            CollectionView itemsView = (CollectionView)CollectionViewSource.GetDefaultView(DataGrid.Items);

            IEnumerable<UniversalLogMessageViewModel> orderedItems;
            if (itemsView != null)
            {
                orderedItems = this.SelectedItems.OrderBy(item => itemsView.IndexOf(item));
            }
            else
            {
                orderedItems = this.SelectedItems;
            }
            return orderedItems;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public UniversalLogDataContext DataContextResolver(CommandData data)
        {
            UniversalLogDataContext dc = null;
            if (!this.Dispatcher.CheckAccess())
            {
                dc = this.Dispatcher.Invoke<UniversalLogDataContext>(() => this.DataContext as UniversalLogDataContext);
            }
            else
            {
                dc = this.DataContext as UniversalLogDataContext;
            }
            return dc;
        }

        /// <summary>
        /// Represents the method that is used to retrieve a command parameter object for the
        /// view commands.
        /// </summary>
        /// <param name="command">
        /// The command whose command parameter needs to be resolved.
        /// </param>
        /// <returns>
        /// The command parameter object for the specified command.
        /// </returns>
        private UniversalLogControl ControlResolver(CommandData data)
        {
            return this;
        }
        #endregion // Command Implementations

        #region Filtering Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        internal void AddAllowedFilterLevel(LogLevel level)
        {
            _allowedLevels.Add(level);

            if (level == LogLevel.Profile)
            {
                _allowedLevels.Add(LogLevel.ProfileEnd);
            }
            if (level == LogLevel.Error)
            {
                _allowedLevels.Add(LogLevel.ToolError);
            }
            ForceFilter();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        internal void RemoveAllowedFilterLevel(LogLevel level)
        {
            _allowedLevels.Remove(level);

            if (level == LogLevel.Profile)
            {
                _allowedLevels.Remove(LogLevel.ProfileEnd);
            }
            if (level == LogLevel.Error)
            {
                _allowedLevels.Remove(LogLevel.ToolError);
            }
            ForceFilter();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filepath"></param>
        internal void AddFileFilter(String filepath)
        {
            _filteredFiles.Add(filepath);
            ForceFilter();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filepath"></param>
        internal void RemoveFileFilter(String filepath)
        {
            _filteredFiles.Remove(filepath);
            ForceFilter();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="filter"></param>
        internal void SetSearchProperties(Regex expression, bool filter)
        {
            SearchExpression = expression;

            if (_searchFilteringEnabled != filter)
            {
                _searchFilteringEnabled = filter;
                ForceFilter();
            }
        }

        /// <summary>
        /// Forces a re-filter of the shown items.
        /// </summary>
        private void ForceFilter()
        {
            ICollectionView items = CollectionViewSource.GetDefaultView(DataGrid.Items);
            if (items != null)
            {
                items.Filter = this.FilterItem;
            }
        }

        /// <summary>
        /// A method that determines whether the specified item can be viewed in the data grid
        /// control or not.
        /// </summary>
        /// <param name="item">The item to test.</param>
        /// <returns>True if the specified item can be viewed in the data grid; otherwise, false.</returns>
        private bool FilterItem(object item)
        {
            UniversalLogMessageViewModel viewModel = item as UniversalLogMessageViewModel;
            if (viewModel == null)
            {
                return false;
            }

            // Are we filtering the items by our search expression?
            bool searchMatches = true;
            if (_searchFilteringEnabled && SearchExpression != null)
            {
                searchMatches = SearchExpression.IsMatch(viewModel.Message);
            }

            // Check whether we can show this log level and file.
            return searchMatches && _allowedLevels.Contains(viewModel.Level) && !_filteredFiles.Contains(viewModel.FilePath);
        }
        #endregion // Filtering Methods

        #region Custom Scrolling Behaviour
        /// <summary>
        /// Scrolls the selected item into view.
        /// </summary>
        public void ScrollSelectionIntoView()
        {
            this.DataGrid.UpdateLayout();
            if (this.DataGrid.SelectedItem != null)
            {
                // The following doesn't work due to the datagrid UI virtualisation.
                //this.DataGrid.ScrollIntoView(this.DataGrid.SelectedItem);

                // Instead we need to add a visibility check after all the arrange passes have completed.
                // I tried to use a few different DispatcherPriorities, however ApplicationIdle is the only one that ensured the check
                // was done at the right point in time.
                CollectionView itemsView = (CollectionView)CollectionViewSource.GetDefaultView(DataGrid.Items);
                _scrollIntoViewItem = (UniversalLogMessageViewModel)this.DataGrid.SelectedItem;
                _attemptingScrollIntoView = true;
                _scrollIntoViewAttempts = 0;

                ScrollViewer scrollViewer = DataGrid.GetVisualDescendent<ScrollViewer>();
                scrollViewer.ScrollToVerticalOffset(itemsView.IndexOf(this.DataGrid.SelectedItem));

                this.Dispatcher.BeginInvoke(new Action(VisibilityCheck), System.Windows.Threading.DispatcherPriority.ApplicationIdle);
            }
        }

        /// <summary>
        /// Scrolls to the bottom of the datagrid.
        /// </summary>
        public void ScrollToBottom()
        {
            this.DataGrid.UpdateLayout();
            ScrollViewer scrollViewer = DataGrid.GetVisualDescendent<ScrollViewer>();
            if (scrollViewer != null)
            {
                CollectionView itemsView = (CollectionView)CollectionViewSource.GetDefaultView(DataGrid.Items);
                if (itemsView.Count > 0)
                {
                    _scrollIntoViewItem = (UniversalLogMessageViewModel)itemsView.GetItemAt(itemsView.Count - 1);
                    _attemptingScrollIntoView = true;
                    _scrollIntoViewAttempts = 0;

                    scrollViewer.ScrollToVerticalOffset(itemsView.Count - 1);
                    this.Dispatcher.BeginInvoke(new Action(VisibilityCheck), System.Windows.Threading.DispatcherPriority.ApplicationIdle);
                }
            }
        }

        /// <summary>
        /// Checks whether the item we wanted to scroll into view is completely visible.
        /// </summary>
        private void VisibilityCheck()
        {
            if (_attemptingScrollIntoView)
            {
                UserVisibilityAwareDataGridRowsPresenter presenter =
                    DataGrid.GetVisualDescendent<UserVisibilityAwareDataGridRowsPresenter>();
                DataGridRow row = (DataGridRow)DataGrid.ItemContainerGenerator.ContainerFromItem(_scrollIntoViewItem);

                if (row != null && presenter.FullyVisibleChildren.Contains(row))
                {
                    _attemptingScrollIntoView = false;
                }
                else if (_scrollIntoViewAttempts < 10)
                {
                    _scrollIntoViewAttempts++;
                    this.DataGrid.ScrollIntoView(_scrollIntoViewItem);
                    this.Dispatcher.BeginInvoke(new Action(VisibilityCheck), System.Windows.Threading.DispatcherPriority.ApplicationIdle);
                }
                else
                {
                    _attemptingScrollIntoView = false;
                }
            }
        }
        #endregion // Custom Scrolling Behaviour
    } // UniversalLogControl
}
