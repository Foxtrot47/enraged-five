﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProjectNode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Media.Imaging;
    using RSG.Base.Extensions;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Editor.View;
    using RSG.Interop.Microsoft.Windows;
    using RSG.Project.Model;
    using RSG.Project.ViewModel.Definitions;
    using RSG.Project.ViewModel.Resources;

    /// <summary>
    /// 
    /// </summary>
    public class ProjectNode : FileNode, ISaveableDocument
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="ExpandedByDefault"/> property.
        /// </summary>
        private bool _isExpandedByDefault;

        /// <summary>
        /// The private field used for the <see cref="isReadOnly"/> property.
        /// </summary>
        private bool _isReadOnly;

        /// <summary>
        /// The private field used for the <see cref="Definition"/> property.
        /// </summary>
        private ProjectDefinition _definition;

        /// <summary>
        /// A private value indicating whether the last load failed.
        /// </summary>
        private bool _loadFailed;

        /// <summary>
        /// The private field used for the <see cref="IsModified"/> property.
        /// </summary>
        private bool _modifiedProperties;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public ProjectNode(ProjectDefinition definition, IHierarchyNode parent, ProjectItem item)
            : base(parent, item)
        {
            if (definition == null)
            {
                throw new SmartArgumentNullException(() => definition);
            }

            this._definition = definition;
            string expandedText = item.GetMetadata("Expanded");
            bool expanded = false;
            if (bool.TryParse(expandedText, out expanded))
            {
                this._isExpandedByDefault = expanded;
            }

            this.Text = Path.GetFileNameWithoutExtension(item.Include) + " (unavailable)";
            this.AddChild(new HierarchyNode(this, null) { Text = "The project file hasn't been loaded yet" });
            this.Icon = ProjectIcons.GenericProject;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="definition">
        /// 
        /// </param>
        public ProjectNode(ProjectDefinition definition)
        {
            if (definition == null)
            {
                throw new SmartArgumentNullException(() => definition);
            }

            this._definition = definition;
            this.Icon = ProjectIcons.GenericProject;
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs just after this object has been saved successfully.
        /// </summary>
        public event EventHandler Saved;
        #endregion Events

        #region Properties
        /// <summary>
        /// Gets a value indicating whether a new or existing project folder can be added to
        /// this hierarchy node.
        /// </summary>
        public override bool CanHaveProjectFoldersAdded
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether this hierarchy node can be removed from the parent
        /// items scope it belongs to.
        /// </summary>
        public override bool CanBeRemoved
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether a new or existing items can be added to this
        /// hierarchy node.
        /// </summary>
        public override bool CanHaveItemsAdded
        {
            get { return this.Loaded; }
        }

        /// <summary>
        /// Gets a value indicating whether this hierarchy nodes data can be saved.
        /// </summary>
        public override bool CanBeSaved
        {
            get { return true; }
        }

        /// <summary>
        /// Gets the definition that was used to create this project node.
        /// </summary>
        public ProjectDefinition Definition
        {
            get { return this._definition; }
        }

        /// <summary>
        /// Gets the invoke controller that can be used to perform a action on this object.
        /// </summary>
        public override IInvokeController InvokeController
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the item definition categories that are associated with this project.
        /// </summary>
        public IReadOnlyList<ItemDefinitionCategory> DefinitionCategories
        {
            get { return this.GetDefinitionCategories(); }
        }

        /// <summary>
        /// Gets or sets the filename to use to display this document.
        /// </summary>
        public string Filename
        {
            get { return this.Text; }
            set { this.Text = value; }
        }

        /// <summary>
        /// Gets or sets the bitmap source that is used to display the icon for this item.
        /// </summary>
        public override BitmapSource Icon
        {
            get
            {
                if (this._loadFailed)
                {
                    return ProjectIcons.Failedproject;
                }

                if (!this.Loaded)
                {
                    return ProjectIcons.Unloadedproject;
                }

                return base.Icon;
            }

            protected set
            {
                base.Icon = value;
            }
        }

        /// <summary>
        /// Gets the friendly save path that this item pushes into the command system to
        /// display on menu items.
        /// </summary>
        public override string FriendlySavePath
        {
            get { return Path.GetFileNameWithoutExtension(this.Filename); }
        }

        /// <summary>
        /// Gets or sets the full file path to the location of this file.
        /// </summary>
        public override string FullPath
        {
            get
            {
                if (this.Model != null)
                {
                    return this.Model.GetMetadata("FullPath");
                }

                return String.Empty;
            }

            set
            {
                Project model = this.Model as Project;
                if (model == null)
                {
                    return;
                }

                if (string.Equals(model.FullPath, value))
                {
                    return;
                }

                model.FullPath = value;
                if (this.ProjectItemScopeNode != null)
                {
                    this.ProjectItemScopeNode.IsModified = true;
                }

                this.Text = System.IO.Path.GetFileNameWithoutExtension(value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this document is currently locked inside
        /// the windows explorer.
        /// </summary>
        public bool IsReadOnly
        {
            get { return this._isReadOnly; }
            set { this.SetProperty(ref this._isReadOnly, value); }
        }

        /// <summary>
        /// Gets a value indicating whether this document represents a temporary document. A
        /// temporary file cannot be saved to the same location and needs to be "Saved As...".
        /// </summary>
        public bool IsTemporaryFile
        {
            get
            {
                if (this.FullPath.StartsWith(Path.GetTempPath()))
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Gets the undo engine associated with this document.
        /// </summary>
        public UndoEngine UndoEngine
        {
            get { return null; }
        }

        /// <summary>
        /// Gets a value indicating whether this item is expanded by default in the user
        /// interface when by rendered inside a hierarchical control.
        /// </summary>
        public override bool IsExpandedByDefault
        {
            get { return this._isExpandedByDefault; }
        }

        /// <summary>
        /// Gets a value indicating whether this project currently contains modified unsaved
        /// data.
        /// </summary>
        public override bool IsModified
        {
            get { return base.IsModified || this._modifiedProperties; }
            set { base.IsModified = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool ModifiedProperties
        {
            get { return this._modifiedProperties; }
            set { this.SetProperty(ref this._modifiedProperties, value, "IsModified"); }
        }

        /// <summary>
        /// Gets a value indicating whether this node is representing a virtual project item or
        /// a project item that is located on the users disk drive.
        /// </summary>
        public override bool IsVirtual
        {
            get { return false; }
        }

        public bool Loaded
        {
            get { return this.Model is Project; }
        }

        /// <summary>
        /// Gets the parent node for this node.
        /// </summary>
        public override ProjectNode ParentProject
        {
            get { return this; }
        }

        /// <summary>
        /// Gets the priority value for this object.
        /// </summary>
        public override int Priority
        {
            get { return 1; }
        }

        /// <summary>
        /// Retrieves a iterator around the project items that have the specified type. This
        /// will return a empty enumerable instead of null.
        /// </summary>
        /// <param name="itemType">
        /// The type of items that should be retrieved.
        /// </param>
        /// <returns>
        /// A iterator around the project items defined for this project with the specified
        /// item type.
        /// </returns>
        public IEnumerable<ProjectItem> this[string itemType]
        {
            get
            {
                if (this.Loaded)
                {
                    return (this.Model as Project)[itemType];
                }

                return Enumerable.Empty<ProjectItem>();
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Called just before this object in the user interface is expanded inside a
        /// hierarchical control.
        /// </summary>
        public async override void BeforeExpand()
        {
            base.BeforeExpand();
            List<FileNode> fileNodes = new List<FileNode>();
            foreach (IHierarchyNode child in this.GetChildren(false))
            {
                FileNode childFile = child as FileNode;
                if (childFile == null)
                {
                    continue;
                }

                fileNodes.Add(childFile);
            }

            IList<IPerforceFileMetaData> metadataList = await this.GetChildMetadataAsync();
            StringComparison comparisonType = StringComparison.OrdinalIgnoreCase;
            foreach (FileNode fileNode in fileNodes)
            {
                IPerforceFileMetaData metadata = null;
                if (metadataList != null)
                {
                    string filePath = fileNode.FullPath;
                    foreach (IPerforceFileMetaData fileMetadata in metadataList)
                    {
                        string localPath = fileMetadata.LocalPath;
                        if (String.Equals(localPath, filePath, comparisonType))
                        {
                            metadata = fileMetadata;
                            break;
                        }
                    }
                }

                fileNode.UpdatePerforceState(metadata);
            }
        }

        /// <summary>
        /// Gets the source control metadata for the child nodes.
        /// </summary>
        /// <returns>
        /// The source control metadata for the child nodes.
        /// </returns>
        private async Task<IList<IPerforceFileMetaData>> GetChildMetadataAsync()
        {
            IPerforceService ps = this.ParentCollection.PerforceService;
            if (ps == null)
            {
                return null;
            }

            List<string> filenames = new List<string>();
            List<FileNode> fileNodes = new List<FileNode>();
            foreach (IHierarchyNode child in this.GetChildren(false))
            {
                FileNode childFile = child as FileNode;
                if (childFile == null)
                {
                    continue;
                }

                filenames.Add(childFile.FullPath);
                fileNodes.Add(childFile);
            }

            if (filenames.Count == 0)
            {
                return null;
            }

            var action = new Func<IList<IPerforceFileMetaData>>(
                delegate
                {
                    using (ps.SuspendExceptionsScope())
                    {
                        return ps.GetFileMetaData(filenames);
                    }
                });

            return await Task.Factory.StartNew(action);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="undoEngine"></param>
        /// <returns></returns>
        public virtual IProjectPropertyContent CreatePropertyContent(UndoEngine undoEngine)
        {
            return null;
        }

        /// <summary>
        /// Creates a new project item object for the file with the specified relative path
        /// and adds a new hierarchy node to the specified parent that represents it.
        /// </summary>
        /// <param name="parent">
        /// The parent node to the new item.
        /// </param>
        /// <param name="relativePath">
        /// The relative path for the new file from this project.
        /// </param>
        /// <returns>
        /// The new hierarchy node that was created to represent the file.
        /// </returns>
        public virtual IHierarchyNode AddNewFile(IHierarchyNode parent, string relativePath)
        {
            IProjectItemScope scope = this.Model as IProjectItemScope;
            if (scope == null)
            {
                return null;
            }

            ProjectItem newItem = scope.AddNewProjectItem("File", relativePath);
            if (parent is FolderNode)
            {
                newItem.SetMetadata("Filter", parent.Include, MetadataLevel.Filter);
            }

            IHierarchyNode newNode = this.CreateFileNode(parent, newItem);
            parent.AddChild(newNode);
            this.IsModified = true;
            return newNode;
        }

        /// <summary>
        /// 
        /// </summary>
        public void SetParentAndModel(IHierarchyNode parent, ProjectItem item)
        {
            this.Parent = parent;
            this.ReplaceModel(item, false);

            string expandedText = item.GetMetadata("Expanded");
            bool expanded = false;
            if (bool.TryParse(expandedText, out expanded))
            {
                this._isExpandedByDefault = expanded;
            }

            this.Text = Path.GetFileNameWithoutExtension(item.Include) + " (unavailable)";
            this.AddChild(new HierarchyNode(this, null) { Text = "The project file hasn't been loaded yet" });
        }

        /// <summary>
        /// Retrieves the item definition categories that have been setup for this project.
        /// </summary>
        /// <returns>
        /// The item definition categories that have been setup for this project.
        /// </returns>
        public virtual IReadOnlyList<ItemDefinitionCategory> GetDefinitionCategories()
        {
            return new List<ItemDefinitionCategory>();
        }

        /// <summary>
        /// Retrieves the value of the property with the specified name.
        /// </summary>
        /// <param name="name">
        /// The name of the property to retrieve the value of.
        /// </param>
        /// <param name="defaultValue">
        /// The value to return if the property doesn't exist.
        /// </param>
        /// <returns>
        /// The value of the property with the specified name if it exists; otherwise, the
        /// specified fall-back value.
        /// </returns>
        public string GetProperty(string name, string defaultValue)
        {
            Project project = this.Model as Project;
            if (project == null)
            {
                return defaultValue;
            }

            return project.GetProperty(name, defaultValue);
        }

        /// <summary>
        /// Retrieves the value of the property with the specified name.
        /// </summary>
        /// <param name="name">
        /// The name of the property to retrieve the value of.
        /// </param>
        /// <returns>
        /// The value of the property with the specified name if it exists; otherwise, null.
        /// </returns>
        public string GetProperty(string name)
        {
            return this.GetProperty(name, null);
        }

        /// <summary>
        /// Retrieves the value of the property with the specified name as a boolean value.
        /// </summary>
        /// <param name="name">
        /// The name of the property to retrieve the value of.
        /// </param>
        /// <param name="defaultValue">
        /// The value to return if the property doesn't exist.
        /// </param>
        /// <returns>
        /// The value of the property with the specified name as a boolean value if it exists;
        /// otherwise, the specified fall-back value.
        /// </returns>
        public bool GetPropertyAsBool(string name, bool defaultValue)
        {
            Project project = this.Model as Project;
            if (project == null)
            {
                return defaultValue;
            }

            return project.GetPropertyAsBool(name, defaultValue);
        }

        /// <summary>
        /// Retrieves the value of the property with the specified name as a boolean value.
        /// </summary>
        /// <param name="name">
        /// The name of the property to retrieve the value of.
        /// </param>
        /// <returns>
        /// The value of the property with the specified name as a boolean value if it exists;
        /// otherwise, false.
        /// </returns>
        public bool GetPropertyAsBool(string name)
        {
            return this.GetPropertyAsBool(name, false);
        }

        /// <summary>
        /// Raises the saved event on this object.
        /// </summary>
        public void RaiseSavedEvent()
        {
            EventHandler handler = this.Saved;
            if (handler == null)
            {
                return;
            }

            handler(this, EventArgs.Empty);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="name">
        /// 
        /// </param>
        /// <param name="value">
        /// 
        /// </param>
        public void SetIndependentProperty(string name, string value)
        {
            Project project = this.Model as Project;
            if (project == null)
            {
                return;
            }

            project.SetIndependentProperty(name, value);
            this.ModifiedProperties = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name">
        /// 
        /// </param>
        /// <param name="value">
        /// 
        /// </param>
        public void SetDependentProperty(string name, string value)
        {
            Project project = this.Model as Project;
            if (project == null)
            {
                return;
            }

            project.SetDependentProperty(name, value);
            this.ModifiedProperties = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name">
        /// 
        /// </param>
        /// <param name="value">
        /// 
        /// </param>
        public void SetDependentBoolProperty(string name, bool value)
        {
            this.SetDependentProperty(name, value.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>
        /// 
        /// </returns>
        public void Load()
        {
            this.Text = Path.GetFileNameWithoutExtension(this.Model.Include) + " (loading)";
            try
            {
                this.ClearChildren();
                Project project = new Project(this.Model);
                string fullPath = this.Model.GetMetadata("FullPath");
                if (!File.Exists(fullPath))
                {
                    throw new FileNotFoundException();
                }

                project.Load(this.Model.GetMetadata("FullPath"));
                this.ReplaceModel(project, false);

                this.ClearChildren();
                foreach (CoreFolderNode coreNode in this.CreateCoreFolders())
                {
                    this.AddChild(coreNode);
                }

                this.ProcessFilters();
                this.ProcessFiles();
                this.ProcessConfigFiles();

                this.Text = Path.GetFileNameWithoutExtension(this.Model.Include);
                this.PostLoad();
                this.IsExpandable = this.Items.Count > 0;
                this._loadFailed = false;
                this.NotifyPropertyChanged("Icon");
            }
            catch (Exception ex)
            {
                this._loadFailed = true;
                IHierarchyNode parent = this.Parent;
                ProjectCollectionNode collection = parent as ProjectCollectionNode;
                while (parent != null && collection == null)
                {
                    parent = parent.Parent;
                    collection = parent as ProjectCollectionNode;
                }

                if (collection != null)
                {
                    collection.OnProjectFailed(this, ex, false);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>
        /// 
        /// </returns>
        public async Task LoadAsync()
        {
            this.Text = Path.GetFileNameWithoutExtension(this.Model.Include) + " (loading)";
            Task task = Task.Factory.StartNew(
                new Action(() =>
                {
                    try
                    {
                        this.ClearChildren();
                        Project project = new Project(this.Model);
                        string fullPath = this.Model.GetMetadata("FullPath");
                        if (!File.Exists(fullPath))
                        {
                            throw new FileNotFoundException();
                        }

                        project.Load(this.Model.GetMetadata("FullPath"));
                        this.ReplaceModel(project, false);

                        this.ClearChildren();
                        foreach (CoreFolderNode coreNode in this.CreateCoreFolders())
                        {
                            this.AddChild(coreNode);
                        }

                        this.ProcessFilters();
                        this.ProcessFiles();
                        this.ProcessConfigFiles();

                        this.Text = Path.GetFileNameWithoutExtension(this.Model.Include);
                        this.PostLoad();
                        this.IsExpandable = this.Items.Count > 0;
                        this._loadFailed = false;
                        this.NotifyPropertyChanged("Icon");
                    }
                    catch (Exception ex)
                    {
                        this._loadFailed = true;
                        IHierarchyNode parent = this.Parent;
                        ProjectCollectionNode collection = parent as ProjectCollectionNode;
                        while (parent != null && collection == null)
                        {
                            parent = parent.Parent;
                            collection = parent as ProjectCollectionNode;
                        }

                        if (collection != null)
                        {
                            collection.OnProjectFailed(this, ex, false);
                        }
                    }
                }));

            await Task.WhenAll(task);
        }

        /// <summary>
        /// Saves the data that is associated with this item to the specified file.
        /// </summary>
        /// <param name="stream">
        /// The io stream that data for this object should be written to.
        /// </param>
        /// <returns>
        /// A value indicating whether the save operation was successful.
        /// </returns>
        public bool Save(Stream stream)
        {
            return Save(stream, MetadataLevel.Core);
        }

        /// <summary>
        /// Saves the current state of this instances specified metadata level to the specified
        /// stream.
        /// </summary>
        /// <param name="stream">
        /// The stream to write this instances data to.
        /// </param>
        /// <param name="metadataLevel">
        /// The metadata level that needs to be serialised into the specified stream.
        /// </param>
        public bool Save(Stream stream, MetadataLevel metadataLevel)
        {
            Project project = this.Model as Project;
            if (project == null)
            {
                return false;
            }

            bool success = project.Save(stream, metadataLevel);

            if(success)
            {
                IHierarchyNode parent = this.Parent;
                ProjectCollectionNode collection = parent as ProjectCollectionNode;
                while (parent != null && collection == null)
                {
                    parent = parent.Parent;
                    collection = parent as ProjectCollectionNode;
                }

                if (collection != null)
                {
                    collection.OnProjectSaved(this);
                }
            }

            return success;
        }

        /// <summary>
        /// Override to create additional folders in your project that are core to your project
        /// but not defined in the project file.
        /// </summary>
        /// <returns>
        /// A list of core folder nodes created by this project.
        /// </returns>
        protected virtual IList<CoreFolderNode> CreateCoreFolders()
        {
            return new List<CoreFolderNode>();
        }

        /// <summary>
        /// Loads folders from the project file into the hierarchy.
        /// </summary>
        protected virtual void ProcessFilters()
        {
            foreach (ProjectItem folder in this["Filter"])
            {
                this.CreateFolderNodes(folder.Include);
            }

            foreach (ProjectItem folder in this["Folder"])
            {
                this.CreateFolderNodes(folder.Include);
            }

            var folders = this["DynamicFolder"].ToList();
            foreach (ProjectItem folder in folders)
            {
                this.CreateDynamicFolderNodes(folder.Include);
            }
        }

        /// <summary>
        /// Loads folders from the project file into the hierarchy.
        /// </summary>
        protected virtual void ProcessFiles()
        {
            foreach (ProjectItem file in this["File"])
            {
                string filter = file.GetMetadata("Filter");
                if (filter != null)
                {
                    FolderNode node = CreateFolderNodes(filter.TrimEnd('\\') + "\\");
                    if (node != null)
                    {
                        node.AddChild(this.CreateFileNode(node, file));
                        continue;
                    }

                    Debug.Assert(false, "Failed to create parent for file.");
                }

                this.AddChild(this.CreateFileNode(this, file));
            }
        }

        /// <summary>
        /// Loads folders from the project configuration files into the hierarchy.
        /// </summary>
        protected virtual void ProcessConfigFiles()
        {
            foreach (ProjectItem file in this["ConfigFile"])
            {
                string filter = file.GetMetadata("Filter");
                if (filter != null)
                {
                    FolderNode node = CreateFolderNodes(filter.TrimEnd('\\') + "\\");
                    if (node != null)
                    {
                        node.AddChild(this.CreateFileNode(node, file));
                        continue;
                    }

                    Debug.Assert(false, "Failed to create parent for file.");
                }

                this.AddChild(this.CreateFileNode(this, file));
            }
        }

        /// <summary>
        /// Performs all post load actions.
        /// </summary>
        protected virtual void PostLoad()
        {
            IHierarchyNode parent = this.Parent;
            ProjectCollectionNode collection = parent as ProjectCollectionNode;
            while (parent != null && collection == null)
            {
                parent = parent.Parent;
                collection = parent as ProjectCollectionNode;
            }

            if (collection != null)
            {
                collection.OnProjectLoaded(this);
            }
        }

        /// <summary>
        /// Performs all post load actions.
        /// </summary>
        protected virtual void PostUnload()
        {
            ProjectCollectionNode collection = this.Parent as ProjectCollectionNode;
            if (collection != null)
            {
                collection.OnProjectUnloaded(this);
            }
        }

        /// <summary>
        /// Walks the subpaths of a project relative path and checks if the folder nodes
        /// hierarchy is already there, if not creates it.
        /// </summary>
        /// <param name="path">
        /// Path of the folder.
        /// </param>
        public virtual FileNode CreateFileNode(IHierarchyNode parent, ProjectItem item)
        {
            return new FileNode(parent, item);
        }

        /// <summary>
        /// Walks the subpaths of a project relative path and checks if the folder nodes
        /// hierarchy is already there, if not creates it.
        /// </summary>
        /// <param name="path">
        /// Path of the folder.
        /// </param>
        protected virtual FolderNode CreateFolderNodes(string path)
        {
            if (String.IsNullOrEmpty(path))
            {
                throw new SmartArgumentNullException(() => path);
            }

            string[] parts = path.Split(new char[] { Path.DirectorySeparatorChar }, StringSplitOptions.RemoveEmptyEntries);
            FolderNode currentParent = null;
            path = String.Empty;

            for (int i = 0; i < parts.Length; i++)
            {
                if (parts[i].Length > 0 && !String.Equals(parts[i], ".."))
                {
                    path += parts[i] + "\\";
                    currentParent = VerifySubFolderExists(path, currentParent);
                }
            }

            return currentParent;
        }

        /// <summary>
        /// Walks the subpaths of a project relative path and checks if the folder nodes
        /// hierarchy is already there, if not creates it.
        /// </summary>
        /// <param name="path">
        /// Path of the folder.
        /// </param>
        protected virtual ProjectDynamicFolderNode CreateDynamicFolderNodes(string path)
        {
            if (String.IsNullOrEmpty(path))
            {
                throw new SmartArgumentNullException(() => path);
            }

            string[] parts = path.Split(Path.DirectorySeparatorChar);
            ProjectDynamicFolderNode currentParent = null;
            path = String.Empty;

            for (int i = 0; i < parts.Length; i++)
            {
                if (parts[i].Length > 0 && !String.Equals(parts[i], ".."))
                {
                    path += parts[i] + "\\";
                    currentParent = VerifySubFolderExists(path, currentParent);
                }
            }

            return currentParent;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        protected virtual ProjectDynamicFolderNode VerifySubFolderExists(
            string path, ProjectDynamicFolderNode parent)
        {
            path = path.TrimEnd('\\');
            ProjectDynamicFolderNode folderNode = null;
            IHierarchyNode parentNode = parent;
            if (parentNode == null)
            {
                parentNode = this;
            }

            foreach (IHierarchyNode node in parentNode.GetChildren(false))
            {
                if (node.Model == null || node.Include == null)
                {
                    continue;
                }

                string nodeInclude = node.Include.TrimEnd('\\');
                while (nodeInclude.StartsWith("..\\"))
                {
                    nodeInclude = nodeInclude.Substring(3);
                }

                if (String.Equals(nodeInclude, path, StringComparison.OrdinalIgnoreCase))
                {
                    Debug.Assert(node is ProjectDynamicFolderNode, "Not a FolderNode");
                    folderNode = node as ProjectDynamicFolderNode;
                    break;
                }
            }

            if (folderNode == null && path != null)
            {
                ProjectItem item = null;
                foreach (ProjectItem folder in this["DynamicFolder"])
                {
                    string folderInclude = folder.Include.TrimEnd('\\');
                    while (folderInclude.StartsWith("..\\"))
                    {
                        folderInclude = folderInclude.Substring(3);
                    }

                    if (String.Equals(folderInclude, path, StringComparison.OrdinalIgnoreCase))
                    {
                        item = folder;
                        break;
                    }
                }

                if (item == null)
                {
                    IProjectItemScope scope = this.Model as IProjectItemScope;
                    item = scope.AddNewProjectItem("DynamicFolder", path.TrimEnd('\\'));
                }

                string text = Path.GetFileName(item.Include);
                folderNode = this.CreateDynamicFolderNode(parentNode, item, text);
                parentNode.AddChild(folderNode);
            }

            return folderNode;
        }

        /// <summary>
        /// To support virtual folders, override this method to return your own folder nodes
        /// </summary>
        /// <param name="path">Path to store for this folder</param>
        /// <param name="element">Element corresponding to the folder</param>
        /// <returns>A FolderNode that can then be added to the hierarchy</returns>
        protected virtual FolderNode CreateFolderNode(IHierarchyNode parent, ProjectItem item)
        {
            return new ProjectFolderNode(parent, item);
        }

        /// <summary>
        /// To support virtual folders, override this method to return your own folder nodes
        /// </summary>
        /// <param name="path">Path to store for this folder</param>
        /// <param name="item">Element corresponding to the folder</param>
        /// <param name="text">The text to use for this folder node.</param>
        /// <returns>A FolderNode that can then be added to the hierarchy</returns>
        protected virtual ProjectDynamicFolderNode CreateDynamicFolderNode(IHierarchyNode parent, ProjectItem item, string text)
        {
            return new ProjectDynamicFolderNode(parent, item, text);
        }

        /// <summary>
        /// Takes a path and verifies that we have a node with that name.
        /// </summary>
        /// <param name="path">
        /// Full path to the subfolder we want to verify.
        /// </param>
        /// <param name="parent">
        /// The parent node where to add the subfolder if it does not exist.
        /// </param>
        /// <returns>
        /// The foldernode corresponding to the path.
        /// </returns>
        protected virtual FolderNode VerifySubFolderExists(string path, FolderNode parent)
        {
            path = path.TrimEnd('\\');
            FolderNode folderNode = null;
            IHierarchyNode parentNode = parent;
            if (parentNode == null)
            {
                parentNode = this;
            }

            List<IHierarchyNode> children = parentNode.GetChildren(false).ToList();
            foreach (IHierarchyNode node in children)
            {
                if (node.Model == null || node.Include == null)
                {
                    continue;
                }

                string nodeInclude = node.Include.TrimEnd('\\');
                while (nodeInclude.StartsWith("..\\"))
                {
                    nodeInclude = nodeInclude.Substring(3);
                }

                if (String.Equals(nodeInclude, path, StringComparison.OrdinalIgnoreCase))
                {
                    Debug.Assert(node is FolderNode, "Not a FolderNode");
                    folderNode = node as FolderNode;
                    break;
                }
            }

            if (folderNode == null && path != null)
            {
                ProjectItem item = null;
                foreach (ProjectItem folder in this["Folder"])
                {
                    string folderInclude = folder.Include.TrimEnd('\\');
                    if (String.Equals(folderInclude, path, StringComparison.OrdinalIgnoreCase))
                    {
                        item = folder;
                        break;
                    }
                }

                if (item == null)
                {
                    item = this.Model.Scope.AddNewProjectItem("Folder", path.TrimEnd('\\'));
                }

                folderNode = this.CreateFolderNode(parentNode, item);
                parentNode.AddChild(folderNode);
            }

            return folderNode;
        }

        /// <summary>
        /// Unloads this project.
        /// </summary>
        public void Unload()
        {
            Project project = this.Model as Project;
            if (project == null)
            {
                Debug.Assert(false, "Unable to unload a project that hasn't yet been loaded");
                return;
            }

            this.ClearChildren();
            this.ReplaceModel(project.DefinitionItem, false);
            this.Text = Path.GetFileNameWithoutExtension(this.Model.Include);
            this.Text += " (unavailable)";
            this.AddChild(new HierarchyNode() { Text = "The project file was unloaded." });
            this.NotifyPropertyChanged("Icon");

            IHierarchyNode parent = this.Parent;
            ProjectCollectionNode collection = parent as ProjectCollectionNode;
            while (parent != null && collection == null)
            {
                parent = parent.Parent;
                collection = parent as ProjectCollectionNode;
            }

            this.PostUnload();
        }

        /// <summary>
        /// Adds a new dynamic folder underneath this node and returns the instance to the
        /// new folder.
        /// </summary>
        /// <param name="path">
        /// The full path to the dynamic directory. 
        /// </param>
        /// <param name="filter">
        /// The file filter string that is used to get the files that will be shown in this
        /// dynamic directory.
        /// </param>
        /// <param name="recursive">
        /// A value indicating whether this is recursive and will show sub dynamic directories
        /// or not.
        /// </param>
        /// <returns>
        /// The new dynamic folder that was created.
        /// </returns>
        public ProjectDynamicFolderNode AddNewDynamicFolder(string path, string filter, bool recursive)
        {
            Project model = this.Model as Project;
            if (model == null)
            {
                return null;
            }

            string relative = Shlwapi.MakeRelativeFromFileToDirectory(model.FullPath, path);
            if (String.Equals(relative, "."))
            {
                relative = String.Empty;
            }

            ProjectItem item = model.AddNewProjectItem("DynamicFolder", relative);
            item.SetMetadata("Filters", filter, MetadataLevel.Core);
            item.SetMetadata("Recursive", recursive.ToString(), MetadataLevel.Core);

            string text = Path.GetFileName(path);
            var node = new ProjectDynamicFolderNode(this, item, Path.GetFileName(path));
            this.AddChild(node);
            return node;
        }

        /// <summary>
        /// Adds a new folder underneath this node and returns the instance to the new folder.
        /// </summary>
        /// <returns>
        /// The new folder that was created.
        /// </returns>
        public override IHierarchyNode AddNewFolder()
        {
            string include = "NewFolder";
            Project model = this.Model as Project;
            if (model == null)
            {
                return null;
            }

            include = model.GetUniqueInclude(include);
            ProjectItem item = model.AddNewProjectItem("Folder", include);
            HierarchyNode node = new ProjectFolderNode(this, item);
            this.AddChild(node);
            return node;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Reload()
        {
            Project project = this.Model as Project;
            if (project != null)
            {
                Debug.Assert(false, "Unable to reload a project that hasn't been unloaded");
                return;
            }

            this.Text = Path.GetFileNameWithoutExtension(this.Model.Include) + " (loading)";
            try
            {
                this.ClearChildren();
                project = new Project(this.Model);
                string fullPath = this.Model.GetMetadata("FullPath");
                if (!File.Exists(fullPath))
                {
                    string msg = StringTable.MissingProjectFileMsg.FormatCurrent(fullPath);
                    throw new FileNotFoundException(msg);
                }

                project.Load(this.Model.GetMetadata("FullPath"));
                this.ReplaceModel(project, false);

                this.ClearChildren();
                foreach (CoreFolderNode coreNode in this.CreateCoreFolders())
                {
                    this.AddChild(coreNode);
                }

                this.ProcessFilters();
                this.ProcessFiles();
                this.ProcessConfigFiles();

                this.Text = Path.GetFileNameWithoutExtension(this.Model.Include);
                this.PostLoad();
                this.IsExpandable = this.Items.Count > 0;
                this._loadFailed = false;
                this.NotifyPropertyChanged("Icon");
            }
            catch (Exception ex)
            {
                this._loadFailed = true;
                IHierarchyNode parent = this.Parent;
                ProjectCollectionNode collection = parent as ProjectCollectionNode;
                while (parent != null && collection == null)
                {
                    parent = parent.Parent;
                    collection = parent as ProjectCollectionNode;
                }

                if (collection != null)
                {
                    collection.OnProjectFailed(this, ex, true);
                }
            }
        }
        #endregion Methods
    } // RSG.Project.Model.ProjectNode {Class}
} // RSG.Project.Model {Namespace}
