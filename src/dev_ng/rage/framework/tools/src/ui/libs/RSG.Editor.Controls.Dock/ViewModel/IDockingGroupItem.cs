﻿//---------------------------------------------------------------------------------------------
// <copyright file="IDockingGroupItem.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock.ViewModel
{
    using System.Collections.Generic;

    /// <summary>
    /// When implemented represents a docking element that can be placed inside a docking
    /// group as one of its children.
    /// </summary>
    public interface IDockingGroupItem
    {
        #region Properties
        /// <summary>
        /// Gets or sets the index for this item inside its parent group.
        /// </summary>
        int GroupIndex { get; set; }

        /// <summary>
        /// Gets or sets the DockingGroup that is the parent to this element.
        /// </summary>
        DockingGroup ParentGroup { get; set; }

        /// <summary>
        /// Gets or sets the splitter length of this item inside its parent group.
        /// </summary>
        SplitterLength SplitterLength { get; set; }

        /// <summary>
        /// Gets a iterator around all of the document items currently opened inside this
        /// docking element.
        /// </summary>
        IEnumerable<DocumentItem> AllDocuments { get; }
        #endregion Properties
    } // RSG.Editor.Controls.Dock.ViewModel.IDockingGroupItem {Interface}
} // RSG.Editor.Controls.Dock.ViewModel {Namespace}
