﻿//---------------------------------------------------------------------------------------------
// <copyright file="MultiCommandItemDefinition.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Windows.Input;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Represents the definition to use for a single multi item command item.
    /// </summary>
    internal class MultiCommandItemDefinition
        : CommandDefinition, ICommandWithToggle, ICommandWithParameter
    {
        #region Fields
        /// <summary>
        /// The private reference to the definition item that is being wrapped.
        /// </summary>
        private IMultiCommandItem _item;

        /// <summary>
        /// The private field used for the <see cref="OriginalDefinition"/> property.
        /// </summary>
        private MultiCommand _originalDefinition;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MultiCommandItemDefinition"/> class.
        /// </summary>
        /// <param name="item">
        /// The definition item that is being wrapped.
        /// </param>
        /// <param name="definition">
        /// The original command definition that has had its items expanded.
        /// </param>
        public MultiCommandItemDefinition(IMultiCommandItem item, MultiCommand definition)
        {
            this._originalDefinition = definition;
            this._item = item;
            PropertyChangedEventManager.AddHandler(item, this.OnItemChanged, String.Empty);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the command category name this definition belongs to.
        /// </summary>
        public override string CategoryName
        {
            get { return this._item.ParentDefinition.CategoryName; }
        }

        /// <summary>
        /// Gets the System.Windows.Input.RoutedCommand that this definition is wrapping.
        /// </summary>
        public override RockstarRoutedCommand Command
        {
            get { return this._item.ParentDefinition.Command; }
        }

        /// <summary>
        /// Gets the parameter that is routed through the element tree with the command.
        /// </summary>
        public object CommandParameter
        {
            get { return this._item.GetCommandParameter(); }
        }

        /// <summary>
        /// Gets the description for this command. This is also what is shown as a tooltip
        /// along with the key gesture if one is set.
        /// </summary>
        public override string Description
        {
            get { return this._item.Description; }
        }

        /// <summary>
        /// Gets the System.Windows.Media.Imaging.BitmapSource object that represents the icon
        /// that is used for this command, this can be null.
        /// </summary>
        public override BitmapSource Icon
        {
            get { return this._item.Icon; }
        }

        /// <summary>
        /// Gets the parameter that is routed through the element tree with the command.
        /// </summary>
        object ICommandWithParameter.CommandParameter
        {
            get { return this.CommandParameter; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this definition is currently in the toggled
        /// state or not.
        /// </summary>
        bool ICommandWithToggle.IsToggled
        {
            get { return this.IsToggled; }
            set { this.IsToggled = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this definition is currently in the toggled
        /// state or not.
        /// </summary>
        public bool IsToggled
        {
            get { return this._item.IsToggled; }
            set { this._item.IsToggled = value; }
        }

        /// <summary>
        /// Gets or sets the System.Windows.Input.KeyGesture object that is associated with
        /// this command definition.
        /// </summary>
        public override KeyGesture KeyGesture
        {
            get { return this._item.KeyGesture; }
            set { this._item.KeyGesture = value; }
        }

        /// <summary>
        /// Gets the collection containing the multi items that make up the items in the
        /// command that this definition has been expanded into.
        /// </summary>
        public ObservableCollection<IMultiCommandItem> MultiCommandItems
        {
            get { return this._item.ParentDefinition.Items; }
        }

        /// <summary>
        /// Gets the reference to the original command definition that has been expanded into
        /// multiple items.
        /// </summary>
        public MultiCommand OriginalDefinition
        {
            get { return this._originalDefinition; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Gets called whenever a property on the associated item changes.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.ComponentModel.PropertyChangedEventArgs event data.
        /// </param>
        private void OnItemChanged(object s, PropertyChangedEventArgs e)
        {
            this.NotifyPropertyChanged(e.PropertyName);
        }
        #endregion Methods
    } // RSG.Editor.MultiCommandItemDefinition {Class}
} // RSG.Editor {Namespace}
