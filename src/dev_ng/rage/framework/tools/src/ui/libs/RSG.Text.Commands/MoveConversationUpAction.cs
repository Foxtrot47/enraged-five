﻿//---------------------------------------------------------------------------------------------
// <copyright file="MoveConversationUpAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Commands
{
    using System.Linq;
    using System.Windows.Input;
    using RSG.Editor;
    using RSG.Text.ViewModel;

    /// <summary>
    /// Implements the <see cref="TextCommands.MoveConversationUp"/> routed command.
    /// </summary>
    public class MoveConversationUpAction : ButtonAction<TextConversationCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MoveConversationUpAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public MoveConversationUpAction(
            ParameterResolverDelegate<TextConversationCommandArgs> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(TextConversationCommandArgs args)
        {
            if (args.Selected == null || args.Selected.Count() != 1)
            {
                return false;
            }

            return args.DataGrid.SelectedIndex != 0;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(TextConversationCommandArgs args)
        {
            int index = args.DataGrid.SelectedIndex;
            foreach (ConversationViewModel conversationViewModel in args.Selected)
            {
                args.Dialogue.Model.MoveConversation(conversationViewModel.Model, index - 1);
            }

            FocusNavigationDirection direction = FocusNavigationDirection.Next;
            args.DataGrid.MoveFocus(new TraversalRequest(direction));
        }
        #endregion Methods
    } // RSG.Text.Commands.MoveConversationUpAction {Class}
} // RSG.Text.Commands {Namespace}
