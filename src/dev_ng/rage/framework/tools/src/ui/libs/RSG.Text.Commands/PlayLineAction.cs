﻿//---------------------------------------------------------------------------------------------
// <copyright file="PlayLineAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Commands
{
    using RSG.Editor;
    using RSG.Text.ViewModel;
    using RSG.Text.Model;
    using System.Diagnostics;
    using System.IO;
    using System.Windows.Media;
    using System.Collections.Generic;
    using System;
    using System.Windows;
    using System.Linq;

    /// <summary>
    /// Implements the <see cref="TextCommands.PlayLine"/> command.
    /// </summary>
    public class PlayLineAction : ButtonAction<TextLineCommandArgs, LineViewModel>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="PlayLineAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public PlayLineAction(ParameterResolverDelegate<TextLineCommandArgs> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="target">
        /// A override target that is used if it's not equal to null.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(TextLineCommandArgs args, LineViewModel target)
        {
            if (target != null)
            {
                if (String.IsNullOrWhiteSpace(target.AudioFilepath))
                {
                    return false;
                }

                if (target.AudioFilepath.StartsWith("SFX_PAUSE"))
                {
                    return false;
                }

                return true;
            }

            if (args.Conversation == null)
            {
                return false;
            }

            if (args.DataGrid.SelectedIndex < 0)
                return false;

            int index = args.DataGrid.SelectedIndex;
            LineViewModel item = args.Conversation.Lines[index];
            ILine line = item.Model;

            if (line.AudioFilepath.StartsWith("SFX_PAUSE"))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="target">
        /// A override target that is used if it's not equal to null as the line that is
        /// played.
        /// </param>
        public override void Execute(TextLineCommandArgs args, LineViewModel target)
        {
            if (target == null)
            {
                int index = args.DataGrid.SelectedIndex;
                target = args.Conversation.Lines[index];
            }

            ILine line = target.Model;
            string characterName = String.Empty;
            if (target.SelectedCharacter != null)
            {
                characterName = target.SelectedCharacter.Name;
            }

            bool isRandom = args.Conversation.IsRandom;
            string audioBranch = args.AudioPath;
            string wavDepotFilepath = PlayLineHelpers.GetWavDepotFilepathForLine(line, isRandom, characterName, audioBranch);
            string wavLocalFilepath = null;
            string notFoundFilePath = wavDepotFilepath;

            IPerforceService service = this.GetService<IPerforceService>();
            if (service == null)
            {
                Debug.Assert(false, "Unable to check out item due to missing service");
                return;
            }

            try
            {
                IList<string> result = service.GetLocalPathsFromDepotPaths(new List<string>() { wavDepotFilepath });
                bool found = false;
                if (result.Count > 0)
                {
                    //If there are multiple results from our wild card lookup just take the first one
                    wavLocalFilepath = result.First();
                    service.GetLatest(new List<string>() { wavLocalFilepath });
                    if (File.Exists(wavLocalFilepath))
                    {
                        found = true;
                    }
                    else
                    {
                        notFoundFilePath = wavLocalFilepath;
                    }
                }

                if(!found)
                {
                    IMessageBoxService mesageBoxService = this.GetService<IMessageBoxService>();
                    if (mesageBoxService != null)
                    {
                        mesageBoxService.Show("Unable to find the wav file for the the selected line.\r\n\r\n" +
                            notFoundFilePath,
                            "Unresolved wav filepath",
                            MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return;
            }

            if (wavLocalFilepath != null)
            {
                AudioPlayer.Play(wavLocalFilepath);
            }
        }
        #endregion Methods
    } // RSG.Text.Commands.PlayLineAction {Class}

    /// <summary>
    /// Helper class for play line actions
    /// </summary>
    public static class PlayLineHelpers
    {
        /// <summary>
        /// Get file path based on line parameters
        /// </summary>
        /// <param name="line"></param>
        /// <param name="isRandom"></param>
        /// <param name="characterName"></param>
        /// <param name="audioBranch"></param>
        /// <returns></returns>
        public static string GetWavDepotFilepathForLine(ILine line, bool isRandom, string characterName, string audioBranch)
        {
            string[] filenameSplit = line.AudioFilepath.Split('_');

            // The wav file gets postfixed with _01.
            string lineFilenameWithPostfix = string.Format("{0}.wav", line.AudioFilepath);

            // Convert X:\rdr3\audio\dev\assets\waves to //rdr3/audio/dev/assets/waves
            string wavesDepotPath;

            // Folder structure depends on whether the line has been recorded or not.
            if (!line.Recorded)
            {
                wavesDepotPath = Path.Combine(
                    audioBranch,
                    "Assets",
                    "Waves",
                    "PS_*",
                    "Placeholder_Speech",
                    filenameSplit[0] + "_placeholder",
                    line.Conversation.Root,
                    characterName,
                    lineFilenameWithPostfix);
            }
            else
            {
                wavesDepotPath = Path.Combine(
                    audioBranch,
                    "Assets",
                    "Waves",
                    "SS_*",
                    "Scripted_Speech",
                    filenameSplit[0],
                    characterName,
                    lineFilenameWithPostfix);
            }
            wavesDepotPath = string.Format("//{0}", wavesDepotPath.Replace('\\', '/').Substring(3));
            return wavesDepotPath;
        }
    }

} // RSG.Text.Commands {Namespace}