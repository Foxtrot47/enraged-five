﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor;
using RSG.Interop.Microsoft.Office;
using RSG.Interop.Microsoft.Office.Outlook;
using RSG.UniversalLog.ViewModel;

namespace RSG.UniversalLog.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public class EmailAction : ButtonAction<UniversalLogDataContext>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="EmailImplementer"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public EmailAction(ParameterResolverDelegate<UniversalLogDataContext> resolver)
            : base(resolver)
        {
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// Override to set logic against the can execute command handler.
        /// </summary>
        /// <param name="commandParameter"></param>
        /// <returns></returns>
        public override bool CanExecute(UniversalLogDataContext dc)
        {
            return dc.AnyFilesLoaded;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="commandParameter">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(UniversalLogDataContext dc)
        {
            try
            {
                using (IApplication app = OutlookApplication.Create())
                {
                    object obj = app.CreateItem(OlItemType.olMailItem);
                    using (IMailItem newMail = (IMailItem)COMWrapper.Wrap(obj, typeof(IMailItem)))
                    {
                        // Fill in the default information.
                        newMail.Recipients.Add("*tools@rockstarnorth.com");
                        newMail.Recipients.ResolveAll();
                        newMail.Subject = String.Format("Universal Log Report from {0}", Environment.UserName);
                        newMail.Body = "Additional Information:\n\n";
                        newMail.Save();

                        IList<String> allFilepaths = dc.LoadedFileViewModels.Select(item => Path.GetFullPath(item.Filepath)).ToList();
                        String commonRootPath = GetCommonRootPath(allFilepaths);

                        // Add all the open files to a temporary zip file and attach it to the email.
                        String zipFilename = String.Format("{0}_{1}.ulogzip", Environment.UserName, DateTime.UtcNow.ToString("yyyy.MM.dd_HH.mm.ss.fff"));
                        String zipFilepath = Path.Combine(Path.GetTempPath(), zipFilename);
                        using (ZipArchive zipFile = ZipFile.Open(zipFilepath, ZipArchiveMode.Create))
                        {
                            foreach (String ulogFilename in dc.LoadedFileViewModels.Select(item => Path.GetFullPath(item.Filepath)))
                            {
                                String entryName = ulogFilename.Replace(commonRootPath, "").TrimStart(Path.DirectorySeparatorChar);
                                zipFile.CreateEntryFromFile(ulogFilename, entryName, CompressionLevel.Optimal);
                            }
                        }

                        newMail.Attachments.Add(zipFilepath, (int)OlAttachmentType.olByValue, newMail.Body.Length + 1, zipFilename);
                        newMail.Display(false);
                    }
                }
            }
            catch (System.Exception e)
            {
                IMessageBoxService msgService = this.GetService<IMessageBoxService>();
                if (msgService == null)
                {
                    Debug.Assert(false, "Unable to save file as service is missing.");
                    return;
                }

                msgService.Show("A problem occurred while attempting to create a new email using Outlook.  Please contact " +
                    "the RSGEDI Tools mailing list including any information about what you were doing and might find relevant.");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filepaths"></param>
        /// <returns></returns>
        private String GetCommonRootPath(IList<String> filepaths)
        {
            IList<IList<String>> allDirectoryComponents =
                filepaths.Select(item => GetDirectoryComponents(item))
                         .OrderBy(item => item.Count)
                         .ToList();


            // Check the first entry against all the other files.
            IList<String> commonComponents = new List<String>();
            bool different = false;
            
            IList<String> firstDirectoryComponents = allDirectoryComponents.First();
            for (int componentIdx = 0; componentIdx < firstDirectoryComponents.Count; ++componentIdx)
            {
                String componentToMatchAgainst = firstDirectoryComponents[componentIdx];

                for (int otherFilepathIdx = 1; otherFilepathIdx < allDirectoryComponents.Count; ++otherFilepathIdx)
                {
                    String otherComponent = allDirectoryComponents[otherFilepathIdx][componentIdx];
                    if (componentToMatchAgainst != otherComponent)
                    {
                        different = true;
                        break;
                    }
                }

                if (different)
                {
                    break;
                }
                else
                {
                    commonComponents.Add(componentToMatchAgainst);
                }
            }

            return String.Join(Path.DirectorySeparatorChar.ToString(), commonComponents);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filepath"></param>
        /// <returns></returns>
        private IList<String> GetDirectoryComponents(String filepath)
        {
            return Path.GetDirectoryName(filepath).Split(new char[] { Path.DirectorySeparatorChar });
        }
        #endregion // Overrides
    } // EmailAction
}
