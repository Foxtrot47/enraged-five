﻿//---------------------------------------------------------------------------------------------
// <copyright file="SelectTunableDelegate.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel
{
    using RSG.Metadata.ViewModel.Tunables;

    /// <summary>
    /// Represents the method that can be used to select a single tunable view mode when
    /// needed in the view while inside a command action.
    /// </summary>
    /// <param name="tunable">
    /// The tunable that needs to be selected.
    /// </param>
    public delegate void SelectTunableDelegate(ITunableViewModel tunable);
} // RSG.Metadata.ViewModel {Namespace}
