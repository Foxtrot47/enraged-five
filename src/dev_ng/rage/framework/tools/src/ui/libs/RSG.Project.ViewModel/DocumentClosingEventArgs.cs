﻿//---------------------------------------------------------------------------------------------
// <copyright file="DocumentClosingEventArgs.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel
{
    using System;
    using RSG.Editor.Controls.Dock.ViewModel;

    /// <summary>
    /// Provides data for the event representing a document object being closed by the user.
    /// </summary>
    public class DocumentClosingEventArgs : DocumentEventArgs
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DocumentClosingEventArgs"/> class.
        /// </summary>
        /// <param name="documentItem">
        /// The document that is being closed.
        /// </param>
        public DocumentClosingEventArgs(DocumentItem documentItem)
            : base(documentItem)
        {
        }
        #endregion Constructors
    } // RSG.Project.ViewModel.DocumentClosingEventArgs {Class}
} // RSG.Project.ViewModel {Namespace}
