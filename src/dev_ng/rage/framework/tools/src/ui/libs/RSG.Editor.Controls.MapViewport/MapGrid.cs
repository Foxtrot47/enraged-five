﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace RSG.Editor.Controls.MapViewport
{
    /// <summary>
    /// Used for rendering a square grid composed of major and minor lines which are
    /// optionally rendered using different brushes.
    /// </summary>
    public class MapGrid : FrameworkElement
    {
        #region Map Bounds Depedency Property
        /// <summary>
        /// Identifies the MapBounds dependency property.
        /// </summary>
        public static readonly DependencyProperty MapBoundsProperty =
            DependencyProperty.Register(
                "MapBounds",
                typeof(Rect),
                typeof(MapGrid),
                new FrameworkPropertyMetadata(Rect.Empty, FrameworkPropertyMetadataOptions.AffectsRender));

        /// <summary>
        /// Gets or sets the grids map bounds dependency property.
        /// </summary>
        public Rect MapBounds
        {
            get { return (Rect)GetValue(MapBoundsProperty); }
            set { SetValue(MapBoundsProperty, value); }
        }
        #endregion

        #region Scale Dependency Property
        /// <summary>
        /// Identifies the <see cref="Scale"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ScaleProperty =
            DependencyProperty.Register(
                "Scale",
                typeof(Vector),
                typeof(MapGrid),
                new FrameworkPropertyMetadata(new Vector(1.0, 1.0), FrameworkPropertyMetadataOptions.AffectsRender));

        /// <summary>
        /// Gets or sets the map grids scale.
        /// </summary>
        public Vector Scale
        {
            get { return (Vector)GetValue(ScaleProperty); }
            set { SetValue(ScaleProperty, value); }
        }
        #endregion

        #region Line Spacing Dependency Properties
        /// <summary>
        /// Identifies the MajorLineSpacing dependency property.
        /// </summary>
        public static readonly DependencyProperty MajorLineSpacingProperty =
            DependencyProperty.Register(
                "MajorLineSpacing",
                typeof(double),
                typeof(MapGrid),
                new FrameworkPropertyMetadata(1000.0, FrameworkPropertyMetadataOptions.AffectsRender));

        /// <summary>
        /// Gets or sets the grids major line spacing dependency property.
        /// </summary>
        public double MajorLineSpacing
        {
            get { return (double)GetValue(MajorLineSpacingProperty); }
            set { SetValue(MajorLineSpacingProperty, value); }
        }

        /// <summary>
        /// Identifies the MinorLineSpacing dependency property.
        /// </summary>
        public static readonly DependencyProperty MinorLineSpacingProperty =
            DependencyProperty.Register(
                "MinorLineSpacing",
                typeof(double),
                typeof(MapGrid),
                new FrameworkPropertyMetadata(100.0, FrameworkPropertyMetadataOptions.AffectsRender));

        /// <summary>
        /// Gets or sets the grids minor line spacing dependency property.
        /// </summary>
        public double MinorLineSpacing
        {
            get { return (double)GetValue(MinorLineSpacingProperty); }
            set { SetValue(MinorLineSpacingProperty, value); }
        }

        #endregion

        #region Render Style Dependency Properties
        /// <summary>
        /// Dependency property for specifying the major line stroke brush.
        /// </summary>
        public static readonly DependencyProperty MajorLineStrokeProperty =
            DependencyProperty.Register(
                "MajorLineStroke",
                typeof(Brush),
                typeof(MapGrid),
                new FrameworkPropertyMetadata(Brushes.Transparent, FrameworkPropertyMetadataOptions.AffectsRender));

        /// <summary>
        /// Property to access the stroke brush.
        /// </summary>
        public Brush MajorLineStroke
        {
            get { return (Brush)GetValue(MajorLineStrokeProperty); }
            set { SetValue(MajorLineStrokeProperty, value); }
        }

        /// <summary>
        /// Dependency property for specifying the minor line stroke brush.
        /// </summary>
        public static readonly DependencyProperty MinorLineStrokeProperty =
            DependencyProperty.Register(
                "MinorLineStroke",
                typeof(Brush),
                typeof(MapGrid),
                new FrameworkPropertyMetadata(Brushes.Transparent, FrameworkPropertyMetadataOptions.AffectsRender));

        /// <summary>
        /// Property to access the stroke brush.
        /// </summary>
        public Brush MinorLineStroke
        {
            get { return (Brush)GetValue(MinorLineStrokeProperty); }
            set { SetValue(MinorLineStrokeProperty, value); }
        }

        /// <summary>
        /// Dependency property for specifying the grid line stroke thickness.
        /// </summary>
        public static readonly DependencyProperty StrokeThicknessProperty =
            DependencyProperty.Register(
                "StrokeThickness",
                typeof(double),
                typeof(MapGrid),
                new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.AffectsRender));

        /// <summary>
        /// Property to access the stroke thickness.
        /// </summary>
        public double StrokeThickness
        {
            get { return (double)GetValue(StrokeThicknessProperty); }
            set { SetValue(StrokeThicknessProperty, value); }
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Override the rendering to create a brush for a single cell of the grid.
        /// </summary>
        /// <param name="drawingContext"></param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (MapBounds == Rect.Empty)
            {
                return;
            }

            // Create the pens we'll use for rendering.
            Pen majorLinePen = new Pen(MajorLineStroke, StrokeThickness);
            Pen minorLinePen = new Pen(MinorLineStroke, StrokeThickness);

            // Draw the minor lines first.
            double firstMinorVerticalLine = ((int)(MapBounds.Left / MinorLineSpacing) * MinorLineSpacing);
            double lastMinorVerticalLine = ((int)(MapBounds.Right / MinorLineSpacing) * MinorLineSpacing);

            for (double x = firstMinorVerticalLine; x <= lastMinorVerticalLine; x += MinorLineSpacing)
            {
                if ((x % MajorLineSpacing) != 0)
                {
                    drawingContext.DrawLine(
                        minorLinePen,
                        new Point(RoundToPoint5((x - MapBounds.Left) * Scale.X), RoundToPoint5(0.0)),
                        new Point(RoundToPoint5((x - MapBounds.Left) * Scale.X), RoundToPoint5(ActualHeight)));
                }
            }

            double firstMinorHorizontalLine = ((int)(-MapBounds.Bottom / MinorLineSpacing) * MinorLineSpacing);
            double lastMinorHorizontalLine = ((int)(-MapBounds.Top / MinorLineSpacing) * MinorLineSpacing);

            for (double y = firstMinorHorizontalLine; y <= lastMinorHorizontalLine; y += MinorLineSpacing)
            {
                if ((y % MajorLineSpacing) != 0)
                {
                    drawingContext.DrawLine(
                        minorLinePen,
                        new Point(RoundToPoint5(0.0), RoundToPoint5((y + MapBounds.Bottom) * Scale.Y)),
                        new Point(RoundToPoint5(ActualWidth), RoundToPoint5((y + MapBounds.Bottom) * Scale.Y)));
                }
            }

            // Draw the major lines next.
            double firstMajorVerticalLine = ((int)(MapBounds.Left / MajorLineSpacing) * MajorLineSpacing);
            double lastMajorVerticalLine = ((int)(MapBounds.Right / MajorLineSpacing) * MajorLineSpacing);

            for (double x = firstMajorVerticalLine; x <= lastMajorVerticalLine; x += MajorLineSpacing)
            {
                drawingContext.DrawLine(
                    majorLinePen,
                    new Point(RoundToPoint5((x - MapBounds.Left) * Scale.X), RoundToPoint5(0.0)),
                    new Point(RoundToPoint5((x - MapBounds.Left) * Scale.X), RoundToPoint5(ActualHeight)));
            }

            double firstMajorHorizontalLine = ((int)(-MapBounds.Bottom / MajorLineSpacing) * MajorLineSpacing);
            double lastMajorHorizontalLine = ((int)(-MapBounds.Top / MajorLineSpacing) * MajorLineSpacing);

            for (double y = firstMajorHorizontalLine; y <= lastMajorHorizontalLine; y += MajorLineSpacing)
            {
                drawingContext.DrawLine(
                    majorLinePen,
                    new Point(RoundToPoint5(0.0), RoundToPoint5((y + MapBounds.Bottom) * Scale.Y)),
                    new Point(RoundToPoint5(ActualWidth), RoundToPoint5((y + MapBounds.Bottom) * Scale.Y)));
            }
        }

        /// <summary>
        /// I have a feeling this should be taking the pen thickness into account instead of
        /// just using 0.5.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private double RoundToPoint5(double value)
        {
            return Math.Round(value + 0.5, MidpointRounding.AwayFromZero) - 0.5;
        }
        #endregion
    }
}
