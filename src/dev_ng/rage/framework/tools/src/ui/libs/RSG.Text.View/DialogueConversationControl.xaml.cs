﻿//---------------------------------------------------------------------------------------------
// <copyright file="DialogueConversationControl.xaml.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.View
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Threading;
    using RSG.Editor;
    using RSG.Editor.Controls;
    using RSG.Editor.Controls.Helpers;
    using RSG.Editor.SharedCommands;
    using RSG.Text.Commands;
    using RSG.Text.Model;
    using RSG.Text.ViewModel;
    using System.ComponentModel;
    using System.Windows.Data;
    using System.Diagnostics;
    using RSG.Editor.Model;
    using RSG.Editor.Controls.AttachedDependencyProperties;
    using System.Xml;

    /// <summary>
    /// Defines the control that can be used to display the lines of a single dialogue
    /// conversation and manipulate them.
    /// </summary>
    [GuidAttribute("EE5BFDC3-DA82-4108-B072-22057A8BE92D")]
    public partial class DialogueConversationControl : RsUserControl, IInitialFocusStealer
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="SelectedItem" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty SelectedItemProperty;

        /// <summary>
        /// The private field used for the <see cref="ColumnVisibilities"/> property.
        /// </summary>
        private static ConversationColumnVisibilities _columnVisibilities;

        /// <summary>
        /// The private event handler that sorts the anonymous delegate for the items source
        /// changed event.
        /// </summary>
        private NotifyCollectionChangedEventHandler _collectionChangedHandler;

        /// <summary>
        /// Identifies the <see cref="SelectedItems" /> dependency property.
        /// </summary>
        private IEnumerable<ConversationViewModel> _selectedItems;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="DialogueConversationControl"/> class.
        /// </summary>
        static DialogueConversationControl()
        {
            _columnVisibilities = new ConversationColumnVisibilities();

            SelectedItemProperty =
                DependencyProperty.Register(
                "SelectedItem",
                typeof(object),
                typeof(DialogueConversationControl),
                new FrameworkPropertyMetadata(null, OnSelectedItemChanged));

            ItemsControl.ItemsSourceProperty.AddOwner(
                typeof(DialogueConversationControl),
                new PropertyMetadata(OnItemsSourceChanged));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="DialogueConversationControl"/> class.
        /// </summary>
        public DialogueConversationControl()
        {
            this.InitializeComponent();
            this.AttachCommandBindings();
            this.ConversationDataGrid.SelectionChanged += this.OnSelectionChanged;

            ICollectionView _customerView = CollectionViewSource.GetDefaultView(this.ConversationDataGrid.Items);
            _customerView.Filter = FilterItem;

            ConversationFilters = (IEnumerable<ConversationFilter>)Enum.GetValues(typeof(ConversationFilter));

            EnhancedFocusScope.SetInitialFocusedElement(this, this.ConversationDataGrid);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the static object containing the visibility property for the data grid
        /// columns.
        /// </summary>
        public static ConversationColumnVisibilities ColumnVisibilities
        {
            get { return _columnVisibilities; }
        }

        /// <summary>
        /// Gets the command items that should be displayed on the context menu.
        /// </summary>
        public CommandBarItemViewModelCollection ContextMenuItems
        {
            get
            {
                return CommandBarItemViewModel.Create(
                    TextViewCommandIds.ConversationContextMenu);
            }
        }

        private IEnumerable<ConversationFilter> _conversationFilters;

        public IEnumerable<ConversationFilter> ConversationFilters
        {
            get
            {
                return _conversationFilters;
            }
            set
            {
                _conversationFilters = value;
                ICollectionView _customerView = CollectionViewSource.GetDefaultView(this.ConversationDataGrid.Items);
                _customerView.Filter = FilterItem;
            }
        }

        /// <summary>
        /// Gets or sets a collection used to generate the tool bars for this tool bar tray.
        /// </summary>
        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)this.GetValue(ItemsControl.ItemsSourceProperty); }
            set { this.SetValue(ItemsControl.ItemsSourceProperty, value); }
        }

        /// <summary>
        /// Gets or sets the first item in the current selection or null if the selection is
        /// empty.
        /// </summary>
        public object SelectedItem
        {
            get { return (object)this.GetValue(SelectedItemProperty); }
            set { this.SetValue(SelectedItemProperty, value); }
        }

        /// <summary>
        /// Gets the first item in the current selection or null if the selection is empty.
        /// </summary>
        public IEnumerable<ConversationViewModel> SelectedItems
        {
            get { return this._selectedItems; }
        }

        /// <summary>
        /// Gets a value indicating whether the layout for this control get serialised on
        /// exit and deserialised after loading.
        /// </summary>
        protected override bool MakeLayoutPersistent
        {
            get { return true; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Called when this control should steal the keyboard focus.
        /// </summary>
        public void StealFocus()
        {
            this.ConversationDataGrid.MoveFocus(
                new TraversalRequest(FocusNavigationDirection.First));
        }

        private bool FilterItem(object item)
        {
            ConversationViewModel vm = item as ConversationViewModel;
            if (vm == null)
            {
                Debug.Assert(false, "Something is bound that isn't a conversation");
                return false;
            }

            return this.FilterItem(vm);
        }

        public bool FilterItem(ConversationViewModel conversationViewModel)
        {

            if (ConversationFilters.Contains(ConversationFilter.ShowPlaceholder))
            {
                if (conversationViewModel.IsPlaceholder)
                    return true;
            }

            if (ConversationFilters.Contains(ConversationFilter.ShowNotPlaceholder))
            {
                if (!conversationViewModel.IsPlaceholder)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Called whenever the ItemsSource dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The object whose ItemsSource dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnItemsSourceChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            DialogueConversationControl control = s as DialogueConversationControl;
            if (control == null)
            {
                return;
            }

            INotifyCollectionChanged oldCollection = e.OldValue as INotifyCollectionChanged;
            if (oldCollection != null)
            {
                oldCollection.CollectionChanged -= control._collectionChangedHandler;
            }

            INotifyCollectionChanged collection = e.NewValue as INotifyCollectionChanged;
            if (collection != null)
            {
                control._collectionChangedHandler = (sender, args) =>
                {
                    if (args.Action == NotifyCollectionChangedAction.Add)
                    {
                        foreach (object item in args.NewItems)
                        {
                            control.Dispatcher.BeginInvoke(
                            (Action)(() =>
                            {
                                control.ConversationDataGrid.ScrollIntoView(item);
                                control.ConversationDataGrid.SelectedItem = item;

                                DependencyObject container = control.ConversationDataGrid.ItemContainerGenerator.ContainerFromItem(item);
                                if (container != null)
                                {
                                    UIElement element = container as UIElement;
                                    element.MoveFocus(new TraversalRequest(FocusNavigationDirection.First));
                                }
                            }),
                            DispatcherPriority.Render);
                        }
                    }
                };

                collection.CollectionChanged += control._collectionChangedHandler;
            }
        }

        /// <summary>
        /// Called whenever the SelectedItem dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The object whose SelectedItem dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnSelectedItemChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            DialogueConversationControl control = s as DialogueConversationControl;
            if (control == null)
            {
                return;
            }

            control.ConversationDataGrid.SelectedItem = e.NewValue;
        }

        /// <summary>
        /// Attaches all of the necessary command bindings to this instance.
        /// </summary>
        private void AttachCommandBindings()
        {
            ICommandAction action = new AddNewConversationAction(this.CommandResolver);
            action.AddBinding(TextCommands.AddNewConversation, this);

            action = new InsertNewConversationAction(this.CommandResolver);
            action.AddBinding(TextCommands.InsertNewConversation, this);

            action = new CopyConversationAction(this.CommandResolver);
            action.AddBinding(RockstarCommands.Copy, this);

            action = new PasteConversationAction(this.CommandResolver);
            action.AddBinding(RockstarCommands.Paste, this);

            action = new CutConversationAction(this.CommandResolver);
            action.AddBinding(RockstarCommands.Cut, this);

            action = new DeleteConversationAction(this.CommandResolver);
            action.AddBinding(RockstarCommands.Delete, this);

            action = new MoveConversationUpAction(this.CommandResolver);
            action.AddBinding(TextCommands.MoveConversationUp, this);

            action = new MoveConversationDownAction(this.CommandResolver);
            action.AddBinding(TextCommands.MoveConversationDown, this);

            action = new FilterConversationsAction(this.ControlsResolver);
            action.AddBinding(TextCommands.FilterConversations, this);
        }

        /// <summary>
        /// Retrieves the command argument structure that is used as a resolver to the bounded
        /// commands for this instance.
        /// </summary>
        /// <param name="commandData">
        /// The command data that has requested the arguments.
        /// </param>
        /// <returns>
        /// The command argument structure used for the command bindings.
        /// </returns>
        private TextConversationCommandArgs CommandResolver(CommandData commandData)
        {
            TextConversationCommandArgs args = new TextConversationCommandArgs();
            args.DataGrid = this.ConversationDataGrid;
            args.Dialogue = this.DataContext as DialogueViewModel;

            List<ConversationViewModel> selection = new List<ConversationViewModel>();
            foreach (object selected in this.ConversationDataGrid.SelectedItems)
            {
                ConversationViewModel conversationViewModel
                    = selected as ConversationViewModel;
                if (conversationViewModel == null)
                {
                    continue;
                }

                selection.Add(conversationViewModel);
            }

            args.Selected = selection;
            return args;
        }

        private IEnumerable<DialogueConversationControl> ControlsResolver(CommandData commandData)
        {
            return Enumerable.Repeat(this, 1);
        }

        private void OnDragReorderingCompleted(object s, RsDataGridDragEventArgs e)
        {
            if (e.State != DragState.FinishedWithMovement)
            {
                return;
            }

            if (e.TargetRowItem == null)
            {
                //// Dropped outside of the datagrid.
                return;
            }

            Dialogue dialogue = (this.DataContext as DialogueViewModel).Model;

            Conversation source = (e.SourceRowItem as ConversationViewModel).Model;
            Conversation target = (e.TargetRowItem as ConversationViewModel).Model;
            int newIndex = dialogue.Conversations.IndexOf(target);
            int oldIndex = dialogue.Conversations.IndexOf(source);

            dialogue.MoveConversation(source, newIndex);
        }

        /// <summary>
        /// Called whenever the selection on the internal data grid changes so that we can push
        /// the selected item out to any observers of this control.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Controls.SelectionChangedEventArgs containing the event data.
        /// </param>
        private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.SelectedItem = this.ConversationDataGrid.SelectedItem;
            this._selectedItems =
                this.ConversationDataGrid.SelectedItems.OfType<ConversationViewModel>();
        }
        #endregion Methods
    } // RSG.Text.View.DialogueConversationControl {Class}
} // RSG.Text.View {Namespace}
