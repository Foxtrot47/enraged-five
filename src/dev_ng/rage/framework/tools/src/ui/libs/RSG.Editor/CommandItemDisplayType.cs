﻿//---------------------------------------------------------------------------------------------
// <copyright file="CommandItemDisplayType.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    /// <summary>
    /// Defines the different ways a command bar item can be displayed to the user.
    /// </summary>
    public enum CommandItemDisplayType
    {
        /// <summary>
        /// Specifies that the command bar item is to be displayed as a batch control.
        /// </summary>
        Batch,

        /// <summary>
        /// Specifies that the command bar item is to be displayed as a button.
        /// </summary>
        Button,

        /// <summary>
        /// Specifies that the command bar item is to be displayed as a combo box.
        /// </summary>
        Combo,

        /// <summary>
        /// Specifies that the command bar item is to be displayed as a controller item.
        /// </summary>
        Controller,

        /// <summary>
        /// Specifies that the command bar item is to be displayed as a dynamic menu control.
        /// </summary>
        DynamicMenu,

        /// <summary>
        /// Specifies that the command bar item is to be displayed as a combo box with check
        /// boxes for each item and a concatenated string for the selection.
        /// </summary>
        FilterCombo,

        /// <summary>
        /// Specifies that the command bar item is to be displayed as a menu control.
        /// </summary>
        Menu,

        /// <summary>
        /// Specifies that the command bar item is to be displayed as a search control.
        /// </summary>
        Search,

        /// <summary>
        /// Specifies that the command bar item is to be displayed as a separator.
        /// </summary>
        Separator,

        /// <summary>
        /// Specifies that the command bar item is to be displayed as a text box.
        /// </summary>
        Text,

        /// <summary>
        /// Specifies that the command bar item is to be displayed as a check box.
        /// </summary>
        Toggle,

        /// <summary>
        /// Specifies that the command bar item is to be displayed as a toolbar inside the
        /// tool bar tray.
        /// </summary>
        ToolBar
    } // RSG.Editor.CommandItemDisplayType {Enumeration}
} // RSG.Editor {Namespace}
