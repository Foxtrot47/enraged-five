﻿//---------------------------------------------------------------------------------------------
// <copyright file="ClassViewNamespaceNode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.ClassView
{
    using System.Collections.Generic;
    using RSG.Metadata.Model.Definitions;

    /// <summary>
    /// The node inside the class view that represents a standard metadata project. This class
    /// cannot be inherited.
    /// </summary>
    public sealed class ClassViewNamespaceNode : ClassViewNodeBase
    {
        #region Fields
        /// <summary>
        /// The private string containing the full namespace that this node represents.
        /// </summary>
        private string _namespace;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ClassViewNamespaceNode"/> class.
        /// </summary>
        /// <param name="fullNamespace">
        /// The full namespace this node is representing.
        /// </param>
        /// <param name="parent">
        /// The parent node for this object.
        /// </param>
        public ClassViewNamespaceNode(string fullNamespace, ClassViewNodeBase parent)
            : base(parent)
        {
            this.IsExpandable = true;
            this._namespace = fullNamespace;
            this.Icon = Icons.NamespaceNodeIcon;
            this.Text = fullNamespace;
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Creates the child elements for this view model. This is only called once the item
        /// has been expanded in the user interface.
        /// </summary>
        /// <param name="collection">
        /// The collection to add the elements to.
        /// </param>
        protected override void CreateChildren(ICollection<object> collection)
        {
            ClassViewProjectNode project = this.ParentProjectNode;
            if (project == null)
            {
                return;
            }

            project.StandardProject.EnsureDefinitionsAreLoaded();
            foreach (IStructure s in project.GetStructuresFromScope(this._namespace))
            {
                collection.Add(new ClassViewStructureNode(s, this));
            }

            foreach (IEnumeration e in project.GetEnumerationsFromScope(this._namespace))
            {
                collection.Add(new ClassViewEnumerationNode(e, this));
            }
        }
        #endregion Methods
    } // RSG.Metadata.ViewModel.ClassView.ClassViewProjectNode {Class}
} // RSG.Metadata.ViewModel.ClassView {Namespace}
