﻿//---------------------------------------------------------------------------------------------
// <copyright file="CreateNewProjectAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Commands
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Windows;
    using RSG.Editor;
    using RSG.Project.Model;
    using RSG.Project.ViewModel;
    using RSG.Project.ViewModel.Definitions;

    /// <summary>
    /// Contains the logic for the <see cref="ProjectCommands.CreateNewProject"/> routed
    /// command. This class cannot be inherited.
    /// </summary>
    public sealed class CreateNewProjectAction : ButtonAction<ProjectCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CreateNewProjectAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public CreateNewProjectAction(ProjectCommandResolver resolver)
            : base(new ParameterResolverDelegate<ProjectCommandArgs>(resolver))
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ProjectCommandArgs args)
        {
            if (args == null)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(ProjectCommandArgs args)
        {
            IMessageBoxService messageService = this.GetService<IMessageBoxService>();
            IProjectAddViewService addViewService = this.GetService<IProjectAddViewService>();
            if (messageService == null || addViewService == null)
            {
                Debug.Assert(false, "Unable to add new item as services are missing.");
                return;
            }

            ProjectCollectionNode collection = args.CollectionNode;
            if (collection == null || collection.Definition == null)
            {
                return;
            }

            if (collection.Definition.ProjectDefinitions.Count == 0)
            {
                messageService.Show(
                    "No projects have been defined for this collection type",
                    null,
                    MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                return;
            }

            ProjectDefinition selectedDefinition = null;
            bool addToCurrentCollection = false;
            string fullPath = null;
            addViewService.ShowNewProjectSelector(
                    collection,
                    out addToCurrentCollection,
                    out selectedDefinition,
                    out fullPath);
            if (selectedDefinition == null || String.IsNullOrWhiteSpace(fullPath))
            {
                throw new OperationCanceledException();
            }

            ProjectItem newItem = collection.Model.AddNewProjectItem("Project", fullPath);
            ProjectNode projectNode = selectedDefinition.CreateProjectNode();
            projectNode.SetParentAndModel(collection, newItem);

            byte[] template = selectedDefinition.Template;
            if (!selectedDefinition.ReplaceVariables(ref template, fullPath))
            {
                throw new OperationCanceledException();
            }

            CloseCollectionAction close = new CloseCollectionAction(this.ServiceProvider);
            if (close.CanExecute(args) && !addToCurrentCollection)
            {
                close.Execute(args);
            }

            if (!Directory.Exists(Path.GetDirectoryName(fullPath)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(fullPath));
            }

            using (FileStream stream = new FileStream(fullPath, FileMode.OpenOrCreate))
            {
                stream.SetLength(0);
                using (BinaryWriter writer = new BinaryWriter(stream))
                {
                    if (template != null)
                    {
                        writer.Write(template);
                    }

                    writer.Flush();
                    writer.Close();
                }
            }

            collection.AddChild(projectNode);
            projectNode.Load();

            collection.IsModified = true;
            if (!addToCurrentCollection)
            {
                collection.Loaded = true;
                collection.IsTemporaryFile = true;
                collection.Name = Path.GetFileNameWithoutExtension(fullPath);
            }
        }
        #endregion Methods
    } // RSG.Project.Commands.CreateNewProjectAction {Class}
} // RSG.Project.Commands {Namespace}
