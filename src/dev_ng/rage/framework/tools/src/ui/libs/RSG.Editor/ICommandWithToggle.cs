﻿//---------------------------------------------------------------------------------------------
// <copyright file="ICommandWithToggle.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    /// <summary>
    /// When implemented represents a command definition class that contains a toggle flag
    /// property.
    /// </summary>
    internal interface ICommandWithToggle
    {
        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether this definition is currently in the toggled
        /// state or not.
        /// </summary>
        bool IsToggled { get; set; }
        #endregion Properties
    } // RSG.Editor.ICommandWithToggle {Interface}
} // RSG.Editor {Namespace}
