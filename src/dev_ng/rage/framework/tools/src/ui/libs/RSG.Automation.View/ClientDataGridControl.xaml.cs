﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Automation.Commands;
using RSG.Editor.Controls;

namespace RSG.Automation.View
{
    /// <summary>
    /// Interaction logic for ClientDataGridControl.xaml
    /// </summary>
    public partial class ClientDataGridControl : RsUserControl
    {
        #region 
        #endregion
        /// <summary>
        /// 
        /// </summary>
        public ClientDataGridControl()
        {
            InitializeComponent();
        }
    }

} // RSG.Automation.View namespace
