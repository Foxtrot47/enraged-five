﻿//---------------------------------------------------------------------------------------------
// <copyright file="ThreadedViewModelCollection{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Data;

    /// <summary>
    /// Represents a collection of objects that can be access via a index or iterated over.
    /// This collection supports thread initialisation.
    /// </summary>
    /// <typeparam name="T">
    /// The type of elements in the collection.
    /// </typeparam>
    public class ThreadedViewModelCollection<T> :
        ObservableCollection<T>, ISupportInitializeNotification
    {
        #region Fields
        /// <summary>
        /// The private initialisation delegate that is called on calling the
        /// <see cref="BeginInit"/> method.
        /// </summary>
        private Action<ICollection<T>> _initialisationDelegate;

        /// <summary>
        /// The private field used for the <see cref="IsInitialized"/> property.
        /// </summary>
        private bool _isInitialised;

        /// <summary>
        /// A private value indicating whether this collection is current being initialised.
        /// </summary>
        private bool _isInitialising;

        /// <summary>
        /// The private field used for the <see cref="BatchAddChildren"/> property.
        /// </summary>
        private bool _batchAddChildren;

        /// <summary>
        /// The generic object that provides a synchronisation lock for this collection.
        /// </summary>
        private object _lock = new object();
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ThreadedViewModelCollection{T}"/>
        /// class.
        /// </summary>
        /// <param name="initialisationDelegate">
        /// The private action that is called when initialisation takes place.
        /// </param>
        public ThreadedViewModelCollection(Action<ICollection<T>> initialisationDelegate)
        {
            if (initialisationDelegate == null)
            {
                throw new SmartArgumentNullException(() => initialisationDelegate);
            }

            this._initialisationDelegate = initialisationDelegate;

            Application app = Application.Current;
            if (app != null && app.Dispatcher != null)
            {
                app.Dispatcher.BeginInvoke(
                (Action)(() =>
                {
                    BindingOperations.EnableCollectionSynchronization(this, this._lock);
                }));
            }
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs when initialisation of the component is completed.
        /// </summary>
        public event EventHandler Initialized;
        #endregion Events

        #region Properties
        /// <summary>
        /// Gets a value indicating whether the component is initialised.
        /// </summary>
        public bool IsInitialized
        {
            get { return this._isInitialised; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the children get added to the items
        /// collection in one go after initialisation or one at a time during initialisation.
        /// </summary>
        internal bool BatchAddChildren
        {
            get { return this._batchAddChildren; }
            set { this._batchAddChildren = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Clears all of the elements in this collection and resets the is initialised flag to
        /// false.
        /// </summary>
        public void ClearInitialisationFlag()
        {
            this.Clear();
            this._isInitialised = false;
        }

        /// <summary>
        /// Signals the collection that initialisation is starting.
        /// </summary>
        public async void BeginInit()
        {
            if (this._isInitialised == true || this._isInitialising == true)
            {
                throw new NotSupportedException(
                    "Cannot initialise a already initialised collection");
            }

            this._isInitialising = true;
            if (this._initialisationDelegate != null)
            {
                ICollection<T> children = this;
                if (this.BatchAddChildren)
                {
                    children = new Collection<T>();
                }

                await Task.WhenAll(this.CallDelegate(children)).ConfigureAwait(true);
                if (!Object.ReferenceEquals(this, children))
                {
                    foreach (T child in children)
                    {
                        this.Add(child);
                    }
                }

                this._isInitialised = true;
                this._isInitialising = false;
                this.EndInit();
            }
        }

        /// <summary>
        /// Signals the collection that initialisation is complete.
        /// </summary>
        public void EndInit()
        {
            EventHandler handler = this.Initialized;
            if (handler == null)
            {
                return;
            }

            handler(this, EventArgs.Empty);
        }

        /// <summary>
        /// Sets this collections initialisation flag to true.
        /// </summary>
        public void SetInitialisationFlag()
        {
            this._isInitialised = true;
        }

        /// <summary>
        /// Calls the associated delegate for this collection.
        /// </summary>
        /// <param name="children">
        /// The child collection that should be passed to the initialisation delegate.
        /// </param>
        /// <returns>
        /// A task that represents the work to do to call the initialisation delegate.
        /// </returns>
        private Task CallDelegate(ICollection<T> children)
        {
            Task task = Task.Factory.StartNew(
                delegate
                {
                    this._initialisationDelegate(children);
                });

            return task;
        }
        #endregion Methods
    } // RSG.Editor.View.ThreadedViewModelCollection{T} {Class}
} // RSG.Editor.View {Namespace}
