﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Editor;
using RSG.UniversalLog.ViewModel;

namespace RSG.UniversalLog.View.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public class FilterByLogLevelAction : FilterAction<UniversalLogControl, LogLevel>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="FilterByLogLevelImplementer"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public FilterByLogLevelAction(ParameterResolverDelegate<UniversalLogControl> resolver)
            : base(resolver)
        {
        }
        #endregion // Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="control">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="secondaryParameter">
        /// The secondary command parameter that has been sent with the command.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(UniversalLogControl control, MultiCommandParameter<LogLevel> secondaryParameter)
        {
            if (control == null)
            {
                throw new SmartArgumentNullException(() => control);
            }
            UniversalLogDataContext dc = GetDataContext(control);

            // Are there any messages with this log level loaded?
            LogLevel level = secondaryParameter.ItemParameter;
            bool levelPresent = dc.ContainsItemsWithLogLevel(level);

            if (level == LogLevel.Profile)
            {
                levelPresent |= dc.ContainsItemsWithLogLevel(LogLevel.ProfileEnd);
            }
            if (level == LogLevel.Error)
            {
                levelPresent |= dc.ContainsItemsWithLogLevel(LogLevel.ToolError);
            }
            return levelPresent;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="control">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="secondaryParameter">
        /// The secondary command parameter that has been sent with the command.
        /// </param>
        public override void Execute(UniversalLogControl control, MultiCommandParameter<LogLevel> secondaryParameter)
        {
            if (control == null)
            {
                throw new SmartArgumentNullException(() => control);
            }

            // Either add or remove a filter.
            if (secondaryParameter.IsToggled)
            {
                control.AddAllowedFilterLevel(secondaryParameter.ItemParameter);
            }
            else
            {
                control.RemoveAllowedFilterLevel(secondaryParameter.ItemParameter);
            }

            // Make sure the selected item is still visible.
            control.ScrollSelectionIntoView();
        }

        /// <summary>
        /// Provide the specified definition with the items to show in the filter.
        /// </summary>
        /// <param name="definition">
        /// The definition that the created items will be used for.
        /// </param>
        /// <returns>
        /// The items to show in the filter.
        /// </returns>
        protected override ObservableCollection<IMultiCommandItem> GetItems(
            MultiCommand definition)
        {
            ObservableCollection<IMultiCommandItem> items = new ObservableCollection<IMultiCommandItem>();

            // Add a filter item for all the browsable log levels.
            foreach (LogLevel level in Enum.GetValues(typeof(LogLevel)))
            {
                BrowsableAttribute att = level.GetAttributeOfType<BrowsableAttribute>();
                if (att == null || att.Browsable == true)
                {
                    items.Add(new FilterByLogLevelItem(definition, level));
                }
            }

            return items;
        }

        /// <summary>
        /// 
        /// </summary>
        public void ReInitialise(UniversalLogControl control, RockstarRoutedCommand command)
        {
            foreach (FilterByLogLevelItem item in (this[command] as FilterCommand).Items)
            {
                if (!item.IsToggled)
                {
                    this.Execute(control, item.GetCommandParameter() as MultiCommandParameter<LogLevel>);
                }
            }
        }
        #endregion // Methods

        #region Private Methods
        /// <summary>
        /// Gets the data context associated with the control.
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        private UniversalLogDataContext GetDataContext(UniversalLogControl control)
        {
            UniversalLogDataContext dc = null;
            if (!control.Dispatcher.CheckAccess())
            {
                dc = control.Dispatcher.Invoke<UniversalLogDataContext>(() => control.DataContext as UniversalLogDataContext);
            }
            else
            {
                dc = control.DataContext as UniversalLogDataContext;
            }

            if (dc == null)
            {
                Debug.Fail("Data context isn't valid.");
                throw new ArgumentException("Data context isn't valid.");
            }
            return dc;
        }
        #endregion // Private Methods

        #region Classes
        /// <summary>
        /// Defines the item published for the items of the
        /// <see cref="FilterByLogLevelItem"/> command definition.
        /// </summary>
        private class FilterByLogLevelItem : MultiCommandItem<LogLevel>
        {
            #region Fields
            /// <summary>
            /// The private field used for the <see cref="Icon"/> property.
            /// </summary>
            private BitmapSource m_icon;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="FilterByLogLevelItem"/> class.
            /// </summary>
            /// <param name="definition">
            /// The multi command definition that owns this item.
            /// </param>
            /// <param name="level">
            /// The parameter that is sent when this item is executed.
            /// </param>
            /// <param name="dataGrid">
            /// The data grid this item is linked to.
            /// </param>
            public FilterByLogLevelItem(MultiCommand definition, LogLevel level)
                : base(definition, level.ToString(), level)
            {
                // Get the icon to use for this log level.
                m_icon = level.GetIcon();
                if (m_icon == null)
                {
                    m_icon = CommonIcons.UnknownFile;
                }

                // Get the name to display for this log level.
                DisplayNameAttribute nameAtt = level.GetAttributeOfType<DisplayNameAttribute>();
                Debug.Assert(nameAtt != null, "LogLevel is missing the DisplayNameAttribute.");
                if (nameAtt != null && nameAtt.DisplayName != null)
                {
                    Text = nameAtt.DisplayName + "s";
                }

                m_description = Text;
                this.IsToggled = true;
            }
            #endregion // Constructors

            #region Properties
            /// <summary>
            /// Gets the description for this item. This is also what is shown as a tooltip
            /// along with the key gesture if one is set if it is being rendered as a
            /// toggle control.
            /// </summary>
            public override String Description
            {
                get { return this.m_description; }
            }
            private String m_description;

            /// <summary>
            /// Gets the System.Windows.Media.Imaging.BitmapSource object that represents the
            /// icon that is used for this item if it is being rendered as a toggle control.
            /// </summary>
            public override BitmapSource Icon
            {
                get { return m_icon; }
            }
            #endregion Properties
        } // FilterByLogLevelItem
        #endregion // Classes
    } // FilterByLogLevelAction
}
