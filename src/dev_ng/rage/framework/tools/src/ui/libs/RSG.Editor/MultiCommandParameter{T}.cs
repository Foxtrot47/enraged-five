﻿//---------------------------------------------------------------------------------------------
// <copyright file="MultiCommandParameter{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    /// <summary>
    /// The command parameter used for a multi command item object.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the actual command parameter value.
    /// </typeparam>
    public class MultiCommandParameter<T> : IMultiCommandParameter
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="IsToggled"/> property.
        /// </summary>
        private bool _isToggled;

        /// <summary>
        /// The private field used for the <see cref="ItemParameter"/> property.
        /// </summary>
        private T _itemParameter;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MultiCommandParameter{T}"/> class.
        /// </summary>
        /// <param name="itemParameter">
        /// The actual command parameter value for the multi command item object this is
        /// being used for.
        /// </param>
        public MultiCommandParameter(T itemParameter)
        {
            this._itemParameter = itemParameter;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether this parameter is currently been sent in
        /// a toggled state or not.
        /// </summary>
        public bool IsToggled
        {
            get { return this._isToggled; }
            set { this._isToggled = value; }
        }

        /// <summary>
        /// Gets the actual command parameter value for the multi command item object this is
        /// being used for.
        /// </summary>
        public T ItemParameter
        {
            get { return this._itemParameter; }
        }
        #endregion Properties
    } // RSG.Editor.MultiCommandParameter{T} {Class}
} // RSG.Editor {Namespace}
