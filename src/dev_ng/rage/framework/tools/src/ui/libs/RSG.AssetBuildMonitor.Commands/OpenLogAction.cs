﻿//---------------------------------------------------------------------------------------------
// <copyright file="OpenLogAction.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.AssetBuildMonitor.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using RSG.AssetBuildMonitor.ViewModel;
    using RSG.AssetBuildMonitor.ViewModel.Project;
    using RSG.Base.Extensions;
    using RSG.Editor;
    using RSG.Editor.Controls.Dock.ViewModel;

    /// <summary>
    /// Contains the logic for the <see cref="Commands.OpenLog"/> routed
    /// command. This class cannot be inherited.
    /// </summary>
    public sealed class OpenLogAction : ButtonAction<CommandArgs>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="OpenLogAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public OpenLogAction(CommandResolver resolver)
            : base(new ParameterResolverDelegate<CommandArgs>(resolver))
        {
        }
        #endregion // Constructor(s)

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(CommandArgs args)
        {
            IEnumerable<AssetBuildItemViewModel> selectedBuildItems = 
                args.GetActiveDocumentSelectedBuildItems();
            return (selectedBuildItems.Any());
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(CommandArgs args)
        {
            IMessageBoxService messageService = this.GetService<IMessageBoxService>();
            if (messageService == null)
            {
                Debug.Fail("Unable to open as services are missing.");
                throw (new NotSupportedException());                
            }
            IDialogServices dialogServices = this.GetService<IDialogServices>();
            if (null == dialogServices)
            {
                Debug.Fail("Unable to add host as IDialogService is missing.");
                throw (new NotSupportedException());
            }

            // Loop through all selected hosts opening their log files.
            IEnumerable<AssetBuildItemViewModel> selectedBuildItems = args.GetActiveDocumentSelectedBuildItems();
            ICollection<String> missingFilenames = new List<String>();
            foreach (AssetBuildItemViewModel item in selectedBuildItems)
            {
                if (File.Exists(item.BuildItem.LogFilename))
                {
                    Process.Start(item.BuildItem.LogFilename);
                }
                else
                {
                    missingFilenames.Add(item.BuildItem.LogFilename);
                }
            }

            if (missingFilenames.Any())
            {
                String title = ((RSG.Editor.Controls.RsApplication)Application.Current).UniqueName;
                dialogServices.ShowFileListDialog(title, "Missing asset build log files.", missingFilenames);
            }
        }
        #endregion Methods
    }

} // RSG.AssetBuildMonitor.Commands namespace
