﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Configuration.Automation;
using RSG.Interop.Bugstar;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Consumers;
using RSG.Interop.Bugstar.Organisation;
using System.Windows.Media.Imaging;

namespace RSG.Automation.ViewModel
{

    /// <summary>
    /// Job View-Model.
    /// </summary>
    public sealed class JobViewModel : ViewModelBase
    {
        #region Properties
        /// <summary>
        /// Automation Service connection URI.
        /// </summary>
        public Uri AutomationService
        {
            get { return _automationService; }
            private set
            {
                SetProperty(ref _automationService, value, "AutomationService");
            }
        }
        private Uri _automationService;

        /// <summary>
        /// Automation Admin Service connection URI.
        /// </summary>
        public Uri AutomationAdminService
        {
            get { return _automationAdminService; }
            private set
            {
               SetProperty(ref _automationAdminService, value, "AutomationAdminService");
            }
        }
        private Uri _automationAdminService;

        /// <summary>
        /// Automation File Transfer Service connection URI.
        /// </summary>
        public Uri FileTransferService
        {
            get { return _fileTransferService; }
            private set
            {
                SetProperty(ref _fileTransferService, value, "FileTransferService");
            }
        }
        private Uri _fileTransferService;

        /// <summary>
        /// Job.
        /// </summary>
        public IJob Job
        {
            get { return (this._job); }
        }

        /// <summary>
        /// Job unique identifier.
        /// </summary>
        public Guid Id
        {
            get { return (this._job.ID); }
        }

        /// <summary>
        /// Job state.
        /// </summary>
        public JobState State
        {
            get { return (this._job.State); }
        }

        /// <summary>
        /// Job priority.
        /// </summary>
        public JobPriority Priority
        {
            get { return (this._job.Priority); }
        }

        /// <summary>
        /// Job trigger.
        /// </summary>
        public ITrigger Trigger
        {
            get { return (this._job.Trigger); }
        }

        /// <summary>
        /// Changelist P4Web URI.
        /// </summary>
        public String ChangelistUri
        {
            get { return _changelistUri; }
            private set
            {
                SetProperty(ref _changelistUri, value, "ChangelistUri");
            }
        }
        private String _changelistUri;

        /// <summary>
        /// User mailto: URI.
        /// </summary>
        public String MailToUri
        {
            get { return _mailToUri; }
            private set
            {
                SetProperty(ref _mailToUri, value, "MailToUri");
            }
        }
        private String _mailToUri;

        /// <summary>
        /// Job trigger submitted timestamp.
        /// </summary>
        public DateTime SubmittedAt
        {
            get
            {
                if (UseUTC) { return this._job.Trigger.TriggeredAt; }
                else { return this._job.Trigger.TriggeredAt.ToLocalTime(); }
            }            
        }
        
        /// <summary>
        /// Job processing start timestamp.
        /// </summary>
        public String ProcessedAt
        {
            get
            {
                if (this._job.ProcessedAt.Equals(DateTime.MaxValue.ToUniversalTime()))
                    return String.Empty;
                else
                {
                    if (UseUTC) { return this._job.ProcessedAt.ToString(); }
                    else { return this._job.ProcessedAt.ToLocalTime().ToString(); }
                }
            }
        }

        /// <summary>
        /// Job processing time (Milliseconds omitted).
        /// </summary>
        public String ProcessingTime
        {
            get { return (this._job.ProcessingTime.ToString(@"hh\:mm\:ss")); }
        }

        /// <summary>
        /// Job Processing Host
        /// </summary>
        public String ProcessingHost
        {
            get { return (this._job.ProcessingHost); }
        }

        /// <summary>
        /// Bugstar User who owns the job.
        /// </summary>
        public User User
        {
            get { return _user; }
            private set
            {
                SetProperty(ref _user, value);
            }
        }
        private User _user;

        /// <summary>
        /// Whether job is owned by current user.
        /// </summary>
        public bool OwnedByCurrentUser
        {
            get { return _ownedByCurrentUser; }
            private set
            {
                SetProperty(ref _ownedByCurrentUser, value);
            }
        }
        private bool _ownedByCurrentUser;

        /// <summary>
        /// Whether to display UTC time or local time
        /// </summary>
        public bool UseUTC
        {
            get { return _useUTC; }
            set
            {
                SetProperty(ref _useUTC, value, "UseUTC", "SubmittedAt", "ProcessedAt");
            }
        }
        private bool _useUTC;
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Model IJob object.
        /// </summary>
        private IJob _job;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Job View-Model constructor.
        /// </summary>
        /// <param name="job"></param>
        /// <param name="p4webroot"></param>
        /// <param name="users"></param>
        public JobViewModel(IAutomationServiceConfig config, IJob job, Uri p4webroot, IEnumerable<User> users, bool UseUTC)
        {
            this._job = job;
            this.AutomationService = new Uri(config.ServerHost, "automation.svc");
            this.AutomationAdminService = new Uri(config.ServerHost, "automation.svc");
            this.FileTransferService = new Uri(config.ServerHost, "FileTransfer.svc");
            this._useUTC = UseUTC;

            String username = String.Empty;
            
            if (this._job.Trigger is ChangelistTrigger)
            {                
                ChangelistTrigger trigger = (ChangelistTrigger)this._job.Trigger;
                this.ChangelistUri = String.Format("{0}{1}?ac=10", p4webroot, trigger.Changelist);
                username = trigger.Username;
            }
            else if (this._job.Trigger is UserRequestTrigger)
            {            
                UserRequestTrigger trigger = (UserRequestTrigger)this._job.Trigger;                
                username = trigger.Username;
            }

            if (!String.IsNullOrEmpty(username))
            {
                this.User = users.Where(u => u.UserName.Equals(username)).FirstOrDefault();
                if (null != this.User)
                {
                    String subject = String.Format("Automation Monitor Job: {0}", this.Job.ID);
                    this.MailToUri = String.Format("mailto:{0}?subject={1}", this.User.Email, subject);
                }
            }

            this.OwnedByCurrentUser = username.Equals(Environment.UserName);
        }
        #endregion // Constructor(s)
    }

} // RSG.Automation.ViewModel namespace
