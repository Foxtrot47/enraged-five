﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Data;
using RSG.Base.Extensions;
using RSG.Editor;
using RSG.UniversalLog.View;
using RSG.UniversalLog.ViewModel;

namespace RSG.UniversalLog.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public class SearchAction : SearchAction<UniversalLogControl>
    {
        #region Fields
        /// <summary>
        /// A value indicating whether the last search produced any results.
        /// </summary>
        private bool _foundResults;

        /// <summary>
        /// The current regular expression.
        /// </summary>
        private Regex _currentRegex;

        /// <summary>
        /// 
        /// </summary>
        private UniversalLogMessageViewModel _currentResult;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SearchImplementer"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public SearchAction(ParameterResolverDelegate<UniversalLogControl> resolver)
            : base(resolver)
        {
        }
        #endregion // Constructors

        #region Overridden Methods
        /// <summary>
        /// Override to set logic against the can execute handler for the
        /// <see cref="RockstarCommands.Find"/> command. By default this returns true.
        /// </summary>
        /// <param name="dc"></param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        protected override bool CanSearch(UniversalLogControl control)
        {
            return GetDataContext(control).AnyFilesLoaded;
        }

        /// <summary>
        /// Override to set logic against the execute command handler for the
        /// <see cref="RockstarCommands.Find"/> command when the search data indicates the
        /// search should be cleared.
        /// </summary>
        /// <param name="dc"></param>
        protected override void ClearSearch(UniversalLogControl control)
        {
            UniversalLogDataContext dc = GetDataContext(control);
            SetSearchProperties(control, dc, null, false);
        }

        /// <summary>
        /// Override to set logic against the execute command handler for the
        /// <see cref="RockstarCommands.Find"/> command.
        /// </summary>
        /// <param name="dc"></param>
        /// <param name="data">
        /// The search data that has been sent with the command.
        /// </param>
        /// <returns>
        /// A task that represents the work done by the search handler.
        /// </returns>
        protected override async Task<bool> Search(UniversalLogControl control, SearchData data)
        {
            bool result = false;
            data.ThrowIfCancellationRequested();
            result = await Task.Factory.StartNew(
                delegate
                {
                    if (data.Cleared)
                    {
                        this.ClearSearch(control);
                        return true;
                    }

                    return this.SearchDelegate(control, data);
                });

            return result;
        }

        /// <summary>
        /// Override to set logic against the execute command handler for the
        /// <see cref="RockstarCommands.FindNext"/> command.
        /// </summary>
        /// <param name="dc"></param>
        /// <param name="data">
        /// The search data that has been sent with the command.
        /// </param>
        /// <returns>
        /// True if the next search result was navigated to successfully; otherwise, false.
        /// </returns>
        protected override bool SearchNext(UniversalLogControl control, SearchData data)
        {
            if (_foundResults == false)
            {
                return false;
            }

            return this.GoToResult(control, false);
        }

        /// <summary>
        /// Override to set logic against the execute command handler for the
        /// <see cref="RockstarCommands.FindPrevious"/> command.
        /// </summary>
        /// <param name="dc"></param>
        /// <param name="data">
        /// The search data that has been sent with the command.
        /// </param>
        /// <returns>
        /// True if the next search result was navigated to successfully; otherwise, false.
        /// </returns>
        protected override bool SearchPrevious(UniversalLogControl control, SearchData data)
        {
            if (_foundResults == false)
            {
                return false;
            }

            return this.GoToResult(control, true);
        }
        #endregion // Overridden Methods

        #region Private Methods
        /// <summary>
        /// Gets the data context associated with the control.
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        private UniversalLogDataContext GetDataContext(UniversalLogControl control)
        {
            UniversalLogDataContext dc = null;
            if (!control.Dispatcher.CheckAccess())
            {
                dc = control.Dispatcher.Invoke<UniversalLogDataContext>(() => control.DataContext as UniversalLogDataContext);
            }
            else
            {
                dc = control.DataContext as UniversalLogDataContext;
            }

            if (dc == null)
            {
                Debug.Fail("Data context isn't valid.");
                throw new ArgumentException("Data context isn't valid.");
            }
            return dc;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="control"></param>
        /// <param name="dc"></param>
        /// <param name="regex"></param>
        /// <param name="filter"></param>
        private void SetSearchProperties(UniversalLogControl control, UniversalLogDataContext dc, Regex regex, bool filter)
        {
            if (!control.Dispatcher.CheckAccess())
            {
                control.Dispatcher.BeginInvoke(
                    new Action(
                        delegate
                        {
                            control.SetSearchProperties(regex, filter);
                        }));
            }
            else
            {
                control.SetSearchProperties(regex, filter);
            }
        }

        /// <summary>
        /// Performs the search described in the specified search data.
        /// </summary>
        /// <param name="controls">
        /// The collection of controls to search in.
        /// </param>
        /// <param name="data">
        /// The data that contains the search parameters.
        /// </param>
        /// <returns>
        /// A value indicating whether the search produced any results.
        /// </returns>
        private bool SearchDelegate(UniversalLogControl control, SearchData data)
        {
            _foundResults = false;
            data.ThrowIfCancellationRequested();
            if (!this.TryCreateSearchExpression(data, out _currentRegex))
            {
                return false;
            }

            UniversalLogDataContext dc = GetDataContext(control);

            data.ThrowIfCancellationRequested();
            SetSearchProperties(control, dc, _currentRegex, data.FilteringResults);

            data.ThrowIfCancellationRequested();
            foreach (UniversalLogMessageViewModel msgVm in dc.AllMessages)
            {
                data.ThrowIfCancellationRequested();

                if (_currentRegex.IsMatch(msgVm.Message))
                {
                    _foundResults = true;
                    break;
                }
            }

            data.ThrowIfCancellationRequested();
            _currentResult = null;
            return _foundResults;
        }

        /// <summary>
        /// Creates a regular expression object that is needed to perform the search defined
        /// in the specified data.
        /// </summary>
        /// <param name="data">
        /// The data to create the regular expression from.
        /// </param>
        /// <param name="expression">
        /// When this method returns contains the expression object needed to perform the
        /// search defined in the specified data.
        /// </param>
        /// <returns>
        /// True if the creation was successful; otherwise, false.
        /// </returns>
        private bool TryCreateSearchExpression(SearchData data, out Regex expression)
        {
            string pattern = data.SearchText;
            RegexOptions options = RegexOptions.None;
            if (!data.RegularExpressions)
            {
                pattern = Regex.Escape(pattern);
            }

            if (!data.MatchCase)
            {
                options |= RegexOptions.IgnoreCase;
            }

            if (data.MatchWord)
            {
                pattern = String.Format(@"\b{0}\b", pattern);
            }

            try
            {
                expression = new Regex(pattern, options);
            }
            catch (ArgumentException ex)
            {
                Debug.Assert(false, "Unable to create regular expression.", "{0}", ex.Message);
                data.RegisterParseError(ex.Message);
                expression = null;
                return false;
            }

            return true;
        }

        /// <summary>
        /// Goes to the next result in the specified control, either going forwards from the
        /// current selection or backwards.
        /// </summary>
        /// <param name="view">
        /// The control to navigator within.
        /// </param>
        /// <param name="reverse">
        /// A value indicating whether to navigator forwards to the next result or backwards.
        /// </param>
        /// <returns>
        /// True if the next result was found; otherwise, false.
        /// </returns>
        private bool GoToResult(UniversalLogControl view, bool reverse)
        {
            // Get the list of items as shown in the data grid.
            CollectionView collectionView = (CollectionView)CollectionViewSource.GetDefaultView(view.DataGrid.Items);
            IList<UniversalLogMessageViewModel> messageVms =
                collectionView.OfType<UniversalLogMessageViewModel>().ToList();
            if (!messageVms.Any())
            {
                return false;
            }

            // Order the vms about the currently selected item.
            if (reverse)
            {
                messageVms = messageVms.Reverse().ToList();
            }

            IList<UniversalLogMessageViewModel> orderedVms = new List<UniversalLogMessageViewModel>();

            UniversalLogMessageViewModel selectedItem = view.DataGrid.SelectedItem as UniversalLogMessageViewModel;
            int selectedIndex = messageVms.IndexOf(selectedItem);
            if (selectedIndex != -1)
            {
                orderedVms.AddRange(messageVms.Skip(selectedIndex + 1));
                orderedVms.AddRange(messageVms.Take(selectedIndex));

                // Determine where the currently selected item should go based on whether we
                // started a new search or the user changed the selected item manually.
                if (view.DataGrid.SelectedItem == _currentResult)
                {
                    orderedVms.Add(selectedItem);
                }
                else
                {
                    orderedVms.Insert(0, selectedItem);
                }
            }
            else
            {
                orderedVms = messageVms;
            }

            // Select the next item that matches the search expression.
            foreach (UniversalLogMessageViewModel msgVm in orderedVms)
            {
                if (_currentRegex.IsMatch(msgVm.Message))
                {
                    view.DataGrid.SelectedItem = msgVm;
                    _currentResult = msgVm;
                    view.ScrollSelectionIntoView();
                    return true;
                }
            }
            
            return false;
        }
        #endregion // Private Methods
    } // SearchAction
}
