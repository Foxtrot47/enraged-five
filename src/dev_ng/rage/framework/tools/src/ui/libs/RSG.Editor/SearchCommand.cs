﻿//---------------------------------------------------------------------------------------------
// <copyright file="SearchCommand.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System.Collections.ObjectModel;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Provides a base class to a definition of a search command.
    /// </summary>
    public abstract class SearchCommand :
        CommandDefinition,
        ICommandWithParameter,
        ICommandWithScopes,
        ICommandWithSearchParameters
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="CanFilterResults"/> property.
        /// </summary>
        private bool _canFilterResults;

        /// <summary>
        /// The private field used for the <see cref="CanUseMatchCase"/> property.
        /// </summary>
        private bool _canUseMatchCase;

        /// <summary>
        /// The private field used for the <see cref="CanUseMatchWholeWord"/> property.
        /// </summary>
        private bool _canUseMatchWholeWord;

        /// <summary>
        /// The private field used for the <see cref="CanUseRegularExpressions"/> property.
        /// </summary>
        private bool _canUseRegularExpressions;

        /// <summary>
        /// The private field used for the <see cref="Scopes"/> property.
        /// </summary>
        private ObservableCollection<string> _scopes;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SearchCommand"/> class.
        /// </summary>
        protected SearchCommand()
        {
            this._scopes = new ObservableCollection<string>();
            this._canFilterResults = true;
            this._canUseMatchCase = true;
            this._canUseMatchWholeWord = true;
            this._canUseRegularExpressions = true;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether the user can filter the view to just show
        /// the search results.
        /// </summary>
        public virtual bool CanFilterResults
        {
            get { return this._canFilterResults; }
            set { this._canFilterResults = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the user can search with case sensitivity.
        /// </summary>
        public virtual bool CanUseMatchCase
        {
            get { return this._canUseMatchCase; }
            set { this._canUseMatchCase = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the user can search for whole words.
        /// </summary>
        public virtual bool CanUseMatchWholeWord
        {
            get { return this._canUseMatchWholeWord; }
            set { this._canUseMatchWholeWord = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the user can search using regular
        /// expressions.
        /// </summary>
        public virtual bool CanUseRegularExpressions
        {
            get { return this._canUseRegularExpressions; }
            set { this._canUseRegularExpressions = value; }
        }

        /// <summary>
        /// Gets the parameter that is routed through the element tree with the command.
        /// </summary>
        public virtual object CommandParameter
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the parameter that is routed through the element tree with the command.
        /// </summary>
        object ICommandWithParameter.CommandParameter
        {
            get { return this.CommandParameter; }
        }

        /// <summary>
        /// Gets the collection defining the different scopes the user can search in.
        /// </summary>
        public ObservableCollection<string> Scopes
        {
            get { return this._scopes; }
            internal set { this._scopes = value; }
        }

        /// <summary>
        /// Gets the delay in milliseconds between the user typing and the search taking place.
        /// Use 0 or less to indicating no delay and int.MaxValue to indicate the user has to
        /// press a button to search.
        /// </summary>
        public virtual int SearchDelay
        {
            get { return 0; }
        }
        #endregion Properties
    }  // RSG.Editor.SearchCommand {Class}
} // RSG.Editor {Namespace}
