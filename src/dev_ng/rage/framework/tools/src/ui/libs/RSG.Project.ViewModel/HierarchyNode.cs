﻿//---------------------------------------------------------------------------------------------
// <copyright file="HierarchyNode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Windows.Media.Imaging;
    using RSG.Editor.View;
    using RSG.Project.Model;

    /// <summary>
    /// 
    /// </summary>
    public class HierarchyNode :
        ThreadedHierarchicalViewModelBase<ProjectItem, IHierarchyNode>,
        ITreeDisplayItem,
        ISupportsExpansionEvents,
        IHierarchyNode,
        IPrioritisedComparable,
        IRenameable
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="IsModified"/> property.
        /// </summary>
        private bool _isModified;

        /// <summary>
        /// The private field used for the <see cref="Parent"/> property.
        /// </summary>
        private IHierarchyNode _parent;

        /// <summary>
        /// The private field used for the <see cref="OverlayIcon"/> property.
        /// </summary>
        private BitmapSource _overlayIcon;

        /// <summary>
        /// The private field used for the <see cref="StateIcon"/> property.
        /// </summary>
        private BitmapSource _stateIcon;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="HierarchyNode"/> class.
        /// </summary>
        public HierarchyNode()
            : base(null)
        {
            this.IsExpandable = false;
            this.Items.SetInitialisationFlag();
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="HierarchyNode"/> class.
        /// </summary>
        /// <param name="initialiseItems">
        /// A flag indicating whether the items collection should have its initialisation flag
        /// set. This should be false if the node is going to dynamically create its children.
        /// </param>
        public HierarchyNode(bool initialiseItems)
            : base(null)
        {
            if (initialiseItems)
            {
                this.IsExpandable = false;
                this.Items.SetInitialisationFlag();
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="HierarchyNode"/> class.
        /// </summary>
        /// <param name="parent">
        /// The hierarchical node whose the parent to this node.
        /// </param>
        /// <param name="model">
        /// The model that this view model will be wrapping.
        /// </param>
        public HierarchyNode(IHierarchyNode parent, ProjectItem model)
            : base(model)
        {
            this.IsExpandable = false;
            this._parent = parent;
            this.Items.SetInitialisationFlag();
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether this hierarchy node can be removed from the parent
        /// items scope it belongs to.
        /// </summary>
        public virtual bool CanBeRemoved
        {
            get { return false; }
        }

        /// <summary>
        /// Gets a value indicating whether this hierarchy nodes data can be saved.
        /// </summary>
        public virtual bool CanBeSaved
        {
            get { return false; }
        }

        /// <summary>
        /// Gets a value indicating whether a new or existing collection folder can be added to
        /// this hierarchy node.
        /// </summary>
        public virtual bool CanHaveCollectionFoldersAdded
        {
            get { return false; }
        }

        /// <summary>
        /// Gets a value indicating whether a new or existing project folder can be added to
        /// this hierarchy node.
        /// </summary>
        public virtual bool CanHaveProjectFoldersAdded
        {
            get { return false; }
        }

        /// <summary>
        /// Gets a value indicating whether a new or existing items can be added to this
        /// hierarchy node.
        /// </summary>
        public virtual bool CanHaveItemsAdded
        {
            get { return false; }
        }

        /// <summary>
        /// Gets a value indicating whether a new or existing project can be added to this
        /// hierarchy node.
        /// </summary>
        public virtual bool CanHaveProjectsAdded
        {
            get { return false; }
        }

        /// <summary>
        /// Gets a value indicating whether this item can be currently renamed by the
        /// controller.
        /// </summary>
        public virtual bool CanBeRenamed
        {
            get { return false; }
        }

        /// <summary>
        /// Gets the bitmap source that is used to display the icon for this item when it has
        /// been expanded.
        /// </summary>
        public virtual BitmapSource ExpandedIcon
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the friendly save path that this item pushes into the command system to
        /// display on menu items.
        /// </summary>
        public virtual string FriendlySavePath
        {
            get { return this.Text; }
        }

        /// <summary>
        /// Gets a value indicating whether this item is expanded when the user double clicks
        /// on it.
        /// </summary>
        public virtual bool ExpandOnDoubleClick
        {
            get { return true; }
        }

        /// <summary>
        /// Gets the include string for the project item this hierarchy node 
        /// </summary>
        public string Include
        {
            get { return this.Model.Include; }
        }

        /// <summary>
        /// Gets a value indicating whether this node is representing a virtual project item or
        /// a project item that is located on the users disk drive.
        /// </summary>
        public virtual bool IsVirtual
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether this document item currently contains modified
        /// unsaved data.
        /// </summary>
        public virtual bool IsModified
        {
            get { return this._isModified; }
            set { this.SetProperty(ref this._isModified, value); }
        }

        /// <summary>
        /// Gets or sets the parent node for this node.
        /// </summary>
        public IHierarchyNode Parent
        {
            get { return this._parent; }
            protected set { this._parent = value; }
        }

        /// <summary>
        /// Gets the parent collection node for this node.
        /// </summary>
        public virtual ProjectCollectionNode ParentCollection
        {
            get
            {
                ProjectCollectionNode parentCollection = this._parent as ProjectCollectionNode;
                if (parentCollection != null)
                {
                    return parentCollection;
                }

                if (this._parent == null)
                {
                    return null;
                }

                return this._parent.ParentCollection;
            }
        }

        /// <summary>
        /// Gets the parent node for this node.
        /// </summary>
        public virtual ProjectNode ParentProject
        {
            get
            {
                ProjectNode parentProject = this._parent as ProjectNode;
                if (parentProject != null)
                {
                    return parentProject;
                }

                if (this._parent == null)
                {
                    return null;
                }

                return this._parent.ParentProject;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual IHierarchyNode ParentModifiedScope
        {
            get
            {
                IProjectItemScope scope = this.ProjectItemScope;
                if (scope == null)
                {
                    return null;
                }

                IHierarchyNode parent = this.Parent;
                while (parent != null)
                {
                    if (Object.ReferenceEquals(scope, parent.Model))
                    {
                        return parent;
                    }

                    parent = parent.Parent;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the priority value for this object. By default all objects are 0.
        /// </summary>
        public virtual int Priority
        {
            get { return 0; }
        }

        /// <summary>
        /// Gets the project item that this hierarachy node is representing.
        /// </summary>
        public ProjectItem ProjectItem
        {
            get { return this.Model; }
        }

        /// <summary>
        /// Gets the project scope for the item that this node is representing.
        /// </summary>
        public IProjectItemScope ProjectItemScope
        {
            get
            {
                ProjectItem item = this.Model;
                if (item != null)
                {
                    return item.Scope;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the project node that this file node is contained within.
        /// </summary>
        public ProjectNode ProjectNode
        {
            get
            {
                IHierarchyNode parent = this.Parent;
                ProjectNode project = parent as ProjectNode;
                while (parent != null && project == null)
                {
                    parent = parent.Parent;
                    project = parent as ProjectNode;
                }

                return project;
            }
        }

        /// <summary>
        /// Gets the parent node who is currently representing this nodes scope item.
        /// </summary>
        public IHierarchyNode ModifiedScopeNode
        {
            get
            {
                IProjectItemScope scope = this.ProjectItemScope;
                if (scope == null)
                {
                    return null;
                }

                IHierarchyNode parent = this.Parent;
                while (parent != null)
                {
                    if (Object.ReferenceEquals(scope, parent.Model))
                    {
                        return parent;
                    }

                    parent = parent.Parent;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the parent node who is currently representing this nodes scope item.
        /// </summary>
        public IHierarchyNode ProjectItemScopeNode
        {
            get
            {
                IProjectItemScope scope = this.ProjectItemScope;
                if (scope == null)
                {
                    return null;
                }

                IHierarchyNode parent = this;
                while (parent != null)
                {
                    if (Object.ReferenceEquals(scope, parent.Model))
                    {
                        return parent;
                    }

                    parent = parent.Parent;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the bitmap source that is used to display the state icon for this item.
        /// </summary>
        public BitmapSource OverlayIcon
        {
            get { return this._overlayIcon; }
            protected set { this.SetProperty(ref this._overlayIcon, value); }
        }

        /// <summary>
        /// Gets the bitmap source that is used to display the state icon for this item.
        /// </summary>
        public BitmapSource StateIcon
        {
            get { return this._stateIcon; }
            protected set { this.SetProperty(ref this._stateIcon, value); }
        }

        /// <summary>
        /// Gets the text that is used to represent the state of this item.
        /// </summary>
        public virtual string StateToolTipText
        {
            get { return null; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Gets the rename controller that is used to rename this item with the specified
        /// container.
        /// </summary>
        /// <param name="container">
        /// The container is the user interface items container that this item is being shown
        /// inside. This is needed to focus the edit mode logic and is got from the command
        /// execute handler.
        /// </param>
        /// <returns>
        /// The rename controller to use for this item.
        /// </returns>
        public virtual IRenameController BeginRename(object container)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Creates the child elements for this view model. This is only called once the item
        /// has been expanded in the user interface.
        /// </summary>
        /// <param name="collection">
        /// The collection to add the elements to.
        /// </param>
        protected override void CreateChildren(ICollection<IHierarchyNode> collection)
        {
        }

        /// <summary>
        /// Adds the specified item to the end of this nodes child collection.
        /// </summary>
        /// <param name="item">
        /// The item to add.
        /// </param>
        /// <returns>
        /// True if the item has been successfully added to the collection; otherwise, false.
        /// </returns>
        public bool AddChild(IHierarchyNode item)
        {
            this.IsExpandable = true;
            this.Items.Add(item);
            return true;
        }

        /// <summary>
        /// Changes the parent node for this node.
        /// </summary>
        /// <param name="newScope">
        /// The new parent node for this item.
        /// </param>
        public virtual void ChangeParent(IHierarchyNode parent)
        {
            bool updating = !Object.ReferenceEquals(parent, this._parent);
            if (!updating)
            {
                Debug.Assert(updating, "Specified new parent is the same as the current one");
                return;
            }

            this.Parent = parent;
        }

        /// <summary>
        /// Makes sure that the filter metadata for this nodes model is correct based on its
        /// current parent.
        /// </summary>
        public void ValidateAndEnsureFilter()
        {
            if (this.Model != null)
            {
                if (this.Parent is FolderNode)
                {
                    this.Model.SetMetadata("Filter", this.Parent.Include, MetadataLevel.Filter);
                }
                else
                {
                    this.Model.RemoveMetadata("Filter", MetadataLevel.Filter);
                }
            }

            foreach (IHierarchyNode child in this.GetChildren(false))
            {
                HierarchyNode childNode = child as HierarchyNode;
                if (childNode != null)
                {
                    childNode.ValidateAndEnsureFilter();
                }
            }
        }

        /// <summary>
        /// Retrieves a iterator around the children underneath this node that share the same
        /// scope as this node.
        /// </summary>
        /// <param name="recursive">
        /// A value indicating whether this method retrieves just the children of this node or
        /// recurses into them as well.
        /// </param>
        /// <returns>
        /// A iterator around the children underneath this node that share the same scope.
        /// </returns>
        public IEnumerable<IHierarchyNode> GetChildrenInSameScope(bool recursive)
        {
            IProjectItemScope scope = this.ProjectItem.Scope;
            List<IHierarchyNode> nodes = new List<IHierarchyNode>();
            foreach (IHierarchyNode child in this.Items)
            {
                if (child == null || child.ProjectItem == null)
                {
                    continue;
                }

                IProjectItemScope childScope = child.ProjectItem.Scope;
                if (childScope != null && object.ReferenceEquals(childScope, scope))
                {
                    nodes.Add(child);
                    if (recursive)
                    {
                        nodes.AddRange(child.GetChildrenInSameScope(true));
                    }
                }
            }

            return nodes;
        }

        /// <summary>
        /// Retrieves a iterator around the children underneath this node.
        /// </summary>
        /// <param name="recursive">
        /// A value indicating whether this method retrieves just the children of this node or
        /// recurses into them as well.
        /// </param>
        /// <returns>
        /// A iterator around the children underneath this node.
        /// </returns>
        public IEnumerable<IHierarchyNode> GetChildren(bool recursive)
        {
            List<IHierarchyNode> nodes = new List<IHierarchyNode>();
            foreach (IHierarchyNode child in this.Items)
            {
                if (child == null)
                {
                    continue;
                }

                nodes.Add(child);
                if (recursive)
                {
                    nodes.AddRange(child.GetChildren(true));
                }
            }

            return nodes;
        }

        /// <summary>
        /// Clears the item collection for this node.
        /// </summary>
        public void ClearChildren()
        {
            this.Items.Clear();
            if (this.Items.Count == 0)
            {
                this.IsExpandable = false;
            }
        }

        /// <summary>
        /// Attempts to retrieve the hierarchy node underneath this one that has the specified
        /// full path.
        /// </summary>
        /// <param name="fullPath">
        /// The full path of the item to try and find.
        /// </param>
        /// <returns>
        /// The hierarchy node underneath this node that has the specified full path if found;
        /// otherwise, false.
        /// </returns>
        public IHierarchyNode FindNodeWithFullPath(string fullPath)
        {
            if (this.Model != null)
            {
                if (String.Equals(this.Model.GetMetadata("Fullpath"), fullPath))
                {
                    return this;
                }
            }

            foreach (IHierarchyNode node in this.Items)
            {
                IHierarchyNode found = node.FindNodeWithFullPath(fullPath);
                if (found != null)
                {
                    return found;
                }
            }

            return null;
        }

        /// <summary>
        /// Removes the first occurrence of the specified item from this nodes child
        /// collection.
        /// </summary>
        /// <param name="item">
        /// The item to remove.
        /// </param>
        /// <returns>
        /// True if the item was present and has been successfully removed; otherwise, false.
        /// </returns>
        public bool RemoveChild(IHierarchyNode item)
        {
            if (!this.Items.Contains(item))
            {
                return false;
            }

            this.Items.Remove(item);
            if (this.Items.Count == 0)
            {
                this.IsExpandable = false;
            }

            return true;
        }

        /// <summary>
        /// Adds a new folder underneath this node and returns the instance to the new folder.
        /// </summary>
        /// <returns>
        /// The new folder that was created.
        /// </returns>
        public virtual IHierarchyNode AddNewFolder()
        {
            return null;
        }

        public int CompareTo(object obj)
        {
            IDisplayItem item = obj as IDisplayItem;
            if (item != null)
            {
                return Comparer.Default.Compare(this.Text, item.Text);
            }

            return 0;
        }
        #endregion Methods
    } // RSG.Project.Model.HierarchyNode {Class}
} // RSG.Project.Model {Namespace}
