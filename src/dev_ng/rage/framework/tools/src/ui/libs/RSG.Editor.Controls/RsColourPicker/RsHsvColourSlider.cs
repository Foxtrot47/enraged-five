﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsHsvColourSlider.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Data;
    using System.Windows.Media;
    using System.Windows.Threading;
    using RSG.Editor.Controls.Helpers;

    /// <summary>
    /// Represents a control that displays a slider and spinner that can control a single
    /// component of a base colour.
    /// </summary>
    public class RsHsvColourSlider : Control, IColourSlider
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="Brush" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty BrushProperty;

        /// <summary>
        /// Identifies the <see cref="Colour" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty ColourProperty;

        /// <summary>
        /// Identifies the <see cref="ColourUpdateMode" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty ColourUpdateModeProperty;

        /// <summary>
        /// Identifies the <see cref="Component" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty ComponentProperty;

        /// <summary>
        /// Identifies the <see cref="Maximum" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty MaximumProperty;

        /// <summary>
        /// Identifies the <see cref="Minimum" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty MinimumProperty;

        /// <summary>
        /// Identifies the <see cref="InternalColour" /> dependency property.
        /// </summary>
        private static readonly DependencyProperty _internalColourProperty;

        /// <summary>
        /// Identifies the <see cref="Brush" /> dependency property.
        /// </summary>
        private static DependencyPropertyKey _brushPropertyKey;

        /// <summary>
        /// A value indicating whether the background brush for this control needs to be
        /// updated when the colour changes. It only ever needs to be updated if the colour
        /// changes from a external source.
        /// </summary>
        private bool _dontUpdateBrushOnColourChanged;

        /// <summary>
        /// A value indicating whether the value needs to be updated when the colour changes.
        /// </summary>
        private bool _dontUpdateValueOnColourChanged;

        /// <summary>
        /// The private reference to the PART_Slider control that has been setup in the
        /// controls template.
        /// </summary>
        private Slider _slider;

        /// <summary>
        /// The private reference to the PART_Spinner control that has been setup in the
        /// controls template.
        /// </summary>
        private RsIntegerSpinner _spinner;

        /// <summary>
        /// The dispatcher timer used to delay the colour update by a certain time span when
        /// the <see cref="ColourUpdateMode"/> is set to Delay.
        /// </summary>
        private DispatcherTimer _timer;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsHsvColourSlider" /> class.
        /// </summary>
        static RsHsvColourSlider()
        {
            _brushPropertyKey =
                DependencyProperty.RegisterReadOnly(
                "Brush",
                typeof(Brush),
                typeof(RsHsvColourSlider),
                new PropertyMetadata(Brushes.Transparent));

            BrushProperty = _brushPropertyKey.DependencyProperty;

            ColourProperty =
                DependencyProperty.Register(
                "Colour",
                typeof(HsvColour),
                typeof(RsHsvColourSlider),
                new FrameworkPropertyMetadata(new HsvColour(0.0, 0.0, 0.0), OnColourChanged));

            ComponentProperty =
                DependencyProperty.Register(
                "Component",
                typeof(HsvColourSliderComponent),
                typeof(RsHsvColourSlider),
                new FrameworkPropertyMetadata(
                    HsvColourSliderComponent.None, OnComponentChanged));

            ColourUpdateModeProperty =
                DependencyProperty.Register(
                "ColourUpdateMode",
                typeof(ColourUpdateMode),
                typeof(RsHsvColourSlider),
                new FrameworkPropertyMetadata(ColourUpdateMode.Realtime));

            MaximumProperty =
                DependencyProperty.Register(
                "Maximum",
                typeof(int),
                typeof(RsHsvColourSlider),
                new PropertyMetadata(255, null, CoerceRangeValue));

            MinimumProperty =
                DependencyProperty.Register(
                "Minimum",
                typeof(int),
                typeof(RsHsvColourSlider),
                new PropertyMetadata(0, null, CoerceRangeValue));

            _internalColourProperty =
                DependencyProperty.Register(
                "InternalColour",
                typeof(HsvColour),
                typeof(RsHsvColourSlider),
                new FrameworkPropertyMetadata(
                    new HsvColour(0.0, 0.0, 0.0),
                    OnInternalColourChanged));

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsHsvColourSlider),
                new FrameworkPropertyMetadata(typeof(RsHsvColourSlider)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsHsvColourSlider" /> class.
        /// </summary>
        public RsHsvColourSlider()
        {
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs as soon as the drag operation has finished.
        /// </summary>
        public event EventHandler DragFinished;
        #endregion Events

        #region Properties
        /// <summary>
        /// Gets the brush that is used for the slider background of the control.
        /// </summary>
        public Brush Brush
        {
            get { return (Brush)this.GetValue(BrushProperty); }
            private set { this.SetValue(_brushPropertyKey, value); }
        }

        /// <summary>
        /// Gets or sets the colour that is being manipulated. This controls the colour range
        /// of the control.
        /// </summary>
        public HsvColour Colour
        {
            get { return (HsvColour)this.GetValue(ColourProperty); }
            set { this.SetValue(ColourProperty, value); }
        }

        /// <summary>
        /// Gets or sets the colour update mode that has been set on this control. This
        /// controls when and how often the colour gets updated from the user actions.
        /// </summary>
        public ColourUpdateMode ColourUpdateMode
        {
            get { return (ColourUpdateMode)this.GetValue(ColourUpdateModeProperty); }
            set { this.SetValue(ColourUpdateModeProperty, value); }
        }

        /// <summary>
        /// Gets or sets the colour component that this control will manipulate.
        /// </summary>
        public HsvColourSliderComponent Component
        {
            get { return (HsvColourSliderComponent)this.GetValue(ComponentProperty); }
            set { this.SetValue(ComponentProperty, value); }
        }

        /// <summary>
        /// Gets or sets the maximum range value for the slider and spinner.
        /// </summary>
        public int Maximum
        {
            get { return (int)this.GetValue(MaximumProperty); }
            set { this.SetValue(MaximumProperty, value); }
        }

        /// <summary>
        /// Gets or sets the minimum range value for the slider and spinner.
        /// </summary>
        public int Minimum
        {
            get { return (int)this.GetValue(MinimumProperty); }
            set { this.SetValue(MinimumProperty, value); }
        }

        /// <summary>
        /// Gets or sets the internal selected colour. This always gets updated in real time
        /// no matter what the <see cref="ColourUpdateMode"/> property has been set to.
        /// </summary>
        private HsvColour InternalColour
        {
            get { return (HsvColour)this.GetValue(_internalColourProperty); }
            set { this.SetValue(_internalColourProperty, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Clears the binding on the colour dependency property.
        /// </summary>
        public void ClearColourBinding()
        {
            BindingOperations.ClearBinding(this, RsHsvColourSlider.ColourProperty);
        }

        /// <summary>
        /// Gets called whenever the template is applied to this control. This makes sure we
        /// have references to the selector and drag area objects in the template as well as
        /// handle all of the events for them.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            if (this._slider != null)
            {
                this._slider.ValueChanged -= this.OnValueChanged;
                this._slider.RemoveHandler(
                    Thumb.DragCompletedEvent,
                    new RoutedEventHandler(this.OnDragCompletedEvent));
            }

            this._slider = GetTemplateChild("PART_Slider") as Slider;
            if (this._slider != null)
            {
                this._slider.ValueChanged += this.OnValueChanged;
                this._slider.AddHandler(
                    Thumb.DragCompletedEvent,
                    new RoutedEventHandler(this.OnDragCompletedEvent));
            }

            this._spinner = GetTemplateChild("PART_Spinner") as RsIntegerSpinner;
            this.UpdateValue();
        }

        /// <summary>
        /// Set the colour dependency property to the specified binding object.
        /// </summary>
        /// <param name="binding">
        /// The binding object to set on the colour dependency property.
        /// </param>
        public void SetColourBinding(Binding binding)
        {
            this.SetBinding(RsHsvColourSlider.ColourProperty, binding);
        }

        /// <summary>
        /// Called whenever the <see cref="Maximum"/> dependency property needs to be
        /// re-evaluated.
        /// </summary>
        /// <param name="s">
        /// The <see cref="RsHsvColourSlider"/> instance whose dependency property needs
        /// evaluated.
        /// </param>
        /// <param name="baseValue">
        /// The new value of the property, prior to any coercion attempt.
        /// </param>
        /// <returns>
        /// The coerced value.
        /// </returns>
        private static object CoerceRangeValue(DependencyObject s, object baseValue)
        {
            RsHsvColourSlider control = s as RsHsvColourSlider;
            if (control == null)
            {
                Debug.Assert(false, "Incorrect dependency property coerce cast.");
                return baseValue;
            }

            if (baseValue is int)
            {
                int value = (int)baseValue;
                if (value < 0)
                {
                    return 0;
                }

                switch (control.Component)
                {
                    case HsvColourSliderComponent.Hue:
                        if (value > 360)
                        {
                            return 360;
                        }

                        break;

                    case HsvColourSliderComponent.Saturation:
                    case HsvColourSliderComponent.Value:
                        if (value > 100)
                        {
                            return 100;
                        }

                        break;

                    case HsvColourSliderComponent.None:
                        if (value > 0)
                        {
                            return 0;
                        }

                        break;
                }

                return baseValue;
            }

            return baseValue;
        }

        /// <summary>
        /// Called whenever the <see cref="Colour"/> dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The object whose <see cref="Colour"/> dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnColourChanged(object s, DependencyPropertyChangedEventArgs e)
        {
            RsHsvColourSlider control = s as RsHsvColourSlider;
            if (control == null)
            {
                Debug.Assert(false, "Incorrect dependency property changed cast.");
                return;
            }

            if (!control._dontUpdateValueOnColourChanged)
            {
                control.UpdateValue();
            }

            if (!control._dontUpdateBrushOnColourChanged)
            {
                control.UpdateBrush();
            }
        }

        /// <summary>
        /// Called whenever the <see cref="Component"/> dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The object whose <see cref="Component"/> dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnComponentChanged(object s, DependencyPropertyChangedEventArgs e)
        {
            RsHsvColourSlider control = s as RsHsvColourSlider;
            if (control == null)
            {
                Debug.Assert(false, "Incorrect dependency property changed cast.");
                return;
            }

            control.UpdateBrush();
            control.UpdateValue();
            switch (control.Component)
            {
                case HsvColourSliderComponent.Hue:
                    control.Maximum = 360;
                    break;

                case HsvColourSliderComponent.Saturation:
                case HsvColourSliderComponent.Value:
                    control.Maximum = 100;
                    break;

                case HsvColourSliderComponent.None:
                    control.Maximum = 0;
                    break;
            }
        }

        /// <summary>
        /// Called whenever the <see cref="InternalColour"/> dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The object whose <see cref="InternalColour"/> dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnInternalColourChanged(
            object s, DependencyPropertyChangedEventArgs e)
        {
            RsHsvColourSlider slider = s as RsHsvColourSlider;
            if (slider == null)
            {
                Debug.Assert(false, "Incorrect dependency property changed cast.");
                return;
            }

            if (slider._timer != null)
            {
                slider._timer.Stop();
                slider._timer = null;
            }

            if (slider.ColourUpdateMode == ColourUpdateMode.Realtime)
            {
                slider.SetCurrentValue(RsHsvColourSlider.ColourProperty, e.NewValue);
            }
            else if (slider.ColourUpdateMode == ColourUpdateMode.Delay)
            {
                slider._timer = new DispatcherTimer();
                slider._timer.Interval = TimeSpan.FromMilliseconds(500);
                slider._timer.Tick += slider.OnTimerTick;
                slider._timer.Start();
            }
        }

        /// <summary>
        /// Called when the dragging event on the slider has finished. This is used to update
        /// the actual colour from the internal one if the <see cref="ColourUpdateMode"/> has
        /// been set to OnDragFinished.
        /// </summary>
        /// <param name="s">
        /// The object that sent the event.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private void OnDragCompletedEvent(object s, RoutedEventArgs e)
        {
            EventHandler handler = this.DragFinished;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }

            if (this.ColourUpdateMode == ColourUpdateMode.OnDragFinished)
            {
                this._dontUpdateValueOnColourChanged = true;
                this._dontUpdateBrushOnColourChanged = true;
                try
                {
                    this.SetCurrentValue(
                        RsHsvColourSlider.ColourProperty, this.InternalColour);
                }
                finally
                {
                    this._dontUpdateValueOnColourChanged = false;
                    this._dontUpdateBrushOnColourChanged = false;
                }
            }
        }

        /// <summary>
        /// Called after the update timer on the selected colour expires so that the update
        /// from internal colour to selected colour can take place.
        /// </summary>
        /// <param name="sender">
        /// The object that fired the event.
        /// </param>
        /// <param name="e">
        /// The System.EventArgs containing the event data.
        /// </param>
        private void OnTimerTick(object sender, EventArgs e)
        {
            this._timer.Stop();
            this._timer = null;
            this._dontUpdateValueOnColourChanged = true;
            this._dontUpdateBrushOnColourChanged = true;
            try
            {
                this.SetCurrentValue(RsHsvColourSlider.ColourProperty, this.InternalColour);
            }
            finally
            {
                this._dontUpdateValueOnColourChanged = false;
                this._dontUpdateBrushOnColourChanged = false;
            }
        }

        /// <summary>
        /// Called whenever the value changes on the spinner and we need to update the internal
        /// colour based on the new value.
        /// </summary>
        /// <param name="sender">
        /// The object that sent the event.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedPropertyChangedEventArgs containing the event data.
        /// </param>
        private void OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            this.UpdateInternalColour();
        }

        /// <summary>
        /// Updates the brush property from the colour and component properties.
        /// </summary>
        private void UpdateBrush()
        {
            switch (this.Component)
            {
                case HsvColourSliderComponent.Hue:
                    {
                        LinearGradientBrush linearBrush = new LinearGradientBrush();
                        linearBrush.StartPoint = new Point(0.0d, 0.5d);
                        linearBrush.EndPoint = new Point(1.0d, 0.0d);

                        List<Color> colourList = ColourUtilities.GenerateHsvSpectrum(0.0, 1.0);
                        double stopIncrement = 1.0 / colourList.Count;

                        int i;
                        for (i = 0; i < colourList.Count; i++)
                        {
                            linearBrush.GradientStops.Add(
                                new GradientStop(colourList[i], i * stopIncrement));
                        }

                        linearBrush.GradientStops[i - 1].Offset = 1.0;
                        this.Brush = linearBrush;
                    }

                    break;

                case HsvColourSliderComponent.Saturation:
                    {
                        HsvColour hsv = this.Colour;
                        Color startColour = ColourUtilities.ConvertHsvToRgb(hsv.H, 0.0, hsv.V);
                        Color endColour = ColourUtilities.ConvertHsvToRgb(hsv.H, 1.0, hsv.V);
                        LinearGradientBrush linearBrush = new LinearGradientBrush();
                        linearBrush.StartPoint = new Point(0.0d, 0.0d);
                        linearBrush.EndPoint = new Point(1.0d, 0.0d);
                        linearBrush.GradientStops.Add(new GradientStop(startColour, 0.0d));
                        linearBrush.GradientStops.Add(new GradientStop(endColour, 1.0f));
                        this.Brush = linearBrush;
                    }

                    break;

                case HsvColourSliderComponent.Value:
                    {
                        HsvColour hsv = this.Colour;
                        Color startColour = ColourUtilities.ConvertHsvToRgb(hsv.H, hsv.S, 0.0);
                        Color endColour = ColourUtilities.ConvertHsvToRgb(hsv.H, hsv.S, 1.0);
                        LinearGradientBrush linearBrush = new LinearGradientBrush();
                        linearBrush.StartPoint = new Point(0.0d, 0.0d);
                        linearBrush.EndPoint = new Point(1.0d, 0.0d);
                        linearBrush.GradientStops.Add(new GradientStop(startColour, 0.0d));
                        linearBrush.GradientStops.Add(new GradientStop(endColour, 1.0f));
                        this.Brush = linearBrush;
                    }

                    break;

                case HsvColourSliderComponent.None:
                    this.Brush = Brushes.Transparent;
                    break;
            }
        }

        /// <summary>
        /// Updates the colour property based on the current value.
        /// </summary>
        private void UpdateInternalColour()
        {
            HsvColour colour = this.Colour;
            double value = (double)this._spinner.Value;
            switch (this.Component)
            {
                case HsvColourSliderComponent.Hue:
                    {
                        colour = this.Colour;
                        colour.H = value;
                    }

                    break;

                case HsvColourSliderComponent.Saturation:
                    {
                        colour = this.Colour;
                        colour.S = value / 100.0;
                    }

                    break;

                case HsvColourSliderComponent.Value:
                    {
                        colour = this.Colour;
                        colour.V = value / 100.0;
                    }

                    break;

                case HsvColourSliderComponent.None:
                    colour = new HsvColour(0.0d, 0.0d, 0.0d);
                    break;
            }

            this.InternalColour = colour;
        }

        /// <summary>
        /// Updates the value property based on the colour and component values.
        /// </summary>
        private void UpdateValue()
        {
            if (this._spinner == null)
            {
                return;
            }

            switch (this.Component)
            {
                case HsvColourSliderComponent.Hue:
                    {
                        this._spinner.Value = (int)this.Colour.H;
                    }

                    break;

                case HsvColourSliderComponent.Saturation:
                    {
                        this._spinner.Value = (int)(this.Colour.S * 100.0);
                    }

                    break;

                case HsvColourSliderComponent.Value:
                    {
                        this._spinner.Value = (int)(this.Colour.V * 100.0);
                    }

                    break;

                case HsvColourSliderComponent.None:
                    this._spinner.Value = 0;
                    break;
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsHsvColourSlider {Class}
} // RSG.Editor.Controls {Namespace}
