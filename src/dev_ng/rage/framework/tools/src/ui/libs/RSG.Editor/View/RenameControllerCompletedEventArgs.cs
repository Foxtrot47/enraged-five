﻿//---------------------------------------------------------------------------------------------
// <copyright file="RenameControllerCompletedEventArgs.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    using System;

    /// <summary>
    /// Contains the event data for the rename controllers completed event.
    /// </summary>
    public class RenameControllerCompletedEventArgs : EventArgs
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Refocus"/> property.
        /// </summary>
        private bool _refocus;

        /// <summary>
        /// The private field used for the <see cref="Result"/> property.
        /// </summary>
        private RenameControllerCompletedResult _result;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="RenameControllerCompletedEventArgs"/>
        /// class.
        /// </summary>
        /// <param name="refocus">
        /// A value indicating whether the view item should regain focus after the operation.
        /// </param>
        /// <param name="result">
        /// The result of the renaming operation.
        /// </param>
        public RenameControllerCompletedEventArgs(
            bool refocus, RenameControllerCompletedResult result)
        {
            this._refocus = refocus;
            this._result = result;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether the view item should regain focus.
        /// </summary>
        public bool Refocus
        {
            get { return this._refocus; }
        }

        /// <summary>
        /// Gets the result of the rename controller.
        /// </summary>
        public RenameControllerCompletedResult Result
        {
            get { return this._result; }
        }
        #endregion Properties
    } // RSG.Editor.View.RenameControllerCompletedEventArgs {Class}
} // RSG.Editor.View {Namespace}
