﻿//---------------------------------------------------------------------------------------------
// <copyright file="ThemeBrushes.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Themes
{
    /// <summary>
    /// Defines all of the resource keys to dynamic brushes that are used to skin an
    /// application using the Editor Framework.
    /// </summary>
    public static class ThemeBrushes
    {
        #region Properties
        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of a
        /// content control when the mouse is positioned over it.
        /// </summary>
        public static object ContentBackgroundHoverKey
        {
            get { return "RsBrush.ContentBackgroundHoverKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of a
        /// inactive content control.
        /// </summary>
        public static object ContentBackgroundInactiveKey
        {
            get { return "RsBrush.ContentBackgroundInactiveKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of a
        /// content control.
        /// </summary>
        public static object ContentBackgroundKey
        {
            get { return "RsBrush.ContentBackgroundKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of a
        /// content control when the mouse is pressed down over it.
        /// </summary>
        public static object ContentBackgroundPressedKey
        {
            get { return "RsBrush.ContentBackgroundPressedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the border of a
        /// content control when the mouse is positioned over it.
        /// </summary>
        public static object ContentBorderHoverKey
        {
            get { return "RsBrush.ContentBorderHoverKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the border of a
        /// inactive content control.
        /// </summary>
        public static object ContentBorderInactiveKey
        {
            get { return "RsBrush.ContentBorderInactiveKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the border of a
        /// content control.
        /// </summary>
        public static object ContentBorderKey
        {
            get { return "RsBrush.ContentBorderKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the border of a
        /// content control when the mouse is pressed down over it.
        /// </summary>
        public static object ContentBorderPressedKey
        {
            get { return "RsBrush.ContentBorderPressedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the glyph inside a
        /// content control when the mouse is positioned over it.
        /// </summary>
        public static object ContentGlyphHoverKey
        {
            get { return "RsBrush.ContentGlyphHoverKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the glyph inside a
        /// inactive content control.
        /// </summary>
        public static object ContentGlyphInactiveKey
        {
            get { return "RsBrush.ContentGlyphInactiveKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the glyph inside a
        /// content control.
        /// </summary>
        public static object ContentGlyphKey
        {
            get { return "RsBrush.ContentGlyphKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the glyph inside a
        /// content control when the mouse is pressed down over it.
        /// </summary>
        public static object ContentGlyphPressedKey
        {
            get { return "RsBrush.ContentGlyphPressedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush that paints the text of a content
        /// control when the mouse is positioned over it.
        /// </summary>
        public static object ContentTextHoverKey
        {
            get { return "RsBrush.ContentTextHoverKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush that paints the text of a inactive
        /// content control.
        /// </summary>
        public static object ContentTextInactiveKey
        {
            get { return "RsBrush.ContentTextInactiveKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush that paints the text of a content
        /// control.
        /// </summary>
        public static object ContentTextKey
        {
            get { return "RsBrush.ContentTextKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush that paints the text of a content
        /// control when the mouse is pressed down over it.
        /// </summary>
        public static object ContentTextPressedKey
        {
            get { return "RsBrush.ContentTextPressedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush that paints the background of a
        /// content control when it is toggled.
        /// </summary>
        public static object ContentToggledBackgroundKey
        {
            get { return "RsBrush.ContentToggledBackgroundKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush that paints the border of a
        /// content control when it is toggled.
        /// </summary>
        public static object ContentToggledBorderKey
        {
            get { return "RsBrush.ContentToggledBorderKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush that paints the text of a content
        /// control when it is toggled.
        /// </summary>
        public static object ContentToggledTextKey
        {
            get { return "RsBrush.ContentToggledTextKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of a
        /// docking glyph button when the mouse is positioned over it.
        /// </summary>
        public static object DockingGlyphBackgroundHoverKey
        {
            get { return "RsBrush.DockingGlyphBackgroundHoverKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of a
        /// docking glyph button.
        /// </summary>
        public static object DockingGlyphBackgroundKey
        {
            get { return "RsBrush.DockingGlyphBackgroundKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of a
        /// docking glyph button when the mouse is pressed down over it.
        /// </summary>
        public static object DockingGlyphBackgroundPressedKey
        {
            get { return "RsBrush.DockingGlyphBackgroundPressedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the glyph on a
        /// docking button when the mouse is positioned over it.
        /// </summary>
        public static object DockingGlyphHoverKey
        {
            get { return "RsBrush.DockingGlyphHoverKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the glyph on a
        /// docking button.
        /// </summary>
        public static object DockingGlyphKey
        {
            get { return "RsBrush.DockingGlyphKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the glyph on a
        /// docking button when the mouse is pressed down over it.
        /// </summary>
        public static object DockingGlyphPressedKey
        {
            get { return "RsBrush.DockingGlyphPressedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of
        /// the glyph inside a docking item that is selected and active when the mouse is
        /// over on it.
        /// </summary>
        public static object DockingItemActiveSelectedGlyphBackgroundHoverKey
        {
            get { return "RsBrush.DockingItemActiveSelectedGlyphBackgroundHoverKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of
        /// the glyph inside a docking item that is selected and active.
        /// </summary>
        public static object DockingItemActiveSelectedGlyphBackgroundKey
        {
            get { return "RsBrush.DockingItemActiveSelectedGlyphBackgroundKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of
        /// the glyph inside a docking item that is selected and active when the mouse is
        /// pressed on it.
        /// </summary>
        public static object DockingItemActiveSelectedGlyphBackgroundPressedKey
        {
            get { return "RsBrush.DockingItemActiveSelectedGlyphBackgroundPressedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the glyph inside a
        /// docking item that is selected and active when the mouse is over on it.
        /// </summary>
        public static object DockingItemActiveSelectedGlyphHoverKey
        {
            get { return "RsBrush.DockingItemActiveSelectedGlyphHoverKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the glyph inside a
        /// docking item that is selected and active.
        /// </summary>
        public static object DockingItemActiveSelectedGlyphKey
        {
            get { return "RsBrush.DockingItemActiveSelectedGlyphKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the glyph inside a
        /// docking item that is selected and active when the mouse is pressed on it.
        /// </summary>
        public static object DockingItemActiveSelectedGlyphPressedKey
        {
            get { return "RsBrush.DockingItemActiveSelectedGlyphPressedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of
        /// the currently active docking item.
        /// </summary>
        public static object DockingItemBackgroundActiveSelectedKey
        {
            get { return "RsBrush.DockingItemBackgroundActiveSelectedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of a
        /// docking item when the mouse is positioned over them.
        /// </summary>
        public static object DockingItemBackgroundHoverKey
        {
            get { return "RsBrush.DockingItemBackgroundHoverKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of a
        /// selected docking item that is inactive.
        /// </summary>
        public static object DockingItemBackgroundInactiveSelectedKey
        {
            get { return "RsBrush.DockingItemBackgroundInactiveSelectedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of a
        /// docking item.
        /// </summary>
        public static object DockingItemBackgroundKey
        {
            get { return "RsBrush.DockingItemBackgroundKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of
        /// the glyph inside a docking item that has the mouse over it when the mouse is over
        /// on it.
        /// </summary>
        public static object DockingItemHoverGlyphBackgroundHoverKey
        {
            get { return "RsBrush.DockingItemHoverGlyphBackgroundHoverKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of
        /// the glyph inside a docking item that has the mouse over it.
        /// </summary>
        public static object DockingItemHoverGlyphBackgroundKey
        {
            get { return "RsBrush.DockingItemHoverGlyphBackgroundKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of
        /// the glyph inside a docking item that has the mouse over it when the mouse is
        /// pressed on it.
        /// </summary>
        public static object DockingItemHoverGlyphBackgroundPressedKey
        {
            get { return "RsBrush.DockingItemHoverGlyphBackgroundPressedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the glyph inside a
        /// docking item that has the mouse over it when the mouse is over on it.
        /// </summary>
        public static object DockingItemHoverGlyphHoverKey
        {
            get { return "RsBrush.DockingItemHoverGlyphHoverKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the glyph inside a
        /// docking item that has the mouse over it.
        /// </summary>
        public static object DockingItemHoverGlyphKey
        {
            get { return "RsBrush.DockingItemHoverGlyphKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the glyph inside a
        /// docking item that has the mouse over it when the mouse is pressed on it.
        /// </summary>
        public static object DockingItemHoverGlyphPressedKey
        {
            get { return "RsBrush.DockingItemHoverGlyphPressedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the text of a the
        /// currently active docking item.
        /// </summary>
        public static object DockingItemTextActiveSelectedKey
        {
            get { return "RsBrush.DockingItemTextActiveSelectedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the text of a
        /// docking item when the mouse is positioned over them.
        /// </summary>
        public static object DockingItemTextHoverKey
        {
            get { return "RsBrush.DockingItemTextHoverKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the text of a
        /// selected docking item that is inactive.
        /// </summary>
        public static object DockingItemTextInactiveSelectedKey
        {
            get { return "RsBrush.DockingItemTextInactiveSelectedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the text of a
        /// docking item.
        /// </summary>
        public static object DockingItemTextKey
        {
            get { return "RsBrush.DockingItemTextKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of
        /// the glyph inside a docking item that is selected and inactive when the mouse is
        /// over on it.
        /// </summary>
        public static object DockingItemInactiveSelectedGlyphBackgroundHoverKey
        {
            get { return "RsBrush.DockingItemInactiveSelectedGlyphBackgroundHoverKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of
        /// the glyph inside a docking item that is selected and inactive.
        /// </summary>
        public static object DockingItemInactiveSelectedGlyphBackgroundKey
        {
            get { return "RsBrush.DockingItemInactiveSelectedGlyphBackgroundKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of
        /// the glyph inside a docking item that is selected and inactive when the mouse is
        /// pressed on it.
        /// </summary>
        public static object DockingItemInactiveSelectedGlyphBackgroundPressedKey
        {
            get { return "RsBrush.DockingItemInactiveSelectedGlyphBackgroundPressedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the glyph inside a
        /// docking item that is selected and inactive when the mouse is over on it.
        /// </summary>
        public static object DockingItemInactiveSelectedGlyphHoverKey
        {
            get { return "RsBrush.DockingItemInactiveSelectedGlyphHoverKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the glyph inside a
        /// docking item that is selected and inactive.
        /// </summary>
        public static object DockingItemInactiveSelectedGlyphKey
        {
            get { return "RsBrush.DockingItemInactiveSelectedGlyphKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the glyph inside a
        /// docking item that is selected and inactive when the mouse is pressed on it.
        /// </summary>
        public static object DockingItemInactiveSelectedGlyphPressedKey
        {
            get { return "RsBrush.DockingItemInactiveSelectedGlyphPressedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background for
        /// the button of the expander control when it is expanded.
        /// </summary>
        public static object ExpanderButtonBackgroundExpandedKey
        {
            get { return "RsBrush.ExpanderButtonBackgroundExpandedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background for
        /// the button of the expander control when the mouse is positioned over it.
        /// </summary>
        public static object ExpanderButtonBackgroundHoverKey
        {
            get { return "RsBrush.ExpanderButtonBackgroundHoverKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background for
        /// the button of the expander control.
        /// </summary>
        public static object ExpanderButtonBackgroundKey
        {
            get { return "RsBrush.ExpanderButtonBackgroundKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the border for
        /// the button of the expander expander control when it is expanded.
        /// </summary>
        public static object ExpanderButtonBorderExpandedKey
        {
            get { return "RsBrush.ExpanderButtonBorderExpandedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the border for
        /// the button of the expander control when the mouse is positioned over it.
        /// </summary>
        public static object ExpanderButtonBorderHoverKey
        {
            get { return "RsBrush.ExpanderButtonBorderHoverKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the border for
        /// the button of the expander control.
        /// </summary>
        public static object ExpanderButtonBorderKey
        {
            get { return "RsBrush.ExpanderButtonBorderKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the text for
        /// the button of the expander expander control when it is expanded.
        /// </summary>
        public static object ExpanderButtonTextExpandedKey
        {
            get { return "RsBrush.ExpanderButtonTextExpandedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the text for the
        /// button of the expander control when the mouse is positioned over it.
        /// </summary>
        public static object ExpanderButtonTextHoverKey
        {
            get { return "RsBrush.ExpanderButtonTextHoverKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the text for the
        /// button of the expander control.
        /// </summary>
        public static object ExpanderButtonTextKey
        {
            get { return "RsBrush.ExpanderButtonTextKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush that paints the background of the
        /// error boxes or error text sat on the main UI.
        /// </summary>
        public static object ErrorVisualKey
        {
            get { return "RsBrush.ErrorVisualKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush that paints the background of a
        /// faded mark on the background.
        /// </summary>
        public static object FadedMarkKey
        {
            get { return "RsBrush.FadedMarkKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the border of a
        /// group control or separator.
        /// </summary>
        public static object GroupBorderKey
        {
            get { return "RsBrush.GroupBorderKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the highlight border
        /// of a group control or separator.
        /// </summary>
        public static object GroupHighlightBorderKey
        {
            get { return "RsBrush.GroupHighlightBorderKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of a
        /// content control that accepts the users keyboard input when the mouse is positioned
        /// over it.
        /// </summary>
        public static object InputBackgroundHoverKey
        {
            get { return "RsBrush.InputBackgroundHoverKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of a
        /// content control that accepts the users keyboard input.
        /// </summary>
        public static object InputBackgroundKey
        {
            get { return "RsBrush.InputBackgroundKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of a
        /// content control that accepts the users keyboard input when the mouse is pressed
        /// down over it.
        /// </summary>
        public static object InputBackgroundPressedKey
        {
            get { return "RsBrush.InputBackgroundPressedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the border of a
        /// content control that accepts the users keyboard input when the mouse is positioned
        /// over it.
        /// </summary>
        public static object InputBorderHoverKey
        {
            get { return "RsBrush.InputBorderHoverKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the border of a
        /// content control that accepts the users keyboard input.
        /// </summary>
        public static object InputBorderKey
        {
            get { return "RsBrush.InputBorderKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the border of a
        /// content control that accepts the users keyboard input when the mouse is pressed
        /// down over it.
        /// </summary>
        public static object InputBorderPressedKey
        {
            get { return "RsBrush.InputBorderPressedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of a
        /// item inside a items control when it is selected and contains the keyboard focus.
        /// </summary>
        public static object ItemBackgroundActiveSelectedKey
        {
            get { return "RsBrush.ItemBackgroundActiveSelectedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of a
        /// detail item inside a items control.
        /// </summary>
        public static object ItemBackgroundDetailKey
        {
            get { return "RsBrush.ItemBackgroundDetailKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of a
        /// item inside a items control when it is selected but doesn't contain the keyboard
        /// focus.
        /// </summary>
        public static object ItemBackgroundInactiveSelectedKey
        {
            get { return "RsBrush.ItemBackgroundInactiveSelectedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the border of a item
        /// inside a items control when it is selected and contains the keyboard focus.
        /// </summary>
        public static object ItemBorderActiveSelectedKey
        {
            get { return "RsBrush.ItemBorderActiveSelectedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the border of a item
        /// inside a items control when it is selected but doesn't contain the keyboard focus.
        /// </summary>
        public static object ItemBorderInactiveSelectedKey
        {
            get { return "RsBrush.ItemBorderInactiveSelectedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of a
        /// items control.
        /// </summary>
        public static object ItemsBackgroundKey
        {
            get { return "RsBrush.ItemsBackgroundKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the border of a
        /// items control.
        /// </summary>
        public static object ItemsBorderKey
        {
            get { return "RsBrush.ItemsBorderKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush that paints the text of a
        /// item inside a items control when it is selected and contains the keyboard focus.
        /// </summary>
        public static object ItemTextActiveSelectedKey
        {
            get { return "RsBrush.ItemTextActiveSelectedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush that paints the text of a inactive
        /// item inside a items control.
        /// </summary>
        public static object ItemTextInactiveKey
        {
            get { return "RsBrush.ItemTextInactiveKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush that paints the text of a
        /// item inside a items control when it is selected but doesn't contain the keyboard
        /// focus.
        /// </summary>
        public static object ItemTextInactiveSelectedKey
        {
            get { return "RsBrush.ItemTextInactiveSelectedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush that paints the text of a item
        /// inside a items control.
        /// </summary>
        public static object ItemTextKey
        {
            get { return "RsBrush.ItemTextKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of a
        /// button control located inside the non-client area of a window when the mouse is
        /// positioned over it.
        /// </summary>
        public static object NonClientButtonBackgroundHoverKey
        {
            get { return "RsBrush.NonClientButtonBackgroundHoverKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of a
        /// button control located inside the non-client area of a window when the mouse is
        /// pressed down over it.
        /// </summary>
        public static object NonClientButtonBackgroundPressedKey
        {
            get { return "RsBrush.NonClientButtonBackgroundPressedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the glyph inside the
        /// non-client area of a window when the mouse is positioned over it.
        /// </summary>
        public static object NonClientGlyphHoverKey
        {
            get { return "RsBrush.NonClientGlyphHoverKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the glyph inside the
        /// non-client area of a window when it is current disabled.
        /// </summary>
        public static object NonClientGlyphInactiveKey
        {
            get { return "RsBrush.NonClientGlyphInactiveKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the glyph inside the
        /// non-client area of a window.
        /// </summary>
        public static object NonClientGlyphKey
        {
            get { return "RsBrush.NonClientGlyphKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the glyph inside the
        /// non-client area of a window when the mouse is pressed down over it.
        /// </summary>
        public static object NonClientGlyphPressedKey
        {
            get { return "RsBrush.NonClientGlyphPressedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush that paints the text inside the
        /// non-client area of a window.
        /// </summary>
        public static object NonClientTextKey
        {
            get { return "RsBrush.NonClientTextKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of a
        /// popup control.
        /// </summary>
        public static object PopupBackgroundKey
        {
            get { return "RsBrush.PopupBackgroundKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the border of a
        /// popup control.
        /// </summary>
        public static object PopupBorderKey
        {
            get { return "RsBrush.PopupBorderKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of a
        /// checkbox inside a popup control when the item it is inside is highlighted.
        /// </summary>
        public static object PopupCheckBoxBackgroundHoverKey
        {
            get { return "RsBrush.PopupCheckBoxBackgroundHoverKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of a
        /// checkbox inside a popup control when the item it is inside is disabled.
        /// </summary>
        public static object PopupCheckBoxBackgroundInactiveKey
        {
            get { return "RsBrush.PopupCheckBoxBackgroundInactiveKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of a
        /// checkbox inside a popup control.
        /// </summary>
        public static object PopupCheckBoxBackgroundKey
        {
            get { return "RsBrush.PopupCheckBoxBackgroundKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the border of a
        /// checkbox inside a popup control when the item it is inside is highlighted.
        /// </summary>
        public static object PopupCheckBoxBorderHoverKey
        {
            get { return "RsBrush.PopupCheckBoxBorderHoverKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the border of a
        /// checkbox inside a popup control when the item it is inside is disabled.
        /// </summary>
        public static object PopupCheckBoxBorderInactiveKey
        {
            get { return "RsBrush.PopupCheckBoxBorderInactiveKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the border of a
        /// checkbox inside a popup control.
        /// </summary>
        public static object PopupCheckBoxBorderKey
        {
            get { return "RsBrush.PopupCheckBoxBorderKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the glyph of a
        /// checkbox inside a popup control when the item it is inside is highlighted.
        /// </summary>
        public static object PopupCheckBoxGlyphHoverKey
        {
            get { return "RsBrush.PopupCheckBoxGlyphHoverKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the glyph of a
        /// checkbox inside a popup control when the item it is inside is disabled.
        /// </summary>
        public static object PopupCheckBoxGlyphInactiveKey
        {
            get { return "RsBrush.PopupCheckBoxGlyphInactiveKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the glyph of a
        /// checkbox inside a popup control.
        /// </summary>
        public static object PopupCheckBoxGlyphKey
        {
            get { return "RsBrush.PopupCheckBoxGlyphKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the glyph inside a
        /// popup control when the mouse is positioned over it.
        /// </summary>
        public static object PopupGlyphHoverKey
        {
            get { return "RsBrush.PopupGlyphHoverKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the glyph inside a
        /// popup control when it is disabled.
        /// </summary>
        public static object PopupGlyphInactiveKey
        {
            get { return "RsBrush.PopupGlyphInactiveKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the glyph inside a
        /// popup control.
        /// </summary>
        public static object PopupGlyphKey
        {
            get { return "RsBrush.PopupGlyphKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of
        /// the icon column inside a popup control.
        /// </summary>
        public static object PopupIconColumnBackgroundKey
        {
            get { return "RsBrush.PopupIconColumnBackgroundKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of a
        /// item inside a popup control when the mouse is positioned over it.
        /// </summary>
        public static object PopupItemBackgroundHoverKey
        {
            get { return "RsBrush.PopupItemBackgroundHoverKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the border of a item
        /// inside a popup control when the mouse is positioned over it.
        /// </summary>
        public static object PopupItemBorderHoverKey
        {
            get { return "RsBrush.PopupItemBorderHoverKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush that paints the text of a item
        /// inside a popup control when the mouse is positioned over it.
        /// </summary>
        public static object PopupItemTextHoverKey
        {
            get { return "RsBrush.PopupItemTextHoverKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush that paints the text of a inactive
        /// item inside a popup control.
        /// </summary>
        public static object PopupItemTextInactiveKey
        {
            get { return "RsBrush.PopupItemTextInactiveKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush that paints the text of a item
        /// inside a popup control.
        /// </summary>
        public static object PopupItemTextKey
        {
            get { return "RsBrush.PopupItemTextKey"; }
        }

        /// <summary>
        /// Gets the resource key for the shadow colour for a popup control.
        /// </summary>
        public static object PopupShadowKey
        {
            get { return "RsBrush.PopupShadowKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the glyph on the
        /// scroll bar control when the mouse is positioned over it.
        /// </summary>
        public static object ScrollBarGlyphHoverKey
        {
            get { return "RsBrush.ScrollBarGlyphHoverKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the glyph on the
        /// scroll bar control.
        /// </summary>
        public static object ScrollBarGlyphKey
        {
            get { return "RsBrush.ScrollBarGlyphKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the glyph on the
        /// scroll bar control when the mouse is pressed down over it.
        /// </summary>
        public static object ScrollBarGlyphPressedKey
        {
            get { return "RsBrush.ScrollBarGlyphPressedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of
        /// the thumb control on the scroll bar.
        /// </summary>
        public static object ScrollBarThumbBackgroundKey
        {
            get { return "RsBrush.ScrollBarThumbBackgroundKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of
        /// the thumb control on the scroll bar when the mouse is positioned over it.
        /// </summary>
        public static object ScrollBarThumbHoverKey
        {
            get { return "RsBrush.ScrollBarThumbHoverKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of
        /// the thumb control on the scroll bar when the mouse is pressed down over it.
        /// </summary>
        public static object ScrollBarThumbPressedKey
        {
            get { return "RsBrush.ScrollBarThumbPressedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of
        /// the thumb control on the slider bar.
        /// </summary>
        public static object SliderThumbBackgroundKey
        {
            get { return "RsBrush.SliderThumbBackgroundKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the border of
        /// the thumb control on the slider bar.
        /// </summary>
        public static object SliderThumbBorderKey
        {
            get { return "RsBrush.SliderThumbBorderKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of
        /// the thumb control on the slider bar when the mouse is positioned over it.
        /// </summary>
        public static object SliderThumbHoverKey
        {
            get { return "RsBrush.SliderThumbHoverKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of
        /// the thumb control on the slider bar when the mouse is pressed down over it.
        /// </summary>
        public static object SliderThumbPressedKey
        {
            get { return "RsBrush.SliderThumbPressedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush that paints the background of the
        /// selected tab items content.
        /// </summary>
        public static object TabContentBackgroundKey
        {
            get { return "RsBrush.TabContentBackgroundKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush that paints the border of the
        /// selected tab items content.
        /// </summary>
        public static object TabContentBorderKey
        {
            get { return "RsBrush.TabContentBorderKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush that paints the text inside the
        /// selected tab items content.
        /// </summary>
        public static object TabContentTextKey
        {
            get { return "RsBrush.TabContentTextKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush that paints the break line of the
        /// tab control.
        /// </summary>
        public static object TabControlBreakLineKey
        {
            get { return "RsBrush.TabControlBreakLineKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush that paints the background of a
        /// tab item when the mouse is positioned over it.
        /// </summary>
        public static object TabItemBackgroundHoverKey
        {
            get { return "RsBrush.TabItemBackgroundHoverKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush that paints the background of a
        /// tab item.
        /// </summary>
        public static object TabItemBackgroundKey
        {
            get { return "RsBrush.TabItemBackgroundKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush that paints the background of a
        /// tab item when that item is selected.
        /// </summary>
        public static object TabItemBackgroundSelectedKey
        {
            get { return "RsBrush.TabItemBackgroundSelectedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush that paints the border of a tab
        /// item when that item is selected.
        /// </summary>
        public static object TabItemBorderSelectedKey
        {
            get { return "RsBrush.TabItemBorderSelectedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush that paints the text of a tab item
        /// when the mouse is positioned over it.
        /// </summary>
        public static object TabItemTextHoverKey
        {
            get { return "RsBrush.TabItemTextHoverKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush that paints the text of a tab
        /// item.
        /// </summary>
        public static object TabItemTextKey
        {
            get { return "RsBrush.TabItemTextKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush that paints the text of a tab item
        /// when that item is selected.
        /// </summary>
        public static object TabItemTextSelectedKey
        {
            get { return "RsBrush.TabItemTextSelectedKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush used to paint the background of
        /// a window.
        /// </summary>
        public static object WindowBackgroundKey
        {
            get { return "RsBrush.WindowBackgroundKey"; }
        }

        /// <summary>
        /// Gets the resource key for the colour used to paint the border of a window.
        /// </summary>
        public static object WindowBorderKey
        {
            get { return "RsBrush.WindowBorderKey"; }
        }

        /// <summary>
        /// Gets the resource key for the solid colour brush that paints the text inside the
        /// status bar of a window.
        /// </summary>
        public static object WindowStatusTextKey
        {
            get { return "RsBrush.WindowStatusTextKey"; }
        }
        #endregion Properties
    } // RSG.Editor.Controls.Themes.ThemeBrushes {Class}
} // RSG.Editor.Controls.Themes {Namespace}
