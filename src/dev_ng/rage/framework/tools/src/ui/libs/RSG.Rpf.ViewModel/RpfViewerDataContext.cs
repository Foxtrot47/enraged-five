﻿//---------------------------------------------------------------------------------------------
// <copyright file="RpfViewerDataContext.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Rpf.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using RSG.Editor;

    /// <summary>
    /// Represents the view model to attach to the RPF view control.
    /// </summary>
    public class RpfViewerDataContext : NotifyPropertyChangedBase
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="PackFileViewModel"/> property.
        /// </summary>
        private PackFileViewModel _packFileViewModel;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="RpfViewerDataContext"/> class.
        /// </summary>
        public RpfViewerDataContext()
        {
            this._packFileViewModel = new PackFileViewModel();
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs after an attempt has been made to load a file.
        /// </summary>
        public event EventHandler<RpfLoadedEventArgs> LoadedFile;

        /// <summary>
        /// Occurs after the current loaded file has been unloaded.
        /// </summary>
        public event EventHandler UnloadedFile;
        #endregion Events

        #region Properties
        /// <summary>
        /// Gets a iterator around all of the pack entry view models for this viewer.
        /// </summary>
        public IEnumerable<PackEntryViewModel> AllEntries
        {
            get
            {
                if (this.PackFileViewModel == null)
                {
                    return Enumerable.Empty<PackEntryViewModel>();
                }

                return this.PackFileViewModel.Entries;
            }
        }

        /// <summary>
        /// Gets the total raw number of entries for this viewer before filtering and
        /// searching.
        /// </summary>
        public int EntryCount
        {
            get
            {
                if (this.PackFileViewModel == null || this.PackFileViewModel.Entries == null)
                {
                    return 0;
                }

                return this.PackFileViewModel.Entries.Count;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the pack file data for this view model has been
        /// loaded.
        /// </summary>
        public bool Loaded
        {
            get
            {
                if (this.PackFileViewModel == null)
                {
                    return false;
                }

                return this.PackFileViewModel.Loaded;
            }
        }

        /// <summary>
        /// Gets the pack file view model that contains the data to show the user in the main
        /// data grid.
        /// </summary>
        public PackFileViewModel PackFileViewModel
        {
            get { return this._packFileViewModel; }
            private set { this.SetProperty(ref this._packFileViewModel, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Loads the file located at the specified file path.
        /// </summary>
        /// <param name="path">
        /// The location of the file to load.
        /// </param>
        public void Load(string path)
        {
            bool successful = this._packFileViewModel.LoadPackFile(path);
            EventHandler<RpfLoadedEventArgs> handler = this.LoadedFile;
            if (handler == null)
            {
                return;
            }

            handler(this, new RpfLoadedEventArgs(path, successful));
        }

        /// <summary>
        /// Loads the file located at the specified file path.
        /// </summary>
        /// <param name="path">
        /// The location of the file to load.
        /// </param>
        /// <returns>
        /// The task that represents the asynchronous Load operation.
        /// </returns>
        public async Task LoadAsync(string path)
        {
            bool successful = await this._packFileViewModel.LoadPackFileAsync(path);
            EventHandler<RpfLoadedEventArgs> handler = this.LoadedFile;
            if (handler == null)
            {
                return;
            }

            handler(this, new RpfLoadedEventArgs(path, successful));
        }

        /// <summary>
        /// Unloads the currently loaded pack file.
        /// </summary>
        public void Unload()
        {
            this._packFileViewModel.UnloadPackFile();
            EventHandler handler = this.UnloadedFile;
            if (handler == null)
            {
                return;
            }

            handler(this, EventArgs.Empty);
        }
        #endregion Methods
    } // RSG.Rpf.ViewModel.RpfViewerDataContext {Class}
} // RSG.Rpf.ViewModel {Namespace}
