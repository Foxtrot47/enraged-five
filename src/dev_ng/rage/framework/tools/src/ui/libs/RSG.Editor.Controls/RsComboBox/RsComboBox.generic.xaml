﻿
<!--
    <copyright file="RsComboBox.generic.xaml" company="Rockstar">
    Copyright © Rockstar Games 2013. All rights reserved
    </copyright>
-->

<ResourceDictionary xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
                    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
                    xmlns:adp="clr-namespace:RSG.Editor.Controls.AttachedDependencyProperties"
                    xmlns:chromes="clr-namespace:RSG.Editor.Controls.Chromes"
                    xmlns:ctrls="clr-namespace:RSG.Editor.Controls"
                    xmlns:ext="clr-namespace:RSG.Editor.Controls.Extensions">

    <ResourceDictionary.MergedDictionaries>
        <ResourceDictionary Source="/RSG.Editor.Controls;component/Geometry.xaml" />
        <ResourceDictionary Source="/RSG.Editor.Controls;component/ErrorTemplate.xaml" />
        <ResourceDictionary Source="/RSG.Editor.Controls;component/FocusVisualStyle.xaml" />
    </ResourceDictionary.MergedDictionaries>

    <!--
        Default style for the type :-
        x:Type={RSG.Editor.Controls.RsComboBox}
    -->
    <Style TargetType="{x:Type ctrls:RsComboBox}">
        <Setter Property="FrameworkElement.FocusVisualStyle"
                Value="{StaticResource ComboBoxFocusVisualStyle}" />
        <Setter Property="Control.Background"
                Value="{ext:ThemeResource InputBackgroundKey}" />
        <Setter Property="Control.BorderBrush"
                Value="{ext:ThemeResource ContentBorderKey}" />
        <Setter Property="Control.BorderThickness"
                Value="1" />
        <Setter Property="Control.Foreground"
                Value="{ext:ThemeResource ContentTextKey}" />
        <Setter Property="UIElement.AllowDrop"
                Value="True" />
        <Setter Property="ScrollViewer.PanningMode"
                Value="VerticalFirst" />
        <Setter Property="Stylus.IsFlicksEnabled"
                Value="False" />
        <Setter Property="Control.Padding"
                Value="3,2,2,3" />
        <Setter Property="Validation.ErrorTemplate"
                Value="{StaticResource ValidationErrorTemplate}" />
        <Setter Property="HorizontalContentAlignment"
                Value="Left" />
        <Setter Property="VerticalContentAlignment"
                Value="Center" />
        <Setter Property="ScrollViewer.HorizontalScrollBarVisibility"
                Value="Auto" />
        <Setter Property="ScrollViewer.VerticalScrollBarVisibility"
                Value="Auto" />
        <Setter Property="Control.Template">
            <Setter.Value>
                <ControlTemplate TargetType="{x:Type ctrls:RsComboBox}">
                    <Border Name="ContainerBorder"
                            Background="{TemplateBinding Control.Background}"
                            BorderBrush="{TemplateBinding Control.BorderBrush}"
                            BorderThickness="{TemplateBinding Control.BorderThickness}"
                            Padding="0">
                        <Grid Name="MainGrid"
                              SnapsToDevicePixels="True">
                            <Grid.ColumnDefinitions>
                                <ColumnDefinition Width="*" />
                                <ColumnDefinition Width="13" />
                            </Grid.ColumnDefinitions>

                            <Popup Name="PART_Popup"
                                   Grid.ColumnSpan="2"
                                   Margin="1"
                                   AllowsTransparency="True"
                                   IsOpen="{Binding Path=IsDropDownOpen,
                                                    RelativeSource={RelativeSource TemplatedParent}}"
                                   Placement="Bottom"
                                   PopupAnimation="{DynamicResource {x:Static SystemParameters.ComboBoxPopupAnimationKey}}">
                                <chromes:SystemDropShadowChrome MaxHeight="{TemplateBinding ComboBox.MaxDropDownHeight}"
                                                                Margin="0,0,5,5"
                                                                Colour="{ext:ThemeResource PopupShadowKey}"
                                                                SnapsToDevicePixels="True">
                                    <Border Name="DropDownBorder"
                                            MinWidth="{Binding ElementName=ContainerBorder,
                                                               Path=ActualWidth}"
                                            MaxHeight="{TemplateBinding ComboBox.MaxDropDownHeight}"
                                            Background="{ext:ThemeResource PopupBackgroundKey}"
                                            BorderBrush="{ext:ThemeResource PopupBorderKey}"
                                            BorderThickness="1"
                                            adp:ThemeProperties.ImageBackgroundColour="{ext:ThemeResource PopupBackgroundKey}">
                                        <ScrollViewer Name="DropDownScrollViewer"
                                                      Background="{ext:ThemeResource PopupBackgroundKey}"
                                                      Padding="2,0,2,0"
                                                      VerticalScrollBarVisibility="Auto">
                                            <Grid RenderOptions.ClearTypeHint="Enabled">
                                                <ItemsPresenter Name="ItemsPresenter"
                                                                KeyboardNavigation.DirectionalNavigation="Contained"
                                                                SnapsToDevicePixels="{TemplateBinding UIElement.SnapsToDevicePixels}" />
                                            </Grid>
                                        </ScrollViewer>
                                    </Border>
                                </chromes:SystemDropShadowChrome>
                            </Popup>

                            <ToggleButton Grid.ColumnSpan="2"
                                          Background="{TemplateBinding Control.Background}"
                                          BorderBrush="{TemplateBinding Control.BorderBrush}"
                                          BorderThickness="0,0,0,0"
                                          Focusable="False"
                                          IsChecked="{Binding Path=IsDropDownOpen,
                                                              Mode=TwoWay,
                                                              RelativeSource={RelativeSource TemplatedParent}}"
                                          Padding="0,0,0,0" />

                            <Border Name="ArrowBorder"
                                    Grid.Column="2"
                                    Background="{TemplateBinding Control.Background}"
                                    BorderBrush="{TemplateBinding Control.BorderBrush}"
                                    BorderThickness="0"
                                    IsHitTestVisible="False">
                                <Path Name="Arrow"
                                      Margin="0"
                                      HorizontalAlignment="Center"
                                      VerticalAlignment="Center"
                                      Data="{StaticResource DownArrowGeometry}"
                                      Fill="{ext:ThemeResource ContentGlyphKey}"
                                      IsHitTestVisible="False" />
                            </Border>

                            <ContentPresenter Margin="{TemplateBinding Control.Padding}"
                                              HorizontalAlignment="{TemplateBinding Control.HorizontalContentAlignment}"
                                              VerticalAlignment="{TemplateBinding Control.VerticalContentAlignment}"
                                              Content="{TemplateBinding ctrls:RsComboBox.DisplayedItem}"
                                              ContentStringFormat="{TemplateBinding ComboBox.SelectionBoxItemStringFormat}"
                                              ContentTemplate="{TemplateBinding ComboBox.SelectionBoxItemTemplate}"
                                              ContentTemplateSelector="{TemplateBinding ItemsControl.ItemTemplateSelector}"
                                              IsHitTestVisible="False"
                                              SnapsToDevicePixels="{TemplateBinding UIElement.SnapsToDevicePixels}" />
                        </Grid>
                    </Border>
                    <ControlTemplate.Triggers>
                        <Trigger Property="ItemsControl.HasItems" Value="False">
                            <Setter TargetName="DropDownBorder"
                                    Property="FrameworkElement.Height"
                                    Value="95" />
                        </Trigger>
                        <Trigger Property="UIElement.IsMouseOver" Value="True">
                            <Setter Property="Control.BorderBrush"
                                    Value="{ext:ThemeResource ContentBorderHoverKey}" />
                            <Setter Property="Control.Background"
                                    Value="{ext:ThemeResource InputBackgroundHoverKey}" />
                            <Setter TargetName="ArrowBorder"
                                    Property="Border.BorderThickness"
                                    Value="1,0,0,0" />
                            <Setter TargetName="ArrowBorder"
                                    Property="Border.BorderBrush"
                                    Value="{ext:ThemeResource ContentBackgroundPressedKey}" />
                            <Setter TargetName="ArrowBorder"
                                    Property="Border.Background"
                                    Value="{ext:ThemeResource ContentBackgroundHoverKey}" />
                            <Setter TargetName="Arrow"
                                    Property="Shape.Fill"
                                    Value="{ext:ThemeResource ContentGlyphHoverKey}" />
                        </Trigger>
                        <Trigger Property="UIElement.IsKeyboardFocusWithin" Value="True">
                            <Setter TargetName="Arrow"
                                    Property="Shape.Fill"
                                    Value="{ext:ThemeResource ContentGlyphHoverKey}" />
                        </Trigger>
                        <Trigger Property="IsDropDownOpen" Value="True">
                            <Setter Property="Control.BorderBrush"
                                    Value="{ext:ThemeResource ContentBorderPressedKey}" />
                            <Setter Property="Control.Background"
                                    Value="{ext:ThemeResource InputBackgroundPressedKey}" />
                            <Setter TargetName="ArrowBorder"
                                    Property="Border.BorderThickness"
                                    Value="1,0,0,0" />
                            <Setter TargetName="ArrowBorder"
                                    Property="Border.BorderBrush"
                                    Value="{ext:ThemeResource ContentBorderPressedKey}" />
                            <Setter TargetName="ArrowBorder"
                                    Property="Border.Background"
                                    Value="{ext:ThemeResource ContentBackgroundPressedKey}" />
                            <Setter TargetName="Arrow"
                                    Property="Shape.Fill"
                                    Value="{ext:ThemeResource ContentGlyphPressedKey}" />
                        </Trigger>
                        <Trigger Property="UIElement.IsEnabled" Value="False">
                            <Setter Property="TextElement.Foreground"
                                    Value="{ext:ThemeResource ContentTextInactiveKey}" />
                            <Setter Property="Control.Background"
                                    Value="{ext:ThemeResource ContentBackgroundInactiveKey}" />
                            <Setter Property="Control.BorderBrush"
                                    Value="{ext:ThemeResource ContentBorderInactiveKey}" />
                            <Setter TargetName="Arrow"
                                    Property="Shape.Fill"
                                    Value="{ext:ThemeResource ContentGlyphInactiveKey}" />
                        </Trigger>
                    </ControlTemplate.Triggers>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>
</ResourceDictionary>