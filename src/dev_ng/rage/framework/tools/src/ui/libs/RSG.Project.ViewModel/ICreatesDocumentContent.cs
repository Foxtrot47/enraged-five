﻿//---------------------------------------------------------------------------------------------
// <copyright file="ICreatesDocumentContent.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel
{
    using RSG.Base.Logging;
    using RSG.Editor.Model;

    /// <summary>
    /// When implemented represents a object that provides a
    /// <see cref="ICreateDocumentContentController"/> instance that can be used to create the
    /// content object for the object inside a document tab.
    /// </summary>
    public interface ICreatesDocumentContent : IHierarchyNode
    {
        #region Properties
        /// <summary>
        /// Gets the creates document content controller that can be used to create a content
        /// object for this object inside a document tab.
        /// </summary>
        ICreateDocumentContentController CreateDocumentContentController { get; }
        #endregion Properties
    } // RSG.Project.ViewModel.ICreatesDocumentContent {Interface}
} // RSG.Project.ViewModel {Namespace}
