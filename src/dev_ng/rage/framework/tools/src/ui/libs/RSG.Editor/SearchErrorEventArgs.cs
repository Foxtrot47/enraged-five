﻿//---------------------------------------------------------------------------------------------
// <copyright file="SearchErrorEventArgs.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;

    /// <summary>
    /// Represents the class containing the data for a error event during a search command.
    /// </summary>
    internal class SearchErrorEventArgs : EventArgs
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Message"/> property.
        /// </summary>
        private string _message;

        /// <summary>
        /// The private field used for the <see cref="ParsingError"/> property.
        /// </summary>
        private bool _parsingError;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SearchErrorEventArgs"/> class.
        /// </summary>
        public SearchErrorEventArgs()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the message that describes the error that has been generated.
        /// </summary>
        public string Message
        {
            get { return this._message; }
            set { this._message = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the error this data is describing is
        /// actually a parsing error.
        /// </summary>
        public bool ParsingError
        {
            get { return this._parsingError; }
            set { this._parsingError = value; }
        }
        #endregion Properties
    } // RSG.Editor.SearchErrorEventArgs {Class}
} // RSG.Editor {Namespaace}
