﻿//---------------------------------------------------------------------------------------------
// <copyright file="VoiceCollection.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.ViewModel
{
    using RSG.Editor.Model;
    using RSG.Editor.View;
    using RSG.Text.Model;

    /// <summary>
    /// Provides a collection class that contains <see cref="DialogueVoiceViewModel"/>
    /// objects that are kept in sync with a collection of <see cref="DialogueVoice"/>
    /// objects.
    /// </summary>
    public class VoiceCollection
        : CollectionConverter<DialogueVoice, DialogueVoiceViewModel>,
        IReadOnlyViewModelCollection<DialogueVoiceViewModel>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="VoiceCollection"/> class.
        /// </summary>
        /// <param name="source">
        /// The model collection containing the source <see cref="DialogueVoice"/> objects
        /// for this collection.
        /// </param>
        public VoiceCollection(ModelCollection<DialogueVoice> source)
            : base(source)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Converts a single <see cref="DialogueVoice"/> object to a new
        /// <see cref="DialogueVoiceViewModel"/> object.
        /// </summary>
        /// <param name="item">
        /// The item to convert.
        /// </param>
        /// <returns>
        /// A new <see cref="DialogueVoiceViewModel"/> object from the given
        /// <see cref="DialogueVoice"/> object.
        /// </returns>
        protected override DialogueVoiceViewModel ConvertItem(DialogueVoice item)
        {
            DialogueVoiceViewModel newViewModel = new DialogueVoiceViewModel(item);
            return newViewModel;
        }
        #endregion Methods
    } // RSG.Text.ViewModel.VoiceCollection {Class}
} // RSG.Text.ViewModel {Namespace}
