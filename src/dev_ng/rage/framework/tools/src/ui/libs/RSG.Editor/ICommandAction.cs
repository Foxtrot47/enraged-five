﻿//---------------------------------------------------------------------------------------------
// <copyright file="ICommandAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Windows;

    /// <summary>
    /// When implemented represents a command implementer class that elements or types can bind
    /// to.
    /// </summary>
    public interface ICommandAction
    {
        #region Methods
        /// <summary>
        /// Binds this implementation to the specified command and the specified user interface
        /// element.
        /// </summary>
        /// <param name="command">
        /// The command to bind to this implementation.
        /// </param>
        /// <param name="element">
        /// The element that this implementation will be bound to.
        /// </param>
        void AddBinding(RockstarRoutedCommand command, UIElement element);

        /// <summary>
        /// Binds this implementation to the specified command for the specified type.
        /// </summary>
        /// <param name="command">
        /// The command to bind to this implementation.
        /// </param>
        /// <param name="type">
        /// The class with which to register the command binding to.
        /// </param>
        void AddBinding(RockstarRoutedCommand command, Type type);
        #endregion Methods
    } // RSG.Editor.ICommandAction {Interface}
} // RSG.Editor {Namespace}
