﻿//---------------------------------------------------------------------------------------------
// <copyright file="DefaultState.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.View
{
    using System.Diagnostics;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Documents;
    using System.Windows.Media;
    using RSG.Editor;
    using RSG.Editor.Controls.Helpers;

    /// <summary>
    /// Provides methods and attached properties that supports showing and hiding a adorner to
    /// show that a tunable value is set to default.
    /// </summary>
    public static class DefaultState
    {
        #region Fields
        /// <summary>
        /// Identifies the DefaultStateTemplate dependency property.
        /// </summary>
        public static readonly DependencyProperty DefaultStateTemplateProperty;

        /// <summary>
        /// Identifies the IsDefaultState dependency property.
        /// </summary>
        public static readonly DependencyProperty IsDefaultStateProperty;

        /// <summary>
        /// Identifies the DefaultStateAdorner dependency property.
        /// </summary>
        private static readonly DependencyProperty _defaultStateAdornerProperty;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="DefaultState"/> class.
        /// </summary>
        static DefaultState()
        {
            IsDefaultStateProperty =
                DependencyProperty.RegisterAttached(
                "IsDefaultState",
                typeof(bool),
                typeof(DefaultState),
                new FrameworkPropertyMetadata(
                    false,
                    new PropertyChangedCallback(OnIsDefaultStateChanged)));

            DefaultStateTemplateProperty =
                DependencyProperty.RegisterAttached(
                "DefaultStateTemplate",
                typeof(ControlTemplate),
                typeof(DefaultState),
                new FrameworkPropertyMetadata(
                    null,
                    new PropertyChangedCallback(OnDefaultStateTemplateChanged)));

            _defaultStateAdornerProperty =
                DependencyProperty.RegisterAttached(
                "DefaultStateAdorner",
                typeof(DefaultStateAdorner),
                typeof(DefaultState),
                new FrameworkPropertyMetadata(
                    null, FrameworkPropertyMetadataOptions.NotDataBindable));
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Retrieves the IsDefaultState dependency property value attached to the
        /// specified object.
        /// </summary>
        /// <param name="obj">
        /// The object whose attached IsDefaultState dependency property value will be
        /// returned.
        /// </param>
        /// <returns>
        /// The IsDefaultState dependency property value attached to the specified object.
        /// </returns>
        public static bool GetIsDefaultState(Visual obj)
        {
            if (obj == null)
            {
                throw new SmartArgumentNullException(() => obj);
            }

            return (bool)obj.GetValue(IsDefaultStateProperty);
        }

        /// <summary>
        /// Hides the default state adorner for the specified visual target.
        /// </summary>
        /// <param name="target">
        /// The visual whose default state adorner should be hidden.
        /// </param>
        public static void HideDefaultStateAdorner(Visual target)
        {
            if (target == null)
            {
                return;
            }

            AdornerLayer layer = AdornerLayer.GetAdornerLayer(target);
            DefaultStateAdorner adorner = GetDefaultStateAdorner(target);
            if (layer == null || adorner == null)
            {
                return;
            }

            adorner.ClearChild();
            layer.Remove(adorner);
            target.ClearValue(_defaultStateAdornerProperty);
        }

        /// <summary>
        /// Sets the DefaultStateTemplate dependency property value on the specified object
        /// to the specified value.
        /// </summary>
        /// <param name="obj">
        /// The object whose attached DefaultStateTemplate dependency property value will be
        /// set.
        /// </param>
        /// <param name="value">
        /// The value to set the attached DefaultStateTemplate dependency property to on the
        /// specified object.
        /// </param>
        public static void SetDefaultStateTemplate(Visual obj, ControlTemplate value)
        {
            if (obj == null)
            {
                throw new SmartArgumentNullException(() => obj);
            }

            obj.SetValue(DefaultStateTemplateProperty, value);
            ShowDefaultStateAdorner(obj);
        }

        /// <summary>
        /// Sets the IsDefaultState dependency property value on the specified object to the
        /// specified value.
        /// </summary>
        /// <param name="obj">
        /// The object whose attached IsDefaultState dependency property value will be set.
        /// </param>
        /// <param name="value">
        /// The value to set the attached IsDefaultState dependency property to on the
        /// specified object.
        /// </param>
        public static void SetIsDefaultState(Visual obj, bool value)
        {
            if (obj == null)
            {
                throw new SmartArgumentNullException(() => obj);
            }

            obj.SetValue(IsDefaultStateProperty, value);
        }

        /// <summary>
        /// Shows the default state adorner for the specified visual target.
        /// </summary>
        /// <param name="target">
        /// The visual whose default state adorner should be shown.
        /// </param>
        public static void ShowDefaultStateAdorner(Visual target)
        {
            FrameworkElement element = target as FrameworkElement;
            if (element == null || target == null)
            {
                Debug.Assert(false, "The visual target for a adorner has to be a element");
                return;
            }

            AdornerLayer layer = AdornerLayer.GetAdornerLayer(target);
            if (layer == null)
            {
                Debug.Assert(false, "Cannot show a adorner without a valid layer");
                return;
            }

            DefaultStateAdorner currentAdorner = GetDefaultStateAdorner(target);
            if (currentAdorner != null)
            {
                HideDefaultStateAdorner(target);
                currentAdorner = GetDefaultStateAdorner(target);
                if (currentAdorner != null)
                {
                    Debug.Assert(false, "Unable to hide the current adorner");
                    return;
                }

                ShowDefaultStateAdorner(target);
                return;
            }

            ControlTemplate defaultTemplate = GetDefaultStateTemplate(target);
            currentAdorner = new DefaultStateAdorner(element, defaultTemplate);

            layer.Add(currentAdorner);
            target.SetValue(_defaultStateAdornerProperty, currentAdorner);
        }

        /// <summary>
        /// Retrieves the DefaultStateAdorner dependency property value attached to the
        /// specified object.
        /// </summary>
        /// <param name="obj">
        /// The object whose attached DefaultStateAdorner dependency property value will be
        /// returned.
        /// </param>
        /// <returns>
        /// The DefaultStateAdorner dependency property value attached to the specified
        /// object.
        /// </returns>
        private static DefaultStateAdorner GetDefaultStateAdorner(Visual obj)
        {
            if (obj == null)
            {
                throw new SmartArgumentNullException(() => obj);
            }

            return obj.GetValue(_defaultStateAdornerProperty) as DefaultStateAdorner;
        }

        /// <summary>
        /// Retrieves the DefaultStateTemplate dependency property value attached to the
        /// specified object.
        /// </summary>
        /// <param name="obj">
        /// The object whose attached DefaultStateTemplate dependency property value will be
        /// returned.
        /// </param>
        /// <returns>
        /// The DefaultStateTemplate dependency property value attached to the specified
        /// object.
        /// </returns>
        private static ControlTemplate GetDefaultStateTemplate(Visual obj)
        {
            if (obj == null)
            {
                throw new SmartArgumentNullException(() => obj);
            }

            return obj.GetValue(DefaultStateTemplateProperty) as ControlTemplate;
        }

        /// <summary>
        /// Called whenever the DefaultStateTemplate dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The object whose DefaultStateTemplate dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnDefaultStateTemplateChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            Visual visual = s as Visual;
            if (visual == null)
            {
                return;
            }

            DefaultStateAdorner adorner = GetDefaultStateAdorner(visual);
            if (adorner == null)
            {
                return;
            }

            HideDefaultStateAdorner(visual);
            ShowDefaultStateAdorner(visual);

            if (e.NewValue != null)
            {
                FrameworkElement element = visual as FrameworkElement;
                if (element != null)
                {
                    element.Loaded += OnElementLoaded;
                }
            }
            else
            {
                FrameworkElement element = visual as FrameworkElement;
                if (element != null)
                {
                    element.Loaded -= OnElementLoaded;
                }
            }
        }

        /// <summary>
        /// Called whenever a element that has been marked to show the default state adorner
        /// gets loaded.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to. The element that was just loaded.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private static void OnElementLoaded(object s, RoutedEventArgs e)
        {
            Visual visual = s as Visual;
            if (visual == null)
            {
                return;
            }

            DefaultStateAdorner adorner = GetDefaultStateAdorner(visual);
            if (adorner == null)
            {
                return;
            }

            HideDefaultStateAdorner(visual);
            ShowDefaultStateAdorner(visual);
        }

        /// <summary>
        /// Called whenever the IsDefaultState dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The object whose IsDefaultState dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnIsDefaultStateChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            Visual visual = s as Visual;
            if (visual == null || !(e.NewValue is bool))
            {
                return;
            }

            bool flag = (bool)e.NewValue;
            if (flag)
            {
                ShowDefaultStateAdorner(visual);
            }
            else
            {
                HideDefaultStateAdorner(visual);
            }
        }
        #endregion Methods
    } // RSG.Metadata.View.DefaultState {Class}
} // RSG.Metadata.View {Namespace}
