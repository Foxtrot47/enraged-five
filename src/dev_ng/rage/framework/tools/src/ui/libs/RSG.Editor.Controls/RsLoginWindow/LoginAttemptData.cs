﻿//---------------------------------------------------------------------------------------------
// <copyright file="LoginAttemptData.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using RSG.Editor.Controls.Resources;

    /// <summary>
    /// Represents the object that is sent with the delegate determining whether the user has
    /// successfully logged in using the login window.
    /// </summary>
    public class LoginAttemptData
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="AttemptCount"/> property.
        /// </summary>
        private int _attemptCount;

        /// <summary>
        /// The private field used for the <see cref="ErrorMessage"/> property.
        /// </summary>
        private string _errorMessage;

        /// <summary>
        /// The private field used for the <see cref="Password"/> property.
        /// </summary>
        private string _password;

        /// <summary>
        /// The private field used for the <see cref="Retry"/> property.
        /// </summary>
        private bool _retry;

        /// <summary>
        /// The private field used for the <see cref="Username"/> property.
        /// </summary>
        private string _username;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="LoginAttemptData"/> class.
        /// </summary>
        /// <param name="username">
        /// The submitted username.
        /// </param>
        /// <param name="password">
        /// The submitted password.
        /// </param>
        /// <param name="retry">
        /// A value indicating whether the user can attempt again if this attempt fails.
        /// </param>
        /// <param name="attempts">
        /// The number of attempts the user has had at logging in.
        /// </param>
        public LoginAttemptData(string username, string password, bool retry, int attempts)
        {
            this._username = username;
            this._password = password;
            this._retry = retry;
            this._errorMessage = LoginWindowResources.DefaultFailMessage;
            this._attemptCount = attempts;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the number of attempts the user has had at logging in.
        /// </summary>
        public int AttemptCount
        {
            get { return this._attemptCount; }
        }

        /// <summary>
        /// Gets or sets the message to display to the user if the login fails.
        /// </summary>
        public string ErrorMessage
        {
            get { return this._errorMessage; }
            set { this._errorMessage = value; }
        }

        /// <summary>
        /// Gets the password that the user has entered.
        /// </summary>
        public string Password
        {
            get { return this._password; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the user can have another attempt at
        /// submitting their login details or the window just closes and returns false.
        /// </summary>
        public bool Retry
        {
            get { return this._retry; }
            set { this._retry = value; }
        }

        /// <summary>
        /// Gets the username that the user has entered.
        /// </summary>
        public string Username
        {
            get { return this._username; }
        }
        #endregion Properties
    } // RSG.Editor.Controls.LoginAttemptData {Class}
} // RSG.Editor.Controls {Namespace}
