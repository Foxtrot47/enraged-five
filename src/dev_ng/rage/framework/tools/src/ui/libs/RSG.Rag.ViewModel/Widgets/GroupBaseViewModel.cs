﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using RSG.Editor;
using RSG.Editor.View;
using RSG.Rag.Core;
using RSG.Rag.Widgets;

namespace RSG.Rag.ViewModel.Widgets
{
    /// <summary>
    /// Base class for group based widget view models.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class GroupBaseViewModel<T> : WidgetViewModel<T>, IWidgetGroupViewModel, IWeakEventListener where T : IWidgetGroup
    {
        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private readonly ObservableCollection<IWidgetViewModel> _children;

        /// <summary>
        /// 
        /// </summary>
        private readonly SortedObservableCollection<IWidgetGroupViewModel> _groupChildren;

        /// <summary>
        /// Reference to the widget view model factory.
        /// </summary>
        private readonly WidgetViewModelFactory _viewModelFactory;

        /// <summary>
        /// Private field for the <see cref="WidgetViewExpanded"/> property.
        /// </summary>
        private bool _widgetViewExpanded;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        /// <param name="vmFactory"></param>
        protected GroupBaseViewModel(T widget, WidgetViewModelFactory vmFactory)
            : base(widget)
        {
            _viewModelFactory = vmFactory;
            _children = new ObservableCollection<IWidgetViewModel>(widget.Children.Select(child => _viewModelFactory.CreateViewModel(child)));
            _groupChildren = new SortedObservableCollection<IWidgetGroupViewModel>();
            
            INotifyCollectionChanged notifyCollection = widget.Children as INotifyCollectionChanged;
            notifyCollection.CollectionChanged += notifyCollection_CollectionChanged;
            /*
            if (notifyCollection != null)
            {
                CollectionChangedEventManager.AddListener(notifyCollection, this);
            }
            */
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void notifyCollection_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        for (int i = 0; i < e.NewItems.Count; i++)
                        {
                            IWidget item = (IWidget)e.NewItems[i];
                            this.InsertSourceItem(e.NewStartingIndex + i, item);
                        }
                    }

                    break;
                case NotifyCollectionChangedAction.Remove:
                    {
                        for (int i = 0; i < e.OldItems.Count; i++)
                        {
                            this.RemoveSourceItem(e.OldStartingIndex);
                        }
                    }

                    break;
                case NotifyCollectionChangedAction.Replace:
                    {
                        for (int i = 0; i < e.NewItems.Count; i++)
                        {
                            IWidget item = (IWidget)e.NewItems[i];
                            this.ReplaceSourceItem(e.OldStartingIndex + i, item);
                        }
                    }

                    break;
                case NotifyCollectionChangedAction.Move:
                    {
                        for (int i = 0; i < e.NewItems.Count; i++)
                        {
                            int oldIndex = e.OldStartingIndex + i;
                            int newIndex = e.NewStartingIndex + i;
                            this.MoveSourceItem(oldIndex, newIndex);
                        }
                    }

                    break;
                case NotifyCollectionChangedAction.Reset:
                    {
                        this.ResetSourceItems(Widget.Children);
                    }

                    break;
            }
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Gets the bitmap source that is used to display the icon for this item when it has
        /// been expanded.
        /// </summary>
        public BitmapSource ExpandedIcon
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the style that the font on this item should be using.
        /// </summary>
        public FontStyle FontStyle
        {
            get { return FontStyles.Normal; }
        }

        /// <summary>
        /// Gets the weight or thickness that the font on this item should be using.
        /// </summary>
        public FontWeight FontWeight
        {
            get { return FontWeights.Normal; }
        }

        /// <summary>
        /// Gets the bitmap source that is used to display the icon for this item.
        /// </summary>
        public abstract BitmapSource Icon { get; }

        /// <summary>
        /// Gets the bitmap source that is used to display the overlay icon for this item.
        /// </summary>
        public BitmapSource OverlayIcon
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the bitmap source that is used to display the state icon for this item.
        /// </summary>
        public BitmapSource StateIcon
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the text that is used to represent the state of this item.
        /// </summary>
        public string StateToolTipText
        {
            get { return null; }
        }

        /// <summary>
        /// Gets or sets the text for this command.
        /// </summary>
        public string Text
        {
            get { return Name; }
        }

        /// <summary>
        /// Gets the text that is used in the tooltip for this item.
        /// </summary>
        public string ToolTip
        {
            get { return null; }
        }

        /// <summary>
        /// Property indicating whether this group has child groups.
        /// </summary>
        public bool HasChildGroups
        {
            get { return GroupChildren.Any(); }
        }

        /// <summary>
        /// 
        /// </summary>
        public IReadOnlyCollection<IWidgetViewModel> Children
        {
            get { return _children; }
        }

        /// <summary>
        /// 
        /// </summary>
        public IReadOnlyCollection<IWidgetGroupViewModel> GroupChildren
        {
            get { return _groupChildren; }
        }

        /// <summary>
        /// Flag indicating whether the group should be displayed as expanded in
        /// the widget view area of the UI.
        /// </summary>
        public bool WidgetViewExpanded
        {
            get { return _widgetViewExpanded; }
            set
            {
                if (SetProperty(ref _widgetViewExpanded, value))
                {
                    if (value)
                    {
                        OnShow();
                    }
                    else
                    {
                        OnHide();
                    }
                }
            }
        }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Adds a reference to the fact that this group is in active use.
        /// </summary>
        public void AddOpenReference()
        {
            Widget.AddOpenReference();
        }

        /// <summary>
        /// 
        /// </summary>
        public void RemoveOpenReference()
        {
            Widget.RemoveOpenReference();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="forceAddReference"></param>
        public void OnShow(bool forceAddReference = false)
        {
            if (WidgetViewExpanded || forceAddReference)
            {
                Widget.AddOpenReference();
            }

            // Iterate over all children.
            foreach (IWidgetGroupViewModel child in GroupChildren)
            {
                if (child.WidgetViewExpanded)
                {
                    child.OnShow();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void OnHide()
        {
            if (WidgetViewExpanded)
            {
                Widget.RemoveOpenReference();
            }

            // Iterate over all children.
            foreach (IWidgetGroupViewModel child in GroupChildren)
            {
                if (child.WidgetViewExpanded)
                {
                    child.OnHide();
                }
            }
        }
        #endregion // Methods

        #region IWeakEventListener Implementation
        /// <summary>
        /// Receives events from the centralized event manager.
        /// </summary>
        /// <param name="managerType">
        /// The type of the System.Windows.WeakEventManager calling this method.
        /// </param>
        /// <param name="sender">
        /// Object that originated the event.
        /// </param>
        /// <param name="e">
        /// Event data.
        /// </param>
        /// <returns>
        /// True if the listener handled the event; otherwise, false.
        /// </returns>
        public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
        {
            NotifyCollectionChangedEventArgs args = e as NotifyCollectionChangedEventArgs;
            if (args == null || managerType != typeof(CollectionChangedEventManager))
            {
                return false;
            }

            switch (args.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        for (int i = 0; i < args.NewItems.Count; i++)
                        {
                            IWidget item = (IWidget)args.NewItems[i];
                            this.InsertSourceItem(args.NewStartingIndex + i, item);
                        }
                    }

                    break;
                case NotifyCollectionChangedAction.Remove:
                    {
                        for (int i = 0; i < args.OldItems.Count; i++)
                        {
                            this.RemoveSourceItem(args.OldStartingIndex);
                        }
                    }

                    break;
                case NotifyCollectionChangedAction.Replace:
                    {
                        for (int i = 0; i < args.NewItems.Count; i++)
                        {
                            IWidget item = (IWidget)args.NewItems[i];
                            this.ReplaceSourceItem(args.OldStartingIndex + i, item);
                        }
                    }

                    break;
                case NotifyCollectionChangedAction.Move:
                    {
                        for (int i = 0; i < args.NewItems.Count; i++)
                        {
                            int oldIndex = args.OldStartingIndex + i;
                            int newIndex = args.NewStartingIndex + i;
                            this.MoveSourceItem(oldIndex, newIndex);
                        }
                    }

                    break;
                case NotifyCollectionChangedAction.Reset:
                    {
                        this.ResetSourceItems(Widget.Children);
                    }

                    break;
            }
            return true;
        }

        /// <summary>
        /// Inserts the converted item from the specified source item into this collection at
        /// the specified index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index where the new item should be inserted.
        /// </param>
        /// <param name="item">
        /// The item whose converted value will added to this collection.
        /// </param>
        protected void InsertSourceItem(int index, IWidget item)
        {
            Application app = Application.Current;
            if (!app.Dispatcher.CheckAccess())
            {
                app.Dispatcher.Invoke(() => InsertSourceItem(index, item));
                return;
            }

            IWidgetViewModel vm = _viewModelFactory.CreateViewModel(item);
            _children.Insert(index, vm);
            if (item is IWidgetGroup)
            {
                _groupChildren.Add((IWidgetGroupViewModel)vm);
                NotifyPropertyChanged("HasChildGroups");
            }
        }

        /// <summary>
        /// Moves the item at the specified old index to the new index in the collection.
        /// </summary>
        /// <param name="oldIndex">
        /// The zero-based index specifying the location of the item to move.
        /// </param>
        /// <param name="newIndex">
        /// The zero-based index specifying the new location of the item.
        /// </param>
        protected virtual void MoveSourceItem(int oldIndex, int newIndex)
        {
            Application app = Application.Current;
            if (!app.Dispatcher.CheckAccess())
            {
                app.Dispatcher.Invoke(() => MoveSourceItem(oldIndex, newIndex));
                return;
            }

            _children.Move(oldIndex, newIndex);
        }

        /// <summary>
        /// Removes the item at the specified zero-based index from this collection.
        /// </summary>
        /// <param name="index">
        /// The zero-based index of the item to remove.
        /// </param>
        protected virtual void RemoveSourceItem(int index)
        {
            Application app = Application.Current;
            if (!app.Dispatcher.CheckAccess())
            {
                app.Dispatcher.Invoke(() => RemoveSourceItem(index));
                return;
            }

            IWidgetViewModel vm = _children[index];
            _children.RemoveAt(index);

            if (vm is IWidgetGroupViewModel)
            {
                _groupChildren.Remove((IWidgetGroupViewModel)vm);
                NotifyPropertyChanged("HasChildGroups");
            }
        }

        /// <summary>
        /// Replaces the item at the specified zero-based index with the specified item.
        /// </summary>
        /// <param name="index">
        /// The zero-based index of the item to replace.
        /// </param>
        /// <param name="item">
        /// The item that will replace the item at the specified index.
        /// </param>
        protected virtual void ReplaceSourceItem(int index, IWidget item)
        {
            Application app = Application.Current;
            if (!app.Dispatcher.CheckAccess())
            {
                app.Dispatcher.Invoke(() => ReplaceSourceItem(index, item));
                return;
            }

            IWidgetViewModel existingVm = _children[index];
            IWidgetViewModel newVm = _viewModelFactory.CreateViewModel(item);
            _children[index] = newVm;

            bool groupChildrenChanged = false;
            if (existingVm is IWidgetGroupViewModel)
            {
                _groupChildren.Remove((IWidgetGroupViewModel)existingVm);
                groupChildrenChanged = true;
            }
            if (newVm is IWidgetGroupViewModel)
            {
                _groupChildren.Add((IWidgetGroupViewModel)newVm);
                groupChildrenChanged = true;
            }

            if (groupChildrenChanged)
            {
                NotifyPropertyChanged("HasChildGroups");
            }
        }

        /// <summary>
        /// Resets the items in this collection to the items specified.
        /// </summary>
        /// <param name="newItems">
        /// The items that will replace the items currently in this collection.
        /// </param>
        protected virtual void ResetSourceItems(IEnumerable newItems)
        {
            Application app = Application.Current;
            if (!app.Dispatcher.CheckAccess())
            {
                app.Dispatcher.Invoke(() => ResetSourceItems(newItems));
                return;
            }

            _children.Clear();
            _groupChildren.Clear();
            if (newItems != null)
            {
                int num = 0;
                IEnumerator enumerator = newItems.GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        IWidget item = (IWidget)enumerator.Current;
                        InsertSourceItem(num, item);
                        num++;
                    }
                }
                finally
                {
                    IDisposable disposable = enumerator as IDisposable;
                    if (disposable != null)
                    {
                        disposable.Dispose();
                    }
                }
            }
        }
        #endregion // IWeakEventListener Implementation
    } // GroupBaseViewModel<T>
}
