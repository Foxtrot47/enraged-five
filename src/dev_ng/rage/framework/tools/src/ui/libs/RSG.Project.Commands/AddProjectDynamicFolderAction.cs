﻿//---------------------------------------------------------------------------------------------
// <copyright file="AddProjectDynamicFolderAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2015. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using RSG.Editor;
    using RSG.Interop.Microsoft.Windows;
    using RSG.Project.Model;
    using RSG.Project.ViewModel;

    /// <summary>
    /// Contains the logic for the <see cref="ProjectCommands.AddProjectFolder"/> routed
    /// command. This class cannot be inherited.
    /// </summary>
    public sealed class AddProjectDynamicFolderAction : ButtonAction<ProjectCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="AddProjectDynamicFolderAction"/>
        /// class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public AddProjectDynamicFolderAction(ProjectCommandResolver resolver)
            : base(new ParameterResolverDelegate<ProjectCommandArgs>(resolver))
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ProjectCommandArgs args)
        {
            if (args == null)
            {
                return false;
            }

            if (args.SelectedNodes != null && args.SelectionCount == 1)
            {
                IHierarchyNode node = args.SelectedNodes.FirstOrDefault();
                if (node is ProjectNode)
                {
                    return ((ProjectNode)node).Loaded;
                }
            }

            return false;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(ProjectCommandArgs args)
        {
            ProjectNode project = args.SelectedNodes.FirstOrDefault() as ProjectNode;
            if (project == null || !project.Loaded)
            {
                return;
            }

            ICommonDialogService dlgService = this.GetService<ICommonDialogService>();
            if (dlgService == null)
            {
                Debug.Fail("Missing the ICommonDialogService service");
                return;
            }

            IMessageBoxService msgService = this.GetService<IMessageBoxService>();
            if (msgService == null)
            {
                Debug.Fail("Missing the IMessageBoxService service");
                return;
            }

            IProjectAddViewService pService = this.GetService<IProjectAddViewService>();
            if (pService == null)
            {
                Debug.Fail("Missing the IProjectAddViewService service");
                return;
            }

            string fullPath = null;
            string filters = null;
            bool recursive = false;
            if (!pService.ShowAddNewDynamicFolder(dlgService, out fullPath, out filters, out recursive))
            {
                throw new OperationCanceledException();
            }

            var currentDynamicFolders = new List<ProjectDynamicFolderNode>();
            foreach (IHierarchyNode child in project.GetChildren(true))
            {
                if (child is ProjectDynamicFolderNode)
                {
                    currentDynamicFolders.Add((ProjectDynamicFolderNode)child);
                }
            }

            foreach (var cdf in currentDynamicFolders)
            {
                string cdfFullPath = cdf.Model.GetMetadata("FullPath");
                bool cdfRecursive = cdf.Model.GetMetadataAsBool("Recursive", false);
                if (String.Equals(cdfFullPath, fullPath))
                {
                    msgService.Show(
                        String.Format(
                        "The folder '{0}' has already been included in this project.",
                        fullPath));

                    return;
                }

                if (recursive)
                {
                    if (cdfFullPath.StartsWith(fullPath))
                    {
                        msgService.Show(
                            String.Format(
                            "Cannot add this folder with recursion as the folder '{0}' has already been added",
                            cdfFullPath));

                        return;
                    }
                }

                if (cdfRecursive)
                {
                    if (fullPath.StartsWith(cdfFullPath))
                    {
                        msgService.Show(
                            String.Format(
                            "The folder '{0}' has already been included in this project.",
                            fullPath));

                        return;
                    }
                }
            }

            var newNode = project.AddNewDynamicFolder(fullPath, filters, recursive);
            newNode.ModifiedScopeNode.IsModified = true;
            if (args.NodeSelectionDelegate != null)
            {
                args.NodeSelectionDelegate(newNode);
            }

            this.ProcessFiles(newNode);
        }

        private void ProcessFiles(ProjectDynamicFolderNode folder)
        {
            ProjectNode project = folder.ParentProject;
            string directory = folder.Model.GetMetadata("FullPath");
            string filters = folder.Model.GetMetadata("Filters");
            bool recursive = folder.Model.GetMetadataAsBool("Recursive", false);
            if (String.IsNullOrWhiteSpace(filters))
            {
                filters = "*.*";
            }

            List<string> filenames = new List<string>();
            foreach (string filter in filters.Split(';'))
            {
                if (String.IsNullOrWhiteSpace(filter))
                {
                    continue;
                }

                filenames.AddRange(Directory.GetFiles(directory, filter));
            }

            IProjectItemScope scope = project.Model as IProjectItemScope;
            string scopeDir = scope.DirectoryPath;
            foreach (string filename in filenames)
            {
                string relative = Shlwapi.MakeRelativeFromDirectoryToFile(scopeDir, filename);
                ProjectItem current = scope.Find(relative);
                if (current != null)
                {
                    IHierarchyNode node = null;
                    foreach (IHierarchyNode child in project.GetChildren(true))
                    {
                        if (Object.ReferenceEquals(child.ProjectItem, current))
                        {
                            node = child;
                            break;
                        }
                    }

                    if (node != null)
                    {
                        node.Parent.ModifiedScopeNode.IsModified = true;
                        node.Parent.RemoveChild(node);
                        scope.RemoveProjectItem(current);
                    }
                }

                project.AddNewFile(folder, relative);
            }

            if (recursive)
            {
                string[] subDirectories = Directory.GetDirectories(directory);
                foreach (string subDirectory in subDirectories)
                {
                    var newNode = folder.AddNewDynamicFolder(subDirectory, filters, true);
                    this.ProcessFiles(newNode);
                }
            }
        }
        #endregion Methods
    } // RSG.Project.Commands.AddProjectDynamicFolderAction {Class}
} // RSG.Project.Commands {Namespace}
