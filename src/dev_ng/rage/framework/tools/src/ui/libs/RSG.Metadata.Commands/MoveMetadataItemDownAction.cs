﻿//---------------------------------------------------------------------------------------------
// <copyright file="MoveMetadataItemDownAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Commands
{
    using System.Linq;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Definitions;
    using RSG.Metadata.Model.Tunables;
    using RSG.Metadata.ViewModel.Tunables;

    /// <summary>
    /// Implements the logic for the <see cref="MetadataCommands.MoveMetadataItemUp"/> routed
    /// command.
    /// </summary>
    public class MoveMetadataItemDownAction : ButtonAction<MetadataCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MoveMetadataItemDownAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public MoveMetadataItemDownAction(MetadataCommandArgsResolver resolver)
            : base(new ParameterResolverDelegate<MetadataCommandArgs>(resolver))
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(MetadataCommandArgs args)
        {
            ITunableViewModel selected = args.SelectedTunables.FirstOrDefault();
            if (selected == null)
            {
                return false;
            }

            ITunable tunable = selected.Model as ITunable;
            if (tunable == null)
            {
                return false;
            }

            IDynamicTunableParent parent = tunable.Parent as IDynamicTunableParent;
            if (parent == null)
            {
                return false;
            }

            int index = parent.IndexOf(tunable);
            return index < parent.ItemCount - 1 && index >= 0;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(MetadataCommandArgs args)
        {
            ITunableViewModel selected = args.SelectedTunables.FirstOrDefault();
            if (selected == null)
            {
                return;
            }

            ITunable tunable = selected.Model as ITunable;
            if (tunable == null)
            {
                return;
            }
            
            IDynamicTunableParent parent = tunable.Parent as IDynamicTunableParent;
            if (parent == null)
            {
                return;
            }

            int index = parent.IndexOf(tunable);
            if (index == parent.ItemCount - 1)
            {
                return;
            }

            using (new UndoRedoBatch(args.UndoEngine))
            {
                parent.MoveItem(index, index + 1);
                if (args.TunableSelectionDelegate != null && selected.Parent != null)
                {
                    args.TunableSelectionDelegate(selected.Parent[index + 1]);
                }
            }
        }
        #endregion Methods
    } // RSG.Metadata.Commands.MoveMetadataItemDownAction {Class}
} // RSG.Metadata.Commands {Namespace}
