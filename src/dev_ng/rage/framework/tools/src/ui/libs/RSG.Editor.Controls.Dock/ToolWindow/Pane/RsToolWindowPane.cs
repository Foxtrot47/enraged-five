﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsToolWindowPane.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock.ToolWindow
{
    using System;
    using System.Collections.Specialized;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Input;
    using System.Windows.Threading;

    /// <summary>
    /// Represents a control that contains <see cref="RsToolWindowItem"/> controls that share
    /// the same screen space.
    /// </summary>
    public class RsToolWindowPane : TabControl
    {
        #region Fields
        /// <summary>
        /// The hide button that is used to close the currently selected tool window item if
        /// supported.
        /// </summary>
        private Button _hideButton;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsToolWindowPane"/> class.
        /// </summary>
        static RsToolWindowPane()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsToolWindowPane),
                new FrameworkPropertyMetadata(typeof(RsToolWindowPane)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsToolWindowPane"/> class.
        /// </summary>
        public RsToolWindowPane()
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Gets called whenever the internal processes set the control template for
        /// this control.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            this._hideButton = this.GetTemplateChild("PART_HideButton") as Button;
        }

        /// <summary>
        /// Creates or identifies the element that is used to display the given item.
        /// </summary>
        /// <returns>
        /// The element that is used to display the given item.
        /// </returns>
        protected override DependencyObject GetContainerForItemOverride()
        {
            return new RsToolWindowItem();
        }

        /// <summary>
        /// Determines if the specified item is (or is eligible to be) its own container.
        /// </summary>
        /// <param name="item">
        /// The item to check.
        /// </param>
        /// <returns>
        /// True if the item is (or is eligible to be) its own container; otherwise, false.
        /// </returns>
        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is RsToolWindowItem;
        }

        /// <summary>
        /// Determines whether the <see cref="DockingCommands.HideWindow"/> routed UI command
        /// can be executed.
        /// </summary>
        /// <param name="s">
        /// The object where the event handler is attached.
        /// </param>
        /// <param name="args">
        /// The System.Windows.Input.CanExecuteRoutedEventArgs data for the event.
        /// </param>
        private void CanHideWindow(object s, CanExecuteRoutedEventArgs args)
        {
            if (this.SelectedItem == null)
            {
                args.CanExecute = false;
                return;
            }

            ItemContainerGenerator generator = this.ItemContainerGenerator;
            DependencyObject container = generator.ContainerFromItem(this.SelectedItem);
            RsToolWindowItem item = container as RsToolWindowItem;
            if (item == null)
            {
                args.CanExecute = false;
                return;
            }

            args.CanExecute = item.CanHide;
        }

        /// <summary>
        /// Get called once the <see cref="DockingCommands.HideWindow"/> routed UI command gets
        /// executed.
        /// </summary>
        /// <param name="s">
        /// The object where the event handler is attached.
        /// </param>
        /// <param name="args">
        /// The System.Windows.Input.ExecutedRoutedEventArgs data for the event.
        /// </param>
        private void OnHideWindow(object s, ExecutedRoutedEventArgs args)
        {

        }
        #endregion Methods
    } // RSG.Editor.Controls.Dock.ToolWindow.RsToolWindowPane {Class}
} // RSG.Editor.Controls.Dock.ToolWindow {Namespace}
