﻿//---------------------------------------------------------------------------------------------
// <copyright file="BackgroundDispatcher.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Threading;
    using System.Windows;
    using System.Windows.Threading;

    /// <summary>
    /// Provides a class that can be used to retrieve dispatcher objects based on names and
    /// stack size.
    /// </summary>
    internal class BackgroundDispatcher
    {
        #region Fields
        /// <summary>
        /// The private internal list of background dispatchers.
        /// </summary>
        private static List<BackgroundDispatcher> _dispatchers;

        /// <summary>
        /// The private name of this background dispatcher.
        /// </summary>
        private readonly string _name;

        /// <summary>
        /// The main dispatcher that is used for async operations.
        /// </summary>
        private Dispatcher _dispatcher;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="BackgroundDispatcher"/> class.
        /// </summary>
        static BackgroundDispatcher()
        {
            _dispatchers = new List<BackgroundDispatcher>();
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="BackgroundDispatcher"/> class.
        /// </summary>
        /// <param name="name">
        /// The name for this background dispatcher.
        /// </param>
        /// <param name="stackSize">
        /// The maximum stack size that the background thread can use.
        /// </param>
        private BackgroundDispatcher(string name, int stackSize)
        {
            this._name = name;
            this.CreateDispatcher(stackSize);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Retrieves a dispatcher object that has the specified name and stack size.
        /// </summary>
        /// <param name="dispatcherName">
        /// The name of the dispatcher to retrieve.
        /// </param>
        /// <param name="stackSize">
        /// The maximum stack size that the background thread can use.
        /// </param>
        /// <returns>
        /// A dispatcher object that has the specified name and stack size.
        /// </returns>
        public static Dispatcher GetBackgroundDispatcher(string dispatcherName, int stackSize)
        {
            Dispatcher result;
            lock (_dispatchers)
            {
                foreach (BackgroundDispatcher current in _dispatchers)
                {
                    if (String.Equals(current._name, dispatcherName))
                    {
                        result = current._dispatcher;
                        return result;
                    }
                }

                BackgroundDispatcher backgroundDispatcher =
                    new BackgroundDispatcher(dispatcherName, stackSize);
                _dispatchers.Add(backgroundDispatcher);
                result = backgroundDispatcher._dispatcher;
            }

            return result;
        }

        /// <summary>
        /// Creates a new dispatcher object that this instance uses for threading.
        /// </summary>
        /// <param name="stackSize">
        /// The maximum stack size that the background thread can use.
        /// </param>
        private void CreateDispatcher(int stackSize)
        {
            Thread thread = new Thread(
                new ParameterizedThreadStart(this.ThreadProc), stackSize)
            {
                IsBackground = true,
                CurrentCulture = CultureInfo.CurrentCulture,
                CurrentUICulture = CultureInfo.CurrentUICulture,
                Name = this._name
            };

            thread.SetApartmentState(ApartmentState.STA);
            ManualResetEvent manualResetEvent = new ManualResetEvent(false);
            thread.Start(manualResetEvent);
            manualResetEvent.WaitOne();
            this.HookEvents();
        }

        /// <summary>
        /// Hooks up some basic events for managing the dispatcher for this instance.
        /// </summary>
        private void HookEvents()
        {
            Application application = Application.Current;
            if (application != null)
            {
                application.Exit += this.OnApplicationExit;
            }

            Dispatcher dispatcher = Dispatcher.CurrentDispatcher;
            if (dispatcher != null)
            {
                dispatcher.ShutdownStarted += this.OnDispatcherShutdownStarted;
            }
        }

        /// <summary>
        /// Gets call if the application exits before the background thread has finished.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.ExitEventArgs data used for this event.
        /// </param>
        private void OnApplicationExit(object sender, ExitEventArgs e)
        {
            if (this._dispatcher != null)
            {
                this._dispatcher.InvokeShutdown();
            }

            this.UnhookEvents();
        }

        /// <summary>
        /// Gets called once the background thread begins to shutdown.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.EventArgs data used for this event.
        /// </param>
        private void OnDispatcherShutdownStarted(object sender, EventArgs e)
        {
            if (this._dispatcher != null)
            {
                this._dispatcher.InvokeShutdown();
            }

            this.UnhookEvents();
        }

        /// <summary>
        /// The call back function called when the background thread first starts.
        /// </summary>
        /// <param name="arg">
        /// The arguments set from the background thread.
        /// </param>
        private void ThreadProc(object arg)
        {
            this._dispatcher = Dispatcher.CurrentDispatcher;
            ManualResetEvent manualResetEvent = arg as ManualResetEvent;
            manualResetEvent.Set();
            Dispatcher.Run();
            this._dispatcher = null;
        }

        /// <summary>
        /// Unhooks the basic events for managing the dispatcher for this instance.
        /// </summary>
        private void UnhookEvents()
        {
            Application application = Application.Current;
            if (application != null)
            {
                application.Exit -= this.OnApplicationExit;
            }

            Dispatcher dispatcher = Dispatcher.CurrentDispatcher;
            if (dispatcher != null)
            {
                dispatcher.ShutdownStarted -= this.OnDispatcherShutdownStarted;
            }
        }
        #endregion
    } // RSG.Editor.Controls.Helpers.BackgroundDispatcher {Class}
} // RSG.Editor.Controls.Helpers {Namespace}
