﻿//---------------------------------------------------------------------------------------------
// <copyright file="ItemAddSelectorWindow.xaml.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.View
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.IO;
    using System.Text.RegularExpressions;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Data;
    using System.Windows.Input;
    using RSG.Editor;
    using RSG.Editor.Controls;
    using RSG.Project.Model;
    using RSG.Project.View.Resources;
    using RSG.Project.ViewModel;
    using RSG.Project.ViewModel.Definitions;

    /// <summary>
    /// Interaction logic for the ItemAddSelectorWindow.
    /// </summary>
    public partial class ItemAddSelectorWindow : RsWindow
    {
        #region Fields
        /// <summary>
        /// The private reference to the new items scope so that a unique name can be
        /// determined.
        /// </summary>
        private IProjectItemScope _itemScope;

        /// <summary>
        /// The regular expression that has been set up from the search control.
        /// </summary>
        private Regex _regex;

        /// <summary>
        /// A private value indicating whether the default name is still being displayed for
        /// the selected definition.
        /// </summary>
        private bool _usingDefaultName;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ItemAddSelectorWindow"/> class.
        /// </summary>
        public ItemAddSelectorWindow()
        {
            this._usingDefaultName = true;
            this.InitializeComponent();
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Sets the definition for this window and gives access to the definitions that will
        /// be available for selection.
        /// </summary>
        /// <param name="project">
        /// The project whose definition contains the available child definitions for
        /// selection.
        /// </param>
        public void SetDefinition(ProjectNode project)
        {
            this.Title = SelectorWindowResources.AddItemTitle;
            if (this.TemplateListBox == null || project == null || project.Definition == null)
            {
                return;
            }

            var test = project.DefinitionCategories;

            this._itemScope = project.Model as IProjectItemScope;
            Binding binding = new Binding("ItemDefinitions");
            binding.Source = test[0];
            binding.UpdateSourceTrigger = UpdateSourceTrigger.Explicit;

            this.TemplateListBox.SetBinding(ListBox.ItemsSourceProperty, binding);
            this.TemplateListBox.ItemContainerGenerator.StatusChanged += this.OnStatusChanged;

            IEnumerable source = this.TemplateListBox.Items;
            if (source == null)
            {
                return;
            }

            ICollectionView view = CollectionViewSource.GetDefaultView(source);
            if (view == null)
            {
                return;
            }

            view.Filter = this.FilterTemplateList;
            view.SortDescriptions.Clear();
            foreach (SortDescription sortDescription in this.GetCurrentSortDescriptions())
            {
                view.SortDescriptions.Add(sortDescription);
            }
        }

        /// <summary>
        /// Determines whether the
        /// <see cref="RSG.Editor.SharedCommands.RockstarViewCommands.ConfirmDialog"/> command
        /// can be executed at this level.
        /// </summary>
        /// <param name="e">
        /// The command data that is past from the command system.
        /// </param>
        /// <returns>
        /// True if the command can be executed here; otherwise, false.
        /// </returns>
        protected override bool CanConfirmDialog(CanExecuteCommandData e)
        {
            if (String.IsNullOrWhiteSpace(this.NameValueTextBox.Text))
            {
                return false;
            }

            if (String.IsNullOrWhiteSpace(this.LocationValueTextBox.Text))
            {
                return false;
            }

            string pattern = "\\A[^{0}]*\\z";
            pattern = String.Format(pattern, new String(Path.GetInvalidFileNameChars()));
            var regex = new System.Text.RegularExpressions.Regex(pattern);
            if (!regex.IsMatch(this.NameValueTextBox.Text))
            {
                return false;
            }

            pattern = "\\A[a-zA-Z]:\\\\[^{0}]*\\z";
            pattern = String.Format(pattern, new String(Path.GetInvalidPathChars()));
            regex = new System.Text.RegularExpressions.Regex(pattern);
            if (!regex.IsMatch(this.LocationValueTextBox.Text))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Handles the
        /// <see cref="RSG.Editor.SharedCommands.RockstarViewCommands.ConfirmDialog"/> command
        /// after it gets caught here.
        /// </summary>
        /// <param name="e">
        /// The command data that is past from the command system.
        /// </param>
        protected override void ConfirmDialog(ExecuteCommandData e)
        {
            this.DialogResult = true;
        }

        /// <summary>
        /// The method the collection view for the list box uses to determine whether a item is
        /// to be shown or not.
        /// </summary>
        /// <param name="item">
        /// The item that is tested for visibility.
        /// </param>
        /// <returns>
        /// True if the specified item should be displayed; otherwise, false.
        /// </returns>
        private bool FilterTemplateList(object item)
        {
            if (this.TemplateSearchControl == null)
            {
                return true;
            }

            if (String.IsNullOrEmpty(this.TemplateSearchControl.SearchText))
            {
                return true;
            }

            ProjectItemDefinition definition = item as ProjectItemDefinition;
            if (definition != null && this._regex != null)
            {
                return this._regex.IsMatch(definition.Name);
            }

            return true;
        }

        /// <summary>
        /// Gets the sort descriptions that the list box should be using based on the currently
        /// selected sort algorithm.
        /// </summary>
        /// <returns>
        /// The sort descriptions that the list box should be using.
        /// </returns>
        private List<SortDescription> GetCurrentSortDescriptions()
        {
            List<SortDescription> sorts = new List<SortDescription>();
            ListSortDirection typeDirection = ListSortDirection.Descending;
            if (this.SortSelector != null)
            {
                switch (this.SortSelector.SelectedIndex)
                {
                    case 0:
                        sorts.Add(new SortDescription("Name", ListSortDirection.Ascending));
                        typeDirection = ListSortDirection.Ascending;
                        break;
                    case 1:
                        sorts.Add(new SortDescription("Name", ListSortDirection.Descending));
                        typeDirection = ListSortDirection.Descending;
                        break;
                }
            }

            sorts.Add(new SortDescription("Type", typeDirection));
            return sorts;
        }

        /// <summary>
        /// Called when the browse button is pressed. Shows the select folder windows dialog
        /// for the user to select.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private void OnBrowseButtonClicked(object sender, RoutedEventArgs e)
        {
            string directory = null;
            Guid id = new Guid("834A6F2A-71BA-4C87-B99D-0761BC38363D");
            RsSelectFolderDialog dlg = new RsSelectFolderDialog(id);
            dlg.Title = SelectorWindowResources.BrowseItemLocationTitle;

            bool? result = dlg.ShowDialog();
            if (result == true)
            {
                directory = dlg.FileName;
            }
            else
            {
                return;
            }

            this.LocationValueTextBox.Text = Path.GetFullPath(directory);
        }

        /// <summary>
        /// Called whenever the selected definition changes so that the name can be updated to
        /// match the new selection.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Controls.SelectionChangedEventArgs containing the event data.
        /// </param>
        private void OnDefinitionSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!this._usingDefaultName || this._itemScope == null)
            {
                return;
            }

            IDefinition definition = this.TemplateListBox.SelectedItem as IDefinition;
            if (definition == null)
            {
                if (this.TemplateListBox.SelectedIndex != -1)
                {
                    return;
                }

                ItemContainerGenerator generator = this.TemplateListBox.ItemContainerGenerator;
                if (generator == null)
                {
                    return;
                }

                Control container = generator.ContainerFromIndex(0) as Control;
                if (container == null)
                {
                    generator.StatusChanged += this.OnStatusChanged;
                    return;
                }

                this.TemplateListBox.SelectedItem = container.DataContext;
                return;
            }

            string baseFilename = definition.DefaultName + definition.Extension;
            string name = this._itemScope.GetUniqueFilename(baseFilename);
            this.NameValueTextBox.TextChanged -= this.OnNameChanged;
            this.NameValueTextBox.Text = name;
            this.NameValueTextBox.TextChanged += this.OnNameChanged;
        }

        /// <summary>
        /// Called whenever the text inside the name text box changes outside of the text
        /// changing due to default name updating.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Controls.TextChangedEventArgs containing the event data.
        /// </param>
        private void OnNameChanged(object sender, TextChangedEventArgs e)
        {
            this._usingDefaultName = false;
        }

        /// <summary>
        /// Called whenever the sort option changes so that the sort description can be update
        /// on the collection view.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Controls.SelectionChangedEventArgs containing the event data.
        /// </param>
        private void OnSortSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.TemplateListBox == null)
            {
                return;
            }

            IEnumerable source = this.TemplateListBox.Items;
            if (source == null)
            {
                return;
            }

            ICollectionView view = CollectionViewSource.GetDefaultView(source);
            if (view == null)
            {
                return;
            }

            view.SortDescriptions.Clear();
            foreach (SortDescription sortDescription in this.GetCurrentSortDescriptions())
            {
                view.SortDescriptions.Add(sortDescription);
            }

            view.Refresh();
        }

        /// <summary>
        /// Called whenever the container generator for the list box updates its status. This
        /// is used to make sure a item is selected whenever the item source changes.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.EventArgs containing the event data.
        /// </param>
        private void OnStatusChanged(object sender, EventArgs e)
        {
            ItemContainerGenerator generator = sender as ItemContainerGenerator;
            if (generator == null)
            {
                return;
            }

            if (generator.Status == GeneratorStatus.ContainersGenerated)
            {
                Control container = generator.ContainerFromIndex(0) as Control;
                if (container == null)
                {
                    return;
                }

                this.TemplateListBox.SelectedItem = container.DataContext;
                generator.StatusChanged -= this.OnStatusChanged;
            }
        }

        /// <summary>
        /// Refreshes the filtering of the current definition list based on a routed event
        /// being fired. This handler to attached to the search events among other ones.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Controls.SelectionChangedEventArgs containing the event data.
        /// </param>
        private void RefreshSearch(object sender, RoutedEventArgs e)
        {
            this.TemplateSearchControl.HasError = false;
            IEnumerable source = this.TemplateListBox.Items;
            if (source == null)
            {
                return;
            }

            ICollectionView view = CollectionViewSource.GetDefaultView(source);
            if (view == null)
            {
                return;
            }

            string searchText = this.TemplateSearchControl.SearchText;
            if (String.IsNullOrEmpty(this.TemplateSearchControl.SearchText))
            {
                this._regex = null;
            }
            else
            {
                string pattern = searchText;
                RegexOptions options = RegexOptions.None;
                if (!this.TemplateSearchControl.RegularExpressionsChecked)
                {
                    pattern = Regex.Escape(pattern);
                }

                if (!this.TemplateSearchControl.MatchCaseChecked)
                {
                    options |= RegexOptions.IgnoreCase;
                }

                if (this.TemplateSearchControl.MatchWordChecked)
                {
                    pattern = String.Format(@"^{0}$", pattern);
                }

                try
                {
                    this._regex = new Regex(pattern, options);
                }
                catch (ArgumentException)
                {
                    this.TemplateSearchControl.HasError = true;
                }
            }

            RsHighlightTextBlock.SetHighlightExpression(this, this._regex);
            view.Filter = this.FilterTemplateList;
            this.TemplateListBox.ScrollIntoView(this.TemplateListBox.SelectedItem);
        }
        #endregion Methods
    } // RSG.Project.View.ItemAddSelectorWindow {Class}
} // RSG.Project.View {Namespace}
