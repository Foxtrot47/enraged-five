﻿//---------------------------------------------------------------------------------------------
// <copyright file="ChildMenu.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;

    /// <summary>
    /// Represents a menu that is positioned inside another menu. This class cannot be
    /// inherited.
    /// </summary>
    public sealed class ChildMenu : CommandMenu
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ChildMenu"/> class.
        /// </summary>
        /// <param name="priority">
        /// The initial priority that this command bar item should be given.
        /// </param>
        /// <param name="startsGroup">
        /// A value indicating whether this command bar item should start a new group. (I.e.
        /// Placed directly after a separator).
        /// </param>
        /// <param name="text">
        /// The text for this command bar item.
        /// </param>
        /// <param name="id">
        /// The unique identifier that is used to reference this item.
        /// </param>
        /// <param name="parentId">
        /// The unique identifier for the parent of this item inside the command bar hierarchy.
        /// </param>
        public ChildMenu(
            ushort priority, bool startsGroup, string text, Guid id, Guid parentId)
            : base(priority, startsGroup, text, id, parentId)
        {
        }
        #endregion Constructors
    } // RSG.Editor.ChildMenu {Class}
} // RSG.Editor {Namespace}
