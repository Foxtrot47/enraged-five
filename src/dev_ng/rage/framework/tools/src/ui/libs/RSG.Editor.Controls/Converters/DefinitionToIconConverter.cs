﻿//---------------------------------------------------------------------------------------------
// <copyright file="DefinitionToIconConverter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Converters
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Converters a command definition object into a bitmap source icon object.
    /// </summary>
    internal class DefinitionToIconConverter :
        ValueConverter<CommandDefinition, BitmapSource>
    {
        #region Methods
        /// <summary>
        /// Converters the given value and returns the result.
        /// </summary>
        /// <param name="definition">
        /// The definition to get the icon from.
        /// </param>
        /// <param name="param">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A converted value.
        /// </returns>
        protected override BitmapSource Convert(
            CommandDefinition definition, object param, CultureInfo culture)
        {
            RockstarRoutedCommand command = definition.Command;
            if (command == null)
            {
                return null;
            }

            return command.Icon;
        }
        #endregion Methods
    } // RSG.Editor.Controls.Converters.DefinitionToIconConverter {Class}
} // RSG.Editor.Controls.Converters {Namespace}
