﻿//---------------------------------------------------------------------------------------------
// <copyright file="DialogueCharacterViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.ViewModel
{
    using System;
    using RSG.Editor.View;
    using RSG.Text.Model;

    /// <summary>
    /// The view model the represents a <see cref="RSG.Text.Model.DialogueCharacter"/> object
    /// instance.
    /// </summary>
    public class DialogueCharacterViewModel : ViewModelBase<DialogueCharacter>
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="UsedCount"/> property.
        /// </summary>
        private int _usedCount;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DialogueCharacterViewModel"/> class.
        /// </summary>
        /// <param name="character">
        /// The <see cref="RSG.Text.Model.DialogueCharacter"/> instance this view model is
        /// wrapping.
        /// </param>
        public DialogueCharacterViewModel(DialogueCharacter character)
            : base(character, true)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the id used by this character. This cannot be changed once setup and is how
        /// the character can be renamed without any data being lost or manipulated.
        /// </summary>
        public Guid Id
        {
            get { return this.Model.Id; }
        }

        /// <summary>
        /// Gets a value indicating whether this character is being used be the conversation it
        /// is defined for.
        /// </summary>
        public bool IsUsed
        {
            get { return this.UsedCount > 0; }
        }

        /// <summary>
        /// Gets or sets the name for this character.
        /// </summary>
        public string Name
        {
            get { return this.Model.Name; }
            set { this.Model.Name = value; }
        }

        public Guid Voice
        {
            get { return this.Model.Voice; }
            set { this.Model.Voice = value; }
        }

        /// <summary>
        /// Gets or sets a count that defines how many times this character is being used in
        /// the conversation.
        /// </summary>
        public int UsedCount
        {
            get { return this._usedCount; }
            set { this.SetProperty(ref this._usedCount, value, "UsedCount", "IsUsed"); }
        }

        /// <summary>
        /// Gets or sets the DontExport flag for this character
        /// </summary>
        public bool Export
        {
            get { return !this.Model.DontExport; }
            set { this.Model.DontExport = !value; }
        }

        /// <summary>
        /// Gets or sets the UsesManualFilenames flag for this character
        /// </summary>
        public bool UsesManualFilenames
        {
            get { return this.Model.UsesManualFilenames; }
            set { this.Model.UsesManualFilenames = value; }
        }

        #endregion Properties
    } // RSG.Text.ViewModel.LineViewModel {Class}
} // RSG.Text.ViewModel {Namespace}
