﻿//---------------------------------------------------------------------------------------------
// <copyright file="TextCommands.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Commands
{
    using System;
    using System.Windows.Input;
    using System.Windows.Media.Imaging;
    using RSG.Editor;
    using RSG.Editor.SharedCommands;
    using RSG.Text.ViewModel;

    /// <summary>
    /// Defines core commands for the text controls. This class cannot be inherited.
    /// </summary>
    public sealed class TextCommands
    {
        #region Fields
        /// <summary>
        /// The private cache of command objects.
        /// </summary>
        private static RockstarRoutedCommand[] _internalCommands = InitialiseInternalStorage();

        /// <summary>
        /// The private string table resource manager used for getting the command related
        /// strings and objects.
        /// </summary>
        private static CommandResourceManager _resourceManager = InitialiseResourceManager();
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Prevents a default instance of the <see cref="TextCommands"/> class from being
        /// created.
        /// </summary>
        private TextCommands()
        {
        }
        #endregion Constructors

        #region Enumerations
        /// <summary>
        /// Defines all of the command identifiers for the different core Rockstar commands.
        /// </summary>
        private enum Id : byte
        {
            /// <summary>
            /// Used to identifier the <see cref="TextCommands.AddNewConversation"/> routed
            /// command.
            /// </summary>
            AddNewConversation,

            /// <summary>
            /// Used to identifier the <see cref="TextCommands.AddNewLine"/> routed command.
            /// </summary>
            AddNewLine,

            /// <summary>
            /// Used to identifier the <see cref="TextCommands.AddNewTextItem"/> routed
            /// command.
            /// </summary>
            AddNewTextItem,

            /// <summary>
            /// Used to identifier the <see cref="TextCommands.CopyFilename"/> routed command.
            /// </summary>
            CopyFilename,

            /// <summary>
            /// Used to identifier the <see cref="TextCommands.EditDialogueConfig"/> routed
            /// command.
            /// </summary>
            EditDialogueConfig,

            /// <summary>
            /// Used to identifier the <see cref="TextCommands.ExportConversationLinesToWord"/>
            /// routed command.
            /// </summary>
            ExportConversationLinesToWord,

            /// <summary>
            /// Used to identifier the <see cref="TextCommands.ExportDocumentLinesToWord"/>
            /// routed command.
            /// </summary>
            ExportDocumentLinesToWord,

            /// <summary>
            /// Used to identifier the <see cref="TextCommands.ExportAllOpenLinesToWord"/>
            /// routed command.
            /// </summary>
            ExportAllOpenLinesToWord,

            /// <summary>
            /// Used to identifier the
            /// <see cref="TextCommands.ExportDocumentConversationsToExcel"/> routed command.
            /// </summary>
            ExportDocumentConversationsToExcel,

            /// <summary>
            /// Used to identifier the <see cref="TextCommands.ExportDocumentLinesToExcel"/>
            /// routed command.
            /// </summary>
            ExportDocumentLinesToExcel,

            /// <summary>
            /// Used to identifier the
            /// <see cref="TextCommands.ExportAllOpenConversationsToExcel"/> routed command.
            /// </summary>
            ExportAllOpenConversationsToExcel,

            /// <summary>
            /// Used to identifier the <see cref="TextCommands.ExportAllOpenLinesToExcel"/>
            /// routed command.
            /// </summary>
            ExportAllOpenLinesToExcel,

            /// <summary>
            /// Used to identifier the
            /// <see cref="TextCommands.ExportProjectConversationsToExcel"/> routed command.
            /// </summary>
            ExportProjectConversationsToExcel,

            /// <summary>
            /// Used to identifier the <see cref="TextCommands.ExportProjectLinesToExcel"/>
            /// routed command.
            /// </summary>
            ExportProjectLinesToExcel,

            /// <summary>
            /// Used to identifier the <see cref="TextCommands.InsertNewLine"/> routed command.
            /// </summary>
            InsertNewLine,

            /// <summary>
            /// Used to identifier the <see cref="TextCommands.MoveConversationDown"/> routed
            /// command.
            /// </summary>
            MoveConversationDown,

            /// <summary>
            /// Used to identifier the <see cref="TextCommands.MoveConversationUp"/> routed
            /// command.
            /// </summary>
            MoveConversationUp,

            /// <summary>
            /// Used to identifier the <see cref="TextCommands.MoveLineDown"/> routed command.
            /// </summary>
            MoveLineDown,

            /// <summary>
            /// Used to identifier the <see cref="TextCommands.MoveLineUp"/> routed command.
            /// </summary>
            MoveLineUp,

            /// <summary>
            /// Used to identify the <see cref="TextCommands.PlayLine"/> routed command.
            /// </summary>
            PlayLine,

            /// <summary>
            /// Used to identify the <see cref="TextCommands.PlayConversation"/> routed
            /// command.
            /// </summary>
            PlayConversation,

            /// <summary>
            /// Used to identify the <see cref="TextCommands.StopPlaying"/> routed command.
            /// </summary>
            StopPlaying,

            /// <summary>
            /// Used to identifier the <see cref="TextCommands.InsertNewConversation"/> routed
            /// command.
            /// </summary>
            InsertNewConversation,

            /// <summary>
            /// Used to identifier the
            /// <see cref="TextCommands.GenerateFilenamesForConversations"/> routed command.
            /// </summary>
            GenerateFilenamesForConversations,

            /// <summary>
            /// Used to identifier the
            /// <see cref="TextCommands.GenerateFilenamesForConversation"/> routed command.
            /// </summary>
            GenerateFilenamesForConversation,

            /// <summary>
            /// Used to identify the <see cref="TextCommands.ValidateSfxLines"/> routed
            /// command.
            /// </summary>
            ValidateSfxLines,

            /// <summary>
            /// Used to identify the <see cref="TextCommands.ValidateAgainstFolder"/> routed
            /// command.
            /// </summary>
            ValidateAgainstFolder,

            /// <summary>
            /// Used to identify the <see cref="TextCommands.FilterConversations"/> routed
            /// command.
            /// </summary>
            FilterConversations,

            /// <summary>
            /// Used to identify the <see cref="TextCommands.FilterLines"/> routed command.
            /// </summary>
            FilterLines,
        } // TextCommands.Id {Enum}
        #endregion Enumerations

        #region Properties
        /// <summary>
        /// Gets the value that represents the Add New Conversation command.
        /// </summary>
        public static RockstarRoutedCommand AddNewConversation
        {
            get { return EnsureCommandExists(Id.AddNewConversation, Icons.NewConversation); }
        }

        /// <summary>
        /// Gets the value that represents the Add New Line command.
        /// </summary>
        public static RockstarRoutedCommand AddNewLine
        {
            get { return EnsureCommandExists(Id.AddNewLine, Icons.NewLine); }
        }

        /// <summary>
        /// Gets the value that represents the Add New Text Item command.
        /// </summary>
        public static RockstarRoutedCommand AddNewTextItem
        {
            get { return EnsureCommandExists(Id.AddNewTextItem, Icons.NewText); }
        }

        /// <summary>
        /// Gets the value that represents the Add New Text Item command.
        /// </summary>
        public static RockstarRoutedCommand CopyFilename
        {
            get { return EnsureCommandExists(Id.CopyFilename, CommonIcons.Copy); }
        }

        /// <summary>
        /// Gets the value that represents the Edit Dialogue Config command.
        /// </summary>
        public static RockstarRoutedCommand EditDialogueConfig
        {
            get { return EnsureCommandExists(Id.EditDialogueConfig); }
        }

        /// <summary>
        /// Gets the value that represents the filter lines command.
        /// </summary>
        public static RockstarRoutedCommand ExportAllOpenConversationsToExcel
        {
            get
            {
                return EnsureCommandExists(
                    Id.ExportAllOpenConversationsToExcel, Icons.ExcelExport);
            }
        }

        /// <summary>
        /// Gets the value that represents the filter lines command.
        /// </summary>
        public static RockstarRoutedCommand ExportAllOpenLinesToExcel
        {
            get
            {
                return EnsureCommandExists(Id.ExportAllOpenLinesToExcel, Icons.ExcelExport);
            }
        }

        /// <summary>
        /// Gets the value that represents the Export All Open Lines To Word command.
        /// </summary>
        public static RockstarRoutedCommand ExportAllOpenLinesToWord
        {
            get { return EnsureCommandExists(Id.ExportAllOpenLinesToWord, Icons.WordExport); }
        }

        /// <summary>
        /// Gets the value that represents the Export Conversation Lines To Word command.
        /// </summary>
        public static RockstarRoutedCommand ExportConversationLinesToWord
        {
            get
            {
                return EnsureCommandExists(Id.ExportConversationLinesToWord, Icons.WordExport);
            }
        }

        /// <summary>
        /// Gets the value that represents the filter lines command.
        /// </summary>
        public static RockstarRoutedCommand ExportDocumentConversationsToExcel
        {
            get
            {
                return EnsureCommandExists(
                    Id.ExportDocumentConversationsToExcel, Icons.ExcelExport);
            }
        }

        /// <summary>
        /// Gets the value that represents the filter lines command.
        /// </summary>
        public static RockstarRoutedCommand ExportDocumentLinesToExcel
        {
            get
            {
                return EnsureCommandExists(Id.ExportDocumentLinesToExcel, Icons.ExcelExport);
            }
        }

        /// <summary>
        /// Gets the value that represents the Export Document Lines To Word command.
        /// </summary>
        public static RockstarRoutedCommand ExportDocumentLinesToWord
        {
            get { return EnsureCommandExists(Id.ExportDocumentLinesToWord, Icons.WordExport); }
        }

        /// <summary>
        /// Gets the value that represents the filter lines command.
        /// </summary>
        public static RockstarRoutedCommand ExportProjectConversationsToExcel
        {
            get
            {
                return EnsureCommandExists(
                    Id.ExportProjectConversationsToExcel, Icons.ExcelExport);
            }
        }

        /// <summary>
        /// Gets the value that represents the filter lines command.
        /// </summary>
        public static RockstarRoutedCommand ExportProjectLinesToExcel
        {
            get
            {
                return EnsureCommandExists(Id.ExportProjectLinesToExcel, Icons.ExcelExport);
            }
        }

        /// <summary>
        /// Gets the value that represents the filter conversations command.
        /// </summary>
        public static RockstarRoutedCommand FilterConversations
        {
            get { return EnsureCommandExists(Id.FilterConversations, null); }
        }

        /// <summary>
        /// Gets the value that represents the filter lines command.
        /// </summary>
        public static RockstarRoutedCommand FilterLines
        {
            get { return EnsureCommandExists(Id.FilterLines, null); }
        }

        /// <summary>
        /// Gets the value that represents the Generate Filenames For Conversation command.
        /// </summary>
        public static RockstarRoutedCommand GenerateFilenamesForConversation
        {
            get
            {
                return EnsureCommandExists(
                    Id.GenerateFilenamesForConversation, Icons.GenerateFilename);
            }
        }

        /// <summary>
        /// Gets the value that represents the Generate Filenames For Conversations command.
        /// </summary>
        public static RockstarRoutedCommand GenerateFilenamesForConversations
        {
            get
            {
                return EnsureCommandExists(
                    Id.GenerateFilenamesForConversations, Icons.GenerateAllFilenames);
            }
        }

        /// <summary>
        /// Gets the value that represents the Insert New Conversation command.
        /// </summary>
        public static RockstarRoutedCommand InsertNewConversation
        {
            get
            {
                return EnsureCommandExists(Id.InsertNewConversation, Icons.InsertConversation);
            }
        }

        /// <summary>
        /// Gets the value that represents the Insert New Line command.
        /// </summary>
        public static RockstarRoutedCommand InsertNewLine
        {
            get { return EnsureCommandExists(Id.InsertNewLine, Icons.InsertLine); }
        }

        /// <summary>
        /// Gets the value that represents the Move Conversation Down command.
        /// </summary>
        public static RockstarRoutedCommand MoveConversationDown
        {
            get { return EnsureCommandExists(Id.MoveConversationDown, null); }
        }

        /// <summary>
        /// Gets the value that represents the Move Conversation Up command.
        /// </summary>
        public static RockstarRoutedCommand MoveConversationUp
        {
            get { return EnsureCommandExists(Id.MoveConversationUp, null); }
        }

        /// <summary>
        /// Gets the value that represents the Move Line Down command.
        /// </summary>
        public static RockstarRoutedCommand MoveLineDown
        {
            get { return EnsureCommandExists(Id.MoveLineDown, null); }
        }

        /// <summary>
        /// Gets the value that represents the Move Line Up command.
        /// </summary>
        public static RockstarRoutedCommand MoveLineUp
        {
            get { return EnsureCommandExists(Id.MoveLineUp, null); }
        }

        /// <summary>
        /// Gets the value that represents the Play Conversation command.
        /// </summary>
        public static RockstarRoutedCommand PlayConversation
        {
            get { return EnsureCommandExists(Id.PlayConversation, Icons.PlayConversation); }
        }

        /// <summary>
        /// Gets the value that represents the Play Line command.
        /// </summary>
        public static RockstarRoutedCommand PlayLine
        {
            get { return EnsureCommandExists(Id.PlayLine, Icons.PlayLine); }
        }

        /// <summary>
        /// Gets the value that represents the Stop Playing command.
        /// </summary>
        public static RockstarRoutedCommand StopPlaying
        {
            get { return EnsureCommandExists(Id.StopPlaying, Icons.StopPlaying); }
        }

        /// <summary>
        /// Gets the value that represents the Validate against folder command.
        /// </summary>
        public static RockstarRoutedCommand ValidateAgainstFolder
        {
            get { return EnsureCommandExists(Id.ValidateAgainstFolder, null); }
        }

        /// <summary>
        /// Gets the value that represents the Validate SFX lines command.
        /// </summary>
        public static RockstarRoutedCommand ValidateSfxLines
        {
            get { return EnsureCommandExists(Id.ValidateSfxLines, null); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="RSG.Editor.RockstarRoutedCommand"/> object that is
        /// associated with the specified identifier.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command to create.
        /// </param>
        /// <param name="icon">
        /// The icon that the created command should be using.
        /// </param>
        /// <returns>
        /// The newly created <see cref="RSG.Editor.RockstarRoutedCommand"/> object.
        /// </returns>
        private static RockstarRoutedCommand CreateCommand(string id, BitmapSource icon)
        {
            string name = _resourceManager.GetName(id);
            string category = _resourceManager.GetCategory(id);
            string description = _resourceManager.GetDescription(id);

            InputGestureCollection gestures = _resourceManager.GetGestures(id);
            return new RockstarRoutedCommand(
                name, typeof(TextCommands), gestures, category, description, icon);
        }

        /// <summary>
        /// Makes sure the internal value for the command associated with the specified
        /// identifier exists and returns it.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command that should be created and returned.
        /// </param>
        /// <returns>
        /// The <see cref="RSG.Editor.RockstarRoutedCommand"/> object associated with the
        /// specified identifier.
        /// </returns>
        private static RockstarRoutedCommand EnsureCommandExists(Id id)
        {
            return EnsureCommandExists(id, null);
        }

        /// <summary>
        /// Makes sure the internal value for the command associated with the specified
        /// identifier exists and returns it.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command that should be created and returned.
        /// </param>
        /// <param name="icon">
        /// The icon that the command should be using.
        /// </param>
        /// <returns>
        /// The <see cref="RSG.Editor.RockstarRoutedCommand"/> object associated with the
        /// specified identifier.
        /// </returns>
        private static RockstarRoutedCommand EnsureCommandExists(Id id, BitmapSource icon)
        {
            int commandIndex = (int)id;
            if (commandIndex < -1 || commandIndex >= _internalCommands.Length)
            {
                return null;
            }

            RockstarRoutedCommand command = _internalCommands[commandIndex];
            if (command == null)
            {
                lock (_internalCommands)
                {
                    if (command == null)
                    {
                        command = CreateCommand(id.ToString(), icon);
                        _internalCommands[commandIndex] = command;
                    }
                }
            }

            return command;
        }

        /// <summary>
        /// Initialises the array used to store the
        /// <see cref="RSG.Editor.RockstarRoutedCommand"/> objects that make up the core
        /// commands for a Rockstar application.
        /// </summary>
        /// <returns>
        /// The array used to store the internal command objects.
        /// </returns>
        private static RockstarRoutedCommand[] InitialiseInternalStorage()
        {
            return new RockstarRoutedCommand[Enum.GetValues(typeof(Id)).Length];
        }

        /// <summary>
        /// Initialises the resource manager to use to retrieve command related strings and
        /// objects from a resource data file.
        /// </summary>
        /// <returns>
        /// The resource manager to use with this class.
        /// </returns>
        private static CommandResourceManager InitialiseResourceManager()
        {
            Type type = typeof(TextCommands);
            string baseName = type.Namespace + ".Resources.CommandStringTable";
            return new CommandResourceManager(baseName, type.Assembly);
        }
        #endregion Methods
    } // RSG.Text.Commands.TextCommands {Class}
} // RSG.Text.Commands {Namespace}
