﻿//---------------------------------------------------------------------------------------------
// <copyright file="CommandInstanceCreationArgs.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    /// <summary>
    /// Encapsulates the arguments used to create a new command instance using the
    /// <see cref="RockstarCommandManager"/> class.
    /// </summary>
    public sealed class CommandInstanceCreationArgs
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="DisplayStyle"/> property.
        /// </summary>
        private CommandItemDisplayStyle _displayStyle;

        /// <summary>
        /// The private field used for the <see cref="AreMultiItemsShown"/> property.
        /// </summary>
        private bool _areMultiItemsShown;

        /// <summary>
        /// The private field used for the <see cref="EachItemStartsGroup"/> property.
        /// </summary>
        private bool _eachItemStartsGroup;

        /// <summary>
        /// The private field used for the <see cref="ShowFilterCount"/> property.
        /// </summary>
        private bool _showFilterCount;

        /// <summary>
        /// The private field used for the <see cref="Width"/> property.
        /// </summary>
        private int _width;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CommandInstanceCreationArgs"/> class.
        /// </summary>
        public CommandInstanceCreationArgs()
        {
            this._displayStyle = CommandItemDisplayStyle.Default;
            this._areMultiItemsShown = true;
            this._eachItemStartsGroup = false;
            this._showFilterCount = true;
            this._width = 160;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether the created command instance shows its
        /// child items as individual items in the user interface.
        /// </summary>
        public bool AreMultiItemsShown
        {
            get { return this._areMultiItemsShown; }
            set { this._areMultiItemsShown = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the created command instance individual
        /// items start a group each or not.
        /// </summary>
        public bool EachItemStartsGroup
        {
            get { return this._eachItemStartsGroup; }
            set { this._eachItemStartsGroup = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the created command instance shows its
        /// filter count in the user interface.
        /// </summary>
        public bool ShowFilterCount
        {
            get { return this._showFilterCount; }
            set { this._showFilterCount = value; }
        }

        /// <summary>
        /// Gets or sets a value that determines what the display style is for the created
        /// command instance.
        /// </summary>
        public CommandItemDisplayStyle DisplayStyle
        {
            get { return this._displayStyle; }
            set { this._displayStyle = value; }
        }

        /// <summary>
        /// Gets or sets the width of the control used for the created command instance.
        /// </summary>
        public int Width
        {
            get { return this._width; }
            set { this._width = value; }
        }
        #endregion Properties
    } // RSG.Editor.CommandInstanceCreationArgs {Class}
} // RSG.Editor {Namespace}
