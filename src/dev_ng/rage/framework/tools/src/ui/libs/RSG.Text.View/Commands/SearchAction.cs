﻿//---------------------------------------------------------------------------------------------
// <copyright file="SearchAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.View.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using System.Windows.Controls;
    using System.Windows.Input;
    using RSG.Editor;
    using RSG.Text.ViewModel;

    /// <summary>
    /// Implements the Rockstar search commands for the dialogue file control. This class
    /// cannot be inherited.
    /// </summary>
    public sealed class SearchAction : SearchAction<DialogueFileControl>
    {
        #region Fields
        /// <summary>
        /// The current regular expression.
        /// </summary>
        private static Regex _currentRegex;

        /// <summary>
        /// A value indicating whether the last search produced any results.
        /// </summary>
        private static bool _foundResults;

        /// <summary>
        /// The previously selected item in the data grid.
        /// </summary>
        private static object _previousSelection;

        /// <summary>
        /// The current result which is selected.
        /// </summary>
        private static object _searchResult;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SearchAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public SearchAction(ParameterResolverDelegate<DialogueFileControl> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the execute command handler for the
        /// <see cref="RockstarCommands.Find"/> command when the search data indicates the
        /// search should be cleared.
        /// </summary>
        /// <param name="control">
        /// The dialogue file control resolved for this implementer.
        /// </param>
        protected override void ClearSearch(DialogueFileControl control)
        {
        }

        /// <summary>
        /// Modifies the search parameters so that the user cannot filter the search results.
        /// </summary>
        /// <param name="definition">
        /// The definition that has been created.
        /// </param>
        protected override void ModifyDefinition(ICommandWithSearchParameters definition)
        {
            definition.CanFilterResults = false;
        }

        /// <summary>
        /// Override to set logic against the execute command handler for the
        /// <see cref="RockstarCommands.Find"/> command.
        /// </summary>
        /// <param name="control">
        /// The dialogue file control resolved for this implementer.
        /// </param>
        /// <param name="data">
        /// The search data that has been sent with the command.
        /// </param>
        /// <returns>
        /// A task that represents the work done by the search handler.
        /// </returns>
        protected override async Task<bool> Search(
            DialogueFileControl control, SearchData data)
        {
            bool result = false;
            data.ThrowIfCancellationRequested();
            result = await Task.Factory.StartNew(
                delegate
                {
                    if (data.Cleared)
                    {
                        this.ClearSearch(control);
                        return true;
                    }

                    return this.SearchDelegate(control, data);
                });

            return result;
        }

        /// <summary>
        /// Override to set logic against the execute command handler for the
        /// <see cref="RockstarCommands.FindNext"/> command.
        /// </summary>
        /// <param name="control">
        /// The control to search in.
        /// </param>
        /// <param name="data">
        /// The search data that has been sent with the command.
        /// </param>
        /// <returns>
        /// True if the next search result was navigated to successfully; otherwise, false.
        /// </returns>
        protected override bool SearchNext(DialogueFileControl control, SearchData data)
        {
            if (_foundResults == false)
            {
                return false;
            }

            return this.GoToResult(control, false);
        }

        /// <summary>
        /// Override to set logic against the execute command handler for the
        /// <see cref="RockstarCommands.FindPrevious"/> command.
        /// </summary>
        /// <param name="control">
        /// The control to search in.
        /// </param>
        /// <param name="data">
        /// The search data that has been sent with the command.
        /// </param>
        /// <returns>
        /// True if the next search result was navigated to successfully; otherwise, false.
        /// </returns>
        protected override bool SearchPrevious(DialogueFileControl control, SearchData data)
        {
            if (_foundResults == false)
            {
                return false;
            }

            return this.GoToResult(control, true);
        }

        /// <summary>
        /// Gets the data context of the specified control as a DialogueViewModel object.
        /// Making sure the logic is done on the correct thread.
        /// </summary>
        /// <param name="control">
        /// The control whose data context need to be retrieved.
        /// </param>
        /// <returns>
        /// The data context currently attached to the specified control.
        /// </returns>
        private static DialogueViewModel GetDataContext(DialogueFileControl control)
        {
            if (!control.Dispatcher.CheckAccess())
            {
                return control.Dispatcher.Invoke<DialogueViewModel>(
                    new Func<DialogueViewModel>(
                        delegate
                        {
                            return GetDataContext(control);
                        }));
            }

            return control.DataContext as DialogueViewModel;
        }

        /// <summary>
        /// Goes to the next result in the specified control, either going forwards from the
        /// current selection or backwards.
        /// </summary>
        /// <param name="control">
        /// The control to navigator within.
        /// </param>
        /// <param name="reverse">
        /// A value indicating whether to navigator forwards to the next result or backwards.
        /// </param>
        /// <returns>
        /// True if the next result was found; otherwise, false.
        /// </returns>
        private bool GoToResult(DialogueFileControl control, bool reverse)
        {
            DataGrid dataGrid = control.ConversationList.ConversationDataGrid;
            bool selectionFirst = false;
            if (!Object.ReferenceEquals(dataGrid.SelectedItem, _previousSelection))
            {
                dataGrid = control.LineList.LineDataGrid;
                if (!Object.ReferenceEquals(dataGrid.SelectedItem, _previousSelection))
                {
                    selectionFirst = !reverse;
                    _searchResult = null;
                    _previousSelection = null;
                }
            }

            IEnumerable<object> items = this.OrderItems(
                control, _previousSelection, reverse, selectionFirst);

            foreach (object item in items)
            {
                ConversationViewModel conversationViewModel = item as ConversationViewModel;
                if (conversationViewModel != null)
                {
                    if (_currentRegex.IsMatch(conversationViewModel.Description))
                    {
                        dataGrid = control.ConversationList.ConversationDataGrid;
                        _searchResult = item;
                        dataGrid.SelectedItem = item;
                        _previousSelection = item;
                        dataGrid.ScrollIntoView(item);
                        return true;
                    }
                }
                else
                {
                    LineViewModel lineViewModel = item as LineViewModel;
                    if (lineViewModel != null)
                    {
                        if (_currentRegex.IsMatch(lineViewModel.Dialogue))
                        {
                            dataGrid = control.ConversationList.ConversationDataGrid;
                            dataGrid.SelectedItem = lineViewModel.Conversation;
                            dataGrid.ScrollIntoView(item);

                            dataGrid = control.LineList.LineDataGrid;
                            _searchResult = item;
                            dataGrid.SelectedItem = item;
                            _previousSelection = item;
                            dataGrid.ScrollIntoView(item);
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Gets the items currently that can be search in the order they need to be searched
        /// in.
        /// </summary>
        /// <param name="control">
        /// The dialogue control that can be used to get any filter information from as well as
        /// the raw list from the data context.
        /// </param>
        /// <param name="selectedItem">
        /// The selected item that the order should start from if selection first is
        /// specified.
        /// </param>
        /// <param name="reverse">
        /// A value indicating whether the items should be reversed before ordering them.
        /// </param>
        /// <param name="selectionFirst">
        /// A value indicating whether the selected item (if set) should be ordered as the
        /// first item to search or the last.
        /// </param>
        /// <returns>
        /// The items that should be searched in in the order they need to be searched in.
        /// </returns>
        private List<object> OrderItems(
            DialogueFileControl control,
            object selectedItem,
            bool reverse,
            bool selectionFirst)
        {
            DialogueViewModel viewModel = GetDataContext(control);
            if (viewModel == null)
            {
                return null;
            }

            DialogueConversationControl conversationControl = control.ConversationList;
            DialogueLineControl lineControl = control.LineList;

            List<object> items = new List<object>();
            foreach (ConversationViewModel conversationViewModel in viewModel.Conversations)
            {
                if (!conversationControl.FilterItem(conversationViewModel))
                {
                    continue;
                }

                items.Add(conversationViewModel);
                foreach (LineViewModel lineViewModel in conversationViewModel.Lines)
                {
                    ////if (!lineControl.FilterItem(lineViewModel))
                    ////{
                    ////    continue;
                    ////}

                    items.Add(lineViewModel);
                }
            }

            if (reverse)
            {
                items.Reverse();
            }

            bool foundSelection = false;
            List<object> first = new List<object>();
            List<object> second = new List<object>();
            foreach (object item in items)
            {
                if (foundSelection)
                {
                    first.Add(item);
                    continue;
                }

                if (Object.ReferenceEquals(item, selectedItem))
                {
                    foundSelection = true;
                }
                else
                {
                    second.Add(item);
                }
            }

            List<object> list = first.Concat(second).ToList();
            if (selectedItem != null)
            {
                int insertIndex = selectionFirst ? 0 : list.Count;
                list.Insert(insertIndex, selectedItem);
            }

            return list;
        }

        /// <summary>
        /// Performs the search described in the specified search data.
        /// </summary>
        /// <param name="control">
        /// The control to search in.
        /// </param>
        /// <param name="data">
        /// The data that contains the search parameters.
        /// </param>
        /// <returns>
        /// A value indicating whether the search produced any results.
        /// </returns>
        private bool SearchDelegate(DialogueFileControl control, SearchData data)
        {
            _foundResults = false;
            data.ThrowIfCancellationRequested();
            if (!this.TryCreateSearchExpression(data, out _currentRegex))
            {
                return false;
            }

            DialogueConversationControl conversationControl = control.ConversationList;
            DialogueLineControl lineControl = control.LineList;
            DialogueViewModel viewModel = GetDataContext(control);
            if (conversationControl == null || lineControl == null || viewModel == null)
            {
                return false;
            }

            foreach (ConversationViewModel conversationViewModel in viewModel.Conversations)
            {
                data.ThrowIfCancellationRequested();
                if (_currentRegex.IsMatch(conversationViewModel.Description))
                {
                    _foundResults = true;
                    break;
                }
                else
                {
                    foreach (LineViewModel lineViewModel in conversationViewModel.Lines)
                    {
                        data.ThrowIfCancellationRequested();
                        if (_currentRegex.IsMatch(lineViewModel.Dialogue))
                        {
                            _foundResults = true;
                            break;
                        }
                    }
                }

                if (_foundResults == true)
                {
                    break;
                }
            }

            data.ThrowIfCancellationRequested();
            _previousSelection = null;
            _searchResult = null;
            return _foundResults;
        }

        /// <summary>
        /// Creates a regular expression object that is needed to perform the search defined
        /// in the specified data.
        /// </summary>
        /// <param name="data">
        /// The data to create the regular expression from.
        /// </param>
        /// <param name="expression">
        /// When this method returns contains the expression object needed to perform the
        /// search defined in the specified data.
        /// </param>
        /// <returns>
        /// True if the creation was successful; otherwise, false.
        /// </returns>
        private bool TryCreateSearchExpression(SearchData data, out Regex expression)
        {
            string pattern = data.SearchText;
            RegexOptions options = RegexOptions.None;
            if (!data.RegularExpressions)
            {
                pattern = Regex.Escape(pattern);
            }

            if (!data.MatchCase)
            {
                options |= RegexOptions.IgnoreCase;
            }

            if (data.MatchWord)
            {
                // Cannot use word boundary as the (.) isn't handled correctly.
                // pattern = String.Format(@"\b{0}\b", pattern);
                pattern = String.Format(@"^{0}$", pattern);
            }

            try
            {
                expression = new Regex(pattern, options);
            }
            catch (ArgumentException ex)
            {
                Debug.Assert(false, "Unable to create regular expression.", "{0}", ex.Message);
                data.RegisterParseError(ex.Message);
                expression = null;
                return false;
            }

            return true;
        }
        #endregion Methods
    } // RSG.Text.View.Commands.SearchAction {Class}
} // RSG.Text.View.Commands {Namespace}
