﻿//---------------------------------------------------------------------------------------------
// <copyright file="ConnectToHostAction.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.AssetBuildMonitor.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Net;
    using RSG.AssetBuildMonitor.ViewModel;
    using RSG.Editor;
    using RSG.Editor.Controls.Dock.ViewModel;
    using RSG.Project.ViewModel;
    using RSG.Project.Model;
    using RSG.AssetBuildMonitor.ViewModel.Project;

    /// <summary>
    /// Contains the logic for the <see cref="Commands.ConnectToHost"/> routed
    /// command. This class cannot be inherited.
    /// </summary>
    public sealed class ConnectToNewHostAction : ButtonAction<CommandArgs>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="ConnectToHostAction"/> class.
        /// </summary>
        /// <param name="serviceProvider">
        /// The service provider that is used by this action to perform application specific
        /// operations, for example selecting a file to open.
        /// </param>
        public ConnectToNewHostAction(ICommandServiceProvider serviceProvider)
            : base(serviceProvider)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ConnectToHostAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public ConnectToNewHostAction(CommandResolver resolver)
            : base(new ParameterResolverDelegate<CommandArgs>(resolver))
        {
        }
        #endregion // Constructor(s)

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(CommandArgs args)
        {
            return (true);
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(CommandArgs args)
        {
            IMessageBoxService messageService = this.GetService<IMessageBoxService>();
            if (null == messageService)
            {
                Debug.Assert(false, "Unable to add host as IMessageBoxService is missing.");
                throw (new NotSupportedException());
            }

            IDialogServices dialogServices = this.GetService<IDialogServices>();
            if (null == dialogServices)
            {
                Debug.Assert(false, "Unable to add host as IDialogService is missing.");
                throw (new NotSupportedException());
            }

            String hostname = String.Empty;
            if (dialogServices.ShowAddNewHostDialog(out hostname))
            {
                // We remap "localhost" to the local hostname; and we resolve IP
                // address.
                String userInput = hostname.Trim().ToUpper();
                IPAddress ip = IPAddress.None;
                if ("LOCALHOST".Equals(userInput))
                {
                    // Map to local hostname.
                    userInput = Environment.MachineName;
                }
                else if (IPAddress.TryParse(userInput, out ip))
                {
                    // Use DNS to get hostname.
                    IPHostEntry hostEntry = Dns.GetHostEntry(ip);
                    userInput = hostEntry.HostName;
                }

                // Before creating the new host we ensure that its not already in our
                // collection; or open.  Selecting it if it is.
                foreach (DocumentItem openedDocument in args.AllDocuments)
                {
                    if (String.Equals(openedDocument.FullPath, hostname))
                    {
                        openedDocument.IsSelected = true;
                        return;
                    }
                }

                DocumentPane pane = args.ViewSite.LastActiveDocumentPane;
                if (pane == null)
                {
                    pane = args.ViewSite.FirstDocumentPane;
                    if (pane == null)
                    {
                        Debug.Assert(false, "Cannot find the pane to open the document in.");
                        return;
                    }
                }

                try
                {
                    IHierarchyNode parent = args.CurrentProjectCollection.SelectedNode;
                    if (parent is HostNode)
                        parent = parent.Parent;
                    IHierarchyNode newHost = args.CurrentProjectCollection.ActiveProjectNode.AddNewFile(parent, userInput);
                    Debug.Assert(newHost is HostNode, "Internal error.");
                    
                    DocumentItem item = null;
                    ICreatesProjectDocument documentCreator = (newHost as ICreatesProjectDocument);
                    if (null == documentCreator)
                    {
                        item = new HostDocumentItem(pane, userInput);
                    }
                    else
                    {
                        item = documentCreator.CreateDocument(pane, userInput);
                    }

                    ICreatesDocumentContent contentCreator = (newHost as ICreatesDocumentContent);
                    item.Content = contentCreator.CreateDocumentContent(item.UndoEngine);
                    item.IsSelected = true;
                    parent.IsModified = true;
                    pane.InsertChild(pane.Children.Count, item);
                }
                catch (Exception)
                {
                    messageService.ShowStandardErrorBox(
                        String.Format(Resources.CommandStringTable.ConnectToHostError, userInput),
                        "Error");
                }
            }
        }
        #endregion // Methods
    }

} // RSG.AssetBuildMonitor.Commands namespace
