﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor.View;

namespace RSG.Rag.ViewModel.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    public interface IWidgetGroupViewModel : IWidgetViewModel, ITreeDisplayItem
    {
        #region Properties
        /// <summary>
        /// Returns the collection of WidgetGroup children.
        /// </summary>
        IReadOnlyCollection<IWidgetGroupViewModel> GroupChildren { get; }

        /// <summary>
        /// Flag indicating whether this group has been expanded.
        /// </summary>
        bool WidgetViewExpanded { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="forceAddReference"></param>
        void OnShow(bool forceAddReference = false);

        /// <summary>
        /// 
        /// </summary>
        void OnHide();
        #endregion // Methods
    } // IWidgetGroupViewModel
}
