﻿//---------------------------------------------------------------------------------------------
// <copyright file="BuildProjectCommandArgs.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Commands
{
    using RSG.Project.Model;

    /// <summary>
    /// Represents the argument class used by the build project commands.
    /// </summary>
    public class BuildProjectCommandArgs : ProjectCommandArgs
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="ProjectBuilder"/> property.
        /// </summary>
        private IProjectBuilder _projectBuilder;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BuildProjectCommandArgs"/> class.
        /// </summary>
        public BuildProjectCommandArgs()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the project builder to use for the build process.
        /// </summary>
        public IProjectBuilder ProjectBuilder
        {
            get { return this._projectBuilder; }
            set { this._projectBuilder = value; }
        }
        #endregion Properties
    } // RSG.Project.Commands.BuildProjectCommandArgs {Class}
} // RSG.Project.Commands {Namespace}
