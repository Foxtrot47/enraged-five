﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProjectCommandArgs.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Commands
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using RSG.Editor;
    using RSG.Editor.Controls.Dock.ViewModel;
    using RSG.Project.ViewModel;
    using RSG.Project.ViewModel.Definitions;

    /// <summary>
    /// Represents the argument class used by the project commands.
    /// </summary>
    public class ProjectCommandArgs
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="CollectionNode"/> property.
        /// </summary>
        private ProjectCollectionNode _collectionNode;

        /// <summary>
        /// The private field used for the <see cref="NodeRenameDelegate"/> property.
        /// </summary>
        private RenameNodeDelegate _nodeRenameDelegate;

        /// <summary>
        /// The private field used for the <see cref="NodeSelectionDelegate"/> property.
        /// </summary>
        private SelectNodeDelegate _nodeSelectionDelegate;

        /// <summary>
        /// The private field used for the <see cref="OpenedDocuments"/> property.
        /// </summary>
        private IEnumerable<DocumentItem> _openedDocuments;

        /// <summary>
        /// The private field used for the <see cref="SelectedNodes"/> property.
        /// </summary>
        private IEnumerable<IHierarchyNode> _selectedNodes;

        /// <summary>
        /// The private field used for the <see cref="ViewSite"/> property.
        /// </summary>
        private ViewSite _viewSite;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ProjectCommandArgs"/> class.
        /// </summary>
        public ProjectCommandArgs()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the collection node that the command will be operating on.
        /// </summary>
        public ProjectCollectionNode CollectionNode
        {
            get { return this._collectionNode; }
            set { this._collectionNode = value; }
        }

        /// <summary>
        /// Gets or sets a delegate method that can be used to show the rename edit control for
        /// a single hierarchy node inside the project explorer.
        /// </summary>
        public RenameNodeDelegate NodeRenameDelegate
        {
            get { return this._nodeRenameDelegate; }
            set { this._nodeRenameDelegate = value; }
        }

        /// <summary>
        /// Gets or sets a delegate method that can be used to select a single hierarchy node
        /// inside the project explorer.
        /// </summary>
        public SelectNodeDelegate NodeSelectionDelegate
        {
            get { return this._nodeSelectionDelegate; }
            set { this._nodeSelectionDelegate = value; }
        }

        /// <summary>
        /// Gets or sets the collection of currently opened documents.
        /// </summary>
        public IEnumerable<DocumentItem> OpenedDocuments
        {
            get { return this._openedDocuments ?? Enumerable.Empty<DocumentItem>(); }
            set { this._openedDocuments = value; }
        }

        /// <summary>
        /// Gets or sets the hierarchy nodes that are currently selected.
        /// </summary>
        public IEnumerable<IHierarchyNode> SelectedNodes
        {
            get { return this._selectedNodes ?? Enumerable.Empty<IHierarchyNode>(); }
            set { this._selectedNodes = value; }
        }

        /// <summary>
        /// Gets the number of hierarchy nodes that are currently selected.
        /// </summary>
        public int SelectionCount
        {
            get { return this.SelectedNodes.Count(); }
        }

        /// <summary>
        /// Gets or sets the view site that can be used to open new documents or manipulate the
        /// docking view for the user.
        /// </summary>
        public ViewSite ViewSite
        {
            get { return this._viewSite; }
            set { this._viewSite = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Determines whether there is a document open that represents the specified node.
        /// </summary>
        /// <param name="node">
        /// The node to test.
        /// </param>
        /// <returns>
        /// True if there is currently a document open that is attached to the specified node;
        /// otherwise, false.
        /// </returns>
        public bool IsOpen(IHierarchyNode node)
        {
            foreach (DocumentItem document in this.OpenedDocuments)
            {
                ProjectDocumentItem projectDocument = document as ProjectDocumentItem;
                if (projectDocument == null)
                {
                    continue;
                }

                if (Object.ReferenceEquals(node, projectDocument.FileNode))
                {
                    return true;
                }
            }

            return false;
        }
        #endregion Methods
    } // RSG.Project.Commands.ProjectCommandArgs {Class}
} // RSG.Project.Commands {Namespace}
