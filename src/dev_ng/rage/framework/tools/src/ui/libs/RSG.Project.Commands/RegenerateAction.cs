﻿//---------------------------------------------------------------------------------------------
// <copyright file="RegenerateAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2015. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using RSG.Editor;
    using RSG.Interop.Microsoft.Windows;
    using RSG.Project.Model;
    using RSG.Project.ViewModel;

    /// <summary>
    /// Contains the logic for the <see cref="ProjectCommands.Regenerate"/> routed command.
    /// This class cannot be inherited.
    /// </summary>
    public sealed class RegenerateAction : ButtonAction<ProjectCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="RegenerateAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public RegenerateAction(ProjectCommandResolver resolver)
            : base(new ParameterResolverDelegate<ProjectCommandArgs>(resolver))
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ProjectCommandArgs args)
        {
            if (args == null)
            {
                return false;
            }

            if (args.SelectedNodes != null && args.SelectionCount == 1)
            {
                IHierarchyNode node = args.SelectedNodes.FirstOrDefault();
                if (node is ProjectNode)
                {
                    var project = (ProjectNode)node;
                    if (project.Loaded)
                    {
                        foreach (IHierarchyNode child in project.GetChildren(false))
                        {
                            if (child is ProjectDynamicFolderNode)
                            {
                                return true;
                            }
                        }
                    }
                }
                else if (node is ProjectDynamicFolderNode)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(ProjectCommandArgs args)
        {
            var folderNodes = new List<ProjectDynamicFolderNode>();
            IHierarchyNode node = args.SelectedNodes.FirstOrDefault();
            if (node is ProjectNode)
            {
                foreach (IHierarchyNode child in ((ProjectNode)node).GetChildren(false))
                {
                    if (child is ProjectDynamicFolderNode)
                    {
                        folderNodes.Add((ProjectDynamicFolderNode)child);
                    }
                }
            }
            else if (node is ProjectDynamicFolderNode)
            {
                folderNodes.Add((ProjectDynamicFolderNode)node);
            }

            if (folderNodes.Count == 0)
            {
                return;
            }

            foreach (var dynamicFolder in folderNodes)
            {
                this.RegenerateFolder(dynamicFolder);
            }
        }

        private void RegenerateFolder(ProjectDynamicFolderNode folder)
        {
            ProjectNode project = folder.ParentProject;
            var children = folder.GetChildren(false).ToList();
            IProjectItemScope scope = folder.ProjectItem.Scope;
            foreach (IHierarchyNode child in children)
            {
                if (folder != null && folder.RemoveChild(child))
                {
                    child.ModifiedScopeNode.IsModified = true;
                    if (folder.ProjectItem != null)
                    {
                        if (scope != null)
                        {
                            scope.RemoveProjectItem(child.ProjectItem);
                        }
                    }
                }
            }

            string filters = folder.Model.GetMetadata("Filters");
            string directory = folder.Model.GetMetadata("FullPath");
            if (String.IsNullOrWhiteSpace(filters))
            {
                filters = "*.*";
            }

            List<string> filenames = new List<string>();
            foreach (string filter in filters.Split(';'))
            {
                if (String.IsNullOrWhiteSpace(filter))
                {
                    continue;
                }

                filenames.AddRange(Directory.GetFiles(directory, filter));
            }

            string scopeDir = scope.DirectoryPath;
            foreach (string filename in filenames)
            {
                string relative = Shlwapi.MakeRelativeFromDirectoryToFile(scopeDir, filename);
                project.AddNewFile(folder, relative);
            }

            bool recursive = folder.Model.GetMetadataAsBool("Recursive", false);
            if (recursive)
            {
                string[] subDirectories = Directory.GetDirectories(directory);
                foreach (string subDirectory in subDirectories)
                {
                    var newNode = folder.AddNewDynamicFolder(subDirectory, filters, true);
                    this.RegenerateFolder(newNode);
                }
            }
        }
        #endregion Methods
    } // RSG.Project.Commands.RegenerateAction {Class}
} // RSG.Project.Commands {Namespace}
