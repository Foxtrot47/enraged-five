﻿//---------------------------------------------------------------------------------------------
// <copyright file="ValidateAgainstFolder.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved.
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Commands
{
    using RSG.Base.Logging;
    using RSG.Base.Logging.Universal;
    using RSG.Editor;
    using RSG.Text.Commands;
    using RSG.Text.ViewModel;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text.RegularExpressions;

    /// <summary>
    /// Implements the <see cref="TextCommands.ValidateAgainstFolder"/> command.
    /// </summary>
    public class ValidateAgainstFolderAction : ButtonAction<TextConversationCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ValidateAgainstFolderAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public ValidateAgainstFolderAction(
            ParameterResolverDelegate<TextConversationCommandArgs> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(TextConversationCommandArgs args)
        {
            return args.Dialogue != null;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(TextConversationCommandArgs args)
        {
            ICommonDialogService service = this.GetService<ICommonDialogService>();
            string resultFolder;
            bool result = service.ShowSelectFolder(new Guid("CD38B3B9-E861-4606-9952-530F811280D6"), "SelectFolder", out resultFolder);
            if (result != true || !Directory.Exists(resultFolder))
            {
                return;
            }

            string[] allWavFiles = Directory.GetFiles(resultFolder, "*.wav", SearchOption.TopDirectoryOnly);
            for (int i = 0; i < allWavFiles.Length; i++)
            {
                allWavFiles[i] = Path.GetFileNameWithoutExtension(allWavFiles[i]);
            }

            string characterName = Path.GetFileNameWithoutExtension(resultFolder);

            bool found = false;
            foreach (var character in args.Dialogue.Model.Configurations.Characters)
            {
                if (character.Name == characterName)
                {
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                IMessageBoxService msgService = this.GetService<IMessageBoxService>();
                msgService.ShowStandardErrorBox("Unable to validate folder. Folder name should be equal to a valid character name.", "Error");
                return;
            }

            IUniversalLog log = LogFactory.CreateUniversalLog("FolderValidation");
            UniversalLogFile logFile = (UniversalLogFile)LogFactory.CreateUniversalLogFile(log);

            List<string> missionIds = new List<string>();
            missionIds.Add(args.Dialogue.Id.ToUpper());
            Dictionary<string, string> characterFilenames = new Dictionary<string, string>();
            foreach (var conversation in args.Dialogue.Conversations)
            {
                foreach (var line in conversation.Lines)
                {
                    if (line.AudioFilepath.StartsWith("SFX"))
                    {
                        continue;
                    }

                    if (!characterFilenames.ContainsKey(line.AudioFilepath))
                    {
                        characterFilenames.Add(line.AudioFilepath.ToUpper(), line.SelectedCharacter.Name);
                    }
                }
            }

            foreach (string error in CheckWavFilenameSyntax(allWavFiles))
            {
                log.Error(error);
            }

            foreach(string error in CheckWavFilenameSyntaxWarnings(allWavFiles, missionIds))
            {
                log.Error(error);
            }

            foreach (string error in CheckWavFilenameNumbers(allWavFiles))
            {
                log.Error(error);
            }

            foreach (string error in CheckWavFilenameExistence(allWavFiles, characterFilenames, characterName))
            {
                log.Error(error);
            }

            if (!log.HasErrors)
            {
                IMessageBoxService msgService = this.GetService<IMessageBoxService>();
                msgService.Show("No validation errors found.");
            }
            else
            {
                LogFactory.ShowUniversalLogViewer(logFile.Filename);
            }
        }

        private List<string> CheckWavFilenameSyntax(string[] filenames)
        {
            List<string> syntaxErrors = new List<string>();
            string pattern = "^_([A-Z]{4})_(\\d{2,})$";
            Regex regex = new Regex(pattern);

            string msg = "The wav file '{0}.wav' appears to have the incorrect filename format.";
            foreach (string filename in filenames)
            {
                int missionIdLength = filename.IndexOf('_');
                if (missionIdLength == -1)
                {
                    syntaxErrors.Add(string.Format("The wav file '{0}.wav' doesn't start with a mission id followed by a underscore.", filename));
                    continue;
                }

                if (!regex.IsMatch(filename.Substring(missionIdLength)))
                {
                    syntaxErrors.Add(string.Format(msg, filename));
                }
            }

            return syntaxErrors;
        }

        private List<string> CheckWavFilenameSyntaxWarnings(string[] filenames, List<string> missionIds)
        {
            List<string> warnings = new List<string>();
            foreach (string filename in filenames)
            {
                int missionIdLength = filename.IndexOf('_');
                if (missionIdLength == -1)
                {
                    continue;
                }

                string missionId = filename.Substring(0, missionIdLength);
                if (!missionIds.Contains(missionId))
                {
                    if (missionIdLength == 5)
                    {
                        missionId = filename.Substring(0, missionIdLength - 1);
                        if (!missionIds.Contains(missionId))
                        {
                            warnings.Add(string.Format("The wav file '{0}.wav' appears to start with a mission id that doesn't seem to exist.", filename));
                            continue;
                        }
                    }
                    else
                    {
                        warnings.Add(string.Format("The wav file '{0}.wav' appears to start with a mission id that doesn't seem to exist.", filename));
                        continue;
                    }
                }
            }

            return warnings;
        }

        private List<string> CheckWavFilenameNumbers(IEnumerable<string> filenames)
        {
            List<string> syntaxErrors = new List<string>();
            string msg = "The wav file '{0}.wav' appears to be in a incorrect sequence.";

            Dictionary<string, List<int>> numberList = new Dictionary<string, List<int>>();
            Dictionary<string, int> numbers = new Dictionary<string, int>();
            foreach (string filename in filenames)
            {
                string filenumber = filename;
                string fileCode = filename;
                int index = filename.LastIndexOf('_');
                if (index != -1)
                {
                    filenumber = filename.Substring(index + 1);
                    fileCode = filename.Substring(0, index);
                    index = fileCode.IndexOf('_') + 1;
                    if (index != -1)
                    {
                        fileCode = fileCode.Substring(index);
                    }
                }

                int number = 0;
                if (!int.TryParse(filenumber, out number))
                {
                    number = 1;
                }

                List<int> list = null;
                if (!numberList.TryGetValue(fileCode, out list))
                {
                    list = new List<int>();
                    numberList.Add(fileCode, list);
                }

                numbers.Add(filename, number);
                numberList[fileCode].Add(number);
            }

            foreach (string filename in filenames)
            {
                if (numbers[filename] > 1)
                {
                    string fileCode = filename;
                    int index = filename.LastIndexOf('_');
                    if (index != -1)
                    {
                        fileCode = filename.Substring(0, index);
                        index = fileCode.IndexOf('_') + 1;
                        if (index != -1)
                        {
                            fileCode = fileCode.Substring(index);
                        }
                    }

                    if (!numberList[fileCode].Contains(numbers[filename] - 1))
                    {
                        syntaxErrors.Add(string.Format(msg, filename));
                    }
                }
            }

            return syntaxErrors;
        }

        private List<string> CheckWavFilenameExistence(IEnumerable<string> filenames, Dictionary<string, string> lineFilenames, string characterName)
        {
            List<string> existenceErrors = new List<string>();
            foreach (string filename in filenames)
            {
                string character = null;
                if (!lineFilenames.TryGetValue(filename, out character))
                {
                    existenceErrors.Add(string.Format("The wav file '{0}.wav' doesn't exist in any dstar file.", filename));
                    continue;
                }

                if (character != characterName)
                {
                    existenceErrors.Add(string.Format("The wav file '{0}.wav' isn't for then character '{1}'", filename, characterName));
                    continue;
                }
            }

            return existenceErrors;
        }

        #endregion Methods
    } // RSG.Text.Commands.ValidateAgainstFolderAction {Class}
} // RSG.Text.Commands {Namespace}
