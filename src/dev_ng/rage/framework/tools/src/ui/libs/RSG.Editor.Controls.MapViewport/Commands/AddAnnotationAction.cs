﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using RSG.Editor;
using RSG.Editor.Controls.Helpers;
using RSG.Editor.Controls.MapViewport.Annotations;

namespace RSG.Editor.Controls.MapViewport.Commands
{
    /// <summary>
    /// The action logic responsible for adding an annotation to the map viewport at the
    /// location under the cursor.
    /// </summary>
    public class AddAnnotationAction : ButtonAction<AddAnnotationCommandArgs>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="AddAnnotationAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public AddAnnotationAction(ParameterResolverDelegate<AddAnnotationCommandArgs> resolver)
            : base(resolver)
        {
        }
        #endregion
        
        #region Overrides
        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The arguments for this action.
        /// </param>
        public override void Execute(AddAnnotationCommandArgs args)
        {
            Point position = args.Position;

            if (!args.PositionSet)
            {
                // Display a dialog allowing the user to specify a position.
                AnnotationCoordinatesViewModel coordsVm = new AnnotationCoordinatesViewModel();
                AnnotationCoordinatesWindow window = new AnnotationCoordinatesWindow();
                window.DataContext = coordsVm;
                window.Owner = args.MapViewport.GetVisualOrLogicalAncestor<Window>();
                if (window.ShowDialog() != true)
                {
                    return;
                }

                position = new Point(coordsVm.X, coordsVm.Y);
            }

            // Add the annotation.
            AnnotationCollection collection = args.MapViewport.Annotations;
            collection.Add(new Annotation(position, collection.UndoEngineProvider));
        }
        #endregion
    }
}
