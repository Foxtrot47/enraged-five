﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsMenuItem.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Commands
{
    using System.Windows;
    using System.Windows.Automation.Peers;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Represents a selectable item inside a <see cref="RSG.Editor.Controls.Commands.RsMenu"/>
    /// control.
    /// </summary>
    public class RsMenuItem : MenuItem
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="CheckmarkBrush"/> dependency property.
        /// </summary>
        public static DependencyProperty CheckmarkBrushProperty;

        /// <summary>
        /// Identifies the <see cref="GlyphBackgroundBrush"/> dependency property.
        /// </summary>
        public static DependencyProperty GlyphBackgroundBrushProperty;

        /// <summary>
        /// Identifies the <see cref="GlyphBorderBrush"/> dependency property.
        /// </summary>
        public static DependencyProperty GlyphBorderBrushProperty;

        /// <summary>
        /// Identifies the <see cref="GlyphBrush"/> dependency property.
        /// </summary>
        public static DependencyProperty GlyphBrushProperty;

        /// <summary>
        /// Identifies the <see cref="IconBitmap"/> dependency property.
        /// </summary>
        public static DependencyProperty IconBitmapProperty;

        /// <summary>
        /// Identifies the <see cref="IconSize"/> dependency property.
        /// </summary>
        public static DependencyProperty IconSizeProperty;

        /// <summary>
        /// Identifies the <see cref="IsPlacedInContextMenu"/> dependency property.
        /// </summary>
        public static DependencyProperty IsPlacedInContextMenuProperty;

        /// <summary>
        /// Identifies the <see cref="IsPlacedOnToolBar"/> dependency property.
        /// </summary>
        public static DependencyProperty IsPlacedOnToolBarProperty;

        /// <summary>
        /// Identifies the <see cref="LinkCheckBoxSizeToIconSize"/> dependency property.
        /// </summary>
        public static DependencyProperty LinkCheckBoxSizeToIconSizeProperty;

        /// <summary>
        /// Identifies the <see cref="MakeHeadersUppercase"/> dependency property.
        /// </summary>
        public static DependencyProperty MakeHeadersUppercaseProperty;

        /// <summary>
        /// Identifies the <see cref="IsPlacedInContextMenu"/> dependency property.
        /// </summary>
        private static readonly DependencyPropertyKey _isPlacedInContextMenuPropertyKey;

        /// <summary>
        /// Identifies the <see cref="IsPlacedOnToolBar"/> dependency property.
        /// </summary>
        private static readonly DependencyPropertyKey _isPlacedOnToolBarPropertyKey;

        /// <summary>
        /// The private field used for the <see cref="BatchStyleKey"/> property.
        /// </summary>
        private static ResourceKey _batchStyleKey = CreateStyleKey("BatchStyleKey");

        /// <summary>
        /// The private field used for the <see cref="ButtonStyleKey"/> property.
        /// </summary>
        private static ResourceKey _buttonStyleKey = CreateStyleKey("ButtonStyleKey");

        /// <summary>
        /// The private field used for the <see cref="ComboBoxStyleKey"/> property.
        /// </summary>
        private static ResourceKey _comboBoxStyleKey = CreateStyleKey("ComboBoxStyleKey");

        /// <summary>
        /// The private field used for the <see cref="ControllerStyleKey"/> property.
        /// </summary>
        private static ResourceKey _controllerStyleKey = CreateStyleKey("ControllerStyleKey");

        /// <summary>
        /// The private field used for the <see cref="FilterComboStyleKey"/> property.
        /// </summary>
        private static ResourceKey _filterComboStyleKey =
            CreateStyleKey("FilterComboStyleKey");

        /// <summary>
        /// The private field used for the <see cref="MenuStyleKey"/> property.
        /// </summary>
        private static ResourceKey _menuStyleKey = CreateStyleKey("MenuStyleKey");

        /// <summary>
        /// The private field used for the <see cref="SeparatorStyleKey"/> property.
        /// </summary>
        private static ResourceKey _separatorStyleKey = CreateStyleKey("SeparatorStyleKey");

        /// <summary>
        /// The private field used for the <see cref="TextStyleKey"/> property.
        /// </summary>
        private static ResourceKey _textStyleKey = CreateStyleKey("TextStyleKey");

        /// <summary>
        /// The private field used for the <see cref="ToggleStyleKey"/> property.
        /// </summary>
        private static ResourceKey _toggleStyleKey = CreateStyleKey("ToggleStyleKey");
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsMenuItem"/> class.
        /// </summary>
        static RsMenuItem()
        {
            CheckmarkBrushProperty =
                DependencyProperty.Register(
                "CheckmarkBrush",
                typeof(Brush),
                typeof(RsMenuItem),
                new FrameworkPropertyMetadata(SystemColors.MenuTextBrush));

            GlyphBackgroundBrushProperty =
                DependencyProperty.Register(
                "GlyphBackgroundBrush",
                typeof(Brush),
                typeof(RsMenuItem),
                new FrameworkPropertyMetadata(SystemColors.MenuTextBrush));

            GlyphBorderBrushProperty =
                DependencyProperty.Register(
                "GlyphBorderBrush",
                typeof(Brush),
                typeof(RsMenuItem),
                new FrameworkPropertyMetadata(SystemColors.MenuTextBrush));

            GlyphBrushProperty =
                DependencyProperty.Register(
                "GlyphBrush",
                typeof(Brush),
                typeof(RsMenuItem),
                new FrameworkPropertyMetadata(SystemColors.MenuTextBrush));

            IconBitmapProperty =
                DependencyProperty.Register(
                "IconBitmap",
                typeof(BitmapSource),
                typeof(RsMenuItem),
                new FrameworkPropertyMetadata(null));

            IconSizeProperty =
                RsMenu.IconSizeProperty.AddOwner(
                typeof(RsMenuItem),
                new FrameworkPropertyMetadata(
                    16.0, FrameworkPropertyMetadataOptions.Inherits));

            LinkCheckBoxSizeToIconSizeProperty =
                RsMenu.LinkCheckBoxSizeToIconSizeProperty.AddOwner(
                typeof(RsMenuItem),
                new FrameworkPropertyMetadata(
                    false, FrameworkPropertyMetadataOptions.Inherits));

            MakeHeadersUppercaseProperty =
                RsMenu.MakeHeadersUppercaseProperty.AddOwner(
                typeof(RsMenuItem),
                new FrameworkPropertyMetadata(
                    false, FrameworkPropertyMetadataOptions.Inherits));

            _isPlacedInContextMenuPropertyKey =
                DependencyProperty.RegisterReadOnly(
                "IsPlacedInContextMenu",
                typeof(bool),
                typeof(RsMenuItem),
                new FrameworkPropertyMetadata(false));

            _isPlacedOnToolBarPropertyKey =
                DependencyProperty.RegisterReadOnly(
                "IsPlacedOnToolBar",
                typeof(bool),
                typeof(RsMenuItem),
                new FrameworkPropertyMetadata(false));

            IsPlacedInContextMenuProperty =
                _isPlacedInContextMenuPropertyKey.DependencyProperty;

            IsPlacedOnToolBarProperty =
                _isPlacedOnToolBarPropertyKey.DependencyProperty;

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsMenuItem),
                new FrameworkPropertyMetadata(typeof(RsMenuItem)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsMenuItem"/> class.
        /// </summary>
        public RsMenuItem()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the resource key used to reference the style used on batch command items.
        /// </summary>
        public static ResourceKey BatchStyleKey
        {
            get { return _batchStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on button command items.
        /// </summary>
        public static ResourceKey ButtonStyleKey
        {
            get { return _buttonStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on combo box command items.
        /// </summary>
        public static ResourceKey ComboBoxStyleKey
        {
            get { return _comboBoxStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on controller command items.
        /// </summary>
        public static ResourceKey ControllerStyleKey
        {
            get { return _controllerStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on filter combo command
        /// items.
        /// </summary>
        public static ResourceKey FilterComboStyleKey
        {
            get { return _filterComboStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on menu command items.
        /// </summary>
        public static ResourceKey MenuStyleKey
        {
            get { return _menuStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on separator command items.
        /// </summary>
        public static new ResourceKey SeparatorStyleKey
        {
            get { return _separatorStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on text command items.
        /// </summary>
        public static ResourceKey TextStyleKey
        {
            get { return _textStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on toggle command items.
        /// </summary>
        public static ResourceKey ToggleStyleKey
        {
            get { return _toggleStyleKey; }
        }

        /// <summary>
        /// Gets or sets the brush that describes the fill of the checkmark used for this
        /// control.
        /// </summary>
        public Brush CheckmarkBrush
        {
            get { return (Brush)this.GetValue(CheckmarkBrushProperty); }
            set { this.SetValue(CheckmarkBrushProperty, value); }
        }

        /// <summary>
        /// Gets or sets the brush that describes the brush used for the background behind the
        /// glyphs for this control.
        /// </summary>
        public Brush GlyphBackgroundBrush
        {
            get { return (Brush)this.GetValue(GlyphBackgroundBrushProperty); }
            set { this.SetValue(GlyphBackgroundBrushProperty, value); }
        }

        /// <summary>
        /// Gets or sets the brush that describes the brush used for the borders around the
        /// glyphs for this control.
        /// </summary>
        public Brush GlyphBorderBrush
        {
            get { return (Brush)this.GetValue(GlyphBorderBrushProperty); }
            set { this.SetValue(GlyphBorderBrushProperty, value); }
        }

        /// <summary>
        /// Gets or sets the brush that describes the fill of the glyphs for this control.
        /// </summary>
        public Brush GlyphBrush
        {
            get { return (Brush)this.GetValue(GlyphBrushProperty); }
            set { this.SetValue(GlyphBrushProperty, value); }
        }

        /// <summary>
        /// Gets or sets the bitmap source object that is shown as the icon.
        /// </summary>
        public BitmapSource IconBitmap
        {
            get { return (BitmapSource)this.GetValue(IconBitmapProperty); }
            set { this.SetValue(IconBitmapProperty, value); }
        }

        /// <summary>
        /// Gets or sets the number of pixels both height and width the icons within the menu
        /// system takes up.
        /// </summary>
        public double IconSize
        {
            get { return (double)this.GetValue(IconSizeProperty); }
            set { this.SetValue(IconSizeProperty, value); }
        }

        /// <summary>
        /// Gets a value indicating whether this item is currently placed inside a Context
        /// Menu control.
        /// </summary>
        public bool IsPlacedInContextMenu
        {
            get { return (bool)this.GetValue(IsPlacedInContextMenuProperty); }
            set { this.SetValue(_isPlacedInContextMenuPropertyKey, value); }
        }

        /// <summary>
        /// Gets a value indicating whether this item is currently placed onto a Tool Bar
        /// control.
        /// </summary>
        public bool IsPlacedOnToolBar
        {
            get { return (bool)this.GetValue(IsPlacedOnToolBarProperty); }
            internal set { this.SetValue(_isPlacedOnToolBarPropertyKey, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the checkbox used for checkable items gets
        /// sized based on the current icon size or is static as a fixed size.
        /// </summary>
        public bool LinkCheckBoxSizeToIconSize
        {
            get { return (bool)this.GetValue(LinkCheckBoxSizeToIconSizeProperty); }
            set { this.SetValue(LinkCheckBoxSizeToIconSizeProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the content for the top level item headers
        /// are all in uppercase or not.
        /// </summary>
        public bool MakeHeadersUppercase
        {
            get { return (bool)this.GetValue(MakeHeadersUppercaseProperty); }
            set { this.SetValue(MakeHeadersUppercaseProperty, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates or identifies the element used to display the child items.
        /// </summary>
        /// <returns>
        /// A new <see cref="RSG.Editor.Controls.Commands.RsMenuItem"/>.
        /// </returns>
        protected override DependencyObject GetContainerForItemOverride()
        {
            RsMenuItem newItem = new RsMenuItem();
            newItem.IsPlacedInContextMenu = this.IsPlacedInContextMenu;
            return newItem;
        }

        /// <summary>
        /// Determines whether the specified item is, or is eligible to be, its own item
        /// container.
        /// </summary>
        /// <param name="item">
        /// The item to check whether it is an item container.
        /// </param>
        /// <returns>
        /// True if the item is eligible to be its own item container; otherwise, false.
        /// </returns>
        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            if (item is MenuItem || item is Separator)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Returns class-specific AutomationPeer implementations for the Windows Presentation
        /// Foundation (WPF) infrastructure.
        /// </summary>
        /// <returns>
        /// The type-specific AutomationPeer implementation associated with this control.
        /// </returns>
        protected override AutomationPeer OnCreateAutomationPeer()
        {
            return new RsMenuItemAutomationPeer(this);
        }

        /// <summary>
        /// Handles when this control receives keyboard focus.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.KeyboardFocusChangedEventArgs data used for this event.
        /// </param>
        protected override void OnGotKeyboardFocus(KeyboardFocusChangedEventArgs e)
        {
            if (e == null)
            {
                throw new SmartArgumentNullException(() => e);
            }

            if (e.NewFocus != this)
            {
                return;
            }

            UIElement uiElement = this.GetTemplateChild("PART_FocusTarget") as UIElement;
            if (uiElement == null)
            {
                return;
            }

            FocusNavigationDirection direction = FocusNavigationDirection.First;
            uiElement.MoveFocus(new TraversalRequest(direction));
        }

        /// <summary>
        /// Prepares the specified element to display the specified item.
        /// </summary>
        /// <param name="element">
        /// Element used to display the specified item.
        /// </param>
        /// <param name="item">
        /// Specified item.
        /// </param>
        protected override void PrepareContainerForItemOverride(
            DependencyObject element, object item)
        {
            CommandBarItemViewModel viewmodel = item as CommandBarItemViewModel;
            if (viewmodel == null)
            {
                base.PrepareContainerForItemOverride(element, item);
                return;
            }

            FrameworkElement frameworkElement = element as FrameworkElement;
            ResourceKey resource = null;
            switch (viewmodel.DisplayType)
            {
                case CommandItemDisplayType.Button:
                    resource = RsMenuItem.ButtonStyleKey;
                    break;
                case CommandItemDisplayType.Combo:
                    resource = RsMenuItem.ComboBoxStyleKey;
                    break;
                case CommandItemDisplayType.FilterCombo:
                    resource = RsMenuItem.FilterComboStyleKey;
                    break;
                case CommandItemDisplayType.Menu:
                    resource = RsMenuItem.MenuStyleKey;
                    break;
                case CommandItemDisplayType.Separator:
                    resource = RsMenuItem.SeparatorStyleKey;
                    break;
                case CommandItemDisplayType.Toggle:
                    resource = RsMenuItem.ToggleStyleKey;
                    break;
            }

            if (resource == null)
            {
                return;
            }

            frameworkElement.SetResourceReference(FrameworkElement.StyleProperty, resource);
        }

        /// <summary>
        /// Creates a new resource key that references a style with the specified name.
        /// </summary>
        /// <param name="styleName">
        /// The name that the resource key will be referencing.
        /// </param>
        /// <returns>
        /// A new resource key that references a style with the specified name.
        /// </returns>
        private static ResourceKey CreateStyleKey(string styleName)
        {
            return new StyleKey<RsMenuItem>(styleName);
        }
        #endregion Methods
    } // RSG.Editor.Controls.Commands.RsMenuItem {Class}
} // RSG.Editor.Controls.Commands {Namespace}
