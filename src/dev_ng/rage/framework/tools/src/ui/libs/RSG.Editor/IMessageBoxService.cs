﻿//---------------------------------------------------------------------------------------------
// <copyright file="IMessageBoxService.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System.Windows;

    /// <summary>
    /// When implemented represents a service that can be used to handle a request to show a
    /// message box to the user.
    /// </summary>
    public interface IMessageBoxService
    {
        #region Methods
        /// <summary>
        /// Creates a new message box that can be modified and shown to the user.
        /// </summary>
        /// <returns>
        /// A newly created message box object.
        /// </returns>
        IMessageBox CreateMessageBox();

        /// <summary>
        /// Displays a message box that has a message and that returns a result.
        /// </summary>
        /// <param name="messageBoxText">
        /// A System.String that specifies the text to display.
        /// </param>
        /// <returns>
        /// A System.Windows.MessageBoxResult value that specifies which message box button is
        /// clicked by the user.
        /// </returns>
        MessageBoxResult Show(string messageBoxText);

        /// <summary>
        /// Displays a message box in front of the specified window that has a message and that
        /// returns a result.
        /// </summary>
        /// <param name="owner">
        /// A System.Windows.Window that represents the owner window of the message box.
        /// </param>
        /// <param name="messageBoxText">
        /// A System.String that specifies the text to display.
        /// </param>
        /// <returns>
        /// A System.Windows.MessageBoxResult value that specifies which message box button is
        /// clicked by the user.
        /// </returns>
        MessageBoxResult Show(Window owner, string messageBoxText);

        /// <summary>
        /// Displays a message box that has a message and title bar caption; and that returns a
        /// result.
        /// </summary>
        /// <param name="messageBoxText">
        /// A System.String that specifies the text to display.
        /// </param>
        /// <param name="caption">
        /// A System.String that specifies the title bar caption to display.
        /// </param>
        /// <returns>
        /// A System.Windows.MessageBoxResult value that specifies which message box button is
        /// clicked by the user.
        /// </returns>
        MessageBoxResult Show(string messageBoxText, string caption);

        /// <summary>
        /// Displays a message box in front of the specified window that has a message and
        /// title bar caption; and that returns a result.
        /// </summary>
        /// <param name="owner">
        /// A System.Windows.Window that represents the owner window of the message box.
        /// </param>
        /// <param name="messageBoxText">
        /// A System.String that specifies the text to display.
        /// </param>
        /// <param name="caption">
        /// A System.String that specifies the title bar caption to display.
        /// </param>
        /// <returns>
        /// A System.Windows.MessageBoxResult value that specifies which message box button is
        /// clicked by the user.
        /// </returns>
        MessageBoxResult Show(Window owner, string messageBoxText, string caption);

        /// <summary>
        /// Displays a message box that has a message, title bar caption, and button; and that
        /// returns a result.
        /// </summary>
        /// <param name="messageBoxText">
        /// A System.String that specifies the text to display.
        /// </param>
        /// <param name="caption">
        /// A System.String that specifies the title bar caption to display.
        /// </param>
        /// <param name="button">
        /// A System.Windows.MessageBoxButton value that specifies which button or buttons to
        /// display.
        /// </param>
        /// <returns>
        /// A System.Windows.MessageBoxResult value that specifies which message box button is
        /// clicked by the user.
        /// </returns>
        MessageBoxResult Show(string messageBoxText, string caption, MessageBoxButton button);

        /// <summary>
        /// Displays a message box in front of the specified window that has a message, title
        /// bar caption, and button; and that returns a result.
        /// </summary>
        /// <param name="owner">
        /// A System.Windows.Window that represents the owner window of the message box.
        /// </param>
        /// <param name="messageBoxText">
        /// A System.String that specifies the text to display.
        /// </param>
        /// <param name="caption">
        /// A System.String that specifies the title bar caption to display.
        /// </param>
        /// <param name="button">
        /// A System.Windows.MessageBoxButton value that specifies which button or buttons to
        /// display.
        /// </param>
        /// <returns>
        /// A System.Windows.MessageBoxResult value that specifies which message box button is
        /// clicked by the user.
        /// </returns>
        MessageBoxResult Show(
            Window owner, string messageBoxText, string caption, MessageBoxButton button);

        /// <summary>
        /// Displays a message box that has a message, title bar caption, button, and icon; and
        /// that returns a result.
        /// </summary>
        /// <param name="messageBoxText">
        /// A System.String that specifies the text to display.
        /// </param>
        /// <param name="caption">
        /// A System.String that specifies the title bar caption to display.
        /// </param>
        /// <param name="button">
        /// A System.Windows.MessageBoxButton value that specifies which button or buttons to
        /// display.
        /// </param>
        /// <param name="icon">
        /// A System.Windows.MessageBoxImage value that specifies the icon to display.
        /// </param>
        /// <returns>
        /// A System.Windows.MessageBoxResult value that specifies which message box button is
        /// clicked by the user.
        /// </returns>
        MessageBoxResult Show(
            string messageBoxText,
            string caption,
            MessageBoxButton button,
            MessageBoxImage icon);

        /// <summary>
        /// Displays a message box in front of the specified window that has a message, title
        /// bar caption, button, and icon; and that returns a result.
        /// </summary>
        /// <param name="owner">
        /// A System.Windows.Window that represents the owner window of the message box.
        /// </param>
        /// <param name="messageBoxText">
        /// A System.String that specifies the text to display.
        /// </param>
        /// <param name="caption">
        /// A System.String that specifies the title bar caption to display.
        /// </param>
        /// <param name="button">
        /// A System.Windows.MessageBoxButton value that specifies which button or buttons to
        /// display.
        /// </param>
        /// <param name="icon">
        /// A System.Windows.MessageBoxImage value that specifies the icon to display.
        /// </param>
        /// <returns>
        /// A System.Windows.MessageBoxResult value that specifies which message box button is
        /// clicked by the user.
        /// </returns>
        MessageBoxResult Show(
            Window owner,
            string messageBoxText,
            string caption,
            MessageBoxButton button,
            MessageBoxImage icon);

        /// <summary>
        /// Displays a message box that has a message, title bar caption, button, and icon; and
        /// that returns a result.
        /// </summary>
        /// <param name="messageBoxText">
        /// A System.String that specifies the text to display.
        /// </param>
        /// <param name="caption">
        /// A System.String that specifies the title bar caption to display.
        /// </param>
        /// <param name="button">
        /// A System.Windows.MessageBoxButton value that specifies which button or buttons to
        /// display.
        /// </param>
        /// <param name="icon">
        /// A System.Windows.MessageBoxImage value that specifies the icon to display.
        /// </param>
        /// <param name="defaultResult">
        /// A System.Windows.MessageBoxResult value that specifies the default result of the
        /// message box.
        /// </param>
        /// <returns>
        /// A System.Windows.MessageBoxResult value that specifies which message box button is
        /// clicked by the user.
        /// </returns>
        MessageBoxResult Show(
            string messageBoxText,
            string caption,
            MessageBoxButton button,
            MessageBoxImage icon,
            MessageBoxResult defaultResult);

        /// <summary>
        /// Displays a message box in front of the specified window that has a message, title
        /// bar caption, button, and icon; and that returns a result.
        /// </summary>
        /// <param name="owner">
        /// A System.Windows.Window that represents the owner window of the message box.
        /// </param>
        /// <param name="messageBoxText">
        /// A System.String that specifies the text to display.
        /// </param>
        /// <param name="caption">
        /// A System.String that specifies the title bar caption to display.
        /// </param>
        /// <param name="button">
        /// A System.Windows.MessageBoxButton value that specifies which button or buttons to
        /// display.
        /// </param>
        /// <param name="icon">
        /// A System.Windows.MessageBoxImage value that specifies the icon to display.
        /// </param>
        /// <param name="defaultResult">
        /// A System.Windows.MessageBoxResult value that specifies the default result of the
        /// message box.
        /// </param>
        /// <returns>
        /// A System.Windows.MessageBoxResult value that specifies which message box button is
        /// clicked by the user.
        /// </returns>
        MessageBoxResult Show(
            Window owner,
            string messageBoxText,
            string caption,
            MessageBoxButton button,
            MessageBoxImage icon,
            MessageBoxResult defaultResult);

        /// <summary>
        /// Shows a standard error message box that has a message, and title. This message box
        /// displays an okay button and the error icon.
        /// </summary>
        /// <param name="messageBoxText">
        /// A System.String that specifies the text to display.
        /// </param>
        /// <param name="caption">
        /// A System.String that specifies the title bar caption to display.
        /// </param>
        void ShowStandardErrorBox(string messageBoxText, string caption);
        #endregion Methods
    } // RSG.Editor.IMessageBoxService {Class}
} // RSG.Editor {Namespace}
