﻿//---------------------------------------------------------------------------------------------
// <copyright file="PerforceIcons.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Perforce
{
    using System;
    using System.Windows.Media.Imaging;
    using RSG.Base.Extensions;

    /// <summary>
    /// Provides static properties that give access to common icons that can be used throughout
    /// a application.
    /// </summary>
    public static class PerforceIcons
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="AddState"/> property.
        /// </summary>
        private static BitmapSource _addState;

        /// <summary>
        /// The private field used for the <see cref="CheckedOut"/> property.
        /// </summary>
        private static BitmapSource _checkedOut;

        /// <summary>
        /// The private field used for the <see cref="CheckedOutState"/> property.
        /// </summary>
        private static BitmapSource _checkedOutState;

        /// <summary>
        /// The private field used for the <see cref="GetLatest"/> property.
        /// </summary>
        private static BitmapSource _getLatest;

        /// <summary>
        /// The private field used for the <see cref="GotLatestOverlay"/> property.
        /// </summary>
        private static BitmapSource _gotLatestOverlay;

        /// <summary>
        /// The private field used for the <see cref="MarkForAdd"/> property.
        /// </summary>
        private static BitmapSource _markForAdd;

        /// <summary>
        /// The private field used for the <see cref="NotLatestOverlay"/> property.
        /// </summary>
        private static BitmapSource _notLatestOverlay;

        /// <summary>
        /// The private field used for the <see cref="Revert"/> property.
        /// </summary>
        private static BitmapSource _revert;

        /// <summary>
        /// The private field used for the <see cref="RevertUnchanged"/> property.
        /// </summary>
        private static BitmapSource _revertUnchanged;

        /// <summary>
        /// A generic object that provides thread safety access to the image sources.
        /// </summary>
        private static object _syncRoot = new object();
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets the icon that shows an icon representing a file is checked out by the current
        /// user in source control.
        /// </summary>
        public static BitmapSource AddState
        {
            get
            {
                return EnsureLoaded(ref _addState, "perforceAddState7");
            }
        }

        /// <summary>
        /// Gets the icon for the check out perforce command.
        /// </summary>
        public static BitmapSource CheckedOut
        {
            get
            {
                return EnsureLoaded(ref _checkedOut, "checkOut16");
            }
        }

        /// <summary>
        /// Gets the icon that shows an icon representing a file is checked out by the current
        /// user in source control.
        /// </summary>
        public static BitmapSource CheckedOutState
        {
            get
            {
                return EnsureLoaded(ref _checkedOutState, "perforceCheckedOutState7");
            }
        }

        /// <summary>
        /// Gets the icon that shows an icon representing a file is up to date in source
        /// control.
        /// </summary>
        public static BitmapSource GotLatestOverlay
        {
            get
            {
                return EnsureLoaded(ref _gotLatestOverlay, "perforceGotLatestOverlay16");
            }
        }

        /// <summary>
        /// Gets the icon for the get latest revision perforce command.
        /// </summary>
        public static BitmapSource GetLatest
        {
            get
            {
                return EnsureLoaded(ref _getLatest, "getLatest16");
            }
        }

        /// <summary>
        /// Gets the icon for the add perforce command.
        /// </summary>
        public static BitmapSource MarkForAdd
        {
            get
            {
                return EnsureLoaded(ref _markForAdd, "markForAdd16");
            }
        }

        /// <summary>
        /// Gets the icon that shows an icon representing a file is out of date in source
        /// control.
        /// </summary>
        public static BitmapSource NotLatestOverlay
        {
            get
            {
                return EnsureLoaded(ref _notLatestOverlay, "perforceNotLatestOverlay16");
            }
        }

        /// <summary>
        /// Gets the icon for the revert perforce command.
        /// </summary>
        public static BitmapSource Revert
        {
            get
            {
                return EnsureLoaded(ref _revert, "revert16");
            }
        }

        /// <summary>
        /// Gets the icon for the revert if unchanged perforce command.
        /// </summary>
        public static BitmapSource RevertUnchanged
        {
            get
            {
                return EnsureLoaded(ref _revertUnchanged, "revertUnchanged16");
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Ensures that the specified image source is loaded with the specified resource name.
        /// </summary>
        /// <param name="source">
        /// The image source that should be loaded.
        /// </param>
        /// <param name="resourceName">
        /// The name of the resource the loaded image source should set as its source.
        /// </param>
        private static BitmapSource EnsureLoaded(ref BitmapSource source, string resourceName)
        {
            if (source == null)
            {
                lock (_syncRoot)
                {
                    if (source == null)
                    {
                        EnsureLoadedCore(ref source, resourceName);
                    }
                }
            }

            return source;
        }

        /// <summary>
        /// Ensures that the specified image source is loaded with the specified resource name.
        /// </summary>
        /// <param name="source">
        /// The image source that should be loaded.
        /// </param>
        /// <param name="resourceName">
        /// The name of the resource the loaded image source should set as its source.
        /// </param>
        private static void EnsureLoadedCore(ref BitmapSource source, string resourceName)
        {
            if (source != null)
            {
                return;
            }

            string assemblyName = typeof(PerforceIcons).Assembly.GetName().Name;
            string path = "Resources/" + resourceName + ".png";
            string bitmapPath = "pack://application:,,,/{0};component/{1}";
            bitmapPath = bitmapPath.FormatInvariant(assemblyName, path);
            Uri uri = new Uri(bitmapPath, UriKind.RelativeOrAbsolute);

            source = new BitmapImage(uri);
            source.Freeze();
        }
        #endregion Methods
    } // RSG.Editor.Controls.Perforce.PerforceIcons {Class}
} // RSG.Editor.Controls.Perforce {Namespace}
