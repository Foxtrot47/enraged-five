﻿//---------------------------------------------------------------------------------------------
// <copyright file="ConsoleCommand.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;

    /// <summary>
    /// Command that you can invoke when using the interactive console.
    /// </summary>
    public class ConsoleCommand
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Handler"/> property.
        /// </summary>
        private readonly Action<string, string[]> _handler;

        /// <summary>
        /// Private field for the <see cref="HelpInfo"/> property.
        /// </summary>
        private readonly string _helpInfo;

        /// <summary>
        /// Private field for the <see cref="Key"/> property.
        /// </summary>
        private readonly string _key;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ConsoleCommand"/> class using
        /// the specified key, handler and help information.
        /// </summary>
        /// <param name="key">
        /// The key used to identify the command.
        /// </param>
        /// <param name="handler">
        /// The handler to invoke when the command is encountered.
        /// </param>
        /// <param name="helpInfo">
        /// The help information about the command.
        /// </param>
        public ConsoleCommand(string key, Action<string, string[]> handler, string helpInfo)
        {
            this._key = key;
            this._handler = handler;
            this._helpInfo = helpInfo;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the handler to invoke when the command is encountered.
        /// </summary>
        public Action<string, string[]> Handler
        {
            get { return this._handler; }
        }

        /// <summary>
        /// Gets the help information about the command.
        /// </summary>
        public string HelpInfo
        {
            get { return this._helpInfo; }
        }

        /// <summary>
        /// Gets the key used to identify the command.
        /// </summary>
        public string Key
        {
            get { return this._key; }
        }
        #endregion Properties
    } // RSG.Editor.Controls.ConsoleCommand {Class}
} // RSG.Editor.Controls {Namespace}
