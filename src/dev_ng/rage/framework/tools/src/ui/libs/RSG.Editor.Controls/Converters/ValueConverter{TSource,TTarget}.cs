﻿//---------------------------------------------------------------------------------------------
// <copyright file="ValueConverter{TSource,TTarget}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Converters
{
    using System;
    using System.Diagnostics;
    using System.Globalization;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Data;
    using RSG.Editor.Controls.Resources;

    /// <summary>
    /// Provides a base class for all the value converters that convert a single source
    /// object into one target object.
    /// </summary>
    /// <typeparam name="TSource">
    /// The type of the source object.
    /// </typeparam>
    /// <typeparam name="TTarget">
    /// The type of the target object.
    /// </typeparam>
    public abstract class ValueConverter<TSource, TTarget> : IValueConverter
    {
        #region Fields
        /// <summary>
        /// A private reference to the MS.Internal.NamedObject that represents a
        /// disconnected item.
        /// </summary>
        private static object _disconnectedItem = CreateDisconnectedItemReference();
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ValueConverter{TSource,TTarget}"/>
        /// class.
        /// </summary>
        protected ValueConverter()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether or not to perform conversion validation before the
        /// convert method is called.
        /// </summary>
        protected virtual bool PerformConversionValidation
        {
            get { return true; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Converts the specified value object into a equivalent value of the type specified
        /// by <paramref name="targetType"/>.
        /// </summary>
        /// <param name="value">
        /// The original value to convert.
        /// </param>
        /// <param name="targetType">
        /// The type that the source value will be turned into.
        /// </param>
        /// <param name="parameter">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A converted value.
        /// </returns>
        public object Convert(
            object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (_disconnectedItem != null && Object.ReferenceEquals(value, _disconnectedItem))
            {
                // Disconnection is a bug in wpf when using a data source that isn't the data
                // context in a binding i.e through the trigger of a control template.
                return value;
            }

            bool valid = this.ValidateConvert(value, targetType);
            if (!valid)
            {
                Debug.Assert(valid == true, "Specified convert parameters are invalid.");
                return value;
            }

            return this.Convert((TSource)((object)value), parameter, culture);
        }

        /// <summary>
        /// Converts the specified value object into a equivalent value of the target type
        /// parameter.
        /// </summary>
        /// <param name="value">
        /// The original value to convert.
        /// </param>
        /// <returns>
        /// A converted value.
        /// </returns>
        public TTarget Convert(TSource value)
        {
            return this.Convert(value, null, CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// Converts the specified converted value back to its source value.
        /// </summary>
        /// <param name="value">
        /// The already converted target value to convert back to its source value.
        /// </param>
        /// <param name="targetType">
        /// The type to convert the value to.
        /// </param>
        /// <param name="parameter">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// The source value for the specified value.
        /// </returns>
        public object ConvertBack(
            object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool valid = this.ValidateConvertBack(value, targetType);
            if (!valid)
            {
                Debug.Assert(valid == true, "Specified convert back parameters are invalid.");
                return value;
            }

            return this.ConvertBack((TTarget)((object)value), parameter, culture);
        }

        /// <summary>
        /// Converters the given value and returns the result.
        /// </summary>
        /// <param name="value">
        /// The original value to convert.
        /// </param>
        /// <param name="param">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A converted value.
        /// </returns>
        protected virtual TTarget Convert(TSource value, object param, CultureInfo culture)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Converts a already converted value back to its original value and returns
        /// the result.
        /// </summary>
        /// <param name="value">
        /// The target value to convert back to its source value.
        /// </param>
        /// <param name="param">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// The source value for the already converted specified value.
        /// </returns>
        protected virtual TSource ConvertBack(TTarget value, object param, CultureInfo culture)
        {
            throw new NotSupportedException(
                "This converter cannot be used in a two-way binding.");
        }

        /// <summary>
        /// Validates the specified parameters to make sure they are consistent with the type
        /// arguments of the class.
        /// </summary>
        /// <param name="value">
        /// The original value to convert.
        /// </param>
        /// <param name="targetType">
        /// The type that the source value will be turned into.
        /// </param>
        /// <returns>
        /// True if the specified parameters are valid; otherwise, false.
        /// </returns>
        protected virtual bool ValidateConvert(object value, Type targetType)
        {
            if (value == DependencyProperty.UnsetValue)
            {
                return false;
            }

            if (!CheckValue<TSource>(value))
            {
                throw new InvalidOperationException(
                        ErrorStringTable.GetString("InvalidValueType", typeof(TTarget).Name));
            }

            if (this.PerformConversionValidation && targetType != null)
            {
                if (!targetType.IsAssignableFrom(typeof(TTarget)))
                {
                    throw new InvalidOperationException(
                        ErrorStringTable.GetString(
                        "InvalidTargetType",
                        typeof(TTarget).Name,
                        targetType.Name));
                }
            }

            return true;
        }

        /// <summary>
        /// Validates the specified parameters to make sure they are consistent with the type
        /// arguments of the class.
        /// </summary>
        /// <param name="value">
        /// The original value to convert.
        /// </param>
        /// <param name="targetType">
        /// The type that the source value will be turned into.
        /// </param>
        /// <returns>
        /// True if the specified parameters are valid; otherwise, false.
        /// </returns>
        protected virtual bool ValidateConvertBack(object value, Type targetType)
        {
            if (!CheckValue<TTarget>(value))
            {
                throw new InvalidOperationException(
                        ErrorStringTable.GetString("InvalidValueType", typeof(TTarget).Name));
            }

            if (this.PerformConversionValidation && targetType != null)
            {
                if (!targetType.IsAssignableFrom(typeof(TSource)))
                {
                    throw new InvalidOperationException(
                        ErrorStringTable.GetString(
                        "InvalidTargetType",
                        typeof(TTarget).Name,
                        targetType.Name));
                }
            }

            return true;
        }

        /// <summary>
        /// Checks to make sure the specified object value is of the correct type.
        /// </summary>
        /// <typeparam name="T">
        /// The type parameter that the specified value should be a instance of.
        /// </typeparam>
        /// <param name="value">
        /// The value to check.
        /// </param>
        /// <returns>
        /// True if the value is of the specified type parameter; otherwise false.
        /// </returns>
        private static bool CheckValue<T>(object value)
        {
            if (!(value is T) && (value != null || typeof(T).IsValueType))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Create a reference to the internal microsoft property that specifies a item that
        /// has been disconnected with its data context.
        /// </summary>
        /// <returns>
        /// A reference to the internal microsoft property that specifies a item that has been
        /// disconnected with its data context.
        /// </returns>
        private static object CreateDisconnectedItemReference()
        {
            Type type = typeof(System.Windows.Data.BindingExpressionBase);
            BindingFlags flags = BindingFlags.Static | BindingFlags.NonPublic;
            FieldInfo fieldInfo = type.GetField("DisconnectedItem", flags);
            if (fieldInfo == null)
            {
                return null;
            }

            return fieldInfo.GetValue(null);
        }
        #endregion Methods
    } // RSG.Editor.Controls.Converters.ValueConverter {Class}
} // RSG.Editor.Controls.Converters {Namespace}
