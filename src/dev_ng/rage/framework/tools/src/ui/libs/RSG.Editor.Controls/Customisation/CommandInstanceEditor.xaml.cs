﻿//---------------------------------------------------------------------------------------------
// <copyright file="CommandInstanceEditor.xaml.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Customisation
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for the command instance editor user control.
    /// </summary>
    internal partial class CommandInstanceEditor : UserControl
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CommandInstanceEditor"/> class.
        /// </summary>
        public CommandInstanceEditor()
        {
            this.InitializeComponent();
        }
        #endregion Constructors
    } // RSG.Editor.Controls.Customisation.CommandInstanceEditor {Class}
} // RSG.Editor.Controls.Customisation {Namespace}
