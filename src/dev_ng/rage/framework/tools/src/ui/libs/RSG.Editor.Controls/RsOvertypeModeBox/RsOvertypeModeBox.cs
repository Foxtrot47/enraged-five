﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsOvertypeModeBox.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Represents a control that shows the current overtype mode state for the application.
    /// </summary>
    public class RsOvertypeModeBox : Control
    {
        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsOvertypeModeBox" /> class.
        /// </summary>
        static RsOvertypeModeBox()
        {
            TypeOverwriteManager.Initialise();

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsOvertypeModeBox),
                new FrameworkPropertyMetadata(typeof(RsOvertypeModeBox)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsOvertypeModeBox" /> class.
        /// </summary>
        public RsOvertypeModeBox()
        {
        }
        #endregion Constructors
    } // RSG.Editor.Controls.RsOvertypeModeBox {Class}
} // RSG.Editor.Controls {Namespace}
