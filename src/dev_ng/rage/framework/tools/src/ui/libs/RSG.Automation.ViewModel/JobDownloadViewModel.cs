﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using SIO = System.IO;
using System.Linq;
using System.Text;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Editor;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Export;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Common.Utils;
using RSG.Pipeline.Automation.Consumers;
using RSG.Pipeline.Core;
using RSG.Pipeline.Content;
using RSG.Pipeline.Services.Platform;
using RSG.Platform;
using RSG.SourceControl.Perforce;
using P4API;
using System.Windows;

namespace RSG.Automation.ViewModel
{

    /// <summary>
    /// Downloading job view-model (any file-transfer; log or output files).
    /// </summary>
    public class JobDownloadViewModel : ViewModelBase
    {
        #region Constants
        // Pending changelist for zip, xml and other export network files.
        private static readonly String NetworkExportChangelistDescription = "Network export changes";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Associated job.
        /// </summary>
        public JobViewModel Job
        {
            get { return _job; }
            private set
            {
                SetProperty(ref _job, value,"Job");
            }
        }
        private JobViewModel _job;

        /// <summary>
        /// Type of download.
        /// </summary>
        public JobDownloadType DownloadType
        {
            get { return _jobDownloadType; }
            private set
            {
                SetProperty(ref _jobDownloadType, value, "DownloadType");
            }
        }
        private JobDownloadType _jobDownloadType;

        /// <summary>
        /// Job trigger.
        /// </summary>
        public ITrigger Trigger
        {
            get { return (this._job.Trigger); }
        }

        /// <summary>
        /// Changelist P4Web URI.
        /// </summary>
        public String ChangelistUri
        {
            get { return _changelistUri; }
            private set
            {
                SetProperty(ref _changelistUri, value, "ChangelistUri");
            }
        }
        private String _changelistUri;

        /// <summary>
        /// Files being download.
        /// </summary>
        public ObservableCollection<String> Files
        {
            get { return _files; }
            private set
            {
                SetProperty(ref _files, value, "Files");
            }
        }
        private ObservableCollection<String> _files;

        /// <summary>
        /// Current download progress (if IsDownloading).
        /// </summary>
        public float DownloadProgress
        {
            get { return _downloadProgress; }
            private set
            {
                SetProperty(ref _downloadProgress, value, "DownloadProgress", "DownloadProgressStatus");
            }
        }
        private float _downloadProgress;

        /// <summary>
        /// Temporary string representation to show if a download has finished, until progress bars used
        /// </summary>
        public string DownloadProgressStatus
        {
            get { return _downloadProgressStatus; }
            set
            {
                SetProperty(ref _downloadProgressStatus, value, "DownloadProgressStatus");
            }
        }
        private string _downloadProgressStatus;

        /// <summary>
        /// Total size of all output files size (bytes).
        /// </summary>
        public ulong OutputFileSize
        {
            get { return _outputFileSize; }
            private set
            {
                SetProperty(ref _outputFileSize, value, "OutputFileSize");
                this.OutputFileSizeText = String.Format("{0}", (RSG.Base.FileSize)this.OutputFileSize);
            }
        }
        private ulong _outputFileSize;

        /// <summary>
        /// Total size of all output files size (pretty string forma tted).
        /// </summary>
        public String OutputFileSizeText
        {
            get { return _outputFileSizeText; }
            private set
            {
                SetProperty(ref _outputFileSizeText, value, "OutputFileSizeText");
            }
        }
        private String _outputFileSizeText;

        /// <summary>
        /// Whether job is owned by current user.
        /// </summary>
        public bool OwnedByCurrentUser
        {
            get { return _ownedByCurrentUser; }
            private set
            {
                SetProperty(ref _ownedByCurrentUser, value, "OwnedByCurrentUser");
            }
        }
        private bool _ownedByCurrentUser;
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Config object.
        /// </summary>
        private IConfig _config;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="config"></param>
        /// <param name="type"></param>
        /// <param name="job"></param>
        public JobDownloadViewModel(IConfig config, JobDownloadType type, JobViewModel job)
        {
            this._config = config;
            this.DownloadType = type;
            this.Job = job;
            this.Files = new ObservableCollection<String>();
            this.OutputFileSize = 0;
            this.DownloadProgress = 0.0f;
            this.DownloadProgressStatus = "Pending";

            String username = String.Empty;

            if (this._job.Trigger is ChangelistTrigger)
            {
                ChangelistTrigger trigger = (ChangelistTrigger)this.Trigger;
                Uri p4webroot = new Uri(String.Format("http://{0}", _config.Studios.ThisStudio.PerforceWebServer));
                this.ChangelistUri = String.Format("{0}{1}?ac=10", p4webroot, trigger.Changelist);
                username = trigger.Username;
            }
            else if (this._job.Trigger is UserRequestTrigger)
            {
                UserRequestTrigger trigger = (UserRequestTrigger)this.Trigger;
                username = trigger.Username;
            }

            this.OwnedByCurrentUser = username.Equals(Environment.UserName);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Start downloading job data (asynchronously).  Opens it with the uLog Viewer when done.
        /// </summary>
        public async Task DownloadJobLogAsync()
        {
            IJob job = this._job.Job;
            FileTransferServiceConsumer consumer = new FileTransferServiceConsumer(
                this._config, this._job.FileTransferService);

            String downloadDirectory = consumer.GetClientFileDirectory(job);

            if (!SIO.Directory.Exists(downloadDirectory))
                SIO.Directory.CreateDirectory(downloadDirectory);

            String filename = String.Format("job_{0}.ulog", job.ID);

            Byte[] md5;
            this.Files.Add(filename);
            
            Task<bool> task = Task.Factory.StartNew(delegate
            {
                this.DownloadProgressStatus = "Downloading";
                // DHM FIX ME: change to segment download file version when we have it.
                // to allow us to report progress.
                if (consumer.FileAvailability2(job).Any(f => f.Filename.Contains("job.ulog")))
                    this.OutputFileSize = (ulong)consumer.FileAvailability2(job).First(f => f.Filename.Contains("job.ulog")).FileSize;
                bool downloadResult = (consumer.DownloadFile(job, "job.ulog", filename, out md5));
                this.DownloadProgress = 1.0f;
                this.DownloadProgressStatus = downloadResult ? "Downloaded" : "Failed";

                if (downloadResult)
                    OpenLogInViewer();

                // Verify MD5?
                return (downloadResult);
            });
            await Task.WhenAll(task);            
        }

        /// <summary>
        /// Open the log file for this job in the Universal Log Viewer
        /// </summary>
        public void OpenLogInViewer()
        {
            IJob job = this._job.Job;
            FileTransferServiceConsumer consumer = new FileTransferServiceConsumer(
                this._config, this._job.FileTransferService);

            String downloadDirectory = consumer.GetClientFileDirectory(job);
            String pathname = SIO.Path.Combine(downloadDirectory, String.Format("job_{0}.ulog", job.ID));

            if (System.IO.File.Exists(pathname))
            {
                // Open log in Universal Log Viewer
                System.Diagnostics.Process logViewerProcess = new System.Diagnostics.Process();
                logViewerProcess.StartInfo.FileName = String.Format("{0}/UniversalLogViewer/UniversalLogViewer.exe", this._config.ToolsBin);
                logViewerProcess.StartInfo.Arguments = pathname;
                logViewerProcess.Start();
            }
            else
            {
                Log.Log__Warning("Universal log file does not exist to open:{0}", pathname);
            }
        }


        /// <summary>
        /// Download job output data
        /// </summary>
        /// <returns></returns>
#warning DHM FIX ME: the download methods are focused on the Map and Cutscene Exporters.  These should be made generic when we have IJob output files.
        public async Task DownloadOutputFilesAsync()
        {
            IJob job = this._job.Job;
            FileTransferServiceConsumer consumer = new FileTransferServiceConsumer(
                this._config, this._job.FileTransferService);
            
            // Map Network Export still use a custom job so we have to handle it
            // with special code.  Hopefully we'll add re-usable "Output Files"
            // support to IJob soon and this code can be cleaned up.
            if (job is MapExportJob)
            {
                MapExportJob mapJob = (MapExportJob)job;
                Task<bool> task = Task.Factory.StartNew(delegate
                {
                    this.DownloadProgressStatus = "Downloading";
                    bool downloadResult = DownloadMapExportNetwork(this._config.Project.DefaultBranch, consumer, mapJob);
                    this.DownloadProgress = 1.0f;
                    this.DownloadProgressStatus = downloadResult ? "Downloaded" : "Failed";
                    // AJM: Change this when doing the proper progress bar stuff?   Botch job putting this here really.
                    NotifyPropertyChanged("Files");
                    return (downloadResult);
                });
                await Task.WhenAll(task);
            }
            else
            {
                if (job.Role.HasFlag(CapabilityType.CutsceneExportNetwork))
                {

                }
                else if (job.Role.HasFlag(CapabilityType.CutscenePackage))
                {

                }
                else
                {
#warning DHM FIX ME: how can I error here with this task stuff?
                    throw (new NotImplementedException());
                }
            }
        }
        #endregion // Controller Methods

        private async Task DownloadCutsceneOutputFilesAsync(IJob job)
        {
            throw (new NotImplementedException());
        }

        #region Private Methods
        /// <summary>
        /// Handles the map export network download function.
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
#warning DHM FIX ME: this is one reason we need to add "Output Files" to IJob!!  Aaarghagrrgaghgrhrgahaghrghargharghraghaghraghaghraghra.
        private bool DownloadMapExportNetwork(IBranch branch, FileTransferServiceConsumer consumer, MapExportJob job)
        {
            bool result = true;
            try
            {
                // Load content-tree and create helper for finding nodes.
                IProcessorCollection processors = new ProcessorCollection(branch.Project.Config);
                IContentTree tree = Factory.CreateTree(branch, processors);
                ContentTreeHelper treeHelper = new ContentTreeHelper(tree, processors);

                using (RSG.SourceControl.Perforce.P4 p4 = branch.Project.SCMConnect())
                {
                    String jobDirectory = consumer.GetClientFileDirectory(job);
                    String mapCacheFolder = SIO.Path.Combine("$(cache)", "raw", "maps");
                    mapCacheFolder = branch.Environment.Subst(mapCacheFolder);

                    if (!SIO.Directory.Exists(jobDirectory))
                        SIO.Directory.CreateDirectory(jobDirectory);
                    if (!SIO.Directory.Exists(mapCacheFolder))
                        SIO.Directory.CreateDirectory(mapCacheFolder);

                    // Check for any previous network export changelists which might now be empty - if multiple job downloads
                    // have been done for example, leaving some empty changelists.
                    DeleteEmptyPendingChangelists(p4, NetworkExportChangelistDescription);
                    String firstMapSectionName = SIO.Path.GetFileNameWithoutExtension(job.ExportProcesses.FirstOrDefault().DCCSourceFilename);
                    String jobCreatedTime = job.CreatedAt.ToString();

                    String changelistDescriptionWithJob =
                        String.Format("{0} for '{1}', sent at '{2}'.  ( GUID = {3} )",
                            NetworkExportChangelistDescription, firstMapSectionName, jobCreatedTime, job.ID.ToString());

                    // Create a changelist for this job download
                    P4PendingChangelist changeList = p4.CreatePendingChangelist(changelistDescriptionWithJob);

                    // List for files to move to the pending changelist
                    List<String> moveToChangelistArgs = new List<String>();
                    moveToChangelistArgs.Add("-c");
                    moveToChangelistArgs.Add(changeList.Number.ToString());

#warning AJM: Need to change this so it gets the file size for what is actually going to be downloaded,msince it might have multiple platform data but only be downloading 1 platform's zip file
                    if (consumer.FileAvailability2(job).Any())
                        this.OutputFileSize = (ulong)consumer.FileAvailability2(job).Sum(f => f.FileSize);

                    foreach (ExportProcess exportProc in job.ExportProcesses)
                    {
                        // Make sure the path isn't knackered
                        exportProc.ExportFiles = exportProc.ExportFiles.Select(x => SIO.Path.GetFullPath(x));

                        FileMapping[] fms = FileMapping.Create(p4, exportProc.ExportFiles.ToArray());
                        FileState[] fss = FileState.Create(p4, fms.Select(fm => fm.DepotFilename).ToArray());

                        Debug.Assert(fms.Length == fss.Length);
                        if (fms.Length != fss.Length)
                        {
                            Log.Log__Error("Filemapping array length does not match filestate array length in DownloadExportAndBuildData!");
                            result = false;
                            continue; // Skip.
                        }

                        //Force sync the xml and zip file to avoid any resolve issues later
                        List<String> args = new List<String>();
                        args.Add("-f");
                        args.AddRange(exportProc.ExportFiles);
                        p4.Run("sync", args.ToArray());

                        // Download the .zip and .xml files
                        for (int n = 0; n < fss.Length; ++n)
                        {
                            // Check to make sure the export file is checked out or writeable                                
                            if (fss[n].OpenAction != FileAction.Edit)
                            {
                                if (fss[n].OtherLocked == true)
                                {
                                    Log.Log__Warning("Unable to check out {0} for download, it's been made writable instead.", fms[n].DepotFilename);
                                    SIO.FileInfo fileInfo = new SIO.FileInfo(fms[n].LocalFilename);
                                    fileInfo.IsReadOnly = false;
                                }
                                else
                                {
                                    try
                                    {
                                        p4.Run("edit", fms[n].DepotFilename);
                                        // We've marked it for edit so add to list to move to pending changelist
                                        moveToChangelistArgs.Add(fms[n].DepotFilename);
                                    }
                                    catch (Exception ex)
                                    {
                                        Log.Log__Error("Error trying to checkout export file {0} for job {1}: {2}", fms[n].DepotFilename, job.ID, ex.Message);
                                        result = false;
                                    }
                                }
                            }
                            else
                            {
                                // We already have it checked out so add to list
                                moveToChangelistArgs.Add(fms[n].DepotFilename);
                            }

                            String relativeFilename = SIO.Path.GetFileName(fms[n].LocalFilename);
                            String downloadFilename = SIO.Path.Combine(jobDirectory, relativeFilename);
                            this.Files.Add(fms[n].LocalFilename);

                            DownloadCopyAndDeleteFile(consumer, job, relativeFilename, downloadFilename, fms[n].LocalFilename);
                        }

                        // Find the content node
                        String dccSourceBasename = SIO.Path.GetFileName(exportProc.DCCSourceFilename);
                        String mapName = SIO.Path.GetFileNameWithoutExtension(dccSourceBasename);
                        IContentNode mapSourceNode = treeHelper.CreateFile(exportProc.DCCSourceFilename);
                        IContentNode mapZipNode = treeHelper.GetExportZipNodeFromMaxFileNode(mapSourceNode);
                        if ((null == mapZipNode) || !(mapZipNode is File))
                        {
                            Log.Log__Error("Failed to find build data for map: {0}.", mapName);
                            result = false;
                        }
                        else
                        {
                            // Get correct rpf output for map prop files
                            IContentNode mapProcZipNode = treeHelper.GetProcessedZipNodeFromExportZipNode(mapZipNode);
                            IContentNode mapAreaCombineNode = treeHelper.GetCombineOutputForNodes(new IContentNode[] { mapZipNode });

                            // Download the platform RPFs for enabled targets
                            foreach (ITarget target in branch.Targets.Values)
                            {
                                if (!target.Enabled)
                                    continue;

                                if (null != mapAreaCombineNode)
                                {
                                    // Combined RPF e.g downtown.rpf
                                    String mapAreaCombineRPFPathname = PlatformPathConversion.ConvertAndRemapFilenameToPlatform(target,
                                        SIO.Path.GetFullPath(((File)mapAreaCombineNode).AbsolutePath));
                                    // Use the prefixed RPF name with map area name to find it on server and download it 
                                    String mapAreaZipFilename = target.Platform.ToString() + SIO.Path.GetFileName(SIO.Path.GetDirectoryName(mapAreaCombineRPFPathname)) + ".zip";
                                    String downloadFilenameCombined = SIO.Path.Combine(jobDirectory, mapAreaZipFilename);
                                    String mapAreaZipDestinationpathname = SIO.Path.Combine(mapCacheFolder, mapAreaZipFilename);

                                    if (consumer.FileAvailability(job).Contains(mapAreaZipFilename, StringComparer.OrdinalIgnoreCase))
                                    {
                                        this.Files.Add(mapAreaZipDestinationpathname);
                                        result &= DownloadCopyAndDeleteFile(consumer, job, mapAreaZipFilename, downloadFilenameCombined, mapAreaZipDestinationpathname);
                                        if (SIO.File.Exists(mapAreaZipDestinationpathname))
                                        {
                                            RSG.Pipeline.Services.Zip.ExtractAll(mapAreaZipDestinationpathname, "x:/", true);
                                            try
                                            {
                                                SIO.File.Delete(mapAreaZipDestinationpathname);
                                            }
                                            catch (System.Exception ex)
                                            {
                                                Log.Log__Warning("Failed to delete map area zip file containing area RPFs - {0} : {1}", mapAreaZipDestinationpathname, ex.ToString());
                                            }
                                        }
                                        else
                                        {
                                            Log.Log__Error("Map area zip filename not found locally to extract after DownloadCopyAndDeleteFile: {0}", mapAreaZipDestinationpathname);
                                            result = false;
                                        }
                                    }
                                    else
                                    {
                                        Log.Log__Warning("Map area zip filename not available on server: {0}", mapAreaZipFilename);
                                    }
                                }
                                else
                                {
                                    // Single RPF per platform e.g. v_bins.rpf or v_int_11.rpf
                                    string platformFilename = RSG.Pipeline.Services.Platform.PlatformPathConversion.ConvertAndRemapFilenameToPlatform(target, 
                                        SIO.Path.GetFullPath(((File)mapProcZipNode).AbsolutePath));
                                    // Use the prefixed rpf name with target name to find it on server and download it with 
                                    String relativeFilename = target.Platform.ToString() + SIO.Path.GetFileName(platformFilename);
                                    String downloadFilename = SIO.Path.Combine(jobDirectory, relativeFilename);

                                    if (consumer.FileAvailability(job).Contains(relativeFilename, StringComparer.OrdinalIgnoreCase))
                                    {
                                        this.Files.Add(platformFilename);
                                        result &= DownloadCopyAndDeleteFile(consumer, job, relativeFilename, downloadFilename, platformFilename);
                                    }
                                }
                            }
                        }
                    }

                    // Check for a textures and TCS zip file, if there is one then mark the files for edit/add and extract
                    String networkExportChangelistPathname = CommonFuncs._networkExportChangelistFilesZip;

                    // If the job has the tcs and textures zip, then download it
                    if (consumer.FileAvailability(job).Contains(networkExportChangelistPathname, StringComparer.OrdinalIgnoreCase))
                    {
                        String texturesAndTCSZipDownloadPathname = SIO.Path.Combine(jobDirectory, networkExportChangelistPathname);
                        String texturesAndTCSZipDestinationPathname = SIO.Path.Combine(mapCacheFolder, networkExportChangelistPathname);

                        DownloadCopyAndDeleteFile(consumer, job, networkExportChangelistPathname, texturesAndTCSZipDownloadPathname, texturesAndTCSZipDestinationPathname);

                        if (SIO.File.Exists(texturesAndTCSZipDestinationPathname))
                        {
                            try
                            {
                                IEnumerable<String> fileListNoDrive = new List<String>();
                                RSG.Pipeline.Services.Zip.GetFileList(texturesAndTCSZipDestinationPathname, out fileListNoDrive);

                                if (fileListNoDrive.Count() > 0)
                                {
                                    // Drive info isn't stored in the zip file but everything else is.  
                                    List<String> fileList = fileListNoDrive.Select(f => "x:/" + f).ToList();

                                    // TCS file list
                                    String[] tcsFiles = fileList.Where(f => SIO.Path.GetExtension(f).Equals(".tcs")).ToArray();

                                    String[] binaryPlusMFiles = fileList.Where(
                                        (f => SIO.Path.GetExtension(f).Equals(".tif") || SIO.Path.GetExtension(f).Equals(".dds") ||
                                                SIO.Path.GetExtension(f).Equals(".clip") || SIO.Path.GetExtension(f).Equals(".anim"))).ToArray();

                                    FileMapping[] tcsFileMappings = new FileMapping[0];
                                    if (tcsFiles.Any())
                                        tcsFileMappings = FileMapping.Create(p4, tcsFiles);

                                    FileMapping[] binaryPlusMFileMappings = new FileMapping[0];
                                    if (binaryPlusMFiles.Any())
                                        binaryPlusMFileMappings = FileMapping.Create(p4, binaryPlusMFiles);

                                    //Force sync the texture/TCS files to avoid any resolve issues later
                                    List<String> forceSyncArgs = new List<String>();
                                    forceSyncArgs.Add("-f");
                                    forceSyncArgs.AddRange(tcsFileMappings.Select(f => f.DepotFilename));
                                    forceSyncArgs.AddRange(binaryPlusMFileMappings.Select(f => f.DepotFilename));
                                    // > 1 due to -f flag
                                    if (forceSyncArgs.Count > 1)
                                        p4.Run("sync", forceSyncArgs.ToArray());

                                    List<String> tcsFilesToEdit = new List<String>();
                                    List<String> tcsFilesToAdd = new List<String>();
                                    List<String> binaryPlusMFilesToEdit = new List<String>();
                                    List<String> binaryPlusMFilesToAdd = new List<String>();

                                    // See if files need adding or edited.
                                    foreach (FileMapping fm in tcsFileMappings)
                                    {
                                        if (p4.Exists(fm.DepotFilename))
                                            tcsFilesToEdit.Add(fm.DepotFilename);
                                        else
                                            tcsFilesToAdd.Add(fm.ClientFilename);
                                    }
                                    foreach (FileMapping fm in binaryPlusMFileMappings)
                                    {
                                        if (p4.Exists(fm.DepotFilename))
                                            binaryPlusMFilesToEdit.Add(fm.DepotFilename);
                                        else
                                            binaryPlusMFilesToAdd.Add(fm.ClientFilename);
                                    }

                                    // Horrid but extract direct to x: drive.
                                    RSG.Pipeline.Services.Zip.ExtractAll(texturesAndTCSZipDestinationPathname, "x:/", true);

                                    if (tcsFilesToEdit.Any())
                                    {
                                        p4.Run("edit", tcsFilesToEdit.ToArray());
                                        moveToChangelistArgs.AddRange(tcsFilesToEdit);
                                    }
                                    if (tcsFilesToAdd.Any())
                                    {
                                        p4.Run("add", tcsFilesToAdd.ToArray());
                                        moveToChangelistArgs.AddRange(tcsFilesToAdd);
                                    }
                                    if (binaryPlusMFilesToEdit.Any())
                                    {
                                        List<String> textureFilesToEditArgs = new List<String> { "-t", "binary+m" };
                                        textureFilesToEditArgs.AddRange(binaryPlusMFilesToEdit);
                                        p4.Run("edit", textureFilesToEditArgs.ToArray());
                                        moveToChangelistArgs.AddRange(binaryPlusMFilesToEdit);
                                    }
                                    if (binaryPlusMFilesToAdd.Any())
                                    {
                                        List<String> textureFilesToAddArgs = new List<String> { "-t", "binary+m" };
                                        textureFilesToAddArgs.AddRange(binaryPlusMFilesToAdd);
                                        p4.Run("add", textureFilesToAddArgs.ToArray());
                                        moveToChangelistArgs.AddRange(binaryPlusMFilesToAdd);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Log.Log__Error("Exception whilst extracting texture/TCS files or creating/reverting perforce changelist: {0}", ex.ToString());
                                result = false;
                            }
                        }
                    }
                    else
                    {
                        Log.Log__Message("{0} does not exist on the server for this job", networkExportChangelistPathname);
                    }

                    // -c and the changelist number are always in moveToChangelistArgs
                    if (moveToChangelistArgs.Count > 2)
                    {
                        p4.Run("reopen", moveToChangelistArgs.ToArray());
                    }

                    // Revert unchanged - somebody might be downloading files which have
                    // been checked in by someone else so are now not needing checked in
                    String[] revertUnchangedFilesArgs = new String[] { "-a", "-c", changeList.Number.ToString() };
                    p4.Run("revert", revertUnchangedFilesArgs);

                    // Try and delete the changelist - it might be empty if unchanged files
                    // were reverted.
                    changeList.Delete();
                }
            }
            catch (Exception ex)
            {
                Log.Log__Error("Perforce exception in DownloadExportAndBuildData: {0}", ex.Message);
                result = false;
            }
            return result;
        }

        /// <summary>
        /// Attempts to Download the file from the server, then copies from the workbench cache into
        /// the correct location and removes the version stored in the cache
        /// </summary>
        /// <param name="job"></param>
        /// <param name="relFilename"></param>
        /// <param name="downFilename"></param>
        /// <param name="destFilename"></param>
        /// <returns></returns>
        private bool DownloadCopyAndDeleteFile(FileTransferServiceConsumer consumer, MapExportJob job, String relFilename, String downFilename, String destFilename)
        {
            Byte[] md5;
            bool fileResult = consumer.DownloadFile(job, relFilename, SIO.Path.GetFileName(downFilename), out md5);
            if (!fileResult)
            {
                Log.Log__Error("Error downloading export file: {0} for job {1}.  MD5: {2}.",
                    relFilename, job.ID, RSG.Base.IO.FileMD5.Format(md5));
            }
            else
            {
                try
                {
                    String destinationDirectory = System.IO.Path.GetDirectoryName(destFilename);
                    if (!System.IO.Directory.Exists(destinationDirectory))
                        System.IO.Directory.CreateDirectory(destinationDirectory);
                    SIO.File.Copy(downFilename, destFilename, true);
                    SIO.File.Delete(downFilename);
                }
                catch (Exception ex)
                {
                    Log.Log__Error("Exception in job {0} while copying or deleting file: {1} to {2}: {3}", job.ID, downFilename, destFilename, ex.Message);
                    fileResult &= false;
                }
            }
            return fileResult;
        }


        /// <summary>
        /// Finds all pending changelists for the user with the given description and deletes them if empty
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="changelistDescription"></param>
        private void DeleteEmptyPendingChangelists(RSG.SourceControl.Perforce.P4 p4, string changelistDescription)
        {
            if (changelistDescription.Equals(String.Empty))
                return;

            String[] findPendingChangelistsArgs = new String[] { "-l", "-s", "pending", "-u", p4.User };
            P4RecordSet pendingChangelistsRecord = p4.Run("changes", findPendingChangelistsArgs);

            List<String> networkExportChangelists = new List<String>();

            foreach (P4Record record in pendingChangelistsRecord.Records)
            {
                // Find any changelists that contain the changelistDescription and add to a list to
                // later check for 0 files in them.
                if (record.Fields.ContainsKey("desc") && record.Fields.ContainsKey("change"))
                {
                    string description = record.Fields["desc"].ToLower();
                    if (description.Contains(changelistDescription.ToLower()))
                    {
                        networkExportChangelists.Add(record.Fields["change"]);
                    }
                }
            }

            // Now check the matching changelists to see if there are 0 files in them, and delete if so.
            if (networkExportChangelists.Any())
            {
                P4RecordSet pendingNetworkExportChangelistRecords = p4.Run("describe", networkExportChangelists.ToArray());

                foreach (P4Record record in pendingNetworkExportChangelistRecords.Records)
                {
                    if (record.Fields.ContainsKey("change"))
                    {
                        // If no depotFiles in the record, the changelist is empty so delete
                        if (!record.ArrayFields.ContainsKey("depotFile"))
                            p4.Run("change", new String[] { "-d", record.Fields["change"] });
                    }
                }
            }
        }
#if false
        private async Task<bool> DownloadCopyAndDeleteFileAsync(FileTransferServiceConsumer consumer,
            IJob job, String remoteFilename, String downloadFilename, String destinationFilename)
        {
            Task task = Task.Factory.StartNew(delegate
            {
                Byte[] md5;
                bool fileResult = consumer.DownloadFile(job, remoteFilename, 
                    SIO.Path.GetFileName(downloadFilename), out md5);
                if (!fileResult)
                {
                    //Log.Log__Error("Error downloading export file: {0} for job {1}.  MD5: {2}.",
                      //  remoteFilename, job.ID, RSG.Base.IO.FileMD5.Format(md5));
                }
                else
                {
                    try
                    {
                        SIO.File.Copy(downloadFilename, destinationFilename, true);
                        SIO.File.Delete(downloadFilename);
                    }
                    catch (Exception ex)
                    {
                        //Log.Log__Error("Exception in job {0} while copying or deleting file: {1} to {2}: {3}", job.ID, downFilename, destFilename, ex.Message);
                        fileResult &= false;
                    }
                }
                return fileResult;
            });
        }
#endif
        #endregion // Private Methods
    }

} // RSG.Automation.ViewModel namespace
