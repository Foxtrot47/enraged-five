﻿//---------------------------------------------------------------------------------------------
// <copyright file="PresenterPart.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Presenters
{
    using System;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Represents a single part of a presenter that is used to render a control.
    /// </summary>
    public abstract class PresenterPart
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Height"/> property.
        /// </summary>
        private double? _height;

        /// <summary>
        /// The private field used for the <see cref="IsConnected"/> property.
        /// </summary>
        private bool _isConnected;

        /// <summary>
        /// The private field used for the <see cref="IsMouseOver"/> property.
        /// </summary>
        private bool _isMouseOver;

        /// <summary>
        /// The private field used for the <see cref="Left"/> property.
        /// </summary>
        private double? _left;

        /// <summary>
        /// The private field used for the <see cref="Owner"/> property.
        /// </summary>
        private CustomPresenterBase _owner;

        /// <summary>
        /// The private field used for the <see cref="Top"/> property.
        /// </summary>
        private double? _top;

        /// <summary>
        /// The private field used for the <see cref="Width"/> property.
        /// </summary>
        private double? _width;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="PresenterPart"/> class.
        /// </summary>
        /// <param name="owner">
        /// The owning <see cref="CustomPresenterBase"/> for this part.
        /// </param>
        protected PresenterPart(CustomPresenterBase owner)
        {
            this._owner = owner;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the rectangle object that defines the bounding area for this layout part.
        /// </summary>
        public virtual Rect Bounds
        {
            get { return new Rect(this.Left, this.Top, this.Width, this.Height); }
        }

        /// <summary>
        /// Gets the UIElement that represents this layout part.
        /// </summary>
        public virtual UIElement Element
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the actual height in pixels of this layout part.
        /// </summary>
        public double Height
        {
            get
            {
                if (this._height.HasValue)
                {
                    return this._height.Value;
                }

                Size size = this.Measure();
                this._width = new double?(Math.Round(size.Width));
                this._height = new double?(Math.Round(size.Height));
                return this._height.Value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this layout part is currently connected to a
        /// parent tree view presenter.
        /// </summary>
        public bool IsConnected
        {
            get { return this._isConnected; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the mouse is directly over the bounding
        /// area of this layout part.
        /// </summary>
        public bool IsMouseOver
        {
            get
            {
                return this._isMouseOver;
            }

            set
            {
                if (this._isMouseOver == value)
                {
                    return;
                }

                this._isMouseOver = value;
                this.OnIsMouseOverChanged();
            }
        }

        /// <summary>
        /// Gets the left most pixel position of this layout part.
        /// </summary>
        public double Left
        {
            get
            {
                if (this._left.HasValue)
                {
                    return this._left.Value;
                }

                Point point = this.Arrange();
                this._left = new double?(Math.Round(point.X));
                this._top = new double?(Math.Round(point.Y));
                return this._left.Value;
            }
        }

        /// <summary>
        /// Gets the top most pixel position of this layout part.
        /// </summary>
        public double Top
        {
            get
            {
                if (this._top.HasValue)
                {
                    return this._top.Value;
                }

                Point point = this.Arrange();
                this._left = new double?(Math.Round(point.X));
                this._top = new double?(Math.Round(point.Y));
                return this._top.Value;
            }
        }

        /// <summary>
        /// Gets the actual width in pixels of this layout part.
        /// </summary>
        public double Width
        {
            get
            {
                if (this._width.HasValue)
                {
                    return this._width.Value;
                }

                Size size = this.Measure();
                this._width = new double?(Math.Round(size.Width));
                this._height = new double?(Math.Round(size.Height));
                return this._width.Value;
            }
        }

        /// <summary>
        /// Gets the owning tree item presenter.
        /// </summary>
        protected CustomPresenterBase Owner
        {
            get { return this._owner; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Connects this layout part to the parent tree item presenter.
        /// </summary>
        public void Connect()
        {
            if (this._isConnected)
            {
                return;
            }

            this._isConnected = true;
            this.OnConnected();
        }

        /// <summary>
        /// Disconnects this layout part to the parent tree item presenter.
        /// </summary>
        public void Disconnect()
        {
            if (!this._isConnected)
            {
                return;
            }

            this._isConnected = false;
            this.OnDisconnected();
        }

        /// <summary>
        /// Invalidates the top and left of this layout part so that it is re-arranged.
        /// </summary>
        public void InvalidateArrange()
        {
            this._top = null;
            this._left = null;
        }

        /// <summary>
        /// Invalidates the height and width of this layout part so that it is re-measured.
        /// </summary>
        public void InvalidateMeasure()
        {
            this._height = null;
            this._width = null;
        }

        /// <summary>
        /// Override to use the specified drawing context to render this layout part is the
        /// layout part doesn't have a specific UI element.
        /// </summary>
        /// <param name="context">
        /// The drawing context to use to render this layout part.
        /// </param>
        public virtual void Render(DrawingContext context)
        {
        }

        /// <summary>
        /// Updates the tooltip placement whenever the content for the tooltip changes.
        /// </summary>
        public virtual void UpdateToolTipPlacement()
        {
        }

        /// <summary>
        /// Override this method to calculate the position of this layout part and return the
        /// top-left corner.
        /// </summary>
        /// <returns>
        /// The point that defines the left-top corner of this layout part.
        /// </returns>
        protected abstract Point Arrange();

        /// <summary>
        /// Retrieves the centre size between the actual size and the available size.
        /// </summary>
        /// <param name="availableSize">
        /// The available size to the layout part.
        /// </param>
        /// <param name="size">
        /// The actual size of the layout part.
        /// </param>
        /// <returns>
        /// The centre size between the actual size and the available size.
        /// </returns>
        protected double GetCentredEdge(double availableSize, double size)
        {
            return Math.Round((availableSize - size) / 2.0);
        }

        /// <summary>
        /// Override this method to measure the UI control that represents this layout part and
        /// return the size.
        /// </summary>
        /// <returns>
        /// The size that defines the width and height of this layout part.
        /// </returns>
        protected abstract Size Measure();

        /// <summary>
        /// Override to handle when this layout part gets connected to the parent tree view
        /// presenter.
        /// </summary>
        protected virtual void OnConnected()
        {
        }

        /// <summary>
        /// Override to handle when this layout part gets disconnected to the parent tree view
        /// presenter.
        /// </summary>
        protected virtual void OnDisconnected()
        {
        }

        /// <summary>
        /// Override to handle the situation where the <see cref="IsMouseOver"/> property
        /// changes.
        /// </summary>
        protected virtual void OnIsMouseOverChanged()
        {
        }
        #endregion Methods
    } // RSG.Editor.Controls.Presenters.PresenterPart {Class}
} // RSG.Editor.Controls.Presenters {Namespace}
