﻿//---------------------------------------------------------------------------------------------
// <copyright file="CommandBarItemViewModelCollection.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Linq;
    using RSG.Base.Extensions;
    using RSG.Editor.SharedCommands;

    /// <summary>
    /// Represents a collection of <see cref="RSG.Editor.CommandBarItemViewModel"/> objects
    /// that produces notification events when manipulated.
    /// </summary>
    public class CommandBarItemViewModelCollection
        : ObservableCollection<CommandBarItemViewModel>
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Id"/> property.
        /// </summary>
        private Guid _id;

        /// <summary>
        /// A private list of all of the invisible items currently associated with this
        /// collection.
        /// </summary>
        private List<CommandBarItem> _invisibleItems;

        /// <summary>
        /// The private field used for the <see cref="IsSealed"/> property.
        /// </summary>
        private bool _isSealed;

        /// <summary>
        /// A private list containing all multi item command objects that have been added to
        /// this collection, as well as their indices into this collection.
        /// </summary>
        private Dictionary<MultiCommand, List<CommandInstance>> _multiCommands;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CommandBarItemViewModelCollection"/>
        /// class as a wrapper for the list specified by its parent id.
        /// </summary>
        /// <param name="id">
        /// The identifier of the parent whose children are contained within this collection.
        /// </param>
        public CommandBarItemViewModelCollection(Guid id)
        {
            this._id = id;
            this._multiCommands = new Dictionary<MultiCommand, List<CommandInstance>>();
            this._invisibleItems = new List<CommandBarItem>();
            this.ResetItems(false, true);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the identifier of the parent whose children are contained within this
        /// collection.
        /// </summary>
        public Guid Id
        {
            get { return this._id; }
        }

        /// <summary>
        /// Gets a value indicating whether this collection is currently sealed from
        /// modifications.
        /// </summary>
        public bool IsSealed
        {
            get { return this._isSealed; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Determines the index after the specified item in the collection. This is to support
        /// multi items currently expanded with in the collection.
        /// </summary>
        /// <param name="item">
        /// The object to locate in the collection.
        /// </param>
        /// <returns>
        /// The index after the specified item if found in the list; otherwise, the last index
        /// in the collection.
        /// </returns>
        public int GetIndexAfter(CommandBarItem item)
        {
            int index = 0;
            foreach (CommandBarItemViewModel internalItem in this)
            {
                CommandBarItem commandBarItem = internalItem.CommandBarItem;
                MultiCommandItemInstance multi = commandBarItem as MultiCommandItemInstance;
                if (multi != null)
                {
                    commandBarItem = multi.OriginalInstance;
                }

                if (Object.ReferenceEquals(commandBarItem, item))
                {
                    if (multi == null)
                    {
                        return index + 1;
                    }

                    return index + multi.SiblingCount + 1;
                }

                index++;
            }

            return this.Count;
        }

        /// <summary>
        /// Determines the index of a specific item in the collection.
        /// </summary>
        /// <param name="item">
        /// The object to locate in the collection.
        /// </param>
        /// <returns>
        /// The index of item if found in the list; otherwise, -1.
        /// </returns>
        public int IndexOf(CommandBarItem item)
        {
            MultiCommandItemInstance instance = item as MultiCommandItemInstance;
            if (instance != null)
            {
                item = instance.OriginalInstance;
            }

            int index = 0;
            foreach (CommandBarItemViewModel internalItem in this)
            {
                CommandBarItem commandBarItem = internalItem.CommandBarItem;
                if (Object.ReferenceEquals(commandBarItem, item))
                {
                    return index;
                }

                MultiCommandItemInstance multi = commandBarItem as MultiCommandItemInstance;
                if (multi != null)
                {
                    if (Object.ReferenceEquals(multi.OriginalInstance, item))
                    {
                        return index;
                    }
                }

                index++;
            }

            return -1;
        }

        /// <summary>
        /// Sets the <see cref="IsSealed"/> property to true so that exceptions will be fired
        /// if someone tries to manipulate the items within the collection.
        /// </summary>
        public void Seal()
        {
            this._isSealed = true;
        }

        /// <summary>
        /// Adds a new separator item located at the specified index. If there is already a
        /// separator that or next to the specified index, nothing happens.
        /// </summary>
        /// <param name="index">
        /// The index the new separator should be located at.
        /// </param>
        internal void AddSeparator(int index)
        {
            CommandBarItemViewModel item = this[index];
            if (item.DisplayType == CommandItemDisplayType.Separator)
            {
                return;
            }

            if (index != 0)
            {
                item = this[index - 1];
                if (item.DisplayType == CommandItemDisplayType.Separator)
                {
                    return;
                }
            }

            if (index == this.Count)
            {
                return;
            }

            base.InsertItem(index, new CommandBarItemViewModel(null));
        }

        /// <summary>
        /// Inserts the specified item into the collection at the specified index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index at which the specified item should be inserted.
        /// </param>
        /// <param name="item">
        /// The object to insert into the collection.
        /// </param>
        internal void Insert(int index, CommandBarItem item)
        {
            this.Insert(index, new CommandBarItemViewModel(item));
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the collection.
        /// </summary>
        /// <param name="item">
        /// The object to remove from the collection.
        /// </param>
        /// <returns>
        /// True if item was successfully removed from the collection, otherwise, false. This
        /// method also returns false if item is not found in the original collection.
        /// </returns>
        internal bool Remove(CommandBarItem item)
        {
            int index = this.IndexOf(item);
            if (index == -1)
            {
                return false;
            }

            this.RemoveAt(index);
            return true;
        }

        /// <summary>
        /// Removed the separator item that is located at the specified index. If the item at
        /// the specified index isn't a separator nothing happens.
        /// </summary>
        /// <param name="index">
        /// The index of the separator to remove.
        /// </param>
        internal void RemoveSeparator(int index)
        {
            if (this.Count <= index)
            {
                return;
            }

            CommandBarItemViewModel item = this[index];
            if (item.DisplayType != CommandItemDisplayType.Separator)
            {
                return;
            }

            base.RemoveItem(index);
        }

        /// <summary>
        /// Removes all items from the collection and re adds them from the rockstar command
        /// manager.
        /// </summary>
        /// <param name="clearCache">
        /// A value indicating whether the cache of multi commands and invisible items should
        /// also be cleared.
        /// </param>
        /// <param name="sortPriorities">
        /// A value indicating whether the priorities for the items should be set in numerical
        /// order.
        /// </param>
        internal void ResetItems(bool clearCache, bool sortPriorities)
        {
            this.CheckSealedFlagBeforeModifying();
            if (clearCache)
            {
                this._multiCommands.Clear();
                this._invisibleItems.Clear();
            }

            this.Clear();
            if (sortPriorities)
            {
                var items = RockstarCommandManager.GetCommandsWithRemovals(this._id).ToList();
                if (items != null)
                {
                    HashSet<ushort> previousPriorities = new HashSet<ushort>();
                    foreach (CommandBarItem item in items)
                    {
                        if (item is CommandToolBar)
                        {
                            continue;
                        }

                        while (previousPriorities.Contains(item.Priority))
                        {
                            RockstarCommandManager.AddOriginalPriorityValue(item);
                            item.Priority++;
                        }

                        previousPriorities.Add(item.Priority);
                    }
                }
            }

            {
                var items = RockstarCommandManager.GetCommands(this._id).ToList();
                if (items != null)
                {
                    foreach (CommandBarItem item in items)
                    {
                        if (item.IsVisible)
                        {
                            this.Add(new CommandBarItemViewModel(item));
                        }
                        else
                        {
                            PropertyChangedEventManager.AddHandler(
                                item, this.OnIsVisibleChanged, "IsVisible");
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Sets the <see cref="IsSealed"/> property to false so that you can manipulate the
        /// items within the collection.
        /// </summary>
        internal void Unseal()
        {
            this._isSealed = false;
        }

        /// <summary>
        /// Removes all items from the collection.
        /// </summary>
        protected override void ClearItems()
        {
            this.CheckSealedFlagBeforeModifying();
            base.ClearItems();
        }

        /// <summary>
        /// Inserts an item into the collection at the specified index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index at which <paramref name="item" /> should be inserted.
        /// </param>
        /// <param name="item">
        /// The object to insert.
        /// </param>
        protected override void InsertItem(int index, CommandBarItemViewModel item)
        {
            if (item == null)
            {
                throw new SmartArgumentNullException(() => item);
            }

            if (!this._invisibleItems.Contains(item.CommandBarItem))
            {
                if (item.CommandBarItem.ParentId != RockstarCommandManager.ToolBarTrayId)
                {
                    PropertyChangedEventManager.AddHandler(
                        item.CommandBarItem, this.OnIsVisibleChanged, "IsVisible");
                }
            }

            this.CheckSealedFlagBeforeModifying();
            bool expandingInstance = false;
            bool startsGroup = false;
            bool eachItemStartsGroup = false;
            if (item.StartsGroup && index != 0)
            {
                startsGroup = true;
            }

            MultiCommand multiCommand = item.CommandDefinition as MultiCommand;
            CommandInstance instance = item.CommandBarItem as CommandInstance;
            if (multiCommand != null && instance != null)
            {
                List<CommandInstance> instances = null;
                if (!this._multiCommands.TryGetValue(multiCommand, out instances))
                {
                    instances = new List<CommandInstance>();
                    this._multiCommands.Add(multiCommand, instances);

                    CollectionChangedEventManager.AddHandler(
                        multiCommand.Items, this.OnMultiItemsChanged);
                }

                PropertyChangedEventManager.AddHandler(
                    instance, this.OnShowMultiItemsChanged, "AreMultiItemsShown");

                if (instance.AreMultiItemsShown)
                {
                    PropertyChangedEventManager.AddHandler(
                        instance, this.OnEachItemStartsGroupChanged, "EachItemStartsGroup");
                }

                instances.Add(instance);
                if (item.CommandBarItem.AreMultiItemsShown)
                {
                    expandingInstance = true;
                }

                if (item.CommandBarItem.EachItemStartsGroup)
                {
                    eachItemStartsGroup = true;
                }
            }

            int currentIndex = index;
            if (startsGroup)
            {
                base.InsertItem(currentIndex, new CommandBarItemViewModel(null));
                currentIndex++;
            }

            if (expandingInstance)
            {
                for (int i = 0; i < multiCommand.Items.Count; i++)
                {
                    if (eachItemStartsGroup && i != 0)
                    {
                        base.InsertItem(currentIndex, new CommandBarItemViewModel(null));
                        currentIndex++;
                    }

                    IMultiCommandItem child = multiCommand.Items[i];
                    CommandBarItemViewModel vm = new CommandBarItemViewModel(child, instance);
                    base.InsertItem(currentIndex++, vm);
                }
            }
            else
            {
                base.InsertItem(currentIndex++, item);
            }

            if (this.Count > currentIndex)
            {
                CommandBarItemViewModel nextItem = this[currentIndex];
                if (nextItem.DisplayType != CommandItemDisplayType.Separator)
                {
                    if (nextItem.StartsGroup)
                    {
                        base.InsertItem(currentIndex, new CommandBarItemViewModel(null));
                    }
                }
            }
        }

        /// <summary>
        /// Moves the item at the specified index to a new location in the collection.
        /// </summary>
        /// <param name="oldIndex">
        /// The zero-based index specifying the location of the item to be moved.
        /// </param>
        /// <param name="newIndex">
        /// The zero-based index specifying the new location of the item.
        /// </param>
        protected override void MoveItem(int oldIndex, int newIndex)
        {
            this.CheckSealedFlagBeforeModifying();
            CommandBarItemViewModel item = this[oldIndex];
            if (item.StartsGroup && oldIndex != 0)
            {
                CommandBarItemViewModel separator = this[oldIndex - 1];
                base.MoveItem(oldIndex, newIndex);
                int separatorIndex = this.IndexOf(separator);
                int newSeparatorIndex = this.IndexOf(item);
                if (newSeparatorIndex != 0)
                {
                    base.MoveItem(separatorIndex, newSeparatorIndex);
                }
                else
                {
                    base.RemoveItem(separatorIndex);
                }
            }
            else
            {
                base.MoveItem(oldIndex, newIndex);
            }
        }

        /// <summary>
        /// Removes the item at the specified index of the collection.
        /// </summary>
        /// <param name="index">
        /// The zero-based index of the element to remove.
        /// </param>
        protected override void RemoveItem(int index)
        {
            this.CheckSealedFlagBeforeModifying();
            bool expandedInstance = false;

            CommandBarItemViewModel item = this[index];
            MultiCommandItemDefinition command =
                item.CommandDefinition as MultiCommandItemDefinition;
            MultiCommandItemInstance instance =
                item.CommandBarItem as MultiCommandItemInstance;

            if (instance != null)
            {
                if (!this._invisibleItems.Contains(instance.OriginalInstance))
                {
                    PropertyChangedEventManager.RemoveHandler(
                        instance.OriginalInstance, this.OnIsVisibleChanged, "IsVisible");
                }
            }
            else
            {
                if (!this._invisibleItems.Contains(item.CommandBarItem))
                {
                    PropertyChangedEventManager.RemoveHandler(
                        item.CommandBarItem, this.OnIsVisibleChanged, "IsVisible");
                }
            }

            if (command != null && instance != null)
            {
                if (item.CommandBarItem.AreMultiItemsShown)
                {
                    CollectionChangedEventManager.RemoveHandler(
                        command.MultiCommandItems, this.OnMultiItemsChanged);

                    expandedInstance = true;
                }

                List<CommandInstance> instances = null;
                if (this._multiCommands.TryGetValue(command.OriginalDefinition, out instances))
                {
                    instances.Remove(instance.OriginalInstance);
                    if (instances.Count == 0)
                    {
                        this._multiCommands.Remove(command.OriginalDefinition);

                        CollectionChangedEventManager.AddHandler(
                            command.OriginalDefinition.Items, this.OnMultiItemsChanged);
                    }
                }

                PropertyChangedEventManager.RemoveHandler(
                    instance.OriginalInstance,
                    this.OnShowMultiItemsChanged,
                    "AreMultiItemsShown");

                PropertyChangedEventManager.RemoveHandler(
                    instance.OriginalInstance,
                    this.OnEachItemStartsGroupChanged,
                    "EachItemStartsGroup");
            }
            else
            {
                MultiCommand multiCommand = item.CommandDefinition as MultiCommand;
                CommandInstance multiInstance = item.CommandBarItem as CommandInstance;
                if (multiCommand != null && multiInstance != null)
                {
                    List<CommandInstance> instances = null;
                    if (this._multiCommands.TryGetValue(multiCommand, out instances))
                    {
                        instances.Remove(multiInstance);
                        if (instances.Count == 0)
                        {
                            this._multiCommands.Remove(multiCommand);

                            CollectionChangedEventManager.AddHandler(
                                multiCommand.Items, this.OnMultiItemsChanged);
                        }
                    }

                    PropertyChangedEventManager.RemoveHandler(
                        multiInstance,
                        this.OnShowMultiItemsChanged,
                        "AreMultiItemsShown");

                    PropertyChangedEventManager.RemoveHandler(
                        multiInstance,
                        this.OnEachItemStartsGroupChanged,
                        "EachItemStartsGroup");
                }
            }

            int numberToRemove = 1;
            if (expandedInstance)
            {
                numberToRemove += instance.SiblingCount;
                if (item.CommandBarItem.EachItemStartsGroup)
                {
                    numberToRemove += instance.SiblingCount;
                }
            }

            if (item.StartsGroup && index != 0)
            {
                numberToRemove++;
                index = index - 1;
            }

            for (int i = 0; i < numberToRemove; i++)
            {
                base.RemoveItem(index);
            }

            if (this.Count > 0 && this[0].DisplayType == CommandItemDisplayType.Separator)
            {
                base.RemoveItem(0);
            }
        }

        /// <summary>
        /// Replaces the element at the specified index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index of the element to replace.
        /// </param>
        /// <param name="item">
        /// The new value for the element at the specified index.
        /// </param>
        protected override void SetItem(int index, CommandBarItemViewModel item)
        {
            if (item == null)
            {
                throw new SmartArgumentNullException(() => item);
            }

            this.CheckSealedFlagBeforeModifying();

            CommandBarItemViewModel oldItem = this[index];
            base.SetItem(index, item);
            if (oldItem.StartsGroup != item.StartsGroup)
            {
                if (oldItem.StartsGroup && index != 0)
                {
                    CommandBarItemViewModel separator = this[index - 1];
                    base.RemoveItem(index - 1);
                    this.Remove(separator);
                }
                else
                {
                    base.InsertItem(index - 1, new CommandBarItemViewModel(null));
                }
            }
        }

        /// <summary>
        /// Throws a exception if the <see cref="IsSealed"/> flag is set to true.
        /// </summary>
        private void CheckSealedFlagBeforeModifying()
        {
            if (!this._isSealed)
            {
                return;
            }

            throw new NotSupportedException(
                "Unable to modify the collection while it is sealed.");
        }

        /// <summary>
        /// Gets the index that the specified item should be inserted within this collection.
        /// If this collection already contains the specified item -1 is returned.
        /// </summary>
        /// <param name="item">
        /// The item whose insertion index should be returned.
        /// </param>
        /// <returns>
        /// The index that the specified item should be inserted within this collection.
        /// </returns>
        private int IndexToPlaceItem(CommandBarItem item)
        {
            if (item.ParentId != this._id)
            {
                return -1;
            }

            int existingIndex = this.IndexOf(item);
            if (existingIndex != -1)
            {
                return -1;
            }

            List<CommandBarItem> items = RockstarCommandManager.GetCommands(this._id).ToList();
            int index = 0;
            for (int i = 0; i < items.Count; i++)
            {
                CommandBarItem currentItem = items[i];
                if (currentItem == item)
                {
                    break;
                }

                index = this.IndexOf(currentItem) + 1;
                CommandInstance instance = currentItem as CommandInstance;
                if (instance == null)
                {
                    continue;
                }

                System.Windows.Input.ICommand cmd = instance.Command;
                CommandDefinition definition = RockstarCommandManager.GetDefinition(cmd);
                MultiCommand multiCommand = definition as MultiCommand;
                if (multiCommand != null && currentItem.AreMultiItemsShown)
                {
                    int count = multiCommand.Items.Count - 1;
                    if (instance.EachItemStartsGroup)
                    {
                        count *= 2;
                    }

                    index += count;
                }
            }

            return index;
        }

        /// <summary>
        /// Gets called whenever the value indicating whether a instance shows its multi items
        /// with separators or not changes.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.ComponentModel.PropertyChangedEventArgs data used for the event.
        /// </param>
        private void OnEachItemStartsGroupChanged(object s, PropertyChangedEventArgs e)
        {
            CommandInstance instance = s as CommandInstance;
            if (instance == null)
            {
                return;
            }

            MultiCommand multiCommand =
                RockstarCommandManager.GetDefinition(instance.Command) as MultiCommand;
            if (multiCommand == null)
            {
                return;
            }

            if (instance.EachItemStartsGroup)
            {
                int index = this.IndexOf(instance);
                if (index == -1)
                {
                    return;
                }

                CommandBarItemViewModel item = this[index];
                if (item == null || !item.CommandBarItem.AreMultiItemsShown)
                {
                    return;
                }

                MultiCommandItemDefinition command =
                    item.CommandDefinition as MultiCommandItemDefinition;
                MultiCommandItemInstance instance1 =
                    item.CommandBarItem as MultiCommandItemInstance;

                int lastIndex = index + instance1.SiblingCount;
                for (int i = lastIndex; i > index; i--)
                {
                    base.InsertItem(i, new CommandBarItemViewModel(null));
                }
            }
            else
            {
                int index = this.IndexOf(instance);
                if (index == -1)
                {
                    return;
                }

                CommandBarItemViewModel item = this[index];
                if (item == null || !item.CommandBarItem.AreMultiItemsShown)
                {
                    return;
                }

                MultiCommandItemDefinition command =
                    item.CommandDefinition as MultiCommandItemDefinition;
                MultiCommandItemInstance instance1 =
                    item.CommandBarItem as MultiCommandItemInstance;

                int lastIndex = index + (instance1.SiblingCount * 2);
                this.Unseal();
                for (int i = lastIndex - 1; i > index; i -= 2)
                {
                    CommandBarItemViewModel viewModel = this[i];
                    if (viewModel.DisplayType != CommandItemDisplayType.Separator)
                    {
                        continue;
                    }

                    this.RemoveAt(i);
                }

                this.Seal();
            }
        }

        /// <summary>
        /// Gets called whenever the value indicating whether a instance is shown to user or
        /// not changes.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.ComponentModel.PropertyChangedEventArgs data used for the event.
        /// </param>
        private void OnIsVisibleChanged(object s, PropertyChangedEventArgs e)
        {
            CommandBarItem item = s as CommandBarItem;
            if (item == null)
            {
                return;
            }

            if (item.IsVisible)
            {
                int index = this.IndexToPlaceItem(item);
                if (index == -1)
                {
                    return;
                }

                this.Unseal();
                this.Insert(index, item);
                this.Seal();

                this._invisibleItems.Remove(item);
            }
            else
            {
                int index = this.IndexOf(item);
                if (index == -1)
                {
                    return;
                }

                this._invisibleItems.Add(item);

                this.Unseal();
                this.Remove(item);
                this.Seal();
            }
        }

        /// <summary>
        /// Called whenever items are added to a multi command items collection.
        /// </summary>
        /// <param name="instances">
        /// The list of instances of the multi command that have been affected.
        /// </param>
        /// <param name="newItems">
        /// The list containing the items that have been added to the multi command.
        /// </param>
        /// <param name="startingIndex">
        /// The index of the first new item in the multi command items.
        /// </param>
        private void OnMultiItemsAdded(
            List<CommandInstance> instances, IList newItems, int startingIndex)
        {
            if (newItems == null || instances == null || instances.Count == 0)
            {
                return;
            }

            foreach (CommandInstance instance in instances)
            {
                int index = this.IndexOf(instance);
                bool addSeparator = false;
                if (index == -1)
                {
                    index = this.IndexToPlaceItem(instance);
                    if (index != 0 && instance.StartsGroup)
                    {
                        addSeparator = true;
                    }
                }

                index += startingIndex;
                if (addSeparator)
                {
                    base.InsertItem(index++, new CommandBarItemViewModel(null));
                }

                foreach (object newItem in newItems)
                {
                    IMultiCommandItem item = newItem as IMultiCommandItem;
                    if (item == null)
                    {
                        continue;
                    }

                    base.InsertItem(index++, new CommandBarItemViewModel(item, instance));
                }

                if (index >= this.Count)
                {
                    return;
                }

                CommandBarItem nextItem = this[index].CommandBarItem;
                if (nextItem != null && nextItem.StartsGroup)
                {
                    this.AddSeparator(index);
                }
            }
        }

        /// <summary>
        /// Gets called whenever the items collection is changed.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.ComponentModel.CollectionChangeEventArgs data for the event.
        /// </param>
        private void OnMultiItemsChanged(object s, NotifyCollectionChangedEventArgs e)
        {
            List<CommandInstance> instances = (from kvp in this._multiCommands
                                               where kvp.Key.Items == s
                                               from i in kvp.Value
                                               where i.AreMultiItemsShown == true
                                               select i).ToList();

            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                this.OnMultiItemsRemoved(instances, e.OldItems.Count, e.OldStartingIndex);
                return;
            }

            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                this.OnMultiItemsAdded(instances, e.NewItems, e.NewStartingIndex);
                return;
            }

            if (e.Action == NotifyCollectionChangedAction.Reset)
            {
                this.OnMultiItemsRemoved(instances, -1, 0);
                return;
            }

            if (e.Action == NotifyCollectionChangedAction.Move)
            {
                this.OnMultiItemsMoved(instances, e.OldStartingIndex, e.NewStartingIndex);
                return;
            }

            if (e.Action == NotifyCollectionChangedAction.Replace)
            {
                this.OnMultiItemsReplaced(instances, e.NewItems, e.NewStartingIndex);
                return;
            }
        }

        /// <summary>
        /// Called whenever items are moved inside a multi command items collection.
        /// </summary>
        /// <param name="instances">
        /// The list of instances of the multi command that have been affected.
        /// </param>
        /// <param name="oldIndex">
        /// The index of the item before it was moved.
        /// </param>
        /// <param name="newIndex">
        /// The new index of the item moved.
        /// </param>
        private void OnMultiItemsMoved(
            List<CommandInstance> instances, int oldIndex, int newIndex)
        {
            if (instances == null || instances.Count == 0)
            {
                return;
            }

            foreach (CommandInstance instance in instances)
            {
                int baseIndex = this.IndexOf(instance);
                if (baseIndex == -1)
                {
                    continue;
                }

                base.MoveItem(baseIndex + oldIndex, baseIndex + newIndex);
            }
        }

        /// <summary>
        /// Called whenever items are removed to a multi command items collection.
        /// </summary>
        /// <param name="instances">
        /// The list of instances of the multi command that have been affected.
        /// </param>
        /// <param name="count">
        /// The number of items that have been removed. A negative number represents all of the
        /// items from the specified index.
        /// </param>
        /// <param name="startingIndex">
        /// The index of the first old item in the multi command items.
        /// </param>
        private void OnMultiItemsRemoved(
            List<CommandInstance> instances, int count, int startingIndex)
        {
            if (instances == null || instances.Count == 0 || count == 0)
            {
                return;
            }

            foreach (CommandInstance instance in instances)
            {
                int index = this.IndexOf(instance);
                if (index == -1)
                {
                    continue;
                }

                CommandBarItemViewModel item = this[index];
                CommandBarItem barItem = item.CommandBarItem;
                MultiCommandItemInstance multiInstance = barItem as MultiCommandItemInstance;
                CommandBarItem originalInstance = multiInstance.OriginalInstance;
                int totalCount = 0;
                while (true)
                {
                    int currentIndex = index + totalCount;
                    if (currentIndex > this.Count - 1)
                    {
                        break;
                    }

                    CommandBarItemViewModel nextItem = this[currentIndex];
                    if (item == null)
                    {
                        break;
                    }

                    multiInstance = nextItem.CommandBarItem as MultiCommandItemInstance;
                    if (multiInstance == null)
                    {
                        break;
                    }

                    if (multiInstance.OriginalInstance != originalInstance)
                    {
                        break;
                    }

                    totalCount++;
                }

                if (count < 0)
                {
                    count = totalCount - startingIndex;
                }

                index += startingIndex;
                for (int i = 0; i < count; i++)
                {
                    base.RemoveItem(index);
                }

                if (count == totalCount)
                {
                    if (instance.StartsGroup && index != 0)
                    {
                        this.RemoveSeparator(index - 1);
                    }
                }

                this.RemoveSeparator(0);
            }
        }

        /// <summary>
        /// Called whenever items are replaced inside a multi command items collection.
        /// </summary>
        /// <param name="instances">
        /// The list of instances of the multi command that have been affected.
        /// </param>
        /// <param name="items">
        /// The list containing the items that have replaced the items located at the specified
        /// starting index.
        /// </param>
        /// <param name="startingIndex">
        /// The index of the replaced item in the multi command items.
        /// </param>
        private void OnMultiItemsReplaced(
            List<CommandInstance> instances, IList items, int startingIndex)
        {
            if (instances == null || instances.Count == 0)
            {
                return;
            }

            foreach (CommandInstance instance in instances)
            {
                int index = this.IndexOf(instance);
                if (index == -1)
                {
                    continue;
                }

                index += startingIndex;
                foreach (object newItem in items)
                {
                    IMultiCommandItem item = newItem as IMultiCommandItem;
                    if (item == null)
                    {
                        continue;
                    }

                    base.SetItem(index++, new CommandBarItemViewModel(item, instance));
                }
            }
        }

        /// <summary>
        /// Gets called whenever the value indicating whether a instance shows its multi items
        /// or not changes.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.ComponentModel.PropertyChangedEventArgs data used for the event.
        /// </param>
        private void OnShowMultiItemsChanged(object s, PropertyChangedEventArgs e)
        {
            CommandInstance instance = s as CommandInstance;
            if (instance == null)
            {
                return;
            }

            if (instance.AreMultiItemsShown)
            {
                PropertyChangedEventManager.RemoveHandler(
                    instance,
                    this.OnEachItemStartsGroupChanged,
                    "EachItemStartsGroup");
            }

            PropertyChangedEventManager.RemoveHandler(
                instance,
                this.OnShowMultiItemsChanged,
                "AreMultiItemsShown");

            if (instance.AreMultiItemsShown)
            {
                int index = this.IndexOf(instance);
                if (index == -1)
                {
                    return;
                }

                this.Unseal();
                this.RemoveAt(index);
                if (instance.StartsGroup && index != 0)
                {
                    index--;
                }

                this.Insert(index, instance);
                this.Seal();
            }
            else
            {
                int index = this.IndexOf(instance);
                if (index == -1)
                {
                    return;
                }

                CommandBarItemViewModel item = this[index];
                if (item == null)
                {
                    return;
                }

                MultiCommandItemInstance multiInstance =
                    item.CommandBarItem as MultiCommandItemInstance;
                if (multiInstance == null)
                {
                    return;
                }

                int numberToRemove = 1;
                numberToRemove += multiInstance.SiblingCount;
                if (item.StartsGroup && index != 0)
                {
                    numberToRemove++;
                    index = index - 1;
                }

                if (instance.EachItemStartsGroup)
                {
                    numberToRemove += multiInstance.SiblingCount;
                }

                for (int i = 0; i < numberToRemove; i++)
                {
                    base.RemoveItem(index);
                }

                if (this.Count > 0 && this[0].DisplayType == CommandItemDisplayType.Separator)
                {
                    base.RemoveItem(0);
                }

                this.Unseal();
                this.Insert(index, instance);
                this.Seal();
            }
        }
        #endregion Methods
    } // RSG.Editor.CommandBarItemViewModelCollection {Class}
} // RSG.Editor {Namespace}
