﻿//---------------------------------------------------------------------------------------------
// <copyright file="MetadataCommandIds.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Commands
{
    using System;

    /// <summary>
    /// Contains static identifiers that can be used to extend the context menu for the project
    /// tree view.
    /// </summary>
    public static class MetadataCommandIds
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="ArrayContextMenu"/> property.
        /// </summary>
        private static Guid? _arrayContextMenu;

        /// <summary>
        /// The private field used for the <see cref="ArrayItemContextMenu"/> property.
        /// </summary>
        private static Guid? _arrayItemContextMenu;

        /// <summary>
        /// The private field used for the <see cref="MapContextMenu"/> property.
        /// </summary>
        private static Guid? _mapContextMenu;

        /// <summary>
        /// The private field used for the <see cref="PointerArrayItemContextMenu"/> property.
        /// </summary>
        private static Guid? _pointerArrayItemContextMenu;

        /// <summary>
        /// The private field used for the <see cref="PointerContextMenu"/> property.
        /// </summary>
        private static Guid? _pointerContextMenu;

        /// <summary>
        /// The private field used for the <see cref="StructureItemContextMenu"/> property.
        /// </summary>
        private static Guid? _structureItemContextMenu;

        /// <summary>
        /// The private field used for the <see cref="TreeListViewItemContextMenu"/> property.
        /// </summary>
        private static Guid? _treeListViewItemContextMenu;

        /// <summary>
        /// A private instance to a generic object used to support thread access.
        /// </summary>
        private static object _syncRoot = new object();
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets the global identifier used for the context menu for a metadata item that
        /// represents an array member.
        /// </summary>
        public static Guid ArrayContextMenu
        {
            get
            {
                if (!_arrayContextMenu.HasValue)
                {
                    lock (_syncRoot)
                    {
                        if (!_arrayContextMenu.HasValue)
                        {
                            _arrayContextMenu =
                                new Guid("5A3C944F-FC65-4DC1-896D-02601DFF874E");
                        }
                    }
                }

                return _arrayContextMenu.Value;
            }
        }

        /// <summary>
        /// Gets the global identifier used for the context menu for a metadata item inside
        /// a tunable array.
        /// </summary>
        public static Guid ArrayItemContextMenu
        {
            get
            {
                if (!_arrayItemContextMenu.HasValue)
                {
                    lock (_syncRoot)
                    {
                        if (!_arrayItemContextMenu.HasValue)
                        {
                            _arrayItemContextMenu =
                                new Guid("D7138C4D-E3F5-4B25-BC54-7F0926C8E240");
                        }
                    }
                }

                return _arrayItemContextMenu.Value;
            }
        }

        /// <summary>
        /// Gets the global identifier used for the context menu for a metadata item that
        /// represents a map member.
        /// </summary>
        public static Guid MapContextMenu
        {
            get
            {
                if (!_mapContextMenu.HasValue)
                {
                    lock (_syncRoot)
                    {
                        if (!_mapContextMenu.HasValue)
                        {
                            _mapContextMenu =
                                new Guid("DC527FC4-A589-4DC4-83B4-1F8DC3F60E36");
                        }
                    }
                }

                return _mapContextMenu.Value;
            }
        }

        /// <summary>
        /// Gets the global identifier used for the context menu for a metadata item that
        /// represents a pointer member that is inside an array.
        /// </summary>
        public static Guid PointerArrayItemContextMenu
        {
            get
            {
                if (!_pointerArrayItemContextMenu.HasValue)
                {
                    lock (_syncRoot)
                    {
                        if (!_pointerArrayItemContextMenu.HasValue)
                        {
                            _pointerArrayItemContextMenu =
                                new Guid("452AC168-FCC1-435E-90A6-CD0539DAF67E");
                        }
                    }
                }

                return _pointerArrayItemContextMenu.Value;
            }
        }

        /// <summary>
        /// Gets the global identifier used for the context menu for a metadata item that
        /// represents a pointer member.
        /// </summary>
        public static Guid PointerContextMenu
        {
            get
            {
                if (!_pointerContextMenu.HasValue)
                {
                    lock (_syncRoot)
                    {
                        if (!_pointerContextMenu.HasValue)
                        {
                            _pointerContextMenu =
                                new Guid("1C980388-09AE-4BB3-B260-5BE2F64EAA8E");
                        }
                    }
                }

                return _pointerContextMenu.Value;
            }
        }

        /// <summary>
        /// Gets the global identifier used for the context menu for a metadata item inside
        /// a tunable structure or a struct tunable.
        /// </summary>
        public static Guid StructureItemContextMenu
        {
            get
            {
                if (!_structureItemContextMenu.HasValue)
                {
                    lock (_syncRoot)
                    {
                        if (!_structureItemContextMenu.HasValue)
                        {
                            _structureItemContextMenu =
                                new Guid("0CC1761A-137E-4DE8-81BC-F5B9100AF219");
                        }
                    }
                }

                return _structureItemContextMenu.Value;
            }
        }

        /// <summary>
        /// Gets the global identifier used for the context menu for a metadata item positioned
        /// on the tree list view control. All items share the same context menu to help the
        /// user understand the commands available, and promote muscle reflex.
        /// </summary>
        public static Guid TreeListViewItemContextMenu
        {
            get
            {
                if (!_treeListViewItemContextMenu.HasValue)
                {
                    lock (_syncRoot)
                    {
                        if (!_treeListViewItemContextMenu.HasValue)
                        {
                            _treeListViewItemContextMenu =
                                new Guid("5BAECC38-C0DD-4285-8CED-FFA3040DF61F");
                        }
                    }
                }

                return _treeListViewItemContextMenu.Value;
            }
        }
        #endregion Properties
    } // RSG.Metadata.Commands.MetadataCommandIds {Class}
} // RSG.Metadata.Commands {Namespace}
