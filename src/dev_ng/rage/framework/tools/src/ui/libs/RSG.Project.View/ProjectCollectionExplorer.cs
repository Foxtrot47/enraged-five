﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProjectCollectionExplorer.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.View
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Input;
    using RSG.Base.Extensions;
    using RSG.Editor;
    using RSG.Editor.Controls;
    using RSG.Editor.Controls.Helpers;
    using RSG.Editor.Controls.Perforce;
    using RSG.Editor.SharedCommands;
    using RSG.Editor.View;
    using RSG.Interop.Microsoft.Windows;
    using RSG.Project.Commands;
    using RSG.Project.Model;
    using RSG.Project.ViewModel;

    /// <summary>
    /// A control used to view the hierarchy for a collection of projects.
    /// </summary>
    public class ProjectCollectionExplorer : Control
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="CollectionLoaded"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty CollectionLoadedProperty;

        /// <summary>
        /// Identifies the <see cref="ProjectCollection" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty ProjectCollectionProperty;

        /// <summary>
        /// Identifies the <see cref="RootItemsSource"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty RootItemsSourceProperty;

        /// <summary>
        /// Identifies the <see cref="SelectedItems"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty SelectedItemsProperty;

        /// <summary>
        /// Identifies the <see cref="CollectionLoaded"/> dependency property.
        /// </summary>
        private static readonly DependencyPropertyKey _collectionLoadedPropertyKey;

        /// <summary>
        /// Identifies the <see cref="RootItemsSource"/> dependency property.
        /// </summary>
        private static readonly DependencyPropertyKey _rootItemsSourcePropertyKey;

        /// <summary>
        /// Identifies the <see cref="SelectedItems"/> dependency property.
        /// </summary>
        private static readonly DependencyPropertyKey _selectedItemsPropertyKey;

        /// <summary>
        /// The private reference to the tree view that has been defined in this controls
        /// control template.
        /// </summary>
        private RsRenameTreeView _treeView;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="ProjectCollectionExplorer" /> class.
        /// </summary>
        static ProjectCollectionExplorer()
        {
            _rootItemsSourcePropertyKey =
                DependencyProperty.RegisterReadOnly(
                "RootItemsSource",
                typeof(IEnumerable),
                typeof(ProjectCollectionExplorer),
                new FrameworkPropertyMetadata(null));

            RootItemsSourceProperty = _rootItemsSourcePropertyKey.DependencyProperty;

            _selectedItemsPropertyKey =
                DependencyProperty.RegisterReadOnly(
                "SelectedItems",
                typeof(IEnumerable<IHierarchyNode>),
                typeof(ProjectCollectionExplorer),
                new FrameworkPropertyMetadata(null));

            SelectedItemsProperty = _selectedItemsPropertyKey.DependencyProperty;

            _collectionLoadedPropertyKey =
                DependencyProperty.RegisterReadOnly(
                "CollectionLoaded",
                typeof(bool),
                typeof(ProjectCollectionExplorer),
                new FrameworkPropertyMetadata(false));

            CollectionLoadedProperty = _collectionLoadedPropertyKey.DependencyProperty;

            ProjectCollectionProperty =
                DependencyProperty.Register(
                "ProjectCollection",
                typeof(ProjectCollectionNode),
                typeof(ProjectCollectionExplorer),
                new PropertyMetadata(null, OnCollectionChanged));

            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(ProjectCollectionExplorer),
                new FrameworkPropertyMetadata(typeof(ProjectCollectionExplorer)));

            EventManager.RegisterClassHandler(
                typeof(RsVirtualisedTreeViewItem),
                RsVirtualisedTreeViewItem.DragStartedEvent,
                new RoutedEventHandler(OnDragStarted));

            EventManager.RegisterClassHandler(
                typeof(RsVirtualisedTreeViewItem),
                UIElement.DragEnterEvent,
                new RoutedEventHandler(OnDragEnter));

            EventManager.RegisterClassHandler(
                typeof(RsVirtualisedTreeViewItem),
                UIElement.DragLeaveEvent,
                new RoutedEventHandler(OnDragLeave));

            EventManager.RegisterClassHandler(
                typeof(RsVirtualisedTreeViewItem),
                UIElement.DragOverEvent,
                new RoutedEventHandler(OnDragOver));

            EventManager.RegisterClassHandler(
                typeof(RsVirtualisedTreeViewItem),
                UIElement.DropEvent,
                new RoutedEventHandler(OnDragDrop));

        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ProjectCollectionExplorer" /> class.
        /// </summary>
        public ProjectCollectionExplorer()
        {
            this.AttachCommandBindings();
            this.AllowDrop = true;

            RockstarCommandManager.AddBinding(
                this,
                PerforceCommands.Refresh,
                this.ExecutePerforceRefreshCmd,
                this.CanExecutePerforceCmd);

            RockstarCommandManager.AddBinding(
                this,
                PerforceCommands.Edit,
                this.ExecutePerforceCheckoutCmd,
                this.CanExecutePerforceCmd);

            RockstarCommandManager.AddBinding(
                this,
                PerforceCommands.Revert,
                this.ExecutePerforceRevertCmd,
                this.CanExecutePerforceCmd);

            RockstarCommandManager.AddBinding(
                this,
                PerforceCommands.RevertIfUnchanged,
                this.ExecutePerforceRevertIfUnchangedCmd,
                this.CanExecutePerforceCmd);

            RockstarCommandManager.AddBinding(
                this,
                PerforceCommands.GetLatestRevision,
                this.ExecutePerforceGetLatestRevisionCmd,
                this.CanExecutePerforceCmd);

            RockstarCommandManager.AddBinding(
                this,
                PerforceCommands.Add,
                this.ExecutePerforceAddCmd,
                this.CanExecutePerforceCmd);
        }

        /// <summary>
        /// Determines whether a perforce command can be executed on this control.
        /// </summary>
        /// <param name="data">
        /// The can execute data from the command pipeline.
        /// </param>
        /// <returns>
        /// True if a perforce command can be executed; otherwise, false.
        /// </returns>
        private bool CanExecutePerforceCmd(CanExecuteCommandData data)
        {
            if (this.ProjectCollection.PerforceService == null)
            {
                return false;
            }

            if (!this._treeView.RealSelectedItems.Any())
            {
                return false;
            }

            foreach (object selected in this._treeView.RealSelectedItems)
            {
                if (selected is FileNode)
                {
                    return true;
                }
                else if (selected is ProjectCollectionNode)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Executes the perforce refresh command on this control.
        /// </summary>
        /// <param name="data">
        /// The execute data from the command pipeline.
        /// </param>
        private void ExecutePerforceRefreshCmd(ExecuteCommandData data)
        {
            IPerforceService service = this.ProjectCollection.PerforceService;
            if (service == null)
            {
                return;
            }

            List<string> filenames = new List<string>();
            foreach (object selected in this._treeView.RealSelectedItems)
            {
                FileNode fileNode = selected as FileNode;
                if (fileNode != null)
                {
                    filenames.Add(fileNode.FullPath);
                }
                else
                {
                    ProjectCollectionNode collectionNode = selected as ProjectCollectionNode;
                    if (collectionNode != null)
                    {
                        filenames.Add(collectionNode.FullPath);
                    }
                }
            }

            if (filenames.Count == 0)
            {
                return;
            }

            var fileMetadataList = service.GetFileMetaData(filenames);
            StringComparison comparisonType = StringComparison.OrdinalIgnoreCase;
            foreach (object selected in this._treeView.RealSelectedItems)
            {
                string filePath = null;
                FileNode fileNode = selected as FileNode;
                ProjectCollectionNode collectionNode = null;
                if (fileNode != null)
                {
                    filePath = fileNode.FullPath;
                }
                else
                {
                    collectionNode = selected as ProjectCollectionNode;
                    if (collectionNode != null)
                    {
                        filePath = collectionNode.FullPath;
                    }
                }

                IPerforceFileMetaData metadata = null;
                if (fileMetadataList != null)
                {
                    foreach (IPerforceFileMetaData fileMetadata in fileMetadataList)
                    {
                        string localPath = fileMetadata.LocalPath;
                        if (String.Equals(localPath, filePath, comparisonType))
                        {
                            metadata = fileMetadata;
                            break;
                        }
                    }
                }

                if (fileNode != null)
                {
                    fileNode.UpdatePerforceState(metadata);
                }
                else if (collectionNode != null)
                {
                    collectionNode.UpdatePerforceState(metadata);
                }
            }
        }

        /// <summary>
        /// Executes the perforce refresh command on this control.
        /// </summary>
        /// <param name="data">
        /// The execute data from the command pipeline.
        /// </param>
        private void ExecutePerforceCheckoutCmd(ExecuteCommandData data)
        {
            IPerforceService service = this.ProjectCollection.PerforceService;
            if (service == null)
            {
                return;
            }

            List<string> filenames = new List<string>();
            foreach (object selected in this._treeView.RealSelectedItems)
            {
                FileNode fileNode = selected as FileNode;
                if (fileNode != null)
                {
                    filenames.Add(fileNode.FullPath);
                }
                else
                {
                    ProjectCollectionNode collectionNode = selected as ProjectCollectionNode;
                    if (collectionNode != null)
                    {
                        filenames.Add(collectionNode.FullPath);
                    }
                }
            }

            if (filenames.Count == 0)
            {
                return;
            }

            service.EditFiles(filenames);
            var fileMetadataList = service.GetFileMetaData(filenames);
            StringComparison comparisonType = StringComparison.OrdinalIgnoreCase;
            foreach (object selected in this._treeView.RealSelectedItems)
            {
                string filePath = null;
                FileNode fileNode = selected as FileNode;
                ProjectCollectionNode collectionNode = null;
                if (fileNode != null)
                {
                    filePath = fileNode.FullPath;
                }
                else
                {
                    collectionNode = selected as ProjectCollectionNode;
                    if (collectionNode != null)
                    {
                        filePath = collectionNode.FullPath;
                    }
                }

                IPerforceFileMetaData metadata = null;
                if (fileMetadataList != null)
                {
                    foreach (IPerforceFileMetaData fileMetadata in fileMetadataList)
                    {
                        string localPath = fileMetadata.LocalPath;
                        if (String.Equals(localPath, filePath, comparisonType))
                        {
                            metadata = fileMetadata;
                            break;
                        }
                    }
                }

                if (fileNode != null)
                {
                    fileNode.UpdatePerforceState(metadata);
                }
                else if (collectionNode != null)
                {
                    collectionNode.UpdatePerforceState(metadata);
                }
            }
        }

        /// <summary>
        /// Executes the perforce refresh command on this control.
        /// </summary>
        /// <param name="data">
        /// The execute data from the command pipeline.
        /// </param>
        private void ExecutePerforceAddCmd(ExecuteCommandData data)
        {
            IPerforceService service = this.ProjectCollection.PerforceService;
            if (service == null)
            {
                return;
            }

            List<string> filenames = new List<string>();
            foreach (object selected in this._treeView.RealSelectedItems)
            {
                FileNode fileNode = selected as FileNode;
                if (fileNode != null)
                {
                    filenames.Add(fileNode.FullPath);
                }
                else
                {
                    ProjectCollectionNode collectionNode = selected as ProjectCollectionNode;
                    if (collectionNode != null)
                    {
                        filenames.Add(collectionNode.FullPath);
                    }
                }
            }

            if (filenames.Count == 0)
            {
                return;
            }

            service.AddFiles(filenames);
            var fileMetadataList = service.GetFileMetaData(filenames);
            StringComparison comparisonType = StringComparison.OrdinalIgnoreCase;
            foreach (object selected in this._treeView.RealSelectedItems)
            {
                string filePath = null;
                FileNode fileNode = selected as FileNode;
                ProjectCollectionNode collectionNode = null;
                if (fileNode != null)
                {
                    filePath = fileNode.FullPath;
                }
                else
                {
                    collectionNode = selected as ProjectCollectionNode;
                    if (collectionNode != null)
                    {
                        filePath = collectionNode.FullPath;
                    }
                }

                IPerforceFileMetaData metadata = null;
                if (fileMetadataList != null)
                {
                    foreach (IPerforceFileMetaData fileMetadata in fileMetadataList)
                    {
                        string localPath = fileMetadata.LocalPath;
                        if (String.Equals(localPath, filePath, comparisonType))
                        {
                            metadata = fileMetadata;
                            break;
                        }
                    }
                }

                if (fileNode != null)
                {
                    fileNode.UpdatePerforceState(metadata);
                }
                else if (collectionNode != null)
                {
                    collectionNode.UpdatePerforceState(metadata);
                }
            }
        }

        /// <summary>
        /// Executes the perforce refresh command on this control.
        /// </summary>
        /// <param name="data">
        /// The execute data from the command pipeline.
        /// </param>
        private void ExecutePerforceRevertCmd(ExecuteCommandData data)
        {
            IPerforceService service = this.ProjectCollection.PerforceService;
            if (service == null)
            {
                return;
            }

            List<string> filenames = new List<string>();
            foreach (object selected in this._treeView.RealSelectedItems)
            {
                FileNode fileNode = selected as FileNode;
                if (fileNode != null)
                {
                    filenames.Add(fileNode.FullPath);
                }
                else
                {
                    ProjectCollectionNode collectionNode = selected as ProjectCollectionNode;
                    if (collectionNode != null)
                    {
                        filenames.Add(collectionNode.FullPath);
                    }
                }
            }

            if (filenames.Count == 0)
            {
                return;
            }

            service.RevertFiles(filenames);
            var fileMetadataList = service.GetFileMetaData(filenames);
            StringComparison comparisonType = StringComparison.OrdinalIgnoreCase;
            foreach (object selected in this._treeView.RealSelectedItems)
            {
                string filePath = null;
                FileNode fileNode = selected as FileNode;
                ProjectCollectionNode collectionNode = null;
                if (fileNode != null)
                {
                    filePath = fileNode.FullPath;
                }
                else
                {
                    collectionNode = selected as ProjectCollectionNode;
                    if (collectionNode != null)
                    {
                        filePath = collectionNode.FullPath;
                    }
                }

                IPerforceFileMetaData metadata = null;
                if (fileMetadataList != null)
                {
                    foreach (IPerforceFileMetaData fileMetadata in fileMetadataList)
                    {
                        string localPath = fileMetadata.LocalPath;
                        if (String.Equals(localPath, filePath, comparisonType))
                        {
                            metadata = fileMetadata;
                            break;
                        }
                    }
                }

                if (fileNode != null)
                {
                    fileNode.UpdatePerforceState(metadata);
                }
                else if (collectionNode != null)
                {
                    collectionNode.UpdatePerforceState(metadata);
                }
            }
        }

        /// <summary>
        /// Executes the perforce refresh command on this control.
        /// </summary>
        /// <param name="data">
        /// The execute data from the command pipeline.
        /// </param>
        private void ExecutePerforceRevertIfUnchangedCmd(ExecuteCommandData data)
        {
            IPerforceService service = this.ProjectCollection.PerforceService;
            if (service == null)
            {
                return;
            }

            List<string> filenames = new List<string>();
            foreach (object selected in this._treeView.RealSelectedItems)
            {
                FileNode fileNode = selected as FileNode;
                if (fileNode != null)
                {
                    filenames.Add(fileNode.FullPath);
                }
                else
                {
                    ProjectCollectionNode collectionNode = selected as ProjectCollectionNode;
                    if (collectionNode != null)
                    {
                        filenames.Add(collectionNode.FullPath);
                    }
                }
            }

            if (filenames.Count == 0)
            {
                return;
            }

            service.RevertUnchangedFiles(filenames);
            var fileMetadataList = service.GetFileMetaData(filenames);
            StringComparison comparisonType = StringComparison.OrdinalIgnoreCase;
            foreach (object selected in this._treeView.RealSelectedItems)
            {
                string filePath = null;
                FileNode fileNode = selected as FileNode;
                ProjectCollectionNode collectionNode = null;
                if (fileNode != null)
                {
                    filePath = fileNode.FullPath;
                }
                else
                {
                    collectionNode = selected as ProjectCollectionNode;
                    if (collectionNode != null)
                    {
                        filePath = collectionNode.FullPath;
                    }
                }

                IPerforceFileMetaData metadata = null;
                if (fileMetadataList != null)
                {
                    foreach (IPerforceFileMetaData fileMetadata in fileMetadataList)
                    {
                        string localPath = fileMetadata.LocalPath;
                        if (String.Equals(localPath, filePath, comparisonType))
                        {
                            metadata = fileMetadata;
                            break;
                        }
                    }
                }

                if (fileNode != null)
                {
                    fileNode.UpdatePerforceState(metadata);
                }
                else if (collectionNode != null)
                {
                    collectionNode.UpdatePerforceState(metadata);
                }
            }
        }

        /// <summary>
        /// Executes the perforce refresh command on this control.
        /// </summary>
        /// <param name="data">
        /// The execute data from the command pipeline.
        /// </param>
        private void ExecutePerforceGetLatestRevisionCmd(ExecuteCommandData data)
        {
            IPerforceService service = this.ProjectCollection.PerforceService;
            if (service == null)
            {
                return;
            }

            List<string> filenames = new List<string>();
            foreach (object selected in this._treeView.RealSelectedItems)
            {
                FileNode fileNode = selected as FileNode;
                if (fileNode != null)
                {
                    filenames.Add(fileNode.FullPath);
                }
                else
                {
                    ProjectCollectionNode collectionNode = selected as ProjectCollectionNode;
                    if (collectionNode != null)
                    {
                        filenames.Add(collectionNode.FullPath);
                    }
                }
            }

            if (filenames.Count == 0)
            {
                return;
            }

            service.GetLatest(filenames);
            var fileMetadataList = service.GetFileMetaData(filenames);
            StringComparison comparisonType = StringComparison.OrdinalIgnoreCase;
            foreach (object selected in this._treeView.RealSelectedItems)
            {
                string filePath = null;
                FileNode fileNode = selected as FileNode;
                ProjectCollectionNode collectionNode = null;
                if (fileNode != null)
                {
                    filePath = fileNode.FullPath;
                }
                else
                {
                    collectionNode = selected as ProjectCollectionNode;
                    if (collectionNode != null)
                    {
                        filePath = collectionNode.FullPath;
                    }
                }

                IPerforceFileMetaData metadata = null;
                if (fileMetadataList != null)
                {
                    foreach (IPerforceFileMetaData fileMetadata in fileMetadataList)
                    {
                        string localPath = fileMetadata.LocalPath;
                        if (String.Equals(localPath, filePath, comparisonType))
                        {
                            metadata = fileMetadata;
                            break;
                        }
                    }
                }

                if (fileNode != null)
                {
                    fileNode.UpdatePerforceState(metadata);
                }
                else if (collectionNode != null)
                {
                    collectionNode.UpdatePerforceState(metadata);
                }
            }
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether this project collection is currently loaded.
        /// </summary>
        public bool CollectionLoaded
        {
            get { return (bool)this.GetValue(CollectionLoadedProperty); }
            private set { this.SetValue(_collectionLoadedPropertyKey, value); }
        }

        /// <summary>
        /// Gets or sets the project collection view model that should be displayed on this
        /// control.
        /// </summary>
        public ProjectCollectionNode ProjectCollection
        {
            get
            {
                return (ProjectCollectionNode)this.GetValue(ProjectCollectionProperty);
            }

            set
            {
                this.SetValue(ProjectCollectionProperty, value);
            }
        }

        /// <summary>
        /// Gets an iterator around the items that are at the root of this control.
        /// </summary>
        public IEnumerable RootItemsSource
        {
            get { return (IEnumerable)this.GetValue(RootItemsSourceProperty); }
            private set { this.SetValue(_rootItemsSourcePropertyKey, value); }
        }

        /// <summary>
        /// Gets a iterator around the currently selected items.
        /// </summary>
        [Bindable(true)]
        public IEnumerable<IHierarchyNode> SelectedItems
        {
            get { return (IEnumerable<IHierarchyNode>)this.GetValue(SelectedItemsProperty); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Retrieves an iterator around the currently selected hierarchy nodes in this tree
        /// view control.
        /// </summary>
        /// <returns>
        /// An iterator around the currently selected hierarchy nodes.
        /// </returns>
        public IEnumerable<IHierarchyNode> GetSelectedHierarchyNodes()
        {
            if (this._treeView == null)
            {
                return Enumerable.Empty<IHierarchyNode>();
            }

            return this._treeView.RealSelectedItems.OfType<IHierarchyNode>();
        }

        /// <summary>
        /// Shows the rename edit control for the specified node.
        /// </summary>
        /// <param name="node">
        /// The node to rename.
        /// </param>
        public void RenameItem(IHierarchyNode node)
        {
            IRenameable renameable = node as IRenameable;
            if (renameable == null)
            {
                return;
            }

            object containerDataContext = this._treeView.GetFirstTreeNode(node);
            IRenameController controller = renameable.BeginRename(containerDataContext);
            this._treeView.BeginRename(controller);
        }

        /// <summary>
        /// Makes sure the specified item is selected if found in the tree view and makes sure
        /// the tree is expanded and scrolled correctly.
        /// </summary>
        /// <param name="node">
        /// The item to select.
        /// </param>
        /// <returns>
        /// A value indicating whether the node was found and selection was attempted.
        /// </returns>
        public bool SelectItem(IHierarchyNode node)
        {
            if (node == null)
            {
                return false;
            }

            IHierarchyNode parent = node.Parent;
            List<IHierarchyNode> path = new List<IHierarchyNode>();
            while (parent != null)
            {
                path.Insert(0, parent);
                parent = parent.Parent;
            }

            foreach (IHierarchyNode pathItem in path)
            {
                this._treeView.ExpandItem(pathItem);
            }

            return this._treeView.SelectItem(node);
        }

        /// <summary>
        /// Gets called whenever the internal processes set the control template for this
        /// control.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            this._treeView = this.GetTemplateChild("PART_TreeView") as RsRenameTreeView;
            if (this._treeView != null)
            {
                this._treeView.GetContextMenuIdCallback = this.GetContextMenuIdCallback;
                this._treeView.SelectionChanged += this.OnSelectionChanged;
            }
        }

        /// <summary>
        /// Called whenever the <see cref="ProjectCollection"/> dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The object whose <see cref="ProjectCollection"/> dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnCollectionChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            ProjectCollectionExplorer explorer = s as ProjectCollectionExplorer;
            if (explorer == null)
            {
                Debug.Assert(explorer != null, "Incorrect change cast on project collection");
                return;
            }

            ProjectCollectionNode oldValue = e.OldValue as ProjectCollectionNode;
            if (oldValue != null)
            {
                oldValue.LoadedStateChanged -= explorer.CollectionLoadedStateChanged;
            }

            ProjectCollectionNode newValue = e.NewValue as ProjectCollectionNode;
            if (newValue == null)
            {
                explorer.RootItemsSource = Enumerable.Empty<object>();
            }
            else
            {
                newValue.LoadedStateChanged += explorer.CollectionLoadedStateChanged;
                explorer.RootItemsSource = Enumerable.Repeat(newValue, 1);
                explorer.CollectionLoaded = newValue.Loaded;
            }
        }

        /// <summary>
        /// Attaches all of the necessary command bindings to this instance.
        /// </summary>
        private void AttachCommandBindings()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnDragOver(DragEventArgs e)
        {
            DragEventArgs args = (DragEventArgs)e;
            args.Effects = DragDropEffects.None;
            e.Handled = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void OnDragStarted(object sender, RoutedEventArgs e)
        {
            RsVirtualisedTreeViewItem item = (RsVirtualisedTreeViewItem)sender;
            object[] items = item.GetVisualAncestor<RsVirtualisedTreeView>().RealSelectedItems;

            List<HierarchyNode> nodes = new List<HierarchyNode>();
            foreach (object selectedItem in items)
            {
                if (!(selectedItem is HierarchyNode) || selectedItem is CoreFolderNode)
                {
                    return;
                }

                nodes.Add((HierarchyNode)selectedItem);
            }

            List<HierarchyNode> commonParents = new List<HierarchyNode>(nodes);
            foreach (HierarchyNode node in nodes)
            {
                IHierarchyNode parent = node.Parent;
                while (parent != null)
                {
                    if (nodes.Contains(parent))
                    {
                        commonParents.Remove(node);
                        break;
                    }

                    parent = parent.Parent;
                }
            }

            items = commonParents.ToArray<object>();
            string dataFormat = "ProjectItem";
            DragDropEffects effects = DragDropEffects.None | DragDropEffects.Move;
            DragDrop.DoDragDrop(item, new DataObject(dataFormat, items), effects);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void OnDragEnter(object sender, RoutedEventArgs e)
        {
            DragEventArgs args = (DragEventArgs)e;
            RsVirtualisedTreeViewItem item = (RsVirtualisedTreeViewItem)sender;
            item.IsSelected = true;

            string dataFormat = "ProjectItem";
            args.Effects = GetEffects(item, args.Data.GetData(dataFormat));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void OnDragLeave(object sender, RoutedEventArgs e)
        {
            DragEventArgs args = (DragEventArgs)e;
            string dataFormat = "ProjectItem";
            object[] draggedObjects = args.Data.GetData(dataFormat) as object[];

            RsVirtualisedTreeViewItem item = (RsVirtualisedTreeViewItem)sender;
            object draggedData = item.DataContext;
            if (draggedData is VirtualisedTreeNode)
            {
                draggedData = ((VirtualisedTreeNode)draggedData).Item;
            }

            foreach (object draggedObject in draggedObjects)
            {
                if (Object.ReferenceEquals(draggedData, draggedObject))
                {
                    return;
                }
            }

            item.IsSelected = false;
            args.Effects = GetEffects(item, args.Data.GetData(dataFormat));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void OnDragOver(object sender, RoutedEventArgs e)
        {
            DragEventArgs args = (DragEventArgs)e;
            string dataFormat = "ProjectItem";
            RsVirtualisedTreeViewItem item = (RsVirtualisedTreeViewItem)sender;
            args.Effects = GetEffects(item, args.Data.GetData(dataFormat));
            e.Handled = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void OnDragDrop(object sender, RoutedEventArgs e)
        {
            DragEventArgs args = (DragEventArgs)e;
            string dataFormat = "ProjectItem";
            object[] draggedObjects = args.Data.GetData(dataFormat) as object[];

            RsVirtualisedTreeViewItem item = (RsVirtualisedTreeViewItem)sender;
            object itemData = item.DataContext;
            IHierarchyNode newParentNode = null;
            if (itemData is VirtualisedTreeNode)
            {
                newParentNode = ((VirtualisedTreeNode)itemData).Item as IHierarchyNode;
            }

            if (newParentNode is DocumentNode && !(newParentNode is ProjectNode))
            {
                newParentNode = newParentNode.Parent as IHierarchyNode;
            }

            RsApplication app = Application.Current as RsApplication;
            IMessageBoxService msgService = app.GetService<IMessageBoxService>();

            foreach (object draggedObject in draggedObjects)
            {
                HierarchyNode draggedNode = draggedObject as HierarchyNode;
                if (Object.ReferenceEquals(draggedNode.Parent, newParentNode))
                {
                    string msg = "Cannot move '{0}'. The destination folder is the same as the source folder.";
                    msg = msg.FormatCurrent(draggedNode.Text);
                    msgService.ShowStandardErrorBox(msg, null);
                    item.IsSelected = false;
                    return;
                }

                if (IsParentASubItem(draggedNode, newParentNode))
                {
                    string msg = "Cannot move '{0}'. The destination folder is a subfolder of the source folder.";
                    msg = msg.FormatCurrent(draggedNode.Text);
                    msgService.ShowStandardErrorBox(msg, null);
                    item.IsSelected = false;
                    return;
                }

                if (draggedObject is FolderNode)
                {
                    foreach (IHierarchyNode child in newParentNode.GetChildren(false))
                    {
                        if (child is FolderNode && child.Text == draggedNode.Text)
                        {
                            string msg = "Cannot move '{0}'. A folder with that name already exists in the destination.";
                            msg = msg.FormatCurrent(draggedNode.Text);
                            msgService.ShowStandardErrorBox(msg, null);
                            item.IsSelected = false;
                            return;
                        }
                    }
                }
            }

            ProjectItem newParent = newParentNode.Model as ProjectItem;
            IProjectItemScope newScope = newParent as IProjectItemScope ?? newParent.Scope;
            foreach (object draggedObject in draggedObjects)
            {
                HierarchyNode draggedNode = draggedObject as HierarchyNode;
                ProjectItem draggedItem = draggedNode.Model;
                IProjectItemScope oldScope = draggedItem.Scope;
                IHierarchyNode oldModifiedScope = draggedNode.ModifiedScopeNode;
                IHierarchyNode oldParentNode = draggedNode.Parent;

                if (!Object.ReferenceEquals(newScope, oldScope))
                {
                    draggedItem.ChangeProjectScope(newScope);
                }

                draggedNode.Model.ChangeParent(newParentNode.Model);
                newParentNode.AddChild(draggedNode);
                oldParentNode.RemoveChild(draggedNode);
                draggedNode.ChangeParent(newParentNode);

                IHierarchyNode newModifiedScope = draggedNode.ModifiedScopeNode;
                oldModifiedScope.IsModified = true;
                newModifiedScope.IsModified = true;
            }
        }

        private static bool IsParentASubItem(HierarchyNode root, IHierarchyNode parent)
        {
            IHierarchyNode current = parent.Parent;
            while (current != null)
            {
                if (Object.ReferenceEquals(root, current))
                {
                    return true;
                }

                current = current.Parent;
            }

            return false;
        }

        private static DragDropEffects GetEffects(RsVirtualisedTreeViewItem item, object data)
        {
            object[] draggedObjects = data as object[];
            if (draggedObjects == null)
            {
                return DragDropEffects.None;
            }
            else
            {
                bool draggingCollectionItem = false;
                foreach (object draggedObject in draggedObjects)
                {
                    HierarchyNode draggedNode = draggedObject as HierarchyNode;
                    if (draggedNode.ProjectItemScopeNode is ProjectCollectionNode)
                    {
                        draggingCollectionItem = true;
                        break;
                    }
                }

                if (draggingCollectionItem)
                {
                    object targetData = item.DataContext;
                    if (targetData is VirtualisedTreeNode)
                    {
                        targetData = ((VirtualisedTreeNode)targetData).Item;
                    }

                    if (targetData is ProjectCollectionNode)
                    {
                        return DragDropEffects.Move;
                    }
                    else if (targetData is CollectionFolderNode)
                    {
                        return DragDropEffects.Move;
                    }
                    else
                    {
                        return DragDropEffects.None;
                    }
                }
                else
                {
                    return DragDropEffects.Move;
                }
            }
        }

        /// <summary>
        /// Called whenever the loaded state of the attached project collection changes.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The RSG.Editor.ValueChangedEventArgs containing the event data.
        /// </param>
        private void CollectionLoadedStateChanged(object sender, ValueChangedEventArgs<bool> e)
        {
            this.CollectionLoaded = e.NewValue;
            if (e.NewValue == false && this._treeView != null)
            {
                this._treeView.UnselectAll();
            }
        }

        /// <summary>
        /// Retrieves the command id to use with the command manager to retrieve the specified
        /// items command children.
        /// </summary>
        /// <param name="item">
        /// The item whose child command id will be returned.
        /// </param>
        /// <returns>
        /// The command id that should be used by the command manager to retrieve the children
        /// for the specified item.
        /// </returns>
        private Guid GetContextMenuIdCallback(object item)
        {
            if (item is CollectionFolderNode)
            {
                return ProjectCommandIds.CollectionFolderContextMenu;
            }
            else if (item is ProjectCollectionNode)
            {
                return ProjectCommandIds.CollectionContextMenu;
            }
            else if (item is ProjectNode)
            {
                return ProjectCommandIds.ProjectContextMenu;
            }
            else if (item is ProjectFolderNode)
            {
                return ProjectCommandIds.ProjectFolderContextMenu;
            }
            else if (item is ProjectDynamicFolderNode)
            {
                return ProjectCommandIds.ProjectDynamicFolderContextMenu;
            }
            else if (item is FileNode)
            {
                return ProjectCommandIds.ProjectItemContextMenu;
            }

            return Guid.Empty;
        }

        /// <summary>
        /// Retrieves the selected hierarchy nodes to be used as the command parameter for the
        /// specified command.
        /// </summary>
        /// <param name="commandData">
        /// The command data that has called this resolver.
        /// </param>
        /// <returns>
        /// An iterator around the currently selected hierarchy nodes.
        /// </returns>
        private IEnumerable<IHierarchyNode> GetSelectedResolver(CommandData commandData)
        {
            if (this._treeView == null)
            {
                return Enumerable.Empty<IHierarchyNode>();
            }

            return this._treeView.RealSelectedItems.OfType<IHierarchyNode>();
        }

        /// <summary>
        /// Called whenever the selection on the embedded tree view changes.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Controls.SelectionChangedEventArgs contains the event data.
        /// </param>
        private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            IEnumerable<IHierarchyNode> value =
                this._treeView.RealSelectedItems.OfType<IHierarchyNode>();

            this.SetValue(_selectedItemsPropertyKey, value);
            if (this.ProjectCollection != null)
            {
                this.ProjectCollection.SelectedItems = value;
            }
        }
        #endregion Methods
    } // RSG.Project.View.ProjectCollectionExplorer {Class}
} // RSG.Project.Viewl {Namespace}
