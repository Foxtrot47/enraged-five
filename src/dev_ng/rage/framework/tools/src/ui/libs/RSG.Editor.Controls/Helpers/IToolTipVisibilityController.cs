﻿//---------------------------------------------------------------------------------------------
// <copyright file="IToolTipVisibilityController.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Helpers
{
    using System.Windows;

    /// <summary>
    /// When implemented represents a control that contains methods used by the
    /// <see cref="ToolTipVisibilityManager"/> to display tooltips.
    /// </summary>
    public interface IToolTipVisibilityController
    {
        #region Methods
        /// <summary>
        /// Determines whether the tooltip for this object can be shown based on the relative
        /// position the tooltip wants to appear.
        /// </summary>
        /// <param name="position">
        /// The relative position to where the tooltip wants to be shown.
        /// </param>
        /// <returns>
        /// True if the tooltip can be shown; otherwise, false.
        /// </returns>
        bool CanShowToolTip(Point position);

        /// <summary>
        /// Retrieves the content that should be shown inside the tooltip presenter.
        /// </summary>
        /// <returns>
        /// The content that should be shown inside the tooltip presenter.
        /// </returns>
        object GetToolTipContent();
        #endregion Methods
    } // RSG.Editor.Controls.Helpers.IToolTipVisibilityController {Interface}
} // RSG.Editor.Controls.Helpers {Namespace}
