﻿//---------------------------------------------------------------------------------------------
// <copyright file="CloseAllDocumentsAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Commands
{
    using System.Diagnostics;
    using System.Linq;
    using RSG.Editor;
    using RSG.Editor.Controls.Dock;

    /// <summary>
    /// Contains the logic for the <see cref="DockingCommands.CloseAll"/> routed command. This
    /// class cannot be inherited.
    /// </summary>
    public sealed class CloseAllDocumentsAction : ButtonAction<ProjectCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CloseAllDocumentsAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public CloseAllDocumentsAction(ProjectCommandResolver resolver)
            : base(new ParameterResolverDelegate<ProjectCommandArgs>(resolver))
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ProjectCommandArgs args)
        {
            if (args == null)
            {
                return false;
            }

            if (!args.OpenedDocuments.Any())
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(ProjectCommandArgs args)
        {
            IDocumentManagerService docService = this.GetService<IDocumentManagerService>();
            if (docService == null)
            {
                Debug.Fail(
                    "Unable to close all documents due to the fact the service is missing.");
                return;
            }

            docService.CloseOpenedDocuments(true);
        }
        #endregion Methods
    } // RSG.Project.Commands.CloseAllDocumentsAction {Class}
} // RSG.Project.Commands {Namespace}
