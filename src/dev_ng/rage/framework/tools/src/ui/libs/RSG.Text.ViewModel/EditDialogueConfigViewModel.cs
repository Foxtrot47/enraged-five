﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor.View;
using RSG.Text.Model;

namespace RSG.Text.ViewModel
{
    public class EditDialogueConfigViewModel : ViewModelBase<DialogueConfigurations>
    {
        private List<CategoryViewModel> _categories;

        public List<CategoryViewModel> Categories
        {
            get { return _categories; }
        }

        public EditDialogueConfigViewModel(DialogueConfigurations configurations)
            :base(configurations)
        {
            _categories = new List<CategoryViewModel>();
            _categories.Add(new CharacterCategoryViewModel(Model));
            _categories.Add(new VoiceCategoryViewModel(Model));
            _categories.Add(new SpecialCategoryViewModel(Model));
            _categories.Add(new AudioTypeCategoryViewModel(Model));
            _categories.Add(new AudibilityCategoryViewModel(Model));
        }
    }
}
