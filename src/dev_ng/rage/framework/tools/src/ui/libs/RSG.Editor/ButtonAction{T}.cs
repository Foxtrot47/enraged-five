﻿//---------------------------------------------------------------------------------------------
// <copyright file="ButtonAction{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    /// <summary>
    /// Provides a abstract base class to any class that is used to implement a button command.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the parameter pass into the can execute and execute methods.
    /// </typeparam>
    public abstract class ButtonAction<T> : CommandAction<T>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ButtonAction{T}"/> class.
        /// </summary>
        /// <param name="serviceProvider">
        /// The service provider that is used by this action to perform application specific
        /// operations, for example selecting a file to open.
        /// </param>
        protected ButtonAction(ICommandServiceProvider serviceProvider)
            : base(serviceProvider)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ButtonAction{T}"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        protected ButtonAction(ParameterResolverDelegate<T> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Create a new command definition for each command associated with this
        /// implementation.
        /// </summary>
        /// <param name="command">
        /// The command to create the definition for.
        /// </param>
        /// <returns>
        /// The new command definition created for the specified command.
        /// </returns>
        protected override CommandDefinition CreateDefinition(RockstarRoutedCommand command)
        {
            ButtonCommand definition = new ButtonCommand(command);
            return definition;
        }
        #endregion Methods
    }  // RSG.Editor.ButtonAction{T} {Class}
} // RSG.Editor {Namespace}
