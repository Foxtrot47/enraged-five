﻿//---------------------------------------------------------------------------------------------
// <copyright file="TunableStructureViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Tunables
{
    using System;
    using System.IO;
    using System.Windows.Media.Imaging;
    using System.Xml;
    using RSG.Editor;
    using RSG.Editor.View;
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// The view model for a single <see cref="TunableStructure"/> object. This is used as the
    /// root view model into a metadata document.
    /// </summary>
    public class TunableStructureViewModel
        : ViewModelBase<TunableStructure>,
        ITreeDisplayItem,
        ISupportsExpansionState,
        ISupportsExpansionEvents,
        ISaveable
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="IsExpandedByDefault"/> property.
        /// </summary>
        private bool _isExpandedByDefault;

        /// <summary>
        /// The private field used for the <see cref="Tunables"/> property.
        /// </summary>
        private TunableCollection _tunables;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="TunableStructureViewModel"/> class.
        /// </summary>
        /// <param name="tunableStructure">
        /// The tunable structure this view model is representing.
        /// </param>
        public TunableStructureViewModel(TunableStructure tunableStructure)
            : base(tunableStructure)
        {
            this.Text = tunableStructure.Definition.ShortDataType;
            this._tunables = new TunableCollection(tunableStructure.Tunables, null);
            this._isExpandedByDefault = true;
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs just after this object has been saved successfully.
        /// </summary>
        public event EventHandler Saved;
        #endregion Events

        #region Properties
        /// <summary>
        /// Gets a value indicating whether this items children are destroyed when it is
        /// collapsed. The resulting behaviour if true is that the references are kept in a
        /// cache and the expansion state is persistent for child nodes.
        /// </summary>
        public bool DestroyNodesOnCollapse
        {
            get { return false; }
        }

        /// <summary>
        /// Gets the bitmap source that is used to display the icon for this item when it has
        /// been expanded.
        /// </summary>
        public BitmapSource ExpandedIcon
        {
            get { return this.Icon; }
        }

        /// <summary>
        /// Gets the friendly save path that this item pushes into the command system to
        /// display on menu items.
        /// </summary>
        public string FriendlySavePath
        {
            get { return null; }
        }

        /// <summary>
        /// Gets a value indicating whether this tunable can be expanded.
        /// </summary>
        public bool IsExpandable
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether this item is expanded by default in the user
        /// interface.
        /// </summary>
        public bool IsExpandedByDefault
        {
            get { return this._isExpandedByDefault; }
        }

        /// <summary>
        /// Gets the map key for this object. This always returns null as it's only here to
        /// stop binding errors.
        /// </summary>
        public object MapKey
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the bitmap source that is used to display the overlay icon for this item.
        /// </summary>
        public virtual BitmapSource OverlayIcon
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the bitmap source that is used to display the state icon for this item.
        /// </summary>
        public BitmapSource StateIcon
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the text that is used to represent the state of this item.
        /// </summary>
        public string StateToolTipText
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the collection of child tunables for this structure.
        /// </summary>
        public IReadOnlyViewModelCollection<ITunableViewModel> Tunables
        {
            get { return this._tunables; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Called after this item is collapsed in the view and is used so that the default
        /// expansion state can be set to the appropriate value.
        /// </summary>
        public void AfterCollapse()
        {
            this._isExpandedByDefault = false;
        }

        /// <summary>
        /// Called before this item is expanded in the view and is used so that the default
        /// expansion state can be set to the appropriate value.
        /// </summary>
        public void BeforeExpand()
        {
            this._isExpandedByDefault = true;
        }

        /// <summary>
        /// Raises the saved event on this object.
        /// </summary>
        public void RaiseSavedEvent()
        {
            EventHandler handler = this.Saved;
            if (handler == null)
            {
                return;
            }

            handler(this, EventArgs.Empty);
        }

        /// <summary>
        /// Saves the data that is associated with this item to the specified file.
        /// </summary>
        /// <param name="stream">
        /// The io stream that data for this object should be written to.
        /// </param>
        /// <returns>
        /// A value indicating whether the save operation was successful.
        /// </returns>
        public bool Save(Stream stream)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.ConformanceLevel = ConformanceLevel.Document;
            using (XmlWriter writer = XmlWriter.Create(stream, settings))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement(this.Model.Definition.MetadataName);
                this.Model.Serialise(writer, false);
                writer.WriteEndElement();
                writer.WriteEndDocument();
            }

            return true;
        }
        #endregion Methods
    } // RSG.Metadata.ViewModel.Tunables.TunableStructureViewModel {Class}
} // RSG.Metadata.ViewModel.Tunables {Namespace}
