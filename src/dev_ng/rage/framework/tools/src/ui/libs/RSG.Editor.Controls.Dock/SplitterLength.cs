﻿//---------------------------------------------------------------------------------------------
// <copyright file="SplitterLength.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock
{
    using System;
    using System.ComponentModel;
    using System.Globalization;

    /// <summary>
    /// Represents the length value a single splitter item can have inside a
    /// <see cref="RsSplitterPanel"/>.
    /// </summary>
    [TypeConverter(typeof(SplitterLengthConverter))]
    public class SplitterLength : IEquatable<SplitterLength>
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="SplitterUnitType"/> property.
        /// </summary>
        private SplitterUnitType _splitterUnitType;

        /// <summary>
        /// The private field used for the <see cref="Value"/> property.
        /// </summary>
        private double _value;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SplitterLength"/> class that will have
        /// the specified length of unit type Stretch.
        /// </summary>
        /// <param name="value">
        /// The value that this length will have.
        /// </param>
        public SplitterLength(double value)
        {
            this._value = value;
            this._splitterUnitType = SplitterUnitType.Stretch;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="SplitterLength"/> class that will have
        /// the specified length and unit type.
        /// </summary>
        /// <param name="value">
        /// The value that this length will have.
        /// </param>
        /// <param name="unitType">
        /// The unit type this length will have.
        /// </param>
        public SplitterLength(double value, SplitterUnitType unitType)
        {
            this._value = value;
            this._splitterUnitType = unitType;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets a value indicating whether or not this length is representing a length value
        /// with a Fill unit type.
        /// </summary>
        public bool IsFill
        {
            get { return this.SplitterUnitType == SplitterUnitType.Fill; }
        }

        /// <summary>
        /// Gets a value indicating whether or not this length is representing a length value
        /// with a Stretch unit type.
        /// </summary>
        public bool IsStretch
        {
            get { return this.SplitterUnitType == SplitterUnitType.Stretch; }
        }

        /// <summary>
        /// Gets the type of units this length is currently representing.
        /// </summary>
        public SplitterUnitType SplitterUnitType
        {
            get { return this._splitterUnitType; }
        }

        /// <summary>
        /// Gets the value of this splitter length.
        /// </summary>
        public double Value
        {
            get { return this._value; }
        }
        #endregion

        #region Operators
        /// <summary>
        /// Determines whether two splitter values are equal.
        /// </summary>
        /// <param name="obj1">
        /// The first splitter length to compare.
        /// </param>
        /// <param name="obj2">
        /// The second splitter length to compare.
        /// </param>
        /// <returns>
        /// True if the two splitter lengths are different from each other; otherwise, false.
        /// </returns>
        public static bool operator !=(SplitterLength obj1, SplitterLength obj2)
        {
            return !(obj1 == obj2);
        }

        /// <summary>
        /// Determines whether two splitter values are equal.
        /// </summary>
        /// <param name="obj1">
        /// The first splitter length to compare.
        /// </param>
        /// <param name="obj2">
        /// The second splitter length to compare.
        /// </param>
        /// <returns>
        /// True if the two splitter lengths are the same; otherwise, false.
        /// </returns>
        public static bool operator ==(SplitterLength obj1, SplitterLength obj2)
        {
            return obj1.SplitterUnitType == obj2.SplitterUnitType && obj1.Value == obj2.Value;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Determines whether this instance is equal to the specified object.
        /// </summary>
        /// <param name="obj">
        /// The object to compare to this instance.
        /// </param>
        /// <returns>
        /// True of the specified object is equal to this instance; otherwise, false.
        /// </returns>
        public override bool Equals(object obj)
        {
            return obj is SplitterLength && this == (SplitterLength)obj;
        }

        /// <summary>
        /// Determines whether this instance is equal to the specified object.
        /// </summary>
        /// <param name="other">
        /// The <see cref="SplitterLength"/> to compare to this instance.
        /// </param>
        /// <returns>
        /// True of the specified object is equal to this instance; otherwise, false.
        /// </returns>
        public bool Equals(SplitterLength other)
        {
            return this == other;
        }

        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        /// A hash code for the current instance.
        /// </returns>
        public override int GetHashCode()
        {
            return (int)((int)this._value + this._splitterUnitType);
        }

        /// <summary>
        /// Returns a System.String that represents the current instance.
        /// </summary>
        /// <returns>
        /// A System.String that represents the current instance.
        /// </returns>
        public override string ToString()
        {
            SplitterLengthConverter converter = new SplitterLengthConverter();
            return converter.ToString(this, CultureInfo.InvariantCulture);
        }
        #endregion
    } // RSG.Editor.Controls.Dock.SplitterLength {Class}
} // RSG.Editor.Controls.Dock {Namespace}
