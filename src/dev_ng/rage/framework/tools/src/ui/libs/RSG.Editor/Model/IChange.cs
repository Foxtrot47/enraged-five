﻿//---------------------------------------------------------------------------------------------
// <copyright file="IChange.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Model
{
    /// <summary>
    /// Represents a single change within a undo event that provides methods to undo and redo
    /// the change that took place.
    /// </summary>
    internal interface IChange
    {
        #region Methods
        /// <summary>
        /// Redoes this single change.
        /// </summary>
        void Redo();

        /// <summary>
        /// Undoes this single change.
        /// </summary>
        void Undo();
        #endregion Methods
    } // RSG.Editor.Model.IChange {Interface}
} // RSG.Editor.Model {Namespace}
