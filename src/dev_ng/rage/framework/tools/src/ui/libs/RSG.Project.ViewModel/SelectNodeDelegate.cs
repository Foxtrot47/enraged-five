﻿//---------------------------------------------------------------------------------------------
// <copyright file="SelectNodeDelegate.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel
{
    /// <summary>
    /// Represents the method that can be used to select a single hierarchy node when needed.
    /// </summary>
    /// <param name="node">
    /// The node that needs to be selected.
    /// </param>
    public delegate void SelectNodeDelegate(IHierarchyNode node);
} // RSG.Project.ViewModel {Namespace}
