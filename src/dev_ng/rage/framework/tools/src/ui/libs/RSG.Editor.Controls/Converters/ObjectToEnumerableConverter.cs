﻿//---------------------------------------------------------------------------------------------
// <copyright file="ObjectToEnumerableConverter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Converters
{
    using System.Collections;
    using System.Globalization;
    using System.Linq;

    /// <summary>
    /// Converts a single object instance into a IEnumerable collection.
    /// </summary>
    public class ObjectToEnumerableConverter : ValueConverter<object, IEnumerable>
    {
        #region Methods
        /// <summary>
        /// Converters the given value and returns the result.
        /// </summary>
        /// <param name="value">
        /// The original value to convert.
        /// </param>
        /// <param name="param">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A converted value.
        /// </returns>
        protected override IEnumerable Convert(object value, object param, CultureInfo culture)
        {
            if (value == null)
            {
                return Enumerable.Empty<object>();
            }

            return Enumerable.Repeat<object>(value, 1);
        }
        #endregion Methods
    } // IntegerToVisibilityConverter.ObjectToEnumerableConverter {Class}
} // RSG.Editor.Controls.Converters {Namespace}
