﻿//---------------------------------------------------------------------------------------------
// <copyright file="NullableIntToNullableLongConverter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Converters
{
    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Data;

    /// <summary>
    /// Converts a nullable integer value into a nullable long value.
    /// </summary>
    public class NullableIntToNullableLongConverter : IValueConverter
    {
        #region Methods
        /// <summary>
        /// Converters the given value and returns the result.
        /// </summary>
        /// <param name="value">
        /// The original value to convert.
        /// </param>
        /// <param name="targetType">
        /// The type that the source value will be turned into.
        /// </param>
        /// <param name="param">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A converted value.
        /// </returns>
        public object Convert(
            object value, Type targetType, object param, CultureInfo culture)
        {
            if (value == null)
            {
                return new int?();
            }

            return new int?((int)value);
        }

        /// <summary>
        /// Converts a already converted value back to its original value and returns
        /// the result.
        /// </summary>
        /// <param name="value">
        /// The target value to convert back to its source value.
        /// </param>
        /// <param name="targetType">
        /// The type that the source value will be turned into.
        /// </param>
        /// <param name="param">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// The source value for the already converted specified value.
        /// </returns>
        public object ConvertBack(
            object value, Type targetType, object param, CultureInfo culture)
        {
            if (value == null)
            {
                return new long?();
            }

            return new long?((long)value);
        }
        #endregion Methods
    } // RSG.Editor.Controls.Converters.NullableIntToNullableLongConverter {Class}
} // RSG.Editor.Controls.Converters {Namespace}
