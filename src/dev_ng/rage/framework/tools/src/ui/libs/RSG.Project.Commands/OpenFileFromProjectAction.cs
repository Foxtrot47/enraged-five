﻿//---------------------------------------------------------------------------------------------
// <copyright file="OpenFileFromProjectAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Base.Logging.Universal;
    using RSG.Editor;
    using RSG.Editor.Controls.Dock.ViewModel;
    using RSG.Project.Commands.Resources;
    using RSG.Project.ViewModel;

    /// <summary>
    /// Contains the logic for the <see cref="ProjectCommands.OpenFileFromProject"/> routed
    /// command. This class cannot be inherited.
    /// </summary>
    public sealed class OpenFileFromProjectAction : ButtonAction<ProjectCommandArgs, FileNode>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="OpenFileFromProjectAction"/> class.
        /// </summary>
        /// <param name="serviceProvider">
        /// The service provider that is used by this action to perform application specific
        /// operations, for example selecting a file to open.
        /// </param>
        public OpenFileFromProjectAction(ICommandServiceProvider serviceProvider)
            : base(serviceProvider)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="OpenFileFromProjectAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public OpenFileFromProjectAction(ProjectCommandResolver resolver)
            : base(new ParameterResolverDelegate<ProjectCommandArgs>(resolver))
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="target">
        /// A override target.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ProjectCommandArgs args, FileNode target)
        {
            if (target != null)
            {
                return true;
            }

            if (args == null || !args.SelectedNodes.Any())
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="target">
        /// A override target.
        /// </param>
        public override void Execute(ProjectCommandArgs args, FileNode target)
        {
            IDocumentManagerService docService = this.GetService<IDocumentManagerService>();
            if (docService == null)
            {
                Debug.Fail("Unable to open file due to the fact the service is missing.");
                return;
            }

            IMessageBoxService messageService = this.GetService<IMessageBoxService>();
            if (messageService == null)
            {
                Debug.Assert(false, "Unable to open as services are missing.");
                return;
            }

            List<IHierarchyNode> nodes = new List<IHierarchyNode>();
            if (target != null)
            {
                nodes.Add(target);
            }
            else
            {
                nodes.AddRange(args.SelectedNodes);
            }

            foreach (IHierarchyNode node in nodes)
            {
                ICreatesDocumentContent contentCreator = node as ICreatesDocumentContent;
                FileNode fileNode = node as FileNode;
                if (fileNode == null || contentCreator == null)
                {
                    continue;
                }

                string fullPath = node.ProjectItem.GetMetadata("FullPath");
                if (fullPath == null)
                {
                    Debug.Assert(false, "Unable to determine the full path to the project.");
                    return;
                }

                foreach (DocumentItem openedDocument in args.OpenedDocuments)
                {
                    if (String.Equals(openedDocument.FullPath, fullPath))
                    {
                        openedDocument.IsSelected = true;
                        return;
                    }
                }

                DocumentPane pane = args.ViewSite.LastActiveDocumentPane;
                if (pane == null)
                {
                    pane = args.ViewSite.FirstDocumentPane;
                    if (pane == null)
                    {
                        Debug.Assert(false, "Cannot find the pane to open the document in.");
                        return;
                    }
                }

                FileInfo info = new FileInfo(fullPath);
                if (!info.Exists)
                {
                    messageService.ShowStandardErrorBox(
                        StringTable.FileMissingMsg.FormatInvariant(fullPath),
                        null);

                    return;
                }

                ProjectDocumentItem item = null;
                IUniversalLog log = LogFactory.CreateUniversalLog("Content Creation");
                string logFilename = Path.Combine(Path.GetTempPath(), "content_creation");
                logFilename = Path.ChangeExtension(logFilename, UniversalLogFile.EXTENSION);
                UniversalLogFile logTarget = new UniversalLogFile(
                    logFilename, FileMode.Create, FlushMode.Never, log);

                try
                {
                    ICreatesProjectDocument documentCreator = node as ICreatesProjectDocument;
                    if (documentCreator != null)
                    {
                        item = documentCreator.CreateDocument(pane, fullPath);
                    }
                    else
                    {
                        item = new ProjectDocumentItem(pane, fullPath, fileNode);
                    }

                    ICreateDocumentContentController controller =
                        contentCreator.CreateDocumentContentController;
                    if (controller != null)
                    {
                        CreateContentArgs e =
                            new CreateContentArgs(log, args.ViewSite, item.UndoEngine);
                        item.Content = docService.GetControlForDocument(controller.CreateContent(node as HierarchyNode, e));
                    }

                    if (log.HasErrors || log.HasWarnings)
                    {
                        logTarget.Flush();
                        Process.Start(logFilename);
                    }
                }
                catch (Exception ex)
                {
                    if (log.HasErrors || log.HasWarnings)
                    {
                        logTarget.Flush();
                        Process.Start(logFilename);
                    }

                    messageService.ShowStandardErrorBox(
                        StringTable.FailedFileParsing.FormatInvariant(fullPath, ex.Message),
                        null);
                    return;
                }

                if (docService.FileWatcher != null)
                {
                    docService.FileWatcher.RegisterFile(fullPath);
                }

                if (docService.BackupManager != null)
                {
                    docService.BackupManager.RegisterFile(item);
                }

                item.IsReadOnly = info.IsReadOnly;
                item.IsSelected = true;
                pane.InsertChild(pane.Children.Count, item);


                docService.FireDocumentOpenedEvent(item);
            }
        }
        #endregion Methods
    } // RSG.Project.Commands.OpenFileFromProjectAction {Class}
} // RSG.Project.Commands {Namespace}
