﻿//---------------------------------------------------------------------------------------------
// <copyright file="LocateInExplorerAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Commands
{
    using RSG.Editor;
    using RSG.Project.ViewModel;

    /// <summary>
    /// Contains the logic for the <see cref="ProjectCommands.LocateInExplorer"/> routed
    /// command.
    /// </summary>
    public sealed class LocateInExplorerAction : ButtonAction<ProjectCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="LocateInExplorerAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public LocateInExplorerAction(ProjectCommandResolver resolver)
            : base(new ParameterResolverDelegate<ProjectCommandArgs>(resolver))
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ProjectCommandArgs args)
        {
            if (args == null)
            {
                return false;
            }

            if (args.ViewSite != null && args.ViewSite.ActiveDocument != null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(ProjectCommandArgs args)
        {
            if (args == null || args.NodeSelectionDelegate == null)
            {
                return;
            }

            if (args.ViewSite == null || args.ViewSite.ActiveDocument == null)
            {
                return;
            }

            ProjectDocumentItem document = args.ViewSite.ActiveDocument as ProjectDocumentItem;
            if (document != null && document.FileNode != null)
            {
                args.NodeSelectionDelegate(document.FileNode);
            }
        }
        #endregion Methods
    } // RSG.Project.Commands.LocateInExplorerAction {Class}
} // RSG.Project.Commands {Namespace}
