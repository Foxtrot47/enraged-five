﻿//---------------------------------------------------------------------------------------------
// <copyright file="CopyAndPasteConversationAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Commands
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Windows;
    using System.Xml;
    using RSG.Editor;
    using RSG.Text.Model;
    using RSG.Text.ViewModel;

    /// <summary>
    /// Provides a base class for the Copy and Paste commands for conversation.
    /// </summary>
    public abstract class CopyAndPasteConversationAction
        : ButtonAction<TextConversationCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CopyAndPasteConversationAction"/>
        /// class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public CopyAndPasteConversationAction(
            ParameterResolverDelegate<TextConversationCommandArgs> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the clipboard currently contains conversation data.
        /// </summary>
        /// <returns>
        /// True if the clipboard currently contains conversation data; otherwise, false.
        /// </returns>
        protected bool ClipboardContainsConversationData()
        {
            Type type = typeof(List<Conversation>);
            string formatName = type.FullName;
            DataFormat dataFormat = DataFormats.GetDataFormat(formatName);

            return Clipboard.ContainsData(dataFormat.Name);
        }

        /// <summary>
        /// Copies the specified conversations to the clipboard.
        /// </summary>
        /// <param name="conversations">
        /// The conversations to copy to the clipboard.
        /// </param>
        protected void CopyConversationsToClipboard(
            IEnumerable<ConversationViewModel> conversations)
        {
            Clipboard.Clear();
            StringBuilder sb = new StringBuilder();
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.ConformanceLevel = ConformanceLevel.Fragment;
            using (XmlWriter writer = XmlWriter.Create(new StringWriter(sb), settings))
            {
                foreach (ConversationViewModel conversation in conversations)
                {
                    writer.WriteStartElement("Item");
                    conversation.Model.Serialise(writer);
                    writer.WriteEndElement();
                }
            }

            Type type = typeof(List<Conversation>);
            string formatName = type.FullName;
            DataFormat dataFormat = DataFormats.GetDataFormat(formatName);

            DataObject dataObject = new DataObject();

            string data = sb.ToString();
            dataObject.SetData(dataFormat.Name, data, false);
            Clipboard.SetDataObject(dataObject, false);
        }

        /// <summary>
        /// Retrieves any conversations that are currently on the clipboard and adds them to
        /// the specified conversation at the specified index.
        /// </summary>
        /// <param name="dialogue">
        /// The dialogue the conversations should be added to.
        /// </param>
        /// <param name="index">
        /// The zero-based index the conversations on the clipboard should be added at.
        /// </param>
        /// <returns>
        /// The conversations that have been created from the data on the clipboard.
        /// </returns>
        protected List<Conversation> GetDataFromClipboard(Dialogue dialogue, int index)
        {
            List<Conversation> conversations = new List<Conversation>();
            IDataObject dataObject = Clipboard.GetDataObject();
            if (dataObject == null)
            {
                return conversations;
            }

            string dataFormat = typeof(List<Conversation>).FullName;
            if (!dataObject.GetDataPresent(dataFormat))
            {
                return conversations;
            }

            string rawData = dataObject.GetData(dataFormat) as string;
            if (rawData == null)
            {
                return conversations;
            }

            XmlReaderSettings settings = new XmlReaderSettings();
            settings.ConformanceLevel = ConformanceLevel.Fragment;
            using (XmlReader reader = XmlReader.Create(new StringReader(rawData), settings))
            {
                reader.MoveToContent();
                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    if (String.Equals(reader.Name, "Item"))
                    {
                        Conversation newConversation =
                            dialogue.AddDeserialisedConversationAt(reader, index++);
                        foreach (ILine line in newConversation.Lines)
                        {
                            line.AudioFilepath = String.Empty;
                            line.Recorded = false;
                            line.SentForRecording = false;
                        }

                        conversations.Add(newConversation);
                    }

                    reader.Skip();
                }
            }

            return conversations;
        }
        #endregion Methods
    } // RSG.Text.Commands.CopyAndPasteConversationAction {Class}
} // RSG.Text.Commands {Namespace}
