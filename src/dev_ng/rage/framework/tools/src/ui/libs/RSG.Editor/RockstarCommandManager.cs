﻿//---------------------------------------------------------------------------------------------
// <copyright file="RockstarCommandManager.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media.Imaging;
    using System.Xml;
    using RSG.Base.Attributes;
    using RSG.Base.Extensions;
    using RSG.Editor.Resources;
    using RSG.Editor.SharedCommands;

    /// <summary>
    /// Responsible for managing all of the defined command sets for this application.
    /// </summary>
    public static class RockstarCommandManager
    {
        #region Fields
        /// <summary>
        /// The private list containing all of the command bar items defined for this command
        /// manager that have been added as user created.
        /// </summary>
        private static readonly List<IUserCreatedCommandBarItem> _addedCommandBarItems;

        /// <summary>
        /// The private list containing all of the command bar items defined for this command
        /// manager included the removed ones.
        /// </summary>
        private static readonly List<CommandBarItem> _allCommandBarItems;

        /// <summary>
        /// The private list containing all of the command bar items defined for this command
        /// manager.
        /// </summary>
        private static readonly List<CommandBarItem> _commandBarItems;

        /// <summary>
        /// The private list containing all of the command definitions defined for this command
        /// manager.
        /// </summary>
        private static readonly List<CommandDefinition> _commandDefinitions;

        /// <summary>
        /// The private dictionary containing the modification made to the command bar items.
        /// </summary>
        private static readonly ModificationContainer _modifications;

        /// <summary>
        /// The private dictionary containing the modification fields and their xml names.
        /// </summary>
        private static readonly Dictionary<ModificationField, string> _modificationXmlNames;

        /// <summary>
        /// The private dictionary containing the original values of a the command bar item
        /// before any modifications are applied.
        /// </summary>
        private static readonly ModificationContainer _originalValues;

        /// <summary>
        /// The private list containing all of the command bar items defined for this command
        /// manager that have been removed by the user.
        /// </summary>
        private static readonly List<Guid> _removedCommandBarItemIds;

        /// <summary>
        /// The private list containing all of the command bar items defined for this command
        /// manager that have been removed by the user.
        /// </summary>
        private static readonly List<CommandBarItem> _removedCommandBarItems;

        /// <summary>
        /// A generic object that provides synchronisation between multiple threads while
        /// operating on this class.
        /// </summary>
        private static readonly object _syncRoot;

        /// <summary>
        /// The private field used for the <see cref="DelayingPriorityChanges"/> property.
        /// </summary>
        private static bool _delayingPriorityChanges;

        /// <summary>
        /// The private instance used for the <see cref="FileMenuId"/> property.
        /// </summary>
        private static Guid? _fileMenuId;

        /// <summary>
        /// The private instance used for the <see cref="HelpMenuId"/> property.
        /// </summary>
        private static Guid? _helpMenuId;

        /// <summary>
        /// The private instance used for the <see cref="HelpToolBarId"/> property.
        /// </summary>
        private static Guid? _helpToolBarId;

        /// <summary>
        /// The private instance used for the <see cref="MainMenuId"/> property.
        /// </summary>
        private static Guid? _mainMenuId;

        /// <summary>
        /// The private instance used for the <see cref="ToolBarTrayId"/> property.
        /// </summary>
        private static Guid? _toolBarTrayId;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RockstarCommandManager"/> class.
        /// </summary>
        static RockstarCommandManager()
        {
            _syncRoot = new object();
            _commandDefinitions = new List<CommandDefinition>();
            _allCommandBarItems = new List<CommandBarItem>();
            _commandBarItems = new List<CommandBarItem>();
            _modifications = new ModificationContainer();
            _originalValues = new ModificationContainer();
            _addedCommandBarItems = new List<IUserCreatedCommandBarItem>();
            _removedCommandBarItemIds = new List<Guid>();
            _removedCommandBarItems = new List<CommandBarItem>();

            _modificationXmlNames = new Dictionary<ModificationField, string>();
            Type type = typeof(ModificationField);
            Type attributeType = typeof(XmlAttributeNameAttribute);
            foreach (ModificationField field in Enum.GetValues(type))
            {
                MemberInfo element = type.GetField(Enum.GetName(type, field));
                Attribute attribute = Attribute.GetCustomAttribute(element, attributeType);

                _modificationXmlNames.Add(
                    field, ((XmlAttributeNameAttribute)attribute).XmlAttributeName);
            }

            AddFileMenu(FileMenuId);
            AddHelpMenu(HelpMenuId);
            AddToolbar(0, int.MaxValue, StringTable.HelpToolBarName, HelpToolBarId);

            RockstarCommandManager.AddCommandDefinition(
                new ButtonCommand(RockstarCommands.ViewHelp));
            RockstarCommandManager.AddCommandDefinition(
                new ButtonCommand(RockstarCommands.ViewAbout));
            RockstarCommandManager.AddCommandDefinition(
                new ButtonCommand(RockstarCommands.ReportBug));
            RockstarCommandManager.AddCommandDefinition(
                new ButtonCommand(RockstarCommands.Exit));

            AddCommandInstance(
                RockstarCommands.Exit,
                ushort.MaxValue,
                true,
                StringTable.ExitButtonText,
                new Guid("6317EAB7-AAF0-4135-ABBA-ECF24223EA55"),
                RockstarCommandManager.FileMenuId);

            AddCommandInstance(
                RockstarCommands.ReportBug,
                0,
                false,
                StringTable.ReportBugButtonText,
                new Guid("547459D5-93EA-4FD4-9CF0-78F2083392EF"),
                RockstarCommandManager.HelpToolBarId);

            AddCommandInstance(
                RockstarCommands.ViewHelp,
                ushort.MaxValue,
                false,
                StringTable.ViewHelpButtonText,
                new Guid("A4E6A2CF-8AB2-43C9-BDA2-C02AD7998D4D"),
                RockstarCommandManager.HelpToolBarId);

            AddCommandInstance(
                RockstarCommands.ViewHelp,
                ushort.MinValue,
                false,
                StringTable.ViewHelpButtonText,
                new Guid("255200C1-5B2C-4854-8DDF-272D6557CA66"),
                RockstarCommandManager.HelpMenuId);

            AddCommandInstance(
                RockstarCommands.ReportBug,
                1,
                false,
                StringTable.ReportBugButtonText,
                new Guid("632FA9DF-87B5-43E2-BB2C-F3E19D844592"),
                RockstarCommandManager.HelpMenuId);

            AddCommandInstance(
                RockstarCommands.ViewAbout,
                ushort.MaxValue,
                true,
                StringTable.AboutButtonText,
                new Guid("C3D0EE45-B6B3-4177-9467-DBBC95C551AE"),
                RockstarCommandManager.HelpMenuId);

            RoutedUICommand appDelete = ApplicationCommands.Delete as RoutedUICommand;
            if (appDelete != null)
            {
                appDelete.InputGestures.Clear();
            }

            RoutedUICommand appCopy = ApplicationCommands.Copy as RoutedUICommand;
            if (appCopy != null)
            {
                appCopy.InputGestures.Clear();
            }

            RoutedUICommand appCut = ApplicationCommands.Cut as RoutedUICommand;
            if (appCut != null)
            {
                appCut.InputGestures.Clear();
            }

            RoutedUICommand appPaste = ApplicationCommands.Paste as RoutedUICommand;
            if (appPaste != null)
            {
                appPaste.InputGestures.Clear();
            }

            CommandManager.RegisterClassCommandBinding(
                typeof(TextBox),
                new CommandBinding(RockstarCommands.Copy, OnTextBoxCopy, CanTextBoxCopy));

            CommandManager.RegisterClassCommandBinding(
                typeof(TextBox),
                new CommandBinding(RockstarCommands.Paste, OnTextBoxPaste, CanTextBoxPaste));

            CommandManager.RegisterClassCommandBinding(
                typeof(TextBox),
                new CommandBinding(RockstarCommands.Cut, OnTextBoxCut, CanTextBoxCut));

            CommandManager.RegisterClassCommandBinding(
                typeof(TextBox),
                new CommandBinding(
                    RockstarCommands.Delete, OnTextBoxDelete, CanTextBoxDelete));
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a iterator over all of the command categories.
        /// </summary>
        public static IEnumerable<string> AllCategories
        {
            get
            {
                lock (_syncRoot)
                {
                    if (_commandDefinitions == null)
                    {
                        return Enumerable.Empty<string>();
                    }

                    return (from d in _commandDefinitions
                            where d != null
                            orderby d.CategoryName
                            select d.CategoryName ?? "Unknown").Distinct();
                }
            }
        }

        /// <summary>
        /// Gets a iterator over all of the command bar items.
        /// </summary>
        public static IEnumerable<CommandBarItem> AllCommandBarItems
        {
            get
            {
                lock (_syncRoot)
                {
                    if (_commandBarItems == null)
                    {
                        return Enumerable.Empty<CommandBarItem>();
                    }

                    return from i in _commandBarItems
                           where i != null
                           select i;
                }
            }
        }

        /// <summary>
        /// Gets a iterator over all of the command definitions.
        /// </summary>
        public static IEnumerable<CommandDefinition> AllCommandDefinitions
        {
            get
            {
                lock (_syncRoot)
                {
                    if (_commandDefinitions == null)
                    {
                        return Enumerable.Empty<CommandDefinition>();
                    }

                    return from d in _commandDefinitions
                           where d != null
                           select d;
                }
            }
        }

        /// <summary>
        /// Gets a iterator over all of the input gestures currently being used on the command
        /// definitions.
        /// </summary>
        public static IEnumerable<KeyGesture> AllKeyGestures
        {
            get
            {
                lock (_syncRoot)
                {
                    if (_commandDefinitions == null)
                    {
                        return Enumerable.Empty<KeyGesture>();
                    }

                    return from d in _commandDefinitions
                           where d != null
                           from g in d.KeyGestures
                           select g;
                }
            }
        }

        /// <summary>
        /// Gets the global identifier used for the file menu.
        /// </summary>
        public static Guid FileMenuId
        {
            get
            {
                if (!_fileMenuId.HasValue)
                {
                    lock (_syncRoot)
                    {
                        if (!_fileMenuId.HasValue)
                        {
                            _fileMenuId = new Guid("31841BFE-ECB1-46DA-8BEC-63D387B49EFF");
                        }
                    }
                }

                return _fileMenuId.Value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether there are any modifications currently registered
        /// with this manager.
        /// </summary>
        public static bool HasModifications
        {
            get
            {
                if (_modifications.Count > 0)
                {
                    return true;
                }

                if (_removedCommandBarItemIds.Count > 0)
                {
                    return true;
                }

                if (_addedCommandBarItems.Count > 0)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Gets the global identifier used for the help menu.
        /// </summary>
        public static Guid HelpMenuId
        {
            get
            {
                if (!_helpMenuId.HasValue)
                {
                    lock (_syncRoot)
                    {
                        if (!_helpMenuId.HasValue)
                        {
                            _helpMenuId = new Guid("95F9DB45-E29B-48CE-9C44-61358143CC76");
                        }
                    }
                }

                return _helpMenuId.Value;
            }
        }

        /// <summary>
        /// Gets the global identifier used for the help toolbar.
        /// </summary>
        public static Guid HelpToolBarId
        {
            get
            {
                if (!_helpToolBarId.HasValue)
                {
                    lock (_syncRoot)
                    {
                        if (!_helpToolBarId.HasValue)
                        {
                            _helpToolBarId = new Guid("3044368B-0519-47D9-8440-6596281FCBFF");
                        }
                    }
                }

                return _helpToolBarId.Value;
            }
        }

        /// <summary>
        /// Gets the global identifier used to for the toolbar tray.
        /// </summary>
        public static Guid ToolBarTrayId
        {
            get
            {
                if (!_toolBarTrayId.HasValue)
                {
                    lock (_syncRoot)
                    {
                        if (!_toolBarTrayId.HasValue)
                        {
                            _toolBarTrayId = new Guid("04E95F3B-B85B-42FE-A596-7F155E6AFA66");
                        }
                    }
                }

                return _toolBarTrayId.Value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the priority changes are currently getting
        /// delayed for the next commit.
        /// </summary>
        internal static bool DelayingPriorityChanges
        {
            get
            {
                return _delayingPriorityChanges;
            }

            set
            {
                if (_delayingPriorityChanges == value)
                {
                    return;
                }

                _delayingPriorityChanges = value;
                if (!_delayingPriorityChanges)
                {
                    CommandBarItemViewModel.CommitPriorityChanges();
                }
            }
        }

        /// <summary>
        /// Gets the global identifier used for the main menu.
        /// </summary>
        internal static Guid MainMenuId
        {
            get
            {
                if (!_mainMenuId.HasValue)
                {
                    lock (_syncRoot)
                    {
                        if (!_mainMenuId.HasValue)
                        {
                            _mainMenuId = new Guid("A899DF52-3B31-4C9A-BA6A-E41D6C4B6C25");
                        }
                    }
                }

                return _mainMenuId.Value;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Adds the action menu into the collection of managed command bar items.
        /// </summary>
        /// <param name="id">
        /// The unique identifier that is used to reference the actions menu.
        /// </param>
        public static void AddActionsMenu(Guid id)
        {
            ActionsTopLevelMenu item = new ActionsTopLevelMenu(id);
            AddCommandBarItem(item, true);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RockstarCommandBinding"/> class.
        /// </summary>
        /// <param name="element">
        /// The element to attached the binding to.
        /// </param>
        /// <param name="command">
        /// The command that the specified execution handler will be bound to.
        /// </param>
        /// <param name="executeHandler">
        /// The handler that gets called once the specified command is executed.
        /// </param>
        public static void AddBinding(
            UIElement element,
            ICommand command,
            ExecuteCommandDelegate executeHandler)
        {
            AddBinding(element, command, executeHandler, null);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RockstarCommandBinding"/> class.
        /// </summary>
        /// <param name="element">
        /// The element to attached the binding to.
        /// </param>
        /// <param name="command">
        /// The command that the specified execution handler will be bound to.
        /// </param>
        /// <param name="executeHandler">
        /// The handler that gets called once the specified command is executed.
        /// </param>
        /// <param name="canExecuteHandler">
        /// The handler that gets called to determine whether the command can be executed.
        /// </param>
        public static void AddBinding(
            UIElement element,
            ICommand command,
            ExecuteCommandDelegate executeHandler,
            CanExecuteCommandDelegate canExecuteHandler)
        {
            RockstarCommandBinding binding = new RockstarCommandBinding(
                command, executeHandler, canExecuteHandler);
            element.CommandBindings.Add(binding);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RockstarCommandBinding"/> class.
        /// </summary>
        /// <param name="type">
        /// The class with which to register the command binding to.
        /// </param>
        /// <param name="command">
        /// The command that the specified execution handler will be bound to.
        /// </param>
        /// <param name="executeHandler">
        /// The handler that gets called once the specified command is executed.
        /// </param>
        public static void AddBinding(
            Type type,
            ICommand command,
            ExecuteCommandDelegate executeHandler)
        {
            AddBinding(type, command, executeHandler, null);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RockstarCommandBinding"/> class.
        /// </summary>
        /// <param name="type">
        /// The class with which to register the command binding to.
        /// </param>
        /// <param name="command">
        /// The command that the specified execution handler will be bound to.
        /// </param>
        /// <param name="executeHandler">
        /// The handler that gets called once the specified command is executed.
        /// </param>
        /// <param name="canExecuteHandler">
        /// The handler that gets called to determine whether the command can be executed.
        /// </param>
        public static void AddBinding(
            Type type,
            ICommand command,
            ExecuteCommandDelegate executeHandler,
            CanExecuteCommandDelegate canExecuteHandler)
        {
            RockstarCommandBinding binding = new RockstarCommandBinding(
                command, executeHandler, canExecuteHandler);
            CommandManager.RegisterClassCommandBinding(type, binding);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RockstarCommandBinding{T}"/> class.
        /// </summary>
        /// <typeparam name="T">
        /// The type of command parameter that should be past into the execution and can
        /// execute methods.
        /// </typeparam>
        /// <param name="element">
        /// The element to attached the binding to.
        /// </param>
        /// <param name="command">
        /// The command that the specified execution handler will be bound to.
        /// </param>
        /// <param name="executeHandler">
        /// The handler that gets called once the specified command is executed.
        /// </param>
        public static void AddBinding<T>(
            UIElement element,
            ICommand command,
            ExecuteCommandDelegate<T> executeHandler)
        {
            AddBinding<T>(element, command, executeHandler, null);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RockstarCommandBinding{T}"/> class.
        /// </summary>
        /// <typeparam name="T">
        /// The type of command parameter that should be past into the execution and can
        /// execute methods.
        /// </typeparam>
        /// <param name="element">
        /// The element to attached the binding to.
        /// </param>
        /// <param name="command">
        /// The command that the specified execution handler will be bound to.
        /// </param>
        /// <param name="executeHandler">
        /// The handler that gets called once the specified command is executed.
        /// </param>
        /// <param name="canExecuteHandler">
        /// The handler that gets called to determine whether the command can be executed.
        /// </param>
        public static void AddBinding<T>(
            UIElement element,
            ICommand command,
            ExecuteCommandDelegate<T> executeHandler,
            CanExecuteCommandDelegate<T> canExecuteHandler)
        {
            RockstarCommandBinding<T> binding = new RockstarCommandBinding<T>(
                command, executeHandler, canExecuteHandler);
            element.CommandBindings.Add(binding);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RockstarCommandBinding{T}"/> class.
        /// </summary>
        /// <typeparam name="T">
        /// The type of command parameter that should be past into the execution and can
        /// execute methods.
        /// </typeparam>
        /// <param name="type">
        /// The class with which to register the command binding to.
        /// </param>
        /// <param name="command">
        /// The command that the specified execution handler will be bound to.
        /// </param>
        /// <param name="executeHandler">
        /// The handler that gets called once the specified command is executed.
        /// </param>
        public static void AddBinding<T>(
            Type type,
            ICommand command,
            ExecuteCommandDelegate<T> executeHandler)
        {
            AddBinding<T>(type, command, executeHandler, null);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RockstarCommandBinding{T}"/> class.
        /// </summary>
        /// <typeparam name="T">
        /// The type of command parameter that should be past into the execution and can
        /// execute methods.
        /// </typeparam>
        /// <param name="type">
        /// The class with which to register the command binding to.
        /// </param>
        /// <param name="command">
        /// The command that the specified execution handler will be bound to.
        /// </param>
        /// <param name="executeHandler">
        /// The handler that gets called once the specified command is executed.
        /// </param>
        /// <param name="canExecuteHandler">
        /// The handler that gets called to determine whether the command can be executed.
        /// </param>
        public static void AddBinding<T>(
            Type type,
            ICommand command,
            ExecuteCommandDelegate<T> executeHandler,
            CanExecuteCommandDelegate<T> canExecuteHandler)
        {
            RockstarCommandBinding<T> binding = new RockstarCommandBinding<T>(
                command, executeHandler, canExecuteHandler);
            CommandManager.RegisterClassCommandBinding(type, binding);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RockstarCommandBinding"/> class.
        /// </summary>
        /// <param name="element">
        /// The element to attached the binding to.
        /// </param>
        /// <param name="command">
        /// The command that the specified execution handler will be bound to.
        /// </param>
        /// <param name="executeHandler">
        /// The handler that gets called once the specified command is executed.
        /// </param>
        public static void AddBindingAsync(
            UIElement element, ICommand command, ExecuteCommandDelegate executeHandler)
        {
            AddBindingAsync(element, command, executeHandler, null);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RockstarCommandBinding"/> class.
        /// </summary>
        /// <param name="element">
        /// The element to attached the binding to.
        /// </param>
        /// <param name="command">
        /// The command that the specified execution handler will be bound to.
        /// </param>
        /// <param name="executeHandler">
        /// The handler that gets called once the specified command is executed.
        /// </param>
        /// <param name="canExecuteHandler">
        /// The handler that gets called to determine whether the command can be executed.
        /// </param>
        public static void AddBindingAsync(
            UIElement element,
            ICommand command,
            ExecuteCommandDelegate executeHandler,
            CanExecuteCommandDelegate canExecuteHandler)
        {
            RockstarCommandBinding binding = new RockstarCommandBinding(
                command, executeHandler, canExecuteHandler, true);
            element.CommandBindings.Add(binding);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RockstarCommandBinding{T}"/> class.
        /// </summary>
        /// <typeparam name="T">
        /// The type of command parameter that should be past into the execution and can
        /// execute methods.
        /// </typeparam>
        /// <param name="element">
        /// The element to attached the binding to.
        /// </param>
        /// <param name="command">
        /// The command that the specified execution handler will be bound to.
        /// </param>
        /// <param name="executeHandler">
        /// The handler that gets called once the specified command is executed.
        /// </param>
        public static void AddBindingAsync<T>(
            UIElement element,
            ICommand command,
            ExecuteCommandDelegate<T> executeHandler)
        {
            AddBindingAsync<T>(element, command, executeHandler, null);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RockstarCommandBinding{T}"/> class.
        /// </summary>
        /// <typeparam name="T">
        /// The type of command parameter that should be past into the execution and can
        /// execute methods.
        /// </typeparam>
        /// <param name="element">
        /// The element to attached the binding to.
        /// </param>
        /// <param name="command">
        /// The command that the specified execution handler will be bound to.
        /// </param>
        /// <param name="executeHandler">
        /// The handler that gets called once the specified command is executed.
        /// </param>
        /// <param name="canExecuteHandler">
        /// The handler that gets called to determine whether the command can be executed.
        /// </param>
        public static void AddBindingAsync<T>(
            UIElement element,
            ICommand command,
            ExecuteCommandDelegate<T> executeHandler,
            CanExecuteCommandDelegate<T> canExecuteHandler)
        {
            RockstarCommandBinding<T> binding = new RockstarCommandBinding<T>(
                command, executeHandler, canExecuteHandler, true);
            element.CommandBindings.Add(binding);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RockstarCommandBinding"/> class.
        /// </summary>
        /// <param name="type">
        /// The class with which to register the command binding to.
        /// </param>
        /// <param name="command">
        /// The command that the specified execution handler will be bound to.
        /// </param>
        /// <param name="executeHandler">
        /// The handler that gets called once the specified command is executed.
        /// </param>
        public static void AddBindingAsync(
            Type type,
            ICommand command,
            ExecuteCommandDelegate executeHandler)
        {
            AddBindingAsync(type, command, executeHandler, null);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RockstarCommandBinding"/> class.
        /// </summary>
        /// <typeparam name="T">
        /// The type of command parameter that should be past into the execution and can
        /// execute methods.
        /// </typeparam>
        /// <param name="type">
        /// The class with which to register the command binding to.
        /// </param>
        /// <param name="command">
        /// The command that the specified execution handler will be bound to.
        /// </param>
        /// <param name="executeHandler">
        /// The handler that gets called once the specified command is executed.
        /// </param>
        public static void AddBindingAsync<T>(
            Type type,
            ICommand command,
            ExecuteCommandDelegate<T> executeHandler)
        {
            AddBindingAsync<T>(type, command, executeHandler, null);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RockstarCommandBinding"/> class.
        /// </summary>
        /// <param name="type">
        /// The class with which to register the command binding to.
        /// </param>
        /// <param name="command">
        /// The command that the specified execution handler will be bound to.
        /// </param>
        /// <param name="executeHandler">
        /// The handler that gets called once the specified command is executed.
        /// </param>
        /// <param name="canExecuteHandler">
        /// The handler that gets called to determine whether the command can be executed.
        /// </param>
        public static void AddBindingAsync(
            Type type,
            ICommand command,
            ExecuteCommandDelegate executeHandler,
            CanExecuteCommandDelegate canExecuteHandler)
        {
            RockstarCommandBinding binding = new RockstarCommandBinding(
                command, executeHandler, canExecuteHandler, true);
            CommandManager.RegisterClassCommandBinding(type, binding);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RockstarCommandBinding"/> class.
        /// </summary>
        /// <typeparam name="T">
        /// The type of command parameter that should be past into the execution and can
        /// execute methods.
        /// </typeparam>
        /// <param name="type">
        /// The class with which to register the command binding to.
        /// </param>
        /// <param name="command">
        /// The command that the specified execution handler will be bound to.
        /// </param>
        /// <param name="executeHandler">
        /// The handler that gets called once the specified command is executed.
        /// </param>
        /// <param name="canExecuteHandler">
        /// The handler that gets called to determine whether the command can be executed.
        /// </param>
        public static void AddBindingAsync<T>(
            Type type,
            ICommand command,
            ExecuteCommandDelegate<T> executeHandler,
            CanExecuteCommandDelegate<T> canExecuteHandler)
        {
            RockstarCommandBinding<T> binding = new RockstarCommandBinding<T>(
                command, executeHandler, canExecuteHandler, true);
            CommandManager.RegisterClassCommandBinding(type, binding);
        }

        /// <summary>
        /// Adds the specified item into the collection of managed command bar items.
        /// </summary>
        /// <param name="item">
        /// The item to add to the collection of managed command bar items.
        /// </param>
        /// <returns>
        /// Returns the added command bar item if added successfully.
        /// </returns>
        public static CommandBarItem AddCommandBarItem(CommandBarItem item)
        {
            return AddCommandBarItem(item, true);
        }

        /// <summary>
        /// Adds the specified command definition into this manager.
        /// </summary>
        /// <param name="definition">
        /// The command definition to add into this manager.
        /// </param>
        public static void AddCommandDefinition(CommandDefinition definition)
        {
            lock (_syncRoot)
            {
                if (definition == null || _commandDefinitions == null)
                {
                    return;
                }

#if DEBUG
                if (!ValidateCommandDefinition(definition))
                {
                    return;
                }
#endif

                _commandDefinitions.Add(definition);
                ResolveAddedCommands(Enumerable.Repeat(definition, 1));
                CommandBarItemViewModel.OnDefinitionAdded(definition);
            }
        }

        /// <summary>
        /// Adds a new command instance item into the collection of managed command bar items.
        /// </summary>
        /// <param name="command">
        /// The command that the command bar item is instancing.
        /// </param>
        /// <param name="priority">
        /// The initial priority that the command bar item should be given.
        /// </param>
        /// <param name="startsGroup">
        /// A value indicating whether the command bar item should start a new group. (I.e.
        /// Placed directly after a separator).
        /// </param>
        /// <param name="text">
        /// The text for the command bar item.
        /// </param>
        /// <param name="id">
        /// The unique identifier that is used to reference the created item.
        /// </param>
        /// <param name="parentId">
        /// The unique identifier for the parent of the item inside the command bar hierarchy.
        /// </param>
        /// <returns>
        /// Returns the added command bar item if added successfully.
        /// </returns>
        public static CommandBarItem AddCommandInstance(
            RockstarRoutedCommand command,
            ushort priority,
            bool startsGroup,
            string text,
            Guid id,
            Guid parentId)
        {
            var item = new CommandInstance(command, priority, startsGroup, text, id, parentId);
            return AddCommandBarItem(item, true);
        }

        /// <summary>
        /// Adds a new command instance item into the collection of managed command bar items.
        /// </summary>
        /// <param name="command">
        /// The command that the command bar item is instancing.
        /// </param>
        /// <param name="priority">
        /// The initial priority that the command bar item should be given.
        /// </param>
        /// <param name="startsGroup">
        /// A value indicating whether the command bar item should start a new group. (I.e.
        /// Placed directly after a separator).
        /// </param>
        /// <param name="text">
        /// The text for the command bar item.
        /// </param>
        /// <param name="id">
        /// The unique identifier that is used to reference the created item.
        /// </param>
        /// <param name="parentId">
        /// The unique identifier for the parent of the item inside the command bar hierarchy.
        /// </param>
        /// <param name="additional">
        /// A argument class that contains additional arguments that can be added to the
        /// created command instance.
        /// </param>
        /// <returns>
        /// Returns the added command bar item if added successfully.
        /// </returns>
        public static CommandBarItem AddCommandInstance(
            RockstarRoutedCommand command,
            ushort priority,
            bool startsGroup,
            string text,
            Guid id,
            Guid parentId,
            CommandInstanceCreationArgs additional)
        {
            var item = new CommandInstance(command, priority, startsGroup, text, id, parentId);
            item.AreMultiItemsShown = additional.AreMultiItemsShown;
            item.DisplayStyle = additional.DisplayStyle;
            item.EachItemStartsGroup = additional.EachItemStartsGroup;
            item.ShowFilterCount = additional.ShowFilterCount;
            item.Width = additional.Width;
            return AddCommandBarItem(item, true);
        }

        /// <summary>
        /// Adds the edit menu into the collection of managed command bar items.
        /// </summary>
        /// <param name="id">
        /// The unique identifier that is used to reference the edit menu.
        /// </param>
        public static void AddEditMenu(Guid id)
        {
            EditTopLevelMenu item = new EditTopLevelMenu(id);
            AddCommandBarItem(item, true);
        }

        /// <summary>
        /// Adds the file menu into the collection of managed command bar items.
        /// </summary>
        /// <param name="id">
        /// The unique identifier that is used to reference the file menu.
        /// </param>
        public static void AddFileMenu(Guid id)
        {
            FileTopLevelMenu item = new FileTopLevelMenu(id);
            AddCommandBarItem(item, true);
        }

        /// <summary>
        /// Adds the help menu into the collection of managed command bar items.
        /// </summary>
        /// <param name="id">
        /// The unique identifier that is used to reference the help menu.
        /// </param>
        public static void AddHelpMenu(Guid id)
        {
            HelpTopLevelMenu item = new HelpTopLevelMenu(id);
            AddCommandBarItem(item, true);
        }

        /// <summary>
        /// Adds a new menu item into the collection of managed command bar items.
        /// </summary>
        /// <param name="priority">
        /// The initial priority that this command bar item should be given.
        /// </param>
        /// <param name="text">
        /// The text for this command bar item.
        /// </param>
        /// <param name="id">
        /// The unique identifier that is used to reference this item.
        /// </param>
        /// <param name="parentId">
        /// The unique identifier for the parent of this item inside the command bar hierarchy.
        /// </param>
        /// <returns>
        /// Returns the added command bar item if added successfully.
        /// </returns>
        public static CommandBarItem AddMenu(
            ushort priority, string text, Guid id, Guid parentId)
        {
            ChildMenu item = new ChildMenu(priority, false, text, id, parentId);
            return AddCommandBarItem(item, true);
        }

        /// <summary>
        /// Adds a new menu item into the collection of managed command bar items.
        /// </summary>
        /// <param name="priority">
        /// The initial priority that this command bar item should be given.
        /// </param>
        /// <param name="startsGroup">
        /// A value indicating whether this menu should start a new group. (I.e. Placed
        /// directly after a separator).
        /// </param>
        /// <param name="text">
        /// The text for this command bar item.
        /// </param>
        /// <param name="id">
        /// The unique identifier that is used to reference this item.
        /// </param>
        /// <param name="parentId">
        /// The unique identifier for the parent of this item inside the command bar hierarchy.
        /// </param>
        /// <returns>
        /// Returns the added command bar item if added successfully.
        /// </returns>
        public static CommandBarItem AddMenu(
            ushort priority, bool startsGroup, string text, Guid id, Guid parentId)
        {
            ChildMenu item = new ChildMenu(priority, startsGroup, text, id, parentId);
            return AddCommandBarItem(item, true);
        }

        /// <summary>
        /// Adds the project menu into the collection of managed command bar items.
        /// </summary>
        /// <param name="id">
        /// The unique identifier that is used to reference the help menu.
        /// </param>
        public static void AddProjectMenu(Guid id)
        {
            ProjectTopLevelMenu item = new ProjectTopLevelMenu(id);
            AddCommandBarItem(item, true);
        }

        /// <summary>
        /// Adds a new toolbar into this manager.
        /// </summary>
        /// <param name="band">
        /// Where the toolbar should be located in the ToolBarTray.
        /// </param>
        /// <param name="bandIndex">
        /// The position of the toolbar on the band.
        /// </param>
        /// <param name="text">
        /// The text for this command bar item.
        /// </param>
        /// <param name="id">
        /// The unique identifier that is used to reference this item.
        /// </param>
        public static void AddToolbar(int band, int bandIndex, string text, Guid id)
        {
            CommandToolBar item = new CommandToolBar(band, bandIndex, text, id);
            AddCommandBarItem(item, true);
        }

        /// <summary>
        /// Adds a new top level menu into this manager.
        /// </summary>
        /// <param name="priority">
        /// The initial priority that this command bar item should be given.
        /// </param>
        /// <param name="text">
        /// The text for this command bar item.
        /// </param>
        /// <param name="id">
        /// The unique identifier that is used to reference this item.
        /// </param>
        public static void AddTopLevelMenu(ushort priority, string text, Guid id)
        {
            TopLevelMenu item = new TopLevelMenu(priority, text, id);
            AddCommandBarItem(item, true);
        }

        /// <summary>
        /// Adds the view menu into the collection of managed command bar items.
        /// </summary>
        /// <param name="id">
        /// The unique identifier that is used to reference the view menu.
        /// </param>
        public static void AddViewMenu(Guid id)
        {
            ViewTopLevelMenu item = new ViewTopLevelMenu(id);
            AddCommandBarItem(item, true);
        }

        /// <summary>
        /// Clears all of the current modifications for the command system with a option
        /// specifying whether or not they should be available in the next session.
        /// </summary>
        /// <param name="permanently">
        /// A value indicating whether the current modifications should be available during the
        /// next session.
        /// </param>
        public static void ClearModifications(bool permanently)
        {
            using (DelayPriorityChanges delayer = new DelayPriorityChanges())
            {
                foreach (IUserCreatedCommandBarItem addedItem in _addedCommandBarItems)
                {
                    CommandBarItem item = addedItem as CommandBarItem;
                    if (item != null)
                    {
                        RemoveCommandBarItem(item);
                    }
                }

                foreach (CommandBarItem removed in _removedCommandBarItems)
                {
                    if (removed != null)
                    {
                        AddCommandBarItem(removed, false);
                    }
                }

                foreach (CommandBarItem item in _commandBarItems)
                {
                    HandleModifications(item, _originalValues);
                }

                if (permanently)
                {
                    _modifications.Clear();
                    _originalValues.Clear();
                    _removedCommandBarItemIds.Clear();
                    _removedCommandBarItems.Clear();
                    _addedCommandBarItems.Clear();
                }
            }
        }

        /// <summary>
        /// Clears the modifications off any items whose parents identifier is specified.
        /// </summary>
        /// <param name="parentId">
        /// The identifier to the command bar item whose children are to be reset.
        /// </param>
        public static void ClearModifications(Guid parentId)
        {
            using (DelayPriorityChanges delayer = new DelayPriorityChanges())
            {
                var removedAddedItems = new List<IUserCreatedCommandBarItem>();
                foreach (IUserCreatedCommandBarItem addedItem in _addedCommandBarItems)
                {
                    CommandBarItem item = addedItem as CommandBarItem;
                    if (item != null && item.ParentId == parentId)
                    {
                        RemoveCommandBarItem(item);
                        removedAddedItems.Add(addedItem);
                    }
                }

                var addedRemovedItems = new List<CommandBarItem>();
                foreach (CommandBarItem removed in _removedCommandBarItems)
                {
                    if (removed != null && removed.ParentId == parentId)
                    {
                        AddCommandBarItem(removed, false);
                        addedRemovedItems.Add(removed);
                    }
                }

                var handledItems = new List<CommandBarItem>();
                foreach (CommandBarItem item in _commandBarItems)
                {
                    if (item != null && item.ParentId == parentId)
                    {
                        HandleModifications(item, _originalValues);
                        handledItems.Add(item);
                    }
                }

                foreach (CommandBarItem item in handledItems)
                {
                    _modifications.Remove(item.Id);
                    _originalValues.Remove(item.Id);
                }

                foreach (CommandBarItem item in addedRemovedItems)
                {
                    _removedCommandBarItemIds.Remove(item.Id);
                    _removedCommandBarItems.Remove(item);
                }

                foreach (IUserCreatedCommandBarItem item in removedAddedItems)
                {
                    _addedCommandBarItems.Remove(item);
                }
            }
        }

        /// <summary>
        /// Gets the command definition that is wrapping the specified command.
        /// </summary>
        /// <param name="command">
        /// The command whose definition should be retrieved.
        /// </param>
        /// <returns>
        /// The command definition that is wrapping the specified command.
        /// </returns>
        public static CommandDefinition GetDefinition(ICommand command)
        {
            lock (_syncRoot)
            {
                if (command == null)
                {
                    return null;
                }

                foreach (CommandDefinition definition in _commandDefinitions)
                {
                    if (Object.ReferenceEquals(definition.Command, command))
                    {
                        return definition;
                    }
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the command definitions that are inside the specified category.
        /// </summary>
        /// <param name="category">
        /// The category whose definitions should be retrieved.
        /// </param>
        /// <returns>
        /// The command definitions that are inside the specified category.
        /// </returns>
        public static IEnumerable<CommandDefinition> GetDefinitions(string category)
        {
            lock (_syncRoot)
            {
                if (_commandDefinitions == null)
                {
                    return Enumerable.Empty<CommandDefinition>();
                }

                return from d in _commandDefinitions
                       where d != null && String.Equals(d.CategoryName, category)
                       orderby d.Name
                       select d;
            }
        }

        /// <summary>
        /// Determines whether any of the children for the specified command bar item have had
        /// modifications placed upon them.
        /// </summary>
        /// <param name="parentId">
        /// The identifier to the command bar item whose children will be checked.
        /// </param>
        /// <returns>
        /// True if any of the children for the specified identifier have been modified.
        /// </returns>
        public static bool HasModificationsForParent(Guid parentId)
        {
            ModificationFieldValue modification = null;
            foreach (CommandBarItem item in GetCommands(parentId))
            {
                if (_modifications.TryGetValue(item.Id, out modification))
                {
                    if (modification.Count > 0)
                    {
                        return true;
                    }
                }
            }

            foreach (CommandBarItem removedItem in _removedCommandBarItems)
            {
                if (removedItem.ParentId == parentId)
                {
                    return true;
                }
            }

            foreach (IUserCreatedCommandBarItem addedItem in _addedCommandBarItems)
            {
                CommandBarItem item = addedItem as CommandBarItem;
                if (item != null && item.ParentId == parentId)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Registers a modification made to a command bar item for a specified field.
        /// </summary>
        /// <param name="item">
        /// The item that has been modified.
        /// </param>
        /// <param name="field">
        /// The field that has been modified on the specified item.
        /// </param>
        /// <param name="oldValue">
        /// The old value of the field before the modification was made.
        /// </param>
        public static void RegisterModification(
            CommandBarItem item, ModificationField field, string oldValue)
        {
            if (_modifications == null)
            {
                return;
            }

            string value = null;
            switch (field)
            {
                case ModificationField.AreMultiItemsShown:
                    value = item.AreMultiItemsShown.ToString();
                    break;

                case ModificationField.DisplayStyle:
                    {
                        CommandInstance instance = item as CommandInstance;
                        if (instance != null)
                        {
                            value = instance.DisplayStyle.ToString();
                        }
                        else
                        {
                            value = CommandItemDisplayStyle.Default.ToString();
                        }
                    }

                    break;

                case ModificationField.EachItemStartsGroup:
                    value = item.EachItemStartsGroup.ToString();
                    break;

                case ModificationField.Priority:
                    value = item.Priority.ToStringInvariant();
                    break;

                case ModificationField.ShowFilterCount:
                    value = item.ShowFilterCount.ToString();
                    break;

                case ModificationField.StartsGroup:
                    value = item.StartsGroup.ToString();
                    break;

                case ModificationField.Text:
                    value = item.Text;
                    break;

                case ModificationField.Visibility:
                    value = item.IsVisible.ToString();
                    break;

                case ModificationField.Width:
                    value = item.Width.ToStringInvariant();
                    break;

                default:
                    Debug.Assert(false, "Unrecognised modification field");
                    return;
            }

            ModificationFieldValue original = null;
            string originalValue = null;
            if (!_originalValues.TryGetValue(item.Id, out original))
            {
                original = new ModificationFieldValue();
                _originalValues.Add(item.Id, original);
                original[field] = oldValue;
                originalValue = oldValue;
            }
            else
            {
                if (!original.TryGetValue(field, out originalValue))
                {
                    original[field] = oldValue;
                    originalValue = oldValue;
                }
            }

            bool remove = false;
            if (String.Equals(originalValue, value))
            {
                remove = true;
            }

            ModificationFieldValue modification = null;
            if (!_modifications.TryGetValue(item.Id, out modification))
            {
                modification = new ModificationFieldValue();
                _modifications.Add(item.Id, modification);
            }

            if (remove)
            {
                modification.Remove(field);
                if (modification.Count == 0)
                {
                    _modifications.Remove(item.Id);
                }
            }
            else
            {
                modification[field] = value;
            }
        }

        /// <summary>
        /// Removes the first occurrence of the specified item from the collection of managed
        /// command bar items.
        /// </summary>
        /// <param name="item">
        /// The item to remove from the collection of managed command bar items.
        /// </param>
        public static void RemoveCommandBarItem(CommandBarItem item)
        {
            lock (_syncRoot)
            {
                if (item == null || _commandBarItems == null)
                {
                    return;
                }

                if (_allCommandBarItems.Contains(item))
                {
                    _allCommandBarItems.Remove(item);
                }

                if (_commandBarItems.Contains(item))
                {
                    _commandBarItems.Remove(item);
                    if (!_commandBarItems.Contains(item))
                    {
                        CommandBarItemViewModel.OnItemRemoved(item, true);
                    }
                }
            }
        }

        /// <summary>
        /// Update the text property on all of the command instances that represent the
        /// specified command.
        /// </summary>
        /// <param name="command">
        /// The command of the instances to update.
        /// </param>
        /// <param name="updatedText">
        /// The text that should replace the existing text.
        /// </param>
        public static void UpdateItemText(ICommand command, string updatedText)
        {
            foreach (CommandInstance item in GetInstances(command))
            {
                item.UpdateTextValue(updatedText);
            }
        }

        /// <summary>
        /// Adds the current value for the specified items priority to the original mapping
        /// field.
        /// </summary>
        /// <param name="item">
        /// The item whose priority value should be stored in the original mapping field.
        /// </param>
        internal static void AddOriginalPriorityValue(CommandBarItem item)
        {
            ModificationFieldValue original = null;
            ModificationField field = ModificationField.Priority;
            string originalValue = item.Priority.ToStringInvariant();
            if (!_originalValues.TryGetValue(item.Id, out original))
            {
                original = new ModificationFieldValue();
                _originalValues.Add(item.Id, original);
                original[field] = originalValue;
            }
            else
            {
                string dummyValue;
                if (!original.TryGetValue(field, out dummyValue))
                {
                    original[field] = originalValue;
                }
            }
        }

        /// <summary>
        /// Deserialises the commands using the data contained within the specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the command data.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        internal static void DeserialiseCommands(XmlReader reader, IFormatProvider provider)
        {
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                if (!reader.IsStartElement())
                {
                    reader.Read();
                    continue;
                }

                if (String.Equals(reader.Name, "Modification"))
                {
                    string idString = reader.GetAttribute("id");
                    Guid id;
                    bool parsed = Guid.TryParse(idString, out id);
                    Debug.Assert(parsed, "Unable to use modification, missing/invalid id.");
                    if (!parsed)
                    {
                        Debug.Assert(false, "Failed to load a modification as id is invalid.");
                        reader.Skip();
                        continue;
                    }

                    bool alreadyPresent = _modifications.ContainsKey(id);
                    if (alreadyPresent)
                    {
                        Debug.Assert(!alreadyPresent, "Duplicate modification detected.");
                        reader.Skip();
                        continue;
                    }

                    var modification = new ModificationFieldValue();
                    _modifications.Add(id, modification);
                    foreach (var modificationXmlName in _modificationXmlNames)
                    {
                        string value = reader.GetAttribute(modificationXmlName.Value);
                        if (value == null)
                        {
                            continue;
                        }

                        modification.Add(modificationXmlName.Key, value);
                    }

                    reader.Skip();
                    continue;
                }
                else if (String.Equals(reader.Name, "AddedCommand"))
                {
                    var instance = new UserCreatedCommandInstance(reader, provider);
                    _addedCommandBarItems.Add(instance);
                }
                else if (String.Equals(reader.Name, "AddedMenu"))
                {
                    UserCreateCommandMenu menu = new UserCreateCommandMenu(reader, provider);
                    _addedCommandBarItems.Add(menu);
                    AddCommandBarItem(menu);
                }
                else if (String.Equals(reader.Name, "AddedToolbar"))
                {
                    var toolbar = new UserCreateCommandToolbar(reader, provider);
                    _addedCommandBarItems.Add(toolbar);
                    AddCommandBarItem(toolbar);
                }
                else if (String.Equals(reader.Name, "Removed"))
                {
                    string idString = reader.ReadElementContentAsString();
                    Guid id;
                    bool parsed = Guid.TryParseExact(idString, "D", out id);
                    if (parsed)
                    {
                        _removedCommandBarItemIds.Add(id);
                    }
                    else
                    {
                        Debug.Assert(false, "Failed to load a removed items id.");
                        reader.Skip();
                        continue;
                    }
                }

                reader.Read();
                continue;
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }

            foreach (CommandBarItem item in _commandBarItems)
            {
                HandleModifications(item, _modifications);
            }

            foreach (Guid id in _removedCommandBarItemIds)
            {
                CommandBarItem item = GetCommandById(id);
                if (item != null)
                {
                    _removedCommandBarItems.Add(item);
                    RemoveCommandBarItem(item);
                }
            }

            lock (_syncRoot)
            {
                ResolveAddedCommands(_commandDefinitions);
            }
        }

        /// <summary>
        /// Retrieves a iterator over the collection of command bar items that have their
        /// parent id set to the specified id.
        /// </summary>
        /// <param name="parentId">
        /// The id whose children will be part of the retrieved iterator.
        /// </param>
        /// <returns>
        /// A iterator over the collection of command bar items that have their parent id set
        /// to the specified id.
        /// </returns>
        internal static IEnumerable<CommandBarItem> GetCommands(Guid parentId)
        {
            if (parentId == RockstarCommandManager.ToolBarTrayId)
            {
                lock (_syncRoot)
                {
                    if (_commandBarItems == null)
                    {
                        return Enumerable.Empty<CommandBarItem>();
                    }

                    return from i in _commandBarItems.OfType<CommandToolBar>()
                           where i != null
                           where i.ParentId == parentId
                           orderby i.Band
                           orderby i.BandIndex
                           select i;
                }
            }
            else
            {
                lock (_syncRoot)
                {
                    if (_commandBarItems == null)
                    {
                        return Enumerable.Empty<CommandBarItem>();
                    }

                    return from i in _commandBarItems
                           where i != null
                           where i.ParentId == parentId
                           orderby i.Priority
                           select i;
                }
            }
        }

        /// <summary>
        /// Retrieves a iterator over the collection of command bar items that have their
        /// parent id set to the specified id.
        /// </summary>
        /// <param name="parentId">
        /// The id whose children will be part of the retrieved iterator.
        /// </param>
        /// <returns>
        /// A iterator over the collection of command bar items that have their parent id set
        /// to the specified id.
        /// </returns>
        internal static IEnumerable<CommandBarItem> GetCommandsWithRemovals(Guid parentId)
        {
            if (parentId == RockstarCommandManager.ToolBarTrayId)
            {
                lock (_syncRoot)
                {
                    if (_allCommandBarItems == null)
                    {
                        return Enumerable.Empty<CommandBarItem>();
                    }

                    return from i in _allCommandBarItems.OfType<CommandToolBar>()
                           where i != null
                           where i.ParentId == parentId
                           orderby i.Band
                           orderby i.BandIndex
                           select i;
                }
            }
            else
            {
                lock (_syncRoot)
                {
                    if (_commandBarItems == null)
                    {
                        return Enumerable.Empty<CommandBarItem>();
                    }

                    return from i in _allCommandBarItems
                           where i != null
                           where i.ParentId == parentId
                           orderby i.Priority
                           select i;
                }
            }
        }

        /// <summary>
        /// Gets a iterator around all of the command instances for the specified definition.
        /// </summary>
        /// <param name="definition">
        /// The definition whose instances are iterated through.
        /// </param>
        /// <returns>
        /// A iterator around all of the command instances for the specified definition.
        /// </returns>
        internal static IEnumerable<CommandInstance> GetInstances(CommandDefinition definition)
        {
            if (definition == null)
            {
                yield break;
            }

            foreach (CommandBarItem item in _commandBarItems)
            {
                CommandInstance instance = item as CommandInstance;
                if (instance == null)
                {
                    continue;
                }

                if (!Object.ReferenceEquals(instance.Command, definition.Command))
                {
                    continue;
                }

                yield return instance;
            }
        }

        /// <summary>
        /// Gets a list containing the command instances for the specified command.
        /// </summary>
        /// <param name="command">
        /// The command whose instances are iterated through.
        /// </param>
        /// <returns>
        /// A list containing the command instances for the specified command.
        /// </returns>
        internal static List<CommandInstance> GetInstances(ICommand command)
        {
            if (command == null)
            {
                return new List<CommandInstance>();
            }

            int count = _commandBarItems.Count;
            List<CommandInstance> instances = new List<CommandInstance>(count);
            for (int i = 0; i < count; i++)
            {
                CommandInstance instance = _commandBarItems[i] as CommandInstance;
                if (instance == null)
                {
                    continue;
                }

                if (!Object.ReferenceEquals(instance.Command, command))
                {
                    continue;
                }

                instances.Add(instance);
            }

            return instances;
        }

        /// <summary>
        /// Retrieves the command parameter to use based on the specified gesture and
        /// command. This looks through all command instances and determines whether any of
        /// them that instance the specified command have overridden the gesture and parameter
        /// properties.
        /// </summary>
        /// <typeparam name="T">
        /// The type that the command parameter should be returned in.
        /// </typeparam>
        /// <param name="gesture">
        /// The gesture that should be associated with the retrieved parameter.
        /// </param>
        /// <param name="command">
        /// The command object that should be associated with the retrieved parameter.
        /// </param>
        /// <returns>
        /// The command parameter that is defined for the specified gesture and command.
        /// </returns>
        internal static T GetParameter<T>(KeyGesture gesture, ICommand command)
        {
            CommandDefinition definition = null;
            lock (_syncRoot)
            {
                definition = GetDefinition(command);
                if (definition == null)
                {
                    return default(T);
                }
            }

            ButtonCommand buttonCommand = definition as ButtonCommand;
            if (buttonCommand != null)
            {
                ICommandWithParameter parameterCommand = definition as ICommandWithParameter;
                if (parameterCommand != null)
                {
                    return (T)parameterCommand.CommandParameter;
                }

                return default(T);
            }

            ToggleButtonCommand toggleCommand = definition as ToggleButtonCommand;
            if (toggleCommand != null)
            {
                return (T)((object)!toggleCommand.IsToggled);
            }

            MultiCommand multiCommand = definition as MultiCommand;
            KeyGestureComparer comparer = new KeyGestureComparer();
            if (multiCommand != null && gesture != null)
            {
                foreach (IMultiCommandItem item in multiCommand.Items)
                {
                    if (comparer.Equals(item.KeyGesture, gesture))
                    {
                        T parameter = (T)item.GetCommandParameter();
                        var multiParameter = parameter as IMultiCommandParameter;
                        if (multiParameter != null && gesture != null)
                        {
                            multiParameter.IsToggled = !multiParameter.IsToggled;
                        }

                        return parameter;
                    }
                }

                return default(T);
            }

            return default(T);
        }

        /// <summary>
        /// Retrieves a iterator over the collection of command tool bar items.
        /// </summary>
        /// <returns>
        /// A iterator over the collection of command tool bar items.
        /// </returns>
        internal static IEnumerable<CommandToolBar> GetToolbars()
        {
            lock (_syncRoot)
            {
                if (_commandBarItems == null)
                {
                    return Enumerable.Empty<CommandToolBar>();
                }

                return from i in _commandBarItems.OfType<CommandToolBar>()
                       where i != null
                       where i.ParentId == RockstarCommandManager.ToolBarTrayId
                       orderby i.Text
                       select i;
            }
        }

        /// <summary>
        /// Registers a command bar item has been added by the user to this manager.
        /// </summary>
        /// <param name="addedCommand">
        /// The item that has been added.
        /// </param>
        internal static void RegisterCommandAdded(IUserCreatedCommandBarItem addedCommand)
        {
            _addedCommandBarItems.Add(addedCommand);
        }

        /// <summary>
        /// Registers a command bar item has been removed by the user to this manager.
        /// </summary>
        /// <param name="removedCommand">
        /// The item that has been removed.
        /// </param>
        internal static void RegisterCommandRemoved(CommandBarItem removedCommand)
        {
            bool contained = _removedCommandBarItemIds.Contains(removedCommand.Id);
            if (contained)
            {
                Debug.Assert(!contained, "Attempted to remove the same item multiple times.");
                return;
            }

            var userCreated = removedCommand as IUserCreatedCommandBarItem;
            if (userCreated != null)
            {
                _addedCommandBarItems.Remove(userCreated);
                return;
            }

            _removedCommandBarItems.Add(removedCommand);
            _removedCommandBarItemIds.Add(removedCommand.Id);
        }

        /// <summary>
        /// Serialises the commands into the specified xml writer.
        /// </summary>
        /// <param name="writer">
        /// The xml writer that the commands gets serialised out to.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        internal static void SerialiseCommands(XmlWriter writer, IFormatProvider provider)
        {
            if (_modifications == null)
            {
                return;
            }

            foreach (var modification in _modifications)
            {
                writer.WriteStartElement("Modification");
                writer.WriteAttributeString("id", modification.Key.ToString());

                foreach (var value in modification.Value)
                {
                    string fieldName = null;
                    _modificationXmlNames.TryGetValue(value.Key, out fieldName);
                    if (fieldName == null)
                    {
                        Debug.Assert(fieldName != null, "Unrecognised modified field.");
                        continue;
                    }

                    writer.WriteAttributeString(fieldName, value.Value);
                }

                writer.WriteEndElement();
            }

            foreach (IUserCreatedCommandBarItem added in _addedCommandBarItems)
            {
                if (added is UserCreatedCommandInstance)
                {
                    writer.WriteStartElement("AddedCommand");
                }
                else if (added is UserCreateCommandMenu)
                {
                    writer.WriteStartElement("AddedMenu");
                }
                else if (added is UserCreateCommandToolbar)
                {
                    writer.WriteStartElement("AddedToolbar");
                }
                else
                {
                    writer.WriteStartElement("Added");
                }

                added.SerialiseCommand(writer, provider);
                writer.WriteEndElement();
            }

            foreach (Guid deleted in _removedCommandBarItemIds)
            {
                writer.WriteElementString("Removed", deleted.ToString("D"));
            }
        }

        /// <summary>
        /// Adds the specified item into the collection of managed command bar items.
        /// </summary>
        /// <param name="item">
        /// The item to add to the collection of managed command bar items.
        /// </param>
        /// <param name="checkRemovedItems">
        /// A value indicating whether before the item is added it is checked against the
        /// removed items id list.
        /// </param>
        /// <returns>
        /// Returns the added command bar item if added successfully.
        /// </returns>
        private static CommandBarItem AddCommandBarItem(
            CommandBarItem item, bool checkRemovedItems)
        {
            lock (_syncRoot)
            {
                if (_commandBarItems == null)
                {
                    return item;
                }

#if DEBUG
                if (!ValidateCommandBarItem(item))
                {
                    return item;
                }
#endif
                if (!_commandBarItems.Contains(item))
                {
                    HandleModifications(item, _modifications);
                    if (!checkRemovedItems || !_removedCommandBarItemIds.Contains(item.Id))
                    {
                        _commandBarItems.Add(item);
                    }

                    if (!_allCommandBarItems.Contains(item))
                    {
                        _allCommandBarItems.Add(item);
                    }

                    if (_commandBarItems.Contains(item))
                    {
                        CommandBarItemViewModel.OnItemAdded(item);
                    }
                }
            }

            return item;
        }

        /// <summary>
        /// Determines whether RockstarCommands.Copy command can be fired from the current
        /// position.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The execute data that has been sent with the command.
        /// </param>
        private static void CanTextBoxCopy(object s, CanExecuteRoutedEventArgs e)
        {
            IInputElement target = e.OriginalSource as IInputElement;
            if (target == null)
            {
                return;
            }

            e.CanExecute = ApplicationCommands.Copy.CanExecute(e.Parameter, target);
        }

        /// <summary>
        /// Determines whether RockstarCommands.Cut command can be fired from the current
        /// position.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The execute data that has been sent with the command.
        /// </param>
        private static void CanTextBoxCut(object s, CanExecuteRoutedEventArgs e)
        {
            IInputElement target = e.OriginalSource as IInputElement;
            if (target == null)
            {
                return;
            }

            e.CanExecute = ApplicationCommands.Cut.CanExecute(e.Parameter, target);
        }

        /// <summary>
        /// Determines whether RockstarCommands.Delete command can be fired from the current
        /// position.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The execute data that has been sent with the command.
        /// </param>
        private static void CanTextBoxDelete(object s, CanExecuteRoutedEventArgs e)
        {
            IInputElement target = e.OriginalSource as IInputElement;
            if (target == null)
            {
                return;
            }

            e.CanExecute = ApplicationCommands.Delete.CanExecute(e.Parameter, target);
        }

        /// <summary>
        /// Determines whether RockstarCommands.Paste command can be fired from the current
        /// position.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The execute data that has been sent with the command.
        /// </param>
        private static void CanTextBoxPaste(object s, CanExecuteRoutedEventArgs e)
        {
            IInputElement target = e.OriginalSource as IInputElement;
            if (target == null)
            {
                return;
            }

            e.CanExecute = ApplicationCommands.Paste.CanExecute(e.Parameter, target);
        }

        /// <summary>
        /// Retrieves the command bar item instance with the specified id if found; otherwise,
        /// null.
        /// </summary>
        /// <param name="id">
        /// The id of the command bar item to retrieve.
        /// </param>
        /// <returns>
        /// The command bar item instance with the specified id if found; otherwise, null.
        /// </returns>
        private static CommandBarItem GetCommandById(Guid id)
        {
            lock (_syncRoot)
            {
                if (_commandBarItems != null)
                {
                    foreach (CommandBarItem commandBarItem in _commandBarItems)
                    {
                        if (Guid.Equals(id, commandBarItem.Id))
                        {
                            return commandBarItem;
                        }
                    }
                }

                if (_removedCommandBarItems != null)
                {
                    foreach (CommandBarItem commandBarItem in _removedCommandBarItems)
                    {
                        if (Guid.Equals(id, commandBarItem.Id))
                        {
                            return commandBarItem;
                        }
                    }
                }

                return null;
            }
        }

        /// <summary>
        /// Handles the modifications that have been set on for the specified item.
        /// </summary>
        /// <param name="item">
        /// The command bar item to setup the modifications on.
        /// </param>
        /// <param name="container">
        /// The container where the modification values will be coming from.
        /// </param>
        private static void HandleModifications(
            CommandBarItem item, ModificationContainer container)
        {
            ModificationFieldValue modifications = null;
            if (!container.TryGetValue(item.Id, out modifications))
            {
                return;
            }

            bool storeOriginals = !Object.ReferenceEquals(container, _originalValues);
            ModificationFieldValue original = null;
            if (!_originalValues.TryGetValue(item.Id, out original))
            {
                original = new ModificationFieldValue();
                _originalValues.Add(item.Id, original);
            }

            foreach (KeyValuePair<ModificationField, string> modification in modifications)
            {
                string originalValue = null;
                string oldValue = null;
                switch (modification.Key)
                {
                    case ModificationField.AreMultiItemsShown:
                        {
                            bool result;
                            if (bool.TryParse(modification.Value, out result))
                            {
                                oldValue = item.AreMultiItemsShown.ToString();
                                item.AreMultiItemsShown = result;
                            }
                        }

                        break;

                    case ModificationField.DisplayStyle:
                        {
                            CommandItemDisplayStyle result;
                            string value = modification.Value;
                            if (Enum.TryParse<CommandItemDisplayStyle>(value, out result))
                            {
                                CommandInstance instance = item as CommandInstance;
                                if (instance != null)
                                {
                                    oldValue = instance.DisplayStyle.ToString();
                                    instance.DisplayStyle = result;
                                }
                            }
                        }

                        break;

                    case ModificationField.EachItemStartsGroup:
                        {
                            bool result;
                            if (bool.TryParse(modification.Value, out result))
                            {
                                oldValue = item.EachItemStartsGroup.ToString();
                                item.EachItemStartsGroup = result;
                            }
                        }

                        break;

                    case ModificationField.Priority:
                        {
                            ushort result;
                            if (ushort.TryParse(modification.Value, out result))
                            {
                                oldValue = item.Priority.ToStringInvariant();
                                item.Priority = result;
                            }
                        }

                        break;

                    case ModificationField.ShowFilterCount:
                        {
                            bool result;
                            if (bool.TryParse(modification.Value, out result))
                            {
                                oldValue = item.ShowFilterCount.ToString();
                                item.ShowFilterCount = result;
                            }
                        }

                        break;

                    case ModificationField.StartsGroup:
                        {
                            bool result;
                            if (bool.TryParse(modification.Value, out result))
                            {
                                oldValue = item.StartsGroup.ToString();
                                item.StartsGroup = result;
                            }
                        }

                        break;

                    case ModificationField.Text:
                        {
                            oldValue = item.Text;
                            item.Text = modification.Value;
                        }

                        break;

                    case ModificationField.Visibility:
                        {
                            bool result;
                            if (bool.TryParse(modification.Value, out result))
                            {
                                oldValue = item.IsVisible.ToString();
                                item.IsVisible = result;
                            }
                        }

                        break;

                    case ModificationField.Width:
                        {
                            int result;
                            if (int.TryParse(modification.Value, out result))
                            {
                                oldValue = item.Width.ToStringInvariant();
                                item.Width = result;
                            }
                        }

                        break;
                }

                if (storeOriginals && oldValue != null)
                {
                    if (!original.TryGetValue(modification.Key, out originalValue))
                    {
                        original[modification.Key] = oldValue;
                    }
                }
            }
        }

        /// <summary>
        /// Called whenever the RockstarCommands.Copy command is fired and caught by a text
        /// box.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The execute data that has been sent with the command.
        /// </param>
        private static void OnTextBoxCopy(object s, ExecutedRoutedEventArgs e)
        {
            IInputElement target = e.OriginalSource as IInputElement;
            if (target == null)
            {
                return;
            }

            if (ApplicationCommands.Copy.CanExecute(e.Parameter, target))
            {
                ApplicationCommands.Copy.Execute(e.Parameter, target);
            }
        }

        /// <summary>
        /// Called whenever the RockstarCommands.Cut command is fired and caught by a text
        /// box.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The execute data that has been sent with the command.
        /// </param>
        private static void OnTextBoxCut(object s, ExecutedRoutedEventArgs e)
        {
            IInputElement target = e.OriginalSource as IInputElement;
            if (target == null)
            {
                return;
            }

            if (ApplicationCommands.Cut.CanExecute(e.Parameter, target))
            {
                ApplicationCommands.Cut.Execute(e.Parameter, target);
            }
        }

        /// <summary>
        /// Called whenever the RockstarCommands.Delete command is fired and caught by a text
        /// box.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The execute data that has been sent with the command.
        /// </param>
        private static void OnTextBoxDelete(object s, ExecutedRoutedEventArgs e)
        {
            IInputElement target = e.OriginalSource as IInputElement;
            if (target == null)
            {
                return;
            }

            if (ApplicationCommands.Delete.CanExecute(e.Parameter, target))
            {
                ApplicationCommands.Delete.Execute(e.Parameter, target);
            }
        }

        /// <summary>
        /// Called whenever the RockstarCommands.Paste command is fired and caught by a text
        /// box.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The execute data that has been sent with the command.
        /// </param>
        private static void OnTextBoxPaste(object s, ExecutedRoutedEventArgs e)
        {
            IInputElement target = e.OriginalSource as IInputElement;
            if (target == null)
            {
                return;
            }

            if (ApplicationCommands.Paste.CanExecute(e.Parameter, target))
            {
                ApplicationCommands.Paste.Execute(e.Parameter, target);
            }
        }

        /// <summary>
        /// Tries to set the command objects on all of the added commands that currently have
        /// their command set to null.
        /// </summary>
        /// <param name="definitions">
        /// The definitions to try and resolve off.
        /// </param>
        private static void ResolveAddedCommands(IEnumerable<CommandDefinition> definitions)
        {
            foreach (IUserCreatedCommandBarItem added in _addedCommandBarItems)
            {
                UserCreatedCommandInstance instance = added as UserCreatedCommandInstance;
                if (instance == null || instance.Command != null)
                {
                    continue;
                }

                foreach (CommandDefinition definition in definitions)
                {
                    if (String.Equals(definition.Command.Name, instance.CommandName))
                    {
                        instance.SetCommand(definition.Command);
                        AddCommandBarItem(instance);
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Validates the specified command bar item before it is added into this manager.
        /// </summary>
        /// <param name="commandItem">
        /// The command bar item that is validated.
        /// </param>
        /// <returns>
        /// True if the specified command item is valid and can be added to this manager;
        /// otherwise, false.
        /// </returns>
        private static bool ValidateCommandBarItem(CommandBarItem commandItem)
        {
            if (commandItem == null)
            {
                return false;
            }

            bool idExists = (from i in _commandBarItems
                             where i != null
                             where i.Id == commandItem.Id
                             select i).Any();

            ////Debug.Assert(!idExists, "Unable to add item as it has a non-unique id.");
            return !idExists;
        }

        /// <summary>
        /// Validates the specified command definition before it is added into this manager.
        /// </summary>
        /// <param name="definition">
        /// The command definition that is validated.
        /// </param>
        /// <returns>
        /// True if the specified command definition is valid and can be added to this manager;
        /// otherwise, false.
        /// </returns>
        private static bool ValidateCommandDefinition(CommandDefinition definition)
        {
            if (definition == null || _commandDefinitions == null)
            {
                return false;
            }

            bool commandExists = false;
            bool gestureExists = false;
            IEqualityComparer<KeyGesture> comparer = new KeyGestureComparer();
            foreach (CommandDefinition d in _commandDefinitions)
            {
                if (!commandExists && Object.ReferenceEquals(d.Command, definition.Command))
                {
                    commandExists = true;
                }

                KeyGesture gesture = d.KeyGesture;
                if (!gestureExists && gesture != null)
                {
                    if (comparer.Equals(definition.KeyGesture, gesture))
                    {
                        gestureExists = true;
                    }
                }
            }

            Debug.Assert(
                !gestureExists,
                "Unable to add definition as it has a non-unique key gesture assigned to it.",
                "{0}\n",
                definition.KeyGesture);

            Debug.Assert(
                !commandExists,
                "Unable to add definition as the command is already defined.",
                "{0}\n",
                definition.Command.Name);

            return !gestureExists && !commandExists;
        }
        #endregion Methods

        #region Structures
        /// <summary>
        /// A structure containing the information needed to deserialise the state for a
        /// command definition.
        /// </summary>
        private struct StateInfomation
        {
            #region Fields
            /// <summary>
            /// The private field used for the <see cref="Data"/> property.
            /// </summary>
            private string _data;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="StateInfomation"/> structure.
            /// </summary>
            /// <param name="data">
            /// The xml data that contains the stat information.
            /// </param>
            public StateInfomation(string data)
            {
                this._data = data;
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets the xml data for this state.
            /// </summary>
            public string Data
            {
                get { return this._data; }
            }
            #endregion Properties
        } // RSG.Editor.RockstarCommandManager.StateInfomation {Structure}
        #endregion Structures

        #region Classes
        /// <summary>
        /// Represents a menu that is positioned on the main menu that represents the "Project"
        /// menu. This class cannot be inherited.
        /// </summary>
        public sealed class ProjectTopLevelMenu : TopLevelMenu
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="ProjectTopLevelMenu"/> class.
            /// </summary>
            /// <param name="id">
            /// The unique identifier that is used to reference this item.
            /// </param>
            public ProjectTopLevelMenu(Guid id)
                : base(4, StringTable.TopLevelMenuProjectText, id)
            {
            }
            #endregion Constructors
        }

        /// <summary>
        /// Represents a menu that is positioned on the main menu that represents the "View"
        /// menu. This class cannot be inherited.
        /// </summary>
        public sealed class ViewTopLevelMenu : TopLevelMenu
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="ViewTopLevelMenu"/> class.
            /// </summary>
            /// <param name="id">
            /// The unique identifier that is used to reference this item.
            /// </param>
            public ViewTopLevelMenu(Guid id)
                : base(3, StringTable.TopLevelMenuViewText, id)
            {
            }
            #endregion Constructors
        } // RockstarCommandManager.ViewTopLevelMenu {Class}

 // RockstarCommandManager.ProjectTopLevelMenu {Class}

        /// <summary>
        /// Represents a menu that is positioned on the main menu that represents the "Actions"
        /// menu. This class cannot be inherited.
        /// </summary>
        private sealed class ActionsTopLevelMenu : TopLevelMenu
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="ActionsTopLevelMenu"/> class.
            /// </summary>
            /// <param name="id">
            /// The unique identifier that is used to reference this item.
            /// </param>
            public ActionsTopLevelMenu(Guid id)
                : base(2, StringTable.ActionsMenuText, id)
            {
            }
            #endregion Constructors
        } // RockstarCommandManager.ActionsTopLevelMenu {Class}

        /// <summary>
        /// Represents a menu that is positioned on the main menu that represents the "Edit"
        /// menu. This class cannot be inherited.
        /// </summary>
        private sealed class EditTopLevelMenu : TopLevelMenu
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="EditTopLevelMenu"/> class.
            /// </summary>
            /// <param name="id">
            /// The unique identifier that is used to reference this item.
            /// </param>
            public EditTopLevelMenu(Guid id)
                : base(1, StringTable.EditMenuText, id)
            {
            }
            #endregion Constructors
        } // RockstarCommandManager.EditTopLevelMenu {Class}

        /// <summary>
        /// Represents a menu that is positioned on the main menu that represents the "File"
        /// menu. This class cannot be inherited.
        /// </summary>
        private sealed class FileTopLevelMenu : TopLevelMenu
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="FileTopLevelMenu"/> class.
            /// </summary>
            /// <param name="id">
            /// The unique identifier that is used to reference this item.
            /// </param>
            public FileTopLevelMenu(Guid id)
                : base(ushort.MinValue, StringTable.FileMenuText, id)
            {
            }
            #endregion Constructors
        } // RockstarCommandManager.FileTopLevelMenu {Class}

        /// <summary>
        /// Represents a menu that is positioned on the main menu that represents the "Help"
        /// menu. This class cannot be inherited.
        /// </summary>
        private sealed class HelpTopLevelMenu : TopLevelMenu
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="HelpTopLevelMenu"/> class.
            /// </summary>
            /// <param name="id">
            /// The unique identifier that is used to reference this item.
            /// </param>
            public HelpTopLevelMenu(Guid id)
                : base(ushort.MaxValue, StringTable.HelpMenuText, id)
            {
            }
            #endregion Constructors
        } // RockstarCommandManager.HelpTopLevelMenu {Class}

        /// <summary>
        /// Represents a collection of command GUID values and their modification values.
        /// </summary>
        private class ModificationContainer : Dictionary<Guid, ModificationFieldValue>
        {
        } // RockstarCommandManager.ModificationContainer {Class}

        /// <summary>
        /// Represents a collection of modification fields and their values.
        /// </summary>
        private class ModificationFieldValue : Dictionary<ModificationField, string>
        {
        } // RockstarCommandManager.ModificationFieldValue {Class}
        #endregion Classes
    } // RSG.Editor.RockstarCommandManager {Class}
} // RSG.Editor {Namespace}
