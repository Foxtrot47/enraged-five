﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace RSG.Editor.Controls.MapViewport.Commands
{
    /// <summary>
    /// Command arguments to use with the <see cref="AddAnnotationAction"/> action.
    /// </summary>
    public class AddAnnotationCommandArgs
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="MapViewport"/> property.
        /// </summary>
        public readonly MapViewport _mapViewport;

        /// <summary>
        /// Private field for the <see cref="Position"/> property.
        /// </summary>
        private readonly Point _position;

        /// <summary>
        /// Private field for the <see cref="PositionSet"/> property.
        /// </summary>
        private readonly bool _positionSet;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="AddAnnotationCommandArgs"/>
        /// class using the specified map viewport.
        /// </summary>
        /// <param name="mapViewport"></param>
        public AddAnnotationCommandArgs(MapViewport mapViewport)
        {
            _mapViewport = mapViewport;
            _positionSet = false;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="AddAnnotationCommandArgs"/>
        /// class using the specified map viewport and position.
        /// </summary>
        /// <param name="mapViewport"></param>
        /// <param name="position"></param>
        public AddAnnotationCommandArgs(MapViewport mapViewport, Point position)
        {
            _mapViewport = mapViewport;
            _position = position;
            _positionSet = true;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the map viewport.
        /// </summary>
        public MapViewport MapViewport
        {
            get { return _mapViewport; }
        }

        /// <summary>
        /// Gets the position.
        /// </summary>
        public Point Position
        {
            get { return _position; }
        }

        /// <summary>
        /// Gets the flag indicating whether the position is set.
        /// </summary>
        public bool PositionSet
        {
            get { return _positionSet; }
        }
        #endregion
    }
}
