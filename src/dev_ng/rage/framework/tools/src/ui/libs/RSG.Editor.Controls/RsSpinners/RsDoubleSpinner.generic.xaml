﻿
<!--
    <copyright file="RsDoubleSpinner.generic.xaml" company="Rockstar">
    Copyright © Rockstar Games 2013. All rights reserved
    </copyright>
-->

<ResourceDictionary xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
                    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
                    xmlns:adp="clr-namespace:RSG.Editor.Controls.AttachedDependencyProperties"
                    xmlns:ext="clr-namespace:RSG.Editor.Controls.Extensions"
                    xmlns:helpers="clr-namespace:RSG.Editor.Controls.Helpers"
                    xmlns:local="clr-namespace:RSG.Editor.Controls">

    <ResourceDictionary.MergedDictionaries>
        <ResourceDictionary Source="/RSG.Editor.Controls;component/Geometry.xaml" />
        <ResourceDictionary Source="/RSG.Editor.Controls;component/ErrorTemplate.xaml" />
        <ResourceDictionary Source="/RSG.Editor.Controls;component/FocusVisualStyle.xaml" />
        <ResourceDictionary Source="/RSG.Editor.Controls;component/Converters.xaml" />
    </ResourceDictionary.MergedDictionaries>

    <Style x:Key="SpinnerButtonStyle"
           TargetType="RepeatButton">
        <Setter Property="OverridesDefaultStyle"
                Value="True" />
        <Setter Property="FrameworkElement.FocusVisualStyle"
                Value="{StaticResource ButtonFocusVisualStyle}" />
        <Setter Property="Control.Background"
                Value="{ext:ThemeResource InputBackgroundKey}" />
        <Setter Property="Control.BorderBrush"
                Value="{ext:ThemeResource ContentBorderKey}" />
        <Setter Property="Control.BorderThickness"
                Value="0" />
        <Setter Property="Control.Foreground"
                Value="{ext:ThemeResource ContentGlyphKey}" />
        <Setter Property="Control.HorizontalContentAlignment"
                Value="Center" />
        <Setter Property="Control.VerticalContentAlignment"
                Value="Center" />
        <Setter Property="Control.HorizontalAlignment"
                Value="Stretch" />
        <Setter Property="Control.VerticalAlignment"
                Value="Stretch" />
        <Setter Property="Control.Padding"
                Value="0" />
        <Setter Property="Control.Margin"
                Value="0" />
        <Setter Property="Validation.ErrorTemplate"
                Value="{x:Null}" />
        <Setter Property="Control.Template">
            <Setter.Value>
                <ControlTemplate TargetType="RepeatButton">
                    <Border Name="Chrome"
                            Background="{TemplateBinding Control.Background}"
                            BorderBrush="{TemplateBinding Control.BorderBrush}"
                            BorderThickness="{TemplateBinding Control.BorderThickness}"
                            SnapsToDevicePixels="True">
                        <Path Margin="{TemplateBinding Padding}"
                              HorizontalAlignment="Center"
                              VerticalAlignment="Center"
                              Data="{TemplateBinding ContentControl.Content}"
                              Fill="{TemplateBinding Foreground}"
                              Stretch="Uniform" />
                    </Border>
                    <ControlTemplate.Triggers>
                        <Trigger Property="UIElement.IsMouseOver" Value="True">
                            <Setter Property="Control.Background"
                                    Value="{ext:ThemeResource ContentBackgroundHoverKey}" />
                            <Setter Property="Control.BorderBrush"
                                    Value="{ext:ThemeResource ContentBorderHoverKey}" />
                            <Setter Property="Control.Foreground"
                                    Value="{ext:ThemeResource ContentGlyphHoverKey}" />
                        </Trigger>
                        <Trigger Property="ButtonBase.IsPressed" Value="True">
                            <Setter Property="Control.Background"
                                    Value="{ext:ThemeResource ContentBackgroundPressedKey}" />
                            <Setter Property="Control.BorderBrush"
                                    Value="{ext:ThemeResource ContentBorderPressedKey}" />
                            <Setter Property="Control.Foreground"
                                    Value="{ext:ThemeResource ContentGlyphPressedKey}" />
                        </Trigger>
                        <Trigger Property="UIElement.IsEnabled" Value="False">
                            <Setter Property="Control.Foreground"
                                    Value="{ext:ThemeResource ContentGlyphInactiveKey}" />
                            <Setter Property="Control.Background"
                                    Value="{ext:ThemeResource ContentBackgroundInactiveKey}" />
                            <Setter Property="Control.BorderBrush"
                                    Value="{ext:ThemeResource ContentBorderInactiveKey}" />
                        </Trigger>
                    </ControlTemplate.Triggers>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>

    <ControlTemplate x:Key="SpinnerTextBoxValidationErrorTemplate">
        <StackPanel HorizontalAlignment="Left"
                    Orientation="Vertical">
            <Border Width="{Binding ElementName=Adorner,
                                    Path=AdornedElement.ActualWidth}"
                    VerticalAlignment="Top"
                    BorderBrush="{ext:ThemeResource ErrorVisualKey}"
                    BorderThickness="1">
                <Grid>
                    <Polygon x:Name="ToolTipCorner"
                             Margin="-1"
                             HorizontalAlignment="Left"
                             VerticalAlignment="Top"
                             Fill="{ext:ThemeResource ErrorVisualKey}"
                             Grid.ZIndex="2"
                             IsHitTestVisible="True"
                             Points="0,7 7,0 0,0" />
                    <helpers:ManualValidatonAdornedPlaceholder Name="Adorner"
                                                               Margin="-1" />
                    <Border x:Name="HitZone"
                            Width="7"
                            Margin="-1"
                            HorizontalAlignment="Left"
                            VerticalAlignment="Stretch"
                            Background="Transparent"
                            Grid.ZIndex="3" />
                </Grid>
            </Border>
            <Popup Name="Popup"
                   AllowsTransparency="True"
                   HorizontalOffset="0"
                   IsOpen="False"
                   Placement="Bottom"
                   VerticalOffset="3">
                <Border Name="ErrorBorder"
                        Width="{Binding ElementName=Adorner,
                                        Path=AdornedElement.ActualWidth}"
                        Height="Auto"
                        MinWidth="150"
                        Margin="0,3,0,0"
                        VerticalAlignment="Top"
                        Background="{ext:ThemeResource ErrorVisualKey}"
                        CornerRadius="0"
                        IsHitTestVisible="False"
                        Opacity="0">
                    <TextBlock Margin="8,2,8,3"
                               VerticalAlignment="Center"
                               Text="{Binding ElementName=Adorner,
                                              Path=TemplatedAdorner.ErrorMessage}"
                               TextBlock.Foreground="#FFFFFFFF"
                               TextWrapping="Wrap" />
                </Border>
            </Popup>
        </StackPanel>
        <ControlTemplate.Triggers>
            <DataTrigger Binding="{Binding ElementName=HitZone,
                                           Path=IsMouseOver}"
                         Value="True">
                <Setter TargetName="Popup"
                        Property="IsOpen"
                        Value="True" />
                <DataTrigger.EnterActions>
                    <BeginStoryboard Name="FadeInStoryboard">
                        <Storyboard>
                            <DoubleAnimation Duration="00:00:00.15"
                                             Storyboard.TargetName="ErrorBorder"
                                             Storyboard.TargetProperty="Opacity"
                                             To="1" />
                        </Storyboard>
                    </BeginStoryboard>
                </DataTrigger.EnterActions>
                <DataTrigger.ExitActions>
                    <StopStoryboard BeginStoryboardName="FadeInStoryboard" />
                    <BeginStoryboard Name="FadeOutStoryBoard">
                        <Storyboard>
                            <DoubleAnimation Duration="00:00:00"
                                             Storyboard.TargetName="ErrorBorder"
                                             Storyboard.TargetProperty="Opacity"
                                             To="0" />
                        </Storyboard>
                    </BeginStoryboard>
                </DataTrigger.ExitActions>
            </DataTrigger>
        </ControlTemplate.Triggers>
    </ControlTemplate>

    <!--
        Default style for the type :-
        x:Type={RSG.Editor.Controls.RsDoubleSpinner}
    -->
    <Style TargetType="{x:Type local:RsDoubleSpinner}">
        <Setter Property="FrameworkElement.FocusVisualStyle"
                Value="{StaticResource ComboBoxFocusVisualStyle}" />
        <Setter Property="Control.Background"
                Value="{ext:ThemeResource InputBackgroundKey}" />
        <Setter Property="Control.BorderBrush"
                Value="{ext:ThemeResource ContentBorderKey}" />
        <Setter Property="Control.BorderThickness"
                Value="1" />
        <Setter Property="Control.Foreground"
                Value="{ext:ThemeResource ContentTextKey}" />
        <Setter Property="UIElement.AllowDrop"
                Value="True" />
        <Setter Property="ScrollViewer.PanningMode"
                Value="VerticalFirst" />
        <Setter Property="TextBoxBase.SelectionBrush"
                Value="{ext:ThemeResource InputBorderHoverKey}" />
        <Setter Property="Stylus.IsFlicksEnabled"
                Value="False" />
        <Setter Property="Control.Padding"
                Value="3,2,2,3" />
        <Setter Property="Validation.ErrorTemplate"
                Value="{StaticResource ValidationErrorTemplate}" />
        <Setter Property="helpers:ManualValidation.ManualErrorTemplate"
                Value="{StaticResource SpinnerTextBoxValidationErrorTemplate}" />
        <Setter Property="HorizontalContentAlignment"
                Value="Left" />
        <Setter Property="VerticalContentAlignment"
                Value="Center" />
        <Setter Property="Control.Template">
            <Setter.Value>
                <ControlTemplate TargetType="{x:Type local:RsDoubleSpinner}">
                    <Border Name="ContainerBorder"
                            Background="{TemplateBinding Control.Background}"
                            BorderBrush="{TemplateBinding Control.BorderBrush}"
                            BorderThickness="{TemplateBinding Control.BorderThickness}"
                            Padding="0">
                        <Grid SnapsToDevicePixels="True">
                            <Grid.RowDefinitions>
                                <RowDefinition />
                                <RowDefinition />
                            </Grid.RowDefinitions>
                            <Grid.ColumnDefinitions>
                                <ColumnDefinition Width="*" />
                                <ColumnDefinition Width="13" />
                            </Grid.ColumnDefinitions>

                            <TextBox Name="PART_EditableTextBox"
                                     Grid.RowSpan="2"
                                     Margin="{TemplateBinding Control.Padding}"
                                     HorizontalAlignment="Stretch"
                                     HorizontalContentAlignment="{TemplateBinding Control.HorizontalContentAlignment}"
                                     VerticalContentAlignment="{TemplateBinding Control.VerticalContentAlignment}"
                                     Background="{TemplateBinding Control.Background}"
                                     BorderThickness="0"
                                     IsReadOnly="{Binding RelativeSource={RelativeSource TemplatedParent},
                                                          Path=IsEditable,
                                                          Converter={StaticResource LogicalNotConverter}}"
                                     Padding="0"
                                     TextAlignment="{TemplateBinding TextAlignment}"
                                     UndoLimit="0"
                                     adp:TextBoxProperties.Active="{TemplateBinding SelectAllOnGotFocus}">
                                <TextBox.Style>
                                    <Style TargetType="TextBox">
                                        <Setter Property="OverridesDefaultStyle"
                                                Value="True" />
                                        <Setter Property="AllowDrop"
                                                Value="True" />
                                        <Setter Property="FocusVisualStyle"
                                                Value="{x:Null}" />
                                        <Setter Property="Validation.ErrorTemplate"
                                                Value="{x:Null}" />
                                        <Setter Property="Control.Template">
                                            <Setter.Value>
                                                <ControlTemplate TargetType="{x:Type TextBox}">
                                                    <ScrollViewer Name="PART_ContentHost"
                                                                  Background="{TemplateBinding Control.Background}"
                                                                  Focusable="False"
                                                                  HorizontalScrollBarVisibility="Hidden"
                                                                  VerticalScrollBarVisibility="Hidden" />
                                                </ControlTemplate>
                                            </Setter.Value>
                                        </Setter>
                                    </Style>
                                </TextBox.Style>
                            </TextBox>

                            <RepeatButton Name="IncrementButton"
                                          Grid.Row="0"
                                          Grid.Column="1"
                                          BorderThickness="1,0,0,1"
                                          Command="{TemplateBinding IncrementCommand}"
                                          CommandParameter="{TemplateBinding Value}"
                                          Content="{StaticResource UpArrowGeometry}"
                                          Style="{StaticResource SpinnerButtonStyle}" />

                            <RepeatButton Name="DecrementButton"
                                          Grid.Row="1"
                                          Grid.Column="1"
                                          BorderThickness="1,1,0,0"
                                          Command="{TemplateBinding DecrementCommand}"
                                          CommandParameter="{TemplateBinding Value}"
                                          Content="{StaticResource DownArrowGeometry}"
                                          Style="{StaticResource SpinnerButtonStyle}" />
                        </Grid>
                    </Border>
                    <ControlTemplate.Triggers>
                        <Trigger Property="UIElement.IsMouseOver" Value="True">
                            <Setter TargetName="ContainerBorder"
                                    Property="Control.BorderBrush"
                                    Value="{ext:ThemeResource InputBorderHoverKey}" />
                            <Setter Property="Control.Background"
                                    Value="{ext:ThemeResource InputBackgroundHoverKey}" />

                            <Setter TargetName="IncrementButton"
                                    Property="Border.BorderBrush"
                                    Value="{ext:ThemeResource ContentBackgroundPressedKey}" />
                            <Setter TargetName="DecrementButton"
                                    Property="Border.BorderBrush"
                                    Value="{ext:ThemeResource ContentBackgroundPressedKey}" />
                        </Trigger>
                        <Trigger Property="UIElement.IsKeyboardFocusWithin" Value="True">
                            <Setter TargetName="ContainerBorder"
                                    Property="Control.BorderBrush"
                                    Value="{ext:ThemeResource InputBorderHoverKey}" />
                            <Setter Property="Control.Background"
                                    Value="{ext:ThemeResource InputBackgroundHoverKey}" />

                            <Setter TargetName="IncrementButton"
                                    Property="Border.BorderBrush"
                                    Value="{ext:ThemeResource ContentBackgroundPressedKey}" />
                            <Setter TargetName="DecrementButton"
                                    Property="Border.BorderBrush"
                                    Value="{ext:ThemeResource ContentBackgroundPressedKey}" />
                        </Trigger>
                        <Trigger Property="IsReadOnly" Value="True">
                            <Setter Property="Control.Foreground"
                                    Value="{ext:ThemeResource ContentTextInactiveKey}" />
                            <Setter TargetName="PART_EditableTextBox"
                                    Property="IsReadOnly"
                                    Value="True"/>
                        </Trigger>
                        <Trigger Property="UIElement.IsEnabled" Value="False">
                            <Setter Property="TextElement.Foreground"
                                    Value="{ext:ThemeResource ContentTextInactiveKey}" />
                            <Setter Property="Control.Background"
                                    Value="{ext:ThemeResource ContentBackgroundInactiveKey}" />
                            <Setter Property="Control.BorderBrush"
                                    Value="{ext:ThemeResource ContentBorderInactiveKey}" />
                            <Setter TargetName="IncrementButton"
                                    Property="Border.BorderThickness"
                                    Value="0" />
                            <Setter TargetName="DecrementButton"
                                    Property="Border.BorderThickness"
                                    Value="0" />
                        </Trigger>
                    </ControlTemplate.Triggers>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>
</ResourceDictionary>