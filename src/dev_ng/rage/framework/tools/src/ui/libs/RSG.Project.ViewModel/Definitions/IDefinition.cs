﻿//---------------------------------------------------------------------------------------------
// <copyright file="IDefinition.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel.Definitions
{
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Represents the definition for a single project item type.
    /// </summary>
    public interface IDefinition
    {
        #region Properties
        /// <summary>
        /// Gets the description for this item definition to be displayed in the add dialog.
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Gets the name that is used as the default value for a new instance of this
        /// definition.
        /// </summary>
        string DefaultName { get; }

        /// <summary>
        /// Gets the extension used for this defined item.
        /// </summary>
        string Extension { get; }

        /// <summary>
        /// Gets the icon that is used to display this project item.
        /// </summary>
        BitmapSource Icon { get; }

        /// <summary>
        /// Gets the name for this defined project.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets a byte array containing the data that is used to create a new instance of the
        /// defined project.
        /// </summary>
        byte[] Template { get; }

        /// <summary>
        /// Gets the type of this item to be displayed in the add dialog.
        /// </summary>
        string Type { get; }
        #endregion Properties
    } // RSG.Project.ViewModel.Definitions.ProjectItemDefinition {Class}
} // RSG.Project.ViewModel.Definitions {Namespace}
