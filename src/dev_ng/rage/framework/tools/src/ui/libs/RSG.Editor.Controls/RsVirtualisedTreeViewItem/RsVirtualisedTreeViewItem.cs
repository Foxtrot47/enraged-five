﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsVirtualisedTreeViewItem.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using RSG.Editor.Controls.Helpers;
    using RSG.Editor.Controls.Presenters;
    using RSG.Editor.View;

    /// <summary>
    /// Represents a selectable item inside a <see cref="RsVirtualisedTreeView"/> control.
    /// </summary>
    public class RsVirtualisedTreeViewItem : ListBoxItem
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="DragCompletedEvent"/> routed event.
        /// </summary>
        public static readonly RoutedEvent DragCompletedEvent;

        /// <summary>
        /// Identifies the <see cref="DragDeltaEvent"/> routed event.
        /// </summary>
        public static readonly RoutedEvent DragDeltaEvent;

        /// <summary>
        /// Identifies the <see cref="DragStartedEvent"/> routed event.
        /// </summary>
        public static readonly RoutedEvent DragStartedEvent;

        /// <summary>
        /// Identifies the <see cref="ExpandOnDoubleClick"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ExpandOnDoubleClickProperty;

        /// <summary>
        /// Identifies the <see cref="Invoked"/> event.
        /// </summary>
        public static readonly RoutedEvent InvokedEvent;

        /// <summary>
        /// Identifies the <see cref="IsDragEnabled"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsDragEnabledProperty;

        /// <summary>
        /// Identifies the <see cref="IsDragging"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsDraggingProperty;

        /// <summary>
        /// Identifies the <see cref="IsExpandable"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsExpandableProperty;

        /// <summary>
        /// Identifies the <see cref="IsModified"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsModifiedProperty;

        /// <summary>
        /// Identifies the <see cref="IsDragging"/> dependency property key.
        /// </summary>
        private static readonly DependencyPropertyKey _isDraggingPropertyKey;

        /// <summary>
        /// The point in screen space of where the drag operation ended.
        /// </summary>
        private Point _lastScreenPoint;

        /// <summary>
        /// A value indicating whether the mouse moved during the drag operation.
        /// </summary>
        private bool _movedDuringDrag;

        /// <summary>
        /// The point in screen space of where the drag operation started.
        /// </summary>
        private Point _originalScreenPoint;

        /// <summary>
        /// The private field used for the <see cref="Presenter"/> property.
        /// </summary>
        private TreeItemPresenter _presenter;

        /// <summary>
        /// A value indicating whether the drag started event has been raised.
        /// </summary>
        private bool _raisedDragStarted;

        /// <summary>
        /// A value indicating whether a selection needs to be made when the mouse button is
        /// released. This happens when the drag operation is started on a mouse click.
        /// </summary>
        private bool _shouldChangeSelectionOnMouseUp;

        /// <summary>
        /// The private field used for the <see cref="TreeNode"/> property.
        /// </summary>
        private VirtualisedTreeNode _treeNode;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsVirtualisedTreeViewItem" /> class.
        /// </summary>
        static RsVirtualisedTreeViewItem()
        {
            DragStartedEvent =
                EventManager.RegisterRoutedEvent(
                "DragStarted",
                RoutingStrategy.Bubble,
                typeof(EventHandler<DragEventArgs>),
                typeof(RsVirtualisedTreeViewItem));

            DragDeltaEvent =
                EventManager.RegisterRoutedEvent(
                "DragDelta",
                RoutingStrategy.Bubble,
                typeof(EventHandler<DragEventArgs>),
                typeof(RsVirtualisedTreeViewItem));

            DragCompletedEvent =
                EventManager.RegisterRoutedEvent(
                "DragCompleted",
                RoutingStrategy.Bubble,
                typeof(EventHandler<DragEventArgs>),
                typeof(RsVirtualisedTreeViewItem));

            IsDragEnabledProperty =
                DependencyProperty.Register(
                "IsDragEnabled",
                typeof(bool),
                typeof(RsVirtualisedTreeViewItem),
                new FrameworkPropertyMetadata(true));

            _isDraggingPropertyKey =
                DependencyProperty.RegisterReadOnly(
                "IsDragging",
                typeof(bool),
                typeof(RsVirtualisedTreeViewItem),
                new FrameworkPropertyMetadata(false));

            IsDraggingProperty = _isDraggingPropertyKey.DependencyProperty;

            IsExpandableProperty =
                 DependencyProperty.Register(
                 "IsExpandable",
                 typeof(bool),
                 typeof(RsVirtualisedTreeViewItem),
                 new FrameworkPropertyMetadata(false));

            IsModifiedProperty =
                 DependencyProperty.Register(
                 "IsModified",
                 typeof(bool),
                 typeof(RsVirtualisedTreeViewItem),
                 new FrameworkPropertyMetadata(false));

            ExpandOnDoubleClickProperty =
                DependencyProperty.Register(
                "ExpandOnDoubleClick",
                typeof(bool),
                typeof(RsVirtualisedTreeViewItem),
                new FrameworkPropertyMetadata(true));

            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsVirtualisedTreeViewItem),
                new FrameworkPropertyMetadata(typeof(RsVirtualisedTreeViewItem)));

            InvokedEvent =
                EventManager.RegisterRoutedEvent(
                "Invoked",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(RsVirtualisedTreeViewItem));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsVirtualisedTreeViewItem"/> class.
        /// </summary>
        public RsVirtualisedTreeViewItem()
        {
            this.AllowDrop = true;
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs whenever this tree view item gets invoked by the user. This happens whenever
        /// the user double clicks on this item or pressed enter while this item has keyboard
        /// focus.
        /// </summary>
        public event RoutedEventHandler Invoked
        {
            add { this.AddHandler(InvokedEvent, value); }
            remove { this.RemoveHandler(InvokedEvent, value); }
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether this tree view item is expanded when the
        /// user double clicks on it.
        /// </summary>
        public bool ExpandOnDoubleClick
        {
            get { return (bool)this.GetValue(ExpandOnDoubleClickProperty); }
            set { this.SetValue(ExpandOnDoubleClickProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the dragging of this control is enabled.
        /// </summary>
        public bool IsDragEnabled
        {
            get { return (bool)this.GetValue(IsDragEnabledProperty); }
            set { this.SetValue(IsDragEnabledProperty, value); }
        }

        /// <summary>
        /// Gets a value indicating whether this control is currently being dragged.
        /// </summary>
        public bool IsDragging
        {
            get { return (bool)this.GetValue(IsDraggingProperty); }
            private set { this.SetValue(_isDraggingPropertyKey, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this tree view item can be expanded by the
        /// user.
        /// </summary>
        public bool IsExpandable
        {
            get { return (bool)this.GetValue(IsExpandableProperty); }
            set { this.SetValue(IsExpandableProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this tree view item is showing a item that
        /// is currently modified. This controls whether the dirty mark is shown.
        /// </summary>
        public bool IsModified
        {
            get { return (bool)this.GetValue(IsModifiedProperty); }
            set { this.SetValue(IsModifiedProperty, value); }
        }

        /// <summary>
        /// Gets the presenter that is responsible for rendering this tree view item onto the
        /// user interface.
        /// </summary>
        internal TreeItemPresenter Presenter
        {
            get { return this._presenter; }
        }

        /// <summary>
        /// Gets or sets the tree node that is associated with this tree view item.
        /// </summary>
        internal VirtualisedTreeNode TreeNode
        {
            get { return this._treeNode; }
            set { this._treeNode = value; }
        }

        /// <summary>
        /// Gets a reference to the virtualised tree view that is the parent items control for
        /// this tree view item.
        /// </summary>
        protected RsVirtualisedTreeView TreeView
        {
            get
            {
                if (this._treeNode == null)
                {
                    return null;
                }

                return this._treeNode.TreeView;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this tree view item can be expanded based on the
        /// users keyboard input.
        /// </summary>
        private bool CanExpandOnInput
        {
            get
            {
                if (!this.IsEnabled || !this.IsExpandable)
                {
                    return false;
                }

                RsVirtualisedTreeView treeView = this.TreeView;
                if (treeView == null)
                {
                    return false;
                }

                if (this._treeNode == null || this._treeNode.Parent == null)
                {
                    return false;
                }

                return !this._treeNode.Parent.IsVirtualRoot || treeView.ShowRootExpander;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this tree view item is currently expanded
        /// in the user interface.
        /// </summary>
        private bool IsExpanded
        {
            get
            {
                return this._treeNode.IsExpanded;
            }

            set
            {
                this._treeNode.IsExpanded = value;
                if (value)
                {
                    this._treeNode.ScrollExpansionIntoView();
                }
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Cancels the current dragging operation.
        /// </summary>
        public void CancelDrag()
        {
            if (!this.IsDragging)
            {
                return;
            }

            this.ReleaseCapture();
            this.OnDragCompleted(this._lastScreenPoint, true);
        }

        /// <summary>
        /// Gets called every time the control template gets attached to this control.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            this._presenter = this.GetTemplateChild("PART_Presenter") as TreeItemPresenter;
        }

        /// <summary>
        /// Ensures that the specified item is the item that is initially selected when the
        /// selection mode is extended.
        /// </summary>
        /// <param name="item">
        /// The item that should be made to be the anchored item.
        /// </param>
        protected void MakeAnchorItem(object item)
        {
            RsVirtualisedTreeView treeView = this.TreeView;
            if (treeView == null)
            {
                return;
            }

            treeView.SetAnchorItem(item);
        }

        /// <summary>
        /// Makes a single selection on the items control for this control.
        /// </summary>
        protected void MakeSingleSelection()
        {
            ListBox listBox = ItemsControl.ItemsControlFromItemContainer(this) as ListBox;
            if (listBox == null)
            {
                return;
            }

            object item = listBox.ItemContainerGenerator.ItemFromContainer(this);
            if (item == null)
            {
                return;
            }

            if (listBox.SelectedItems.Count != 1 || listBox.SelectedItems[0] != item)
            {
                listBox.SelectedItems.Clear();
                listBox.SelectedItem = item;
                this.MakeAnchorItem(item);
            }

            if (listBox.IsKeyboardFocusWithin)
            {
                this.Focus();
            }
        }

        /// <summary>
        /// Makes a toggle selection on the items control for this control.
        /// </summary>
        protected void MakeToggleSelection()
        {
            ListBox listBox = ItemsControl.ItemsControlFromItemContainer(this) as ListBox;
            if (listBox == null)
            {
                return;
            }

            object item = listBox.ItemContainerGenerator.ItemFromContainer(this);
            if (item == null)
            {
                return;
            }

            if (listBox.SelectedItems.Contains(item))
            {
                listBox.SelectedItems.Remove(item);
            }
            else
            {
                listBox.SelectedItems.Add(item);
            }

            if (listBox.IsKeyboardFocusWithin)
            {
                this.Focus();
            }
        }

        /// <summary>
        /// Called whenever the context menu is being opened.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Controls.ContextMenuEventArgs data for the event.
        /// </param>
        protected override void OnContextMenuOpening(ContextMenuEventArgs e)
        {
            RsVirtualisedTreeView treeView = this.TreeView;
            if (treeView == null)
            {
                return;
            }

            if (treeView.GetContextMenuIdCallback == null)
            {
                this.ContextMenu = null;
                base.OnContextMenuOpening(e);
                return;
            }

            if (treeView.SelectedItems == null || treeView.SelectedItems.Count != 1)
            {
                this.ContextMenu = null;
                base.OnContextMenuOpening(e);
                return;
            }

            ContextMenu menu = new Commands.RsContextMenu();
            foreach (object selectedItem in treeView.SelectedItems)
            {
                object item = selectedItem;
                VirtualisedTreeNode treeNode = selectedItem as VirtualisedTreeNode;
                if (treeNode != null)
                {
                    item = treeNode.Item;
                }

                Guid id = treeView.GetContextMenuIdCallback(item);
                if (id != Guid.Empty)
                {
                    menu.ItemsSource = CommandBarItemViewModel.Create(id);
                }
            }

            if (menu.Items.Count == 0)
            {
                this.ContextMenu = null;
            }
            else
            {
                this.ContextMenu = menu;
            }

            base.OnContextMenuOpening(e);
        }

        /// <summary>
        /// Raises the <see cref="DragCompletedEvent"/> routed event.
        /// </summary>
        /// <param name="point">
        /// The screen position of the mouse when the drag operation finished.
        /// </param>
        /// <param name="cancelled">
        /// A value indicating whether the drag operation was cancelled or not.
        /// </param>
        protected virtual void OnDragCompleted(Point point, bool cancelled)
        {
            DragState state = DragState.NotFinished;
            if (cancelled == true)
            {
                state = DragState.Cancelled;
            }
            else
            {
                if (this._movedDuringDrag)
                {
                    state = DragState.FinishedWithMovement;
                }
                else
                {
                    state = DragState.FinishedWithoutMovement;
                }
            }

            this.RaiseEvent(new RsDragEventArgs(DragCompletedEvent, point, state));
        }

        /// <summary>
        /// Raises the <see cref="DragDeltaEvent"/> routed event.
        /// </summary>
        /// <param name="point">
        /// The current screen position of the mouse.
        /// </param>
        protected virtual void OnDragDelta(Point point)
        {
            DragState state = DragState.NotFinished;
            this.RaiseEvent(new RsDragEventArgs(DragDeltaEvent, point, state));
        }

        /// <summary>
        /// Raises the <see cref="DragStartedEvent"/> routed event.
        /// </summary>
        /// <param name="point">
        /// The original point where the drag operation started from.
        /// </param>
        protected virtual void OnDragStarted(Point point)
        {
            DragState state = DragState.NotFinished;
            this.RaiseEvent(new RsDragEventArgs(DragStartedEvent, point, state));
        }

        /// <summary>
        /// Handles the event fired when this control receives focus.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs data used for this event.
        /// </param>
        protected override void OnGotFocus(RoutedEventArgs e)
        {
            RsVirtualisedTreeView treeView = this.TreeView;
            if (treeView != null && treeView.IsDirectlyGainingKeyboardFocus)
            {
                this.RaiseEvent(e);
                return;
            }

            base.OnGotFocus(e);
        }

        /// <summary>
        /// Occurs when the keyboard is focused on this element.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.KeyboardFocusChangedEventArgs data used for this event.
        /// </param>
        protected override void OnGotKeyboardFocus(KeyboardFocusChangedEventArgs e)
        {
            this.TreeView.ChangeSelection(this.TreeNode, SelectionAction.SetAnchorItem, false);
            base.OnGotKeyboardFocus(e);
        }

        /// <summary>
        /// Called if the mouse starts or stops capturing this item and makes sure to cancel
        /// the drag operation if the mouse capture is set to a different control.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data used for this event.
        /// </param>
        protected override void OnIsMouseCapturedChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnIsMouseCapturedChanged(e);
            if (!this.IsMouseCaptured)
            {
                this.CancelDrag();
            }
        }

        /// <summary>
        /// Handles the event fired when a keyboard key is pressed while this control has
        /// keyboard focus.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.KeyEventArgs data used for this event.
        /// </param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            if (!e.Handled)
            {
                e.Handled = this.HandleKeyDown(e.Key, e.OriginalSource);
                if (e.Handled == false)
                {
                    if (this.IsSelected && e.Key == Key.Return)
                    {
                        e.Handled = true;
                        this.InvokeItem(InputSource.Keyboard);
                    }
                }
            }
        }

        /// <summary>
        /// Responds to the System.Windows.UIElement.MouseLeftButtonDown event.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.MouseButtonEventArgs data used for the event.
        /// </param>
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            if (e.ClickCount % 2 == 0 && (!this.IsExpandable || !this.ExpandOnDoubleClick))
            {
                this.InvokeItem(InputSource.Mouse);
                return;
            }

            if (e.ClickCount % 2 == 0)
            {
                if (this.ExpandOnDoubleClick && this.IsExpandable == true)
                {
                    this.IsExpanded = !this.IsExpanded;
                    e.Handled = true;
                }
                else
                {
                    this.InvokeItem(InputSource.Mouse);
                }

                base.OnMouseLeftButtonDown(e);
                return;
            }

            this._shouldChangeSelectionOnMouseUp = false;
            if (this.IsSelected)
            {
                if (Keyboard.Modifiers == ModifierKeys.None ||
                    Keyboard.Modifiers == ModifierKeys.Control)
                {
                    this._shouldChangeSelectionOnMouseUp = true;
                }
            }

            if (this._shouldChangeSelectionOnMouseUp)
            {
                this.Focus();
                e.Handled = true;
            }

            base.OnMouseLeftButtonDown(e);
            if (Mouse.LeftButton != MouseButtonState.Pressed || !this.IsDragEnabled)
            {
                return;
            }

            if (!(this as DependencyObject).IsConnectedToPresentationSource())
            {
                return;
            }

            Point screenPoint = this.PointToScreen(e.GetPosition(this));
            this.BeginDragging(screenPoint);
        }

        /// <summary>
        /// Responds to the System.Windows.UIElement.MouseLeftButtonUp event.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.MouseButtonEventArgs data used for the event.
        /// </param>
        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            bool connected = PresentationSource.FromVisual(this) != null;
            if (this.IsMouseCaptured && this.IsDragging && connected)
            {
                this._lastScreenPoint = this.PointToScreen(e.GetPosition(this));
                this.CompleteDrag();
            }

            if (this._shouldChangeSelectionOnMouseUp)
            {
                switch (Keyboard.Modifiers)
                {
                    case ModifierKeys.None:
                        if (this.IsSelected)
                        {
                            this.MakeSingleSelection();
                        }

                        break;
                    case ModifierKeys.Control:
                        if (this._shouldChangeSelectionOnMouseUp)
                        {
                            this.MakeToggleSelection();
                        }

                        break;
                }
            }

            base.OnMouseLeftButtonUp(e);
        }

        /// <summary>
        /// Responds to the System.Windows.UIElement.MouseMove event.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.MouseButtonEventArgs data used for the event.
        /// </param>
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            bool connected = PresentationSource.FromVisual(this) != null;
            if (!this.IsMouseCaptured || !this.IsDragging || !connected)
            {
                return;
            }

            this._movedDuringDrag = true;
            Point point = this.PointToScreen(e.GetPosition(this));
            if (this._raisedDragStarted)
            {
                this.OnDragDelta(point);
            }

            if (this.IsOutsideSensitivity(point) && !this._raisedDragStarted)
            {
                this._shouldChangeSelectionOnMouseUp = false;
                this._raisedDragStarted = true;
                this.OnDragStarted(this._originalScreenPoint);
            }

            this._lastScreenPoint = point;
        }

        /// <summary>
        /// Handles the event fired when the right mouse button is pressed while over this
        /// control.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.MouseButtonEventArgs data used for this event.
        /// </param>
        protected override void OnPreviewMouseRightButtonDown(MouseButtonEventArgs e)
        {
            if (!this.IsKeyboardFocusWithin)
            {
                this.Focus();
            }

            base.OnPreviewMouseRightButtonDown(e);
        }

        /// <summary>
        /// Beings the dragging operation.
        /// </summary>
        /// <param name="screenPoint">
        /// The screen pont at which the drag operation has been started.
        /// </param>
        private void BeginDragging(Point screenPoint)
        {
            if (!this.CaptureMouse())
            {
                return;
            }

            this.IsDragging = true;
            this._originalScreenPoint = screenPoint;
            this._lastScreenPoint = screenPoint;
            this._movedDuringDrag = false;
            this._raisedDragStarted = false;
        }

        /// <summary>
        /// Completes the current drag operation.
        /// </summary>
        private void CompleteDrag()
        {
            if (!this.IsDragging)
            {
                return;
            }

            this.ReleaseCapture();
            this.OnDragCompleted(this._lastScreenPoint, false);
        }

        /// <summary>
        /// Handles the event fired when a keyboard key is pressed while this control has
        /// keyboard focus.
        /// </summary>
        /// <param name="key">
        /// The key that was pressed.
        /// </param>
        /// <param name="originalSource">
        /// The original source for this event.
        /// </param>
        /// <returns>
        /// True if the event has been handled; otherwise, false.
        /// </returns>
        private bool HandleKeyDown(Key key, object originalSource)
        {
            if (this.IsLogicalLeft(key))
            {
                if (this.IsControlPressed())
                {
                    return false;
                }

                if (this.IsExpanded && this.CanExpandOnInput)
                {
                    this.IsExpanded = false;
                    return true;
                }
                else
                {
                    VirtualisedTreeNode parent = null;
                    if (this.TreeNode != null)
                    {
                        parent = this.TreeNode.Parent;
                    }

                    RsVirtualisedTreeView treeView = this.TreeView;
                    if (parent == null || parent.IsVirtualRoot || treeView == null)
                    {
                        return false;
                    }

                    treeView.ScrollIntoView(parent);
                    UIElement container = parent.Container;
                    if (container == null)
                    {
                        return false;
                    }

                    container.Focus();
                    return true;
                }
            }

            if (this.IsLogicalRight(key))
            {
                if (this.IsControlPressed())
                {
                    return false;
                }

                if (!this.IsExpanded)
                {
                    if (this.CanExpandOnInput)
                    {
                        this.IsExpanded = true;
                    }

                    return true;
                }

                if (this.MoveFocus(new TraversalRequest(FocusNavigationDirection.Down)))
                {
                    return true;
                }

                return false;
            }

            if (!this.CanExpandOnInput)
            {
                return false;
            }

            switch (key)
            {
                case Key.Multiply:
                    RsVirtualisedTreeView parentTreeView = this.TreeView;
                    if (parentTreeView != null && this.IsEnabled && originalSource == this)
                    {
                        this.TreeView.ExpandNodeRecursively(this.TreeNode);
                        return true;
                    }

                    break;
                case Key.Add:
                    if (!this.IsExpanded && originalSource == this)
                    {
                        this.IsExpanded = true;
                        return true;
                    }

                    break;
                case Key.Subtract:
                    if (this.IsExpanded && originalSource == this)
                    {
                        this.IsExpanded = false;
                        return true;
                    }

                    break;
                case Key.Separator:
                default:
                    break;
            }

            return false;
        }

        /// <summary>
        /// Invokes this item using the invoke controller attached to the display item.
        /// </summary>
        /// <param name="inputSource">
        /// The source of the input that caused this item to be invoked.
        /// </param>
        /// <returns>
        /// A value indicating whether any items were successfully invoked.
        /// </returns>
        private bool InvokeItem(InputSource inputSource)
        {
            if (!this.IsSelected)
            {
                return false;
            }

            RsVirtualisedTreeView treeView = this.TreeView;
            if (treeView != null)
            {
                ICollection<object> selection = treeView.GetDistinctSelection<object>();
                Dictionary<IInvokeController, List<IInvokable>> invokeMap =
                    new Dictionary<IInvokeController, List<IInvokable>>();
                foreach (object selectedItem in selection)
                {
                    IInvokable invokable = selectedItem as IInvokable;
                    if (invokable == null || invokable.InvokeController == null)
                    {
                        continue;
                    }

                    List<IInvokable> invokables = null;
                    if (!invokeMap.TryGetValue(invokable.InvokeController, out invokables))
                    {
                        invokables = new List<IInvokable>();
                        invokeMap.Add(invokable.InvokeController, invokables);
                    }

                    invokables.Add(invokable);
                }

                bool invoked = false;
                foreach (KeyValuePair<IInvokeController, List<IInvokable>> kvp in invokeMap)
                {
                    invoked = kvp.Key.Invoke(kvp.Value, inputSource, this) || invoked;
                }

                if (invoked)
                {
                    this.RaiseEvent(new RoutedEventArgs(InvokedEvent, this));
                    return true;
                }

                if (this.CanExpandOnInput)
                {
                    this.IsExpanded = !this.IsExpanded;
                    return true;
                }

                return false;
            }

            return false;
        }

        /// <summary>
        /// Determines whether the control key on the keyboard is currently pressed.
        /// </summary>
        /// <returns>
        /// True if the control key on the keyboard is pressed.
        /// </returns>
        private bool IsControlPressed()
        {
            return (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control;
        }

        /// <summary>
        /// Determines whether the specified key is the key to press to logically move to the
        /// left.
        /// </summary>
        /// <param name="key">
        /// The key to test.
        /// </param>
        /// <returns>
        /// True if the specified key is the key to press to logically move to the left.
        /// </returns>
        private bool IsLogicalLeft(Key key)
        {
            if (this.FlowDirection == FlowDirection.LeftToRight)
            {
                return key == Key.Left;
            }

            return key == Key.Right;
        }

        /// <summary>
        /// Determines whether the specified key is the key to press to logically move to the
        /// right.
        /// </summary>
        /// <param name="key">
        /// The key to test.
        /// </param>
        /// <returns>
        /// True if the specified key is the key to press to logically move to the right.
        /// </returns>
        private bool IsLogicalRight(Key key)
        {
            if (this.FlowDirection == FlowDirection.LeftToRight)
            {
                return key == Key.Right;
            }

            return key == Key.Left;
        }

        /// <summary>
        /// Determines whether the original starting point and the current point are a
        /// significant distance between each other to say the dragged item has been moved.
        /// </summary>
        /// <param name="point">
        /// The point to test.
        /// </param>
        /// <returns>
        /// True if the difference is significant between the two points.
        /// </returns>
        private bool IsOutsideSensitivity(Point point)
        {
            point.Offset(-this._originalScreenPoint.X, -this._originalScreenPoint.Y);
            point.X = Math.Abs(point.X);
            point.Y = Math.Abs(point.Y);
            double minimumDeltaX = SystemParameters.MinimumHorizontalDragDistance;
            double minimumDeltaY = SystemParameters.MinimumVerticalDragDistance;
            return point.X > minimumDeltaX || point.Y > minimumDeltaY;
        }

        /// <summary>
        /// Releases the mouse capture of this control if it had it.
        /// </summary>
        private void ReleaseCapture()
        {
            if (!this.IsDragging)
            {
                return;
            }

            this.ClearValue(_isDraggingPropertyKey);
            if (this.IsMouseCaptured)
            {
                this.ReleaseMouseCapture();
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsVirtualisedTreeViewItem {Class}
} // RSG.Editor.Controls {Namespace}
