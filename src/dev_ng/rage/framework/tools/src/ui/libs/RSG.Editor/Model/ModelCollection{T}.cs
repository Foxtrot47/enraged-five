﻿//---------------------------------------------------------------------------------------------
// <copyright file="ModelCollection{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Model
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;

    /// <summary>
    /// Represents a collection of objects that can be access via a index or iterated over.
    /// Also supports notifications and undo / redo.
    /// </summary>
    /// <typeparam name="T">
    /// The type of elements in the collection.
    /// </typeparam>
    public class ModelCollection<T>
        : NotifyPropertyChangedBase, INotifyCollectionChanged, IList<T>, IReadOnlyList<T>
    {
        #region Fields
        /// <summary>
        /// The private list that is used as the internal data for the collection.
        /// </summary>
        private List<T> _internalList;

        /// <summary>
        /// This private field used for the <see cref="UndoEngineProvider"/> property.
        /// </summary>
        private IUndoEngineProvider _undoEngineProvider;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ModelCollection{T}"/> class without an
        /// initial undo engine provider.
        /// </summary>
        public ModelCollection()
        {
            this._internalList = new List<T>();
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ModelCollection{T}"/> class.
        /// </summary>
        /// <param name="undoProvider">
        /// Gets the undo engine provider instance that this collection uses.
        /// </param>
        public ModelCollection(IUndoEngineProvider undoProvider)
        {
            this._internalList = new List<T>();
            this._undoEngineProvider = undoProvider;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ModelCollection{T}"/> class.
        /// </summary>
        /// <param name="undoProvider">
        /// Gets the undo engine provider instance that this collection uses.
        /// </param>
        /// <param name="capacity">
        /// The number of elements that the new list can initially store.
        /// </param>
        public ModelCollection(IUndoEngineProvider undoProvider, int capacity)
        {
            this._internalList = new List<T>(capacity);
            this._undoEngineProvider = undoProvider;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ModelCollection{T}"/> class as a
        /// wrapper for the specified list.
        /// </summary>
        /// <param name="collection">
        /// The collection whose elements are copied to the new list.
        /// </param>
        public ModelCollection(IEnumerable<T> collection)
        {
            if (collection != null)
            {
                this._internalList = new List<T>(collection.Count());
                foreach (T item in collection)
                {
                    this._internalList.Add(item);
                }
            }
            else
            {
                this._internalList = new List<T>();
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ModelCollection{T}"/> class as a
        /// wrapper for the specified list.
        /// </summary>
        /// <param name="undoProvider">
        /// Gets the undo engine provider instance that this collection uses.
        /// </param>
        /// <param name="collection">
        /// The collection whose elements are copied to the new list.
        /// </param>
        public ModelCollection(IUndoEngineProvider undoProvider, IEnumerable<T> collection)
        {
            this._undoEngineProvider = undoProvider;
            if (collection != null)
            {
                this._internalList = new List<T>(collection.Count());
                foreach (T item in collection)
                {
                    this._internalList.Add(item);
                }
            }
            else
            {
                this._internalList = new List<T>();
            }
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs when the collection changes.
        /// </summary>
        public event NotifyCollectionChangedEventHandler CollectionChanged;
        #endregion Events

        #region Properties
        /// <summary>
        /// Gets the number of elements contained in the collection.
        /// </summary>
        public int Count
        {
            get { return this._internalList.Count; }
        }

        /// <summary>
        /// Gets a value indicating whether the collection is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Gets the undo engine provider instance that this collection uses.
        /// </summary>
        internal protected IUndoEngineProvider UndoEngineProvider
        {
            get { return this._undoEngineProvider; }
        }

        /// <summary>
        /// Gets the undo engine this collection should use to store changes in.
        /// </summary>
        protected virtual UndoEngine UndoEngine
        {
            get
            {
                if (this._undoEngineProvider == null)
                {
                    return null;
                }

                return this._undoEngineProvider.UndoEngine;
            }
        }

        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index of the element to get or set.
        /// </param>
        /// <returns>
        /// The element at the specified index.
        /// </returns>
        public T this[int index]
        {
            get { return (T)this._internalList[index]; }
            set { this.Replace(index, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Adds an item to the end of the collection.
        /// </summary>
        /// <param name="item">
        /// The object to add to the collection.
        /// </param>
        public void Add(T item)
        {
            int index = this.Count;
            this._internalList.Add(item);
            this.OnItemAdded(item, index);
            this.OnPropertyChange();
        }

        /// <summary>
        /// Adds multiple items to the end of the collection.
        /// </summary>
        /// <param name="items">
        /// The list of items that should be added to the collection.
        /// </param>
        public void AddRange(IEnumerable<T> items)
        {
            if (items == null)
            {
                return;
            }

            int index = this.Count;
            int count = 0;
            foreach (T item in items)
            {
                count++;
                this._internalList.Add(item);
            }

            if (count == 0)
            {
                return;
            }

            this.OnItemsAdded(items, index);
            this.OnPropertyChange();
        }

        /// <summary>
        /// Adds multiple items to the end of the collection.
        /// </summary>
        /// <param name="items">
        /// The list of items that should be added to the collection.
        /// </param>
        public void AddRange(IEnumerable items)
        {
            if (items == null)
            {
                return;
            }

            IList<T> castItems = new List<T>();
            foreach (object item in items)
            {
                T castItem = (T)item;
                if (castItem == null)
                {
                    continue;
                }

                castItems.Add(castItem);
            }

            this.AddRange(castItems);
        }

        /// <summary>
        /// Removes all items from the collection.
        /// </summary>
        public void Clear()
        {
            if (this.Count == 0)
            {
                return;
            }

            IList copy = null;
            UndoEngine undoEngine = this.UndoEngine;
            if (undoEngine != null && undoEngine.PerformingEvent == false)
            {
                copy = new List<T>(this);
            }

            this._internalList.Clear();
            if (copy != null)
            {
                NotifyCollectionChangedEventArgs args = new NotifyCollectionChangedEventArgs(
                    NotifyCollectionChangedAction.Remove, copy, 0);
                CollectionChange<T> change = new CollectionChange<T>(this, args);
                undoEngine.CreateUndoEvent(change);
            }

            this.OnCollectionReset();
            this.OnPropertyChange();
        }

        /// <summary>
        /// Determines whether the collection contains a specific value.
        /// </summary>
        /// <param name="item">
        /// The object to locate in the collection.
        /// </param>
        /// <returns>
        /// True if item is found in the collection; otherwise false.
        /// </returns>
        public bool Contains(T item)
        {
            return this._internalList.Contains(item);
        }

        /// <summary>
        /// Copies the elements of the collection to an System.Array, starting at a
        /// particular index.
        /// </summary>
        /// <param name="array">
        /// The one-dimensional System.Array that is the destination of the elements.
        /// </param>
        /// <param name="arrayIndex">
        /// The zero-based index in array at which copying begins.
        /// </param>
        public void CopyTo(T[] array, int arrayIndex)
        {
            this._internalList.CopyTo(array, arrayIndex);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A System.Collections.Generic.IEnumerator{T} that can be used to iterate through the
        /// collection.
        /// </returns>
        public IEnumerator<T> GetEnumerator()
        {
            return new ModelCollectionEnumerator<T>(this._internalList.GetEnumerator());
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A System.Collections.Generic.IEnumerator{T} that can be used to iterate through the
        /// collection.
        /// </returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this._internalList.GetEnumerator();
        }

        /// <summary>
        /// Determines the index of a specific item in the collection.
        /// </summary>
        /// <param name="item">
        /// The object to locate in the collection.
        /// </param>
        /// <returns>
        /// The index of item if found in the list; otherwise, -1.
        /// </returns>
        public int IndexOf(T item)
        {
            return this._internalList.IndexOf(item);
        }

        /// <summary>
        /// Inserts the specified item into the collection at the specified index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index at which item should be inserted.
        /// </param>
        /// <param name="item">
        /// The object to insert into the collection.
        /// </param>
        public void Insert(int index, T item)
        {
            this._internalList.Insert(index, item);
            this.OnItemAdded(item, index);
            this.OnPropertyChange();
        }

        /// <summary>
        /// Inserts the specified item into the collection at the specified index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index at which item should be inserted.
        /// </param>
        /// <param name="items">
        /// The objects to insert into the collection.
        /// </param>
        public void Insert(int index, IList<T> items)
        {
            int count = 0;
            foreach (T item in items.Reverse())
            {
                count++;
                this._internalList.Insert(index, item);
            }

            if (count == 0)
            {
                return;
            }

            this.OnItemsAdded(items, index);
            this.OnPropertyChange();
        }

        /// <summary>
        /// Inserts multiple items into the collection at the specified index.
        /// </summary>
        /// <param name="index">
        /// The non-zero index where the items should be inserted.
        /// </param>
        /// <param name="items">
        /// The list of items that should be added to the collection.
        /// </param>
        public void Insert(int index, IList items)
        {
            if (items == null)
            {
                return;
            }

            IList<T> castItems = new List<T>();
            foreach (object item in items)
            {
                T castItem = (T)item;
                if (castItem == null)
                {
                    continue;
                }

                castItems.Add(castItem);
            }

            this.Insert(index, castItems);
        }

        /// <summary>
        /// Moves the item at the specified index to a new location in the collection.
        /// </summary>
        /// <param name="oldIndex">
        /// The zero-based index specifying the location of the item to be moved.
        /// </param>
        /// <param name="newIndex">
        /// The zero-based index specifying the new location of the item.
        /// </param>
        public void Move(int oldIndex, int newIndex)
        {
            T item = (T)this._internalList[oldIndex];

            this._internalList.RemoveAt(oldIndex);
            this._internalList.Insert(newIndex, item);

            this.OnItemMoved(item, newIndex, oldIndex);
            this.OnPropertyChange();
        }

        /// <summary>
        /// Moves the first occurrence of the specified item to the specified index.
        /// </summary>
        /// <param name="item">
        /// The item that should be moved.
        /// </param>
        /// <param name="newIndex">
        /// The zero-based index specifying the new location of the item.
        /// </param>
        public void Move(T item, int newIndex)
        {
            int oldIndex = this._internalList.IndexOf(item);

            this._internalList.RemoveAt(oldIndex);
            this._internalList.Insert(newIndex, item);

            this.OnItemMoved(item, newIndex, oldIndex);
            this.OnPropertyChange();
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the collection.
        /// </summary>
        /// <param name="item">
        /// The object to remove from the collection.
        /// </param>
        /// <returns>
        /// True if item was successfully removed from the collection, otherwise, false. This
        /// method also returns false if item is not found in the original collection.
        /// </returns>
        public bool Remove(T item)
        {
            int index = this.IndexOf(item);
            if (index == -1)
            {
                return false;
            }

            this.RemoveAt(index);
            return true;
        }

        /// <summary>
        /// Removes the item at the specified index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index of the item to remove.
        /// </param>
        public void RemoveAt(int index)
        {
            T item = this[index];
            this._internalList.RemoveAt(index);

            this.OnItemRemoved(item, index);
            this.OnPropertyChange();
        }

        /// <summary>
        /// Removes a range of elements from the collection.
        /// </summary>
        /// <param name="index">
        /// The zero-based index specifying the starting location of the items to be removed.
        /// </param>
        /// <param name="count">
        /// The number of elements to remove.
        /// </param>
        public void RemoveRange(int index, int count)
        {
            if (count <= 0)
            {
                return;
            }

            IList<T> range = new List<T>(this._internalList.GetRange(index, count).Cast<T>());
            this._internalList.RemoveRange(index, count);
            this.OnItemsRemoved(range, index);
            this.OnPropertyChange();
        }

        /// <summary>
        /// Replaces the first occurrence of the specified old item with the
        /// specified new item.
        /// </summary>
        /// <param name="oldItem">
        /// The item that is going to be replaced.
        /// </param>
        /// <param name="newItem">
        /// The item that is going to be replacing the old item.
        /// </param>
        public void Replace(T oldItem, T newItem)
        {
            int index = this._internalList.IndexOf(oldItem);
            this._internalList.RemoveAt(index);
            this._internalList.Insert(index, newItem);
            this.OnItemReplaced(newItem, oldItem, index);
            this.OnPropertyChange();
        }

        /// <summary>
        /// Adds an item to the end of the collection.
        /// </summary>
        /// <param name="index">
        /// The zero-based index specifying the location of the item to be replaced.
        /// </param>
        /// <param name="newItem">
        /// The item that is going to be replacing the old item.
        /// </param>
        public void Replace(int index, T newItem)
        {
            T oldItem = (T)this._internalList[index];
            this._internalList.RemoveAt(index);
            this._internalList.Insert(index, newItem);
            this.OnItemReplaced(newItem, oldItem, index);
            this.OnPropertyChange();
        }

        /// <summary>
        /// Sets the undo engine provider this collection uses to generate undo events on
        /// collection changes.
        /// </summary>
        /// <param name="undoProvider">
        /// The undo engine provider instance that this collection uses.
        /// </param>
        public void SetUndoEngineProvider(IUndoEngineProvider undoProvider)
        {
            this._undoEngineProvider = undoProvider;
        }

        /// <summary>
        /// Calls the collection changed event handler the describes a Reset change.
        /// </summary>
        private void OnCollectionReset()
        {
            NotifyCollectionChangedEventHandler handler = this.CollectionChanged;
            if (handler != null)
            {
                NotifyCollectionChangedAction action = NotifyCollectionChangedAction.Reset;
                handler(this, new NotifyCollectionChangedEventArgs(action, null));
            }
        }

        /// <summary>
        /// Calls the collection changed event handler that describes a one-item change.
        /// </summary>
        /// <param name="item">
        /// The item that is affected by the change.
        /// </param>
        /// <param name="index">
        /// The zero-based index at which the item was added.
        /// </param>
        private void OnItemAdded(object item, int index)
        {
            NotifyCollectionChangedEventHandler handler = this.CollectionChanged;
            NotifyCollectionChangedEventArgs args = new NotifyCollectionChangedEventArgs(
                NotifyCollectionChangedAction.Add, item, index);

            if (handler != null)
            {
                handler(this, args);
            }

            UndoEngine undoEngine = this.UndoEngine;
            if (undoEngine != null && undoEngine.PerformingEvent == false)
            {
                CollectionChange<T> change = new CollectionChange<T>(this, args);
                undoEngine.CreateUndoEvent(change);
            }
        }

        /// <summary>
        /// Calls the collection changed event handler that describes a one-item Move change.
        /// </summary>
        /// <param name="item">
        /// The item affected by the change.
        /// </param>
        /// <param name="newIndex">
        /// The new index for the changed item.
        /// </param>
        /// <param name="oldIndex">
        /// The old index for the changed item.
        /// </param>
        private void OnItemMoved(object item, int newIndex, int oldIndex)
        {
            NotifyCollectionChangedEventHandler handler = this.CollectionChanged;
            NotifyCollectionChangedEventArgs args = new NotifyCollectionChangedEventArgs(
                NotifyCollectionChangedAction.Move, item, newIndex, oldIndex);

            if (handler != null)
            {
                handler(this, args);
            }

            UndoEngine undoEngine = this.UndoEngine;
            if (undoEngine == null || undoEngine.PerformingEvent == true)
            {
                return;
            }

            CollectionChange<T> change = new CollectionChange<T>(this, args);
            undoEngine.CreateUndoEvent(change);
        }

        /// <summary>
        /// Calls the collection changed event handler that describes a one-item change.
        /// </summary>
        /// <param name="item">
        /// The item that is affected by the change.
        /// </param>
        /// <param name="index">
        /// The zero-based index at which the item was removed.
        /// </param>
        private void OnItemRemoved(object item, int index)
        {
            NotifyCollectionChangedEventHandler handler = this.CollectionChanged;
            NotifyCollectionChangedEventArgs args = new NotifyCollectionChangedEventArgs(
                NotifyCollectionChangedAction.Remove, item, index);

            if (handler != null)
            {
                handler(this, args);
            }

            UndoEngine undoEngine = this.UndoEngine;
            if (undoEngine != null && undoEngine.PerformingEvent == false)
            {
                CollectionChange<T> change = new CollectionChange<T>(this, args);
                undoEngine.CreateUndoEvent(change);
            }
        }

        /// <summary>
        /// Calls the collection changed event handler that describes a one-item
        /// Replace change.
        /// </summary>
        /// <param name="newItem">
        /// The new item that is replacing the original item.
        /// </param>
        /// <param name="oldItem">
        /// The original item that is replaced.
        /// </param>
        /// <param name="index">
        /// The zero-based index at which the items was replaced.
        /// </param>
        private void OnItemReplaced(object newItem, object oldItem, int index)
        {
            NotifyCollectionChangedEventHandler handler = this.CollectionChanged;
            NotifyCollectionChangedEventArgs args = new NotifyCollectionChangedEventArgs(
                NotifyCollectionChangedAction.Replace, newItem, oldItem, index);

            if (handler != null)
            {
                handler(this, args);
            }

            UndoEngine undoEngine = this.UndoEngine;
            if (undoEngine != null && undoEngine.PerformingEvent == false)
            {
                CollectionChange<T> change = new CollectionChange<T>(this, args);
                undoEngine.CreateUndoEvent(change);
            }
        }

        /// <summary>
        /// Calls the collection changed event handler that describes a multi-item change.
        /// </summary>
        /// <param name="items">
        /// The items that are affected by the change.
        /// </param>
        /// <param name="index">
        /// The zero-based index at which the items was added.
        /// </param>
        private void OnItemsAdded(IEnumerable<T> items, int index)
        {
            NotifyCollectionChangedEventHandler handler = this.CollectionChanged;
            UndoEngine undoEngine = this.UndoEngine;
            bool undo = undoEngine != null && undoEngine.PerformingEvent == false;
            if (handler == null && !undo)
            {
                return;
            }

            IList list = items.ToList() as IList;
            NotifyCollectionChangedEventArgs args = new NotifyCollectionChangedEventArgs(
                NotifyCollectionChangedAction.Add, list, index);

            if (handler != null)
            {
                handler(this, args);
            }

            if (undo)
            {
                CollectionChange<T> change = new CollectionChange<T>(this, args);
                undoEngine.CreateUndoEvent(change);
            }
        }

        /// <summary>
        /// Calls the collection changed event handler that describes a multi-item change.
        /// </summary>
        /// <param name="items">
        /// The items that are affected by the change.
        /// </param>
        /// <param name="index">
        /// The zero-based index at which the items were removed.
        /// </param>
        private void OnItemsRemoved(IList<T> items, int index)
        {
            NotifyCollectionChangedEventHandler handler = this.CollectionChanged;
            NotifyCollectionChangedEventArgs args = new NotifyCollectionChangedEventArgs(
                NotifyCollectionChangedAction.Remove, items as IList, index);

            if (handler != null)
            {
                handler(this, args);
            }

            UndoEngine undoEngine = this.UndoEngine;
            if (undoEngine != null && undoEngine.PerformingEvent == false)
            {
                CollectionChange<T> change = new CollectionChange<T>(this, args);
                undoEngine.CreateUndoEvent(change);
            }
        }

        /// <summary>
        /// Calls the property changed event handler that describes a change to the properties.
        /// </summary>
        private void OnPropertyChange()
        {
            this.NotifyPropertyChanged("Count", "Item[]");
        }
        #endregion Methods
    } // RSG.Editor.Model.ModelCollection{T} {Class}
} // RSG.Editor.Model {Namespace}
