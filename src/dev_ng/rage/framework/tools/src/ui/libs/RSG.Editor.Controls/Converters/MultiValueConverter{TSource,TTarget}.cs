﻿//---------------------------------------------------------------------------------------------
// <copyright file="MultiValueConverter{TSource,TTarget}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Data;
    using RSG.Editor.Controls.Resources;

    /// <summary>
    /// Provides a base class for all the value converters that convert n-number of source
    /// objects of a single type into one target object.
    /// </summary>
    /// <typeparam name="TSource">
    /// The type of the source objects.
    /// </typeparam>
    /// <typeparam name="TTarget">
    /// The type of the target object.
    /// </typeparam>
    public abstract class MultiValueConverter<TSource, TTarget> : IMultiValueConverter
    {
        #region Fields
        /// <summary>
        /// A private reference to the MS.Internal.NamedObject that represents a
        /// disconnected item.
        /// </summary>
        private static object _disconnectedItem = CreateDisconnectedItemReference();
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="MultiValueConverter{TSource,TTarget}"/> class.
        /// </summary>
        protected MultiValueConverter()
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Converts the specified value object into a equivalent value of the type specified
        /// by <paramref name="targetType"/>.
        /// </summary>
        /// <param name="values">
        /// The original value to convert.
        /// </param>
        /// <param name="targetType">
        /// The type that the source value will be turned into.
        /// </param>
        /// <param name="parameter">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A converted value.
        /// </returns>
        public object Convert(
            object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null)
            {
                throw new SmartArgumentNullException(() => values);
            }

            bool valid = this.ValidateConvert(values, targetType);
            if (!valid)
            {
                return default(TTarget);
            }

            List<TSource> sources = new List<TSource>();
            foreach (object sourceValue in values)
            {
                sources.Add((TSource)sourceValue);
            }

            return this.Convert(sources.ToArray(), parameter, culture);
        }

        /// <summary>
        /// Converts the specified converted value back to its source value.
        /// </summary>
        /// <param name="value">
        /// The already converted target value to convert back to its source value.
        /// </param>
        /// <param name="targetTypes">
        /// The type to convert the value to.
        /// </param>
        /// <param name="parameter">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// The source value for the specified value.
        /// </returns>
        public object[] ConvertBack(
            object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            bool valid = this.ValidateConvertBack(value, targetTypes);
            if (!valid)
            {
                if (!Debugger.IsAttached)
                {
                    Debug.Assert(valid == true, "Invalid convert back parameters.");
                }

                return new object[3] { value, value, value };
            }

            return this.ConvertBack((TTarget)((object)value), parameter, culture);
        }

        /// <summary>
        /// Converters the specified source values to a single target value.
        /// </summary>
        /// <param name="values">
        /// The pre-converted source values.
        /// </param>
        /// <param name="param">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A target value created by converting the two specified source values.
        /// </returns>
        protected virtual TTarget Convert(TSource[] values, object param, CultureInfo culture)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Converters the target value to its source values.
        /// </summary>
        /// <param name="value">
        /// The target value that needs converting back to its source values.
        /// </param>
        /// <param name="param">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A array containing the source values that were converted to the given target value.
        /// </returns>
        protected virtual object[] ConvertBack(
            TTarget value, object param, CultureInfo culture)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Validates the specified parameters to make sure they are consistent with the type
        /// arguments of the class.
        /// </summary>
        /// <param name="values">
        /// The original value to convert.
        /// </param>
        /// <param name="targetType">
        /// The type that the source value will be turned into.
        /// </param>
        /// <returns>
        /// True if the specified parameters are valid; otherwise, false.
        /// </returns>
        protected virtual bool ValidateConvert(object[] values, Type targetType)
        {
            if (values == null)
            {
                throw new SmartArgumentNullException(() => values);
            }

            for (int i = 0; i < values.Length; i++)
            {
                object obj = values[i];
                if (obj == DependencyProperty.UnsetValue)
                {
                    return false;
                }

                if (_disconnectedItem != null &&
                    Object.ReferenceEquals(obj, _disconnectedItem))
                {
                    return false;
                }

                if (!CheckValue<TSource>(values[i]))
                {
                    throw new InvalidOperationException(
                        ErrorStringTable.GetString(
                        "InvalidValueItemType",
                        i.ToString(),
                        typeof(TTarget).Name));
                }
            }

            return true;
        }

        /// <summary>
        /// Validates the specified parameters to make sure they are consistent with the type
        /// arguments of the class.
        /// </summary>
        /// <param name="value">
        /// The original value to convert.
        /// </param>
        /// <param name="targetTypes">
        /// The type that the source value will be turned into.
        /// </param>
        /// <returns>
        /// True if the specified parameters are valid; otherwise, false.
        /// </returns>
        protected virtual bool ValidateConvertBack(object value, Type[] targetTypes)
        {
            if (targetTypes == null)
            {
                throw new SmartArgumentNullException(() => targetTypes);
            }

            return true;
        }

        /// <summary>
        /// Checks to make sure the specified object value is of the correct type.
        /// </summary>
        /// <typeparam name="T">
        /// The type parameter that the specified value should be a instance of.
        /// </typeparam>
        /// <param name="value">
        /// The value to check.
        /// </param>
        /// <returns>
        /// True if the value is of the specified type parameter; otherwise false.
        /// </returns>
        private static bool CheckValue<T>(object value)
        {
            if (!(value is T) && (value != null || typeof(T).IsValueType))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Create a reference to the internal microsoft property that specifies a item that
        /// has been disconnected with its data context.
        /// </summary>
        /// <returns>
        /// A reference to the internal microsoft property that specifies a item that has been
        /// disconnected with its data context.
        /// </returns>
        private static object CreateDisconnectedItemReference()
        {
            Type type = typeof(System.Windows.Data.BindingExpressionBase);
            BindingFlags flags = BindingFlags.Static | BindingFlags.NonPublic;
            FieldInfo fieldInfo = type.GetField("DisconnectedItem", flags);
            if (fieldInfo == null)
            {
                return null;
            }

            return fieldInfo.GetValue(null);
        }
        #endregion Methods
    } // RSG.Editor.Controls.Converters.MultiValueConverter {Class}
} // RSG.Editor.Controls.Converters {Namespace}
