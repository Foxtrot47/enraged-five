﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Rag.Widgets;

namespace RSG.Rag.ViewModel.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class ToggleViewModel<T> : WidgetViewModel<WidgetToggle<T>>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        protected ToggleViewModel(WidgetToggle<T> widget)
            : base(widget)
        {
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public bool Toggled
        {
            get { return Widget.Toggled; }
            set { Widget.Toggled = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override bool IsModified
        {
            get { return Widget.Toggled != Widget.OriginalValue; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override string DefaultValue
        {
            get { return Widget.OriginalValue.ToString(); }
        }
        #endregion // Properties

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        protected override void OnWidgetPropertyChanged(String propertyName)
        {
            base.OnWidgetPropertyChanged(propertyName);
            if (propertyName == "Toggled")
            {
                NotifyPropertyChanged("IsModified");
            }
        }
        #endregion // Event Handlers
    } // ToggleViewModel<T>
}
