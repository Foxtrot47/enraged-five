﻿//---------------------------------------------------------------------------------------------
// <copyright file="BitsetTunableViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Tunables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows.Data;
    using RSG.Editor;
    using RSG.Editor.View;
    using RSG.Metadata.Model.Definitions;
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// The view model for a <see cref="BitsetTunable"/> model object.
    /// </summary>
    public class BitsetTunableViewModel : TunableViewModelBase<BitsetTunable>
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Items"/> property.
        /// </summary>
        private List<object> _items;

        /// <summary>
        /// The private field used for the <see cref="Value"/> property.
        /// </summary>
        private string _value;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BitsetTunableViewModel"/> class.
        /// </summary>
        /// <param name="tunable">
        /// The bitset tunable that this view model is representing.
        /// </param>
        /// <param name="parent">
        /// The parent tunable view model for this view model.
        /// </param>
        public BitsetTunableViewModel(BitsetTunable tunable, ITunableViewModel parent)
            : base(tunable, parent)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the text that displays what is currently selected.
        /// </summary>
        public string DisplayText
        {
            get
            {
                List<string> values = new List<string>();
                foreach (BitsetItemViewModel item in this._items)
                {
                    if (item.IsChecked)
                    {
                        values.Add(item.Name);
                    }
                }

                return String.Join(" | ", values);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the bitset has a unknown size and therefore cannot
        /// be rendered as a toggle combo box control and has to instead be a text box for
        /// hexidecimal values.
        /// </summary>
        public bool HasIndeterminateSize
        {
            get { return this.Model.BitsetMember.NumBits <= 0; }
        }

        /// <summary>
        /// Gets the bitset items that represent the individual bits in the bitset, with each
        /// bit managing their own flag value.
        /// </summary>
        public List<object> Items
        {
            get
            {
                if (this._items == null)
                {
                    this._items = new List<object>();
                    foreach (BitsetItem item in this.Model.Items)
                    {
                        this._items.Add(new BitsetItemViewModel(this, item));
                    }

                    DispatcherHelper.CheckBeginInvokeOnUI(new Action(() =>
                    {
                        ListSortDirection descending = ListSortDirection.Descending;
                        ListSortDirection ascending = ListSortDirection.Ascending;
                        ICollectionView view =
                            CollectionViewSource.GetDefaultView(this._items);
                        view.GroupDescriptions.Add(new PropertyGroupDescription("IsChecked"));
                        view.SortDescriptions.Add(
                            new SortDescription("IsChecked", descending));
                        view.SortDescriptions.Add(
                            new SortDescription("AssociatedWithEnumValue", descending));

                        IEnumeration enumeration = this.Model.BitsetMember.Values;
                        EnumerationSortMethod sortMethod = EnumerationSortMethod.Default;
                        if (enumeration != null)
                        {
                            sortMethod = enumeration.SortMethod;
                        }

                        switch (sortMethod)
                        {
                            case EnumerationSortMethod.Alphabetical:
                                view.SortDescriptions.Add(
                                    new SortDescription("Name", ascending));
                                break;
                            case EnumerationSortMethod.Value:
                                view.SortDescriptions.Add(
                                    new SortDescription("Value", ascending));
                                break;
                            case EnumerationSortMethod.None:
                            default:
                                view.SortDescriptions.Add(
                                    new SortDescription("Index", ascending));
                                break;
                        }
                    }));
                }

                return this._items;
            }
        }

        /// <summary>
        /// Gets the value of this bitset tunable when it is of indeterminate size.
        /// </summary>
        public string Value
        {
            get
            {
                return this.Model.Value.ToString();
            }

            set
            {
                this._value = value;
                ulong newValue = 0;
                if (ulong.TryParse(value, out newValue))
                {
                    this.Model.Value = newValue;
                }
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Makes sure the value string representation keeps in sync with the models integer
        /// value.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.ComponentModel.PropertyChangedEventArgs containing the event data.
        /// </param>
        protected override void OnModelPropertyChanged(object s, PropertyChangedEventArgs e)
        {
            base.OnModelPropertyChanged(s, e);
            if (e.PropertyName == "Value")
            {
                this._value = this.Model.Value.ToString();
            }
        }

        /// <summary>
        /// Updates the display text for this bitset tunable based on the currently checked
        /// items.
        /// </summary>
        private void UpdateDisplayText()
        {
            this.NotifyPropertyChanged("DisplayText");
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// Represents a single bit value inside a bitset control.
        /// </summary>
        private class BitsetItemViewModel : ViewModelBase<BitsetItem>
        {
            #region Fields
            /// <summary>
            /// The private reference to the bitset view model that created this instance.
            /// </summary>
            private BitsetTunableViewModel _parent;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="BitsetItemViewModel"/> class.
            /// </summary>
            /// <param name="parent">
            /// A reference to the bitset view model that created this instance.
            /// </param>
            /// <param name="model">
            /// The bitset item this view model is representing.
            /// </param>
            public BitsetItemViewModel(BitsetTunableViewModel parent, BitsetItem model)
                : base(model)
            {
                this._parent = parent;
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets a value indicating whether this bit item is associated with a enum
            /// constant value or not.
            /// </summary>
            public bool AssociatedWithEnumValue
            {
                get { return this.Model.AssociatedWithEnumValue; }
            }

            /// <summary>
            /// Gets the index of this item in its zero-based non-sorted location.
            /// </summary>
            public int Index
            {
                get { return this._parent.Model.Items.IndexOf(this.Model); }
            }

            /// <summary>
            /// Gets or sets a value indicating whether this bit is currently 'turned on' or
            /// not.
            /// </summary>
            public bool IsChecked
            {
                get { return this.Model.IsChecked; }
                set { this.Model.IsChecked = value; }
            }

            /// <summary>
            /// Gets the text that is displayed for this bitset item.
            /// </summary>
            public string Name
            {
                get { return this.Model.Name; }
            }

            /// <summary>
            /// Gets the value of the item based on the associated enum constant or index.
            /// </summary>
            public long Value
            {
                get { return this.Model.Value; }
            }
            #endregion Properties

            #region Methods
            /// <summary>
            /// Makes sure the display text on the parent is updated whenever the is checked
            /// property changes.
            /// </summary>
            /// <param name="s">
            /// The object this handler is attached to.
            /// </param>
            /// <param name="e">
            /// The System.ComponentModel.PropertyChangedEventArgs containing the event data.
            /// </param>
            protected override void OnModelPropertyChanged(
                object s, PropertyChangedEventArgs e)
            {
                base.OnModelPropertyChanged(s, e);
                this._parent.UpdateDisplayText();
            }
            #endregion Methods
        } // BitsetTunableViewModel.BitsetItemViewModel {Class}
        #endregion
    } // RSG.Metadata.ViewModel.Tunables.BitsetTunableViewModel {Class}
} // RSG.Metadata.ViewModel.Tunables {Namespace}
