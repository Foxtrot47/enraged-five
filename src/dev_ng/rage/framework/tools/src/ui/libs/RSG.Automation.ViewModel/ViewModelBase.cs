﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor;

namespace RSG.Automation.ViewModel
{

    /// <summary>
    /// Automation Monitor base view model class; handles the error 
    /// and provides validation methods for subclasses.
    /// </summary>
    /// <see cref="http://msdn.microsoft.com/en-us/library/system.componentmodel.inotifydataerrorinfo(v=vs.95).aspx"/>
    public abstract class ViewModelBase :
        NotifyPropertyChangedBase,
        INotifyDataErrorInfo
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ViewModelBase()
        {
            this.m_Errors = new Dictionary<String, List<String>>();
        }
        #endregion // Constructor(s)

        #region Protected Methods
        /// <summary>
        /// Dictionary storing ViewModel errors.
        /// </summary>
        private IDictionary<String, List<String>> m_Errors;

        /// <summary>
        /// Add an error to our collection (called by subclasses).
        /// </summary>
        /// <param name="property"></param>
        /// <param name="errorMessage"></param>
        /// <param name="isWarning"></param>
        protected void AddError(String property, String errorMessage, bool isWarning)
        {
            if (!this.m_Errors.ContainsKey(property))
                this.m_Errors[property] = new List<String>();

            if (!this.m_Errors[property].Contains(errorMessage))
            {
                if (isWarning)
                    this.m_Errors[property].Add(errorMessage);
                else
                    this.m_Errors[property].Insert(0, errorMessage);

                RaiseErrorsChanged(property);
            }
        }

        /// <summary>
        /// Remove an error from our collection (called by subclasses).
        /// </summary>
        /// <param name="property"></param>
        /// <param name="errorMessage"></param>
        protected void RemoveError(String property, String errorMessage)
        {
            if (this.m_Errors.ContainsKey(property) &&
                this.m_Errors[property].Contains(errorMessage))
            {
                this.m_Errors[property].Remove(errorMessage);
                if (0 == this.m_Errors[property].Count)
                    this.m_Errors.Remove(property);

                RaiseErrorsChanged(property);
            }
        }

        /// <summary>
        /// Clear all errors from our collection (called by subclasses).
        /// </summary>
        /// <param name="property"></param>
        protected void ClearAllErrors(String property)
        {
            if (this.m_Errors.ContainsKey(property))
            {
                this.m_Errors[property].Clear();
                RaiseErrorsChanged(property);
            }
        }

        /// <summary>
        /// Raise the ErrorsChanged event for a given property.
        /// </summary>
        /// <param name="property"></param>
        protected void RaiseErrorsChanged(String property)
        {
            if (null != ErrorsChanged)
                ErrorsChanged(this, new DataErrorsChangedEventArgs(property));
        }
        #endregion // Protected Methods

        #region INotifyDataErrorInfo Interface
        /// <summary>
        /// Occurs when the validation errors have changed for a property or for the 
        /// entire entity.
        /// </summary>
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        /// <summary>
        /// Gets the validation errors for a specified property or for the entire 
        /// entity.
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public IEnumerable GetErrors(String property)
        {
            if (String.IsNullOrEmpty(property) ||
                !this.m_Errors.ContainsKey(property)) return null;
            return this.m_Errors[property];
        }

        /// <summary>
        /// Gets a value that indicates whether the entity has validation errors.
        /// </summary>
        public bool HasErrors
        {
            get { return (this.m_Errors.Count > 0); }
        }
        #endregion // INotifyDataErrorInfo Interface
    }

} // RSG.Automation.ViewModel namespace

