﻿//---------------------------------------------------------------------------------------------
// <copyright file="ClassViewProjectNode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.ClassView
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using RSG.Metadata.Model.Definitions;
    using RSG.Metadata.ViewModel.Project;
    using RSG.Project.ViewModel;

    /// <summary>
    /// The node inside the class view that represents a standard metadata project. This class
    /// cannot be inherited.
    /// </summary>
    public sealed class ClassViewProjectNode : ClassViewNodeBase
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="StandardProject"/> property.
        /// </summary>
        private StandardProjectNode _standardProject;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ClassViewProjectNode"/> class.
        /// </summary>
        /// <param name="standardProject">
        /// The standard metadata project whose metadata definitions will be used for this
        /// class view project.
        /// </param>
        public ClassViewProjectNode(StandardProjectNode standardProject)
            : base(null)
        {
            this.IsExpandable = true;
            this._standardProject = standardProject;
            this.Text = standardProject.Text;
            standardProject.PropertyChanged += this.OnProjectPropertyChanged;
            this.Icon = ProjectIcons.GenericProject;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the definition dictionary containing the metadata definitions that have been
        /// tied to this project.
        /// </summary>
        public IDefinitionDictionary Definitions
        {
            get { return this._standardProject.MetadataDefinitions; }
        }

        /// <summary>
        /// Gets the metadata project node that was used to create this node.
        /// </summary>
        public StandardProjectNode StandardProject
        {
            get { return this._standardProject; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Gets all of the enumerations that have been loaded that are located in the
        /// specified scope.
        /// </summary>
        /// <param name="scope">
        /// The scope whose enumerations should be retrieved.
        /// </param>
        /// <returns>
        /// A read-only list of enumerations that have been loaded that are located in the
        /// specified scope.
        /// </returns>
        public IReadOnlyList<IEnumeration> GetEnumerationsFromScope(string scope)
        {
            return this.Definitions.GetEnumerationsFromScope(scope);
        }

        /// <summary>
        /// Gets all of the structures that have been loaded that are located in the specified
        /// scope.
        /// </summary>
        /// <param name="scope">
        /// The scope whose structures should be retrieved.
        /// </param>
        /// <returns>
        /// A read-only list of structures that have been loaded that are located in the
        /// specified scope.
        /// </returns>
        public IReadOnlyList<IStructure> GetStructuresFromScope(string scope)
        {
            return this.Definitions.GetStructuresFromScope(scope);
        }

        /// <summary>
        /// Creates the child elements for this view model. This is only called once the item
        /// has been expanded in the user interface.
        /// </summary>
        /// <param name="collection">
        /// The collection to add the elements to.
        /// </param>
        protected override void CreateChildren(ICollection<object> collection)
        {
            ClassViewProjectNode project = this.ParentProjectNode;
            if (project == null)
            {
                return;
            }

            this.StandardProject.EnsureDefinitionsAreLoaded();
            foreach (string ns in this.Definitions.GetNamespaces())
            {
                collection.Add(new ClassViewNamespaceNode(ns, this));
            }

            foreach (IStructure structure in this.GetStructuresFromScope(null))
            {
                collection.Add(new ClassViewStructureNode(structure, this));
            }

            foreach (IEnumeration enumeration in this.GetEnumerationsFromScope(null))
            {
                collection.Add(new ClassViewEnumerationNode(enumeration, this));
            }

            foreach (IConstant constant in this.Definitions.Constants)
            {
                collection.Add(new ClassViewConstantNode(constant, this));
            }
        }

        /// <summary>
        /// Called whenever a property on the associated standard project changes.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.ComponentModel.PropertyChangedEventArgs containing the event data.
        /// </param>
        private void OnProjectPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Text")
            {
                this.NotifyPropertyChanged("Text");
            }
        }
        #endregion Methods
    } // RSG.Metadata.ViewModel.Project.ClassViewProjectNode {Class}
} // RSG.Metadata.ViewModel.Project {Namespace}
