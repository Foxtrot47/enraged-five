﻿//---------------------------------------------------------------------------------------------
// <copyright file="CreateDocumentContentController.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.ViewModel.Project.Controllers
{
    using System;
    using System.IO;
    using System.Xml;
    using RSG.Project.ViewModel;
    using RSG.Text.Model;

    /// <summary>
    /// Used to create the document content for a project node that is representing a local
    /// dialogue file.
    /// </summary>
    public class CreateDocumentContentController : ICreateDocumentContentController
    {
        #region Methods
        /// <summary>
        /// Creates content to be used inside a document for the specified hierarchy node.
        /// </summary>
        /// <param name="node">
        /// The node whose content will be created.
        /// </param>
        /// <param name="e">
        /// A structure containing objects that can be used during construction, i.e. a log
        /// object to log errors and warnings to and the view site that the document is being
        /// opened in.
        /// </param>
        /// <returns>
        /// The object that represents the document content for the specified node if created;
        /// otherwise, null.
        /// </returns>
        /// <remarks>
        /// If for some reason the document shouldn't be created throw a
        /// <see cref="System.OperationCanceledException"/>.
        /// </remarks>
        public object CreateContent(HierarchyNode node, CreateContentArgs e)
        {
            string fullPath = node.ProjectItem.GetMetadata("FullPath");
            ProjectNode project = node.ParentProject;
            if (fullPath == null)
            {
                throw new NotSupportedException("Unable to retrieve full path from node.");
            }

            if (!File.Exists(fullPath))
            {
                throw new FileNotFoundException("Unable to open node", fullPath);
            }

            DialogueProjectNode dialogueProject = project as DialogueProjectNode;
            DialogueConfigurations configurations = null;
            if (dialogueProject != null)
            {
                configurations = dialogueProject.Configurations;
            }

            Dialogue dialogue = null;
            using (XmlReader reader = XmlReader.Create(fullPath))
            {
                dialogue = new Dialogue(reader, e.UndoEngine, configurations);
            }

            return new DialogueViewModel(dialogue, node as FileNode);
        }
        #endregion Methods
    } // RSG.Text.ViewModel.Project.Controllers.CreateDocumentContentController {Class}
} // RSG.Text.ViewModel.Project.Controllers {Namespace}
