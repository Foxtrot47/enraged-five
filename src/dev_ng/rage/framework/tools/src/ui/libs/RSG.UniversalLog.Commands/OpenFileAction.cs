﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor;
using RSG.UniversalLog.ViewModel;

namespace RSG.UniversalLog.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public class OpenFileAction : ButtonAction<UniversalLogDataContext>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="OpenFileImplementer"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public OpenFileAction(ParameterResolverDelegate<UniversalLogDataContext> resolver)
            : base(resolver)
        {
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="commandParameter">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(UniversalLogDataContext dc)
        {
            ICommonDialogService dlgService = this.GetService<ICommonDialogService>();
            if (dlgService == null)
            {
                Debug.Assert(false, "Unable to open file as service is missing.");
                return;
            }

            String filter = "Universal Log Files (*.ulog)|*.ulog|Universal Zip Files (*.ulogzip)|*.ulogzip|All Files (*.*)|*.*";
            String[] filepaths;
            if (!dlgService.ShowOpenFile(null, null, filter, 0, out filepaths))
            {
                return;
            }

            // Open the file.
            dc.OpenFiles(filepaths, false);
        }
        #endregion // Overrides
    } // OpenFileAction
}
