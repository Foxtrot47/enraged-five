﻿using RSG.Rag.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.ViewModel.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    public class SliderInt32ViewModel : SliderViewModel<Int32>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        public SliderInt32ViewModel(WidgetSlider<Int32> widget)
            : base(widget)
        {
        }
        #endregion // Constructor(s)
    } // SliderInt32ViewModel
}
