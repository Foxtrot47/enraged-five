﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using RSG.Base.Logging;
using RSG.Base.Extensions;
using RSG.Base.Logging.Universal;
using RSG.Base.Attributes;
using RSG.Editor;

namespace RSG.UniversalLog.ViewModel
{
    /// <summary>
    /// 
    /// </summary>
    public class UniversalLogMessageViewModel : NotifyPropertyChangedBase
    {
        #region Fields
        /// <summary>
        /// Reference to the parent universal log view model.
        /// </summary>
        private UniversalLogFileViewModel _logViewModel;
        #endregion // Fields

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public UniversalLogMessageViewModel(UniversalLogFileBufferedMessage msg, UniversalLogFileViewModel parent, String systemContext)
        {
            _bufferedMessage = msg;
            _logViewModel = parent;
            _systemContext = systemContext;
            _formattedMessage = DecodeMessage(msg.Message);
            _localTimestamp = msg.Timestamp.ToLocalTime();
        }
        #endregion // Constructors

        #region Properties
        /// <summary>
        /// Buffered message this view model wraps.
        /// </summary>
        public UniversalLogFileBufferedMessage BufferedMessage
        {
            get { return _bufferedMessage; }
        }
        private UniversalLogFileBufferedMessage _bufferedMessage;

        /// <summary>
        /// Proxy property for the log level.
        /// </summary>
        public LogLevel Level
        {
            get { return BufferedMessage.Level; }
        }

        /// <summary>
        /// Application Context
        /// </summary>
        public String SystemContext
        {
            get { return _systemContext; }
        }
        private String _systemContext;

        /// <summary>
        /// Proxy property for the message context.
        /// </summary>
        public String MessageContext
        {
            get { return BufferedMessage.Context; }
        }

        /// <summary>
        /// File path property.
        /// </summary>
        public String FilePath
        {
            get { return _logViewModel.Filepath; }
        }

        /// <summary>
        /// File context property.
        /// </summary>
        public String FileContext
        {
            get { return System.IO.Path.GetFileName(FilePath); }
        }

        /// <summary>
        /// Gets the message associated with this component.
        /// </summary>
        public String Message
        {
            get { return _formattedMessage; }
            internal set { SetProperty(ref _formattedMessage, value); }
        }
        private String _formattedMessage;

        /// <summary>
        /// Gets the icon bitmap object that represents the entry category.
        /// </summary>
        public BitmapSource IconBitmap
        {
            get { return Level.GetDisplayIcon(); }
        }

        /// <summary>
        /// Gets the timestamp associated with this component.
        /// </summary>
        public DateTime LocalTimestamp
        {
            get { return _localTimestamp; }
        }
        private DateTime _localTimestamp;

        /// <summary>
        /// Tick representation of the timestamp.
        /// </summary>
        public long Ticks
        {
            get { return _localTimestamp.Ticks; }
        }

        /// <summary>
        /// Flag indicating whether we should be showing additional details for this message.
        /// </summary>
        public bool ShowDetails
        {
            get { return _showDetails; }
            set { SetProperty(ref _showDetails, value); }
        }
        private bool _showDetails;

        /// <summary>
        /// Retrieves the text that should be used when copying to the clipboard.
        /// </summary>
        public String CopyText
        {
            get { return String.Format("{0}[{1}]:{2} - {3}", LocalTimestamp, Level, MessageContext, Message); }
        }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Converts encoded xml strings back to their original values.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private String DecodeMessage(String message)
        {
            const uint maxLines = 100;

            // Sanity check the message length.
            StringBuilder sb;

            if (message.Count(item => item == '\n') > maxLines)
            {
                sb = new StringBuilder();
                foreach (String line in message.Split(new char[] { '\n' }).Take(100))
                {
                    sb.AppendLine(line);
                }

                sb.AppendLine("...");
                sb.AppendFormat("<<message longer than {0} lines was truncated>>", maxLines);
            }
            else
            {
                sb = new StringBuilder(message);
            }

            // Change the escaped characters to non-escaped ones.
            sb.Replace("&amp;", "&");
            sb.Replace("&lt;", "<");
            sb.Replace("&gt;", ">");
            sb.Replace("&apos;", "'");
            sb.Replace("&quot;", "\"");
            return sb.ToString();
        }
        #endregion // Methods
    } // UniversalLogMessageViewModel
}
