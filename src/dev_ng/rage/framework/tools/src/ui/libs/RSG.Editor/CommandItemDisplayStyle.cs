﻿//---------------------------------------------------------------------------------------------
// <copyright file="CommandItemDisplayStyle.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using RSG.Base.Attributes;

    /// <summary>
    /// Defines the different ways a command bar item can be displayed to the user.
    /// </summary>
    public enum CommandItemDisplayStyle
    {
        /// <summary>
        /// Specifies that the command bar item is to be styled as default based on the type.
        /// </summary>
        [FieldDisplayName("Default")]
        Default = 0,

        /// <summary>
        /// Specifies that the command bar item is to be styled so that only the image is ever
        /// displayed to the user.
        /// </summary>
        [FieldDisplayName("Image Only")]
        ImageOnly = 1,

        /// <summary>
        /// Specifies that the command bar item is to be styled so that only the text is ever
        /// displayed to the user.
        /// </summary>
        [FieldDisplayName("Text Only")]
        TextOnly = 2,

        /// <summary>
        /// Specifies that the command bar item is to be styled so that both the icon and the
        /// text is displayed.
        /// </summary>
        [FieldDisplayName("Image And Text")]
        ImageAndText = 3
    } // RSG.Editor.CommandItemDisplayStyle {Enumeration}
} // RSG.Editor {Namespace}
