﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using RSG.Base.Math;
using RSG.Editor.Controls.CurveEditor;
using RSG.Editor.Model;

namespace RSG.Editor.Controls.CurveEditor.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class Tangent : ModelBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public TangentMode Mode
        {
            get { return _mode; }
            set { SetProperty(ref _mode, value); }
        }
        private TangentMode _mode;

        /// <summary>
        /// 
        /// </summary>
        public double PositionX
        {
            get { return _positionX; }
            set { SetProperty(ref _positionX, value); }
        }
        private double _positionX;

        /// <summary>
        /// 
        /// </summary>
        public double PositionY
        {
            get { return _positionY; }
            set { SetProperty(ref _positionY, value); }
        }
        private double _positionY;
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlElem"></param>
        public Tangent(XElement xmlElem)
        {
            // Mode.
            XElement xmlModeElem = xmlElem.Element("Mode");
            Debug.Assert(xmlModeElem != null, "Tangent XElement is missing the Mode element.");
            if (xmlModeElem != null)
            {
                Mode = (TangentMode)Enum.Parse(typeof(TangentMode), xmlModeElem.Value);
            }

            // Parse the key's position.
            XElement xmlPositionElem = xmlElem.Element("Position");
            Debug.Assert(xmlPositionElem != null, "Tangent XElement is missing the Position element.");
            if (xmlPositionElem != null)
            {
                XAttribute xAtt = xmlPositionElem.Attribute("x");
                Debug.Assert(xAtt != null, "Vector2d missing x-attribute.");
                if (xAtt != null)
                {
                    this._positionX = Double.Parse(xAtt.Value);
                }

                XAttribute yAtt = xmlPositionElem.Attribute("y");
                Debug.Assert(yAtt != null, "Vector2d missing y-attribute.");
                if (yAtt != null)
                {
                    this._positionY = Double.Parse(yAtt.Value);
                }
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="position"></param>
        public Tangent(TangentMode mode, double x = 0.0, double y = 0.0)
        {
            _mode = mode;
            _positionX = x;
            _positionY = y;
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        internal XElement ToXElement(XName name)
        {
            return
                new XElement(name,
                    new XElement("Mode", Mode.ToString()),
                    new XElement("Position", new XAttribute("x", PositionX), new XAttribute("y", PositionY))
                );
        }
        #endregion // Public Methods
        
        #region Overrides
        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Indicates whether the current object is equal to the specified object. When this
        /// method is called it has already been determined that the specified object is not
        /// equal to the current object or null through reference equality.
        /// </summary>
        /// <param name="other">
        /// An object to compare with this object.
        /// </param>
        /// <returns>
        /// True if the current object is equal to the other parameter; otherwise, false.
        /// </returns>
        protected override bool EqualsCore(IModel other)
        {
            Tangent tangent = (Tangent)other;
            return (Mode == tangent.Mode &&
                PositionX == tangent.PositionX &&
                PositionY == tangent.PositionY);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return Mode.ToString();
        }
        #endregion // Overrides
    } // TangentModel
}
