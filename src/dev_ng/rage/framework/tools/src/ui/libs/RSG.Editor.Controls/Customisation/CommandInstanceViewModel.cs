﻿//---------------------------------------------------------------------------------------------
// <copyright file="CommandInstanceViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Customisation
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Input;
    using RSG.Base.Extensions;

    /// <summary>
    /// Represents a view model that wraps a single instance of the
    /// <see cref="CommandInstance"/> class.
    /// </summary>
    internal class CommandInstanceViewModel : CommandViewModel
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="CommandStyles"/> property.
        /// </summary>
        private static List<CustomCommandStyle> _commandStyles = CreateCommandStyles();

        /// <summary>
        /// The command definition that this command view model is using.
        /// </summary>
        private CommandDefinition _definition;

        /// <summary>
        /// The private reference to the command instance that this view model is wrapping.
        /// </summary>
        private CommandInstance _instance;

        /// <summary>
        /// The private field used for the <see cref="SelectedCommandStyle"/> property.
        /// </summary>
        private CustomCommandStyle _selectedCommandStyle;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CommandInstanceViewModel"/> class.
        /// </summary>
        /// <param name="item">
        /// The item that this view model is wrapping.
        /// </param>
        /// <param name="container">
        /// The container that this view model is currently inside.
        /// </param>
        public CommandInstanceViewModel(
            CommandInstance item, CommandContainerViewModel container)
            : base(item, container)
        {
            this._instance = item;
            CommandItemDisplayStyle currentStyle = CommandItemDisplayStyle.Default;
            currentStyle = this._instance.DisplayStyle;
            ICommand cmd = this._instance.Command;
            this._definition = RockstarCommandManager.GetDefinition(cmd);

            foreach (CustomCommandStyle commandStyle in _commandStyles)
            {
                if (commandStyle.Style != currentStyle)
                {
                    continue;
                }

                this._selectedCommandStyle = commandStyle;
                break;
            }
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether the individual items are shown to the user
        /// for this instance.
        /// </summary>
        public bool AreMultiItemsShown
        {
            get
            {
                return this._instance.AreMultiItemsShown;
            }

            set
            {
                if (this._instance.AreMultiItemsShown == value)
                {
                    return;
                }

                string oldValue = this._instance.AreMultiItemsShown.ToString();
                this._instance.AreMultiItemsShown = value;
                this.RegisterModification(ModificationField.AreMultiItemsShown, oldValue);
            }
        }

        /// <summary>
        /// Gets a value indicating whether this commands multi item properties can be edited.
        /// </summary>
        public bool CanEditMultiItemProperties
        {
            get
            {
                if (this._definition is ComboCommand)
                {
                    return true;
                }
                else if (this._definition is FilterCommand)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this commands show filter count can be edited.
        /// </summary>
        public bool CanEditShowFilterCount
        {
            get
            {
                if (this._definition is FilterCommand)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this commands text can be edited.
        /// </summary>
        public bool CanEditText
        {
            get
            {
                if (this._definition is ComboCommand)
                {
                    return false;
                }

                return true;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this commands width can be edited.
        /// </summary>
        public bool CanEditWidth
        {
            get
            {
                if (this._definition is ComboCommand)
                {
                    return true;
                }
                else if (this._definition is FilterCommand)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Gets the list of all available command styles available to the command system.
        /// </summary>
        public List<CustomCommandStyle> CommandStyles
        {
            get { return _commandStyles; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the individual items start a group each or
        /// not. Only applicable when instancing a multi command with
        /// <see cref="AreMultiItemsShown"/> set to true.
        /// </summary>
        public bool EachItemStartsGroup
        {
            get
            {
                return this._instance.EachItemStartsGroup;
            }

            set
            {
                if (this._instance.EachItemStartsGroup == value)
                {
                    return;
                }

                string oldValue = this._instance.EachItemStartsGroup.ToString();
                this._instance.EachItemStartsGroup = value;
                this.RegisterModification(ModificationField.EachItemStartsGroup, oldValue);
            }
        }

        /// <summary>
        /// Gets or sets the currently selected command style for this command.
        /// </summary>
        public CustomCommandStyle SelectedCommandStyle
        {
            get
            {
                return this._selectedCommandStyle;
            }

            set
            {
                if (this.SetProperty(ref this._selectedCommandStyle, value))
                {
                    CommandInstance instance = this._instance as CommandInstance;
                    if (instance != null)
                    {
                        if (instance.DisplayStyle == value.Style)
                        {
                            return;
                        }

                        string oldValue = instance.DisplayStyle.ToString();
                        instance.DisplayStyle = value.Style;
                        this.RegisterModification(ModificationField.DisplayStyle, oldValue);
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the filter check count is shown to the
        /// user in the user interface.
        /// </summary>
        public bool ShowFilterCount
        {
            get
            {
                return this._instance.ShowFilterCount;
            }

            set
            {
                if (this._instance.ShowFilterCount == value)
                {
                    return;
                }

                string oldValue = this._instance.ShowFilterCount.ToString();
                this._instance.ShowFilterCount = value;
                this.RegisterModification(ModificationField.ShowFilterCount, oldValue);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this item is proceeded by a separator to
        /// indicate the start of a new group inside the command bar hierarchy.
        /// </summary>
        public bool StartsGroup
        {
            get
            {
                return this._instance.StartsGroup;
            }

            set
            {
                if (this._instance.StartsGroup == value)
                {
                    return;
                }

                string oldValue = this._instance.StartsGroup.ToString();
                this._instance.StartsGroup = value;
                this.RegisterModification(ModificationField.StartsGroup, oldValue);
            }
        }

        /// <summary>
        /// Gets or sets the width used for this command bar item.
        /// </summary>
        public int Width
        {
            get
            {
                return this._instance.Width;
            }

            set
            {
                if (this._instance.Width == value)
                {
                    return;
                }

                string oldValue = this._instance.Width.ToStringInvariant();
                this._instance.Width = value;
                this.RegisterModification(ModificationField.Width, oldValue);
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates the static list containing the custom command styles.
        /// </summary>
        /// <returns>
        /// A list containing the custom command styles.
        /// </returns>
        private static List<CustomCommandStyle> CreateCommandStyles()
        {
            List<CustomCommandStyle> styles = new List<CustomCommandStyle>();
            Type type = typeof(CommandItemDisplayStyle);
            foreach (CommandItemDisplayStyle style in Enum.GetValues(type))
            {
                styles.Add(new CustomCommandStyle(style));
            }

            return styles;
        }
        #endregion Methods
    } // RSG.Editor.Controls.Customisation.CommandInstanceViewModel {Class}
} // RSG.Editor.Controls.Customisation {Namespace}
