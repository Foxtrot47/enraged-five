﻿//---------------------------------------------------------------------------------------------
// <copyright file="IPerforceFileMetaData.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// When implemented contains the metadata values for a file inside of source control.
    /// </summary>
    public interface IPerforceFileMetaData
    {
        #region Properties
        /// <summary>
        /// Gets the current users action on this file.
        /// </summary>
        PerforceFileAction Action { get; }

        /// <summary>
        /// Gets the change list number that this file is inside in the current users pending
        /// change lists if applicable; otherwise, -1.
        /// </summary>
        int Change { get; }

        /// <summary>
        /// Gets the location of the file in the clients workspace.
        /// </summary>
        string ClientPath { get; }

        /// <summary>
        /// Gets the location of the file in the clients depot system.
        /// </summary>
        string DepotPath { get; }

        /// <summary>
        /// Gets the revision index the current user has of this file.
        /// </summary>
        int HaveRevision { get; }

        /// <summary>
        /// Gets the action that was performed on the file at the head revision.
        /// </summary>
        PerforceFileAction HeadAction { get; }

        /// <summary>
        /// Gets the revision index the of this files head revision.
        /// </summary>
        int HeadRevision { get; }

        /// <summary>
        /// Gets the date and time the head revision of the file was submitted.
        /// </summary>
        DateTime HeadTime { get; }

        /// <summary>
        /// Gets a value indicating whether the file is inside the clients workspace.
        /// </summary>
        bool IsInClient { get; }

        /// <summary>
        /// Gets a value indicating whether the file is inside the clients depot.
        /// </summary>
        bool IsInDepot { get; }

        /// <summary>
        /// Gets a value indicating whether the file is mapped in the clients workspace.
        /// </summary>
        bool IsMapped { get; }

        /// <summary>
        /// Gets the location of the file in the client's file system.
        /// </summary>
        string LocalPath { get; }

        /// <summary>
        /// Gets the list containing all of the actions other users are currently performing on
        /// the file.
        /// </summary>
        IList<PerforceFileAction> OtherActions { get; }

        /// <summary>
        /// Gets a value indicating whether the file is locked by another user.
        /// </summary>
        bool OtherLock { get; }
        #endregion Properties
    } // RSG.Editor.IPerforceFileMetaData {Interface}
} // RSG.Editor {Namespace}
