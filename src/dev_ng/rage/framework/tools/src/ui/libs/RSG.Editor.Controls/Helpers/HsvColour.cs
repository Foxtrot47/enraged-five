﻿//---------------------------------------------------------------------------------------------
// <copyright file="HsvColour.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Helpers
{
    using System;
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Contains the cylindrical coordinate representation of a RSG colour with hue, saturation
    /// and value/brightness.
    /// The hue value ranges from 0 - 360, and represents the angle on the colour wheel.
    /// The saturation ranges from 0 - 1, and represents the radius on the colour wheel.
    /// The value ranges from 0 - 1, and represents the brightness percentage.
    /// </summary>
    public struct HsvColour : IEquatable<HsvColour>
    {
        #region Fields
        /// <summary>
        /// The private field for the <see cref="H"/> property.
        /// </summary>
        private double _h;

        /// <summary>
        /// The private field for the <see cref="S"/> property.
        /// </summary>
        private double _s;

        /// <summary>
        /// The private field for the <see cref="V"/> property.
        /// </summary>
        private double _v;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="HsvColour"/> structure.
        /// </summary>
        /// <param name="h">
        /// The hue component of the colour.
        /// </param>
        /// <param name="s">
        /// The saturation component of the colour.
        /// </param>
        /// <param name="v">
        /// The value component of the colour.
        /// </param>
        public HsvColour(double h, double s, double v)
        {
            this._h = h;
            this._s = s;
            this._v = v;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the hue component of the colour.
        /// </summary>
        public double H
        {
            get { return this._h; }
            set { this._h = value; }
        }

        /// <summary>
        /// Gets or sets the saturation component of the colour.
        /// </summary>
        public double S
        {
            get { return this._s; }
            set { this._s = value; }
        }

        /// <summary>
        /// Gets or sets the value component of the colour.
        /// </summary>
        public double V
        {
            get { return this._v; }
            set { this._v = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Compares two colours for exact equality.
        /// </summary>
        /// <param name="colour1">
        /// The first item to compare.
        /// </param>
        /// <param name="colour2">
        /// The second item to compare.
        /// </param>
        /// <returns>
        /// True if the two objects are equal; otherwise, false.
        /// </returns>
        public static bool operator ==(HsvColour colour1, HsvColour colour2)
        {
            if (colour1.H != colour2.H)
            {
                return false;
            }

            if (colour1.S != colour2.S)
            {
                return false;
            }

            if (colour1.V != colour2.V)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Compares two colours for inequality.
        /// </summary>
        /// <param name="colour1">
        /// The first item to compare.
        /// </param>
        /// <param name="colour2">
        /// The second item to compare.
        /// </param>
        /// <returns>
        /// True if the two objects aren't equal; otherwise, false.
        /// </returns>
        public static bool operator !=(HsvColour colour1, HsvColour colour2)
        {
            return !(colour1 == colour2);
        }

        /// <summary>
        /// Equality method for two colours.
        /// </summary>
        /// <param name="colour1">
        /// The first colour to compare.
        /// </param>
        /// <param name="colour2">
        /// The second colour to compare.
        /// </param>
        /// <returns>
        /// True if the two colours are equal, otherwise false.
        /// </returns>
        public static bool Equals(HsvColour colour1, HsvColour colour2)
        {
            return colour1 == colour2;
        }

        /// <summary>
        /// Compares two colours for exact equality.
        /// </summary>
        /// <param name='colour'>
        /// The colour to compare to "this".
        /// </param>
        /// <returns>
        /// True if the two colours are equal; otherwise, false.
        /// </returns>
        public bool Equals(HsvColour colour)
        {
            return this == colour;
        }

        /// <summary>
        /// Compares two colours for exact equality.
        /// </summary>
        /// <param name='o'>
        /// The object to compare to "this".
        /// </param>
        /// <returns>
        /// True if the two colours are equal; otherwise, false.
        /// </returns>
        public override bool Equals(object o)
        {
            if (o is HsvColour)
            {
                HsvColour colour = (HsvColour)o;
                return this == colour;
            }

            return false;
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>
        /// The hash code as calculated by the runtime helper API.
        /// </returns>
        public override int GetHashCode()
        {
            return RuntimeHelpers.GetHashCode(this);
        }
        #endregion Methods
    } // RSG.Editor.Controls.Helpers.HsvColour {Structure}
} // RSG.Editor.Controls.Helpers {Namespace}
