﻿//---------------------------------------------------------------------------------------------
// <copyright file="CommandMenu.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;

    /// <summary>
    /// Provides a abstract base class to objects inside the command hierarchy that represent a
    /// simple menu that shows a subset of child command bar items.
    /// </summary>
    public abstract class CommandMenu : CommandBarItem
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CommandMenu"/> class.
        /// </summary>
        /// <param name="priority">
        /// The initial priority that this command bar item should be given.
        /// </param>
        /// <param name="startsGroup">
        /// A value indicating whether this command bar item should start a new group. (I.e.
        /// Placed directly after a separator).
        /// </param>
        /// <param name="text">
        /// The text for this command bar item.
        /// </param>
        /// <param name="id">
        /// The unique identifier that is used to reference this item.
        /// </param>
        /// <param name="parentId">
        /// The unique identifier for the parent of this item inside the command bar hierarchy.
        /// </param>
        protected CommandMenu(
            ushort priority, bool startsGroup, string text, Guid id, Guid parentId)
            : base(priority, startsGroup, text, id, parentId)
        {
        }
        #endregion Constructors
    }  // RSG.Editor.CommandMenu {Class}
} // RSG.Editor {Namespace}
