﻿//---------------------------------------------------------------------------------------------
// <copyright file="UIntToColourConverter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.View
{
    using System.Globalization;
    using System.Windows.Media;
    using RSG.Editor.Controls.Converters;

    /// <summary>
    /// Converters a single 32-bit unsigned integer value in a media colour object and back
    /// again.
    /// </summary>
    internal class UIntToColourConverter : ValueConverter<uint, Color>
    {
        #region Methods
        /// <summary>
        /// Converters the given value and returns the result.
        /// </summary>
        /// <param name="value">
        /// The original value to convert.
        /// </param>
        /// <param name="param">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A converted value.
        /// </returns>
        protected override Color Convert(uint value, object param, CultureInfo culture)
        {
            byte a = (byte)(value >> 24);
            byte r = (byte)(value >> 16);
            byte g = (byte)(value >> 8);
            byte b = (byte)(value >> 0);
            return Color.FromArgb(a, r, g, b);
        }

        /// <summary>
        /// Converts a already converted value back to its original value and returns
        /// the result.
        /// </summary>
        /// <param name="value">
        /// The target value to convert back to its source value.
        /// </param>
        /// <param name="param">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// The source value for the already converted specified value.
        /// </returns>
        protected override uint ConvertBack(Color value, object param, CultureInfo culture)
        {
            return (uint)((value.A << 24) | (value.R << 16) | (value.G << 8) | (value.B << 0));
        }
        #endregion Methods
    } // RSG.Metadata.View.UIntToColourConverter {Class}
} // RSG.Metadata.View {Namespace}
