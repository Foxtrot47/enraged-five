﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsDropDownContextMenu.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Data;
    using System.Windows.Media;
    using RSG.Editor.Controls.Commands;
    using RSG.Interop.Microsoft.Windows;

    /// <summary>
    /// Represents a button that shows a glyph on it with a pop-up context menu shown when it
    /// has been clicked.
    /// </summary>
    public class RsDropDownContextMenu : RsComboBox, INonClientArea
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="GlyphForeground"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty GlyphForegroundProperty;

        /// <summary>
        /// Identifies the <see cref="HoverBackground"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty HoverBackgroundProperty;

        /// <summary>
        /// Identifies the <see cref="HoverBorderBrush"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty HoverBorderBrushProperty;

        /// <summary>
        /// Identifies the <see cref="HoverBorderThickness"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty HoverBorderThicknessProperty;

        /// <summary>
        /// Identifies the <see cref="HoverForeground"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty HoverForegroundProperty;

        /// <summary>
        /// Identifies the <see cref="PressedBackground"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty PressedBackgroundProperty;

        /// <summary>
        /// Identifies the <see cref="PressedBorderBrush"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty PressedBorderBrushProperty;

        /// <summary>
        /// Identifies the <see cref="PressedBorderThickness"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty PressedBorderThicknessProperty;

        /// <summary>
        /// Identifies the <see cref="PressedForeground"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty PressedForegroundProperty;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsDropDownContextMenu"/> class.
        /// </summary>
        static RsDropDownContextMenu()
        {
            GlyphForegroundProperty =
                DependencyProperty.Register(
                "GlyphForeground",
                typeof(Brush),
                typeof(RsDropDownContextMenu));

            HoverBackgroundProperty =
                DependencyProperty.Register(
                "HoverBackground",
                typeof(Brush),
                typeof(RsDropDownContextMenu));

            HoverBorderBrushProperty =
                DependencyProperty.Register(
                "HoverBorderBrush",
                typeof(Brush),
                typeof(RsDropDownContextMenu));

            HoverBorderThicknessProperty =
                DependencyProperty.Register(
                "HoverBorderThickness",
                typeof(Thickness),
                typeof(RsDropDownContextMenu));

            HoverForegroundProperty =
                DependencyProperty.Register(
                "HoverForeground",
                typeof(Brush),
                typeof(RsDropDownContextMenu));

            PressedBackgroundProperty =
                DependencyProperty.Register(
                "PressedBackground",
                typeof(Brush),
                typeof(RsDropDownContextMenu));

            PressedBorderBrushProperty =
                DependencyProperty.Register(
                "PressedBorderBrush",
                typeof(Brush),
                typeof(RsDropDownContextMenu));

            PressedBorderThicknessProperty =
                DependencyProperty.Register(
                "PressedBorderThickness",
                typeof(Thickness),
                typeof(RsDropDownContextMenu));

            PressedForegroundProperty =
                DependencyProperty.Register(
                "PressedForeground",
                typeof(Brush),
                typeof(RsDropDownContextMenu));

            Button.ContentProperty.AddOwner(typeof(RsDropDownContextMenu));

            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsDropDownContextMenu),
                new FrameworkPropertyMetadata(typeof(RsDropDownContextMenu)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsDropDownContextMenu"/> class.
        /// </summary>
        public RsDropDownContextMenu()
        {
            this.ContextMenuOpening += this.OnContextMenuOpening;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the brush that is used to paint the glyph on the button.
        /// </summary>
        public object Content
        {
            get { return (object)this.GetValue(Button.ContentProperty); }
            set { this.SetValue(Button.ContentProperty, value); }
        }

        /// <summary>
        /// Gets or sets the brush that is used to paint the glyph on the button.
        /// </summary>
        public Brush GlyphForeground
        {
            get { return (Brush)this.GetValue(GlyphForegroundProperty); }
            set { this.SetValue(GlyphForegroundProperty, value); }
        }

        /// <summary>
        /// Gets or sets the brush that is used to paint the background when the user is
        /// hovering over the button.
        /// </summary>
        public Brush HoverBackground
        {
            get { return (Brush)this.GetValue(HoverBackgroundProperty); }
            set { this.SetValue(HoverBackgroundProperty, value); }
        }

        /// <summary>
        /// Gets or sets the brush that is used to paint the border when the user is
        /// hovering over the button.
        /// </summary>
        public Brush HoverBorderBrush
        {
            get { return (Brush)this.GetValue(HoverBorderBrushProperty); }
            set { this.SetValue(HoverBorderBrushProperty, value); }
        }

        /// <summary>
        /// Gets or sets the thickness of the border when the user is hovering over the button.
        /// </summary>
        public Thickness HoverBorderThickness
        {
            get { return (Thickness)this.GetValue(HoverBorderThicknessProperty); }
            set { this.SetValue(HoverBorderThicknessProperty, value); }
        }

        /// <summary>
        /// Gets or sets the brush that is used to paint the foreground when the user is
        /// hovering over the button.
        /// </summary>
        public Brush HoverForeground
        {
            get { return (Brush)this.GetValue(HoverForegroundProperty); }
            set { this.SetValue(HoverForegroundProperty, value); }
        }

        /// <summary>
        /// Gets or sets the brush that is used to paint the background when the user is
        /// pressing the button.
        /// </summary>
        public Brush PressedBackground
        {
            get { return (Brush)this.GetValue(PressedBackgroundProperty); }
            set { this.SetValue(PressedBackgroundProperty, value); }
        }

        /// <summary>
        /// Gets or sets the brush that is used to paint the border when the user is
        /// pressing the button.
        /// </summary>
        public Brush PressedBorderBrush
        {
            get { return (Brush)this.GetValue(PressedBorderBrushProperty); }
            set { this.SetValue(PressedBorderBrushProperty, value); }
        }

        /// <summary>
        /// Gets or sets the thickness of the border when the user is pressing the button.
        /// </summary>
        public Thickness PressedBorderThickness
        {
            get { return (Thickness)this.GetValue(PressedBorderThicknessProperty); }
            set { this.SetValue(PressedBorderThicknessProperty, value); }
        }

        /// <summary>
        /// Gets or sets the brush that is used to paint the foreground when the user is
        /// pressing the button.
        /// </summary>
        public Brush PressedForeground
        {
            get { return (Brush)this.GetValue(PressedForegroundProperty); }
            set { this.SetValue(PressedForegroundProperty, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Tests the specified point coordinate and returns a Hit Test result based on the
        /// system hot spot.
        /// </summary>
        /// <param name="point">
        /// The point to test.
        /// </param>
        /// <returns>
        /// A result indicating the position of the cursor hot spot.
        /// </returns>
        public NcHitTestResult HitTest(Point point)
        {
            return NcHitTestResult.CLIENT;
        }

        /// <summary>
        /// Opens the context menu for this button.
        /// </summary>
        public void ShowDropDown()
        {
            if (this.ContextMenu == null || this.ContextMenu.Items == null)
            {
                return;
            }

            if (this.ContextMenu.Items.Count == 0)
            {
                ItemsControl itemsControl = this.ContextMenu.TemplatedParent as ItemsControl;
                if (itemsControl == null || !itemsControl.HasItems)
                {
                    return;
                }
            }

            CollectionView collectionView = this.ContextMenu.ItemsSource as CollectionView;
            if (collectionView != null)
            {
                collectionView.Refresh();
            }

            this.ContextMenu.Placement = PlacementMode.Bottom;
            this.ContextMenu.PlacementTarget = this;
            this.ContextMenu.IsOpen = true;
        }

        /// <summary>
        /// Gets called whenever the context menu for this control is currently being opened.
        /// </summary>
        /// <param name="sender">
        /// The object that this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Controls.ContextMenuEventArgs data for this event.
        /// </param>
        private void OnContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            if (this.ContextMenu != null)
            {
                this.ContextMenu.IsOpen = false;
            }

            e.Handled = true;
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsDropDownContextMenu {Class}
} // RSG.Editor.Controls {Namespace}
