﻿//---------------------------------------------------------------------------------------------
// <copyright file="MultiCommandItemInstance.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.ComponentModel;
    using System.Windows.Input;

    /// <summary>
    /// Represents the item to use for a single multi item command item.
    /// </summary>
    internal class MultiCommandItemInstance : CommandInstance
    {
        #region Fields
        /// <summary>
        /// The private reference to the command item that is being instanced.
        /// </summary>
        private IMultiCommandItem _item;

        /// <summary>
        /// The private field used for the <see cref="OriginalInstance"/> property.
        /// </summary>
        private CommandInstance _originalInstance;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MultiCommandItemInstance"/> class.
        /// </summary>
        /// <param name="instance">
        /// The original command instance that is being expanded.
        /// </param>
        /// <param name="item">
        /// The multi command item that is being instanced.
        /// </param>
        public MultiCommandItemInstance(CommandInstance instance, IMultiCommandItem item)
            : base(instance.Command, 0, false, item.Text, Guid.Empty, instance.ParentId)
        {
            this._originalInstance = instance;
            this._item = item;
            PropertyChangedEventManager.AddHandler(item, this.OnItemChanged, String.Empty);
            PropertyChangedEventManager.AddHandler(instance, this.OnItemChanged, String.Empty);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether the individual items are shown to the user for this
        /// instance.
        /// </summary>
        public override bool AreMultiItemsShown
        {
            get { return this._originalInstance.AreMultiItemsShown; }
            set { this._originalInstance.AreMultiItemsShown = value; }
        }

        /// <summary>
        /// Gets or sets a value that can be used to change how the control that is used to
        /// display this item gets styled.
        /// </summary>
        public override CommandItemDisplayStyle DisplayStyle
        {
            get { return this._originalInstance.DisplayStyle; }
            set { this._originalInstance.DisplayStyle = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the individual items start a group each or
        /// not. Only applicable when instancing a multi command with
        /// <see cref="AreMultiItemsShown"/> set to true.
        /// </summary>
        public override bool EachItemStartsGroup
        {
            get { return this._originalInstance.EachItemStartsGroup; }
            set { this._originalInstance.EachItemStartsGroup = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this command bar item is visible to the
        /// user.
        /// </summary>
        public override bool IsVisible
        {
            get { return this._originalInstance.IsVisible; }
            set { this._originalInstance.IsVisible = value; }
        }

        /// <summary>
        /// Gets the reference to the original command instance that has been expanded into
        /// multiple items.
        /// </summary>
        public CommandInstance OriginalInstance
        {
            get { return this._originalInstance; }
        }

        /// <summary>
        /// Gets or sets the priority of the item which indicates the position of it relative
        /// to the other items under the same parent.
        /// </summary>
        public override ushort Priority
        {
            get { return this._originalInstance.Priority; }
            set { this._originalInstance.Priority = value; }
        }

        /// <summary>
        /// Gets the number of siblings the are apart of the same multi command expansion as
        /// this instance.
        /// </summary>
        public int SiblingCount
        {
            get { return this._item.ParentDefinition.Items.Count - 1; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this item is proceeded by a separator to
        /// indicate the start of a new group inside the command bar hierarchy.
        /// </summary>
        public override bool StartsGroup
        {
            get { return this._originalInstance.StartsGroup; }
            set { this._originalInstance.StartsGroup = value; }
        }

        /// <summary>
        /// Gets or sets the text for this command bar item. This appears on the menu item in
        /// the hierarchy.
        /// </summary>
        public override string Text
        {
            get { return this._item.Text; }
            set { this._item.Text = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Gets called whenever a property on the associated item changes.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.ComponentModel.PropertyChangedEventArgs event data.
        /// </param>
        private void OnItemChanged(object s, PropertyChangedEventArgs e)
        {
            this.NotifyPropertyChanged(e.PropertyName);
        }
        #endregion Methods
    } // RSG.Editor.MultiCommandItemInstance {Class}
} // RSG.Editor {Namespace}
