﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace RSG.Editor.Controls.MapViewport.Annotations
{
    /// <summary>
    /// View model for use with the <see cref="AnnoationCoordinatesWindow"/> class.
    /// </summary>
    public class AnnotationCoordinatesViewModel : NotifyPropertyChangedBase
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="X"/> property.
        /// </summary>
        private double _x;

        /// <summary>
        /// Private field for the <see cref="Y"/> property.
        /// </summary>
        private double _y;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="AnnotationCoordinatesViewModel"/>
        /// class.
        /// </summary>
        public AnnotationCoordinatesViewModel()
            : this(0.0, 0.0)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="AnnotationCoordinatesViewModel"/>
        /// class using the specified x and y coordinates.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public AnnotationCoordinatesViewModel(double x, double y)
        {
            _x = x;
            _y = y;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the x coordinate.
        /// </summary>
        public double X
        {
            get { return _x; }
            set { SetProperty(ref _x, value); }
        }

        /// <summary>
        /// Gets or sets the y coordinate.
        /// </summary>
        public double Y
        {
            get { return _y; }
            set { SetProperty(ref _y, value); }
        }
        #endregion
    }
}
