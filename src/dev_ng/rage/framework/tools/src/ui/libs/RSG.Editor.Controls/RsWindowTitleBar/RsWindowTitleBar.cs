﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsWindowTitleBar.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using RSG.Editor.Controls.Helpers;
    using RSG.Interop.Microsoft.Windows;

    /// <summary>
    /// Represents the control used to encapsulate a title bar for a <see cref="RsWindow"/>,
    /// including the system buttons, icon and title.
    /// </summary>
    public class RsWindowTitleBar : Control, INonClientArea
    {
        #region Properties
        /// <summary>
        /// Identifies the <see cref="EnableCloseButton"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty EnableCloseButtonProperty;

        /// <summary>
        /// Identifies the <see cref="ShowHelpButton"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ShowHelpButtonProperty;

        /// <summary>
        /// Identifies the <see cref="ShowMaximiseButton"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ShowMaximiseButtonProperty;

        /// <summary>
        /// Identifies the <see cref="ShowMinimiseButton"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ShowMinimiseButtonProperty;

        /// <summary>
        /// Identifies the <see cref="ShowOptionsButton"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ShowOptionsButtonProperty;

        /// <summary>
        /// Identifies the <see cref="Title"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty TitleProperty;
        #endregion Properties

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsWindowTitleBar" /> class.
        /// </summary>
        static RsWindowTitleBar()
        {
            TitleProperty = Window.TitleProperty.AddOwner(typeof(RsWindowTitleBar));

            EnableCloseButtonProperty =
                DependencyProperty.Register(
                "EnableCloseButton",
                typeof(bool),
                typeof(RsWindowTitleBar),
                new PropertyMetadata(true));

            ShowHelpButtonProperty =
                DependencyProperty.Register(
                "ShowHelpButton",
                typeof(bool),
                typeof(RsWindowTitleBar),
                new PropertyMetadata(false));

            ShowMinimiseButtonProperty =
                DependencyProperty.Register(
                "ShowMinimiseButton",
                typeof(bool),
                typeof(RsWindowTitleBar),
                new PropertyMetadata(true));

            ShowMaximiseButtonProperty =
                DependencyProperty.Register(
                "ShowMaximiseButton",
                typeof(bool),
                typeof(RsWindowTitleBar),
                new PropertyMetadata(true));

            ShowOptionsButtonProperty =
                 DependencyProperty.Register(
                 "ShowOptionsButton",
                 typeof(bool),
                 typeof(RsWindowTitleBar),
                 new PropertyMetadata(false));

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsWindowTitleBar),
                new FrameworkPropertyMetadata(typeof(RsWindowTitleBar)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsWindowTitleBar"/> class.
        /// </summary>
        public RsWindowTitleBar()
        {
#if FALSE
            System.Drawing.Font font = System.Drawing.SystemFonts.CaptionFont;
            double fontSize = font.Size;
            if (font.Unit != System.Drawing.GraphicsUnit.Pixel)
            {
                fontSize = (font.SizeInPoints / 72.0) * DpiHelper.DeviceDpiY;
            }

            this.FontSize = fontSize;
#endif
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether the close button is enabled or disabled.
        /// </summary>
        public bool EnableCloseButton
        {
            get { return (bool)this.GetValue(EnableCloseButtonProperty); }
            set { this.SetValue(EnableCloseButtonProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the help button is shown to the user.
        /// </summary>
        public bool ShowHelpButton
        {
            get { return (bool)this.GetValue(ShowHelpButtonProperty); }
            set { this.SetValue(ShowHelpButtonProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the maximise button is shown to the user.
        /// </summary>
        public bool ShowMaximiseButton
        {
            get { return (bool)this.GetValue(ShowMaximiseButtonProperty); }
            set { this.SetValue(ShowMaximiseButtonProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the minimise button is shown to the user.
        /// </summary>
        public bool ShowMinimiseButton
        {
            get { return (bool)this.GetValue(ShowMinimiseButtonProperty); }
            set { this.SetValue(ShowMinimiseButtonProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the options button is shown to the user.
        /// </summary>
        public bool ShowOptionsButton
        {
            get { return (bool)this.GetValue(ShowOptionsButtonProperty); }
            set { this.SetValue(ShowOptionsButtonProperty, value); }
        }

        /// <summary>
        /// Gets or sets the window's title.
        /// </summary>
        public string Title
        {
            get { return (string)this.GetValue(TitleProperty); }
            set { this.SetValue(TitleProperty, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Tests the specified point coordinate and returns a Hit Test result based on the
        /// system hot spot.
        /// </summary>
        /// <param name="point">
        /// The point to test.
        /// </param>
        /// <returns>
        /// A result indicating the position of the cursor hot spot.
        /// </returns>
        NcHitTestResult INonClientArea.HitTest(Point point)
        {
            return NcHitTestResult.CAPTION;
        }

        /// <summary>
        /// Determines whether the specified point coordinate value is within the bounds of
        /// this visual object.
        /// </summary>
        /// <param name="hitTestParameters">
        /// Describes the hit test to perform, including the initial hit point.
        /// </param>
        /// <returns>
        /// Results of the test, including the evaluated point.
        /// </returns>
        protected override HitTestResult HitTestCore(PointHitTestParameters hitTestParameters)
        {
            if (hitTestParameters == null)
            {
                throw new SmartArgumentNullException(() => hitTestParameters);
            }

            return new PointHitTestResult(this, hitTestParameters.HitPoint);
        }

        /// <summary>
        /// Invoked when an unhandled System.Windows.UIElement.MouseRightButtonUp routed
        /// event reaches this element.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.MouseButtonEventArgs for this event.
        /// </param>
        protected override void OnMouseRightButtonUp(MouseButtonEventArgs e)
        {
            if (e == null)
            {
                throw new SmartArgumentNullException(() => e);
            }

            RsWindow window = this.GetVisualAncestor<RsWindow>();
            if (window == null)
            {
                return;
            }

            Point position = e.GetPosition(this);
            window.ShowWindowMenu(this, position, this.RenderSize);
            e.Handled = true;
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsWindowTitleBar {Class}
} // RSG.Editor.Controls {Namespace}
