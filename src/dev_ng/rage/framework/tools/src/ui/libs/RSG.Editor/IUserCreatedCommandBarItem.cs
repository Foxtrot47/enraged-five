﻿//---------------------------------------------------------------------------------------------
// <copyright file="IUserCreatedCommandBarItem.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Xml;

    /// <summary>
    /// When implemented represents a command bar item that has been created by the user.
    /// </summary>
    internal interface IUserCreatedCommandBarItem
    {
        #region Methods
        /// <summary>
        /// Serialises this command to the end of the specified writer.
        /// </summary>
        /// <param name="writer">
        /// The xml writer that the command gets serialised out to.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        void SerialiseCommand(XmlWriter writer, IFormatProvider provider);
        #endregion Methods
    } // RSG.Editor.IUserCreatedCommandBarItem {Interface}
} // RSG.Editor {Namespace}
