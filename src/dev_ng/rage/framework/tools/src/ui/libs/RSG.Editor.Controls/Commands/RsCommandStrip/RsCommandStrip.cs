﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsCommandStrip.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Commands
{
    using System.Windows;
    using System.Windows.Controls.Primitives;
    using System.Windows.Data;

    /// <summary>
    /// Represents a control that enables you to display elements associated with commands and
    /// event handlers in a single horizontal or vertical strip.
    /// </summary>
    public class RsCommandStrip : RsToolBar
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="CommandTarget"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty CommandTargetProperty;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsCommandStrip"/> class.
        /// </summary>
        static RsCommandStrip()
        {
            CommandTargetProperty =
                DependencyProperty.Register(
                "CommandTarget",
                typeof(IInputElement),
                typeof(RsCommandStrip),
                new FrameworkPropertyMetadata(null));

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsCommandStrip),
                new FrameworkPropertyMetadata(typeof(RsCommandStrip)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsCommandStrip"/> class.
        /// </summary>
        public RsCommandStrip()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the element on which to raise the commands bound to the items inside
        /// this control.
        /// </summary>
        public IInputElement CommandTarget
        {
            get { return (IInputElement)this.GetValue(CommandTargetProperty); }
            set { this.SetValue(CommandTargetProperty, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Prepares the specified element to display the specified item.
        /// </summary>
        /// <param name="element">
        /// Element used to display the specified item.
        /// </param>
        /// <param name="item">
        /// Specified item.
        /// </param>
        protected override void PrepareContainerForItemOverride(
            DependencyObject element, object item)
        {
            base.PrepareContainerForItemOverride(element, item);

            FrameworkElement frameworkElement = element as FrameworkElement;
            if (frameworkElement == null)
            {
                return;
            }

            Binding binding = new Binding("CommandTarget");
            binding.Source = this;
            frameworkElement.SetBinding(ButtonBase.CommandTargetProperty, binding);
        }
        #endregion Methods
    } // RSG.Editor.Controls.Commands.RsCommandStrip {Class}
} // RSG.Editor.Controls.Commands {Namespace}
