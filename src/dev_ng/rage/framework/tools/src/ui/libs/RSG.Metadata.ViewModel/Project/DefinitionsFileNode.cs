﻿//---------------------------------------------------------------------------------------------
// <copyright file="DefinitionsFileNode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Project
{
    using System.Collections.Generic;
    using RSG.Metadata.Model.Definitions;
    using RSG.Project.ViewModel;

    /// <summary>
    /// The node inside the project explorer that represents a parCodeGen file containing
    /// definitions. This class cannot be inherited.
    /// </summary>
    public sealed class DefinitionsFileNode : HierarchyNode
    {
        #region Fields
        /// <summary>
        /// The full path to the file this node is representing.
        /// </summary>
        private string _fullPath;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DefinitionsFileNode"/> class.
        /// </summary>
        /// <param name="fullPath">
        /// The full path to the file this node is representing.
        /// </param>
        /// <param name="parent">
        /// The parent node.
        /// </param>
        public DefinitionsFileNode(string fullPath, IHierarchyNode parent)
            : base(false)
        {
            this._fullPath = fullPath;
            this.Text = System.IO.Path.GetFileName(fullPath);
            this.Parent = parent;
            this.Icon = Icons.DefinitionFileNodeIcon;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether this items children are destroyed when it is
        /// collapsed. The resulting behaviour if true is that the references are kept in a
        /// cache and the expansion state is persistent for child nodes.
        /// </summary>
        public override bool DestroyNodesOnCollapse
        {
            get { return true; }
        }

        /// <summary>
        /// Gets the priority value for this object. By default all objects are 0.
        /// </summary>
        public override int Priority
        {
            get { return 1; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates the child elements for this view model. This is only called once the item
        /// has been expanded in the user interface.
        /// </summary>
        /// <param name="collection">
        /// The collection to add the elements to.
        /// </param>
        protected override void CreateChildren(ICollection<IHierarchyNode> collection)
        {
            StandardProjectNode project = this.ParentProject as StandardProjectNode;
            if (project == null)
            {
                return;
            }

            project.EnsureDefinitionsAreLoaded();
            foreach (IStructure structure in project.GetStructures(this._fullPath))
            {
                collection.Add(new StructureDefinitionNode(structure, this));
            }

            foreach (IEnumeration enumeration in project.GetEnumerations(this._fullPath))
            {
                collection.Add(new EnumerationDefinitionNode(enumeration, this));
            }

            foreach (IConstant constant in project.GetConstants(this._fullPath))
            {
                collection.Add(new ConstantNode(constant, this));
            }
        }
        #endregion Methodss
    } // RSG.Metadata.ViewModel.Project.DefinitionsFileNode {Class}
} // RSG.Metadata.ViewModel.Project {Namespace}
