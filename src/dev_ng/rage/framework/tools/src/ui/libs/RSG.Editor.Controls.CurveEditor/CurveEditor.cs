﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using RSG.Base.Collections;
using RSG.Editor.Controls.Commands;
using RSG.Editor.Controls.CurveEditor.Commands;
using RSG.Editor.Controls.CurveEditor.Commmands;
using RSG.Editor.Controls.CurveEditor.ViewModel;
using RSG.Editor.Controls.Helpers;
using RSG.Editor.Controls.ZoomCanvas;
using RSG.Editor.Controls.ZoomCanvas.Commands;
using RSG.Editor.Model;

namespace RSG.Editor.Controls.CurveEditor
{
    /// <summary>
    /// 
    /// </summary>
    public class CurveEditor : ItemsControl
    {
        #region Constants
        /// <summary>
        /// Toolbar id.
        /// </summary>
        private static readonly Guid _toolBarId = new Guid("7D61A2D0-784E-4B0F-A231-70FFA8C2653C");
        #endregion // Constants

        #region Fields
        /// <summary>
        /// Reference to the points listbox for managing point selection.
        /// </summary>
        private ListBox _pointsListBox;

        /// <summary>
        /// Reference to the zoomable canvas for hooking up event handlers.
        /// </summary>
        private ZoomableCanvas _zoomableCanvas;

        /// <summary>
        /// Reference to the curve geometry items control for hit testing against the paths.
        /// </summary>
        private ItemsControl _curveGeometryItemsControl;

        /// <summary>
        /// Adorner for displaying a selection area/zoom region area.
        /// </summary>
        private AreaAdorner _adorner;

        /// <summary>
        /// Flag indicating whether we are currently using the quick pan feature.
        /// </summary>
        private bool _isQuickPanning = false;

        /// <summary>
        /// Flag indicating that we are drag selecting points.
        /// </summary>
        private bool _isDragSelectingPoints = false;

        /// <summary>
        /// Flag indicating whether we are in the process of panning or zooming the zoomable canvas.
        /// </summary>
        private bool _isPanOrZooming = false;

        /// <summary>
        /// Stores the last mouse position when moving the mouse.
        /// </summary>
        private Point _lastMousePosition;

        /// <summary>
        /// Stores the original mouse position where the mouse cursor was pressed.
        /// </summary>
        private Point _originalMousePosition;

        /// <summary>
        /// 
        /// </summary>
        private UndoRedoBatch _currentUndoBatch;
        #endregion // Fields

        #region AllPoints Dependency Property
        /// <summary>
        /// 
        /// </summary>
        public static readonly DependencyProperty AllPointsProperty =
            DependencyProperty.Register(
                "AllPoints",
                typeof(CompositeCollection),
                typeof(CurveEditor),
                new PropertyMetadata(new CompositeCollection()));

        /// <summary>
        /// 
        /// </summary>
        public CompositeCollection AllPoints
        {
            get { return (CompositeCollection)GetValue(AllPointsProperty); }
            set { SetValue(AllPointsProperty, value); }
        }
        #endregion // AllPoints Dependency Property

        #region Scale/Offset Dependency Properties
        /// <summary>
        /// 
        /// </summary>
        public static readonly DependencyProperty ScaleProperty =
            DependencyProperty.Register(
                "Scale",
                typeof(Vector),
                typeof(CurveEditor),
                new PropertyMetadata(new Vector(1.0, 1.0), OnScaleChanged, CoerceScaleValue));

        /// <summary>
        /// 
        /// </summary>
        public Vector Scale
        {
            get { return (Vector)GetValue(ScaleProperty); }
            set { SetValue(ScaleProperty, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnScaleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            CurveEditor curve = (CurveEditor)d;
            curve.UpdateGuideLineCellSize((Vector)e.NewValue);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="d"></param>
        /// <param name="baseValue"></param>
        /// <returns></returns>
        private static object CoerceScaleValue(DependencyObject d, object baseValue)
        {
            // Make sure that neither of the scale values are 0.
            // TODO: Potentially extend to min/max scale.
            Vector value = (Vector)baseValue;
            if (value.X == 0.0 || value.Y == 0.0)
            {
                value = new Vector(1.0, 1.0);
            }
            return value;
        }

        /// <summary>
        /// 
        /// </summary>
        public static readonly DependencyProperty OffsetProperty =
            DependencyProperty.Register(
                "Offset",
                typeof(Point),
                typeof(CurveEditor),
                new PropertyMetadata(new Point(0.0, 0.0)));

        /// <summary>
        /// 
        /// </summary>
        public Point Offset
        {
            get { return (Point)GetValue(OffsetProperty); }
            set { SetValue(OffsetProperty, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public static readonly DependencyProperty ViewboxProperty =
            DependencyProperty.Register(
                "Viewbox",
                typeof(Rect),
                typeof(CurveEditor),
                new PropertyMetadata(Rect.Empty));

        /// <summary>
        /// 
        /// </summary>
        public Rect Viewbox
        {
            get { return (Rect)GetValue(ViewboxProperty); }
            set { SetValue(ViewboxProperty, value); }
        }
        #endregion // Scale/Offset Dependency Properties

        #region GuideLinesCellSize Dependency Property
        /// <summary>
        /// 
        /// </summary>
        public static readonly DependencyPropertyKey CellSizePropertyKey =
            DependencyProperty.RegisterReadOnly(
                "CellSize",
                typeof(Size),
                typeof(CurveEditor),
                new FrameworkPropertyMetadata(new Size(20.0, 20.0)));

        /// <summary>
        /// 
        /// </summary>
        public static readonly DependencyProperty CellSizeProperty =
            CellSizePropertyKey.DependencyProperty;

        /// <summary>
        /// 
        /// </summary>
        public Size CellSize
        {
            get { return (Size)GetValue(CellSizeProperty); }
            private set { SetValue(CellSizePropertyKey, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public static readonly DependencyPropertyKey ScaledCellSizePropertyKey =
            DependencyProperty.RegisterReadOnly(
                "ScaledCellSize",
                typeof(Size),
                typeof(CurveEditor),
                new FrameworkPropertyMetadata(new Size(20.0, 20.0)));

        /// <summary>
        /// 
        /// </summary>
        public static readonly DependencyProperty ScaledCellSizeProperty =
            ScaledCellSizePropertyKey.DependencyProperty;

        /// <summary>
        /// 
        /// </summary>
        public Size ScaledCellSize
        {
            get { return (Size)GetValue(ScaledCellSizeProperty); }
            private set { SetValue(ScaledCellSizePropertyKey, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private void UpdateGuideLineCellSize(Vector scale)
        {
            ScaledCellSize = new Size(CellSize.Width * scale.X, CellSize.Height * scale.Y);
        }
        #endregion // GuideLinesCellSize Dependency Property

        #region InteractionMode Dependency Property
        /// <summary>
        /// 
        /// </summary>
        public static readonly DependencyProperty InteractionModeProperty =
            DependencyProperty.Register(
                "InteractionMode",
                typeof(InteractionMode),
                typeof(CurveEditor),
                new PropertyMetadata(InteractionMode.MoveFree, OnInteractionModeChanged));

        /// <summary>
        /// 
        /// </summary>
        public InteractionMode InteractionMode
        {
            get { return (InteractionMode)GetValue(InteractionModeProperty); }
            set { SetValue(InteractionModeProperty, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnInteractionModeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            CurveEditor curve = (CurveEditor)d;
            if (curve._zoomableCanvas != null)
            {
                curve.DisconnectInteractionModeHandlers((InteractionMode)e.OldValue);
                curve.ConnectInteractionModeHandlers((InteractionMode)e.NewValue);
            }
        }
        #endregion // InteractionMode Dependency Property

        #region HorizontalLimits Dependency Properties
        /// <summary>
        /// Identifies the <see cref="HorizontalLimits"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty HorizontalLimitsProperty =
            DependencyProperty.Register(
                "HorizontalLimits",
                typeof(Range<float>),
                typeof(CurveEditor),
                new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsRender));

        /// <summary>
        /// Gets or sets the valid horizontal workable area.
        /// </summary>
        public Range<float> HorizontalLimits
        {
            get { return (Range<float>)GetValue(HorizontalLimitsProperty); }
            set { SetValue(HorizontalLimitsProperty, value); }
        }
        #endregion // HorizontalLimits Dependency Properties

        #region ZoomExtentsOnLoad Dependency Property
        /// <summary>
        /// 
        /// </summary>
        public static readonly DependencyProperty ZoomExtentsOnLoadProperty =
            DependencyProperty.Register(
                "ZoomExtentsOnLoad",
                typeof(bool),
                typeof(CurveEditor),
                new FrameworkPropertyMetadata(false));

        /// <summary>
        /// 
        /// </summary>
        public bool ZoomExtentsOnLoad
        {
            get { return (bool)GetValue(ZoomExtentsOnLoadProperty); }
            set { SetValue(ZoomExtentsOnLoadProperty, value); }
        }
        #endregion // ZoomExtentsOnLoad Dependency Property

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public CurveEditor()
        {
            Loaded += CurveEditor_Loaded;

            AttachCommandBindings();
            
            // Set up the initial zoom extents.
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Loaded, new Action(() =>
                {
                    if (ZoomExtentsOnLoad)
                    {
                        InitialZoomExtents();
                    }
                }));
        }

        /// <summary>
        /// Static constructor for overriding inherited dependency property metadata
        /// and registering routed event handlers.
        /// </summary>
        static CurveEditor()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(CurveEditor),
                new FrameworkPropertyMetadata(typeof(CurveEditor)));

            ItemsSourceProperty.OverrideMetadata(
                typeof(CurveEditor),
                new FrameworkPropertyMetadata(OnItemsChanged));

            // Register routed event handlers for the list box item drag events.
            EventManager.RegisterClassHandler(
                typeof(CurveEditor),
                RsDraggableListBoxItem.DragStartedEvent,
                new RoutedEventHandler(OnPointDragStarted));

            EventManager.RegisterClassHandler(
                typeof(CurveEditor),
                RsDraggableListBoxItem.DragDeltaEvent,
                new EventHandler<RsDragEventArgs>(OnPointDragDelta));

            EventManager.RegisterClassHandler(
                typeof(CurveEditor),
                RsDraggableListBoxItem.DragCompletedEvent,
                new RoutedEventHandler(OnPointDragCompleted));

            // Register a routed event handler for listening to the zoomable canvases actual
            // viewbox change events.
            EventManager.RegisterClassHandler(
                typeof(CurveEditor),
                ZoomableCanvas.ActualViewboxChangedEvent,
                new EventHandler<RoutedValueChangedEventArgs<Rect>>(OnViewboxChanged));

            AddCommandBarItems();
        }
        #endregion // Constructor(s)

        #region Editor Commands
        /// <summary>
        /// Gets the collection containing all of the commands that should be shown on the tool
        /// bar tray.
        /// </summary>
        [Bindable(true)]
        public CommandBarItemViewModelCollection ToolBarCommands
        {
            get { return CommandBarItemViewModel.Create(_toolBarId); }
        }

        /// <summary>
        /// 
        /// </summary>
        private void AttachCommandBindings()
        {
            new InteractionModeAction(this.CurveEditorResolver).AddBinding(CurveEditorCommands.InteractionMode, this);
            new ZoomAction(this.ZoomableCanvasResolver).AddBinding(ZoomCanvasCommands.Zoom, this);
            new ZoomExtentsAction(this.ZoomableCanvasResolver).AddBinding(ZoomCanvasCommands.ZoomExtents, this);
        }

        /// <summary>
        /// Represents the method that is used to retrieve a command parameter object for the
        /// view commands.
        /// </summary>
        /// <param name="command">
        /// The command whose command parameter needs to be resolved.
        /// </param>
        /// <returns>
        /// The command parameter object for the specified command.
        /// </returns>
        private CurveEditor CurveEditorResolver(CommandData data)
        {
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        private ZoomableCanvas ZoomableCanvasResolver(CommandData data)
        {
            return _zoomableCanvas;
        }

        /// <summary>
        /// 
        /// </summary>
        private static void AddCommandBarItems()
        {
            RockstarCommandManager.AddCommandInstance(
                CurveEditorCommands.InteractionMode,
                0,
                false,
                "Interaction Mode",
                new Guid("FC62DBF9-1B10-47D5-A2DA-C00AC9B6E2B7"),
                _toolBarId,
                new CommandInstanceCreationArgs
                {
                    AreMultiItemsShown = true,
                    EachItemStartsGroup = false,
                    DisplayStyle = CommandItemDisplayStyle.ImageOnly
                });

            ZoomCanvasCommandInstance instance =
                new ZoomCanvasCommandInstance(
                    ZoomCanvasCommands.Zoom,
                    1,
                    true,
                    "Zoom In",
                    new Guid("A4F4D114-7F6B-4165-ABDC-23D7AFAA8CD2"),
                    _toolBarId,
                    ZoomCanvasIcons.ZoomIn,
                    ZoomAction.ZoomInAboutCenterParameters);
            RockstarCommandManager.AddCommandBarItem(instance);

            instance =
                new ZoomCanvasCommandInstance(
                    ZoomCanvasCommands.Zoom,
                    2,
                    false,
                    "Zoom Out",
                    new Guid("F6993643-8DC4-41AE-BB23-D59027841A96"),
                    _toolBarId,
                    ZoomCanvasIcons.ZoomOut,
                    ZoomAction.ZoomOutAboutCenterParameters);
            RockstarCommandManager.AddCommandBarItem(instance);

            instance =
                new ZoomCanvasCommandInstance(
                    ZoomCanvasCommands.ZoomExtents,
                    2,
                    false,
                    "Zoom Extents",
                    new Guid("14ECAE39-A777-4B87-8566-5FB3BBB41BA1"),
                    _toolBarId,
                    ZoomCanvasIcons.ZoomExtents,
                    ZoomExtentsMode.HorizontalAndVertical);
            RockstarCommandManager.AddCommandBarItem(instance);

            instance =
                new ZoomCanvasCommandInstance(
                    ZoomCanvasCommands.ZoomExtents,
                    2,
                    false,
                    "Zoom Horizontal Extents",
                    new Guid("C6042CE9-A6AF-417A-9379-8AE64B55A148"),
                    _toolBarId,
                    ZoomCanvasIcons.ZoomHorizontalExtents,
                    ZoomExtentsMode.Horizontal);
            RockstarCommandManager.AddCommandBarItem(instance);

            instance =
                new ZoomCanvasCommandInstance(
                    ZoomCanvasCommands.ZoomExtents,
                    2,
                    false,
                    "Zoom Vertical Extents",
                    new Guid("9B726C7D-3F24-4A6B-AC8A-AA004E3F2AAD"),
                    _toolBarId,
                    ZoomCanvasIcons.ZoomVerticalExtents,
                    ZoomExtentsMode.Vertical);
            RockstarCommandManager.AddCommandBarItem(instance);
        }
        #endregion // Commands

        #region Initialisation
        /// <summary>
        /// 
        /// </summary>
        private void InitialZoomExtents()
        {
            Rect adjustedExtents = _zoomableCanvas.Extent;
            if (adjustedExtents.IsEmpty)
            {
                adjustedExtents.Union(new Point(HorizontalLimits.Start, 0.0));
                adjustedExtents.Union(new Point(HorizontalLimits.End, -HorizontalLimits.End));
            }
            if (adjustedExtents.Height == 0)
            {
                adjustedExtents.Height = adjustedExtents.Width;
            }

            _zoomableCanvas.ZoomToContent(adjustedExtents, true, true, 25.0);
        }
        #endregion // Initialisation

        #region Property Changed Callbacks/Routed Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnItemsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            CurveEditor curve = (CurveEditor)d;
            curve.OnItemsChanged((IEnumerable)e.OldValue, (IEnumerable)e.NewValue);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oldValue"></param>
        /// <param name="newValue"></param>
        private void OnItemsChanged(IEnumerable oldValue, IEnumerable newValue)
        {
            // Check whether we need to remove/add weak event listeners for the points changing.
            if (oldValue is INotifyCollectionChanged)
            {
                (oldValue as INotifyCollectionChanged).CollectionChanged -= CurveEditor_CollectionChanged;
                AllPoints.Clear();
            }

            if (newValue is INotifyCollectionChanged)
            {
                (newValue as INotifyCollectionChanged).CollectionChanged += CurveEditor_CollectionChanged;


                AllPoints.Clear();
                foreach (CurveViewModel vm in ItemsSource.OfType<CurveViewModel>())
                {
                    CollectionContainer pointsContainer = new CollectionContainer();
                    pointsContainer.Collection = vm.Points;
                    AllPoints.Add(pointsContainer);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CurveEditor_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        for (int i = 0; i < e.NewItems.Count; i++)
                        {
                            CurveViewModel item = (CurveViewModel)e.NewItems[i];
                            CollectionContainer pointsContainer = new CollectionContainer();
                            pointsContainer.Collection = item.Points;
                            AllPoints.Insert(e.NewStartingIndex + i, pointsContainer);
                        }
                    }

                    break;
                case NotifyCollectionChangedAction.Remove:
                    {
                        for (int i = 0; i < e.OldItems.Count; i++)
                        {
                            AllPoints.RemoveAt(e.OldStartingIndex);
                        }
                    }

                    break;
                case NotifyCollectionChangedAction.Replace:
                    {
                        for (int i = 0; i < e.NewItems.Count; i++)
                        {
                            CurveViewModel item = (CurveViewModel)e.NewItems[i];
                            CollectionContainer pointsContainer = new CollectionContainer();
                            pointsContainer.Collection = item.Points;
                            AllPoints[e.OldStartingIndex + i] = pointsContainer;
                        }
                    }

                    break;
                case NotifyCollectionChangedAction.Move:
                    {
                        for (int i = 0; i < e.NewItems.Count; i++)
                        {
                            int oldIndex = e.OldStartingIndex + i;
                            int newIndex = e.NewStartingIndex + i;
                            CollectionContainer pointsContainer = (CollectionContainer)AllPoints[oldIndex];
                            AllPoints.RemoveAt(oldIndex);
                            AllPoints.Insert(newIndex, pointsContainer);
                        }
                    }

                    break;
                case NotifyCollectionChangedAction.Reset:
                    {
                        if (ItemsSource != null)
                        {
                            AllPoints.Clear();
                            foreach (CurveViewModel vm in ItemsSource.OfType<CurveViewModel>())
                            {
                                CollectionContainer pointsContainer = new CollectionContainer();
                                pointsContainer.Collection = vm.Points;
                                AllPoints.Add(pointsContainer);
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void OnViewboxChanged(object sender, RoutedValueChangedEventArgs<Rect> e)
        {
            CurveEditor curve = e.Source as CurveEditor;
            if (curve != null)
            {
                curve.Viewbox = new Rect(
                    e.NewValue.Left * curve.Scale.X,
                    e.NewValue.Top * curve.Scale.Y,
                    e.NewValue.Width * curve.Scale.X,
                    e.NewValue.Height * curve.Scale.Y);
            }
        }
        #endregion // Property Changed Callbacks/Routed Event Handlers

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            _pointsListBox = GetTemplateChild("PART_PointsListBox") as ListBox;
        }
        #endregion // Overrides

        #region Event Handlers
        /// <summary>
        /// Callback for when the curve editor is loaded to get at the zoomable canvas.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CurveEditor_Loaded(object sender, RoutedEventArgs e)
        {
            // Get the zoomable canvas.
            _zoomableCanvas = this.GetVisualDescendent<ZoomableCanvas>();
            _zoomableCanvas.MouseDown += QuickPan_MouseDown;
            _zoomableCanvas.MouseMove += QuickPan_MouseMove;
            _zoomableCanvas.MouseUp += QuickPan_MouseUp;
            _zoomableCanvas.MouseWheel += QuickZoom_MouseWheel;
            _zoomableCanvas.PreviewKeyDown += DeletePoint_KeyDown;

            // Get all ItemsControl descendents and find the one with the right name.
            foreach (ItemsControl itemsControl in this.GetVisualDescendents<ItemsControl>())
            {
                if (itemsControl.Name == "CurveGeometry")
                {
                    _curveGeometryItemsControl = itemsControl;
                }
            }
            Debug.Assert(_curveGeometryItemsControl != null, "Curve geometry items control doesn't exist!");
            /*
            // Get all ScrollViewer descendents and find the one we are after.
            ScrollViewer listboxScrollViewer = null;
            foreach (ScrollViewer scrollViewer in this.GetVisualDescendents<ScrollViewer>())
            {
                if (scrollViewer.Name == "ListBoxScrollViewer")
                {
                    listboxScrollViewer = scrollViewer;
                    break;
                }
            }
            Debug.Assert(listboxScrollViewer != null, "Listbox scrollviewer doesn't exist!");
            */
            //IScrollInfo scrollInfo = (IScrollInfo)_zoomableCanvas;
            //scrollInfo.ScrollOwner = listboxScrollViewer;

            // Create the area adorner for drag selection/zoom to region.
            CreateAdorner();

            ConnectInteractionModeHandlers(InteractionMode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeletePoint_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                // Create a copy of the selection so the enumeration doesn't get interrupted while removing the points.
                IList<CurvePointViewModel> selectionItemsCopy =
                    new List<CurvePointViewModel>(_pointsListBox.SelectedItems.OfType<CurvePointViewModel>());

                foreach (CurvePointViewModel pointVm in selectionItemsCopy)
                {
                    CurveViewModel curveVm = pointVm.Parent;
                    curveVm.RemovePoint(pointVm);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void QuickPan_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Middle)
            {
                _zoomableCanvas.CaptureMouse();
                e.Handled = true;

                // Keep track of the mouse position over the zoomable canvas.
                Point position = e.GetPosition(_zoomableCanvas);
                _lastMousePosition = position;
                _originalMousePosition = position;
                _isQuickPanning = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void QuickPan_MouseMove(object sender, MouseEventArgs e)
        {
            if (_isQuickPanning && e.MiddleButton == MouseButtonState.Pressed)
            {
                Point position = e.GetPosition(_zoomableCanvas);

                Offset -= position - _lastMousePosition;
                e.Handled = true;

                // Keep track of the mouse position over the zoomable canvas.
                _lastMousePosition = position;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void QuickPan_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Middle)
            {
                _zoomableCanvas.ReleaseMouseCapture();
                _isQuickPanning = false;
                e.Handled = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void QuickZoom_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            // Adjust the offset to make the point under the mouse stay still.
            double scaleAdjustment = Math.Pow(2, e.Delta / 3.0 / Mouse.MouseWheelDeltaForOneLine);
            Vector position = (Vector)e.GetPosition(_zoomableCanvas);

            Scale *= scaleAdjustment;
            Offset = (Point)((Vector)(Offset + position) * scaleAdjustment - position);

            e.Handled = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mode"></param>
        private void ConnectInteractionModeHandlers(InteractionMode mode)
        {
            if (mode == InteractionMode.MoveFree ||
                mode == InteractionMode.MoveHorizontally ||
                mode == InteractionMode.MoveVertically)
            {
                _zoomableCanvas.MouseLeftButtonDown += MovePointSelection_MouseLeftButtonDown;
                _zoomableCanvas.MouseMove += MovePointSelection_MouseMove;
                _zoomableCanvas.MouseLeftButtonUp += MovePointSelection_MouseLeftButtonUp;
            }
            else if (mode == InteractionMode.InsertPoint)
            {
                _zoomableCanvas.MouseLeftButtonDown += InsertPoint_MouseLeftButtonDown;
            }
            else if (mode == InteractionMode.Panning)
            {
                _zoomableCanvas.MouseLeftButtonDown += ZoomAndPan_MouseLeftButtonDown;
                _zoomableCanvas.MouseMove += Pan_MouseMove;
                _zoomableCanvas.MouseLeftButtonUp += ZoomAndPan_MouseLeftButtonUp;
            }
            else if (mode == InteractionMode.Zooming)
            {
                _zoomableCanvas.MouseLeftButtonDown += ZoomAndPan_MouseLeftButtonDown;
                _zoomableCanvas.MouseMove += Zoom_MouseMove;
                _zoomableCanvas.MouseLeftButtonUp += ZoomAndPan_MouseLeftButtonUp;
            }
            else if (mode == InteractionMode.ZoomRegion)
            {
                _zoomableCanvas.MouseLeftButtonDown += ZoomAndPan_MouseLeftButtonDown;
                _zoomableCanvas.MouseMove += ZoomRegion_MouseMove;
                _zoomableCanvas.MouseLeftButtonUp += ZoomRegion_MouseLeftButtonUp;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mode"></param>
        private void DisconnectInteractionModeHandlers(InteractionMode mode)
        {
            if (mode == InteractionMode.MoveFree ||
                mode == InteractionMode.MoveHorizontally ||
                mode == InteractionMode.MoveVertically)
            {
                _zoomableCanvas.MouseLeftButtonDown -= MovePointSelection_MouseLeftButtonDown;
                _zoomableCanvas.MouseMove -= MovePointSelection_MouseMove;
                _zoomableCanvas.MouseLeftButtonUp -= MovePointSelection_MouseLeftButtonUp;
            }
            else if (mode == InteractionMode.InsertPoint)
            {
                _zoomableCanvas.MouseLeftButtonDown -= InsertPoint_MouseLeftButtonDown;
            }
            else if (mode == InteractionMode.Panning)
            {
                _zoomableCanvas.MouseLeftButtonDown -= ZoomAndPan_MouseLeftButtonDown;
                _zoomableCanvas.MouseMove -= Pan_MouseMove;
                _zoomableCanvas.MouseLeftButtonUp -= ZoomAndPan_MouseLeftButtonUp;
            }
            else if (mode == InteractionMode.Zooming)
            {
                _zoomableCanvas.MouseLeftButtonDown -= ZoomAndPan_MouseLeftButtonDown;
                _zoomableCanvas.MouseMove -= Zoom_MouseMove;
                _zoomableCanvas.MouseLeftButtonUp -= ZoomAndPan_MouseLeftButtonUp;
            }
            else if (mode == InteractionMode.ZoomRegion)
            {
                _zoomableCanvas.MouseLeftButtonDown -= ZoomAndPan_MouseLeftButtonDown;
                _zoomableCanvas.MouseMove -= ZoomRegion_MouseMove;
                _zoomableCanvas.MouseLeftButtonUp -= ZoomRegion_MouseLeftButtonUp;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void MovePointSelection_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Point position = e.GetPosition(_zoomableCanvas);
            _zoomableCanvas.CaptureMouse();

            _lastMousePosition = position;
            _originalMousePosition = position;
            _isDragSelectingPoints = true;
            SetAdornerArea(_originalMousePosition, _originalMousePosition);
            e.Handled = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void MovePointSelection_MouseMove(object sender, MouseEventArgs e)
        {
            if (_isDragSelectingPoints && e.LeftButton == MouseButtonState.Pressed)
            {
                Point position = e.GetPosition(_zoomableCanvas);
                SetAdornerArea(_originalMousePosition, position);
                e.Handled = true;
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void MovePointSelection_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (_isDragSelectingPoints)
            {
                ApplyDragSelection();
                _zoomableCanvas.ReleaseMouseCapture();
                _isDragSelectingPoints = false;
                e.Handled = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void ApplyDragSelection()
        {
            if (!(Keyboard.Modifiers.HasFlag(ModifierKeys.Control) ||
                Keyboard.Modifiers.HasFlag(ModifierKeys.Alt)))
            {
                _pointsListBox.SelectedItem = null;
            }

            bool selectPoints = !Keyboard.Modifiers.HasFlag(ModifierKeys.Alt);

            // Apply drag selection.
            for (int i = 0; i < _pointsListBox.Items.Count; i++)
            {
                FrameworkElement item = _pointsListBox.ItemContainerGenerator.ContainerFromIndex(i) as FrameworkElement;
                if (item != null && item.Focusable)
                {
                    // Get the bounds in the parent's co-ordinates.
                    Point topLeft = item.TranslatePoint(new Point(0, 0), _zoomableCanvas);
                    Rect itemBounds = new Rect(topLeft.X, topLeft.Y, item.ActualWidth, item.ActualHeight);

                    // Only change the selection if it intersects with the area
                    // (or intersected i.e. we changed the value last time).
                    if (itemBounds.IntersectsWith(_adorner.Area))
                    {
                        Selector.SetIsSelected(item, selectPoints);
                        item.MoveFocus(new TraversalRequest(FocusNavigationDirection.First));
                    }
                }
            }

            HideAdorner();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void InsertPoint_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Point position = e.GetPosition(_zoomableCanvas);

            // Get the curve view model behind the line.
            HitTestResult result = VisualTreeHelper.HitTest(_curveGeometryItemsControl, position);
            if (result == null || result.VisualHit == null)
            {
                return;
            }

            Path path = result.VisualHit as Path;
            if (path == null)
            {
                return;
            }

            // Add in the point.
            CurveViewModel curveVm = path.DataContext as CurveViewModel;
            if (curveVm != null)
            {
                // Determine where the user clicked and convert it into canvas space.
                Point canvasPoint = _zoomableCanvas.GetCanvasPoint(position);
                canvasPoint.Y *= -1.0;

                // Add the new point to the model.
                curveVm.AddPoint(canvasPoint);
                e.Handled = true;
            }
        }

        /// <summary>
        /// Event handler for dealing with the middle click pan behaviour.
        /// </summary>
        private void ZoomAndPan_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _zoomableCanvas.CaptureMouse();

            Point position = e.GetPosition(_zoomableCanvas);
            _lastMousePosition = position;
            _originalMousePosition = position;
            _isPanOrZooming = true;
            e.Handled = true;
        }

        /// <summary>
        /// Event handler for dealing with the middle click pan behaviour.
        /// </summary>
        /// <param name="e"></param>
        private void Pan_MouseMove(object sender, MouseEventArgs e)
        {
            if (_isPanOrZooming && e.LeftButton == MouseButtonState.Pressed)
            {
                Point position = e.GetPosition(_zoomableCanvas);

                _zoomableCanvas.Offset -= position - _lastMousePosition;
                e.Handled = true;

                _lastMousePosition = position;
            }
        }

        /// <summary>
        /// Event handler for dealing with the middle click pan behaviour.
        /// </summary>
        /// <param name="e"></param>
        private void Zoom_MouseMove(object sender, MouseEventArgs e)
        {
            if (_isPanOrZooming && e.LeftButton == MouseButtonState.Pressed)
            {
                // The user is left-dragging the mouse.
                // Zoom the viewport by the appropriate amount.
                Point position = e.GetPosition(_zoomableCanvas);
                Vector dragOffset = position - _lastMousePosition;

                double scaleAdjustment = 1.0;
                if (dragOffset.Y > 0)
                {
                    scaleAdjustment = Math.Pow(2, (1.0 / 20));
                }
                else if (dragOffset.Y < 0)
                {
                    scaleAdjustment = Math.Pow(2, -(1.0 / 20));
                }

                _zoomableCanvas.Scale *= scaleAdjustment;
                _zoomableCanvas.Offset = (Point)((Vector)(_zoomableCanvas.Offset + (Vector)_originalMousePosition) * scaleAdjustment - (Vector)_originalMousePosition);

                _lastMousePosition = position;
                e.Handled = true;
            }
        }

        /// <summary>
        /// Event handler for dealing with the middle click pan behaviour.
        /// </summary>
        private void ZoomAndPan_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            _zoomableCanvas.ReleaseMouseCapture();
            _isPanOrZooming = false;
            e.Handled = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ZoomRegion_MouseMove(object sender, MouseEventArgs e)
        {
            if (_isPanOrZooming && e.LeftButton == MouseButtonState.Pressed)
            {
                Point position = e.GetPosition(_zoomableCanvas);
                SetAdornerArea(_originalMousePosition, position);
                e.Handled = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ZoomRegion_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            // When drag-zooming has finished we zoom in on the rectangle that was highlighted by the user.
            ApplyDragZoom();
            _zoomableCanvas.ReleaseMouseCapture();
            _isPanOrZooming = false;
            e.Handled = true;
        }

        /// <summary>
        /// When the user has finished dragging out the rectangle the zoom operation is applied.
        /// </summary>
        private void ApplyDragZoom()
        {
            // Retrieve the rectangle that the user dragged out and zoom in on it
            // ensuring that the dimensions are valid.
            if (_adorner.Area.Width != 0.0 && !_adorner.Area.Width.IsNaN() &&
                _adorner.Area.Height != 0.0 && !_adorner.Area.Height.IsNaN())
            {
                _zoomableCanvas.ZoomToViewport(_adorner.Area);
            }

            // Hide the adorner.
            _adorner.IsEnabled = false;
        }
        #endregion // Event Handlers

        #region Draggable Point Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void OnPointDragStarted(object sender, RoutedEventArgs e)
        {
            RsDraggableListBoxItem item = sender as RsDraggableListBoxItem;
            if (item != null)
            {
                CurvePointViewModel pointVm = item.DataContext as CurvePointViewModel;
                CurveEditor curve = e.Source as CurveEditor;
                curve._currentUndoBatch = new UndoRedoBatch(pointVm.Model.UndoEngine);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void OnPointDragDelta(object sender, RsDragEventArgs e)
        {
            CurveEditor curve = e.Source as CurveEditor;
            if (curve != null)
            {
                curve.MoveSelectedPoints(e.HorizontalChange, e.VerticalChange);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void OnPointDragCompleted(object sender, RoutedEventArgs e)
        {
            CurveEditor curve = e.Source as CurveEditor;
            if (curve._currentUndoBatch != null)
            {
                curve._currentUndoBatch.Dispose();
            }
        }

        /// <summary>
        /// Moves the selected curve points by the requested amounts.
        /// </summary>
        /// <param name="horizontalChange">Horizontal change.</param>
        /// <param name="verticalChange">Vertical change.</param>
        /// <returns>Whether any points were modified.</returns>
        private bool MoveSelectedPoints(double horizontalChange, double verticalChange)
        {
            bool movedPoint = false;

            foreach (CurvePointViewModel pointVm in _pointsListBox.SelectedItems)
            {
                if (InteractionMode == InteractionMode.MoveFree ||
                    InteractionMode == InteractionMode.MoveHorizontally)
                {
                    pointVm.X += (horizontalChange / Scale.X);
                    movedPoint = true;
                }
                if (InteractionMode == InteractionMode.MoveFree ||
                    InteractionMode == InteractionMode.MoveVertically)
                {
                    pointVm.InvertedY += (verticalChange / Scale.Y);
                    movedPoint = true;
                }
            }

            return movedPoint;
        }
        #endregion // Draggable Point Event Handlers

        #region Adorner Helpers
        /// <summary>
        /// 
        /// </summary>
        private void CreateAdorner()
        {
            _adorner = new AreaAdorner(_zoomableCanvas);
            _adorner.IsEnabled = false;

            AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(_zoomableCanvas);
            adornerLayer.Add(_adorner);
        }

        /// <summary>
        /// Update the position and size of the rectangle that user is dragging out.
        /// </summary>
        private void SetAdornerArea(Point pt1, Point pt2)
        {
            // Determine x,y,width and height of the rect inverting the points if necessary.
            double x, y, width, height;

            if (pt2.X < pt1.X)
            {
                x = pt2.X;
                width = pt1.X - pt2.X;
            }
            else
            {
                x = pt1.X;
                width = pt2.X - pt1.X;
            }

            if (pt2.Y < pt1.Y)
            {
                y = pt2.Y;
                height = pt1.Y - pt2.Y;
            }
            else
            {
                y = pt1.Y;
                height = pt2.Y - pt1.Y;
            }

            // Update the coordinates of the rectangle that is being dragged out by the user.
            // The we offset and rescale to convert from content coordinates.
            _adorner.IsEnabled = true;
            _adorner.Area = new Rect(x, y, width, height);
        }

        /// <summary>
        /// Hides the adorner layer.
        /// </summary>
        private void HideAdorner()
        {
            _adorner.IsEnabled = false;
        }
        #endregion // Adorner helpers
    } // CurveEditor
}
