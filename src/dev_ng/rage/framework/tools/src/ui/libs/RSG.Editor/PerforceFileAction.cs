﻿//---------------------------------------------------------------------------------------------
// <copyright file="PerforceFileAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    /// <summary>
    /// Describes the pending or completed action related to open, resolve, or integration for
    /// a specific file in source control.
    /// </summary>
    public enum PerforceFileAction : long
    {
        /// <summary>
        /// No action.
        /// </summary>
        None = 0x0000,

        /// <summary>
        /// Opened for add.
        /// </summary>
        Add = 1,

        /// <summary>
        /// Opened for branch.
        /// </summary>
        Branch = 2,

        /// <summary>
        /// Opened for edit.
        /// </summary>
        Edit = 3,

        /// <summary>
        /// Opened for integrate.
        /// </summary>
        Integrate = 4,

        /// <summary>
        /// File has been deleted.
        /// </summary>
        Delete = 5,

        /// <summary>
        /// File was integrated from partner-file, and partner-file had been previously
        /// deleted.
        /// </summary>
        DeleteFrom = 6,

        /// <summary>
        /// File was integrated into partner-file, and file had been previously deleted.
        /// </summary>
        DeleteInto = 7,

        /// <summary>
        /// File has been synced.
        /// </summary>
        Sync = 8,

        /// <summary>
        /// File has been updated.
        /// </summary>
        Updated = 9,

        /// <summary>
        /// File has been added.
        /// </summary>
        Added = 10,

        /// <summary>
        /// File was integrated into previously non existent partner-file, and partner-file was
        /// reopened for add before submission.
        /// </summary>
        AddInto = 11,

        /// <summary>
        /// File has been refreshed.
        /// </summary>
        Refreshed = 12,

        /// <summary>
        /// File was integrated from partner-file, accepting yours.
        /// </summary>
        Ignored = 13,

        /// <summary>
        /// File was integrated into partner-file, accepting yours.
        /// </summary>
        IgnoredBy = 14,

        /// <summary>
        /// File has been abandoned.
        /// </summary>
        Abandoned = 15,

        /// <summary>
        /// File has been edited and ignored.
        /// </summary>
        EditIgnored = 16,

        /// <summary>
        /// File is opened for move.
        /// </summary>
        Move = 17,

        /// <summary>
        /// File has been added as part of a move.
        /// </summary>
        MoveAdd = 18,

        /// <summary>
        /// File has been deleted as part of a move.
        /// </summary>
        MoveDelete = 19,

        /// <summary>
        /// File was integrated from partner-file, accepting theirs and deleting the original.
        /// </summary>
        MovedFrom = 20,

        /// <summary>
        /// File was integrated into partner-file, accepting merge.
        /// </summary>
        MovedInto = 21,

        /// <summary>
        /// File has not been resolved.
        /// </summary>
        Unresolved = 22,

        /// <summary>
        /// File was integrated from partner-file, accepting theirs.
        /// </summary>
        CopyFrom = 23,

        /// <summary>
        /// File was integrated into partner-file, accepting theirs.
        /// </summary>
        CopyInto = 24,

        /// <summary>
        /// File was integrated from partner-file, accepting merge.
        /// </summary>
        MergeFrom = 25,

        /// <summary>
        /// File was integrated into partner-file, accepting merge.
        /// </summary>
        MergeInto = 26,

        /// <summary>
        /// File was integrated from partner-file, and file was edited within the p4 resolve
        /// process. This allows you to determine whether the change should ever be integrated
        /// back; automated changes (merge from) needn't be, but original user edits (edit
        /// from) performed during the resolve should be.
        /// </summary>
        EditFrom = 27,

        /// <summary>
        /// File was integrated into partner-file, and partner-file was reopened for edit
        /// before submission.
        /// </summary>
        EditInto = 28,

        /// <summary>
        /// File was purged.
        /// </summary>
        Purge = 29,

        /// <summary>
        /// File was imported.
        /// </summary>
        Import = 30,

        /// <summary>
        /// File did not previously exist; it was created as a copy of partner-file.
        /// </summary>
        BranchFrom = 31,

        /// <summary>
        /// Partner-file did not previously exist; it was created as a copy of file.
        /// </summary>
        BranchInto = 32,

        /// <summary>
        /// File was reverted.
        /// </summary>
        Reverted = 33,
    } // RSG.Editor.PerforceFileAction {Enum}
} // RSG.Editor {Namespace}
