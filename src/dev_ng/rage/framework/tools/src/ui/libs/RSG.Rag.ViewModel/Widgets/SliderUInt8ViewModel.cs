﻿using RSG.Rag.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.ViewModel.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    public class SliderUInt8ViewModel : SliderViewModel<Byte>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        public SliderUInt8ViewModel(WidgetSlider<Byte> widget)
            : base(widget)
        {
        }
        #endregion // Constructor(s)
    } // SliderUInt8ViewModel
}
