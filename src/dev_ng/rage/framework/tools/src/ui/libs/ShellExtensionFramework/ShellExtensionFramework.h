#ifndef SHELLEXTENSIONFRAMEWORK_H
#define SHELLEXTENSIONFRAMEWORK_H

/**
	\mainpage Shell Extension Framework

	This documentation covers Rockstar Games Shell Extension Framework which 
	is a static library to assist in developing Windows Shell Extensions.

	We use this framework for the Rockstar Games Windows Explorer menu
	shell and namespace extension.
 */

#if defined _MSC_VER && _MSC_VER >= 1300
#pragma once
#endif

#include <Windows.h>
#include "propkeydef.h"
#include "ShlObj.h"

#define SHELL_EXTENSION_FRAMEWORK_NS		ShellExtensionFramework
#define BEGIN_SHELL_EXTENSION_FRAMEWORK_NS	namespace SHELL_EXTENSION_FRAMEWORK_NS {
#define END_SHELL_EXTENSION_FRAMEWORK_NS	}
#define USE_SHELL_EXTENSION_FRAMEWORK_NS	using namespace SHELL_EXTENSION_FRAMEWORK_NS;

#ifdef _DEBUG
#define HR(expr)  { HRESULT _hr; if(FAILED(_hr=(expr))) { _CrtDbgReport(_CRT_ASSERT, __FILE__, __LINE__, NULL, #expr); _CrtDbgBreak(); return _hr; } }  
#else
#define HR(expr)  { HRESULT _hr; if(FAILED(_hr=(expr))) return _hr; }
#endif // _DEBUG

#endif // SHELLEXTENSIONFRAMEWORK_H
