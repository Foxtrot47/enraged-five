﻿//---------------------------------------------------------------------------------------------
// <copyright file="ISupportsDisposalState.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    using System.ComponentModel;

    /// <summary>
    /// When implemented represents a object that contains a property that specifies whether it
    /// has been disposed of and a event that fires when it changes.
    /// </summary>
    public interface ISupportsDisposalState : INotifyPropertyChanged
    {
        #region Properties
        /// <summary>
        /// Gets a value indicating whether this instance has been disposed of.
        /// </summary>
        bool IsDisposed { get; }
        #endregion Properties
    } // RSG.Editor.View.ISupportsDisposalState {Interface}
} // RSG.Editor.View {Namespace}
