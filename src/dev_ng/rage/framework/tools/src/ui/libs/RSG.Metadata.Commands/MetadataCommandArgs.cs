﻿//---------------------------------------------------------------------------------------------
// <copyright file="MetadataCommandArgs.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using RSG.Editor.Model;
    using RSG.Metadata.ViewModel;
    using RSG.Metadata.ViewModel.Tunables;

    /// <summary>
    /// Represents the argument class used by the metadata commands.
    /// </summary>
    public class MetadataCommandArgs
    {
        #region Fields
        /// <summary>
        /// A value indicating whether these args are locked and cannot be edited.
        /// </summary>
        private bool _locked;

        /// <summary>
        /// The private field used for the <see cref="SelectedTunables"/> property.
        /// </summary>
        private IEnumerable<ITunableViewModel> _selectedTunables;

        /// <summary>
        /// The private field used for the <see cref="TunableCollapseDelegate"/> property.
        /// </summary>
        private CollapseTunableDelegate _tunableCollapseDelegate;

        /// <summary>
        /// The private field used for the <see cref="TunableExpansionDelegate"/> property.
        /// </summary>
        private ExpandTunableDelegate _tunableExpansionDelegate;

        /// <summary>
        /// The private field used for the <see cref="TunableSelectionDelegate"/> property.
        /// </summary>
        private SelectTunableDelegate _tunableSelectionDelegate;

        /// <summary>
        /// The private field used for the <see cref="UndoEngine"/> property.
        /// </summary>
        private UndoEngine _undoEngine;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MetadataCommandArgs"/> class.
        /// </summary>
        public MetadataCommandArgs()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the tunable view models that are currently selected.
        /// </summary>
        public IEnumerable<ITunableViewModel> SelectedTunables
        {
            get
            {
                return this._selectedTunables ?? Enumerable.Empty<ITunableViewModel>();
            }

            set
            {
                if (this._locked)
                {
                    throw new NotSupportedException(
                        "Arguments are locked and cannot be edited.");
                }

                this._selectedTunables = value;
            }
        }

        /// <summary>
        /// Gets or sets a delegate method that can be used to collapse a single tunable view
        /// model inside the view.
        /// </summary>
        public CollapseTunableDelegate TunableCollapseDelegate
        {
            get
            {
                return this._tunableCollapseDelegate;
            }

            set
            {
                if (this._locked)
                {
                    throw new NotSupportedException(
                        "Arguments are locked and cannot be edited.");
                }

                this._tunableCollapseDelegate = value;
            }
        }

        /// <summary>
        /// Gets or sets a delegate method that can be used to expand a single tunable view
        /// model inside the view.
        /// </summary>
        public ExpandTunableDelegate TunableExpansionDelegate
        {
            get
            {
                return this._tunableExpansionDelegate;
            }

            set
            {
                if (this._locked)
                {
                    throw new NotSupportedException(
                        "Arguments are locked and cannot be edited.");
                }

                this._tunableExpansionDelegate = value;
            }
        }

        /// <summary>
        /// Gets or sets a delegate method that can be used to select a single tunable view
        /// model inside the view.
        /// </summary>
        public SelectTunableDelegate TunableSelectionDelegate
        {
            get
            {
                return this._tunableSelectionDelegate;
            }

            set
            {
                if (this._locked)
                {
                    throw new NotSupportedException(
                        "Arguments are locked and cannot be edited.");
                }

                this._tunableSelectionDelegate = value;
            }
        }

        /// <summary>
        /// Gets or sets the undo engine that the action can use to batch changes.
        /// </summary>
        public UndoEngine UndoEngine
        {
            get
            {
                return this._undoEngine;
            }

            set
            {
                if (this._locked)
                {
                    throw new NotSupportedException(
                        "Arguments are locked and cannot be edited.");
                }

                this._undoEngine = value;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Locks this argument class so that it becomes read-only and any attempted changes to
        /// it throws an exception.
        /// </summary>
        public void Lock()
        {
            this._locked = true;
        }
        #endregion Methods
    } // RSG.Metadata.Commands.MetadataCommandArgs {Class}
} // RSG.Metadata.Commands {Namespace}
