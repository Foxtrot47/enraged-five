﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Configuration.Bugstar;
using RSG.Interop.Bugstar;
using RSG.Interop.Bugstar.Organisation;

namespace RSG.Editor.Controls.Bugstar
{
    /// <summary>
    /// When implemented represents a service that can be used to acquire a bugstar connection.
    /// </summary>
    public interface IBugstarService
    {
        #region Properties
        /// <summary>
        /// Retrieves the bugstar project id for the currently installed tools project.
        /// </summary>
        uint ProjectId { get; }
        #endregion

        #region Methods
        /// <summary>
        /// Gets the list of available bugstar projects.
        /// </summary>
        IEnumerable<Project> GetAvailableProjects();
        
        /// <summary>
        /// Gets the bugstar project for the currently installed tools project.
        /// </summary>
        /// <returns></returns>
        Project GetActiveProject();

        /// <summary>
        /// Gets a particular project by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Project GetProjectById(uint id);
        #endregion
    }
}
