﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor;
using RSG.AnimationToolkit.ViewModel;

namespace RSG.AnimationToolkit.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public class SelectAllAction : ButtonAction<AnimationToolkitDataContext>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="OpenFileImplementer"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public SelectAllAction(ParameterResolverDelegate<AnimationToolkitDataContext> resolver)
            : base(resolver)
        {
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="commandParameter">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(AnimationToolkitDataContext dc)
        {
            // Open the file.
            dc.GetViewModel.SelectItems(true);
        }
        #endregion // Overrides
    } // OpenFileAction
}
