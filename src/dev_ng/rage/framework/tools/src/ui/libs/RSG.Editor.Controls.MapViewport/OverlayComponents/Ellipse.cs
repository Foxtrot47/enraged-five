﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace RSG.Editor.Controls.MapViewport.OverlayComponents
{
    /// <summary>
    /// 
    /// </summary>
    public class Ellipse : Shape
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Height"/> property.
        /// </summary>
        private double _height;

        /// <summary>
        /// Private field for the <see cref="Position"/> property.
        /// </summary>
        private Point _position;

        /// <summary>
        /// Private field for the <see cref="Width"/> property.
        /// </summary>
        private double _width;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Ellipse"/> class using the
        /// specified position, width and height.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public Ellipse(Point position, double width, double height)
        {
            _position = position;
            _width = width;
            _height = height;
        }
        #endregion

        #region Properties
        /// <summary>
        /// The height of the ellipse.
        /// </summary>
        public double Height
        {
            get { return _height; }
            set { SetProperty(ref _height, value); }
        }
        
        /// <summary>
        /// Position of this component in world space.
        /// </summary>
        public override Point Position
        {
            get { return _position; }
            set { SetProperty(ref _position, value); }
        }

        /// <summary>
        /// Where the position relates to in terms of the component's bounds.
        /// </summary>
        public override PositionOrigin PositionOrigin
        {
            get { return PositionOrigin.Center; }
            set { throw new NotSupportedException(); }
        }

        /// <summary>
        /// The width of the ellipse.
        /// </summary>
        public double Width
        {
            get { return _width; }
            set { SetProperty(ref _width, value); }
        }
        #endregion
    }
}
