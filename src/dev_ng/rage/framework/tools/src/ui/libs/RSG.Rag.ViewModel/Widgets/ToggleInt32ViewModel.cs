﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Rag.Widgets;

namespace RSG.Rag.ViewModel.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    public class ToggleInt32ViewModel : ToggleViewModel<Int32>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        public ToggleInt32ViewModel(WidgetToggle<Int32> widget)
            : base(widget)
        {
        }
        #endregion // Constructor(s)
    } // ToggleInt32ViewModel
}
