﻿//---------------------------------------------------------------------------------------------
// <copyright file="ValueColumnCellTemplateSelector.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.View
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Controls;
    using RSG.Editor.Controls;
    using RSG.Editor.Controls.Helpers;
    using RSG.Metadata.ViewModel.Tunables;

    /// <summary>
    /// Represents the cell template that is associated with the value column in the tunable
    /// structure control.
    /// </summary>
    public class ValueColumnCellTemplateSelector : DataTemplateSelector
    {
        #region Fields
        /// <summary>
        /// The private dictionary containing the data templates that are used for this
        /// selector.
        /// </summary>
        private static Dictionary<Type, DataTemplate> _templates;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="ValueColumnCellTemplateSelector" />
        /// class.
        /// </summary>
        static ValueColumnCellTemplateSelector()
        {
            _templates = new Dictionary<Type, DataTemplate>();
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Returns the data template to use for the specified item inside the value column for
        /// the tunable structure control.
        /// </summary>
        /// <param name="item">
        /// The data content.
        /// </param>
        /// <param name="container">
        /// The element to which the template will be applied.
        /// </param>
        /// <returns>
        /// The data template to use for the specified item inside the value column for the
        /// tunable structure control.
        /// </returns>
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            VirtualisedTreeNode node = item as VirtualisedTreeNode;
            if (node == null)
            {
                return new DataTemplate();
            }

            TunableStructureControl control = GetRootControl(container);
            PointerTunableViewModel pointerVm = node.Item as PointerTunableViewModel;
            if (pointerVm != null)
            {
                string templateName = "PointerValueEditor";
                if (pointerVm.OwnsObject)
                {
                    templateName = "OwnerPointerValueEditor";
                }
                else
                {
                    templateName = "LinkPointerValueEditor";
                }

                object resource = control.TryFindResource(templateName);
                DataTemplate template = resource as DataTemplate;
                if (template == null)
                {
                    template = new DataTemplate();
                }

                return template;
            }

            BitsetTunableViewModel bitsetVm = node.Item as BitsetTunableViewModel;
            if (bitsetVm != null)
            {
                string templateName = "BitsetValueEditor";
                if (bitsetVm.HasIndeterminateSize)
                {
                    templateName = "BitsetIndeterminateValueEditor";
                }

                object resource = control.TryFindResource(templateName);
                DataTemplate template = resource as DataTemplate;
                if (template == null)
                {
                    template = new DataTemplate();
                }

                return template;
            }

            Unsigned32TunableViewModel unsigned32Vm = node.Item as Unsigned32TunableViewModel;
            if (unsigned32Vm != null)
            {
                if (unsigned32Vm.Model.Unsigned32Member.RepresentsColourValue)
                {
                    string templateName = "Unsigned32ColourValueEditor";
                    object resource = control.TryFindResource(templateName);
                    DataTemplate template = resource as DataTemplate;
                    if (template == null)
                    {
                        template = new DataTemplate();
                    }

                    return template;
                }
            }

            Vector3TunableViewModel vector3Vm = node.Item as Vector3TunableViewModel;
            if (vector3Vm != null && vector3Vm.Model.Vector3Member.RepresentsColourValue)
            {
                string templateName = "VectorColourValueEditor";
                object resource = control.TryFindResource(templateName);
                DataTemplate template = resource as DataTemplate;
                if (template == null)
                {
                    template = new DataTemplate();
                }

                return template;
            }


            Vec3VTunableViewModel vec3Vm = node.Item as Vec3VTunableViewModel;
            if (vec3Vm != null && vec3Vm.Model.Vec3VMember.RepresentsColourValue)
            {
                string templateName = "VectorColourValueEditor";
                object resource = control.TryFindResource(templateName);
                DataTemplate template = resource as DataTemplate;
                if (template == null)
                {
                    template = new DataTemplate();
                }

                return template;
            }

            return CreateTemplate(node.Item.GetType(), control);
        }

        /// <summary>
        /// Creates a data template that contains the editor controls for the specified type.
        /// </summary>
        /// <param name="type">
        /// The type of the item whose data template should be created.
        /// </param>
        /// <param name="control">
        /// The control to use to try and find the templates in.
        /// </param>
        /// <returns>
        /// A data template that contains the editor controls for the specified type.
        /// </returns>
        private static DataTemplate CreateTemplate(Type type, TunableStructureControl control)
        {
            DataTemplate template = null;
            if (_templates.TryGetValue(type, out template))
            {
                return template;
            }

            if (control == null)
            {
                return new DataTemplate();
            }

            object resource = null;
            if (type == typeof(ArrayTunableViewModel))
            {
                resource = control.TryFindResource("ArrayValueEditor");
            }
            else if (type == typeof(MapTunableViewModel))
            {
                resource = control.TryFindResource("MapValueEditor");
            }
            else if (type == typeof(StructTunableViewModel))
            {
                resource = control.TryFindResource("StructValueEditor");
            }
            else if (type == typeof(BoolTunableViewModel))
            {
                resource = control.TryFindResource("BoolValueEditor");
            }
            else if (type == typeof(BoolVectorTunableViewModel))
            {
                resource = control.TryFindResource("BoolVectorValueEditor");
            }
            else if (type == typeof(BoolVTunableViewModel))
            {
                resource = control.TryFindResource("BoolValueEditor");
            }
            else if (type == typeof(CharTunableViewModel))
            {
                resource = control.TryFindResource("CharValueEditor");
            }
            else if (type == typeof(ColourTunableViewModel))
            {
                resource = control.TryFindResource("ColourValueEditor");
            }
            else if (type == typeof(DoubleTunableViewModel))
            {
                resource = control.TryFindResource("DoubleValueEditor");
            }
            else if (type == typeof(EnumTunableViewModel))
            {
                resource = control.TryFindResource("EnumValueEditor");
            }
            else if (type == typeof(Float16TunableViewModel))
            {
                resource = control.TryFindResource("Float16ValueEditor");
            }
            else if (type == typeof(FloatTunableViewModel))
            {
                resource = control.TryFindResource("FloatValueEditor");
            }
            else if (type == typeof(IntTunableViewModel))
            {
                resource = control.TryFindResource("IntValueEditor");
            }
            else if (type == typeof(PtrDiffTunableViewModel))
            {
                resource = control.TryFindResource("PtrDiffValueEditor");
            }
            else if (type == typeof(ScalarVTunableViewModel))
            {
                resource = control.TryFindResource("ScalarVValueEditor");
            }
            else if (type == typeof(ShortTunableViewModel))
            {
                resource = control.TryFindResource("ShortValueEditor");
            }
            else if (type == typeof(Signed16TunableViewModel))
            {
                resource = control.TryFindResource("Signed16ValueEditor");
            }
            else if (type == typeof(Signed32TunableViewModel))
            {
                resource = control.TryFindResource("Signed32ValueEditor");
            }
            else if (type == typeof(Signed64TunableViewModel))
            {
                resource = control.TryFindResource("Signed64ValueEditor");
            }
            else if (type == typeof(Signed8TunableViewModel))
            {
                resource = control.TryFindResource("Signed8ValueEditor");
            }
            else if (type == typeof(SizeTTunableViewModel))
            {
                resource = control.TryFindResource("SizeTValueEditor");
            }
            else if (type == typeof(StringTunableViewModel))
            {
                resource = control.TryFindResource("StringValueEditor");
            }
            else if (type == typeof(Unsigned16TunableViewModel))
            {
                resource = control.TryFindResource("Unsigned16ValueEditor");
            }
            else if (type == typeof(Unsigned32TunableViewModel))
            {
                resource = control.TryFindResource("Unsigned32ValueEditor");
            }
            else if (type == typeof(Unsigned64TunableViewModel))
            {
                resource = control.TryFindResource("Unsigned64ValueEditor");
            }
            else if (type == typeof(Unsigned8TunableViewModel))
            {
                resource = control.TryFindResource("Unsigned8ValueEditor");
            }
            else if (type == typeof(Vec2VTunableViewModel))
            {
                resource = control.TryFindResource("Vector2ValueEditor");
            }
            else if (type == typeof(Vec3VTunableViewModel))
            {
                resource = control.TryFindResource("Vector3ValueEditor");
            }
            else if (type == typeof(Vec4VTunableViewModel))
            {
                resource = control.TryFindResource("Vector4ValueEditor");
            }
            else if (type == typeof(Vector2TunableViewModel))
            {
                resource = control.TryFindResource("Vector2ValueEditor");
            }
            else if (type == typeof(Vector3TunableViewModel))
            {
                resource = control.TryFindResource("Vector3ValueEditor");
            }
            else if (type == typeof(Vector4TunableViewModel))
            {
                resource = control.TryFindResource("Vector4ValueEditor");
            }
            else if (type == typeof(Mat33VTunableViewModel))
            {
                resource = control.TryFindResource("Matrix33ValueEditor");
            }
            else if (type == typeof(Mat34VTunableViewModel))
            {
                resource = control.TryFindResource("Matrix34ValueEditor");
            }
            else if (type == typeof(Mat44VTunableViewModel))
            {
                resource = control.TryFindResource("Matrix44ValueEditor");
            }
            else if (type == typeof(Matrix34TunableViewModel))
            {
                resource = control.TryFindResource("Matrix34ValueEditor");
            }
            else if (type == typeof(Matrix44TunableViewModel))
            {
                resource = control.TryFindResource("Matrix44ValueEditor");
            }

            template = resource as DataTemplate;
            if (template == null)
            {
                template = new DataTemplate();
            }

            _templates.Add(type, template);
            return template;
        }

        /// <summary>
        /// Retrieves the root tunable structure control that the specified container is
        /// inside.
        /// </summary>
        /// <param name="container">
        /// The element to which the template will be applied.
        /// </param>
        /// <returns>
        /// The root tunable structure control that the specified container is inside.
        /// </returns>
        private static TunableStructureControl GetRootControl(DependencyObject container)
        {
            return container.GetVisualAncestor<TunableStructureControl>();
        }
        #endregion Methods
    } // RSG.Metadata.View.ValueColumnCellTemplateSelector {Class}
} // RSG.Metadata.View {Namespace}
