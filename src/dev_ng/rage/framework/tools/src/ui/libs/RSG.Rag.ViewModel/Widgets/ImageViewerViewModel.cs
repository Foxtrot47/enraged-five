﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Rag.Widgets;
using RSG.Editor;
using System.Diagnostics;
using System.Windows.Input;
using System.IO;

namespace RSG.Rag.ViewModel.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    public class ImageViewerViewModel : WidgetViewModel<WidgetImageViewer>
    {
        #region Fields
        /// <summary>
        /// 
        /// </summary>
        private ICommand _openFileCommand;
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        public ImageViewerViewModel(WidgetImageViewer widget)
            : base(widget)
        {
        }
        #endregion // Constructor(s)

        #region Commands
        /// <summary>
        /// 
        /// </summary>
        public ICommand OpenFileCommand
        {
            get
            {
                if (_openFileCommand == null)
                {
                    _openFileCommand = new RelayCommand(param => OpenFile(param), param => CanOpenFile(param));
                }
                return _openFileCommand;
            }
        }
        #endregion // Commands

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public String Filename
        {
            get { return Widget.Filename; }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool FilenameValid
        {
            get { return File.Exists(Widget.Filename); }
        }
        #endregion // Properties

        #region Command Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool CanOpenFile(Object parameter)
        {
            return !String.IsNullOrEmpty(Filename);
        }

        /// <summary>
        /// Called to press the button.
        /// </summary>
        public void OpenFile(Object parameter)
        {
            Process.Start(new ProcessStartInfo(Filename));  
        }
        #endregion // Command Methods

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        protected override void OnWidgetPropertyChanged(String propertyName)
        {
            base.OnWidgetPropertyChanged(propertyName);
            if (propertyName == "Filename")
            {
                NotifyPropertyChanged("FilenameValid");
            }
        }
        #endregion // Event Handlers
    } // ImageViewerViewModel
}
