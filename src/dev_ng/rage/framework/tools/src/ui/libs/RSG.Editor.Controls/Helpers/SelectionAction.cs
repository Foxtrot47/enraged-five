﻿//---------------------------------------------------------------------------------------------
// <copyright file="SelectionAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Helpers
{
    /// <summary>
    /// Defines the different types of selection actions that can be performed.
    /// </summary>
    public enum SelectionAction
    {
        /// <summary>
        /// Clears the current selection and selects just a single item.
        /// </summary>
        SingleSelection,

        /// <summary>
        /// Extends the selection from the current anchored item to another item.
        /// </summary>
        ExtendSelection,

        /// <summary>
        /// Toggles the selection of a single item.
        /// </summary>
        ToggleSelection,

        /// <summary>
        /// Adds a single item to the current selection.
        /// </summary>
        AddToSelection,

        /// <summary>
        /// Removes a single item from the current selection.
        /// </summary>
        RemoveFromSelection,

        /// <summary>
        /// Sets the current anchored item.
        /// </summary>
        SetAnchorItem
    } // RSG.Editor.Controls.Helpers.SelectionAction {Enum}
} // RSG.Editor.Controls.Helpers {Namespace}
