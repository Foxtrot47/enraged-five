﻿//---------------------------------------------------------------------------------------------
// <copyright file="Icons.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock
{
    using System;
    using System.Windows.Media.Imaging;
    using RSG.Base.Extensions;

    /// <summary>
    /// Provides static properties that give access to common icons that can be used throughout
    /// a application.
    /// </summary>
    public static class Icons
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="CloseAllDocuments"/> property.
        /// </summary>
        private static BitmapSource _closeAllDocuments;

        /// <summary>
        /// The private field used for the <see cref="Lock"/> property.
        /// </summary>
        private static BitmapSource _lock;

        /// <summary>
        /// A generic object that provides thread safety access to the image sources.
        /// </summary>
        private static object _syncRoot = new object();
        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the icon that shows a close all documents icon.
        /// </summary>
        public static BitmapSource CloseAllDocuments
        {
            get
            {
                if (_closeAllDocuments == null)
                {
                    lock (_syncRoot)
                    {
                        if (_closeAllDocuments == null)
                        {
                            EnsureLoaded(ref _closeAllDocuments, "closeAllDocuments16");
                        }
                    }
                }

                return _closeAllDocuments;
            }
        }

        /// <summary>
        /// Gets the icon that shows a lock icon.
        /// </summary>
        public static BitmapSource Lock
        {
            get
            {
                if (_lock == null)
                {
                    lock (_syncRoot)
                    {
                        if (_lock == null)
                        {
                            EnsureLoaded(ref _lock, "lock");
                        }
                    }
                }

                return _lock;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Ensures that the specified image source is loaded with the specified resource name.
        /// </summary>
        /// <param name="source">
        /// The image source that should be loaded.
        /// </param>
        /// <param name="resourceName">
        /// The name of the resource the loaded image source should set as its source.
        /// </param>
        private static void EnsureLoaded(ref BitmapSource source, string resourceName)
        {
            if (source != null)
            {
                return;
            }

            string assemblyName = typeof(Icons).Assembly.GetName().Name;
            string path = "Resources/" + resourceName + ".png";
            string bitmapPath = "pack://application:,,,/{0};component/{1}";
            bitmapPath = bitmapPath.FormatInvariant(assemblyName, path);
            Uri uri = new Uri(bitmapPath, UriKind.RelativeOrAbsolute);

            source = new BitmapImage(uri);
            source.Freeze();
        }
        #endregion Methods
    } // RSG.Editor.Controls.Dock.Icons {Class}
} // RSG.Editor.Controls.Dock {Namespace}
