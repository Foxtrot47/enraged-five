﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using RSG.Editor.Model;

namespace RSG.Editor.Controls.MapViewport.OverlayComponents
{
    /// <summary>
    /// Abstract base class from which map viewport overlay components should inherit
    /// from.
    /// </summary>
    public abstract class Component : UndoablePropertyChangedBase
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="LeftMouseDownCommand"/> property.
        /// </summary>
        private ICommand _leftMouseDownCommand;

        /// <summary>
        /// Private field for the <see cref="ScaleFactor"/> property.
        /// </summary>
        private double _scaleFactor;

        /// <summary>
        /// Private field for the <see cref="Tag"/> property.
        /// </summary>
        private object _tag;

        /// <summary>
        /// Private field for the <see cref="ToolTip"/> property.
        /// </summary>
        private string _tooltip;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Component"/> class.
        /// </summary>
        protected Component()
        {
            _scaleFactor = 1.0;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Command to execute when the component is clicked.
        /// </summary>
        public ICommand LeftMouseDownCommand
        {
            get { return _leftMouseDownCommand; }
            set { SetProperty(ref _leftMouseDownCommand, value); }
        }

        /// <summary>
        /// Gets or sets the position of this component in world space.
        /// </summary>
        public abstract Point Position { get; set; }

        /// <summary>
        /// Gets or sets the position relates to in terms of the component's bounds.
        /// </summary>
        public abstract PositionOrigin PositionOrigin { get; set; }

        /// <summary>
        /// Gets or sets the scale factor to apply when the map viewport zooms in/out.
        /// </summary>
        public double ScaleFactor
        {
            get { return _scaleFactor; }
            set { SetProperty(ref _scaleFactor, value); }
        }

        /// <summary>
        /// Gets or sets an arbitrary bit of information you can associate with this
        /// component.
        /// </summary>
        public object Tag
        {
            get { return _tag; }
            set { SetProperty(ref _tag, value); }
        }

        /// <summary>
        /// Gets or sets the tooltip associated with this component.
        /// </summary>
        public string ToolTip
        {
            get { return _tooltip; }
            set { SetProperty(ref _tooltip, value); }
        }
        #endregion
    }
}
