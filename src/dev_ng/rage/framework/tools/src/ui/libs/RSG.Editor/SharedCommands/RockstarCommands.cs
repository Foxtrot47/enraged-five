﻿//---------------------------------------------------------------------------------------------
// <copyright file="RockstarCommands.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.SharedCommands
{
    using System;
    using System.Diagnostics;
    using System.Globalization;
    using System.Windows.Input;
    using System.Windows.Media.Imaging;
    using RSG.Base.Extensions;
    using RSG.Editor.Resources;

    /// <summary>
    /// Defines core commands for any Rockstar C#/WPF application. This class cannot be
    /// inherited.
    /// </summary>
    public sealed class RockstarCommands
    {
        #region Fields
        /// <summary>
        /// The private cache of command objects.
        /// </summary>
        private static RockstarRoutedCommand[] _internalCommands = InitialiseInternalStorage();
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Prevents a default instance of the <see cref="RockstarCommands"/> class from being
        /// created.
        /// </summary>
        private RockstarCommands()
        {
        }
        #endregion Constructors

        #region Enumerations
        /// <summary>
        /// Defines all of the command identifiers for the different core Rockstar commands.
        /// </summary>
        private enum Id : byte
        {
            /// <summary>
            /// Used to identifier the <see cref="RockstarCommands.Close"/> routed command.
            /// </summary>
            Close,

            /// <summary>
            /// Used to identifier the <see cref="RockstarCommands.Collapse"/> routed command.
            /// </summary>
            Collapse,
            
            /// <summary>
            /// Used to identifier the <see cref="RockstarCommands.CollapseAll"/> routed
            /// command.
            /// </summary>
            CollapseAll,

            /// <summary>
            /// Used to identifier the <see cref="RockstarCommands.Copy"/> routed command.
            /// </summary>
            Copy,

            /// <summary>
            /// Used to identifier the <see cref="RockstarCommands.Cut"/> routed command.
            /// </summary>
            Cut,

            /// <summary>
            /// Used to identifier the <see cref="RockstarCommands.Delete"/> routed command.
            /// </summary>
            Delete,

            /// <summary>
            /// Used to identifier the <see cref="RockstarCommands.Exit"/> routed command.
            /// </summary>
            Exit,

            /// <summary>
            /// Used to identifier the <see cref="RockstarCommands.Expand"/> routed command.
            /// </summary>
            Expand,

            /// <summary>
            /// Used to identifier the <see cref="RockstarCommands.ExpandAll"/> routed command.
            /// </summary>
            ExpandAll,

            /// <summary>
            /// Used to identifier the <see cref="RockstarCommands.Find"/> routed command.
            /// </summary>
            Find,

            /// <summary>
            /// Used to identifier the <see cref="RockstarCommands.FindInFiles"/> routed
            /// command.
            /// </summary>
            FindInFiles,

            /// <summary>
            /// Used to identifier the <see cref="RockstarCommands.FindNext"/> routed
            /// command.
            /// </summary>
            FindNext,

            /// <summary>
            /// Used to identifier the <see cref="RockstarCommands.FindPrevious"/> routed
            /// command.
            /// </summary>
            FindPrevious,

            /// <summary>
            /// Used to identifier the <see cref="RockstarCommands.InvertSelection"/> routed
            /// command.
            /// </summary>
            InvertSelection,

            /// <summary>
            /// Used to identifier the <see cref="RockstarCommands.NewFile"/> routed command.
            /// </summary>
            NewFile,

            /// <summary>
            /// Used to identifier the <see cref="RockstarCommands.OpenFile"/> routed command.
            /// </summary>
            OpenFile,

            /// <summary>
            /// Used to identifier the <see cref="RockstarCommands.OpenFileFromMru"/> routed
            /// command.
            /// </summary>
            OpenFileFromMru,

            /// <summary>
            /// Used to identifier the <see cref="RockstarCommands.OpenProjectFromMru"/> routed
            /// command.
            /// </summary>
            OpenProjectFromMru,

            /// <summary>
            /// Used to identifier the <see cref="RockstarCommands.Paste"/> routed command.
            /// </summary>
            Paste,

            /// <summary>
            /// Used to identifier the <see cref="RockstarCommands.QuickFind"/> routed command.
            /// </summary>
            QuickFind,

            /// <summary>
            /// Used to identifier the <see cref="RockstarCommands.QuickReplace"/> routed
            /// command.
            /// </summary>
            QuickReplace,

            /// <summary>
            /// Used to identifier the <see cref="RockstarCommands.Redo"/> routed command.
            /// </summary>
            Redo,

            /// <summary>
            /// Used to identifier the <see cref="RockstarCommands.Rename"/> routed command.
            /// </summary>
            Rename,

            /// <summary>
            /// Used to identifier the <see cref="RockstarCommands.ReplaceInFiles"/> routed
            /// command.
            /// </summary>
            ReplaceInFiles,

            /// <summary>
            /// Used to identifier the <see cref="RockstarCommands.ReportBug"/> routed
            /// command.
            /// </summary>
            ReportBug,

            /// <summary>
            /// Used to identifier the <see cref="RockstarCommands.Restart"/> routed command.
            /// </summary>
            Restart,

            /// <summary>
            /// Used to identifier the <see cref="RockstarCommands.Save"/> routed command.
            /// </summary>
            Save,

            /// <summary>
            /// Used to identifier the <see cref="RockstarCommands.SaveAll"/> routed command.
            /// </summary>
            SaveAll,

            /// <summary>
            /// Used to identifier the <see cref="RockstarCommands.SaveAs"/> routed command.
            /// </summary>
            SaveAs,

            /// <summary>
            /// Used to identifier the <see cref="RockstarCommands.SelectAll"/> routed command.
            /// </summary>
            SelectAll,

            /// <summary>
            /// Used to identify the <see cref="RockstarCommands.ShellExecute"/> routed
            /// command.
            /// </summary>
            ShellExecute,

            /// <summary>
            /// Used to identifier the <see cref="RockstarCommands.Undo"/> routed command.
            /// </summary>
            Undo,

            /// <summary>
            /// Used to identifier the <see cref="RockstarCommands.ViewAbout"/> routed command.
            /// </summary>
            ViewAbout,

            /// <summary>
            /// Used to identifier the <see cref="RockstarCommands.ViewHelp"/> routed command.
            /// </summary>
            ViewHelp,

            /// <summary>
            /// Defines the number of commands defined as core commands for a Rockstar
            /// application.
            /// </summary>
            CommandCount
        } // RpfViewerCommands.CommandId {Enum}

        /// <summary>
        /// Defines the different string values a command has that can be retrieved from the
        /// string table.
        /// </summary>
        private enum StringValue : byte
        {
            /// <summary>
            /// Defines the category string value located in the string table as
            /// {id}Category.
            /// </summary>
            Category,

            /// <summary>
            /// Defines the description string value located in the string table as
            /// {id}Description.
            /// </summary>
            Description,

            /// <summary>
            /// Defines the description string value located in the string table as
            /// {id}KeyGestures.
            /// </summary>
            KeyGestures,

            /// <summary>
            /// Defines the name string value located in the string table as
            /// {id}Name.
            /// </summary>
            Name,
        } // RpfViewerCommands.StringValue {Enum}
        #endregion Enumerations

        #region Properties
        /// <summary>
        /// Gets the value that represents the Close command.
        /// </summary>
        public static RockstarRoutedCommand Close
        {
            get { return EnsureCommandExists(Id.Close, CommonIcons.Close); }
        }

        /// <summary>
        /// Gets the value that represents the Collapse command.
        /// </summary>
        public static RockstarRoutedCommand Collapse
        {
            get { return EnsureCommandExists(Id.Collapse, CommonIcons.Collapse); }
        }

        /// <summary>
        /// Gets the value that represents the Collapse All command.
        /// </summary>
        public static RockstarRoutedCommand CollapseAll
        {
            get { return EnsureCommandExists(Id.CollapseAll, CommonIcons.CollapseAll); }
        }

        /// <summary>
        /// Gets the value that represents the Copy command.
        /// </summary>
        public static RockstarRoutedCommand Copy
        {
            get { return EnsureCommandExists(Id.Copy, CommonIcons.Copy); }
        }

        /// <summary>
        /// Gets the value that represents the Cut command.
        /// </summary>
        public static RockstarRoutedCommand Cut
        {
            get { return EnsureCommandExists(Id.Cut, CommonIcons.Cut); }
        }

        /// <summary>
        /// Gets the value that represents the Delete command.
        /// </summary>
        public static RockstarRoutedCommand Delete
        {
            get { return EnsureCommandExists(Id.Delete, CommonIcons.Delete); }
        }

        /// <summary>
        /// Gets the value that represents the Exit command.
        /// </summary>
        public static RockstarRoutedCommand Exit
        {
            get { return EnsureCommandExists(Id.Exit, CommonIcons.Close); }
        }

        /// <summary>
        /// Gets the value that represents the Expand command.
        /// </summary>
        public static RockstarRoutedCommand Expand
        {
            get { return EnsureCommandExists(Id.Expand, CommonIcons.Expand); }
        }

        /// <summary>
        /// Gets the value that represents the Expand All command.
        /// </summary>
        public static RockstarRoutedCommand ExpandAll
        {
            get { return EnsureCommandExists(Id.ExpandAll, CommonIcons.ExpandAll); }
        }

        /// <summary>
        /// Gets the value that represents the Find command.
        /// </summary>
        public static RockstarRoutedCommand Find
        {
            get { return EnsureCommandExists(Id.Find); }
        }

        /// <summary>
        /// Gets the value that represents the Find In Files command.
        /// </summary>
        public static RockstarRoutedCommand FindInFiles
        {
            get { return EnsureCommandExists(Id.FindInFiles, CommonIcons.FindInFiles); }
        }

        /// <summary>
        /// Gets the value that represents the Find Next command.
        /// </summary>
        public static RockstarRoutedCommand FindNext
        {
            get { return EnsureCommandExists(Id.FindNext, CommonIcons.FindNext); }
        }

        /// <summary>
        /// Gets the value that represents the Find Previous command.
        /// </summary>
        public static RockstarRoutedCommand FindPrevious
        {
            get { return EnsureCommandExists(Id.FindPrevious, CommonIcons.FindPrevious); }
        }

        /// <summary>
        /// Gets the value that represents the Invert Selection command.
        /// </summary>
        public static RockstarRoutedCommand InvertSelection
        {
            get { return EnsureCommandExists(Id.InvertSelection); }
        }

        /// <summary>
        /// Gets the value that represents the New File command.
        /// </summary>
        public static RockstarRoutedCommand NewFile
        {
            get { return EnsureCommandExists(Id.NewFile, CommonIcons.NewFile); }
        }

        /// <summary>
        /// Gets the value that represents the Open File command.
        /// </summary>
        public static RockstarRoutedCommand OpenFile
        {
            get { return EnsureCommandExists(Id.OpenFile, CommonIcons.OpenFile); }
        }

        /// <summary>
        /// Gets the value that represents theOpen File From MRU command.
        /// </summary>
        public static RockstarRoutedCommand OpenFileFromMru
        {
            get { return EnsureCommandExists(Id.OpenFileFromMru); }
        }

        /// <summary>
        /// Gets the value that represents the Open Project From MRU command.
        /// </summary>
        public static RockstarRoutedCommand OpenProjectFromMru
        {
            get { return EnsureCommandExists(Id.OpenProjectFromMru); }
        }

        /// <summary>
        /// Gets the value that represents the Paste command.
        /// </summary>
        public static RockstarRoutedCommand Paste
        {
            get { return EnsureCommandExists(Id.Paste, CommonIcons.Paste); }
        }

        /// <summary>
        /// Gets the value that represents the Quick Find command.
        /// </summary>
        public static RockstarRoutedCommand QuickFind
        {
            get { return EnsureCommandExists(Id.QuickFind); }
        }

        /// <summary>
        /// Gets the value that represents the Quick Replace command.
        /// </summary>
        public static RockstarRoutedCommand QuickReplace
        {
            get { return EnsureCommandExists(Id.QuickReplace); }
        }

        /// <summary>
        /// Gets the value that represents the Redo command.
        /// </summary>
        public static RockstarRoutedCommand Redo
        {
            get { return EnsureCommandExists(Id.Redo, CommonIcons.Redo); }
        }

        /// <summary>
        /// Gets the value that represents the Rename command.
        /// </summary>
        public static RockstarRoutedCommand Rename
        {
            get { return EnsureCommandExists(Id.Rename, CommonIcons.Rename); }
        }

        /// <summary>
        /// Gets the value that represents the Replace In Files command.
        /// </summary>
        public static RockstarRoutedCommand ReplaceInFiles
        {
            get { return EnsureCommandExists(Id.ReplaceInFiles); }
        }

        /// <summary>
        /// Gets the value that represents the Report Bug command.
        /// </summary>
        public static RockstarRoutedCommand ReportBug
        {
            get { return EnsureCommandExists(Id.ReportBug, CommonIcons.ReportBug); }
        }

        /// <summary>
        /// Gets the value that represents the Restart command.
        /// </summary>
        public static RockstarRoutedCommand Restart
        {
            get { return EnsureCommandExists(Id.Restart, CommonIcons.Restart); }
        }

        /// <summary>
        /// Gets the value that represents the Save command.
        /// </summary>
        public static RockstarRoutedCommand Save
        {
            get { return EnsureCommandExists(Id.Save, CommonIcons.Save); }
        }

        /// <summary>
        /// Gets the value that represents the Save All command.
        /// </summary>
        public static RockstarRoutedCommand SaveAll
        {
            get { return EnsureCommandExists(Id.SaveAll, CommonIcons.SaveAll); }
        }

        /// <summary>
        /// Gets the value that represents the Save As command.
        /// </summary>
        public static RockstarRoutedCommand SaveAs
        {
            get { return EnsureCommandExists(Id.SaveAs, CommonIcons.Save); }
        }

        /// <summary>
        /// Gets the value that represents the Select All command.
        /// </summary>
        public static RockstarRoutedCommand SelectAll
        {
            get { return EnsureCommandExists(Id.SelectAll, CommonIcons.SelectAll); }
        }

        /// <summary>
        /// Gets the value that represents the Shell Execute command.
        /// </summary>
        public static RockstarRoutedCommand ShellExecute
        {
            get { return EnsureCommandExists(Id.ShellExecute); }
        }

        /// <summary>
        /// Gets the value that represents the Undo command.
        /// </summary>
        public static RockstarRoutedCommand Undo
        {
            get { return EnsureCommandExists(Id.Undo, CommonIcons.Undo); }
        }

        /// <summary>
        /// Gets the value that represents the View About command.
        /// </summary>
        public static RockstarRoutedCommand ViewAbout
        {
            get { return EnsureCommandExists(Id.ViewAbout); }
        }

        /// <summary>
        /// Gets the value that represents the Help command.
        /// </summary>
        public static RockstarRoutedCommand ViewHelp
        {
            get { return EnsureCommandExists(Id.ViewHelp, CommonIcons.Help); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new <see cref="RSG.Editor.RockstarRoutedCommand"/> object that is
        /// associated with the specified identifier.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command to create.
        /// </param>
        /// <param name="icon">
        /// The icon that the created command should be using.
        /// </param>
        /// <returns>
        /// The newly created <see cref="RSG.Editor.RockstarRoutedCommand"/> object.
        /// </returns>
        private static RockstarRoutedCommand CreateCommand(string id, BitmapSource icon)
        {
            string name = GetName(id);
            string category = GetStringValue(id, StringValue.Category);
            string description = GetStringValue(id, StringValue.Description);

            InputGestureCollection gestures = GetGestures(id);
            return new RockstarRoutedCommand(
                name, typeof(RockstarCommands), gestures, category, description, icon);
        }

        /// <summary>
        /// Makes sure the internal value for the command associated with the specified
        /// identifier exists and returns it.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command that should be created and returned.
        /// </param>
        /// <returns>
        /// The <see cref="RSG.Editor.RockstarRoutedCommand"/> object associated with the
        /// specified identifier.
        /// </returns>
        private static RockstarRoutedCommand EnsureCommandExists(Id id)
        {
            return EnsureCommandExists(id, null);
        }

        /// <summary>
        /// Makes sure the internal value for the command associated with the specified
        /// identifier exists and returns it.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command that should be created and returned.
        /// </param>
        /// <param name="icon">
        /// The icon that the command should be using.
        /// </param>
        /// <returns>
        /// The <see cref="RSG.Editor.RockstarRoutedCommand"/> object associated with the
        /// specified identifier.
        /// </returns>
        private static RockstarRoutedCommand EnsureCommandExists(Id id, BitmapSource icon)
        {
            int commandIndex = (int)id;
            if (commandIndex < -1 || commandIndex >= _internalCommands.Length)
            {
                return null;
            }

            RockstarRoutedCommand command = _internalCommands[commandIndex];
            if (command == null)
            {
                lock (_internalCommands)
                {
                    if (command == null)
                    {
                        command = CreateCommand(id.ToString(), icon);
                        _internalCommands[commandIndex] = command;
                    }
                }
            }

            return command;
        }

        /// <summary>
        /// Creates a System.Windows.InputGestureCollection object that contains the default
        /// input gestures for the command with the specified identifier.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command whose default gestures should be created.
        /// </param>
        /// <returns>
        /// A System.Windows.InputGestureCollection object that contains the default input
        /// gestures for the command with the specified identifier.
        /// </returns>
        private static InputGestureCollection GetGestures(string id)
        {
            InputGestureCollection inputGestureCollection = new InputGestureCollection();
            string keyGestures = GetStringValue(id, StringValue.KeyGestures);
            if (String.IsNullOrWhiteSpace(keyGestures))
            {
                return inputGestureCollection;
            }

            KeyGestureConverter converter = new KeyGestureConverter();
            while (!String.IsNullOrEmpty(keyGestures))
            {
                int num = keyGestures.IndexOf(";", StringComparison.Ordinal);
                string token;
                if (num >= 0)
                {
                    token = keyGestures.Substring(0, num);
                    keyGestures = keyGestures.Substring(num + 1);
                }
                else
                {
                    token = keyGestures;
                    keyGestures = String.Empty;
                }

                KeyGesture gesture = null;

                try
                {
                    gesture = converter.ConvertFromInvariantString(token) as KeyGesture;
                }
                catch (Exception)
                {
                    Debug.Assert(false, "Unable to create the key gesture", "{0}\n", token);
                    continue;
                }

                if (gesture != null)
                {
                    inputGestureCollection.Add(gesture);
                }
            }

            return inputGestureCollection;
        }

        /// <summary>
        /// Retrieves the name string value for the command with the specified id.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command whose name should be retrieved.
        /// </param>
        /// <returns>
        /// The name value for the command with the specified id.
        /// </returns>
        public static string GetName(string id)
        {
            return GetStringValue(id, StringValue.Name) ?? id;
        }

        /// <summary>
        /// Retrieves the specified string value for the command with the specified id from
        /// the command string table.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command whose string value should be retrieved.
        /// </param>
        /// <param name="value">
        /// Specifies which string value to retrieve.
        /// </param>
        /// <returns>
        /// The specified string value for the command with the specified id.
        /// </returns>
        private static string GetStringValue(string id, StringValue value)
        {
            string tableId = id;
            switch (value)
            {
                case StringValue.Category:
                    tableId += "Category";
                    break;
                case StringValue.Description:
                    tableId += "Description";
                    break;
                case StringValue.Name:
                    tableId += "Name";
                    break;
                case StringValue.KeyGestures:
                    tableId += "KeyGestures";
                    break;
                default:
                    tableId = null;
                    Debug.Assert(false, "Unknown string value specified.");
                    break;
            }

            if (tableId == null)
            {
                return String.Empty;
            }

            return CommandStringTable.GetString(tableId);
        }

        /// <summary>
        /// Initialises the array used to store the System.Windows.Input.RoutedCommand objects
        /// that make up the core commands for a Rockstar application.
        /// </summary>
        /// <returns>
        /// The array used to store the internal System.Windows.Input.RoutedCommand objects.
        /// </returns>
        private static RockstarRoutedCommand[] InitialiseInternalStorage()
        {
            return new RockstarRoutedCommand[(int)Id.CommandCount];
        }
        #endregion Methods
    } // RSG.Editor.SharedCommands.RockstarCommands {Class}
} // RSG.Editor.SharedCommands {Namespace}
