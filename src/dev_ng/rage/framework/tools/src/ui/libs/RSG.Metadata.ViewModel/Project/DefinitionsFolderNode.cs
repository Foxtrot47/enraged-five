﻿//---------------------------------------------------------------------------------------------
// <copyright file="DefinitionsFolderNode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Project
{
    using System.Collections.Generic;
    using System.IO;
    using System.Windows.Media.Imaging;
    using RSG.Editor;
    using RSG.Project.ViewModel;

    /// <summary>
    /// The node inside the project explorer that represents a standard metadata project. This
    /// class cannot be inherited.
    /// </summary>
    public sealed class DefinitionsFolderNode : HierarchyNode
    {
        #region Fields
        /// <summary>
        /// The full path to the directory this node is representing.
        /// </summary>
        private string _fullPath;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DefinitionsFolderNode"/> class.
        /// </summary>
        /// <param name="fullPath">
        /// The full path to the directory this node is representing.
        /// </param>
        /// <param name="parent">
        /// The parent node.
        /// </param>
        public DefinitionsFolderNode(string fullPath, IHierarchyNode parent)
            : base(false)
        {
            this._fullPath = fullPath;
            this.Text = System.IO.Path.GetFileName(fullPath);
            this.Parent = parent;
            this.Icon = CommonIcons.ClosedFolder;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the bitmap source that is used to display the icon for this item when it has
        /// been expanded.
        /// </summary>
        public override BitmapSource ExpandedIcon
        {
            get { return CommonIcons.OpenedFolder; }
        }

        /// <summary>
        /// Gets the priority value for this object. By default all objects are 0.
        /// </summary>
        public override int Priority
        {
            get { return 0; }
        }
        #endregion Propertiess

        #region Methods
        /// <summary>
        /// Called just before this object in the user interface is expanded inside a
        /// hierarchical control.
        /// </summary>
        public override void BeforeExpand()
        {
            base.BeforeExpand();
            foreach (IHierarchyNode child in this.Items)
            {
                child.IsExpandable = true;
            }
        }

        /// <summary>
        /// Creates the child elements for this view model. This is only called once the item
        /// has been expanded in the user interface.
        /// </summary>
        /// <param name="collection">
        /// The collection to add the elements to.
        /// </param>
        protected override void CreateChildren(ICollection<IHierarchyNode> collection)
        {
            ProjectNode project = this.ParentProject;
            if (project == null)
            {
                return;
            }

            if (Directory.Exists(this._fullPath))
            {
                string value = project.GetProperty("IncludeSubDirectories");
                bool includeSubDirectories = false;
                if (!bool.TryParse(value, out includeSubDirectories))
                {
                    includeSubDirectories = false;
                }

                if (includeSubDirectories)
                {
                    string[] directories = System.IO.Directory.GetDirectories(this._fullPath);
                    foreach (string directory in directories)
                    {
                        collection.Add(new DefinitionsFolderNode(directory, this));
                    }
                }

                string[] files = System.IO.Directory.GetFiles(this._fullPath, "*.psc");
                foreach (string file in files)
                {
                    collection.Add(new DefinitionsFileNode(file, this));
                }
            }

            if (collection.Count == 0)
            {
                this.IsExpandable = false;
            }
        }
        #endregion Methods
    } // RSG.Metadata.ViewModel.Project.DefinitionsFolderNode {Class}
} // RSG.Metadata.ViewModel.Project {Namespace}
