﻿//---------------------------------------------------------------------------------------------
// <copyright file="ExtractItemsAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Rpf.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text;
    using RSG.Base.Extensions;
    using RSG.Editor;
    using RSG.Rpf.Commands.Resources;
    using RSG.Rpf.ViewModel;

    /// <summary>
    /// Implements the <see cref="RpfViewerCommands.ExtractRpfItems"/> and
    /// <see cref="RpfViewerCommands.ExtractEntireRpf"/> commands.
    /// </summary>
    public class ExtractItemsAction : ExtractionAction
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ExtractItemsAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public ExtractItemsAction(
            ParameterResolverDelegate<IEnumerable<PackEntryViewModel>> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="commandParameter">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(IEnumerable<PackEntryViewModel> commandParameter)
        {
            return commandParameter.Count() > 0;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="commandParameter">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(IEnumerable<PackEntryViewModel> commandParameter)
        {
            int count = commandParameter.Count();
            if (count == 0)
            {
                return;
            }

            IMessageBoxService messageService = this.GetService<IMessageBoxService>();
            ICommonDialogService dlgService = this.GetService<ICommonDialogService>();
            if (messageService == null || dlgService == null)
            {
                Debug.Assert(false, "Unable to extract as services are missing.");
                return;
            }

            string directory = null;
            Guid id = new Guid("9B17365E-0E9A-486C-9F7B-99C402D1C164");
            string title = "Select destination for the extracted file.";
            if (count != 1)
            {
                title = "Select destination for the extracted '" + count + "' files.";
            }

            if (dlgService.ShowSelectFolder(id, title, out directory) != true)
            {
                return;
            }

            var failedExtractions = new Dictionary<PackEntryViewModel, string>();
            foreach (PackEntryViewModel item in commandParameter)
            {
                if (item == null)
                {
                    continue;
                }

                string name = item.Name;
                string path = item.Path;
                string destination = System.IO.Path.Combine(directory, path, name);
                FileInfo fileInfo = new FileInfo(destination);
                if (fileInfo.Exists && fileInfo.IsReadOnly)
                {
                    IMessageBox messageBox = messageService.CreateMessageBox();
                    messageBox.SetIconToExclamation();
                    messageBox.Text = StringTable.WriteProtectedMsg.FormatCurrent(name);
                    messageBox.AddButton(StringTable.ExtractAsButtonText, 1, false, false);
                    messageBox.AddButton(StringTable.OverwriteButtonText, 2, false, false);
                    messageBox.AddButton(StringTable.SkipButtonText, 3, false, false);

                UserChoice:
                    switch (messageBox.ShowMessageBox())
                    {
                        case 1:
                            if (!dlgService.ShowSaveFile(directory, name, out destination))
                            {
                                continue;
                            }

                            break;
                        case 2:
                            try
                            {
                                fileInfo.IsReadOnly = false;
                            }
                            catch
                            {
                                messageService.Show(
                                    StringTable.OverwriteFailedMsg.FormatCurrent(name),
                                    String.Empty,
                                    System.Windows.MessageBoxButton.OK,
                                    System.Windows.MessageBoxImage.Error);

                                goto UserChoice;
                            }

                            break;
                        case 3:
                            continue;
                        default:
                            Debug.Assert(false, "Unexpected result from message box");
                            continue;
                    }
                }

                try
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(destination));
                    if (!this.Extract(item, destination))
                    {
                        failedExtractions.Add(item, StringTable.ExtractionStreamFailedMsg);
                    }
                }
                catch (UnauthorizedAccessException)
                {
                    failedExtractions.Add(item, StringTable.ExtractionPermissionFailedMsg);
                }
                catch (Exception)
                {
                    failedExtractions.Add(item, StringTable.ExtractionUnknownFailedMsg);
                }
            }

            if (failedExtractions.Count == 0)
            {
                messageService.Show(StringTable.ExtractAllSuccessMsg.FormatCurrent(count));
            }
            else
            {
                string msg = StringTable.ExtractionFailedMsg;
                StringBuilder failedItems = new StringBuilder();
                foreach (KeyValuePair<PackEntryViewModel, string> failure in failedExtractions)
                {
                    failedItems.Append(failure.Key.Name);
                    failedItems.Append(" - ");
                    failedItems.AppendLine(failure.Value);
                }

                messageService.Show(
                    StringTable.ExtractionFailedMsg.FormatCurrent(failedItems),
                    String.Empty,
                    System.Windows.MessageBoxButton.OK,
                    System.Windows.MessageBoxImage.Error);
            }
        }
        #endregion Methods
    } // RSG.Rpf.Commands.ExtractItemsAction {Class}
} // RSG.Rpf.Commands {Namespace}
