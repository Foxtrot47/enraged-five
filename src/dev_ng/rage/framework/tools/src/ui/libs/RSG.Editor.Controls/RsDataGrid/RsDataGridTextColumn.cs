﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsDataGridTextColumn.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using RSG.Editor.Controls.Helpers;

    /// <summary>
    /// Represents a data grid column that hosts textual content in its data grid cells.
    /// </summary>
    public class RsDataGridTextColumn : DataGridTextColumn
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="EditableStyleKey"/> property.
        /// </summary>
        private static ResourceKey _editableStyleKey = CreateStyleKey("EditableStyleKey");

        /// <summary>
        /// The private field used for the <see cref="ReadOnlyStyleKey"/> property.
        /// </summary>
        private static ResourceKey _readOnlyStyleKey = CreateStyleKey("ReadOnlyStyleKey");

        /// <summary>
        /// The private field used for the <see cref="IsReadOnlyBinding"/> property.
        /// </summary>
        private BindingBase _isReadOnlyBinding;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsDataGridTextColumn"/> class.
        /// </summary>
        static RsDataGridTextColumn()
        {
            DataGridBoundColumn.IsReadOnlyProperty.OverrideMetadata(
                typeof(RsDataGridTextColumn),
                new FrameworkPropertyMetadata(OnReadOnlyPropertyChanged));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsDataGridTextColumn"/> class.
        /// </summary>
        public RsDataGridTextColumn()
        {
            this.CellStyle = new DataGridCellStyle();
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the resource key used to reference the style used on editable check boxes.
        /// </summary>
        public static ResourceKey EditableStyleKey
        {
            get { return _editableStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on read only check boxes.
        /// </summary>
        public static ResourceKey ReadOnlyStyleKey
        {
            get { return _readOnlyStyleKey; }
        }

        /// <summary>
        /// Gets or sets the binding that will be applied to the IsReadOnly property of the
        /// TextBox control.
        /// </summary>
        public virtual BindingBase IsReadOnlyBinding
        {
            get
            {
                return this._isReadOnlyBinding;
            }

            set
            {
                if (this._isReadOnlyBinding != value)
                {
                    this._isReadOnlyBinding = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the text box inside the cell gets all
        /// its text selected when the cell gets focus using keyboard navigation.
        /// </summary>
        public bool SelectAllOnGotFocus
        {
            get
            {
                DataGridCellStyle style = this.CellStyle as DataGridCellStyle;
                if (style == null)
                {
                    return false;
                }

                return style.SelectAllOnGotFocus;
            }

            set
            {
                DataGridCellStyle style = this.CellStyle as DataGridCellStyle;
                if (style == null)
                {
                    Debug.Fail("Cannot set SelectAllOnGotFocus when cell style is overridden");
                    return;
                }

                style.SelectAllOnGotFocus = value;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates the visual tree that will become the content of the specified cell.
        /// </summary>
        /// <param name="cell">
        /// The cell the visual tree is being created for.
        /// </param>
        /// <param name="dataItem">
        /// The data item currently bound to the specified cell.
        /// </param>
        /// <returns>
        /// The root of the visual tree that will become the content of the specified cell.
        /// </returns>
        protected override FrameworkElement GenerateEditingElement(
            DataGridCell cell, object dataItem)
        {
            TextBox textBox = new TextBox();
            this.ApplyProperties(textBox);
            textBox.SetResourceReference(FrameworkElement.StyleProperty, EditableStyleKey);

            BindingBase binding = this.Binding;
            if (binding != null)
            {
                BindingOperations.SetBinding(textBox, TextBox.TextProperty, binding);
            }
            else
            {
                BindingOperations.ClearBinding(textBox, TextBox.TextProperty);
            }

            return textBox;
        }

        /// <summary>
        /// Creates the visual tree that will become the content of the specified cell.
        /// </summary>
        /// <param name="cell">
        /// The cell the visual tree is being created for.
        /// </param>
        /// <param name="dataItem">
        /// The data item currently bound to the specified cell.
        /// </param>
        /// <returns>
        /// The root of the visual tree that will become the content of the specified cell.
        /// </returns>
        protected override FrameworkElement GenerateElement(DataGridCell cell, object dataItem)
        {
            if (!this.IsReadOnly)
            {
                return this.GenerateEditingElement(cell, dataItem);
            }

            TextBlock textBlock = new TextBlock();
            textBlock.Margin = new Thickness(2.0, 0.0, 2.0, 0.0);
            this.ApplyProperties(textBlock);
            textBlock.SetResourceReference(FrameworkElement.StyleProperty, ReadOnlyStyleKey);

            BindingBase binding = this.Binding;
            if (binding != null)
            {
                BindingOperations.SetBinding(textBlock, TextBlock.TextProperty, binding);
            }
            else
            {
                BindingOperations.ClearBinding(textBlock, TextBlock.TextProperty);
            }

            return textBlock;
        }

        /// <summary>
        /// Creates a new resource key that references a style with the specified name.
        /// </summary>
        /// <param name="styleName">
        /// The name that the resource key will be referencing.
        /// </param>
        /// <returns>
        /// A new resource key that references a style with the specified name.
        /// </returns>
        private static ResourceKey CreateStyleKey(string styleName)
        {
            return new StyleKey<RsDataGridTextColumn>(styleName);
        }

        /// <summary>
        /// Called whenever the IsReadOnly dependency property changes.
        /// </summary>
        /// <param name="d">
        /// The object whose IsReadOnly dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnReadOnlyPropertyChanged(
            DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RsDataGridTextColumn column = d as RsDataGridTextColumn;
            if (column == null)
            {
                Debug.Assert(false, "Incorrect dependency object cast");
                return;
            }

            // This will make sure the visual tree for all of the cells are rebuilt.
            column.NotifyPropertyChanged("ElementStyle");
            column.NotifyPropertyChanged("EditingElementStyle");
        }

        /// <summary>
        /// Applies all of the properties on the specified element by syncing the property
        /// value from the column.
        /// </summary>
        /// <param name="element">
        /// The element the properties are being set on.
        /// </param>
        private void ApplyProperties(FrameworkElement element)
        {
            DependencyProperty textBoxProperty = TextBox.IsReadOnlyProperty;
            this.ApplyBinding(this.IsReadOnlyBinding, element, textBoxProperty);

            this.ApplyProperty(
                element,
                TextElement.FontFamilyProperty,
                DataGridTextColumn.FontFamilyProperty);

            this.ApplyProperty(
                element, TextElement.FontSizeProperty, DataGridTextColumn.FontSizeProperty);

            this.ApplyProperty(
                element, TextElement.FontStyleProperty, DataGridTextColumn.FontStyleProperty);

            this.ApplyProperty(
                element,
                TextElement.FontWeightProperty,
                DataGridTextColumn.FontWeightProperty);

            this.ApplyProperty(
                element,
                TextElement.ForegroundProperty,
                DataGridTextColumn.ForegroundProperty);
        }

        /// <summary>
        /// Applies a single binding on the target object.
        /// </summary>
        /// <param name="binding">
        /// The binding to apply.
        /// </param>
        /// <param name="target">
        /// The target object for the binding.
        /// </param>
        /// <param name="property">
        /// The dependency property to set the binding on.
        /// </param>
        private void ApplyBinding(
            BindingBase binding, DependencyObject target, DependencyProperty property)
        {
            if (binding != null)
            {
                BindingOperations.SetBinding(target, property, binding);
                return;
            }

            BindingOperations.ClearBinding(target, property);
        }

        /// <summary>
        /// Applies the specified column property onto the specified combo box.
        /// </summary>
        /// <param name="element">
        /// The combo box to set the values on.
        /// </param>
        /// <param name="comboBoxProperty">
        /// The property on the combo box to set.
        /// </param>
        /// <param name="columnProperty">
        /// The property on the column box to take.
        /// </param>
        private void ApplyProperty(
            FrameworkElement element,
            DependencyProperty comboBoxProperty,
            DependencyProperty columnProperty)
        {
            ValueSource source = DependencyPropertyHelper.GetValueSource(this, columnProperty);
            if (source.BaseValueSource == BaseValueSource.Default)
            {
                element.ClearValue(comboBoxProperty);
                return;
            }

            element.SetValue(comboBoxProperty, this.GetValue(columnProperty));
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// The text box control class to use as the editable element for this column.
        /// </summary>
        private class DataGridCellStyle : Style
        {
            #region Fields
            /// <summary>
            /// The private field used for the <see cref="SelectAllOnGotFocus"/> property.
            /// </summary>
            private bool _selectAllOnGotFocus;
            #endregion Fields

            #region Constructors
            public DataGridCellStyle()
                : base(
                typeof(DataGridCell),
                (Style)Application.Current.FindResource(typeof(DataGridCell)))
            {
                this.Setters.Add(new EventSetter(UIElement.GotFocusEvent, new RoutedEventHandler(this.OnGotFocus)));

                this.Seal();            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets or sets a value indicating whether the text box inside the cell gets all
            /// its text selected when the cell gets focus using keyboard navigation.
            /// </summary>
            public bool SelectAllOnGotFocus
            {
                get { return this._selectAllOnGotFocus; }
                set { this._selectAllOnGotFocus = value; }
            }
            #endregion Properties

            #region Methods
            /// <summary>
            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            private void OnGotFocus(object sender, RoutedEventArgs e)
            {
                if (!this.SelectAllOnGotFocus)
                {
                    return;
                }

                if (!Keyboard.IsKeyDown(Key.Enter) && !Keyboard.IsKeyDown(Key.Tab))
                {
                    return;
                }

                DataGridCell cell = ((DataGridCell)sender);
                TextBox textBox = cell.GetVisualDescendent<TextBox>();
                if (textBox == null)
                {
                    return;
                }

                if (textBox.Text != null)
                {
                    textBox.MoveFocus(new TraversalRequest(FocusNavigationDirection.First));
                    textBox.SelectAll();
                }
            }
            #endregion Methods
        } // RsDataGridTextColumn.DataGridCellStyle
        #endregion
    } // RSG.Editor.Controls.RsDataGridTextColumn {Class}
} // RSG.Editor.Controls {Namespace}
