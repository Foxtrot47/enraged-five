﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.UniversalLog.View
{
    /// <summary>
    /// 
    /// </summary>
    public static class UniversalLogCommandIds
    {
        #region Fields
        /// <summary>
        /// The private instance used for the <see cref="ContextMenu"/> property.
        /// </summary>
        private static Guid? m_contextMenu;

        /// <summary>
        /// A private instance to a generic object used to support thread access.
        /// </summary>
        private static object m_syncRoot = new object();
        #endregion // Fields

        #region Properties
        /// <summary>
        /// Gets the global identifier used for the context menu for the individual items in
        /// the RPF viewer list.
        /// </summary>
        public static Guid ContextMenu
        {
            get
            {
                if (!m_contextMenu.HasValue)
                {
                    lock (m_syncRoot)
                    {
                        if (!m_contextMenu.HasValue)
                        {
                            m_contextMenu = new Guid("D91EFAAB-340A-49EB-AFB3-1D034B149804");
                        }
                    }
                }

                return m_contextMenu.Value;
            }
        }
        #endregion // Properties
    } // UniversalLogCommandIds
}
