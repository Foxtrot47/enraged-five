﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProjectCommandIds.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel
{
    using System;

    /// <summary>
    /// Contains static identifiers that can be used to extend the context menu for the project
    /// tree view.
    /// </summary>
    public static class ProjectCommandIds
    {
        #region Fields
        /// <summary>
        /// The private instance used for the <see cref="CollectionContextMenu"/> property.
        /// </summary>
        private static Guid? _collectionContextMenu;

        /// <summary>
        /// The private instance used for the <see cref="CollectionFolderContextMenu"/>
        /// property.
        /// </summary>
        private static Guid? _collectionFolderContextMenu;

        /// <summary>
        /// The private instance used for the <see cref="ProjectContextMenu"/>
        /// property.
        /// </summary>
        private static Guid? _projectContextMenu;

        /// <summary>
        /// The private instance used for the <see cref="ProjectDynamicFolderContextMenu"/>
        /// property.
        /// </summary>
        private static Guid? _projectDynamicFolderContextMenu;

        /// <summary>
        /// The private instance used for the <see cref="ProjectFolderContextMenu"/>
        /// property.
        /// </summary>
        private static Guid? _projectFolderContextMenu;

        /// <summary>
        /// The private instance used for the <see cref="ProjectItemContextMenu"/>
        /// property.
        /// </summary>
        private static Guid? _projectItemContextMenu;

        /// <summary>
        /// A private instance to a generic object used to support thread access.
        /// </summary>
        private static object _syncRoot = new object();
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets the global identifier used for the context menu for a collection node object.
        /// </summary>
        public static Guid CollectionContextMenu
        {
            get
            {
                if (!_collectionContextMenu.HasValue)
                {
                    lock (_syncRoot)
                    {
                        if (!_collectionContextMenu.HasValue)
                        {
                            _collectionContextMenu =
                                new Guid("AE36472D-0B0A-4524-A1F7-24230E57BCEB");
                        }
                    }
                }

                return _collectionContextMenu.Value;
            }
        }

        /// <summary>
        /// Gets the global identifier used for the context menu for a collection folder node
        /// object.
        /// </summary>
        public static Guid CollectionFolderContextMenu
        {
            get
            {
                if (!_collectionFolderContextMenu.HasValue)
                {
                    lock (_syncRoot)
                    {
                        if (!_collectionFolderContextMenu.HasValue)
                        {
                            _collectionFolderContextMenu =
                                new Guid("EED8133B-598A-42C2-AE6B-673D4DAC0EAE");
                        }
                    }
                }

                return _collectionFolderContextMenu.Value;
            }
        }

        /// <summary>
        /// Gets the global identifier used for the context menu for a project node object.
        /// </summary>
        public static Guid ProjectContextMenu
        {
            get
            {
                if (!_projectContextMenu.HasValue)
                {
                    lock (_syncRoot)
                    {
                        if (!_projectContextMenu.HasValue)
                        {
                            _projectContextMenu =
                                new Guid("31FB191B-ED45-460A-86E2-99988F6DC537");
                        }
                    }
                }

                return _projectContextMenu.Value;
            }
        }

        /// <summary>
        /// Gets the global identifier used for the context menu for a project dynamic folder
        /// node object.
        /// </summary>
        public static Guid ProjectDynamicFolderContextMenu
        {
            get
            {
                if (!_projectDynamicFolderContextMenu.HasValue)
                {
                    lock (_syncRoot)
                    {
                        if (!_projectDynamicFolderContextMenu.HasValue)
                        {
                            _projectDynamicFolderContextMenu =
                                new Guid("72DC7FA7-088A-4228-9564-CFCD4F49F623");
                        }
                    }
                }

                return _projectDynamicFolderContextMenu.Value;
            }
        }
        

        /// <summary>
        /// Gets the global identifier used for the context menu for a project folder node
        /// object.
        /// </summary>
        public static Guid ProjectFolderContextMenu
        {
            get
            {
                if (!_projectFolderContextMenu.HasValue)
                {
                    lock (_syncRoot)
                    {
                        if (!_projectFolderContextMenu.HasValue)
                        {
                            _projectFolderContextMenu =
                                new Guid("6E1D5DEE-DFA8-4C8F-AFCA-E4A8AC526989");
                        }
                    }
                }

                return _projectFolderContextMenu.Value;
            }
        }

        /// <summary>
        /// Gets the global identifier used for the context menu for a project item node
        /// object.
        /// </summary>
        public static Guid ProjectItemContextMenu
        {
            get
            {
                if (!_projectItemContextMenu.HasValue)
                {
                    lock (_syncRoot)
                    {
                        if (!_projectItemContextMenu.HasValue)
                        {
                            _projectItemContextMenu =
                                new Guid("46719377-807F-4ABB-B4BD-0168FE0405F3");
                        }
                    }
                }

                return _projectItemContextMenu.Value;
            }
        }
        #endregion Properties
    } // RSG.Project.ViewModel.ProjectCommandIds {Class}
} // RSG.Project.ViewModel {Namespace}
