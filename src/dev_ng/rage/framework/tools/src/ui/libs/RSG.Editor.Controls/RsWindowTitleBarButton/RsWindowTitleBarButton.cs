﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsWindowTitleBarButton.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System.Windows;
    using RSG.Interop.Microsoft.Windows;

    /// <summary>
    /// Represents a button that can be used as a system button on the windows title bar.
    /// </summary>
    public class RsWindowTitleBarButton : RsGlyphButton, INonClientArea
    {
        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsWindowTitleBarButton" /> class.
        /// </summary>
        static RsWindowTitleBarButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsWindowTitleBarButton),
                new FrameworkPropertyMetadata(typeof(RsWindowTitleBarButton)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsWindowTitleBarButton"/> class.
        /// </summary>
        public RsWindowTitleBarButton()
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Tests the specified point coordinate and returns a Hit Test result based on the
        /// system hot spot.
        /// </summary>
        /// <param name="point">
        /// The point to test.
        /// </param>
        /// <returns>
        /// A result indicating the position of the cursor hot spot.
        /// </returns>
        public NcHitTestResult HitTest(Point point)
        {
            return NcHitTestResult.CLIENT;
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsWindowTitleBarButton {Class}
} // RSG.Editor.Controls {Namespace}
