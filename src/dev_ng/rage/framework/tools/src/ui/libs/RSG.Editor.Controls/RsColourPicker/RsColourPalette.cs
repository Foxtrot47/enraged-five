﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsColourPalette.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Contains a list of selectable colour arranged inside a uniform grid.
    /// </summary>
    public class RsColourPalette : ListBox
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="Columns" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty ColumnsProperty;

        /// <summary>
        /// Identifies the <see cref="Rows" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty RowsProperty;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsColourPalette" /> class.
        /// </summary>
        static RsColourPalette()
        {
            ColumnsProperty =
                DependencyProperty.Register(
                "Columns",
                typeof(int),
                typeof(RsColourPaletteItem),
                new FrameworkPropertyMetadata(0));

            RowsProperty =
                DependencyProperty.Register(
                "Rows",
                typeof(int),
                typeof(RsColourPaletteItem),
                new FrameworkPropertyMetadata(0));

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsColourPalette),
                new FrameworkPropertyMetadata(typeof(RsColourPalette)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsColourPalette" /> class.
        /// </summary>
        public RsColourPalette()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the number of columns that are in the grid.
        /// </summary>
        public int Columns
        {
            get { return (int)this.GetValue(ColumnsProperty); }
            set { this.SetValue(ColumnsProperty, value); }
        }

        /// <summary>
        /// Gets or sets the number of rows that are in the grid.
        /// </summary>
        public int Rows
        {
            get { return (int)this.GetValue(RowsProperty); }
            set { this.SetValue(RowsProperty, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates or identifies the element used to display the child items.
        /// </summary>
        /// <returns>
        /// A new <see cref="RsColourPaletteItem"/> object.
        /// </returns>
        protected override DependencyObject GetContainerForItemOverride()
        {
            return new RsColourPaletteItem();
        }

        /// <summary>
        /// Determines whether the specified item is, or is eligible to be, its own item
        /// container.
        /// </summary>
        /// <param name="item">
        /// The item to check whether it is an item container.
        /// </param>
        /// <returns>
        /// True if the item is eligible to be its own item container, if the item is a
        /// <see cref="RsColourPaletteItem"/> object; otherwise, false.
        /// </returns>
        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is RsColourPaletteItem;
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsColourPalette {Class}
} // RSG.Editor.Controls {Namespace}
