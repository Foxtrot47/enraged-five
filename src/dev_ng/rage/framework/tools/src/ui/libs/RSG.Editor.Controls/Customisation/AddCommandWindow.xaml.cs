﻿//---------------------------------------------------------------------------------------------
// <copyright file="AddCommandWindow.xaml.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Customisation
{
    using System.Windows;
    using System.Windows.Input;

    /// <summary>
    /// Interaction logic for the command customisation window.
    /// </summary>
    public partial class AddCommandWindow : RsWindow
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="AddCommandWindow"/> class.
        /// </summary>
        public AddCommandWindow()
        {
            this.InitializeComponent();

            this.CommandBindings.Add(
                new CommandBinding(
                    CustomViewCommands.CancelCommandSelection,
                    this.OnCancelCommandSelectionExecuted));

            this.CommandBindings.Add(
                new CommandBinding(
                    CustomViewCommands.ConfirmCommandSelection,
                    this.OnConfirmCommandSelectionExecuted,
                    this.CanConfirmCommandSelection));
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the <see cref="CustomViewCommands.IncreasePriority"/> command
        /// can be executed based on the current state of the application.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The can execute data that has been sent with the command.
        /// </param>
        private void CanConfirmCommandSelection(object s, CanExecuteRoutedEventArgs e)
        {
            AddCommandWindowDataContext dc = this.DataContext as AddCommandWindowDataContext;
            if (dc == null)
            {
                e.CanExecute = false;
                return;
            }

            e.CanExecute = dc.SelectedDefinition != null;
        }

        /// <summary>
        /// Called whenever the <see cref="CustomViewCommands.CancelCommandSelection"/> command
        /// is fired and needs handling at this instance level.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The execute data that has been sent with the command.
        /// </param>
        private void OnCancelCommandSelectionExecuted(object s, ExecutedRoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        /// <summary>
        /// Called whenever the <see cref="CustomViewCommands.DecreasePriority"/> command is
        /// fired and needs handling at this instance level.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The execute data that has been sent with the command.
        /// </param>
        private void OnConfirmCommandSelectionExecuted(object s, ExecutedRoutedEventArgs e)
        {
            this.DialogResult = true;
        }
        #endregion Methods
    } // RSG.Editor.Controls.Customisation.AddCommandWindow {Class}
} // RSG.Editor.Controls.Customisation {Namespace}
