﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using Tasks = System.Threading.Tasks;
using System.Windows.Media;
using RSG.Base.Configuration;
using RSG.Base.Configuration.Automation;
using RSG.Base.Configuration.Bugstar;
using RSG.Editor;
using RSG.Interop.Bugstar;
using RSG.Interop.Bugstar.Organisation;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Client;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Tasks;
using RSG.Pipeline.Automation.Consumers;
using System.Threading;

namespace RSG.Automation.ViewModel
{

    /// <summary>
    /// Automation Monitor View-Model;
    /// </summary>
    public class AutomationMonitorDataContext : NotifyPropertyChangedBase
    {
        #region Properties
        /// <summary>
        /// Whether the UI displays UTC timestamps.
        /// </summary>
        public bool UseUTC
        {
            get { return _useUTC; }
            set
            {
                SetProperty(ref _useUTC, value, "UseUTC");
            }
        }
        private bool _useUTC;
        
        /// <summary>
        /// Collection of projects (for user selection).
        /// </summary>
        public ObservableCollection<ProjectViewModel> Projects
        {
            get { return _projects; }
            private set
            {
                SetProperty(ref _projects, value, "Projects");
            }
        }
        private ObservableCollection<ProjectViewModel> _projects;

        /// <summary>
        /// Automation Service connection.
        /// </summary>
        public AutomationServiceViewModel SelectedAutomationService
        {
            get { return _selectedAutomationService; }
            set
            {
                SetProperty(ref _selectedAutomationService, value, "SelectedAutomationService");
            }
        }
        private AutomationServiceViewModel _selectedAutomationService;

        /// <summary>
        /// Whether we managed to connect to the selected Automation Service.
        /// </summary>
        public bool IsConnected
        {
            get { return _isConnected; }
            private set
            {
                SetProperty(ref _isConnected, value, "IsConnected");
            }
        }
        private bool _isConnected;

        /// <summary>
        /// Whether the selected Automation Service has jobs to display.
        /// </summary>
        public bool HasJobs
        {
            get { return _hasJobs; }
            private set
            {
                SetProperty(ref _hasJobs, value, "HasJobs");
            }
        }
        private bool _hasJobs;

        /// <summary>
        /// Collection of jobs.
        /// </summary>
        public ObservableCollection<JobViewModel> Jobs
        {
            get { return _jobs; }
            private set
            {
                SetProperty(ref _jobs, value, "Jobs");
                this.HasJobs = (this._jobs.Count > 0);
            }
        }
        private ObservableCollection<JobViewModel> _jobs;

        /// <summary>
        /// Gets or sets a iterator around the currently selected log message view models in the
        /// viewer controls data grid.
        /// </summary>
        public IEnumerable<JobViewModel> SelectedJobs
        {
            get { return _selectedJobs; }
            set
            {
                _selectedJobs = value;
                //RefreshCommandParameters();
            }
        }
        private IEnumerable<JobViewModel> _selectedJobs;

        /// <summary>
        /// Jobs being downloaded; state is saved.
        /// </summary>
        public ObservableCollection<JobDownloadViewModel> DownloadJobs
        {
            get { return _downloadJobs; }
            private set
            {
                SetProperty(ref _downloadJobs, value, "DownloadJobs");
            }
        }
        private ObservableCollection<JobDownloadViewModel> _downloadJobs;

        /// <summary>
        /// Whether the selected Automation Service has clients to display.
        /// </summary>
        public bool HasClients
        {
            get { return _hasClients; }
            private set
            {
                SetProperty(ref _hasClients, value, "HasClients");
            }
        }
        private bool _hasClients;

        /// <summary>
        /// Collection of clients.
        /// </summary>
        public ObservableCollection<ClientViewModel> Clients
        {
            get { return _clients; }
            private set
            {
                SetProperty(ref _clients, value, "Clients");
                this.HasClients = (this._clients.Count > 0);
            }
        }
        private ObservableCollection<ClientViewModel> _clients;

        /// <summary>
        /// Gets the title that should be set on the main window.
        /// </summary>
        public String Title
        {
            get { return this.m_sTitle; }
            private set { this.SetProperty(ref this.m_sTitle, value); }
        }
        private String m_sTitle;

        /// <summary>
        /// Gets the status text that should be displayed in the status bar.
        /// </summary>
        public String StatusText
        {
            get { return _statusText; }
            private set
            {
                SetProperty(ref _statusText, value, "StatusText");
            }
        }
        private String _statusText;

        #region Display Option Properties
        /// <summary>
        /// Color of jobs where local user is owner.
        /// </summary>
        public Color OwnerColor
        {
            get { return _ownerColor; }
            set
            {
                SetProperty(ref _ownerColor, value, "OwnerColor");
            }
        }
        private Color _ownerColor;

        /// <summary>
        /// Color of jobs that are completed where local user is owner.
        /// </summary>
        public Color CompletedOwnerColor
        {
            get { return _completedOwnerColor; }
            set
            {
                SetProperty(ref _completedOwnerColor, value, "CompletedOwnerColor");
            }
        }
        private Color _completedOwnerColor;

        /// <summary>
        /// Color of jobs for active downloads.
        /// </summary>
        public Color ActiveDownloadColor
        {
            get { return _activeDownloadColor; }
            set
            {
                SetProperty(ref _activeDownloadColor, value, "ActiveDownloadColor");
            }
        }
        private Color _activeDownloadColor;
        #endregion // Display Option Properties
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Configuration data.
        /// </summary>
        protected IConfig _config;

        /// <summary>
        /// BUgstar configuration object.
        /// </summary>
        protected IBugstarConfig _bugstarConfig;
        
        /// <summary>
        /// Automation services configuration data.
        /// </summary>
        protected IEnumerable<IAutomationServiceConfig> _automationServicesConfig;
        private Object _automationLock = new Object();

        /// <summary>
        /// Bugstar user objects.
        /// </summary>
        private IEnumerable<RSG.Interop.Bugstar.Organisation.User> _bugstarUsers;

        /// <summary>
        /// Refresh timestamp.
        /// </summary>
        private DateTime _refreshTimestamp;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="config"></param>
        /// <param name="automationServicesConfig"></param>
        /// <param name="bugstarConfig"></param>
        public AutomationMonitorDataContext(IConfig config, IBugstarConfig bugstarConfig)
        {
            this._config = config;
            this._bugstarConfig = bugstarConfig;
            this.IsConnected = false;
            this.Projects = new ObservableCollection<ProjectViewModel>();
            this.Jobs = new ObservableCollection<JobViewModel>();
            this.DownloadJobs = new ObservableCollection<JobDownloadViewModel>();
            this.Clients = new ObservableCollection<ClientViewModel>();
            this.OwnerColor = System.Windows.Media.Color.FromArgb(100, 0, 128, 0);
            this.CompletedOwnerColor = System.Windows.Media.Color.FromArgb(100, 0, 200, 0);
            this._selectedJobs = new JobViewModel[0]; // Initial empty selection.
            this.Title = String.Empty;
            ReloadAutomationConfig();
            RefreshServicesAsync();

            // Bugstar User initialisation.
            BugstarConnection c = new BugstarConnection(bugstarConfig.RESTService, bugstarConfig.AttachmentService);
            c.Login(_bugstarConfig.ReadOnlyUsername, _bugstarConfig.ReadOnlyPassword, String.Empty);
            Project p = Project.GetProjectById(c, this._bugstarConfig.ProjectId);
            _bugstarUsers = p.Users;
        }
        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// Update window title.
        /// </summary>
        public void UpdateTitle()
        {
            if (null != SelectedAutomationService)
            {
                this.Title = SelectedAutomationService.DisplayName;
            }
            else
            {
                this.Title = String.Empty;
            }
        }

        /// <summary>
        /// Update refresh timestamp (and localised String variant).
        /// </summary>
        public void UpdateStatusText()
        {
            this._refreshTimestamp = DateTime.UtcNow;

            Debug.Assert(DateTimeKind.Utc == this._refreshTimestamp.Kind);
            if (UseUTC)
                this.StatusText = String.Format("Refreshed: {0} (UTC)", this._refreshTimestamp.ToString("F"));
            else
                this.StatusText = String.Format("Refreshed: {0} (Local Time)", this._refreshTimestamp.ToLocalTime().ToString("F"));
        }

        /// <summary>
        /// Refresh view from Automation Service.
        /// </summary>
        public async void RefreshAsync()
        {
            UpdateStatusText();

            if (null == this.SelectedAutomationService)
            {
                this.IsConnected = false;
                return;
            }

            bool successful = false;
            IEnumerable<WorkerStatus> clientStatus = null;
            IEnumerable<TaskStatus> taskStatus = null;
            Tasks.Task refreshTask = Tasks.Task.Factory.StartNew(() =>
            {
                try
                {
                    AutomationAdminConsumer consumer = new AutomationAdminConsumer(
                        new Uri(this.SelectedAutomationService.AutomationService.ServerHost, "automation.svc"));
#warning DHM FIX ME: consumers won't tell us whether there are no jobs/clients or connection problem.  This needs fixed.
                    clientStatus = consumer.Clients();
                    taskStatus = consumer.Monitor();
                    if (null == taskStatus || 0 == taskStatus.Count())
                        successful = false;
                    else
                        successful = true;
                }
                catch (Exception)
                {
                    successful = false;
                }
            });
            await Tasks.Task.WhenAll(refreshTask);

            this.IsConnected = successful;
            if (successful)
                RefreshAsync(taskStatus, clientStatus);
        }

        /// <summary>
        /// Refresh the Automation Services configuration.
        /// </summary>
        public async void RefreshServicesAsync()
        {
            // Reload Automation Config from disk.
            ReloadAutomationConfig();

            this.Projects = new ObservableCollection<ProjectViewModel>(RebuildProjectViewModels());
            OnRefreshServices();
        }

        /// <summary>
        /// Refresh Automation services (rebuilds Job and Client View-Models).
        /// </summary>
        public async void RefreshAsync(IEnumerable<TaskStatus> taskStatus, 
            IEnumerable<WorkerStatus> clientStatus)
        {
            Tasks.Task<IEnumerable<JobViewModel>> refreshJobsTask = 
                Tasks.Task.Factory.StartNew<IEnumerable<JobViewModel>>(RebuildJobViewModels, taskStatus.FirstOrDefault());
            Tasks.Task<IEnumerable<ClientViewModel>> refreshClientsTask = 
                Tasks.Task.Factory.StartNew<IEnumerable<ClientViewModel>>(RebuildClientViewModels, clientStatus);
            await Tasks.Task.WhenAll(refreshJobsTask, refreshClientsTask);

            this.Jobs = new ObservableCollection<JobViewModel>(refreshJobsTask.Result);
            this.Clients = new ObservableCollection<ClientViewModel>(refreshClientsTask.Result);

            OnRefreshSelectedService();
        }

        /// <summary>
        /// Checks if a job has already had the specified download type done for it.
        /// </summary>
        /// <param name="msgBoxService"></param>
        /// <param name="jobDownloadType"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool CheckIfJobFilesHaveBeenDownloaded(IMessageBoxService msgBoxService, JobDownloadType jobDownloadType, Guid id)
        {
            if (this.DownloadJobs.Where(n => n.Job.Id.Equals(id) && n.DownloadType.Equals(jobDownloadType)).Any())
            {
                string msg = String.Format("The {0} for job {1} is already in your Download Jobs list.", jobDownloadType.ToString(), id.ToString());
                msgBoxService.Show(msg, "", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Warning);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Any Jobs with this JobState?
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        public bool ContainsItemsWithJobState(JobState state)
        {
            return Jobs.Any(item => item.State == state);
        }

        #region Download Job Log Command
        /// <summary>
        /// Determines whether the <see cref="AutomationMonitorCommands.DownloadJobLog"/> 
        /// command can be executed based on application state.
        /// </summary>
        /// 
        /// <returns>
        /// True if the command can be executed; otherwise, false.
        /// </returns>
        public bool CanDownloadSelectedJobsLogs()
        {
            return (AreAllJobsLogDownloadable(this.SelectedJobs));
        }

        /// <summary>
        /// Called whenever the <see cref="AutomationMonitorCommands.DownloadJobLog"/> 
        /// command is fired and needs handling.
        /// </summary>
        /// <param name="data"></param>
        public async Tasks.Task OnDownloadJobLog(IMessageBoxService msgBoxService)
        {
#warning DHM FIX ME : this is nasty - read from previously constructed config.
            RSG.Base.Configuration.IConfig config = RSG.Base.Configuration.ConfigFactory.CreateConfig();
            List<JobDownloadViewModel> downloadViewModels = new List<JobDownloadViewModel>();  
            // Add to the DownloadJobs view model before starting to download
            foreach (JobViewModel job in this.SelectedJobs)
            {
                JobDownloadViewModel jobDownloadVM = new JobDownloadViewModel(config,
                    JobDownloadType.Log, job);
                DownloadJobs.Add(jobDownloadVM);
                downloadViewModels.Add(jobDownloadVM);
            }

            // Download individually
            foreach (JobDownloadViewModel jobDownloadVM in downloadViewModels)
            {
                ICollection<Tasks.Task> tasks = new List<Tasks.Task>();                
                tasks.Add(jobDownloadVM.DownloadJobLogAsync());
                await Tasks.Task.WhenAll(tasks);
            }
            
        }

        /// <summary>
        /// Return whether all job's log data is downloadable (Completed or Error states).
        /// </summary>
        /// <param name="jobs"></param>
        /// <returns></returns>
        public bool AreAllJobsLogDownloadable(IEnumerable<JobViewModel> jobs)
        {
            IEnumerable<JobViewModel> completedJobs = jobs.Where(j =>
                j.State.Equals(JobState.Completed) || j.State.Equals(JobState.Errors));
            return ((jobs.Count() == completedJobs.Count()) && jobs.Count() > 0);
        }
        #endregion // Download Job Log Command

        #region Download Job Output Command
        /// <summary>
        /// Determines whether the <see cref="AutomationMonitorCommands.DownloadJobOutput"/> 
        /// command can be executed based on application state.
        /// </summary>
        /// <param name="data"></param>
        /// <returns>
        /// True if the command can be executed; otherwise, false.
        /// </returns>
        public bool CanDownloadSelectedJobsOutput()
        {
            return (AreAllJobsDataDownloadable(this.SelectedJobs));
        }

        /// <summary>
        /// Called whenever the <see cref="AutomationMonitorCommands.DownloadJobOutput"/> 
        /// command is fired and needs handling.
        /// </summary>
        /// <param name="data"></param>
        public async Tasks.Task OnDownloadJobOutput(IMessageBoxService msgBoxService)
        {
#warning DHM FIX ME : this is nasty - read from previously constructed config.
            RSG.Base.Configuration.IConfig config = RSG.Base.Configuration.ConfigFactory.CreateConfig();   
         
            // Show all the jobs in the DownloadJob lists before starting to download individually
            List<JobDownloadViewModel> downloadViewModels = new List<JobDownloadViewModel>();
            foreach (JobViewModel job in this.SelectedJobs)
            {
                JobDownloadViewModel jobDownloadVM = new JobDownloadViewModel(config,
                    JobDownloadType.Output, job);

                this.DownloadJobs.Add(jobDownloadVM);
                downloadViewModels.Add(jobDownloadVM);
            }

            // Download one job at a time rather than trying to get all at once
            foreach (JobDownloadViewModel jobDownloadVM in downloadViewModels)
            {
                ICollection<Tasks.Task> tasks = new List<Tasks.Task>();
                tasks.Add(jobDownloadVM.DownloadOutputFilesAsync());
                await Tasks.Task.WhenAll(tasks);
            }            
        }

        /// <summary>
        /// Return whether all job's output data is downloadable (Completed state).
        /// </summary>
        /// <param name="jobs"></param>
        /// <returns></returns>
        public bool AreAllJobsDataDownloadable(IEnumerable<JobViewModel> jobs)
        {
            IEnumerable<JobViewModel> completedJobs = jobs.Where(j =>
                j.State.Equals(JobState.Completed) && (j.Job is MapExportJob));
            return ((jobs.Count() == completedJobs.Count()) && jobs.Count() > 0);
        }
        #endregion // Download Job Output Command

        #region Skip Job Command
        /// <summary>
        /// Determines whether the <see cref="AutomationMonitorCommands.SkipJob"/> 
        /// command can be executed based on application state.
        /// </summary>
        /// 
        /// <returns>
        /// True if the command can be executed; otherwise, false.
        /// </returns>
        public bool CanSkipSelectedJobs()
        {
            return (AreAllJobsSkippable(this.SelectedJobs));
        }

        /// <summary>
        /// Called whenever the <see cref="AutomationMonitorCommands.SkipJob"/> 
        /// command is fired and needs handling.
        /// </summary>
        /// <param name="data"></param>
        public async Tasks.Task OnSkipJobs()
        {
            foreach (JobViewModel job in this.SelectedJobs)
            {
                AutomationAdminConsumer consumer = new AutomationAdminConsumer(job.AutomationAdminService);
                consumer.SkipJob(job.Id);
            }
        }

        /// <summary>
        /// Return whether all job's log data is skippable (Pending state).
        /// </summary>
        /// <param name="jobs"></param>
        /// <returns></returns>
        public bool AreAllJobsSkippable(IEnumerable<JobViewModel> jobs)
        {
            IEnumerable<JobViewModel> pendingJobs = jobs.Where(j =>
                j.State.Equals(JobState.Pending));
            return (jobs.Count() == pendingJobs.Count() && jobs.Count() > 0);
        }
        #endregion // Skip Job Command

        /*
        #region Open Job Log Command
        /// <summary>
        /// Determines whether the <see cref="AutomationMonitorCommands.OpenJobLog"/> 
        /// command can be executed based on application state.
        /// </summary>
        /// 
        /// <returns>
        /// True if the command can be executed; otherwise, false.
        /// </returns>
        public bool CanDownloadSelectedJobsLogs()
        {
            return (AreAllJobsLogDownloadable(this.SelectedJobs));
        }

        /// <summary>
        /// Called whenever the <see cref="AutomationMonitorCommands.DownloadJobLog"/> 
        /// command is fired and needs handling.
        /// </summary>
        /// <param name="data"></param>
        public async Tasks.Task OnDownloadJobLog(IMessageBoxService msgBoxService)
        {
#warning DHM FIX ME : this is nasty - read from previously constructed config.
            RSG.Base.Configuration.IConfig config = RSG.Base.Configuration.ConfigFactory.CreateConfig();
            ICollection<Tasks.Task> tasks = new List<Tasks.Task>();
            foreach (JobViewModel job in this.SelectedJobs)
            {
                JobDownloadViewModel jobDownloadVM = new JobDownloadViewModel(config,
                    JobDownloadType.Log, job);

                if (!CheckIfJobFilesHaveBeenDownloaded(msgBoxService, JobDownloadType.Log, job.Id))
                {
                    DownloadJobs.Add(jobDownloadVM);
                    tasks.Add(jobDownloadVM.DownloadJobLogAsync());
                }
            }
            await Tasks.Task.WhenAll(tasks);
        }

        /// <summary>
        /// Return whether all job's log data is downloadable (Completed or Error states).
        /// </summary>
        /// <param name="jobs"></param>
        /// <returns></returns>
        public bool AreAllJobsLogDownloadable(IEnumerable<JobViewModel> jobs)
        {
            IEnumerable<JobViewModel> completedJobs = jobs.Where(j =>
                j.State.Equals(JobState.Completed) || j.State.Equals(JobState.Errors));
            return (jobs.Count() == completedJobs.Count());
        }
        #endregion // Download Job Log Command
        */
        #endregion // Controller Methods

        #region Protected Methods
        /// <summary>
        /// Called when services are refreshed.
        /// </summary>
        protected void OnRefreshServices()
        {

        }

        /// <summary>
        /// Called when selected service jobs and clients are refreshed.
        /// </summary>
        protected void OnRefreshSelectedService()
        {

        }
        #endregion // Protected Methods
        
        #region Private Methods
        /// <summary>
        /// Reload Automation Service config from disk.
        /// </summary>
        private void ReloadAutomationConfig()
        {
            lock (_automationLock)
                this._automationServicesConfig = ConfigFactory.CreateAutomationServicesConfig(_config.Project).Services;
        }

        /// <summary>
        /// Rebuild Project View-Model objects (from config data).
        /// </summary>
        /// <returns></returns>
        private IEnumerable<ProjectViewModel> RebuildProjectViewModels()
        {
#warning DHM FIX ME: with IConfig2 this will be expanded for multiple-ProjectViewModels.

            List<ProjectViewModel> projectVMs = new List<ProjectViewModel>();
            lock (_automationLock)
            {
                projectVMs.Add(new ProjectViewModel(this._config.Project, true, 
                    this._automationServicesConfig));
            }
            return (projectVMs);
        }
        
        /// <summary>
        /// Rebuild Job View-Model objects (from model TaskStatus objects).
        /// </summary>
        /// <param name="parameter">TaskStatus object</param>
        /// <returns></returns>
        private IEnumerable<JobViewModel> RebuildJobViewModels(Object parameter)
        {
            if (null == parameter)
                return (new JobViewModel[0]);

            Debug.Assert(parameter is TaskStatus);
            TaskStatus taskStatus = (TaskStatus)parameter;

            List<JobViewModel> jobVMs = new List<JobViewModel>();
            Uri p4web = new Uri(String.Format("http://{0}", _config.Studios.ThisStudio.PerforceWebServer));
            foreach (IJob job in taskStatus.Jobs)
            {
                IAutomationServiceConfig config = this._selectedAutomationService.AutomationService;
                jobVMs.Add(new JobViewModel(config, job, p4web, this._bugstarUsers, UseUTC));
            }
            return (jobVMs);
        }

        /// <summary>
        /// Rebuild Client View-Model objects (from model WorkerStatus objects).
        /// </summary>
        /// <param name="parameter">WorkerStatus object</param>
        /// <returns></returns>
        private static IEnumerable<ClientViewModel> RebuildClientViewModels(Object parameter)
        {
            if (null == parameter)
                return (new ClientViewModel[0]);

            Debug.Assert(parameter is IEnumerable<WorkerStatus>);
            IEnumerable<WorkerStatus> clientStatus = (IEnumerable<WorkerStatus>)parameter;

            List<ClientViewModel> clientVMs = new List<ClientViewModel>();
            foreach (WorkerStatus worker in clientStatus)
            {
                clientVMs.Add(new ClientViewModel(worker));
            }
            return (clientVMs);
        }

        /*
        /// <summary>
        /// Initialise Command Definition objects.
        /// </summary>
        private void InitCommandDefinitions()
        {
            if (null == _abortJobDefinition)
            {
                _abortJobDefinition = new AbortJobDefinition(this);
                RockstarCommandManager.AddCommandDefinition(_abortJobDefinition);
            }
            if (null == _downloadJobLogDefinition)
            {
                _downloadJobLogDefinition = new DownloadJobLogDefinition(this);
                RockstarCommandManager.AddCommandDefinition(_downloadJobLogDefinition);
            }
            if (null == _downloadJobOutputDefinition)
            {
                _downloadJobOutputDefinition = new DownloadJobOutputDefinition(this);
                RockstarCommandManager.AddCommandDefinition(_downloadJobOutputDefinition);
            }
            if (null == _prioritiseJobDefinition)
            {
                _prioritiseJobDefinition = new PrioritiseJobDefinition(this);
                RockstarCommandManager.AddCommandDefinition(_prioritiseJobDefinition);
            }
        }

        /// <summary>
        /// Add commands.
        /// </summary>
        private void AddCommands()
        {
            // Job Context-Menu
            RockstarCommandManager.AddCommandBarItem(
                new DownloadJobOutputInstance(AutomationMonitorCommandIds.JobContextMenu,
                    new Guid("6FA2EDB3-081C-440C-B1FC-962264860FFF")));
            RockstarCommandManager.AddCommandBarItem(
                new DownloadJobLogInstance(AutomationMonitorCommandIds.JobContextMenu,
                    new Guid("3231EA7F-54D8-4834-881F-A288122B1237")));
            RockstarCommandManager.AddCommandBarItem(
                new PrioritiseJobInstance(AutomationMonitorCommandIds.JobContextMenu,
                    new Guid("5733BF5E-A527-49B5-901D-92A5BE50BED5")));
            RockstarCommandManager.AddCommandBarItem(
                new AbortJobInstance(AutomationMonitorCommandIds.JobContextMenu,
                    new Guid("3F7D5405-D18C-4923-B298-6890865CE174")));
        }

        /// <summary>
        /// Refresh Command Parameters notification; for selected job changes.
        /// </summary>
        private void RefreshCommandParameters()
        {
            if (null != _abortJobDefinition)
                _abortJobDefinition.RefreshCommandParameter();
            if (null != _downloadJobLogDefinition)
                _downloadJobLogDefinition.RefreshCommandParameter();
            if (null != _downloadJobOutputDefinition)
                _downloadJobOutputDefinition.RefreshCommandParameter();
            if (null != _prioritiseJobDefinition)
                _prioritiseJobDefinition.RefreshCommandParameter();
        }
         * */
        #endregion // Private Methods

    }

} // RSG.Automation.ViewModel namespace
