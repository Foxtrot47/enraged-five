﻿//---------------------------------------------------------------------------------------------
// <copyright file="IValidationItem.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Model
{
    /// <summary>
    /// When implemented represents a item that describes a situation that has been found
    /// through validating a model object.
    /// </summary>
    public interface IValidationItem
    {
        #region Properties
        /// <summary>
        /// Gets a value indicating whether this object can provide information about the
        /// location of the validation item inside a file.
        /// </summary>
        bool HasLocationInformation { get; }

        /// <summary>
        /// Gets the file location that this error is associated with.
        /// </summary>
        FileLocation Location { get; }

        /// <summary>
        /// Gets the message that describes the error.
        /// </summary>
        string Message { get; }
        #endregion Properties
    } // RSG.Editor.Model.IValidationItem {Interface}
} // RSG.Editor.Model {Namespace}
