﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Editor.Controls.CurveEditor.Model
{
    /// <summary>
    /// 
    /// </summary>
    public enum TangentMode
    {
        /// <summary>
        /// Used to have linear interpolation at a key.
        /// </summary>
        Linear,

        /// <summary>
        /// Used to have a binary interpolation from one key t the next.
        /// </summary>
        Constant,

        /// <summary>
        /// 
        /// </summary>
        //Free,

        /// <summary>
        /// 
        /// </summary>
        Spline
    } // TangentMode
}
