﻿//---------------------------------------------------------------------------------------------
// <copyright file="CommandBarItemViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;
    using System.Windows.Input;
    using System.Windows.Media.Imaging;
    using RSG.Base.Extensions;
    using RSG.Editor.Resources;
    using RSG.Editor.SharedCommands;
    using RSG.Editor.View;

    /// <summary>
    /// Defines the view model for a <see cref="RSG.Editor.CommandBarItem"/> object.
    /// </summary>
    public class CommandBarItemViewModel : NotifyPropertyChangedBase
    {
        #region Fields
        /// <summary>
        /// A routed command that is used in the case of a missing definition. Using
        /// this command makes sure the command will not be implemented anyway and be
        /// shown as disabled.
        /// </summary>
        private static readonly RoutedCommand _emptyCommand;

        /// <summary>
        /// A generic object that provides synchronisation between multiple threads while
        /// creating and altering view models.
        /// </summary>
        private static readonly object _syncRoot;

        /// <summary>
        /// A cache that contains the created view models indexed by the parent identifier.
        /// </summary>
        private static Dictionary<Guid, CommandBarItemViewModelCollection> _creationCache;

        /// <summary>
        /// The private collections containing the identifiers to the parents whose children
        /// have had a priority change during a delayed point.
        /// </summary>
        private static List<Guid> _delayedPriorityChanges;

        /// <summary>
        /// A value indicating whether items inside a collection view model are currently
        /// being reset.
        /// </summary>
        private static bool _resettingItems;

        /// <summary>
        /// A value indicating whether there exists any view models that have an unresolved
        /// definition in them.
        /// </summary>
        private static int _unresolvedDefinitions;

        /// <summary>
        /// The private field used for the <see cref="ComboItems"/> property.
        /// </summary>
        private ComboItemsCollection _comboItems;

        /// <summary>
        /// The private field used for the <see cref="CommandBarItem"/> property.
        /// </summary>
        private CommandBarItem _commandBarItem;

        /// <summary>
        /// The private field used for the <see cref="CommandDefinition"/> property.
        /// </summary>
        private CommandDefinition _commandDefinition;

        /// <summary>
        /// The private field used for the <see cref="DisplayType"/> property.
        /// </summary>
        private CommandItemDisplayType _displayType;

        /// <summary>
        /// The private collection containing the children that represent the items inside the
        /// dynamic menu. This is due to the dynamic menu having to create the children on the
        /// fly without needing to embed them in a pre-existing list and the items don't
        /// reference the parent by id.
        /// </summary>
        private DynamicMenuChildren _dynamicChildren;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="CommandBarItemViewModel"/> class.
        /// </summary>
        static CommandBarItemViewModel()
        {
            _creationCache = new Dictionary<Guid, CommandBarItemViewModelCollection>();
            _delayedPriorityChanges = new List<Guid>();
            _syncRoot = new object();
            _emptyCommand = new RoutedCommand("Empty", typeof(CommandBarItemViewModel));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="CommandBarItemViewModel"/> class to
        /// represent the specified command bar item.
        /// </summary>
        /// <param name="item">
        /// The command bar item that this view model will be representing.
        /// </param>
        internal CommandBarItemViewModel(CommandBarItem item)
        {
            if (item == null)
            {
                this._displayType = CommandItemDisplayType.Separator;
                return;
            }

            this.CommandBarItem = item;
            CommandInstance instance = item as CommandInstance;
            if (instance != null)
            {
                ICommand command = instance.Command;
                this.CommandDefinition = RockstarCommandManager.GetDefinition(command);
                if (this.CommandDefinition == null)
                {
                    _unresolvedDefinitions++;
                    this._displayType = CommandItemDisplayType.Button;
                }
                else
                {
                    if (this._commandDefinition is ToggleButtonCommand)
                    {
                        this._displayType = CommandItemDisplayType.Toggle;
                    }
                    else if (this._commandDefinition is ButtonCommand)
                    {
                        this._displayType = CommandItemDisplayType.Button;
                    }
                    else if (this._commandDefinition is ComboCommand)
                    {
                        this._displayType = CommandItemDisplayType.Combo;
                    }
                    else if (this._commandDefinition is FilterCommand)
                    {
                        this._displayType = CommandItemDisplayType.FilterCombo;
                    }
                    else if (this._commandDefinition is SearchCommand)
                    {
                        this._displayType = CommandItemDisplayType.Search;
                    }
                    else if (this._commandDefinition is DynamicMenuCommand)
                    {
                        this._displayType = CommandItemDisplayType.DynamicMenu;
                        this.InitialiseDynamicMenu();
                    }
                    else
                    {
                        this._displayType = CommandItemDisplayType.Separator;
                    }
                }
            }
            else
            {
                if (item is CommandMenu)
                {
                    this._displayType = CommandItemDisplayType.Menu;
                }
                else if (item is CommandToolBar)
                {
                    this._displayType = CommandItemDisplayType.ToolBar;
                }
                else
                {
                    this._displayType = CommandItemDisplayType.Separator;
                }
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="CommandBarItemViewModel"/> class to
        /// represent the specified definition item.
        /// </summary>
        /// <param name="item">
        /// The multi item that this view model is being created for.
        /// </param>
        /// <param name="instance">
        /// The original command instance that is being broken into multiple items.
        /// </param>
        internal CommandBarItemViewModel(IMultiCommandItem item, CommandInstance instance)
        {
            this.CommandBarItem = new MultiCommandItemInstance(instance, item);
            MultiCommand definition =
                RockstarCommandManager.GetDefinition(instance.Command) as MultiCommand;
            this.CommandDefinition = new MultiCommandItemDefinition(item, definition);
            if (item.ParentDefinition.CanItemsBeSelected)
            {
                this._displayType = CommandItemDisplayType.Toggle;
            }
            else
            {
                this._displayType = CommandItemDisplayType.Button;
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="CommandBarItemViewModel"/> class with
        /// a dynamic item.
        /// </summary>
        /// <param name="dynamicItem">
        /// The item that is used to describe the command bar item and command definition.
        /// </param>
        private CommandBarItemViewModel(IDynamicMenuItem dynamicItem)
        {
            this.CommandBarItem = new DynamicCommandInstance(
                dynamicItem.ParentDefinition.Command,
                dynamicItem.Text,
                dynamicItem.CommandParameter);

            this._displayType = CommandItemDisplayType.Button;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets a value that indicates where the toolbar should be located in the
        /// ToolBarTray.
        /// </summary>
        public int Band
        {
            get
            {
                CommandToolBar toolbar = this._commandBarItem as CommandToolBar;
                if (toolbar == null)
                {
                    return 0;
                }

                return toolbar.Band;
            }

            set
            {
                CommandToolBar toolbar = this._commandBarItem as CommandToolBar;
                if (toolbar == null)
                {
                    return;
                }

                toolbar.Band = value;
            }
        }

        /// <summary>
        /// Gets or sets the band index number that indicates the position of the toolbar on
        /// the band.
        /// </summary>
        public int BandIndex
        {
            get
            {
                CommandToolBar toolbar = this._commandBarItem as CommandToolBar;
                if (toolbar == null)
                {
                    return 0;
                }

                return toolbar.BandIndex;
            }

            set
            {
                CommandToolBar toolbar = this._commandBarItem as CommandToolBar;
                if (toolbar == null)
                {
                    return;
                }

                toolbar.BandIndex = value;
            }
        }

        /// <summary>
        /// Gets the collection of child command view models of this view model.
        /// </summary>
        public CommandBarItemViewModelCollection Children
        {
            get
            {
                if (this._commandBarItem == null)
                {
                    return null;
                }

                return Create(this._commandBarItem.Id);
            }
        }

        /// <summary>
        /// Gets the items that can be used for a combo control.
        /// </summary>
        public ComboItemsCollection ComboItems
        {
            get
            {
                if (this._comboItems == null)
                {
                    lock (_syncRoot)
                    {
                        if (this._comboItems == null)
                        {
                            ObservableCollection<IMultiCommandItem> source = null;
                            MultiCommand command = this._commandDefinition as MultiCommand;
                            if (command == null)
                            {
                                source = new ObservableCollection<IMultiCommandItem>();
                            }
                            else
                            {
                                source = command.Items;
                            }

                            this._comboItems = new ComboItemsCollection(source, this);
                        }
                    }
                }

                return this._comboItems;
            }
        }

        /// <summary>
        /// Gets the command that the associated definition is wrapping.
        /// </summary>
        public ICommand Command
        {
            get
            {
                if (this._commandDefinition != null)
                {
                    return this._commandDefinition.Command;
                }
                else
                {
                    CommandInstance instance = this._commandBarItem as CommandInstance;
                    if (instance != null)
                    {
                        return instance.Command;
                    }
                }

                return _emptyCommand;
            }
        }

        /// <summary>
        /// Gets the command bar item that is the part of the associated data for this view
        /// model.
        /// </summary>
        public CommandBarItem CommandBarItem
        {
            get
            {
                return this._commandBarItem;
            }

            private set
            {
                if (this._commandBarItem != null)
                {
                    PropertyChangedEventManager.RemoveHandler(
                        this._commandBarItem, this.SourcePropertyChanged, String.Empty);
                }

                this._commandBarItem = value;
                if (this._commandBarItem != null)
                {
                    PropertyChangedEventManager.AddHandler(
                        this._commandBarItem, this.SourcePropertyChanged, String.Empty);
                }
            }
        }

        /// <summary>
        /// Gets the command definition that is the part of the associated data for this view
        /// model.
        /// </summary>
        public CommandDefinition CommandDefinition
        {
            get
            {
                return this._commandDefinition;
            }

            private set
            {
                if (this._commandDefinition != null)
                {
                    PropertyChangedEventManager.RemoveHandler(
                        this._commandDefinition, this.SourcePropertyChanged, String.Empty);
                }

                this._commandDefinition = value;
                if (this._commandDefinition != null)
                {
                    PropertyChangedEventManager.AddHandler(
                        this._commandDefinition, this.SourcePropertyChanged, String.Empty);
                }
            }
        }

        /// <summary>
        /// Gets the parameter that is routed through the element tree with the command.
        /// </summary>
        public object CommandParameter
        {
            get
            {
                ICommandWithParameter parameterInstance =
                    this._commandBarItem as ICommandWithParameter;
                if (parameterInstance != null)
                {
                    return parameterInstance.CommandParameter;
                }

                ICommandWithParameter command =
                    this._commandDefinition as ICommandWithParameter;
                if (command != null)
                {
                    return command.CommandParameter;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets or sets a value that can be used to change how the control that is used to
        /// display this item gets styled.
        /// </summary>
        public CommandItemDisplayStyle DisplayStyle
        {
            get
            {
                CommandInstance instance = this._commandBarItem as CommandInstance;
                if (instance != null)
                {
                    if (this._commandDefinition == null && this.Icon == null)
                    {
                        return CommandItemDisplayStyle.TextOnly;
                    }

                    return instance.DisplayStyle;
                }

                return CommandItemDisplayStyle.Default;
            }

            set
            {
                CommandInstance instance = this._commandBarItem as CommandInstance;
                if (instance == null)
                {
                    return;
                }

                instance.DisplayStyle = value;
            }
        }

        /// <summary>
        /// Gets the display type for this command bar item view model which specifies how the
        /// command should be rendered in the command hierarchy.
        /// </summary>
        public CommandItemDisplayType DisplayType
        {
            get { return this._displayType; }
        }

        /// <summary>
        /// Gets the read-only collection containing the dynamic menu child items.
        /// </summary>
        public IReadOnlyViewModelCollection<CommandBarItemViewModel> DynamicChildren
        {
            get { return this._dynamicChildren; }
        }

        /// <summary>
        /// Gets the filter count string to show for a filter command.
        /// </summary>
        public string FilterCount
        {
            get
            {
                FilterCommand command = this._commandDefinition as FilterCommand;
                if (command == null)
                {
                    return null;
                }

                string fmt = "({0} of {1})";
                int totalCount = 0;
                int checkCount = 0;
                foreach (var viewModel in this.ComboItems)
                {
                    totalCount++;
                    if (viewModel.IsToggled)
                    {
                        checkCount++;
                    }
                }

                return fmt.FormatCurrent(checkCount, totalCount);
            }
        }

        /// <summary>
        /// Gets the System.Windows.Media.Imaging.BitmapSource object that represents the
        /// command icon.
        /// </summary>
        public BitmapSource Icon
        {
            get
            {
                ICommandWithIcon iconInstance = this._commandBarItem as ICommandWithIcon;
                if (iconInstance != null)
                {
                    return iconInstance.Icon;
                }
                else if (this._commandDefinition != null)
                {
                    return this._commandDefinition.Icon;
                }
                else
                {
                    CommandInstance instance = this._commandBarItem as CommandInstance;
                    if (instance != null)
                    {
                        return instance.Command.Icon;
                    }
                }

                return null;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this command bar item is currently enabled.
        /// </summary>
        public bool IsEnabled
        {
            get
            {
                CommandMenu menu = this._commandBarItem as CommandMenu;
                if (menu == null)
                {
                    if (this._commandDefinition == null)
                    {
                        return true;
                    }

                    return this._commandDefinition.IsEnabled;
                }

                return this.Children.Count > 0;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the command definition is currently in the
        /// toggled state or not.
        /// </summary>
        public bool IsToggled
        {
            get
            {
                ICommandWithToggle command = this._commandDefinition as ICommandWithToggle;
                if (command != null)
                {
                    return command.IsToggled;
                }

                return false;
            }

            set
            {
                ICommandWithToggle command = this._commandDefinition as ICommandWithToggle;
                if (command != null)
                {
                    command.IsToggled = value;
                }
            }
        }

        /// <summary>
        /// Gets the text that contains the key gesture that is attached to this command bar
        /// item.
        /// </summary>
        public string KeyGestureText
        {
            get
            {
                KeyGesture keyGesture = null;
                if (this._commandDefinition != null)
                {
                    keyGesture = this._commandDefinition.KeyGesture;
                }

                if (keyGesture != null)
                {
                    KeyGestureConverter converter = new KeyGestureConverter();
                    string text = converter.ConvertToInvariantString(keyGesture);
                    if (String.Equals(text, "Delete"))
                    {
                        return "Del";
                    }

                    text = text.Replace("Return", "Enter");
                    return text;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the collection defining the different scopes for the command.
        /// </summary>
        public ObservableCollection<string> Scopes
        {
            get
            {
                ICommandWithScopes command = this._commandDefinition as ICommandWithScopes;
                if (command == null)
                {
                    return new ObservableCollection<string>();
                }

                return command.Scopes;
            }
        }

        /// <summary>
        /// Gets the search parameters from the command definition if a conversion is possible;
        /// otherwise, returns null.
        /// </summary>
        public ICommandWithSearchParameters SearchParameters
        {
            get
            {
                ICommandWithSearchParameters parameters =
                    this._commandDefinition as ICommandWithSearchParameters;
                if (parameters == null)
                {
                    return null;
                }

                return parameters;
            }
        }

        /// <summary>
        /// Gets or sets the item that is currently selected in the combo control.
        /// </summary>
        public IMultiCommandItem SelectedItem
        {
            get
            {
                MultiCommand multiCommand = this._commandDefinition as MultiCommand;
                if (multiCommand == null)
                {
                    return null;
                }

                MultiCommandItemViewModel value = multiCommand.SelectedItem as MultiCommandItemViewModel;
                if (value != null)
                {
                    return value;
                }

                foreach (MultiCommandItemViewModel vm in this.ComboItems)
                {
                    if (object.ReferenceEquals(vm.Model, multiCommand.SelectedItem))
                    {
                        return vm;
                    }
                }

                return null;
            }

            set
            {
                MultiCommand multiCommand = this._commandDefinition as MultiCommand;
                if (multiCommand == null)
                {
                    return;
                }

                MultiCommandItemViewModel vm = value as MultiCommandItemViewModel;
                if (vm == null)
                {
                    throw new NotSupportedException("This should be bound to a combobox using combo items as its source");
                }

                multiCommand.SelectedItem = vm.Model;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this command item starts a new group.
        /// </summary>
        public bool StartsGroup
        {
            get
            {
                if (this.CommandBarItem == null)
                {
                    return false;
                }

                return this._commandBarItem.StartsGroup;
            }

            set
            {
                if (this.CommandBarItem == null)
                {
                    return;
                }

                this._commandBarItem.StartsGroup = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the filter check count is shown to the
        /// user in the user interface.
        /// </summary>
        public bool ShowFilterCount
        {
            get
            {
                if (this._commandBarItem == null)
                {
                    return false;
                }

                return this._commandBarItem.ShowFilterCount;
            }

            set
            {
                if (this._commandBarItem == null)
                {
                    return;
                }

                this._commandBarItem.ShowFilterCount = value;
            }
        }

        /// <summary>
        /// Gets or sets the text that appears with this command bar item.
        /// </summary>
        public string Text
        {
            get
            {
                if (this.CommandBarItem == null)
                {
                    return null;
                }

                return this._commandBarItem.Text;
            }

            set
            {
                if (this.CommandBarItem == null)
                {
                    return;
                }

                this._commandBarItem.Text = value;
            }
        }

        /// <summary>
        /// Gets the tooltip that will be shown for this command bar item.
        /// </summary>
        public string ToolTip
        {
            get
            {
                string core = null;
                if (this._commandDefinition != null)
                {
                    core = this._commandDefinition.Description;
                }

                if (this._commandBarItem != null)
                {
                    core = this._commandBarItem.Text;
                }

                if (core == null)
                {
                    return null;
                }

                if (!(this._commandDefinition is MultiCommand))
                {
                    string keyGestureText = this.KeyGestureText;
                    if (!String.IsNullOrWhiteSpace(keyGestureText))
                    {
                        return StringTable.ToolTipTemplate.FormatCurrent(core, keyGestureText);
                    }
                }

                return core;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this command bar item is visible to the
        /// user.
        /// </summary>
        public bool IsVisible
        {
            get { return this._commandBarItem.IsVisible; }
            set { this._commandBarItem.IsVisible = value; }
        }

        /// <summary>
        /// Gets the width of the control attached to this view model.
        /// </summary>
        public double Width
        {
            get
            {
                if (this._commandBarItem != null)
                {
                    return this._commandBarItem.Width;
                }

                return 160;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Retrieves a collection of view models that wrap command bar items whose parent id
        /// property equals the specified id.
        /// </summary>
        /// <param name="id">
        /// The identifier of the parent whose items should have their corresponding view
        /// models retrieved.
        /// </param>
        /// <returns>
        /// A collection of view models that wrap command bar items whose parent id property
        /// equals the specified id.
        /// </returns>
        public static CommandBarItemViewModelCollection Create(Guid id)
        {
            if (id == Guid.Empty)
            {
                return null;
            }

            lock (_syncRoot)
            {
                CommandBarItemViewModelCollection viewModels = null;
                if (_creationCache.TryGetValue(id, out viewModels))
                {
                    return viewModels;
                }

                viewModels = new CommandBarItemViewModelCollection(id);
                viewModels.Seal();
                _creationCache.Add(id, viewModels);
                CollectionChangedEventManager.AddHandler(viewModels, OnChildrenChanged);
                return viewModels;
            }
        }

        /// <summary>
        /// Commits the currently delayed priority changes by resetting the necessary
        /// collections.
        /// </summary>
        internal static void CommitPriorityChanges()
        {
            foreach (Guid id in _delayedPriorityChanges)
            {
                ResetItems(id);
            }
        }

        /// <summary>
        /// Called whenever a command definition is added to the command manager.
        /// </summary>
        /// <param name="definition">
        /// The definition that has been added to the command manager.
        /// </param>
        internal static void OnDefinitionAdded(CommandDefinition definition)
        {
            lock (_syncRoot)
            {
                if (_unresolvedDefinitions == 0)
                {
                    return;
                }

                foreach (var viewModelCollection in _creationCache.Values)
                {
                    bool resetItems = false;
                    foreach (var viewModel in viewModelCollection)
                    {
                        if (viewModel.ResolveDefinition(definition))
                        {
                            resetItems = true;
                        }
                    }

                    if (resetItems)
                    {
                        // Best thing to do is reset the items as the display
                        // type will have changed and is not done through binding.
                        try
                        {
                            _resettingItems = true;
                            viewModelCollection.Unseal();
                            viewModelCollection.ResetItems(true, true);
                            viewModelCollection.Seal();
                        }
                        finally
                        {
                            _resettingItems = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Called whenever a item is added to the command manager.
        /// </summary>
        /// <param name="addedItem">
        /// The item that has been added to the command manager.
        /// </param>
        internal static void OnItemAdded(CommandBarItem addedItem)
        {
            lock (_syncRoot)
            {
                Guid id = addedItem.ParentId;
                CommandBarItemViewModelCollection viewModels = null;
                if (!_creationCache.TryGetValue(id, out viewModels))
                {
                    return;
                }

                List<CommandBarItem> items = RockstarCommandManager.GetCommands(id).ToList();
                int newIndex = items.IndexOf(addedItem);
                if (newIndex == -1)
                {
                    return;
                }

                if (newIndex != 0)
                {
                    newIndex = viewModels.GetIndexAfter(items[newIndex - 1]);
                }

                viewModels.Unseal();
                viewModels.Insert(newIndex, addedItem);
                viewModels.Seal();

                CommandBarItemViewModel parent = (from c in _creationCache
                                                  from vm in c.Value
                                                  where vm != null &&
                                                  vm._commandBarItem != null &&
                                                  vm._commandBarItem.Id == id
                                                  select vm).FirstOrDefault();
                if (parent != null)
                {
                    parent.NotifyPropertyChanged("IsEnabled");
                }
            }
        }

        /// <summary>
        /// Called whenever a item is removed from the command manager.
        /// </summary>
        /// <param name="removedItem">
        /// The item that has been removed from to the command manager.
        /// </param>
        /// <param name="sendEvent">
        /// A value indicating whether the property changed event is fired for the
        /// <see cref="IsEnabled"/> property.
        /// </param>
        internal static void OnItemRemoved(CommandBarItem removedItem, bool sendEvent)
        {
            lock (_syncRoot)
            {
                CommandBarItemViewModelCollection viewModels = null;
                if (_creationCache.TryGetValue(removedItem.ParentId, out viewModels))
                {
                    viewModels.Unseal();
                    viewModels.Remove(removedItem);
                    viewModels.Seal();
                }

                CommandBarItemViewModelCollection childViewModels = null;
                if (_creationCache.TryGetValue(removedItem.Id, out childViewModels))
                {
                    _creationCache.Remove(removedItem.Id);
                    CollectionChangedEventManager.AddHandler(
                        childViewModels, OnChildrenChanged);

                    foreach (CommandBarItemViewModel childViewModel in childViewModels)
                    {
                        if (childViewModel != null && childViewModel.CommandBarItem != null)
                        {
                            OnItemRemoved(childViewModel.CommandBarItem, false);
                        }
                    }

                    childViewModels.Unseal();
                    childViewModels.Clear();
                    childViewModels.Seal();
                }

                if (!sendEvent)
                {
                    return;
                }

                CommandBarItemViewModel parent = (from c in _creationCache
                                                  from vm in c.Value
                                                  where vm != null &&
                                                  vm._commandBarItem != null &&
                                                  vm._commandBarItem.Id == removedItem.ParentId
                                                  select vm).FirstOrDefault();

                if (parent != null)
                {
                    parent.NotifyPropertyChanged("IsEnabled");
                }
            }
        }

        /// <summary>
        /// Called whenever a change is made to one of the child collections.
        /// </summary>
        /// <param name="s">
        /// The collection that has changed.
        /// </param>
        /// <param name="e">
        /// The System.Collections.Specialized.NotifyCollectionChangedEventArgs data for the
        /// event.
        /// </param>
        private static void OnChildrenChanged(object s, NotifyCollectionChangedEventArgs e)
        {
            CommandBarItemViewModelCollection collection =
                s as CommandBarItemViewModelCollection;
            if (collection == null)
            {
                return;
            }

            CommandBarItemViewModel parent = (from c in _creationCache
                                              from vm in c.Value
                                              where vm != null &&
                                              vm._commandBarItem != null &&
                                              vm._commandBarItem.Id == collection.Id
                                              select vm).FirstOrDefault();

            if (parent != null)
            {
                parent.NotifyPropertyChanged("IsEnabled");
            }
        }

        /// <summary>
        /// Called whenever the priority value on a command bar item has been changed.
        /// </summary>
        /// <param name="item">
        /// The item that has had its priority value changed.
        /// </param>
        private static void OnPriorityChanged(CommandBarItem item)
        {
            lock (_syncRoot)
            {
                if (RockstarCommandManager.DelayingPriorityChanges)
                {
                    if (!_delayedPriorityChanges.Contains(item.ParentId))
                    {
                        _delayedPriorityChanges.Add(item.ParentId);
                    }

                    return;
                }

                if (_resettingItems)
                {
                    return;
                }

                throw new NotSupportedException(
                    "Unable to change a priority outside of a delay block");
            }
        }

        /// <summary>
        /// Called whenever the start group flag on a command bar item has been changed.
        /// </summary>
        /// <param name="item">
        /// The item that has had its starts group flag changed.
        /// </param>
        private static void OnStartsGroupChanged(CommandBarItem item)
        {
            lock (_syncRoot)
            {
                CommandBarItemViewModelCollection viewModels = null;
                if (!_creationCache.TryGetValue(item.ParentId, out viewModels))
                {
                    return;
                }

                int index = viewModels.IndexOf(item);
                if (index == -1 || index == 0)
                {
                    return;
                }

                if (item.StartsGroup)
                {
                    viewModels.Unseal();
                    viewModels.AddSeparator(index);
                    viewModels.Seal();
                }
                else
                {
                    viewModels.Unseal();
                    viewModels.RemoveSeparator(index - 1);
                    viewModels.Seal();
                }
            }
        }

        /// <summary>
        /// Resets the collection for the parent with the specified identifier.
        /// </summary>
        /// <param name="id">
        /// The identifier of the parent whose collection needs to be reset.
        /// </param>
        private static void ResetItems(Guid id)
        {
            CommandBarItemViewModelCollection viewModels = null;
            if (!_creationCache.TryGetValue(id, out viewModels))
            {
                return;
            }

            try
            {
                _resettingItems = true;
                viewModels.Unseal();
                viewModels.ResetItems(true, true);
                viewModels.Seal();
            }
            finally
            {
                _resettingItems = false;
            }
        }

        /// <summary>
        /// Initialises a dynamic menu item by binding this view models children with the items
        /// on the dynamic menu.
        /// </summary>
        private void InitialiseDynamicMenu()
        {
            DynamicMenuCommand command = this._commandDefinition as DynamicMenuCommand;
            if (command == null)
            {
                Debug.Assert(
                    false, "Unable to initialise a dynamic menu without a dynamic menu.");
                return;
            }

            this._dynamicChildren = new DynamicMenuChildren(command.Items);
        }

        /// <summary>
        /// Called whenever a new command definition is added to the command
        /// manager in-case this view model has a unresolved command definition.
        /// </summary>
        /// <param name="definition">
        /// The command definition that has been added to the command manager.
        /// </param>
        /// <returns>
        /// A value indicating whether the definition is now resolved.
        /// </returns>
        private bool ResolveDefinition(CommandDefinition definition)
        {
            if (this._commandDefinition != null)
            {
                return false;
            }

            CommandInstance instance = this._commandBarItem as CommandInstance;
            if (instance == null)
            {
                return false;
            }

            ICommand command = instance.Command;
            if (!Object.ReferenceEquals(definition.Command, command))
            {
                return false;
            }

            _unresolvedDefinitions--;
            this.CommandDefinition = definition;
            this.NotifyPropertyChanged("Icon");
            this.NotifyPropertyChanged("Command");
            if (this._commandDefinition is ToggleButtonCommand)
            {
                this._displayType = CommandItemDisplayType.Toggle;
            }
            else if (this._commandDefinition is ButtonCommand)
            {
                this._displayType = CommandItemDisplayType.Button;
            }
            else if (this._commandDefinition is ComboCommand)
            {
                this._displayType = CommandItemDisplayType.Combo;
            }
            else if (this._commandDefinition is FilterCommand)
            {
                this._displayType = CommandItemDisplayType.FilterCombo;
            }
            else if (this._commandDefinition is SearchCommand)
            {
                this._displayType = CommandItemDisplayType.Search;
            }
            else if (this._commandDefinition is DynamicMenuCommand)
            {
                this._displayType = CommandItemDisplayType.DynamicMenu;
                this.InitialiseDynamicMenu();
            }
            else
            {
                this._displayType = CommandItemDisplayType.Separator;
            }

            return true;
        }

        /// <summary>
        /// Gets called whenever a property changed event occurs from the associated command
        /// bar item or the associated command definition.
        /// </summary>
        /// <param name="sender">
        /// The command bar item that fired the event.
        /// </param>
        /// <param name="e">
        /// The event data including the name of the property that changed.
        /// </param>
        private void SourcePropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            CommandBarItem item = sender as CommandBarItem;
            string propertyName = e.PropertyName;
            if (item != null)
            {
                if (propertyName == "StartsGroup")
                {
                    OnStartsGroupChanged(item);
                }

                if (propertyName == "Priority")
                {
                    OnPriorityChanged(item);
                }

                propertyName = propertyName.Replace("Override", String.Empty);
            }

            if (propertyName == "Description")
            {
                propertyName = "ToolTip";
            }

            this.NotifyPropertyChanged(propertyName);
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// Represents the collection of command bar item view model objects that are kept in
        /// sync with a collection of dynamic menu items.
        /// </summary>
        private class DynamicMenuChildren
             : CollectionConverter<IDynamicMenuItem, CommandBarItemViewModel>,
            IReadOnlyViewModelCollection<CommandBarItemViewModel>
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="DynamicMenuChildren"/> class.
            /// </summary>
            /// <param name="source">
            /// The source items for this collection.
            /// </param>
            public DynamicMenuChildren(ObservableCollection<IDynamicMenuItem> source)
                : base(source)
            {
            }
            #endregion Constructors

            #region Methods
            /// <summary>
            /// Creates a command bar item view model from the specified multi command item.
            /// </summary>
            /// <param name="item">
            /// The multi command item object to use to construct the new view model.
            /// </param>
            /// <returns>
            /// A new command bar item view model constructed from the specified item.
            /// </returns>
            protected override CommandBarItemViewModel ConvertItem(IDynamicMenuItem item)
            {
                return new CommandBarItemViewModel(item);
            }
            #endregion Methods
        } // CommandBarItemViewModel.DynamicMenuChildren {Class}

        /// <summary>
        /// The command instance that is created to represent a dynamic menu command item.
        /// </summary>
        private class DynamicCommandInstance : CommandInstance, ICommandWithParameter
        {
            #region Fields
            /// <summary>
            /// The private field used for the <see cref="CommandParameter"/> property.
            /// </summary>
            private object _commandParameter;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="DynamicCommandInstance"/> class.
            /// </summary>
            /// <param name="command">
            /// The command that this command bar item is instancing.
            /// </param>
            /// <param name="text">
            /// The text for this command bar item.
            /// </param>
            /// <param name="commandParameter">
            /// The command parameter that should be sent through the UI element to the
            /// execution logic.
            /// </param>
            public DynamicCommandInstance(
                RockstarRoutedCommand command,
                string text,
                object commandParameter)
                : base(command, 0, false, text, Guid.Empty, Guid.Empty)
            {
                this._commandParameter = commandParameter;
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets the parameter that is routed through the element tree with the command.
            /// </summary>
            public object CommandParameter
            {
                get { return this._commandParameter; }
            }
            #endregion Properties
        } // CommandBarItemViewModel.DynamicCommandInstance {Class}
        #endregion
    } // RSG.Editor.CommandBarItemViewModel {Class}
} // RSG.Editor {Namespace}
