﻿//---------------------------------------------------------------------------------------------
// <copyright file="IViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    using System.ComponentModel;
    using RSG.Editor.Model;

    /// <summary>
    /// Represents a generic view model that can be used in binding to the view.
    /// </summary>
    public interface IViewModel : IDisplayItem, INotifyPropertyChanged
    {
        #region Properties
        /// <summary>
        /// Gets the model that this view model is currently wrapping.
        /// </summary>
        IModel Model { get; }
        #endregion Properties
    } // RSG.Editor.View.IViewModel {Interface}
} // RSG.Editor.View {Namespace}
