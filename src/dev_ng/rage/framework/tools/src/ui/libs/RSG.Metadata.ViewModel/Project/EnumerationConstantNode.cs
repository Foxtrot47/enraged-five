﻿//---------------------------------------------------------------------------------------------
// <copyright file="EnumerationConstantNode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Project
{
    using RSG.Metadata.Model.Definitions;
    using RSG.Project.ViewModel;

    /// <summary>
    /// The node inside the project explorer that represents a constant of a parCodeGen
    /// enumeration definition. This class cannot be inherited.
    /// </summary>
    public sealed class EnumerationConstantNode : HierarchyNode
    {
        #region Fields
        /// <summary>
        /// The enumeration constant that this node is representing.
        /// </summary>
        private IEnumConstant _constant;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="EnumerationConstantNode"/> class.
        /// </summary>
        /// <param name="constant">
        /// The enumeration constant object this node is representing.
        /// </param>
        /// <param name="parent">
        /// The parent node.
        /// </param>
        public EnumerationConstantNode(IEnumConstant constant, IHierarchyNode parent)
            : base(false)
        {
            this.IsExpandable = false;
            this._constant = constant;
            this.Parent = parent;
            this.Icon = Icons.EnumMemberNodeIcon;
            this.Text = constant.Name;
        }
        #endregion Constructors
    } // RSG.Metadata.ViewModel.Project.EnumerationConstantNode {Class}
} // RSG.Metadata.ViewModel.Project {Namespace}
