﻿//---------------------------------------------------------------------------------------------
// <copyright file="Searcher.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Search
{
    using RSG.Editor.Model;

    /// <summary>
    /// Provides a base class for the type specific searchers that can be used by the model.
    /// </summary>
    public class Searcher : ModelBase
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Condition"/> property.
        /// </summary>
        private SearchCondition _condition;

        /// <summary>
        /// The private field used for the <see cref="SearchTerm"/> property.
        /// </summary>
        private string _searchTerm;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Searcher"/> class.
        /// </summary>
        public Searcher()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the search condition for the value.
        /// </summary>
        public SearchCondition Condition
        {
            get { return this._condition; }
            set { this.SetProperty(ref this._condition, value); }
        }

        /// <summary>
        /// Gets or sets the search condition for the value.
        /// </summary>
        public string SearchTerm
        {
            get { return this._searchTerm; }
            set { this.SetProperty(ref this._searchTerm, value); }
        }
        #endregion Properties
    } // RSG.Editor.Controls.Search.Searcher {Class}
} // RSG.Editor.Controls.Search {Namespace}
