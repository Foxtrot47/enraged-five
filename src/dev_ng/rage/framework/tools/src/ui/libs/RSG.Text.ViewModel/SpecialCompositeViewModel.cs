﻿//---------------------------------------------------------------------------------------------
// <copyright file="SpecialCompositeViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.ViewModel
{
    using System.Collections.Generic;
    using System.Linq;
    using RSG.Text.Model;

    /// <summary>
    /// A composite class containing n-number of <see cref="DialogueSpecialViewModel"/>
    /// objects. This class cannot be inherited.
    /// </summary>
    public sealed class SpecialCompositeViewModel : CompositeViewModel
    {
        #region Fields
        /// <summary>
        /// The private collection containing the <see cref="DialogueSpecialViewModel"/>
        /// objects that are currently managed by this composite class.
        /// </summary>
        private List<DialogueSpecialViewModel> _items;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SpecialCompositeViewModel"/> class.
        /// </summary>
        /// <param name="configurations">
        /// The <see cref="DialogueConfigurations"/> object that the configuration values have
        /// come from.
        /// </param>
        /// <param name="items">
        /// The collection of <see cref="DialogueSpecialViewModel"/> objects that this
        /// composite object should manage.
        /// </param>
        public SpecialCompositeViewModel(
            DialogueConfigurations configurations,
            IEnumerable<DialogueSpecialViewModel> items)
            : base(configurations)
        {
            this._items = new List<DialogueSpecialViewModel>(items);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the collective headset flag for all of the dialogue special objects
        /// managed by this class.
        /// </summary>
        public bool? Headset
        {
            get
            {
                if (this._items == null || this._items.Count == 0)
                {
                    return null;
                }

                IEnumerable<bool> flags = this._items.Select(x => x.HasHeadsetFlag).Distinct();
                if (flags.Count() == 1)
                {
                    return flags.First();
                }

                return null;
            }

            set
            {
                if (this._items == null)
                {
                    return;
                }

                foreach (DialogueSpecialViewModel item in this._items)
                {
                    item.HasHeadsetFlag = value.Value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the collective pad speaker flag for all of the dialogue special
        /// objects managed by this class.
        /// </summary>
        public bool? PadSpeaker
        {
            get
            {
                if (this._items == null || this._items.Count == 0)
                {
                    return null;
                }

                IEnumerable<bool> flags =
                    this._items.Select(x => x.HasPadSpeakerFlag).Distinct();
                if (flags.Count() == 1)
                {
                    return flags.First();
                }

                return null;
            }

            set
            {
                if (this._items == null)
                {
                    return;
                }

                foreach (DialogueSpecialViewModel item in this._items)
                {
                    item.HasPadSpeakerFlag = value.Value;
                }
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Deletes all of the values managed by this composite collection from the dialogue
        /// configuration object.
        /// </summary>
        public override void Delete()
        {
            List<DialogueSpecial> delete = new List<DialogueSpecial>();
            foreach (DialogueSpecialViewModel viewModel in this._items)
            {
                foreach (DialogueSpecial special in this.Model.Specials)
                {
                    if (viewModel.Id == special.Id)
                    {
                        delete.Add(special);
                    }
                }
            }

            foreach (DialogueSpecial special in delete)
            {
                this.Model.Specials.Remove(special);
            }
        }
        #endregion Methods
    } // RSG.Text.ViewModel.SpecialCompositeViewModel {Class}
} // RSG.Text.ViewModel {Namespace}
