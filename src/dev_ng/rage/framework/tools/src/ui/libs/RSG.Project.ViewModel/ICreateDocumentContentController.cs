﻿//---------------------------------------------------------------------------------------------
// <copyright file="ICreatesDocumentContentController.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel
{
    /// <summary>
    /// When implemented represents a class that can be used to create the document content
    /// for a specified object.
    /// </summary>
    public interface ICreateDocumentContentController
    {
        #region Methods
        /// <summary>
        /// Creates content to be used inside a document for the specified hierarchy node.
        /// </summary>
        /// <param name="node">
        /// The node whose content will be created.
        /// </param>
        /// <param name="e">
        /// A structure containing objects that can be used during construction, i.e. a log
        /// object to log errors and warnings to and the view site that the document is being
        /// opened in.
        /// </param>
        /// <returns>
        /// The object that represents the document content for the specified node if created;
        /// otherwise, null.
        /// </returns>
        /// <remarks>
        /// If for some reason the document shouldn't be created throw a
        /// <see cref="System.OperationCanceledException"/>.
        /// </remarks>
        object CreateContent(HierarchyNode node, CreateContentArgs e);
        #endregion Methods
    } // RSG.Project.ViewModel.ICreatesDocumentContentController {Interface}
} // RSG.Project.ViewModel {Namespace}
