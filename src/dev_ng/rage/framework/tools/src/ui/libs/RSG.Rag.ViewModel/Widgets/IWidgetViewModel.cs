﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.ViewModel.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    public interface IWidgetViewModel : IComparable<IWidgetViewModel>
    {
        /// <summary>
        /// Name of the widget.
        /// </summary>
        String Name { get; }
    } // IWidgetViewModel
}
