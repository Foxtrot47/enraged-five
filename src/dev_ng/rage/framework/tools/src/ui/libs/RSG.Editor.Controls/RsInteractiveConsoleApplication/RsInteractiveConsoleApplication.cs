﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsInteractiveConsoleApplication.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using RSG.Base.Logging;
    using RSG.Base.OS;

    /// <summary>
    /// Provides an abstract base class for a interactive console based application that
    /// supports executing commands at an interactive console as well as displaying editor
    /// framework based windows.
    /// </summary>
    public abstract class RsInteractiveConsoleApplication : RsConsoleApplication
    {
        #region Fields
        /// <summary>
        /// Log context for use when logging command messages.
        /// </summary>
        protected const string CommandLogCtx = "Command";

        /// <summary>
        /// List of commands that this application supports.
        /// </summary>
        private readonly IDictionary<string, ConsoleCommand> _commands =
            new SortedDictionary<string, ConsoleCommand>();
        #endregion Fields

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="RsInteractiveConsoleApplication"/>
        /// class.
        /// </summary>
        public RsInteractiveConsoleApplication()
            : base()
        {
            // Register the supported command line args.
            this.RegisterCommandlineArg(
                "script",
                LongOption.ArgType.Required,
                "Command script filename to execute.");

            this.RegisterCommandlineArg(
                "command",
                LongOption.ArgType.Required,
                "Single command to run prior to exiting.");

            // Register the built in commands we wish to support.
            this.RegisterConsoleCommand(
                "help", this.HelpCommandHandler, "Display this command/help information.");
            this.RegisterConsoleCommand("exit", null, "Exit console.");
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Gets the text to display in the consoles caret.
        /// </summary>
        protected virtual string CaretText
        {
            get { return "?"; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Runs the console application's core logic.
        /// </summary>
        /// <returns>
        /// The exit code for the console application.
        /// </returns>
        protected override int ConsoleMain()
        {
            // Determine if we have a command-line script to execute.
            if (CommandOptions.ContainsOption("script"))
            {
                string scriptFilename = (string)CommandOptions["script"];
                this.ProcessScript(scriptFilename);
            }
            else if (CommandOptions.ContainsOption("command"))
            {
                string command = (string)CommandOptions["command"];
                this.HandleCommand(command);
            }
            else
            {
                this.ProcessConsoleInput();
            }

            // TODO: Determine return code properly.
            return 0;
        }

        /// <summary>
        /// Help command handler.
        /// </summary>
        /// <param name="command">
        /// The command that is being handled by this method.
        /// </param>
        /// <param name="arguments">
        /// The arguments that have be sent with the command.
        /// </param>
        protected void HelpCommandHandler(string command, string[] arguments)
        {
            Log.MessageCtx(CommandLogCtx, "Available console commands:");

            int longestCommandLen = this._commands.Keys.Max(cmd => cmd.Length);
            foreach (ConsoleCommand cmd in this._commands.Values)
            {
                Log.MessageCtx(
                    CommandLogCtx,
                    "{0}\t{1}",
                    cmd.Key.PadRight(longestCommandLen),
                    cmd.HelpInfo);
            }
        }

        /// <summary>
        /// Pretty-print a table of information; alternating console background and foreground
        /// colours.
        /// </summary>
        /// <typeparam name="T">
        /// The type of objects that are being printed out by this method.
        /// </typeparam>
        /// <param name="header">
        /// The header for the table.
        /// </param>
        /// <param name="objects">
        /// The collection of objects to print out.
        /// </param>
        /// <param name="formatter">
        /// The action that takes a single specified object and formats it for the table.
        /// </param>
        protected void PrettyPrintTable<T>(
            string header, IEnumerable<T> objects, Action<T> formatter)
        {
            ConsoleColor startFgCol = System.Console.ForegroundColor;
            ConsoleColor startBgCol = System.Console.BackgroundColor;
            try
            {
                ConsoleColor backgroundColour = ConsoleColor.DarkGray;
                ConsoleColor foregroundColour = ConsoleColor.Gray;
                System.Console.WriteLine(header);
                int n = 0;
                foreach (T o in objects)
                {
                    System.Console.ForegroundColor = foregroundColour;
                    System.Console.BackgroundColor = backgroundColour;
                    formatter.Invoke(o);

                    if (0 == n % 2)
                    {
                        foregroundColour = ConsoleColor.DarkGray;
                        backgroundColour = ConsoleColor.Gray;
                    }
                    else
                    {
                        backgroundColour = ConsoleColor.DarkGray;
                        foregroundColour = ConsoleColor.Gray;
                    }
                    ++n;
                }
            }
            finally
            {
                // Restore.
                System.Console.ForegroundColor = startFgCol;
                System.Console.BackgroundColor = startBgCol;
            }
        }

        /// <summary>
        /// Registers a new console command with the specified arguments.
        /// </summary>
        /// <param name="key">
        /// The key used to identify the command.
        /// </param>
        /// <param name="handler">
        /// The handler to invoke when the command is encountered.
        /// </param>
        /// <param name="helpInfo">
        /// The help information about the command.
        /// </param>
        protected void RegisterConsoleCommand(
            string key, Action<string, string[]> handler, string helpInfo)
        {
            this._commands.Add(key, new ConsoleCommand(key, handler, helpInfo));
        }

        /// <summary>
        /// Command not implemented handler.
        /// </summary>
        /// <param name="command">
        /// The command that is being handled by this method.
        /// </param>
        /// <param name="arguments">
        /// The arguments that have be sent with the command.
        /// </param>
        private void CommandNotImplementedHandler(string command, string[] arguments)
        {
            Log.ErrorCtx(
                CommandLogCtx,
                "Command '{0}' ({1} arguments) not implemented.  Ignoring.",
                command,
                arguments.Length);
        }

        /// <summary>
        /// Handle processing of a unparsed command String.
        /// </summary>
        /// <param name="commandString">
        /// The command string.
        /// </param>
        /// <returns>
        /// Command result object.
        /// </returns>
        private ConsoleCommandResult HandleCommand(string commandString)
        {
            if (String.IsNullOrEmpty(commandString))
            {
                return ConsoleCommandResult.Ignored;
            }

            string[] tokens = SplitCommandString(commandString);
            if (tokens.Length == 0)
            {
                return ConsoleCommandResult.Ignored;
            }

            string command = tokens[0].ToLower();
            string[] arguments = null;
            if (tokens.Length > 1)
            {
                arguments = new string[tokens.Length - 1];
                Array.Copy(tokens, 1, arguments, 0, tokens.Length - 1);
            }

            return this.ProcessCommand(command, arguments ?? new string[0]);
        }

        /// <summary>
        /// Pretty-print cursor/caret for user-input.
        /// </summary>
        private void PrettyPrintCaret()
        {
            System.Console.Write("{0} > ", this.CaretText);
        }

        /// <summary>
        /// Processes the specified command with the specified arguments.
        /// </summary>
        /// <param name="command">
        /// The command to process.
        /// </param>
        /// <param name="arguments">
        /// The arguments sent with the command.
        /// </param>
        /// <returns>
        /// The result from processing the command.
        /// </returns>
        private ConsoleCommandResult ProcessCommand(string command, string[] arguments)
        {
            System.Console.WriteLine();
            ConsoleCommandResult result = ConsoleCommandResult.Ok;

            ConsoleCommand cmd;
            if (command == "exit")
            {
                result = ConsoleCommandResult.Exit;
            }
            else if (this._commands.TryGetValue(command, out cmd))
            {
                try
                {
                    cmd.Handler.Invoke(command, arguments);
                }
                catch (Exception e)
                {
                    Log.ToolExceptionCtx(
                        CommandLogCtx,
                        e,
                        "Exception thrown while processing the '{0}' command.",
                        command);

                    result = ConsoleCommandResult.Faulted;
                }
            }
            else
            {
                this.CommandNotImplementedHandler(command, arguments);
                result = ConsoleCommandResult.Ignored;
            }

            System.Console.WriteLine();
            return result;
        }

        /// <summary>
        /// Processes console input from the user.
        /// </summary>
        private void ProcessConsoleInput()
        {
            // Regular standard input command input loop.
            bool isRunning = true;
            while (isRunning)
            {
                LogFactory.ApplicationConsoleLog.Flush();
                this.PrettyPrintCaret();
                string commandString = System.Console.ReadLine();

                ConsoleCommandResult result = this.HandleCommand(commandString);
                if (result == ConsoleCommandResult.Exit)
                {
                    Log.MessageCtx(CommandLogCtx, "Exiting...");
                    isRunning = false;
                }
            }
        }

        /// <summary>
        /// Processes a script command from the command-line.
        /// </summary>
        /// <param name="scriptFilename">
        /// The script filename to process.
        /// </param>
        private void ProcessScript(string scriptFilename)
        {
            if (File.Exists(scriptFilename))
            {
                using (StreamReader sr = new StreamReader(scriptFilename))
                {
                    string commandLine = String.Empty;
                    while (null != (commandLine = sr.ReadLine()))
                    {
                        ConsoleCommandResult result = this.HandleCommand(commandLine);
                        if (ConsoleCommandResult.Exit == result)
                        {
                            Log.MessageCtx(CommandLogCtx, "Exiting...");
                        }
                    }
                }
            }
            else
            {
                Log.ErrorCtx(
                    CommandLogCtx,
                    "Script file: {0} does not exist.  Exiting.",
                    scriptFilename);
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsInteractiveConsoleApplication {Class}
} // RSG.Editor.Controls {Namespace}
