﻿
<!--
    <copyright file="RsToolBar.generic.xaml" company="Rockstar">
    Copyright © Rockstar Games 2013. All rights reserved
    </copyright>
-->

<ResourceDictionary xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
                    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
                    xmlns:adp="clr-namespace:RSG.Editor.Controls.AttachedDependencyProperties"
                    xmlns:ctrls="clr-namespace:RSG.Editor.Controls"
                    xmlns:editor="clr-namespace:RSG.Editor;assembly=RSG.Editor"
                    xmlns:ext="clr-namespace:RSG.Editor.Controls.Extensions"
                    xmlns:local="clr-namespace:RSG.Editor.Controls.Commands"
                    xmlns:po="http://schemas.microsoft.com/winfx/2006/xaml/presentation/options"
                    xmlns:rsg="http://schemas.rockstargames.com/2013/xaml/editor"
                    xmlns:sys="clr-namespace:System;assembly=mscorlib">

    <ResourceDictionary.MergedDictionaries>
        <ResourceDictionary Source="../CommonCommandTemplates.generic.xaml" />
    </ResourceDictionary.MergedDictionaries>

    <!--
        Default style for the type :-
        x:Type={RSG.Editor.Controls.RsToolBar}
    -->
    <Style TargetType="{x:Type local:RsToolBar}">
        <Setter Property="Control.Background"
                Value="{ext:ThemeResource WindowBackgroundKey}" />
        <Setter Property="adp:ThemeProperties.ImageBackgroundColour"
                Value="{ext:ThemeResource WindowBackgroundKey}" />
        <Setter Property="Control.BorderThickness"
                Value="1" />
        <Setter Property="FrameworkElement.FocusVisualStyle"
                Value="{x:Null}" />
        <Setter Property="local:RsToolBar.ShowHeader"
                Value="False" />
        <Setter Property="FocusManager.IsFocusScope"
                Value="True" />
        <Setter Property="FrameworkElement.Margin"
                Value="0" />
        <Setter Property="Padding"
                Value="0,2,0,2" />
        <Setter Property="Focusable"
                Value="False" />
        <Setter Property="CornerRadius"
                Value="0" />
        <Setter Property="Height"
                Value="Auto" />
        <Setter Property="Control.BorderBrush"
                Value="{ext:ThemeResource WindowBackgroundKey}" />
        <Setter Property="Control.Template"
                Value="{StaticResource CoreToolBarTemplate}" />
    </Style>

    <!--
        Style for the embedded style resource key -
        resource key={RSG.Editor.Controls.Commands.RsToolBar+SeparatorStyleKey}
    -->
    <Style x:Key="{x:Static local:RsToolBar.SeparatorStyleKey}"
           TargetType="{x:Type local:RsToolBarItem}">
        <Setter Property="Focusable"
                Value="False" />
        <Setter Property="IsHitTestVisible"
                Value="False" />
        <Setter Property="Background"
                Value="{ext:ThemeResource GroupBorderKey}" />
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="{x:Type local:RsToolBarItem}">
                    <StackPanel Margin="2,0,2,0"
                                VerticalAlignment="Stretch"
                                Orientation="Horizontal">
                        <Rectangle Width="1"
                                   Fill="{TemplateBinding Background}" />
                        <Rectangle Width="1"
                                   Fill="{ext:ThemeResource GroupHighlightBorderKey}" />
                    </StackPanel>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>

    <!--
        Style for the embedded style resource key -
        resource key={RSG.Editor.Controls.Commands.RsToolBar+CommandButtonStyleKey}
    -->
    <Style x:Key="{x:Static local:RsToolBar.CommandButtonStyleKey}"
           TargetType="{x:Type local:RsToolBarItem}">
        <Setter Property="UIElement.IsEnabled"
                Value="{Binding IsEnabled,
                                UpdateSourceTrigger=PropertyChanged}" />
        <Setter Property="Content"
                Value="{Binding Text,
                                UpdateSourceTrigger=PropertyChanged}" />
        <Setter Property="FrameworkElement.ToolTip"
                Value="{Binding ToolTip,
                                UpdateSourceTrigger=PropertyChanged}" />
        <Setter Property="ToolTipService.ShowOnDisabled"
                Value="True" />
        <Setter Property="local:RsToolBarItem.IconBitmap"
                Value="{Binding Icon}" />
        <Setter Property="Button.CommandParameter"
                Value="{Binding CommandParameter,
                                UpdateSourceTrigger=PropertyChanged}" />
        <Setter Property="Button.Command"
                Value="{Binding Command,
                                UpdateSourceTrigger=PropertyChanged}" />
        <Setter Property="Control.Template">
            <Setter.Value>
                <StaticResource ResourceKey="CoreToolBarItemTemplate" />
            </Setter.Value>
        </Setter>
        <Setter Property="Control.Padding"
                Value="2" />
        <Setter Property="BorderThickness"
                Value="1,1,1,1" />
        <Style.Triggers>
            <DataTrigger Binding="{Binding DisplayStyle,
                                           UpdateSourceTrigger=PropertyChanged}"
                         Value="{x:Static editor:CommandItemDisplayStyle.TextOnly}">
                <Setter Property="local:RsToolBarItem.IconBitmap"
                        Value="{x:Null}" />
            </DataTrigger>
            <DataTrigger Binding="{Binding DisplayStyle,
                                           UpdateSourceTrigger=PropertyChanged}"
                         Value="{x:Static editor:CommandItemDisplayStyle.ImageOnly}">
                <Setter Property="Content"
                        Value="{x:Null}" />
            </DataTrigger>
            <DataTrigger Binding="{Binding DisplayStyle,
                                           UpdateSourceTrigger=PropertyChanged}"
                         Value="{x:Static editor:CommandItemDisplayStyle.Default}">
                <Setter Property="Content"
                        Value="{x:Null}" />
            </DataTrigger>
        </Style.Triggers>
    </Style>

    <!--
        Style for the embedded style resource key -
        resource key={RSG.Editor.Controls.Commands.RsToolBar+ToggleStyleKey}
    -->
    <Style x:Key="{x:Static local:RsToolBar.ToggleStyleKey}"
           TargetType="{x:Type local:RsToolBarItem}">
        <Setter Property="UIElement.IsEnabled"
                Value="{Binding IsEnabled,
                                UpdateSourceTrigger=PropertyChanged}" />
        <Setter Property="Content"
                Value="{Binding Text,
                                UpdateSourceTrigger=PropertyChanged}" />
        <Setter Property="FrameworkElement.ToolTip"
                Value="{Binding ToolTip,
                                UpdateSourceTrigger=PropertyChanged}" />
        <Setter Property="ToolTipService.ShowOnDisabled"
                Value="True" />
        <Setter Property="local:RsToolBarItem.IconBitmap"
                Value="{Binding Icon}" />
        <Setter Property="MenuItem.IsCheckable"
                Value="True" />
        <Setter Property="ToggleButton.IsChecked"
                Value="{Binding IsToggled,
                                Mode=TwoWay}" />
        <Setter Property="local:RsMenuItem.IconBitmap"
                Value="{x:Null}" />
        <Setter Property="MenuItem.CommandParameter"
                Value="{Binding RelativeSource={RelativeSource Self},
                                Path=(ToggleButton.IsChecked)}" />
        <Setter Property="MenuItem.Command"
                Value="{Binding Command,
                                UpdateSourceTrigger=PropertyChanged}" />
        <Setter Property="MenuItem.CommandTarget"
                Value="{Binding RelativeSource={RelativeSource AncestorType=local:RsToolBar},
                                BindsDirectlyToSource=True}" />
        <Setter Property="Control.Template">
            <Setter.Value>
                <StaticResource ResourceKey="CoreToolBarItemTemplate" />
            </Setter.Value>
        </Setter>
        <Setter Property="Control.Padding"
                Value="0,1,5,1" />
        <Setter Property="BorderThickness"
                Value="1,1,1,1" />
        <Style.Triggers>
            <DataTrigger Binding="{Binding DisplayStyle,
                                           UpdateSourceTrigger=PropertyChanged}"
                         Value="{x:Static rsg:CommandItemDisplayStyle.TextOnly}">
                <Setter Property="local:RsToolBarItem.IconBitmap"
                        Value="{x:Null}" />
            </DataTrigger>
            <DataTrigger Binding="{Binding DisplayStyle,
                                           UpdateSourceTrigger=PropertyChanged}"
                         Value="{x:Static rsg:CommandItemDisplayStyle.ImageOnly}">
                <Setter Property="Content"
                        Value="{x:Null}" />
            </DataTrigger>
            <DataTrigger Binding="{Binding CommandParameter,
                                           UpdateSourceTrigger=PropertyChanged,
                                           Converter={StaticResource NotNullConverter}}"
                         Value="True">
                <Setter Property="CommandParameter"
                        Value="{Binding CommandParameter}" />
            </DataTrigger>
        </Style.Triggers>
    </Style>

    <!--
        Style for the embedded style resource key -
        resource key={RSG.Editor.Controls.Commands.RsToolBar+CommandComboBoxStyleKey}
    -->
    <Style x:Key="{x:Static local:RsToolBar.CommandComboBoxStyleKey}"
           TargetType="{x:Type local:RsToolBarItem}">
        <Setter Property="UIElement.IsEnabled"
                Value="{Binding IsEnabled,
                                UpdateSourceTrigger=PropertyChanged}" />
        <Setter Property="FrameworkElement.ToolTip"
                Value="{Binding ToolTip,
                                Mode=OneTime}" />
        <Setter Property="ToolTipService.ShowOnDisabled"
                Value="True" />
        <Setter Property="MenuItem.IsCheckable"
                Value="False" />
        <Setter Property="Control.Margin"
                Value="3,1,3,0" />
        <Setter Property="Control.Height"
                Value="Auto" />
        <Setter Property="Control.Width"
                Value="{Binding Width}" />
        <Setter Property="Control.Template">
            <Setter.Value>
                <ControlTemplate TargetType="local:RsToolBarItem">
                    <ctrls:RsComboBox x:Name="PART_FocusTarget"
                                      Command="{Binding Command}"
                                      ItemsSource="{Binding ComboItems}"
                                      SelectedItem="{Binding SelectedItem,
                                                             Mode=TwoWay}">
                        <ctrls:RsComboBox.ItemTemplate>
                            <DataTemplate>
                                <StackPanel Orientation="Horizontal">
                                    <ctrls:RsThemeImage Width="16"
                                                        Height="16"
                                                        Margin="2,0,0,0"
                                                        IconBitmap="{Binding Icon,
                                                                             UpdateSourceTrigger=PropertyChanged}"
                                                        adp:ThemeProperties.ImageBackgroundColour="{Binding RelativeSource={RelativeSource AncestorType=Border},
                                                                                                            Path=Background}" />
                                    <TextBlock Margin="5,0,0,0"
                                               VerticalAlignment="Center"
                                               Text="{Binding Text}"
                                               TextTrimming="CharacterEllipsis" />
                                </StackPanel>
                            </DataTemplate>
                        </ctrls:RsComboBox.ItemTemplate>
                        <ctrls:RsComboBox.ItemContainerStyle>
                            <Style TargetType="ctrls:RsComboBoxItem">
                                <Setter Property="CommandParameter"
                                        Value="{Binding CommandParameter,
                                                        UpdateSourceTrigger=PropertyChanged}" />
                            </Style>
                        </ctrls:RsComboBox.ItemContainerStyle>
                    </ctrls:RsComboBox>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
        <Style.Triggers>
            <Trigger Property="UIElement.IsEnabled" Value="False">
                <Setter Property="FrameworkElement.ToolTip">
                    <Setter.Value>
                        <ToolTip Content="{Binding ToolTip,
                                                   Mode=OneTime}"
                                 ContentStringFormat="{}{0} (Disabled)" />
                    </Setter.Value>
                </Setter>
            </Trigger>
        </Style.Triggers>
    </Style>

    <!--
        Style for the embedded style resource key -
        resource key={RSG.Editor.Controls.Commands.RsToolBar+FilterComboStyleKey}
    -->
    <Style x:Key="{x:Static local:RsToolBar.FilterComboStyleKey}"
           TargetType="{x:Type local:RsToolBarItem}">
        <Setter Property="UIElement.IsEnabled"
                Value="{Binding IsEnabled,
                                UpdateSourceTrigger=PropertyChanged}" />
        <Setter Property="FrameworkElement.ToolTip"
                Value="{Binding ToolTip}" />
        <Setter Property="ToolTipService.ShowOnDisabled"
                Value="True" />
        <Setter Property="MenuItem.IsCheckable"
                Value="False" />
        <Setter Property="Control.Margin"
                Value="3,1,3,0" />
        <Setter Property="Control.Height"
                Value="Auto" />
        <Setter Property="Control.Width"
                Value="{Binding Width}" />
        <Setter Property="Control.Template">
            <Setter.Value>
                <ControlTemplate TargetType="local:RsToolBarItem">
                    <ctrls:RsToggleComboBox x:Name="PART_FocusTarget"
                                            Width="{Binding Width}"
                                            CheckCount="{Binding FilterCount}"
                                            Command="{Binding Command}"
                                            DisplayText="{Binding Text}"
                                            ItemsSource="{Binding ComboItems}"
                                            ShowCheckCount="{Binding ShowFilterCount}">
                        <ctrls:RsToggleComboBox.ItemTemplate>
                            <DataTemplate>
                                <StackPanel HorizontalAlignment="Stretch"
                                            VerticalAlignment="Center"
                                            Background="Transparent"
                                            Orientation="Horizontal">
                                    <ctrls:RsThemeImage Width="16"
                                                        Height="16"
                                                        Margin="2,1,0,0"
                                                        IconBitmap="{Binding Icon,
                                                                             UpdateSourceTrigger=PropertyChanged}"
                                                        adp:ThemeProperties.ImageBackgroundColour="{Binding RelativeSource={RelativeSource AncestorType=Border},
                                                                                                            Path=Background}" />
                                    <TextBlock Margin="5,1,0,0"
                                               VerticalAlignment="Center"
                                               Text="{Binding Text}"
                                               TextTrimming="CharacterEllipsis" />
                                </StackPanel>
                            </DataTemplate>
                        </ctrls:RsToggleComboBox.ItemTemplate>
                        <ctrls:RsToggleComboBox.ItemContainerStyle>
                            <Style TargetType="ctrls:RsToggleComboBoxItem">
                                <Setter Property="ToolTip"
                                        Value="{Binding Description}" />
                                <Setter Property="CommandParameter">
                                    <Setter.Value>
                                        <MultiBinding Converter="{StaticResource MultiCommandParameterConverter}"
                                                      UpdateSourceTrigger="PropertyChanged">
                                            <MultiBinding.Bindings>
                                                <Binding BindsDirectlyToSource="True" />
                                                <Binding Path="CommandParameter"
                                                         UpdateSourceTrigger="PropertyChanged" />
                                                <Binding Path="IsToggled"
                                                         UpdateSourceTrigger="PropertyChanged" />
                                            </MultiBinding.Bindings>
                                        </MultiBinding>
                                    </Setter.Value>
                                </Setter>
                                <Setter Property="IsChecked"
                                        Value="{Binding IsToggled,
                                                        Mode=TwoWay,
                                                        UpdateSourceTrigger=PropertyChanged}" />
                            </Style>
                        </ctrls:RsToggleComboBox.ItemContainerStyle>
                    </ctrls:RsToggleComboBox>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
        <Style.Triggers>
            <Trigger Property="UIElement.IsEnabled" Value="False">
                <Setter Property="FrameworkElement.ToolTip">
                    <Setter.Value>
                        <ToolTip Content="{Binding ToolTip}"
                                 ContentStringFormat="{}{0} (Disabled)" />
                    </Setter.Value>
                </Setter>
            </Trigger>
        </Style.Triggers>
    </Style>

    <!--
        Style for the embedded style resource key -
        resource key={RSG.Editor.Controls.Commands.RsToolBar+SearchStyleKey}
    -->
    <Style x:Key="{x:Static local:RsToolBar.SearchStyleKey}"
           TargetType="{x:Type local:RsToolBarItem}">
        <Setter Property="UIElement.IsEnabled"
                Value="{Binding IsEnabled,
                                UpdateSourceTrigger=PropertyChanged}" />
        <Setter Property="FrameworkElement.ToolTip"
                Value="{Binding ToolTip}" />
        <Setter Property="ToolTipService.ShowOnDisabled"
                Value="True" />
        <Setter Property="MenuItem.IsCheckable"
                Value="False" />
        <Setter Property="Control.Margin"
                Value="3,1,3,0" />
        <Setter Property="Control.Height"
                Value="Auto" />
        <Setter Property="Control.Width"
                Value="{Binding Width}" />
        <Setter Property="Control.Template">
            <Setter.Value>
                <ControlTemplate TargetType="local:RsToolBarItem">
                    <ctrls:RsSearchControl x:Name="PART_FocusTarget"
                                           Command="{Binding Command}"
                                           Scopes="{Binding Scopes}"
                                           SearchTrimsWhitespaces="True"
                                           SearchUsesMru="True"
                                           SupportFiltering="{Binding SearchParameters.CanFilterResults}"
                                           SupportMatchCase="{Binding SearchParameters.CanUseMatchCase}"
                                           SupportMatchWord="{Binding SearchParameters.CanUseMatchWholeWord}"
                                           SupportRegularExpressions="{Binding SearchParameters.CanUseRegularExpressions}"
                                           Watermark="{Binding Text}" />
                </ControlTemplate>
            </Setter.Value>
        </Setter>
        <Style.Triggers>
            <Trigger Property="UIElement.IsEnabled" Value="False">
                <Setter Property="FrameworkElement.ToolTip">
                    <Setter.Value>
                        <ToolTip Content="{Binding ToolTip}"
                                 ContentStringFormat="{}{0} (Disabled)" />
                    </Setter.Value>
                </Setter>
            </Trigger>
        </Style.Triggers>
    </Style>
</ResourceDictionary>
