﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Editor.Controls;
using RSG.UniversalLog.ViewModel;

namespace RSG.UniversalLog.View
{
    /// <summary>
    /// Interaction logic for UniversalLogStatusControl.xaml
    /// </summary>
    public partial class UniversalLogStatusControl : RsUserControl
    {
        public UniversalLogStatusControl()
        {
            InitializeComponent();
        }
    } // UniversalLogStatusControl
}
