﻿//---------------------------------------------------------------------------------------------
// <copyright file="RpfViewControl.xaml.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Rpf.View
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Text.RegularExpressions;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Input;
    using RSG.Editor;
    using RSG.Editor.Controls;
    using RSG.Editor.Controls.Helpers;
    using RSG.Editor.SharedCommands;
    using RSG.Rpf.Commands;
    using RSG.Rpf.View.Commands;
    using RSG.Rpf.ViewModel;

    /// <summary>
    /// Defines the control that can be used to display the contents of a Rockstar pack file
    /// and perform extraction operations on it.
    /// </summary>
    [GuidAttribute("2E2E7E0D-FCE4-3A7D-A67D-1FEF6A2B1829")]
    public partial class RpfViewControl : RsUserControl
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="FilterSearchItemsProperty"/> property.
        /// </summary>
        private static DependencyProperty _filterSearchItemsProperty;

        /// <summary>
        /// The private field used for the <see cref="SearchExpressionProperty"/> property.
        /// </summary>
        private static DependencyProperty _searchExpressionProperty;

        /// <summary>
        /// The private field used for the <see cref="AllowedCategories"/> property.
        /// </summary>
        private IEnumerable<PackEntryCategory> _allowedCategories;

        /// <summary>
        /// The private field used for the <see cref="HorizontalScrollBarVisibility"/>
        /// property.
        /// </summary>
        private ScrollBarVisibility _horizontalScrollBarVisibility;

        /// <summary>
        /// The private field used for the <see cref="InitialSortMember"/> property.
        /// </summary>
        private InitialSortMember _initialSortMember;

        /// <summary>
        /// A private value indicating whether a selection suppression has been preformed due
        /// to supporting dragging multiple files.
        /// </summary>
        private DataGridRow _suppressedSelection;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RpfViewControl"/> class.
        /// </summary>
        static RpfViewControl()
        {
            ItemsControl.ItemsSourceProperty.AddOwner(
                typeof(RpfViewControl),
                new PropertyMetadata(null, OnItemsSourceChanged));

            _filterSearchItemsProperty =
                DependencyProperty.Register(
                "FilterSearchItems",
                typeof(bool),
                typeof(RpfViewControl),
                new FrameworkPropertyMetadata(false, OnFilterPropertyChanged));

            _searchExpressionProperty =
                DependencyProperty.Register(
                "SearchExpression",
                typeof(Regex),
                typeof(RpfViewControl),
                new FrameworkPropertyMetadata(null, OnFilterPropertyChanged));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RpfViewControl"/> class.
        /// </summary>
        public RpfViewControl()
        {
            this.InitializeComponent();
            ICollectionView items = CollectionViewSource.GetDefaultView(this.DataGrid.Items);
            if (items != null)
            {
                items.CollectionChanged += this.OnVisibleItemsChanged;
            }

            this._allowedCategories = new List<PackEntryCategory>()
            {
                PackEntryCategory.Directory,
                PackEntryCategory.File,
                PackEntryCategory.Resource,
                PackEntryCategory.Unknown
            };

            Binding binding = new Binding("ItemsSource");
            binding.Source = this;
            this.DataGrid.SetBinding(DataGrid.ItemsSourceProperty, binding);
            this._horizontalScrollBarVisibility = this.DataGrid.HorizontalScrollBarVisibility;
            this.ReFilterItems();
            this.AttachCommandBindings();
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the dependency property for the <see cref="FilterSearchItems"/>
        /// property.
        /// </summary>
        public static DependencyProperty FilterSearchItemsProperty
        {
            get { return _filterSearchItemsProperty; }
            set { _filterSearchItemsProperty = value; }
        }

        /// <summary>
        /// Gets or sets the dependency property for the <see cref="SearchExpression"/>
        /// property.
        /// </summary>
        public static DependencyProperty SearchExpressionProperty
        {
            get { return _searchExpressionProperty; }
            set { _searchExpressionProperty = value; }
        }

        /// <summary>
        /// Gets or sets the categories that are currently allowed to be shown.
        /// </summary>
        public IEnumerable<PackEntryCategory> AllowedCategories
        {
            get
            {
                return this._allowedCategories;
            }

            set
            {
                this._allowedCategories = value;
                this.ReFilterItems();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether items that fail the search expression, if
        /// not null, should still be shown in the interface.
        /// </summary>
        public bool FilterSearchItems
        {
            get { return (bool)this.GetValue(FilterSearchItemsProperty); }
            set { this.SetValue(FilterSearchItemsProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating how the horizontal scroll bar is displayed.
        /// </summary>
        public ScrollBarVisibility HorizontalScrollBarVisibility
        {
            get
            {
                return this._horizontalScrollBarVisibility;
            }

            set
            {
                this._horizontalScrollBarVisibility = value;
                this.DataGrid.HorizontalScrollBarVisibility = value;
            }
        }

        /// <summary>
        /// Sets the member which is initially sorted ascending.
        /// </summary>
        public InitialSortMember InitialSortMember
        {
            set { this._initialSortMember = value; }
        }

        /// <summary>
        /// Gets or sets a collection used to generate the content of this control.
        /// </summary>
        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)this.GetValue(ItemsControl.ItemsSourceProperty); }
            set { this.SetValue(ItemsControl.ItemsSourceProperty, value); }
        }

        /// <summary>
        /// Gets or sets the search expression to use during filtering of the items.
        /// </summary>
        public Regex SearchExpression
        {
            get { return (Regex)this.GetValue(SearchExpressionProperty); }
            set { this.SetValue(SearchExpressionProperty, value); }
        }

        /// <summary>
        /// Gets a value indicating whether the layout for this control get serialised on
        /// exit and deserialised after loading.
        /// </summary>
        protected override bool MakeLayoutPersistent
        {
            get { return true; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Resets the filter delegate so that all the items get pushed through it again.
        /// </summary>
        internal void ReFilterItems()
        {
            if (!this.Dispatcher.CheckAccess())
            {
                this.Dispatcher.BeginInvoke(new Action(delegate { this.ReFilterItems(); }));
                return;
            }

            ICollectionView items = CollectionViewSource.GetDefaultView(this.DataGrid.Items);
            if (items != null)
            {
                items.Filter = this.FilterItem;
            }
        }

        /// <summary>
        /// Called whenever a filter dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The object whose filter dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnFilterPropertyChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            RpfViewControl rpfViewer = s as RpfViewControl;
            if (rpfViewer == null)
            {
                return;
            }

            if (e.Property == RpfViewControl.SearchExpressionProperty)
            {
                if (rpfViewer.FilterSearchItems == false)
                {
                    return;
                }
            }

            rpfViewer.ReFilterItems();
        }

        /// <summary>
        /// Called whenever the <see cref="ItemsSource"/> dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The object whose <see cref="ItemsSource"/> dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnItemsSourceChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            RpfViewControl rpfViewer = s as RpfViewControl;
            if (rpfViewer == null)
            {
                return;
            }

            if (e.OldValue == null)
            {
                string propertyName = String.Empty;
                switch (rpfViewer._initialSortMember)
                {
                    case InitialSortMember.Index:
                        propertyName = "Index";
                        break;
                    case InitialSortMember.Name:
                        propertyName = "Name";
                        break;
                    case InitialSortMember.Offset:
                        propertyName = "Offset";
                        break;
                    case InitialSortMember.Packed:
                        propertyName = "Packed";
                        break;
                    case InitialSortMember.PhysicalSize:
                        propertyName = "PhysicalSize";
                        break;
                    case InitialSortMember.Size:
                        propertyName = "Size";
                        break;
                    case InitialSortMember.VirtualSize:
                        propertyName = "VirtualSize";
                        break;
                    default:
                        return;
                }

                DataGridColumn sortColumn = null;
                foreach (DataGridColumn column in rpfViewer.DataGrid.Columns)
                {
                    if (column.SortDirection != null)
                    {
                        return;
                    }

                    if (String.Equals(column.SortMemberPath, propertyName))
                    {
                        sortColumn = column;
                        break;
                    }
                }

                if (sortColumn == null)
                {
                    return;
                }

                sortColumn.SortDirection = ListSortDirection.Ascending;
                rpfViewer.DataGrid.Items.SortDescriptions.Clear();
                SortDescription description =
                    new SortDescription(propertyName, ListSortDirection.Ascending);
                rpfViewer.DataGrid.Items.SortDescriptions.Add(description);
            }
            else
            {
                ListSortDirection direction = ListSortDirection.Ascending;
                DataGridColumn sortColumn = null;
                foreach (DataGridColumn column in rpfViewer.DataGrid.Columns)
                {
                    if (column.SortDirection == null)
                    {
                        continue;
                    }

                    direction = (ListSortDirection)column.SortDirection;
                    sortColumn = column;
                    break;
                }

                if (sortColumn == null)
                {
                    return;
                }

                rpfViewer.Dispatcher.BeginInvoke(
                    new Action(
                    delegate
                    {
                        sortColumn.SortDirection = direction;
                        rpfViewer.DataGrid.Items.SortDescriptions.Clear();
                        SortDescription description =
                            new SortDescription(sortColumn.SortMemberPath, direction);
                        rpfViewer.DataGrid.Items.SortDescriptions.Add(description);
                    }),
                    System.Windows.Threading.DispatcherPriority.Render);
            }
        }

        /// <summary>
        /// Attaches all of the necessary command bindings to this instance.
        /// </summary>
        private void AttachCommandBindings()
        {
            ICommandAction action = new ExtractItemsAction(this.EntriesResolver);
            action.AddBinding(RpfViewerCommands.ExtractEntireRpf, this);
            action.AddBinding(RpfViewerCommands.ExtractRpfItems, this);

            action = new ViewFileAction(this.EntriesResolver);
            action.AddBinding(RpfViewerCommands.ViewRpfItem, this);

            action = new SelectAllAction(this.ControlsResolver);
            action.AddBinding(RockstarCommands.SelectAll, this);

            action = new InvertSelectionAction(this.ControlsResolver);
            action.AddBinding(RockstarCommands.InvertSelection, this);

            action = new FilterByCategoryAction(this.ControlsResolver);
            action.AddBinding(RpfViewerCommands.FilterRpfByCategory, this);

            action = new CopyFileNameAction(this.EntriesResolver);
            action.AddBinding(RpfViewerCommands.CopyFileName, this);

            SearchAction searchImplementer = new SearchAction(this.ControlsResolver);
            searchImplementer.AddBinding(this);
        }

        /// <summary>
        /// Represents the method that is used to retrieve a command parameter object for the
        /// view commands.
        /// </summary>
        /// <param name="commandData">
        /// The command data that has been sent.
        /// </param>
        /// <returns>
        /// The command parameter object for the specified command.
        /// </returns>
        private IEnumerable<RpfViewControl> ControlsResolver(CommandData commandData)
        {
            return Enumerable.Repeat(this, 1);
        }

        /// <summary>
        /// Represents the method that is used to retrieve a command parameter object for the
        /// view commands.
        /// </summary>
        /// <param name="commandData">
        /// The command data that has been sent.
        /// </param>
        /// <returns>
        /// The command parameter object for the specified command.
        /// </returns>
        private IEnumerable<PackEntryViewModel> EntriesResolver(CommandData commandData)
        {
            ICollectionView items = CollectionViewSource.GetDefaultView(this.DataGrid.Items);
            if (items == null)
            {
                Debug.Assert(items != null, "Unable to resolve parameter.");
                return Enumerable.Empty<PackEntryViewModel>();
            }

            if (commandData.Command == RpfViewerCommands.ExtractEntireRpf)
            {
                return items.SourceCollection.OfType<PackEntryViewModel>();
            }
            else if (commandData.Command == RpfViewerCommands.ExtractRpfItems)
            {
                return this.DataGrid.SelectedItems.OfType<PackEntryViewModel>();
            }
            else if (commandData.Command == RpfViewerCommands.ViewRpfItem)
            {
                return this.DataGrid.SelectedItems.OfType<PackEntryViewModel>();
            }
            else if (commandData.Command == RpfViewerCommands.CopyFileName)
            {
                return this.DataGrid.SelectedItems.OfType<PackEntryViewModel>();
            }

            throw new ArgumentException("Unexpected command past to resolver");
        }

        /// <summary>
        /// A method that determines whether the specified item can be viewed in the data grid
        /// control or not.
        /// </summary>
        /// <param name="item">
        /// The item to test.
        /// </param>
        /// <returns>
        /// True if the specified item can be viewed in the data grid; otherwise, false.
        /// </returns>
        private bool FilterItem(object item)
        {
            PackEntryViewModel viewModel = item as PackEntryViewModel;
            if (viewModel == null)
            {
                return false;
            }

            bool valid = true;
            if (this.SearchExpression != null && this.FilterSearchItems)
            {
                if (!this.SearchExpression.IsMatch(viewModel.Name))
                {
                    valid = false;
                }
            }

            if (valid == true)
            {
                switch (viewModel.Category)
                {
                    case PackEntryCategory.Directory:
                        valid = this.AllowedCategories.Contains(PackEntryCategory.Directory);
                        break;
                    case PackEntryCategory.File:
                        valid = this.AllowedCategories.Contains(PackEntryCategory.File);
                        break;
                    case PackEntryCategory.Resource:
                        valid = this.AllowedCategories.Contains(PackEntryCategory.Resource);
                        break;
                    case PackEntryCategory.Unknown:
                        valid = this.AllowedCategories.Contains(PackEntryCategory.Unknown);
                        break;
                }
            }

            viewModel.IncludedInFilter = valid;
            return valid;
        }

        /// <summary>
        /// Called when the context menu is opening for a data grid row so that the
        /// selection can be manipulated to make sure it is selected.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to. (The data grid row through a XAML style).
        /// </param>
        /// <param name="e">
        /// The System.Windows.Controls.ContextMenuEventArgs containing the event data.
        /// </param>
        private void OnContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            DependencyObject obj = sender as DependencyObject;
            if (obj == null)
            {
                Debug.Assert(false, "Incorrect evenr handler");
                return;
            }

            DataGridRow row = obj.GetVisualAncestorOrSelf<DataGridRow>();
            if (row == null)
            {
                return;
            }

            if (row.IsSelected == false)
            {
                this.DataGrid.UnselectAll();
                this.DataGrid.SelectedItem = row.DataContext;
            }
        }

        /// <summary>
        /// Called whenever the left mouse button is released over a data grid row item.
        /// </summary>
        /// <param name="sender">
        /// The object that this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.MouseEventArgs containing the event data.
        /// </param>
        private void OnMouseLeftButtonUp(object sender, MouseEventArgs e)
        {
            if (this._suppressedSelection != null)
            {
                this.DataGrid.SelectedItems.Clear();
                this._suppressedSelection.IsSelected = true;
            }
        }

        /// <summary>
        /// Called whenever the mouse is moved over a data grid row item.
        /// </summary>
        /// <param name="sender">
        /// The object that this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.MouseEventArgs containing the event data.
        /// </param>
        private void OnMouseMoved(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                var virtualFileDataObject = new VirtualFileDataObject();

                List<FileDescriptor> descriptors = new List<FileDescriptor>();
                foreach (var item in this.DataGrid.SelectedItems)
                {
                    PackEntryViewModel vm = item as PackEntryViewModel;
                    if (vm != null)
                    {
                        FileDescriptor descriptor = new FileDescriptor();
                        descriptor.Name = vm.Name;
                        descriptor.ChangeTimeUtc = DateTime.UtcNow;
                        descriptor.StreamContents = stream =>
                        {
                            byte[] contents = vm.Parent.ExtractEntryToByteArray(vm);
                            stream.Write(contents, 0, contents.Length);
                        };

                        descriptors.Add(descriptor);
                    }
                }

                if (descriptors.Count == 0)
                {
                    return;
                }

                virtualFileDataObject.SetData(descriptors);
                var source = sender as DependencyObject;
                DragDropEffects effects = DragDropEffects.Move;
                VirtualFileDataObject.DoDragDrop(source, virtualFileDataObject, effects);
                this._suppressedSelection = null;
            }
        }

        /// <summary>
        /// Called whenever the left mouse button is pressed down over a data grid row item.
        /// </summary>
        /// <param name="sender">
        /// The object that this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.MouseEventArgs containing the event data.
        /// </param>
        private void OnPreviewMouseLeftButtonDown(object sender, MouseEventArgs e)
        {
            this._suppressedSelection = null;
            DataGridRow row = (DataGridRow)sender;
            if (row.IsSelected)
            {
                e.Handled = true;
                this._suppressedSelection = row;
            }
        }

        /// <summary>
        /// Called whenever the items currently in the data grid are changed.
        /// </summary>
        /// <param name="sender">
        /// The object that this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The collection changed arguments.
        /// </param>
        private void OnVisibleItemsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            ItemCollection collection = sender as ItemCollection;
            if (collection == null)
            {
                Debug.Assert(collection != null, "Incorrect cast in collection arguments.");
                return;
            }

            RpfViewerDataContext dc =
                this.Dispatcher.Invoke(new Func<RpfViewerDataContext>(
                    delegate
                    {
                        return this.DataContext as RpfViewerDataContext;
                    }));

            if (dc == null || dc.PackFileViewModel == null)
            {
                Debug.Assert(collection != null, "Incorrect data context cast in rpf viewer.");
                return;
            }

            int fileCount = 0;
            int resourceCount = 0;
            int unknownCount = 0;
            foreach (object item in collection)
            {
                PackEntryViewModel viewModel = item as PackEntryViewModel;
                if (viewModel == null)
                {
                    continue;
                }

                if (viewModel.Category == PackEntryCategory.File)
                {
                    fileCount++;
                }
                else if (viewModel.Category == PackEntryCategory.Resource)
                {
                    resourceCount++;
                }
                else if (viewModel.Category == PackEntryCategory.Unknown)
                {
                    unknownCount++;
                }
            }

            dc.PackFileViewModel.FilteredFileCount = fileCount;
            dc.PackFileViewModel.FilteredResourceCount = resourceCount;
            dc.PackFileViewModel.FilteredUnknownCount = unknownCount;
            dc.PackFileViewModel.OnTotalsChanged();
        }
        #endregion Methods
    } // RSG.Rpf.View.RpfViewControl {Class}
} // RSG.Rpf.View {Namespace}
