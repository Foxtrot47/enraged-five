﻿//---------------------------------------------------------------------------------------------
// <copyright file="ICommandWithIcon.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System.Windows.Media.Imaging;

    /// <summary>
    /// When implemented represents a command definition class that contains a command
    /// parameter property.
    /// </summary>
    public interface ICommandWithIcon
    {
        #region Properties
        /// <summary>
        /// Gets the System.Windows.Media.Imaging.BitmapSource object that represents the icon
        /// that is used for this command, this can be null.
        /// </summary>
        BitmapSource Icon { get; }
        #endregion Properties
    } // RSG.Editor.ICommandWithIcon {Interface}
} // RSG.Editor {Namespace}
