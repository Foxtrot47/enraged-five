﻿//---------------------------------------------------------------------------------------------
// <copyright file="Mat34VTunableViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Tunables
{
    using System.Collections.Generic;
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// The view model for a <see cref="Mat34VTunable"/> model object.
    /// </summary>
    public class Mat34VTunableViewModel : TunableViewModelBase<Mat34VTunable>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Mat34VTunableViewModel"/> class.
        /// </summary>
        /// <param name="tunable">
        /// The 3 by 4 matrix tunable that this view model is representing.
        /// </param>
        /// <param name="parent">
        /// The parent tunable view model for this view model.
        /// </param>
        public Mat34VTunableViewModel(Mat34VTunable tunable, ITunableViewModel parent)
            : base(tunable, parent)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the incremental step for the XX parameter.
        /// </summary>
        public float StepXX
        {
            get { return this.Model.Mat34VMember.Step[0]; }
        }

        /// <summary>
        /// Gets the incremental step for the XY parameter.
        /// </summary>
        public float StepXY
        {
            get { return this.Model.Mat34VMember.Step[1]; }
        }

        /// <summary>
        /// Gets the incremental step for the XZ parameter.
        /// </summary>
        public float StepXZ
        {
            get { return this.Model.Mat34VMember.Step[2]; }
        }

        /// <summary>
        /// Gets the incremental step for the XW parameter.
        /// </summary>
        public float StepXW
        {
            get { return this.Model.Mat34VMember.Step[3]; }
        }

        /// <summary>
        /// Gets the incremental step for the YX parameter.
        /// </summary>
        public float StepYX
        {
            get { return this.Model.Mat34VMember.Step[4]; }
        }

        /// <summary>
        /// Gets the incremental step for the YY parameter.
        /// </summary>
        public float StepYY
        {
            get { return this.Model.Mat34VMember.Step[5]; }
        }

        /// <summary>
        /// Gets the incremental step for the YZ parameter.
        /// </summary>
        public float StepYZ
        {
            get { return this.Model.Mat34VMember.Step[6]; }
        }

        /// <summary>
        /// Gets the incremental step for the YW parameter.
        /// </summary>
        public float StepYW
        {
            get { return this.Model.Mat34VMember.Step[7]; }
        }

        /// <summary>
        /// Gets the incremental step for the ZX parameter.
        /// </summary>
        public float StepZX
        {
            get { return this.Model.Mat34VMember.Step[8]; }
        }

        /// <summary>
        /// Gets the incremental step for the ZY parameter.
        /// </summary>
        public float StepZY
        {
            get { return this.Model.Mat34VMember.Step[9]; }
        }

        /// <summary>
        /// Gets the incremental step for the ZZ parameter.
        /// </summary>
        public float StepZZ
        {
            get { return this.Model.Mat34VMember.Step[10]; }
        }

        /// <summary>
        /// Gets the incremental step for the ZW parameter.
        /// </summary>
        public float StepZW
        {
            get { return this.Model.Mat34VMember.Step[11]; }
        }

        /// <summary>
        /// Gets or sets the value of the xw component for this matrix.
        /// </summary>
        public float XW
        {
            get { return this.Model.XW; }
            set { this.Model.XW = value; }
        }

        /// <summary>
        /// Gets or sets the value of the xx component for this matrix.
        /// </summary>
        public float XX
        {
            get { return this.Model.XX; }
            set { this.Model.XX = value; }
        }

        /// <summary>
        /// Gets or sets the value of the xy component for this matrix.
        /// </summary>
        public float XY
        {
            get { return this.Model.XY; }
            set { this.Model.XY = value; }
        }

        /// <summary>
        /// Gets or sets the value of the xz component for this matrix.
        /// </summary>
        public float XZ
        {
            get { return this.Model.XZ; }
            set { this.Model.XZ = value; }
        }

        /// <summary>
        /// Gets or sets the value of the yw component for this matrix.
        /// </summary>
        public float YW
        {
            get { return this.Model.YW; }
            set { this.Model.YW = value; }
        }

        /// <summary>
        /// Gets or sets the value of the yx component for this matrix.
        /// </summary>
        public float YX
        {
            get { return this.Model.YX; }
            set { this.Model.YX = value; }
        }

        /// <summary>
        /// Gets or sets the value of the yy component for this matrix.
        /// </summary>
        public float YY
        {
            get { return this.Model.YY; }
            set { this.Model.YY = value; }
        }

        /// <summary>
        /// Gets or sets the value of the yz component for this matrix.
        /// </summary>
        public float YZ
        {
            get { return this.Model.YZ; }
            set { this.Model.YZ = value; }
        }

        /// <summary>
        /// Gets or sets the value of the zw component for this matrix.
        /// </summary>
        public float ZW
        {
            get { return this.Model.ZW; }
            set { this.Model.ZW = value; }
        }

        /// <summary>
        /// Gets or sets the value of the zx component for this matrix.
        /// </summary>
        public float ZX
        {
            get { return this.Model.ZX; }
            set { this.Model.ZX = value; }
        }

        /// <summary>
        /// Gets or sets the value of the zy component for this matrix.
        /// </summary>
        public float ZY
        {
            get { return this.Model.ZY; }
            set { this.Model.ZY = value; }
        }

        /// <summary>
        /// Gets or sets the value of the zz component for this matrix.
        /// </summary>
        public float ZZ
        {
            get { return this.Model.ZZ; }
            set { this.Model.ZZ = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Determines whether the validation pipeline should be started due to the property
        /// with the specified name changing.
        /// </summary>
        /// <param name="propertyName">
        /// The property name to test.
        /// </param>
        /// <returns>
        /// True if the validation should be run due to the specified property changing;
        /// otherwise, false.
        /// </returns>
        protected override bool ShouldRunValidation(string propertyName)
        {
            HashSet<string> propertyNames = new HashSet<string>()
            {
                "XX", "XY", "XZ", "XW",
                "YX", "YY", "YZ", "YW",
                "ZX", "ZY", "ZZ", "ZW",
            };

            if (propertyNames.Contains(propertyName))
            {
                return true;
            }

            return base.ShouldRunValidation(propertyName);
        }
        #endregion Methods
    } // RSG.Metadata.ViewModel.Tunables.Mat34VTunableViewModel {Class}
} // RSG.Metadata.ViewModel.Tunables {Namespace}
