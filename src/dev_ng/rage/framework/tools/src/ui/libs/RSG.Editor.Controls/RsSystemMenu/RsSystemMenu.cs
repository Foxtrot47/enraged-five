﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsSystemMenu.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using RSG.Editor.Controls.Helpers;
    using RSG.Interop.Microsoft.Windows;

    /// <summary>
    /// Represents a system menu that can be placed onto a window title bar to mimic the
    /// behaviour of the windows icon. This class cannot be inherited.
    /// </summary>
    public sealed class RsSystemMenu : Control, INonClientArea
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="Source"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty SourceProperty;

        /// <summary>
        /// Identifies the <see cref="VectorFill"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty VectorFillProperty;

        /// <summary>
        /// Identifies the <see cref="VectorIcon"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty VectorIconProperty;

        /// <summary>
        /// The image source that is used to render the icon. This source is created using
        /// the <see cref="Source"/> dependency property and the current render size.
        /// </summary>
        private ImageSource _optimalImageForSize;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsSystemMenu"/> class.
        /// </summary>
        static RsSystemMenu()
        {
            SourceProperty =
                Image.SourceProperty.AddOwner(
                typeof(RsSystemMenu),
                new FrameworkPropertyMetadata(
                    new PropertyChangedCallback(OnSourceChanged)));

            VectorFillProperty
                = DependencyProperty.Register(
                "VectorFill",
                typeof(Brush),
                typeof(RsSystemMenu));

            VectorIconProperty =
                DependencyProperty.Register(
                "VectorIcon",
                typeof(Geometry),
                typeof(RsSystemMenu),
                new FrameworkPropertyMetadata(
                    null, FrameworkPropertyMetadataOptions.AffectsRender));

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsSystemMenu), new FrameworkPropertyMetadata(typeof(RsSystemMenu)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsSystemMenu"/> class.
        /// </summary>
        public RsSystemMenu()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the source for the image that will be rendered as the icon.
        /// </summary>
        public ImageSource Source
        {
            get { return (ImageSource)this.GetValue(SourceProperty); }
            set { this.SetValue(SourceProperty, value); }
        }

        /// <summary>
        /// Gets or sets the brush that is used to paint the vector icon if one is set.
        /// </summary>
        public Brush VectorFill
        {
            get { return (Brush)this.GetValue(VectorFillProperty); }
            set { this.SetValue(VectorFillProperty, value); }
        }

        /// <summary>
        /// Gets or sets the geometry to use for the vector to be displayed as the icon if
        /// the source image isn't set.
        /// </summary>
        public Geometry VectorIcon
        {
            get { return (Geometry)this.GetValue(VectorIconProperty); }
            set { this.SetValue(VectorIconProperty, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Tests the specified point coordinate and returns a Hit Test result based on the
        /// system hot spot.
        /// </summary>
        /// <param name="point">
        /// The point to test.
        /// </param>
        /// <returns>
        /// A result indicating the position of the cursor hot spot.
        /// </returns>
        NcHitTestResult INonClientArea.HitTest(Point point)
        {
            return NcHitTestResult.CLIENT;
        }

        /// <summary>
        /// Called when the mouse is double clicked on this UI item.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.MouseButtonEventArgs data for this event.
        /// </param>
        protected override void OnMouseDoubleClick(MouseButtonEventArgs e)
        {
            if (e == null)
            {
                throw new SmartArgumentNullException(() => e);
            }

            RsWindow window = this.GetVisualAncestor<RsWindow>();
            if (window == null)
            {
                return;
            }

            if (!window.CanCloseWindowWithButton)
            {
                return;
            }

            window.Close();
            e.Handled = true;
        }

        /// <summary>
        /// Called whenever the left mouse button is pressed while the mouse is over
        /// this UI element.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.MouseButtonEventArgs data for this event.
        /// </param>
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            if (e == null)
            {
                throw new SmartArgumentNullException(() => e);
            }

            if (e.ClickCount != 1)
            {
                return;
            }

            RsWindow window = this.GetVisualAncestor<RsWindow>();
            if (window == null || window.ShowingSystemIconMenu)
            {
                window.ShowingSystemIconMenu = false;
                return;
            }

            Point position = new Point(8, 29);
            window.ShowWindowMenu(window, position, window.RenderSize);
            window.ShowingSystemIconMenu = true;
            e.Handled = true;
        }

        /// <summary>
        /// Gets called whenever the control needs to be rendered to the screen.
        /// </summary>
        /// <param name="drawingContext">
        /// The drawing context to use while drawing.
        /// </param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (drawingContext == null)
            {
                throw new SmartArgumentNullException(() => drawingContext);
            }

            if (this.VectorIcon != null)
            {
                return;
            }

            this.CoerceOptimalImageForSize();
            if (this._optimalImageForSize == null)
            {
                return;
            }

            Rect region = new Rect(new Point(0.0, 0.0), this.RenderSize);
            drawingContext.DrawImage(this._optimalImageForSize, region);
        }

        /// <summary>
        /// Called whenever the render size for this control changes.
        /// </summary>
        /// <param name="sizeInfo">
        /// A System.Windows.SizeChangedInfo instance that describes the new size properties.
        /// </param>
        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            base.OnRenderSizeChanged(sizeInfo);
            this._optimalImageForSize = null;
        }

        /// <summary>
        /// Gets called whenever the <see cref="Source"/> dependency property or the
        /// <see cref="Source"/> dependency property changes on a <see cref="RsSystemMenu"/>
        /// instance.
        /// </summary>
        /// <param name="s">
        /// The <see cref="RsSystemMenu"/> instance whose dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data used for this event.
        /// </param>
        private static void OnSourceChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            RsSystemMenu systemMenu = s as RsSystemMenu;
            if (systemMenu == null)
            {
                return;
            }

            systemMenu._optimalImageForSize = null;
        }

        /// <summary>
        /// Creates the image that will be rendered from the <see cref="Source"/> property
        /// based on the actual render size.
        /// </summary>
        private void CoerceOptimalImageForSize()
        {
            if (this._optimalImageForSize != null)
            {
                return;
            }

            BitmapFrame frame = this.Source as BitmapFrame;
            int num = (int)this.RenderSize.LogicalToDeviceUnits().Width;
            int num2 = -1;
            if (frame == null)
            {
                this._optimalImageForSize = this.Source;
                return;
            }

            using (IEnumerator<BitmapFrame> enumerator = frame.Decoder.Frames.GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    BitmapSource current = enumerator.Current;
                    int pixelWidth = current.PixelWidth;
                    if (pixelWidth == num)
                    {
                        this._optimalImageForSize = current;
                        break;
                    }

                    if (pixelWidth > num)
                    {
                        if (num2 < num || pixelWidth < num2)
                        {
                            num2 = pixelWidth;
                            this._optimalImageForSize = current;
                        }
                    }
                    else
                    {
                        if (pixelWidth > num2)
                        {
                            num2 = pixelWidth;
                            this._optimalImageForSize = current;
                        }
                    }
                }
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsSystemMenu {Class}
} // RSG.Editor.Controls {Namespace}
