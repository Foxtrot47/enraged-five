﻿//---------------------------------------------------------------------------------------------
// <copyright file="SmartArgumentNullException.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Linq.Expressions;
    using System.Runtime.Serialization;
    using RSG.Editor.Resources;

    /// <summary>
    /// The exception that is thrown when a null reference is found in a public method that
    /// doesn't expect it as a valid argument.
    /// </summary>
    [Serializable]
    public class SmartArgumentNullException : Exception
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SmartArgumentNullException"/> class
        /// using the default message.
        /// </summary>
        public SmartArgumentNullException()
            : base(ErrorStringTable.GetString("ArgumentNull"))
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="SmartArgumentNullException"/> class
        /// using the default
        /// message.
        /// </summary>
        /// <param name="argument">
        /// A reference to the argument that is null.
        /// </param>
        public SmartArgumentNullException(Expression<SimpleFunctionDelegate> argument)
            : base(GetMessage(argument))
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="SmartArgumentNullException"/> class
        /// with the specified message.
        /// </summary>
        /// <param name="message">
        /// The message that describes the error.
        /// </param>
        public SmartArgumentNullException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="SmartArgumentNullException"/> class
        /// with the specified inner exception and the default message.
        /// </summary>
        /// <param name="innerException">
        /// The exception that is the cause of this current exception, or a null reference if
        /// no inner exception is specified.
        /// </param>
        public SmartArgumentNullException(Exception innerException)
            : base(ErrorStringTable.GetString("ArgumentNull"), innerException)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="SmartArgumentNullException"/> class
        /// with the specified inner exception and the default message.
        /// </summary>
        /// <param name="argument">
        /// A reference to the argument that is null.
        /// </param>
        /// <param name="innerException">
        /// The exception that is the cause of this current exception, or a null reference if
        /// no inner exception is specified.
        /// </param>
        public SmartArgumentNullException(
            Expression<SimpleFunctionDelegate> argument, Exception innerException)
            : base(GetMessage(argument), innerException)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="SmartArgumentNullException"/> class
        /// with the specified message and inner exception.
        /// </summary>
        /// <param name="message">
        /// The message that describes the error.
        /// </param>
        /// <param name="innerException">
        /// The exception that is the cause of this current exception, or a null reference if
        /// no inner exception is specified.
        /// </param>
        public SmartArgumentNullException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="SmartArgumentNullException"/> class
        /// with serialised data.
        /// </summary>
        /// <param name="info">
        /// The System.Runtime.Serialization.SerializationInfo that holds the serialised
        /// object data about the exception being thrown.
        /// </param>
        /// <param name="context">
        /// The System.Runtime.Serialization.StreamingContext that contains contextual
        /// information about the source or destination.
        /// </param>
        protected SmartArgumentNullException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Gets the name of the specified expressions body.
        /// </summary>
        /// <param name="expression">
        /// The expression that the name will be returned for.
        /// </param>
        /// <returns>
        /// The name of the specified expressions body.
        /// </returns>
        private static string GetMemberName(Expression<SimpleFunctionDelegate> expression)
        {
            MemberExpression expressionBody = (MemberExpression)expression.Body;
            return expressionBody.Member.Name;
        }

        /// <summary>
        /// Gets the message to go with this exception.
        /// </summary>
        /// <param name="expression">
        /// The argument whose name will be included in the message.
        /// </param>
        /// <returns>
        /// The message to go with this exception.
        /// </returns>
        private static string GetMessage(Expression<SimpleFunctionDelegate> expression)
        {
            return
                ErrorStringTable.GetString("ArgumentNullWithName", GetMemberName(expression));
        }
        #endregion Methods
    } // RSG.Editor.SmartArgumentNullException {Class}
} // RSG.Editor {Namespace}
