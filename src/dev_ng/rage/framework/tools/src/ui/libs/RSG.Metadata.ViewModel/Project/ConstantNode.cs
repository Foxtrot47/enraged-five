﻿//---------------------------------------------------------------------------------------------
// <copyright file="ConstantNode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Project
{
    using RSG.Metadata.Model.Definitions;
    using RSG.Project.ViewModel;

    /// <summary>
    /// The node inside the project explorer that represents a parCodeGen constant definition.
    /// This class cannot be inherited.
    /// </summary>
    public sealed class ConstantNode : HierarchyNode
    {
        #region Fields
        /// <summary>
        /// The constant that this node is representing.
        /// </summary>
        private IConstant _constant;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ConstantNode"/> class.
        /// </summary>
        /// <param name="constant">
        /// The constant object this node is representing.
        /// </param>
        /// <param name="parent">
        /// The parent node.
        /// </param>
        public ConstantNode(IConstant constant, IHierarchyNode parent)
            : base(false)
        {
            this.IsExpandable = false;
            this._constant = constant;
            this.Parent = parent;
            this.Icon = Icons.ConstantNodeIcon;
            this.Text = constant.Name;
        }
        #endregion Constructors
    } // RSG.Metadata.ViewModel.Project.ConstantNode {Class}
} // RSG.Metadata.ViewModel.Project {Namespace}
