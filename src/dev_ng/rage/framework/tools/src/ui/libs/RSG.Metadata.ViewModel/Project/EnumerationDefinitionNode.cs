﻿//---------------------------------------------------------------------------------------------
// <copyright file="EnumerationDefinitionNode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Project
{
    using System.Collections.Generic;
    using RSG.Metadata.Model.Definitions;
    using RSG.Project.ViewModel;

    /// <summary>
    /// The node inside the project explorer that represents a parCodeGen enumeration
    /// definition. This class cannot be inherited.
    /// </summary>
    public sealed class EnumerationDefinitionNode : HierarchyNode
    {
        #region Fields
        /// <summary>
        /// The enumeration that this node is representing.
        /// </summary>
        private IEnumeration _enum;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="EnumerationDefinitionNode"/> class.
        /// </summary>
        /// <param name="enumeration">
        /// The enumeration object this node is representing.
        /// </param>
        /// <param name="parent">
        /// The parent node.
        /// </param>
        public EnumerationDefinitionNode(IEnumeration enumeration, IHierarchyNode parent)
            : base(false)
        {
            this._enum = enumeration;
            this.Parent = parent;
            this.Icon = Icons.EnumNodeIcon;
            this.Text = enumeration.DataType;
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Creates the child elements for this view model. This is only called once the item
        /// has been expanded in the user interface.
        /// </summary>
        /// <param name="collection">
        /// The collection to add the elements to.
        /// </param>
        protected override void CreateChildren(ICollection<IHierarchyNode> collection)
        {
            foreach (IEnumConstant constant in this._enum.EnumConstants)
            {
                collection.Add(new EnumerationConstantNode(constant, this));
            }
        }
        #endregion Methods
    } // RSG.Metadata.ViewModel.Project.EnumerationDefinitionNode {Class}
} // RSG.Metadata.ViewModel.Project {Namespace}
