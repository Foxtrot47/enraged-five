﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Automation.Commands;
using RSG.Automation.ViewModel;
using RSG.Automation.View.Commands;
using RSG.Base.Extensions;
using RSG.Editor;
using RSG.Editor.Controls;
using RSG.Pipeline.Automation.Common.Jobs;

namespace RSG.Automation.View
{
    /// <summary>
    /// Interaction logic for JobDataGridControl.xaml
    /// </summary>
    public partial class JobDataGridControl : RsUserControl
    {
        #region Properties
        /// <summary>
        /// Gets or sets the Job States that are currently allowed to be shown.
        /// </summary>
        public ISet<JobState> DisplayedJobStates
        {
            get
            {
                return this._displayedJobStates;
            }
        }
        private ISet<JobState> _displayedJobStates;

        /// <summary>
        /// Sets the member which is initially sorted ascending.
        /// </summary>
        public InitialJobSortMember InitialSortMember
        {
            set { this._initialSortMember = value; }
        }
        private InitialJobSortMember _initialSortMember;

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<JobViewModel> SelectedItems
        {
            get
            {
                return (IEnumerable<JobViewModel>)this.GetValue(SelectedItemsProperty);
            }

            set
            {
                throw new Exception("An attempt to modify a Read-Only property was made.");
            }
        }

        /// <summary>
        /// Gets or sets the dependency property for the <see cref="SelectedItems"/> property.
        /// </summary>
        public static DependencyProperty SelectedItemsProperty
        {
            get { return _selectedItemsProperty; }
            set { _selectedItemsProperty = value; }
        }
        private static DependencyProperty _selectedItemsProperty;

        /// <summary>
        /// Gets or sets a collection used to generate the content of this control.
        /// </summary>
        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)this.GetValue(ItemsControl.ItemsSourceProperty); }
            set { this.SetValue(ItemsControl.ItemsSourceProperty, value); }
        }

        /// <summary>
        /// Filter by log level action.
        /// </summary>
        public FilterByJobStateAction FilterByJobStateAction
        {
            get { return _filterByJobStateAction; }
        }
        private FilterByJobStateAction _filterByJobStateAction;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Static constructor.
        /// </summary>
        static JobDataGridControl()
        {
            SelectedItemsProperty = DependencyProperty.Register("SelectedItems",
                typeof(IEnumerable<JobViewModel>),
                typeof(JobDataGridControl),
                new FrameworkPropertyMetadata(Enumerable.Empty<JobViewModel>()));
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public JobDataGridControl()
        {
            InitializeComponent();

            // Initialise job filter states.
            _displayedJobStates = new HashSet<JobState>();
            _displayedJobStates.AddRange((JobState[])Enum.GetValues(typeof(JobState)));
            
            DataGridColumn columnToSortInitially = this.JobDataGrid.Columns.FirstOrDefault(n => n.Header.Equals("Submitted At"));
            columnToSortInitially.SortDirection = ListSortDirection.Descending;
            _initialSortMember = InitialJobSortMember.SubmittedAt;

            AttachCommandBindings();
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// Represents the method that is used to retrieve a command parameter object for the
        /// view commands.
        /// </summary>
        /// <param name="command">
        /// The command whose command parameter needs to be resolved.
        /// </param>
        /// <returns>
        /// The command parameter object for the specified command.
        /// </returns>
        private JobDataGridControl ControlResolver(CommandData data)
        {
            return this;
        }

        /// <summary>
        /// Attaches all of the necessary command bindings to this instance.
        /// </summary>
        private void AttachCommandBindings()
        {
            _filterByJobStateAction = new FilterByJobStateAction(this.ControlResolver);
            _filterByJobStateAction.AddBinding(AutomationMonitorCommands.FilterByJobState, this);
        }

        /*
        /// <summary>
        /// Called whenever the <see cref="ItemsSource"/> dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The object whose <see cref="ItemsSource"/> dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnItemsSourceChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            JobDataGridControl jobGrid = s as JobDataGridControl;
            if (jobGrid == null)
            {
                return;
            }

            if (e.OldValue == null)
            {
                string propertyName = String.Empty;
                switch (jobGrid._initialSortMember)
                {
                    case InitialJobSortMember.Changelist:
                        propertyName = "Changelist";
                        break;
                    case InitialJobSortMember.DownloadProgress:
                        propertyName = "DownloadProgress";
                        break;
                    case InitialJobSortMember.DownloadSize:
                        propertyName = "DownloadSize";
                        break;
                    case InitialJobSortMember.Id:
                        propertyName = "Id";
                        break;
                    case InitialJobSortMember.Priority:
                        propertyName = "Priority";
                        break;
                    case InitialJobSortMember.ProcessedAt:
                        propertyName = "ProcessedAt";
                        break;
                    case InitialJobSortMember.State:
                        propertyName = "State";
                        break;
                    case InitialJobSortMember.SubmittedAt:
                        propertyName = "SubmittedAt";
                        break;
                    case InitialJobSortMember.Username:
                        propertyName = "Username";
                        break;
                    default:
                        return;
                }

                DataGridColumn sortColumn = null;
                foreach (DataGridColumn column in jobGrid.JobDataGrid.Columns)
                {
                    if (column.SortDirection != null)
                    {
                        return;
                    }

                    if (String.CompareOrdinal(column.SortMemberPath, propertyName) == 0)
                    {
                        sortColumn = column;
                        break;
                    }
                }

                if (sortColumn == null)
                {
                    return;
                }

                sortColumn.SortDirection = ListSortDirection.Ascending;
                jobGrid.JobDataGrid.Items.SortDescriptions.Clear();
                SortDescription description =
                    new SortDescription(propertyName, ListSortDirection.Ascending);
                jobGrid.JobDataGrid.Items.SortDescriptions.Add(description);
            }
            else
            {
                ListSortDirection direction = ListSortDirection.Ascending;
                DataGridColumn sortColumn = null;
                foreach (DataGridColumn column in jobGrid.DataGrid.Columns)
                {
                    if (column.SortDirection == null)
                    {
                        continue;
                    }

                    direction = (ListSortDirection)column.SortDirection;
                    sortColumn = column;
                    break;
                }

                if (sortColumn == null)
                {
                    return;
                }

                jobGrid.Dispatcher.BeginInvoke(
                    new Action(
                    delegate
                    {
                        sortColumn.SortDirection = direction;
                        jobGrid.DataGrid.Items.SortDescriptions.Clear();
                        SortDescription description =
                            new SortDescription(sortColumn.SortMemberPath, direction);
                        jobGrid.DataGrid.Items.SortDescriptions.Add(description);

                        jobGrid.UpdateSelectedItems();
                    }),
                    System.Windows.Threading.DispatcherPriority.Render);
            }
                 
        }
        * */

        /// <summary>
        /// Event handler for Job DataGrid selection changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataGrid_SelectionChanged(Object sender, SelectionChangedEventArgs e)
        {
            Debug.Assert(this.DataContext is AutomationMonitorDataContext);
            this.SetValue(SelectedItemsProperty,
                this.JobDataGrid.SelectedItems.OfType<JobViewModel>());
        }
        #endregion // Private Methods

        #region Filtering Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="state"></param>
        internal void AddAllowedFilterLevel(JobState state)
        {
            _displayedJobStates.Add(state);
            ForceFilter();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="state"></param>
        internal void RemoveAllowedFilterLevel(JobState state)
        {
            _displayedJobStates.Remove(state);

            ForceFilter();
        }

        /// <summary>
        /// Forces a re-filter of the shown items.
        /// </summary>
        private void ForceFilter()
        {
            ICollectionView items = CollectionViewSource.GetDefaultView(JobDataGrid.Items);
            if (items != null)
            {
                items.Filter = this.FilterItem;
            }
        }

        /// <summary>
        /// A method that determines whether the specified item can be viewed in the data grid
        /// control or not.
        /// </summary>
        /// <param name="item">The item to test.</param>
        /// <returns>True if the specified item can be viewed in the data grid; otherwise, false.</returns>
        private bool FilterItem(object item)
        {
            JobViewModel viewModel = item as JobViewModel;
            if (viewModel == null)
            {
                return false;
            }

            // Check whether we can show this job
            return _displayedJobStates.Contains(viewModel.State);
        }
        #endregion // Filtering Methods
    }

} // RSG.Automation.View namespace
