﻿//---------------------------------------------------------------------------------------------
// <copyright file="FloatTunableViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Tunables
{
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// The view model for a <see cref="FloatTunable"/> model object.
    /// </summary>
    public class FloatTunableViewModel : TunableViewModelBase<FloatTunable>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="FloatTunableViewModel"/> class.
        /// </summary>
        /// <param name="tunable">
        /// The float tunable that this view model is representing.
        /// </param>
        /// <param name="parent">
        /// The parent tunable view model for this view model.
        /// </param>
        public FloatTunableViewModel(FloatTunable tunable, ITunableViewModel parent)
            : base(tunable, parent)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the value currently assigned to the float tunable.
        /// </summary>
        public float Value
        {
            get { return this.Model.Value; }
            set { this.Model.Value = value; }
        }
        #endregion Properties
    } // RSG.Metadata.ViewModel.Tunables.FloatTunableViewModel {Class}
} // RSG.Metadata.ViewModel.Tunables {Namespace}
