﻿//---------------------------------------------------------------------------------------------
// <copyright file="MetadataCommandArgsResolver.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Commands
{
    using RSG.Editor;

    /// <summary>
    /// Represents the method that is used to retrieve a metadata command argument object
    /// for the specified command data.
    /// </summary>
    /// <param name="commandData">
    /// The data that was sent with the command containing among other things the command
    /// whose command parameter needs to be resolved.
    /// </param>
    /// <returns>
    /// The metadata command argument object for the specified command.
    /// </returns>
    public delegate MetadataCommandArgs MetadataCommandArgsResolver(CommandData commandData);
} // RSG.Metadata.Commands {Namespace}
