﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Collections;
using RSG.Editor.Model;

namespace RSG.Editor.Controls.CurveEditor.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class Curve : ModelBase
    {
        #region Fields
        /// <summary>
        /// Name for this curve.
        /// </summary>
        private String _name;

        /// <summary>
        /// Color associated with this curve.
        /// </summary>
        private Color _color;

        /// <summary>
        /// 
        /// </summary>
        private readonly ModelCollection<Point> _points;

        /// <summary>
        /// The private field used for the <see cref="Points"/> property.
        /// </summary>
        private readonly ReadOnlyModelCollection<Point> _readOnlyPoints;

        /// <summary>
        /// 
        /// </summary>
        private readonly UndoEngine _undoEngine;
        #endregion // Fields

        #region Properties
        /// <summary>
        /// Name for this curve.
        /// </summary>
        public String Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }

        /// <summary>
        /// Color to use when displaying this curve.
        /// </summary>
        public Color Color
        {
            get { return _color; }
            set { SetProperty(ref _color, value); }
        }

        /// <summary>
        /// List of points this curve contains.
        /// </summary>
        public ReadOnlyModelCollection<Point> Points
        {
            get { return _readOnlyPoints; }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlElem"></param>
        public Curve(IModel parent, XElement xmlElem)
            : base(parent)
        {
            // Parse the curve's name.
            XElement xmlNameElem = xmlElem.Element("Name");
            Debug.Assert(xmlNameElem != null, "Curve XElement is missing the Name element.");
            if (xmlNameElem != null)
            {
                _name = xmlNameElem.Value;
            }

            // Parse the points.
            _points = new ModelCollection<Point>(this);
            foreach (XElement pointElem in xmlElem.XPathSelectElements("Points/Point"))
            {
                _points.Add(new Point(this, pointElem));
            }
            _readOnlyPoints = new ReadOnlyModelCollection<Point>(_points);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="undoEngine"></param>
        /// <param name="xmlElem"></param>
        public Curve(UndoEngine undoEngine, XElement xmlElem)
            : this((IModel)null, xmlElem)
        {
            _undoEngine = undoEngine;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="color"></param>
        public Curve(String name, Color color)
        {
            _name = name;
            _color = color;
            _points = new ModelCollection<Point>(this);
            _readOnlyPoints = new ReadOnlyModelCollection<Point>(_points);
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// Gets the undo engine this collection should use to store changes in.
        /// </summary>
        public override UndoEngine UndoEngine
        {
            get { return _undoEngine ?? (Parent != null ? Parent.UndoEngine : null); }
        }
        #endregion // Overrides

        #region Serialisation
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public XElement ToXElement(XName name)
        {
            return
                new XElement(name,
                    new XElement("Name", Name),
                    new XElement("Points",
                        Points.Select(point => point.ToXElement("Point"))
                    )
                );
        }
        #endregion // Serialisation

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="insertionIndex"></param>
        /// <param name="newPoint"></param>
        public void InsertPoint(int insertionIndex, Point newPoint)
        {
            _points.Insert(insertionIndex, newPoint);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="point"></param>
        public void RemovePoint(Point point)
        {
            _points.Remove(point);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        public void RemovePointAt(int index)
        {
            _points.RemoveAt(index);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="points"></param>
        public void ResetPoints(IEnumerable<Point> points)
        {
            _points.Clear();
            _points.AddRange(points);
        }
        #endregion // Methods

        #region Overrides
        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Indicates whether the current object is equal to the specified object. When this
        /// method is called it has already been determined that the specified object is not
        /// equal to the current object or null through reference equality.
        /// </summary>
        /// <param name="other">
        /// An object to compare with this object.
        /// </param>
        /// <returns>
        /// True if the current object is equal to the other parameter; otherwise, false.
        /// </returns>
        protected override bool EqualsCore(IModel other)
        {
            Curve curve = (Curve)other;
            if (Points.Count != curve.Points.Count)
            {
                return false;
            }

            // Two curves are equal if all their points are the same.
            for (int i = 0; i < Points.Count; ++i)
            {
                if (!Points[i].Equals(curve.Points[i]))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return Name;
        }
        #endregion // Overrides
    } // CurveModel
}
