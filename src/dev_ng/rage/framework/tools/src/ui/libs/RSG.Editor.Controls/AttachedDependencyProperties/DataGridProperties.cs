﻿//---------------------------------------------------------------------------------------------
// <copyright file="DataGridProperties.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.AttachedDependencyProperties
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Data;
    using System.Windows.Input;
    using RSG.Editor.Controls.Commands;

    /// <summary>
    /// Contains attached properties that are related to the data grid control.
    /// </summary>
    public static class DataGridProperties
    {
        #region Fields
        /// <summary>
        /// Identifies the AllowVisibilityToggle dependency property.
        /// </summary>
        public static readonly DependencyProperty AllowVisibilityToggleProperty;

        /// <summary>
        /// Identifies the HandleResetCommand dependency property.
        /// </summary>
        public static readonly DependencyProperty HandleResetCommandProperty;

        /// <summary>
        /// Identifies the ResetSize dependency property.
        /// </summary>
        public static readonly DependencyProperty ResetSizeProperty;

        /// <summary>
        /// Identifies the ResetVisibility dependency property.
        /// </summary>
        public static readonly DependencyProperty ResetVisibilityProperty;

        /// <summary>
        /// Identifies the ShowHeaderVisibilityMenu dependency property.
        /// </summary>
        public static readonly DependencyProperty ShowHeaderVisibilityMenuProperty;

        /// <summary>
        /// The private field used for the <see cref="ResetDataGridCommand"/> property.
        /// </summary>
        private static RoutedCommand _resetDataGridCommand;

        /// <summary>
        /// The private reference to the command binding used to handle the ResetDataGrid
        /// command.
        /// </summary>
        private static CommandBinding _resetDataGridCommandBinding;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="DataGridProperties"/> class.
        /// </summary>
        static DataGridProperties()
        {
            _resetDataGridCommand =
                new RoutedCommand("ResetDataGridCommand", typeof(DataGridProperties));

            _resetDataGridCommandBinding =
                new CommandBinding(ResetDataGridCommand, ResetDataGrid);

            HandleResetCommandProperty =
                DependencyProperty.RegisterAttached(
                "HandleResetCommand",
                typeof(bool),
                typeof(DataGridProperties),
                new PropertyMetadata(false, OnHandleResetCommandChanged));

            AllowVisibilityToggleProperty =
                DependencyProperty.RegisterAttached(
                "AllowVisibilityToggle",
                typeof(bool),
                typeof(DataGridProperties),
                new PropertyMetadata(true));

            ResetSizeProperty =
                DependencyProperty.RegisterAttached(
                "ResetSize",
                typeof(DataGridLength),
                typeof(DataGridProperties),
                new PropertyMetadata(
                    new DataGridLength(1.0, DataGridLengthUnitType.Star)));

            ResetVisibilityProperty =
                DependencyProperty.RegisterAttached(
                "ResetVisibility",
                typeof(Visibility),
                typeof(DataGridProperties),
                new PropertyMetadata(Visibility.Visible));

            ShowHeaderVisibilityMenuProperty =
                DependencyProperty.RegisterAttached(
                "ShowHeaderVisibilityMenu",
                typeof(bool),
                typeof(DataGridProperties),
                new PropertyMetadata(false, OnShowHeaderVisibilityMenuChanged));
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the command that is fired when the user chooses to reset the data grids width
        /// and visibility options.
        /// </summary>
        public static RoutedCommand ResetDataGridCommand
        {
            get { return _resetDataGridCommand; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Retrieves the AllowVisibilityToggle dependency property value attached to the
        /// specified object.
        /// </summary>
        /// <param name="element">
        /// The object whose attached AllowVisibilityToggle dependency property value will be
        /// returned.
        /// </param>
        /// <returns>
        /// The AllowVisibilityToggle dependency property value attached to the specified
        /// object.
        /// </returns>
        [AttachedPropertyBrowsableForChildrenAttribute(IncludeDescendants = false)]
        [AttachedPropertyBrowsableForType(typeof(DataGridColumn))]
        public static bool GetAllowVisibilityToggle(DataGridColumn element)
        {
            return (bool)element.GetValue(AllowVisibilityToggleProperty);
        }

        /// <summary>
        /// Retrieves the HandleResetCommand dependency property value attached to the
        /// specified object.
        /// </summary>
        /// <param name="element">
        /// The object whose attached HandleResetCommand dependency property value will be
        /// returned.
        /// </param>
        /// <returns>
        /// The HandleResetCommand dependency property value attached to the specified
        /// object.
        /// </returns>
        [AttachedPropertyBrowsableForChildrenAttribute(IncludeDescendants = false)]
        [AttachedPropertyBrowsableForType(typeof(DataGrid))]
        public static bool GetHandleResetCommand(DataGrid element)
        {
            return (bool)element.GetValue(HandleResetCommandProperty);
        }

        /// <summary>
        /// Retrieves the ResetSize dependency property value attached to the specified object.
        /// </summary>
        /// <param name="element">
        /// The object whose attached ResetSize dependency property value will be returned.
        /// </param>
        /// <returns>
        /// The ResetSize dependency property value attached to the specified object.
        /// </returns>
        [AttachedPropertyBrowsableForChildrenAttribute(IncludeDescendants = false)]
        [AttachedPropertyBrowsableForType(typeof(DataGridColumn))]
        public static DataGridLength GetResetSize(DataGridColumn element)
        {
            return (DataGridLength)element.GetValue(ResetSizeProperty);
        }

        /// <summary>
        /// Retrieves the ResetVisibility dependency property value attached to the specified
        /// object.
        /// </summary>
        /// <param name="element">
        /// The object whose attached ResetVisibility dependency property value will be
        /// returned.
        /// </param>
        /// <returns>
        /// The ResetVisibility dependency property value attached to the specified object.
        /// </returns>
        [AttachedPropertyBrowsableForChildrenAttribute(IncludeDescendants = false)]
        [AttachedPropertyBrowsableForType(typeof(DataGridColumn))]
        public static Visibility GetResetVisibility(DataGridColumn element)
        {
            if (element == null)
            {
                throw new SmartArgumentNullException(() => element);
            }

            return (Visibility)element.GetValue(ResetVisibilityProperty);
        }

        /// <summary>
        /// Retrieves the ShowHeaderVisibilityMenu dependency property value attached to the
        /// specified object.
        /// </summary>
        /// <param name="element">
        /// The object whose attached ShowHeaderVisibilityMenu dependency property value will
        /// be returned.
        /// </param>
        /// <returns>
        /// The ShowHeaderVisibilityMenu dependency property value attached to the specified
        /// object.
        /// </returns>
        [AttachedPropertyBrowsableForChildrenAttribute(IncludeDescendants = false)]
        [AttachedPropertyBrowsableForType(typeof(DataGrid))]
        public static bool GetShowHeaderVisibilityMenu(DataGrid element)
        {
            return (bool)element.GetValue(ShowHeaderVisibilityMenuProperty);
        }

        /// <summary>
        /// Sets the AllowVisibilityToggle dependency property value on the specified object to
        /// the specified value.
        /// </summary>
        /// <param name="element">
        /// The object whose attached AllowVisibilityToggle dependency property value will be
        /// set.
        /// </param>
        /// <param name="value">
        /// The value to set the attached AllowVisibilityToggle dependency property to on the
        /// specified object.
        /// </param>
        public static void SetAllowVisibilityToggle(DataGridColumn element, bool value)
        {
            element.SetValue(AllowVisibilityToggleProperty, value);
        }

        /// <summary>
        /// Sets the HandleResetCommand dependency property value on the specified object to
        /// the specified value.
        /// </summary>
        /// <param name="element">
        /// The object whose attached HandleResetCommand dependency property value will be set.
        /// </param>
        /// <param name="value">
        /// The value to set the attached HandleResetCommand dependency property to on the
        /// specified object.
        /// </param>
        public static void SetHandleResetCommand(DataGrid element, bool value)
        {
            element.SetValue(HandleResetCommandProperty, value);
        }

        /// <summary>
        /// Sets the ResetSize dependency property value on the specified object to the
        /// specified value.
        /// </summary>
        /// <param name="element">
        /// The object whose attached ResetSize dependency property value will be set.
        /// </param>
        /// <param name="value">
        /// The value to set the attached ResetSize dependency property to on the specified
        /// object.
        /// </param>
        public static void SetResetSize(DataGridColumn element, DataGridLength value)
        {
            element.SetValue(ResetSizeProperty, value);
        }

        /// <summary>
        /// Sets the ResetVisibility dependency property value on the specified object to the
        /// specified value.
        /// </summary>
        /// <param name="element">
        /// The object whose attached ResetVisibility dependency property value will be set.
        /// </param>
        /// <param name="value">
        /// The value to set the attached ResetVisibility dependency property to on the
        /// specified object.
        /// </param>
        public static void SetResetVisibility(DataGridColumn element, Visibility value)
        {
            if (element == null)
            {
                throw new SmartArgumentNullException(() => element);
            }

            element.SetValue(ResetVisibilityProperty, value);
        }

        /// <summary>
        /// Sets the ShowHeaderVisibilityMenu dependency property value on the specified object
        /// to the specified value.
        /// </summary>
        /// <param name="element">
        /// The object whose attached ShowHeaderVisibilityMenu dependency property value will
        /// be set.
        /// </param>
        /// <param name="value">
        /// The value to set the attached ShowHeaderVisibilityMenu dependency property to on
        /// the specified object.
        /// </param>
        public static void SetShowHeaderVisibilityMenu(DataGridColumn element, bool value)
        {
            element.SetValue(ShowHeaderVisibilityMenuProperty, value);
        }

        /// <summary>
        /// Called whenever the HandleResetCommand dependency property changes.
        /// </summary>
        /// <param name="d">
        /// The object whose HandleResetCommand dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnHandleResetCommandChanged(
            DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            DataGrid dataGrid = d as DataGrid;
            if (dataGrid == null)
            {
                throw new NotSupportedException("HandleResetCommand only valid on DataGrids");
            }

            if ((bool)e.NewValue)
            {
                dataGrid.CommandBindings.Add(_resetDataGridCommandBinding);
            }
            else
            {
                dataGrid.CommandBindings.Remove(_resetDataGridCommandBinding);
            }
        }

        /// <summary>
        /// Called whenever the ShowHeaderVisibilityMenu dependency property changes.
        /// </summary>
        /// <param name="d">
        /// The object whose ShowHeaderVisibilityMenu dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnShowHeaderVisibilityMenuChanged(
            DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            DataGrid dataGrid = d as DataGrid;
            if (dataGrid == null)
            {
                throw new NotSupportedException(
                    "ShowHeaderVisibilityMenu only valid on DataGrids");
            }

            if ((bool)e.NewValue)
            {
                RsContextMenu menu = new RsContextMenu();
                CompositeCollection itemsSource = new CompositeCollection();

                CollectionContainer container = new CollectionContainer();
                Binding binding = new Binding("Columns");
                binding.Source = dataGrid;
                BindingOperations.SetBinding(
                    container, CollectionContainer.CollectionProperty, binding);

                RsMenuItem resetItem = new RsMenuItem();
                resetItem.Command = ResetDataGridCommand;
                resetItem.Header = "Reset Width & Visibility";
                Binding targetBinding = new Binding("PlacementTarget");
                targetBinding.Source = menu;
                resetItem.SetBinding(MenuItem.CommandTargetProperty, targetBinding);

                itemsSource.Add(container);
                itemsSource.Add(new Separator());
                itemsSource.Add(resetItem);
                menu.ItemsSource = itemsSource;
                menu.ItemContainerStyleSelector = new HeaderContextMenuStyleSelector();

                Style style = new Style(typeof(DataGridColumnHeader));
                style.BasedOn = (Style)dataGrid.FindResource(typeof(DataGridColumnHeader));
                style.Setters.Add(new Setter(FrameworkElement.ContextMenuProperty, menu));
                dataGrid.Resources.Add(typeof(DataGridColumnHeader), style);

                SetHandleResetCommand(dataGrid, true);
            }
            else
            {
                dataGrid.Resources.Remove(typeof(DataGridColumnHeader));
                SetHandleResetCommand(dataGrid, false);
            }
        }

        /// <summary>
        /// Called when the user executes the ResetDataGrid command. This resets the data grid
        /// that is the source for this execution.
        /// </summary>
        /// <param name="s">
        /// The object that caught the ResetDataGrid command.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.ExecutedRoutedEventArgs containing the event data.
        /// </param>
        private static void ResetDataGrid(object s, ExecutedRoutedEventArgs e)
        {
            DataGrid dataGrid = s as DataGrid;
            if (dataGrid == null)
            {
                return;
            }

            foreach (DataGridColumn column in dataGrid.Columns)
            {
                column.Visibility = DataGridProperties.GetResetVisibility(column);
                column.Width = DataGridProperties.GetResetSize(column);
            }
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// Applies styles based on logic for the items inside the context menu of a data grid
        /// header.
        /// </summary>
        private class HeaderContextMenuStyleSelector : StyleSelector
        {
            #region Methods
            /// <summary>
            /// Gets the style to be applied to the specified container with the specified
            /// item.
            /// </summary>
            /// <param name="item">
            /// The data content of the specified container.
            /// </param>
            /// <param name="container">
            /// The element to which the style will be applied.
            /// </param>
            /// <returns>
            /// A specific style to apply, or null.
            /// </returns>
            public override Style SelectStyle(object item, DependencyObject container)
            {
                FrameworkElement element = container as FrameworkElement;
                if (item is DataGridColumn)
                {
                    return element.FindResource("ColumnVisibilityItemStyle") as Style;
                }

                return null;
            }
            #endregion Methods
        } // DataGridProperties.HeaderContextMenuStyleSelector {Class}
        #endregion Classes
    } // RSG.Editor.Controls.AttachedDependencyProperties.DataGridProperties {Class}
} // RSG.Editor.Controls.AttachedDependencyProperties {Namespace}
