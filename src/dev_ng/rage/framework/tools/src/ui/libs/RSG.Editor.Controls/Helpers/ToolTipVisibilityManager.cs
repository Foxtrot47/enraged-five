﻿//---------------------------------------------------------------------------------------------
// <copyright file="ToolTipVisibilityManager.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Helpers
{
    using System.ComponentModel;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Input;

    /// <summary>
    /// A utility class that manages the visibility of the tooltip for a specified element
    /// using a <see cref="IToolTipVisibilityController"/> instance.
    /// </summary>
    public sealed class ToolTipVisibilityManager
    {
        #region Fields
        /// <summary>
        /// The tooltip controller for this manager that is used to retrieve the tooltips
        /// content and determine whether the tooltip should be shown.
        /// </summary>
        private IToolTipVisibilityController _controller;

        /// <summary>
        /// The private reference to the element this tool tip manager is attached to.
        /// </summary>
        private FrameworkElement _element;

        /// <summary>
        /// The content presenter being used to display the tool tip.
        /// </summary>
        private ContentPresenter _tooltipContentPresenter;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ToolTipVisibilityManager"/> class.
        /// </summary>
        /// <param name="element">
        /// The element this manager is attached to.
        /// </param>
        /// <param name="controller">
        /// The controller that contains the tooltips content.
        /// </param>
        public ToolTipVisibilityManager(
            FrameworkElement element, IToolTipVisibilityController controller)
        {
            if (element == null)
            {
                throw new SmartArgumentNullException(() => element);
            }

            this._element = element;
            this._controller = controller;

            ToolTipEventHandler handler = new ToolTipEventHandler(this.OnToolTipOpening);
            this._element.AddHandler(FrameworkElement.ToolTipOpeningEvent, handler);
            this._tooltipContentPresenter = new ContentPresenter();
            this._element.ToolTip = new ToolTip
            {
                Placement = PlacementMode.Bottom,
                Content = this._tooltipContentPresenter
            };
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Removes the opening handler and destroys the content of the tooltip.
        /// </summary>
        public void DestroyToolTip()
        {
            ToolTipEventHandler handler = new ToolTipEventHandler(this.OnToolTipOpening);
            this._element.RemoveHandler(FrameworkElement.ToolTipOpeningEvent, handler);
            this._element.ToolTip = null;
        }

        /// <summary>
        /// Sets up a horizontal and vertical offset to the tooltip.
        /// </summary>
        /// <param name="horizontal">
        /// The horizontal offset to the start of the tooltip.
        /// </param>
        /// <param name="vertical">
        /// The vertical offset to the start of the tooltip.
        /// </param>
        public void SetToolTipOffsets(double horizontal, double vertical)
        {
            ToolTip toolTip = this._element.ToolTip as ToolTip;
            if (toolTip != null)
            {
                toolTip.HorizontalOffset = horizontal;
                toolTip.VerticalOffset = vertical;
            }
        }

        /// <summary>
        /// Sets the tooltip placement.
        /// </summary>
        /// <param name="mode">
        /// The placement mode for the tooltip.
        /// </param>
        public void SetToolTipPlacement(PlacementMode mode)
        {
            ToolTip toolTip = this._element.ToolTip as ToolTip;
            if (toolTip != null)
            {
                toolTip.Placement = mode;
            }
        }

        /// <summary>
        /// Sets the target element for the tooltip.
        /// </summary>
        /// <param name="placementTarget">
        /// The placement element for the tooltip.
        /// </param>
        public void SetToolTipTarget(UIElement placementTarget)
        {
            ToolTip toolTip = this._element.ToolTip as ToolTip;
            if (toolTip != null)
            {
                toolTip.PlacementTarget = placementTarget;
            }
        }

        /// <summary>
        /// Called whenever the tooltip is opening for the attached presenter.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Controls.ToolTipEventArgs data used for the event.
        /// </param>
        private void OnToolTipOpening(object sender, ToolTipEventArgs e)
        {
            try
            {
                Point position = Mouse.GetPosition(this._element);
                e.Handled = !this._controller.CanShowToolTip(position);
                if (e.Handled)
                {
                    return;
                }

                this._tooltipContentPresenter.Content = this._controller.GetToolTipContent();
            }
            catch (Win32Exception)
            {
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.Helpers.ToolTipVisibilityManager {Class}
} // RSG.Editor.Controls.Helpers {Namespace}
