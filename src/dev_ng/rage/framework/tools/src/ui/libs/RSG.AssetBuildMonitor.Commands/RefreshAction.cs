﻿//---------------------------------------------------------------------------------------------
// <copyright file="OpenLogAction.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.AssetBuildMonitor.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using RSG.AssetBuildMonitor.ViewModel;
    using RSG.AssetBuildMonitor.ViewModel.Project;
    using RSG.Editor;
    using RSG.Editor.Controls.Dock.ViewModel;

    /// <summary>
    /// Contains the logic for the <see cref="Commands.Refresh"/> routed
    /// command. This class cannot be inherited.
    /// </summary>
    public class RefreshAction : ButtonAction<CommandArgs>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="RefreshAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public RefreshAction(CommandResolver resolver)
            : base(new ParameterResolverDelegate<CommandArgs>(resolver))
        {
        }
        #endregion // Constructor(s)

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(CommandArgs args)
        {
            return (null != args.ActiveDocument);
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(CommandArgs args)
        {
            AssetBuildHistoryViewModel activeVM = args.GetActiveDocumentViewModel();
            activeVM.Refresh();
        }
        #endregion // Methods
    }

} // RSG.AssetBuildMonitor.Commands namespace
