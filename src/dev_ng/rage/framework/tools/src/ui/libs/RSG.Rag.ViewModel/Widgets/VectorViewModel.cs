﻿using RSG.Base;
using RSG.Rag.Widgets;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.ViewModel.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    public class VectorViewModel : WidgetViewModel<WidgetVector>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        public VectorViewModel(WidgetVector widget)
            : base(widget)
        {
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Property for the view to determine whether it needs to show the Z component.
        /// </summary>
        public bool ShowZ
        {
            get { return (Widget.NumComponents == 3 || Widget.NumComponents == 4); }
        }

        /// <summary>
        /// Property for the view to determine whether it needs to show the W component.
        /// </summary>
        public bool ShowW
        {
            get { return Widget.NumComponents == 4; }
        }

        /// <summary>
        /// X component.
        /// </summary>
        public float X
        {
            get { return Widget.Value[0]; }
            set
            {
                float[] newValue = new float[Widget.NumComponents];
                Array.Copy(Widget.Value, newValue, Widget.NumComponents);
                newValue[0] = value;
                Widget.Value = newValue;
            }
        }

        /// <summary>
        /// Y component.
        /// </summary>
        public float Y
        {
            get { return Widget.Value[1]; }
            set
            {
                float[] newValue = new float[Widget.NumComponents];
                Array.Copy(Widget.Value, newValue, Widget.NumComponents);
                newValue[1] = value;
                Widget.Value = newValue;
            }
        }

        /// <summary>
        /// Z component.
        /// </summary>
        public float Z
        {
            get { return (Widget.NumComponents > 2 ? Widget.Value[2] : 0.0f); }
            set
            {
                float[] newValue = new float[Widget.NumComponents];
                Array.Copy(Widget.Value, newValue, Widget.NumComponents);
                newValue[2] = value;
                Widget.Value = newValue;
            }
        }

        /// <summary>
        /// W component.
        /// </summary>
        public float W
        {
            get { return (Widget.NumComponents > 3 ? Widget.Value[3] : 0.0f); }
            set
            {
                float[] newValue = new float[Widget.NumComponents];
                Array.Copy(Widget.Value, newValue, Widget.NumComponents);
                newValue[3] = value;
                Widget.Value = newValue;
            }
        }

        /// <summary>
        /// Flag indicating whether the widget has changed value from it's original one.
        /// </summary>
        public bool Modified
        {
            get { return !StructuralEqualityComparer<float[]>.Default.Equals(Widget.Value, Widget.OriginalValue); }
        }

        /// <summary>
        /// Minimum value and component of this vector can have.
        /// </summary>
        public float Minimum
        {
            get { return Widget.Minimum; }
        }

        /// <summary>
        /// Maximum value any component of this vector can have.
        /// </summary>
        public float Maximum
        {
            get { return Widget.Maximum; }
        }

        /// <summary>
        /// Step to use in the spinner control.
        /// </summary>
        public float Step
        {
            get { return Widget.Step; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override bool IsModified
        {
            get
            {
                return !StructuralEqualityComparer<float[]>.Default.Equals(Widget.Value, Widget.OriginalValue);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override string DefaultValue
        {
            get
            {
                String defaultValue = String.Format("X:{0} Y:{1}", Widget.OriginalValue[0], Widget.OriginalValue[1]);
                if (Widget.NumComponents > 2)
                {
                    defaultValue += String.Format(" Z:{0}", Widget.OriginalValue[2]);
                }
                if (Widget.NumComponents > 3)
                {
                    defaultValue += String.Format(" W:{0}", Widget.OriginalValue[3]);
                }
                return defaultValue;
            }
        }
        #endregion // Properties

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        protected override void OnWidgetPropertyChanged(String propertyName)
        {
            if (propertyName == "Value")
            {
                NotifyPropertyChanged("X", "Y", "Z", "W", "IsModified");
            }
            else
            {
                base.OnWidgetPropertyChanged(propertyName);
            }
        }
        #endregion // Event Handlers
    } // VectorViewModel
}
