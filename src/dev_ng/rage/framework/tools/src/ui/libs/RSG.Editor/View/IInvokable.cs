﻿//---------------------------------------------------------------------------------------------
// <copyright file="IInvokable.cs" company="Rockstar">
//     Copyright © Rockstar Games 2012. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    /// <summary>
    /// When implemented represents a object publishes a <see cref="IInvokeController"/> that
    /// can be used to perform a action on the item.
    /// </summary>
    public interface IInvokable
    {
        #region Properties
        /// <summary>
        /// Gets the invoke controller that can be used to perform a action on this object.
        /// </summary>
        IInvokeController InvokeController { get; }
        #endregion Properties
    } // RSG.Editor.View.IInvokable {Interface}
} // RSG.Editor.View {Namespace}
