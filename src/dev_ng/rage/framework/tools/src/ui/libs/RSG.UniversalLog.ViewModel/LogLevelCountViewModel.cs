﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Editor;

namespace RSG.UniversalLog.ViewModel
{
    /// <summary>
    /// 
    /// </summary>
    public class LogLevelCountViewModel : NotifyPropertyChangedBase
    {
        #region Fields
        /// <summary>
        /// 
        /// </summary>
        private LogLevel _level;
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        public LogLevelCountViewModel(LogLevel level)
        {
            _level = level;
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Log level this vm is for.
        /// </summary>
        public LogLevel Level
        {
            get { return _level; }
        }

        /// <summary>
        /// Icon to display for this log level.
        /// </summary>
        public BitmapSource Icon
        {
            get { return _level.GetDisplayIcon(); }
        }

        /// <summary>
        /// Number of visible items.
        /// </summary>
        public int VisibleCount
        {
            get { return _visibleCount; }
            set { SetProperty(ref _visibleCount, value, "VisibleCount", "AllVisible"); }
        }
        private int _visibleCount;

        /// <summary>
        /// Total number of items.
        /// </summary>
        public int TotalCount
        {
            get { return _totalCount; }
            set { SetProperty(ref _totalCount, value, "TotalCount", "AllVisible"); }
        }
        private int _totalCount;

        /// <summary>
        /// Flag indicating whether all items are visible.
        /// </summary>
        public bool AllVisible
        {
            get { return VisibleCount == TotalCount; }
        }
        #endregion // Properties
    } // LogLevelCountViewModel
}
