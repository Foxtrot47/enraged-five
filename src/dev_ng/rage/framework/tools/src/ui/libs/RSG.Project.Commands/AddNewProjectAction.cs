﻿//---------------------------------------------------------------------------------------------
// <copyright file="AddNewProjectAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Commands
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Windows;
    using RSG.Editor;
    using RSG.Project.Model;
    using RSG.Project.ViewModel;
    using RSG.Project.ViewModel.Definitions;

    /// <summary>
    /// Contains the logic for the <see cref="ProjectCommands.AddNewProject"/> routed command.
    /// This class cannot be inherited.
    /// </summary>
    public sealed class AddNewProjectAction : ButtonAction<ProjectCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="AddNewProjectAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public AddNewProjectAction(ProjectCommandResolver resolver)
            : base(new ParameterResolverDelegate<ProjectCommandArgs>(resolver))
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ProjectCommandArgs args)
        {
            if (args == null || args.CollectionNode == null || !args.CollectionNode.Loaded)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(ProjectCommandArgs args)
        {
            IHierarchyNode parent = args.SelectedNodes.FirstOrDefault();
            ProjectCollectionNode collection = args.CollectionNode;
            if (parent == null || !parent.CanHaveProjectsAdded || args.SelectionCount > 1)
            {
                parent = args.CollectionNode;
            }

            IMessageBoxService messageService = this.GetService<IMessageBoxService>();
            IProjectAddViewService addViewService = this.GetService<IProjectAddViewService>();
            if (messageService == null || addViewService == null)
            {
                Debug.Assert(false, "Unable to add new item as services are missing.");
                return;
            }

            IProjectItemScope scope = parent.Model as IProjectItemScope;
            if (scope == null)
            {
                scope = parent.ProjectItemScope;
                if (scope == null)
                {
                    Debug.Assert(
                        false,
                        "Unable to open existing project, unable to find the correct scope.");
                    return;
                }
            }

            if (collection == null || collection.Definition == null)
            {
                return;
            }

            if (collection.Definition.ProjectDefinitions.Count == 0)
            {
                messageService.Show(
                    "No projects have been defined for this collection type",
                    null,
                    MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                return;
            }

            string directory = scope.DirectoryPath;
            ProjectDefinition selectedDefinition = null;
            string fullPath = null;
            addViewService.ShowNewProjectSelector(
                collection,
                directory,
                out selectedDefinition,
                out fullPath);
            if (selectedDefinition == null || String.IsNullOrWhiteSpace(fullPath))
            {
                throw new OperationCanceledException();
            }

            ProjectItem newItem = collection.Model.AddNewProjectItem("Project", fullPath);
            ProjectNode projectNode = selectedDefinition.CreateProjectNode();
            projectNode.SetParentAndModel(collection, newItem);

            byte[] template = selectedDefinition.Template;
            if (!selectedDefinition.ReplaceVariables(ref template, fullPath))
            {
                throw new OperationCanceledException();
            }

            if (!Directory.Exists(Path.GetDirectoryName(fullPath)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(fullPath));
            }

            using (FileStream stream = new FileStream(fullPath, FileMode.OpenOrCreate))
            {
                stream.SetLength(0);
                using (BinaryWriter writer = new BinaryWriter(stream))
                {
                    if (template != null)
                    {
                        writer.Write(template);
                    }

                    writer.Flush();
                    writer.Close();
                }
            }

            parent.AddChild(projectNode);
            projectNode.ModifiedScopeNode.IsModified = true;
            if (args.NodeSelectionDelegate != null)
            {
                args.NodeSelectionDelegate(projectNode);
            }

            projectNode.Load();
        }
        #endregion Methods
    } // RSG.Project.Commands.AddNewProjectAction {Class}
} // RSG.Project.Commands {Namespace}
