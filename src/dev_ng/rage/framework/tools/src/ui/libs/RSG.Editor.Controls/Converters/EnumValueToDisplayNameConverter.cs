﻿//---------------------------------------------------------------------------------------------
// <copyright file="EnumValueToDisplayNameConverter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Converters
{
    using System;
    using System.Globalization;
    using RSG.Base.Attributes;
    using RSG.Base.Extensions;

    /// <summary>
    /// Converters enum value to its string representation using the
    /// <see cref="RSG.Base.Attributes.FieldDisplayNameAttribute"/> class.
    /// </summary>
    public class EnumValueToDisplayNameConverter : ValueConverter<Enum, string>
    {
        #region Methods
        /// <summary>
        /// Converters the given value and returns the result.
        /// </summary>
        /// <param name="value">
        /// The original value to convert.
        /// </param>
        /// <param name="param">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A converted value.
        /// </returns>
        protected override string Convert(Enum value, object param, CultureInfo culture)
        {
            if (value == null)
            {
                return String.Empty;
            }

            var att = value.GetAttributeOfType<FieldDisplayNameAttribute>();
            if (att == null)
            {
                return value.ToString();
            }

            return att.DisplayName;
        }
        #endregion Methods
    } // RSG.Editor.Controls.Converters.EnumValueToDisplayNameConverter {Class}
} // RSG.Editor.Controls.Converters {Namespace}
