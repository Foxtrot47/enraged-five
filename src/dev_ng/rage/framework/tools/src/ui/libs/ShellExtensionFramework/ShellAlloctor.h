#ifndef SEF_SHELLALLOCATOR_H
#define SEF_SHELLALLOCATOR_H

// Windows headers
#include <atlbase.h>
#include <Windows.h>
#include <ShlObj.h>

/**
	\brief Shell Memory Allocator.
 */
class CShellAllocator
{
public:
	LPMALLOC m_pMalloc;
	HRESULT Initialise()
	{
		m_pMalloc = NULL;
		// It is safe to call ::SHGetMalloc()/::CoGetMalloc() without
		// first calling ::CoInitialize() according to MSDN.
		if( FAILED( ::SHGetMalloc(&m_pMalloc) ) ) 
		{
			// TODO: TERMINATE
			return E_FAIL;
		}
		return S_OK;
	}
	void Terminate()
	{
		if( m_pMalloc!=NULL ) 
			m_pMalloc->Release();
	}
	operator LPMALLOC() const
	{
		return m_pMalloc;
	}
	LPVOID Alloc(ULONG cb)
	{
		ATLASSERT(m_pMalloc!=NULL);
		ATLASSERT(cb>0);
		return m_pMalloc->Alloc(cb);
	}
	void Free(LPVOID p)
	{
		ATLASSERT(m_pMalloc!=NULL);
		ATLASSERT(p);
		m_pMalloc->Free(p);
	}
};

#endif // SEF_SHELLALLOCATOR_H