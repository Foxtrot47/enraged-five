﻿//---------------------------------------------------------------------------------------------
// <copyright file="SpecialCategoryViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.ViewModel
{
    using System.Collections;
    using System.Collections.Generic;
    using RSG.Text.Model;
    using RSG.Text.ViewModel.Resources;

    /// <summary>
    /// Represents the dialogue configuration editor category for the special items in the
    /// configuration. This class cannot be inherited.
    /// </summary>
    public sealed class SpecialCategoryViewModel : CategoryViewModel
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Items"/> property.
        /// </summary>
        private SpecialCollection _specials;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SpecialCategoryViewModel"/> class
        /// for the specified configurations.
        /// </summary>
        /// <param name="configurations">
        /// The configurations whose special items will be shown as part of this category.
        /// </param>
        public SpecialCategoryViewModel(DialogueConfigurations configurations)
            : base(StringTable.SpecialCategoryHeader, configurations)
        {
            this._specials = new SpecialCollection(configurations.Specials);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the list of audibility values this category contains.
        /// </summary>
        public SpecialCollection Items
        {
            get { return this._specials; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a composite view model object containing the
        /// <see cref="DialogueSpecialViewModel"/> objects inside the specified list.
        /// </summary>
        /// <param name="list">
        /// The collection that contains the individual items that should be grouped inside the
        /// resulting composite object.
        /// </param>
        /// <returns>
        /// A composite view model object that contains the
        /// <see cref="DialogueSpecialViewModel"/> objects inside the specified list.
        /// </returns>
        public override CompositeViewModel CreateComposite(IList list)
        {
            List<DialogueSpecialViewModel> items = new List<DialogueSpecialViewModel>();
            foreach (object item in list)
            {
                DialogueSpecialViewModel special = item as DialogueSpecialViewModel;
                if (special == null)
                {
                    continue;
                }

                items.Add(special);
            }

            return new SpecialCompositeViewModel(this.Model, items);
        }
        #endregion Methods
    } // RSG.Text.ViewModel.SpecialCategoryViewModel {Class}
} // RSG.Text.ViewModel {Namespace}
