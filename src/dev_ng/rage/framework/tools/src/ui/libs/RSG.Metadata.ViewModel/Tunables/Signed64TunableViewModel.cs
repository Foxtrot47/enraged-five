﻿//---------------------------------------------------------------------------------------------
// <copyright file="Signed64TunableViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Tunables
{
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// The view model for a <see cref="Signed64Tunable"/> model object.
    /// </summary>
    public class Signed64TunableViewModel : TunableViewModelBase<Signed64Tunable>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Signed64TunableViewModel"/> class.
        /// </summary>
        /// <param name="tunable">
        /// The 64-bit integer tunable that this view model is representing.
        /// </param>
        /// <param name="parent">
        /// The parent tunable view model for this view model.
        /// </param>
        public Signed64TunableViewModel(Signed64Tunable tunable, ITunableViewModel parent)
            : base(tunable, parent)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the value currently assigned to the 64-bit integer tunable.
        /// </summary>
        public long Value
        {
            get { return this.Model.Value; }
            set { this.Model.Value = value; }
        }
        #endregion Properties
    } // RSG.Metadata.ViewModel.Tunables.Signed64TunableViewModel {Class}
} // RSG.Metadata.ViewModel.Tunables {Namespace}
