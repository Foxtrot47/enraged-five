﻿//---------------------------------------------------------------------------------------------
// <copyright file="DockingGroup.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock.ViewModel
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Windows.Controls;

    /// <summary>
    /// Represents a group of docking elements that can be packed into a group and displayed
    /// in a certain orientation with splitters between them.
    /// </summary>
    public class DockingGroup : NotifyPropertyChangedBase, IDockingGroupItem, IDockingElement
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Children"/> property.
        /// </summary>
        private ObservableCollection<IDockingGroupItem> _children;

        /// <summary>
        /// The private field used for the <see cref="GroupIndex"/> property.
        /// </summary>
        private int _groupIndex;

        /// <summary>
        /// The private field used for the <see cref="Orientation"/> property.
        /// </summary>
        private Orientation _orientation;

        /// <summary>
        /// The private field used for the <see cref="ParentElement"/> property.
        /// </summary>
        private IDockingElement _parentElement;

        /// <summary>
        /// The private field used for the <see cref="ParentGroup"/> property.
        /// </summary>
        private DockingGroup _parentGroup;

        /// <summary>
        /// The private field used for the <see cref="Children"/> property.
        /// </summary>
        private ReadOnlyObservableCollection<IDockingGroupItem> _readOnlyChildren;

        /// <summary>
        /// The private field used for the <see cref="Children"/> property.
        /// </summary>
        private SplitterLength _splitterLength;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DockingGroup"/> class.
        /// </summary>
        /// <param name="viewSite">
        /// The view site that this docking group is underneath.
        /// </param>
        public DockingGroup(ViewSite viewSite)
        {
            this._parentElement = viewSite;
            this._splitterLength = new SplitterLength(1, SplitterUnitType.Stretch);
            this._children = new ObservableCollection<IDockingGroupItem>();
            this._readOnlyChildren =
                new ReadOnlyObservableCollection<IDockingGroupItem>(this._children);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a iterator around all of the document items currently opened inside this
        /// docking group.
        /// </summary>
        public IEnumerable<DocumentItem> AllDocuments
        {
            get
            {
                List<DocumentItem> documents = new List<DocumentItem>();
                foreach (IDockingGroupItem child in this._children)
                {
                    foreach (DocumentItem document in child.AllDocuments)
                    {
                        documents.Add(document);
                    }
                }

                return documents;
            }
        }

        /// <summary>
        /// Gets the read only collection of docking group items that belong to this group.
        /// </summary>
        public ReadOnlyObservableCollection<IDockingGroupItem> Children
        {
            get { return this._readOnlyChildren; }
        }

        /// <summary>
        /// Gets the first document pane in the logical order under this docking group.
        /// </summary>
        public DocumentPane FirstDocumentPane
        {
            get
            {
                foreach (IDockingGroupItem item in this._children)
                {
                    DocumentPane pane = item as DocumentPane;
                    if (pane != null)
                    {
                        return pane;
                    }

                    DockingGroup group = item as DockingGroup;
                    if (group != null)
                    {
                        DocumentPane groupPane = group.FirstDocumentPane;
                        if (groupPane != null)
                        {
                            return groupPane;
                        }
                    }
                }

                return null;
            }
        }

        /// <summary>
        /// Gets or sets the index for this item inside its parent group.
        /// </summary>
        public int GroupIndex
        {
            get { return this._groupIndex; }
            set { this._groupIndex = value; }
        }

        /// <summary>
        /// Gets or sets the Orientation that the child group elements should be arranged in.
        /// </summary>
        public Orientation Orientation
        {
            get { return this._orientation; }
            set { this.SetProperty(ref this._orientation, value); }
        }

        /// <summary>
        /// Gets the parent docking element.
        /// </summary>
        public IDockingElement ParentElement
        {
            get { return this._parentElement; }
        }

        /// <summary>
        /// Gets or sets the DockingGroup that is the parent to this element.
        /// </summary>
        public DockingGroup ParentGroup
        {
            get { return this._parentGroup; }
            set { this._parentGroup = value; }
        }

        /// <summary>
        /// Gets or sets the splitter length of this item inside its parent group.
        /// </summary>
        public SplitterLength SplitterLength
        {
            get { return this._splitterLength; }
            set { this.SetProperty(ref this._splitterLength, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Inserts the specified item into this groups children at the specified location.
        /// </summary>
        /// <param name="index">
        /// The zero-based index where the specified item should be added.
        /// </param>
        /// <param name="item">
        /// The item to add to this groups child collection.
        /// </param>
        public void InsertChild(int index, IDockingGroupItem item)
        {
            this._children.Insert(index, item);
            item.ParentGroup = this;
        }
        #endregion Methods
    } // RSG.Editor.Controls.Dock.ViewModel.DockingGroup {Class}
} // RSG.Editor.Controls.Dock.ViewModel {Namespace}
