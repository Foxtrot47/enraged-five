﻿//---------------------------------------------------------------------------------------------
// <copyright file="NotifyPropertyChangedBase.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Provides a abstract class that can be inherited by classes that wish to implement the
    /// System.ComponentModel.INotifyPropertyChanged interface.
    /// </summary>
    public abstract class NotifyPropertyChangedBase : INotifyPropertyChanged
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="NotifyPropertyChangedBase"/> class.
        /// </summary>
        protected NotifyPropertyChangedBase()
        {
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion Events

        #region Methods
        /// <summary>
        /// Fires the property changed event for the specified property names.
        /// </summary>
        /// <param name="names">
        /// A array of property names that have been changed.
        /// </param>
        public void NotifyPropertyChanged(params string[] names)
        {
            this.NotifyPropertyChanged(Enumerable.Distinct(names));
        }

        /// <summary>
        /// Fires the property changed event for the specified property names.
        /// </summary>
        /// <param name="name">
        /// The name of the property that has been changed.
        /// </param>
        protected void NotifyPropertyChanged([CallerMemberName]string name = "")
        {
            this.NotifyPropertyChanged(Enumerable.Repeat(name, 1));
        }

        /// <summary>
        /// Fires the property changed event for the specified property names.
        /// </summary>
        /// <param name="names">
        /// A iterator around all of property names that are changed due to this one change.
        /// </param>
        protected virtual void NotifyPropertyChanged(IEnumerable<string> names)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (names == null || handler == null)
            {
                return;
            }

            foreach (string name in names)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        /// <summary>
        /// Sets the given field to the given value making sure the correct property changed
        /// event is fired.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the field that is getting set.
        /// </typeparam>
        /// <param name="field">
        /// A reference to the field that is getting set.
        /// </param>
        /// <param name="value">
        /// The value to set the field to.
        /// </param>
        /// <param name="name">
        /// The name of the property that has been changed.
        /// </param>
        /// <returns>
        /// True if the property was changed; otherwise, false.
        /// </returns>
        protected bool SetProperty<T>(ref T field, T value, [CallerMemberName]string name = "")
        {
            return this.SetProperty(ref field, value, Enumerable.Repeat(name, 1));
        }

        /// <summary>
        /// Sets the given field to the given value making sure the correct property changed
        /// event is fired.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the field that is getting set.
        /// </typeparam>
        /// <param name="field">
        /// A reference to the field that is getting set.
        /// </param>
        /// <param name="value">
        /// The value to set the field to.
        /// </param>
        /// <param name="comparer">
        /// The equality comparer to use to determine whether the value has changed.
        /// </param>
        /// <param name="name">
        /// The name of the property that has been changed.
        /// </param>
        /// <returns>
        /// True if the property was changed; otherwise, false.
        /// </returns>
        protected bool SetProperty<T>(
            ref T field,
            T value,
            IEqualityComparer<T> comparer,
            [CallerMemberName]string name = "")
        {
            return this.SetProperty(ref field, value, comparer, Enumerable.Repeat(name, 1));
        }

        /// <summary>
        /// Sets the given field to the given value making sure the correct property changed
        /// event is fired.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the field that is getting set.
        /// </typeparam>
        /// <param name="field">
        /// A reference to the field that is getting set.
        /// </param>
        /// <param name="value">
        /// The value to set the field to.
        /// </param>
        /// <param name="names">
        /// An array of property names that are changed due to this one change.
        /// </param>
        /// <returns>
        /// True if the property was changed; otherwise, false.
        /// </returns>
        protected bool SetProperty<T>(ref T field, T value, params string[] names)
        {
            return this.SetProperty(ref field, value, Enumerable.Distinct(names));
        }

        /// <summary>
        /// Sets the given field to the given value making sure the correct property changed
        /// event is fired.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the field that is getting set.
        /// </typeparam>
        /// <param name="field">
        /// A reference to the field that is getting set.
        /// </param>
        /// <param name="value">
        /// The value to set the field to.
        /// </param>
        /// <param name="comparer">
        /// The equality comparer to use to determine whether the value has changed.
        /// </param>
        /// <param name="names">
        /// An array of property names that are changed due to this one change.
        /// </param>
        /// <returns>
        /// True if the property was changed; otherwise, false.
        /// </returns>
        protected bool SetProperty<T>(
            ref T field, T value, IEqualityComparer<T> comparer, params string[] names)
        {
            return this.SetProperty(ref field, value, comparer, Enumerable.Distinct(names));
        }

        /// <summary>
        /// Sets the given field to the given value making sure the correct property changed
        /// event is fired.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the field that is getting set.
        /// </typeparam>
        /// <param name="field">
        /// A reference to the field that is getting set.
        /// </param>
        /// <param name="value">
        /// The value to set the field to.
        /// </param>
        /// <param name="names">
        /// A iterator around all of property names that are changed due to this one change.
        /// </param>
        /// <returns>
        /// True if the property was changed; otherwise, false.
        /// </returns>
        protected bool SetProperty<T>(ref T field, T value, IEnumerable<string> names)
        {
            return this.SetProperty(ref field, value, EqualityComparer<T>.Default, names);
        }

        /// <summary>
        /// Sets the given field to the given value making sure the correct property changed
        /// event is fired.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the field that is getting set.
        /// </typeparam>
        /// <param name="field">
        /// A reference to the field that is getting set.
        /// </param>
        /// <param name="value">
        /// The value to set the field to.
        /// </param>
        /// <param name="comparer">
        /// The equality comparer to use to determine whether the value has changed.
        /// </param>
        /// <param name="names">
        /// A iterator around all of property names that are changed due to this one change.
        /// </param>
        /// <returns>
        /// True if the property was changed; otherwise, false.
        /// </returns>
        protected virtual bool SetProperty<T>(
            ref T field, T value, IEqualityComparer<T> comparer, IEnumerable<string> names)
        {
            if (comparer.Equals(field, value))
            {
                return false;
            }

            field = value;
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler == null || names == null)
            {
                return true;
            }

            foreach (string name in names)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }

            return true;
        }
        #endregion Methods
    } // RSG.Editor.NotifyPropertyChangedBase {Class}
} // RSG.Editor {Namespace}
