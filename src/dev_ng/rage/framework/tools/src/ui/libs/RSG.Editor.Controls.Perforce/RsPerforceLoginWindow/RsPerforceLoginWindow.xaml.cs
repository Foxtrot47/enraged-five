﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsPerforceLoginWindow.xaml.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Perforce
{
    using System;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using RSG.Interop.Perforce;

    /// <summary>
    /// Interaction logic for the PerforceLoginWindow window control.
    /// </summary>
    internal partial class RsPerforceLoginWindow : RsWindow
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="AttemptCommand"/> property.
        /// </summary>
        private static RoutedCommand _attemptCommand;

        /// <summary>
        /// The number of attempts the user has had to login.
        /// </summary>
        private int _attempts;

        /// <summary>
        /// The delegate that is fired when the user attempts to login.
        /// </summary>
        private PerforceLoginAttemptDelegate _delegate;

        /// <summary>
        /// The private field used for the <see cref="PerforceObject"/> property.
        /// </summary>
        private P4 _perforceObject;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsPerforceLoginWindow"/> class.
        /// </summary>
        static RsPerforceLoginWindow()
        {
            _attemptCommand =
                new RoutedCommand("AttemptCommand", typeof(RsPerforceLoginWindow));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsPerforceLoginWindow"/> class.
        /// </summary>
        public RsPerforceLoginWindow()
        {
            this.InitializeComponent();
            KeyStates state = Keyboard.GetKeyStates(Key.CapsLock);
            if ((state & KeyStates.Toggled) == KeyStates.Toggled)
            {
                this.CapsLockMessage.Visibility = Visibility.Visible;
            }
            else
            {
                this.CapsLockMessage.Visibility = Visibility.Collapsed;
            }

            this.CommandBindings.Add(
                new CommandBinding(AttemptCommand, this.AttemptLogin, this.CanAttemptLogin));
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the command that is fired when the user selects the OK button.
        /// </summary>
        public static RoutedCommand AttemptCommand
        {
            get { return _attemptCommand; }
        }

        /// <summary>
        /// Gets or sets the text shown for the client value.
        /// </summary>
        public string Client
        {
            get { return this.ClientTextBox.Text; }
            set { this.ClientTextBox.Text = value; }
        }

        /// <summary>
        /// Sets the delegate that is passed the login information that actual does the login.
        /// </summary>
        public PerforceLoginAttemptDelegate LoginDelegate
        {
            set { this._delegate = value; }
        }

        /// <summary>
        /// Gets the password that has been set for this login window.
        /// </summary>
        public string Password
        {
            get { return this.PasswordBox.Password; }
        }

        /// <summary>
        /// Sets the perforce object that this login window is trying to establish a connection
        /// with.
        /// </summary>
        public P4 PerforceObject
        {
            set { this._perforceObject = value; }
        }

        /// <summary>
        /// Gets or sets the text shown for the port value.
        /// </summary>
        public string Port
        {
            get { return this.PortTextBox.Text; }
            set { this.PortTextBox.Text = value; }
        }

        /// <summary>
        /// Gets or sets the text shown for the username value.
        /// </summary>
        public string Username
        {
            get { return this.UsernameTextBox.Text; }
            set { this.UsernameTextBox.Text = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Called whenever this window receives keyboard focus.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        protected override void OnGotFocus(RoutedEventArgs e)
        {
            base.OnGotFocus(e);
            KeyStates state = Keyboard.GetKeyStates(Key.CapsLock);
            if ((state & KeyStates.Toggled) == KeyStates.Toggled)
            {
                this.CapsLockMessage.Visibility = Visibility.Visible;
            }
            else
            {
                this.CapsLockMessage.Visibility = Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Called whenever a key is pressed down so that we can determine whether the caps
        /// lock message is shown.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.KeyEventArgs containing the event data.
        /// </param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            KeyStates state = Keyboard.GetKeyStates(Key.CapsLock);
            if ((state & KeyStates.Toggled) == KeyStates.Toggled)
            {
                this.CapsLockMessage.Visibility = Visibility.Visible;
            }
            else
            {
                this.CapsLockMessage.Visibility = Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Called when the user executes the AttemptLogin command. This forwards the currently
        /// submitted data to the associated login delegate.
        /// </summary>
        /// <param name="s">
        /// The object that fired the AttemptLogin command.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.ExecutedRoutedEventArgs containing the event data.
        /// </param>
        private async void AttemptLogin(object s, ExecutedRoutedEventArgs e)
        {
            this.SpinControl.IsSpinning = true;
            this.SpinControl.Visibility = System.Windows.Visibility.Visible;
            try
            {
                if (this._delegate == null)
                {
                    throw new MissingMethodException(
                        "Unable to attempt a login without a login delegate");
                }

                if (this._perforceObject == null)
                {
                    throw new ArgumentNullException(
                        "PerforceObject",
                        "Unable to attempt a login without a perforce wrapper object");
                }

                PerforceLoginAttemptData args = new PerforceLoginAttemptData(
                    this.UsernameTextBox.Text,
                    this.PasswordBox.Password,
                    true,
                    this._attempts++,
                    this._perforceObject);

                bool success = await Task.Factory.StartNew<bool>(
                    new Func<bool>(
                        delegate
                        {
                            return this._delegate(args);
                        }));

                if (success)
                {
                    this.DialogResult = true;
                    return;
                }

                this.ErrorMessage.Text = args.ErrorMessage;
                if (!args.Retry)
                {
                    this.DialogResult = false;
                    return;
                }
            }
            finally
            {
                this.SpinControl.Visibility = System.Windows.Visibility.Collapsed;
                this.SpinControl.IsSpinning = false;
            }
        }

        /// <summary>
        /// Determines whether the AttemptLogin command can be executed by the user. We don't
        /// want the user to try and login before they have entered any data.
        /// </summary>
        /// <param name="s">
        /// The object that fired the AttemptLogin command.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.CanExecuteRoutedEventArgs containing the event data.
        /// </param>
        private void CanAttemptLogin(object s, CanExecuteRoutedEventArgs e)
        {
            if (this._delegate == null)
            {
                e.CanExecute = false;
                return;
            }

            if (String.IsNullOrWhiteSpace(this.UsernameTextBox.Text))
            {
                e.CanExecute = false;
                return;
            }

            if (String.IsNullOrWhiteSpace(this.PasswordBox.Password))
            {
                e.CanExecute = false;
                return;
            }

            e.CanExecute = true;
        }
        #endregion Methods
    } // RSG.Editor.Controls.Perforce.RsPerforceLoginWindow {Class}
} // RSG.Editor.Controls.Perforce {Namespace}
