﻿//---------------------------------------------------------------------------------------------
// <copyright file="ButtonCommandImplementer{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System.Windows.Input;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Provides a abstract base class to any class that is used to implement a button command.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the parameter pass into the can execute and execute methods.
    /// </typeparam>
    public abstract class ButtonCommandImplementer<T> : CommandImplementer<T>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ButtonCommandImplementer{T}"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        protected ButtonCommandImplementer(ParameterResolverDelegate<T> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Create a new command definition for each command associated with this
        /// implementation.
        /// </summary>
        /// <param name="command">
        /// The command to create the definition for.
        /// </param>
        /// <returns>
        /// The new command definition created for the specified command.
        /// </returns>
        internal override CommandDefinition CreateDefinition(RockstarRoutedCommand command)
        {
            ButtonCommand definition = new Definition(command);
            return definition;
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// The command definition class that is used to create the commands for this
        /// implementer.
        /// </summary>
        private class Definition : ButtonCommand
        {
            #region Fields
            /// <summary>
            /// The private field used for the <see cref="Command"/> property.
            /// </summary>
            private RockstarRoutedCommand _command;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="Definition"/> class.
            /// </summary>
            /// <param name="command">
            /// The command that this definition is wrapping.
            /// </param>
            public Definition(RockstarRoutedCommand command)
            {
                this._command = command;
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets the System.Windows.Input.RoutedCommand that this definition is wrapping.
            /// </summary>
            public override RockstarRoutedCommand Command
            {
                get { return this._command; }
            }
            #endregion Properties
        } // ButtonCommandImplementer{T}.Definition
        #endregion Classes
    }  // RSG.Editor.ButtonCommandImplementer{T} {Class}
} // RSG.Editor {Namespace}
