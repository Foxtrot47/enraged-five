﻿//---------------------------------------------------------------------------------------------
// <copyright file="Float16TunableViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Tunables
{
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// The view model for a <see cref="Float16Tunable"/> model object.
    /// </summary>
    public class Float16TunableViewModel : TunableViewModelBase<Float16Tunable>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Float16TunableViewModel"/> class.
        /// </summary>
        /// <param name="tunable">
        /// The float tunable that this view model is representing.
        /// </param>
        /// <param name="parent">
        /// The parent tunable view model for this view model.
        /// </param>
        public Float16TunableViewModel(Float16Tunable tunable, ITunableViewModel parent)
            : base(tunable, parent)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the value currently assigned to the float tunable.
        /// </summary>
        public float Value
        {
            get { return this.Model.Value; }
            set { this.Model.Value = value; }
        }
        #endregion Properties
    } // RSG.Metadata.ViewModel.Tunables.Float16TunableViewModel {Class}
} // RSG.Metadata.ViewModel.Tunables {Namespace}
