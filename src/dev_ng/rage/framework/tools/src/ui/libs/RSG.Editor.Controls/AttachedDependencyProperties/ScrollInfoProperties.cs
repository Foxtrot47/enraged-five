﻿//---------------------------------------------------------------------------------------------
// <copyright file="ScrollInfoProperties.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.AttachedDependencyProperties
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Data;
    using System.Windows.Threading;
    using RSG.Editor.Controls.Helpers;

    /// <summary>
    /// Contains attached properties that are related to the check box.
    /// </summary>
    public static class ScrollInfoProperties
    {
        #region Fields
        /// <summary>
        /// Identifies the EnableScrollOffsetBinding dependency property.
        /// </summary>
        public static readonly DependencyProperty EnableScrollOffsetBindingProperty;

        /// <summary>
        /// Identifies the HorizontalScrollOffset dependency property.
        /// </summary>
        public static readonly DependencyProperty HorizontalScrollOffsetProperty;

        /// <summary>
        /// Identifies the VerticalScrollOffset dependency property.
        /// </summary>
        public static readonly DependencyProperty VerticalScrollOffsetProperty;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="ScrollInfoProperties"/> class.
        /// </summary>
        static ScrollInfoProperties()
        {
            VerticalScrollOffsetProperty =
                DependencyProperty.RegisterAttached(
                "VerticalScrollOffset",
                typeof(double),
                typeof(ScrollInfoProperties),
                new FrameworkPropertyMetadata(
                    0.0,
                    FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                    OnVerticalScrollOffsetChanged)
                    {
                        DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
                    });

            HorizontalScrollOffsetProperty =
                DependencyProperty.RegisterAttached(
                "HorizontalScrollOffset",
                typeof(double),
                typeof(ScrollInfoProperties),
                new FrameworkPropertyMetadata(
                    0.0,
                    FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                    OnHorizontalScrollOffsetChanged)
                    {
                        DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
                    });

            EnableScrollOffsetBindingProperty =
                DependencyProperty.RegisterAttached(
                "EnableScrollOffsetBinding",
                typeof(bool),
                typeof(ScrollInfoProperties),
                new PropertyMetadata(false, OnEnableScrollOffsetBindingChanged));
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Retrieves the EnableScrollOffsetBinding dependency property value attached to the
        /// specified object.
        /// </summary>
        /// <param name="element">
        /// The object whose attached EnableScrollOffsetBinding dependency property value will
        /// be returned.
        /// </param>
        /// <returns>
        /// The EnableScrollOffsetBinding dependency property value attached to the specified
        /// object.
        /// </returns>
        public static bool GetEnableScrollOffsetBinding(DependencyObject element)
        {
            return (bool)element.GetValue(VerticalScrollOffsetProperty);
        }

        /// <summary>
        /// Retrieves the HorizontalScrollOffset dependency property value attached to the
        /// specified object.
        /// </summary>
        /// <param name="element">
        /// The object whose attached HorizontalScrollOffset dependency property value will be
        /// returned.
        /// </param>
        /// <returns>
        /// The HorizontalScrollOffset dependency property value attached to the specified
        /// object.
        /// </returns>
        public static double GetHorizontalScrollOffset(DependencyObject element)
        {
            return (double)element.GetValue(HorizontalScrollOffsetProperty);
        }

        /// <summary>
        /// Retrieves the VerticalScrollOffset dependency property value attached to the
        /// specified object.
        /// </summary>
        /// <param name="element">
        /// The object whose attached VerticalScrollOffset dependency property value will be
        /// returned.
        /// </param>
        /// <returns>
        /// The VerticalScrollOffset dependency property value attached to the specified
        /// object.
        /// </returns>
        public static double GetVerticalScrollOffset(DependencyObject element)
        {
            return (double)element.GetValue(VerticalScrollOffsetProperty);
        }

        /// <summary>
        /// Sets the EnableScrollOffsetBinding dependency property value on the specified
        /// object to the specified value.
        /// </summary>
        /// <param name="element">
        /// The object whose attached EnableScrollOffsetBinding dependency property value will
        /// be set.
        /// </param>
        /// <param name="value">
        /// The value to set the attached EnableScrollOffsetBinding dependency property to on
        /// the specified object.
        /// </param>
        public static void SetEnableScrollOffsetBinding(DependencyObject element, bool value)
        {
            element.SetValue(VerticalScrollOffsetProperty, value);
        }

        /// <summary>
        /// Sets the HorizontalScrollOffset dependency property value on the specified object
        /// to the specified value.
        /// </summary>
        /// <param name="element">
        /// The object whose attached HorizontalScrollOffset dependency property value will be
        /// set.
        /// </param>
        /// <param name="value">
        /// The value to set the attached HorizontalScrollOffset dependency property to on the
        /// specified object.
        /// </param>
        public static void SetHorizontalScrollOffset(DependencyObject element, double value)
        {
            element.SetValue(HorizontalScrollOffsetProperty, value);
        }

        /// <summary>
        /// Sets the VerticalScrollOffset dependency property value on the specified object to
        /// the specified value.
        /// </summary>
        /// <param name="element">
        /// The object whose attached VerticalScrollOffset dependency property value will be
        /// set.
        /// </param>
        /// <param name="value">
        /// The value to set the attached VerticalScrollOffset dependency property to on the
        /// specified object.
        /// </param>
        public static void SetVerticalScrollOffset(DependencyObject element, double value)
        {
            element.SetValue(VerticalScrollOffsetProperty, value);
        }

        /// <summary>
        /// Called when the specified element has been loaded and needs the scroll bar value
        /// changed event attached to it.
        /// </summary>
        /// <param name="element">
        /// The element that has been loaded and needs the scroll event handlers attached to
        /// it.
        /// </param>
        private static void LoadedLogic(FrameworkElement element)
        {
            ScrollViewer scrollViewer = element.GetVisualDescendent<ScrollViewer>();
            if (scrollViewer != null)
            {
                scrollViewer.ScrollToVerticalOffset(GetVerticalScrollOffset(element));
            }

            foreach (ScrollBar scrollBar in element.GetVisualDescendents<ScrollBar>())
            {
                if (scrollBar.Orientation == Orientation.Vertical)
                {
                    scrollBar.ValueChanged += (s, args) =>
                    {
                        SetVerticalScrollOffset(element, args.NewValue);
                    };

                    break;
                }
            }
        }

        /// <summary>
        /// Called when a element that has been tagged to be tracked is loaded.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private static void OnElementLoaded(object sender, RoutedEventArgs e)
        {
            FrameworkElement element = sender as FrameworkElement;
            if (element == null)
            {
                return;
            }

            element.Loaded -= OnElementLoaded;
            LoadedLogic(element);
        }

        /// <summary>
        /// Called whenever the EnableScrollOffsetBinding dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The object whose EnableScrollOffsetBinding dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnEnableScrollOffsetBindingChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            FrameworkElement element = s as FrameworkElement;
            if (element == null)
            {
                return;
            }

            if ((bool)e.NewValue)
            {
                if (element.IsLoaded)
                {
                    LoadedLogic(element);
                }
                else
                {
                    element.Loaded += OnElementLoaded;
                }
            }
        }

        /// <summary>
        /// Called whenever the HorizontalScrollOffset dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The object whose HorizontalScrollOffset dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnHorizontalScrollOffsetChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            FrameworkElement element = s as FrameworkElement;
            if (element == null)
            {
                return;
            }

            ScrollViewer scrollViewer = element.GetVisualDescendent<ScrollViewer>();
            if (scrollViewer == null)
            {
                return;
            }

            double newValue = (double)e.NewValue;
            if (scrollViewer.HorizontalOffset == newValue)
            {
                return;
            }

            scrollViewer.ScrollToHorizontalOffset(newValue);
            SetHorizontalScrollOffset(element, scrollViewer.HorizontalOffset);
        }

        /// <summary>
        /// Called whenever the OnVerticalScrollOffset dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The object whose OnVerticalScrollOffset dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnVerticalScrollOffsetChanged(
            DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            FrameworkElement element = s as FrameworkElement;
            if (element == null)
            {
                return;
            }

            ScrollViewer scrollViewer = element.GetVisualDescendent<ScrollViewer>();
            if (scrollViewer == null)
            {
                return;
            }

            double newValue = (double)e.NewValue;
            if (scrollViewer.VerticalOffset == newValue)
            {
                return;
            }

            scrollViewer.ScrollToVerticalOffset(newValue);
            SetVerticalScrollOffset(element, scrollViewer.VerticalOffset);
        }
        #endregion Methods
    } // RSG.Editor.Controls.AttachedDependencyProperties.ScrollInfoProperties {Class}
} // RSG.Editor.Controls.AttachedDependencyProperties {Namespace}
