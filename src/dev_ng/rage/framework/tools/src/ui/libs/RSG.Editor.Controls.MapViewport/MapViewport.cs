﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using RSG.Base.Math;
using RSG.Editor.Controls.AttachedDependencyProperties;
using RSG.Editor.Controls.Helpers;
using RSG.Editor.Controls.MapViewport.Commands;
using RSG.Editor.Controls.ZoomCanvas;
using RSG.Editor.Controls.ZoomCanvas.Commands;
using RSG.Editor.Controls.MapViewport.Converters;
using RSG.Editor.Controls.MapViewport.Annotations;

namespace RSG.Editor.Controls.MapViewport
{
    /// <summary>
    /// Defines a control that can be used to render a map along with arbitrary geometry
    /// overlayed on top of it.
    /// </summary>
    public class MapViewport : ItemsControl
    {
        #region Constants
        /// <summary>
        /// Editor framework guid for the MapViewport's context menu.
        /// </summary>
        public static readonly Guid ContextMenuId = new Guid("8FD8CEA0-8BE6-4EF7-83B1-94A2941A8962");
        #endregion

        #region Fields
        /// <summary>
        /// Private field for the <see cref="ContextMenuWorldTriggerPosition"/> property.
        /// </summary>
        private Point _contextMenuWorldTriggerPosition;

        /// <summary>
        /// Flag indicating whether we are currently using the quick pan feature.
        /// </summary>
        private bool _isQuickPanning = false;

        /// <summary>
        /// Stores the last mouse position when moving the mouse.
        /// </summary>
        private Point _lastMousePosition;

        /// <summary>
        /// Reference to the zoomable canvas for hooking up event handlers.
        /// </summary>
        internal ZoomableCanvas _zoomableCanvas;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MapViewport"/> control.
        /// </summary>
        public MapViewport()
        {
            InternalItemsSource = new CompositeCollection();
            InternalItemsSource.Add(CreateBackgroundImageLayer());
            InternalItemsSource.Add(CreateGridLayer());
            InternalItemsSource.Add(CreateItemsSourceLayers());
            InternalItemsSource.Add(CreateAnnotationsLayer());

            // ZoomCanvas actions.
            new PanAction(ResolveZoomableCanvas)
                .AddBinding(ZoomCanvasCommands.Pan, this);
            new ZoomAction(ResolveZoomableCanvas)
                .AddBinding(ZoomCanvasCommands.Zoom, this);
            new RSG.Editor.Controls.MapViewport.Commands.ZoomExtentsAction(ResolveMapViewport)
                .AddBinding(ZoomCanvasCommands.ZoomExtents, this);

            // MapViewport actions.
            new AddAnnotationAction(ResolveAddAnnotationArgs)
                .AddBinding(MapViewportCommands.AddAnnotation, this);
            new DeleteAnnotationAction(ResolveMapViewport)
                .AddBinding(MapViewportCommands.DeleteAnnotation, this);
            new SaveToImageAction(ResolveMapViewport)
                .AddBinding(MapViewportCommands.SaveToImage, this);

            // Set the map viewport's context menu identifier.
            SetValue(ContextMenuProperties.ContextMenuIdentifierProperty, ContextMenuId);
        }

        /// <summary>
        /// Initialises the static fields of the <see cref="MapViewport"/> control.
        /// </summary>
        static MapViewport()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(MapViewport),
                new FrameworkPropertyMetadata(typeof(MapViewport)));
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the position at which the context menu opened on the map.
        /// </summary>
        public Point ContextMenuWorldTriggerPosition
        {
            get { return _contextMenuWorldTriggerPosition; }
        }

        /// <summary>
        /// Gets the map viewport's underlying zoomable canvas.
        /// </summary>
        public ZoomableCanvas ZoomCanvas
        {
            get { return _zoomableCanvas; }
        }
        #endregion

        #region BackgroundImages Dependency Property
        /// <summary>
        /// Identifies the BackgroundImages dependency property.
        /// </summary>
        public static readonly DependencyProperty BackgroundImagesProperty =
            DependencyProperty.Register(
                "BackgroundImages",
                typeof(MapBackgroundImageCollection),
                typeof(MapViewport),
                new FrameworkPropertyMetadata(null, OnBackgroundImagesChanged));
                
        /// <summary>
        /// Gets or sets the collection of background images that the map viewport can use.
        /// </summary>
        public MapBackgroundImageCollection BackgroundImages
        {
            get { return (MapBackgroundImageCollection)GetValue(BackgroundImagesProperty); }
            set { SetValue(BackgroundImagesProperty, value); }
        }
        
        /// <summary>
        /// Handles the event that occurs when the value of the <see cref="BackgroundImagesProperty"/> dependency property has changed.
        /// </summary>
        /// <param name="d">The dependency object on which the dependency property has changed.</param>
        /// <param name="e">The event args containing the old and new values of the dependency property.</param>
        private static void OnBackgroundImagesChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            d.CoerceValue(SelectedBackgroundImageProperty);

            MapViewport viewport = (MapViewport)d;
            if (viewport.BackgroundImages != null &&
                !viewport.BackgroundImages.Contains(viewport.SelectedBackgroundImage))
            {
                //viewport.SelectedBackgroundImage = viewport.BackgroundImages.FirstOrDefault();
            }
        }
        #endregion

        #region SelectedBackgroundImage Dependency Property
        /// <summary>
        /// Identifies the SelectedBackgroundImage dependency property.
        /// </summary>
        public static readonly DependencyProperty SelectedBackgroundImageProperty =
            DependencyProperty.Register(
                "SelectedBackgroundImage",
                typeof(MapBackgroundImage),
                typeof(MapViewport),
                new FrameworkPropertyMetadata(
                    null,
                    FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                    OnSelectedBackgroundImageChanged,
                    CoerceSelectedBackgroundImage));

        /// <summary>
        /// Gets or sets the background image that is currently used in the map viewport.
        /// </summary>
        public MapBackgroundImage SelectedBackgroundImage
        {
            get { return (MapBackgroundImage)GetValue(SelectedBackgroundImageProperty); }
            set { SetValue(SelectedBackgroundImageProperty, value); }
        }
        
        /// <summary>
        /// Handles the event that occurs when the value of the <see cref="SelectedBackgroundImage"/> dependency property has changed.
        /// </summary>
        /// <param name="d">The dependency object on which the dependency property has changed.</param>
        /// <param name="e">The event args containing the old and new values of the dependency property.</param>
        private static void OnSelectedBackgroundImageChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
        }
        
        /// <summary>
        /// Makes sure that the background image that is set is in the <see cref="BackgroundImages"/> collection.
        /// </summary>
        /// <param name="d">Dependency object whos value is being coerced.</param>
        /// <param name="value">The original uncoerced value.</param>
        /// <returns>A <see cref="Double"/> representing scale of the content that is currently being displayed.</returns>
        private static object CoerceSelectedBackgroundImage(DependencyObject d, object value)
        {
            if (value == null)
            {
                return value;
            }

            MapViewport viewport = (MapViewport)d;
            if (viewport.BackgroundImages != null && viewport.BackgroundImages.Contains((MapBackgroundImage)value))
            {
                return value;
            }

            return DependencyProperty.UnsetValue;
        }
        #endregion

        #region ShowMapForeground Dependency Property
        /// <summary>
        /// Identifies the ShowMapForeground dependency property.
        /// </summary>
        public static readonly DependencyProperty ShowMapForegroundProperty =
            DependencyProperty.Register(
                "ShowMapForeground",
                typeof(bool),
                typeof(MapViewport),
                new FrameworkPropertyMetadata(true));
        
        /// <summary>
        /// Flag indicating whether the map foreground should be shown.
        /// </summary>
        public bool ShowMapForeground
        {
            get { return (bool)GetValue(ShowMapForegroundProperty); }
            set { SetValue(ShowMapForegroundProperty, value); }
        }
        #endregion

        #region MapForegroundStyle Dependency Property
        /// <summary>
        /// Identifies the MapForegroundStyle dependency property.
        /// </summary>
        public static readonly DependencyProperty MapForegroundStyleProperty =
            DependencyProperty.Register(
                "MapForegroundStyle",
                typeof(Style),
                typeof(MapViewport),
                new FrameworkPropertyMetadata(null));
        
        /// <summary>
        /// Style to use for the map foreground.
        /// </summary>
        public Style MapForegroundStyle
        {
            get { return (Style)GetValue(MapForegroundStyleProperty); }
            set { SetValue(MapForegroundStyleProperty, value); }
        }
        #endregion

        #region Zoom Dependency Property
        /// <summary>
        /// Identifies the Zoom dependency property.
        /// </summary>
        public static readonly DependencyProperty ZoomProperty =
            DependencyProperty.Register(
                "Zoom",
                typeof(double),
                typeof(MapViewport),
                new FrameworkPropertyMetadata(1.0));

        /// <summary>
        /// 
        /// </summary>
        public double Zoom
        {
            get { return (double)GetValue(ZoomProperty); }
            set { SetValue(ZoomProperty, value); }
        }
        #endregion

        #region MinZoom Dependency Property
        /// <summary>
        /// Identifies the MinZoom dependency property.
        /// </summary>
        public static readonly DependencyProperty MinZoomProperty =
            DependencyProperty.Register(
                "MinZoom",
                typeof(double),
                typeof(MapViewport),
                new FrameworkPropertyMetadata(0.001));

        /// <summary>
        /// 
        /// </summary>
        public double MinZoom
        {
            get { return (double)GetValue(MinZoomProperty); }
            set { SetValue(MinZoomProperty, value); }
        }
        #endregion

        #region MaxZoom Dependency Property
        /// <summary>
        /// Identifies the MaxZoom dependency property.
        /// </summary>
        public static readonly DependencyProperty MaxZoomProperty =
            DependencyProperty.Register(
                "MaxZoom",
                typeof(double),
                typeof(MapViewport),
                new FrameworkPropertyMetadata(2.0));

        /// <summary>
        /// 
        /// </summary>
        public double MaxZoom
        {
            get { return (double)GetValue(MaxZoomProperty); }
            set { SetValue(MaxZoomProperty, value); }
        }
        #endregion

        #region MouseWorldCoords Dependency Property
        /// <summary>
        /// Identifies the MouseWorldCoords dependency property key.
        /// </summary>
        public static readonly DependencyPropertyKey MouseWorldCoordsPropertyKey =
            DependencyProperty.RegisterReadOnly(
                "MouseWorldCoords",
                typeof(Point),
                typeof(MapViewport),
                new FrameworkPropertyMetadata(null));

        /// <summary>
        /// Identifies the MouseWorldCoords dependency property.
        /// </summary>
        public static readonly DependencyProperty MouseWorldCoordsProperty =
            MouseWorldCoordsPropertyKey.DependencyProperty;

        /// <summary>
        /// 
        /// </summary>
        public Point MouseWorldCoords
        {
            get { return (Point)GetValue(MouseWorldCoordsProperty); }
            private set { SetValue(MouseWorldCoordsPropertyKey, value); }
        }
        #endregion

        #region InternalItemsSource Dependency Property
        /// <summary>
        /// 
        /// </summary>
        public static readonly DependencyPropertyKey InternalItemsSourcePropertyKey =
            DependencyProperty.RegisterReadOnly(
                "InternalItemsSource",
                typeof(CompositeCollection),
                typeof(MapViewport),
                new FrameworkPropertyMetadata(null));

        /// <summary>
        /// Identifies the InternalItemsSource dependency property.
        /// </summary>
        public static readonly DependencyProperty InternalItemsSourceProperty =
            InternalItemsSourcePropertyKey.DependencyProperty;

        /// <summary>
        /// 
        /// </summary>
        internal CompositeCollection InternalItemsSource
        {
            get { return (CompositeCollection)GetValue(InternalItemsSourceProperty); }
            private set { SetValue(InternalItemsSourcePropertyKey, value); }
        }
        #endregion

        #region Annotations Dependency Property
        /// <summary>
        /// Identifies the Annotations dependency property.
        /// </summary>
        public static readonly DependencyProperty AnnotationsProperty =
            DependencyProperty.Register(
                "Annotations",
                typeof(AnnotationCollection),
                typeof(MapViewport),
                new FrameworkPropertyMetadata(null));

        /// <summary>
        /// Gets or sets the collection of annotations that are currently displayed on the map viewport.
        /// </summary>
        public AnnotationCollection Annotations
        {
            get { return (AnnotationCollection)GetValue(AnnotationsProperty); }
            set { SetValue(AnnotationsProperty, value); }
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Gets called whenever the template is applied to this control. This makes sure we
        /// have a reference to the zoomable canvas in the template as well as setting up the
        /// required event handlers for it.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            if (_zoomableCanvas != null)
            {
            }

            _zoomableCanvas = GetTemplateChild("PART_ZoomableCanvas") as ZoomableCanvas;
            if (_zoomableCanvas != null)
            {
            }
        }

        /// <summary>
        /// Creates or identifies the element used to display the child items.
        /// </summary>
        /// <returns>
        /// A new <see cref="RSG.Editor.Controls.MapViewport.MapLayer" />.
        /// </returns>
        protected override DependencyObject GetContainerForItemOverride()
        {
            if (Debugger.IsAttached)
            {
                return null;
            }
            Debug.Fail("Children of the MapViewport control must be wrapped inside of a MapLayer control.");
            throw new NotSupportedException("Children of the MapViewport control must be wrapped inside of a MapLayer control.");
        }

        /// <summary>
        /// Determines whether the specified item is, or is eligible to be, its own item
        /// container.
        /// </summary>
        /// <param name="item">
        /// The item to check whether it is an item container.
        /// </param>
        /// <returns>
        /// True if the item is eligible to be its own item container; otherwise, false.
        /// </returns>
        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is MapLayer;
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Event handler for supporting the quick pan operation.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Middle)
            {
                this.CaptureMouse();
                e.Handled = true;

                // Keep track of the mouse position over the zoomable canvas.
                Point position = e.GetPosition(this);
                _lastMousePosition = position;
                _isQuickPanning = true;
            }
            else
            {
                base.OnMouseDown(e);
            }
        }

        /// <summary>
        /// Event handler for supporting the quick pan operation.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseMove(MouseEventArgs e)
        {
            Point mousePos = _zoomableCanvas.MousePosition;
            mousePos.Y *= -1;
            MouseWorldCoords = mousePos;

            if (_isQuickPanning && e.MiddleButton == MouseButtonState.Pressed)
            {
                Point position = e.GetPosition(this);
                Vector diff = _lastMousePosition - position;
                if (ZoomCanvasCommands.Pan.CanExecute(diff, _zoomableCanvas))
                {
                    ZoomCanvasCommands.Pan.Execute(diff, _zoomableCanvas);
                }
                e.Handled = true;

                // Keep track of the mouse position over the zoomable canvas.
                _lastMousePosition = position;
            }
            else
            {
                base.OnMouseMove(e);
            }
        }

        /// <summary>
        /// Event handler for supporting the quick pan operation.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Middle)
            {
                this.ReleaseMouseCapture();
                _isQuickPanning = false;
                e.Handled = true;
            }
            else
            {
                base.OnMouseUp(e);
            }
        }

        /// <summary>
        /// Event handler for supporting the quick zoom operation.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseWheel(MouseWheelEventArgs e)
        {
            Point position = e.GetPosition(this);
            double scaleAdjustment = Math.Pow(2, e.Delta / 3.0 / Mouse.MouseWheelDeltaForOneLine);
            Vector zoomAmount = new Vector(scaleAdjustment, scaleAdjustment);

            ZoomCommandArgs args = new ZoomCommandArgs(zoomAmount, position);
            if (ZoomCanvasCommands.Zoom.CanExecute(args, _zoomableCanvas))
            {
                ZoomCanvasCommands.Zoom.Execute(args, _zoomableCanvas);
                CommandManager.InvalidateRequerySuggested();
            }

            e.Handled = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnContextMenuOpening(ContextMenuEventArgs e)
        {
            base.OnContextMenuOpening(e);
            _contextMenuWorldTriggerPosition = new Point(_zoomableCanvas.MousePosition.X, -_zoomableCanvas.MousePosition.Y);
        }
        #endregion

        #region Command Argument Resolvers
        /// <summary>
        /// The action resolver that returns the map viewport control.
        /// </summary>
        /// <param name="commandData">
        /// The command data sent with the executing command.
        /// </param>
        /// <returns>
        /// The map viewport itself.
        /// </returns>
        private MapViewport ResolveMapViewport(CommandData commandData)
        {
            return this;
        }

        /// <summary>
        /// The action resolver that returns command args to use with the
        /// <see cref="AddAnnotationAction"/>.
        /// </summary>
        /// <param name="commandData"></param>
        /// <returns></returns>
        private AddAnnotationCommandArgs ResolveAddAnnotationArgs(CommandData commandData)
        {
            return new AddAnnotationCommandArgs(this, _contextMenuWorldTriggerPosition);
        }

        /// <summary>
        /// The action resolver that returns the zoomable canvas.
        /// </summary>
        /// <param name="commandData">
        /// The command data sent with the executing command.
        /// </param>
        /// <returns>
        /// The zoomable canvas used for rendering the map viewport.
        /// </returns>
        private ZoomableCanvas ResolveZoomableCanvas(CommandData commandData)
        {
            return _zoomableCanvas;
        }
        #endregion

        #region Layer Creation
        /// <summary>
        /// Creates a map layer for hosting the background image in.
        /// </summary>
        /// <returns></returns>
        private MapLayer CreateBackgroundImageLayer()
        {
            Binding dcBinding = new Binding("SelectedBackgroundImage");
            dcBinding.RelativeSource = new RelativeSource(RelativeSourceMode.FindAncestor, typeof(MapViewport), 1);

            MapLayerItem mapLayerItem = new MapLayerItem();
            mapLayerItem.SetBinding(ContentControl.DataContextProperty, dcBinding);
            mapLayerItem.SetBinding(ContentControl.ContentProperty, dcBinding);

            Binding positionBinding = new Binding("SelectedBackgroundImage.WorldExtents.BottomLeft");
            positionBinding.RelativeSource = new RelativeSource(RelativeSourceMode.FindAncestor, typeof(MapViewport), 1);
            positionBinding.TargetNullValue = new Point(0, 0);
            mapLayerItem.SetBinding(MapLayerPanel.PositionProperty, positionBinding);

            // Create the map layer and add the map layer item as a child.
            MapLayer layer = new MapLayer();
            layer.DisplayName = "Map Image";
            layer.Items.Add(mapLayerItem);

            return layer;
        }

        /// <summary>
        /// Creates a map layer for hosting the map grid in.
        /// </summary>
        /// <returns></returns>
        private MapLayer CreateGridLayer()
        {
            Binding dcBinding = new Binding("SelectedBackgroundImage");
            dcBinding.RelativeSource = new RelativeSource(RelativeSourceMode.FindAncestor, typeof(MapViewport), 1);

            // Create the grid that will be placed into the layer.
            MapGrid grid = new MapGrid();
            grid.SetBinding(FrameworkElement.DataContextProperty, dcBinding);
            grid.StrokeThickness = 1;
            grid.MajorLineSpacing = 1000;
            grid.MinorLineSpacing = 100;
            grid.MajorLineStroke = Brushes.Black;
            grid.MinorLineStroke = Brushes.Gray;
            grid.Opacity = 0.5;
            grid.SetBinding(MapGrid.MapBoundsProperty, new Binding("WorldExtents"));

            Binding scaleBinding = new Binding("Scale");
            scaleBinding.RelativeSource = new RelativeSource(RelativeSourceMode.FindAncestor, typeof(ZoomableCanvas), 1);
            grid.SetBinding(MapGrid.ScaleProperty, scaleBinding);

            Binding zoomBinding = new Binding("Zoom");
            zoomBinding.RelativeSource = new RelativeSource(RelativeSourceMode.FindAncestor, typeof(MapViewport), 1);

            MultiBinding widthBinding = new MultiBinding();
            widthBinding.Bindings.Add(new Binding("WorldExtents.Width"));
            widthBinding.Bindings.Add(zoomBinding);
            widthBinding.Converter = new DoubleMultiplicationConverter();
            grid.SetBinding(FrameworkElement.WidthProperty, widthBinding);

            MultiBinding heightBinding = new MultiBinding();
            heightBinding.Bindings.Add(new Binding("WorldExtents.Height"));
            heightBinding.Bindings.Add(zoomBinding);
            heightBinding.Converter = new DoubleMultiplicationConverter();
            grid.SetBinding(FrameworkElement.HeightProperty, heightBinding);

            // Create a map layer item and add the grid as a child.
            MapLayerItem mapLayerItem = new MapLayerItem();
            mapLayerItem.Content = grid;
            mapLayerItem.SetBinding(ContentControl.DataContextProperty, dcBinding);
            mapLayerItem.SetBinding(MapLayerPanel.PositionProperty, new Binding("WorldExtents.BottomLeft"));

            // Create the map layer and add the map layer item as a child.
            MapLayer layer = new MapLayer();
            layer.DisplayName = "Grid";
            layer.Items.Add(mapLayerItem);

            return layer;
        }

        /// <summary>
        /// Creates a collection container for hosting the overlay geometry in.
        /// </summary>
        /// <returns></returns>
        private CollectionContainer CreateItemsSourceLayers()
        {
            CollectionContainer container = new CollectionContainer();
            container.Collection = Items;
            return container;
        }

        /// <summary>
        /// Creates a map layer for hosting the map annotations.
        /// </summary>
        /// <returns></returns>
        private MapLayer CreateAnnotationsLayer()
        {
            Binding itemsSourceBinding = new Binding("Annotations");
            itemsSourceBinding.RelativeSource = new RelativeSource(RelativeSourceMode.FindAncestor, typeof(MapViewport), 1);

            // Create the map layer and add the annotations as children.
            MapLayer layer = new MapLayer();
            layer.DisplayName = "Annotations";
            layer.StartsGroup = true;
            layer.SetBinding(MapLayer.ItemsSourceProperty, itemsSourceBinding);
            return layer;
        }
        #endregion
    }
}
