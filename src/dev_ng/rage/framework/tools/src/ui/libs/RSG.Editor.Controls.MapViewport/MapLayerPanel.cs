﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace RSG.Editor.Controls.MapViewport
{
    /// <summary>
    /// In charge of laying out the elements that are contained within a map panel.
    /// </summary>
    public class MapLayerPanel : Panel
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="ChildrenExtents"/> property.
        /// </summary>
        private Rect _extents;
        #endregion

        #region Scale Dependency Property
        /// <summary>
        /// Identifies the <see cref="Scale"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ScaleProperty =
            DependencyProperty.Register(
                "Scale",
                typeof(Vector),
                typeof(MapLayerPanel),
                new FrameworkPropertyMetadata(new Vector(1.0, 1.0), FrameworkPropertyMetadataOptions.AffectsArrange));

        /// <summary>
        /// Gets or sets the scale of the <see cref="MapLayerPanel"/>.
        /// </summary>
        public Vector Scale
        {
            get { return (Vector)GetValue(ScaleProperty); }
            set { SetValue(ScaleProperty, value); }
        }
        #endregion

        #region Offset Dependency Property
        /// <summary>
        /// Identifies the <see cref="Offset"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty OffsetProperty =
            DependencyProperty.Register(
                "Offset",
                typeof(Point),
                typeof(MapLayerPanel),
                new FrameworkPropertyMetadata(new Point(0.0, 0.0)));

        public Point Offset
        {
            get { return (Point)GetValue(OffsetProperty); }
            set { SetValue(OffsetProperty, value); }
        }
        #endregion

        #region Position Attached Property
        /// <summary>
        /// Identifies the Position attached property.
        /// </summary>
        public static readonly DependencyProperty PositionProperty =
            DependencyProperty.RegisterAttached(
                "Position",
                typeof(Point),
                typeof(MapLayerPanel),
                new FrameworkPropertyMetadata(new Point(0, 0), FrameworkPropertyMetadataOptions.AffectsArrange));

        /// <summary>
        /// Gets the Position dependency property value for the specified element.
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static Point GetPosition(UIElement element)
        {
            if (element == null)
            {
                throw new SmartArgumentNullException(() => element);
            }

            return (Point)element.GetValue(PositionProperty);
        }

        /// <summary>
        /// Sets the Position dependency property on the specified element to the
        /// specified value.
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetPosition(UIElement element, Point value)
        {
            if (element == null)
            {
                throw new SmartArgumentNullException(() => element);
            }

            element.SetValue(PositionProperty, value);
        }
        #endregion

        #region PositionOrigin Attached Property
        /// <summary>
        /// Identifies the PositionOrigin attached property.
        /// </summary>
        public static readonly DependencyProperty PositionOriginProperty =
            DependencyProperty.RegisterAttached(
                "PositionOrigin",
                typeof(PositionOrigin),
                typeof(MapLayerPanel),
                new FrameworkPropertyMetadata(PositionOrigin.TopLeft, FrameworkPropertyMetadataOptions.AffectsArrange));

        /// <summary>
        /// Gets the PositionOrigin dependency property value for the specified element.
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static PositionOrigin GetPositionOrigin(UIElement element)
        {
            if (element == null)
            {
                throw new SmartArgumentNullException(() => element);
            }

            return (PositionOrigin)element.GetValue(PositionOriginProperty);
        }

        /// <summary>
        /// Sets the PositionOrigin dependency property on the specified element to the
        /// specified value.
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetPositionOrigin(UIElement element, PositionOrigin value)
        {
            if (element == null)
            {
                throw new SmartArgumentNullException(() => element);
            }

            element.SetValue(PositionOriginProperty, value);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Represents the extent of the instantiated UIElements calculated during <see cref="ArrangeOverride"/>.
        /// </summary>
        public Rect Extents
        {
            get { return _extents; }
            set { _extents = value; }
        }
        #endregion

        #region Measure/Arrange Logic
        /// <summary>
        /// Measures the child elements of a <see cref="MapLayerPanel"/> in anticipation of arranging them during the <see cref="ArrangeOverride"/> pass.
        /// </summary>
        /// <param name="availableSize">An upper limit <see cref="Size"/> that should not be exceeded.</param>
        /// <returns>A <see cref="Size"/> that represents the size that is required to arrange child content.</returns>
        protected override Size MeasureOverride(Size availableSize)
        {
            Size childConstraint = new Size(Double.PositiveInfinity, Double.PositiveInfinity);

            foreach (UIElement child in InternalChildren)
            {
                if (child != null)
                {
                    child.Measure(childConstraint);
                }
            }

            return new Size();
        }

        /// <summary>
        /// Arranges the content of a <see cref="MapLayerPanel"/> element.
        /// </summary>
        /// <param name="finalSize">The size that this <see cref="MapLayerPanel"/> element should use to arrange its child elements.</param>
        /// <returns>A <see cref="Size"/> that represents the arranged size of this <see cref="MapLayerPanel"/> element and its descendants.</returns>
        protected override Size ArrangeOverride(Size finalSize)
        {
            Rect extents = Rect.Empty;

            foreach (UIElement child in InternalChildren)
            {
                Point position = MapLayerPanel.GetPosition(child);

                double xPos = position.X * Scale.X;
                double yPos = -position.Y * Scale.Y;

                PositionOrigin origin = MapLayerPanel.GetPositionOrigin(child);

                // Adjust the x position.
                if (origin == PositionOrigin.TopCenter || origin == PositionOrigin.Center || origin == PositionOrigin.BottomCenter)
                {
                    xPos -= child.DesiredSize.Width / 2.0;
                }
                else if (origin == PositionOrigin.TopRight || origin == PositionOrigin.CenterRight || origin == PositionOrigin.BottomRight)
                {
                    xPos -= child.DesiredSize.Width;
                }

                // Adjust the y position
                if (origin == PositionOrigin.CenterLeft || origin == PositionOrigin.Center || origin == PositionOrigin.CenterRight)
                {
                    yPos -= child.DesiredSize.Height / 2.0;
                }
                else if (origin == PositionOrigin.BottomLeft || origin == PositionOrigin.BottomCenter || origin == PositionOrigin.BottomRight)
                {
                    yPos -= child.DesiredSize.Height;
                }

                // Create the bounds for the child and arrange it.
                Rect bounds = new Rect(xPos, yPos, child.DesiredSize.Width, child.DesiredSize.Height);
                extents.Union(bounds);

                child.Arrange(bounds);
            }
            Extents = extents;

            return finalSize;
        }
        #endregion
    }
}
