﻿//---------------------------------------------------------------------------------------------
// <copyright file="ConversationViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.ViewModel
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Linq;
    using System.Windows.Data;
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Editor.View;
    using RSG.Text.Model;

    /// <summary>
    /// The view model the represents a <see cref="RSG.Text.Model.Conversation"/> class
    /// instance.
    /// </summary>
    public class ConversationViewModel : ValidatingViewModelBase<Conversation>
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Dialogue"/> property.
        /// </summary>
        private DialogueViewModel _dialogue;

        /// <summary>
        /// The private field used for the <see cref="Lines"/> property.
        /// </summary>
        private LineCollection _lines;

        /// <summary>
        /// 
        /// </summary>
        private double _lineScrollOffset;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ConversationViewModel"/> class.
        /// </summary>
        /// <param name="conversation">
        /// The <see cref="RSG.Text.Model.Conversation"/> instance this view model is wrapping.
        /// </param>
        public ConversationViewModel(Conversation conversation, DialogueViewModel dialogue)
            : base(conversation, true, true)
        {
            this._dialogue = dialogue;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the category that this conversation uses.
        /// </summary>
        public string Category
        {
            get { return this.Model.Category; }
            set { this.Model.Category = value; }
        }

        /// <summary>
        /// Gets the collection of characters that can be used by the lines inside this
        /// conversation object.
        /// </summary>
        public CharacterCollection Characters
        {
            get { return this._dialogue.Characters; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this conversation contains subtitles used
        /// during a cut-scene.
        /// </summary>
        public bool CutsceneSubtitles
        {
            get { return this.Model.CutsceneSubtitles; }
            set { this.Model.CutsceneSubtitles = value; }
        }

        /// <summary>
        /// Gets or sets the description for this conversation.
        /// </summary>
        public string Description
        {
            get { return this.Model.Description; }
            set { this.Model.Description = value; }
        }

        /// <summary>
        /// Gets the parent dialogue view model object which this conversation belongs to.
        /// </summary>
        public DialogueViewModel Dialogue
        {
            get { return this._dialogue; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this conversation can be interrupted by
        /// other game audio.
        /// </summary>
        public bool Interruptible
        {
            get { return this.Model.Interruptible; }
            set { this.Model.Interruptible = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this conversation is currently placeholder
        /// created by the designer.
        /// </summary>
        public bool IsPlaceholder
        {
            get { return this.Model.IsPlaceholder; }
            set { this.Model.IsPlaceholder = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this conversation contains random lines of
        /// dialogue.
        /// </summary>
        public bool IsRandom
        {
            get { return this.Model.IsRandom; }
            set { this.Model.IsRandom = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this conversation is triggered off by a
        /// specified animation and not called from script.
        /// </summary>
        public bool IsTriggeredByAnimation
        {
            get { return this.Model.IsTriggeredByAnimation; }
            set { this.Model.IsTriggeredByAnimation = value; }
        }

        /// <summary>
        /// Gets the list of errors that are contained within the lines for this conversation.
        /// </summary>
        public IReadOnlyList<string> LineErrors
        {
            get
            {
                HashSet<string> errors = new HashSet<string>();
                foreach (LineViewModel line in this.Lines)
                {
                    lock (line.ErrorLock)
                    {
                        errors.UnionWith(line.CurrentValidation.AllUniqueErrors);
                    }
                }

                return errors.ToList();
            }
        }

        /// <summary>
        /// Gets the collection of lines belonging to this conversation.
        /// </summary>
        public IReadOnlyViewModelCollection<LineViewModel> Lines
        {
            get
            {
                if (this._lines == null)
                {
                    this._lines = new LineCollection(this.Model.Lines, this);
                    this._lines.AddCollectionChangedHandler(this.OnLinesChanged);
                    this.UpdateLineErrors();
                }

                return this._lines;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the lines belonging to this conversation contain
        /// errors within them.
        /// </summary>
        public bool LinesContainErrors
        {
            get
            {
                HashSet<string> entityErrors = new HashSet<string>();
                foreach (LineViewModel line in this.Lines)
                {
                    lock (line.ErrorLock)
                    {
                        if (line.CurrentValidation.HasErrors)
                        {
                            return true;
                        }
                    }
                }

                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public double LineScrollOffset
        {
            get { return this._lineScrollOffset; }
            set { this.SetProperty(ref this._lineScrollOffset, value); }
        }

        /// <summary>
        /// Gets or sets the root identifier for this conversation.
        /// </summary>
        public string Root
        {
            get { return this.Model.Root; }
            set { this.Model.Root = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DialogueSubtitleType SubtitleType
        {
            get
            {
                return this.Model.Configurations.GetSubtitleType(this.Model.SubtitleType);
            }

            set
            {
                if (value != null)
                {
                    this.Model.SubtitleType = value.Id;
                }
                else
                {
                    this.Model.SubtitleType = Guid.Empty;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ModelCollection<DialogueSubtitleType> SubtitleTypes
        {
            get { return this.Model.Configurations.SubtitleTypes; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Updates the properties that are associated with the errors contained within the
        /// lines belonging to this conversation.
        /// </summary>
        internal void UpdateLineErrors()
        {
            if (this._lines != null)
            {
                this.NotifyPropertyChanged("LinesContainErrors");
                this.NotifyPropertyChanged("LineErrors");
            }
        }

        /// <summary>
        /// Determines whether the validation pipeline should be started due to the property
        /// with the specified name changing.
        /// </summary>
        /// <param name="propertyName">
        /// The property name to test.
        /// </param>
        /// <returns>
        /// True if the validation should be run due to the specified property changing;
        /// otherwise, false.
        /// </returns>
        protected override bool ShouldRunValidation(string propertyName)
        {
            if (String.Equals(propertyName, "Root"))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// The core method used to validate this class. By default this calls the models
        /// validate methods and uses that result.
        /// </summary>
        /// <param name="propertyName">
        /// The name of the property if this validation is associated with a change to a
        /// specific property; otherwise, null.
        /// </param>
        /// <returns>
        /// The result of the validation.
        /// </returns>
        protected override ValidationResult ValidateCore(string propertyName)
        {
            ValidationResult result = base.ValidateCore(propertyName);
            if (String.Equals(propertyName, "Root"))
            {
                foreach (ConversationViewModel conversation in this._dialogue.Conversations)
                {
                    if (Object.ReferenceEquals(conversation, this))
                    {
                        continue;
                    }

                    conversation.Validate();
                }
            }

            return result;
        }

        /// <summary>
        /// Called whenever the collection of lines belonging to the conversation changes so
        /// that the line error status can be changed.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to. (this._lines).
        /// </param>
        /// <param name="args">
        /// The event arguments that specify how the collection has been changed.
        /// </param>
        private void OnLinesChanged(object s, NotifyCollectionChangedEventArgs args)
        {
            foreach (LineViewModel line in this._lines)
            {
                line.Validate();
            }

            this.UpdateLineErrors();
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// A collection class specifically for the use of the Conversation View Model for its
        /// line collection.
        /// </summary>
        public class LineCollection
            : CollectionConverter<ILine, LineViewModel>,
            IReadOnlyViewModelCollection<LineViewModel>
        {
            #region Fields
            /// <summary>
            /// The conversation view model that owns this collection.
            /// </summary>
            private ConversationViewModel _owner;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="LineCollection"/> class.
            /// </summary>
            /// <param name="source">
            /// The source model items for this collection.
            /// </param>
            public LineCollection(
                ReadOnlyModelCollection<ILine> source, ConversationViewModel owner)
            {
                this._owner = owner;
                this.ResetSourceItems(source);
            }
            #endregion Constructors

            #region Methods
            /// <summary>
            /// Converts the single specified line to a new instance of the line view model
            /// class.
            /// </summary>
            /// <param name="item">
            /// The item to convert.
            /// </param>
            /// <returns>
            /// A new instance of the line view model class created from the specified line.
            /// </returns>
            protected override LineViewModel ConvertItem(ILine item)
            {
                LineViewModel newViewModel = new LineViewModel(item, this._owner);
                return newViewModel;
            }
            #endregion Methods
        } // ConversationViewModel.LineCollection {Class}
        #endregion Classes
    } // RSG.Text.ViewModel.ConversationViewModel {Class}
} // RSG.Text.ViewModel {Namespace}
