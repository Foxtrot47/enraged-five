﻿//---------------------------------------------------------------------------------------------
// <copyright file="MetadataFileNode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Project
{
    using System.IO;
    using System.Xml;
    using RSG.Base.Logging;
    using RSG.Editor.Controls.Dock.ViewModel;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Definitions;
    using RSG.Metadata.Model.Tunables;
    using RSG.Metadata.ViewModel.Project.Controllers;
    using RSG.Metadata.ViewModel.Tunables;
    using RSG.Project.Model;
    using RSG.Project.ViewModel;

    /// <summary>
    /// A file node object that represents a metadata document inside the project explorer.
    /// </summary>
    public class MetadataFileNode : FileNode
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MetadataFileNode"/> class.
        /// </summary>
        /// <param name="parent">
        /// The parent hierarchy node that this node will be placed under.
        /// </param>
        /// <param name="model">
        /// The project item for this node that will act as its model.
        /// </param>
        public MetadataFileNode(IHierarchyNode parent, ProjectItem model)
            : base(parent, model)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the creates document content controller that can be used to create a content
        /// object for this object inside a document tab.
        /// </summary>
        public override ICreateDocumentContentController CreateDocumentContentController
        {
            get { return new CreateDocumentContentController(); }
        }
        #endregion Properties
    } // RSG.Metadata.ViewModel.Project.MetadataFileNode {Class}
} // RSG.Metadata.ViewModel.Project {Namespace}
