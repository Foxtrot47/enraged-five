﻿//---------------------------------------------------------------------------------------------
// <copyright file="CloseDocumentAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Commands
{
    using System.Diagnostics;
    using System.Windows;
    using RSG.Editor;
    using RSG.Editor.Controls.Dock;
    using RSG.Editor.Controls.Dock.ViewModel;

    /// <summary>
    /// Contains the logic for the <see cref="DockingCommands.CloseItem"/> routed command. This
    /// class cannot be inherited.
    /// </summary>
    public sealed class CloseDocumentAction : ButtonAction<ProjectCommandArgs, object>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CloseDocumentAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public CloseDocumentAction(ProjectCommandResolver resolver)
            : base(new ParameterResolverDelegate<ProjectCommandArgs>(resolver))
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="target">
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ProjectCommandArgs args, object target)
        {
            if (target != null)
            {
                return true;
            }

            if (args == null)
            {
                return false;
            }

            if (args.ViewSite.ActiveDocument == null)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="target">
        /// </param>
        public override void Execute(ProjectCommandArgs args, object target)
        {
            IDocumentManagerService docService = this.GetService<IDocumentManagerService>();
            if (docService == null)
            {
                Debug.Fail(
                    "Unable to close document due to the fact the service is missing.");
                return;
            }

            DocumentItem document = args.ViewSite.ActiveDocument;
            FrameworkElement element = target as FrameworkElement;
            if (element != null)
            {
                document = element.DataContext as DocumentItem;
            }

            if (document == null)
            {
                document = args.ViewSite.ActiveDocument;
            }

            docService.CloseDocument(document, true);
        }
        #endregion Methods
    } // RSG.Project.Commands.CloseDocumentAction {Class}
} // RSG.Project.Commands {Namespace}
