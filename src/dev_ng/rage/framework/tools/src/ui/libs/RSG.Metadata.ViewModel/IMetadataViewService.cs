﻿//---------------------------------------------------------------------------------------------
// <copyright file="IMetadataViewService.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel
{
    /// <summary>
    /// Provides the ability to perform actions that require views for metadata specific
    /// commands and methods.
    /// </summary>
    public interface IMetadataViewService
    {
        #region Methods
        /// <summary>
        /// Displays the wizard that is used for the standard metadata project type.
        /// </summary>
        /// <returns>
        /// Returns a structure containing the results of the shown wizard.
        /// </returns>
        StandardProjectWizardResult ShowStandardProjectWizard();
        #endregion Methods
    } // RSG.Metadata.ViewModel.IMetadataViewService {Interface}
} // RSG.Metadata.ViewModel {Namespace}
