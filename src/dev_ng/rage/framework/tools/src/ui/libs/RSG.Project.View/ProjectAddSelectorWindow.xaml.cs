﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProjectAddSelectorWindow.xaml.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.View
{
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.IO;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using RSG.Editor;
    using RSG.Editor.Controls;
    using RSG.Project.Model;
    using RSG.Project.View.Resources;
    using RSG.Project.ViewModel;
    using RSG.Project.ViewModel.Definitions;

    /// <summary>
    /// Interaction logic for the ProjectAddSelectorWindow.
    /// </summary>
    public partial class ProjectAddSelectorWindow : RsWindow
    {
        #region Fields
        /// <summary>
        /// The private reference to the new items scope so that a unique name can be
        /// determined.
        /// </summary>
        private IProjectItemScope _itemScope;

        /// <summary>
        /// A private value indicating whether the default name is still being displayed for
        /// the selected definition.
        /// </summary>
        private bool _usingDefaultName;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ProjectAddSelectorWindow"/> class.
        /// </summary>
        public ProjectAddSelectorWindow()
        {
            this._usingDefaultName = true;
            this.InitializeComponent();
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs before the window closes so that any listener can cancel the close in case
        /// invalid options have been set.
        /// </summary>
        public event EventHandler<ConfirmAddEventArgs> ConfirmAdd;
        #endregion Events

        #region Methods
        /// <summary>
        /// Makes all of the controls that are to do with the collection option invisible to
        /// the user.
        /// </summary>
        public void MakeCollectionOptionsInvisible()
        {
            this.CollectionLabel.Visibility = Visibility.Collapsed;
            this.CollectionOptionComboBox.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// Sets the definition for this window and gives access to the definitions that will
        /// be available for selection.
        /// </summary>
        /// <param name="collection">
        /// The collection whose definition contains the available child definitions for
        /// selection.
        /// </param>
        public void SetDefinition(ProjectCollectionNode collection)
        {
            this.Title = SelectorWindowResources.AddProjectTitle;
            if (this.TemplateListBox == null || collection == null)
            {
                return;
            }

            this._itemScope = collection.Model as IProjectItemScope;
            Binding binding = new Binding("ProjectDefinitions");
            binding.Source = collection.Definition;
            binding.UpdateSourceTrigger = UpdateSourceTrigger.Explicit;

            this.TemplateListBox.SetBinding(ListBox.ItemsSourceProperty, binding);
            IEnumerable source = this.TemplateListBox.Items;
            if (source == null)
            {
                return;
            }

            ICollectionView view = CollectionViewSource.GetDefaultView(source);
            if (view == null)
            {
                return;
            }

            view.Filter = this.FilterTemplateList;
            SortDescription? sortDescription = this.GetCurrentSortDescription();
            if (sortDescription == null)
            {
                view.SortDescriptions.Clear();
            }
            else
            {
                view.SortDescriptions.Add((SortDescription)sortDescription);
            }
        }

        /// <summary>
        /// Determines whether the
        /// <see cref="RSG.Editor.SharedCommands.RockstarViewCommands.ConfirmDialog"/> command
        /// can be executed at this level.
        /// </summary>
        /// <param name="e">
        /// The command data that is past from the command system.
        /// </param>
        /// <returns>
        /// True if the command can be executed here; otherwise, false.
        /// </returns>
        protected override bool CanConfirmDialog(CanExecuteCommandData e)
        {
            if (String.IsNullOrWhiteSpace(this.NameValueTextBox.Text))
            {
                return false;
            }

            if (String.IsNullOrWhiteSpace(this.LocationValueTextBox.Text))
            {
                return false;
            }

            string pattern = "\\A[^{0}]*\\z";
            pattern = String.Format(pattern, new String(Path.GetInvalidFileNameChars()));
            var regex = new System.Text.RegularExpressions.Regex(pattern);
            if (!regex.IsMatch(this.NameValueTextBox.Text))
            {
                return false;
            }

            pattern = "\\A[a-zA-Z]:\\\\[^{0}]*\\z";
            pattern = String.Format(pattern, new String(Path.GetInvalidPathChars()));
            regex = new System.Text.RegularExpressions.Regex(pattern);
            if (!regex.IsMatch(this.LocationValueTextBox.Text))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Handles the
        /// <see cref="RSG.Editor.SharedCommands.RockstarViewCommands.ConfirmDialog"/> command
        /// after it gets caught here.
        /// </summary>
        /// <param name="e">
        /// The command data that is past from the command system.
        /// </param>
        protected override void ConfirmDialog(ExecuteCommandData e)
        {
            EventHandler<ConfirmAddEventArgs> handler = this.ConfirmAdd;
            if (handler != null)
            {
                string name = this.NameValueTextBox.Text;
                string location = this.LocationValueTextBox.Text;
                IDefinition definition = this.TemplateListBox.SelectedItem as IDefinition;
                ConfirmAddEventArgs args = new ConfirmAddEventArgs(name, location, definition);
                handler(this, args);
                if (args.Cancel)
                {
                    return;
                }
            }

            this.DialogResult = true;
        }

        /// <summary>
        /// The method the collection view for the list box uses to determine whether a item is
        /// to be shown or not.
        /// </summary>
        /// <param name="item">
        /// The item that is tested for visibility.
        /// </param>
        /// <returns>
        /// True if the specified item should be displayed; otherwise, false.
        /// </returns>
        private bool FilterTemplateList(object item)
        {
            if (this.TemplateSearchControl == null)
            {
                return true;
            }

            if (String.IsNullOrEmpty(this.TemplateSearchControl.SearchText))
            {
                return true;
            }

            ProjectItemDefinition definition = item as ProjectItemDefinition;
            if (definition != null)
            {
                return definition.Name.StartsWith(this.TemplateSearchControl.SearchText);
            }

            return true;
        }

        /// <summary>
        /// Gets the sort description that the list box should be using based on the currently
        /// select sort algorithm.
        /// </summary>
        /// <returns>
        /// The sort description that the list box should be using.
        /// </returns>
        private SortDescription? GetCurrentSortDescription()
        {
            if (this.SortSelector == null)
            {
                return null;
            }

            switch (this.SortSelector.SelectedIndex)
            {
                case 0:
                    return new SortDescription("Name", ListSortDirection.Ascending);
                case 1:
                    return new SortDescription("Name", ListSortDirection.Descending);
            }

            return null;
        }

        /// <summary>
        /// Called when the browse button is pressed. Shows the select folder windows dialog
        /// for the user to select.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private void OnBrowseButtonClicked(object sender, RoutedEventArgs e)
        {
            string directory = null;
            Guid id = new Guid("F7985C53-BD7C-451F-8852-40CA29329E72");

            RsSelectFolderDialog dlg = new RsSelectFolderDialog(id);
            dlg.Title = SelectorWindowResources.BrowseProjectLocationTitle;

            bool? result = dlg.ShowDialog();
            if (result == true)
            {
                directory = dlg.FileName;
            }
            else
            {
                return;
            }

            this.LocationValueTextBox.Text = Path.GetFullPath(directory);
        }

        /// <summary>
        /// Called whenever the selected definition changes so that the name can be updated to
        /// match the new selection.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Controls.SelectionChangedEventArgs containing the event data.
        /// </param>
        private void OnDefinitionSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!this._usingDefaultName || this._itemScope == null)
            {
                return;
            }

            IDefinition definition = this.TemplateListBox.SelectedItem as IDefinition;
            if (definition == null)
            {
                return;
            }

            string baseFilename = definition.DefaultName + definition.Extension;
            string name = this._itemScope.GetUniqueFilename(baseFilename);
            this.NameValueTextBox.TextChanged -= this.OnNameChanged;
            this.NameValueTextBox.Text = name;
            this.NameValueTextBox.TextChanged += this.OnNameChanged;
        }

        /// <summary>
        /// Called whenever the text inside the name text box changes outside of the text
        /// changing due to default name updating.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Controls.TextChangedEventArgs containing the event data.
        /// </param>
        private void OnNameChanged(object sender, TextChangedEventArgs e)
        {
            this._usingDefaultName = false;
        }

        /// <summary>
        /// Called whenever the sort option changes so that the sort description can be update
        /// on the collection view.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Controls.SelectionChangedEventArgs containing the event data.
        /// </param>
        private void OnSortSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.TemplateListBox == null)
            {
                return;
            }

            IEnumerable source = this.TemplateListBox.Items;
            if (source == null)
            {
                return;
            }

            ICollectionView view = CollectionViewSource.GetDefaultView(source);
            if (view == null)
            {
                return;
            }

            SortDescription? sortDescription = this.GetCurrentSortDescription();
            if (sortDescription == null)
            {
                view.SortDescriptions.Clear();
            }
            else
            {
                view.SortDescriptions.Add((SortDescription)sortDescription);
            }

            view.Refresh();
        }

        /// <summary>
        /// Refreshes the filtering of the current definition list based on a routed event
        /// being fired. This handler to attached to the search events among other ones.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Controls.SelectionChangedEventArgs containing the event data.
        /// </param>
        private void RefreshFiltering(object sender, RoutedEventArgs e)
        {
            IEnumerable source = this.TemplateListBox.Items;
            if (source == null)
            {
                return;
            }

            ICollectionView view = CollectionViewSource.GetDefaultView(source);
            if (view == null)
            {
                return;
            }

            view.Refresh();
        }
        #endregion Methods
    } // RSG.Project.View.ProjectAddSelectorWindow {Class}
} // RSG.Project.View {Namespace}
