﻿//---------------------------------------------------------------------------------------------
// <copyright file="ViewFileAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Rpf.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using RSG.Base.Extensions;
    using RSG.Editor;
    using RSG.Rpf.Commands.Resources;
    using RSG.Rpf.ViewModel;

    /// <summary>
    /// Implements the <see cref="RpfViewerCommands.ViewRpfItem"/> command.
    /// </summary>
    public class ViewFileAction : ExtractionAction
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ViewFileAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public ViewFileAction(
            ParameterResolverDelegate<IEnumerable<PackEntryViewModel>> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="commandParameter">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(IEnumerable<PackEntryViewModel> commandParameter)
        {
            return commandParameter.Count() == 1;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="commandParameter">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(IEnumerable<PackEntryViewModel> commandParameter)
        {
            int count = commandParameter.Count();
            if (count != 1)
            {
                return;
            }

            IMessageBoxService messageService = this.GetService<IMessageBoxService>();
            ICommonDialogService dlgService = this.GetService<ICommonDialogService>();
            if (messageService == null || dlgService == null)
            {
                Debug.Assert(false, "Unable to extract as services are missing.");
                return;
            }

            string directory = Path.GetTempPath();
            var failedExtractions = new Dictionary<PackEntryViewModel, string>();
            foreach (PackEntryViewModel item in commandParameter)
            {
                if (item == null)
                {
                    continue;
                }

                string name = item.Name;
                string destination = System.IO.Path.Combine(directory, name);
                FileInfo fileInfo = new FileInfo(destination);
                if (fileInfo.Exists && fileInfo.IsReadOnly)
                {
                    int attempts = 0;
                    string baseFilename = Path.GetFileNameWithoutExtension(destination);
                    while (baseFilename.Contains('.'))
                    {
                        baseFilename = Path.GetFileNameWithoutExtension(baseFilename);
                    }

                    do
                    {
                        string attempt = null;
                        if (attempts >= 100)
                        {
                            attempt =
                                Guid.NewGuid().ToString("B", CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            attempt = (attempts++).ToStringInvariant("D2");
                        }

                        string filename = baseFilename + attempt;
                        destination = System.IO.Path.Combine(directory, filename);
                        fileInfo = new FileInfo(destination);
                    }
                    while (fileInfo.Exists && fileInfo.IsReadOnly);
                }

                try
                {
                    if (this.Extract(item, destination))
                    {
                        Process.Start(destination);
                    }
                    else
                    {
                        failedExtractions.Add(item, StringTable.ExtractionStreamFailedMsg);
                    }
                }
                catch (UnauthorizedAccessException)
                {
                    failedExtractions.Add(item, StringTable.ExtractionPermissionFailedMsg);
                }
                catch (Exception)
                {
                    failedExtractions.Add(item, StringTable.ExtractionUnknownFailedMsg);
                }
            }

            if (failedExtractions.Count != 0)
            {
                string msg = StringTable.ExtractionFailedMsg;
                StringBuilder failedItems = new StringBuilder();
                foreach (KeyValuePair<PackEntryViewModel, string> failure in failedExtractions)
                {
                    failedItems.Append(failure.Key.Name);
                    failedItems.Append(" - ");
                    failedItems.AppendLine(failure.Value);
                }

                messageService.Show(
                    StringTable.ExtractionFailedMsg.FormatCurrent(failedItems),
                    String.Empty,
                    System.Windows.MessageBoxButton.OK,
                    System.Windows.MessageBoxImage.Error);
            }
        }
        #endregion Methods
    } // RSG.Rpf.Commands.ViewFileAction {Class}
} // RSG.Rpf.Commands {Namespace}
