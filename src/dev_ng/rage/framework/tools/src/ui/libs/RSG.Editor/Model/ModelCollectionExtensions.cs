﻿//---------------------------------------------------------------------------------------------
// <copyright file="ModelCollectionExtensions.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Model
{
    /// <summary>
    /// Provides extensions onto the <see cref="ModelCollection{T}"/> class.
    /// </summary>
    public static class ModelCollectionExtensions
    {
        #region Methods
        /// <summary>
        /// Creates a read-only version of this model collection.
        /// </summary>
        /// <typeparam name="T">
        /// The type parameter of the collection being created.
        /// </typeparam>
        /// <param name="collection">
        /// The collection to create a read-only version of.
        /// </param>
        /// <returns>
        /// A new instance of a read-only model collection that uses this collection as its
        /// inner collection.
        /// </returns>
        public static ReadOnlyModelCollection<T> ToReadOnly<T>(
            this ModelCollection<T> collection) where T : IModel
        {
            if (collection == null)
            {
                return null;
            }

            return new ReadOnlyModelCollection<T>(collection);
        }
        #endregion Methods
    } // RSG.Editor.Model.ModelCollectionExtensions {Class}
} // RSG.Editor.Model {Namespace}
