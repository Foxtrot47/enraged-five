﻿//---------------------------------------------------------------------------------------------
// <copyright file="SearchAction{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Collections.ObjectModel;
    using System.Media;
    using System.Threading.Tasks;
    using System.Windows;
    using RSG.Editor.SharedCommands;

    /// <summary>
    /// Provides a abstract base class to any class that is used to implement the search
    /// commands (search, search next, search previous).
    /// </summary>
    /// <typeparam name="T">
    /// The type of the parameter passed into the can execute and execute methods.
    /// </typeparam>
    public abstract class SearchAction<T>
    {
        #region Fields
        /// <summary>
        /// The private cache of the current search data to use with the search next and search
        /// previous search commands.
        /// </summary>
        private static SearchData _currentSearchData;

        /// <summary>
        /// The private resolver that this class uses to obtain the command parameter from.
        /// </summary>
        private ParameterResolverDelegate<T> _resolver;

        /// <summary>
        /// The private field used for the <see cref="ServiceProvider"/> property.
        /// </summary>
        private ICommandServiceProvider _serviceProvider;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SearchAction{T}"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        protected SearchAction(ParameterResolverDelegate<T> resolver)
        {
            this._resolver = resolver;
            this._serviceProvider = Application.Current as ICommandServiceProvider;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Sets the service provider that can be used in the CanExecute and Execute methods.
        /// </summary>
        public ICommandServiceProvider ServiceProvider
        {
            set
            {
                if (value == null)
                {
                    throw new SmartArgumentNullException(() => value);
                }

                this._serviceProvider = value;
            }
        }

        /// <summary>
        /// Gets the command definition that has been created for this implementer for the
        /// specified routed command.
        /// </summary>
        /// <param name="command">
        /// The command whose definition should be returned.
        /// </param>
        /// <returns>
        /// The command definition that has been created for this implementer for the specified
        /// routed command.
        /// </returns>
        public CommandDefinition this[RockstarRoutedCommand command]
        {
            get
            {
                CommandDefinition definition = RockstarCommandManager.GetDefinition(command);
                if (definition == null)
                {
                    definition = this.CreateDefinition(command);
                    if (definition != null)
                    {
                        RockstarCommandManager.AddCommandDefinition(definition);
                    }
                }

                return definition;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Binds this implementation to the common search commands and the specified user
        /// interface element.
        /// </summary>
        /// <param name="element">
        /// The element that this implementation will be bound to.
        /// </param>
        public void AddBinding(UIElement element)
        {
            if (element == null)
            {
                throw new SmartArgumentNullException(() => element);
            }

            RockstarRoutedCommand command = RockstarCommands.Find;
            CommandDefinition definition = RockstarCommandManager.GetDefinition(command);
            if (definition == null)
            {
                definition = this.CreateDefinition(command);
                if (definition != null)
                {
                    RockstarCommandManager.AddCommandDefinition(definition);
                }
            }

            RockstarCommandManager.AddBinding<SearchData>(
                element, command, this.ExecuteWithParameter, this.CanExecuteWithParameter);

            command = RockstarCommands.FindNext;
            definition = RockstarCommandManager.GetDefinition(command);
            if (definition == null)
            {
                definition = new ButtonCommand(command);
                if (definition != null)
                {
                    RockstarCommandManager.AddCommandDefinition(definition);
                }
            }

            RockstarCommandManager.AddBinding(
                element, command, this.Execute, this.CanExecute);

            command = RockstarCommands.FindPrevious;
            definition = RockstarCommandManager.GetDefinition(command);
            if (definition == null)
            {
                definition = new ButtonCommand(command);
                if (definition != null)
                {
                    RockstarCommandManager.AddCommandDefinition(definition);
                }
            }

            RockstarCommandManager.AddBinding(
                element, command, this.Execute, this.CanExecute);
        }

        /// <summary>
        /// Binds this implementation to the common search commands and registers them with
        /// the specified class type.
        /// </summary>
        /// <param name="type">
        /// The class with which to register the command binding to.
        /// </param>
        public void AddBinding(Type type)
        {
            if (type == null)
            {
                throw new SmartArgumentNullException(() => type);
            }

            RockstarRoutedCommand command = RockstarCommands.Find;
            CommandDefinition definition = RockstarCommandManager.GetDefinition(command);
            if (definition == null)
            {
                definition = this.CreateDefinition(command);
                if (definition != null)
                {
                    RockstarCommandManager.AddCommandDefinition(definition);
                }
            }

            RockstarCommandManager.AddBinding<SearchData>(
                type, command, this.ExecuteWithParameter, this.CanExecuteWithParameter);

            command = RockstarCommands.FindNext;
            definition = RockstarCommandManager.GetDefinition(command);
            if (definition == null)
            {
                definition = new ButtonCommand(command);
                if (definition != null)
                {
                    RockstarCommandManager.AddCommandDefinition(definition);
                }
            }

            RockstarCommandManager.AddBinding(
                type, command, this.Execute, this.CanExecute);

            command = RockstarCommands.FindPrevious;
            definition = RockstarCommandManager.GetDefinition(command);
            if (definition == null)
            {
                definition = new ButtonCommand(command);
                if (definition != null)
                {
                    RockstarCommandManager.AddCommandDefinition(definition);
                }
            }

            RockstarCommandManager.AddBinding(
                type, command, this.Execute, this.CanExecute);
        }

        /// <summary>
        /// Override to set logic against the can execute handler for the
        /// <see cref="RockstarCommands.Find"/> command. By default this returns true.
        /// </summary>
        /// <param name="commandParameter">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        protected virtual bool CanSearch(T commandParameter)
        {
            return true;
        }

        /// <summary>
        /// Override to set logic against the execute command handler for the
        /// <see cref="RockstarCommands.Find"/> command when the data indicates the search
        /// should be cleared.
        /// </summary>
        /// <param name="commandParameter">
        /// The command parameter that has been requested.
        /// </param>
        protected abstract void ClearSearch(T commandParameter);

        /// <summary>
        /// Override the provide the created definition with the items to show in the filter.
        /// </summary>
        /// <returns>
        /// The items to show in the filter.
        /// </returns>
        protected virtual ObservableCollection<string> GetScopes()
        {
            return null;
        }

        /// <summary>
        /// Gets the service object of the specified type.
        /// </summary>
        /// <typeparam name="TService">
        /// The type of service object to get.
        /// </typeparam>
        /// <returns>
        /// A service object of the specified type or null if there is no service object of
        /// that type.
        /// </returns>
        protected TService GetService<TService>() where TService : class
        {
            TService service = null;
            if (this._serviceProvider != null)
            {
                service = this._serviceProvider.GetService<TService>();
            }

            if (service != null)
            {
                return service;
            }

            throw new ExecuteCommandException(
                "Unable to perform command as service is missing");
        }

        /// <summary>
        /// Override the modify the search parameters on the specified definition.
        /// </summary>
        /// <param name="definition">
        /// The definition that has been created.
        /// </param>
        protected virtual void ModifyDefinition(ICommandWithSearchParameters definition)
        {
        }

        /// <summary>
        /// Override to set logic against the execute command handler for the
        /// <see cref="RockstarCommands.Find"/> command.
        /// </summary>
        /// <param name="commandParameter">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="data">
        /// The search data that has been sent with the command.
        /// </param>
        /// <returns>
        /// True if some results were found; otherwise, false.
        /// </returns>
        protected abstract Task<bool> Search(T commandParameter, SearchData data);

        /// <summary>
        /// Override to set logic against the execute command handler for the
        /// <see cref="RockstarCommands.FindNext"/> command.
        /// </summary>
        /// <param name="commandParameter">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="data">
        /// The search data that has been sent with the command.
        /// </param>
        /// <returns>
        /// True if the next search result was navigated to successfully; otherwise, false.
        /// </returns>
        protected virtual bool SearchNext(T commandParameter, SearchData data)
        {
            return false;
        }

        /// <summary>
        /// Override to set logic against the execute command handler for the
        /// <see cref="RockstarCommands.FindPrevious"/> command.
        /// </summary>
        /// <param name="commandParameter">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="data">
        /// The search data that has been sent with the command.
        /// </param>
        /// <returns>
        /// True if the next search result was navigated to successfully; otherwise, false.
        /// </returns>
        protected virtual bool SearchPrevious(T commandParameter, SearchData data)
        {
            return false;
        }

        /// <summary>
        /// Determines whether the associated command can be executed based on the current
        /// state of the application.
        /// </summary>
        /// <param name="data">
        /// The can execute data that has been sent with the command.
        /// </param>
        /// <returns>
        /// True if the command can be executed; otherwise, false.
        /// </returns>
        private bool CanExecute(CanExecuteCommandData data)
        {
            T commandParameter = default(T);
            if (this._resolver != null)
            {
                commandParameter = this._resolver(data);
            }

            if (data.Command == RockstarCommands.FindNext)
            {
                return this.CanSearch(commandParameter);
            }
            else if (data.Command == RockstarCommands.FindPrevious)
            {
                return this.CanSearch(commandParameter);
            }

            throw new InvalidOperationException("Unexpected command fired for handler");
        }

        /// <summary>
        /// Determines whether the associated command can be executed based on the current
        /// state of the application.
        /// </summary>
        /// <param name="data">
        /// The can execute data that has been sent with the command.
        /// </param>
        /// <returns>
        /// True if the command can be executed; otherwise, false.
        /// </returns>
        private bool CanExecuteWithParameter(CanExecuteCommandData<SearchData> data)
        {
            T commandParameter = default(T);
            if (this._resolver != null)
            {
                commandParameter = this._resolver(data);
            }

            if (data.Command == RockstarCommands.Find)
            {
                return this.CanSearch(commandParameter);
            }

            throw new InvalidOperationException("Unexpected command fired for handler");
        }

        /// <summary>
        /// Create a new command definition for each command associated with this
        /// implementation.
        /// </summary>
        /// <param name="command">
        /// The command to create the definition for.
        /// </param>
        /// <returns>
        /// The new command definition created for the specified command.
        /// </returns>
        private CommandDefinition CreateDefinition(RockstarRoutedCommand command)
        {
            SearchCommand definition = new SearchDefinition(command);
            this.ModifyDefinition(definition);
            ObservableCollection<string> items = this.GetScopes();
            if (items != null)
            {
                definition.Scopes = items;
            }

            return definition;
        }

        /// <summary>
        /// Called whenever the associated command is fired and needs handling at this instance
        /// level.
        /// </summary>
        /// <param name="data">
        /// The execute data that has been sent with the command.
        /// </param>
        private void Execute(ExecuteCommandData data)
        {
            T commandParameter = default(T);
            if (this._resolver != null)
            {
                commandParameter = this._resolver(data);
            }

            if (data.Command == RockstarCommands.FindNext)
            {
                if (_currentSearchData != null)
                {
                    if (!this.SearchNext(commandParameter, _currentSearchData))
                    {
                        if (!String.IsNullOrEmpty(_currentSearchData.SearchText))
                        {
                            SystemSounds.Asterisk.Play();
                        }
                    }
                }

                return;
            }
            else if (data.Command == RockstarCommands.FindPrevious)
            {
                if (_currentSearchData != null)
                {
                    if (!this.SearchPrevious(commandParameter, _currentSearchData))
                    {
                        if (!String.IsNullOrEmpty(_currentSearchData.SearchText))
                        {
                            SystemSounds.Asterisk.Play();
                        }
                    }
                }

                return;
            }

            throw new InvalidOperationException("Unexpected command fired for handler");
        }

        /// <summary>
        /// Called whenever the associated command is fired and needs handling at this instance
        /// level.
        /// </summary>
        /// <param name="data">
        /// The execute data that has been sent with the command.
        /// </param>
        private async void ExecuteWithParameter(ExecuteCommandData<SearchData> data)
        {
            try
            {
                T commandParameter = default(T);
                if (this._resolver != null)
                {
                    commandParameter = this._resolver(data);
                }

                _currentSearchData = null;
                if (data.Command == RockstarCommands.Find)
                {
                    data.Parameter.ThrowIfCancellationRequested();
                    data.Parameter.RegisterSearchStarting();

                    bool foundResults = false;
                    if (data.Parameter.Cleared)
                    {
                        this.ClearSearch(commandParameter);
                        foundResults = true;
                    }
                    else
                    {
                        _currentSearchData = data.Parameter;
                        foundResults = await this.Search(commandParameter, data.Parameter);
                        this.SearchNext(commandParameter, data.Parameter);
                    }

                    data.Parameter.ThrowIfCancellationRequested();
                    data.Parameter.RegisterSearchFinished(foundResults);
                    return;
                }
            }
            catch (OperationCanceledException)
            {
                return;
            }

            throw new InvalidOperationException("Unexpected command fired for handler");
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// The command definition class that is used to create the commands for this
        /// implementer.
        /// </summary>
        private class SearchDefinition : SearchCommand
        {
            #region Fields
            /// <summary>
            /// The private field used for the <see cref="Command"/> property.
            /// </summary>
            private RockstarRoutedCommand _command;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="SearchDefinition"/> class.
            /// </summary>
            /// <param name="command">
            /// The command that this definition is wrapping.
            /// </param>
            public SearchDefinition(RockstarRoutedCommand command)
            {
                this._command = command;
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets the System.Windows.Input.RoutedCommand that this definition is wrapping.
            /// </summary>
            public override RockstarRoutedCommand Command
            {
                get { return this._command; }
            }
            #endregion Properties
        } // SearchAction{T}.SearchDefinition
        #endregion Classes
    }  // RSG.Editor.SearchAction{T} {Class}
} // RSG.Editor {Namespace}
