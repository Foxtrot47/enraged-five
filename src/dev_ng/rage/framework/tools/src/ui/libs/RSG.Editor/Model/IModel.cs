﻿//---------------------------------------------------------------------------------------------
// <copyright file="IModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Model
{
    using System;
    using System.ComponentModel;
using RSG.Base.Logging;

    /// <summary>
    /// When implemented represents a model (data source) object in the Editor Framework.
    /// </summary>
    public interface IModel :
        INotifyPropertyChanged,
        ICloneable,
        IEquatable<IModel>,
        IUndoEngineProvider
    {
        #region Properties
        /// <summary>
        /// Gets a value indicating whether this model can be reloaded from the source that
        /// initialised it.
        /// </summary>
        bool CanReload { get; }

        /// <summary>
        /// Gets a reference to this instances parent model object inside a model hierarchy if
        /// appropriate so that the undo engine can be set once on the root.
        /// </summary>
        IModel Parent { get; }

        /// <summary>
        /// Gets the top most model in the hierarchy this this model belongs to.
        /// </summary>
        IModel RootParent { get; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Changes the parent for this object to the one specified.
        /// </summary>
        /// <param name="newParent">
        /// The new parent for this object.
        /// </param>
        void ChangeParent(IModel newParent);

        /// <summary>
        /// Fires the property changed event for the specified property names.
        /// </summary>
        /// <param name="names">
        /// A array of property names that have been changed.
        /// </param>
        void NotifyPropertyChanged(params string[] names);

        /// <summary>
        /// Reloads the model from whichever source it was initialised with.
        /// </summary>
        void ReloadModel();

        /// <summary>
        /// Validates this entity and creates a validation result object containing the errors
        /// and warnings.
        /// </summary>
        /// <param name="recursive">
        /// A value indicating whether any child models to this one should also be validated.
        /// </param>
        /// <returns>
        /// A validation result object containing all of the errors and warnings associated
        /// with this validation pass.
        /// </returns>
        ValidationResult Validate(bool recursive);

        /// <summary>
        /// Validates this entity and adds the errors and warnings to the specified log object.
        /// </summary>
        /// <param name="log">
        /// The log object the errors and warnings for this entity should be added.
        /// </param>
        /// <param name="recursive">
        /// A value indicating whether any child models to this one should also be validated.
        /// </param>
        void Validate(ILog log, bool recursive);
        #endregion Methods
    } // RSG.Editor.Model.IModel {Interface}
} // RSG.Editor.Model {Namespace}
