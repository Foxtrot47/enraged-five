﻿//---------------------------------------------------------------------------------------------
// <copyright file="ICreatesProjectDocument.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel
{
    using RSG.Editor.Controls.Dock.ViewModel;

    /// <summary>
    /// Represents a hierarchy node that contains a project document object that can be placed
    /// inside a document tab.
    /// </summary>
    public interface ICreatesProjectDocument : IHierarchyNode
    {
        #region Methods
        /// <summary>
        /// Creates the document for this node.
        /// </summary>
        /// <param name="pane">
        /// The document pane that the document is going to be opened with.
        /// </param>
        /// <param name="fullPath">
        /// The full-path to the document being created.
        /// </param>
        /// <returns>
        /// A new project document item for this node.
        /// </returns>
        ProjectDocumentItem CreateDocument(DocumentPane pane, string fullPath);
        #endregion Methods
    } // RSG.Project.ViewModel.ICreatesProjectDocument {Interface}
} // RSG.Project.ViewModel {Namespace}
