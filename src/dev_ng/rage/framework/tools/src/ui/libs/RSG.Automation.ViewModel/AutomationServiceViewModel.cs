﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Configuration.Automation;

namespace RSG.Automation.ViewModel
{

    /// <summary>
    /// Automation Service View-Model.
    /// </summary>
    public class AutomationServiceViewModel : ViewModelBase
    {
        #region Properties
        /// <summary>
        /// Service display name (on TreeView).
        /// </summary>
        public String DisplayName 
        {
            get { return (String.Format("{0} [{1}]", 
                this._automationService.FriendlyName,
                this._automationService.ServerHost.Host.ToUpper())); } 
        }

        /// <summary>
        /// Core service.
        /// </summary>
        public IAutomationServiceConfig AutomationService
        {
            get { return _automationService; }
            private set
            {
                SetProperty(ref _automationService, value, "AutomationService");
            }
        }
        private IAutomationServiceConfig _automationService;

        /// <summary>
        /// If the tree view for this automation service is expanded.
        /// </summary>
        public bool IsExpanded
        {
            get { return _isExpanded; }
            private set
            {
                SetProperty(ref _isExpanded, value, "IsExpanded");
            }
        }
        private bool _isExpanded;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="service"></param>
        public AutomationServiceViewModel(IAutomationServiceConfig service)
        {
            this._automationService = service;
        }
        #endregion // Constructor(s)
    }

} // RSG.Automation.ViewModel namespace
