﻿//---------------------------------------------------------------------------------------------
// <copyright file="IRequiresUpdatingDefinition.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel.Definitions
{
    using System.IO;

    /// <summary>
    /// Represents the definition for a single project item type that requires updating before
    /// it can be added to the project.
    /// </summary>
    public interface IRequiresUpdatingDefinition
    {
        #region Properties
        /// <summary>
        /// Gets the extension that the old file is updated to.
        /// </summary>
        string UpdatedExtension { get; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Called to update an old file format to a new format.
        /// </summary>
        /// <param name="inStream">
        /// The stream that the old data can be read from.
        /// </param>
        /// <param name="outStream">
        /// The new stream that the new data can be written to.
        /// </param>
        /// <param name="project">
        /// The project the updated file is being added to.
        /// </param>
        void Update(Stream inStream, Stream outStream, ProjectNode project);
        #endregion Methods
    } // RSG.Project.ViewModel.Definitions.IRequiresUpdatingDefinition {Interface}
} // RSG.Project.ViewModel.Definitions {Namespace}
