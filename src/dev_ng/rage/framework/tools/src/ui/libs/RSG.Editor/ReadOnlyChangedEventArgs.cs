﻿//---------------------------------------------------------------------------------------------
// <copyright file="ReadOnlyChangedEventArgs.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    /// <summary>
    /// Provides data for a event that is representing a read-only change to a file.
    /// </summary>
    public class ReadOnlyChangedEventArgs : ValueChangedEventArgs<bool>
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="FullPath"/> property.
        /// </summary>
        private string _fullPath;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ReadOnlyChangedEventArgs"/> class.
        /// </summary>
        /// <param name="oldValue">
        /// The value before the changed occurred.
        /// </param>
        /// <param name="newValue">
        /// The value after the changed occurred.
        /// </param>
        /// <param name="fullpath">
        /// The full path to the file whose read-only state has changed.
        /// </param>
        public ReadOnlyChangedEventArgs(bool oldValue, bool newValue, string fullpath)
            : base(oldValue, newValue)
        {
            this._fullPath = fullpath;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the full path to the file whose read-only state has changed.
        /// </summary>
        public string FullPath
        {
            get { return this._fullPath; }
        }
        #endregion Properties
    } // RSG.Editor.ReadOnlyChangedEventArgs {Class}
} // RSG.Editor {Namespace}
