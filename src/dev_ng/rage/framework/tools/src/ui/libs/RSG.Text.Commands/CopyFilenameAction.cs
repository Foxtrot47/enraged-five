﻿//---------------------------------------------------------------------------------------------
// <copyright file="CopyFilenameAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Commands
{
    using System.Diagnostics;
    using System.Linq;
    using System.Windows;
    using RSG.Editor;
    using RSG.Text.ViewModel;

    /// <summary>
    /// Implements the <see cref="TextCommands.CopyFilename"/> command.
    /// </summary>
    public class CopyFilenameAction : ButtonAction<TextLineCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CopyFilenameAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public CopyFilenameAction(ParameterResolverDelegate<TextLineCommandArgs> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(TextLineCommandArgs args)
        {
            return args.Selected != null && args.Selected.Any();
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(TextLineCommandArgs args)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            bool first = true;
            foreach (LineViewModel line in args.Selected)
            {
                if (System.String.IsNullOrEmpty(line.AudioFilepath))
                {
                    continue;
                }

                if (!first)
                {
                    sb.AppendLine();
                }

                sb.Append(line.AudioFilepath);
                first = false;
            }

            Clipboard.SetText(sb.ToString());
        }
        #endregion Methods
    } // RSG.Text.Commands.CopyFilenameAction {Class}
} // RSG.Text.Commands {Namespace}
