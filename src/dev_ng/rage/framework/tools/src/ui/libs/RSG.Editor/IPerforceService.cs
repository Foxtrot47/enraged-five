﻿//---------------------------------------------------------------------------------------------
// <copyright file="IPerforceService.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    /// <summary>
    /// When implemented represents a service that can be used to handle perforce requests.
    /// </summary>
    public interface IPerforceService
    {
        #region Properties
        /// <summary>
        /// Gets or sets the perforce command execution timeout.
        /// </summary>
        TimeSpan CommandTimeout { get; set; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Adds the specified files and puts them into the default change list.
        /// </summary>
        /// <param name="fullPaths">
        ///  The full paths to the files that need adding.
        /// </param>
        void AddFiles(IList<string> fullPaths);

        /// <summary>
        /// Adds the specified files and puts them into the change list with the specified id.
        /// </summary>
        /// <param name="fullPaths">
        ///  The full paths to the files that need adding.
        /// </param>
        /// <param name="changelistId">
        /// The id of the change list the files should be added to.
        /// </param>
        void AddFiles(IList<string> fullPaths, int changelistId);

        /// <summary>
        /// Returns which files are checked out locally from the list passed in.
        /// </summary>
        /// <param name="fullPaths">
        /// The collection containing the full paths of the files to check.
        /// </param>
        /// <param name="actions">
        /// The actions to test.
        /// </param>
        /// <returns>
        /// True if any of the given files have the actions; otherwise, false.
        /// </returns>
        bool AnyFilesWithState(IList<String> fullPaths, params PerforceFileAction[] actions);

        /// <summary>
        /// Creates a new changelist using the specified description.
        /// </summary>
        /// <param name="description">
        /// The description of the change list we are creating.
        /// </param>
        /// <returns>
        /// The id of changelist that was created.
        /// </returns>
        int CreateChangelist(string description);

        /// <summary>
        /// Checks out the specified files into a new change list with the specified
        /// description and optionally adds any file that isn't currently mapped.
        /// </summary>
        /// <param name="fullPaths">
        ///  The full paths to the files that need checking out.
        /// </param>
        /// <param name="description">
        /// The description of the change list the files should be checked out in.
        /// </param>
        /// <param name="add">
        /// A value indicating whether any files currently not mapped should be added.
        /// </param>
        void EditFiles(IList<string> fullPaths, string description, bool add);

        /// <summary>
        /// Checks out the specified files into the change list with the specified id and
        /// optionally adds any file that isn't currently mapped.
        /// </summary>
        /// <param name="fullPaths">
        ///  The full paths to the files that need checking out.
        /// </param>
        /// <param name="changelistId">
        /// The id of the change list the files should be checked out in.
        /// </param>
        /// <param name="add">
        /// A value indicating whether any files currently not mapped should be added.
        /// </param>
        void EditFiles(IList<string> fullPaths, int changelistId, bool add);

        /// <summary>
        /// Checks out the specified files into the default change list.
        /// </summary>
        /// <param name="fullPaths">
        ///  The full paths to the files that need checking out.
        /// </param>
        void EditFiles(IList<string> fullPaths);

        /// <summary>
        /// Returns which files are checked out locally from the list passed in.
        /// </summary>
        /// <param name="fullPaths">
        /// The collection containing the full paths of the files to check.
        /// </param>
        /// <returns>
        /// A list containing the files that are currently checked out out of the specified
        /// list.
        /// </returns>
        IList<string> GetCheckedOutFiles(IList<string> fullPaths);

        /// <summary>
        /// Gets the contents of the specified list of files.
        /// </summary>
        /// <param name="fileSpecs">
        /// The file specifications for the files to get the contents for.
        /// </param>
        /// <returns>
        /// Mapping of file specifications to streams containing the file contents.
        /// </returns>
        /// <remarks>
        /// The calling code is in charge of disposing of the returned streams.
        /// </remarks>
        IDictionary<IPerforceFileSpec, Stream> GetFileContents(IList<IPerforceFileSpec> fileSpecs);

        /// <summary>
        /// Gets the metadata for the specified list of files. The metadata contains the
        /// results from the 'fstat' perforce command.
        /// </summary>
        /// <param name="fullPaths">
        /// The collection containing the full paths of the files to get.
        /// </param>
        /// <returns>
        /// A list containing the metadata for the specified files.
        /// </returns>
        IList<IPerforceFileMetaData> GetFileMetaData(IList<string> fullPaths);

        /// <summary>
        /// Gets the metadata for the specified list of files. The metadata contains the
        /// results from the 'fstat' perforce command.
        /// </summary>
        /// <param name="fileSpecs">
        /// The file specifications for the files to get metadata for.
        /// </param>
        /// <returns>
        /// A list containing the metadata for the specified files.
        /// </returns>
        IList<IPerforceFileMetaData> GetFileMetaData(IList<IPerforceFileSpec> fileSpecs);

        /// <summary>
        /// Gets the head revision of the specified files.
        /// </summary>
        /// <param name="fullPaths">
        /// The full paths to the files that needs to be synced to the head revision.
        /// </param>
        void GetLatest(IList<string> fullPaths);

        /// <summary>
        /// Gets (or previews) the head revision of the specified files.
        /// </summary>
        /// <param name="fileSpecs">
        /// The file specifications for the files to get metadata for.
        /// </param>
        /// <param name="preview">
        /// Flag indicating whether we only wish to preview the results of the sync
        /// operation.
        /// </param>
        /// <returns>
        /// A list containing the metadata for the files that were/would be synced
        /// </returns>
        IList<IPerforceFileMetaData> GetLatest(IList<IPerforceFileSpec> fileSpecs, bool preview = false);

        /// <summary>
        /// Gets a set of local paths from a set of depot paths.
        /// </summary>
        /// <param name="depotPaths">
        /// The depot paths to convert to local paths.
        /// </param>
        /// <returns>
        /// The list of local paths which correspond to the specified list of depot paths.
        /// </returns>
        IList<string> GetLocalPathsFromDepotPaths(IList<string> depotPaths);

        /// <summary>
        /// Marks the specified files for deletion.
        /// </summary>
        /// <param name="fullPaths">
        /// The full paths to the files that are to be mark for delete.
        /// </param>
        void MarkForDelete(IList<string> fullPaths);

        /// <summary>
        /// Moves the specified files into the specified changelist.
        /// </summary>
        /// <param name="fullPaths">
        /// The full paths to the files that needs to be synced to the head revision.
        /// </param>
        /// <param name="changelistId">
        /// The id of the changelist to move the files into.
        /// </param>
        void MoveFilesIntoChangelist(IList<string> fullPaths, int changelistId);

        /// <summary>
        /// Reverts all of the specified files.
        /// </summary>
        /// <param name="fullPaths">
        /// The full paths to the files that are to be reverted.
        /// </param>
        void RevertFiles(IList<string> fullPaths);

        /// <summary>
        /// Reverts any of the specified files that are unchanged.
        /// </summary>
        /// <param name="fullPaths">
        /// The full paths to the files that needs to be synced to the head revision.
        /// </param>
        void RevertUnchangedFiles(IList<string> fullPaths);

        /// <summary>
        /// Submits the specified changelist.
        /// </summary>
        /// <param name="changelistId">
        /// The id of the changelist to move the files into.
        /// </param>
        void SubmitChangelist(int changelistId);

        /// <summary>
        /// Creates a disposable object whose scope turns the perforce exceptions off.
        /// </summary>
        /// <returns></returns>
        IDisposable SuspendExceptionsScope();

        /// <summary>
        /// Attempts to delete the specified changelist.
        /// </summary>
        /// <param name="changelistId">
        /// The id of the changelist to attempt to delete.
        /// </param>
        void TryToDeleteChangelist(int changelistId);
        #endregion Methods
    } // RSG.Editor.IPerforceService {Interface}
} // RSG.Editor {Namespace}
