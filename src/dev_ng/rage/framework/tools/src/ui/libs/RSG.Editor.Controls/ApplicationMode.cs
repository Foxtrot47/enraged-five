﻿//---------------------------------------------------------------------------------------------
// <copyright file="ApplicationMode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    /// <summary>
    /// The different modes the application can be started under. These modes determine how
    /// many instances of the same application can be running at the same time.
    /// </summary>
    public enum ApplicationMode
    {
        /// <summary>
        /// Only a single instance of this application is ever allowed to be running at the
        /// same time.
        /// </summary>
        Single,

        /// <summary>
        /// Any number of instances of this application are allowed to be running at the
        /// same time.
        /// </summary>
        Multiple
    } // RSG.Editor.Controls.ApplicationMode {Enum}
} // RSG.Editor.Controls {Namespace}
