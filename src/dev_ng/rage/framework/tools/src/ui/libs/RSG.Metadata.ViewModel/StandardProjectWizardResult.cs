﻿//---------------------------------------------------------------------------------------------
// <copyright file="StandardProjectWizardResult.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel
{
    /// <summary>
    /// The structure containing the results provided by the standard project wizard.
    /// </summary>
    public struct StandardProjectWizardResult
    {
        #region Properties
        /// <summary>
        /// Gets or sets the root directory set for the definitions.
        /// </summary>
        public string DefinitionRoot
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether from the root definitions directory sub
        /// directories should be included.
        /// </summary>
        public bool IncludeSubDirectories
        {
            get;
            set;
        }
        #endregion Properties
    } // RSG.Metadata.ViewModel.StandardProjectWizardResult {Structure}
} // RSG.Metadata.ViewModel {Namespace}
