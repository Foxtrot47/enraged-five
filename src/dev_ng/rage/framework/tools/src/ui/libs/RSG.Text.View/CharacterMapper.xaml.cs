﻿
namespace RSG.Text.View
{
    using System.Windows.Input;
    using RSG.Editor.Controls;

    /// <summary>
    /// Interaction logic for CharacterMapper.xaml
    /// </summary>
    public partial class CharacterMapper : RsUserControl
    {
        private static RoutedCommand _unmap;

        static CharacterMapper()
        {
            _unmap = new RoutedCommand("Unmap", typeof(CharacterMapper));
        }

        public CharacterMapper()
        {
            InitializeComponent();

            this.CommandBindings.Add(new CommandBinding(Unmap, OnUnmap, CanUnmap));
        }

        public static RoutedCommand Unmap
        {
            get { return _unmap; }
        }

        public void CanUnmap(object s, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        public void OnUnmap(object s, ExecutedRoutedEventArgs e)
        {
            RSG.Text.ViewModel.CharacterMappingViewModel vm = e.Parameter as RSG.Text.ViewModel.CharacterMappingViewModel;
            vm.SelectedCharacter = null;
        }
    }
}
