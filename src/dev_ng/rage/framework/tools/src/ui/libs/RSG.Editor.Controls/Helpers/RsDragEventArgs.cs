﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsDragEventArgs.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Helpers
{
    using System.Windows;

    /// <summary>
    /// Contains information and event data for a screen drag event.
    /// </summary>
    public class RsDragEventArgs : RoutedEventArgs
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="ScreenPoint"/> property.
        /// </summary>
        private Point _screenPoint;

        /// <summary>
        /// The private field used for the <see cref="State"/> property.
        /// </summary>
        private DragState _state;

        /// <summary>
        /// The private field used for the <see cref="HorizontalChange"/> property.
        /// </summary>
        private double _horizontalChange;

        /// <summary>
        /// The private field used for the <see cref="VerticalChange"/> property.
        /// </summary>
        private double _verticalChange;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="RsDragEventArgs"/> class.
        /// </summary>
        /// <param name="routedEvent">
        /// The routed event identifier for this instance.
        /// </param>
        /// <param name="screenPoint">
        /// The screen point that is associated with this event, either the original screen
        /// point, the current screen point, or the point the drag was completed.
        /// </param>
        /// <param name="state">
        /// The current state of the drag operation this event is reporting on.
        /// </param>
        /// <param name="horizontalChange">
        /// The distance the mouse cursor has changed in the horizontal direction.
        /// </param>
        /// <param name="verticalChange">
        /// The distance the mouse cursor has changed in the vertical direction.
        /// </param>
        public RsDragEventArgs(
            RoutedEvent routedEvent,
            Point screenPoint,
            DragState state,
            double horizontalChange = 0.0,
            double verticalChange = 0.0)
            : base(routedEvent)
        {
            this._screenPoint = screenPoint;
            this._state = state;
            this._horizontalChange = horizontalChange;
            this._verticalChange = verticalChange;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether the drag operation has finished.
        /// </summary>
        public bool HasFinished
        {
            get { return this._state != DragState.NotFinished; }
        }

        /// <summary>
        /// Gets a value indicating whether the drag operation has ended with movement.
        /// </summary>
        public bool IsValidDrag
        {
            get { return this.State == DragState.FinishedWithMovement; }
        }

        /// <summary>
        /// Gets the point in screen space where the drag event occurred.
        /// </summary>
        public Point ScreenPoint
        {
            get { return this._screenPoint; }
        }

        /// <summary>
        /// Gets the distance the mouse cursor has changed in the horizontal direction.
        /// </summary>
        public double HorizontalChange
        {
            get { return this._horizontalChange; }
        }

        /// <summary>
        /// Gets the distance the mouse cursor has changed in the vertical direction.
        /// </summary>
        public double VerticalChange
        {
            get { return this._verticalChange; }
        }

        /// <summary>
        /// Gets the current state of the drag operation.
        /// </summary>
        public DragState State
        {
            get { return this._state; }
        }
        #endregion Properties
    } // RSG.Editor.Controls.Helpers.RsDragEventArgs {Class}
} // RSG.Editor.Controls.Helpers {Namespace}
