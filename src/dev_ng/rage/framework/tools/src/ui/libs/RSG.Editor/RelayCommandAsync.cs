﻿//---------------------------------------------------------------------------------------------
// <copyright file="RelayCommandAsync.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Threading.Tasks;

    /// <summary>
    /// Asynchronous version of the RelayCommand.
    /// </summary>
    public class RelayCommandAsync : RelayCommandAsync<Object>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="RelayCommandAsync"/> class, using the
        /// specified action method as a command delegate.
        /// </summary>
        /// <param name="action">
        /// The method to call when this command executes.
        /// </param>
        public RelayCommandAsync(Func<Object, Task> action)
            : base(action)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RelayCommandAsync"/> class, using the
        /// specified prediction and action methods as command delegates.
        /// </summary>
        /// <param name="action">
        /// The method to call when this command executes. This only gets called if the
        /// specified prediction method returns true.
        /// </param>
        /// <param name="prediction">
        /// The method to call when determining if this command can be executed or not. Can
        /// be null.
        /// </param>
        public RelayCommandAsync(Func<Object, Task> action, Predicate<Object> prediction)
            : base(action, prediction)
        {
        }
        #endregion Constructors
    } // RSG.Editor.RelayCommandAsync {Class}
} // RSG.Editor {Namespace}
