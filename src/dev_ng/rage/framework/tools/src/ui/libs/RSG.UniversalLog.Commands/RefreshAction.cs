﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor;
using RSG.UniversalLog.ViewModel;

namespace RSG.UniversalLog.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public class RefreshAction : ButtonAction<UniversalLogDataContext>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="RefreshImplementer"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public RefreshAction(ParameterResolverDelegate<UniversalLogDataContext> resolver)
            : base(resolver)
        {
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// Override to set logic against the can execute command handler.
        /// </summary>
        /// <param name="commandParameter"></param>
        /// <returns></returns>
        public override bool CanExecute(UniversalLogDataContext dc)
        {
            return dc.AnyFilesLoaded;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="commandParameter">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(UniversalLogDataContext dc)
        {
            dc.RefreshFiles();
        }
        #endregion // Overrides
    } // RefreshAction
}
