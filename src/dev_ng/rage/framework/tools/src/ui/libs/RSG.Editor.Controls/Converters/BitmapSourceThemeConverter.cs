﻿//---------------------------------------------------------------------------------------------
// <copyright file="BitmapSourceThemeConverter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Converters
{
    using System.Globalization;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using RSG.Editor.Controls.Helpers;

    /// <summary>
    /// A converter used to convert a input bitmap source object to a themed bitmap source
    /// object. This class cannot be inherited.
    /// </summary>
    public sealed class BitmapSourceThemeConverter
        : ValueConverter<BitmapSource, SolidColorBrush, bool, BitmapSource>
    {
        #region Methods
        /// <summary>
        /// Converters the two specified source values to a single target value.
        /// </summary>
        /// <param name="input">
        /// The input bitmap source object to transform.
        /// </param>
        /// <param name="background">
        /// The theme background colour that is to be used in the transformation.
        /// </param>
        /// <param name="enabled">
        /// A value indicating whether the transformed bitmap source image should be
        /// grey-scaled or not.
        /// </param>
        /// <param name="parameter">
        /// The converter parameter to use (optional a colour specifying the grey-scale bias
        /// weights.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A target value created by converting the two specified source values.
        /// </returns>
        protected override BitmapSource Convert(
            BitmapSource input,
            SolidColorBrush background,
            bool enabled,
            object parameter,
            CultureInfo culture)
        {
            if (input == null)
            {
                return null;
            }

            BitmapSource source;
            if (background == null || (background.Color.A == 0 && enabled))
            {
                source = input;
            }
            else
            {
                Color bias = Color.FromArgb(127, 255, 255, 255);
                if (parameter is Color)
                {
                    bias = (Color)parameter;
                }

                ThemeBitmapArguments args = new ThemeBitmapArguments(input);
                args.Background = background.Color;
                args.Enabled = enabled;
                args.Bias = bias;
                source = ThemeHelper.GetOrCreateThemedBitmapSource(args);
            }

            return source;
        }
        #endregion Methods
    } // RSG.Editor.Controls.Converters.BitmapSourceThemeConverter {Class}
} // RSG.Editor.Controls.Converters.Themes {Namespace}
