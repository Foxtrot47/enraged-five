﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor;
using RSG.AnimationToolkit.ViewModel;

namespace RSG.AnimationToolkit.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public class OpenClipFilesAction : ButtonAction<AnimationToolkitDataContext>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="OpenFileImplementer"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public OpenClipFilesAction(ParameterResolverDelegate<AnimationToolkitDataContext> resolver)
            : base(resolver)
        {
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// Override to set logic against the canexecute command handler.
        /// </summary>
        /// <param name="dc">
        /// The command parameter that has been requested.
        /// </param>
        public override bool CanExecute(AnimationToolkitDataContext dc)
        {
            return (dc.GetViewModel.SelectedModeTabIndex == 0);
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="dc">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(AnimationToolkitDataContext dc)
        {
            ICommonDialogService dlgService = this.GetService<ICommonDialogService>();
            if (dlgService == null)
            {
                Debug.Assert(false, "Unable to open file as service is missing.");
                return;
            }

            String[] filepaths;
            if (!dlgService.ShowMultiSelectFolder(new Guid(), dc.GetViewModel.GetInitialExportPath(), "", out filepaths))
            {
                return;
            }

            // Open the file.
            dc.GetViewModel.OpenClipFiles(filepaths);
            dc.GetViewModel.SelectedTabIndex = Convert.ToInt32(RSG.AnimationToolkit.ViewModel.AnimationToolkitViewModel.Tabs.ClipProcessingTab);
        }
        #endregion // Overrides
    } // OpenFileAction
}
