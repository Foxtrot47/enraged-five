﻿//---------------------------------------------------------------------------------------------
// <copyright file="AddExistingItemAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Windows;
    using RSG.Base.Extensions;
    using RSG.Editor;
    using RSG.Interop.Microsoft.Windows;
    using RSG.Project.Commands.Resources;
    using RSG.Project.Model;
    using RSG.Project.ViewModel;
    using RSG.Project.ViewModel.Definitions;

    /// <summary>
    /// Contains the logic for the <see cref="ProjectCommands.AddExistingItem"/> routed
    /// command. This class cannot be inherited.
    /// </summary>
    public sealed class AddExistingItemAction : ButtonAction<ProjectCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="AddExistingItemAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public AddExistingItemAction(ProjectCommandResolver resolver)
            : base(new ParameterResolverDelegate<ProjectCommandArgs>(resolver))
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ProjectCommandArgs args)
        {
            if (args == null)
            {
                return false;
            }

            List<ProjectNode> projectSelection = this.ProjectSelection(args.SelectedNodes);
            if (projectSelection.Count != 1 || !projectSelection[0].CanHaveItemsAdded)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(ProjectCommandArgs args)
        {
            ICommonDialogService dlgService = this.GetService<ICommonDialogService>();
            IMessageBoxService msgService = this.GetService<IMessageBoxService>();
            IProjectAddViewService projViewService = this.GetService<IProjectAddViewService>();
            if (dlgService == null || msgService == null || projViewService == null)
            {
                Debug.Assert(false, "Unable to add existing item, a service is missing.");
                return;
            }

            List<ProjectNode> projectSelection = this.ProjectSelection(args.SelectedNodes);
            ProjectNode project = projectSelection.FirstOrDefault();
            if (projectSelection.Count != 1 || !project.CanHaveItemsAdded)
            {
                return;
            }

            IHierarchyNode parent = this.GetNewItemsParent(args.SelectedNodes) ?? project;
            if (parent == null)
            {
                Debug.Assert(parent != null, "Parent sanity check failed.");
                return;
            }

            IProjectItemScope scope = project.Model as IProjectItemScope;
            if (scope == null)
            {
                Debug.Assert(false, "Unable to add existing item, no scope can be found.");
                return;
            }

            string filter = this.GetOpenFilter(project);
            if (filter == null)
            {
                Debug.Assert(false, "Unable to determine the filter string from definition.");
                return;
            }

            string[] fullPaths = null;
            string directory = scope.DirectoryPath;
            if (!dlgService.ShowOpenFile(directory, null, filter, 0, out fullPaths))
            {
                throw new OperationCanceledException();
            }

            bool yesToAll = false;
            bool noToAll = false;
            List<string> updatedFullPaths = new List<string>();
            List<string> filesToUpdate = new List<string>();
            for (int i = 0; i < fullPaths.Length; i++)
            {
                string fullPath = fullPaths[i];
                string extension = Path.GetExtension(fullPath);
                ProjectItemDefinition def = project.Definition.GetItemDefinition(extension);
                IRequiresUpdatingDefinition updateDef = def as IRequiresUpdatingDefinition;
                if (updateDef == null)
                {
                    updatedFullPaths.Add(fullPath);
                    continue;
                }

                bool update = false;
                if (yesToAll)
                {
                    update = true;
                }
                else if (noToAll)
                {
                    update = false;
                }
                else
                {
                    IMessageBox msgBox = msgService.CreateMessageBox();
                    msgBox.Text = StringTable.UpdateMsg.FormatInvariant(fullPath);
                    msgBox.AddButton(StringTable.UpdateYesButtonContent, 1, true, false);
                    msgBox.AddButton(StringTable.UpdateYesToAllButtonContent, 2, false, false);
                    msgBox.AddButton(StringTable.UpdateNoButtonContent, 3, false, false);
                    msgBox.AddButton(StringTable.UpdateNoToAllButtonContent, 4, false, false);
                    msgBox.AddButton(StringTable.UpdateCancelButtonContent, 5, false, true);
                    long result = msgBox.ShowMessageBox();
                    if (result == 1)
                    {
                        update = true;
                    }
                    else if (result == 2)
                    {
                        yesToAll = true;
                        update = true;
                    }
                    else if (result == 3)
                    {
                        update = false;
                    }
                    else if (result == 4)
                    {
                        noToAll = true;
                        update = false;
                    }
                    else if (result == 5)
                    {
                        throw new OperationCanceledException();
                    }
                }

                if (!update)
                {
                    continue;
                }

                filesToUpdate.Add(fullPath);
            }

            if (filesToUpdate.Count > 0)
            {
                updatedFullPaths.AddRange(
                    projViewService.ShowFileUpdatingProgressWindow(project, filesToUpdate));
            }

            foreach (string fullPath in updatedFullPaths)
            {
                string relativePath =
                    Shlwapi.MakeRelativeFromDirectoryToFile(directory, fullPath);
                if (String.IsNullOrWhiteSpace(relativePath))
                {
                    relativePath = fullPath;
                }

                if (scope.Find(relativePath) != null)
                {
                    continue;
                }

                IHierarchyNode newNode = project.AddNewFile(parent, relativePath);
                if (newNode != null && args.NodeSelectionDelegate != null)
                {
                    args.NodeSelectionDelegate(newNode);
                }
            }
        }

        /// <summary>
        /// Retrieves the project selection from the specified selection collection by
        /// collecting all of the selected nodes parent project and removing duplicates.
        /// </summary>
        /// <param name="selection">
        /// The collection containing all of the selected nodes.
        /// </param>
        /// <returns>
        /// The project selection from the specified selection collection.
        /// </returns>
        private List<ProjectNode> ProjectSelection(IEnumerable<IHierarchyNode> selection)
        {
            List<ProjectNode> projectSelection = new List<ProjectNode>();
            if (selection == null)
            {
                return projectSelection;
            }

            foreach (IHierarchyNode selectedNode in selection)
            {
                ProjectNode parentProject = selectedNode.ParentProject;
                if (parentProject != null && !projectSelection.Contains(parentProject))
                {
                    projectSelection.Add(parentProject);
                }
            }

            return projectSelection;
        }

        /// <summary>
        /// Determines which item should be the parent to a new added item from the specified
        /// collection of items.
        /// </summary>
        /// <param name="items">
        /// The collection of items that could be the parent.
        /// </param>
        /// <returns>
        /// The first item if all of them are valid; otherwise, null.
        /// </returns>
        private IHierarchyNode GetNewItemsParent(IEnumerable<IHierarchyNode> items)
        {
            IHierarchyNode parent = null;
            foreach (IHierarchyNode selectedNode in items)
            {
                if (selectedNode == null)
                {
                    continue;
                }

                if (!selectedNode.CanHaveItemsAdded)
                {
                    return null;
                }
                else if (parent == null)
                {
                    parent = selectedNode;
                }
            }

            return parent;
        }

        /// <summary>
        /// Constructs the open filter to use in the open dialog for selecting a file to add.
        /// </summary>
        /// <param name="project">
        /// The project that the item is being added to.
        /// </param>
        /// <returns>
        /// The filter to use in the open dialog.
        /// </returns>
        private string GetOpenFilter(ProjectNode project)
        {
            ProjectDefinition definition = project.Definition;
            if (definition == null)
            {
                Debug.Assert(false, "No definition setup for the selected project.");
                return null;
            }

            return definition.NewItemFilter;
        }
        #endregion Methods
    } // RSG.Project.Commands.AddExistingItemAction {Class}
} // RSG.Project.Commands {Namespace}
