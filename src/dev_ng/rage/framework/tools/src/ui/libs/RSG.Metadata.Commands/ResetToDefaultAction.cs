﻿//---------------------------------------------------------------------------------------------
// <copyright file="ResetToDefaultAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Commands
{
    using RSG.Editor;
    using RSG.Metadata.ViewModel.Tunables;

    /// <summary>
    /// Contains the logic for the <see cref="MetadataCommands.ResetToDefault"/> routed
    /// command. This class cannot be inherited.
    /// </summary>
    public sealed class ResetToDefaultAction : MetadataCommandActionBase
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ResetToDefaultAction"/> class.
        /// </summary>
        /// <param name="serviceProvider">
        /// The service provider that is used by this action to perform application specific
        /// operations, for example selecting a file to open.
        /// </param>
        public ResetToDefaultAction(ICommandServiceProvider serviceProvider)
            : base(serviceProvider)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ResetToDefaultAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public ResetToDefaultAction(MetadataCommandArgsResolver resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(MetadataCommandArgs args)
        {
            return true;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(MetadataCommandArgs args)
        {
            foreach (ITunableViewModel tunable in args.SelectedTunables)
            {
                tunable.ResetValueToDefault();
            }
        }
        #endregion Methods
    } // RSG.Metadata.Commands.ResetToDefaultAction {Class}
} // RSG.Metadata.Commands {Namespace}
