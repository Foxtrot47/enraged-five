﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsSplitterPanel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Automation;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using RSG.Editor.Controls.Helpers;

    /// <summary>
    /// Represents a panel that arranges its children based on the value of the SplitterLength
    /// attached property for each child.
    /// </summary>
    public class RsSplitterPanel : Panel
    {
        #region Fields
        /// <summary>
        /// Identifies the ActualSplitterLength read-only attached dependency property.
        /// </summary>
        public static readonly DependencyProperty ActualSplitterLengthProperty;

        /// <summary>
        /// Identifies the Index read-only attached dependency property.
        /// </summary>
        public static readonly DependencyProperty IndexProperty;

        /// <summary>
        /// Identifies the IsFirst read-only attached dependency property.
        /// </summary>
        public static readonly DependencyProperty IsFirstProperty;

        /// <summary>
        /// Identifies the IsLast read-only attached dependency property.
        /// </summary>
        public static readonly DependencyProperty IsLastProperty;

        /// <summary>
        /// Identifies the Maximum Length attached dependency property.
        /// </summary>
        public static readonly DependencyProperty MaximumLengthProperty;

        /// <summary>
        /// Identifies the Minimum Length attached dependency property.
        /// </summary>
        public static readonly DependencyProperty MinimumLengthProperty;

        /// <summary>
        /// Identifies the <see cref="Orientation"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty OrientationProperty;

        /// <summary>
        /// Identifies the ShowResizePreview attached dependency property.
        /// </summary>
        public static readonly DependencyProperty ShowResizePreviewProperty;

        /// <summary>
        /// Identifies the Splitter Length attached dependency property.
        /// </summary>
        public static readonly DependencyProperty SplitterLengthProperty;

        /// <summary>
        /// Identifies the ActualSplitterLength dependency property key.
        /// </summary>
        private static readonly DependencyPropertyKey _actualSplitterLengthPropertyKey;

        /// <summary>
        /// Identifies the Index dependency property key.
        /// </summary>
        private static readonly DependencyPropertyKey _indexPropertyKey;

        /// <summary>
        /// Identifies the IsFirst dependency property key.
        /// </summary>
        private static readonly DependencyPropertyKey _isFirstPropertyKey;

        /// <summary>
        /// Identifies the IsLast dependency property key.
        /// </summary>
        private static readonly DependencyPropertyKey _isLastPropertyKey;

        /// <summary>
        /// The private reference to the review adorner that is currently being shown
        /// to the user.
        /// </summary>
        private RsSplitterResizePreviewWindow _currentPreviewWindow;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsSplitterPanel"/> class.
        /// </summary>
        static RsSplitterPanel()
        {
            FrameworkPropertyMetadataOptions metadataOptions =
                FrameworkPropertyMetadataOptions.AffectsParentMeasure |
                FrameworkPropertyMetadataOptions.AffectsParentArrange;

            SplitterLengthProperty =
                DependencyProperty.RegisterAttached(
                "SplitterLength",
                typeof(SplitterLength),
                typeof(RsSplitterPanel),
                new FrameworkPropertyMetadata(
                    new SplitterLength(100.0),
                    metadataOptions));

            OrientationProperty =
                DependencyProperty.Register(
                "Orientation",
                typeof(Orientation),
                typeof(RsSplitterPanel),
                new FrameworkPropertyMetadata(
                    Orientation.Vertical, FrameworkPropertyMetadataOptions.AffectsMeasure));

            ShowResizePreviewProperty =
                DependencyProperty.RegisterAttached(
                "ShowResizePreview",
                typeof(bool),
                typeof(RsSplitterPanel),
                new FrameworkPropertyMetadata(
                    false, FrameworkPropertyMetadataOptions.Inherits));

            MinimumLengthProperty =
                DependencyProperty.RegisterAttached(
                "MinimumLength",
                typeof(double),
                typeof(RsSplitterPanel),
                new FrameworkPropertyMetadata(
                    5.0, metadataOptions));

            MaximumLengthProperty =
                DependencyProperty.RegisterAttached(
                "MaximumLength",
                typeof(double),
                typeof(RsSplitterPanel),
                new FrameworkPropertyMetadata(
                    double.MaxValue, metadataOptions));

            _actualSplitterLengthPropertyKey =
                DependencyProperty.RegisterAttachedReadOnly(
                "ActualSplitterLength",
                typeof(double),
                typeof(RsSplitterPanel),
                new FrameworkPropertyMetadata(0.0));

            _indexPropertyKey =
                DependencyProperty.RegisterAttachedReadOnly(
                "Index",
                typeof(int),
                typeof(RsSplitterPanel),
                new FrameworkPropertyMetadata(-1));

            _isFirstPropertyKey =
                DependencyProperty.RegisterAttachedReadOnly(
                "IsFirst",
                typeof(bool),
                typeof(RsSplitterPanel),
                new FrameworkPropertyMetadata(false));

            _isLastPropertyKey =
                DependencyProperty.RegisterAttachedReadOnly(
                "IsLast",
                typeof(bool),
                typeof(RsSplitterPanel),
                new FrameworkPropertyMetadata(false));

            ActualSplitterLengthProperty = _actualSplitterLengthPropertyKey.DependencyProperty;
            IndexProperty = _indexPropertyKey.DependencyProperty;
            IsFirstProperty = _isFirstPropertyKey.DependencyProperty;
            IsLastProperty = _isLastPropertyKey.DependencyProperty;

            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsSplitterPanel),
                new FrameworkPropertyMetadata(typeof(RsSplitterPanel)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsSplitterPanel" /> class.
        /// </summary>
        public RsSplitterPanel()
        {
            DragStartedEventHandler handler = this.OnSplitterDragStarted;
            this.AddHandler(Thumb.DragStartedEvent, handler);
            AutomationProperties.SetAutomationId(this, "RsSplitterPanel");
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets a value that represents the orientation the child items are
        /// arranged in.
        /// </summary>
        public Orientation Orientation
        {
            get { return (Orientation)this.GetValue(OrientationProperty); }
            set { this.SetValue(OrientationProperty, value); }
        }

        /// <summary>
        /// Gets a value indicating whether the preview window is currently visible.
        /// </summary>
        private bool IsShowingResizePreview
        {
            get { return this._currentPreviewWindow != null; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Gets the ActualSplitterLength dependency property value for the specified element.
        /// </summary>
        /// <param name="element">
        /// The element to get the ActualSplitterLength dependency property value for.
        /// </param>
        /// <returns>
        /// The value of the ActualSplitterLength dependency property on the specified element.
        /// </returns>
        /// <exception cref="RSG.Editor.SmartArgumentNullException">
        /// Thrown if the specified <paramref name="element"/> is null.
        /// </exception>
        public static double GetActualSplitterLength(UIElement element)
        {
            if (element == null)
            {
                throw new SmartArgumentNullException(() => element);
            }

            return (double)element.GetValue(ActualSplitterLengthProperty);
        }

        /// <summary>
        /// Gets the Index dependency property value for the specified element.
        /// </summary>
        /// <param name="element">
        /// The element to get the Index dependency property value for.
        /// </param>
        /// <returns>
        /// The value of the Index dependency property on the specified element.
        /// </returns>
        /// <exception cref="RSG.Editor.SmartArgumentNullException">
        /// Thrown if the specified <paramref name="element"/> is null.
        /// </exception>
        public static int GetIndex(UIElement element)
        {
            if (element == null)
            {
                throw new SmartArgumentNullException(() => element);
            }

            return (int)element.GetValue(IndexProperty);
        }

        /// <summary>
        /// Gets the IsFirst dependency property value for the specified element.
        /// </summary>
        /// <param name="element">
        /// The element to get the IsFirst dependency property value for.
        /// </param>
        /// <returns>
        /// The value of the IsFirst dependency property on the specified element.
        /// </returns>
        /// <exception cref="RSG.Editor.SmartArgumentNullException">
        /// Thrown if the specified <paramref name="element"/> is null.
        /// </exception>
        public static bool GetIsFirst(UIElement element)
        {
            if (element == null)
            {
                throw new SmartArgumentNullException(() => element);
            }

            return (bool)element.GetValue(IsFirstProperty);
        }

        /// <summary>
        /// Gets the IsLast dependency property value for the specified element.
        /// </summary>
        /// <param name="element">
        /// The element to get the IsLast dependency property value for.
        /// </param>
        /// <returns>
        /// The value of the IsLast dependency property on the specified element.
        /// </returns>
        /// <exception cref="RSG.Editor.SmartArgumentNullException">
        /// Thrown if the specified <paramref name="element"/> is null.
        /// </exception>
        public static bool GetIsLast(UIElement element)
        {
            if (element == null)
            {
                throw new SmartArgumentNullException(() => element);
            }

            return (bool)element.GetValue(IsLastProperty);
        }

        /// <summary>
        /// Gets the MaximumLength dependency property value for the specified element.
        /// </summary>
        /// <param name="element">
        /// The element to get the MaximumLength dependency property value for.
        /// </param>
        /// <returns>
        /// The value of the MaximumLength dependency property on the specified element.
        /// </returns>
        /// <exception cref="RSG.Editor.SmartArgumentNullException">
        /// Thrown if the specified <paramref name="element"/> is null.
        /// </exception>
        public static double GetMaximumLength(UIElement element)
        {
            if (element == null)
            {
                throw new SmartArgumentNullException(() => element);
            }

            return (double)element.GetValue(MaximumLengthProperty);
        }

        /// <summary>
        /// Gets the MinimumLength dependency property value for the specified element.
        /// </summary>
        /// <param name="element">
        /// The element to get the MinimumLength dependency property value for.
        /// </param>
        /// <returns>
        /// The value of the MinimumLength dependency property on the specified element.
        /// </returns>
        /// <exception cref="RSG.Editor.SmartArgumentNullException">
        /// Thrown if the specified <paramref name="element"/> is null.
        /// </exception>
        public static double GetMinimumLength(UIElement element)
        {
            if (element == null)
            {
                throw new SmartArgumentNullException(() => element);
            }

            if (GetIsLast(element))
            {
                return (double)element.GetValue(MinimumLengthProperty);
            }
            else
            {
                double splitterSize = RsSplitterItemsControl.GetSplitterGripSize(element);
                splitterSize = Math.Max(0.0, splitterSize);
                double min = (double)element.GetValue(MinimumLengthProperty);
                min = Math.Max(0.0, min);

                return splitterSize + min;
            }
        }

        /// <summary>
        /// Gets the ShowResizePreview dependency property value for the specified element.
        /// </summary>
        /// <param name="element">
        /// The element to get the ShowResizePreview dependency property value for.
        /// </param>
        /// <returns>
        /// The value of the ShowResizePreview dependency property on the specified element.
        /// </returns>
        /// <exception cref="RSG.Editor.SmartArgumentNullException">
        /// Thrown if the specified <paramref name="element"/> is null.
        /// </exception>
        public static bool GetShowResizePreview(UIElement element)
        {
            if (element == null)
            {
                throw new SmartArgumentNullException(() => element);
            }

            return (bool)element.GetValue(ShowResizePreviewProperty);
        }

        /// <summary>
        /// Gets the SplitterLength dependency property value for the specified element.
        /// </summary>
        /// <param name="element">
        /// The element to get the SplitterLength dependency property value for.
        /// </param>
        /// <returns>
        /// The value of the SplitterLength dependency property on the specified element.
        /// </returns>
        /// <exception cref="RSG.Editor.SmartArgumentNullException">
        /// Thrown if the specified <paramref name="element"/> is null.
        /// </exception>
        public static SplitterLength GetSplitterLength(UIElement element)
        {
            if (element == null)
            {
                throw new SmartArgumentNullException(() => element);
            }

            return (SplitterLength)element.GetValue(SplitterLengthProperty);
        }

        /// <summary>
        /// Sets the MaximumLength dependency property on the specified element to the
        /// specified value.
        /// </summary>
        /// <param name="element">
        /// The element to set the MaximumLength dependency property on.
        /// </param>
        /// <param name="value">
        /// The value to set the MaximumLength dependency property to on the
        /// specified element.
        /// </param>
        /// <exception cref="RSG.Editor.SmartArgumentNullException">
        /// Thrown if the specified <paramref name="element"/> is null.
        /// </exception>
        public static void SetMaximumLength(UIElement element, double value)
        {
            if (element == null)
            {
                throw new SmartArgumentNullException(() => element);
            }

            element.SetValue(MaximumLengthProperty, value);
        }

        /// <summary>
        /// Sets the MinimumLength dependency property on the specified element to the
        /// specified value.
        /// </summary>
        /// <param name="element">
        /// The element to set the MinimumLength dependency property on.
        /// </param>
        /// <param name="value">
        /// The value to set the MinimumLength dependency property to on the
        /// specified element.
        /// </param>
        /// <exception cref="RSG.Editor.SmartArgumentNullException">
        /// Thrown if the specified <paramref name="element"/> is null.
        /// </exception>
        public static void SetMinimumLength(UIElement element, double value)
        {
            if (element == null)
            {
                throw new SmartArgumentNullException(() => element);
            }

            element.SetValue(MinimumLengthProperty, value);
        }

        /// <summary>
        /// Sets the ShowResizePreview dependency property on the specified element to the
        /// specified value.
        /// </summary>
        /// <param name="element">
        /// The element to set the ShowResizePreview dependency property on.
        /// </param>
        /// <param name="value">
        /// The value to set the ShowResizePreview dependency property to on the
        /// specified element.
        /// </param>
        /// <exception cref="RSG.Editor.SmartArgumentNullException">
        /// Thrown if the specified <paramref name="element"/> is null.
        /// </exception>
        public static void SetShowResizePreview(UIElement element, bool value)
        {
            if (element == null)
            {
                throw new SmartArgumentNullException(() => element);
            }

            element.SetValue(ShowResizePreviewProperty, value);
        }

        /// <summary>
        /// Sets the SplitterLength dependency property on the specified element to the
        /// specified value.
        /// </summary>
        /// <param name="element">
        /// The element to set the SplitterLength dependency property on.
        /// </param>
        /// <param name="value">
        /// The value to set the SplitterLength dependency property to on the
        /// specified element.
        /// </param>
        /// <exception cref="RSG.Editor.SmartArgumentNullException">
        /// Thrown if the specified <paramref name="element"/> is null.
        /// </exception>
        public static void SetSplitterLength(UIElement element, SplitterLength value)
        {
            if (element == null)
            {
                throw new SmartArgumentNullException(() => element);
            }

            element.SetValue(SplitterLengthProperty, value);
        }

        /// <summary>
        /// Sets the ActualSplitterLength dependency property on the specified element to the
        /// specified value.
        /// </summary>
        /// <param name="element">
        /// The element to set the ActualSplitterLength dependency property on.
        /// </param>
        /// <param name="value">
        /// The value to set the ActualSplitterLength dependency property to on the
        /// specified element.
        /// </param>
        /// <exception cref="RSG.Editor.SmartArgumentNullException">
        /// Thrown if the specified <paramref name="element"/> is null.
        /// </exception>
        protected static void SetActualSplitterLength(UIElement element, double value)
        {
            if (element == null)
            {
                throw new SmartArgumentNullException(() => element);
            }

            element.SetValue(_actualSplitterLengthPropertyKey, value);
        }

        /// <summary>
        /// Sets the Index dependency property on the specified element to the
        /// specified value.
        /// </summary>
        /// <param name="element">
        /// The element to set the Index dependency property on.
        /// </param>
        /// <param name="value">
        /// The value to set the Index dependency property to on the specified element.
        /// </param>
        /// <exception cref="RSG.Editor.SmartArgumentNullException">
        /// Thrown if the specified <paramref name="element"/> is null.
        /// </exception>
        protected static void SetIndex(UIElement element, int value)
        {
            if (element == null)
            {
                throw new SmartArgumentNullException(() => element);
            }

            element.SetValue(_indexPropertyKey, value);
        }

        /// <summary>
        /// Sets the IsFirst dependency property on the specified element to the
        /// specified value.
        /// </summary>
        /// <param name="element">
        /// The element to set the IsFirst dependency property on.
        /// </param>
        /// <param name="value">
        /// The value to set the IsFirst dependency property to on the specified element.
        /// </param>
        /// <exception cref="RSG.Editor.SmartArgumentNullException">
        /// Thrown if the specified <paramref name="element"/> is null.
        /// </exception>
        protected static void SetIsFirst(UIElement element, bool value)
        {
            if (element == null)
            {
                throw new SmartArgumentNullException(() => element);
            }

            element.SetValue(_isFirstPropertyKey, value);
        }

        /// <summary>
        /// Sets the IsLast dependency property on the specified element to the
        /// specified value.
        /// </summary>
        /// <param name="element">
        /// The element to set the IsLast dependency property on.
        /// </param>
        /// <param name="value">
        /// The value to set the IsLast dependency property to on the specified element.
        /// </param>
        /// <exception cref="RSG.Editor.SmartArgumentNullException">
        /// Thrown if the specified <paramref name="element"/> is null.
        /// </exception>
        protected static void SetIsLast(UIElement element, bool value)
        {
            element.SetValue(_isLastPropertyKey, value);
        }

        /// <summary>
        /// Positions child elements and determines a size for this class.
        /// </summary>
        /// <param name="finalSize">
        /// The final area within the parent that this element should use to arrange itself
        /// and its children.
        /// </param>
        /// <returns>
        /// The actual size used.
        /// </returns>
        protected override Size ArrangeOverride(Size finalSize)
        {
            Rect finalRect = new Rect(0.0, 0.0, finalSize.Width, finalSize.Height);
            foreach (UIElement element in this.InternalChildren)
            {
                if (element == null)
                {
                    continue;
                }

                double actualSplitterLength = GetActualSplitterLength(element);
                if (this.Orientation == Orientation.Horizontal)
                {
                    finalRect.Width = actualSplitterLength;
                    element.Arrange(finalRect);
                    finalRect.X += actualSplitterLength;
                }
                else
                {
                    finalRect.Height = actualSplitterLength;
                    element.Arrange(finalRect);
                    finalRect.Y += actualSplitterLength;
                }
            }

            return finalSize;
        }

        /// <summary>
        /// Measures the size in layout required for child elements and determines a size for
        /// this class.
        /// </summary>
        /// <param name="availableSize">
        /// The available size that this element can give to child elements.
        /// </param>
        /// <returns>
        /// The size that this element determines it needs during layout, based on its
        /// calculations of child element sizes.
        /// </returns>
        protected override Size MeasureOverride(Size availableSize)
        {
            this.UpdateIndices();
            int childCount = this.InternalChildren.Count;
            IEnumerable<SplitterMeasureData> measureData =
                SplitterMeasureData.FromElements(this.InternalChildren);

            return this.Measure(availableSize, measureData, true);
        }

        /// <summary>
        /// Commits the resize produced by the displacement of the specified grip.
        /// </summary>
        /// <param name="grip">
        /// The grip that has been moved to cause the resizing.
        /// </param>
        /// <param name="amount">
        /// The amount in pixels the resize is.
        /// </param>
        private void CommitResize(RsSplitterGrip grip, double amount)
        {
            int gripIndex;
            int index1 = 0;
            int index2 = 0;
            if (!this.GetResizeIndices(grip, out gripIndex, out index1, out index2))
            {
                return;
            }

            this.ResizeChildren(index1, index2, amount);
        }

        /// <summary>
        /// Gets the indices that make up a resize operation when the specified grip is the
        /// one that is doing the resizing.
        /// </summary>
        /// <param name="grip">
        /// The grip that is currently being dragged to cause the resize.
        /// </param>
        /// <param name="gripIndex">
        /// When this method returns contains the index to the grip that is being dragged.
        /// </param>
        /// <param name="index1">
        /// When this method returns contains the index to the first item that it getting
        /// resized by the grip.
        /// </param>
        /// <param name="index2">
        /// When this method returns contains the index to the second item that it getting
        /// resized by the grip.
        /// </param>
        /// <returns>
        /// True if there is a valid resizing operation; otherwise false.
        /// </returns>
        /// <exception cref="System.InvalidOperationException">
        /// Thrown if the specified grips <see cref="RsSplitterGrip.ResizeBehaviour"/>
        /// parameter is set to BasedOnAlignment.
        /// </exception>
        private bool GetResizeIndices(
            RsSplitterGrip grip, out int gripIndex, out int index1, out int index2)
        {
            for (int i = 0; i < this.InternalChildren.Count; i++)
            {
                UIElement element = this.InternalChildren[i];
                if (!element.IsAncestorOf(grip))
                {
                    continue;
                }

                gripIndex = i;
                switch (grip.ResizeBehaviour)
                {
                    case GridResizeBehavior.CurrentAndNext:
                        index1 = i;
                        index2 = i + 1;
                        break;
                    case GridResizeBehavior.PreviousAndCurrent:
                        index1 = i - 1;
                        index2 = i;
                        break;
                    case GridResizeBehavior.PreviousAndNext:
                        index1 = i - 1;
                        index2 = i + 1;
                        break;
                    default:
                        throw new InvalidOperationException(
                            "BasedOnAlignment is not a valid resize behaviour");
                }

                if (index1 < 0 || index2 < 0)
                {
                    return false;
                }

                int childCount = this.InternalChildren.Count;
                if (index1 >= childCount || index2 >= childCount)
                {
                    return false;
                }

                return true;
            }

            gripIndex = -1;
            index1 = -1;
            index2 = -1;
            return false;
        }

        /// <summary>
        /// Determines whether the specified double value is a Real value or not.
        /// </summary>
        /// <param name="value">
        /// The double value to test.
        /// </param>
        /// <returns>
        /// True if the specified value is non real; otherwise, false.
        /// </returns>
        private bool IsNonreal(double value)
        {
            return double.IsNaN(value) || double.IsInfinity(value);
        }

        /// <summary>
        /// Measures the size in layout required for child elements and determines a size for
        /// this class.
        /// </summary>
        /// <param name="size">
        /// The available size that this element can give to child elements.
        /// </param>
        /// <param name="data">
        /// A list of the measure data for the child elements.
        /// </param>
        /// <param name="remeasure">
        /// A value indicating whether the child elements need to be re-measured or not.
        /// </param>
        /// <returns>
        /// The size that this element determines it needs during layout, based on its
        /// calculations of child element sizes.
        /// </returns>
        private Size Measure(Size size, IEnumerable<SplitterMeasureData> data, bool remeasure)
        {
            if (this.Orientation == Orientation.Horizontal && this.IsNonreal(size.Width))
            {
                return this.MeasureNonreal(size, data, remeasure);
            }
            else if (this.Orientation == Orientation.Vertical && this.IsNonreal(size.Height))
            {
                return this.MeasureNonreal(size, data, remeasure);
            }

            double totalStretch = 0.0;
            double totalFill = 0.0;
            double fillMinLength = 0.0;
            double stretchMinLength = 0.0;
            foreach (SplitterMeasureData current in data)
            {
                SplitterLength attachedLength = current.AttachedLength;
                double minimumLength = GetMinimumLength(current.Element);
                if (attachedLength.IsStretch)
                {
                    totalStretch += attachedLength.Value;
                    stretchMinLength += minimumLength;
                }
                else
                {
                    totalFill += attachedLength.Value;
                    fillMinLength += minimumLength;
                }

                current.IsMinimumReached = false;
                current.IsMaximumReached = false;
            }

            double totalMinLength = stretchMinLength + fillMinLength;
            double width = size.Width;
            double height = size.Height;
            bool horizontal = this.Orientation == Orientation.Horizontal;
            double availableLength = horizontal ? width : height;

            double reservedFill = 0.0;
            if (totalFill != 0.0)
            {
                reservedFill = Math.Max(0.0, availableLength - totalStretch);
            }

            double reservedStretch = availableLength;
            if (reservedFill != 0.0)
            {
                reservedStretch = totalStretch;
            }

            if (totalMinLength <= availableLength)
            {
                this.ResolveMeasureForSmallLength(
                    data,
                    ref totalStretch,
                    ref reservedStretch,
                    ref totalFill,
                    ref reservedFill,
                    availableLength,
                    fillMinLength);
            }

            Size availableSize = new Size(width, height);
            Rect rect = new Rect(0.0, 0.0, width, height);
            foreach (SplitterMeasureData current in data)
            {
                SplitterLength attachedLength = current.AttachedLength;
                double newSize = 0.0;
                if (!current.IsMinimumReached)
                {
                    if (attachedLength.IsFill)
                    {
                        if (totalFill != 0.0)
                        {
                            newSize = (attachedLength.Value / totalFill) * reservedFill;
                        }
                    }
                    else
                    {
                        if (totalStretch != 0.0)
                        {
                            newSize = (attachedLength.Value / totalStretch) * reservedStretch;
                        }
                    }
                }
                else
                {
                    newSize = GetMinimumLength(current.Element);
                }

                if (remeasure)
                {
                    SetActualSplitterLength(current.Element, newSize);
                }

                if (this.Orientation == Orientation.Horizontal)
                {
                    availableSize.Width = newSize;
                    current.MeasuredBounds =
                        new Rect(rect.Left, rect.Top, newSize, rect.Height);
                    rect.X += newSize;
                    if (remeasure)
                    {
                        current.Element.Measure(availableSize);
                    }
                }
                else
                {
                    availableSize.Height = newSize;
                    current.MeasuredBounds =
                        new Rect(rect.Left, rect.Top, rect.Width, newSize);
                    rect.Y += newSize;
                    if (remeasure)
                    {
                        current.Element.Measure(availableSize);
                    }
                }
            }

            return new Size(width, height);
        }

        /// <summary>
        /// Measures the size in layout required for child elements and determines a size for
        /// this class.
        /// </summary>
        /// <param name="size">
        /// The available size that this element can give to child elements.
        /// </param>
        /// <param name="data">
        /// A list of the measure data for the child elements.
        /// </param>
        /// <param name="remeasure">
        /// A value indicating whether the child elements need to be re-measured or not.
        /// </param>
        /// <returns>
        /// The size that this element determines it needs during layout, based on its
        /// calculations of child element sizes.
        /// </returns>
        private Size MeasureNonreal(
            Size size, IEnumerable<SplitterMeasureData> data, bool remeasure)
        {
            double width = 0.0;
            double height = 0.0;
            foreach (SplitterMeasureData current in data)
            {
                if (remeasure)
                {
                    current.Element.Measure(size);
                }

                if (this.Orientation == Orientation.Horizontal)
                {
                    width += current.Element.DesiredSize.Width;
                    height = Math.Max(height, current.Element.DesiredSize.Height);
                }
                else
                {
                    width = Math.Max(width, current.Element.DesiredSize.Width);
                    height += current.Element.DesiredSize.Height;
                }
            }

            Rect measuredBounds = new Rect(0.0, 0.0, width, height);
            foreach (SplitterMeasureData current in data)
            {
                if (this.Orientation == Orientation.Horizontal)
                {
                    measuredBounds.Width = current.Element.DesiredSize.Width;
                    current.MeasuredBounds = measuredBounds;
                    measuredBounds.X += measuredBounds.Width;
                }
                else
                {
                    measuredBounds.Height = current.Element.DesiredSize.Height;
                    current.MeasuredBounds = measuredBounds;
                    measuredBounds.Y += measuredBounds.Height;
                }
            }

            return new Size(width, height);
        }

        /// <summary>
        /// Gets called whenever a splitter grip within the panel fires a drag completed event.
        /// </summary>
        /// <param name="sender">
        /// The grip that was dragged to produce this event.
        /// </param>
        /// <param name="args">
        /// The System.Windows.Controls.Primitives.DragCompletedEventArgs data for the event.
        /// </param>
        private void OnSplitterDragCompleted(object sender, DragCompletedEventArgs args)
        {
            RsSplitterGrip splitterGrip = sender as RsSplitterGrip;
            if (splitterGrip == null)
            {
                return;
            }

            args.Handled = true;
            if (this.IsShowingResizePreview)
            {
                this._currentPreviewWindow.Hide();
                this._currentPreviewWindow = null;
                if (!args.Canceled)
                {
                    Point point = new Point(args.HorizontalChange, args.VerticalChange);
                    point = point.DeviceToLogicalUnits();
                    bool horizontal = this.Orientation == Orientation.Horizontal;
                    double amount = horizontal ? point.X : point.Y;

                    this.CommitResize(splitterGrip, amount);
                }
            }

            splitterGrip.DragDelta -= this.OnSplitterResized;
            splitterGrip.DragCompleted -= this.OnSplitterDragCompleted;
        }

        /// <summary>
        /// Gets called whenever a splitter grip within the panel fires a drag started event.
        /// </summary>
        /// <param name="sender">
        /// The grip that was dragged to produce this event.
        /// </param>
        /// <param name="args">
        /// The System.Windows.Controls.Primitives.DragStartedEventArgs data for the event.
        /// </param>
        private void OnSplitterDragStarted(object sender, DragStartedEventArgs args)
        {
            RsSplitterGrip splitterGrip = args.OriginalSource as RsSplitterGrip;
            if (splitterGrip == null)
            {
                return;
            }

            args.Handled = true;
            splitterGrip.DragDelta += this.OnSplitterResized;
            splitterGrip.DragCompleted += this.OnSplitterDragCompleted;
            if (GetShowResizePreview(this))
            {
                this._currentPreviewWindow = new RsSplitterResizePreviewWindow();
                this._currentPreviewWindow.Show(splitterGrip);
            }
        }

        /// <summary>
        /// Gets called whenever a splitter grip within the panel fires a drag delta event.
        /// </summary>
        /// <param name="sender">
        /// The grip that was dragged to produce this event.
        /// </param>
        /// <param name="args">
        /// The System.Windows.Controls.Primitives.DragDeltaEventArgs data for the event.
        /// </param>
        private void OnSplitterResized(object sender, DragDeltaEventArgs args)
        {
            RsSplitterGrip splitterGrip = sender as RsSplitterGrip;
            if (splitterGrip == null)
            {
                return;
            }

            args.Handled = true;
            bool horizontal = this.Orientation == Orientation.Horizontal;
            double amount = horizontal ? args.HorizontalChange : args.VerticalChange;
            if (!this.IsShowingResizePreview)
            {
                this.CommitResize(splitterGrip, amount);
                return;
            }

            this.UpdatePreview(splitterGrip, amount);
        }

        /// <summary>
        /// Resizes the children at the specified index by the specified number of pixels.
        /// </summary>
        /// <param name="index1">
        /// The first child index to resize.
        /// </param>
        /// <param name="index2">
        /// The second child index to resize.
        /// </param>
        /// <param name="amount">
        /// The amount of pixels to resize the children by.
        /// </param>
        private void ResizeChildren(int index1, int index2, double amount)
        {
            UIElement element1 = this.InternalChildren[index1];
            if (element1 == null)
            {
                return;
            }

            UIElement element2 = this.InternalChildren[index2];
            if (element2 == null)
            {
                return;
            }

            SplitterMeasureData data1 = new SplitterMeasureData(element1);
            SplitterMeasureData data2 = new SplitterMeasureData(element2);
            if (!this.ResizeChildrenCore(data1, data2, amount))
            {
                return;
            }

            SetSplitterLength(element1, data1.AttachedLength);
            SetSplitterLength(element2, data2.AttachedLength);
            this.InvalidateMeasure();
        }

        /// <summary>
        /// Resizes the specified measure data by the specified number of pixels.
        /// </summary>
        /// <param name="data1">
        /// The first measure data to resize.
        /// </param>
        /// <param name="data2">
        /// The second measure data to resize.
        /// </param>
        /// <param name="amount">
        /// The amount of pixels to resize the data by.
        /// </param>
        /// <returns>
        /// True if there was a update to the measure data; otherwise, false.
        /// </returns>
        private bool ResizeChildrenCore(
            SplitterMeasureData data1, SplitterMeasureData data2, double amount)
        {
            UIElement element1 = data1.Element;
            UIElement element2 = data2.Element;
            SplitterLength length1 = data1.AttachedLength;
            SplitterLength length2 = data2.AttachedLength;
            double actualLength1 = GetActualSplitterLength(element1);
            double actualLength2 = GetActualSplitterLength(element2);
            double minimumLength1 = GetMinimumLength(element1);
            double minimumLength2 = GetMinimumLength(element2);

            double newActualLength1 = Math.Max(
                0.0, Math.Min(actualLength1 + actualLength2, actualLength1 + amount));
            double newActualLength2 = Math.Max(
                0.0, Math.Min(actualLength1 + actualLength2, actualLength2 - amount));

            if (minimumLength1 + minimumLength2 > newActualLength1 + newActualLength2)
            {
                return false;
            }

            if (newActualLength1 < minimumLength1)
            {
                newActualLength2 -= minimumLength1 - newActualLength1;
                newActualLength1 = minimumLength1;
            }

            if (newActualLength2 < minimumLength2)
            {
                newActualLength1 -= minimumLength2 - newActualLength2;
                newActualLength2 = minimumLength2;
            }

            SplitterUnitType unitType = length1.SplitterUnitType;
            bool sameUnit = true;
            if (length1.SplitterUnitType != length2.SplitterUnitType)
            {
                unitType = SplitterUnitType.Stretch;
                sameUnit = false;
            }

            if (sameUnit)
            {
                double actualLength = newActualLength1 + newActualLength2;
                double unitLength = length1.Value + length2.Value;
                double lengthRatio = 1.0 / (actualLength / unitLength);
                double newLength1 = newActualLength1 * lengthRatio;
                double newLength2 = newActualLength2 * lengthRatio;

                data1.AttachedLength = new SplitterLength(newLength1, unitType);
                data2.AttachedLength = new SplitterLength(newLength2, unitType);
            }
            else
            {
                if (length1.IsFill)
                {
                    data2.AttachedLength = new SplitterLength(newActualLength2, unitType);
                }
                else
                {
                    data1.AttachedLength = new SplitterLength(newActualLength1, unitType);
                }
            }

            return true;
        }

        /// <summary>
        /// Resolves the specified measurements for when the total available length for the
        /// control isn't enough room to show the items at their own minimum pixel size.
        /// </summary>
        /// <param name="data">
        /// A list of the measure data for the child elements.
        /// </param>
        /// <param name="totalStretch">
        /// The amount of pixels that have been specified on the child items with stretched
        /// splitter lengths.
        /// </param>
        /// <param name="reservedStretch">
        /// The amount of pixels that have been reserved for the items with stretched
        /// splitter lengths.
        /// </param>
        /// <param name="totalFill">
        /// The amount of portions that have been specified on the child items with filled
        /// splitter lengths.
        /// </param>
        /// <param name="reservedFill">
        /// The amount of pixels that have been reserved for the items with filled
        /// splitter lengths.
        /// </param>
        /// <param name="availableLength">
        /// The total length available to both stretched and filled child items.
        /// </param>
        /// <param name="fillMinLength">
        /// The minimum number of pixels the filled child items have been specified to take up.
        /// </param>
        private void ResolveMeasureForSmallLength(
            IEnumerable<SplitterMeasureData> data,
            ref double totalStretch,
            ref double reservedStretch,
            ref double totalFill,
            ref double reservedFill,
            double availableLength,
            double fillMinLength)
        {
            foreach (SplitterMeasureData current in data)
            {
                SplitterLength attachedLength = current.AttachedLength;
                double maximumLength = GetMaximumLength(current.Element);
                if (attachedLength.IsFill)
                {
                    continue;
                }

                double num11 = 0.0;
                if (totalStretch != 0.0)
                {
                    num11 = attachedLength.Value / totalStretch * reservedStretch;
                }

                if (num11 <= maximumLength)
                {
                    continue;
                }

                current.IsMaximumReached = true;
                if (totalStretch == attachedLength.Value)
                {
                    totalStretch = maximumLength;
                    current.AttachedLength = new SplitterLength(maximumLength);
                }
                else
                {
                    totalStretch -= attachedLength.Value;
                    current.AttachedLength = new SplitterLength(totalStretch);
                    totalStretch += totalStretch;
                }

                reservedFill = 0.0;
                if (totalFill != 0.0)
                {
                    reservedFill = Math.Max(0.0, availableLength - totalStretch);
                }

                reservedStretch = availableLength;
                if (reservedFill != 0.0)
                {
                    reservedStretch = totalStretch;
                }
            }

            if (reservedFill < fillMinLength)
            {
                reservedFill = fillMinLength;
                reservedStretch = availableLength - reservedFill;
            }

            foreach (SplitterMeasureData current in data)
            {
                SplitterLength attachedLength = current.AttachedLength;
                double minimumLength = GetMinimumLength(current.Element);
                if (attachedLength.IsFill)
                {
                    double num11 = 0.0;
                    if (totalFill != 0.0)
                    {
                        num11 = attachedLength.Value / totalFill * reservedFill;
                    }

                    if (num11 < minimumLength)
                    {
                        current.IsMinimumReached = true;
                        reservedFill -= minimumLength;
                        totalFill -= attachedLength.Value;
                    }
                }
                else
                {
                    double num11 = 0.0;
                    if (totalStretch != 0.0)
                    {
                        num11 = attachedLength.Value / totalStretch * reservedStretch;
                    }

                    if (num11 < minimumLength)
                    {
                        current.IsMinimumReached = true;
                        reservedStretch -= minimumLength;
                        totalStretch -= attachedLength.Value;
                    }
                }
            }
        }

        /// <summary>
        /// Updates the Index, IsFirst, IsLast attached property on each of the child
        /// items in this panel.
        /// </summary>
        private void UpdateIndices()
        {
            int count = this.InternalChildren.Count;
            int lastIndex = this.InternalChildren.Count - 1;
            int firstIndex = 0;
            for (int i = 0; i < count; i++)
            {
                UIElement element = this.InternalChildren[i];
                if (element == null)
                {
                    continue;
                }

                SetIndex(element, i);
                SetIsFirst(element, i == firstIndex);
                SetIsLast(element, i == lastIndex);
            }
        }

        /// <summary>
        /// Updates the position of the current drag preview window.
        /// </summary>
        /// <param name="grip">
        /// The grip that was moved to produce the resizing.
        /// </param>
        /// <param name="amount">
        /// The amount in pixels the resize is.
        /// </param>
        private void UpdatePreview(RsSplitterGrip grip, double amount)
        {
            int gripIndex = 0;
            int index1 = 0;
            int index2 = 0;
            if (!this.GetResizeIndices(grip, out gripIndex, out index1, out index2))
            {
                return;
            }

            IList<SplitterMeasureData> data =
                SplitterMeasureData.FromElements(this.InternalChildren);

            this.ResizeChildrenCore(data[index1], data[index2], amount);
            this.Measure(this.RenderSize, data, false);

            Point point = grip.TransformToAncestor(this).Transform(new Point(0.0, 0.0));
            if (this.Orientation == Orientation.Horizontal)
            {
                double gripWidth = this.InternalChildren[gripIndex].RenderSize.Width;
                point.X += data[gripIndex].MeasuredBounds.Width - gripWidth;
            }
            else
            {
                double gripHeight = this.InternalChildren[gripIndex].RenderSize.Height;
                point.Y += data[gripIndex].MeasuredBounds.Height - gripHeight;
            }

            point = this.PointToScreen(point);
            this._currentPreviewWindow.Move((double)((int)point.X), (double)((int)point.Y));
        }
        #endregion Methods
    } // RSG.Editor.Controls.Dock.RsSplitterPanel {Class}
} // RSG.Editor.Controls.Dock {Namespace}
