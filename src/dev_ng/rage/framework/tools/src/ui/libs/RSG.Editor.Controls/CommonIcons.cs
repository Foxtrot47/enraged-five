﻿//---------------------------------------------------------------------------------------------
// <copyright file="CommonIcons.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Globalization;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Provides static properties that give access to common icons that can be used throughout
    /// a application.
    /// </summary>
    public static class CommonIcons
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Save"/> property.
        /// </summary>
        private static BitmapSource _save;

        /// <summary>
        /// A generic object that provides thread safety access to the image sources.
        /// </summary>
        private static object _syncRoot = new object();
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets the icon that shows a save to disk icon.
        /// </summary>
        public static BitmapSource Save
        {
            get
            {
                if (_save == null)
                {
                    lock (_syncRoot)
                    {
                        if (_save == null)
                        {
                            EnsureLoaded(ref _save, "save16");
                        }
                    }
                }

                return _save;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Ensures that the specified image source is loaded with the specified resource name.
        /// </summary>
        /// <param name="source">
        /// The image source that should be loaded.
        /// </param>
        /// <param name="resourceName">
        /// The name of the resource the loaded image source should set as its source.
        /// </param>
        private static void EnsureLoaded(ref BitmapSource source, string resourceName)
        {
            if (source != null)
            {
                return;
            }

            string assemblyName = typeof(CommonIcons).Assembly.GetName().Name;
            string path = "Resources/" + resourceName + ".png";
            string bitmapPath = "pack://application:,,,/{0};component/{1}";
            bitmapPath = String.Format(
                CultureInfo.InvariantCulture, bitmapPath, assemblyName, path);
            Uri uri = new Uri(bitmapPath, UriKind.Absolute);

            source = new BitmapImage(uri);
        }
        #endregion Methods
    } // RSG.Editor.Controls.CommonIcons {Class}
} // RSG.Editor.Controls.Commands {Namespace}
