﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsDataGridIntegerSpinnerColumn.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;

    /// <summary>
    /// A column that displays a integer spinner control.
    /// </summary>
    public class RsDataGridIntegerSpinnerColumn : DataGridBoundColumn
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="EditableStyleKey"/> property.
        /// </summary>
        private static ResourceKey _editableStyleKey = CreateStyleKey("EditableStyleKey");

        /// <summary>
        /// The private field used for the <see cref="ReadOnlyStyleKey"/> property.
        /// </summary>
        private static ResourceKey _readOnlyStyleKey = CreateStyleKey("ReadOnlyStyleKey");
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsDataGridIntegerSpinnerColumn"/>
        /// class.
        /// </summary>
        static RsDataGridIntegerSpinnerColumn()
        {
            DataGridBoundColumn.IsReadOnlyProperty.OverrideMetadata(
                typeof(RsDataGridIntegerSpinnerColumn),
                new FrameworkPropertyMetadata(OnReadOnlyPropertyChanged));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsDataGridIntegerSpinnerColumn"/>
        /// class.
        /// </summary>
        public RsDataGridIntegerSpinnerColumn()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the resource key used to reference the style used on editable spinner
        /// controls.
        /// </summary>
        public static ResourceKey EditableStyleKey
        {
            get { return _editableStyleKey; }
        }

        /// <summary>
        /// Gets the resource key used to reference the style used on read only spinner
        /// controls.
        /// </summary>
        public static ResourceKey ReadOnlyStyleKey
        {
            get { return _readOnlyStyleKey; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates the visual tree that will become the content of the specified cell.
        /// </summary>
        /// <param name="cell">
        /// The cell the visual tree is being created for.
        /// </param>
        /// <param name="dataItem">
        /// The data item currently bound to the specified cell.
        /// </param>
        /// <returns>
        /// The root of the visual tree that will become the content of the specified cell.
        /// </returns>
        protected override FrameworkElement GenerateEditingElement(
            DataGridCell cell, object dataItem)
        {
            return this.GenerateIntegerSpinner(cell);
        }

        /// <summary>
        /// Creates the visual tree that will become the content of the specified cell.
        /// </summary>
        /// <param name="cell">
        /// The cell the visual tree is being created for.
        /// </param>
        /// <param name="dataItem">
        /// The data item currently bound to the specified cell.
        /// </param>
        /// <returns>
        /// The root of the visual tree that will become the content of the specified cell.
        /// </returns>
        protected override FrameworkElement GenerateElement(DataGridCell cell, object dataItem)
        {
            if (!this.IsReadOnly)
            {
                return this.GenerateEditingElement(cell, dataItem);
            }

            TextBlock textBlock = new TextBlock();
            this.ApplyProperties(textBlock);
            textBlock.SetResourceReference(FrameworkElement.StyleProperty, ReadOnlyStyleKey);

            BindingBase binding = this.Binding;
            if (binding != null)
            {
                BindingOperations.SetBinding(textBlock, TextBlock.TextProperty, binding);
            }
            else
            {
                BindingOperations.ClearBinding(textBlock, TextBlock.TextProperty);
            }

            return textBlock;
        }

        /// <summary>
        /// Creates a new resource key that references a style with the specified name.
        /// </summary>
        /// <param name="styleName">
        /// The name that the resource key will be referencing.
        /// </param>
        /// <returns>
        /// A new resource key that references a style with the specified name.
        /// </returns>
        private static ResourceKey CreateStyleKey(string styleName)
        {
            return new StyleKey<RsDataGridIntegerSpinnerColumn>(styleName);
        }

        /// <summary>
        /// Called whenever the IsReadOnly dependency property changes.
        /// </summary>
        /// <param name="d">
        /// The object whose IsReadOnly dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnReadOnlyPropertyChanged(
            DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RsDataGridIntegerSpinnerColumn column = d as RsDataGridIntegerSpinnerColumn;
            if (column == null)
            {
                Debug.Assert(false, "Incorrect dependency object cast");
                return;
            }

            // This will make sure the visual tree for all of the cells are rebuilt.
            column.NotifyPropertyChanged("ElementStyle");
            column.NotifyPropertyChanged("EditingElementStyle");
        }

        /// <summary>
        /// Applies all of the properties on the specified element by syncing the property
        /// value from the column.
        /// </summary>
        /// <param name="element">
        /// The element the properties are being set on.
        /// </param>
        private void ApplyProperties(FrameworkElement element)
        {
            this.ApplyProperty(
                element,
                TextElement.FontFamilyProperty,
                DataGridTextColumn.FontFamilyProperty);

            this.ApplyProperty(
                element, TextElement.FontSizeProperty, DataGridTextColumn.FontSizeProperty);

            this.ApplyProperty(
                element, TextElement.FontStyleProperty, DataGridTextColumn.FontStyleProperty);

            this.ApplyProperty(
                element,
                TextElement.FontWeightProperty,
                DataGridTextColumn.FontWeightProperty);

            this.ApplyProperty(
                element,
                TextElement.ForegroundProperty,
                DataGridTextColumn.ForegroundProperty);
        }

        /// <summary>
        /// Applies the specified column property onto the specified combo box.
        /// </summary>
        /// <param name="element">
        /// The combo box to set the values on.
        /// </param>
        /// <param name="spinnerProperty">
        /// The property on the combo box to set.
        /// </param>
        /// <param name="columnProperty">
        /// The property on the column box to take.
        /// </param>
        private void ApplyProperty(
            FrameworkElement element,
            DependencyProperty spinnerProperty,
            DependencyProperty columnProperty)
        {
            ValueSource source = DependencyPropertyHelper.GetValueSource(this, columnProperty);
            if (source.BaseValueSource == BaseValueSource.Default)
            {
                element.ClearValue(spinnerProperty);
                return;
            }

            element.SetValue(spinnerProperty, this.GetValue(columnProperty));
        }

        /// <summary>
        /// Creates the integer spinner control that will become the content of the specified
        /// cell.
        /// </summary>
        /// <param name="cell">
        /// The data grid cell whose integer spinner is being created.
        /// </param>
        /// <returns>
        /// The integer spinner control that should be the root of the specified cells visual
        /// tree.
        /// </returns>
        private RsIntegerSpinner GenerateIntegerSpinner(DataGridCell cell)
        {
            RsIntegerSpinner spinner =
                (cell != null) ? (cell.Content as RsIntegerSpinner) : null;
            if (spinner == null)
            {
                spinner = new RsIntegerSpinner();
                spinner.HorizontalAlignment = HorizontalAlignment.Stretch;
                spinner.VerticalAlignment = VerticalAlignment.Top;
                this.ApplyProperties(spinner);
            }

            BindingBase binding = this.Binding;
            if (binding != null)
            {
                DependencyProperty property = RsIntegerSpinner.ValueProperty;
                BindingOperations.SetBinding(spinner, property, binding);
            }
            else
            {
                DependencyProperty property = RsIntegerSpinner.ValueProperty;
                BindingOperations.ClearBinding(spinner, property);
            }

            DependencyProperty styleProperty = FrameworkElement.StyleProperty;
            spinner.SetResourceReference(styleProperty, EditableStyleKey);
            spinner.IsHitTestVisible = true;
            spinner.Focusable = true;
            if (cell != null)
            {
                cell.IsTabStop = false;
            }

            return spinner;
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsDataGridIntegerSpinnerColumn {Class}
} // RSG.Editor.Controls {Namespace}
