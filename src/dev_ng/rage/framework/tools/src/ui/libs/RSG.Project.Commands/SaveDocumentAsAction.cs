﻿//---------------------------------------------------------------------------------------------
// <copyright file="SaveDocumentAsAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Commands
{
    using System.Diagnostics;
    using RSG.Editor;
    using RSG.Editor.SharedCommands;

    /// <summary>
    /// Contains the logic for the <see cref="RockstarCommands.SaveAs"/> routed command. This
    /// class cannot be inherited.
    /// </summary>
    public sealed class SaveDocumentAsAction : ButtonAction<ProjectCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SaveDocumentAsAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public SaveDocumentAsAction(ProjectCommandResolver resolver)
            : base(new ParameterResolverDelegate<ProjectCommandArgs>(resolver))
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ProjectCommandArgs args)
        {
            string fallbackText = "Save Selected Items As...";
            if (args == null || args.ViewSite == null || args.ViewSite.ActiveDocument == null)
            {
                RockstarCommandManager.UpdateItemText(RockstarCommands.SaveAs, fallbackText);
                return false;
            }

            string name = args.ViewSite.ActiveDocument.FriendlySavePath;
            string updatedText = "Save " + name + " As...";
            RockstarCommandManager.UpdateItemText(RockstarCommands.SaveAs, updatedText);
            return true;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(ProjectCommandArgs args)
        {
            IDocumentManagerService docService = this.GetService<IDocumentManagerService>();
            if (docService == null)
            {
                Debug.Fail(
                    "Unable to save document as due to the fact the service is missing.");
                return;
            }

            docService.SaveActiveDocument(true);
        }
        #endregion Methods
    } // RSG.Project.Commands.SaveDocumentAsAction {Class}
} // RSG.Project.Commands {Namespace}
