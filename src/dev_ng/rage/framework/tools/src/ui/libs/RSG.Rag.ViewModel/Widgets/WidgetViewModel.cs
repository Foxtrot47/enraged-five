﻿using RSG.Editor;
using RSG.Rag.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Diagnostics;

namespace RSG.Rag.ViewModel.Widgets
{
    /// <summary>
    /// Base view model class for all widgets.
    /// </summary>
    public abstract class WidgetViewModel : NotifyPropertyChangedBase, IWidgetViewModel
    {
        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private IWidget _widget;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="WidgetViewModel"/> class.
        /// </summary>
        /// <param name="widget">Widget that this viewmodel is for.</param>
        protected WidgetViewModel(IWidget widget)
        {
            Debug.Assert(widget != null, "Widget can't be null.");
            _widget = widget;
            _widget.PropertyChanged += Widget_PropertyChanged;
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Widget that this viewmodel wraps.
        /// </summary>
        protected IWidget Widget
        {
            get { return _widget; }
        }

        /// <summary>
        /// 
        /// </summary>
        public WidgetGuid Guid
        {
            get { return Widget.Guid; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Name
        {
            get { return Widget.Name ?? ""; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Path
        {
            get { return Widget.Path; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Tooltip
        {
            get { return Widget.Tooltip; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Color FillColor
        {
            get { return Widget.FillColor; }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool ReadOnly
        {
            get { return Widget.ReadOnly; }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsEnabled
        {
            get { return !Widget.ReadOnly; }
        }

        /// <summary>
        /// Flag indicating whether this item has been modified from it's default value.
        /// </summary>
        public virtual bool IsModified
        {
            get { return false; }
        }

        /// <summary>
        /// The default value for this widget.
        /// </summary>
        public virtual String DefaultValue
        {
            get { return ""; }
        }
        #endregion // Properties

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Widget_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnWidgetPropertyChanged(e.PropertyName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        protected virtual void OnWidgetPropertyChanged(String propertyName)
        {
            if (propertyName == "ReadOnly")
            {
                NotifyPropertyChanged("IsEnabled");
            }
            NotifyPropertyChanged(propertyName);
        }
        #endregion // Event Handlers

        #region IComparable<T> Methods
        /// <summary>
        /// Compares the current object with another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// A value that indicates the relative order of the objects being compared.
        /// The return value has the following meanings: Value Meaning Less than zero
        /// This object is less than the other parameter.Zero This object is equal to
        /// other. Greater than zero This object is greater than other.</returns>
        public int CompareTo(IWidgetViewModel other)
        {
            if (other == null)
            {
                return 1;
            }

            return Name.CompareTo(other.Name);
        }
        #endregion // IComparable<T> Methods
    } // WidgetViewModel

    /// <summary>
    /// Generic base class for widget view models.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class WidgetViewModel<T> : WidgetViewModel where T : IWidget
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="WidgetViewModel{T}"/> class.
        /// </summary>
        /// <param name="widget">Widget that this viewmodel is for.</param>
        protected WidgetViewModel(T widget)
            : base(widget)
        {
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Typed widget that this viewmodel wraps.
        /// </summary>
        protected new T Widget
        {
            get { return (T)base.Widget; }
        }
        #endregion // Properties
    } // WidgetViewModel<T>
}
