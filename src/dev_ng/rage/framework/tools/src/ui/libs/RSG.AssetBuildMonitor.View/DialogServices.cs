﻿//---------------------------------------------------------------------------------------------
// <copyright file="DialogServices.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.AssetBuildMonitor.View
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Windows;
    using RSG.AssetBuildMonitor.ViewModel;

    /// <summary>
    /// Provides functionality to show dialogs specific to the Asset Build Monitor application.
    /// </summary>
    public sealed class DialogServices : IDialogServices
    {
        /// <summary>
        /// Show the Add New Host dialog box; returning user-specified hostname.
        /// </summary>
        /// <param name="hostname"></param>
        /// <returns></returns>
        public bool ShowAddNewHostDialog(out String hostname)
        {
            ConnectToNewHostDialog dialog = new ConnectToNewHostDialog();
            dialog.Owner = Application.Current.MainWindow;
            Nullable<bool> dialogResult = dialog.ShowDialog();

            if (dialogResult.HasValue && dialogResult.Value)
            {
                hostname = dialog.HostnameText.Text.Trim().ToUpper();
                return (true);
            }
            hostname = String.Empty;
            return (false);
        }

        /// <summary>
        /// Show the Files List dialog box.
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <param name="filenames"></param>
        /// <returns></returns>
        public bool ShowFileListDialog(String title, String message, IEnumerable<String> filenames)
        {
            FileListDialog dialog = new FileListDialog(title, message, filenames);
            dialog.Owner = Application.Current.MainWindow;
            dialog.ShowDialog();

            return (true);
        }
    }

} // RSG.AssetBuildMonitor.View namespace
