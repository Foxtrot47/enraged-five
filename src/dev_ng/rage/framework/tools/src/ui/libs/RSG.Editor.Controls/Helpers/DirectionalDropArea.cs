﻿//---------------------------------------------------------------------------------------------
// <copyright file="DirectionalDropArea.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Helpers
{
    /// <summary>
    /// Defines the different drop area locations a single control can have.
    /// </summary>
    public enum DirectionalDropArea
    {
        /// <summary>
        /// Specifies that the drop area is undefined.
        /// </summary>
        None,

        /// <summary>
        /// Specifies that the dropped item is dropped on top of the area.
        /// </summary>
        On,

        /// <summary>
        /// Specifies that the dropped item is dropped above the area.
        /// </summary>
        Above,

        /// <summary>
        /// Specifies that the dropped item is dropped below the area.
        /// </summary>
        Below
    } // RSG.Editor.Controls.Presenters.DirectionalDropArea {Enum}
} // RSG.Editor.Controls.Presenters {Namespace}
