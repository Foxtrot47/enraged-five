﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsLoginInternalWindow.xaml.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using RSG.Base.Extensions;
    using RSG.Editor.Controls.Resources;

    /// <summary>
    /// A window that displays a username and password text box that can be used to allow the
    /// user to login to a system.
    /// </summary>
    internal partial class RsLoginInternalWindow : RsWindow
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="AttemptCommand"/> property.
        /// </summary>
        private static RoutedCommand _attemptCommand;

        /// <summary>
        /// The number of attempts the user has had to login.
        /// </summary>
        private int _attempts;

        /// <summary>
        /// The delegate that is fired when the user attempts to login.
        /// </summary>
        private LoginAttemptDelegate _delegate;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsLoginInternalWindow"/> class.
        /// </summary>
        static RsLoginInternalWindow()
        {
            _attemptCommand =
                new RoutedCommand("AttemptCommand", typeof(RsLoginInternalWindow));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsLoginInternalWindow"/> class.
        /// </summary>
        public RsLoginInternalWindow()
        {
            this.InitializeComponent();
            KeyStates state = Keyboard.GetKeyStates(Key.CapsLock);
            if ((state & KeyStates.Toggled) == KeyStates.Toggled)
            {
                this.CapsLockMessage.Visibility = Visibility.Visible;
            }
            else
            {
                this.CapsLockMessage.Visibility = Visibility.Collapsed;
            }

            this.CommandBindings.Add(
                new CommandBinding(AttemptCommand, this.AttemptLogin, this.CanAttemptLogin));
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the command that is fired when the user selects the OK button.
        /// </summary>
        public static RoutedCommand AttemptCommand
        {
            get { return _attemptCommand; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Sets the source, width, and height for the icon that appears to the user.
        /// </summary>
        /// <param name="source">
        /// The image source for the login icon.
        /// </param>
        /// <param name="width">
        /// The width of the icon.
        /// </param>
        /// <param name="height">
        /// The height of the icon.
        /// </param>
        internal void SetIcon(ImageSource source, double width, double height)
        {
            this.LoginIcon.Source = source;
            this.LoginIcon.Width = width;
            this.LoginIcon.Height = height;
            if (source == null)
            {
                this.LoginIcon.Visibility = Visibility.Collapsed;
            }
            else
            {
                this.LoginIcon.Visibility = Visibility.Visible;
            }
        }

        /// <summary>
        /// Sets the login delegate that's called when the user attempts to login in.
        /// </summary>
        /// <param name="loginDelegate">
        /// The delegate that will be called when the user attempts to login in.
        /// </param>
        internal void SetLoginDelegate(LoginAttemptDelegate loginDelegate)
        {
            this._delegate = loginDelegate;
        }

        /// <summary>
        /// Sets the initial value for the password.
        /// </summary>
        /// <param name="password">
        /// The initial value for the password.
        /// </param>
        internal void SetPassword(string password)
        {
            if (password == null)
            {
                return;
            }

            this.PasswordBox.Password = password;
        }

        /// <summary>
        /// Sets the initial value for the username.
        /// </summary>
        /// <param name="username">
        /// The initial value for the username.
        /// </param>
        internal void SetUsername(string username)
        {
            if (username == null)
            {
                return;
            }

            this.UserNameBox.Text = username;
        }

        /// <summary>
        /// Called whenever a key is pressed down so that we can determine whether the caps
        /// lock message is shown.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.KeyEventArgs containing the event data.
        /// </param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            KeyStates state = Keyboard.GetKeyStates(Key.CapsLock);
            if ((state & KeyStates.Toggled) == KeyStates.Toggled)
            {
                this.CapsLockMessage.Visibility = Visibility.Visible;
            }
            else
            {
                this.CapsLockMessage.Visibility = Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Called when the user executes the AttemptLogin command. This forwards the currently
        /// submitted data to the associated login delegate.
        /// </summary>
        /// <param name="s">
        /// The object that fired the AttemptLogin command.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.ExecutedRoutedEventArgs containing the event data.
        /// </param>
        private void AttemptLogin(object s, ExecutedRoutedEventArgs e)
        {
            if (this._delegate == null)
            {
                throw new MissingMethodException(
                    "Unable to attempt a login without a login delegate");
            }

            LoginAttemptData args = new LoginAttemptData(
                this.UserNameBox.Text, this.PasswordBox.Password, true, this._attempts++);

            bool success = this._delegate(args);
            if (success)
            {
                this.DialogResult = true;
                return;
            }

            this.ErrorMessage.Text = args.ErrorMessage;
            if (!args.Retry)
            {
                this.DialogResult = false;
                return;
            }
        }

        /// <summary>
        /// Determines whether the AttemptLogin command can be executed by the user. We don't
        /// want the user to try and login before they have entered any data.
        /// </summary>
        /// <param name="s">
        /// The object that fired the AttemptLogin command.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.CanExecuteRoutedEventArgs containing the event data.
        /// </param>
        private void CanAttemptLogin(object s, CanExecuteRoutedEventArgs e)
        {
            if (this._delegate == null)
            {
                e.CanExecute = false;
                return;
            }

            if (String.IsNullOrWhiteSpace(this.UserNameBox.Text))
            {
                e.CanExecute = false;
                return;
            }

            if (String.IsNullOrWhiteSpace(this.PasswordBox.Password))
            {
                e.CanExecute = false;
                return;
            }

            e.CanExecute = true;
        }

        /// <summary>
        /// Called whenever the user presses the cancel button. This just closes the dialog
        /// making sure false is returned.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private void CancelButtonClicked(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsLoginInternalWindow {Class}
} // RSG.Editor.Controls {Namespace}
