﻿//---------------------------------------------------------------------------------------------
// <copyright file="ConnectToHostAction.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.AssetBuildMonitor.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using RSG.Base.Extensions;
    using RSG.AssetBuildMonitor.ViewModel;
    using RSG.AssetBuildMonitor.ViewModel.Project;
    using RSG.Editor;
    using RSG.Editor.Controls.Dock.ViewModel;
    using RSG.Project.ViewModel;

    /// <summary>
    /// Contains the logic for the <see cref="Commands.ConnectToHost"/> routed
    /// command. This class cannot be inherited.
    /// </summary>
    public sealed class ConnectToHostAction : ButtonAction<CommandArgs>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="ConnectToHostAction"/> class.
        /// </summary>
        /// <param name="serviceProvider">
        /// The service provider that is used by this action to perform application specific
        /// operations, for example selecting a file to open.
        /// </param>
        public ConnectToHostAction(ICommandServiceProvider serviceProvider)
            : base(serviceProvider)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ConnectToHostAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public ConnectToHostAction(CommandResolver resolver)
            : base(new ParameterResolverDelegate<CommandArgs>(resolver))
        {
        }
        #endregion // Constructor(s)
        
        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(CommandArgs args)
        {
            String fallbackText = Resources.CommandStringTable.ConnectToHostName;
            if (null == args || null == args.ViewSite || args.SelectedHosts.None())
            {
                RockstarCommandManager.UpdateItemText(Commands.ConnectToHost, fallbackText);
                return (false);
            }

            // Update the command string label to show the hostname.
            if (args.SelectedHosts.Count() == 1)
            {
                String name = args.SelectedHosts.First().Text;
                String updatedText = String.Format("Connect to {0}", name);
                RockstarCommandManager.UpdateItemText(Commands.ConnectToHost, updatedText);
            }
            else
            {
                RockstarCommandManager.UpdateItemText(Commands.ConnectToHost, 
                    Resources.CommandStringTable.ConnectToHostMultiple);
            }
            return true;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(CommandArgs args)
        {
            IMessageBoxService messageService = this.GetService<IMessageBoxService>();
            if (messageService == null)
            {
                Debug.Assert(false, "Unable to open as services are missing.");
                return;
            }
            
            foreach (HostNode host in args.SelectedHosts)
            {
                // If the host is all ready open; select it.
                foreach (DocumentItem openedDocument in args.AllDocuments)
                {
                    if (String.Equals(openedDocument.FullPath, host.Text))
                    {
                        openedDocument.IsSelected = true;
                        return;
                    }
                }

                DocumentPane pane = args.ViewSite.LastActiveDocumentPane;
                if (pane == null)
                {
                    pane = args.ViewSite.FirstDocumentPane;
                    if (pane == null)
                    {
                        Debug.Assert(false, "Cannot find the pane to open the document in.");
                        return;
                    }
                }

                try
                {
                    DocumentItem item = null;
                    ICreatesProjectDocument documentCreator = (host as ICreatesProjectDocument);
                    if (null == documentCreator)
                    {
                        item = new HostDocumentItem(pane, host.Text);
                    }
                    else
                    {
                        item = documentCreator.CreateDocument(pane, host.Text);
                    }

                    ICreatesDocumentContent contentCreator = (host as ICreatesDocumentContent);
                    item.Content = contentCreator.CreateDocumentContent(item.UndoEngine);
                    //item.Content = new AssetBuildHistoryViewModel(RSG.Base.Logging.LogFactory.ApplicationLog,
                      //      host, args.BugstarUsers);
                    item.IsSelected = true;
                    pane.InsertChild(pane.Children.Count, item);
                }
                catch (Exception)
                {
                    messageService.ShowStandardErrorBox(
                        String.Format(Resources.CommandStringTable.ConnectToHostError, host.Text),
                        "Error");
                }
            }
        }
        #endregion Methods
    }

} // RSG.AssetBuildMonitor.Commands namespace
