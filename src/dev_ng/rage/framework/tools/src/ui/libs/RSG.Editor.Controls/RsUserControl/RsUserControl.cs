﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsUserControl.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Windows;
    using System.Windows.Controls;
    using System.Xml;
    using RSG.Base.Extensions;
    using RSG.Editor.Controls.AttachedDependencyProperties;
    using RSG.Editor.Controls.Helpers;
    using RSG.Editor.Controls.Themes;

    /// <summary>
    /// Defines a user control that supports saving and loading persistent data.
    /// </summary>
    public partial class RsUserControl : UserControl
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="LayoutPersistentId"/> property.
        /// </summary>
        private Guid _layoutPersistentId;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="RsUserControl"/> class.
        /// </summary>
        public RsUserControl()
        {
            this._layoutPersistentId = Guid.Empty;
            this.Loaded += this.OnLoaded;

            if (DesignerProperties.GetIsInDesignMode(this))
            {
                Collection<ResourceDictionary> resources = this.Resources.MergedDictionaries;
                if (resources == null)
                {
                    return;
                }

                string theme = "Themes/theme.standard.xaml";
                string themePath = "pack://application:,,,/RSG.Editor.Controls;component/{0}";
                string standardTheme = themePath.FormatInvariant(theme);
                Uri standardSource = new Uri(standardTheme, System.UriKind.RelativeOrAbsolute);

                theme = "Themes/theme.light.xaml";
                string lightTheme = themePath.FormatInvariant(theme);
                Uri lightSource = new Uri(lightTheme, System.UriKind.RelativeOrAbsolute);

                try
                {
                    ResourceDictionary lightDictionary = new ResourceDictionary();
                    lightDictionary.Source = lightSource;
                    resources.Add(lightDictionary);

                    ResourceDictionary standardDictionary = new ResourceDictionary();
                    standardDictionary.Source = standardSource;
                    resources.Add(standardDictionary);

                    DependencyProperty property = RsUserControl.BackgroundProperty;
                    object reference = ThemeBrushes.WindowBackgroundKey;
                    this.SetResourceReference(property, reference);
                }
                catch (Exception)
                {
                    return;
                }
            }
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the unique id that is used as the location of the persistent layout
        /// data.
        /// </summary>
        public Guid LayoutPersistentId
        {
            get
            {
                if (this._layoutPersistentId == Guid.Empty)
                {
                    return this.GetType().GUID;
                }

                return this._layoutPersistentId;
            }

            set
            {
                this._layoutPersistentId = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the layout for this control gets serialised on
        /// exit and deserialised after loading.
        /// </summary>
        protected virtual bool MakeLayoutPersistent
        {
            get { return false; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Override to deserialise any persistent data used by this control.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that the data is contained within.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        public virtual void DeserialiseData(XmlReader reader, IFormatProvider provider)
        {
        }

        /// <summary>
        /// Override to serialise any persistent data used by this control.
        /// </summary>
        /// <param name="writer">
        /// The xml writer that the persistent data should get serialised out to.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        public virtual void SerialiseData(XmlWriter writer, IFormatProvider provider)
        {
        }

        /// <summary>
        /// Deserialises the layout of this control from the specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that the layout data is contained within.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        internal void DeserialiseLayout(XmlReader reader, IFormatProvider provider)
        {
            var map = new Dictionary<string, DependencyObject>();
            foreach (DependencyObject obj in this.GetSerialisableDescendents())
            {
                string uid = SerialisationProperties.GetSerialiseUid(obj);
                if (uid == null)
                {
                    Debug.Assert(
                        uid != null, "Unable to deserialise a control without a uid.");
                    continue;
                }

                if (!map.ContainsKey(uid))
                {
                    map.Add(uid, obj);
                }
            }

            while (reader.MoveToContent() != XmlNodeType.None)
            {
                if (reader.IsStartElement() == false || reader.Name == "Layout")
                {
                    reader.Read();
                    continue;
                }

                DependencyObject obj = null;
                if (map.TryGetValue(reader.Name, out obj))
                {
                    obj.DeserialiseLayout(reader, provider);
                    reader.Read();
                }
                else
                {
                    reader.Skip();
                }
            }
        }

        /// <summary>
        /// Serialises the layout of this control into the specified xml writer.
        /// </summary>
        /// <param name="writer">
        /// The xml writer that the layout gets serialised out to.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        internal void SerialiseLayout(XmlWriter writer, IFormatProvider provider)
        {
            if (!this.MakeLayoutPersistent)
            {
                return;
            }

            foreach (DependencyObject obj in this.GetSerialisableDescendents())
            {
                string uid = SerialisationProperties.GetSerialiseUid(obj);
                if (uid == null)
                {
                    Debug.Assert(uid != null, "Unable to serialise a control without a uid.");
                    continue;
                }

                writer.WriteStartElement(uid);
                obj.SerialiseLayout(writer, provider);
                writer.WriteEndElement();
            }
        }

        /// <summary>
        /// Serialises the persistent data for this control.
        /// </summary>
        internal void SerialisePersistentData()
        {
            Environment.SpecialFolder folder = Environment.SpecialFolder.LocalApplicationData;
            string appData = Environment.GetFolderPath(folder);
            string uniqueName = this.ConstructUniqueNameForPersistence();
            string persistentDirectory = Path.Combine(appData, "Rockstar Games", uniqueName);
            if (!Directory.Exists(persistentDirectory))
            {
                Directory.CreateDirectory(persistentDirectory);
            }

            string layoutFilename = Path.Combine(persistentDirectory, "layout.xml");
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            using (XmlWriter writer = XmlWriter.Create(layoutFilename, settings))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Layout");
                this.SerialiseLayout(writer, CultureInfo.InvariantCulture);
                writer.WriteEndElement();
                writer.WriteEndDocument();
            }

            string dataFilename = Path.Combine(persistentDirectory, "data.xml");
            using (XmlWriter writer = XmlWriter.Create(dataFilename, settings))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Data");
                this.SerialiseData(writer, CultureInfo.InvariantCulture);
                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }

        /// <summary>
        /// Retrieves the name of the directory that stores the persistent data for this
        /// control.
        /// </summary>
        /// <returns>
        /// The name of the directory that stores the persistent data for this control.
        /// </returns>
        private string ConstructUniqueNameForPersistence()
        {
            Guid id = this.LayoutPersistentId;
            string idPortion = id.ToString("B").ToUpper(CultureInfo.InvariantCulture);
            string namePortion = this.GetType().Name;
            return idPortion + "_" + namePortion;
        }

        /// <summary>
        /// Called once the user control has loaded so that the persistent data and layout can
        /// be loaded.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// This System.Windows.RoutedEventArgs data for this event.
        /// </param>
        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            RsApplication application = Application.Current as RsApplication;
            if (application != null)
            {
                application.RegisterRsUserControl(this);
            }

            this.Loaded -= this.OnLoaded;
            Environment.SpecialFolder folder = Environment.SpecialFolder.LocalApplicationData;
            string appData = Environment.GetFolderPath(folder);
            string uniqueName = this.ConstructUniqueNameForPersistence();
            string persistentDirectory = Path.Combine(appData, "Rockstar Games", uniqueName);

            if (this.MakeLayoutPersistent)
            {
                string layoutFilename = Path.Combine(persistentDirectory, "layout.xml");
                if (File.Exists(layoutFilename))
                {
                    try
                    {
                        using (XmlReader reader = XmlReader.Create(layoutFilename))
                        {
                            reader.MoveToContent();
                            this.DeserialiseLayout(reader, CultureInfo.InvariantCulture);
                        }
                    }
                    catch (Exception)
                    {
                        // Ignore any exceptions that are thrown while parsing the layout file.
                    }
                }
            }

            string dataFilename = Path.Combine(persistentDirectory, "data.xml");
            if (File.Exists(dataFilename))
            {
                try
                {
                    using (XmlReader reader = XmlReader.Create(dataFilename))
                    {
                        reader.MoveToContent();
                        this.DeserialiseData(reader, CultureInfo.InvariantCulture);
                    }
                }
                catch (Exception)
                {
                    // Ignore any exceptions that are thrown while parsing the data file.
                }
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsUserControl {Class}
} // RSG.Editor.Controls {Namespace}
