﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsToggleComboBox.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Input;

    /// <summary>
    /// Represents a selection control with a drop-down list that can be shown or hidden that
    /// contains items that can be toggled.
    /// </summary>
    public class RsToggleComboBox : ComboBox
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="CheckCount" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty CheckCountProperty;

        /// <summary>
        /// Identifies the <see cref="Command" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty CommandProperty;

        /// <summary>
        /// Identifies the <see cref="CommandTarget" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty CommandTargetProperty;

        /// <summary>
        /// Identifies the <see cref="DisplayText" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty DisplayTextProperty;

        /// <summary>
        /// Identifies the <see cref="ShowCheckCount" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty ShowCheckCountProperty;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsToggleComboBox" /> class.
        /// </summary>
        static RsToggleComboBox()
        {
            CommandProperty =
                ButtonBase.CommandProperty.AddOwner(
                typeof(RsToggleComboBox), new FrameworkPropertyMetadata(OnCommandChanged));

            CommandTargetProperty =
                ButtonBase.CommandTargetProperty.AddOwner(
                typeof(RsToggleComboBox), new FrameworkPropertyMetadata(null));

            DisplayTextProperty =
                DependencyProperty.Register(
                "DisplayText",
                typeof(string),
                typeof(RsToggleComboBox),
                new FrameworkPropertyMetadata(null));

            ShowCheckCountProperty =
                DependencyProperty.Register(
                "ShowCheckCount",
                typeof(bool),
                typeof(RsToggleComboBox),
                new FrameworkPropertyMetadata(true));

            CheckCountProperty =
                DependencyProperty.Register(
                "CheckCount",
                typeof(string),
                typeof(RsToggleComboBox),
                new FrameworkPropertyMetadata("(0 of 0)"));

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsToggleComboBox),
                new FrameworkPropertyMetadata(typeof(RsToggleComboBox)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsToggleComboBox" /> class.
        /// </summary>
        public RsToggleComboBox()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the string used to display the check count of the filter.
        /// </summary>
        public string CheckCount
        {
            get { return (string)this.GetValue(CheckCountProperty); }
            set { this.SetValue(CheckCountProperty, value); }
        }

        /// <summary>
        /// Gets or sets the command associated with the menu item.
        /// </summary>
        public ICommand Command
        {
            get { return (ICommand)this.GetValue(CommandProperty); }
            set { this.SetValue(CommandProperty, value); }
        }

        /// <summary>
        /// Gets or sets the target element on which to raise the specified command.
        /// </summary>
        public IInputElement CommandTarget
        {
            get { return (IInputElement)this.GetValue(CommandTargetProperty); }
            set { this.SetValue(CommandTargetProperty, value); }
        }

        /// <summary>
        /// Gets or sets the text that is displayed in the controls content area. The default
        /// value is null.
        /// </summary>
        public string DisplayText
        {
            get { return (string)this.GetValue(DisplayTextProperty); }
            set { this.SetValue(DisplayTextProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the show check count is displayed in the
        /// content area for this control.
        /// </summary>
        public bool ShowCheckCount
        {
            get { return (bool)this.GetValue(ShowCheckCountProperty); }
            set { this.SetValue(ShowCheckCountProperty, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Handles the toggling of one of the combo box items.
        /// </summary>
        /// <param name="item">
        /// The item that has been selected.
        /// </param>
        /// <param name="index">
        /// The index of the item that has been selected.
        /// </param>
        internal void HandleToggle(RsToggleComboBoxItem item, int index)
        {
            object commandParameter = this.GetCommandParameter(item, index);
            IInputElement inputElement = this.CommandTarget;
            RoutedCommand routedCommand = this.Command as RoutedCommand;
            if (routedCommand != null)
            {
                if (inputElement == null)
                {
                    inputElement = this as IInputElement;
                }

                if (routedCommand.CanExecute(commandParameter, inputElement))
                {
                    routedCommand.Execute(commandParameter, inputElement);
                }
            }
            else if (this.Command != null)
            {
                if (this.Command.CanExecute(commandParameter))
                {
                    this.Command.Execute(commandParameter);
                }
            }
        }

        /// <summary>
        /// Creates or identifies the element used to display the child items.
        /// </summary>
        /// <returns>
        /// A new <see cref="RSG.Editor.Controls.RsToggleComboBoxItem" />.
        /// </returns>
        protected override DependencyObject GetContainerForItemOverride()
        {
            RsToggleComboBoxItem item = new RsToggleComboBoxItem();

            item.OnCommandChanged(
                new DependencyPropertyChangedEventArgs(
                    RsComboBox.CommandProperty, null, this.Command));

            return item;
        }

        /// <summary>
        /// Determines whether the specified item is, or is eligible to be, its own item
        /// container.
        /// </summary>
        /// <param name="item">
        /// The item to check whether it is an item container.
        /// </param>
        /// <returns>
        /// True if the item is eligible to be its own item container; otherwise, false.
        /// </returns>
        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            if (item is RsToggleComboBoxItem)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Called whenever the <see cref="Command"/> dependency property changes.
        /// </summary>
        /// <param name="d">
        /// The object whose <see cref="Command"/> dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnCommandChanged(
            DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RsToggleComboBox comboBox = d as RsToggleComboBox;
            if (comboBox == null)
            {
                return;
            }

            for (int i = 0; i < comboBox.Items.Count; i++)
            {
                DependencyObject obj = comboBox.ItemContainerGenerator.ContainerFromIndex(i);
                RsToggleComboBoxItem item = obj as RsToggleComboBoxItem;
                if (item == null)
                {
                    continue;
                }

                item.OnCommandChanged(e);
            }
        }

        /// <summary>
        /// Gets the command parameter for the specified item with the specified index.
        /// </summary>
        /// <param name="item">
        /// The item to get the command parameter for. Can be null.
        /// </param>
        /// <param name="index">
        /// The index of the item to get the command parameter for. Can be -1.
        /// </param>
        /// <returns>
        /// The command parameter that is associated with the specified item if not null or the
        /// item at the specified index.
        /// </returns>
        private object GetCommandParameter(RsToggleComboBoxItem item, int index)
        {
            object commandParameter = null;
            if (index == -1)
            {
                commandParameter = this.Text;
            }
            else
            {
                if (item != null)
                {
                    commandParameter = item.CommandParameter;
                }
            }

            return commandParameter;
        }

        /// <summary>
        /// Handles the selection of one of the combo box items.
        /// </summary>
        /// <param name="index">
        /// The index of the item that has been selected.
        /// </param>
        private void HandleToggle(int index)
        {
            DependencyObject obj = null;
            if (index != -1)
            {
                obj = this.ItemContainerGenerator.ContainerFromIndex(index);
            }

            this.HandleToggle(obj as RsToggleComboBoxItem, index);
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsToggleComboBox {Class}
} // RSG.Editor.Controls {Namespace}
