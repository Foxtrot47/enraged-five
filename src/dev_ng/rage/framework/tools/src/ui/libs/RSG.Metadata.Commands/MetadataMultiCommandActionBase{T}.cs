﻿//---------------------------------------------------------------------------------------------
// <copyright file="MetadataMultiCommandActionBase{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Commands
{
    using RSG.Editor;

    /// <summary>
    /// Provides a base class to all metadata command actions that need a secondary multi
    /// command parameter.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the second command parameter.
    /// </typeparam>
    public abstract class MetadataMultiCommandActionBase<T>
        : ButtonAction<MetadataCommandArgs, MultiCommandParameter<T>>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MetadataMultiCommandActionBase{T}"/>
        /// class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        protected MetadataMultiCommandActionBase(MetadataCommandArgsResolver resolver)
            : base(new ParameterResolverDelegate<MetadataCommandArgs>(resolver))
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="secondaryParameter">
        /// The secondary command parameter that has been sent with the command.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(
            MetadataCommandArgs args, MultiCommandParameter<T> secondaryParameter)
        {
            T parameter = default(T);
            if (secondaryParameter != null)
            {
                parameter = secondaryParameter.ItemParameter;
            }

            return this.CanExecute(args, parameter);
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="secondaryParameter">
        /// The secondary command parameter that has been sent with the command.
        /// </param>
        public override void Execute(
            MetadataCommandArgs args, MultiCommandParameter<T> secondaryParameter)
        {
            T parameter = default(T);
            if (secondaryParameter != null)
            {
                parameter = secondaryParameter.ItemParameter;
            }

            this.Execute(args, parameter);
        }

        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="secondaryParameter">
        /// The secondary command parameter that has been sent with the command.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        protected virtual bool CanExecute(MetadataCommandArgs args, T secondaryParameter)
        {
            return true;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="secondaryParameter">
        /// The secondary command parameter that has been sent with the command.
        /// </param>
        protected virtual void Execute(MetadataCommandArgs args, T secondaryParameter)
        {
        }
        #endregion Methods
    } // RSG.Metadata.Commands.MetadataMultiCommandActionBase {Class}
} // RSG.Metadata.Commands {Namespace}
