﻿using System.Collections.ObjectModel;
using RSG.Editor.Model;

namespace RSG.Editor.Controls.MapViewport.OverlayComponents
{
    /// <summary>
    /// Represents a collection of <see cref="Component"/> objects.
    /// </summary>
    public class Layer : NotifyPropertyChangedBase
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Components"/> property.
        /// </summary>
        private readonly ObservableCollection<Component> _components;

        /// <summary>
        /// Private field for the <see cref="Name"/> property.
        /// </summary>
        private string _name;

        /// <summary>
        /// Private field for the <see cref="StartsGroup"/> property.
        /// </summary>
        private bool _startsGroup;

        /// <summary>
        /// Private field for the <see cref="Visible"/> property.
        /// </summary>
        private bool _visible;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Layer"/> class.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="startsGroup"></param>
        /// <param name="visible"></param>
        public Layer(string name, bool startsGroup, bool visible = true)
            : base()
        {
            _components = new ObservableCollection<Component>();
            _name = name;
            _startsGroup = startsGroup;
            _visible = visible;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Retrieves the overlay components that make up this layer.
        /// </summary>
        public ObservableCollection<Component> Components
        {
            get { return _components; }
        }

        /// <summary>
        /// Name to display for this layer in the layer display on the map foreground.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }

        /// <summary>
        /// Returns whether this layer should start a new group in the layer display on the map foreground.
        /// </summary>
        public bool StartsGroup
        {
            get { return _startsGroup; }
            set { SetProperty(ref _startsGroup, value); }
        }

        /// <summary>
        /// Returns whether this layer should start a new group in the layer display on the map foreground.
        /// </summary>
        public bool Visible
        {
            get { return _visible; }
            set { SetProperty(ref _visible, value); }
        }
        #endregion
    }
}
