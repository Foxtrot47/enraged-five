﻿//---------------------------------------------------------------------------------------------
// <copyright file="ManualValidatonAdornedPlaceholder.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Helpers
{
    using System;
    using System.Collections;
    using System.Windows;
    using System.Windows.Markup;
    using System.Windows.Media;

    /// <summary>
    /// Represents the element used in a Control Template to specify where a decorated control
    /// is placed relative to other elements in the Template.
    /// </summary>
    public class ManualValidatonAdornedPlaceholder : FrameworkElement, IAddChild
    {
        #region Fields
        /// <summary>
        /// The reference to the one and only visual child.
        /// </summary>
        private UIElement _child;

        /// <summary>
        /// The private field used for the <see cref="TemplatedAdorner"/> property.
        /// </summary>
        private ManualValidationAdorner _templatedAdorner;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ManualValidatonAdornedPlaceholder"/>
        /// class.
        /// </summary>
        public ManualValidatonAdornedPlaceholder()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the adorner that this object is reserving space for.
        /// </summary>
        public UIElement AdornedElement
        {
            get
            {
                ManualValidationAdorner adorner = this.TemplatedAdorner;
                if (adorner != null)
                {
                    return this.TemplatedAdorner.AdornedElement;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets or sets the single child object of this object.
        /// </summary>
        public virtual UIElement Child
        {
            get
            {
                return this._child;
            }

            set
            {
                UIElement child = this._child;
                if (child != value)
                {
                    this.RemoveVisualChild(child);
                    this.RemoveLogicalChild(child);
                    this._child = value;
                    this.AddVisualChild(this._child);
                    this.AddLogicalChild(value);
                    this.InvalidateMeasure();
                }
            }
        }

        /// <summary>
        /// Gets the adorner object that has been defined in the common control template.
        /// </summary>
        public ManualValidationAdorner TemplatedAdorner
        {
            get
            {
                if (this._templatedAdorner != null)
                {
                    return this._templatedAdorner;
                }

                FrameworkElement frameworkElement = this.TemplatedParent as FrameworkElement;
                if (frameworkElement != null)
                {
                    DependencyObject parent = VisualTreeHelper.GetParent(frameworkElement);
                    this._templatedAdorner = parent as ManualValidationAdorner;

                    if (this._templatedAdorner != null)
                    {
                        if (this._templatedAdorner.AdornedPlaceholder == null)
                        {
                            this._templatedAdorner.AdornedPlaceholder = this;
                        }
                    }
                }

                return this._templatedAdorner;
            }
        }

        /// <summary>
        /// Gets an enumerator for the logical child elements of this object.
        /// </summary>
        protected override IEnumerator LogicalChildren
        {
            get { yield return this._child; }
        }

        /// <summary>
        /// Gets the number of visual child objects.
        /// </summary>
        protected override int VisualChildrenCount
        {
            get { return this._child == null ? 0 : 1; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// This type or member supports the Windows Presentation Foundation (WPF)
        /// infrastructure and is not intended to be used directly from your code.
        /// </summary>
        /// <param name="value">
        /// An object to add as a child.
        /// </param>
        void IAddChild.AddChild(object value)
        {
            if (value == null)
            {
                return;
            }

            if (!(value is UIElement))
            {
                return;
            }

            if (this.Child != null)
            {
                return;
            }

            this.Child = (UIElement)value;
        }

        /// <summary>
        /// This type or member supports the Windows Presentation Foundation (WPF)
        /// infrastructure and is not intended to be used directly from your code.
        /// </summary>
        /// <param name="text">
        /// A string to add to the object.
        /// </param>
        void IAddChild.AddText(string text)
        {
        }

        /// <summary>
        /// Positions the first visual child object and returns the size in layout required.
        /// </summary>
        /// <param name="arrangeBounds">
        /// The size that this object should use to arrange its child element.
        /// </param>
        /// <returns>
        /// The actual size needed by the element.
        /// </returns>
        protected override Size ArrangeOverride(Size arrangeBounds)
        {
            UIElement child = this.Child;
            if (child != null)
            {
                child.Arrange(new Rect(arrangeBounds));
            }

            return arrangeBounds;
        }

        /// <summary>
        /// Retrieves the visual child object at the specified index.
        /// </summary>
        /// <param name="index">
        /// The zero-based index that specifies the child object to retrieve.
        /// </param>
        /// <returns>
        /// The visual child object at the specified index.
        /// </returns>
        protected override Visual GetVisualChild(int index)
        {
            if (this._child == null || index != 0)
            {
                return null;
            }

            return this._child;
        }

        /// <summary>
        /// Determines the size of this object.
        /// </summary>
        /// <param name="constraint">
        /// An upper limit value that the return value should not exceed.
        /// </param>
        /// <returns>
        /// The desired size of this  object.
        /// </returns>
        protected override Size MeasureOverride(Size constraint)
        {
            if (this.TemplatedParent == null)
            {
                return new Size(0.0, 0.0);
            }

            if (this.AdornedElement == null)
            {
                return new Size(0.0, 0.0);
            }

            Size renderSize = this.AdornedElement.RenderSize;
            UIElement child = this.Child;
            if (child != null)
            {
                child.Measure(renderSize);
            }

            return renderSize;
        }

        /// <summary>
        /// Raises the IsInitialized event. This method is called when IsInitialized is set to
        /// true internally.
        /// </summary>
        /// <param name="e">
        /// Arguments of the event.
        /// </param>
        protected override void OnInitialized(EventArgs e)
        {
            if (this.TemplatedParent == null)
            {
                return;
            }

            base.OnInitialized(e);
        }
        #endregion Methods
    } // RSG.Editor.Controls.Helpers.ManualValidatonAdornedPlaceholder {Class}
} // RSG.Editor.Controls.Helpers {Namespace}
