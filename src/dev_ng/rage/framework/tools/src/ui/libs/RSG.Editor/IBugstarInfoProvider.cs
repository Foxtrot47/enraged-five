﻿//---------------------------------------------------------------------------------------------
// <copyright file="IBugstarInfoProvider.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    /// <summary>
    /// Interface from which bugstar information can be obtained.
    /// </summary>
    public interface IBugstarInfoProvider : IProductInfoService
    {
        /// <summary>
        /// Gets the name of the bugstar component that bugs should have their component
        /// set to.
        /// </summary>
        string BugstarComponent { get; }

        /// <summary>
        /// Gets the name of the bugstar project that bugs should be added to.
        /// </summary>
        string BugstarProjectName { get; }

        /// <summary>
        /// Gets the name of the default owner to assign bugs to.
        /// </summary>
        string DefaultBugOwner { get; }
    }
}
