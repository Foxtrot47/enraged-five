﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProjectCollectionDefinition.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel.Definitions
{
    using System.Collections.Generic;
    using System.Windows.Media.Imaging;
    using RSG.Project.Model;

    /// <summary>
    /// Represents the definition for a single project collection type that includes all of the
    /// definitions for any valid project and item for this collection.
    /// </summary>
    public abstract class ProjectCollectionDefinition
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="ProjectDefinition"/> property.
        /// </summary>
        private List<ProjectDefinition> _projectDefinitions;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ProjectCollectionDefinition"/> class.
        /// </summary>
        protected ProjectCollectionDefinition()
        {
            this._projectDefinitions = new List<ProjectDefinition>();
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the extension used for this defined collection.
        /// </summary>
        public abstract string Extension { get; }

        /// <summary>
        /// Gets the string used as the filter in a open or save dialog for this defined
        /// collection.
        /// </summary>
        public abstract string Filter { get; }

        /// <summary>
        /// Gets the icon that is used to display this project collection.
        /// </summary>
        public virtual BitmapSource Icon
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the read-only list of all of the project definitions defined for this
        /// collection.
        /// </summary>
        public IReadOnlyList<ProjectDefinition> ProjectDefinitions
        {
            get { return this._projectDefinitions; }
        }

        /// <summary>
        /// Gets a byte array containing the data that is used to create a new instance of the
        /// defined project collection.
        /// </summary>
        public virtual byte[] Template
        {
            get { return null; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Adds a new project definition into this collection definition.
        /// </summary>
        /// <param name="definition">
        /// The project definition to add.
        /// </param>
        public void AddProjectDefinition(ProjectDefinition definition)
        {
            this._projectDefinitions.Add(definition);
        }
        #endregion Methods
    } // RSG.Project.ViewModel.Definitions.ProjectCollectionDefinition {Class}
} // RSG.Project.ViewModel.Definitions {Namespace}
