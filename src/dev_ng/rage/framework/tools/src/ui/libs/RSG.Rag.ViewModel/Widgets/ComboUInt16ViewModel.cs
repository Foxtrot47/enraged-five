﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Rag.Widgets;

namespace RSG.Rag.ViewModel.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    public class ComboUInt16ViewModel : ComboViewModel<UInt16>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        public ComboUInt16ViewModel(WidgetCombo<UInt16> widget)
            : base(widget)
        {
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public override int SelectedItem
        {
            get { return (int)(Widget.SelectedItem - (UInt16)Widget.Offset); }
            set { Widget.SelectedItem = (UInt16)(value + Widget.Offset); }
        }

        /// <summary>
        /// 
        /// </summary>
        public override string DefaultValue
        {
            get { return String.Format("{0} (Index: {1})", Widget.Items[Widget.DefaultItem], Widget.DefaultItem); }
        }
        #endregion // Properties
    } // ComboUInt16ViewModel
}
