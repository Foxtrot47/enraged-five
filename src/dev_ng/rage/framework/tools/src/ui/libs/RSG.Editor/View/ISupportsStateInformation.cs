﻿//---------------------------------------------------------------------------------------------
// <copyright file="ISupportsStateInformation.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    using System.Windows.Media.Imaging;

    /// <summary>
    /// When implemented represents a item that contains state information about the object.
    /// </summary>
    public interface ISupportsStateInformation
    {
        #region Properties
        /// <summary>
        /// Gets the icon that is used to display the state of this item over the normal icon.
        /// </summary>
        BitmapSource Overlayicon { get; }

        /// <summary>
        /// Gets the icon that is used to display the state of this item.
        /// </summary>
        BitmapSource StateIcon { get; }

        /// <summary>
        /// Gets the tooltip text that this item has that describes its current state.
        /// </summary>
        string StateToolTipText { get; }
        #endregion Properties
    } // RSG.Editor.View.ISupportsStateInformation {Interface}
} // RSG.Editor.View {Namespace}
