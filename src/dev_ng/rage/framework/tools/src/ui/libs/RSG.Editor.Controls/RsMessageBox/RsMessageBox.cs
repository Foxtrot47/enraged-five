﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsMessageBox.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Drawing;
    using System.Windows;
    using System.Windows.Interop;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using RSG.Editor.Controls.Resources;

    /// <summary>
    /// Provides the functionality to show the user a message box with custom buttons and
    /// supports modern theming.
    /// </summary>
    public class RsMessageBox : IMessageBox
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Buttons"/> property.
        /// </summary>
        private List<CustomMessageBoxButton> _buttons;

        /// <summary>
        /// The private field used for the <see cref="Caption"/> property.
        /// </summary>
        private string _caption;

        /// <summary>
        /// The private field used for the <see cref="HelpUrl"/> property.
        /// </summary>
        private string _helpUrl;

        /// <summary>
        /// The private field used for the <see cref="ImageSource"/> property.
        /// </summary>
        private ImageSource _imageSource;

        /// <summary>
        /// The private reference to the owner window this message box was created with.
        /// </summary>
        private Window _owner;

        /// <summary>
        /// The private field used for the <see cref="Text"/> property.
        /// </summary>
        private string _text;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="RsMessageBox"/> class.
        /// </summary>
        public RsMessageBox()
        {
            this._buttons = new List<CustomMessageBoxButton>();
            this.ImageSource = null;
            this.Caption = string.Empty;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsMessageBox"/> class.
        /// </summary>
        /// <param name="owner">
        /// A System.Windows.Window that represents the owner window of the message box.
        /// </param>
        public RsMessageBox(Window owner)
        {
            this._buttons = new List<CustomMessageBoxButton>();
            this._owner = owner;
            this.ImageSource = null;
            this.Caption = string.Empty;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the string that specifies the title bar caption to display.
        /// </summary>
        public string Caption
        {
            get { return this._caption; }
            set { this._caption = value; }
        }

        /// <summary>
        /// Gets or sets the url string that is shown when the user chooses to view help. The
        /// help button is only shown if this is set to non empty/null string.
        /// </summary>
        public string HelpUrl
        {
            get { return this._helpUrl; }
            set { this._helpUrl = value; }
        }

        /// <summary>
        /// Gets or sets the image source of the 32x32 icon that should be displayed.
        /// </summary>
        public ImageSource ImageSource
        {
            get { return this._imageSource; }
            set { this._imageSource = value; }
        }

        /// <summary>
        /// Gets or sets the string that specifies the text to display.
        /// </summary>
        public string Text
        {
            get { return this._text; }
            set { this._text = value; }
        }

        /// <summary>
        /// Gets the list of all of the custom buttons that have been added to this message
        /// box.
        /// </summary>
        internal List<CustomMessageBoxButton> Buttons
        {
            get { return this._buttons; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Displays a message box that has a message and that returns a result.
        /// </summary>
        /// <param name="messageBoxText">
        /// A System.String that specifies the text to display.
        /// </param>
        /// <returns>
        /// A System.Windows.MessageBoxResult value that specifies which message box button is
        /// clicked by the user.
        /// </returns>
        public static MessageBoxResult Show(string messageBoxText)
        {
            return RsMessageBox.Show(
                null,
                messageBoxText,
                null,
                MessageBoxButton.OK,
                MessageBoxImage.None,
                MessageBoxResult.None);
        }

        /// <summary>
        /// Displays a message box in front of the specified window that has a message and that
        /// returns a result.
        /// </summary>
        /// <param name="owner">
        /// A System.Windows.Window that represents the owner window of the message box.
        /// </param>
        /// <param name="messageBoxText">
        /// A System.String that specifies the text to display.
        /// </param>
        /// <returns>
        /// A System.Windows.MessageBoxResult value that specifies which message box button is
        /// clicked by the user.
        /// </returns>
        public static MessageBoxResult Show(Window owner, string messageBoxText)
        {
            return RsMessageBox.Show(
                owner,
                messageBoxText,
                null,
                MessageBoxButton.OK,
                MessageBoxImage.None,
                MessageBoxResult.None);
        }

        /// <summary>
        /// Displays a message box that has a message and title bar caption; and that returns a
        /// result.
        /// </summary>
        /// <param name="messageBoxText">
        /// A System.String that specifies the text to display.
        /// </param>
        /// <param name="caption">
        /// A System.String that specifies the title bar caption to display.
        /// </param>
        /// <returns>
        /// A System.Windows.MessageBoxResult value that specifies which message box button is
        /// clicked by the user.
        /// </returns>
        public static MessageBoxResult Show(string messageBoxText, string caption)
        {
            return RsMessageBox.Show(
                null,
                messageBoxText,
                caption,
                MessageBoxButton.OK,
                MessageBoxImage.None,
                MessageBoxResult.None);
        }

        /// <summary>
        /// Displays a message box in front of the specified window that has a message and
        /// title bar caption; and that returns a result.
        /// </summary>
        /// <param name="owner">
        /// A System.Windows.Window that represents the owner window of the message box.
        /// </param>
        /// <param name="messageBoxText">
        /// A System.String that specifies the text to display.
        /// </param>
        /// <param name="caption">
        /// A System.String that specifies the title bar caption to display.
        /// </param>
        /// <returns>
        /// A System.Windows.MessageBoxResult value that specifies which message box button is
        /// clicked by the user.
        /// </returns>
        public static MessageBoxResult Show(
            Window owner, string messageBoxText, string caption)
        {
            return RsMessageBox.Show(
                owner,
                messageBoxText,
                caption,
                MessageBoxButton.OK,
                MessageBoxImage.None,
                MessageBoxResult.None);
        }

        /// <summary>
        /// Displays a message box that has a message, title bar caption, and button; and that
        /// returns a result.
        /// </summary>
        /// <param name="messageBoxText">
        /// A System.String that specifies the text to display.
        /// </param>
        /// <param name="caption">
        /// A System.String that specifies the title bar caption to display.
        /// </param>
        /// <param name="button">
        /// A System.Windows.MessageBoxButton value that specifies which button or buttons to
        /// display.
        /// </param>
        /// <returns>
        /// A System.Windows.MessageBoxResult value that specifies which message box button is
        /// clicked by the user.
        /// </returns>
        public static MessageBoxResult Show(
            string messageBoxText, string caption, MessageBoxButton button)
        {
            return RsMessageBox.Show(
                null,
                messageBoxText,
                caption,
                button,
                MessageBoxImage.None,
                MessageBoxResult.None);
        }

        /// <summary>
        /// Displays a message box in front of the specified window that has a message, title
        /// bar caption, and button; and that returns a result.
        /// </summary>
        /// <param name="owner">
        /// A System.Windows.Window that represents the owner window of the message box.
        /// </param>
        /// <param name="messageBoxText">
        /// A System.String that specifies the text to display.
        /// </param>
        /// <param name="caption">
        /// A System.String that specifies the title bar caption to display.
        /// </param>
        /// <param name="button">
        /// A System.Windows.MessageBoxButton value that specifies which button or buttons to
        /// display.
        /// </param>
        /// <returns>
        /// A System.Windows.MessageBoxResult value that specifies which message box button is
        /// clicked by the user.
        /// </returns>
        public static MessageBoxResult Show(
            Window owner, string messageBoxText, string caption, MessageBoxButton button)
        {
            return RsMessageBox.Show(
                owner,
                messageBoxText,
                caption,
                button,
                MessageBoxImage.None,
                MessageBoxResult.None);
        }

        /// <summary>
        /// Displays a message box that has a message, title bar caption, button, and icon; and
        /// that returns a result.
        /// </summary>
        /// <param name="messageBoxText">
        /// A System.String that specifies the text to display.
        /// </param>
        /// <param name="caption">
        /// A System.String that specifies the title bar caption to display.
        /// </param>
        /// <param name="button">
        /// A System.Windows.MessageBoxButton value that specifies which button or buttons to
        /// display.
        /// </param>
        /// <param name="icon">
        /// A System.Windows.MessageBoxImage value that specifies the icon to display.
        /// </param>
        /// <returns>
        /// A System.Windows.MessageBoxResult value that specifies which message box button is
        /// clicked by the user.
        /// </returns>
        public static MessageBoxResult Show(
            string messageBoxText,
            string caption,
            MessageBoxButton button,
            MessageBoxImage icon)
        {
            return RsMessageBox.Show(
                null, messageBoxText, caption, button, icon, MessageBoxResult.None);
        }

        /// <summary>
        /// Displays a message box in front of the specified window that has a message, title
        /// bar caption, button, and icon; and that returns a result.
        /// </summary>
        /// <param name="owner">
        /// A System.Windows.Window that represents the owner window of the message box.
        /// </param>
        /// <param name="messageBoxText">
        /// A System.String that specifies the text to display.
        /// </param>
        /// <param name="caption">
        /// A System.String that specifies the title bar caption to display.
        /// </param>
        /// <param name="button">
        /// A System.Windows.MessageBoxButton value that specifies which button or buttons to
        /// display.
        /// </param>
        /// <param name="icon">
        /// A System.Windows.MessageBoxImage value that specifies the icon to display.
        /// </param>
        /// <returns>
        /// A System.Windows.MessageBoxResult value that specifies which message box button is
        /// clicked by the user.
        /// </returns>
        public static MessageBoxResult Show(
            Window owner,
            string messageBoxText,
            string caption,
            MessageBoxButton button,
            MessageBoxImage icon)
        {
            return RsMessageBox.Show(
                owner, messageBoxText, caption, button, icon, MessageBoxResult.None);
        }

        /// <summary>
        /// Displays a message box that has a message, title bar caption, button, and icon; and
        /// that returns a result.
        /// </summary>
        /// <param name="messageBoxText">
        /// A System.String that specifies the text to display.
        /// </param>
        /// <param name="caption">
        /// A System.String that specifies the title bar caption to display.
        /// </param>
        /// <param name="button">
        /// A System.Windows.MessageBoxButton value that specifies which button or buttons to
        /// display.
        /// </param>
        /// <param name="icon">
        /// A System.Windows.MessageBoxImage value that specifies the icon to display.
        /// </param>
        /// <param name="defaultResult">
        /// A System.Windows.MessageBoxResult value that specifies the default result of the
        /// message box.
        /// </param>
        /// <returns>
        /// A System.Windows.MessageBoxResult value that specifies which message box button is
        /// clicked by the user.
        /// </returns>
        public static MessageBoxResult Show(
            string messageBoxText,
            string caption,
            MessageBoxButton button,
            MessageBoxImage icon,
            MessageBoxResult defaultResult)
        {
            return RsMessageBox.Show(
                null, messageBoxText, caption, button, icon, defaultResult);
        }

        /// <summary>
        /// Displays a message box in front of the specified window that has a message, title
        /// bar caption, button, and icon; and that returns a result.
        /// </summary>
        /// <param name="owner">
        /// A System.Windows.Window that represents the owner window of the message box.
        /// </param>
        /// <param name="messageBoxText">
        /// A System.String that specifies the text to display.
        /// </param>
        /// <param name="caption">
        /// A System.String that specifies the title bar caption to display.
        /// </param>
        /// <param name="button">
        /// A System.Windows.MessageBoxButton value that specifies which button or buttons to
        /// display.
        /// </param>
        /// <param name="icon">
        /// A System.Windows.MessageBoxImage value that specifies the icon to display.
        /// </param>
        /// <param name="defaultResult">
        /// A System.Windows.MessageBoxResult value that specifies the default result of the
        /// message box.
        /// </param>
        /// <returns>
        /// A System.Windows.MessageBoxResult value that specifies which message box button is
        /// clicked by the user.
        /// </returns>
        public static MessageBoxResult Show(
            Window owner,
            string messageBoxText,
            string caption,
            MessageBoxButton button,
            MessageBoxImage icon,
            MessageBoxResult defaultResult)
        {
            RsMessageBox messageBox = new RsMessageBox();
            messageBox.Text = messageBoxText;
            RsApplication app = Application.Current as RsApplication;
            if (!String.IsNullOrWhiteSpace(caption) || app == null)
            {
                messageBox.Caption = caption;
            }
            else
            {
                messageBox.Caption = app.ProductName;
            }

            switch (button)
            {
                case MessageBoxButton.OKCancel:
                    {
                        bool okDefault = defaultResult == MessageBoxResult.OK;
                        bool cancelDefault = defaultResult == MessageBoxResult.Cancel;
                        if (!okDefault && !cancelDefault)
                        {
                            okDefault = true;
                        }

                        messageBox.AddButton(
                            MessageBoxResources.OK,
                            (int)MessageBoxResult.OK,
                            okDefault,
                            false);
                        messageBox.AddButton(
                            MessageBoxResources.Cancel,
                            (int)MessageBoxResult.Cancel,
                            cancelDefault,
                            true);
                    }

                    break;
                case MessageBoxButton.YesNo:
                    {
                        bool yesDefault = defaultResult == MessageBoxResult.Yes;
                        bool noDefault = defaultResult == MessageBoxResult.No;
                        if (!yesDefault && !noDefault)
                        {
                            yesDefault = true;
                        }

                        messageBox.AddButton(
                            MessageBoxResources.Yes,
                            (int)MessageBoxResult.Yes,
                            yesDefault,
                            false);
                        messageBox.AddButton(
                            MessageBoxResources.No,
                            (int)MessageBoxResult.No,
                            noDefault,
                            false);
                    }

                    break;
                case MessageBoxButton.YesNoCancel:
                    {
                        bool yesDefault = defaultResult == MessageBoxResult.Yes;
                        bool noDefault = defaultResult == MessageBoxResult.No;
                        bool cancelDefault = defaultResult == MessageBoxResult.Cancel;
                        if (!yesDefault && !noDefault && !cancelDefault)
                        {
                            yesDefault = true;
                        }

                        messageBox.AddButton(
                            MessageBoxResources.Yes,
                            (int)MessageBoxResult.Yes,
                            yesDefault,
                            false);
                        messageBox.AddButton(
                            MessageBoxResources.No,
                            (int)MessageBoxResult.No,
                            noDefault,
                            false);
                        messageBox.AddButton(
                            MessageBoxResources.Cancel,
                            (int)MessageBoxResult.Cancel,
                            cancelDefault,
                            true);
                    }

                    break;
                case MessageBoxButton.OK:
                default:
                    messageBox.AddButton(
                        MessageBoxResources.OK, (int)MessageBoxResult.OK, true, false);
                    break;
            }

            messageBox.SetSystemIcon(icon);
            long result = messageBox.ShowMessageBox(owner);
            return (MessageBoxResult)result;
        }

        /// <summary>
        /// Adds a button to the message box for the user to interact with with the specified
        /// content as the text and the specified value as the value that is returned to the
        /// client if the user selects this value.
        /// </summary>
        /// <param name="content">
        /// The text that should appear on the button.
        /// </param>
        /// <param name="value">
        /// The value that is set as the selected value if the user closes the window by
        /// pressing this button.
        /// </param>
        /// <param name="isDefault">
        /// A value indicating whether this is the default button for the user to press.
        /// </param>
        /// <param name="isCancel">
        /// A value indicating whether this is the cancel button for the window and the button
        /// that will be executed when the user presses escape.
        /// </param>
        public void AddButton(string content, long value, bool isDefault, bool isCancel)
        {
            if (isDefault)
            {
                foreach (CustomMessageBoxButton customButton in this._buttons)
                {
                    customButton.IsDefault = false;
                }
            }

            if (isCancel)
            {
                foreach (CustomMessageBoxButton customButton in this._buttons)
                {
                    customButton.IsCancel = false;
                }
            }

            CustomMessageBoxButton newButton = new CustomMessageBoxButton(content, value);
            newButton.IsCancel = isCancel;
            newButton.IsDefault = isDefault;
            this._buttons.Add(newButton);
        }

        /// <summary>
        /// Sets the icon image to the systems exclamation icon.
        /// </summary>
        public void SetIconToExclamation()
        {
            this.SetImageSourceToHIcon(SystemIcons.Warning.Handle);
        }

        /// <summary>
        /// Sets a system message box icon.
        /// </summary>
        /// <param name="icon">
        /// A value indicating the system message box icon to show.
        /// </param>
        public void SetSystemIcon(MessageBoxImage icon)
        {
            switch (icon)
            {
                case MessageBoxImage.Asterisk: // MessageBoxImage.Information:
                    this.SetImageSourceToHIcon(SystemIcons.Asterisk.Handle);
                    break;
                case MessageBoxImage.Error: // MessageBoxImage.Hand, MessageBoxImage.Stop:
                    this.SetImageSourceToHIcon(SystemIcons.Error.Handle);
                    break;
                case MessageBoxImage.Exclamation: // MessageBoxImage.Warning:
                    this.SetImageSourceToHIcon(SystemIcons.Exclamation.Handle);
                    break;
                case MessageBoxImage.Question:
                    this.SetImageSourceToHIcon(SystemIcons.Question.Handle);
                    break;
                case MessageBoxImage.None:
                    break;
                default:
                    Debug.Fail("Unsupported MessageBoxIcon.  Ignored.");
                    throw (new ArgumentOutOfRangeException("icon"));
            }
        }

        /// <summary>
        /// Displays this message box as it has been setup and returns a result.
        /// </summary>
        /// <returns>
        /// A long value that specifies which message box button is clicked by the user.
        /// </returns>
        public long ShowMessageBox()
        {
            return this.ShowMessageBox(this._owner);
        }

        /// <summary>
        /// Displays this message box as it has been setup and returns a result.
        /// </summary>
        /// <param name="owner">
        /// A System.Windows.Window that represents the owner window of the message box.
        /// </param>
        /// <returns>
        /// A long value that specifies which message box button is clicked by the user.
        /// </returns>
        public long ShowMessageBox(Window owner)
        {
            RsApplication app = Application.Current as RsApplication;
            if (String.IsNullOrWhiteSpace(this.Caption) && app != null)
            {
                this.Caption = app.ProductName;
            }

            RsMessageBoxWindow window = new RsMessageBoxWindow(owner, this);
            CustomMessageBoxButton result = window.ShowMessageBox();
            return result.Value;
        }

        /// <summary>
        /// Set the message box ImageSource to a icon handle.
        /// </summary>
        /// <param name="hIcon"></param>
        private void SetImageSourceToHIcon(IntPtr hIcon)
        {
            this.ImageSource = Imaging.CreateBitmapSourceFromHIcon(
                hIcon,
                Int32Rect.Empty,
                BitmapSizeOptions.FromEmptyOptions());
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsMessageBox {Class}
} // RSG.Editor.Controls {Namespace}
