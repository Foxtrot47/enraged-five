﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using RSG.Base;
using RSG.Editor;
using RSG.Rag.Widgets;

namespace RSG.Rag.ViewModel.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    public class DataViewModel : WidgetViewModel<WidgetData>
    {
        #region Fields
        /// <summary>
        /// 
        /// </summary>
        private byte[] _data;

        /// <summary>
        /// 
        /// </summary>
        private String _dataAsText;

        /// <summary>
        /// 
        /// </summary>
        private ICommand _sendCommand;
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        public DataViewModel(WidgetData widget)
            : base(widget)
        {
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public byte[] Data
        {
            get { return _data; }
            set
            {
                if (SetProperty(ref _data, value, "Data", "IsModified"))
                {
                    DataAsText = new String(_data.Select(item => Convert.ToChar(item)).ToArray());
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public String DataAsText
        {
            get { return _dataAsText; }
            set
            {
                if (SetProperty(ref _dataAsText, value, "DataAsText"))
                {
                    Data = _dataAsText.Select(item => Convert.ToByte(item)).ToArray();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ushort MaxBufferSize
        {
            get { return Widget.MaxBufferSize; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override bool IsModified
        {
            get { return StructuralEqualityComparer<byte[]>.Default.Equals(Widget.Data, _data); }
        }
        #endregion // Properties

        #region Commands
        /// <summary>
        /// 
        /// </summary>
        public ICommand SendCommand
        {
            get
            {
                if (_sendCommand == null)
                {
                    _sendCommand = new RelayCommand(param => Send(param), param => CanSend(param));
                }
                return _sendCommand;
            }
        }
        #endregion // Commands

        #region Command Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool CanSend(Object parameter)
        {
            return !ReadOnly;
        }

        /// <summary>
        /// Called to press the button.
        /// </summary>
        public void Send(Object parameter)
        {
            Widget.Data = _data;
        }
        #endregion // Command Methods
        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        protected override void OnWidgetPropertyChanged(String propertyName)
        {
            base.OnWidgetPropertyChanged(propertyName);
            if (propertyName == "Data")
            {
                Data = Widget.Data;
            }
        }
        #endregion // Event Handlers
    } // DataViewModel
}
