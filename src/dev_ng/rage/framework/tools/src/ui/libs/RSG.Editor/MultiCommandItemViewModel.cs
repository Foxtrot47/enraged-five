﻿//---------------------------------------------------------------------------------------------
// <copyright file="MultiCommandItemViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System.ComponentModel;
    using System.Windows.Input;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// The view model class that wraps a single <see cref="IMultiCommandItem"/> object to be
    /// used by the view.
    /// </summary>
    public class MultiCommandItemViewModel : NotifyPropertyChangedBase, IMultiCommandItem
    {
        #region Fields
        /// <summary>
        /// The private <see cref="IMultiCommandItem"/> object this view model is wrapping.
        /// </summary>
        private IMultiCommandItem _item;

        /// <summary>
        /// The parent view model for this object.
        /// </summary>
        private CommandBarItemViewModel _parent;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MultiCommandItemViewModel"/> class.
        /// </summary>
        /// <param name="item">
        /// The <see cref="IMultiCommandItem"/> object this view model is wrapping.
        /// </param>
        /// <param name="parent">
        /// The parent view model.
        /// </param>
        public MultiCommandItemViewModel(
            IMultiCommandItem item, CommandBarItemViewModel parent)
        {
            this._item = item;
            this._parent = parent;
            if (item != null)
            {
                item.PropertyChanged += this.ItemPropertyChanged;
            }
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the generic object that is sent with the command if this item is the selected
        /// item in the combo control or toggled in the filter control.
        /// </summary>
        public object CommandParameter
        {
            get { return this._item.GetCommandParameter(); }
        }

        /// <summary>
        /// Gets the description for this item. This is also what is shown as a tooltip along
        /// with the key gesture if one is set if it is being rendered as a toggle control.
        /// </summary>
        public string Description
        {
            get { return this._item.Description; }
        }

        /// <summary>
        /// Gets the System.Windows.Media.Imaging.BitmapSource object that represents the icon
        /// that is used for this item if it is being rendered as a toggle control.
        /// </summary>
        public BitmapSource Icon
        {
            get { return this._item.Icon; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this item is currently the selected item on
        /// the owning command definition.
        /// </summary>
        public bool IsToggled
        {
            get { return this._item.IsToggled; }
            set { this._item.IsToggled = value; }
        }

        /// <summary>
        /// Gets or sets the System.Windows.Input.KeyGesture object that is associated with
        /// this combo command item.
        /// </summary>
        public KeyGesture KeyGesture
        {
            get { return this._item.KeyGesture; }
            set { this._item.KeyGesture = value; }
        }

        /// <summary>
        /// Gets the model for this view model.
        /// </summary>
        public IMultiCommandItem Model
        {
            get { return this._item; }
        }

        /// <summary>
        /// Gets the parent command definition that this is an item for.
        /// </summary>
        public MultiCommand ParentDefinition
        {
            get { return this._item.ParentDefinition; }
        }

        /// <summary>
        /// Gets or sets the text that is used to display this item inside the combo control.
        /// </summary>
        public string Text
        {
            get { return this._item.Text; }
            set { this._item.Text = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Gets the command parameter used for this multi command item.
        /// </summary>
        /// <returns>
        /// The command parameter used for this multi command item.
        /// </returns>
        public IMultiCommandParameter GetCommandParameter()
        {
            return this._item.GetCommandParameter();
        }

        /// <summary>
        /// Sets the <see cref="IsToggled"/> property from an internal operation.
        /// </summary>
        /// <param name="isToggled">
        /// A value to set the <see cref="IsToggled"/> property to.
        /// </param>
        /// <param name="handleSelectionOnParent">
        /// A value indicating whether the toggle change should result in it being handled in
        /// the parent command definition.
        /// </param>
        public void SetIsToggled(bool isToggled, bool handleSelectionOnParent)
        {
            this._item.SetIsToggled(isToggled, handleSelectionOnParent);
        }

        /// <summary>
        /// Called whenever a property value changes inside the associated multi command item.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.ComponentModel.PropertyChangedEventArgs data for this event.
        /// </param>
        private void ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsToggled")
            {
                this._parent.NotifyPropertyChanged("FilterCount");
            }

            this.NotifyPropertyChanged(e.PropertyName);
        }
        #endregion Methods
    } // RSG.Editor.MultiCommandItemViewModel {Class}
} // RSG.Editor {Namespace}
