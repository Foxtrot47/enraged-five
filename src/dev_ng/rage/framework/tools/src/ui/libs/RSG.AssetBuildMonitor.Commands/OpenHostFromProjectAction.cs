﻿//---------------------------------------------------------------------------------------------
// <copyright file="OpenHostFromProjectAction.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.AssetBuildMonitor.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using RSG.Base.Extensions;
    using RSG.AssetBuildMonitor.ViewModel;
    using RSG.AssetBuildMonitor.ViewModel.Project;
    using RSG.Editor;
    using RSG.Editor.Controls.Dock.ViewModel;
    using RSG.Project.Commands;
    using RSG.Project.ViewModel;

    /// <summary>
    /// Contains the logic for the <see cref="Commands.ConnectToHost"/> routed
    /// command. This class cannot be inherited.
    /// </summary>
    public sealed class OpenHostFromProjectAction : ButtonAction<ProjectCommandArgs>
    {
        #region Delegates
        /// <summary>
        /// Delegate for determine whether we can open a host.
        /// </summary>
        /// <param name="host"></param>
        /// <returns></returns>
        public delegate bool CanOpenHostCallback(HostNode host);

        /// <summary>
        /// Delegate for Host Opened callback.
        /// </summary>
        /// <param name="host"></param>
        public delegate void HostOpenedCallback(HostNode host);
        #endregion // Delegates

        #region Member Data
        private CanOpenHostCallback _canOpenHostCallback;
        private HostOpenedCallback _hostOpenedCallback;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="ConnectToHostAction"/> class.
        /// </summary>
        /// <param name="serviceProvider">
        /// The service provider that is used by this action to perform application specific
        /// operations, for example selecting a file to open.
        /// </param>
        public OpenHostFromProjectAction(CanOpenHostCallback canOpenCb, HostOpenedCallback openedCb,
            ICommandServiceProvider serviceProvider)
            : base(serviceProvider)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ConnectToHostAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public OpenHostFromProjectAction(CanOpenHostCallback canOpenCb, HostOpenedCallback openedCb, 
            ProjectCommandResolver resolver)
            : base(new ParameterResolverDelegate<ProjectCommandArgs>(resolver))
        {
            this._canOpenHostCallback = canOpenCb;
            this._hostOpenedCallback = openedCb;
        }
        #endregion // Constructor(s)

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ProjectCommandArgs args)
        {
            if (null == args || args.SelectedNodes.None())
                return (false);
            return (true);
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(ProjectCommandArgs args)
        {
            IMessageBoxService messageService = this.GetService<IMessageBoxService>();
            if (messageService == null)
            {
                Debug.Fail("Unable to open as services are missing.");
                return;
            }

            // Loop through all selected hosts and attempt to open them.
            foreach (HostNode host in args.SelectedNodes)
            {
                if ((null != _canOpenHostCallback) && this._canOpenHostCallback(host))
                    continue; // Skip host.
                
                DocumentPane pane = args.ViewSite.LastActiveDocumentPane;
                if (pane == null)
                {
                    pane = args.ViewSite.FirstDocumentPane;
                    if (pane == null)
                    {
                        Debug.Fail("Cannot find the pane to open the document in.");
                        return;
                    }
                }

                try
                {
                    DocumentItem item = null;
                    ICreatesProjectDocument documentCreator = (host as ICreatesProjectDocument);
                    if (null == documentCreator)
                    {
                        item = new HostDocumentItem(pane, host.Text);
                    }
                    else
                    {
                        item = documentCreator.CreateDocument(pane, host.Text);
                    }

                    ICreatesDocumentContent contentCreator = (host as ICreatesDocumentContent);
                    AssetBuildHistoryViewModel vm = (AssetBuildHistoryViewModel)contentCreator.CreateDocumentContent(item.UndoEngine);
                    if (vm.IsConnected)
                    {
                        item.Content = vm;
                        item.IsSelected = true;
                        pane.InsertChild(pane.Children.Count, item);

                        // Invoke callback.
                        if (null != this._hostOpenedCallback)
                            this._hostOpenedCallback(host);
                    }
                    else
                    {
                        messageService.ShowStandardErrorBox(
                            String.Format(Resources.CommandStringTable.ConnectToHostError, host.Text),
                            "Error");
                    }
                }
                catch (Exception)
                {
                    messageService.ShowStandardErrorBox(
                        String.Format(Resources.CommandStringTable.ConnectToHostError, host.Text),
                        "Error");
                }
            }
        }
        #endregion // Methods
    }

} // RSG.AssetBuildMonitor.Commands namespace
