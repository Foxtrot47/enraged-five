﻿using RSG.Rag.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.ViewModel.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    public class SliderUInt32ViewModel : SliderViewModel<UInt32>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        public SliderUInt32ViewModel(WidgetSlider<UInt32> widget)
            : base(widget)
        {
        }
        #endregion // Constructor(s)
    } // SliderUInt32ViewModel
}
