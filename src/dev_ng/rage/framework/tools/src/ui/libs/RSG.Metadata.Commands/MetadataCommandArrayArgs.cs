﻿//---------------------------------------------------------------------------------------------
// <copyright file="MetadataCommandArrayArgs.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Commands
{
    using RSG.Metadata.Model.Definitions;
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// Represents the secondary argument class that is used by metadata commands that are
    /// making changes to a targeted array.
    /// </summary>
    public class MetadataCommandArrayArgs
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="DynamicParent"/> property.
        /// </summary>
        private IDynamicTunableParent _dynamicParent;

        /// <summary>
        /// The private field used for the <see cref="Index"/> property.
        /// </summary>
        private int _index;

        /// <summary>
        /// The private field used for the <see cref="Structure"/> property.
        /// </summary>
        private IStructure _structure;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MetadataCommandArrayArgs"/> class.
        /// </summary>
        /// <param name="dynamicParent">
        /// The dynamic parent being manipulated.
        /// </param>
        /// <param name="index">
        /// The main index where the manipulation is taking place.
        /// </param>
        /// <param name="structure">
        /// An optional structure that is used to identify the type of item in an array of
        /// structures/pointers.
        /// </param>
        public MetadataCommandArrayArgs(
            IDynamicTunableParent dynamicParent, int index, IStructure structure)
        {
            this._dynamicParent = dynamicParent;
            this._index = index;
            this._structure = structure;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the dynamic parent that is being manipulated.
        /// </summary>
        public IDynamicTunableParent DynamicParent
        {
            get { return this._dynamicParent; }
        }

        /// <summary>
        /// Gets the main index where the manipulation is taking place.
        /// </summary>
        public int Index
        {
            get { return this._index; }
        }

        /// <summary>
        /// Gets an optional structure object that is used to identify the type of item in an
        /// array of structures/pointers.
        /// </summary>
        public IStructure Structure
        {
            get { return this._structure; }
        }
        #endregion Properties
    } // RSG.Metadata.Commands.MetadataCommandArrayArgs {Class}
} // RSG.Metadata.Commands {Namespace}
