﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProjectLoadFailedEventArgs.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel
{
    using System;

    /// <summary>
    /// Provides data for the event representing a project node failing to load due to a
    /// exception.
    /// </summary>
    public class ProjectLoadFailedEventArgs : EventArgs
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Exception"/> property.
        /// </summary>
        private Exception _exception;

        /// <summary>
        /// The private field used for the <see cref="Project"/> property.
        /// </summary>
        private ProjectNode _project;

        /// <summary>
        /// The private field used for the <see cref="Reloaded"/> property.
        /// </summary>
        private bool _reloaded;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ProjectLoadFailedEventArgs"/> class.
        /// </summary>
        /// <param name="project">
        /// The project node that failed to load.
        /// </param>
        /// <param name="exception">
        /// The exception that occurred during the load.
        /// </param>
        /// <param name="reloaded">
        /// A value indicating whether the fail has come from the initial load or a user
        /// executed reload.
        /// </param>
        public ProjectLoadFailedEventArgs(
            ProjectNode project, Exception exception, bool reloaded)
        {
            this._project = project;
            this._exception = exception;
            this._reloaded = reloaded;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the exception that occurred during the load.
        /// </summary>
        public Exception Exception
        {
            get { return this._exception; }
        }

        /// <summary>
        /// Gets the project node that failed to load.
        /// </summary>
        public ProjectNode Project
        {
            get { return this._project; }
        }

        /// <summary>
        /// Gets a value indicating whether this fail has come from a initial load or a user
        /// executed reload.
        /// </summary>
        public bool Reloaded
        {
            get { return this._reloaded; }
        }
        #endregion Properties
    } // RSG.Project.ViewModel.ProjectLoadFailedEventArgs {Class}
} // RSG.Project.ViewModel {Namespace}
