﻿//---------------------------------------------------------------------------------------------
// <copyright file="ClassViewEnumerationNode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.ClassView
{
    using RSG.Metadata.Model.Definitions;
    using RSG.Metadata.ViewModel.Project;

    /// <summary>
    /// The node inside the class view that represents a standard metadata project. This class
    /// cannot be inherited.
    /// </summary>
    public sealed class ClassViewEnumerationNode : ClassViewNodeBase
    {
        #region Fields
        /// <summary>
        /// The enumeration that this node is representing.
        /// </summary>
        private IEnumeration _enumeration;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ClassViewEnumerationNode"/> class.
        /// </summary>
        /// <param name="enumeration">
        /// The enumeration object this node is representing.
        /// </param>
        /// <param name="parent">
        /// The parent node.
        /// </param>
        public ClassViewEnumerationNode(IEnumeration enumeration, ClassViewNodeBase parent)
            : base(parent)
        {
            this.IsExpandable = false;
            this._enumeration = enumeration;
            this.Icon = Icons.EnumNodeIcon;
            this.Text = enumeration.ShortDataType;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the priority value for this object.
        /// </summary>
        public override int Priority
        {
            get { return 2; }
        }
        #endregion Properties
    } // RSG.Metadata.ViewModel.ClassView.ClassViewEnumerationNode {Class}
} // RSG.Metadata.ViewModel.ClassView {Namespace}
