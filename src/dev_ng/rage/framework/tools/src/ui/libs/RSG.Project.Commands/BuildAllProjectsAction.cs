﻿//---------------------------------------------------------------------------------------------
// <copyright file="BuildAllProjectsAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Commands
{
    using System.Collections.Generic;
    using RSG.Editor;
    using RSG.Project.Model;
    using RSG.Project.ViewModel;

    /// <summary>
    /// Contains the logic for the <see cref="ProjectCommands.BuildAllProjects"/> routed
    /// command. This class cannot be inherited.
    /// </summary>
    public sealed class BuildAllProjectsAction : BuildActionBase
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BuildAllProjectsAction"/> class.
        /// </summary>
        /// <param name="serviceProvider">
        /// The service provider that is used by this action to perform application specific
        /// operations, for example selecting a file to open.
        /// </param>
        public BuildAllProjectsAction(ICommandServiceProvider serviceProvider)
            : base(serviceProvider)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="BuildAllProjectsAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public BuildAllProjectsAction(BuildProjectCommandResolver resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(BuildProjectCommandArgs args)
        {
            if (args == null || args.ProjectBuilder == null || args.CollectionNode == null)
            {
                return false;
            }

            if (this.AllProjects(args).Count == 0)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(BuildProjectCommandArgs args)
        {
            this.Build(this.AllProjects(args), args);
        }

        /// <summary>
        /// Retrieves all of the loaded projects from the specified argument object.
        /// </summary>
        /// <param name="args">
        /// The argument object used to retrieve the projects.
        /// </param>
        /// <returns>
        /// The collection containing all of the loaded projects.
        /// </returns>
        private List<Project> AllProjects(ProjectCommandArgs args)
        {
            List<Project> projects = new List<Project>();
            if (args.CollectionNode == null)
            {
                return projects;
            }

            foreach (ProjectNode projectNode in args.CollectionNode.GetProjects())
            {
                if (projectNode == null)
                {
                    continue;
                }

                Project project = projectNode.Model as Project;
                if (project != null && !projects.Contains(project))
                {
                    projects.Add(project);
                }
            }

            return projects;
        }
        #endregion Methods
    } // RSG.Project.Commands.BuildAllProjectsAction {Class}
} // RSG.Project.Commands {Namespace}
