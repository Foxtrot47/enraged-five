﻿//---------------------------------------------------------------------------------------------
// <copyright file="InputSource.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    /// <summary>
    /// Defines the different places/devices that can supply input.
    /// </summary>
    public enum InputSource
    {
        /// <summary>
        /// There is no specific input source defined.
        /// </summary>
        None,

        /// <summary>
        /// The input source has come directly from a mouse device.
        /// </summary>
        Mouse,

        /// <summary>
        /// The input source has come directly from a keyboard device.
        /// </summary>
        Keyboard
    } // RSG.Editor.View.InputSource {Enum}
} // RSG.Editor.View {Namespace}
