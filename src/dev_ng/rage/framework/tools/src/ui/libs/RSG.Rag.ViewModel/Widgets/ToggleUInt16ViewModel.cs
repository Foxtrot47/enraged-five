﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Rag.Widgets;

namespace RSG.Rag.ViewModel.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    public class ToggleUInt16ViewModel : ToggleViewModel<UInt16>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        public ToggleUInt16ViewModel(WidgetToggle<UInt16> widget)
            : base(widget)
        {
        }
        #endregion // Constructor(s)
    } // ToggleUInt16ViewModel
}
