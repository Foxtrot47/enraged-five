﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Automation.ViewModel;
using RSG.Editor;

namespace RSG.Automation.Commands
{
    public class UseUTCAction : ToggleButtonAction<AutomationMonitorDataContext>  
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="FilterByJobStateAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public UseUTCAction(ParameterResolverDelegate<AutomationMonitorDataContext> resolver)
            : base(resolver)
        {
        }
        #endregion // Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="control">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(AutomationMonitorDataContext dc)
        {
            dc.UseUTC = IsToggled;
            foreach (JobViewModel jvm in dc.Jobs)
            {
                jvm.UseUTC = IsToggled;
            }

            dc.RefreshAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        protected override bool GetInitialToggleState(RockstarRoutedCommand command)
        {
            return false;
        }
        #endregion // Methods

        #region Private Methods

        #endregion // Private Methods
    } // UseUTCAction
}
