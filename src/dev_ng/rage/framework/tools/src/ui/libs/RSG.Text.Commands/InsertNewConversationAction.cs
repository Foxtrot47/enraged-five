﻿//---------------------------------------------------------------------------------------------
// <copyright file="InsertNewConversationAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Commands
{
    using RSG.Editor;
    using RSG.Editor.Model;
    using RSG.Text.Model;
    using RSG.Text.ViewModel;

    /// <summary>
    /// Implements the <see cref="TextCommands.InsertNewConversation"/> command.
    /// </summary>
    public class InsertNewConversationAction : ButtonAction<TextConversationCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="InsertNewConversationAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public InsertNewConversationAction(
            ParameterResolverDelegate<TextConversationCommandArgs> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(TextConversationCommandArgs args)
        {
            return args.Dialogue != null;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(TextConversationCommandArgs args)
        {
            int index = args.DataGrid.SelectedIndex;
            using (new UndoRedoBatch(args.Dialogue.Model.UndoEngine))
            {
                Conversation conversation = args.Dialogue.Model.AddNewConversationAt(index);
                foreach (ConversationViewModel viewModel in args.Dialogue.Conversations)
                {
                    if (object.ReferenceEquals(viewModel.Model, conversation))
                    {
                        viewModel.Model.AddNewLine();
                        break;
                    }
                }
            }
        }
        #endregion Methods
    } // RSG.Text.Commands.InsertNewConversationAction {Class}
} // RSG.Text.Commands {Namespace}
