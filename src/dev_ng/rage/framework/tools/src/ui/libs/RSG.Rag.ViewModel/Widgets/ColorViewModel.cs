﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Rag.Widgets;

namespace RSG.Rag.ViewModel.Widgets
{
    /// <summary>
    /// 
    /// </summary>
    public class ColorViewModel : WidgetViewModel<WidgetColor>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        public ColorViewModel(WidgetColor widget)
            : base(widget)
        {
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public int Red
        {
            get { return (int)Math.Round(Widget.Red * 255.0f, MidpointRounding.AwayFromZero); }
            set { Widget.Red = (value / 255.0f); }
        }

        /// <summary>
        /// 
        /// </summary>
        public float Green
        {
            get { return (int)Math.Round(Widget.Green * 255.0f, MidpointRounding.AwayFromZero); }
            set { Widget.Green = (value / 255.0f); }
        }

        /// <summary>
        /// 
        /// </summary>
        public float Blue
        {
            get { return (int)Math.Round(Widget.Blue * 255.0f, MidpointRounding.AwayFromZero); }
            set { Widget.Blue = (value / 255.0f); }
        }

        /// <summary>
        /// 
        /// </summary>
        public float Alpha
        {
            get { return (int)Math.Round(Widget.Alpha * 255.0f, MidpointRounding.AwayFromZero); }
            set { Widget.Alpha = (value / 255.0f); }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool ShowAlpha
        {
            get { return Widget.Type != WidgetColor.ColorType.Vector3; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override bool IsModified
        {
            get
            {
                return (Widget.Red != Widget.DefaultRed ||
                        Widget.Green != Widget.DefaultGreen ||
                        Widget.Blue != Widget.DefaultBlue ||
                        Widget.Alpha != Widget.DefaultAlpha);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override string DefaultValue
        {
            get
            {
                int red = (int)Math.Round(Widget.DefaultRed * 255.0f, MidpointRounding.AwayFromZero);
                int green = (int)Math.Round(Widget.DefaultGreen * 255.0f, MidpointRounding.AwayFromZero);
                int blue = (int)Math.Round(Widget.DefaultBlue * 255.0f, MidpointRounding.AwayFromZero);

                if (ShowAlpha)
                {
                    int alpha = (int)Math.Round(Widget.DefaultAlpha * 255.0f, MidpointRounding.AwayFromZero);
                    return String.Format("R:{0} G:{1} B:{2} A:{3}", red, green, blue, alpha);
                }
                else
                {
                    return String.Format("R:{0} G:{1} B:{2}", red, green, blue);
                }
            }
        }
        #endregion // Properties

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        protected override void OnWidgetPropertyChanged(String propertyName)
        {
            base.OnWidgetPropertyChanged(propertyName);
            if (propertyName == "Red" ||
                propertyName == "Green" ||
                propertyName == "Blue" ||
                propertyName == "Alpha")
            {
                NotifyPropertyChanged("IsModified");
            }
        }
        #endregion // Event Handlers
    } // ColorViewModel
}
