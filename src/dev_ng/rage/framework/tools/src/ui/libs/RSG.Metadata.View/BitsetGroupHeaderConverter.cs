﻿//---------------------------------------------------------------------------------------------
// <copyright file="BitsetGroupHeaderConverter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.View
{
    using System;
    using System.Globalization;
    using RSG.Editor.Controls.Converters;

    /// <summary>
    /// Converters a group header string for the bitset item list to a user friendly name.
    /// </summary>
    internal class BitsetGroupHeaderConverter : ValueConverter<bool, string>
    {
        #region Methods
        /// <summary>
        /// Converters the given value and returns the result.
        /// </summary>
        /// <param name="value">
        /// The original value to convert.
        /// </param>
        /// <param name="param">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A converted value.
        /// </returns>
        protected override string Convert(bool value, object param, CultureInfo culture)
        {
            return value ? "Checked" : "Not Checked";
        }
        #endregion Methods
    } // RSG.Metadata.View.BitsetGroupHeaderConverter {Class}
} // RSG.Metadata.View {Namespace}
