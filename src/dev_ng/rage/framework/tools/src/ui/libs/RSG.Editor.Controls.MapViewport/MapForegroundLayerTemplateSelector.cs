﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace RSG.Editor.Controls.MapViewport
{
    /// <summary>
    /// Data template selector for the map foreground's layer popup which differentiates
    /// between <see cref="MapLayer"/> and <see cref="Layer"/> objects.
    /// </summary>
    public class MapForegroundLayerTemplateSelector : DataTemplateSelector
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="LayerTemplate"/> property.
        /// </summary>
        private DataTemplate _layerTemplate;
        
        /// <summary>
        /// Private field for the <see cref="MapLayerTemplate"/> property.
        /// </summary>
        private DataTemplate _mapLayerTemplate;
        #endregion

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public DataTemplate LayerTemplate
        {
            get { return _layerTemplate; }
            set { _layerTemplate = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DataTemplate MapLayerTemplate
        {
            get { return _mapLayerTemplate; }
            set { _mapLayerTemplate = value; }
        }
        #endregion

        #region DataTemplateSelector Methods
        /// <summary>
        /// Returns the data template that should be used for the specified item.
        /// </summary>
        /// <param name="item">The data object for which to select the template.</param>
        /// <param name="container">The data-bound object.</param>
        /// <returns>
        /// Returns a <see cref="DataTemplate"/> or null. The default value is null.
        /// </returns>
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            return (item is MapLayer ? MapLayerTemplate : LayerTemplate);
        }
        #endregion
    }
}
