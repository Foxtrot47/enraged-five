﻿//---------------------------------------------------------------------------------------------
// <copyright file="MultiCommandSelectionMode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    /// <summary>
    /// Defines the different selection modes a <see cref="MultiCommand"/> instance can
    /// adopt.
    /// </summary>
    public enum MultiCommandSelectionMode
    {
        /// <summary>
        /// Indicates that only a single item can be selected at once. Used for combo
        /// commands.
        /// </summary>
        Single,

        /// <summary>
        /// Indicates that one or more items can be selected at the same time. Used for
        /// filter commands.
        /// </summary>
        Multiple,
    } // RSG.Editor.MultiCommandSelectionMode {Enum}
} // RSG.Editor {Namespace}
