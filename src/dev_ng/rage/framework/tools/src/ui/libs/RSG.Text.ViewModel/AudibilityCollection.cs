﻿//---------------------------------------------------------------------------------------------
// <copyright file="AudibilityCollection.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.ViewModel
{
    using RSG.Editor.Model;
    using RSG.Editor.View;
    using RSG.Text.Model;

    /// <summary>
    /// Provides a collection class that contains <see cref="DialogueAudibilityViewModel"/>
    /// objects that are kept in sync with a collection of <see cref="DialogueAudibility"/>
    /// objects.
    /// </summary>
    public class AudibilityCollection
        : CollectionConverter<DialogueAudibility, DialogueAudibilityViewModel>,
        IReadOnlyViewModelCollection<DialogueAudibilityViewModel>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="AudibilityCollection"/> class.
        /// </summary>
        /// <param name="source">
        /// The model collection containing the source <see cref="DialogueAudibility"/> objects
        /// for this collection.
        /// </param>
        public AudibilityCollection(ModelCollection<DialogueAudibility> source)
            : base(source)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Converts a single <see cref="DialogueAudibility"/> object to a new
        /// <see cref="DialogueAudibilityViewModel"/> object.
        /// </summary>
        /// <param name="item">
        /// The item to convert.
        /// </param>
        /// <returns>
        /// A new <see cref="DialogueAudibilityViewModel"/> object from the given
        /// <see cref="DialogueAudibility"/> object.
        /// </returns>
        protected override DialogueAudibilityViewModel ConvertItem(DialogueAudibility item)
        {
            DialogueAudibilityViewModel newViewModel = new DialogueAudibilityViewModel(item);
            return newViewModel;
        }
        #endregion Methods
    } // RSG.Text.ViewModel.AudibilityCollection {Class}
} // RSG.Text.ViewModel {Namespace}
