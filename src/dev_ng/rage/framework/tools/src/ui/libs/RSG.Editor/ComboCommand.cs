﻿//---------------------------------------------------------------------------------------------
// <copyright file="ComboCommand.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    /// <summary>
    /// Provides a base class to a definition of a combo command.
    /// </summary>
    public abstract class ComboCommand : MultiCommand
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ComboCommand"/> class.
        /// </summary>
        protected ComboCommand()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether the individual items are selectable.
        /// </summary>
        public override bool CanItemsBeSelected
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating the selection mode style adopted by this instance.
        /// </summary>
        public override MultiCommandSelectionMode SelectionMode
        {
            get { return MultiCommandSelectionMode.Single; }
        }
        #endregion Properties
    } // RSG.Editor.ComboCommand {Class}
} // RSG.Editor {Namespace}
