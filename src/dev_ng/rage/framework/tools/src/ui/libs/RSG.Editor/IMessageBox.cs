﻿//---------------------------------------------------------------------------------------------
// <copyright file="IMessageBox.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// When implemented represents a message box that can be modified and shown to the user.
    /// </summary>
    public interface IMessageBox
    {
        #region Properties
        /// <summary>
        /// Gets or sets the string that specifies the title bar caption to display.
        /// </summary>
        string Caption { get; set; }

        /// <summary>
        /// Gets or sets the url string that is shown when the user chooses to view help. The
        /// help button is only shown if this is set to non empty/null string.
        /// </summary>
        string HelpUrl { get; set; }

        /// <summary>
        /// Gets or sets the image source of the 32x32 icon that should be displayed.
        /// </summary>
        ImageSource ImageSource { get; set; }

        /// <summary>
        /// Gets or sets the string that specifies the text to display.
        /// </summary>
        string Text { get; set; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Adds a button to the message box for the user to interact with with the specified
        /// content as the text and the specified value as the value that is returned to the
        /// client if the user selects this value.
        /// </summary>
        /// <param name="content">
        /// The text that should appear on the button.
        /// </param>
        /// <param name="value">
        /// The value that is set as the selected value if the user closes the window by
        /// pressing this button.
        /// </param>
        /// <param name="isDefault">
        /// A value indicating whether this is the default button for the user to press.
        /// </param>
        /// <param name="isCancel">
        /// A value indicating whether this is the cancel button for the window and the button
        /// that will be executed when the user presses escape.
        /// </param>
        void AddButton(string content, long value, bool isDefault, bool isCancel);

        /// <summary>
        /// Sets the icon image to the systems exclamation icon.
        /// </summary>
        void SetIconToExclamation();
        
        /// <summary>
        /// Sets a system message box icon.
        /// </summary>
        /// <param name="icon">
        /// A value indicating the system message box icon to show.
        /// </param>
        void SetSystemIcon(MessageBoxImage icon);

        /// <summary>
        /// Displays this message box as it has been setup and returns a result.
        /// </summary>
        /// <returns>
        /// A long value that specifies which message box button is clicked by the user.
        /// </returns>
        long ShowMessageBox();

        /// <summary>
        /// Displays this message box as it has been setup and returns a result.
        /// </summary>
        /// <param name="owner">
        /// A System.Windows.Window that represents the owner window of the message box.
        /// </param>
        /// <returns>
        /// A long value that specifies which message box button is clicked by the user.
        /// </returns>
        long ShowMessageBox(Window owner);
        #endregion Methods
    } // RSG.Editor.IMessageBox {Class}
} // RSG.Editor {Namespace}
