﻿//---------------------------------------------------------------------------------------------
// <copyright file="UndoEvent.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Model
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Represents a single event inside the undo / redo system can be undone that can be used
    /// by a instance of the <see cref="RSG.Editor.Model.UndoEngine"/> class.
    /// </summary>
    internal class UndoEvent
    {
        #region Fields
        /// <summary>
        /// The private list of the changes that have occurred within this undo event.
        /// </summary>
        private List<IChange> _changes;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="UndoEvent"/> class.
        /// </summary>
        public UndoEvent()
        {
            this._changes = new List<IChange>();
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the number of changes that this event currently has in it.
        /// </summary>
        public int ChangeCount
        {
            get { return this._changes == null ? 0 : this._changes.Count; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Adds the specified change to this undo event.
        /// </summary>
        /// <param name="change">
        /// The single change that should be added to this undo event.
        /// </param>
        internal void AddChange(IChange change)
        {
            this._changes.Add(change);
        }

        /// <summary>
        /// Redoes this undo event.
        /// </summary>
        internal void Redo()
        {
            if (this._changes == null || this._changes.Count <= 0)
            {
                return;
            }

            foreach (IChange change in this._changes)
            {
                change.Redo();
            }
        }

        /// <summary>
        /// Undoes this undo event.
        /// </summary>
        internal void Undo()
        {
            if (this._changes == null || this._changes.Count <= 0)
            {
                return;
            }

            foreach (IChange change in this._changes.Reverse<IChange>())
            {
                change.Undo();
            }
        }
        #endregion Methods
    } // RSG.Editor.Model.UndoEvent {Class}
} // RSG.Editor.Model {Namespace}
