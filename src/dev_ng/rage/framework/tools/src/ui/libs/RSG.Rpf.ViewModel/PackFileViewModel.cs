﻿//---------------------------------------------------------------------------------------------
// <copyright file="PackFileViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Rpf.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Media.Imaging;
    using RSG.Editor;
    using RSG.ManagedRage;
    using RSG.Rpf.ViewModel.Resources;

    /// <summary>
    /// The view model that contains the data retrieved from a
    /// <see cref="RSG.ManagedRage.Packfile"/> object.
    /// </summary>
    public class PackFileViewModel : NotifyPropertyChangedBase
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="CategoryNames"/> property.
        /// </summary>
        private static Dictionary<PackEntryCategory, string> _categoryNames = Categories();

        /// <summary>
        /// The private field used for the <see cref="Entries"/> property.
        /// </summary>
        private List<PackEntryViewModel> _entries;

        /// <summary>
        /// The private field used for the <see cref="FilteredFileCount"/> property.
        /// </summary>
        private int _filteredFileCount;

        /// <summary>
        /// The private field used for the <see cref="FilteredResourceCount"/> property.
        /// </summary>
        private int _filteredResourceCount;

        /// <summary>
        /// The private field used for the <see cref="FilteredUnknownCount"/> property.
        /// </summary>
        private int _filteredUnknownCount;

        /// <summary>
        /// The private field used for the <see cref="HighlightCaseSensitive"/> property.
        /// </summary>
        private bool _highlightCaseSensitive;

        /// <summary>
        /// The private field used for the <see cref="HighlightText"/> property.
        /// </summary>
        private string _highlightText;

        /// <summary>
        /// The private field used for the <see cref="Loaded"/> property.
        /// </summary>
        private bool _loaded;

        /// <summary>
        /// The private reference to the pack file that this view model is wrapping.
        /// </summary>
        private Packfile _pack;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="PackFileViewModel"/> class.
        /// </summary>
        public PackFileViewModel()
        {
            this._entries = new List<PackEntryViewModel>();
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the static map between entry categories and their display names.
        /// </summary>
        public static Dictionary<PackEntryCategory, string> CategoryNames
        {
            get { return PackFileViewModel._categoryNames; }
        }

        /// <summary>
        /// Gets the collection of pack entry view models that have been initialised along with
        /// this pack file view model.
        /// </summary>
        public List<PackEntryViewModel> Entries
        {
            get { return this._entries; }
            private set { this.SetProperty(ref this._entries, value); }
        }

        /// <summary>
        /// Gets the number of entries in this file that are files.
        /// </summary>
        public int FileCount
        {
            get
            {
                return this._entries.Where(e => e.Category == PackEntryCategory.File).Count();
            }
        }

        /// <summary>
        /// Gets or sets the current number of entries currently valid based on the filters and
        /// search parameters that are files.
        /// </summary>
        public int FilteredFileCount
        {
            get { return this._filteredFileCount; }
            set { this.SetProperty(ref this._filteredFileCount, value); }
        }

        /// <summary>
        /// Gets or sets the current number of entries currently valid based on the filters and
        /// search parameters that are resources.
        /// </summary>
        public int FilteredResourceCount
        {
            get { return this._filteredResourceCount; }
            set { this.SetProperty(ref this._filteredResourceCount, value); }
        }

        /// <summary>
        /// Gets or sets the current number of entries currently valid based on the filters and
        /// search parameters that have a unknown category type.
        /// </summary>
        public int FilteredUnknownCount
        {
            get { return this._filteredUnknownCount; }
            set { this.SetProperty(ref this._filteredUnknownCount, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the highlight text should be used as case
        /// sensitive or not.
        /// </summary>
        public bool HighlightCaseSensitive
        {
            get { return this._highlightCaseSensitive; }
            set { this.SetProperty(ref this._highlightCaseSensitive, value); }
        }

        /// <summary>
        /// Gets or sets the text that should be highlighted throughout the entries.
        /// </summary>
        public string HighlightText
        {
            get { return this._highlightText; }
            set { this.SetProperty(ref this._highlightText, value); }
        }

        /// <summary>
        /// Gets a value indicating whether the pack file data for this view model was loaded
        /// successfully.
        /// </summary>
        public bool Loaded
        {
            get { return this._loaded; }
        }

        /// <summary>
        /// Gets the physical leaf size set on this RPF file.
        /// </summary>
        public ulong PhysicalLeafSize
        {
            get
            {
                if (this._pack == null)
                {
                    return 0;
                }

                return this._pack.PhysicalLeafSize;
            }
        }

        /// <summary>
        /// Gets the encryption key for this RPF file.
        /// </summary>
        public ulong EncryptionKey
        {
            get
            {
                if (this._pack == null)
                {
                    return (0);
                }

                return this._pack.EncryptionKey;
            }
        }

        /// <summary>
        /// Gets the platform that this pack file has been resourced for.
        /// </summary>
        public Packfile.etPlatform Platform
        {
            get
            {
                if (this._pack == null)
                {
                    return Packfile.etPlatform.Unknown;
                }

                return this._pack.Platform;
            }
        }

        /// <summary>
        /// Gets the icon to use to represent the platform that this pack file has been
        /// resourced for.
        /// </summary>
        public BitmapSource PlatformIcon
        {
            get
            {
                if (this._pack == null)
                {
                    return null;
                }

                switch (this._pack.Platform)
                {
                    case Packfile.etPlatform.Unknown:
                        break;
                    case Packfile.etPlatform.Orbis:
                        return CommonIcons.Playstation;
                    case Packfile.etPlatform.PS3:
                        return CommonIcons.Playstation;
                    case Packfile.etPlatform.Xenon:
                        return CommonIcons.XBox;
                    case Packfile.etPlatform.Win32:
                        return CommonIcons.Windows;
                    case Packfile.etPlatform.Win64:
                        return CommonIcons.Windows;
                    case Packfile.etPlatform.Durango:
                        return CommonIcons.XBox;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the number of entries in this file that are resources.
        /// </summary>
        public int ResourceCount
        {
            get
            {
                if (this._pack == null)
                {
                    return 0;
                }

                return
                    this._entries.Where(e => e.Category == PackEntryCategory.Resource).Count();
            }
        }

        /// <summary>
        /// Gets the total packed size of all of the current entries that are included in the
        /// current filter if there is one.
        /// </summary>
        public string TotalPacked
        {
            get
            {
                ulong value = 0;
                if (this._entries != null && this._entries.Count != 0)
                {
                    foreach (PackEntryViewModel viewModel in this._entries)
                    {
                        if (!viewModel.IncludedInFilter)
                        {
                            continue;
                        }

                        value += viewModel.Packed;
                    }
                }

                return String.Format(
                    CultureInfo.CurrentUICulture, StringTable.TotalSizeDisplayString, value);
            }
        }

        /// <summary>
        /// Gets the total number of physical chucks of all of the current entries that are
        /// included in the current filter if there is one.
        /// </summary>
        public string TotalPhysicalChunkCount
        {
            get
            {
                ulong value = 0;
                if (this._entries != null && this._entries.Count != 0)
                {
                    foreach (PackEntryViewModel viewModel in this._entries)
                    {
                        if (!viewModel.IncludedInFilter)
                        {
                            continue;
                        }

                        value += viewModel.PhysicalChunkCount;
                    }
                }

                return String.Format(
                    CultureInfo.CurrentUICulture, StringTable.TotalCountDisplayString, value);
            }
        }

        /// <summary>
        /// Gets the total physical size of all of the current entries that are included in the
        /// current filter if there is one.
        /// </summary>
        public string TotalPhysicalSize
        {
            get
            {
                ulong value = 0;
                if (this._entries != null && this._entries.Count != 0)
                {
                    foreach (PackEntryViewModel viewModel in this._entries)
                    {
                        if (!viewModel.IncludedInFilter)
                        {
                            continue;
                        }

                        value += viewModel.PhysicalSize;
                    }
                }

                return String.Format(
                    CultureInfo.CurrentUICulture, StringTable.TotalSizeDisplayString, value);
            }
        }

        /// <summary>
        /// Gets the total size of all of the current entries that are included in the current
        /// filter if there is one.
        /// </summary>
        public string TotalSize
        {
            get
            {
                ulong value = 0;
                if (this._entries != null && this._entries.Count != 0)
                {
                    foreach (PackEntryViewModel viewModel in this._entries)
                    {
                        if (!viewModel.IncludedInFilter)
                        {
                            continue;
                        }

                        value += viewModel.Size;
                    }
                }

                return String.Format(
                    CultureInfo.CurrentUICulture, StringTable.TotalSizeDisplayString, value);
            }
        }

        /// <summary>
        /// Gets the total number of virtual chucks of all of the current entries that are
        /// included in the current filter if there is one.
        /// </summary>
        public string TotalVirtualChunkCount
        {
            get
            {
                ulong value = 0;
                if (this._entries != null && this._entries.Count != 0)
                {
                    foreach (PackEntryViewModel viewModel in this._entries)
                    {
                        if (!viewModel.IncludedInFilter)
                        {
                            continue;
                        }

                        value += viewModel.VirtualChunkCount;
                    }
                }

                return String.Format(
                    CultureInfo.CurrentUICulture, StringTable.TotalCountDisplayString, value);
            }
        }

        /// <summary>
        /// Gets the total virtual size of all of the current entries that are included in the
        /// current filter if there is one.
        /// </summary>
        public string TotalVirtualSize
        {
            get
            {
                ulong value = 0;
                if (this._entries != null && this._entries.Count != 0)
                {
                    foreach (PackEntryViewModel viewModel in this._entries)
                    {
                        if (!viewModel.IncludedInFilter)
                        {
                            continue;
                        }

                        value += viewModel.VirtualSize;
                    }
                }

                return String.Format(
                    CultureInfo.CurrentUICulture, StringTable.TotalSizeDisplayString, value);
            }
        }

        /// <summary>
        /// Gets the number of entries in this file that have a unknown category type.
        /// </summary>
        public int UnknownCount
        {
            get
            {
                if (this._pack == null)
                {
                    return 0;
                }

                return
                    this._entries.Where(e => e.Category == PackEntryCategory.Unknown).Count();
            }
        }

        /// <summary>
        /// Gets the virtual leaf size set on this RPF file.
        /// </summary>
        public ulong VirtualLeafSize
        {
            get
            {
                if (this._pack == null)
                {
                    return 0;
                }

                return this._pack.VirtualLeafSize;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Extracts the specified pack entry to the returned memory stream.
        /// </summary>
        /// <param name="entry">
        /// The entry to extract.
        /// </param>
        /// <returns>
        /// A new memory stream that contains the extracted data.
        /// </returns>
        public MemoryStream ExtractEntry(PackEntryViewModel entry)
        {
            byte[] buffer = this._pack.Extract(Path.Combine(entry.Path, entry.Name));
            MemoryStream stream = new MemoryStream();
            if (buffer != null)
            {
                stream = new MemoryStream(buffer);
            }

            return stream;
        }

        /// <summary>
        /// Extracts the specified pack entry to the returned byte array.
        /// </summary>
        /// <param name="entry">
        /// The entry to extract.
        /// </param>
        /// <returns>
        /// A new byte array that contains the extracted data.
        /// </returns>
        public byte[] ExtractEntryToByteArray(PackEntryViewModel entry)
        {
            return this._pack.Extract(Path.Combine(entry.Path, entry.Name));
        }

        /// <summary>
        /// Called whenever the total values might have changed due to a change in the item
        /// collection.
        /// </summary>
        public void OnTotalsChanged()
        {
            this.NotifyPropertyChanged(
                "TotalSize",
                "TotalPacked",
                "TotalVirtualSize",
                "TotalPhysicalSize",
                "TotalVirtualChunkCount",
                "TotalPhysicalChunkCount");
        }

        /// <summary>
        /// Uses the specified string as the path to the .RPF file to load.
        /// </summary>
        /// <param name="path">
        /// The path to the .RPF file that will be loaded to produce the data for this view
        /// model.
        /// </param>
        /// <returns>
        /// True if loading was successful; otherwise, false.
        /// </returns>
        internal bool LoadPackFile(string path)
        {
            this.UnloadPackFile();
            if (!File.Exists(path))
            {
                throw new FileNotFoundException(
                    "Unable to create a packfile view model from a path that doesn't exist.");
            }

            this._pack = new Packfile();
            try
            {
                this._loaded = this._pack.Load(path);

                Debug.Assert(
                    this._loaded, String.Format("Failed to create the pack file {0}", path));

                List<PackEntryViewModel> entries = new List<PackEntryViewModel>();
                if (this._loaded)
                {
                    foreach (PackEntry entry in this._pack.Entries)
                    {
                        entries.Add(new PackEntryViewModel(entry, this));
                    }
                }

                this.Entries = entries;

                this.OnPackFileChanged();
                this.OnTotalsChanged();
                return this._loaded;
            }
            catch (Exception)
            {
                this.Entries = new List<PackEntryViewModel>();
                return (false);
            }
        }

        /// <summary>
        /// Uses the specified string as the path to the .RPF file to load.
        /// </summary>
        /// <param name="path">
        /// The path to the .RPF file that will be loaded to produce the data for this view
        /// model.
        /// </param>
        /// <returns>
        /// True if loading was successful; otherwise, false.
        /// </returns>
        internal async Task<bool> LoadPackFileAsync(string path)
        {
            this.UnloadPackFile();
            if (!File.Exists(path))
            {
                throw new FileNotFoundException(
                    "Unable to create a packfile view model from a path that doesn't exist.");
            }

            this._pack = new Packfile();
            try
            {
                Task task = Task.Factory.StartNew(
                    delegate { this.ActionDelegate(this._pack, path); });
                await Task.WhenAll(task);

                List<PackEntryViewModel> entries = new List<PackEntryViewModel>();
                if (this._loaded)
                {
                    foreach (PackEntry entry in this._pack.Entries)
                    {
                        entries.Add(new PackEntryViewModel(entry, this));
                    }
                }
                this.Entries = entries;

                this.OnPackFileChanged();
                this.OnTotalsChanged();
                return this._loaded;
            }
            catch (Exception ex)
            {
                String m = ex.Message;

                this.Entries = new List<PackEntryViewModel>();
                return (false);
            }
        }

        /// <summary>
        /// Unloads the currently loaded pack file.
        /// </summary>
        internal void UnloadPackFile()
        {
            foreach (PackEntryViewModel entry in this._entries)
            {
                entry.UnloadPackEntry();
            }

            this.Entries = new List<PackEntryViewModel>();
            this._loaded = false;
            if (this._pack != null)
            {
                using (this._pack)
                {
                }

                this._pack = null;
            }

            this.OnPackFileChanged();
            this.OnTotalsChanged();
        }

        /// <summary>
        /// Creates the map between pack entry categories and their display names.
        /// </summary>
        /// <returns>
        /// The map between pack entry categories and their display names.
        /// </returns>
        private static Dictionary<PackEntryCategory, string> Categories()
        {
            var categories = new Dictionary<PackEntryCategory, string>();
            categories[PackEntryCategory.Directory] = "Directory";
            categories[PackEntryCategory.File] = "File";
            categories[PackEntryCategory.Resource] = "Resource";
            categories[PackEntryCategory.Unknown] = "Unknown";
            return categories;
        }

        /// <summary>
        /// Loaded the .RPF file located at the specified path into the specified pack
        /// instance.
        /// </summary>
        /// <param name="packfile">
        /// The instance of the pack file that should contain the loaded data.
        /// </param>
        /// <param name="path">
        /// The path to the .RPF file that will be loaded.
        /// </param>
        private void ActionDelegate(Packfile packfile, string path)
        {
            this._loaded = packfile.Load(path);
        }

        /// <summary>
        /// Called whenever the pack file has changed.
        /// </summary>
        private void OnPackFileChanged()
        {
            this.NotifyPropertyChanged(
                "Platform",
                "PlatformIcon",
                "VirtualLeafSize",
                "PhysicalLeafSize",
                "EncryptionKey",
                "Loaded",
                "FileCount",
                "ResourceCount",
                "UnknownCount");
        }
        #endregion Methods
    } // RSG.Rpf.ViewModel.PackFileViewModel {Class}
} // RSG.Rpf.ViewModel {Namespace}
