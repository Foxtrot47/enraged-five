﻿//---------------------------------------------------------------------------------------------
// <copyright file="EmptyTask.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System.Threading.Tasks;

    /// <summary>
    /// A static class used to retrieve a static task that has already completed.
    /// </summary>
    public static class EmptyTask
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="CompletedTask"/> property.
        /// </summary>
        private static readonly Task<bool> _completedTask = Task.FromResult(false);
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets a already completed non-generic task.
        /// </summary>
        public static Task CompletedTask
        {
            get { return _completedTask; }
        }
        #endregion Properties
    } // RSG.Editor.EmptyTask {Class}
} // RSG.Editor {Namespace}
