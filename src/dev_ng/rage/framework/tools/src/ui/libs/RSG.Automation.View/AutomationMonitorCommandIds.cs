﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Automation.View
{

    /// <summary>
    /// Defines commands for the Automation Monitor.
    /// </summary>
    public static class AutomationMonitorCommandIds
    {
        #region Properties
        /// <summary>
        /// Job item context-menu.
        /// </summary>
        public static Guid JobContextMenu
        {
            get
            {
                if (!_jobContextMenu.HasValue)
                {
                    lock (_syncRoot)
                    {
                        if (!_jobContextMenu.HasValue)
                        {
                            _jobContextMenu =
                                new Guid("98A015AF-FEE9-49A7-B4F2-068F20F3AC3C");
                        }
                    }
                }

                return _jobContextMenu.Value;
            }
        }
        private static Guid? _jobContextMenu;

        /// <summary>
        /// Job header context-menu.
        /// </summary>
        public static Guid JobHeaderContextMenu
        {
            get
            {
                if (!_jobHeaderContextMenu.HasValue)
                {
                    lock (_syncRoot)
                    {
                        if (!_jobHeaderContextMenu.HasValue)
                        {
                            _jobHeaderContextMenu =
                                new Guid("29958A3F-B034-4451-9020-1CA887CD9917");
                        }
                    }
                }

                return _jobHeaderContextMenu.Value;
            }
        }
        private static Guid? _jobHeaderContextMenu;

        /// <summary>
        /// Global identifier for Client Context Menu
        /// </summary>
        public static Guid ClientHeaderContextMenu
        {
            get
            {
                if (!_clientHeaderContextMenu.HasValue)
                {
                    lock (_syncRoot)
                    {
                        if (!_clientHeaderContextMenu.HasValue)
                        {
                            _clientHeaderContextMenu =
                                new Guid("708A4C7E-66D8-4818-8D06-E9BDDE1C9B09");
                        }
                    }
                }

                return _clientHeaderContextMenu.Value;
            }
        }
        private static Guid? _clientHeaderContextMenu;
        #endregion // Properties

        #region Fields
        /// <summary>
        /// A private instance to a generic object used to support thread access.
        /// </summary>
        private static Object _syncRoot = new Object();
        #endregion // Fields
    }

} // RSG.Automation.View
