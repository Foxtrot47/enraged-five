﻿//---------------------------------------------------------------------------------------------
// <copyright file="AudioTypeCollection.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.ViewModel
{
    using RSG.Editor.Model;
    using RSG.Editor.View;
    using RSG.Text.Model;

    /// <summary>
    /// Provides a collection class that contains <see cref="DialogueAudioTypeViewModel"/>
    /// objects that are kept in sync with a collection of <see cref="DialogueAudioType"/>
    /// objects.
    /// </summary>
    public class AudioTypeCollection
        : CollectionConverter<DialogueAudioType, DialogueAudioTypeViewModel>,
        IReadOnlyViewModelCollection<DialogueAudioTypeViewModel>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="AudioTypeCollection"/> class.
        /// </summary>
        /// <param name="source">
        /// The model collection containing the source <see cref="DialogueAudioType"/> objects
        /// for this collection.
        /// </param>
        public AudioTypeCollection(ModelCollection<DialogueAudioType> source)
            : base(source)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Converts a single <see cref="DialogueAudioType"/> object to a new
        /// <see cref="DialogueAudioTypeViewModel"/> object.
        /// </summary>
        /// <param name="item">
        /// The item to convert.
        /// </param>
        /// <returns>
        /// A new <see cref="DialogueAudioTypeViewModel"/> object from the given
        /// <see cref="DialogueAudioType"/> object.
        /// </returns>
        protected override DialogueAudioTypeViewModel ConvertItem(DialogueAudioType item)
        {
            DialogueAudioTypeViewModel newViewModel = new DialogueAudioTypeViewModel(item);
            return newViewModel;
        }
        #endregion Methods
    } // RSG.Text.ViewModel.AudioTypeCollection {Class}
} // RSG.Text.ViewModel {Namespace}
