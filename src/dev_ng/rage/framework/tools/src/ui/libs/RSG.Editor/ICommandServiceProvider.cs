﻿//---------------------------------------------------------------------------------------------
// <copyright file="ICommandServiceProvider.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;

    /// <summary>
    /// When implemented defines a service provider that can be used by the command
    /// implementers to retrieve services to use in their execute handlers.
    /// </summary>
    public interface ICommandServiceProvider : IServiceProvider
    {
        #region Methods
        /// <summary>
        /// Gets the service object of the specified type.
        /// </summary>
        /// <typeparam name="T">
        /// The type of service object to get.
        /// </typeparam>
        /// <returns>
        /// A service object of the specified type or null if there is no service object of
        /// that type.
        /// </returns>
        T GetService<T>() where T : class;
        #endregion Methods
    } // RSG.Editor.ICommandServiceProvider {Interface}
} // RSG.Editor {Namespace}
