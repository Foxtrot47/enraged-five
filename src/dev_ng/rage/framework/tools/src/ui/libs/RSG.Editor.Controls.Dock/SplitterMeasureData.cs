﻿//---------------------------------------------------------------------------------------------
// <copyright file="SplitterMeasureData.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Defines a set of measuring parameters used to measure a single UIElement that has an
    /// attached SplitterLength value while it is being resized.
    /// </summary>
    public class SplitterMeasureData
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="AttachedLength"/> property.
        /// </summary>
        private SplitterLength _attachedLength;

        /// <summary>
        /// The private field used for the <see cref="Element"/> property.
        /// </summary>
        private UIElement _element;

        /// <summary>
        /// The private field used for the <see cref="IsMaximumReached"/> property.
        /// </summary>
        private bool _isMaximumReached;

        /// <summary>
        /// The private field used for the <see cref="IsMinimumReached"/> property.
        /// </summary>
        private bool _isMinimumReached;

        /// <summary>
        /// The private field used for the <see cref="MeasuredBounds"/> property.
        /// </summary>
        private Rect _measuredBounds;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SplitterMeasureData"/> class using the
        /// specified element for the source to the attached properties.
        /// </summary>
        /// <param name="element">
        /// The element that has the attached length properties on it.
        /// </param>
        public SplitterMeasureData(UIElement element)
        {
            this._element = element;
            this._attachedLength = RsSplitterPanel.GetSplitterLength(element);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the attached length for the element that this measure data is
        /// associated with.
        /// </summary>
        public SplitterLength AttachedLength
        {
            get { return this._attachedLength; }
            set { this._attachedLength = value; }
        }

        /// <summary>
        /// Gets the UIElement that this measure data is associated with.
        /// </summary>
        public UIElement Element
        {
            get { return this._element; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the associated element has reached its
        /// maximum length value.
        /// </summary>
        public bool IsMaximumReached
        {
            get { return this._isMaximumReached; }
            set { this._isMaximumReached = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the associated element has reached its
        /// minimum length value.
        /// </summary>
        public bool IsMinimumReached
        {
            get { return this._isMinimumReached; }
            set { this._isMinimumReached = value; }
        }

        /// <summary>
        /// Gets or sets the bounds used to measure the associated element with.
        /// </summary>
        public Rect MeasuredBounds
        {
            get { return this._measuredBounds; }
            set { this._measuredBounds = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a list of <see cref="SplitterMeasureData"/> instances based on the
        /// specified list of elements.
        /// </summary>
        /// <param name="elements">
        /// The list of elements to create <see cref="SplitterMeasureData"/> instances from.
        /// </param>
        /// <returns>
        /// A list of <see cref="SplitterMeasureData"/> instances one for each element in the
        /// specified list of elements.
        /// </returns>
        public static IList<SplitterMeasureData> FromElements(IList elements)
        {
            List<SplitterMeasureData> list = new List<SplitterMeasureData>(elements.Count);
            foreach (UIElement element in elements)
            {
                if (element == null)
                {
                    list.Add(null);
                    continue;
                }

                list.Add(new SplitterMeasureData(element));
            }

            return list;
        }
        #endregion Methods
    } // RSG.Editor.Controls.Dock.SplitterMeasureData {Class}
} // RSG.Editor.Controls.Dock {Namespace}
