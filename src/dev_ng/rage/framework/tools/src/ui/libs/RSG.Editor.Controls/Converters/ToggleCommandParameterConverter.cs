﻿//---------------------------------------------------------------------------------------------
// <copyright file="ToggleCommandParameterConverter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Converters
{
    using System;
    using System.Globalization;

    /// <summary>
    /// Converters two separator items into a single tuple.
    /// </summary>
    public sealed class ToggleCommandParameterConverter :
        ValueConverter<bool, object, Tuple<bool, object>>
    {
        #region Methods
        /// <summary>
        /// Converters the two specified source values to a single tuple value.
        /// </summary>
        /// <param name="value1">
        /// The first source value.
        /// </param>
        /// <param name="value2">
        /// The second source value.
        /// </param>
        /// <param name="param">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A target value created by converting the two specified source values.
        /// </returns>
        protected override Tuple<bool, object> Convert(
            bool value1,
            object value2,
            object param,
            CultureInfo culture)
        {
            return Tuple.Create(value1, value2);
        }
        #endregion Methods
    } // RSG.Editor.Controls.Converters.ToggleCommandParameterConverter {Class}
} // RSG.Editor.Controls.Converters {Namespace}
