﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsDropDownControl.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Threading;
    using RSG.Editor.Controls.Helpers;
    using RSG.Interop.Microsoft.Windows;

    /// <summary>
    /// Represents a control with a drop down area that can be shown or hidden that displays
    /// generic content.
    /// </summary>
    public class RsDropDownControl : HeaderedContentControl
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="IsReadOnly"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsReadOnlyProperty;

        /// <summary>
        /// The private field used for the <see cref="IsContextMenuOpen"/> property.
        /// </summary>
        private bool _isContextMenuOpen;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsDropDownControl" /> class.
        /// </summary>
        static RsDropDownControl()
        {
            ComboBox.IsDropDownOpenProperty.AddOwner(
                typeof(RsDropDownControl),
                new FrameworkPropertyMetadata(OnIsDropDownOpenChanged, CoerceIsDropDownOpen));

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsDropDownControl),
                new FrameworkPropertyMetadata(typeof(RsDropDownControl)));

            EventManager.RegisterClassHandler(
                typeof(RsDropDownControl),
                Mouse.MouseWheelEvent,
                new MouseWheelEventHandler(OnMouseWheel),
                true);

            EventManager.RegisterClassHandler(
                typeof(RsDropDownControl),
                Mouse.LostMouseCaptureEvent,
                new MouseEventHandler(OnLostMouseCapture));

            EventManager.RegisterClassHandler(
                typeof(RsDropDownControl),
                Mouse.MouseDownEvent,
                new MouseButtonEventHandler(OnMouseButtonDown),
                true);

            EventManager.RegisterClassHandler(
                typeof(RsDropDownControl),
                ContextMenuService.ContextMenuOpeningEvent,
                new ContextMenuEventHandler(OnContextMenuOpen),
                true);

            EventManager.RegisterClassHandler(
                typeof(RsDropDownControl),
                ContextMenuService.ContextMenuClosingEvent,
                new ContextMenuEventHandler(OnContextMenuClose),
                true);

            IsReadOnlyProperty =
                DependencyProperty.Register(
                "IsReadOnly",
                typeof(bool),
                typeof(RsDropDownControl),
                new PropertyMetadata(false));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsDropDownControl" /> class.
        /// </summary>
        public RsDropDownControl()
        {
            Mouse.AddPreviewMouseDownOutsideCapturedElementHandler(
                this, this.OnMouseDownOutsideCapturedElement);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether the popup for this control is open or not.
        /// </summary>
        public bool IsDropDownOpen
        {
            get { return (bool)this.GetValue(ComboBox.IsDropDownOpenProperty); }
            set { this.SetValue(ComboBox.IsDropDownOpenProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the context menu for this control is
        /// opened.
        /// </summary>
        private bool IsContextMenuOpen
        {
            get { return this._isContextMenuOpen; }
            set { this._isContextMenuOpen = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the user can change the value for this
        /// spinner.
        /// </summary>
        public bool IsReadOnly
        {
            get { return (bool)this.GetValue(IsReadOnlyProperty); }
            set { this.SetValue(IsReadOnlyProperty, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Called whenever the <see cref="IsDropDownOpen"/> dependency property needs to be
        /// re-evaluated. This makes sure the popup doesn't try to open before the control is
        /// loaded.
        /// </summary>
        /// <param name="d">
        /// The instance whose dependency property needs evaluated.
        /// </param>
        /// <param name="value">
        /// The new value of the property, prior to any coercion attempt.
        /// </param>
        /// <returns>
        /// The coerced value.
        /// </returns>
        private static object CoerceIsDropDownOpen(DependencyObject d, object value)
        {
            if ((bool)value)
            {
                RsDropDownControl control = (RsDropDownControl)d;
                if (control.IsReadOnly)
                {
                    return false;
                }

                if (!control.IsLoaded)
                {
                    control.Loaded += new RoutedEventHandler(control.OpenOnLoad);
                    return false;
                }
            }

            return value;
        }

        /// <summary>
        /// Called when the context menu for this control closes.
        /// </summary>
        /// <param name="sender">
        /// The object that sent this event.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Controls.ContextMenuEventArgs containing the event data.
        /// </param>
        private static void OnContextMenuClose(object sender, ContextMenuEventArgs e)
        {
            ((RsDropDownControl)sender).IsContextMenuOpen = false;
        }

        /// <summary>
        /// Called when the context menu for this control opens.
        /// </summary>
        /// <param name="sender">
        /// The object that sent this event.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Controls.ContextMenuEventArgs containing the event data.
        /// </param>
        private static void OnContextMenuOpen(object sender, ContextMenuEventArgs e)
        {
            ((RsDropDownControl)sender).IsContextMenuOpen = true;
        }

        /// <summary>
        /// Called whenever the <see cref="IsDropDownOpen"/> dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The object whose <see cref="IsDropDownOpen"/> dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnIsDropDownOpenChanged(
            object s, DependencyPropertyChangedEventArgs e)
        {
            RsDropDownControl control = s as RsDropDownControl;
            if (control == null)
            {
                Debug.Assert(false, "Incorrect dependency property changed cast.");
                return;
            }

            bool newValue = (bool)e.NewValue;
            bool oldValue = !newValue;

            if (newValue)
            {
                Mouse.Capture(control, CaptureMode.SubTree);
            }
            else
            {
                if (control.IsKeyboardFocusWithin)
                {
                    control.Focus();
                }

                if (Mouse.Captured == control)
                {
                    Mouse.Capture(null);
                }
            }

            control.CoerceValue(ToolTipService.IsEnabledProperty);
        }

        /// <summary>
        /// Called when this control loses the mouse capture for another element. This method
        /// either closes the drop down or takes the capture back.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.MouseEventArgs containing the event data.
        /// </param>
        private static void OnLostMouseCapture(object sender, MouseEventArgs e)
        {
            RsDropDownControl control = (RsDropDownControl)sender;

            if (Mouse.Captured != control)
            {
                if (e.OriginalSource == control)
                {
                    if (Mouse.Captured == null)
                    {
                        control.Close();
                        return;
                    }
                    else if (!control.HasDescendant(Mouse.Captured as DependencyObject))
                    {
                        control.Close();
                        return;
                    }
                }
                else
                {
                    if (control.HasDescendant(e.OriginalSource as DependencyObject))
                    {
                        bool emptyCapture = User32.GetCapture() == IntPtr.Zero;
                        if (control.IsDropDownOpen && Mouse.Captured == null && emptyCapture)
                        {
                            Mouse.Capture(control, CaptureMode.SubTree);
                            e.Handled = true;
                        }
                    }
                    else
                    {
                        control.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Called if the mouse button is pressed down. This makes sure that if the user
        /// presses in a area outside of the capture area the drop down is closes including the
        /// toggle button.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.MouseButtonEventArgs containing the event data.
        /// </param>
        private static void OnMouseButtonDown(object sender, MouseButtonEventArgs e)
        {
            RsDropDownControl control = (RsDropDownControl)sender;
            if (!control.IsContextMenuOpen && !control.IsKeyboardFocusWithin)
            {
                control.Focus();
            }

            e.Handled = true;
            if (Mouse.Captured == control && e.OriginalSource == control)
            {
                control.Close();
            }
        }

        /// <summary>
        /// Handles the event reporting a mouse wheel rotation. This makes sure the event is
        /// eaten if the popup is open or the keyboard focus is somewhere inside this control.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.MouseWheelEventArgs containing the event data.
        /// </param>
        private static void OnMouseWheel(object s, MouseWheelEventArgs e)
        {
            RsDropDownControl control = s as RsDropDownControl;
            if (control == null)
            {
                Debug.Assert(false, "Incorrect class event handler cast.");
                return;
            }

            if (control.IsDropDownOpen)
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Closes this controls popup without messing with any bindings on it.
        /// </summary>
        private void Close()
        {
            if (this.IsDropDownOpen)
            {
                this.SetCurrentValue(ComboBox.IsDropDownOpenProperty, false);
            }
        }

        /// <summary>
        /// Called whenever the mouse down event is fired outside of the captured element. Used
        /// to make sure the popup is closed if the user clicks away from it.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.MouseButtonEventArgs containing the mouse event data.
        /// </param>
        private void OnMouseDownOutsideCapturedElement(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.Captured == this && e.OriginalSource == this)
            {
                this.Close();
            }
        }

        /// <summary>
        /// Opens this controls popup once it has been loaded. Makes sure the actual open
        /// doesn't happen until after the 1st render.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to. (This control).
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private void OpenOnLoad(object sender, RoutedEventArgs e)
        {
            Dispatcher.BeginInvoke(
                DispatcherPriority.Input,
                new DispatcherOperationCallback(delegate(object param)
                {
                    this.CoerceValue(ComboBox.IsDropDownOpenProperty);

                    return null;
                }),
                null);
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsDropDownControl {Class}
} // RSG.Editor.Controls {Namespace}
