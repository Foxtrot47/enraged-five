﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsDocumentTabPanel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock.Document
{
    using System;
    using System.Threading;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;

    /// <summary>
    /// Handles the layout of the tab items inside a <see cref="RsDocumentPane"/> control.
    /// </summary>
    internal class RsDocumentTabPanel : RsReorderTabPanel
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="HasHiddenItemsAtEnd"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty HasHiddenItemsAtEndProperty;

        /// <summary>
        /// Identifies the <see cref="HasHiddenItemsAtStart"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty HasHiddenItemsAtStartProperty;

        /// <summary>
        /// Identifies the <see cref="HasHiddenItems"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty HasHiddenItemsProperty;

        /// <summary>
        /// Identifies the key used for the <see cref="HasHiddenItemsAtEnd"/>
        /// dependency property.
        /// </summary>
        private static readonly DependencyPropertyKey _hasHiddenItemsAtEndPropertyKey;

        /// <summary>
        /// Identifies the key used for the <see cref="HasHiddenItemsAtStart"/>
        /// dependency property.
        /// </summary>
        private static readonly DependencyPropertyKey _hasHiddenItemsAtStartPropertyKey;

        /// <summary>
        /// Identifies the key used for the <see cref="HasHiddenItems"/> dependency property.
        /// </summary>
        private static readonly DependencyPropertyKey _hasHiddenItemsPropertyKey;

        /// <summary>
        /// The private field used for the <see cref="FirstVisibleTab"/> property.
        /// </summary>
        private int _firstVisibleTab;

        /// <summary>
        /// The private timer used to prevent the selected item being kept in view.
        /// </summary>
        private Timer _selectedVisiblePreventer;

        /// <summary>
        /// The private field used for the <see cref="VisibleTabs"/> property.
        /// </summary>
        private int _visibleTabs;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsDocumentTabPanel"/> class.
        /// </summary>
        static RsDocumentTabPanel()
        {
            _hasHiddenItemsPropertyKey =
                DependencyProperty.RegisterReadOnly(
                "HasHiddenItems",
                typeof(bool),
                typeof(RsDocumentTabPanel),
                new PropertyMetadata(false));

            HasHiddenItemsProperty = _hasHiddenItemsPropertyKey.DependencyProperty;

            _hasHiddenItemsAtEndPropertyKey =
                DependencyProperty.RegisterReadOnly(
                "HasHiddenItemsAtEnd",
                typeof(bool),
                typeof(RsDocumentTabPanel),
                new PropertyMetadata(false));

            HasHiddenItemsAtEndProperty = _hasHiddenItemsAtEndPropertyKey.DependencyProperty;

            _hasHiddenItemsAtStartPropertyKey =
                DependencyProperty.RegisterReadOnly(
                "HasHiddenItemsAtStart",
                typeof(bool),
                typeof(RsDocumentTabPanel),
                new PropertyMetadata(false));

            HasHiddenItemsAtStartProperty =
                _hasHiddenItemsAtStartPropertyKey.DependencyProperty;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsDocumentTabPanel"/> class.
        /// </summary>
        public RsDocumentTabPanel()
        {
            this.HorizontalAlignment = HorizontalAlignment.Left;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the zero-based index for the tab that is the first tab to be
        /// visible to the user on the panel.
        /// </summary>
        public int FirstVisibleTab
        {
            get { return this._firstVisibleTab; }
            set { this._firstVisibleTab = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this panel currently has items on it
        /// that couldn't fit into the panels available render size.
        /// </summary>
        public bool HasHiddenItems
        {
            get { return (bool)this.GetValue(HasHiddenItemsProperty); }
            protected set { this.SetValue(_hasHiddenItemsPropertyKey, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this panel currently has items on it
        /// that couldn't fit into the panels available render size and are hidden off the end
        /// of the panel.
        /// </summary>
        public bool HasHiddenItemsAtEnd
        {
            get { return (bool)this.GetValue(HasHiddenItemsAtEndProperty); }
            protected set { this.SetValue(_hasHiddenItemsAtEndPropertyKey, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this panel currently has items on it
        /// that couldn't fit into the panels available render size and are hidden before the
        /// start of the panel.
        /// </summary>
        public bool HasHiddenItemsAtStart
        {
            get { return (bool)this.GetValue(HasHiddenItemsAtStartProperty); }
            protected set { this.SetValue(_hasHiddenItemsAtStartPropertyKey, value); }
        }

        /// <summary>
        /// Gets the number of tabs that are currently visible on the panel.
        /// </summary>
        public int VisibleTabs
        {
            get { return this._visibleTabs; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Ensures that the item that is currently selected is visible to the user or at the
        /// very least is the first item in the collection.
        /// </summary>
        public void EnsureSelectionIsVisible()
        {
            if (this._selectedVisiblePreventer != null)
            {
                using (this._selectedVisiblePreventer)
                {
                    this._selectedVisiblePreventer = null;
                }
            }

            this.InvalidateArrange();
        }

        /// <summary>
        /// Starts the selection preventer so that for the next arrange the selected item
        /// isn't kept in view.
        /// </summary>
        public void StartPreventerTimer()
        {
            if (this._selectedVisiblePreventer != null)
            {
                using (this._selectedVisiblePreventer)
                {
                    this._selectedVisiblePreventer = null;
                }
            }

            TimeSpan timeSpan = new TimeSpan(0, 0, 0, 1);
            this._selectedVisiblePreventer =
                new Timer(
                    new TimerCallback(this.OnPreventerTick), this, timeSpan, timeSpan);
        }

        /// <summary>
        /// Positions child elements and determines a size for this class.
        /// </summary>
        /// <param name="finalSize">
        /// The final area within the parent that this element should use to arrange itself
        /// and its children.
        /// </param>
        /// <returns>
        /// The actual size used.
        /// </returns>
        protected override Size ArrangeOverride(Size finalSize)
        {
            double num = 0.0;
            UIElementCollection internalChildren = this.InternalChildren;
            int selectedIndex = 0;
            bool selectedItemHidden = false;
            bool selectedItemBeforeFirst = false;
            bool selectionFound = false;
            bool visible = true;
            this._visibleTabs = 0;
            for (int i = 0; i < internalChildren.Count; i++)
            {
                visible = true;
                FrameworkElement uiElement = internalChildren[i] as FrameworkElement;
                double desiredSize = uiElement.DesiredSize.Width;
                if (i != this.FirstVisibleTab)
                {
                    if (i < this.FirstVisibleTab)
                    {
                        visible = false;
                    }
                    else if (visible)
                    {
                        double right = num + desiredSize;
                        visible = !this.IsSignificantlyGreater(finalSize.Width, right);
                    }
                }

                uiElement.Visibility = visible ? Visibility.Visible : Visibility.Hidden;
                if (i >= this.FirstVisibleTab)
                {
                    if (visible)
                    {
                        this._visibleTabs++;
                        uiElement.Arrange(new Rect(num, 0.0, desiredSize, finalSize.Height));
                    }

                    num += desiredSize;
                }

                TabItem tabItem = uiElement as TabItem;
                if (tabItem != null && tabItem.IsSelected == true)
                {
                    selectionFound = true;
                    selectedIndex = i;
                    selectedItemHidden = !visible;
                    selectedItemBeforeFirst = i < this.FirstVisibleTab;
                }
            }

            this.HasHiddenItems = !visible || this.FirstVisibleTab > 0;
            this.HasHiddenItemsAtEnd = !visible;
            this.HasHiddenItemsAtStart = this.FirstVisibleTab > 0;
            Size result = default(Size);
            result.Width = num;
            result.Height = finalSize.Height;
            if (this._selectedVisiblePreventer == null)
            {
                if (selectionFound == true)
                {
                    if (selectedItemBeforeFirst)
                    {
                        this.FirstVisibleTab = selectedIndex;
                        this.InvalidateMeasure();
                    }

                    double finalWidth;
                    if (selectedItemHidden)
                    {
                        this.FirstVisibleTab = 0;
                        finalWidth = finalSize.Width;
                        for (int i = selectedIndex; i >= 0; i--)
                        {
                            UIElement uiElement = internalChildren[i];
                            finalWidth -= uiElement.DesiredSize.Width;
                            if (finalWidth < 0.0)
                            {
                                this.FirstVisibleTab = i + 1;
                                break;
                            }
                        }

                        this.InvalidateMeasure();
                    }
                }
            }

            if (this.FirstVisibleTab != 0)
            {
                double finalWidth = finalSize.Width - num;
                for (int i = this.FirstVisibleTab - 1; i >= 0; i--)
                {
                    if (i <= internalChildren.Count - 1)
                    {
                        UIElement uiElement = internalChildren[i];
                        finalWidth -= uiElement.DesiredSize.Width;
                        if (finalWidth < 0.0 || this.FirstVisibleTab <= 0)
                        {
                            break;
                        }
                    }

                    this.FirstVisibleTab--;
                    this.InvalidateMeasure();
                }
            }

            return result;
        }

        /// <summary>
        /// Returns a geometry for a clipping mask. The mask applies if the layout system
        /// attempts to arrange an element that is larger than the available display space.
        /// </summary>
        /// <param name="layoutSlotSize">
        /// The size of the part of the element that does visual presentation.
        /// </param>
        /// <returns>
        /// The clipping geometry.
        /// </returns>
        protected override Geometry GetLayoutClip(Size layoutSlotSize)
        {
            if (this.ClipToBounds == false)
            {
                return null;
            }

            return base.GetLayoutClip(layoutSlotSize);
        }

        /// <summary>
        /// Measures the size in layout required for child elements and determines a size for
        /// this class.
        /// </summary>
        /// <param name="availableSize">
        /// The available size that this element can give to child elements.
        /// </param>
        /// <returns>
        /// The size that this element determines it needs during layout, based on its
        /// calculations of child element sizes.
        /// </returns>
        protected override Size MeasureOverride(Size availableSize)
        {
            Size result = default(Size);
            Size actualAvailableSize = new Size(double.PositiveInfinity, availableSize.Height);
            UIElementCollection internalChildren = this.InternalChildren;
            int count = internalChildren.Count;
            for (int i = 0; i < count; i++)
            {
                UIElement element = internalChildren[i];
                if (element == null)
                {
                    continue;
                }

                element.Measure(actualAvailableSize);
                Size desiredSize = element.DesiredSize;
                if (i == this.FirstVisibleTab)
                {
                    if (desiredSize.Width > availableSize.Width)
                    {
                        element.Measure(availableSize);
                        desiredSize = element.DesiredSize;
                    }
                }

                result.Width += desiredSize.Width;
                result.Height = Math.Max(result.Height, desiredSize.Height);
            }

            result.Width = Math.Min(result.Width, availableSize.Width);
            return result;
        }

        /// <summary>
        /// Raises the shift events for the tab panel when the mouse wheels delta changes.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.MouseWheelEventArgs containing the event data.
        /// </param>
        protected override void OnMouseWheel(MouseWheelEventArgs e)
        {
            base.OnMouseWheel(e);
            if (e.Delta < 0)
            {
                RoutedCommand command = DockingCommands.ShiftLeft;
                if (command.CanExecute(null, this))
                {
                    command.Execute(null, this);
                }
            }
            else if (e.Delta > 1)
            {
                RoutedCommand command = DockingCommands.ShiftRight;
                if (command.CanExecute(null, this))
                {
                    command.Execute(null, this);
                }
            }
        }

        /// <summary>
        /// Determines whether two double values are nearly equal to each other based on a
        /// double epsilon of 1E-05.
        /// </summary>
        /// <param name="value1">
        /// The first value to test.
        /// </param>
        /// <param name="value2">
        /// The second value to test.
        /// </param>
        /// <returns>
        /// True if the two specified double values are within a double epsilon of each other;
        /// otherwise, false.
        /// </returns>
        private bool AreNearlyEqual(double value1, double value2)
        {
            if (this.IsNonreal(value1) || this.IsNonreal(value2))
            {
                return value1.CompareTo(value2) == 0;
            }

            return Math.Abs(value1 - value2) < 1E-05;
        }

        /// <summary>
        /// Determines whether the specified double value is a Real value or not.
        /// </summary>
        /// <param name="value">
        /// The double value to test.
        /// </param>
        /// <returns>
        /// True if the specified value is non real; otherwise, false.
        /// </returns>
        private bool IsNonreal(double value)
        {
            return double.IsNaN(value) || double.IsInfinity(value);
        }

        /// <summary>
        /// Determines whether the specified <paramref name="value2"/> parameter is
        /// significantly greater than the specified <paramref name="value1"/> parameter.
        /// </summary>
        /// <param name="value1">
        /// The first value to test.
        /// </param>
        /// <param name="value2">
        /// The second value to test.
        /// </param>
        /// <returns>
        /// True if the two specified <paramref name="value2"/> parameter is significantly
        /// greater than the specified <paramref name="value1"/> parameter; otherwise, false.
        /// </returns>
        private bool IsSignificantlyGreater(double value1, double value2)
        {
            return value2 > value1 && !this.AreNearlyEqual(value1, value2);
        }

        /// <summary>
        /// Gets called after the <see cref="_selectedVisiblePreventer"/> timer ticks.
        /// </summary>
        /// <param name="state">
        /// An object containing application-specific information relevant to the method
        /// invoked by this delegate, or null.
        /// </param>
        private void OnPreventerTick(object state)
        {
            using (this._selectedVisiblePreventer)
            {
                this._selectedVisiblePreventer = null;
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.Dock.Document.RsDocumentTabPanel {Class}
} // RSG.Editor.Controls.Dock.Document {Namespace}
