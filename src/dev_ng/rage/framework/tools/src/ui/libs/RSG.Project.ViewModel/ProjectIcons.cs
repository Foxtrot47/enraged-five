﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProjectIcons.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel
{
    using System;
    using System.Windows.Media.Imaging;
    using RSG.Base.Extensions;

    /// <summary>
    /// Provides static properties that give access to common icons that can be used throughout
    /// a application.
    /// </summary>
    public static class ProjectIcons
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="AddExistingItem"/> property.
        /// </summary>
        private static BitmapSource _addExistingItem;

        /// <summary>
        /// The private field used for the <see cref="AddNewItem"/> property.
        /// </summary>
        private static BitmapSource _addNewItem;

        /// <summary>
        /// The private field used for the <see cref="BuildCollection"/> property.
        /// </summary>
        private static BitmapSource _buildCollection;

        /// <summary>
        /// The private field used for the <see cref="BuildSelection"/> property.
        /// </summary>
        private static BitmapSource _buildSelection;

        /// <summary>
        /// The private field used for the <see cref="DynamicFolder"/> property.
        /// </summary>
        private static BitmapSource _dynamicFolder;

        /// <summary>
        /// The private field used for the <see cref="DynamicOpenedFolder"/> property.
        /// </summary>
        private static BitmapSource _dynamicOpenedFolder;

        /// <summary>
        /// The private field used for the <see cref="Failedproject"/> property.
        /// </summary>
        private static BitmapSource _failedProject;

        /// <summary>
        /// The private field used for the <see cref="GenericFile"/> property.
        /// </summary>
        private static BitmapSource _genericFile;

        /// <summary>
        /// The private field used for the <see cref="GenericOpenProjectCollection"/> property.
        /// </summary>
        private static BitmapSource _genericOpenProjectCollection;

        /// <summary>
        /// The private field used for the <see cref="GenericProject"/> property.
        /// </summary>
        private static BitmapSource _genericProject;

        /// <summary>
        /// The private field used for the <see cref="GenericProjectCollection"/> property.
        /// </summary>
        private static BitmapSource _genericProjectCollection;

        /// <summary>
        /// The private field used for the <see cref="InvalidLinkedItem"/> property.
        /// </summary>
        private static BitmapSource _invalidLinkedItem;

        /// <summary>
        /// The private field used for the <see cref="LocateInExplorer"/> property.
        /// </summary>
        private static BitmapSource _locateInExplorer;

        /// <summary>
        /// The private field used for the <see cref="NewFolder"/> property.
        /// </summary>
        private static BitmapSource _newFolder;

        /// <summary>
        /// The private field used for the <see cref="OpenFile"/> property.
        /// </summary>
        private static BitmapSource _openFile;

        /// <summary>
        /// A generic object that provides thread safety access to the image sources.
        /// </summary>
        private static object _syncRoot = new object();

        /// <summary>
        /// The private field used for the <see cref="Unloadedproject"/> property.
        /// </summary>
        private static BitmapSource _unloadedProject;
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets the icon that shows a add existing item icon.
        /// </summary>
        public static BitmapSource AddExistingItem
        {
            get
            {
                if (_addExistingItem == null)
                {
                    lock (_syncRoot)
                    {
                        if (_addExistingItem == null)
                        {
                            EnsureLoaded(ref _addExistingItem, "addExistingItem16");
                        }
                    }
                }

                return _addExistingItem;
            }
        }

        /// <summary>
        /// Gets the icon that shows a add new item icon.
        /// </summary>
        public static BitmapSource AddNewItem
        {
            get
            {
                if (_addNewItem == null)
                {
                    lock (_syncRoot)
                    {
                        if (_addNewItem == null)
                        {
                            EnsureLoaded(ref _addNewItem, "addNewItem16");
                        }
                    }
                }

                return _addNewItem;
            }
        }

        /// <summary>
        /// Gets the icon that shows a build collection icon.
        /// </summary>
        public static BitmapSource BuildCollection
        {
            get
            {
                if (_buildCollection == null)
                {
                    lock (_syncRoot)
                    {
                        if (_buildCollection == null)
                        {
                            EnsureLoaded(ref _buildCollection, "buildCollection16");
                        }
                    }
                }

                return _buildCollection;
            }
        }

        /// <summary>
        /// Gets the icon that shows a build selection icon.
        /// </summary>
        public static BitmapSource BuildSelection
        {
            get
            {
                if (_buildSelection == null)
                {
                    lock (_syncRoot)
                    {
                        if (_buildSelection == null)
                        {
                            EnsureLoaded(ref _buildSelection, "buildSelection16");
                        }
                    }
                }

                return _buildSelection;
            }
        }

        /// <summary>
        /// Gets the icon that shows a dynamic folder icon.
        /// </summary>
        public static BitmapSource DynamicFolder
        {
            get
            {
                if (_dynamicFolder == null)
                {
                    lock (_syncRoot)
                    {
                        if (_dynamicFolder == null)
                        {
                            EnsureLoaded(ref _dynamicFolder, "dynamicFolder16");
                        }
                    }
                }

                return _dynamicFolder;
            }
        }

        /// <summary>
        /// Gets the icon that shows a dynamic opened folder icon.
        /// </summary>
        public static BitmapSource DynamicOpenedFolder
        {
            get
            {
                if (_dynamicOpenedFolder == null)
                {
                    lock (_syncRoot)
                    {
                        if (_dynamicOpenedFolder == null)
                        {
                            EnsureLoaded(ref _dynamicOpenedFolder, "dynamicOpenedFolder16");
                        }
                    }
                }

                return _dynamicOpenedFolder;
            }
        }

        /// <summary>
        /// Gets the icon that shows a failed project icon.
        /// </summary>
        public static BitmapSource Failedproject
        {
            get
            {
                if (_failedProject == null)
                {
                    lock (_syncRoot)
                    {
                        if (_failedProject == null)
                        {
                            EnsureLoaded(ref _failedProject, "failedProject16");
                        }
                    }
                }

                return _failedProject;
            }
        }

        /// <summary>
        /// Gets the icon that shows a generic file icon.
        /// </summary>
        public static BitmapSource GenericFile
        {
            get
            {
                if (_genericFile == null)
                {
                    lock (_syncRoot)
                    {
                        if (_genericFile == null)
                        {
                            EnsureLoaded(ref _genericFile, "genericFile16");
                        }
                    }
                }

                return _genericFile;
            }
        }

        /// <summary>
        /// Gets the icon that shows a generic open project collection icon.
        /// </summary>
        public static BitmapSource GenericOpenProjectCollection
        {
            get
            {
                if (_genericOpenProjectCollection == null)
                {
                    lock (_syncRoot)
                    {
                        if (_genericOpenProjectCollection == null)
                        {
                            EnsureLoaded(
                                ref _genericOpenProjectCollection,
                                "genericOpenProjectCollection16");
                        }
                    }
                }

                return _genericOpenProjectCollection;
            }
        }

        /// <summary>
        /// Gets the icon that shows a generic project icon.
        /// </summary>
        public static BitmapSource GenericProject
        {
            get
            {
                if (_genericProject == null)
                {
                    lock (_syncRoot)
                    {
                        if (_genericProject == null)
                        {
                            EnsureLoaded(ref _genericProject, "genericProject16");
                        }
                    }
                }

                return _genericProject;
            }
        }

        /// <summary>
        /// Gets the icon that shows a generic project collection icon.
        /// </summary>
        public static BitmapSource GenericProjectCollection
        {
            get
            {
                if (_genericProjectCollection == null)
                {
                    lock (_syncRoot)
                    {
                        if (_genericProjectCollection == null)
                        {
                            EnsureLoaded(
                                ref _genericProjectCollection, "genericProjectCollection16");
                        }
                    }
                }

                return _genericProjectCollection;
            }
        }

        /// <summary>
        /// Gets the icon that shows a invalid linked item icon.
        /// </summary>
        public static BitmapSource InvalidLinkedItem
        {
            get
            {
                if (_invalidLinkedItem == null)
                {
                    lock (_syncRoot)
                    {
                        if (_invalidLinkedItem == null)
                        {
                            EnsureLoaded(
                                ref _invalidLinkedItem, "invalidLinkedItem16");
                        }
                    }
                }

                return _invalidLinkedItem;
            }
        }

        /// <summary>
        /// Gets the icon that shows a locate in explorer icon.
        /// </summary>
        public static BitmapSource LocateInExplorer
        {
            get
            {
                if (_locateInExplorer == null)
                {
                    lock (_syncRoot)
                    {
                        if (_locateInExplorer == null)
                        {
                            EnsureLoaded(ref _locateInExplorer, "locateInExplorer16");
                        }
                    }
                }

                return _locateInExplorer;
            }
        }

        /// <summary>
        /// Gets the icon that shows a new folder icon.
        /// </summary>
        public static BitmapSource NewFolder
        {
            get
            {
                if (_newFolder == null)
                {
                    lock (_syncRoot)
                    {
                        if (_newFolder == null)
                        {
                            EnsureLoaded(ref _newFolder, "newFolder16");
                        }
                    }
                }

                return _newFolder;
            }
        }

        /// <summary>
        /// Gets the icon that shows a open file icon.
        /// </summary>
        public static BitmapSource OpenFile
        {
            get
            {
                if (_openFile == null)
                {
                    lock (_syncRoot)
                    {
                        if (_openFile == null)
                        {
                            EnsureLoaded(ref _openFile, "openFile16");
                        }
                    }
                }

                return _openFile;
            }
        }

        /// <summary>
        /// Gets the icon that shows a unloaded project icon.
        /// </summary>
        public static BitmapSource Unloadedproject
        {
            get
            {
                if (_unloadedProject == null)
                {
                    lock (_syncRoot)
                    {
                        if (_unloadedProject == null)
                        {
                            EnsureLoaded(ref _unloadedProject, "unloadedProject16");
                        }
                    }
                }

                return _unloadedProject;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Ensures that the specified image source is loaded with the specified resource name.
        /// </summary>
        /// <param name="source">
        /// The image source that should be loaded.
        /// </param>
        /// <param name="resourceName">
        /// The name of the resource the loaded image source should set as its source.
        /// </param>
        private static void EnsureLoaded(ref BitmapSource source, string resourceName)
        {
            if (source != null)
            {
                return;
            }

            string assemblyName = typeof(ProjectIcons).Assembly.GetName().Name;
            string path = "Resources/" + resourceName + ".png";
            string bitmapPath = "pack://application:,,,/{0};component/{1}";
            bitmapPath = bitmapPath.FormatInvariant(assemblyName, path);
            Uri uri = new Uri(bitmapPath, UriKind.RelativeOrAbsolute);

            source = new BitmapImage(uri);
            source.Freeze();
        }
        #endregion Methods
    } // RSG.Project.ViewModel.ProjectIcons {Class}
} // RSG.Project.ViewModel.Commands {Namespace}
