﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProjectDocumentItem.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel
{
    using System;
    using System.IO;
    using System.Windows.Controls;
    using RSG.Editor;
    using RSG.Editor.Controls.Dock.ViewModel;

    /// <summary>
    /// 
    /// </summary>
    public class ProjectDocumentItem : DocumentItem
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="FileNode"/> property.
        /// </summary>
        private FileNode _fileNode;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DocumentItem"/> class.
        /// </summary>
        /// <param name="parentPane">
        /// The parent docking pane.
        /// </param>
        /// <param name="path">
        /// The full path to the document.
        /// </param>
        /// <param name="fileNode">
        /// The file node that is associated with this document.
        /// </param>
        public ProjectDocumentItem(
            DockingPane parentPane, string path, FileNode fileNode)
            : base(parentPane, path)
        {
            this._fileNode = fileNode;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the project file node associated with this document.
        /// </summary>
        public FileNode FileNode
        {
            get { return this._fileNode; }
        }

        /// <summary>
        /// Gets a value indicating whether this document was opened using the project
        /// explorer.
        /// </summary>
        public bool OpenedFromProject
        {
            get { return this._fileNode != null; }
        }

        /// <summary>
        /// Gets the friendly save path that this item pushes into the command system.
        /// </summary>
        public override string FriendlySavePath
        {
            get
            {
                if (this._fileNode != null)
                {
                    return this._fileNode.Include;
                }

                return this.Filename;
            }
        }

        /// <summary>
        /// Gets or sets the full file path to the location of this file.
        /// </summary>
        public override string FullPath
        {
            get
            {
                return base.FullPath;
            }

            set
            {
                if (string.Equals(base.FullPath, value))
                {
                    return;
                }

                base.FullPath = value;
                this.Filename = System.IO.Path.GetFileName(value);
                
                FileNode node = this._fileNode;
                if (node == null)
                {
                    return;
                }

                if (node.ProjectItem.UpdateIncludeValueFromPathChange(value))
                {
                    node.ProjectItemScopeNode.IsModified = true;
                }
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Override to provide core save logic to the data that this document represents.
        /// </summary>
        /// <param name="stream">
        /// The io stream that data for this document should be written to.
        /// </param>
        /// <returns>
        /// A value indicating whether the save operation was successful.
        /// </returns>
        protected override bool SaveCore(Stream stream)
        {
            ISaveable content = this.Content as ISaveable;
            if (content == null)
            {
                UserControl userControl = this.Content as UserControl;
                DispatcherHelper.InvokeOnUI(new Action(() =>
                {
                    content = userControl.DataContext as ISaveable;
                }),
                System.Windows.Threading.DispatcherPriority.Send);

                if (content == null)
                {
                    return false;
                }
            }

            return content.Save(stream);
        }
        #endregion Methods
    } // RSG.Project.ViewModel.ProjectDocumentItem {Class}
} // RSG.Project.ViewModel {Namespace}
