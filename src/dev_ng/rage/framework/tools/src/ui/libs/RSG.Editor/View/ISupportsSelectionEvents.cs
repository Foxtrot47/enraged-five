﻿//---------------------------------------------------------------------------------------------
// <copyright file="ISupportsSelectionEvents.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    using System.ComponentModel;

    /// <summary>
    /// When implemented represents a object that has methods related to the item getting
    /// selected and unselected in the user interface so that it can respond accordingly.
    /// </summary>
    public interface ISupportsSelectionEvents
    {
        #region Methods
        /// <summary>
        /// Called just before this object in the user interface is selected.
        /// </summary>
        void BeforeSelected();

        /// <summary>
        /// Called just after this object in the user interface is unselected.
        /// </summary>
        void AfterUnselected();
        #endregion Methods
    } // RSG.Editor.View.ISupportsSelectionEvents {Interface}
} // RSG.Editor.View {Namespace}
