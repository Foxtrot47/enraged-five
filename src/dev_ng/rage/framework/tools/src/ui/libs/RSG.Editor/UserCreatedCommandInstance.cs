﻿//---------------------------------------------------------------------------------------------
// <copyright file="UserCreatedCommandInstance.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Diagnostics;
    using System.Windows.Input;
    using System.Xml;

    /// <summary>
    /// A class that represents a command instance that has been created by the user during
    /// customisation.
    /// </summary>
    internal sealed class UserCreatedCommandInstance :
        CommandInstance, IUserCreatedCommandBarItem
    {
        #region Fields
        /// <summary>
        /// The private field containing the name of the command.
        /// </summary>
        private string _commandName;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="UserCreatedCommandInstance"/> class
        /// using the specified command definition as a root.
        /// </summary>
        /// <param name="definition">
        /// The definition that this command instance is to be instancing.
        /// </param>
        /// <param name="priority">
        /// The initial priority that this command bar item should be given.
        /// </param>
        /// <param name="parentId">
        /// The unique identifier for the parent of this item inside the command bar hierarchy.
        /// </param>
        public UserCreatedCommandInstance(
            CommandDefinition definition, ushort priority, Guid parentId)
            : base(
            definition.Command,
            priority,
            false,
            definition.Name,
            Guid.NewGuid(),
            parentId)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="UserCreatedCommandInstance"/> class
        /// using the specified reader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The reader to use as a data provider.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        public UserCreatedCommandInstance(XmlReader reader, IFormatProvider provider)
            : base(0, false, String.Empty, Guid.Empty, Guid.Empty)
        {
            this.DeserialiseCommand(reader, provider);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the name associated with the command.
        /// </summary>
        public string CommandName
        {
            get
            {
                if (this.Command != null)
                {
                    return this.Command.Name;
                }

                return this._commandName;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Serialises this command to the end of the specified writer.
        /// </summary>
        /// <param name="writer">
        /// The xml writer that the command gets serialised out to.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        public void SerialiseCommand(XmlWriter writer, IFormatProvider provider)
        {
            writer.WriteAttributeString(
                "aremultiitemsshown", this.AreMultiItemsShown.ToString(provider));
            writer.WriteAttributeString(
                "eachitemstartsgroup", this.EachItemStartsGroup.ToString(provider));
            writer.WriteAttributeString("id", this.Id.ToString("D").ToUpperInvariant());
            writer.WriteAttributeString("visible", this.IsVisible.ToString(provider));
            writer.WriteAttributeString(
                "parent", this.ParentId.ToString("D").ToUpperInvariant());
            writer.WriteAttributeString("priority", this.Priority.ToString(provider));
            writer.WriteAttributeString(
                "showfiltercount", this.ShowFilterCount.ToString(provider));
            writer.WriteAttributeString("startsgroup", this.StartsGroup.ToString(provider));
            writer.WriteAttributeString("text", this.Text.ToString(provider));
            writer.WriteAttributeString("width", this.Width.ToString(provider));
            writer.WriteAttributeString("style", this.DisplayStyle.ToString());
            writer.WriteAttributeString("command", this.Command.Name);
        }

        /// <summary>
        /// Sets the command for this user created command instance to the specified value.
        /// </summary>
        /// <param name="command">
        /// The command to set this instance to.
        /// </param>
        public void SetCommand(RockstarRoutedCommand command)
        {
            this.Command = command;
        }

        /// <summary>
        /// Deserialises the command using the data contained within the specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the command data.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        private void DeserialiseCommand(XmlReader reader, IFormatProvider provider)
        {
            bool boolResult = false;
            Guid guidResult = Guid.Empty;

            string areMultiItemsShown = reader.GetAttribute("aremultiitemsshown");
            if (areMultiItemsShown != null)
            {
                bool parsed = bool.TryParse(areMultiItemsShown, out boolResult);
                Debug.Assert(parsed, "Failed to parse a user created command parameter.");
                if (parsed)
                {
                    this.AreMultiItemsShown = boolResult;
                }
            }

            string eachItemStartsGroup = reader.GetAttribute("eachitemstartsgroup");
            if (eachItemStartsGroup != null)
            {
                bool parsed = bool.TryParse(eachItemStartsGroup, out boolResult);
                Debug.Assert(parsed, "Failed to parse a user created command parameter.");
                if (parsed)
                {
                    this.EachItemStartsGroup = boolResult;
                }
            }

            string id = reader.GetAttribute("id");
            if (id != null)
            {
                bool parsed = Guid.TryParseExact(id, "D", out guidResult);
                Debug.Assert(parsed, "Failed to parse a user created command parameter.");
                if (parsed)
                {
                    this.Id = guidResult;
                }
            }

            string visible = reader.GetAttribute("visible");
            if (visible != null)
            {
                bool parsed = bool.TryParse(visible, out boolResult);
                Debug.Assert(parsed, "Failed to parse a user created command parameter.");
                if (parsed)
                {
                    this.IsVisible = boolResult;
                }
            }

            string parent = reader.GetAttribute("parent");
            if (parent != null)
            {
                bool parsed = Guid.TryParseExact(parent, "D", out guidResult);
                Debug.Assert(parsed, "Failed to parse a user created command parameter.");
                if (parsed)
                {
                    this.ParentId = guidResult;
                }
            }

            string priority = reader.GetAttribute("priority");
            if (priority != null)
            {
                ushort result = 0;
                bool parsed = ushort.TryParse(priority, out result);
                Debug.Assert(parsed, "Failed to parse a user created command parameter.");
                if (parsed)
                {
                    this.Priority = result;
                }
            }

            string showFilterCount = reader.GetAttribute("showfiltercount");
            if (showFilterCount != null)
            {
                bool parsed = bool.TryParse(showFilterCount, out boolResult);
                Debug.Assert(parsed, "Failed to parse a user created command parameter.");
                if (parsed)
                {
                    this.ShowFilterCount = boolResult;
                }
            }

            string startsGroup = reader.GetAttribute("startsgroup");
            if (startsGroup != null)
            {
                bool parsed = bool.TryParse(startsGroup, out boolResult);
                Debug.Assert(parsed, "Failed to parse a user created command parameter.");
                if (parsed)
                {
                    this.StartsGroup = boolResult;
                }
            }

            this.Text = reader.GetAttribute("text") ?? String.Empty;
            string width = reader.GetAttribute("width");
            if (width != null)
            {
                int result = 0;
                bool parsed = int.TryParse(width, out result);
                Debug.Assert(parsed, "Failed to parse a user created command parameter.");
                if (parsed)
                {
                    this.Width = result;
                }
            }

            string style = reader.GetAttribute("style");
            if (style != null)
            {
                CommandItemDisplayStyle result = CommandItemDisplayStyle.Default;
                bool parsed = Enum.TryParse<CommandItemDisplayStyle>(style, out result);
                Debug.Assert(parsed, "Failed to parse a user created command parameter.");
                if (parsed)
                {
                    this.DisplayStyle = result;
                }
            }

            this._commandName = reader.GetAttribute("command");
        }
        #endregion Methods
    } // RSG.Editor.UserCreatedCommandInstance {Class}
} // RSG.Editor {Namespace}
