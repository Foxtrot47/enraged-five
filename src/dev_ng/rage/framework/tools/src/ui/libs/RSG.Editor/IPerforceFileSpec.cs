﻿//---------------------------------------------------------------------------------------------
// <copyright file="IPerforceFileSpec.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// When implemented contains a perforce file specification that can be represented
    /// as either a client, depot or local path.
    /// </summary>
    public interface IPerforceFileSpec
    {
        /// <summary>
        /// Gets the value that represents the files client path.
        /// </summary>
        string ClientPath { get; }

        /// <summary>
        /// Gets the value that represents the files perforce depot path.
        /// </summary>
        string DepotPath { get; }

        /// <summary>
        /// Gets the value that represents the files local path.
        /// </summary>
        string LocalPath { get; }
    }
}
