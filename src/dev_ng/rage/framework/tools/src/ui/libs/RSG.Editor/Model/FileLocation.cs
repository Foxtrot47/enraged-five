﻿//---------------------------------------------------------------------------------------------
// <copyright file="FileLocation.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Model
{
    using System;

    /// <summary>
    /// Represents a location of a validation result.
    /// </summary>
    public class FileLocation
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="NotApplicable"/> property.
        /// </summary>
        private static readonly FileLocation _notApplicable;

        /// <summary>
        /// The private field used for the <see cref="Column"/> property.
        /// </summary>
        private int _column;

        /// <summary>
        /// The private field used for the <see cref="FullPath"/> property.
        /// </summary>
        private string _fullPath;

        /// <summary>
        /// The private field used for the <see cref="Line"/> property.
        /// </summary>
        private int _line;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="FileLocation"/> class.
        /// </summary>
        static FileLocation()
        {
            _notApplicable = new FileLocation(-1, -1, String.Empty);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="FileLocation"/> class.
        /// </summary>
        /// <param name="line">
        /// The line number for this location.
        /// </param>
        /// <param name="column">
        /// The column index for this location.
        /// </param>
        /// <param name="fullPath">
        /// The full path for this location.
        /// </param>
        public FileLocation(int line, int column, string fullPath)
        {
            this._line = line;
            this._column = column;
            this._fullPath = fullPath;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the instance of this object that represents a empty location that can be set
        /// when the location is not applicable to the situation.
        /// </summary>
        public static FileLocation NotApplicable
        {
            get { return _notApplicable; }
        }

        /// <summary>
        /// Gets or sets the column index for this location.
        /// </summary>
        public int Column
        {
            get { return this._column; }
            set { this._column = value; }
        }

        /// <summary>
        /// Gets or sets the full path to the file this location is representing.
        /// </summary>
        public string FullPath
        {
            get { return this._fullPath; }
            set { this._fullPath = value; }
        }

        /// <summary>
        /// Gets or sets the line number for this location.
        /// </summary>
        public int Line
        {
            get { return this._line; }
            set { this._line = value; }
        }
        #endregion Properties
    } // RSG.Editor.Model.FileLocation {Class}
} // RSG.Editor.Model {Namespace}
