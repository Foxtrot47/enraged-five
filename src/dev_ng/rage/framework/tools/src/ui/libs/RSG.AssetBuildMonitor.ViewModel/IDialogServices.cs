﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssetBuildHostExplorer.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.AssetBuildMonitor.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for showing dialogs specific to the Asset Build Monitor application.
    /// </summary>
    public interface IDialogServices
    {
        /// <summary>
        /// Show the Add New Host dialog box; returning user-specified hostname.
        /// </summary>
        /// <param name="hostname"></param>
        /// <returns></returns>
        bool ShowAddNewHostDialog(out String hostname);
        
        /// <summary>
        /// Show the Files Not Found dialog box.
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <param name="filenames"></param>
        /// <returns></returns>
        bool ShowFileListDialog(String title, String message, IEnumerable<String> filenames);
    }

} // RSG.AssetBuildMonitor.ViewModel namespace
