﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Xml.Linq;
using RSG.Editor.Model;

namespace RSG.Editor.Controls.MapViewport.Annotations
{
    /// <summary>
    /// Represents a collection of <see cref="Annotation"/> objects.
    /// </summary>
    public class AnnotationCollection : ModelCollection<Annotation>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="AnnotationCollection"/> class.
        /// </summary>
        public AnnotationCollection()
            : base()
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="AnnotationCollection"/> class.
        /// </summary>
        /// <param name="undoEngineProvider"></param>
        public AnnotationCollection(IUndoEngineProvider undoEngineProvider)
            : base(undoEngineProvider)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="AnnotationCollection"/> class
        /// using the specified xml element.
        /// </summary>
        /// <param name="element"></param>
        /// <param name="undoEngineProvider"></param>
        public AnnotationCollection(XElement annotationsElement, IUndoEngineProvider undoEngineProvider)
            : base(undoEngineProvider)
        {
            foreach (XElement annotationElement in annotationsElement.Elements("Annotation"))
            {
                Add(new Annotation(annotationElement, undoEngineProvider));
            }
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the undo engine provider instance that this collection uses.
        /// </summary>
        public new IUndoEngineProvider UndoEngineProvider
        {
            get { return base.UndoEngineProvider; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Converts the annotations collection to an xml element.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public XElement ToXElement(string name)
        {
            return new XElement(
                name,
                this.Select(item => item.ToXElement("Annotation")));
        }
        #endregion
    }
}
