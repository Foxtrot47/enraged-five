﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace RSG.Editor.Controls.CurveEditor
{
    /// <summary>
    /// Used for displaying an arbitrary set of grid lines.
    /// </summary>
    public class GuideLines : FrameworkElement
    {
        #region Cell Size/Location Dependency Properties
        /// <summary>
        /// Dependency property for specifying the size of a single grid cell.
        /// </summary>
        public static readonly DependencyProperty CellSizeProperty =
            DependencyProperty.Register(
                "CellSize",
                typeof(Size),
                typeof(GuideLines),
                new FrameworkPropertyMetadata(new Size(1.0, 1.0), FrameworkPropertyMetadataOptions.AffectsRender));

        /// <summary>
        /// Property to access the cell size for this grid.
        /// </summary>
        public Size CellSize
        {
            get { return (Size)GetValue(CellSizeProperty); }
            set { SetValue(CellSizeProperty, value); }
        }
        
        /// <summary>
        /// Dependency property for specifying the top left offset of a single grid cell.
        /// </summary>
        public static readonly DependencyProperty CellOffsetProperty =
            DependencyProperty.Register(
                "CellOffset",
                typeof(Point),
                typeof(GuideLines),
                new FrameworkPropertyMetadata(new Point(0.0, 0.0), FrameworkPropertyMetadataOptions.AffectsRender));

        /// <summary>
        /// Property to access the grid cell offset.
        /// </summary>
        public Point CellOffset
        {
            get { return (Point)GetValue(CellOffsetProperty); }
            set { SetValue(CellOffsetProperty, value); }
        }
        #endregion // Cell Size Dependency Properties

        #region Render Style Dependency Properties
        /// <summary>
        /// Dependency property for specifying the grid line stroke brush.
        /// </summary>
        public static readonly DependencyProperty StrokeProperty =
            DependencyProperty.Register(
                "Stroke",
                typeof(Brush),
                typeof(GuideLines),
                new FrameworkPropertyMetadata(Brushes.Transparent, FrameworkPropertyMetadataOptions.AffectsRender, OnRenderStyleChanged));

        /// <summary>
        /// Property to access the stroke brush.
        /// </summary>
        public Brush Stroke
        {
            get { return (Brush)GetValue(StrokeProperty); }
            set { SetValue(StrokeProperty, value); }
        }

        /// <summary>
        /// Dependency property for specifying the grid line stroke thickness.
        /// </summary>
        public static readonly DependencyProperty StrokeThicknessProperty =
            DependencyProperty.Register(
                "StrokeThickness",
                typeof(double),
                typeof(GuideLines),
                new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.AffectsRender, OnRenderStyleChanged));

        /// <summary>
        /// Property to access the stroke thickness.
        /// </summary>
        public double StrokeThickness
        {
            get { return (double)GetValue(StrokeThicknessProperty); }
            set { SetValue(StrokeThicknessProperty, value); }
        }

        /// <summary>
        /// Called whenever one of the pen related render properties change.
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnRenderStyleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as GuideLines).UpdatePen();
        }

        /// <summary>
        /// Updates the pen that is cached.
        /// </summary>
        public void UpdatePen()
        {
            _cachedPen = new Pen(this.Stroke, this.StrokeThickness);
        }

        /// <summary>
        /// Pen to use while rendering.
        /// </summary>
        private Pen _cachedPen;
        #endregion // Render Style Dependency Properties

        #region Overrides
        /// <summary>
        /// Override the rendering to create a brush for a single cell of the grid.
        /// </summary>
        /// <param name="drawingContext"></param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            /*
             * Using a DrawBrush would be the most efficient way to do this, however I encountered
             * issues whereby the grid line was no longer a single pixel wide at most zoom levels.
             * 
            // Create the drawing brush that will contain our grid tile
            DrawingBrush brush = new DrawingBrush();
            brush.Viewport = new Rect(0, 0, CellWidth, CellHeight);
            brush.ViewportUnits = BrushMappingMode.Absolute;
            brush.Stretch = Stretch.None;
            brush.TileMode = TileMode.Tile;

            // Compute the offset of where the lines should appear on the brush viewport.
            double xOffset = -CellOffset.X % CellWidth;
            double yOffset = -CellOffset.Y % CellHeight;

            if (xOffset < 0.0)
            {
                xOffset += CellWidth;
            }
            if (yOffset < 0.0)
            {
                yOffset += CellHeight;
            }

            GeometryGroup geomGroup = new GeometryGroup();
            geomGroup.Children.Add(new LineGeometry(new Point(0.0, RoundToPoint5(yOffset)), new Point(CellWidth, RoundToPoint5(yOffset))));
            geomGroup.Children.Add(new LineGeometry(new Point(RoundToPoint5(xOffset), 0.0), new Point(RoundToPoint5(xOffset), CellHeight)));

            // Set up the geometry and add it to the brush.
            GeometryDrawing geomDrawing = new GeometryDrawing();
            geomDrawing.Geometry = geomGroup;
            geomDrawing.Pen = _cachedPen;

            brush.Drawing = geomDrawing;
            brush.Freeze();
            
            // Finally draw a rectangle that fills the control's render size using the brush we've created.
            drawingContext.DrawRectangle(brush, _cachedPen, new Rect(0.0, 0.0, this.RenderSize.Width, this.RenderSize.Height));
            */

            // Compute the start offset of where the lines should appear.
            double xOffset = -CellOffset.X % CellSize.Width;
            double yOffset = -CellOffset.Y % CellSize.Height;

            if (xOffset < 0.0)
            {
                xOffset += CellSize.Width;
            }
            if (yOffset < 0.0)
            {
                yOffset += CellSize.Height;
            }

            // Draw the vertical lines first.
            for (double x = xOffset; x < this.ActualWidth; x += CellSize.Width)
            {
                drawingContext.DrawLine(_cachedPen, new Point(RoundToPoint5(x), 0.0), new Point(RoundToPoint5(x), this.ActualHeight));
            }

            // Draw the horizontal lines next.
            for (double y = yOffset; y < this.ActualHeight; y += CellSize.Height)
            {
                drawingContext.DrawLine(_cachedPen, new Point(0.0, RoundToPoint5(y)), new Point(this.ActualWidth, RoundToPoint5(y)));
            }
        }

        /// <summary>
        /// I have a feeling this should be taking the pen thickness into account instead of
        /// just using 0.5.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private double RoundToPoint5(double value)
        {
            return Math.Round(value - 0.5, MidpointRounding.AwayFromZero) + 0.5;
        }
        #endregion // Overrides
    } // GridLines
}