﻿//---------------------------------------------------------------------------------------------
// <copyright file="ConversationColumnVisibilities.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.View
{
    using System.Windows;
    using RSG.Editor;

    /// <summary>
    /// Contains the visibility states for the columns inside the conversation user control.
    /// </summary>
    public class ConversationColumnVisibilities : NotifyPropertyChangedBase
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="AnimTriggeredColumn"/> property.
        /// </summary>
        private Visibility _animTriggeredColumn;

        /// <summary>
        /// The private field used for the <see cref="CutsceneSubtitlesColumn"/> property.
        /// </summary>
        private Visibility _cutsceneSubtitlesColumn;

        /// <summary>
        /// The private field used for the <see cref="InterruptibleColumn"/> property.
        /// </summary>
        private Visibility _interruptibleColumn;

        /// <summary>
        /// The private field used for the <see cref="PlaceholderColumn"/> property.
        /// </summary>
        private Visibility _placeholderColumn;

        /// <summary>
        /// The private field used for the <see cref="RandomColumn"/> property.
        /// </summary>
        private Visibility _randomColumn;

        /// <summary>
        /// The private field used for the <see cref="SubtitleTypeColumn"/> property.
        /// </summary>
        private Visibility _subtitleTypeColumn;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ConversationColumnVisibilities"/>
        /// class.
        /// </summary>
        public ConversationColumnVisibilities()
        {
            this._animTriggeredColumn = Visibility.Collapsed;
            this._cutsceneSubtitlesColumn = Visibility.Collapsed;
            this._interruptibleColumn = Visibility.Visible;
            this._placeholderColumn = Visibility.Collapsed;
            this._randomColumn = Visibility.Visible;
            this._subtitleTypeColumn = Visibility.Visible;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the visibility for the animation triggered data grid column.
        /// </summary>
        public Visibility AnimTriggeredColumn
        {
            get { return this._animTriggeredColumn; }
            set { this.SetProperty(ref this._animTriggeredColumn, value); }
        }

        /// <summary>
        /// Gets or sets the visibility for the cut-scene subtitles data grid column.
        /// </summary>
        public Visibility CutsceneSubtitlesColumn
        {
            get { return this._cutsceneSubtitlesColumn; }
            set { this.SetProperty(ref this._cutsceneSubtitlesColumn, value); }
        }

        /// <summary>
        /// Gets or sets the visibility for the interruptible data grid column.
        /// </summary>
        public Visibility InterruptibleColumn
        {
            get { return this._interruptibleColumn; }
            set { this.SetProperty(ref this._interruptibleColumn, value); }
        }

        /// <summary>
        /// Gets or sets the visibility for the placeholder data grid column.
        /// </summary>
        public Visibility PlaceholderColumn
        {
            get { return this._placeholderColumn; }
            set { this.SetProperty(ref this._placeholderColumn, value); }
        }

        /// <summary>
        /// Gets or sets the visibility for the random data grid column.
        /// </summary>
        public Visibility RandomColumn
        {
            get { return this._randomColumn; }
            set { this.SetProperty(ref this._randomColumn, value); }
        }

        /// <summary>
        /// Gets or sets the visibility for the subtitle type data grid column.
        /// </summary>
        public Visibility SubtitleTypeColumn
        {
            get { return this._subtitleTypeColumn; }
            set { this.SetProperty(ref this._subtitleTypeColumn, value); }
        }
        #endregion Properties
    } // RSG.Text.View.View.ConversationColumnVisibilities {Class}
} // RSG.Text.View.View {Namespace}
