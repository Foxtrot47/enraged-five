﻿//---------------------------------------------------------------------------------------------
// <copyright file="IProjectAddViewService.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel
{
    using System.Collections.Generic;
    using RSG.Editor;
    using RSG.Editor.Controls.Dock;
    using RSG.Project.ViewModel.Definitions;

    /// <summary>
    /// Provides functionality to show the views specific to the project add commands.
    /// </summary>
    public interface IProjectAddViewService
    {
        #region Methods
        /// <summary>
        /// Shows a modal window which displays the progress of the update for a set of files.
        /// </summary>
        /// <param name="project">
        /// The project that the updated files will be added to.
        /// </param>
        /// <param name="filesToUpdate">
        /// A list of full paths to the files that need updating.
        /// </param>
        /// <returns>
        /// The full path to all of the updated files.
        /// </returns>
        IReadOnlyList<string> ShowFileUpdatingProgressWindow(
            ProjectNode project, List<string> filesToUpdate);

        /// <summary>
        /// Shows the new item selector dialog window.
        /// </summary>
        /// <param name="project">
        /// The project whose item definitions should be available to select.
        /// </param>
        /// <param name="initialLocation">
        /// The initial location for the new item.
        /// </param>
        /// <param name="initialName">
        /// The initial name for the new item.
        /// </param>
        /// <param name="selected">
        /// The item that the user selected, or null if the user has chosen to cancel.
        /// </param>
        /// <param name="fullPath">
        /// The full path where the user has chosen the new item to be located.
        /// </param>
        void ShowNewItemSelector(
            ProjectNode project,
            string initialLocation,
            string initialName,
            out ProjectItemDefinition selected,
            out string fullPath);

        /// <summary>
        /// Shows the new project selector dialog window.
        /// </summary>
        /// <param name="collection">
        /// The project collection whose project definitions should be available to select.
        /// </param>
        /// <param name="initialLocation">
        /// The initial location for the new project.
        /// </param>
        /// <param name="selected">
        /// The project that the user selected, or null if the user has chosen to cancel.
        /// </param>
        /// <param name="fullPath">
        /// The full path where the user has chosen the new project to be located.
        /// </param>
        void ShowNewProjectSelector(
            ProjectCollectionNode collection,
            string initialLocation,
            out ProjectDefinition selected,
            out string fullPath);

        /// <summary>
        /// Shows the new project selector dialog window.
        /// </summary>
        /// <param name="collection">
        /// The project collection whose project definitions should be available to select.
        /// </param>
        /// <param name="addToCurrentCollection">
        /// A value indicating whether the new project is being added to the current collection
        /// or a new collection should be created.
        /// </param>
        /// <param name="selected">
        /// The project that the user selected, or null if the user has chosen to cancel.
        /// </param>
        /// <param name="fullPath">
        /// The full path where the user has chosen the new project to be located.
        /// </param>
        void ShowNewProjectSelector(
            ProjectCollectionNode collection,
            out bool addToCurrentCollection,
            out ProjectDefinition selected,
            out string fullPath);

        /// <summary>
        /// Shows the add dynamic folder dialog window.
        /// </summary>
        /// <param name="dlgService">
        /// The common dialog service.
        /// </param>
        /// <param name="path">
        /// The full path to the selected folder.
        /// </param>
        /// <param name="filters">
        /// The list of filters the user has selected.
        /// </param>
        /// <param name="recursive">
        /// A value indicating whether this is going to be recursive.
        /// </param>
        bool ShowAddNewDynamicFolder(
            ICommonDialogService dlgService,
            out string path,
            out string filters,
            out bool recursive);
        #endregion Methods
    } // RSG.Project.ViewModel.IProjectAddViewService {Interface}
} // RSG.Project.ViewModel
