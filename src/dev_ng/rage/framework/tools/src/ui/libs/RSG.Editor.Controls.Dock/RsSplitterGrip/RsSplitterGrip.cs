﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsSplitterGrip.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock
{
    using System.Windows;
    using System.Windows.Automation;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;

    /// <summary>
    /// Represents a grip control that can be used to resize individual
    /// <see cref="RsSplitterItem"/> controls inside a <see cref="RsSplitterPanel"/> control.
    /// </summary>
    public class RsSplitterGrip : Thumb
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="Orientation"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty OrientationProperty;

        /// <summary>
        /// Identifies the <see cref="ResizeBehaviour"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ResizeBehaviourProperty;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsSplitterGrip"/> class.
        /// </summary>
        static RsSplitterGrip()
        {
            OrientationProperty =
                DependencyProperty.Register(
                "Orientation",
                typeof(Orientation),
                typeof(RsSplitterGrip),
                new FrameworkPropertyMetadata(Orientation.Vertical));

            ResizeBehaviourProperty =
                DependencyProperty.Register(
                "ResizeBehaviour",
                typeof(GridResizeBehavior),
                typeof(RsSplitterGrip),
                new FrameworkPropertyMetadata(GridResizeBehavior.CurrentAndNext));

            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsSplitterGrip), new FrameworkPropertyMetadata(typeof(RsSplitterGrip)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsSplitterGrip"/> class.
        /// </summary>
        public RsSplitterGrip()
        {
            AutomationProperties.SetAutomationId(this, "RsSplitterGrip");
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the orientation for the splitter grip which defines the cursor
        /// shown while the mouse is over the grip.
        /// </summary>
        public Orientation Orientation
        {
            get { return (Orientation)this.GetValue(OrientationProperty); }
            set { this.SetValue(OrientationProperty, value); }
        }

        /// <summary>
        /// Gets or sets which columns or rows are resized relative to this grip.
        /// </summary>
        public GridResizeBehavior ResizeBehaviour
        {
            get { return (GridResizeBehavior)this.GetValue(ResizeBehaviourProperty); }
            set { this.SetValue(ResizeBehaviourProperty, value); }
        }
        #endregion Properties
    } // RSG.Editor.Controls.Dock.RsSplitterGrip {Class}
} // RSG.Editor.Controls.Dock {Namespace}
