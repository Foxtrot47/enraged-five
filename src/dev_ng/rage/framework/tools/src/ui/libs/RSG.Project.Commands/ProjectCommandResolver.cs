﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProjectCommandResolver.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Commands
{
    using RSG.Editor;

    /// <summary>
    /// Represents the method that is used to retrieve a command parameter object of the
    /// specified type for the specified command.
    /// </summary>
    /// <param name="commandData">
    /// The data that was sent with the command containing among other things the command
    /// whose command parameter needs to be resolved.
    /// </param>
    /// <returns>
    /// The command parameter object of the specified type for the specified command.
    /// </returns>
    public delegate ProjectCommandArgs ProjectCommandResolver(CommandData commandData);
} // RSG.Project.Commands {Namespace}
