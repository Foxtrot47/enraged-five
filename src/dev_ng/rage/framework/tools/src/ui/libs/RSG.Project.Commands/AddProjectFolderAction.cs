﻿//---------------------------------------------------------------------------------------------
// <copyright file="AddProjectFolderAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.Commands
{
    using System.Linq;
    using RSG.Editor;
    using RSG.Project.ViewModel;

    /// <summary>
    /// Contains the logic for the <see cref="ProjectCommands.AddProjectFolder"/> routed
    /// command. This class cannot be inherited.
    /// </summary>
    public sealed class AddProjectFolderAction : ButtonAction<ProjectCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="AddProjectFolderAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public AddProjectFolderAction(ProjectCommandResolver resolver)
            : base(new ParameterResolverDelegate<ProjectCommandArgs>(resolver))
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ProjectCommandArgs args)
        {
            if (args == null)
            {
                return false;
            }

            if (args.SelectedNodes != null && args.SelectionCount == 1)
            {
                IHierarchyNode node = args.SelectedNodes.FirstOrDefault();
                if (node != null && node.CanHaveProjectFoldersAdded)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(ProjectCommandArgs args)
        {
            IHierarchyNode node = args.SelectedNodes.FirstOrDefault();
            if (node == null)
            {
                return;
            }

            IHierarchyNode folderNode = node.AddNewFolder();
            folderNode.ModifiedScopeNode.IsModified = true;
            if (args.NodeSelectionDelegate != null)
            {
                args.NodeSelectionDelegate(folderNode);
            }

            if (args.NodeRenameDelegate != null)
            {
                args.NodeRenameDelegate(folderNode);
            }
        }
        #endregion Methods
    } // RSG.Project.Commands.AddProjectFolderAction {Class}
} // RSG.Project.Commands {Namespace}
