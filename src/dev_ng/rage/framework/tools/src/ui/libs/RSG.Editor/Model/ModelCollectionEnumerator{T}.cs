﻿//---------------------------------------------------------------------------------------------
// <copyright file="ModelCollectionEnumerator{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Model
{
    using System.Collections;
    using System.Collections.Generic;

    /// <summary>
    /// The enumerator that is used to iterate over a
    /// <see cref="RSG.Editor.Model.ModelCollection{T}"/> class.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the items that the enumerator will be iterating through.
    /// </typeparam>
    internal class ModelCollectionEnumerator<T> : IEnumerator<T>
    {
        #region Fields
        /// <summary>
        /// The private un-typed enumerator to warp around.
        /// </summary>
        private readonly IEnumerator _enumerator;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ModelCollectionEnumerator{T}"/> class.
        /// </summary>
        /// <param name="enumerator">
        /// The un-typed enumerator to cast from.
        /// </param>
        public ModelCollectionEnumerator(IEnumerator enumerator)
        {
            this._enumerator = enumerator;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the element in the collection at the current position of the enumerator.
        /// </summary>
        public T Current
        {
            get { return (T)this._enumerator.Current; }
        }

        /// <summary>
        /// Gets the current element in the collection.
        /// </summary>
        object IEnumerator.Current
        {
            get { return this.Current; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or
        /// resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
        }

        /// <summary>
        /// Advances the enumerator to the next element  of the collection.
        /// </summary>
        /// <returns>
        /// True if the enumerator was successfully advanced to the next element; false if
        /// the enumerator has passed the end of the collection.
        /// </returns>
        public bool MoveNext()
        {
            return this._enumerator.MoveNext();
        }

        /// <summary>
        /// Sets the enumerator to its initial position, which is before the first element
        /// in the collection.
        /// </summary>
        public void Reset()
        {
            this._enumerator.Reset();
        }
        #endregion Methods
    } // RSG.Editor.Model.ModelCollectionEnumerator{T} {Class}
} // RSG.Editor.Model {Namespace}
