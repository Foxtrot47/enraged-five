﻿//---------------------------------------------------------------------------------------------
// <copyright file="TextBoxProperties.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.AttachedDependencyProperties
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using RSG.Editor.Controls.Helpers;

    /// <summary>
    /// Contains attached properties that are related to the textbox.
    /// </summary>
    public static class TextBoxProperties
    {
        #region Fields
        /// <summary>
        /// Identifies the Active dependency property.
        /// </summary>
        public static readonly DependencyProperty ActiveProperty;

        /// <summary>
        /// Identifies the SuppressSystemUndo dependency property.
        /// </summary>
        public static readonly DependencyProperty SuppressSystemUndoProperty;

        /// <summary>
        /// The private key binding, binding the redo key gesture to the application
        /// "NotACommand" command.
        /// </summary>
        private static KeyBinding _redoBinding;

        /// <summary>
        /// The private key binding, binding the undo key gesture to the application
        /// "NotACommand" command.
        /// </summary>
        private static KeyBinding _undoBinding;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="TextBoxProperties"/> class.
        /// </summary>
        static TextBoxProperties()
        {
            _undoBinding = new KeyBinding(
                ApplicationCommands.NotACommand,
                new KeyGesture(Key.Z, ModifierKeys.Control));

            _redoBinding = new KeyBinding(
                ApplicationCommands.NotACommand,
                new KeyGesture(Key.Y, ModifierKeys.Control));

            ActiveProperty =
                DependencyProperty.RegisterAttached(
                "Active",
                typeof(bool),
                typeof(TextBoxProperties),
                new PropertyMetadata(false, ActivePropertyChanged));

            SuppressSystemUndoProperty =
                DependencyProperty.RegisterAttached(
                "SuppressSystemUndo",
                typeof(bool),
                typeof(TextBoxProperties),
                new FrameworkPropertyMetadata(
                    false,
                    FrameworkPropertyMetadataOptions.Inherits,
                    SuppressSystemUndoPropertyChanged));
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Retrieves the Active dependency property value attached to the specified object.
        /// </summary>
        /// <param name="element">
        /// The object whose attached Active dependency property value will be returned.
        /// </param>
        /// <returns>
        /// The Active dependency property value attached to the specified object.
        /// </returns>
        [AttachedPropertyBrowsableForChildrenAttribute(IncludeDescendants = false)]
        [AttachedPropertyBrowsableForType(typeof(TextBox))]
        public static bool GetActive(DependencyObject element)
        {
            return (bool)element.GetValue(ActiveProperty);
        }

        /// <summary>
        /// Retrieves the SuppressSystemUndo dependency property value attached to the
        /// specified object.
        /// </summary>
        /// <param name="element">
        /// The object whose attached SuppressSystemUndo dependency property value will be
        /// returned.
        /// </param>
        /// <returns>
        /// The SuppressSystemUndo dependency property value attached to the specified object.
        /// </returns>
        public static bool GetSuppressSystemUndo(DependencyObject element)
        {
            return (bool)element.GetValue(SuppressSystemUndoProperty);
        }

        /// <summary>
        /// Sets the Active dependency property value on the specified object to the specified
        /// value.
        /// </summary>
        /// <param name="element">
        /// The object whose attached Active dependency property value will be set.
        /// </param>
        /// <param name="value">
        /// The value to set the attached Active dependency property to on the specified
        /// object.
        /// </param>
        public static void SetActive(DependencyObject element, bool value)
        {
            element.SetValue(ActiveProperty, value);
        }

        /// <summary>
        /// Sets the SuppressSystemUndo dependency property value on the specified object to
        /// the specified
        /// value.
        /// </summary>
        /// <param name="element">
        /// The object whose attached SuppressSystemUndo dependency property value will be set.
        /// </param>
        /// <param name="value">
        /// The value to set the attached SuppressSystemUndo dependency property to on the
        /// specified object.
        /// </param>
        public static void SetSuppressSystemUndo(DependencyObject element, bool value)
        {
            element.SetValue(SuppressSystemUndoProperty, value);
        }

        /// <summary>
        /// Called whenever the Active dependency property changes.
        /// </summary>
        /// <param name="d">
        /// The object whose Active dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void ActivePropertyChanged(
            DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TextBox textBox = d as TextBox;
            if (textBox == null)
            {
                return;
            }

            if ((e.NewValue as bool?).GetValueOrDefault(false))
            {
                textBox.GotKeyboardFocus += OnGotKeyboardFocus;
                textBox.PreviewMouseLeftButtonDown += OnMouseLeftButtonDown;
            }
            else
            {
                textBox.GotKeyboardFocus -= OnGotKeyboardFocus;
                textBox.PreviewMouseLeftButtonDown -= OnMouseLeftButtonDown;
            }
        }

        /// <summary>
        /// Called whenever the text box receives keyboard focus.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.KeyboardFocusChangedEventArgs data for this event.
        /// </param>
        private static void OnGotKeyboardFocus(object s, KeyboardFocusChangedEventArgs e)
        {
            TextBox textBox = e.OriginalSource as TextBox;
            if (textBox != null)
            {
                textBox.SelectAll();
            }
        }

        /// <summary>
        /// Called whenever the left mouse button is pressed while being over the text box.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.MouseButtonEventArgs data for this event.
        /// </param>
        private static void OnMouseLeftButtonDown(object s, MouseButtonEventArgs e)
        {
            TextBox textBox = (s as DependencyObject).GetVisualAncestorOrSelf<TextBox>();
            if (textBox == null)
            {
                return;
            }

            if (!textBox.IsKeyboardFocusWithin)
            {
                textBox.Focus();
                e.Handled = true;
            }
        }

        /// <summary>
        /// Called whenever the SuppressSystemUndo dependency property changes.
        /// </summary>
        /// <param name="d">
        /// The object whose SuppressSystemUndo dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void SuppressSystemUndoPropertyChanged(
            DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TextBox textBox = d as TextBox;
            if (textBox == null)
            {
                return;
            }

            textBox.IsUndoEnabled = false;
            textBox.UndoLimit = 0;
            textBox.InputBindings.Add(_undoBinding);
            textBox.InputBindings.Add(_redoBinding);
        }
        #endregion Methods
    } // RSG.Editor.Controls.AttachedDependencyProperties.TextBoxProperties {Class}
} // RSG.Editor.Controls.AttachedDependencyProperties {Namespace}
