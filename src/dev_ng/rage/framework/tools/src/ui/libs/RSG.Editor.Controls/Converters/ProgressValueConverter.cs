﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProgressValueConverter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Converters
{
    using System.Globalization;
    using System.Windows.Controls;

    /// <summary>
    /// A converter used to convert a progress bar and it's value to the width or height of
    /// the indicator track used for it.
    /// </summary>
    public sealed class ProgressValueConverter
        : ValueConverter<ProgressBar, double, double, double>
    {
        #region Methods
        /// <summary>
        /// Converters the specified values into a double that represents the length of the
        /// progress bar indicator.
        /// </summary>
        /// <param name="progressBar">
        /// The progress bar that contains the min and max values for the indicator.
        /// </param>
        /// <param name="trackSize">
        /// The actual length of the track that the progress bar indicator is contained in.
        /// </param>
        /// <param name="value">
        /// The actual current value of the progress bar that determines the length of the
        /// indicator.
        /// </param>
        /// <param name="parameter">
        /// The converter parameter to use (optional a colour specifying the grey-scale bias
        /// weights.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A target value created by converting the two specified source values.
        /// </returns>
        protected override double Convert(
            ProgressBar progressBar,
            double trackSize,
            double value,
            object parameter,
            CultureInfo culture)
        {
            double maximum = progressBar.Maximum;
            double minimum = progressBar.Minimum;
            if (progressBar.IsIndeterminate || maximum <= minimum)
            {
                return trackSize;
            }

            double num = (value - minimum) / (maximum - minimum);
            return num * trackSize;
        }
        #endregion Methods
    } // RSG.Editor.Controls.Converters.ProgressValueConverter {Class}
} // RSG.Editor.Controls.Converters {Namespace}
