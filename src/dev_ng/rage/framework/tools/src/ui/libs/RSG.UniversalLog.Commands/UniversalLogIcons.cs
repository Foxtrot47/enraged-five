﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace RSG.UniversalLog.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public static class UniversalLogIcons
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Email"/> property.
        /// </summary>
        private static BitmapSource _email;

        /// <summary>
        /// A generic object that provides thread safety access to the image sources.
        /// </summary>
        private static object _syncRoot = new object();
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets the icon that shows a extract icon.
        /// </summary>
        public static BitmapSource Email
        {
            get
            {
                if (_email == null)
                {
                    lock (_syncRoot)
                    {
                        if (_email == null)
                        {
                            _email = EnsureLoaded("email16cyan.png");
                        }
                    }
                }

                return _email;
            }
        }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Ensures that the specified image source is loaded with the specified resource name.
        /// </summary>
        /// <param name="resourceName">
        /// The name of the resource the loaded image source should set as its source.
        /// </param>
        private static BitmapSource EnsureLoaded(String resourceName)
        {
            String assemblyName = typeof(UniversalLogIcons).Assembly.GetName().Name;
            String bitmapPath = String.Format("pack://application:,,,/{0};component/Resources/{1}", assemblyName, resourceName);
            Uri uri = new Uri(bitmapPath, UriKind.RelativeOrAbsolute);

            BitmapSource source = new BitmapImage(uri);
            source.Freeze();
            return source;
        }
        #endregion //  Methods
    } // UniversalLogViewerIcons
}
