﻿//---------------------------------------------------------------------------------------------
// <copyright file="TunableStructureControl.xaml.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.View
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Xml;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Editor;
    using RSG.Editor.Controls;
    using RSG.Editor.Controls.AttachedDependencyProperties;
    using RSG.Editor.Controls.Helpers;
    using RSG.Editor.Model;
    using RSG.Editor.SharedCommands;
    using RSG.Editor.View;
    using RSG.Metadata.Commands;
    using RSG.Metadata.Model.Tunables;
    using RSG.Metadata.ViewModel.Tunables;

    /// <summary>
    /// Represents the control that is used to display a tunable structure object and its child
    /// tunables in a hierarchical tree structure.
    /// </summary>
    public partial class TunableStructureControl : RsUserControl
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="TunableStructure"/> dependency property.
        /// </summary>
        public static DependencyProperty TunableStructureProperty;

        /// <summary>
        /// The private reference to the add new item list command definition that gets updated
        /// when the selection changes.
        /// </summary>
        private static AddNewMetadataItemDefinition _addNewMetadataDefinition;

        /// <summary>
        /// The private reference to the add new item list command definition that gets updated
        /// when the selection changes.
        /// </summary>
        private static InsertNewMetadataItemAboveDefinition _insertNewMetadataAboveDefinition;

        /// <summary>
        /// The private reference to the add new item list command definition that gets updated
        /// when the selection changes.
        /// </summary>
        private static InsertNewMetadataItemBelowDefinition _insertNewMetadataBelowDefinition;

        /// <summary>
        /// The private reference to the change pointer list command definition that gets
        /// updated when the selection changes.
        /// </summary>
        private static ChangePointerTypeListDefinition _changePointerTypeDefinition;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="TunableStructureControl"/> class.
        /// </summary>
        static TunableStructureControl()
        {
            TunableStructureProperty =
                DependencyProperty.Register(
                "TunableStructure",
                typeof(TunableStructureViewModel),
                typeof(TunableStructureControl),
                new FrameworkPropertyMetadata(null));

            _changePointerTypeDefinition = new ChangePointerTypeListDefinition();
            RockstarCommandManager.AddCommandDefinition(_changePointerTypeDefinition);

            _addNewMetadataDefinition = new AddNewMetadataItemDefinition();
            RockstarCommandManager.AddCommandDefinition(_addNewMetadataDefinition);

            _insertNewMetadataAboveDefinition = new InsertNewMetadataItemAboveDefinition();
            RockstarCommandManager.AddCommandDefinition(_insertNewMetadataAboveDefinition);

            _insertNewMetadataBelowDefinition = new InsertNewMetadataItemBelowDefinition();
            RockstarCommandManager.AddCommandDefinition(_insertNewMetadataBelowDefinition);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="TunableStructureControl"/> class.
        /// </summary>
        public TunableStructureControl()
        {
            this.InitializeComponent();
            this.TreeListView.GetContextMenuIdCallback = this.GetContextMenuId;

            this.AttachCommandBindings();

            EnhancedFocusScope.SetIsEnhancedFocusScope(this, true);
            EnhancedFocusScope.SetInitialFocusedElement(this, this.TreeListView);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="TunableStructureControl"/> class.
        /// </summary>
        /// <param name="tunableStructure">
        /// The tunable structure view model that this control is displaying.
        /// </param>
        public TunableStructureControl(TunableStructureViewModel tunableStructure)
        {
            this.DataContext = tunableStructure;
            this.InitializeComponent();
            this.TreeListView.GetContextMenuIdCallback = this.GetContextMenuId;

            this.TunableStructure = tunableStructure;
            this.AttachCommandBindings();

            EnhancedFocusScope.SetIsEnhancedFocusScope(this, true);
            EnhancedFocusScope.SetInitialFocusedElement(this, this.TreeListView);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the tunable structure view model that this control is displaying.
        /// </summary>
        public TunableStructureViewModel TunableStructure
        {
            get { return (TunableStructureViewModel)this.GetValue(TunableStructureProperty); }
            set { this.SetValue(TunableStructureProperty, value); }
        }

        /// <summary>
        /// Gets the array tunable that is determined to be the active one, based on the
        /// current selection.
        /// </summary>
        private ArrayTunable ActiveArray
        {
            get
            {
                RsTreeListView view = this.TreeListView;
                if (view == null)
                {
                    return null;
                }

                ITunableViewModel tunable = view.RealSelectedItem as ITunableViewModel;
                if (view.SelectedItems.Count != 1 || tunable == null)
                {
                    return null;
                }

                ArrayTunable array = null;
                ArrayTunableViewModel arrayViewModel = tunable as ArrayTunableViewModel;
                if (arrayViewModel == null)
                {
                    array = tunable.Model.Parent as ArrayTunable;
                }
                else
                {
                    array = arrayViewModel.Model;
                }

                return array;
            }
        }

        /// <summary>
        /// Gets the dynamic parent tunable that is determined to be the active one, based on
        /// the current selection.
        /// </summary>
        private IDynamicTunableParent ActiveDynamicParent
        {
            get
            {
                RsTreeListView view = this.TreeListView;
                if (view == null)
                {
                    return null;
                }

                ITunableViewModel tunable = view.RealSelectedItem as ITunableViewModel;
                if (view.SelectedItems.Count != 1 || tunable == null)
                {
                    return null;
                }

                IDynamicTunableParent dynamicParent = tunable.Model as IDynamicTunableParent;
                if (dynamicParent == null)
                {
                    dynamicParent = tunable.Model.Parent as IDynamicTunableParent;
                }

                return dynamicParent;
            }
        }

        /// <summary>
        /// Gets the pointer tunable that is determined to be the active one, based on the
        /// current selection.
        /// </summary>
        private PointerTunable ActivePointer
        {
            get
            {
                RsTreeListView view = this.TreeListView;
                if (view == null)
                {
                    return null;
                }

                ITunableViewModel tunable = view.RealSelectedItem as ITunableViewModel;
                if (view.SelectedItems.Count != 1 || tunable == null)
                {
                    return null;
                }

                PointerTunable pointer = null;
                PointerTunableViewModel pointerViewModel = tunable as PointerTunableViewModel;
                if (pointerViewModel != null)
                {
                    pointer = pointerViewModel.Model;
                }

                return pointer;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Attaches all of the necessary command bindings to this control.
        /// </summary>
        private void AttachCommandBindings()
        {
            ICommandAction action = new ResetToDefaultAction(this.CommandResolver);
            action.AddBinding(MetadataCommands.ResetToDefault, this);

            action = new ChangePointerTypeAction(this.CommandResolver);
            action.AddBinding(MetadataCommands.ChangePointerType, this);

            action = new DeleteTunableAction(this.CommandResolver);
            action.AddBinding(RockstarCommands.Delete, this);

            action = new MoveMetadataItemUpAction(this.CommandResolver);
            action.AddBinding(MetadataCommands.MoveMetadataItemUp, this);

            action = new MoveMetadataItemDownAction(this.CommandResolver);
            action.AddBinding(MetadataCommands.MoveMetadataItemDown, this);

            action = new AddNewMetadataItemAction(
                this.CommandResolver, _addNewMetadataDefinition);
            action.AddBinding(MetadataCommands.AddNewMetadataItem, this);

            action = new InsertNewMetadataItemAboveAction(
                this.CommandResolver, _insertNewMetadataAboveDefinition);
            action.AddBinding(MetadataCommands.InsertNewMetadataItemAbove, this);

            action = new InsertNewMetadataItemBelowAction(
                this.CommandResolver, _insertNewMetadataBelowDefinition);
            action.AddBinding(MetadataCommands.InsertNewMetadataItemBelow, this);

            this.CommandBindings.Add(new CommandBinding(RockstarCommands.Copy, this.OnCopy));

            this.CommandBindings.Add(
                new CommandBinding(MetadataCommands.CopyTunableName, this.OnCopyTunableName));

            this.CommandBindings.Add(
                new CommandBinding(MetadataCommands.CopyTunableType, this.OnCopyTunableType));

            this.CommandBindings.Add(new CommandBinding(RockstarCommands.Paste, this.OnPaste));

            action = new ExpandAllAction(this.CommandResolver);
            action.AddBinding(RockstarCommands.ExpandAll, this);

            action = new CollapseAllAction(this.CommandResolver);
            action.AddBinding(RockstarCommands.CollapseAll, this);

            action = new ExpandAction(this.CommandResolver);
            action.AddBinding(RockstarCommands.Expand, this);

            action = new CollapseAction(this.CommandResolver);
            action.AddBinding(RockstarCommands.Collapse, this);
        }

        /// <summary>
        /// The action resolver that returns a instance of the project command args class that
        /// project commands can use to execute their logic.
        /// </summary>
        /// <param name="commandData">
        /// The command data sent with the executing command.
        /// </param>
        /// <returns>
        /// The currently active document item inside the view site.
        /// </returns>
        private MetadataCommandArgs CommandResolver(CommandData commandData)
        {
            MetadataCommandArgs args = new MetadataCommandArgs();
            if (this.TreeListView != null)
            {
                List<ITunableViewModel> selected = new List<ITunableViewModel>();
                foreach (object selectedItem in this.TreeListView.RealSelectedItems)
                {
                    ITunableViewModel vm = selectedItem as ITunableViewModel;
                    if (vm == null)
                    {
                        continue;
                    }

                    selected.Add(vm);
                }

                args.SelectedTunables = selected;
            }

            IViewModel dc = this.DataContext as IViewModel;
            if (dc != null || dc.Model != null)
            {
                args.UndoEngine = dc.Model.UndoEngine;
            }

            args.TunableSelectionDelegate = this.SelectItem;
            args.TunableExpansionDelegate = this.ExpandItem;
            args.TunableCollapseDelegate = this.CollapseItem;
            args.Lock();
            return args;
        }

        /// <summary>
        /// Retrieves the context menu command id to use for the specified item inside the tree
        /// structure for the view.
        /// </summary>
        /// <param name="item">
        /// The item whose command menu id should be returned.
        /// </param>
        /// <returns>
        /// The context menu command id to use for the specified item.
        /// </returns>
        private Guid GetContextMenuId(object item)
        {
            ITunableViewModel tunableViewModel = item as ITunableViewModel;
            if (tunableViewModel == null)
            {
                return Guid.Empty;
            }

            return MetadataCommandIds.TreeListViewItemContextMenu;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void OnCopy(object s, ExecutedRoutedEventArgs e)
        {
            object[] selectedItems = this.TreeListView.RealSelectedItems;
            List<ITunable> selectedTunables = new List<ITunable>();
            foreach (object selectedItem in selectedItems)
            {
                ITunableViewModel tunableViewModel = selectedItem as ITunableViewModel;
                if (tunableViewModel == null)
                {
                    continue;
                }

                bool found = false;
                ITunableViewModel currentParent = tunableViewModel.Parent;
                while (currentParent != null && found == false)
                {
                    foreach (object si in selectedItems)
                    {
                        if (Object.ReferenceEquals(si, currentParent))
                        {
                            found = true;
                            break;
                        }
                    }

                    if (found)
                    {
                        break;
                    }

                    currentParent = currentParent.Parent;
                }

                if (!found)
                {
                    selectedTunables.Add(tunableViewModel.Model as ITunable);
                }
            }

            this.CopyItemsToClipboard(selectedTunables);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void OnCopyTunableName(object s, ExecutedRoutedEventArgs e)
        {
            object[] selectedItems = this.TreeListView.RealSelectedItems;
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < selectedItems.Length; i++)
            {
                ITunableViewModel tunableViewModel = selectedItems[i] as ITunableViewModel;
                if (tunableViewModel == null)
                {
                    continue;
                }

                if (i != 0)
                {
                    sb.AppendLine();
                }

                sb.Append(tunableViewModel.Text);
            }

            Clipboard.Clear();
            Clipboard.SetText(sb.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void OnCopyTunableType(object s, ExecutedRoutedEventArgs e)
        {
            object[] selectedItems = this.TreeListView.RealSelectedItems;
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < selectedItems.Length; i++)
            {
                ITunableViewModel tunableViewModel = selectedItems[i] as ITunableViewModel;
                if (tunableViewModel == null)
                {
                    continue;
                }

                if (i != 0)
                {
                    sb.AppendLine();
                }

                sb.Append(tunableViewModel.TypeValue);
            }

            Clipboard.Clear();
            Clipboard.SetText(sb.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void OnPaste(object s, ExecutedRoutedEventArgs e)
        {
            ITunable selectedTunable = null;
            foreach (object selectedItem in this.TreeListView.RealSelectedItems)
            {
                ITunableViewModel tunableViewModel = selectedItem as ITunableViewModel;
                if (tunableViewModel == null)
                {
                    continue;
                }
                
                selectedTunable = tunableViewModel.Model as ITunable;
                break;
            }

            IDynamicTunableParent parent = selectedTunable.Parent as IDynamicTunableParent;
            if (parent == null)
            {
                parent = selectedTunable as IDynamicTunableParent;
            }

            if (parent == null)
            {
                return;
            }

            using (new UndoRedoBatch(this.TunableStructure.Model.UndoEngine))
            {
                List<string> copiedTunables = this.GetDataFromClipboard();

                XmlReaderSettings settings = new XmlReaderSettings();
                settings.ConformanceLevel = ConformanceLevel.Fragment;
                settings.CloseInput = true;
                foreach (string copiedTunable in copiedTunables.Reverse<string>())
                {
                    StringReader copiedData = new StringReader(copiedTunable);
                    string pscType = null;
                    string mapKey = null;
                    using (XmlReader reader = XmlReader.Create(copiedData, settings))
                    {
                        reader.MoveToContent();
                        reader.ReadStartElement();
                        while (reader.MoveToContent() == XmlNodeType.Element)
                        {
                            if (String.Equals(reader.Name, "PscType"))
                            {
                                pscType = reader.ReadElementContentAsString();
                                continue;
                            }
                            else if (String.Equals(reader.Name, "MapKey"))
                            {
                                mapKey = reader.ReadElementContentAsString();
                                continue;
                            }
                            else if (String.Equals(reader.Name, "Data"))
                            {
                                reader.ReadStartElement();
                                reader.MoveToContent();

                                ILog log = LogFactory.CreateUniversalLog("Paste Content");
                                int index = parent.IndexOf(selectedTunable);
                                if (index == -1)
                                {
                                    index = parent.ItemCount;
                                }
                                else
                                {
                                    index += 1;
                                }

                                ITunable newTunable = parent.InsertNewItem(index, reader, log);
                                MapTunable map = parent as MapTunable;
                                if (map != null)
                                {
                                    map[newTunable] = mapKey;
                                }

                                continue;
                            }

                            reader.Skip();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Retrieves any lines that are currently on the clipboard and adds them to the
        /// specified conversation at the specified index.
        /// </summary>
        /// <returns>
        /// </returns>
        protected List<string> GetDataFromClipboard()
        {
            List<string> copiedTunables = new List<string>();
            IDataObject dataObject = Clipboard.GetDataObject();
            if (dataObject == null)
            {
                return copiedTunables;
            }

            string dataFormat = typeof(List<ITunable>).FullName;
            if (!dataObject.GetDataPresent(dataFormat))
            {
                return copiedTunables;
            }

            string rawData = dataObject.GetData(dataFormat) as string;
            if (rawData == null)
            {
                return copiedTunables;
            }

            XmlReaderSettings settings = new XmlReaderSettings();
            settings.ConformanceLevel = ConformanceLevel.Fragment;
            using (XmlReader reader = XmlReader.Create(new StringReader(rawData), settings))
            {
                reader.MoveToContent();
                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    if (String.Equals(reader.Name, "CopiedTunable"))
                    {
                        copiedTunables.Add(reader.ReadOuterXml());
                        continue;
                    }
                    
                    reader.Skip();
                }
            }

            return copiedTunables;
        }

        /// <summary>
        /// Copies the specified tunables to the clipboard.
        /// </summary>
        /// <param name="tunables">
        /// The tunables to copy to the clipboard.
        /// </param>
        private void CopyItemsToClipboard(IEnumerable<ITunable> tunables)
        {
            Clipboard.Clear();
            StringBuilder sb = new StringBuilder();
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.ConformanceLevel = ConformanceLevel.Fragment;
            using (XmlWriter writer = XmlWriter.Create(new StringWriter(sb), settings))
            {
                foreach (ITunable tunable in tunables)
                {
                    this.CopyTunable(tunable, writer);
                }
            }

            Type type = typeof(List<ITunable>);
            string formatName = type.FullName;
            DataFormat dataFormat = DataFormats.GetDataFormat(formatName);

            DataObject dataObject = new DataObject();

            string data = sb.ToString();
            dataObject.SetData(dataFormat.Name, data, false);
            dataObject.SetData(DataFormats.Text, data, false);
            Clipboard.SetDataObject(dataObject, false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tunable"></param>
        /// <param name="writer"></param>
        private void CopyTunable(ITunable tunable, XmlWriter writer)
        {
            StringBuilder xpathBuilder = new StringBuilder();
            xpathBuilder.Append("/" + this.GetMetadataName(tunable));
            ITunable parent = tunable.Parent as ITunable;
            while (parent != null)
            {
                IDynamicTunableParent dynamic = parent.Parent as IDynamicTunableParent;
                string index = null;
                if (dynamic != null)
                {
                    index = "[" + (dynamic.IndexOf(parent) + 1).ToStringInvariant() + "]";
                }

                string part = "/" + this.GetMetadataName(parent) + index;
                xpathBuilder.Insert(0, part);

                parent = parent.Parent as ITunable;
            }

            xpathBuilder.Insert(0, "/" + tunable.TunableStructure.Definition.MetadataName);

            string basePath = this.TunableStructure.Model.BasePath;
            string parentIndex = "0";
            string pscLocation = tunable.Member.Location.FullPath;
            string pscType = tunable.Member.GetType().Name;

            string datatype = String.Empty;
            StructTunable structTunable = tunable as StructTunable;
            PointerTunable pointerTunable = tunable as PointerTunable;
            if (structTunable != null)
            {
                datatype = structTunable.StructMember.ReferencedStructure.DataType;
            }
            else if (pointerTunable != null)
            {
                if (pointerTunable != null)
                {
                    if (pointerTunable.OwnedStructure != null)
                    {
                        datatype = pointerTunable.OwnedStructure.DataType;
                    }
                    else
                    {
                        datatype = pointerTunable.PointerMember.ReferencedStructure.DataType;
                    }
                }
            }
            else
            {
                datatype = tunable.Member.TypeName;
            }

            string mapKey = null;
            MapTunable mapParent = tunable.Parent as MapTunable;
            if (mapParent != null)
            {
                mapKey = mapParent[tunable];
            }

            IDynamicTunableParent dynamicParent = tunable.Parent as IDynamicTunableParent;
            if (dynamicParent != null)
            {
                int index = dynamicParent.IndexOf(tunable);
                parentIndex = index.ToStringInvariant();
                xpathBuilder.Append("[" + (index + 1).ToStringInvariant() + "]");
            }

            writer.WriteStartElement("CopiedTunable");

            writer.WriteElementString("BasePath", basePath);
            writer.WriteElementString("XPath", xpathBuilder.ToString());
            writer.WriteElementString("DataType", datatype);
            writer.WriteElementString("Name", tunable.Member.Name);
            writer.WriteElementString("MapKey", mapKey);
            writer.WriteElementString("ParentIndex", parentIndex);
            writer.WriteElementString("PscType", pscType);
            writer.WriteElementString("PscLocation", pscLocation);
            writer.WriteStartElement("Data");

            writer.WriteStartElement(this.GetMetadataName(tunable));
            tunable.Serialise(writer, false);
            writer.WriteEndElement();

            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tunable"></param>
        /// <returns></returns>
        private string GetMetadataName(ITunable tunable)
        {
            string name = tunable.Member.MetadataName;
            if (String.IsNullOrWhiteSpace(name))
            {
                name = "Item";
            }

            return name;
        }

        /// <summary>
        /// Called whenever the tree view controls selection changes.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to. The tree view control whose selection
        /// changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Controls.SelectionChangedEventArgs containing the event data
        /// including the selection data.
        /// </param>
        private void OnTreeViewSelectionChanged(object s, SelectionChangedEventArgs e)
        {
            ITunableViewModel selected = null;
            foreach (object selectedItem in this.TreeListView.RealSelectedItems)
            {
                ITunableViewModel vm = selectedItem as ITunableViewModel;
                if (vm == null)
                {
                    continue;
                }

                selected = vm;
                break;
            }

            if (((RsTreeListView)s).SelectedItems.Count != 1 || selected == null)
            {
                _changePointerTypeDefinition.Items.Clear();

                _addNewMetadataDefinition.Items.Clear();
                _insertNewMetadataBelowDefinition.Items.Clear();
                _insertNewMetadataAboveDefinition.Items.Clear();
                return;
            }

            _changePointerTypeDefinition.CreateItemsFromPointer(this.ActivePointer);
            _addNewMetadataDefinition.CreateItems(selected.Model as IDynamicTunableParent);

            IDynamicTunableParent parent = selected.Model.Parent as IDynamicTunableParent;
            _insertNewMetadataBelowDefinition.CreateItems(parent);
            _insertNewMetadataAboveDefinition.CreateItems(parent);
        }

        /// <summary>
        /// Makes sure the specified item is collapsed if found in the tree view.
        /// </summary>
        /// <param name="tunable">
        /// The item to collapse.
        /// </param>
        /// <param name="recursively">
        /// A value indicating whether the collapse is recursive through the whole tree.
        /// </param>
        public void CollapseItem(ITunableViewModel tunable, bool recursively)
        {
            if (tunable == null)
            {
                return;
            }
            
            if (recursively)
            {
                this.TreeListView.CollapseItemRecursively(tunable);
            }
            else
            {
                this.TreeListView.CollapseItem(tunable);
            }
        }

        /// <summary>
        /// Makes sure the specified item is expanded if found in the tree view and makes sure
        /// the tree is expanded correctly.
        /// </summary>
        /// <param name="tunable">
        /// The item to expand.
        /// </param>
        /// <param name="recursively">
        /// A value indicating whether the expansion is recursive through the whole tree.
        /// </param>
        public void ExpandItem(ITunableViewModel tunable, bool recursively)
        {
            if (tunable == null)
            {
                return;
            }

            ITunableViewModel parent = tunable.Parent;
            List<ITunableViewModel> path = new List<ITunableViewModel>();
            while (parent != null)
            {
                path.Insert(0, parent);
                parent = parent.Parent;
            }

            foreach (ITunableViewModel pathItem in path)
            {
                this.TreeListView.ExpandItem(pathItem);
            }

            if (recursively)
            {
                this.TreeListView.ExpandItemRecursively(tunable);
            }
            else
            {
                this.TreeListView.ExpandItem(tunable);
            }
        }

        /// <summary>
        /// Makes sure the specified item is selected if found in the tree view and makes sure
        /// the tree is expanded and scrolled correctly.
        /// </summary>
        /// <param name="tunable">
        /// The item to select.
        /// </param>
        public void SelectItem(ITunableViewModel tunable)
        {
            if (tunable == null)
            {
                return;
            }

            ITunableViewModel parent = tunable.Parent;
            List<ITunableViewModel> path = new List<ITunableViewModel>();
            while (parent != null)
            {
                path.Insert(0, parent);
                parent = parent.Parent;
            }

            foreach (ITunableViewModel pathItem in path)
            {
                this.TreeListView.ExpandItem(pathItem);
            }

            this.TreeListView.SelectItem(tunable);
        }
        #endregion Methods
    } // RSG.Metadata.View.TunableStructureControl {Class}
} // RSG.Metadata.View {Namespace}
