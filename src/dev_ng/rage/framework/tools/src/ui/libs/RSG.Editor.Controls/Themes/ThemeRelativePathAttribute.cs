﻿//---------------------------------------------------------------------------------------------
// <copyright file="ThemeRelativePathAttribute.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Themes
{
    using System;

    /// <summary>
    /// Specifies the theme directory path for a field (used on the application theme enum).
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    public sealed class ThemeRelativePathAttribute : Attribute
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="RelativePath"/> property.
        /// </summary>
        private string _relativePath;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ThemeRelativePathAttribute"/> class
        /// using the specified relative path.
        /// </summary>
        /// <param name="relativePath">
        /// The relative path.
        /// </param>
        public ThemeRelativePathAttribute(string relativePath)
        {
            this._relativePath = relativePath;
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Gets the relative path this attribute holds.
        /// </summary>
        public string RelativePath
        {
            get { return this._relativePath; }
        }
        #endregion Methods
    } // RSG.Editor.Controls.Themes.ThemeRelativePathAttribute {Class}
} // RSG.Editor.Controls.Themes {Namespace}
