﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using RSG.Editor.Controls.ZoomCanvas.Commands;

namespace RSG.Editor.Controls.ZoomCanvas.AttachedProperties
{
    /// <summary>
    /// Contains attached properties that are related to the <see cref="ZoomableCanvas"/>.
    /// </summary>
    public static class ZoomableCanvasProperties
    {
        #region Fields
        /// <summary>
        /// Identifies the MinZoom attached property.
        /// </summary>
        public static readonly DependencyProperty MinZoomProperty =
            DependencyProperty.RegisterAttached(
                "MinZoom",
                typeof(double),
                typeof(ZoomableCanvasProperties),
                new PropertyMetadata(Double.Epsilon, OnMinZoomChanged));

        /// <summary>
        /// Identifies the MinZoom attached property.
        /// </summary>
        public static readonly DependencyProperty MaxZoomProperty =
            DependencyProperty.RegisterAttached(
                "MaxZoom",
                typeof(double),
                typeof(ZoomableCanvasProperties),
                new PropertyMetadata(Double.MaxValue, OnMaxZoomChanged));

        /// <summary>
        /// Identifies the Zoom attached property.
        /// </summary>
        public static readonly DependencyProperty ZoomProperty =
            DependencyProperty.RegisterAttached(
                "Zoom",
                typeof(double),
                typeof(ZoomableCanvasProperties),
                new PropertyMetadata(1.0, OnZoomChanged));
        #endregion

        #region Methods
        /// <summary>
        /// Retrieves the MinZoom dependency property value attached to the
        /// specified object.
        /// </summary>
        /// <param name="element">
        /// The object whose attached MinZoom dependency property value will be
        /// returned.
        /// </param>
        /// <returns>
        /// The MinZoom dependency property value attached to the specified
        /// object.
        /// </returns>
        public static double GetMinZoom(DependencyObject element)
        {
            return (double)element.GetValue(MinZoomProperty);
        }

        /// <summary>
        /// Sets the MinZoom dependency property value on the specified object to
        /// the specified value.
        /// </summary>
        /// <param name="element">
        /// The object whose attached MinZoom dependency property value will be
        /// set.
        /// </param>
        /// <param name="value">
        /// The value to set the attached MinZoom dependency property to on the
        /// specified object.
        /// </param>
        public static void SetMinZoom(DependencyObject element, double value)
        {
            element.SetValue(MinZoomProperty, value);
        }

        /// <summary>
        /// Retrieves the MaxZoom dependency property value attached to the
        /// specified object.
        /// </summary>
        /// <param name="element">
        /// The object whose attached MaxZoom dependency property value will be
        /// returned.
        /// </param>
        /// <returns>
        /// The MaxZoom dependency property value attached to the specified
        /// object.
        /// </returns>
        public static double GetMaxZoom(DependencyObject element)
        {
            return (double)element.GetValue(MaxZoomProperty);
        }

        /// <summary>
        /// Sets the MaxZoom dependency property value on the specified object to
        /// the specified value.
        /// </summary>
        /// <param name="element">
        /// The object whose attached MaxZoom dependency property value will be
        /// set.
        /// </param>
        /// <param name="value">
        /// The value to set the attached MaxZoom dependency property to on the
        /// specified object.
        /// </param>
        public static void SetMaxZoom(DependencyObject element, double value)
        {
            element.SetValue(MaxZoomProperty, value);
        }

        /// <summary>
        /// Retrieves the Zoom dependency property value attached to the
        /// specified object.
        /// </summary>
        /// <param name="element">
        /// The object whose attached Zoom dependency property value will be
        /// returned.
        /// </param>
        /// <returns>
        /// The Zoom dependency property value attached to the specified
        /// object.
        /// </returns>
        public static double GetZoom(DependencyObject element)
        {
            return (double)element.GetValue(ZoomProperty);
        }

        /// <summary>
        /// Sets the Zoom dependency property value on the specified object to
        /// the specified value.
        /// </summary>
        /// <param name="element">
        /// The object whose attached Zoom dependency property value will be
        /// set.
        /// </param>
        /// <param name="value">
        /// The value to set the attached Zoom dependency property to on the
        /// specified object.
        /// </param>
        public static void SetZoom(DependencyObject element, double value)
        {
            element.SetValue(ZoomProperty, value);
        }

        /// <summary>
        /// Called whenever the Zoom dependency property changes.
        /// </summary>
        /// <param name="d">
        /// The object whose Zoom dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnMinZoomChanged(
            DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ZoomableCanvas canvas = d as ZoomableCanvas;
            if (canvas == null)
            {
                throw new NotSupportedException("UniformScale only valid for ZoomableCanvas objects.");
            }
            if (canvas.ScaleMode != ScaleMode.Uniform)
            {
                throw new NotSupportedException("UniformScale only valid for ZoomableCanvas objects which are set to Uniform scale mode.");
            }

            canvas.MinScale = new Vector((double)e.NewValue, (double)e.NewValue);
        }

        /// <summary>
        /// Called whenever the MaxZoom dependency property changes.
        /// </summary>
        /// <param name="d">
        /// The object whose MaxZoom dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnMaxZoomChanged(
            DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ZoomableCanvas canvas = d as ZoomableCanvas;
            if (canvas == null)
            {
                throw new NotSupportedException("MaxZoom only valid for ZoomableCanvas objects.");
            }
            if (canvas.ScaleMode != ScaleMode.Uniform)
            {
                throw new NotSupportedException("MaxZoom only valid for ZoomableCanvas objects which are set to Uniform scale mode.");
            }

            canvas.MaxScale = new Vector((double)e.NewValue, (double)e.NewValue);
        }

        /// <summary>
        /// Called whenever the MinZoom dependency property changes.
        /// </summary>
        /// <param name="d">
        /// The object whose MinZoom dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnZoomChanged(
            DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ZoomableCanvas canvas = d as ZoomableCanvas;
            if (canvas == null)
            {
                throw new NotSupportedException("MinZoom only valid for ZoomableCanvas objects.");
            }
            if (canvas.ScaleMode != ScaleMode.Uniform)
            {
                throw new NotSupportedException("MinZoom only valid for ZoomableCanvas objects which are set to Uniform scale mode.");
            }

            // Zooming is done about the center of the canvas
            Vector oldScale = canvas.Scale;
            Vector newScale = new Vector((double)e.NewValue, (double)e.NewValue);

            //if (newScale != oldScale)
            {
                Vector zoomAmount = new Vector(newScale.X / oldScale.X, newScale.Y / oldScale.Y);// newScale - oldScale + new Vector(1.0, 1.0);

                ZoomCommandArgs args = new ZoomCommandArgs(zoomAmount);
                if (ZoomCanvasCommands.Zoom.CanExecute(args, canvas))
                {
                    ZoomCanvasCommands.Zoom.Execute(args, canvas);
                    CommandManager.InvalidateRequerySuggested();
                }
            }
        }
        #endregion
    }
}
