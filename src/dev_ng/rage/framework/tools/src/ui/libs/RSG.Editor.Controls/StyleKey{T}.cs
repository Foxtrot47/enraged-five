﻿//---------------------------------------------------------------------------------------------
// <copyright file="StyleKey{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System.Windows;

    /// <summary>
    /// Defines or references resource keys based on the specified type parameter as well as
    /// an additional identifier.
    /// </summary>
    /// <typeparam name="T">
    /// Specifies the type that defines the resource key.
    /// </typeparam>
    public class StyleKey<T> : ComponentResourceKey
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="StyleKey{T}"/> class with the
        /// specified ResourceId.
        /// </summary>
        /// <param name="resourceId">
        /// Specifies a unique ID to differentiate this key from others associated with
        /// this type.
        /// </param>
        public StyleKey(string resourceId)
            : base(typeof(T), resourceId)
        {
        }
        #endregion Constructors
    } // RSG.Editor.Controls.StyleKey{T} {Class}
} // RSG.Editor.Controls {Namespace}
