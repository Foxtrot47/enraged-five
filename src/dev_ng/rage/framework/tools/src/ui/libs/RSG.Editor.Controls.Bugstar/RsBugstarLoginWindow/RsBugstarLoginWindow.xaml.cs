﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsBugstarLoginWindow.xaml.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Bugstar
{
    using System;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using RSG.Interop.Bugstar;

    /// <summary>
    /// Interaction logic for the PerforceLoginWindow window control.
    /// </summary>
    internal partial class RsBugstarLoginWindow : RsWindow
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="AttemptCommand"/> property.
        /// </summary>
        private static RoutedCommand _attemptCommand;

        /// <summary>
        /// The number of attempts the user has had to login.
        /// </summary>
        private int _attempts;

        /// <summary>
        /// The delegate that is fired when the user attempts to login.
        /// </summary>
        private BugstarLoginAttemptDelegate _delegate;

        /// <summary>
        /// The private field used for the <see cref="PerforceObject"/> property.
        /// </summary>
        private BugstarConnection _bugstarConnection;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsBugstarLoginWindow"/> class.
        /// </summary>
        static RsBugstarLoginWindow()
        {
            _attemptCommand =
                new RoutedCommand("AttemptCommand", typeof(RsBugstarLoginWindow));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsBugstarLoginWindow"/> class.
        /// </summary>
        public RsBugstarLoginWindow()
        {
            this.InitializeComponent();
            KeyStates state = Keyboard.GetKeyStates(Key.CapsLock);
            if ((state & KeyStates.Toggled) == KeyStates.Toggled)
            {
                this.CapsLockMessage.Visibility = Visibility.Visible;
            }
            else
            {
                this.CapsLockMessage.Visibility = Visibility.Collapsed;
            }

            this.CommandBindings.Add(
                new CommandBinding(AttemptCommand, this.AttemptLogin, this.CanAttemptLogin));
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the command that is fired when the user selects the OK button.
        /// </summary>
        public static RoutedCommand AttemptCommand
        {
            get { return _attemptCommand; }
        }

        /// <summary>
        /// Sets the delegate that is passed the login information that actual does the login.
        /// </summary>
        public BugstarLoginAttemptDelegate LoginDelegate
        {
            set { this._delegate = value; }
        }

        /// <summary>
        /// Gets the password that has been set for this login window.
        /// </summary>
        public string Password
        {
            get { return this.PasswordBox.Password; }
        }

        /// <summary>
        /// Sets the perforce object that this login window is trying to establish a connection
        /// with.
        /// </summary>
        public BugstarConnection BugstarConnection
        {
            set { this._bugstarConnection = value; }
        }

        /// <summary>
        /// Gets or sets the text shown for the username value.
        /// </summary>
        public string Username
        {
            get { return this.UsernameTextBox.Text; }
            set { this.UsernameTextBox.Text = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Called whenever this window receives keyboard focus.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        protected override void OnGotFocus(RoutedEventArgs e)
        {
            base.OnGotFocus(e);
            KeyStates state = Keyboard.GetKeyStates(Key.CapsLock);
            if ((state & KeyStates.Toggled) == KeyStates.Toggled)
            {
                this.CapsLockMessage.Visibility = Visibility.Visible;
            }
            else
            {
                this.CapsLockMessage.Visibility = Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Called whenever a key is pressed down so that we can determine whether the caps
        /// lock message is shown.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.KeyEventArgs containing the event data.
        /// </param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            KeyStates state = Keyboard.GetKeyStates(Key.CapsLock);
            if ((state & KeyStates.Toggled) == KeyStates.Toggled)
            {
                this.CapsLockMessage.Visibility = Visibility.Visible;
            }
            else
            {
                this.CapsLockMessage.Visibility = Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Called when the user executes the AttemptLogin command. This forwards the currently
        /// submitted data to the associated login delegate.
        /// </summary>
        /// <param name="s">
        /// The object that fired the AttemptLogin command.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.ExecutedRoutedEventArgs containing the event data.
        /// </param>
        private async void AttemptLogin(object s, ExecutedRoutedEventArgs e)
        {
            this.SpinControl.IsSpinning = true;
            this.SpinControl.Visibility = System.Windows.Visibility.Visible;
            await Task.Delay(2000);
            this.SpinControl.Visibility = System.Windows.Visibility.Collapsed;
            this.SpinControl.IsSpinning = false;

            if (this._delegate == null)
            {
                throw new MissingMethodException(
                    "Unable to attempt a login without a login delegate");
            }

            if (this._bugstarConnection == null)
            {
                throw new ArgumentNullException(
                    "BugstarConnection",
                    "Unable to attempt a login without a BugstarConnection object");
            }

            BugstarLoginAttemptData args = new BugstarLoginAttemptData(
                this.UsernameTextBox.Text,
                this.PasswordBox.Password,
                true,
                this._attempts++,
                this._bugstarConnection);

            bool success = this._delegate(args);
            if (success)
            {
                this.DialogResult = true;
                return;
            }

            this.ErrorMessage.Text = args.ErrorMessage;
            this.ErrorMessage.Visibility = System.Windows.Visibility.Visible;
            if (!args.Retry)
            {
                this.DialogResult = false;
                return;
            }
        }

        /// <summary>
        /// Determines whether the AttemptLogin command can be executed by the user. We don't
        /// want the user to try and login before they have entered any data.
        /// </summary>
        /// <param name="s">
        /// The object that fired the AttemptLogin command.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.CanExecuteRoutedEventArgs containing the event data.
        /// </param>
        private void CanAttemptLogin(object s, CanExecuteRoutedEventArgs e)
        {
            if (this._delegate == null)
            {
                e.CanExecute = false;
                return;
            }

            if (String.IsNullOrWhiteSpace(this.UsernameTextBox.Text))
            {
                e.CanExecute = false;
                return;
            }

            if (String.IsNullOrWhiteSpace(this.PasswordBox.Password))
            {
                e.CanExecute = false;
                return;
            }

            e.CanExecute = true;
        }
        #endregion Methods
    } // RSG.Editor.Controls.Perforce.RsPerforceLoginWindow {Class}
} // RSG.Editor.Controls.Perforce {Namespace}
