﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsComboBoxItem.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------
namespace RSG.Editor.Controls
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Input;

    /// <summary>
    /// Implements a selectable item inside a <see cref="RsComboBox"/> control.
    /// </summary>
    public class RsComboBoxItem : ComboBoxItem
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="CommandParameter" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty CommandParameterProperty;

        /// <summary>
        /// The private field used for the <see cref="CanExecute"/> property.
        /// </summary>
        private bool _canExecute;

        /// <summary>
        /// The private reference to the parent combo box that owns this item.
        /// </summary>
        private RsComboBox _parent;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsComboBoxItem" /> class.
        /// </summary>
        static RsComboBoxItem()
        {
            CommandParameterProperty =
                ButtonBase.CommandParameterProperty.AddOwner(
                typeof(RsComboBoxItem),
                new FrameworkPropertyMetadata(OnCommandParameterChanged));

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsComboBoxItem),
                new FrameworkPropertyMetadata(typeof(RsComboBoxItem)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsComboBoxItem"/> class.
        /// </summary>
        public RsComboBoxItem()
        {
            this.Loaded += this.OnLoaded;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the parameter to pass to the Command property.
        /// </summary>
        public object CommandParameter
        {
            get { return this.GetValue(CommandParameterProperty); }
            set { this.SetValue(CommandParameterProperty, value); }
        }

        /// <summary>
        /// Gets a value that indicates whether the IsEnabled property is true for the current
        /// combo box.
        /// </summary>
        protected override bool IsEnabledCore
        {
            get { return base.IsEnabledCore && this.CanExecute; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the command attached to this control can be
        /// executed by the user.
        /// </summary>
        private bool CanExecute
        {
            get
            {
                return this._canExecute;
            }

            set
            {
                if (this._canExecute != value)
                {
                    this._canExecute = value;
                    this.CoerceValue(UIElement.IsEnabledProperty);
                }
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Called whenever the Command dependency property changes on the parent items
        /// control.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        internal void OnCommandChanged(DependencyPropertyChangedEventArgs e)
        {
            ICommand oldCommand = e.OldValue as ICommand;
            if (oldCommand != null)
            {
                CanExecuteChangedEventManager.RemoveHandler(
                    oldCommand, this.OnCanExecuteChanged);
            }

            ICommand newCommand = e.NewValue as ICommand;
            if (newCommand != null)
            {
                CanExecuteChangedEventManager.AddHandler(newCommand, this.OnCanExecuteChanged);
            }

            if (this.IsLoaded)
            {
                this.UpdateCanExecute();
            }
        }

        /// <summary>
        /// Invoked when an unhandled System.Windows.Input.Keyboard.KeyDown attached event
        /// reaches an element in its route that is derived from this class. Implement this
        /// method to add class handling for this event.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.KeyEventArgs that contains the event data.
        /// </param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e == null)
            {
                throw new SmartArgumentNullException(() => e);
            }

            if (e.Key == Key.Return)
            {
                if (this._parent != null)
                {
                    int index = this._parent.ItemContainerGenerator.IndexFromContainer(this);
                    this._parent.HandleSelection(true, this, index);
                }

                e.Handled = true;
                return;
            }

            base.OnKeyDown(e);
        }

        /// <summary>
        /// Invoked when an unhandled System.Windows.Input.Mouse.MouseDown attached event
        /// reaches an element in its route that is derived from this class. Implement this
        /// method to add class handling for this event.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.MouseButtonEventArgs that contains the event data.
        /// </param>
        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            if (this._parent != null)
            {
                int index = this._parent.ItemContainerGenerator.IndexFromContainer(this);
                this._parent.HandleSelection(true, this, index);
            }
        }

        /// <summary>
        /// Called whenever the <see cref="CommandParameter"/> dependency property changes.
        /// </summary>
        /// <param name="d">
        /// The object whose <see cref="CommandParameter"/> dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnCommandParameterChanged(
            DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RsComboBoxItem item = d as RsComboBoxItem;
            if (item == null)
            {
                return;
            }

            item.UpdateCanExecute();
            item.CoerceValue(UIElement.IsEnabledProperty);
        }

        /// <summary>
        /// Called whenever the command attached to this control sends an event that indicates
        /// the can execute state of it might have changed.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.EventArgs data for this event.
        /// </param>
        private void OnCanExecuteChanged(object sender, EventArgs e)
        {
            this.UpdateCanExecute();
        }

        /// <summary>
        /// Called when this control is loaded for the first time so that the can execute value
        /// can be resolved.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs event data for this event.
        /// </param>
        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            ItemsControl itemsControl = ItemsControl.ItemsControlFromItemContainer(this);
            this._parent = itemsControl as RsComboBox;

            this.UpdateCanExecute();
            this.Loaded -= this.OnLoaded;
        }

        /// <summary>
        /// Updates the <see cref="CanExecute"/> property based on the attached command.
        /// </summary>
        private void UpdateCanExecute()
        {
            if (this._parent == null)
            {
                this.CanExecute = true;
                return;
            }

            if (this._parent.Command == null)
            {
                this.CanExecute = true;
                return;
            }

            object commandParameter = this.CommandParameter;
            IInputElement inputElement = this._parent.CommandTarget;
            RoutedCommand routedCommand = this._parent.Command as RoutedCommand;
            if (routedCommand != null)
            {
                if (inputElement == null)
                {
                    inputElement = this._parent.Command as IInputElement;
                }

                this.CanExecute = routedCommand.CanExecute(commandParameter, inputElement);
            }
            else
            {
                this.CanExecute = this._parent.Command.CanExecute(commandParameter);
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsComboBoxItem {Class}
} // RSG.Editor.Controls {Namespace}
