﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsToolWindowItem.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock.ToolWindow
{
    using System;
    using System.ComponentModel;
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Represents a selectable item inside the <see cref="RsToolWindowPane"/> control.
    /// </summary>
    public class RsToolWindowItem : TabItem
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="CanHide"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty CanHideProperty;

        /// <summary>
        /// Identifies the <see cref="IsActive"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsActiveProperty;

        /// <summary>
        /// Identifies the <see cref="IsModified"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsModifiedProperty;

        /// <summary>
        /// Identifies the <see cref="IsReadOnly"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsReadOnlyProperty;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsToolWindowItem"/> class.
        /// </summary>
        static RsToolWindowItem()
        {
            CanHideProperty =
                DependencyProperty.Register(
                "CanHide",
                typeof(bool),
                typeof(RsToolWindowItem),
                new PropertyMetadata(false));

            IsActiveProperty =
                DependencyProperty.Register(
                "IsActive",
                typeof(bool),
                typeof(RsToolWindowItem),
                new PropertyMetadata(true));

            IsModifiedProperty =
                DependencyProperty.Register(
                "IsModified",
                typeof(bool),
                typeof(RsToolWindowItem),
                new PropertyMetadata(false));

            IsReadOnlyProperty =
                DependencyProperty.Register(
                "IsReadOnly",
                typeof(bool),
                typeof(RsToolWindowItem),
                new PropertyMetadata(false));

            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsToolWindowItem),
                new FrameworkPropertyMetadata(typeof(RsToolWindowItem)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsToolWindowItem"/> class.
        /// </summary>
        public RsToolWindowItem()
        {
            this.Loaded += this.UpdateTabStripPlacement;
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// This event is raised when the item is closed.
        /// </summary>
        public event EventHandler Closed;

        /// <summary>
        /// This event is raised before the item is closed.
        /// </summary>
        public event CancelEventHandler Closing;
        #endregion Events

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether or not this control can be hidden within
        /// the docking manager.
        /// </summary>
        public bool CanHide
        {
            get { return (bool)this.GetValue(CanHideProperty); }
            set { this.SetValue(CanHideProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not this control is currently the
        /// active content in the parent DockingManager.
        /// </summary>
        public bool IsActive
        {
            get { return (bool)this.GetValue(IsActiveProperty); }
            set { this.SetValue(IsActiveProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not this control should show its dirty
        /// indicator.
        /// </summary>
        public bool IsModified
        {
            get { return (bool)this.GetValue(IsModifiedProperty); }
            set { this.SetValue(IsModifiedProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not this control is currently in a read
        /// only state.
        /// </summary>
        public bool IsReadOnly
        {
            get { return (bool)this.GetValue(IsReadOnlyProperty); }
            set { this.SetValue(IsReadOnlyProperty, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Call to fire this items <see cref="Closed"/> event.
        /// </summary>
        internal void FireClosedEvent()
        {
            EventHandler handler = this.Closed;
            if (handler == null)
            {
                return;
            }

            handler(this, EventArgs.Empty);
        }

        /// <summary>
        /// Call to fire this items <see cref="Closing"/> event with the specified arguments.
        /// </summary>
        /// <param name="e">
        /// The arguments that should be fired with the event.
        /// </param>
        internal void FireClosingEvent(CancelEventArgs e)
        {
            CancelEventHandler handler = this.Closing;
            if (handler == null)
            {
                return;
            }

            handler(this, e);
        }

        /// <summary>
        /// Updates the tab strip plpacement of this item as when using a pure XAML
        /// implementation this will be set to top by default and not change.
        /// </summary>
        /// <param name="sender">
        /// This control.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private void UpdateTabStripPlacement(object sender, RoutedEventArgs e)
        {
            this.CoerceValue(TabItem.TabStripPlacementProperty);
        }
        #endregion Methods
    } // RSG.Editor.Controls.Dock.ToolWindow.RsToolWindowItem {Class}
} // RSG.Editor.Controls.Dock.ToolWindow {Namespace}
