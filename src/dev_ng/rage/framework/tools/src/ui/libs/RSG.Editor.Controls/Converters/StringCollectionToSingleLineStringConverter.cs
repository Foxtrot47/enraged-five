﻿//---------------------------------------------------------------------------------------------
// <copyright file="StringCollectionToSingleLineStringConverter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    /// <summary>
    /// Converter used to convert an enumerable of strings to a single string object for
    /// display.
    /// </summary>
    public class StringCollectionToSingleLineStringConverter
        : ValueConverter<IEnumerable<string>, string>
    {
        #region Methods
        /// <summary>
        /// Converters the given value and returns the result.
        /// </summary>
        /// <param name="value">
        /// The original value to convert.
        /// </param>
        /// <param name="param">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A converted value.
        /// </returns>
        protected override string Convert(
            IEnumerable<string> value, object param, CultureInfo culture)
        {
            if (value == null)
            {
                return null;
            }

            int count = value.Count();
            if (count == 0)
            {
                return String.Empty;
            }
            else if (count == 1)
            {
                return value.First();
            }
            else
            {
                return String.Format(culture, "{0}, ...", value.First());
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.Converters.StringCollectionToSingleLineStringConverter {Class}
} // RSG.Editor.Controls.Converters {Namespace}
