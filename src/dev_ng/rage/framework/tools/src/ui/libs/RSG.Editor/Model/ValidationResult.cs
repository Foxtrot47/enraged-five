﻿//---------------------------------------------------------------------------------------------
// <copyright file="ValidationResult.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Model
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    /// <summary>
    /// Contains the results of a model validation, which include the validation results per
    /// property as well as for the entire entity.
    /// </summary>
    public class ValidationResult
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Empty"/> property.
        /// </summary>
        private static readonly ValidationResult _empty;

        /// <summary>
        /// The private field used for the <see cref="ChildResults"/> property.
        /// </summary>
        private List<ValidationResult> _childResults;

        /// <summary>
        /// The private list containing the errors that have been found during the validation
        /// process.
        /// </summary>
        private List<IValidationError> _errors;

        /// <summary>
        /// The private list containing the warnings that have been found during the validation
        /// process.
        /// </summary>
        private List<IValidationWarning> _warnings;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="ValidationResult"/> class.
        /// </summary>
        static ValidationResult()
        {
            _empty = new ValidationResult();
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ValidationResult"/> class.
        /// </summary>
        public ValidationResult()
        {
            this._errors = new List<IValidationError>();
            this._warnings = new List<IValidationWarning>();
            this._childResults = new List<ValidationResult>();
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the instance of this object that represents validation with no results.
        /// </summary>
        public static ValidationResult Empty
        {
            get { return _empty; }
        }

        /// <summary>
        /// Gets a hash set containing all of the unique errors inside this container
        /// independent on property name.
        /// </summary>
        public HashSet<string> AllUniqueErrors
        {
            get
            {
                HashSet<string> errors = new HashSet<string>();
                foreach (IValidationError validationError in this._errors)
                {
                    IPropertyValidation property = validationError as IPropertyValidation;
                    if (property == null)
                    {
                        continue;
                    }

                    errors.Add(property.Message);
                }

                return errors;
            }
        }

        /// <summary>
        /// Gets a iterator around the child result containers for this result container.
        /// </summary>
        public IEnumerable<ValidationResult> ChildResults
        {
            get
            {
                foreach (ValidationResult childResult in this._childResults)
                {
                    yield return childResult;
                }
            }
        }

        /// <summary>
        /// Gets a iterator around the errors contained within this result container.
        /// </summary>
        public IEnumerable<IValidationError> Errors
        {
            get
            {
                foreach (IValidationError error in this._errors)
                {
                    yield return error;
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether this result contains has validation errors.
        /// </summary>
        public bool HasErrors
        {
            get { return this._errors.Count > 0; }
        }

        /// <summary>
        /// Gets the property names that have data about them in this validation results
        /// object.
        /// </summary>
        public IEnumerable<string> PropertyNames
        {
            get
            {
                HashSet<string> propertyNames = new HashSet<string>();
                foreach (IValidationError validationError in this._errors)
                {
                    IPropertyValidation property = validationError as IPropertyValidation;
                    if (property == null)
                    {
                        continue;
                    }

                    propertyNames.Add(property.PropertyName);
                }

                foreach (IValidationWarning validationWarning in this._warnings)
                {
                    IPropertyValidation property = validationWarning as IPropertyValidation;
                    if (property == null)
                    {
                        continue;
                    }

                    propertyNames.Add(property.PropertyName);
                }

                return propertyNames;
            }
        }

        /// <summary>
        /// Gets a iterator around the warnings contained within this result container.
        /// </summary>
        public IEnumerable<IValidationWarning> Warnings
        {
            get
            {
                foreach (IValidationWarning warning in this._warnings)
                {
                    yield return warning;
                }
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Adds a child validation results container to this container.
        /// </summary>
        /// <param name="childResult">
        /// The child results to add to this container.
        /// </param>
        public void AddChildResults(ValidationResult childResult)
        {
            if (childResult._errors.Count == 0 && childResult._warnings.Count == 0)
            {
                if (childResult._childResults.Count == 0)
                {
                    return;
                }
            }

            this._childResults.Add(childResult);
        }

        /// <summary>
        /// Adds a new property error into this results container for a property with the
        /// specified name, description, and location.
        /// </summary>
        /// <param name="propertyName">
        /// The name of the property the error is associated with.
        /// </param>
        /// <param name="error">
        /// The description of the error.
        /// </param>
        /// <param name="location">
        /// The file location of the error.
        /// </param>
        public void AddError(string propertyName, string error, FileLocation location)
        {
            this._errors.Add(new PropertyValidationError(propertyName, error, location));
        }

        /// <summary>
        /// Adds a new property error into this results container for a property with the
        /// specified name and description.
        /// </summary>
        /// <param name="propertyName">
        /// The name of the property the error is associated with.
        /// </param>
        /// <param name="error">
        /// The description of the error.
        /// </param>
        public void AddError(string propertyName, string error)
        {
            this._errors.Add(new PropertyValidationError(propertyName, error));
        }

        /// <summary>
        /// Adds a new property error into this results container for a property with the
        /// specified description and location.
        /// </summary>
        /// <param name="error">
        /// The description of the error.
        /// </param>
        /// <param name="location">
        /// The file location of the error.
        /// </param>
        public void AddError(string error, FileLocation location)
        {
            this._errors.Add(new ValidationError(error, location));
        }

        /// <summary>
        /// Adds a new property error into this results container for a property with the
        /// specified description.
        /// </summary>
        /// <param name="error">
        /// The description of the error.
        /// </param>
        public void AddError(string error)
        {
            this._errors.Add(new ValidationError(error));
        }

        /// <summary>
        /// Adds a new property warning into this results container for a property with the
        /// specified name, description, and location.
        /// </summary>
        /// <param name="propertyName">
        /// The name of the property the warning is associated with.
        /// </param>
        /// <param name="warning">
        /// The description of the warning.
        /// </param>
        /// <param name="location">
        /// The file location of the warning.
        /// </param>
        public void AddWarning(string propertyName, string warning, FileLocation location)
        {
            this._warnings.Add(new PropertyValidationWarning(propertyName, warning, location));
        }

        /// <summary>
        /// Adds a new property warning into this results container for a property with the
        /// specified name and description.
        /// </summary>
        /// <param name="propertyName">
        /// The name of the property the warning is associated with.
        /// </param>
        /// <param name="warning">
        /// The description of the warning.
        /// </param>
        public void AddWarning(string propertyName, string warning)
        {
            this._warnings.Add(new PropertyValidationWarning(propertyName, warning));
        }

        /// <summary>
        /// Adds a new property warning into this results container for a property with the
        /// specified description and location.
        /// </summary>
        /// <param name="warning">
        /// The description of the warning.
        /// </param>
        /// <param name="location">
        /// The file location of the warning.
        /// </param>
        public void AddWarning(string warning, FileLocation location)
        {
            this._warnings.Add(new ValidationWarning(warning, location));
        }

        /// <summary>
        /// Adds a new property warning into this results container for a property with the
        /// specified description.
        /// </summary>
        /// <param name="warning">
        /// The description of the warning.
        /// </param>
        public void AddWarning(string warning)
        {
            this._warnings.Add(new ValidationWarning(warning));
        }

        /// <summary>
        /// Gets the validation errors for a specified property or for the entire entity.
        /// </summary>
        /// <param name="propertyName">
        /// The name of the property to retrieve validation errors for; or null or Empty, to
        /// retrieve entity-level errors.
        /// </param>
        /// <returns>
        /// The validation errors for the property or entity.
        /// </returns>
        public IEnumerable GetErrors(string propertyName)
        {
            List<string> errors = new List<string>();
            foreach (IValidationError validationError in this._errors)
            {
                IPropertyValidation property = validationError as IPropertyValidation;
                if (property == null)
                {
                    continue;
                }

                if (String.Equals(property.PropertyName, propertyName))
                {
                    errors.Add(property.Message);
                }
            }

            return errors;
        }

        /// <summary>
        /// Gets all of the errors contained within this validation container and its child
        /// containers recursively.
        /// </summary>
        /// <returns>
        /// All of the erros contained within this validation container.
        /// </returns>
        public IList<IValidationError> GetErrorsIncludingChildren()
        {
            List<IValidationError> errors = new List<IValidationError>();
            errors.AddRange(this.Errors);
            foreach (ValidationResult child in this.ChildResults)
            {
                errors.AddRange(child.GetErrorsIncludingChildren());
            }

            return errors;
        }
        #endregion Methods
    } // RSG.Editor.Model.ValidationResult {Class}
} // RSG.Editor.Model {Namespace}
