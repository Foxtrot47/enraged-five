﻿//---------------------------------------------------------------------------------------------
// <copyright file="CommandMenuViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Customisation
{
    using System;
    using RSG.Editor.Controls.Resources;

    /// <summary>
    /// Represents a view model that wraps a single menu command instance.
    /// </summary>
    internal class CommandMenuViewModel : CommandContainerViewModel
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CommandMenuViewModel"/> class.
        /// </summary>
        /// <param name="name">
        /// The name of the menu.
        /// </param>
        /// <param name="id">
        /// The id to use to get the child commands from the
        /// <see cref="RockstarCommandManager"/> class.
        /// </param>
        private CommandMenuViewModel(string name, Guid id)
            : base(name, id)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="CommandMenuViewModel"/> class.
        /// </summary>
        /// <param name="item">
        /// The menu command that this view model is representing.
        /// </param>
        /// <param name="container">
        /// The container that this view model is currently inside.
        /// </param>
        private CommandMenuViewModel(CommandMenu item, CommandContainerViewModel container)
            : base(item, container)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Creates a new instance of the <see cref="CommandMenuViewModel"/> class that is used
        /// for the main menu bar.
        /// </summary>
        /// <returns>
        /// A new instance of the <see cref="CommandMenuViewModel"/> class.
        /// </returns>
        public static CommandMenuViewModel CreateForMainMenu()
        {
            return new CommandMenuViewModel(
                CustomisationResources.MainMenuRootText,
                RockstarCommandManager.MainMenuId);
        }

        /// <summary>
        /// Creates a new instance of the <see cref="CommandMenuViewModel"/> class that wraps
        /// the specified command menu item.
        /// </summary>
        /// <param name="item">
        /// The command menu item the new instance will be wrapping.
        /// </param>
        /// <returns>
        /// A new instance of the <see cref="CommandMenuViewModel"/> class.
        /// </returns>
        /// <param name="container">
        /// The container that the new view model is a child of.
        /// </param>
        public static CommandMenuViewModel CreateChild(
            CommandMenu item, CommandContainerViewModel container)
        {
            return new CommandMenuViewModel(item, container);
        }
        #endregion Methods
    } // RSG.Editor.Controls.Customisation.CommandMenuViewModel {Class}
} // RSG.Editor.Controls.Customisation {Namespace}
