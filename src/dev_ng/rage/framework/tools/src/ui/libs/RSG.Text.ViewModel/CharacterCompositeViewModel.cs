﻿//---------------------------------------------------------------------------------------------
// <copyright file="CharacterCompositeViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using RSG.Text.Model;

    /// <summary>
    /// A composite class containing n-number of <see cref="DialogueCharacterViewModel"/>
    /// objects. This class cannot be inherited.
    /// </summary>
    public sealed class CharacterCompositeViewModel : CompositeViewModel
    {
        #region Fields
        /// <summary>
        /// The private collection containing the <see cref="DialogueCharacterViewModel"/>
        /// objects that are currently managed by this composite class.
        /// </summary>
        private List<DialogueCharacterViewModel> _items;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CharacterCompositeViewModel"/> class.
        /// </summary>
        /// <param name="configurations">
        /// The <see cref="DialogueConfigurations"/> object that the configuration values have
        /// come from.
        /// </param>
        /// <param name="items">
        /// The collection of <see cref="DialogueCharacterViewModel"/> objects that this
        /// composite object should manage.
        /// </param>
        public CharacterCompositeViewModel(
            DialogueConfigurations configurations,
            IEnumerable<DialogueCharacterViewModel> items)
            : base(configurations)
        {
            this._items = new List<DialogueCharacterViewModel>(items);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the collective export flag for all of the dialogue character objects
        /// managed by this class.
        /// </summary>
        public bool? Export
        {
            get
            {
                if (this._items == null || this._items.Count == 0)
                {
                    return null;
                }

                IEnumerable<bool> flags = this._items.Select(x => x.Export).Distinct();
                if (flags.Count() == 1)
                {
                    return flags.First();
                }

                return null;
            }

            set
            {
                foreach (DialogueCharacterViewModel item in this._items)
                {
                    item.Export = value.Value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the collective uses manual filename flag for all of the dialogue
        /// character objects managed by this class.
        /// </summary>
        public bool? UsesManualFilenames
        {
            get
            {
                if (this._items == null || this._items.Count == 0)
                {
                    return null;
                }

                IEnumerable<bool> flags =
                    this._items.Select(x => x.UsesManualFilenames).Distinct();
                if (flags.Count() == 1)
                {
                    return flags.First();
                }

                return null;
            }

            set
            {
                foreach (DialogueCharacterViewModel item in this._items)
                {
                    item.UsesManualFilenames = value.Value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the collective voice value for all of the dialogue character objects
        /// managed by this class.
        /// </summary>
        public DialogueVoice Voice
        {
            get
            {
                if (this._items == null || this._items.Count == 0)
                {
                    return null;
                }

                IEnumerable<Guid> voicesId = this._items.Select(x => x.Voice).Distinct();
                if (voicesId.Count() != 1)
                {
                    return null;
                }

                Guid voiceId = voicesId.First();
                if (this.Model.Voices == null || this.Model.Voices.Count == 0)
                {
                    return null;
                }

                DialogueVoice voice = this.Model.Voices.FirstOrDefault(x => x.Id == voiceId);
                return voice;
            }

            set
            {
                DialogueVoice voice = this.Model.Voices.FirstOrDefault(x => x.Id == value.Id);
                foreach (DialogueCharacterViewModel item in this._items)
                {
                    item.Voice = voice.Id;
                }
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Deletes all of the values managed by this composite collection from the dialogue
        /// configuration object.
        /// </summary>
        public override void Delete()
        {
            List<DialogueCharacter> delete = new List<DialogueCharacter>();
            foreach (DialogueCharacterViewModel viewModel in this._items)
            {
                foreach (DialogueCharacter character in this.Model.Characters)
                {
                    if (viewModel.Id == character.Id)
                    {
                        delete.Add(character);
                    }
                }
            }

            foreach (DialogueCharacter character in delete)
            {
                this.Model.Characters.Remove(character);
            }
        }
        #endregion Methods
    } // RSG.Text.ViewModel.CharacterCompositeViewModel {Class}
} // RSG.Text.ViewModel {Namespace}
