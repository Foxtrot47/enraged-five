﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsDropDownColourPicker.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Shapes;
    using System.Windows.Threading;
    using RSG.Editor.Controls.Converters;
    using RSG.Editor.Controls.Helpers;
    using RSG.Interop.Microsoft.Windows;

    /// <summary>
    /// Represents a control with a drop down area that can be shown or hidden that displays
    /// a number of colour picker controls.
    /// </summary>
    public class RsDropDownColourPicker : Control
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="ColourUpdateMode" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty ColourUpdateModeProperty;

        /// <summary>
        /// Identifies the <see cref="CurrentColour" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty CurrentColourProperty;

        /// <summary>
        /// Identifies the <see cref="IsReadOnly"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsReadOnlyProperty;

        /// <summary>
        /// Identifies the <see cref="SelectedColour" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty SelectedColourProperty;

        /// <summary>
        /// Identifies the <see cref="SupportsAlpha" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty SupportsAlphaProperty;

        /// <summary>
        /// Identifies the <see cref="InternalColour" /> dependency property.
        /// </summary>
        private static readonly DependencyProperty _internalColourProperty;

        /// <summary>
        /// Identifies the <see cref="InternalHsvColour" /> dependency property.
        /// </summary>
        private static readonly DependencyProperty _internalHsvColourProperty;

        /// <summary>
        /// Identifies the <see cref="CurrentColour" /> dependency property.
        /// </summary>
        private static DependencyPropertyKey _currentColourPropertyKey;

        /// <summary>
        /// The private field used for the <see cref="DefaultPalette"/> property.
        /// </summary>
        private static List<PaletteItem> _defaultPalette;

        /// <summary>
        /// The private field used for the <see cref="DefaultPaletteWithAlpha"/> property.
        /// </summary>
        private static List<PaletteItem> _defaultPaletteWithAlpha;

        /// <summary>
        /// A value indicating whether the colour has been modified while the drop down has
        /// been open. This is to make sure we don't update the colour to a standard default
        /// one.
        /// </summary>
        private bool _colourChanged;

        /// <summary>
        /// A private value indicating whether when the HSV colour or RGB colour changes the
        /// other one should be updated or not.
        /// </summary>
        private bool _dontSyncColours;

        /// <summary>
        /// The private field used for the <see cref="IsContextMenuOpen"/> property.
        /// </summary>
        private bool _isContextMenuOpen;

        /// <summary>
        /// The private reference to the shape defined in the controls template that should
        /// display a live update of the colour.
        /// </summary>
        private Shape _liveUpdateTile;

        /// <summary>
        /// The private reference to the <see cref="RsColourPalette"/> control defined in the
        /// controls template.
        /// </summary>
        private RsColourPalette _palette;

        /// <summary>
        /// The private field used for the <see cref="PaletteItems"/> property.
        /// </summary>
        private ObservableCollection<PaletteItem> _paletteItems;

        /// <summary>
        /// A private collection of <see cref="IColourSlider"/> objects defined in the controls
        /// template.
        /// </summary>
        private List<IColourSlider> _sliders;

        /// <summary>
        /// The dispatcher timer used to delay the colour update by a certain time span when
        /// the <see cref="ColourUpdateMode"/> is set to Delay.
        /// </summary>
        private DispatcherTimer _timer;

        /// <summary>
        /// The private reference to the <see cref="RsColourWheel"/> control defined in the
        /// controls template.
        /// </summary>
        private RsColourWheel _wheel;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsDropDownColourPicker" /> class.
        /// </summary>
        static RsDropDownColourPicker()
        {
            SelectedColourProperty =
                DependencyProperty.Register(
                "SelectedColour",
                typeof(Color),
                typeof(RsDropDownColourPicker),
                new FrameworkPropertyMetadata(Colors.Transparent, OnColourChanged));

            IsReadOnlyProperty =
                DependencyProperty.Register(
                "IsReadOnly",
                typeof(bool),
                typeof(RsDropDownColourPicker),
                new PropertyMetadata(false));

            SupportsAlphaProperty =
                DependencyProperty.Register(
                "SupportsAlpha",
                typeof(bool),
                typeof(RsDropDownColourPicker),
                new FrameworkPropertyMetadata(true, OnSupportsAlphaChanged));

            _internalColourProperty =
                DependencyProperty.Register(
                "InternalColour",
                typeof(Color),
                typeof(RsDropDownColourPicker),
                new FrameworkPropertyMetadata(
                    Colors.Transparent,
                    OnInternalColourChanged));

            _internalHsvColourProperty =
                DependencyProperty.Register(
                "InternalHsvColour",
                typeof(HsvColour),
                typeof(RsDropDownColourPicker),
                new FrameworkPropertyMetadata(
                    new HsvColour(0.0, 0.0, 1.0), OnInternalHsvColourChanged));

            ComboBox.IsDropDownOpenProperty.AddOwner(
                typeof(RsDropDownColourPicker),
                new FrameworkPropertyMetadata(OnIsDropDownOpenChanged, CoerceIsDropDownOpen));

            _currentColourPropertyKey =
                DependencyProperty.RegisterReadOnly(
                "CurrentColour",
                typeof(Color),
                typeof(RsDropDownColourPicker),
                new FrameworkPropertyMetadata(Colors.Transparent));

            CurrentColourProperty = _currentColourPropertyKey.DependencyProperty;

            ColourUpdateModeProperty =
                DependencyProperty.Register(
                "ColourUpdateMode",
                typeof(ColourUpdateMode),
                typeof(RsDropDownColourPicker),
                new FrameworkPropertyMetadata(ColourUpdateMode.Realtime));

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsDropDownColourPicker),
                new FrameworkPropertyMetadata(typeof(RsDropDownColourPicker)));

            EventManager.RegisterClassHandler(
                typeof(RsDropDownColourPicker),
                Mouse.MouseWheelEvent,
                new MouseWheelEventHandler(OnMouseWheel),
                true);

            EventManager.RegisterClassHandler(
                typeof(RsDropDownColourPicker),
                Mouse.LostMouseCaptureEvent,
                new MouseEventHandler(OnLostMouseCapture));

            EventManager.RegisterClassHandler(
                typeof(RsDropDownColourPicker),
                Mouse.MouseDownEvent,
                new MouseButtonEventHandler(OnMouseButtonDown),
                true);

            EventManager.RegisterClassHandler(
                typeof(RsDropDownColourPicker),
                ContextMenuService.ContextMenuOpeningEvent,
                new ContextMenuEventHandler(OnContextMenuOpen),
                true);

            EventManager.RegisterClassHandler(
                typeof(RsDropDownColourPicker),
                ContextMenuService.ContextMenuClosingEvent,
                new ContextMenuEventHandler(OnContextMenuClose),
                true);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsDropDownColourPicker" /> class.
        /// </summary>
        public RsDropDownColourPicker()
        {
            Mouse.AddPreviewMouseDownOutsideCapturedElementHandler(
                this, this.OnMouseDownOutsideCapturedElement);

            this._paletteItems =
                new ObservableCollection<PaletteItem>(DefaultPaletteWithAlpha);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the colour update mode that has been set on this control. This
        /// controls when and how often the colour gets updated from the user actions.
        /// </summary>
        public ColourUpdateMode ColourUpdateMode
        {
            get { return (ColourUpdateMode)this.GetValue(ColourUpdateModeProperty); }
            set { this.SetValue(ColourUpdateModeProperty, value); }
        }

        /// <summary>
        /// Gets the current colour value before any editing was done.
        /// </summary>
        public Color CurrentColour
        {
            get { return (Color)this.GetValue(CurrentColourProperty); }
            private set { this.SetValue(_currentColourPropertyKey, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the popup for this control is open or not.
        /// </summary>
        public bool IsDropDownOpen
        {
            get { return (bool)this.GetValue(ComboBox.IsDropDownOpenProperty); }
            set { this.SetValue(ComboBox.IsDropDownOpenProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the user can change the value for this
        /// spinner.
        /// </summary>
        public bool IsReadOnly
        {
            get { return (bool)this.GetValue(IsReadOnlyProperty); }
            set { this.SetValue(IsReadOnlyProperty, value); }
        }

        /// <summary>
        /// Gets or sets the selected colour for this picker.
        /// </summary>
        public Color SelectedColour
        {
            get { return (Color)this.GetValue(SelectedColourProperty); }
            set { this.SetValue(SelectedColourProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the selected colours from this picker can
        /// have a alpha value different than 255.
        /// </summary>
        public bool SupportsAlpha
        {
            get { return (bool)this.GetValue(SupportsAlphaProperty); }
            set { this.SetValue(SupportsAlphaProperty, value); }
        }

        /// <summary>
        /// Gets a collection of palette items that make up the default set of colours.
        /// </summary>
        private static List<PaletteItem> DefaultPalette
        {
            get
            {
                if (_defaultPalette == null)
                {
                    _defaultPalette = CreateDefaultPalette(false);
                }

                return _defaultPalette;
            }
        }

        /// <summary>
        /// Gets a collection of palette items that make up the default set of colours
        /// including colours that contain a alpha component.
        /// </summary>
        private static List<PaletteItem> DefaultPaletteWithAlpha
        {
            get
            {
                if (_defaultPaletteWithAlpha == null)
                {
                    _defaultPaletteWithAlpha = CreateDefaultPalette(true);
                }

                return _defaultPaletteWithAlpha;
            }
        }

        /// <summary>
        /// Gets or sets the internal selected colour. This always gets updated in real time
        /// no matter what the <see cref="ColourUpdateMode"/> property has been set to.
        /// </summary>
        private Color InternalColour
        {
            get { return (Color)this.GetValue(_internalColourProperty); }
            set { this.SetValue(_internalColourProperty, value); }
        }

        /// <summary>
        /// Gets or sets the internal selected HSV colour. This always gets updated in real
        /// time no matter what the <see cref="ColourUpdateMode"/> property has been set to.
        /// </summary>
        private HsvColour InternalHsvColour
        {
            get { return (HsvColour)this.GetValue(_internalHsvColourProperty); }
            set { this.SetValue(_internalHsvColourProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the context menu for this control is
        /// opened.
        /// </summary>
        private bool IsContextMenuOpen
        {
            get { return this._isContextMenuOpen; }
            set { this._isContextMenuOpen = value; }
        }

        /// <summary>
        /// Gets the collection of palette items used to dynamically change the palette.
        /// </summary>
        private ObservableCollection<PaletteItem> PaletteItems
        {
            get { return this._paletteItems; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Gets called once the template tree has been created. We use this to pick up the
        /// palette control so that private binding can be applied to it.
        /// </summary>
        public override void OnApplyTemplate()
        {
            if (this._palette != null)
            {
                this._palette.SelectionChanged -= this.OnPaletteSelectionChanged;
                BindingOperations.ClearBinding(
                    this._palette, ItemsControl.ItemsSourceProperty);
            }

            if (this._wheel != null)
            {
                this._wheel.SelectedColourChanged -= this.OnWheelSelectionChanged;
                this._wheel.DragFinished -= this.OnComponentDragFinished;
                BindingOperations.ClearBinding(
                    this._wheel, RsColourWheel.SelectedColourProperty);
            }

            if (this._liveUpdateTile != null)
            {
                BindingOperations.ClearBinding(this._liveUpdateTile, Shape.FillProperty);
            }

            if (this._sliders != null)
            {
                foreach (IColourSlider colourSlider in this._sliders)
                {
                    if (colourSlider != null)
                    {
                        colourSlider.ClearColourBinding();
                        colourSlider.DragFinished -= this.OnComponentDragFinished;
                    }
                }

                this._sliders.Clear();
                this._sliders = null;
            }

            base.OnApplyTemplate();

            this._palette = this.GetTemplateChild("PART_Palette") as RsColourPalette;
            if (this._palette != null)
            {
                this._palette.SelectionChanged += this.OnPaletteSelectionChanged;
                this._palette.SetBinding(
                    ItemsControl.ItemsSourceProperty,
                    new Binding()
                    {
                        Source = this.PaletteItems,
                        BindsDirectlyToSource = true,
                    });
            }

            this._wheel = this.GetTemplateChild("PART_Wheel") as RsColourWheel;
            if (this._wheel != null)
            {
                this._wheel.SelectedColourChanged += this.OnWheelSelectionChanged;
                this._wheel.DragFinished += this.OnComponentDragFinished;
                this._wheel.SetBinding(
                    RsColourWheel.SelectedColourProperty,
                    new Binding("InternalColour")
                    {
                        Source = this,
                        Mode = BindingMode.OneWay,
                    });
            }

            this._liveUpdateTile = this.GetTemplateChild("PART_LiveUpdateTile") as Shape;
            if (this._liveUpdateTile != null)
            {
                this._liveUpdateTile.SetBinding(
                    Shape.FillProperty,
                    new Binding("InternalColour")
                    {
                        Source = this,
                        Mode = BindingMode.OneWay,
                        Converter = new ColourToSolidBrushConverter(),
                    });
            }

            this._sliders = new List<IColourSlider>();
            this._sliders.Add(this.GetTemplateChild("PART_RedComponent") as IColourSlider);
            this._sliders.Add(this.GetTemplateChild("PART_GreenComponent") as IColourSlider);
            this._sliders.Add(this.GetTemplateChild("PART_BlueComponent") as IColourSlider);
            this._sliders.Add(this.GetTemplateChild("PART_AlphaComponent") as IColourSlider);
            this._sliders.Add(this.GetTemplateChild("PART_HueComponent") as IColourSlider);
            this._sliders.Add(this.GetTemplateChild("PART_SatComponent") as IColourSlider);
            this._sliders.Add(this.GetTemplateChild("PART_ValueComponent") as IColourSlider);

            Binding colourBinding = new Binding("InternalColour");
            colourBinding.Source = this;
            colourBinding.Mode = BindingMode.TwoWay;
            for (int i = 0; i < 4; i++)
            {
                IColourSlider slider = this._sliders[i];
                if (slider == null)
                {
                    continue;
                }

                slider.SetColourBinding(colourBinding);
                slider.DragFinished += this.OnComponentDragFinished;
            }

            Binding hsvColourBinding = new Binding("InternalHsvColour");
            hsvColourBinding.Source = this;
            hsvColourBinding.Mode = BindingMode.TwoWay;
            for (int i = 4; i < 7; i++)
            {
                IColourSlider slider = this._sliders[i];
                if (slider == null)
                {
                    continue;
                }

                slider.SetColourBinding(hsvColourBinding);
                slider.DragFinished += this.OnComponentDragFinished;
            }

            UIElement alphaSlider = this.GetTemplateChild("PART_AlphaComponent") as UIElement;
            if (this.SupportsAlpha)
            {
                if (alphaSlider != null)
                {
                    alphaSlider.Visibility = Visibility.Visible;
                }
            }
            else
            {
                if (alphaSlider != null)
                {
                    alphaSlider.Visibility = Visibility.Collapsed;
                }
            }
        }

        /// <summary>
        /// Called whenever the <see cref="IsDropDownOpen"/> dependency property needs to be
        /// re-evaluated. This makes sure the popup doesn't try to open before the control is
        /// loaded.
        /// </summary>
        /// <param name="d">
        /// The instance whose dependency property needs evaluated.
        /// </param>
        /// <param name="value">
        /// The new value of the property, prior to any coercion attempt.
        /// </param>
        /// <returns>
        /// The coerced value.
        /// </returns>
        private static object CoerceIsDropDownOpen(DependencyObject d, object value)
        {
            if ((bool)value)
            {
                RsDropDownColourPicker picker = (RsDropDownColourPicker)d;
                if (picker.IsReadOnly)
                {
                    return false;
                }

                if (!picker.IsLoaded)
                {
                    picker.Loaded += new RoutedEventHandler(picker.OpenOnLoad);
                    return false;
                }
            }

            return value;
        }

        /// <summary>
        /// Creates a set of palette items for the default state.
        /// </summary>
        /// <param name="alpha">
        /// A value indicating whether the items should include the alpha colours or not.
        /// </param>
        /// <returns>
        /// A set of palette items for the default state.
        /// </returns>
        private static List<PaletteItem> CreateDefaultPalette(bool alpha)
        {
            List<PaletteItem> items = new List<PaletteItem>();
            items.Add(new PaletteItem(Color.FromRgb(0, 0, 0)));
            items.Add(new PaletteItem(Color.FromRgb(64, 64, 64)));
            items.Add(new PaletteItem(Color.FromRgb(255, 0, 0)));
            items.Add(new PaletteItem(Color.FromRgb(255, 106, 0)));
            items.Add(new PaletteItem(Color.FromRgb(255, 216, 0)));
            items.Add(new PaletteItem(Color.FromRgb(182, 255, 0)));
            items.Add(new PaletteItem(Color.FromRgb(76, 255, 0)));
            items.Add(new PaletteItem(Color.FromRgb(0, 255, 33)));
            items.Add(new PaletteItem(Color.FromRgb(0, 255, 144)));
            items.Add(new PaletteItem(Color.FromRgb(0, 255, 255)));
            items.Add(new PaletteItem(Color.FromRgb(0, 148, 255)));
            items.Add(new PaletteItem(Color.FromRgb(0, 38, 255)));
            items.Add(new PaletteItem(Color.FromRgb(72, 0, 255)));
            items.Add(new PaletteItem(Color.FromRgb(178, 0, 255)));
            items.Add(new PaletteItem(Color.FromRgb(255, 0, 220)));
            items.Add(new PaletteItem(Color.FromRgb(255, 0, 110)));

            items.Add(new PaletteItem(Color.FromRgb(255, 255, 255)));
            items.Add(new PaletteItem(Color.FromRgb(128, 128, 128)));
            items.Add(new PaletteItem(Color.FromRgb(127, 0, 0)));
            items.Add(new PaletteItem(Color.FromRgb(127, 51, 0)));
            items.Add(new PaletteItem(Color.FromRgb(127, 106, 0)));
            items.Add(new PaletteItem(Color.FromRgb(91, 127, 0)));
            items.Add(new PaletteItem(Color.FromRgb(38, 127, 0)));
            items.Add(new PaletteItem(Color.FromRgb(0, 127, 14)));
            items.Add(new PaletteItem(Color.FromRgb(0, 127, 70)));
            items.Add(new PaletteItem(Color.FromRgb(0, 127, 127)));
            items.Add(new PaletteItem(Color.FromRgb(0, 74, 127)));
            items.Add(new PaletteItem(Color.FromRgb(0, 19, 127)));
            items.Add(new PaletteItem(Color.FromRgb(33, 0, 127)));
            items.Add(new PaletteItem(Color.FromRgb(87, 0, 127)));
            items.Add(new PaletteItem(Color.FromRgb(127, 0, 110)));
            items.Add(new PaletteItem(Color.FromRgb(127, 0, 55)));

            items.Add(new PaletteItem(Color.FromRgb(160, 160, 160)));
            items.Add(new PaletteItem(Color.FromRgb(48, 48, 48)));
            items.Add(new PaletteItem(Color.FromRgb(255, 127, 127)));
            items.Add(new PaletteItem(Color.FromRgb(255, 178, 127)));
            items.Add(new PaletteItem(Color.FromRgb(255, 233, 127)));
            items.Add(new PaletteItem(Color.FromRgb(218, 255, 127)));
            items.Add(new PaletteItem(Color.FromRgb(165, 255, 127)));
            items.Add(new PaletteItem(Color.FromRgb(127, 255, 142)));
            items.Add(new PaletteItem(Color.FromRgb(127, 255, 197)));
            items.Add(new PaletteItem(Color.FromRgb(127, 255, 255)));
            items.Add(new PaletteItem(Color.FromRgb(127, 201, 255)));
            items.Add(new PaletteItem(Color.FromRgb(127, 146, 255)));
            items.Add(new PaletteItem(Color.FromRgb(161, 127, 255)));
            items.Add(new PaletteItem(Color.FromRgb(214, 127, 255)));
            items.Add(new PaletteItem(Color.FromRgb(255, 127, 237)));
            items.Add(new PaletteItem(Color.FromRgb(255, 127, 182)));

            items.Add(new PaletteItem(Color.FromRgb(192, 192, 192)));
            items.Add(new PaletteItem(Color.FromRgb(96, 96, 96)));
            items.Add(new PaletteItem(Color.FromRgb(127, 63, 63)));
            items.Add(new PaletteItem(Color.FromRgb(127, 89, 63)));
            items.Add(new PaletteItem(Color.FromRgb(127, 116, 63)));
            items.Add(new PaletteItem(Color.FromRgb(109, 127, 63)));
            items.Add(new PaletteItem(Color.FromRgb(82, 127, 63)));
            items.Add(new PaletteItem(Color.FromRgb(63, 127, 71)));
            items.Add(new PaletteItem(Color.FromRgb(63, 127, 98)));
            items.Add(new PaletteItem(Color.FromRgb(63, 127, 127)));
            items.Add(new PaletteItem(Color.FromRgb(63, 100, 127)));
            items.Add(new PaletteItem(Color.FromRgb(63, 73, 127)));
            items.Add(new PaletteItem(Color.FromRgb(80, 63, 127)));
            items.Add(new PaletteItem(Color.FromRgb(107, 63, 127)));
            items.Add(new PaletteItem(Color.FromRgb(127, 63, 118)));
            items.Add(new PaletteItem(Color.FromRgb(127, 63, 91)));

            if (alpha)
            {
                items.Add(new PaletteItem(Color.FromArgb(127, 0, 0, 0)));
                items.Add(new PaletteItem(Color.FromArgb(127, 64, 64, 64)));
                items.Add(new PaletteItem(Color.FromArgb(127, 255, 0, 0)));
                items.Add(new PaletteItem(Color.FromArgb(127, 255, 106, 0)));
                items.Add(new PaletteItem(Color.FromArgb(127, 255, 216, 0)));
                items.Add(new PaletteItem(Color.FromArgb(127, 182, 255, 0)));
                items.Add(new PaletteItem(Color.FromArgb(127, 76, 255, 0)));
                items.Add(new PaletteItem(Color.FromArgb(127, 0, 255, 33)));
                items.Add(new PaletteItem(Color.FromArgb(127, 0, 255, 144)));
                items.Add(new PaletteItem(Color.FromArgb(127, 0, 255, 255)));
                items.Add(new PaletteItem(Color.FromArgb(127, 0, 148, 255)));
                items.Add(new PaletteItem(Color.FromArgb(127, 0, 38, 255)));
                items.Add(new PaletteItem(Color.FromArgb(127, 72, 0, 255)));
                items.Add(new PaletteItem(Color.FromArgb(127, 178, 0, 255)));
                items.Add(new PaletteItem(Color.FromArgb(127, 255, 0, 220)));
                items.Add(new PaletteItem(Color.FromArgb(127, 255, 0, 110)));

                items.Add(new PaletteItem(Color.FromArgb(127, 255, 255, 255)));
                items.Add(new PaletteItem(Color.FromArgb(127, 128, 128, 128)));
                items.Add(new PaletteItem(Color.FromArgb(127, 127, 0, 0)));
                items.Add(new PaletteItem(Color.FromArgb(127, 127, 51, 0)));
                items.Add(new PaletteItem(Color.FromArgb(127, 127, 106, 0)));
                items.Add(new PaletteItem(Color.FromArgb(127, 91, 127, 0)));
                items.Add(new PaletteItem(Color.FromArgb(127, 38, 127, 0)));
                items.Add(new PaletteItem(Color.FromArgb(127, 0, 127, 14)));
                items.Add(new PaletteItem(Color.FromArgb(127, 0, 127, 70)));
                items.Add(new PaletteItem(Color.FromArgb(127, 0, 127, 127)));
                items.Add(new PaletteItem(Color.FromArgb(127, 0, 74, 127)));
                items.Add(new PaletteItem(Color.FromArgb(127, 0, 19, 127)));
                items.Add(new PaletteItem(Color.FromArgb(127, 33, 0, 127)));
                items.Add(new PaletteItem(Color.FromArgb(127, 87, 0, 127)));
                items.Add(new PaletteItem(Color.FromArgb(127, 127, 0, 110)));
                items.Add(new PaletteItem(Color.FromArgb(127, 127, 0, 55)));
            }

            return items;
        }

        /// <summary>
        /// Called whenever the <see cref="SelectedColour"/> dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The object whose <see cref="SelectedColour"/> dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnColourChanged(object s, DependencyPropertyChangedEventArgs e)
        {
            RsDropDownColourPicker picker = s as RsDropDownColourPicker;
            if (picker == null)
            {
                Debug.Assert(false, "Incorrect dependency property changed cast.");
                return;
            }

            Color newValue = (Color)e.NewValue;
            if (picker.InternalColour != newValue)
            {
                picker.InternalColour = newValue;
            }
        }

        /// <summary>
        /// Called when the context menu for this control closes.
        /// </summary>
        /// <param name="sender">
        /// The object that sent this event.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Controls.ContextMenuEventArgs containing the event data.
        /// </param>
        private static void OnContextMenuClose(object sender, ContextMenuEventArgs e)
        {
            ((RsDropDownColourPicker)sender).IsContextMenuOpen = false;
        }

        /// <summary>
        /// Called when the context menu for this control opens.
        /// </summary>
        /// <param name="sender">
        /// The object that sent this event.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Controls.ContextMenuEventArgs containing the event data.
        /// </param>
        private static void OnContextMenuOpen(object sender, ContextMenuEventArgs e)
        {
            ((RsDropDownColourPicker)sender).IsContextMenuOpen = true;
        }

        /// <summary>
        /// Called whenever the <see cref="InternalColour"/> dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The object whose <see cref="InternalColour"/> dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnInternalColourChanged(
            object s, DependencyPropertyChangedEventArgs e)
        {
            RsDropDownColourPicker picker = s as RsDropDownColourPicker;
            if (picker == null)
            {
                Debug.Assert(false, "Incorrect dependency property changed cast.");
                return;
            }

            if (!picker._dontSyncColours)
            {
                HsvColour colour = ColourUtilities.ConvertRgbToHsv(picker.InternalColour);
                if (picker.InternalHsvColour != colour)
                {
                    picker._dontSyncColours = true;
                    picker.InternalHsvColour = colour;
                    picker._dontSyncColours = false;
                }
            }

            if (picker._timer != null)
            {
                picker._timer.Stop();
                picker._timer = null;
            }

            if (picker.ColourUpdateMode == ColourUpdateMode.Realtime)
            {
                if (picker.SelectedColour != (Color)e.NewValue)
                {
                    picker.SetCurrentValue(
                        RsDropDownColourPicker.SelectedColourProperty, e.NewValue);
                }
            }
            else if (picker.ColourUpdateMode == ColourUpdateMode.Delay)
            {
                picker._timer = new DispatcherTimer();
                picker._timer.Interval = TimeSpan.FromMilliseconds(500);
                picker._timer.Tick += picker.OnTimerTick;
                picker._timer.Start();
            }
        }

        /// <summary>
        /// Called whenever the <see cref="InternalHsvColour"/> dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The object whose <see cref="InternalHsvColour"/> dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnInternalHsvColourChanged(
            object s, DependencyPropertyChangedEventArgs e)
        {
            RsDropDownColourPicker picker = (RsDropDownColourPicker)s;
            if (picker._dontSyncColours)
            {
                return;
            }

            Color colour = ColourUtilities.ConvertHsvToRgb(picker.InternalHsvColour);
            colour.A = picker.InternalColour.A;
            if (picker.InternalColour != colour)
            {
                picker._dontSyncColours = true;
                picker.InternalColour = colour;
                picker._dontSyncColours = false;
            }
        }

        /// <summary>
        /// Called whenever the <see cref="IsDropDownOpen"/> dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The object whose <see cref="IsDropDownOpen"/> dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnIsDropDownOpenChanged(
            object s, DependencyPropertyChangedEventArgs e)
        {
            RsDropDownColourPicker picker = s as RsDropDownColourPicker;
            if (picker == null)
            {
                Debug.Assert(false, "Incorrect dependency property changed cast.");
                return;
            }

            bool newValue = (bool)e.NewValue;
            bool oldValue = !newValue;

            if (newValue)
            {
                picker._colourChanged = false;
                picker.CurrentColour = picker.SelectedColour;
                Mouse.Capture(picker, CaptureMode.SubTree);
            }
            else
            {
                if (picker._timer != null)
                {
                    picker._timer.Stop();
                    picker._timer = null;
                }

                if (picker._colourChanged)
                {
                    if (picker.SelectedColour != picker.InternalColour)
                    {
                        picker.SetCurrentValue(
                            RsDropDownColourPicker.SelectedColourProperty,
                            picker.InternalColour);
                    }
                }

                if (picker.IsKeyboardFocusWithin)
                {
                    picker.Focus();
                }

                if (Mouse.Captured == picker)
                {
                    Mouse.Capture(null);
                }
            }

            picker.CoerceValue(ToolTipService.IsEnabledProperty);
        }

        /// <summary>
        /// Called when this control loses the mouse capture for another element. This method
        /// either closes the drop down or takes the capture back.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.MouseEventArgs containing the event data.
        /// </param>
        private static void OnLostMouseCapture(object sender, MouseEventArgs e)
        {
            RsDropDownColourPicker picker = (RsDropDownColourPicker)sender;

            if (Mouse.Captured != picker)
            {
                if (e.OriginalSource == picker)
                {
                    if (Mouse.Captured == null)
                    {
                        picker.Close();
                        return;
                    }
                    else if (!picker.HasDescendant(Mouse.Captured as DependencyObject))
                    {
                        picker.Close();
                        return;
                    }
                }
                else
                {
                    if (picker.HasDescendant(e.OriginalSource as DependencyObject))
                    {
                        bool emptyCapture = User32.GetCapture() == IntPtr.Zero;
                        if (picker.IsDropDownOpen && Mouse.Captured == null && emptyCapture)
                        {
                            Mouse.Capture(picker, CaptureMode.SubTree);
                            e.Handled = true;
                        }
                    }
                    else
                    {
                        picker.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Called if the mouse button is pressed down. This makes sure that if the user
        /// presses in a area outside of the capture area the drop down is closes including the
        /// toggle button.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.MouseButtonEventArgs containing the event data.
        /// </param>
        private static void OnMouseButtonDown(object sender, MouseButtonEventArgs e)
        {
            RsDropDownColourPicker picker = (RsDropDownColourPicker)sender;
            if (!picker.IsContextMenuOpen && !picker.IsKeyboardFocusWithin)
            {
                picker.Focus();
            }

            e.Handled = true;
            if (Mouse.Captured == picker && e.OriginalSource == picker)
            {
                picker.Close();
            }
        }

        /// <summary>
        /// Handles the event reporting a mouse wheel rotation. This makes sure the event is
        /// eaten if the popup is open or the keyboard focus is somewhere inside this control.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.MouseWheelEventArgs containing the event data.
        /// </param>
        private static void OnMouseWheel(object s, MouseWheelEventArgs e)
        {
            RsDropDownColourPicker picker = s as RsDropDownColourPicker;
            if (picker == null)
            {
                Debug.Assert(false, "Incorrect class event handler cast.");
                return;
            }

            if (picker.IsKeyboardFocusWithin || picker.IsDropDownOpen)
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Called whenever the <see cref="SupportsAlpha"/> dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The object whose <see cref="SupportsAlpha"/> dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnSupportsAlphaChanged(
            object s, DependencyPropertyChangedEventArgs e)
        {
            RsDropDownColourPicker picker = s as RsDropDownColourPicker;
            if (picker == null)
            {
                Debug.Assert(false, "Incorrect dependency property changed cast.");
                return;
            }

            UIElement element = picker.GetTemplateChild("PART_AlphaComponent") as UIElement;
            if ((bool)e.NewValue)
            {
                picker._paletteItems =
                    new ObservableCollection<PaletteItem>(DefaultPaletteWithAlpha);
                if (element != null)
                {
                    element.Visibility = Visibility.Visible;
                }
            }
            else
            {
                picker._paletteItems = new ObservableCollection<PaletteItem>(DefaultPalette);
                if (element != null)
                {
                    element.Visibility = Visibility.Collapsed;
                }
            }
        }

        /// <summary>
        /// Closes this controls popup without messing with any bindings on it.
        /// </summary>
        private void Close()
        {
            if (this.IsDropDownOpen)
            {
                this.SetCurrentValue(ComboBox.IsDropDownOpenProperty, false);
            }
        }

        /// <summary>
        /// Called whenever any of the individual components in this control sends a drag
        /// finished event. This then sets the selected colour from the internal one if this
        /// update mode is OnDragFinished.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.EventArgs containing the event data.
        /// </param>
        private void OnComponentDragFinished(object sender, EventArgs e)
        {
            if (this.ColourUpdateMode == Controls.ColourUpdateMode.OnDragFinished)
            {
                if (this.SelectedColour != this.InternalColour)
                {
                    this._colourChanged = true;
                    this.SelectedColour = this.InternalColour;
                }
            }
        }

        /// <summary>
        /// Called whenever the mouse down event is fired outside of the captured element. Used
        /// to make sure the popup is closed if the user clicks away from it.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.MouseButtonEventArgs containing the mouse event data.
        /// </param>
        private void OnMouseDownOutsideCapturedElement(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.Captured == this && e.OriginalSource == this)
            {
                this.Close();
            }
        }

        /// <summary>
        /// Closes the drop down and sets the colour based on the colour selected in the
        /// palette.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Controls.SelectionChangedEventArgs containing the event data.
        /// </param>
        private void OnPaletteSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            RsColourPalette palette = (RsColourPalette)sender;
            PaletteItem item = (PaletteItem)palette.SelectedItem;
            this._colourChanged = true;
            this.InternalColour = item.Colour;
            this.SelectedColour = this.InternalColour;
            this.Close();
        }

        /// <summary>
        /// Called after the update timer on the selected colour expires so that the update
        /// from internal colour to selected colour can take place.
        /// </summary>
        /// <param name="sender">
        /// The object that fired the event.
        /// </param>
        /// <param name="e">
        /// The System.EventArgs containing the event data.
        /// </param>
        private void OnTimerTick(object sender, EventArgs e)
        {
            if (this._timer != null)
            {
                this._timer.Stop();
                this._timer = null;
            }

            this._colourChanged = true;
            this.SelectedColour = this.InternalColour;
        }

        /// <summary>
        /// Called when the colour wheels selection changes. This updates the internal colour
        /// property.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.EventArgs containing the event data.
        /// </param>
        private void OnWheelSelectionChanged(object sender, EventArgs e)
        {
            if (this.ColourUpdateMode != Controls.ColourUpdateMode.None)
            {
                Color newColour = ((RsColourWheel)sender).SelectedColour;
                if (this.InternalColour != newColour)
                {
                    this._colourChanged = true;
                    this.InternalColour = newColour;
                }
            }
        }

        /// <summary>
        /// Opens this controls popup once it has been loaded. Makes sure the actual open
        /// doesn't happen until after the 1st render.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to. (This control).
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private void OpenOnLoad(object sender, RoutedEventArgs e)
        {
            Dispatcher.BeginInvoke(
                DispatcherPriority.Input,
                new DispatcherOperationCallback(delegate(object param)
                {
                    this.CoerceValue(ComboBox.IsDropDownOpenProperty);

                    return null;
                }),
                null);
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// Used as the internal view model for the items for the colour palette inside the
        /// pickers drop down.
        /// </summary>
        private class PaletteItem
        {
            #region Fields
            /// <summary>
            /// The private field used for the <see cref="Colour"/> property.
            /// </summary>
            private Color _colour;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="PaletteItem"/> class.
            /// </summary>
            /// <param name="colour">
            /// The colour that is being assigned to this palette item.
            /// </param>
            public PaletteItem(Color colour)
            {
                this._colour = colour;
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets the colour that has been assigned to this palette item.
            /// </summary>
            public Color Colour
            {
                get { return this._colour; }
            }
            #endregion Properties
        } // RsDropDownColourPicker.PaletteItem {Class}
        #endregion
    } // RSG.Editor.Controls.RsDropDownColourPicker {Class}
} // RSG.Editor.Controls {Namespace}
