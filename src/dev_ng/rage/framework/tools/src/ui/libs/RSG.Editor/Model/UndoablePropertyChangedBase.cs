﻿//---------------------------------------------------------------------------------------------
// <copyright file="UndoablePropertyChangedBase.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Model
{
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Provides an abstract class that can be inherited by classes that wish to
    /// implement the <see cref="System.ComponentModel.INotifyPropertyChanged"/>
    /// interface as well as support undo redo functionality.
    /// </summary>
    public abstract class UndoablePropertyChangedBase : NotifyPropertyChangedBase, IUndoEngineProvider
    {
        #region Fields
        /// <summary>
        /// Reference to the undo engine provider instance that this object uses.
        /// </summary>
        private IUndoEngineProvider _undoEngineProvider;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="UndoablePropertyChangedBase"/>
        /// class.
        /// </summary>
        protected UndoablePropertyChangedBase()
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="UndoablePropertyChangedBase"/>
        /// class using the specified undo engine provider.
        /// </summary>
        /// <param name="undoEngineProvider">
        /// Gets the undo engine provider instance that this class uses.
        /// </param>
        protected UndoablePropertyChangedBase(IUndoEngineProvider undoEngineProvider)
        {
            this._undoEngineProvider = undoEngineProvider;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the undo engine this object should use to store changes in.
        /// </summary>
        public virtual UndoEngine UndoEngine
        {
            get
            {
                if (this._undoEngineProvider == null)
                {
                    return null;
                }

                return this._undoEngineProvider.UndoEngine;
            }
        }

        /// <summary>
        /// Gets the undo engine provider instance that this class uses.
        /// </summary>
        protected IUndoEngineProvider UndoEngineProvider
        {
            get { return this._undoEngineProvider; }
            set { this._undoEngineProvider = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Sets the given field to the given value making sure the correct property changed
        /// event is fired.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the field that is getting set.
        /// </typeparam>
        /// <param name="field">
        /// A reference to the field that is getting set.
        /// </param>
        /// <param name="value">
        /// The value to set the field to.
        /// </param>
        /// <param name="name">
        /// The name of the property that has been changed.
        /// </param>
        /// <returns>
        /// True if the property was changed; otherwise, false.
        /// </returns>
        protected bool SetUndoableProperty<T>(ref T field, T value, [CallerMemberName]string name = "")
        {
            return this.SetUndoableProperty(ref field, value, Enumerable.Repeat(name, 1));
        }

        /// <summary>
        /// Sets the given field to the given value making sure the correct property changed
        /// event is fired.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the field that is getting set.
        /// </typeparam>
        /// <param name="field">
        /// A reference to the field that is getting set.
        /// </param>
        /// <param name="value">
        /// The value to set the field to.
        /// </param>
        /// <param name="comparer">
        /// The equality comparer to use to determine whether the value has changed.
        /// </param>
        /// <param name="name">
        /// The name of the property that has been changed.
        /// </param>
        /// <returns>
        /// True if the property was changed; otherwise, false.
        /// </returns>
        protected bool SetUndoableProperty<T>(
            ref T field,
            T value,
            IEqualityComparer<T> comparer,
            [CallerMemberName]string name = "")
        {
            return this.SetUndoableProperty(ref field, value, comparer, Enumerable.Repeat(name, 1));
        }

        /// <summary>
        /// Sets the given field to the given value making sure the correct property changed
        /// event is fired.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the field that is getting set.
        /// </typeparam>
        /// <param name="field">
        /// A reference to the field that is getting set.
        /// </param>
        /// <param name="value">
        /// The value to set the field to.
        /// </param>
        /// <param name="names">
        /// An array of property names that are changed due to this one change.
        /// </param>
        /// <returns>
        /// True if the property was changed; otherwise, false.
        /// </returns>
        protected bool SetUndoableProperty<T>(ref T field, T value, params string[] names)
        {
            return this.SetUndoableProperty(ref field, value, Enumerable.Distinct(names));
        }

        /// <summary>
        /// Sets the given field to the given value making sure the correct property changed
        /// event is fired.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the field that is getting set.
        /// </typeparam>
        /// <param name="field">
        /// A reference to the field that is getting set.
        /// </param>
        /// <param name="value">
        /// The value to set the field to.
        /// </param>
        /// <param name="comparer">
        /// The equality comparer to use to determine whether the value has changed.
        /// </param>
        /// <param name="names">
        /// An array of property names that are changed due to this one change.
        /// </param>
        /// <returns>
        /// True if the property was changed; otherwise, false.
        /// </returns>
        protected bool SetUndoableProperty<T>(
            ref T field, T value, IEqualityComparer<T> comparer, params string[] names)
        {
            return this.SetUndoableProperty(ref field, value, comparer, Enumerable.Distinct(names));
        }

        /// <summary>
        /// Sets the given field to the given value making sure the correct property changed
        /// event is fired.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the field that is getting set.
        /// </typeparam>
        /// <param name="field">
        /// A reference to the field that is getting set.
        /// </param>
        /// <param name="value">
        /// The value to set the field to.
        /// </param>
        /// <param name="names">
        /// A iterator around all of property names that are changed due to this one change.
        /// </param>
        /// <returns>
        /// True if the property was changed; otherwise, false.
        /// </returns>
        protected bool SetUndoableProperty<T>(ref T field, T value, IEnumerable<string> names)
        {
            return SetUndoableProperty(ref field, value, EqualityComparer<T>.Default, names);
        }

        /// <summary>
        /// Sets the given field to the given value making sure the correct property changed
        /// event is fired.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the field that is getting set.
        /// </typeparam>
        /// <param name="field">
        /// A reference to the field that is getting set.
        /// </param>
        /// <param name="value">
        /// The value to set the field to.
        /// </param>
        /// <param name="comparer">
        /// The equality comparer to use to determine whether the value has changed.
        /// </param>
        /// <param name="names">
        /// A iterator around all of property names that are changed due to this one change.
        /// </param>
        /// <returns>
        /// True if the property was changed; otherwise, false.
        /// </returns>
        protected bool SetUndoableProperty<T>(
            ref T field, T value, IEqualityComparer<T> comparer, IEnumerable<string> names)
        {
            T oldValue = field;
            if (!base.SetProperty<T>(ref field, value, comparer, names))
            {
                return false;
            }

            UndoEngine undoEngine = this.UndoEngine;
            if (undoEngine != null && undoEngine.PerformingEvent == false)
            {
                string name = names.FirstOrDefault();
                PropertyChange<T> change = new PropertyChange<T>(oldValue, value, this, name);
                undoEngine.CreateUndoEvent(change);
            }

            return true;
        }
        #endregion
    }
}
