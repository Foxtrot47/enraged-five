﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace RSG.Editor.Controls.CurveEditor
{
    /// <summary>
    /// 
    /// </summary>
    public class RulerTickBar : FrameworkElement
    {
        #region Ruler Dependency Properties
        /// <summary>
        /// Identifies the <see cref="TickBrush"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty TickBrushProperty =
            DependencyProperty.Register(
                "TickBrush",
                typeof(Brush),
                typeof(RulerTickBar),
                new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsRender));

        /// <summary>
        /// Gets or sets the <see cref="Brush"/> that is used to draw the tick marks.
        /// </summary>
        public Brush TickBrush
        {
            get { return (Brush)GetValue(TickBrushProperty); }
            set { SetValue(TickBrushProperty, value); }
        }

        /// <summary>
        /// Identifies the <see cref="Placement"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty PlacementProperty =
            DependencyProperty.Register(
                "Placement",
                typeof(TickBarPlacement),
                typeof(RulerTickBar),
                new FrameworkPropertyMetadata(TickBarPlacement.Top, FrameworkPropertyMetadataOptions.AffectsRender));

        /// <summary>
        /// Location where this ruler is placed.
        /// </summary>
        public TickBarPlacement Placement
        {
            get { return (TickBarPlacement)GetValue(PlacementProperty); }
            set { SetValue(PlacementProperty, value); }
        }

        /// <summary>
        /// Identifies the <see cref="TickWidth"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty TickWidthProperty =
            DependencyProperty.Register(
                "TickWidth",
                typeof(double),
                typeof(RulerTickBar),
                new FrameworkPropertyMetadata(10.0, FrameworkPropertyMetadataOptions.AffectsRender));

        /// <summary>
        /// Width in pixels between each tick.
        /// </summary>
        public double TickWidth
        {
            get { return (double)GetValue(TickWidthProperty); }
            set { SetValue(TickWidthProperty, value); }
        }

        /// <summary>
        /// Identifies the <see cref="TickIncrement"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty TickIncrementProperty =
            DependencyProperty.Register(
                "TickIncrement",
                typeof(double),
                typeof(RulerTickBar),
                new FrameworkPropertyMetadata(1.0, FrameworkPropertyMetadataOptions.AffectsRender));

        /// <summary>
        /// Value differential between each tick.
        /// </summary>
        public double TickIncrement
        {
            get { return (double)GetValue(TickIncrementProperty); }
            set { SetValue(TickIncrementProperty, value); }
        }

        /// <summary>
        /// Minimum value to display on the ruler.
        /// </summary>
        public static readonly DependencyProperty MinimumProperty =
            DependencyProperty.Register(
                "Minimum",
                typeof(double),
                typeof(RulerTickBar),
                new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.AffectsRender));

        /// <summary>
        /// 
        /// </summary>
        public double Minimum
        {
            get { return (double)GetValue(MinimumProperty); }
            set { SetValue(MinimumProperty, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public static readonly DependencyProperty MaximumProperty =
            DependencyProperty.Register(
                "Maximum",
                typeof(double),
                typeof(RulerTickBar),
                new FrameworkPropertyMetadata(10.0, FrameworkPropertyMetadataOptions.AffectsRender));

        /// <summary>
        /// 
        /// </summary>
        public double Maximum
        {
            get { return (double)GetValue(MaximumProperty); }
            set { SetValue(MaximumProperty, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public static readonly DependencyProperty LabelPositionProperty =
            DependencyProperty.Register(
                "LabelPosition",
                typeof(TickBarLabelPosition),
                typeof(RulerTickBar),
                new FrameworkPropertyMetadata(TickBarLabelPosition.On, FrameworkPropertyMetadataOptions.AffectsRender));

        /// <summary>
        /// Location where the label gets position relative to the ruler's tick.
        /// </summary>
        public TickBarLabelPosition LabelPosition
        {
            get { return (TickBarLabelPosition)GetValue(LabelPositionProperty); }
            set { SetValue(LabelPositionProperty, value); }
        }

        /// <summary>
        /// Identifies the <see cref="InvertAxis"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty InvertAxisProperty =
            DependencyProperty.Register(
                "InvertAxis",
                typeof(bool),
                typeof(RulerTickBar),
                new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.AffectsRender));

        /// <summary>
        /// Used to reverse the direction of the ruler.
        /// </summary>
        public bool InvertAxis
        {
            get { return (bool)GetValue(InvertAxisProperty); }
            set { SetValue(InvertAxisProperty, value); }
        }
        #endregion // Ruler Dependency Properties

        #region Font Dependeny Properties
        /// <summary>
        /// 
        /// </summary>
        public Brush Foreground
        {
            get { return (Brush)GetValue(Control.ForegroundProperty); }
            set { SetValue(Control.ForegroundProperty, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public FontFamily FontFamily
        {
            get { return (FontFamily)GetValue(Control.FontFamilyProperty); }
            set { SetValue(Control.FontFamilyProperty, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public double FontSize
        {
            get { return (double)GetValue(Control.FontSizeProperty); }
            set { SetValue(Control.FontSizeProperty, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public FontStretch FontStretch
        {
            get { return (FontStretch)GetValue(Control.FontStretchProperty); }
            set { SetValue(Control.FontStretchProperty, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public FontStyle FontStyle
        {
            get { return (FontStyle)GetValue(Control.FontStyleProperty); }
            set { SetValue(Control.FontStyleProperty, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public FontWeight FontWeight
        {
            get { return (FontWeight)GetValue(Control.FontWeightProperty); }
            set { SetValue(Control.FontWeightProperty, value); }
        }
        #endregion // Font Dependency Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        static RulerTickBar()
        {
            Control.ForegroundProperty.AddOwner(typeof(RulerTickBar));
            Control.FontFamilyProperty.AddOwner(typeof(RulerTickBar));
            Control.FontSizeProperty.AddOwner(typeof(RulerTickBar));
            Control.FontStretchProperty.AddOwner(typeof(RulerTickBar));
            Control.FontStyleProperty.AddOwner(typeof(RulerTickBar));
            Control.FontWeightProperty.AddOwner(typeof(RulerTickBar));
        }
        #endregion // Constructor(s)

        #region OnRender Override
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dc"></param>
        protected override void OnRender(DrawingContext dc)
        {
            // Determine the number of ticks we need to render.
            int tickCount = (int)(Math.Abs(Maximum - Minimum) / TickWidth);
            int firstTickIndex = (int)Math.Ceiling(Minimum / TickWidth);
            double offsetToFirstTick = -Minimum % TickWidth;
            if (offsetToFirstTick < 0.0)
            {
                offsetToFirstTick += TickWidth;
            }

            // Draw each tick along with it's corresponding text.
            for (int i = 0; i <= tickCount; i++)
            {
                int textIndex = Convert.ToInt32(TickIncrement * (i + firstTickIndex));
                if (InvertAxis)
                {
                    textIndex = -textIndex;
                }

                String text = Convert.ToString(textIndex, 10);
                FormattedText formattedText =
                    new FormattedText(
                        text,
                        CultureInfo.InvariantCulture,
                        FlowDirection.LeftToRight,
                        new Typeface(FontFamily, FontStyle, FontWeight, FontStretch),
                        FontSize,
                        Foreground);

                double tickOffset = offsetToFirstTick + (TickWidth * i);
                dc.DrawText(formattedText, GetTextPoint(tickOffset, formattedText.Width, formattedText.Height));
                dc.DrawLine(
                    new Pen(TickBrush, 1.0),
                    GetLineStart(tickOffset, formattedText.Width, formattedText.Height),
                    GetLineEnd(tickOffset, formattedText.Width, formattedText.Height));
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="tickOffset"></param>
        /// <param name="textWidth"></param>
        /// <param name="textHeight"></param>
        /// <returns></returns>
        private Point GetTextPoint(double tickOffset, double textWidth, double textHeight)
        {
            // Calculate the text's X position.
            double textX = 0.0;
            if (Placement == TickBarPlacement.Top || Placement == TickBarPlacement.Bottom)
            {
                if (LabelPosition == TickBarLabelPosition.On)
                    textX = tickOffset - (textWidth / 2.0);
                else if (LabelPosition == TickBarLabelPosition.Before)
                    textX = tickOffset - textWidth;
                else if (LabelPosition == TickBarLabelPosition.After)
                    textX = tickOffset;
            }
            else if (Placement == TickBarPlacement.Right)
            {
                textX = ActualWidth - textWidth;
            }

            // Calculate the text's Y position.
            double textY = 0.0;
            if (Placement == TickBarPlacement.Bottom)
            {
                textY = ActualHeight - textHeight;
            }
            else if (Placement == TickBarPlacement.Left || Placement == TickBarPlacement.Right)
            {
                if (LabelPosition == TickBarLabelPosition.On)
                    textY = tickOffset - (textHeight / 2.0);
                else if (LabelPosition == TickBarLabelPosition.Before)
                    textY = tickOffset - textHeight;
                else if (LabelPosition == TickBarLabelPosition.After)
                    textY = tickOffset;
            }

            return new Point(RoundToPoint5(textX), RoundToPoint5(textY));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tickOffset"></param>
        /// <param name="textWidth"></param>
        /// <param name="textHeight"></param>
        /// <returns></returns>
        private Point GetLineStart(double tickOffset, double textWidth, double textHeight)
        {
            double startX = 0.0;
            if (Placement == TickBarPlacement.Top || Placement == TickBarPlacement.Bottom)
            {
                startX = tickOffset;
            }
            else if (Placement == TickBarPlacement.Left && LabelPosition == TickBarLabelPosition.On)
            {
                startX = textWidth;
            }

            double startY = 0.0;
            if (Placement == TickBarPlacement.Top && LabelPosition == TickBarLabelPosition.On)
            {
                startY = textHeight;
            }
            else if (Placement == TickBarPlacement.Left || Placement == TickBarPlacement.Right)
            {
                startY = tickOffset;
            }

            return new Point(RoundToPoint5(startX), RoundToPoint5(startY));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tickOffset"></param>
        /// <param name="textWidth"></param>
        /// <param name="textHeight"></param>
        /// <returns></returns>
        private Point GetLineEnd(double tickOffset, double textWidth, double textHeight)
        {
            double endX = 0.0;
            if (Placement == TickBarPlacement.Top || Placement == TickBarPlacement.Bottom)
            {
                endX = tickOffset;
            }
            else if (Placement == TickBarPlacement.Left)
            {
                endX = ActualWidth;
            }
            else if (Placement == TickBarPlacement.Right)
            {
                if (LabelPosition == TickBarLabelPosition.On)
                    endX = ActualWidth - textWidth;
                else
                    endX = ActualWidth;
            }

            double endY = 0.0;
            if (Placement == TickBarPlacement.Top)
            {
                endY = ActualHeight;
            }
            else if (Placement == TickBarPlacement.Bottom)
            {
                if (LabelPosition == TickBarLabelPosition.On)
                    endY = ActualHeight - textHeight;
                else
                    endY = ActualHeight;
            }
            else if (Placement == TickBarPlacement.Left || Placement == TickBarPlacement.Right)
            {
                endY = tickOffset;
            }

            return new Point(RoundToPoint5(endX), RoundToPoint5(endY));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private double RoundToPoint5(double value)
        {
            return Math.Round(value - 0.5, MidpointRounding.AwayFromZero) + 0.5;
        }
        #endregion // OnRender Override
    } // RulerElement
}
