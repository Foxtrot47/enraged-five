﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsColourWheel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Threading;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Threading;
    using RSG.Editor.Controls.Helpers;

    /// <summary>
    /// Represents a control that displays a colour wheel showing hue and saturation.
    /// </summary>
    public class RsColourWheel : Control
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="ColourUpdateMode" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty ColourUpdateModeProperty;

        /// <summary>
        /// Identifies the <see cref="SelectedColour" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty SelectedColourProperty;

        /// <summary>
        /// Identifies the <see cref="InternalColour" /> dependency property.
        /// </summary>
        private static readonly DependencyProperty _internalColourProperty;

        /// <summary>
        /// A private value indicating whether on colour change the position should be updated.
        /// </summary>
        private bool _dontUpdatePositionOnColourChanged;

        /// <summary>
        /// The private reference to the PART_DragArea control that has been setup in the
        /// controls template.
        /// </summary>
        private FrameworkElement _dragArea;

        /// <summary>
        /// The private reference to the PART_Picker control that has been setup in the
        /// controls template.
        /// </summary>
        private Canvas _picker;

        /// <summary>
        /// The render transform that is attached to the picker so that it can be moved around
        /// during a drag event.
        /// </summary>
        private TranslateTransform _pickerTransform;

        /// <summary>
        /// The dispatcher timer used to delay the colour update by a certain time span when
        /// the <see cref="ColourUpdateMode"/> is set to Delay.
        /// </summary>
        private DispatcherTimer _timer;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsColourWheel" /> class.
        /// </summary>
        static RsColourWheel()
        {
            _internalColourProperty =
                DependencyProperty.Register(
                "InternalColour",
                typeof(Color),
                typeof(RsColourWheel),
                new FrameworkPropertyMetadata(
                    Colors.Transparent,
                    OnInternalColourChanged));

            SelectedColourProperty =
                DependencyProperty.Register(
                "SelectedColour",
                typeof(Color),
                typeof(RsColourWheel),
                new FrameworkPropertyMetadata(
                    Colors.Transparent,
                    FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                    OnColourChanged));

            ColourUpdateModeProperty =
                DependencyProperty.Register(
                "ColourUpdateMode",
                typeof(ColourUpdateMode),
                typeof(RsColourWheel),
                new FrameworkPropertyMetadata(ColourUpdateMode.Realtime));

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsColourWheel),
                new FrameworkPropertyMetadata(typeof(RsColourWheel)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsColourWheel" /> class.
        /// </summary>
        public RsColourWheel()
        {
            this._pickerTransform = new TranslateTransform();
            this._pickerTransform.X = 0.0;
            this._pickerTransform.Y = 0.0;
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// Occurs after the selected colour property has changed.
        /// </summary>
        public event EventHandler SelectedColourChanged;

        /// <summary>
        /// Occurs as soon as the drag operation has finished.
        /// </summary>
        public event EventHandler DragFinished;
        #endregion Events

        #region Properties
        /// <summary>
        /// Gets or sets the colour update mode that has been set on this control. This
        /// controls when and how often the selected colour gets updated from the user actions.
        /// </summary>
        public ColourUpdateMode ColourUpdateMode
        {
            get { return (ColourUpdateMode)this.GetValue(ColourUpdateModeProperty); }
            set { this.SetValue(ColourUpdateModeProperty, value); }
        }

        /// <summary>
        /// Gets or sets the selected colour.
        /// </summary>
        public Color SelectedColour
        {
            get { return (Color)this.GetValue(SelectedColourProperty); }
            set { this.SetValue(SelectedColourProperty, value); }
        }

        /// <summary>
        /// Gets or sets the internal selected colour. This always gets updated in real time
        /// no matter what the <see cref="ColourUpdateMode"/> property has been set to.
        /// </summary>
        private Color InternalColour
        {
            get { return (Color)this.GetValue(_internalColourProperty); }
            set { this.SetValue(_internalColourProperty, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Gets called whenever the template is applied to this control. This makes sure we
        /// have references to the selector and drag area objects in the template as well as
        /// handle all of the events for them.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            if (this._picker != null)
            {
                this._picker.MouseLeftButtonDown -= this.OnDragAreaMouseLeftButtonDown;
                this._picker.MouseLeftButtonUp -= this.OnDragAreaMouseLeftButtonUp;
                this._picker.RenderTransform = null;
            }

            if (this._dragArea != null)
            {
                this._dragArea.MouseLeftButtonDown -= this.OnDragAreaMouseLeftButtonDown;
                this._dragArea.MouseLeftButtonUp -= this.OnDragAreaMouseLeftButtonUp;
                this._dragArea.MouseMove -= this.OnDragAreaMouseMove;
            }

            this._picker = GetTemplateChild("PART_Picker") as Canvas;
            if (this._picker != null)
            {
                this._picker.MouseLeftButtonDown += this.OnDragAreaMouseLeftButtonDown;
                this._picker.MouseLeftButtonUp += this.OnDragAreaMouseLeftButtonUp;
                this._picker.RenderTransform = this._pickerTransform;
            }

            this._dragArea = GetTemplateChild("PART_DragArea") as FrameworkElement;
            if (this._dragArea != null)
            {
                this._dragArea.MouseLeftButtonDown += this.OnDragAreaMouseLeftButtonDown;
                this._dragArea.MouseLeftButtonUp += this.OnDragAreaMouseLeftButtonUp;
                this._dragArea.MouseMove += this.OnDragAreaMouseMove;
                this._dragArea.SizeChanged += this.OnDragAreaSizeChanged;
            }

            this.SetPosition();
        }

        /// <summary>
        /// Called whenever the <see cref="SelectedColour"/> dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The object whose <see cref="SelectedColour"/> dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnColourChanged(object s, DependencyPropertyChangedEventArgs e)
        {
            RsColourWheel wheel = s as RsColourWheel;
            if (wheel == null)
            {
                Debug.Assert(false, "Incorrect dependency property changed cast.");
                return;
            }

            if (!wheel._dontUpdatePositionOnColourChanged)
            {
                wheel.SetPosition();
            }

            EventHandler handler = wheel.SelectedColourChanged;
            if (handler != null)
            {
                handler(wheel, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Called whenever the <see cref="InternalColour"/> dependency property changes.
        /// </summary>
        /// <param name="s">
        /// The object whose <see cref="InternalColour"/> dependency property changed.
        /// </param>
        /// <param name="e">
        /// The System.Windows.DependencyPropertyChangedEventArgs data that describes the
        /// change made to the property.
        /// </param>
        private static void OnInternalColourChanged(
            object s, DependencyPropertyChangedEventArgs e)
        {
            RsColourWheel wheel = s as RsColourWheel;
            if (wheel == null)
            {
                Debug.Assert(false, "Incorrect dependency property changed cast.");
                return;
            }

            if (wheel._timer != null)
            {
                wheel._timer.Stop();
                wheel._timer = null;
            }

            if (wheel.ColourUpdateMode == ColourUpdateMode.Realtime)
            {
                wheel.SetCurrentValue(RsColourWheel.SelectedColourProperty, e.NewValue);
            }
            else if (wheel.ColourUpdateMode == ColourUpdateMode.Delay)
            {
                wheel._timer = new DispatcherTimer();
                wheel._timer.Interval = TimeSpan.FromMilliseconds(500);
                wheel._timer.Tick += wheel.OnTimerTick;
                wheel._timer.Start();
            }
        }

        /// <summary>
        /// Calculates and sets the colour based on the current mouse position relative to the
        /// drag area.
        /// </summary>
        /// <param name="e">
        /// The mouse event arguments that are used to get the current position of the mouse
        /// relative to the drag area.
        /// </param>
        private void CalculateColor(MouseEventArgs e)
        {
            if (this._dragArea == null)
            {
                return;
            }

            Point position = e.GetPosition(this._dragArea);

            double x = Math.Min(Math.Truncate(position.X), this._dragArea.ActualWidth);
            x = Math.Max(x, 0.0);
            double y = Math.Min(Math.Truncate(position.Y), this._dragArea.ActualHeight);
            y = Math.Max(y, 0.0);

            double radius = this._dragArea.ActualWidth / 2.0;
            double distanceX = Math.Abs(x - radius);
            double distanceY = Math.Abs(y - radius);
            double distanceSquared = (distanceX * distanceX) + (distanceY * distanceY);
            double angle = Math.Atan(distanceY / distanceX);
            if (distanceSquared >= (radius * radius))
            {
                if (distanceX != 0.0)
                {
                    distanceY = radius * Math.Sin(angle);
                    distanceX = radius * Math.Cos(angle);
                }
                else
                {
                    distanceY = radius;
                }
            }

            distanceSquared = (distanceX * distanceX) + (distanceY * distanceY);
            double distance = Math.Sqrt(distanceSquared);

            angle = (angle * 180.0d) / Math.PI;
            if (angle == 0.0 && x < radius)
            {
                angle = 180.0d;
            }

            if (y > radius)
            {
                if (x == radius)
                {
                    angle = 270.0;
                }
                else if (x > radius)
                {
                    angle = 360.0d - angle;
                }
                else if (x < radius)
                {
                    angle = 180.0d + angle;
                }
            }
            else if (y < radius)
            {
                if (x < radius)
                {
                    angle = 180.0d - angle;
                }
            }

            this._dontUpdatePositionOnColourChanged = true;
            try
            {
                HsvColour hsv = new HsvColour(angle, distance / radius, 1.0);
                this.InternalColour = ColourUtilities.ConvertHsvToRgb(hsv);
            }
            finally
            {
                this._dontUpdatePositionOnColourChanged = false;
            }
        }

        /// <summary>
        /// Called whenever the mouse lefts button is pressed down on the drag area or the
        /// picker. This captures the mouse device, moves the picker and starts the drag
        /// process.
        /// </summary>
        /// <param name="sender">
        /// The object that fired the event.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.MouseButtonEventArgs containing the event data as well as
        /// the mouse device that is used to retrieve the relative positions.
        /// </param>
        private void OnDragAreaMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (this._dragArea == null)
            {
                return;
            }

            Point point = e.GetPosition(this._dragArea);
            this.UpdatePickerPositionAndColour(e);
            this._dragArea.CaptureMouse();
            e.Handled = true;
        }

        /// <summary>
        /// Called whenever the mouse lefts button is released on the drag area or the picker.
        /// This releases the capture on the mouse device, and stops the drag process.
        /// </summary>
        /// <param name="sender">
        /// The object that fired the event.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.MouseButtonEventArgs containing the event data.
        /// </param>
        private void OnDragAreaMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (this._dragArea == null)
            {
                return;
            }

            this._dragArea.ReleaseMouseCapture();
            EventHandler handler = this.DragFinished;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }

            if (this.ColourUpdateMode == ColourUpdateMode.OnDragFinished)
            {
                this._dontUpdatePositionOnColourChanged = true;
                try
                {
                    this.SetCurrentValue(
                        RsColourWheel.SelectedColourProperty, this.InternalColour);
                }
                finally
                {
                    this._dontUpdatePositionOnColourChanged = false;
                }
            }
        }

        /// <summary>
        /// Called whenever the mouse moves over the drag area or picker. This updates the
        /// pickers location if the left button is pressed.
        /// </summary>
        /// <param name="sender">
        /// The object that fired the event.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.MouseEventArgs containing the event data.
        /// </param>
        private void OnDragAreaMouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                this.UpdatePickerPositionAndColour(e);
                Mouse.Synchronize();
            }
        }

        /// <summary>
        /// Called whenever the render size of the dragging area changes. This updates the
        /// pickers location based on the current selected colour.
        /// </summary>
        /// <param name="sender">
        /// The object that fired the event.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.MouseEventArgs containing the event data.
        /// </param>
        private void OnDragAreaSizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.SetPosition();
        }

        /// <summary>
        /// Called after the update timer on the selected colour expires so that the update
        /// from internal colour to selected colour can take place.
        /// </summary>
        /// <param name="sender">
        /// The object that fired the event.
        /// </param>
        /// <param name="e">
        /// The System.EventArgs containing the event data.
        /// </param>
        private void OnTimerTick(object sender, EventArgs e)
        {
            this._timer.Stop();
            this._timer = null;
            this._dontUpdatePositionOnColourChanged = true;
            try
            {
                this.SetCurrentValue(
                    RsColourWheel.SelectedColourProperty, this.InternalColour);
            }
            finally
            {
                this._dontUpdatePositionOnColourChanged = false;
            }
        }

        /// <summary>
        /// Sets the position of the picker based on the current selected colour.
        /// </summary>
        private void SetPosition()
        {
            if (this._dragArea == null)
            {
                return;
            }

            Color colour = this.SelectedColour;
            HsvColour hsv = ColourUtilities.ConvertRgbToHsv(colour);

            double angle = hsv.H;
            double distance = hsv.S * (this._dragArea.ActualWidth / 2.0d);

            double radians = (angle * Math.PI) / 180.0d;

            double distanceX = Math.Cos(radians) * distance;
            double distanceY = Math.Sin(radians) * distance;

            this._pickerTransform.X = distanceX;

            if (angle >= 0 && angle <= 180)
            {
                if (distanceY <= 0.0d)
                {
                    this._pickerTransform.Y = distanceY;
                }
                else
                {
                    this._pickerTransform.Y = -distanceY;
                }
            }
            else
            {
                if (distanceY >= 0.0d)
                {
                    this._pickerTransform.Y = distanceY;
                }
                else
                {
                    this._pickerTransform.Y = -distanceY;
                }
            }
        }

        /// <summary>
        /// Updates the pickers position based on the current position of the mouse relative to
        /// the drag area.
        /// </summary>
        /// <param name="position">
        /// The mouse position relative to the drag area.
        /// </param>
        private void UpdatePickerPosition(Point position)
        {
            if (this._dragArea == null)
            {
                return;
            }

            if (position.Y < 0)
            {
                position.Y = 0;
            }

            if (position.X < 0)
            {
                position.X = 0;
            }

            if (position.X > this._dragArea.ActualWidth)
            {
                position.X = this._dragArea.ActualWidth;
            }

            if (position.Y > this._dragArea.ActualHeight)
            {
                position.Y = this._dragArea.ActualHeight;
            }

            double radius = this._dragArea.ActualWidth / 2.0;
            double x = position.X - radius;
            double y = position.Y - radius;
            double distanceSquared = (x * x) + (y * y);
            if (distanceSquared >= ((radius - 1) * (radius - 1)))
            {
                double angle = 0.0;
                if (x != 0.0)
                {
                    angle = Math.Atan(Math.Abs(y) / Math.Abs(x));
                    y = (radius - 1) * Math.Sin(angle) * (y < 0 ? -1.0 : 1.0);
                    x = (radius - 1) * Math.Cos(angle) * (x < 0 ? -1.0 : 1.0);
                }
                else
                {
                    double factor = y < 0 ? -1.0 : 1.0;
                    y = (radius - 1) * factor;
                }
            }

            this._pickerTransform.X = x;
            this._pickerTransform.Y = y;
        }

        /// <summary>
        /// Updates the pickers position based on the position of the mouse relative to the
        /// drag area and updates the colour.
        /// </summary>
        /// <param name="e">
        /// A object that can be used to retrieve the position of the mouse relative to a
        /// control.
        /// </param>
        private void UpdatePickerPositionAndColour(MouseEventArgs e)
        {
            if (this._dragArea == null)
            {
                return;
            }

            Point position = e.GetPosition(this._dragArea);
            this.UpdatePickerPosition(position);
            this.CalculateColor(e);
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsColourWheel {Class}
} // RSG.Editor.Controls {Namespace}
