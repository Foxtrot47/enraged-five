﻿//---------------------------------------------------------------------------------------------
// <copyright file="CopyAndPasteLineAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Commands
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Windows;
    using System.Xml;
    using RSG.Editor;
    using RSG.Text.Model;
    using RSG.Text.ViewModel;

    /// <summary>
    /// Provides a base class for the Copy and Paste commands for lines.
    /// </summary>
    public abstract class CopyAndPasteLineAction : ButtonAction<TextLineCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CopyAndPasteLineAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public CopyAndPasteLineAction(ParameterResolverDelegate<TextLineCommandArgs> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the clipboard currently contains line data.
        /// </summary>
        /// <returns>
        /// True if the clipboard currently contains line data; otherwise, false.
        /// </returns>
        protected bool ClipboardContainsLineData()
        {
            Type type = typeof(List<ILine>);
            string formatName = type.FullName;
            DataFormat dataFormat = DataFormats.GetDataFormat(formatName);

            return Clipboard.ContainsData(dataFormat.Name);
        }

        /// <summary>
        /// Copies the specified lines to the clipboard.
        /// </summary>
        /// <param name="lines">
        /// The lines to copy to the clipboard.
        /// </param>
        protected void CopyLinesToClipboard(IEnumerable<LineViewModel> lines)
        {
            Clipboard.Clear();
            StringBuilder sb = new StringBuilder();
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.ConformanceLevel = ConformanceLevel.Fragment;
            using (XmlWriter writer = XmlWriter.Create(new StringWriter(sb), settings))
            {
                foreach (LineViewModel line in lines)
                {
                    writer.WriteStartElement("Item");
                    line.Model.Serialise(writer);
                    writer.WriteEndElement();
                }
            }

            Type type = typeof(List<ILine>);
            string formatName = type.FullName;
            DataFormat dataFormat = DataFormats.GetDataFormat(formatName);

            DataObject dataObject = new DataObject();

            string data = sb.ToString();
            dataObject.SetData(dataFormat.Name, data, false);
            Clipboard.SetDataObject(dataObject, false);
        }

        /// <summary>
        /// Retrieves any lines that are currently on the clipboard and adds them to the
        /// specified conversation at the specified index.
        /// </summary>
        /// <param name="conversation">
        /// The conversation the lines should be added to.
        /// </param>
        /// <param name="index">
        /// The zero-based index the lines on the clipboard should be added at.
        /// </param>
        /// <returns>
        /// The lines that have been created from the data on the clipboard.
        /// </returns>
        protected List<ILine> GetDataFromClipboard(Conversation conversation, int index)
        {
            List<ILine> lines = new List<ILine>();
            IDataObject dataObject = Clipboard.GetDataObject();
            if (dataObject == null)
            {
                return lines;
            }

            string dataFormat = typeof(List<ILine>).FullName;
            if (!dataObject.GetDataPresent(dataFormat))
            {
                return lines;
            }

            string rawData = dataObject.GetData(dataFormat) as string;
            if (rawData == null)
            {
                return lines;
            }

            XmlReaderSettings settings = new XmlReaderSettings();
            settings.ConformanceLevel = ConformanceLevel.Fragment;
            using (XmlReader reader = XmlReader.Create(new StringReader(rawData), settings))
            {
                reader.MoveToContent();
                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    if (String.Equals(reader.Name, "Item"))
                    {
                        lines.Add(conversation.AddDeserialisedLineAt(reader, index++));
                    }

                    reader.Skip();
                }
            }

            return lines;
        }
        #endregion Methods
    } // RSG.Text.Commands.CopyAndPasteLineAction {Class}
} // RSG.Text.Commands {Namespace}
