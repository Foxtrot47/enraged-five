﻿//---------------------------------------------------------------------------------------------
// <copyright file="ErrorStringTable.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Resources
{
    using System;
    using System.Globalization;
    using System.Resources;
    using RSG.Base;
    using RSG.Base.Extensions;

    /// <summary>
    /// Provides the ability to lookup string resources inside the local string table.
    /// </summary>
    internal static class ErrorStringTable
    {
        #region Fields
        /// <summary>
        /// The private reference to the resource manager that contains all of the strings
        /// located inside the string table.
        /// </summary>
        private static ResourceManager _manager = InitialiseManager();
        #endregion Fields

        #region Methods
        /// <summary>
        /// Retrieves the embedded string resource indexed by the specified identifier.
        /// </summary>
        /// <param name="id">
        /// The identifier for the embedded string resource to retrieve.
        /// </param>
        /// <returns>
        /// The embedded string resource indexed by the specified identifier.
        /// </returns>
        public static string GetString(string id)
        {
            if (String.IsNullOrWhiteSpace(id))
            {
                return "INVALID_RESOURCE_ID";
            }

            return TryToGetString(id, CultureInfo.CurrentUICulture).Value;
        }

        /// <summary>
        /// Retrieves the embedded string resource indexed by the specified identifier
        /// formatted with the specified arguments.
        /// </summary>
        /// <param name="id">
        /// The identifier for the embedded string resource to retrieve.
        /// </param>
        /// <param name="args">
        /// An object array that contains zero or more objects to format.
        /// </param>
        /// <returns>
        /// The embedded string resource indexed by the specified identifier in which the
        /// format items have been replaced by the string representations of the corresponding
        /// objects in <paramref name="args"/>.
        /// </returns>
        public static string GetString(string id, params string[] args)
        {
            if (String.IsNullOrWhiteSpace(id))
            {
                return "INVALID_RESOURCE_ID";
            }

            TryResult<string> result = TryToGetString(id, CultureInfo.CurrentUICulture);
            if (result.Success == false)
            {
                return result.Value;
            }

            try
            {
                return result.Value.FormatCurrentUI(args);
            }
            catch (ArgumentNullException)
            {
                return "LOADING_'{0}'_RESOURCE_FAILED".FormatCurrent(id);
            }
            catch (FormatException)
            {
                return "INVALID_FORMAT_ON_'{0}'_RESOURCE".FormatCurrent(id);
            }
        }

        /// <summary>
        /// Creates the resource manager that contains the embedded string resources.
        /// </summary>
        /// <returns>
        /// The newly created System.Resources.ResourceManager object.
        /// </returns>
        private static ResourceManager InitialiseManager()
        {
            string baseName = typeof(ErrorStringTable).Namespace + ".ErrorStringTable";
            return new ResourceManager(baseName, typeof(ErrorStringTable).Assembly);
        }

        /// <summary>
        /// Retrieves the embedded string resource indexed by the specified identifier.
        /// </summary>
        /// <param name="id">
        /// The identifier for the embedded string resource to retrieve.
        /// </param>
        /// <param name="cultureInfo">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        /// <returns>
        /// A structure containing the result of the operation and a value indicating whether
        /// the resource was successfully located.
        /// </returns>
        private static TryResult<string> TryToGetString(string id, CultureInfo cultureInfo)
        {
            try
            {
                string resource = _manager.GetString(id, cultureInfo);
                return new TryResult<string>(resource, true);
            }
            catch (ArgumentNullException)
            {
                return new TryResult<string>("INVALID_RESOURCE_ID", false);
            }
            catch (InvalidOperationException)
            {
                string value = "LOADING_'{0}'_RESOURCE_FAILED".FormatCurrent(id);
                return new TryResult<string>(value, false);
            }
            catch (MissingManifestResourceException)
            {
                string value = "RESOURCE_'{0}'_MISSING".FormatCurrent(id);
                return new TryResult<string>(value, false);
            }
            catch (MissingSatelliteAssemblyException)
            {
                string value = "RESOURCE_'{0}'_MISSING".FormatCurrent(id);
                return new TryResult<string>(value, false);
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.Resources.ErrorStringTable {Static Class}
} // RSG.Editor.Controls.Resources {Namespace}
