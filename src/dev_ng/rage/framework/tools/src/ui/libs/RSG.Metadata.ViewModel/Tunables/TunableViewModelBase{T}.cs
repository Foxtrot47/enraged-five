﻿//---------------------------------------------------------------------------------------------
// <copyright file="TunableViewModelBase{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Tunables
{
    using System;
    using System.ComponentModel;
    using System.Windows.Media.Imaging;
    using RSG.Editor;
    using RSG.Editor.View;
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// Provides a base view model class that is used for view models wrapping a tunable
    /// class type.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the tunable that is being wrapped by this view model.
    /// </typeparam>
    public abstract class TunableViewModelBase<T>
        : ValidatingViewModelBase<T>,
        ITunableViewModel,
        IProvidesExpandableState,
        IPrioritisedComparable where T : ITunable
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="MapKey"/> property.
        /// </summary>
        private MapKeyViewModel _mapKey;

        /// <summary>
        /// The private field used for the <see cref="Parent"/> property.
        /// </summary>
        private ITunableViewModel _parent;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="TunableViewModelBase{T}"/> class.
        /// </summary>
        /// <param name="tunable">
        /// The tunable that this view model is representing.
        /// </param>
        /// <param name="parent">
        /// The parent tunable view model for this view model.
        /// </param>
        protected TunableViewModelBase(T tunable, ITunableViewModel parent)
            : base(tunable, true, true)
        {
            this._parent = parent;
            this.Text = tunable.Member.MetadataName;
            this.Validate();

            this.UpdateIcon();
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether or not the value for this tunable can be inherited.
        /// </summary>
        public bool CanInheritValue
        {
            get { return this.Model.CanInheritValue; }
        }

        /// <summary>
        /// Gets the bitmap source that is used to display the icon for this item when it has
        /// been expanded.
        /// </summary>
        public virtual BitmapSource ExpandedIcon
        {
            get { return this.Icon; }
        }

        /// <summary>
        /// Gets a value indicating whether this tunable can be expanded.
        /// </summary>
        public virtual bool IsExpandable
        {
            get { return false; }
            set { }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this tunable is currently locked from being
        /// edited.
        /// </summary>
        public bool IsLocked
        {
            get { return this.Model.IsLocked; }
            set { this.Model.IsLocked = value; }
        }

        /// <summary>
        /// Gets a value indicating whether this tunable currently has it's default value. This
        /// is the equivalent of having no source data.
        /// </summary>
        public bool HasDefaultValue
        {
            get { return this.Model.HasDefaultValue; }
        }

        /// <summary>
        /// Gets or sets the map key view model object for this tunable.
        /// </summary>
        public MapKeyViewModel MapKey
        {
            get { return this._mapKey; }
            set { this.SetProperty(ref this._mapKey, value); }
        }

        /// <summary>
        /// Gets the bitmap source that is used to display the overlay icon for this item.
        /// </summary>
        public virtual BitmapSource OverlayIcon
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the parent tunable view model for this view model.
        /// </summary>
        public ITunableViewModel Parent
        {
            get { return this._parent; }
        }

        /// <summary>
        /// Gets the parent tunable view model for this view model.
        /// </summary>
        public int Priority
        {
            get { return this.Model.Member.Priority; }
        }

        /// <summary>
        /// Gets the bitmap source that is used to display the state icon for this item.
        /// </summary>
        public virtual BitmapSource StateIcon
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the text that is used to represent the state of this item.
        /// </summary>
        public virtual string StateToolTipText
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the tunable model this view model is wrapping.
        /// </summary>
        public ITunable Tunable
        {
            get { return this.Model as ITunable; }
        }

        /// <summary>
        /// Gets the value of the type for this tunable view model that should be shown to the
        /// user.
        /// </summary>
        public virtual string TypeValue
        {
            get { return this.Model.Member.TypeName; }
        }

        /// <summary>
        /// Gets the tunable at the specified item from within this tunables item collection if
        /// applicable.
        /// </summary>
        /// <param name="index">
        /// The zero-based index of the tunable to get.
        /// </param>
        /// <returns>
        /// The tunable at the specified index if applicable; otherwise, null.
        /// </returns>
        public virtual ITunableViewModel this[int index]
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the tunable view model that belongs to this tunable that is currently wrapping
        /// the specified model.
        /// </summary>
        /// <param name="model">
        /// The model of the tunable view model to get.
        /// </param>
        /// <returns>
        /// The tunable view model that is wrapping the specified model if applicable;
        /// otherwise, null.
        /// </returns>
        public virtual ITunableViewModel this[ITunable model]
        {
            get { return null; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Makes sure that the icon gets updated when the locked state changes.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.ComponentModel.PropertyChangedEventArgs containing the event data.
        /// </param>
        protected override void OnModelPropertyChanged(object s, PropertyChangedEventArgs e)
        {
            base.OnModelPropertyChanged(s, e);
            if (e.PropertyName == "IsLocked")
            {
                this.UpdateIcon();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            ITunableViewModel item = obj as ITunableViewModel;
            if (item != null)
            {
                int index = ((ITunableParent)this.Tunable.Parent).Tunables.IndexOf(this.Tunable);
                int index1 = ((ITunableParent)item.Tunable.Parent).Tunables.IndexOf(item.Tunable);

                return System.Collections.Comparer.Default.Compare(index, index1);
            }

            return 0;
        }

        /// <summary>
        /// Resets this tunable to have its default value. (i.e no source data).
        /// </summary>
        public virtual void ResetValueToDefault()
        {
            ITunable tunableModel = this.Model as ITunable;
            if (tunableModel != null)
            {
                tunableModel.ResetValueToDefault();
            }
        }

        /// <summary>
        /// Determines whether the validation pipeline should be started due to the property
        /// with the specified name changing.
        /// </summary>
        /// <param name="propertyName">
        /// The property name to test.
        /// </param>
        /// <returns>
        /// True if the validation should be run due to the specified property changing;
        /// otherwise, false.
        /// </returns>
        protected override bool ShouldRunValidation(string propertyName)
        {
            if (String.Equals(propertyName, "Value"))
            {
                return true;
            }

            return base.ShouldRunValidation(propertyName);
        }

        /// <summary>
        /// Updates the icon based on the locked state of the model.
        /// </summary>
        private void UpdateIcon()
        {
            if (this.Model.IsLocked)
            {
                this.Icon = CommonIcons.Lock;
            }
            else
            {
                this.Icon = null;
            }
        }
        #endregion Methods
    } // RSG.Metadata.ViewModel.Tunables.TunableViewModelBase{T} {Class}
} // RSG.Metadata.ViewModel.Tunables {Namespace}
