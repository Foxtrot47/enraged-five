﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsDocumentItem.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock.Document
{
    using System;
    using System.ComponentModel;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using RSG.Editor.Controls.AttachedDependencyProperties;
    using RSG.Editor.Controls.Helpers;

    /// <summary>
    /// Represents a selectable item inside the <see cref="RsDocumentPane"/> control.
    /// </summary>
    public class RsDocumentItem : TabItem
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="IsActive"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsActiveProperty;

        /// <summary>
        /// Identifies the <see cref="IsModified"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsModifiedProperty;

        /// <summary>
        /// Identifies the <see cref="IsReadOnly"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsReadOnlyProperty;

        /// <summary>
        /// A private value indicating whether the items position has changed during the
        /// current dragging operation.
        /// </summary>
        private bool _changedPositionWhileDragging;

        /// <summary>
        /// The drag boundary for this item. Once the mouse goes outside this element a drag
        /// operation is initialised.
        /// </summary>
        private FrameworkElement _dragBoundary;

        /// <summary>
        /// The private reference to the framework element defined in the control template
        /// that is acting as the drag handle for this item.
        /// </summary>
        private FrameworkElement _dragHandle;

        /// <summary>
        /// A private value indicating whether the current dragging is going in a forwards
        /// direction or a backwards direction.
        /// </summary>
        private bool _previouslyDraggingForward;

        /// <summary>
        /// A private value indicating whether the mouse is inside the drag boundary during the
        /// last mouse move event.
        /// </summary>
        private bool _previouslyDraggingWithin;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsDocumentItem"/> class.
        /// </summary>
        static RsDocumentItem()
        {
            IsActiveProperty =
                DependencyProperty.Register(
                "IsActive",
                typeof(bool),
                typeof(RsDocumentItem),
                new PropertyMetadata(true));

            IsModifiedProperty =
                DependencyProperty.Register(
                "IsModified",
                typeof(bool),
                typeof(RsDocumentItem),
                new PropertyMetadata(false));

            IsReadOnlyProperty =
                DependencyProperty.Register(
                "IsReadOnly",
                typeof(bool),
                typeof(RsDocumentItem),
                new PropertyMetadata(false));

            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsDocumentItem),
                new FrameworkPropertyMetadata(typeof(RsDocumentItem)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsDocumentItem"/> class.
        /// </summary>
        public RsDocumentItem()
        {
        }
        #endregion

        #region Events
        /// <summary>
        /// This event is raised when the item is closed.
        /// </summary>
        public event EventHandler Closed;

        /// <summary>
        /// This event is raised before the item is closed.
        /// </summary>
        public event CancelEventHandler Closing;

        /// <summary>
        /// This event is raised after the content for this tab has been changed.
        /// </summary>
        public event EventHandler<SpinnerValueChangedEventArgs<object>> ContentChanged;
        #endregion Events

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether or not this control is currently the
        /// active content in the parent DockingManager.
        /// </summary>
        public bool IsActive
        {
            get { return (bool)this.GetValue(IsActiveProperty); }
            set { this.SetValue(IsActiveProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not this control should show its dirty
        /// indicator.
        /// </summary>
        public bool IsModified
        {
            get { return (bool)this.GetValue(IsModifiedProperty); }
            set { this.SetValue(IsModifiedProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not this control is currently in a read
        /// only state.
        /// </summary>
        public bool IsReadOnly
        {
            get { return (bool)this.GetValue(IsReadOnlyProperty); }
            set { this.SetValue(IsReadOnlyProperty, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Gets called whenever the internal processes set the control template for
        /// this control.
        /// </summary>
        public override void OnApplyTemplate()
        {
            if (this._dragHandle != null)
            {
                this._dragHandle.MouseLeftButtonDown -= this.DragLeftButtonDown;
                this._dragHandle.MouseLeftButtonUp -= this.DragLeftButtonUp;
            }

            base.OnApplyTemplate();
            this._dragHandle = this.GetTemplateChild("PART_DragHandle") as FrameworkElement;
            if (this._dragHandle != null)
            {
                this._dragHandle.MouseLeftButtonDown += this.DragLeftButtonDown;
                this._dragHandle.MouseLeftButtonUp += this.DragLeftButtonUp;
            }

            this._dragBoundary = this.GetVisualDescendent<FrameworkElement>();
        }

        /// <summary>
        /// Call to fire this items <see cref="Closed"/> event.
        /// </summary>
        internal void FireClosedEvent()
        {
            EventHandler handler = this.Closed;
            if (handler == null)
            {
                return;
            }

            handler(this, EventArgs.Empty);
        }

        /// <summary>
        /// Call to fire this items <see cref="Closing"/> event with the specified arguments.
        /// </summary>
        /// <param name="e">
        /// The arguments that should be fired with the event.
        /// </param>
        internal void FireClosingEvent(CancelEventArgs e)
        {
            CancelEventHandler handler = this.Closing;
            if (handler == null)
            {
                return;
            }

            handler(this, e);
        }

        /// <summary>
        /// Called when the content property changes so that the content changed event can be
        /// raised.
        /// </summary>
        /// <param name="oldContent">
        /// The old value of the content property.
        /// </param>
        /// <param name="newContent">
        /// The new value of the content property.
        /// </param>
        protected override void OnContentChanged(object oldContent, object newContent)
        {
            base.OnContentChanged(oldContent, newContent);
            EventHandler<SpinnerValueChangedEventArgs<object>> handler = this.ContentChanged;
            if (handler == null)
            {
                return;
            }

            handler(this, new SpinnerValueChangedEventArgs<object>(oldContent, newContent));
        }

        /// <summary>
        /// Makes sure the item is selected before the context menu is open.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Controls.ContextMenuEventArgs containing the event data.
        /// </param>
        protected override void OnContextMenuOpening(ContextMenuEventArgs e)
        {
            this.IsSelected = true;
            base.OnContextMenuOpening(e);
        }

        /// <summary>
        /// Called whenever the mouse button is pressed so that the middle button can be setup
        /// to close this item.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.MouseButtonEventArgs containing the event data.
        /// </param>
        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            if (e.ChangedButton != MouseButton.Middle)
            {
                return;
            }

            RoutedCommand command = DockingCommands.CloseItem;
            if (command.CanExecute(null, this))
            {
                command.Execute(null, this);
            }
        }

        /// <summary>
        /// Selects this tab item. We have to do it here as we have set focusable to false so
        /// the base class will not do it for us.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Input.MouseButtonEventArgs containing the event data.
        /// </param>
		protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);
            this.IsSelected = false;
            this.IsSelected = true;
        }

        /// <summary>
        /// Called when the System.Windows.Controls.TabItem.IsSelected property
        /// changes to true.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs data for the event.
        /// </param>
        protected override void OnSelected(RoutedEventArgs e)
        {
            ItemsControl parent = ItemsControl.ItemsControlFromItemContainer(this);
            RsDocumentPane pane = parent as RsDocumentPane;
            if (pane != null)
            {
                pane.SelectedContainer = this;
            }

            base.OnSelected(e);
            this.TryToFocusContent(true);
        }

        /// <summary>
        /// Gets called when the left mouse button is pressed while the drag handle is
        /// capturing the mouse device.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.MouseButtonEventArgs data for this event.
        /// </param>
        private void DragLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (this._dragHandle == null)
            {
                (sender as UIElement).MouseLeftButtonDown -= this.DragLeftButtonDown;
                return;
            }

            e.MouseDevice.Capture(this._dragHandle);
            this._dragHandle.MouseMove += this.DragMouseMove;
        }

        /// <summary>
        /// Gets called when the left mouse button is un-pressed while the drag handle is
        /// capturing the mouse device.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.MouseButtonEventArgs data for this event.
        /// </param>
        private void DragLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            e.MouseDevice.Capture(null);
            this._changedPositionWhileDragging = false;
            if (this._dragHandle == null)
            {
                (sender as UIElement).MouseLeftButtonUp -= this.DragLeftButtonUp;
                return;
            }

            this._dragHandle.MouseMove -= this.DragMouseMove;
        }

        /// <summary>
        /// Gets called when mouse is moved while the drag handler has captured the
        /// mouse device.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.MouseEventArgs data for this event.
        /// </param>
        private void DragMouseMove(object sender, MouseEventArgs e)
        {
            if (this._dragBoundary == null)
            {
                (sender as UIElement).MouseMove -= this.DragMouseMove;
                return;
            }

            Point pos = e.MouseDevice.GetPosition(this._dragBoundary);
            if (pos.X < 0.0 && this._previouslyDraggingWithin)
            {
                ItemsControl parent = ItemsControl.ItemsControlFromItemContainer(this);
                RsDocumentPane pane = parent as RsDocumentPane;
                RsDocumentTabPanel panel = pane == null ? null : pane.DocumentPanel;
                if (pane == null || panel == null)
                {
                    return;
                }

                int index = parent.ItemContainerGenerator.IndexFromContainer(this);
                if (index == panel.FirstVisibleTab)
                {
                    ////if (DockingCommands.FloatView.CanExecute(item, this))
                    ////{
                    ////    e.MouseDevice.Capture(null);
                    ////    Point location = NativeMethods.GetCursorPos();
                    ////   item.Site.UndockingInformation = new UndockingInformation(location);
                    ////    this._dragHandle.MouseMove -= this.DragMouseMove;
                    ////    DockingCommands.FloatView.Execute(item, this);
                    ////    this._changedPositionWhileDragging = false;
                    ////}
                }
                else
                {
                    if (DockingCommands.MoveItemLeft.CanExecute(null, this))
                    {
                        DockingCommands.MoveItemLeft.Execute(null, this);
                        this._changedPositionWhileDragging = true;
                    }
                }

                this._previouslyDraggingForward = false;
                this._previouslyDraggingWithin = false;
                return;
            }
            else if (pos.X > this._dragBoundary.ActualWidth && this._previouslyDraggingWithin)
            {
                ItemsControl parent = ItemsControl.ItemsControlFromItemContainer(this);
                RsDocumentPane pane = parent as RsDocumentPane;
                RsDocumentTabPanel panel = pane == null ? null : pane.DocumentPanel;
                if (pane == null || panel == null)
                {
                    return;
                }

                int index = parent.ItemContainerGenerator.IndexFromContainer(this);
                if (index + 1 == panel.FirstVisibleTab + panel.VisibleTabs)
                {
                    ////if (DockingCommands.FloatView.CanExecute(item, this))
                    ////{
                    ////    e.MouseDevice.Capture(null);
                    ////    Point location = NativeMethods.GetCursorPos();
                    ////   item.Site.UndockingInformation = new UndockingInformation(location);
                    ////    this._dragHandle.MouseMove -= this.DragMouseMove;
                    ////    DockingCommands.FloatView.Execute(item, this);
                    ////    this._changedPositionWhileDragging = false;
                    ////}
                }
                else
                {
                    if (DockingCommands.MoveItemRight.CanExecute(null, this))
                    {
                        DockingCommands.MoveItemRight.Execute(null, this);
                        this._changedPositionWhileDragging = true;
                    }
                }

                this._previouslyDraggingForward = true;
                this._previouslyDraggingWithin = false;
                return;
            }

            if (pos.X >= 0.0 && pos.X <= this._dragBoundary.ActualWidth)
            {
                this._previouslyDraggingWithin = true;
            }
            else
            {
                this._previouslyDraggingWithin = false;
            }

            if (!this._previouslyDraggingWithin)
            {
                if (this._changedPositionWhileDragging)
                {
                    bool forwards = pos.X > this._dragBoundary.ActualWidth;
                    if (forwards && this._previouslyDraggingForward)
                    {
                        this._previouslyDraggingWithin = true;
                    }
                    else if (!forwards && !this._previouslyDraggingForward)
                    {
                        this._previouslyDraggingWithin = true;
                    }
                }
            }

            if (this._changedPositionWhileDragging == false)
            {
                if (pos.Y < 0.0 || pos.Y > this._dragBoundary.ActualHeight)
                {
                    ////DockingItem item = this.DataContext as DockingItem;
                    ////if (item == null)
                    ////{
                    ////    return;
                    ////}

                    ////if (DockingCommands.FloatView.CanExecute(item, this))
                    ////{
                    ////    e.MouseDevice.Capture(null);
                    ////    Point location = NativeMethods.GetCursorPos();
                    ////    tem.Site.UndockingInformation = new UndockingInformation(location);
                    ////    this._dragHandle.MouseMove -= this.DragMouseMove;
                    ////    DockingCommands.FloatView.Execute(item, this);
                    ////    this._previouslyDraggingWithin = false;
                    ////}
                }
            }
            else
            {
                ////if (pos.Y < -75.0 || pos.Y > this._dragBoundary.ActualHeight + 75.0)
                ////{
                ////    DockingItem item = this.DataContext as DockingItem;
                ////    if (item == null)
                ////    {
                ////        return;
                ////    }

                ////    if (DockingCommands.FloatView.CanExecute(item, this))
                ////    {
                ////        e.MouseDevice.Capture(null);
                ////        Point location = NativeMethods.GetCursorPos();
                ////       item.Site.UndockingInformation = new UndockingInformation(location);
                ////        this._dragHandle.MouseMove -= this.DragMouseMove;
                ////        DockingCommands.FloatView.Execute(item, this);
                ////        this._previouslyDraggingWithin = false;
                ////    }
                ////}
            }
        }

        /// <summary>
        /// Tries to set focus inside the focus scope that the content for this document item
        /// contains.
        /// </summary>
        /// <param name="delay">
        /// A value indicating whether the operation should be started as a background action.
        /// </param>
        private void TryToFocusContent(bool delay)
        {
            FrameworkElement element = this.Content as FrameworkElement;
            if (element == null)
            {
                return;
            }

            EnhancedFocusScope.SetFocusOnActiveElementInScope(element, delay);
        }
        #endregion
    } // RSG.Editor.Controls.Dock.Document.RsDocumentItem {Class}
} // RSG.Editor.Controls.Dock.Document {Namespace}
