﻿//---------------------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel
{
    using System.Collections.Generic;
    using RSG.Editor.View;
    using RSG.Project.Model;

    /// <summary>
    /// 
    /// </summary>
    public interface IHierarchyNode : IViewModel
    {
        #region Properties
        /// <summary>
        /// Gets a value indicating whether this hierarchy node can be removed from the parent
        /// items scope it belongs to.
        /// </summary>
        bool CanBeRemoved { get; }

        /// <summary>
        /// Gets a value indicating whether this hierarchy nodes data can be saved.
        /// </summary>
        bool CanBeSaved { get; }

        /// <summary>
        /// Gets a value indicating whether a new or existing collection folder can be added to
        /// this hierarchy node.
        /// </summary>
        bool CanHaveCollectionFoldersAdded { get; }

        /// <summary>
        /// Gets a value indicating whether a new or existing items can be added to this
        /// hierarchy node.
        /// </summary>
        bool CanHaveItemsAdded { get; }

        /// <summary>
        /// Gets a value indicating whether a new or existing project folder can be added to
        /// this hierarchy node.
        /// </summary>
        bool CanHaveProjectFoldersAdded { get; }

        /// <summary>
        /// Gets a value indicating whether a new or existing project can be added to this
        /// hierarchy node.
        /// </summary>
        bool CanHaveProjectsAdded { get; }

        /// <summary>
        /// Gets the friendly save path that this item pushes into the command system to
        /// display on menu items.
        /// </summary>
        string FriendlySavePath { get; }

        /// <summary>
        /// Gets the include string for the project item this hierarchy node 
        /// </summary>
        string Include { get; }

        /// <summary>
        /// Gets or sets a value indicating whether this node can be expanded.
        /// </summary>
        bool IsExpandable { get; set; }

        /// <summary>
        /// Gets a value indicating whether this node currently contains modified (unsaved)
        /// data.
        /// </summary>
        bool IsModified { get; set; }

        /// <summary>
        /// Gets a value indicating whether this node is representing a virtual project item or
        /// a project item that is located on the users disk drive.
        /// </summary>
        bool IsVirtual { get; }

        /// <summary>
        /// Gets the parent node who is currently representing this nodes scope item.
        /// </summary>
        IHierarchyNode ModifiedScopeNode { get; }

        /// <summary>
        /// Gets the parent node for this node.
        /// </summary>
        IHierarchyNode Parent { get; }

        /// <summary>
        /// Gets the parent collection node for this node.
        /// </summary>
        ProjectCollectionNode ParentCollection { get; }

        /// <summary>
        /// Gets the first project node above this node if found; otherwise, null.
        /// </summary>
        ProjectNode ParentProject { get; }

        /// <summary>
        /// Gets the project item that this hierarachy node is representing.
        /// </summary>
        ProjectItem ProjectItem { get; }

        /// <summary>
        /// Gets the project scope for the item that this node is representing.
        /// </summary>
        IProjectItemScope ProjectItemScope { get; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Adds the specified item to the end of this nodes child collection.
        /// </summary>
        /// <param name="item">
        /// The item to add.
        /// </param>
        /// <returns>
        /// True if the item has been successfully added to the collection; otherwise, false.
        /// </returns>
        bool AddChild(IHierarchyNode item);

        /// <summary>
        /// Adds a new folder underneath this node and returns the instance to the new folder.
        /// </summary>
        /// <returns>
        /// The new folder that was created.
        /// </returns>
        IHierarchyNode AddNewFolder();

        /// <summary>
        /// Clears the item collection for this node.
        /// </summary>
        void ClearChildren();

        /// <summary>
        /// Attempts to retrieve the hierarchy node underneath this one that has the specified
        /// full path.
        /// </summary>
        /// <param name="fullPath">
        /// The full path of the item to try and find.
        /// </param>
        /// <returns>
        /// The hierarchy node underneath this node that has the specified full path if found;
        /// otherwise, false.
        /// </returns>
        IHierarchyNode FindNodeWithFullPath(string fullPath);

        /// <summary>
        /// Retrieves a iterator around the children underneath this node.
        /// </summary>
        /// <param name="recursive">
        /// A value indicating whether this method retrieves just the children of this node or
        /// recurses into them as well.
        /// </param>
        /// <returns>
        /// A iterator around the children underneath this node.
        /// </returns>
        IEnumerable<IHierarchyNode> GetChildren(bool recursive);

        /// <summary>
        /// Retrieves a iterator around the children underneath this node that share the same
        /// scope as this node.
        /// </summary>
        /// <param name="recursive">
        /// A value indicating whether this method retrieves just the children of this node or
        /// recurses into them as well.
        /// </param>
        /// <returns>
        /// A iterator around the children underneath this node that share the same scope.
        /// </returns>
        IEnumerable<IHierarchyNode> GetChildrenInSameScope(bool recursive);
        /// <summary>
        /// Removes the first occurrence of the specified item from this nodes child
        /// collection.
        /// </summary>
        /// <param name="item">
        /// The item to remove.
        /// </param>
        /// <returns>
        /// True if the item was present and has been successfully removed; otherwise, false.
        /// </returns>
        bool RemoveChild(IHierarchyNode item);
        #endregion Methods
    } // RSG.Project.ViewModel.IHierarchyNode {Interface}
} // RSG.Project.ViewModel {Namespace}
