﻿//---------------------------------------------------------------------------------------------
// <copyright file="ApplicationTheme.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using RSG.Base.Attributes;
    using RSG.Editor.Controls.Themes;

    /// <summary>
    /// Defines the different themes that can be set onto a <see cref="RsApplication"/>.
    /// </summary>
    public enum ApplicationTheme
    {
        /// <summary>
        /// The light theme.
        /// </summary>
        [FieldDisplayName("Light")]
        [ThemeRelativePath("Themes/theme.light.xaml")]
        Light,

        /// <summary>
        /// A light theme that doesn't have as harsh a contrast.
        /// </summary>
        [FieldDisplayName("Light Alternative")]
        [ThemeRelativePath("Themes/theme.lightalternative.xaml")]
        LightAlternative,

        /// <summary>
        /// The dark theme.
        /// </summary>
        [FieldDisplayName("Dark")]
        [ThemeRelativePath("Themes/theme.dark.xaml")]
        Dark,

        /// <summary>
        /// A dark theme that is closer to the 3ds max dark theme.
        /// </summary>
        [FieldDisplayName("Dark Alternative")]
        [ThemeRelativePath("Themes/theme.darkalternative.xaml")]
        DarkAlternative,
    } // RSG.Editor.Controls {Enum}
} // RSG.Editor.Controls {Namespace}
