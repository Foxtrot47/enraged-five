﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Automation.ViewModel;
using RSG.Base.Extensions;
using RSG.Editor;
using RSG.Pipeline.Automation.Common.Jobs;

namespace RSG.Automation.View.Commands
{
    public class FilterByJobStateAction : FilterAction<JobDataGridControl, JobState>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="FilterByJobStateAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public FilterByJobStateAction(ParameterResolverDelegate<JobDataGridControl> resolver)
            : base(resolver)
        {
        }
        #endregion // Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="control">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="secondaryParameter">
        /// The secondary command parameter that has been sent with the command.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(JobDataGridControl control, MultiCommandParameter<JobState> secondaryParameter)
        {
            if (control == null)
            {
                throw new SmartArgumentNullException(() => control);
            }
            AutomationMonitorDataContext dc = GetDataContext(control);

            // Are there any jobs with this state?
            JobState state = secondaryParameter.ItemParameter;
            return dc.ContainsItemsWithJobState(state);
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="control">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="secondaryParameter">
        /// The secondary command parameter that has been sent with the command.
        /// </param>
        public override void Execute(JobDataGridControl control, MultiCommandParameter<JobState> secondaryParameter)
        {
            if (control == null)
            {
                throw new SmartArgumentNullException(() => control);
            }
            
            // Either add or remove a filter.
            if (secondaryParameter.IsToggled)
            {
                control.AddAllowedFilterLevel(secondaryParameter.ItemParameter);
            }
            else
            {
                control.RemoveAllowedFilterLevel(secondaryParameter.ItemParameter);
            }            
        }

        /// <summary>
        /// Provide the specified definition with the items to show in the filter.
        /// </summary>
        /// <param name="definition">
        /// The definition that the created items will be used for.
        /// </param>
        /// <returns>
        /// The items to show in the filter.
        /// </returns>
        protected override ObservableCollection<IMultiCommandItem> GetItems(
            MultiCommand definition)
        {
            ObservableCollection<IMultiCommandItem> items = new ObservableCollection<IMultiCommandItem>();

            // Add a filter item for all the browsable job states.
            foreach (JobState state in Enum.GetValues(typeof(JobState)))
            {
                BrowsableAttribute att = state.GetAttributeOfType<BrowsableAttribute>();
                if (att == null || att.Browsable == true)
                {
                    items.Add(new FilterByJobStateItem(definition, state));
                }
            }
            return items;
        }

        /// <summary>
        /// 
        /// </summary>
        public void ReInitialise(JobDataGridControl control, RockstarRoutedCommand command)
        {
            foreach (FilterByJobStateItem item in (this[command] as FilterCommand).Items)
            {
                if (!item.IsToggled)
                {
                    this.Execute(control, item.GetCommandParameter() as MultiCommandParameter<JobState>);
                }
            }
        }
        #endregion // Methods

        #region Private Methods
        /// <summary>
        /// Gets the data context associated with the control.
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        private AutomationMonitorDataContext GetDataContext(JobDataGridControl control)
        {
            AutomationMonitorDataContext dc = null;
            if (!control.Dispatcher.CheckAccess())
            {
                dc = control.Dispatcher.Invoke<AutomationMonitorDataContext>(() => control.DataContext as AutomationMonitorDataContext);
            }
            else
            {
                dc = control.DataContext as AutomationMonitorDataContext;
            }

            if (dc == null)
            {
                Debug.Fail("Data context isn't valid.");
                throw new ArgumentException("Data context isn't valid.");
            }
            return dc;
        }
        #endregion // Private Methods

        #region Classes
        /// <summary>
        /// Defines the item published for the items of the
        /// <see cref="FilterByJobStateItem"/> command definition.
        /// </summary>
        private class FilterByJobStateItem : MultiCommandItem<JobState>
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="FilterByJobStateItem"/> class.
            /// </summary>
            /// <param name="definition">
            /// The multi command definition that owns this item.
            /// </param>
            /// <param name="state">
            /// The parameter that is sent when this item is executed.
            /// </param>
            public FilterByJobStateItem(MultiCommand definition, JobState state)
                : base(definition, state.ToString(), state)
            {
                // Get the name to display for this JobState level.
                DisplayNameAttribute nameAtt = state.GetAttributeOfType<DisplayNameAttribute>();
                Debug.Assert(nameAtt != null, "JobState is missing the DisplayNameAttribute.");
                if (nameAtt != null && nameAtt.DisplayName != null)
                {
                    Text = nameAtt.DisplayName;
                }

                m_description = Text;
                this.IsToggled = true;
            }
            #endregion // Constructors

            #region Properties
            /// <summary>
            /// Gets the description for this item. This is also what is shown as a tooltip
            /// along with the key gesture if one is set if it is being rendered as a
            /// toggle control.
            /// </summary>
            public override String Description
            {
                get { return this.m_description; }
            }
            private String m_description;
            #endregion Properties
        } // FilterByJobStateItem
        #endregion // Classes
    } // FilterByJobStateAction
}