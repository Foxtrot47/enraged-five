﻿//---------------------------------------------------------------------------------------------
// <copyright file="CommandData.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System.Windows;
    using System.Windows.Input;

    /// <summary>
    /// Provides the base class for data that is sent to the execute or can execute command
    /// delegates.
    /// </summary>
    public abstract class CommandData
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Command"/> property.
        /// </summary>
        private ICommand _command;

        /// <summary>
        /// The private field used for the <see cref="OriginalSource"/> property.
        /// </summary>
        private object _originalSource;

        /// <summary>
        /// The private field used for the <see cref="Sender"/> property.
        /// </summary>
        private UIElement _sender;

        /// <summary>
        /// The private field used for the <see cref="Source"/> property.
        /// </summary>
        private object _source;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CommandData"/> class.
        /// </summary>
        /// <param name="command">
        /// The command associated with this data.
        /// </param>
        internal CommandData(ICommand command)
        {
            if (command == null)
            {
                throw new SmartArgumentNullException(() => command);
            }

            this._command = command;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the command associated with this data.
        /// </summary>
        public ICommand Command
        {
            get { return this._command; }
        }

        /// <summary>
        /// Gets the original reporting source as determined by pure hit testing.
        /// </summary>
        public object OriginalSource
        {
            get { return this._originalSource; }
            internal set { this._originalSource = value; }
        }

        /// <summary>
        /// Gets or sets the System.Windows.UIElement where the event handler is attached.
        /// </summary>
        public UIElement Sender
        {
            get { return this._sender; }
            set { this._sender = value; }
        }

        /// <summary>
        /// Gets a reference to the object that raised the routed command.
        /// </summary>
        public object Source
        {
            get { return this._source; }
            internal set { this._source = value; }
        }
        #endregion Properties
    } // RSG.Editor.CommandData {Class}
} // RSG.Editor {Namespace}
