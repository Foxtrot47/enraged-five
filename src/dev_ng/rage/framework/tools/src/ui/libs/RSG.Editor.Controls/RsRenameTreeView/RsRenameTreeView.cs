﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsRenameTreeView.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls
{
    using System.Diagnostics;
    using System.Windows;
    using RSG.Editor.Controls.Helpers;
    using RSG.Editor.View;

    /// <summary>
    /// Represents a virtualised tree view that supports its items being renamed inline with
    /// the tree view items.
    /// </summary>
    public class RsRenameTreeView : RsVirtualisedTreeView
    {
        #region Fields
        /// <summary>
        /// A reference to the tree view item that is currently being edited.
        /// </summary>
        private RsRenameTreeViewItem _editModeItem;

        /// <summary>
        /// A reference to the tree view item that is currently being edited.
        /// </summary>
        private IRenameController _renameController;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="RsRenameTreeView"/> class.
        /// </summary>
        public RsRenameTreeView()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether there is a tree view item that is currently being
        /// renamed by the user.
        /// </summary>
        internal bool IsInRenameMode
        {
            get { return this._editModeItem != null; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Begins renaming the item associated with the specified controller by pushing it
        /// into edit mode.
        /// </summary>
        /// <param name="controller">
        /// The rename controller that specifies the rename behaviour to use.
        /// </param>
        public void BeginRename(IRenameController controller)
        {
            if (this.IsInRenameMode)
            {
                Debug.Fail("Cannot begin renaming while already in rename mode.");
                return;
            }

            this.ScrollIntoView(controller.ContainerDataContext);
            RsRenameTreeViewItem container =
                this.ItemContainerGenerator.ContainerFromItem(controller.ContainerDataContext)
                as RsRenameTreeViewItem;

            controller.Completed += this.OnRenameControllerCompleted;
            this.EnterEditMode(container, controller);
        }

        /// <summary>
        /// Commits the current rename operation for validation.
        /// </summary>
        /// <param name="refocus">
        /// A value indicating whether the tree view item should be refocused on successfully
        /// renaming the item.
        /// </param>
        public void CommitRename(bool refocus)
        {
            if (this.IsInRenameMode && this._renameController != null)
            {
                this._renameController.Commit(refocus);
            }
        }

        /// <summary>
        /// Creates or identifies the element used to display the child items.
        /// </summary>
        /// <returns>
        /// A new <see cref="RsRenameTreeViewItem"/>.
        /// </returns>
        protected override DependencyObject GetContainerForItemOverride()
        {
            return new RsRenameTreeViewItem();
        }

        /// <summary>
        /// Determines whether the specified item is, or is eligible to be, its own item
        /// container.
        /// </summary>
        /// <param name="item">
        /// The item to check whether it is an item container.
        /// </param>
        /// <returns>
        /// True if the item is eligible to be its own item container; otherwise, false.
        /// </returns>
        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is RsRenameTreeViewItem;
        }

        /// <summary>
        /// Puts the tree item presenter being used by the specified item into its edit mode.
        /// If the presenter isn't loaded yet it will pend the edit mode until it is loaded.
        /// </summary>
        /// <param name="item">
        /// The tree view item that should be placed into its edit mode.
        /// </param>
        /// <param name="controller">
        /// The rename controller that specifies the rename behaviour to use.
        /// </param>
        private void EnterEditMode(RsRenameTreeViewItem item, IRenameController controller)
        {
            this._editModeItem = item;
            this._renameController = controller;
            item.EnterEditMode(controller);
        }

        /// <summary>
        /// Removes the tree item presenter being used by the specified item from its edit
        /// mode.
        /// </summary>
        private void ExitEditMode()
        {
            if (this._editModeItem != null)
            {
                this._editModeItem.ExitEditMode();
                this._renameController = null;
                this._editModeItem = null;
            }
        }

        /// <summary>
        /// Called whenever the rename controller completes. This makes sure any refocusing or
        /// retries are handled correctly.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to. (The controller that completed).
        /// </param>
        /// <param name="e">
        /// The rename controller completed arguments containing the event data.
        /// </param>
        private void OnRenameControllerCompleted(
            object s, RenameControllerCompletedEventArgs e)
        {
            IRenameController controller = (IRenameController)s;
            VirtualisedTreeNode node = this.GetFirstTreeNode(controller.Item);
            if (e.Refocus && e.Result != RenameControllerCompletedResult.ValidationFailed)
            {
                this.FocusNode(node);
            }

            controller.Completed -= this.OnRenameControllerCompleted;
            this.ExitEditMode();
            if (e.Refocus && e.Result == RenameControllerCompletedResult.ValidationFailed)
            {
                RsRenameTreeViewItem container =
                    this.ItemContainerGenerator.ContainerFromItem(node)
                    as RsRenameTreeViewItem;

                controller.Completed += this.OnRenameControllerCompleted;
                this.EnterEditMode(container, controller);
            }
        }
        #endregion Methods
    } // RSG.Editor.Controls.RsRenameTreeView {Class}
} // RSG.Editor.Controls {Namespace}
