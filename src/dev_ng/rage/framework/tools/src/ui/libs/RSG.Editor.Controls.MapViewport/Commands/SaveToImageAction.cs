﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using RSG.Editor;

namespace RSG.Editor.Controls.MapViewport.Commands
{
    /// <summary>
    /// The action logic responsible for saving the contents of the map viewport out to
    /// a file on disk.
    /// </summary>
    public class SaveToImageAction : ButtonAction<MapViewport>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="SaveToImageAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public SaveToImageAction(ParameterResolverDelegate<MapViewport> resolver)
            : base(resolver)
        {
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="viewport">
        /// The location to warp to.
        /// </param>
        public override void Execute(MapViewport viewport)
        {
            ICommonDialogService dialogService = GetService<ICommonDialogService>();
            if (dialogService == null)
            {
                throw new ArgumentNullException("dialogService");
            }

            IMessageBoxService messageService = GetService<IMessageBoxService>();
            if (messageService == null)
            {
                throw new ArgumentNullException("messageService");
            }

            String filter = "PNG (*.png)|*.png";
            String destination;
            if (!dialogService.ShowSaveFile(null, null, filter, 0, out destination))
            {
                return;
            }

            try
            {
                // Keep track of the previous scale/offset so we can restore them after saving out the viewport to an image.
                Vector currentScale = viewport.ZoomCanvas.Scale;
                Point currentOffset = viewport.ZoomCanvas.Offset;

                // Move the zoom canvas so that the top left is the start of the image and we are rendering 1 pixel per meter.
                viewport.ZoomCanvas.Scale = new Vector(1.0, 1.0);
                viewport.ZoomCanvas.Offset = new Point(0.0, 0.0);

                // Force the canvas to rearrange itself prior to determining the bounds.
                viewport.ZoomCanvas.InvalidateArrange();
                viewport.ZoomCanvas.UpdateLayout();

                Rect canvasBounds = VisualTreeHelper.GetDescendantBounds(viewport.ZoomCanvas);

                // Update the offset and force another rearrange.
                viewport.ZoomCanvas.Offset = new Point(canvasBounds.Left, canvasBounds.Top);
                viewport.ZoomCanvas.InvalidateArrange();
                viewport.ZoomCanvas.UpdateLayout();

                // Create a render target and render the zoom canvas into it.
                RenderTargetBitmap renderTarget = new RenderTargetBitmap(
                    (int)canvasBounds.Width,
                    (int)canvasBounds.Height,
                    96d,
                    96d,
                    PixelFormats.Default);
                renderTarget.Render(viewport.ZoomCanvas);

                // Save the render target to a png file.
                PngBitmapEncoder png = new PngBitmapEncoder();
                png.Frames.Add(BitmapFrame.Create(renderTarget));

                using (Stream fileStream = File.Create(destination))
                {
                    png.Save(fileStream);
                    fileStream.Flush();
                }

                // Reset the viewport scale/offset.
                viewport.ZoomCanvas.Scale = currentScale;
                viewport.ZoomCanvas.Offset = currentOffset;
            }
            catch (System.Exception)
            {
                messageService.Show("A problem occurred while trying to save the map viewport to file.");
            }
        }
        #endregion
    }
}
