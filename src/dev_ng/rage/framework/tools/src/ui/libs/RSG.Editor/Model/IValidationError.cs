﻿//---------------------------------------------------------------------------------------------
// <copyright file="IValidationError.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Model
{
    /// <summary>
    /// When implemented represents a object that describes a error that has been found through
    /// validating a model object.
    /// </summary>
    public interface IValidationError : IValidationItem
    {
    } // RSG.Editor.Model.IValidationError {Interface}
} // RSG.Editor.Model {Namespace}
