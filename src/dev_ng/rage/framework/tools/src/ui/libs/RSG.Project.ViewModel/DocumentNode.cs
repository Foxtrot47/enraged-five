﻿//---------------------------------------------------------------------------------------------
// <copyright file="FolderNode.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel
{
    using System;
    using System.ComponentModel;
    using RSG.Editor.View;
    using RSG.Project.Model;
    using RSG.Project.ViewModel.Controllers;

    /// <summary>
    /// Represents an item in the project explorer tree view that isn't associated with a
    /// physical file on the users local machine but still creates a document when
    /// invoked/opened.
    /// </summary>
    public class DocumentNode : HierarchyNode, IInvokable, ICreatesDocumentContent
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DocumentNode"/> class.
        /// </summary>
        protected DocumentNode()
        {
            this.IsExpandable = false;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="DocumentNode"/> class.
        /// </summary>
        /// <param name="model">
        /// The model that this view model will be wrapping.
        /// </param>
        public DocumentNode(IHierarchyNode parent, ProjectItem model)
            : base(parent, model)
        {
            this.IsExpandable = false;
            this.Text = model.Include;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether this hierarchy node can be removed from the parent
        /// items scope it belongs to.
        /// </summary>
        public override bool CanBeRemoved
        {
            get { return true; }
        }

        /// <summary>
        /// Gets the creates document content controller that can be used to create a content
        /// object for this object inside a document tab.
        /// </summary>
        public virtual ICreateDocumentContentController CreateDocumentContentController
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Gets a value indicating whether this item is expanded when the user double clicks
        /// on it.
        /// </summary>
        public override bool ExpandOnDoubleClick
        {
            get { return false; }
        }

        /// <summary>
        /// Gets the invoke controller that can be used to perform a action on this object.
        /// </summary>
        public virtual IInvokeController InvokeController
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Gets the priority value for this object.
        /// </summary>
        public override int Priority
        {
            get { return int.MaxValue; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Changes the parent node for this node.
        /// </summary>
        /// <param name="newScope">
        /// The new parent node for this item.
        /// </param>
        public override void ChangeParent(IHierarchyNode parent)
        {
            base.ChangeParent(parent);
            if (parent is FolderNode)
            {
                this.Model.SetMetadata("Filter", parent.Include, MetadataLevel.Filter);
            }
            else
            {
                this.Model.RemoveMetadata("Filter", MetadataLevel.Filter);
            }

            this.ValidateAndEnsureFilter();
        }

        /// <summary>
        /// Called whenever a property inside the model changes.
        /// </summary>
        /// <param name="s">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The data for this event including the name of the property that changed.
        /// </param>
        protected override void OnModelPropertyChanged(object s, PropertyChangedEventArgs e)
        {
            if (String.Equals(e.PropertyName, "Include"))
            {
                this.Text = this.Model.Include;
            }
        }
        #endregion Methods
    } // RSG.Project.ViewModel.FileNode {Class}
} // RSG.Project.ViewModel {Namespace}
