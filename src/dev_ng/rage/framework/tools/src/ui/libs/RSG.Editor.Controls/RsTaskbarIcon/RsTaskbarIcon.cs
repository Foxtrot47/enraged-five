﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using RSG.Interop.Microsoft.Windows;
using System.ComponentModel;
using System.Threading;
using System.Windows.Controls.Primitives;
using System.Windows.Controls;
using SDraw = System.Drawing;
using System.Windows.Media;
using System.Windows.Input;
using System.Windows.Threading;
using System.Windows.Resources;
using System.Windows.Interop;
using System.Diagnostics;

namespace RSG.Editor.Controls
{
    /// <summary>
    /// A WPF proxy to for a taskbar icon (NotifyIcon) that sits in the system's
    /// taskbar notification area ("system tray").
    /// </summary>
    public class RsTaskbarIcon : FrameworkElement, IDisposable
    {
        #region Constants
        /// <summary>
        /// Category name that is set on designer properties.
        /// </summary>
        public const string CategoryName = "RsTaskbarIcon";
        #endregion

        #region Fields
        /// <summary>
        /// Represents the current icon data.
        /// </summary>
        private NOTIFYICONDATA _iconData;

        /// <summary>
        /// Receives messages from the taskbar icon.
        /// </summary>
        private readonly WindowMessageSink _messageSink;

        /// <summary>
        /// An action that is being invoked if the
        /// <see cref="_singleClickTimer"/> fires.
        /// </summary>
        private Action _singleClickTimerAction;

        /// <summary>
        /// A timer that is used to differentiate between single
        /// and double clicks.
        /// </summary>
        private readonly Timer _singleClickTimer;

        /// <summary>
        /// A timer that is used to close open balloon tooltips.
        /// </summary>
        private readonly Timer _balloonCloseTimer;

        /// <summary>
        /// Indicates whether the taskbar icon has been created or not.
        /// </summary>
        public bool IsTaskbarIconCreated { get; private set; }

        /// <summary>
        /// Indicates whether custom tooltips are supported, which depends
        /// on the OS. Windows Vista or higher is required in order to
        /// support this feature.
        /// </summary>
        public bool SupportsCustomToolTips
        {
            get { return _messageSink.Version == NotifyIconVersion.Vista; }
        }

        /// <summary>
        /// Checks whether a non-tooltip popup is currently opened.
        /// </summary>
        private bool IsPopupOpen
        {
            get
            {
                var popup = TrayPopupResolved;
                var menu = ContextMenu;
                var balloon = CustomBalloon;

                return popup != null && popup.IsOpen ||
                       menu != null && menu.IsOpen ||
                       balloon != null && balloon.IsOpen;
            }
        }

        /// <summary>
        /// Scaling factor to support non-standard DPIs.
        /// </summary>
        private double _scalingFactor = double.NaN;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Inits the taskbar icon and registers a message listener
        /// in order to receive events from the taskbar area.
        /// </summary>
        public RsTaskbarIcon()
        {
            //using dummy sink in design mode
            _messageSink = DesignerProperties.GetIsInDesignMode(this)
                ? WindowMessageSink.CreateEmpty()
                : new WindowMessageSink(NotifyIconVersion.Windows95);

            //init icon data structure
            _iconData = NOTIFYICONDATA.CreateDefault(_messageSink.MessageWindowHandle);
            _iconData.uCallbackMessage = (int)WindowMessage.USER;

            //create the taskbar icon
            CreateTaskbarIcon();

            //register event listeners
            _messageSink.MouseEventReceived += OnMouseEvent;
            _messageSink.TaskbarCreated += OnTaskbarCreated;
            _messageSink.ChangeToolTipStateRequest += OnToolTipChange;
            _messageSink.BalloonToolTipChanged += OnBalloonToolTipChanged;

            //init single click / balloon timers
            _singleClickTimer = new Timer(DoSingleClickAction);
            _balloonCloseTimer = new Timer(CloseBalloonCallback);

            //register listener in order to get notified when the application closes
            if (Application.Current != null)
            {
                Application.Current.Exit += OnExit;
            }
        }

        /// <summary>
        /// Registers properties.
        /// </summary>
        static RsTaskbarIcon()
        {
            //register change listener for the Visibility property
            VisibilityProperty.OverrideMetadata(
                typeof(RsTaskbarIcon),
                new PropertyMetadata(Visibility.Visible, VisibilityPropertyChanged));

            //register change listener for the DataContext property
            DataContextProperty.OverrideMetadata(
                typeof(RsTaskbarIcon),
                new FrameworkPropertyMetadata(new PropertyChangedCallback(DataContextPropertyChanged)));

            //register change listener for the ContextMenu property
            ContextMenuProperty.OverrideMetadata(
                typeof(RsTaskbarIcon),
                new FrameworkPropertyMetadata(new PropertyChangedCallback(ContextMenuPropertyChanged)));
        }
        #endregion

        #region Popup Controls
        #region TrayPopupResolved
        /// <summary>
        /// TrayPopupResolved Read-Only Dependency Property
        /// </summary>
        private static readonly DependencyPropertyKey TrayPopupResolvedPropertyKey =
            DependencyProperty.RegisterReadOnly("TrayPopupResolved", typeof(Popup), typeof(RsTaskbarIcon),
                new FrameworkPropertyMetadata(null));


        /// <summary>
        /// A read-only dependency property that returns the <see cref="Popup"/>
        /// that is being displayed in the taskbar area based on a user action.
        /// </summary>
        public static readonly DependencyProperty TrayPopupResolvedProperty =
            TrayPopupResolvedPropertyKey.DependencyProperty;

        /// <summary>
        /// Gets the TrayPopupResolved property. Returns
        /// a <see cref="Popup"/> which is either the
        /// <see cref="TrayPopup"/> control itself or a
        /// <see cref="Popup"/> control that contains the
        /// <see cref="TrayPopup"/>.
        /// </summary>
        [Category(CategoryName)]
        public Popup TrayPopupResolved
        {
            get { return (Popup)GetValue(TrayPopupResolvedProperty); }
        }

        /// <summary>
        /// Provides a secure method for setting the TrayPopupResolved property.  
        /// This dependency property indicates ....
        /// </summary>
        /// <param name="value">The new value for the property.</param>
        protected void SetTrayPopupResolved(Popup value)
        {
            SetValue(TrayPopupResolvedPropertyKey, value);
        }
        #endregion

        #region TrayToolTipResolved
        /// <summary>
        /// TrayToolTipResolved Read-Only Dependency Property
        /// </summary>
        private static readonly DependencyPropertyKey TrayToolTipResolvedPropertyKey =
            DependencyProperty.RegisterReadOnly("TrayToolTipResolved", typeof(ToolTip), typeof(RsTaskbarIcon),
                new FrameworkPropertyMetadata(null));


        /// <summary>
        /// A read-only dependency property that returns the <see cref="ToolTip"/>
        /// that is being displayed.
        /// </summary>
        public static readonly DependencyProperty TrayToolTipResolvedProperty =
            TrayToolTipResolvedPropertyKey.DependencyProperty;

        /// <summary>
        /// Gets the TrayToolTipResolved property. Returns 
        /// a <see cref="ToolTip"/> control that was created
        /// in order to display either <see cref="TrayToolTip"/>
        /// or <see cref="ToolTipText"/>.
        /// </summary>
        [Category(CategoryName)]
        [Browsable(true)]
        [Bindable(true)]
        public ToolTip TrayToolTipResolved
        {
            get { return (ToolTip)GetValue(TrayToolTipResolvedProperty); }
        }

        /// <summary>
        /// Provides a secure method for setting the <see cref="TrayToolTipResolved"/>
        /// property.  
        /// </summary>
        /// <param name="value">The new value for the property.</param>
        protected void SetTrayToolTipResolved(ToolTip value)
        {
            SetValue(TrayToolTipResolvedPropertyKey, value);
        }
        #endregion

        #region CustomBalloon
        /// <summary>
        /// CustomBalloon Read-Only Dependency Property
        /// </summary>
        private static readonly DependencyPropertyKey CustomBalloonPropertyKey =
            DependencyProperty.RegisterReadOnly("CustomBalloon", typeof(Popup), typeof(RsTaskbarIcon),
                new FrameworkPropertyMetadata(null));

        /// <summary>
        /// Maintains a currently displayed custom balloon.
        /// </summary>
        public static readonly DependencyProperty CustomBalloonProperty =
            CustomBalloonPropertyKey.DependencyProperty;

        /// <summary>
        /// A custom popup that is being displayed in the tray area in order
        /// to display messages to the user.
        /// </summary>
        public Popup CustomBalloon
        {
            get { return (Popup)GetValue(CustomBalloonProperty); }
        }

        /// <summary>
        /// Provides a secure method for setting the <see cref="CustomBalloon"/> property.  
        /// </summary>
        /// <param name="value">The new value for the property.</param>
        protected void SetCustomBalloon(Popup value)
        {
            SetValue(CustomBalloonPropertyKey, value);
        }
        #endregion
        #endregion

        #region Dependency Properties
        #region Icon property / IconSource dependency property
        /// <summary>
        /// Private field for the <see cref="Icon"/> property.
        /// </summary>
        private SDraw.Icon icon;

        /// <summary>
        /// Gets or sets the icon to be displayed. This is not a
        /// dependency property - if you want to assign the property
        /// through XAML, please use the <see cref="IconSource"/>
        /// dependency property.
        /// </summary>
        [Browsable(false)]
        public SDraw.Icon Icon
        {
            get { return icon; }
            set
            {
                icon = value;
                _iconData.Icon = value == null ? IntPtr.Zero : icon.Handle;

                RsTaskbarIconUtil.WriteIconData(ref _iconData, NotifyIconMessage.Modify, NotifyIconFlags.Icon);
            }
        }

        /// <summary>
        /// Resolves an image source and updates the <see cref="Icon" /> property accordingly.
        /// </summary>
        public static readonly DependencyProperty IconSourceProperty =
            DependencyProperty.Register("IconSource",
                typeof(ImageSource),
                typeof(RsTaskbarIcon),
                new FrameworkPropertyMetadata(null, IconSourcePropertyChanged));

        /// <summary>
        /// A property wrapper for the <see cref="IconSourceProperty"/>
        /// dependency property:<br/>
        /// Resolves an image source and updates the <see cref="Icon" /> property accordingly.
        /// </summary>
        [Category(CategoryName)]
        [Description("Sets the displayed taskbar icon.")]
        public ImageSource IconSource
        {
            get { return (ImageSource)GetValue(IconSourceProperty); }
            set { SetValue(IconSourceProperty, value); }
        }

        /// <summary>
        /// A static callback listener which is being invoked if the
        /// <see cref="IconSourceProperty"/> dependency property has
        /// been changed. Invokes the <see cref="OnIconSourcePropertyChanged"/>
        /// instance method of the changed instance.
        /// </summary>
        /// <param name="d">The currently processed owner of the property.</param>
        /// <param name="e">Provides information about the updated property.</param>
        private static void IconSourcePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RsTaskbarIcon owner = (RsTaskbarIcon)d;
            owner.OnIconSourcePropertyChanged(e);
        }

        /// <summary>
        /// Handles changes of the <see cref="IconSourceProperty"/> dependency property. As
        /// WPF internally uses the dependency property system and bypasses the
        /// <see cref="IconSource"/> property wrapper, updates of the property's value
        /// should be handled here.
        /// </summary>
        /// <param name="e">Provides information about the updated property.</param>
        private void OnIconSourcePropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            ImageSource newValue = (ImageSource)e.NewValue;

            //resolving the ImageSource at design time is unlikely to work
            if (!DesignerProperties.GetIsInDesignMode(this))
            {
                if (newValue == null)
                {
                    Icon = null;
                }
                else
                {
                    Uri uri = new Uri(newValue.ToString());
                    StreamResourceInfo streamInfo = Application.GetResourceStream(uri);

                    if (streamInfo == null)
                    {
                        string msg = "The supplied image source '{0}' could not be resolved.";
                        msg = String.Format(msg, newValue);
                        throw new ArgumentException(msg);
                    }

                    Icon = new SDraw.Icon(streamInfo.Stream);
                }
            }
        }
        #endregion

        #region ToolTipText dependency property
        /// <summary>
        /// A tooltip text that is being displayed if no custom <see cref="ToolTip"/>
        /// was set or if custom tooltips are not supported.
        /// </summary>
        public static readonly DependencyProperty ToolTipTextProperty =
            DependencyProperty.Register("ToolTipText",
                typeof(string),
                typeof(RsTaskbarIcon),
                new FrameworkPropertyMetadata(String.Empty, ToolTipTextPropertyChanged));

        /// <summary>
        /// A property wrapper for the <see cref="ToolTipTextProperty"/>
        /// dependency property:<br/>
        /// A tooltip text that is being displayed if no custom <see cref="ToolTip"/>
        /// was set or if custom tooltips are not supported.
        /// </summary>
        [Category(CategoryName)]
        [Description("Alternative to a fully blown ToolTip, which is only displayed on Vista and above.")]
        public string ToolTipText
        {
            get { return (string)GetValue(ToolTipTextProperty); }
            set { SetValue(ToolTipTextProperty, value); }
        }

        /// <summary>
        /// A static callback listener which is being invoked if the
        /// <see cref="ToolTipTextProperty"/> dependency property has
        /// been changed. Invokes the <see cref="OnToolTipTextPropertyChanged"/>
        /// instance method of the changed instance.
        /// </summary>
        /// <param name="d">The currently processed owner of the property.</param>
        /// <param name="e">Provides information about the updated property.</param>
        private static void ToolTipTextPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RsTaskbarIcon owner = (RsTaskbarIcon)d;
            owner.OnToolTipTextPropertyChanged(e);
        }

        /// <summary>
        /// Handles changes of the <see cref="ToolTipTextProperty"/> dependency property. As
        /// WPF internally uses the dependency property system and bypasses the
        /// <see cref="ToolTipText"/> property wrapper, updates of the property's value
        /// should be handled here.
        /// </summary>
        /// <param name="e">Provides information about the updated property.</param>
        private void OnToolTipTextPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            //do not touch tooltips if we have a custom tooltip element
            if (TrayToolTip == null)
            {
                ToolTip currentToolTip = TrayToolTipResolved;
                if (currentToolTip == null)
                {
                    //if we don't have a wrapper tooltip for the tooltip text, create it now
                    CreateCustomToolTip();
                }
                else
                {
                    //if we have a wrapper tooltip that shows the old tooltip text, just update content
                    currentToolTip.Content = e.NewValue;
                }
            }

            WriteToolTipSettings();
        }
        #endregion

        #region TrayToolTip dependency property
        /// <summary>
        /// A custom UI element that is displayed as a tooltip if the user hovers over the taskbar icon.
        /// Works only with Vista and above. Accordingly, you should make sure that
        /// the <see cref="ToolTipText"/> property is set as well.
        /// </summary>
        public static readonly DependencyProperty TrayToolTipProperty =
            DependencyProperty.Register("TrayToolTip",
                typeof(UIElement),
                typeof(RsTaskbarIcon),
                new FrameworkPropertyMetadata(null, TrayToolTipPropertyChanged));

        /// <summary>
        /// A property wrapper for the <see cref="TrayToolTipProperty"/>
        /// dependency property:<br/>
        /// A custom UI element that is displayed as a tooltip if the user hovers over the taskbar icon.
        /// Works only with Vista and above. Accordingly, you should make sure that
        /// the <see cref="ToolTipText"/> property is set as well.
        /// </summary>
        [Category(CategoryName)]
        [Description("Custom UI element that is displayed as a tooltip. Only on Vista and above")]
        public UIElement TrayToolTip
        {
            get { return (UIElement)GetValue(TrayToolTipProperty); }
            set { SetValue(TrayToolTipProperty, value); }
        }

        /// <summary>
        /// A static callback listener which is being invoked if the
        /// <see cref="TrayToolTipProperty"/> dependency property has
        /// been changed. Invokes the <see cref="OnTrayToolTipPropertyChanged"/>
        /// instance method of the changed instance.
        /// </summary>
        /// <param name="d">The currently processed owner of the property.</param>
        /// <param name="e">Provides information about the updated property.</param>
        private static void TrayToolTipPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RsTaskbarIcon owner = (RsTaskbarIcon)d;
            owner.OnTrayToolTipPropertyChanged(e);
        }

        /// <summary>
        /// Handles changes of the <see cref="TrayToolTipProperty"/> dependency property. As
        /// WPF internally uses the dependency property system and bypasses the
        /// <see cref="TrayToolTip"/> property wrapper, updates of the property's value
        /// should be handled here.
        /// </summary>
        /// <param name="e">Provides information about the updated property.</param>
        private void OnTrayToolTipPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            //recreate tooltip control
            CreateCustomToolTip();

            if (e.OldValue != null)
            {
                //remove the taskbar icon reference from the previously used element
                SetParentTaskbarIcon((DependencyObject)e.OldValue, null);
            }

            if (e.NewValue != null)
            {
                //set this taskbar icon as a reference to the new tooltip element
                SetParentTaskbarIcon((DependencyObject)e.NewValue, this);
            }

            //update tooltip settings - needed to make sure a string is set, even
            //if the ToolTipText property is not set. Otherwise, the event that
            //triggers tooltip display is never fired.
            WriteToolTipSettings();
        }
        #endregion

        #region TrayPopup dependency property
        /// <summary>
        /// A control that is displayed as a popup when the taskbar icon is clicked.
        /// </summary>
        public static readonly DependencyProperty TrayPopupProperty =
            DependencyProperty.Register("TrayPopup",
                typeof(UIElement),
                typeof(RsTaskbarIcon),
                new FrameworkPropertyMetadata(null, TrayPopupPropertyChanged));

        /// <summary>
        /// A property wrapper for the <see cref="TrayPopupProperty"/>
        /// dependency property:<br/>
        /// A control that is displayed as a popup when the taskbar icon is clicked.
        /// </summary>
        [Category(CategoryName)]
        [Description("Displayed as a Popup if the user clicks on the taskbar icon.")]
        public UIElement TrayPopup
        {
            get { return (UIElement)GetValue(TrayPopupProperty); }
            set { SetValue(TrayPopupProperty, value); }
        }

        /// <summary>
        /// A static callback listener which is being invoked if the
        /// <see cref="TrayPopupProperty"/> dependency property has
        /// been changed. Invokes the <see cref="OnTrayPopupPropertyChanged"/>
        /// instance method of the changed instance.
        /// </summary>
        /// <param name="d">The currently processed owner of the property.</param>
        /// <param name="e">Provides information about the updated property.</param>
        private static void TrayPopupPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RsTaskbarIcon owner = (RsTaskbarIcon)d;
            owner.OnTrayPopupPropertyChanged(e);
        }

        /// <summary>
        /// Handles changes of the <see cref="TrayPopupProperty"/> dependency property. As
        /// WPF internally uses the dependency property system and bypasses the
        /// <see cref="TrayPopup"/> property wrapper, updates of the property's value
        /// should be handled here.
        /// </summary>
        /// <param name="e">Provides information about the updated property.</param>
        private void OnTrayPopupPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if (e.OldValue != null)
            {
                //remove the taskbar icon reference from the previously used element
                SetParentTaskbarIcon((DependencyObject)e.OldValue, null);
            }


            if (e.NewValue != null)
            {
                //set this taskbar icon as a reference to the new tooltip element
                SetParentTaskbarIcon((DependencyObject)e.NewValue, this);
            }

            //create a pop
            CreatePopup();
        }
        #endregion

        #region MenuActivation dependency property
        /// <summary>
        /// Defines what mouse events display the context menu.
        /// Defaults to <see cref="PopupActivationMode.RightClick"/>.
        /// </summary>
        public static readonly DependencyProperty MenuActivationProperty =
            DependencyProperty.Register("MenuActivation",
                typeof(PopupActivationMode),
                typeof(RsTaskbarIcon),
                new FrameworkPropertyMetadata(PopupActivationMode.RightClick));

        /// <summary>
        /// A property wrapper for the <see cref="MenuActivationProperty"/>
        /// dependency property:<br/>
        /// Defines what mouse events display the context menu.
        /// Defaults to <see cref="PopupActivationMode.RightClick"/>.
        /// </summary>
        [Category(CategoryName)]
        [Description("Defines what mouse events display the context menu.")]
        public PopupActivationMode MenuActivation
        {
            get { return (PopupActivationMode)GetValue(MenuActivationProperty); }
            set { SetValue(MenuActivationProperty, value); }
        }
        #endregion

        #region PopupActivation dependency property
        /// <summary>
        /// Defines what mouse events trigger the <see cref="TrayPopup" />.
        /// Default is <see cref="PopupActivationMode.LeftClick" />.
        /// </summary>
        public static readonly DependencyProperty PopupActivationProperty =
            DependencyProperty.Register("PopupActivation",
                typeof(PopupActivationMode),
                typeof(RsTaskbarIcon),
                new FrameworkPropertyMetadata(PopupActivationMode.LeftClick));

        /// <summary>
        /// A property wrapper for the <see cref="PopupActivationProperty"/>
        /// dependency property:<br/>
        /// Defines what mouse events trigger the <see cref="TrayPopup" />.
        /// Default is <see cref="PopupActivationMode.LeftClick" />.
        /// </summary>
        [Category(CategoryName)]
        [Description("Defines what mouse events display the TaskbarIconPopup.")]
        public PopupActivationMode PopupActivation
        {
            get { return (PopupActivationMode)GetValue(PopupActivationProperty); }
            set { SetValue(PopupActivationProperty, value); }
        }
        #endregion

        #region Visibility dependency property override
        /// <summary>
        /// A static callback listener which is being invoked if the
        /// <see cref="UIElement.VisibilityProperty"/> dependency property has
        /// been changed. Invokes the <see cref="OnVisibilityPropertyChanged"/>
        /// instance method of the changed instance.
        /// </summary>
        /// <param name="d">The currently processed owner of the property.</param>
        /// <param name="e">Provides information about the updated property.</param>
        private static void VisibilityPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RsTaskbarIcon owner = (RsTaskbarIcon)d;
            owner.OnVisibilityPropertyChanged(e);
        }

        /// <summary>
        /// Handles changes of the <see cref="UIElement.VisibilityProperty"/> dependency property. As
        /// WPF internally uses the dependency property system and bypasses the
        /// <see cref="Visibility"/> property wrapper, updates of the property's value
        /// should be handled here.
        /// </summary>
        /// <param name="e">Provides information about the updated property.</param>
        private void OnVisibilityPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            Visibility newValue = (Visibility)e.NewValue;

            //update
            if (newValue == Visibility.Visible)
            {
                CreateTaskbarIcon();
            }
            else
            {
                RemoveTaskbarIcon();
            }
        }
        #endregion

        #region DataContext dependency property override / target update
        /// <summary>
        /// Updates the <see cref="FrameworkElement.DataContextProperty"/> of a given
        /// <see cref="FrameworkElement"/>. This method only updates target elements
        /// that do not already have a data context of their own, and either assigns
        /// the <see cref="FrameworkElement.DataContext"/> of the NotifyIcon, or the
        /// NotifyIcon itself, if no data context was assigned at all.
        /// </summary>
        private void UpdateDataContext(FrameworkElement target, object oldDataContextValue, object newDataContextValue)
        {
            //if there is no target or it's data context is determined through a binding
            //of its own, keep it
            if (target == null || target.GetBindingExpression(FrameworkElement.DataContextProperty) != null)
            {
                return;
            }

            //if the target's data context is the NotifyIcon's old DataContext or the NotifyIcon itself,
            //update it
            if (ReferenceEquals(this, target.DataContext) || Equals(oldDataContextValue, target.DataContext))
            {
                //assign own data context, if available. If there is no data
                //context at all, assign NotifyIcon itself.
                target.DataContext = newDataContextValue ?? this;
            }
        }

        /// <summary>
        /// A static callback listener which is being invoked if the
        /// <see cref="FrameworkElement.DataContextProperty"/> dependency property has
        /// been changed. Invokes the <see cref="OnDataContextPropertyChanged"/>
        /// instance method of the changed instance.
        /// </summary>
        /// <param name="d">The currently processed owner of the property.</param>
        /// <param name="e">Provides information about the updated property.</param>
        private static void DataContextPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RsTaskbarIcon owner = (RsTaskbarIcon)d;
            owner.OnDataContextPropertyChanged(e);
        }

        /// <summary>
        /// Handles changes of the <see cref="FrameworkElement.DataContextProperty"/> dependency property. As
        /// WPF internally uses the dependency property system and bypasses the
        /// <see cref="FrameworkElement.DataContext"/> property wrapper, updates of the property's value
        /// should be handled here.
        /// </summary>
        /// <param name="e">Provides information about the updated property.</param>
        private void OnDataContextPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            object newValue = e.NewValue;
            object oldValue = e.OldValue;

            //replace custom data context for ToolTips, Popup, and
            //ContextMenu
            UpdateDataContext(TrayPopupResolved, oldValue, newValue);
            UpdateDataContext(TrayToolTipResolved, oldValue, newValue);
            UpdateDataContext(ContextMenu, oldValue, newValue);
        }
        #endregion

        #region ContextMenu dependency property override
        /// <summary>
        /// A static callback listener which is being invoked if the
        /// <see cref="FrameworkElement.ContextMenuProperty"/> dependency property has
        /// been changed. Invokes the <see cref="OnContextMenuPropertyChanged"/>
        /// instance method of the changed instance.
        /// </summary>
        /// <param name="d">The currently processed owner of the property.</param>
        /// <param name="e">Provides information about the updated property.</param>
        private static void ContextMenuPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RsTaskbarIcon owner = (RsTaskbarIcon)d;
            owner.OnContextMenuPropertyChanged(e);
        }

        /// <summary>
        /// Releases the old and updates the new <see cref="ContextMenu"/> property
        /// in order to reflect both the NotifyIcon's <see cref="FrameworkElement.DataContext"/>
        /// property and have the <see cref="ParentTaskbarIconProperty"/> assigned.
        /// </summary>
        /// <param name="e">Provides information about the updated property.</param>
        private void OnContextMenuPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if (e.OldValue != null)
            {
                //remove the taskbar icon reference from the previously used element
                SetParentTaskbarIcon((DependencyObject)e.OldValue, null);
            }

            if (e.NewValue != null)
            {
                //set this taskbar icon as a reference to the new tooltip element
                SetParentTaskbarIcon((DependencyObject)e.NewValue, this);
            }

            UpdateDataContext((ContextMenu)e.NewValue, null, DataContext);
        }
        #endregion

        #region DoubleClickCommand dependency property
        /// <summary>
        /// Associates a command that is being executed if the tray icon is being
        /// double clicked.
        /// </summary>
        public static readonly DependencyProperty DoubleClickCommandProperty =
            DependencyProperty.Register("DoubleClickCommand",
                typeof(ICommand),
                typeof(RsTaskbarIcon),
                new FrameworkPropertyMetadata(null));

        /// <summary>
        /// A property wrapper for the <see cref="DoubleClickCommandProperty"/>
        /// dependency property:<br/>
        /// Associates a command that is being executed if the tray icon is being
        /// double clicked.
        /// </summary>
        [Category(CategoryName)]
        [Description("A command that is being executed if the tray icon is being double-clicked.")]
        public ICommand DoubleClickCommand
        {
            get { return (ICommand)GetValue(DoubleClickCommandProperty); }
            set { SetValue(DoubleClickCommandProperty, value); }
        }
        #endregion

        #region DoubleClickCommandParameter dependency property
        /// <summary>
        /// Command parameter for the <see cref="DoubleClickCommand"/>.
        /// </summary>
        public static readonly DependencyProperty DoubleClickCommandParameterProperty =
            DependencyProperty.Register("DoubleClickCommandParameter",
                typeof(object),
                typeof(RsTaskbarIcon),
                new FrameworkPropertyMetadata(null));

        /// <summary>
        /// A property wrapper for the <see cref="DoubleClickCommandParameterProperty"/>
        /// dependency property:<br/>
        /// Command parameter for the <see cref="DoubleClickCommand"/>.
        /// </summary>
        [Category(CategoryName)]
        [Description("Parameter to submit to the DoubleClickCommand when the user double clicks on the NotifyIcon.")]
        public object DoubleClickCommandParameter
        {
            get { return GetValue(DoubleClickCommandParameterProperty); }
            set { SetValue(DoubleClickCommandParameterProperty, value); }
        }
        #endregion

        #region DoubleClickCommandTarget dependency property
        /// <summary>
        /// The target of the command that is fired if the notify icon is double clicked.
        /// </summary>
        public static readonly DependencyProperty DoubleClickCommandTargetProperty =
            DependencyProperty.Register("DoubleClickCommandTarget",
                typeof(IInputElement),
                typeof(RsTaskbarIcon),
                new FrameworkPropertyMetadata(null));

        /// <summary>
        /// A property wrapper for the <see cref="DoubleClickCommandTargetProperty"/>
        /// dependency property:<br/>
        /// The target of the command that is fired if the notify icon is double clicked.
        /// </summary>
        [Category(CategoryName)]
        [Description("The target of the command that is fired if the notify icon is double clicked.")]
        public IInputElement DoubleClickCommandTarget
        {
            get { return (IInputElement)GetValue(DoubleClickCommandTargetProperty); }
            set { SetValue(DoubleClickCommandTargetProperty, value); }
        }
        #endregion

        #region LeftClickCommand dependency property
        /// <summary>
        /// Associates a command that is being executed if the tray icon is being
        /// double clicked.
        /// </summary>
        public static readonly DependencyProperty LeftClickCommandProperty =
            DependencyProperty.Register("LeftClickCommand",
                typeof(ICommand),
                typeof(RsTaskbarIcon),
                new FrameworkPropertyMetadata(null));

        /// <summary>
        /// A property wrapper for the <see cref="LeftClickCommandProperty"/>
        /// dependency property:<br/>
        /// Associates a command that is being executed if the tray icon is being
        /// left-clicked.
        /// </summary>
        [Category(CategoryName)]
        [Description("A command that is being executed if the tray icon is being left-clicked.")]
        public ICommand LeftClickCommand
        {
            get { return (ICommand)GetValue(LeftClickCommandProperty); }
            set { SetValue(LeftClickCommandProperty, value); }
        }
        #endregion

        #region LeftClickCommandParameter dependency property
        /// <summary>
        /// Command parameter for the <see cref="LeftClickCommand"/>.
        /// </summary>
        public static readonly DependencyProperty LeftClickCommandParameterProperty =
            DependencyProperty.Register("LeftClickCommandParameter",
                typeof(object),
                typeof(RsTaskbarIcon),
                new FrameworkPropertyMetadata(null));

        /// <summary>
        /// A property wrapper for the <see cref="LeftClickCommandParameterProperty"/>
        /// dependency property:<br/>
        /// Command parameter for the <see cref="LeftClickCommand"/>.
        /// </summary>
        [Category(CategoryName)]
        [Description("The target of the command that is fired if the notify icon is clicked with the left mouse button.")]
        public object LeftClickCommandParameter
        {
            get { return GetValue(LeftClickCommandParameterProperty); }
            set { SetValue(LeftClickCommandParameterProperty, value); }
        }
        #endregion

        #region LeftClickCommandTarget dependency property
        /// <summary>
        /// The target of the command that is fired if the notify icon is clicked.
        /// </summary>
        public static readonly DependencyProperty LeftClickCommandTargetProperty =
            DependencyProperty.Register("LeftClickCommandTarget",
                typeof(IInputElement),
                typeof(RsTaskbarIcon),
                new FrameworkPropertyMetadata(null));

        /// <summary>
        /// A property wrapper for the <see cref="LeftClickCommandTargetProperty"/>
        /// dependency property:<br/>
        /// The target of the command that is fired if the notify icon is clicked.
        /// </summary>
        [Category(CategoryName)]
        [Description("The target of the command that is fired if the notify icon is clicked with the left mouse button.")]
        public IInputElement LeftClickCommandTarget
        {
            get { return (IInputElement)GetValue(LeftClickCommandTargetProperty); }
            set { SetValue(LeftClickCommandTargetProperty, value); }
        }
        #endregion
        #endregion

        #region Attached Properties
        #region ParentTaskbarIcon
        /// <summary>
        /// An attached property that is assigned to displayed UI elements (balloos, tooltips, context menus), and
        /// that can be used to bind to this control. The attached property is being derived, so binding is
        /// quite straightforward:
        /// <code>
        /// <TextBlock Text="{Binding RelativeSource={RelativeSource Self}, Path=(tb:TaskbarIcon.ParentTaskbarIcon).ToolTipText}" />
        /// </code>
        /// </summary>  
        public static readonly DependencyProperty ParentTaskbarIconProperty =
            DependencyProperty.RegisterAttached(
                "ParentTaskbarIcon",
                typeof(RsTaskbarIcon),
                typeof(RsTaskbarIcon),
                new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.Inherits));

        /// <summary>
        /// Gets the ParentTaskbarIcon property.  This dependency property 
        /// indicates ....
        /// </summary>
        public static RsTaskbarIcon GetParentTaskbarIcon(DependencyObject d)
        {
            return (RsTaskbarIcon)d.GetValue(ParentTaskbarIconProperty);
        }

        /// <summary>
        /// Sets the ParentTaskbarIcon property.  This dependency property 
        /// indicates ....
        /// </summary>
        public static void SetParentTaskbarIcon(DependencyObject d, RsTaskbarIcon value)
        {
            d.SetValue(ParentTaskbarIconProperty, value);
        }
        #endregion
        #endregion

        #region Events
        #region TrayLeftMouseDown
        /// <summary>
        /// TrayLeftMouseDown Routed Event
        /// </summary>
        public static readonly RoutedEvent TrayLeftMouseDownEvent =
            EventManager.RegisterRoutedEvent(
                "TrayLeftMouseDown",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(RsTaskbarIcon));

        /// <summary>
        /// Occurs when the user presses the left mouse button.
        /// </summary>
        [Category(CategoryName)]
        public event RoutedEventHandler TrayLeftMouseDown
        {
            add { AddHandler(TrayLeftMouseDownEvent, value); }
            remove { RemoveHandler(TrayLeftMouseDownEvent, value); }
        }

        /// <summary>
        /// A helper method to raise the TrayLeftMouseDown event.
        /// </summary>
        protected RoutedEventArgs RaiseTrayLeftMouseDownEvent()
        {
            RoutedEventArgs args = RaiseTrayLeftMouseDownEvent(this);
            return args;
        }

        /// <summary>
        /// A static helper method to raise the TrayLeftMouseDown event on a target element.
        /// </summary>
        /// <param name="target">UIElement or ContentElement on which to raise the event</param>
        internal static RoutedEventArgs RaiseTrayLeftMouseDownEvent(DependencyObject target)
        {
            if (target == null)
            {
                return null;
            }

            RoutedEventArgs args = new RoutedEventArgs();
            args.RoutedEvent = TrayLeftMouseDownEvent;
            RoutedEventHelper.RaiseEvent(target, args);
            return args;
        }
        #endregion

        #region TrayRightMouseDown
        /// <summary>
        /// TrayRightMouseDown Routed Event
        /// </summary>
        public static readonly RoutedEvent TrayRightMouseDownEvent =
            EventManager.RegisterRoutedEvent(
                "TrayRightMouseDown",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(RsTaskbarIcon));

        /// <summary>
        /// Occurs when the presses the right mouse button.
        /// </summary>
        public event RoutedEventHandler TrayRightMouseDown
        {
            add { AddHandler(TrayRightMouseDownEvent, value); }
            remove { RemoveHandler(TrayRightMouseDownEvent, value); }
        }

        /// <summary>
        /// A helper method to raise the TrayRightMouseDown event.
        /// </summary>
        protected RoutedEventArgs RaiseTrayRightMouseDownEvent()
        {
            return RaiseTrayRightMouseDownEvent(this);
        }

        /// <summary>
        /// A static helper method to raise the TrayRightMouseDown event on a target element.
        /// </summary>
        /// <param name="target">UIElement or ContentElement on which to raise the event</param>
        internal static RoutedEventArgs RaiseTrayRightMouseDownEvent(DependencyObject target)
        {
            if (target == null)
            {
                return null;
            }

            RoutedEventArgs args = new RoutedEventArgs();
            args.RoutedEvent = TrayRightMouseDownEvent;
            RoutedEventHelper.RaiseEvent(target, args);
            return args;
        }
        #endregion

        #region TrayMiddleMouseDown
        /// <summary>
        /// TrayMiddleMouseDown Routed Event
        /// </summary>
        public static readonly RoutedEvent TrayMiddleMouseDownEvent =
            EventManager.RegisterRoutedEvent(
                "TrayMiddleMouseDown",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(RsTaskbarIcon));

        /// <summary>
        /// Occurs when the user presses the middle mouse button.
        /// </summary>
        public event RoutedEventHandler TrayMiddleMouseDown
        {
            add { AddHandler(TrayMiddleMouseDownEvent, value); }
            remove { RemoveHandler(TrayMiddleMouseDownEvent, value); }
        }

        /// <summary>
        /// A helper method to raise the TrayMiddleMouseDown event.
        /// </summary>
        protected RoutedEventArgs RaiseTrayMiddleMouseDownEvent()
        {
            return RaiseTrayMiddleMouseDownEvent(this);
        }

        /// <summary>
        /// A static helper method to raise the TrayMiddleMouseDown event on a target element.
        /// </summary>
        /// <param name="target">UIElement or ContentElement on which to raise the event</param>
        internal static RoutedEventArgs RaiseTrayMiddleMouseDownEvent(DependencyObject target)
        {
            if (target == null)
            {
                return null;
            }

            RoutedEventArgs args = new RoutedEventArgs();
            args.RoutedEvent = TrayMiddleMouseDownEvent;
            RoutedEventHelper.RaiseEvent(target, args);
            return args;
        }
        #endregion

        #region TrayLeftMouseUp
        /// <summary>
        /// TrayLeftMouseUp Routed Event
        /// </summary>
        public static readonly RoutedEvent TrayLeftMouseUpEvent =
            EventManager.RegisterRoutedEvent(
                "TrayLeftMouseUp",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(RsTaskbarIcon));

        /// <summary>
        /// Occurs when the user releases the left mouse button.
        /// </summary>
        public event RoutedEventHandler TrayLeftMouseUp
        {
            add { AddHandler(TrayLeftMouseUpEvent, value); }
            remove { RemoveHandler(TrayLeftMouseUpEvent, value); }
        }

        /// <summary>
        /// A helper method to raise the TrayLeftMouseUp event.
        /// </summary>
        protected RoutedEventArgs RaiseTrayLeftMouseUpEvent()
        {
            return RaiseTrayLeftMouseUpEvent(this);
        }

        /// <summary>
        /// A static helper method to raise the TrayLeftMouseUp event on a target element.
        /// </summary>
        /// <param name="target">UIElement or ContentElement on which to raise the event</param>
        internal static RoutedEventArgs RaiseTrayLeftMouseUpEvent(DependencyObject target)
        {
            if (target == null)
            {
                return null;
            }

            RoutedEventArgs args = new RoutedEventArgs();
            args.RoutedEvent = TrayLeftMouseUpEvent;
            RoutedEventHelper.RaiseEvent(target, args);
            return args;
        }

        #endregion

        #region TrayRightMouseUp
        /// <summary>
        /// TrayRightMouseUp Routed Event
        /// </summary>
        public static readonly RoutedEvent TrayRightMouseUpEvent =
            EventManager.RegisterRoutedEvent(
                "TrayRightMouseUp",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(RsTaskbarIcon));

        /// <summary>
        /// Occurs when the user releases the right mouse button.
        /// </summary>
        public event RoutedEventHandler TrayRightMouseUp
        {
            add { AddHandler(TrayRightMouseUpEvent, value); }
            remove { RemoveHandler(TrayRightMouseUpEvent, value); }
        }

        /// <summary>
        /// A helper method to raise the TrayRightMouseUp event.
        /// </summary>
        protected RoutedEventArgs RaiseTrayRightMouseUpEvent()
        {
            return RaiseTrayRightMouseUpEvent(this);
        }

        /// <summary>
        /// A static helper method to raise the TrayRightMouseUp event on a target element.
        /// </summary>
        /// <param name="target">UIElement or ContentElement on which to raise the event</param>
        internal static RoutedEventArgs RaiseTrayRightMouseUpEvent(DependencyObject target)
        {
            if (target == null)
            {
                return null;
            }

            RoutedEventArgs args = new RoutedEventArgs();
            args.RoutedEvent = TrayRightMouseUpEvent;
            RoutedEventHelper.RaiseEvent(target, args);
            return args;
        }
        #endregion

        #region TrayMiddleMouseUp
        /// <summary>
        /// TrayMiddleMouseUp Routed Event
        /// </summary>
        public static readonly RoutedEvent TrayMiddleMouseUpEvent =
            EventManager.RegisterRoutedEvent(
                "TrayMiddleMouseUp",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(RsTaskbarIcon));

        /// <summary>
        /// Occurs when the user releases the middle mouse button.
        /// </summary>
        public event RoutedEventHandler TrayMiddleMouseUp
        {
            add { AddHandler(TrayMiddleMouseUpEvent, value); }
            remove { RemoveHandler(TrayMiddleMouseUpEvent, value); }
        }

        /// <summary>
        /// A helper method to raise the TrayMiddleMouseUp event.
        /// </summary>
        protected RoutedEventArgs RaiseTrayMiddleMouseUpEvent()
        {
            return RaiseTrayMiddleMouseUpEvent(this);
        }

        /// <summary>
        /// A static helper method to raise the TrayMiddleMouseUp event on a target element.
        /// </summary>
        /// <param name="target">UIElement or ContentElement on which to raise the event</param>
        internal static RoutedEventArgs RaiseTrayMiddleMouseUpEvent(DependencyObject target)
        {
            if (target == null)
            {
                return null;
            }

            RoutedEventArgs args = new RoutedEventArgs();
            args.RoutedEvent = TrayMiddleMouseUpEvent;
            RoutedEventHelper.RaiseEvent(target, args);
            return args;
        }

        #endregion

        #region TrayMouseDoubleClick
        /// <summary>
        /// TrayMouseDoubleClick Routed Event
        /// </summary>
        public static readonly RoutedEvent TrayMouseDoubleClickEvent =
            EventManager.RegisterRoutedEvent(
                "TrayMouseDoubleClick",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(RsTaskbarIcon));

        /// <summary>
        /// Occurs when the user double-clicks the taskbar icon.
        /// </summary>
        public event RoutedEventHandler TrayMouseDoubleClick
        {
            add { AddHandler(TrayMouseDoubleClickEvent, value); }
            remove { RemoveHandler(TrayMouseDoubleClickEvent, value); }
        }

        /// <summary>
        /// A helper method to raise the TrayMouseDoubleClick event.
        /// </summary>
        protected RoutedEventArgs RaiseTrayMouseDoubleClickEvent()
        {
            RoutedEventArgs args = RaiseTrayMouseDoubleClickEvent(this);
            DoubleClickCommand.ExecuteIfEnabled(DoubleClickCommandParameter, DoubleClickCommandTarget ?? this);
            return args;
        }

        /// <summary>
        /// A static helper method to raise the TrayMouseDoubleClick event on a target element.
        /// </summary>
        /// <param name="target">UIElement or ContentElement on which to raise the event</param>
        internal static RoutedEventArgs RaiseTrayMouseDoubleClickEvent(DependencyObject target)
        {
            if (target == null)
            {
                return null;
            }

            RoutedEventArgs args = new RoutedEventArgs();
            args.RoutedEvent = TrayMouseDoubleClickEvent;
            RoutedEventHelper.RaiseEvent(target, args);
            return args;
        }
        #endregion

        #region TrayMouseMove
        /// <summary>
        /// TrayMouseMove Routed Event
        /// </summary>
        public static readonly RoutedEvent TrayMouseMoveEvent =
            EventManager.RegisterRoutedEvent(
                "TrayMouseMove",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(RsTaskbarIcon));

        /// <summary>
        /// Occurs when the user moves the mouse over the taskbar icon.
        /// </summary>
        public event RoutedEventHandler TrayMouseMove
        {
            add { AddHandler(TrayMouseMoveEvent, value); }
            remove { RemoveHandler(TrayMouseMoveEvent, value); }
        }

        /// <summary>
        /// A helper method to raise the TrayMouseMove event.
        /// </summary>
        protected RoutedEventArgs RaiseTrayMouseMoveEvent()
        {
            return RaiseTrayMouseMoveEvent(this);
        }

        /// <summary>
        /// A static helper method to raise the TrayMouseMove event on a target element.
        /// </summary>
        /// <param name="target">UIElement or ContentElement on which to raise the event</param>
        internal static RoutedEventArgs RaiseTrayMouseMoveEvent(DependencyObject target)
        {
            if (target == null)
            {
                return null;
            }

            var args = new RoutedEventArgs();
            args.RoutedEvent = TrayMouseMoveEvent;
            RoutedEventHelper.RaiseEvent(target, args);
            return args;
        }
        #endregion

        #region TrayBalloonTipShown
        /// <summary>
        /// TrayBalloonTipShown Routed Event
        /// </summary>
        public static readonly RoutedEvent TrayBalloonTipShownEvent =
            EventManager.RegisterRoutedEvent(
                "TrayBalloonTipShown",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(RsTaskbarIcon));

        /// <summary>
        /// Occurs when a balloon ToolTip is displayed.
        /// </summary>
        public event RoutedEventHandler TrayBalloonTipShown
        {
            add { AddHandler(TrayBalloonTipShownEvent, value); }
            remove { RemoveHandler(TrayBalloonTipShownEvent, value); }
        }

        /// <summary>
        /// A helper method to raise the TrayBalloonTipShown event.
        /// </summary>
        protected RoutedEventArgs RaiseTrayBalloonTipShownEvent()
        {
            return RaiseTrayBalloonTipShownEvent(this);
        }

        /// <summary>
        /// A static helper method to raise the TrayBalloonTipShown event on a target element.
        /// </summary>
        /// <param name="target">UIElement or ContentElement on which to raise the event</param>
        internal static RoutedEventArgs RaiseTrayBalloonTipShownEvent(DependencyObject target)
        {
            if (target == null)
            {
                return null;
            }

            RoutedEventArgs args = new RoutedEventArgs();
            args.RoutedEvent = TrayBalloonTipShownEvent;
            RoutedEventHelper.RaiseEvent(target, args);
            return args;
        }
        #endregion

        #region TrayBalloonTipClosed
        /// <summary>
        /// TrayBalloonTipClosed Routed Event
        /// </summary>
        public static readonly RoutedEvent TrayBalloonTipClosedEvent =
            EventManager.RegisterRoutedEvent(
                "TrayBalloonTipClosed",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(RsTaskbarIcon));

        /// <summary>
        /// Occurs when a balloon ToolTip was closed.
        /// </summary>
        public event RoutedEventHandler TrayBalloonTipClosed
        {
            add { AddHandler(TrayBalloonTipClosedEvent, value); }
            remove { RemoveHandler(TrayBalloonTipClosedEvent, value); }
        }

        /// <summary>
        /// A helper method to raise the TrayBalloonTipClosed event.
        /// </summary>
        protected RoutedEventArgs RaiseTrayBalloonTipClosedEvent()
        {
            return RaiseTrayBalloonTipClosedEvent(this);
        }

        /// <summary>
        /// A static helper method to raise the TrayBalloonTipClosed event on a target element.
        /// </summary>
        /// <param name="target">UIElement or ContentElement on which to raise the event</param>
        internal static RoutedEventArgs RaiseTrayBalloonTipClosedEvent(DependencyObject target)
        {
            if (target == null)
            {
                return null;
            }

            RoutedEventArgs args = new RoutedEventArgs();
            args.RoutedEvent = TrayBalloonTipClosedEvent;
            RoutedEventHelper.RaiseEvent(target, args);
            return args;
        }
        #endregion

        #region TrayBalloonTipClicked
        /// <summary>
        /// TrayBalloonTipClicked Routed Event
        /// </summary>
        public static readonly RoutedEvent TrayBalloonTipClickedEvent =
            EventManager.RegisterRoutedEvent(
                "TrayBalloonTipClicked",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(RsTaskbarIcon));

        /// <summary>
        /// Occurs when the user clicks on a balloon ToolTip.
        /// </summary>
        public event RoutedEventHandler TrayBalloonTipClicked
        {
            add { AddHandler(TrayBalloonTipClickedEvent, value); }
            remove { RemoveHandler(TrayBalloonTipClickedEvent, value); }
        }

        /// <summary>
        /// A helper method to raise the TrayBalloonTipClicked event.
        /// </summary>
        protected RoutedEventArgs RaiseTrayBalloonTipClickedEvent()
        {
            return RaiseTrayBalloonTipClickedEvent(this);
        }

        /// <summary>
        /// A static helper method to raise the TrayBalloonTipClicked event on a target element.
        /// </summary>
        /// <param name="target">UIElement or ContentElement on which to raise the event</param>
        internal static RoutedEventArgs RaiseTrayBalloonTipClickedEvent(DependencyObject target)
        {
            if (target == null)
            {
                return null;
            }

            RoutedEventArgs args = new RoutedEventArgs();
            args.RoutedEvent = TrayBalloonTipClickedEvent;
            RoutedEventHelper.RaiseEvent(target, args);
            return args;
        }
        #endregion

        #region TrayContextMenuOpen (and PreviewTrayContextMenuOpen)
        /// <summary>
        /// TrayContextMenuOpen Routed Event
        /// </summary>
        public static readonly RoutedEvent TrayContextMenuOpenEvent =
            EventManager.RegisterRoutedEvent(
                "TrayContextMenuOpen",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(RsTaskbarIcon));

        /// <summary>
        /// Bubbled event that occurs when the context menu of the taskbar icon is being displayed.
        /// </summary>
        public event RoutedEventHandler TrayContextMenuOpen
        {
            add { AddHandler(TrayContextMenuOpenEvent, value); }
            remove { RemoveHandler(TrayContextMenuOpenEvent, value); }
        }

        /// <summary>
        /// A helper method to raise the TrayContextMenuOpen event.
        /// </summary>
        protected RoutedEventArgs RaiseTrayContextMenuOpenEvent()
        {
            return RaiseTrayContextMenuOpenEvent(this);
        }

        /// <summary>
        /// A static helper method to raise the TrayContextMenuOpen event on a target element.
        /// </summary>
        /// <param name="target">UIElement or ContentElement on which to raise the event</param>
        internal static RoutedEventArgs RaiseTrayContextMenuOpenEvent(DependencyObject target)
        {
            if (target == null)
            {
                return null;
            }

            RoutedEventArgs args = new RoutedEventArgs();
            args.RoutedEvent = TrayContextMenuOpenEvent;
            RoutedEventHelper.RaiseEvent(target, args);
            return args;
        }

        /// <summary>
        /// PreviewTrayContextMenuOpen Routed Event
        /// </summary>
        public static readonly RoutedEvent PreviewTrayContextMenuOpenEvent =
            EventManager.RegisterRoutedEvent(
                "PreviewTrayContextMenuOpen",
                RoutingStrategy.Tunnel,
                typeof(RoutedEventHandler),
                typeof(RsTaskbarIcon));

        /// <summary>
        /// Tunneled event that occurs when the context menu of the taskbar icon is being displayed.
        /// </summary>
        public event RoutedEventHandler PreviewTrayContextMenuOpen
        {
            add { AddHandler(PreviewTrayContextMenuOpenEvent, value); }
            remove { RemoveHandler(PreviewTrayContextMenuOpenEvent, value); }
        }

        /// <summary>
        /// A helper method to raise the PreviewTrayContextMenuOpen event.
        /// </summary>
        protected RoutedEventArgs RaisePreviewTrayContextMenuOpenEvent()
        {
            return RaisePreviewTrayContextMenuOpenEvent(this);
        }

        /// <summary>
        /// A static helper method to raise the PreviewTrayContextMenuOpen event on a target element.
        /// </summary>
        /// <param name="target">UIElement or ContentElement on which to raise the event</param>
        internal static RoutedEventArgs RaisePreviewTrayContextMenuOpenEvent(DependencyObject target)
        {
            if (target == null)
            {
                return null;
            }

            RoutedEventArgs args = new RoutedEventArgs();
            args.RoutedEvent = PreviewTrayContextMenuOpenEvent;
            RoutedEventHelper.RaiseEvent(target, args);
            return args;
        }
        #endregion

        #region TrayPopupOpen (and PreviewTrayPopupOpen)
        /// <summary>
        /// TrayPopupOpen Routed Event
        /// </summary>
        public static readonly RoutedEvent TrayPopupOpenEvent =
            EventManager.RegisterRoutedEvent(
                "TrayPopupOpen",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(RsTaskbarIcon));

        /// <summary>
        /// Bubbled event that occurs when the custom popup is being opened.
        /// </summary>
        public event RoutedEventHandler TrayPopupOpen
        {
            add { AddHandler(TrayPopupOpenEvent, value); }
            remove { RemoveHandler(TrayPopupOpenEvent, value); }
        }

        /// <summary>
        /// A helper method to raise the TrayPopupOpen event.
        /// </summary>
        protected RoutedEventArgs RaiseTrayPopupOpenEvent()
        {
            return RaiseTrayPopupOpenEvent(this);
        }

        /// <summary>
        /// A static helper method to raise the TrayPopupOpen event on a target element.
        /// </summary>
        /// <param name="target">UIElement or ContentElement on which to raise the event</param>
        internal static RoutedEventArgs RaiseTrayPopupOpenEvent(DependencyObject target)
        {
            if (target == null)
            {
                return null;
            }

            RoutedEventArgs args = new RoutedEventArgs();
            args.RoutedEvent = TrayPopupOpenEvent;
            RoutedEventHelper.RaiseEvent(target, args);
            return args;
        }

        /// <summary>
        /// PreviewTrayPopupOpen Routed Event
        /// </summary>
        public static readonly RoutedEvent PreviewTrayPopupOpenEvent =
            EventManager.RegisterRoutedEvent(
                "PreviewTrayPopupOpen",
                RoutingStrategy.Tunnel,
                typeof(RoutedEventHandler),
                typeof(RsTaskbarIcon));

        /// <summary>
        /// Tunneled event that occurs when the custom popup is being opened.
        /// </summary>
        public event RoutedEventHandler PreviewTrayPopupOpen
        {
            add { AddHandler(PreviewTrayPopupOpenEvent, value); }
            remove { RemoveHandler(PreviewTrayPopupOpenEvent, value); }
        }

        /// <summary>
        /// A helper method to raise the PreviewTrayPopupOpen event.
        /// </summary>
        protected RoutedEventArgs RaisePreviewTrayPopupOpenEvent()
        {
            return RaisePreviewTrayPopupOpenEvent(this);
        }

        /// <summary>
        /// A static helper method to raise the PreviewTrayPopupOpen event on a target element.
        /// </summary>
        /// <param name="target">UIElement or ContentElement on which to raise the event</param>
        internal static RoutedEventArgs RaisePreviewTrayPopupOpenEvent(DependencyObject target)
        {
            if (target == null)
            {
                return null;
            }

            RoutedEventArgs args = new RoutedEventArgs();
            args.RoutedEvent = PreviewTrayPopupOpenEvent;
            RoutedEventHelper.RaiseEvent(target, args);
            return args;
        }
        #endregion

        #region TrayToolTipOpen (and PreviewTrayToolTipOpen)
        /// <summary>
        /// TrayToolTipOpen Routed Event
        /// </summary>
        public static readonly RoutedEvent TrayToolTipOpenEvent =
            EventManager.RegisterRoutedEvent(
                "TrayToolTipOpen",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(RsTaskbarIcon));

        /// <summary>
        /// Bubbled event that occurs when the custom ToolTip is being displayed.
        /// </summary>
        public event RoutedEventHandler TrayToolTipOpen
        {
            add { AddHandler(TrayToolTipOpenEvent, value); }
            remove { RemoveHandler(TrayToolTipOpenEvent, value); }
        }

        /// <summary>
        /// A helper method to raise the TrayToolTipOpen event.
        /// </summary>
        protected RoutedEventArgs RaiseTrayToolTipOpenEvent()
        {
            return RaiseTrayToolTipOpenEvent(this);
        }

        /// <summary>
        /// A static helper method to raise the TrayToolTipOpen event on a target element.
        /// </summary>
        /// <param name="target">UIElement or ContentElement on which to raise the event</param>
        internal static RoutedEventArgs RaiseTrayToolTipOpenEvent(DependencyObject target)
        {
            if (target == null)
            {
                return null;
            }

            RoutedEventArgs args = new RoutedEventArgs();
            args.RoutedEvent = TrayToolTipOpenEvent;
            RoutedEventHelper.RaiseEvent(target, args);
            return args;
        }

        /// <summary>
        /// PreviewTrayToolTipOpen Routed Event
        /// </summary>
        public static readonly RoutedEvent PreviewTrayToolTipOpenEvent =
            EventManager.RegisterRoutedEvent(
                "PreviewTrayToolTipOpen",
                RoutingStrategy.Tunnel,
                typeof(RoutedEventHandler),
                typeof(RsTaskbarIcon));

        /// <summary>
        /// Tunneled event that occurs when the custom ToolTip is being displayed.
        /// </summary>
        public event RoutedEventHandler PreviewTrayToolTipOpen
        {
            add { AddHandler(PreviewTrayToolTipOpenEvent, value); }
            remove { RemoveHandler(PreviewTrayToolTipOpenEvent, value); }
        }

        /// <summary>
        /// A helper method to raise the PreviewTrayToolTipOpen event.
        /// </summary>
        protected RoutedEventArgs RaisePreviewTrayToolTipOpenEvent()
        {
            return RaisePreviewTrayToolTipOpenEvent(this);
        }

        /// <summary>
        /// A static helper method to raise the PreviewTrayToolTipOpen event on a target element.
        /// </summary>
        /// <param name="target">UIElement or ContentElement on which to raise the event</param>
        internal static RoutedEventArgs RaisePreviewTrayToolTipOpenEvent(DependencyObject target)
        {
            if (target == null)
            {
                return null;
            }

            RoutedEventArgs args = new RoutedEventArgs();
            args.RoutedEvent = PreviewTrayToolTipOpenEvent;
            RoutedEventHelper.RaiseEvent(target, args);
            return args;
        }
        #endregion

        #region TrayToolTipClose (and PreviewTrayToolTipClose)
        /// <summary>
        /// TrayToolTipClose Routed Event
        /// </summary>
        public static readonly RoutedEvent TrayToolTipCloseEvent =
            EventManager.RegisterRoutedEvent(
                "TrayToolTipClose",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(RsTaskbarIcon));

        /// <summary>
        /// Bubbled event that occurs when a custom tooltip is being closed.
        /// </summary>
        public event RoutedEventHandler TrayToolTipClose
        {
            add { AddHandler(TrayToolTipCloseEvent, value); }
            remove { RemoveHandler(TrayToolTipCloseEvent, value); }
        }

        /// <summary>
        /// A helper method to raise the TrayToolTipClose event.
        /// </summary>
        protected RoutedEventArgs RaiseTrayToolTipCloseEvent()
        {
            return RaiseTrayToolTipCloseEvent(this);
        }

        /// <summary>
        /// A static helper method to raise the TrayToolTipClose event on a target element.
        /// </summary>
        /// <param name="target">UIElement or ContentElement on which to raise the event</param>
        internal static RoutedEventArgs RaiseTrayToolTipCloseEvent(DependencyObject target)
        {
            if (target == null)
            {
                return null;
            }

            RoutedEventArgs args = new RoutedEventArgs();
            args.RoutedEvent = TrayToolTipCloseEvent;
            RoutedEventHelper.RaiseEvent(target, args);
            return args;
        }

        /// <summary>
        /// PreviewTrayToolTipClose Routed Event
        /// </summary>
        public static readonly RoutedEvent PreviewTrayToolTipCloseEvent =
            EventManager.RegisterRoutedEvent(
                "PreviewTrayToolTipClose",
                RoutingStrategy.Tunnel,
                typeof(RoutedEventHandler),
                typeof(RsTaskbarIcon));

        /// <summary>
        /// Tunneled event that occurs when a custom tooltip is being closed.
        /// </summary>
        public event RoutedEventHandler PreviewTrayToolTipClose
        {
            add { AddHandler(PreviewTrayToolTipCloseEvent, value); }
            remove { RemoveHandler(PreviewTrayToolTipCloseEvent, value); }
        }

        /// <summary>
        /// A helper method to raise the PreviewTrayToolTipClose event.
        /// </summary>
        protected RoutedEventArgs RaisePreviewTrayToolTipCloseEvent()
        {
            return RaisePreviewTrayToolTipCloseEvent(this);
        }

        /// <summary>
        /// A static helper method to raise the PreviewTrayToolTipClose event on a target element.
        /// </summary>
        /// <param name="target">UIElement or ContentElement on which to raise the event</param>
        internal static RoutedEventArgs RaisePreviewTrayToolTipCloseEvent(DependencyObject target)
        {
            if (target == null)
            {
                return null;
            }

            RoutedEventArgs args = new RoutedEventArgs();
            args.RoutedEvent = PreviewTrayToolTipCloseEvent;
            RoutedEventHelper.RaiseEvent(target, args);
            return args;
        }
        #endregion
        #endregion

        #region Attached Events
        #region PopupOpened
        /// <summary>
        /// PopupOpened Attached Routed Event
        /// </summary>
        public static readonly RoutedEvent PopupOpenedEvent =
            EventManager.RegisterRoutedEvent(
                "PopupOpened",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(RsTaskbarIcon));

        /// <summary>
        /// Adds a handler for the PopupOpened attached event
        /// </summary>
        /// <param name="element">UIElement or ContentElement that listens to the event</param>
        /// <param name="handler">Event handler to be added</param>
        public static void AddPopupOpenedHandler(DependencyObject element, RoutedEventHandler handler)
        {
            RoutedEventHelper.AddHandler(element, PopupOpenedEvent, handler);
        }

        /// <summary>
        /// Removes a handler for the PopupOpened attached event
        /// </summary>
        /// <param name="element">UIElement or ContentElement that listens to the event</param>
        /// <param name="handler">Event handler to be removed</param>
        public static void RemovePopupOpenedHandler(DependencyObject element, RoutedEventHandler handler)
        {
            RoutedEventHelper.RemoveHandler(element, PopupOpenedEvent, handler);
        }

        /// <summary>
        /// A static helper method to raise the PopupOpened event on a target element.
        /// </summary>
        /// <param name="target">UIElement or ContentElement on which to raise the event</param>
        internal static RoutedEventArgs RaisePopupOpenedEvent(DependencyObject target)
        {
            if (target == null)
            {
                return null;
            }

            RoutedEventArgs args = new RoutedEventArgs();
            args.RoutedEvent = PopupOpenedEvent;
            RoutedEventHelper.RaiseEvent(target, args);
            return args;
        }
        #endregion

        #region ToolTipOpened
        /// <summary>
        /// ToolTipOpened Attached Routed Event
        /// </summary>
        public static readonly RoutedEvent ToolTipOpenedEvent =
            EventManager.RegisterRoutedEvent(
                "ToolTipOpened",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(RsTaskbarIcon));

        /// <summary>
        /// Adds a handler for the ToolTipOpened attached event
        /// </summary>
        /// <param name="element">UIElement or ContentElement that listens to the event</param>
        /// <param name="handler">Event handler to be added</param>
        public static void AddToolTipOpenedHandler(DependencyObject element, RoutedEventHandler handler)
        {
            RoutedEventHelper.AddHandler(element, ToolTipOpenedEvent, handler);
        }

        /// <summary>
        /// Removes a handler for the ToolTipOpened attached event
        /// </summary>
        /// <param name="element">UIElement or ContentElement that listens to the event</param>
        /// <param name="handler">Event handler to be removed</param>
        public static void RemoveToolTipOpenedHandler(DependencyObject element, RoutedEventHandler handler)
        {
            RoutedEventHelper.RemoveHandler(element, ToolTipOpenedEvent, handler);
        }

        /// <summary>
        /// A static helper method to raise the ToolTipOpened event on a target element.
        /// </summary>
        /// <param name="target">UIElement or ContentElement on which to raise the event</param>
        internal static RoutedEventArgs RaiseToolTipOpenedEvent(DependencyObject target)
        {
            if (target == null)
            {
                return null;
            }

            RoutedEventArgs args = new RoutedEventArgs();
            args.RoutedEvent = ToolTipOpenedEvent;
            RoutedEventHelper.RaiseEvent(target, args);
            return args;
        }
        #endregion

        #region ToolTipClose
        /// <summary>
        /// ToolTipClose Attached Routed Event
        /// </summary>
        public static readonly RoutedEvent ToolTipCloseEvent =
            EventManager.RegisterRoutedEvent(
                "ToolTipClose",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(RsTaskbarIcon));

        /// <summary>
        /// Adds a handler for the ToolTipClose attached event
        /// </summary>
        /// <param name="element">UIElement or ContentElement that listens to the event</param>
        /// <param name="handler">Event handler to be added</param>
        public static void AddToolTipCloseHandler(DependencyObject element, RoutedEventHandler handler)
        {
            RoutedEventHelper.AddHandler(element, ToolTipCloseEvent, handler);
        }

        /// <summary>
        /// Removes a handler for the ToolTipClose attached event
        /// </summary>
        /// <param name="element">UIElement or ContentElement that listens to the event</param>
        /// <param name="handler">Event handler to be removed</param>
        public static void RemoveToolTipCloseHandler(DependencyObject element, RoutedEventHandler handler)
        {
            RoutedEventHelper.RemoveHandler(element, ToolTipCloseEvent, handler);
        }

        /// <summary>
        /// A static helper method to raise the ToolTipClose event on a target element.
        /// </summary>
        /// <param name="target">UIElement or ContentElement on which to raise the event</param>
        internal static RoutedEventArgs RaiseToolTipCloseEvent(DependencyObject target)
        {
            if (target == null)
            {
                return null;
            }

            RoutedEventArgs args = new RoutedEventArgs();
            args.RoutedEvent = ToolTipCloseEvent;
            RoutedEventHelper.RaiseEvent(target, args);
            return args;
        }
        #endregion

        #region BalloonShowing
        /// <summary>
        /// BalloonShowing Attached Routed Event
        /// </summary>
        public static readonly RoutedEvent BalloonShowingEvent =
            EventManager.RegisterRoutedEvent(
                "BalloonShowing",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(RsTaskbarIcon));

        /// <summary>
        /// Adds a handler for the BalloonShowing attached event
        /// </summary>
        /// <param name="element">UIElement or ContentElement that listens to the event</param>
        /// <param name="handler">Event handler to be added</param>
        public static void AddBalloonShowingHandler(DependencyObject element, RoutedEventHandler handler)
        {
            RoutedEventHelper.AddHandler(element, BalloonShowingEvent, handler);
        }

        /// <summary>
        /// Removes a handler for the BalloonShowing attached event
        /// </summary>
        /// <param name="element">UIElement or ContentElement that listens to the event</param>
        /// <param name="handler">Event handler to be removed</param>
        public static void RemoveBalloonShowingHandler(DependencyObject element, RoutedEventHandler handler)
        {
            RoutedEventHelper.RemoveHandler(element, BalloonShowingEvent, handler);
        }

        /// <summary>
        /// A static helper method to raise the BalloonShowing event on a target element.
        /// </summary>
        /// <param name="target">UIElement or ContentElement on which to raise the event</param>
        /// <param name="source">The <see cref="RsTaskbarIcon"/> instance that manages the balloon.</param>
        internal static RoutedEventArgs RaiseBalloonShowingEvent(DependencyObject target, RsTaskbarIcon source)
        {
            if (target == null)
            {
                return null;
            }

            RoutedEventArgs args = new RoutedEventArgs(BalloonShowingEvent, source);
            RoutedEventHelper.RaiseEvent(target, args);
            return args;
        }
        #endregion

        #region BalloonClosing
        /// <summary>
        /// BalloonClosing Attached Routed Event
        /// </summary>
        public static readonly RoutedEvent BalloonClosingEvent =
            EventManager.RegisterRoutedEvent(
                "BalloonClosing",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(RsTaskbarIcon));

        /// <summary>
        /// Adds a handler for the BalloonClosing attached event
        /// </summary>
        /// <param name="element">UIElement or ContentElement that listens to the event</param>
        /// <param name="handler">Event handler to be added</param>
        public static void AddBalloonClosingHandler(DependencyObject element, RoutedEventHandler handler)
        {
            RoutedEventHelper.AddHandler(element, BalloonClosingEvent, handler);
        }

        /// <summary>
        /// Removes a handler for the BalloonClosing attached event
        /// </summary>
        /// <param name="element">UIElement or ContentElement that listens to the event</param>
        /// <param name="handler">Event handler to be removed</param>
        public static void RemoveBalloonClosingHandler(DependencyObject element, RoutedEventHandler handler)
        {
            RoutedEventHelper.RemoveHandler(element, BalloonClosingEvent, handler);
        }

        /// <summary>
        /// A static helper method to raise the BalloonClosing event on a target element.
        /// </summary>
        /// <param name="target">UIElement or ContentElement on which to raise the event</param>
        /// <param name="source">The <see cref="RsTaskbarIcon"/> instance that manages the balloon.</param>
        internal static RoutedEventArgs RaiseBalloonClosingEvent(DependencyObject target, RsTaskbarIcon source)
        {
            if (target == null)
            {
                return null;
            }

            RoutedEventArgs args = new RoutedEventArgs(BalloonClosingEvent, source);
            RoutedEventHelper.RaiseEvent(target, args);
            return args;
        }
        #endregion
        #endregion

        #region Custom Balloons
        /// <summary>
        /// Shows a custom control as a tooltip in the tray location.
        /// </summary>
        /// <param name="balloon"></param>
        /// <param name="animation">An optional animation for the popup.</param>
        /// <param name="timeout">The time after which the popup is being closed.
        /// Submit null in order to keep the balloon open inde
        /// </param>
        /// <exception cref="ArgumentNullException">If <paramref name="balloon"/>
        /// is a null reference.</exception>
        public void ShowCustomBalloon(UIElement balloon, PopupAnimation animation, int? timeout)
        {
            Dispatcher dispatcher = this.Dispatcher;
            if (Application.Current != null)
            {
                dispatcher = Application.Current.Dispatcher;
            }
            if (!dispatcher.CheckAccess())
            {
                var action = new Action(() => ShowCustomBalloon(balloon, animation, timeout));
                dispatcher.Invoke(DispatcherPriority.Normal, action);
                return;
            }

            if (balloon == null)
            {
                throw new ArgumentNullException("balloon");
            }
            if (timeout.HasValue && timeout < 500)
            {
                string msg = "Invalid timeout of {0} milliseconds. Timeout must be at least 500 ms";
                msg = String.Format(msg, timeout);
                throw new ArgumentOutOfRangeException("timeout", msg);
            }

            EnsureNotDisposed();

            //make sure we don't have an open balloon
            lock (this)
            {
                CloseBalloon();
            }

            //create an invisible popup that hosts the UIElement
            Popup popup = new Popup();
            popup.AllowsTransparency = true;

            //provide the popup with the taskbar icon's data context
            UpdateDataContext(popup, null, DataContext);

            //don't animate by default - devs can use attached
            //events or override
            popup.PopupAnimation = animation;

            //in case the balloon is cleaned up through routed events, the
            //control didn't remove the balloon from its parent popup when
            //if was closed the last time - just make sure it doesn't have
            //a parent that is a popup
            Popup parent = LogicalTreeHelper.GetParent(balloon) as Popup;
            if (parent != null)
            {
                parent.Child = null;
            }

            if (parent != null)
            {
                string msg =
                    "Cannot display control [{0}] in a new balloon popup - that control already has a parent. You may consider creating new balloons every time you want to show one.";
                msg = String.Format(msg, balloon);
                throw new InvalidOperationException(msg);
            }

            popup.Child = balloon;

            //don't set the PlacementTarget as it causes the popup to become hidden if the
            //TaskbarIcon's parent is hidden, too...
            //popup.PlacementTarget = this;

            popup.Placement = PlacementMode.AbsolutePoint;
            popup.StaysOpen = true;

            Point position = TrayInfo.GetTrayLocation();
            position = GetDeviceCoordinates(position);
            popup.HorizontalOffset = position.X - 1;
            popup.VerticalOffset = position.Y - 1;

            //store reference
            lock (this)
            {
                SetCustomBalloon(popup);
            }

            //assign this instance as an attached property
            SetParentTaskbarIcon(balloon, this);

            //fire attached event
            RaiseBalloonShowingEvent(balloon, this);

            //display item
            popup.IsOpen = true;

            if (timeout.HasValue)
            {
                //register timer to close the popup
                _balloonCloseTimer.Change(timeout.Value, Timeout.Infinite);
            }
        }

        /// <summary>
        /// Resets the closing timeout, which effectively
        /// keeps a displayed balloon message open until
        /// it is either closed programmatically through
        /// <see cref="CloseBalloon"/> or due to a new
        /// message being displayed.
        /// </summary>
        /// <param name="timeout">
        /// The new time after which the popup is being closed.
        /// Use null in order to keep the balloon open indefinitely.
        /// </param>
        public void ResetBalloonCloseTimer(int? timeout = null)
        {
            if (IsDisposed)
            {
                return;
            }

            lock (this)
            {
                //reset timer in any case
                _balloonCloseTimer.Change(timeout ?? Timeout.Infinite, Timeout.Infinite);
            }
        }

        /// <summary>
        /// Closes the current <see cref="CustomBalloon"/>, if the
        /// property is set.
        /// </summary>
        public void CloseBalloon()
        {
            if (IsDisposed)
            {
                return;
            }

            Dispatcher dispatcher = this.Dispatcher;
            if (Application.Current != null)
            {
                dispatcher = Application.Current.Dispatcher;
            }
            if (!dispatcher.CheckAccess())
            {
                Action action = CloseBalloon;
                dispatcher.Invoke(DispatcherPriority.Normal, action);
                return;
            }

            lock (this)
            {
                //reset timer in any case
                _balloonCloseTimer.Change(Timeout.Infinite, Timeout.Infinite);

                //reset old popup, if we still have one
                Popup popup = CustomBalloon;
                if (popup != null)
                {
                    UIElement element = popup.Child;

                    //announce closing
                    RoutedEventArgs eventArgs = RaiseBalloonClosingEvent(element, this);
                    if (!eventArgs.Handled)
                    {
                        //if the event was handled, clear the reference to the popup,
                        //but don't close it - the handling code has to manage this stuff now

                        //close the popup
                        popup.IsOpen = false;

                        //remove the reference of the popup to the balloon in case we want to reuse
                        //the balloon (then added to a new popup)
                        popup.Child = null;

                        //reset attached property
                        if (element != null) SetParentTaskbarIcon(element, null);
                    }

                    //remove custom balloon anyway
                    SetCustomBalloon(null);
                }
            }
        }

        /// <summary>
        /// Timer-invoke event which closes the currently open balloon and
        /// resets the <see cref="CustomBalloon"/> dependency property.
        /// </summary>
        private void CloseBalloonCallback(object state)
        {
            if (IsDisposed)
            {
                return;
            }

            //switch to UI thread
            Action action = CloseBalloon;

            Dispatcher dispatcher = this.Dispatcher;
            if (Application.Current != null)
            {
                dispatcher = Application.Current.Dispatcher;
            }
            dispatcher.Invoke(action);
        }
        #endregion

        #region Process Incoming Mouse Events
        /// <summary>
        /// Processes mouse events, which are bubbled
        /// through the class' routed events, trigger
        /// certain actions (e.g. show a popup), or
        /// both.
        /// </summary>
        /// <param name="me">Event flag.</param>
        private void OnMouseEvent(MouseEvent me)
        {
            if (IsDisposed)
            {
                return;
            }

            switch (me)
            {
                case MouseEvent.MouseMove:
                    RaiseTrayMouseMoveEvent();
                    //immediately return - there's nothing left to evaluate
                    return;
                case MouseEvent.IconRightMouseDown:
                    RaiseTrayRightMouseDownEvent();
                    break;
                case MouseEvent.IconLeftMouseDown:
                    RaiseTrayLeftMouseDownEvent();
                    break;
                case MouseEvent.IconRightMouseUp:
                    RaiseTrayRightMouseUpEvent();
                    break;
                case MouseEvent.IconLeftMouseUp:
                    RaiseTrayLeftMouseUpEvent();
                    break;
                case MouseEvent.IconMiddleMouseDown:
                    RaiseTrayMiddleMouseDownEvent();
                    break;
                case MouseEvent.IconMiddleMouseUp:
                    RaiseTrayMiddleMouseUpEvent();
                    break;
                case MouseEvent.IconDoubleClick:
                    //cancel single click timer
                    _singleClickTimer.Change(Timeout.Infinite, Timeout.Infinite);
                    //bubble event
                    RaiseTrayMouseDoubleClickEvent();
                    break;
                case MouseEvent.BalloonToolTipClicked:
                    RaiseTrayBalloonTipClickedEvent();
                    break;
                default:
                    throw new ArgumentOutOfRangeException("me", "Missing handler for mouse event flag: " + me);
            }

            //get mouse coordinates
            Point cursorPosition = new Point();
            if (_messageSink.Version == NotifyIconVersion.Vista)
            {
                //physical cursor position is supported for Vista and above
                cursorPosition = User32.GetPhysicalCursorPos();
            }
            else
            {
                cursorPosition = User32.GetCursorPos();
            }

            cursorPosition = GetDeviceCoordinates(new Point(cursorPosition.X, cursorPosition.Y));

            bool isLeftClickCommandInvoked = false;

            //show popup, if requested
            if (Match(me, PopupActivation))
            {
                if (me == MouseEvent.IconLeftMouseUp)
                {
                    //show popup once we are sure it's not a double click
                    _singleClickTimerAction = () =>
                    {
                        LeftClickCommand.ExecuteIfEnabled(LeftClickCommandParameter, LeftClickCommandTarget ?? this);
                        ShowTrayPopup(cursorPosition);
                    };
                    _singleClickTimer.Change(User32.GetDoubleClickTime(), Timeout.Infinite);
                    isLeftClickCommandInvoked = true;
                }
                else
                {
                    //show popup immediately
                    ShowTrayPopup(cursorPosition);
                }
            }

            //show context menu, if requested
            if (Match(me, MenuActivation))
            {
                if (me == MouseEvent.IconLeftMouseUp)
                {
                    //show context menu once we are sure it's not a double click
                    _singleClickTimerAction = () =>
                    {
                        LeftClickCommand.ExecuteIfEnabled(LeftClickCommandParameter, LeftClickCommandTarget ?? this);
                        ShowContextMenu(cursorPosition);
                    };
                    _singleClickTimer.Change(User32.GetDoubleClickTime(), Timeout.Infinite);
                    isLeftClickCommandInvoked = true;
                }
                else
                {
                    //show context menu immediately
                    ShowContextMenu(cursorPosition);
                }
            }

            //make sure the left click command is invoked on mouse clicks
            if (me == MouseEvent.IconLeftMouseUp && !isLeftClickCommandInvoked)
            {
                //show context menu once we are sure it's not a double click
                _singleClickTimerAction =
                    () =>
                    {
                        LeftClickCommand.ExecuteIfEnabled(LeftClickCommandParameter, LeftClickCommandTarget ?? this);
                    };
                _singleClickTimer.Change(User32.GetDoubleClickTime(), Timeout.Infinite);
            }
        }

        /// <summary>
        /// Utility method for determining if a mouse event matches a popup activation mode.
        /// </summary>
        /// <param name="me"></param>
        /// <param name="activationMode"></param>
        /// <returns></returns>
        private bool Match(MouseEvent me, PopupActivationMode activationMode)
        {
            switch (activationMode)
            {
                case PopupActivationMode.LeftClick:
                    return me == MouseEvent.IconLeftMouseUp;
                case PopupActivationMode.RightClick:
                    return me == MouseEvent.IconRightMouseUp;
                case PopupActivationMode.LeftOrRightClick:
                    return me == MouseEvent.IconLeftMouseUp || me == MouseEvent.IconRightMouseUp;
                case PopupActivationMode.LeftOrDoubleClick:
                    return me == MouseEvent.IconLeftMouseUp || me == MouseEvent.IconDoubleClick;
                case PopupActivationMode.DoubleClick:
                    return me == MouseEvent.IconDoubleClick;
                case PopupActivationMode.MiddleClick:
                    return me == MouseEvent.IconMiddleMouseUp;
                case PopupActivationMode.All:
                    //return true for everything except mouse movements
                    return me != MouseEvent.MouseMove;
                default:
                    throw new ArgumentOutOfRangeException("activationMode");
            }
        }
        #endregion

        #region ToolTips
        /// <summary>
        /// Displays a custom tooltip, if available. This method is only
        /// invoked for Windows Vista and above.
        /// </summary>
        /// <param name="visible">Whether to show or hide the tooltip.</param>
        private void OnToolTipChange(bool visible)
        {
            //if we don't have a tooltip, there's nothing to do here...
            if (TrayToolTipResolved == null)
            {
                return;
            }

            if (visible)
            {
                if (IsPopupOpen)
                {
                    //ignore if we are already displaying something down there
                    return;
                }

                var args = RaisePreviewTrayToolTipOpenEvent();
                if (args.Handled)
                {
                    return;
                }

                TrayToolTipResolved.IsOpen = true;

                //raise attached event first
                if (TrayToolTip != null) RaiseToolTipOpenedEvent(TrayToolTip);

                //bubble routed event
                RaiseTrayToolTipOpenEvent();
            }
            else
            {
                var args = RaisePreviewTrayToolTipCloseEvent();
                if (args.Handled)
                {
                    return;
                }

                //raise attached event first
                if (TrayToolTip != null) RaiseToolTipCloseEvent(TrayToolTip);

                TrayToolTipResolved.IsOpen = false;

                //bubble event
                RaiseTrayToolTipCloseEvent();
            }
        }

        /// <summary>
        /// Creates a <see cref="ToolTip"/> control that either
        /// wraps the currently set <see cref="TrayToolTip"/>
        /// control or the <see cref="ToolTipText"/> string.<br/>
        /// If <see cref="TrayToolTip"/> itself is already
        /// a <see cref="ToolTip"/> instance, it will be used directly.
        /// </summary>
        /// <remarks>We use a <see cref="ToolTip"/> rather than
        /// <see cref="Popup"/> because there was no way to prevent a
        /// popup from causing cyclic open/close commands if it was
        /// placed under the mouse. ToolTip internally uses a Popup of
        /// its own, but takes advance of Popup's internal <see cref="UIElement.IsHitTestVisible"/>
        /// property which prevents this issue.</remarks>
        private void CreateCustomToolTip()
        {
            //check if the item itself is a tooltip
            ToolTip tt = TrayToolTip as ToolTip;

            if (tt == null && TrayToolTip != null)
            {
                //create an invisible wrapper tooltip that hosts the UIElement
                tt = new ToolTip();
                tt.Placement = PlacementMode.Mouse;

                //do *not* set the placement target, as it causes the popup to become hidden if the
                //TaskbarIcon's parent is hidden, too. At runtime, the parent can be resolved through
                //the ParentTaskbarIcon attached dependency property:
                //tt.PlacementTarget = this;

                //make sure the tooltip is invisible
                tt.HasDropShadow = false;
                tt.BorderThickness = new Thickness(0);
                tt.Background = System.Windows.Media.Brushes.Transparent;

                //setting the 
                tt.StaysOpen = true;
                tt.Content = TrayToolTip;
            }
            else if (tt == null && !String.IsNullOrEmpty(ToolTipText))
            {
                //create a simple tooltip for the ToolTipText string
                tt = new ToolTip();
                tt.Content = ToolTipText;
            }

            //the tooltip explicitly gets the DataContext of this instance.
            //If there is no DataContext, the TaskbarIcon assigns itself
            if (tt != null)
            {
                UpdateDataContext(tt, null, DataContext);
            }

            //store a reference to the used tooltip
            SetTrayToolTipResolved(tt);
        }

        /// <summary>
        /// Sets tooltip settings for the class depending on defined
        /// dependency properties and OS support.
        /// </summary>
        private void WriteToolTipSettings()
        {
            const NotifyIconFlags flags = NotifyIconFlags.Tip;
            _iconData.ToolTipText = ToolTipText;

            if (_messageSink.Version == NotifyIconVersion.Vista)
            {
                //we need to set a tooltip text to get tooltip events from the
                //taskbar icon
                if (String.IsNullOrEmpty(_iconData.ToolTipText) && TrayToolTipResolved != null)
                {
                    //if we have not tooltip text but a custom tooltip, we
                    //need to set a dummy value (we're displaying the ToolTip control, not the string)
                    _iconData.ToolTipText = "ToolTip";
                }
            }

            //update the tooltip text
            RsTaskbarIconUtil.WriteIconData(ref _iconData, NotifyIconMessage.Modify, flags);
        }
        #endregion

        #region Custom Popup
        /// <summary>
        /// Creates a <see cref="ToolTip"/> control that either
        /// wraps the currently set <see cref="TrayToolTip"/>
        /// control or the <see cref="ToolTipText"/> string.<br/>
        /// If <see cref="TrayToolTip"/> itself is already
        /// a <see cref="ToolTip"/> instance, it will be used directly.
        /// </summary>
        /// <remarks>We use a <see cref="ToolTip"/> rather than
        /// <see cref="Popup"/> because there was no way to prevent a
        /// popup from causing cyclic open/close commands if it was
        /// placed under the mouse. ToolTip internally uses a Popup of
        /// its own, but takes advance of Popup's internal <see cref="UIElement.IsHitTestVisible"/>
        /// property which prevents this issue.</remarks>
        private void CreatePopup()
        {
            //check if the item itself is a popup
            Popup popup = TrayPopup as Popup;

            if (popup == null && TrayPopup != null)
            {
                //create an invisible popup that hosts the UIElement
                popup = new Popup();
                popup.AllowsTransparency = true;

                //don't animate by default - devs can use attached
                //events or override
                popup.PopupAnimation = PopupAnimation.None;

                //the CreateRootPopup method outputs binding errors in the debug window because
                //it tries to bind to "Popup-specific" properties in case they are provided by the child.
                //We don't need that so just assign the control as the child.
                popup.Child = TrayPopup;

                //do *not* set the placement target, as it causes the popup to become hidden if the
                //TaskbarIcon's parent is hidden, too. At runtime, the parent can be resolved through
                //the ParentTaskbarIcon attached dependency property:
                //popup.PlacementTarget = this;

                popup.Placement = PlacementMode.AbsolutePoint;
                popup.StaysOpen = false;
            }

            //the popup explicitly gets the DataContext of this instance.
            //If there is no DataContext, the TaskbarIcon assigns itself
            if (popup != null)
            {
                UpdateDataContext(popup, null, DataContext);
            }

            //store a reference to the used tooltip
            SetTrayPopupResolved(popup);
        }

        /// <summary>
        /// Displays the <see cref="TrayPopup"/> control if
        /// it was set.
        /// </summary>
        private void ShowTrayPopup(Point cursorPosition)
        {
            if (IsDisposed)
            {
                return;
            }

            //raise preview event no matter whether popup is currently set
            //or not (enables client to set it on demand)
            var args = RaisePreviewTrayPopupOpenEvent();
            if (args.Handled)
            {
                return;
            }

            if (TrayPopup != null)
            {
                //use absolute position, but place the popup centered above the icon
                TrayPopupResolved.Placement = PlacementMode.AbsolutePoint;
                TrayPopupResolved.HorizontalOffset = cursorPosition.X;
                TrayPopupResolved.VerticalOffset = cursorPosition.Y;

                //open popup
                TrayPopupResolved.IsOpen = true;

                IntPtr handle = IntPtr.Zero;
                if (TrayPopupResolved.Child != null)
                {
                    //try to get a handle on the popup itself (via its child)
                    HwndSource source = (HwndSource)PresentationSource.FromVisual(TrayPopupResolved.Child);
                    if (source != null)
                    {
                        handle = source.Handle;
                    }
                }

                //if we don't have a handle for the popup, fall back to the message sink
                if (handle == IntPtr.Zero)
                {
                    handle = _messageSink.MessageWindowHandle;
                }

                //activate either popup or message sink to track deactivation.
                //otherwise, the popup does not close if the user clicks somewhere else
                User32.SetForegroundWindow(handle);

                //raise attached event - item should never be null unless developers
                //changed the CustomPopup directly...
                if (TrayPopup != null)
                {
                    RaisePopupOpenedEvent(TrayPopup);
                }

                //bubble routed event
                RaiseTrayPopupOpenEvent();
            }
        }
        #endregion

        #region Context Menu
        /// <summary>
        /// Displays the <see cref="ContextMenu"/> if
        /// it was set.
        /// </summary>
        private void ShowContextMenu(Point cursorPosition)
        {
            if (IsDisposed)
            {
                return;
            }

            //raise preview event no matter whether context menu is currently set
            //or not (enables client to set it on demand)
            var args = RaisePreviewTrayContextMenuOpenEvent();
            if (args.Handled)
            {
                return;
            }

            if (ContextMenu != null)
            {
                //use absolute positioning. We need to set the coordinates, or a delayed opening
                //(e.g. when left-clicked) opens the context menu at the wrong place if the mouse
                //is moved!
                ContextMenu.Placement = PlacementMode.AbsolutePoint;
                ContextMenu.HorizontalOffset = cursorPosition.X;
                ContextMenu.VerticalOffset = cursorPosition.Y;
                ContextMenu.IsOpen = true;

                IntPtr handle = IntPtr.Zero;

                //try to get a handle on the context itself
                HwndSource source = (HwndSource)PresentationSource.FromVisual(ContextMenu);
                if (source != null)
                {
                    handle = source.Handle;
                }

                //if we don't have a handle for the popup, fall back to the message sink
                if (handle == IntPtr.Zero) handle = _messageSink.MessageWindowHandle;

                //activate the context menu or the message window to track deactivation - otherwise, the context menu
                //does not close if the user clicks somewhere else. With the message window
                //fallback, the context menu can't receive keyboard events - should not happen though
                User32.SetForegroundWindow(handle);

                //bubble event
                RaiseTrayContextMenuOpenEvent();
            }
        }
        #endregion

        #region Balloon Tips
        /// <summary>
        /// Bubbles events if a balloon ToolTip was displayed
        /// or removed.
        /// </summary>
        /// <param name="visible">Whether the ToolTip was just displayed
        /// or removed.</param>
        private void OnBalloonToolTipChanged(bool visible)
        {
            if (visible)
            {
                RaiseTrayBalloonTipShownEvent();
            }
            else
            {
                RaiseTrayBalloonTipClosedEvent();
            }
        }

        /// <summary>
        /// Displays a balloon tip with the specified title,
        /// text, and icon in the taskbar for the specified time period.
        /// </summary>
        /// <param name="title">The title to display on the balloon tip.</param>
        /// <param name="message">The text to display on the balloon tip.</param>
        /// <param name="symbol">A symbol that indicates the severity.</param>
        public void ShowBalloonTip(string title, string message, BalloonIcon symbol)
        {
            lock (this)
            {
                ShowBalloonTip(title, message, symbol.GetBalloonFlag(), IntPtr.Zero);
            }
        }

        /// <summary>
        /// Displays a balloon tip with the specified title,
        /// text, and a custom icon in the taskbar for the specified time period.
        /// </summary>
        /// <param name="title">The title to display on the balloon tip.</param>
        /// <param name="message">The text to display on the balloon tip.</param>
        /// <param name="customIcon">A custom icon.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="customIcon"/>
        /// is a null reference.</exception>
        public void ShowBalloonTip(string title, string message, SDraw.Icon customIcon)
        {
            if (customIcon == null)
            {
                throw new ArgumentNullException("customIcon");
            }

            lock (this)
            {
                ShowBalloonTip(title, message, NotifyIconBalloonFlags.User, customIcon.Handle);
            }
        }

        /// <summary>
        /// Invokes <see cref="Shell32.NotifyIcon"/> in order to display
        /// a given balloon ToolTip.
        /// </summary>
        /// <param name="title">The title to display on the balloon tip.</param>
        /// <param name="message">The text to display on the balloon tip.</param>
        /// <param name="flags">Indicates what icon to use.</param>
        /// <param name="balloonIconHandle">A handle to a custom icon, if any, or
        /// <see cref="IntPtr.Zero"/>.</param>
        private void ShowBalloonTip(string title, string message, NotifyIconBalloonFlags flags, IntPtr balloonIconHandle)
        {
            EnsureNotDisposed();

            _iconData.BalloonText = message ?? String.Empty;
            _iconData.BalloonTitle = title ?? String.Empty;

            _iconData.BalloonFlags = flags;
            _iconData.hBalloonIcon = balloonIconHandle;
            RsTaskbarIconUtil.WriteIconData(ref _iconData, NotifyIconMessage.Modify, NotifyIconFlags.Info | NotifyIconFlags.Icon);
        }

        /// <summary>
        /// Hides a balloon ToolTip, if any is displayed.
        /// </summary>
        public void HideBalloonTip()
        {
            EnsureNotDisposed();

            //reset balloon by just setting the info to an empty string
            _iconData.BalloonText = _iconData.BalloonTitle = String.Empty;
            RsTaskbarIconUtil.WriteIconData(ref _iconData, NotifyIconMessage.Modify, NotifyIconFlags.Info);
        }
        #endregion

        #region Single Click Timer event
        /// <summary>
        /// Performs a delayed action if the user requested an action
        /// based on a single click of the left mouse.<br/>
        /// This method is invoked by the <see cref="_singleClickTimer"/>.
        /// </summary>
        private void DoSingleClickAction(object state)
        {
            if (IsDisposed)
            {
                return;
            }

            //run action
            Action action = _singleClickTimerAction;
            if (action != null)
            {
                //cleanup action
                _singleClickTimerAction = null;

                //switch to UI thread
                Dispatcher dispatcher = this.Dispatcher;
                if (Application.Current != null)
                {
                    dispatcher = Application.Current.Dispatcher;
                }
                dispatcher.Invoke(action);
            }
        }
        #endregion

        #region Set Version (API)
        /// <summary>
        /// Sets the version flag for the <see cref="_iconData"/>.
        /// </summary>
        private void SetVersion()
        {
            _iconData.VersionOrTimeout = (int)NotifyIconVersion.Vista;
            bool status = Shell32.NotifyIcon(NotifyIconMessage.SetVersion, ref _iconData);

            if (!status)
            {
                _iconData.VersionOrTimeout = (int)NotifyIconVersion.Windows2000;
                status = RsTaskbarIconUtil.WriteIconData(ref _iconData, NotifyIconMessage.SetVersion);
            }

            if (!status)
            {
                _iconData.VersionOrTimeout = (int)NotifyIconVersion.Windows95;
                status = RsTaskbarIconUtil.WriteIconData(ref _iconData, NotifyIconMessage.SetVersion);
            }

            if (!status)
            {
                Debug.Fail("Could not set version");
            }
        }
        #endregion

        #region Create / Remove Taskbar Icon
        /// <summary>
        /// Recreates the taskbar icon if the whole taskbar was
        /// recreated (e.g. because Explorer was shut down).
        /// </summary>
        private void OnTaskbarCreated()
        {
            IsTaskbarIconCreated = false;
            CreateTaskbarIcon();
        }

        /// <summary>
        /// Creates the taskbar icon. This message is invoked during initialization,
        /// if the taskbar is restarted, and whenever the icon is displayed.
        /// </summary>
        private void CreateTaskbarIcon()
        {
            lock (this)
            {
                if (!IsTaskbarIconCreated)
                {
                    const NotifyIconFlags members = NotifyIconFlags.Message
                                                    | NotifyIconFlags.Icon
                                                    | NotifyIconFlags.Tip;

                    //write initial configuration
                    var status = RsTaskbarIconUtil.WriteIconData(ref _iconData, NotifyIconMessage.Add, members);
                    if (!status)
                    {
                        //couldn't create the icon - we can assume this is because explorer is not running (yet!)
                        //-> try a bit later again rather than throwing an exception. Typically, if the windows
                        // shell is being loaded later, this method is being reinvoked from OnTaskbarCreated
                        // (we could also retry after a delay, but that's currently YAGNI)
                        return;
                    }

                    //set to most recent version
                    SetVersion();
                    _messageSink.Version = (NotifyIconVersion)_iconData.VersionOrTimeout;

                    IsTaskbarIconCreated = true;
                }
            }
        }

        /// <summary>
        /// Closes the taskbar icon if required.
        /// </summary>
        private void RemoveTaskbarIcon()
        {
            lock (this)
            {
                //make sure we didn't schedule a creation
                if (IsTaskbarIconCreated)
                {
                    RsTaskbarIconUtil.WriteIconData(ref _iconData, NotifyIconMessage.Delete, NotifyIconFlags.Message);
                    IsTaskbarIconCreated = false;
                }
            }
        }
        #endregion

        /// <summary>
        /// Recalculates OS coordinates in order to support WPFs coordinate
        /// system if OS scaling (DPIs) is not 100%.
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        private Point GetDeviceCoordinates(Point point)
        {
            if (double.IsNaN(_scalingFactor))
            {
                var presentationSource = PresentationSource.FromVisual(this);
                if (presentationSource == null)
                {
                    _scalingFactor = 1;
                }
                else
                {
                    var transform = presentationSource.CompositionTarget.TransformToDevice;
                    _scalingFactor = 1 / transform.M11;
                }
            }

            //on standard DPI settings, just return the point
            if (_scalingFactor == 1.0)
            {
                return point;
            }

            return new Point() { X = (int)(point.X * _scalingFactor), Y = (int)(point.Y * _scalingFactor) };
        }

        #region Dispose / Exit
        /// <summary>
        /// Set to true as soon as <c>Dispose</c> has been invoked.
        /// </summary>
        public bool IsDisposed { get; private set; }

        /// <summary>
        /// Checks if the object has been disposed and
        /// raises a <see cref="ObjectDisposedException"/> in case
        /// the <see cref="IsDisposed"/> flag is true.
        /// </summary>
        private void EnsureNotDisposed()
        {
            if (IsDisposed)
            {
                throw new ObjectDisposedException(Name ?? GetType().FullName);
            }
        }

        /// <summary>
        /// Disposes the class if the application exits.
        /// </summary>
        private void OnExit(object sender, EventArgs e)
        {
            Dispose();
        }

        /// <summary>
        /// This destructor will run only if the <see cref="Dispose()"/>
        /// method does not get called. This gives this base class the
        /// opportunity to finalize.
        /// <para>
        /// Important: Do not provide destructors in types derived from
        /// this class.
        /// </para>
        /// </summary>
        ~RsTaskbarIcon()
        {
            Dispose(false);
        }

        /// <summary>
        /// Disposes the object.
        /// </summary>
        /// <remarks>This method is not virtual by design. Derived classes
        /// should override <see cref="Dispose(bool)"/>.
        /// </remarks>
        public void Dispose()
        {
            Dispose(true);

            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue 
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Closes the tray and releases all resources.
        /// </summary>
        /// <summary>
        /// <c>Dispose(bool disposing)</c> executes in two distinct scenarios.
        /// If disposing equals <c>true</c>, the method has been called directly
        /// or indirectly by a user's code. Managed and unmanaged resources
        /// can be disposed.
        /// </summary>
        /// <param name="disposing">If disposing equals <c>false</c>, the method
        /// has been called by the runtime from inside the finalizer and you
        /// should not reference other objects. Only unmanaged resources can
        /// be disposed.</param>
        /// <remarks>Check the <see cref="IsDisposed"/> property to determine whether
        /// the method has already been called.</remarks>
        private void Dispose(bool disposing)
        {
            //don't do anything if the component is already disposed
            if (IsDisposed || !disposing)
            {
                return;
            }

            lock (this)
            {
                IsDisposed = true;

                //deregister application event listener
                if (Application.Current != null)
                {
                    Application.Current.Exit -= OnExit;
                }

                //stop timers
                _singleClickTimer.Dispose();
                _balloonCloseTimer.Dispose();

                //dispose message sink
                _messageSink.Dispose();

                //remove icon
                RemoveTaskbarIcon();
            }
        }
        #endregion
    }
}
