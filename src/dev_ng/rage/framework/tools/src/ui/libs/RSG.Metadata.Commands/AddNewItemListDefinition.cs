﻿//---------------------------------------------------------------------------------------------
// <copyright file="AddNewItemListDefinition.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Commands
{
    using System.Collections.Generic;
    using RSG.Editor;
    using RSG.Metadata.Model.Definitions;
    using RSG.Metadata.Model.Definitions.Members;
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// The multi command definition for the valid list of items that can be added to the
    /// active array. This class cannot be inherited.
    /// </summary>
    public sealed class AddNewItemListDefinition : MultiCommand
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="AddNewItemListDefinition"/> class.
        /// </summary>
        public AddNewItemListDefinition()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the System.Windows.Input.RoutedCommand that this definition is wrapping.
        /// </summary>
        public override RockstarRoutedCommand Command
        {
            get { return MetadataCommands.AddArrayItem; }
        }

        /// <summary>
        /// Gets the name of this command definition.
        /// </summary>
        public override string Name
        {
            get { return "Add New Array Item List"; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates this definitions items using the specified dynamic parent as a base. This
        /// adds items to the definition based on the valid items from the specified object.
        /// </summary>
        /// <param name="dynamicParent">
        /// The dynamic parent instance that the items should be created from.
        /// </param>
        public void CreateItems(IDynamicTunableParent dynamicParent)
        {
            if (this.Items.Count > 0)
            {
                this.Items.Clear();
            }

            if (dynamicParent == null)
            {
                return;
            }

            IMember elementType = dynamicParent.ElementType;
            IStructure structure = null;
            if (elementType is PointerMember)
            {
                structure = ((PointerMember)elementType).ReferencedStructure;
                if (structure != null)
                {
                    foreach (IStructure descendant in GetSelfAndAllDescendents(structure))
                    {
                        MetadataCommandArrayArgs args = new MetadataCommandArrayArgs(
                            dynamicParent, -1, descendant);
                        this.Items.Add(
                            new AddNewItemListItem(this, descendant.ShortDataType, args));
                    }
                }
            }
            else if (elementType is StructMember)
            {
                structure = ((StructMember)elementType).ReferencedStructure;
                if (structure != null)
                {
                    MetadataCommandArrayArgs args = new MetadataCommandArrayArgs(
                        dynamicParent, -1, structure);
                    this.Items.Add(
                        new AddNewItemListItem(this, structure.ShortDataType, args));
                }
            }
            else
            {
                MetadataCommandArrayArgs args = new MetadataCommandArrayArgs(
                    dynamicParent, -1, null);
                this.Items.Add(new AddNewItemListItem(this, elementType.TypeName, args));
            }
        }

        /// <summary>
        /// Iterators through the specified structure root object and all of its descendants,
        /// immediate and children.
        /// </summary>
        /// <param name="root">
        /// The structure to start the iterator from.
        /// </param>
        /// <returns>
        /// A iterator through the specified structure root object and all of its descendants.
        /// </returns>
        private static IEnumerable<IStructure> GetSelfAndAllDescendents(IStructure root)
        {
            if (root == null)
            {
                yield break;
            }

            yield return root;
            foreach (IStructure descendant in root.ImmediateDescendants)
            {
                foreach (IStructure childDescendant in GetSelfAndAllDescendents(descendant))
                {
                    yield return childDescendant;
                }
            }
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// Defines the item used for the <see cref="AddNewItemListDefinition"/> command
        /// definition.
        /// </summary>
        private class AddNewItemListItem : MultiCommandItem<MetadataCommandArrayArgs>
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="AddNewItemListItem"/> class.
            /// </summary>
            /// <param name="definition">
            /// The multi command definition that owns this item.
            /// </param>
            /// <param name="text">
            /// The text that is used to display this item.
            /// </param>
            /// <param name="parameter">
            /// The parameter that is sent when this item is executed.
            /// </param>
            public AddNewItemListItem(
                MultiCommand definition, string text, MetadataCommandArrayArgs parameter)
                : base(definition, text, parameter)
            {
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// Gets the description for this item. This is also what is shown as a tooltip
            /// along with the key gesture if one is set if it is being rendered as a
            /// toggle control.
            /// </summary>
            public override string Description
            {
                get { return null; }
            }
            #endregion Properties
        } // AddNewItemListDefinition.AddNewItemListItem {Class}
        #endregion Classes
    } // RSG.Metadata.Commands.AddNewItemListDefinition {Class}
} // RSG.Metadata.Commands {Namespace}
