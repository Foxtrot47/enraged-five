﻿//---------------------------------------------------------------------------------------------
// <copyright file="RenameNodeDelegate.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Project.ViewModel
{
    /// <summary>
    /// Represents the method that can be used to activate the rename logic for a single
    /// hierarchy node when needed.
    /// </summary>
    /// <param name="node">
    /// The node that needs to be renamed.
    /// </param>
    public delegate void RenameNodeDelegate(IHierarchyNode node);
} // RSG.Project.ViewModel {Namespace}
