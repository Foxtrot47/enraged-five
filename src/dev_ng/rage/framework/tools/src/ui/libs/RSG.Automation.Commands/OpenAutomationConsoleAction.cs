﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Configuration;
using RSG.Editor;
using RSG.Automation.ViewModel;

namespace RSG.Automation.Commands
{

    /// <summary>
    /// 
    /// </summary>
    public class OpenAutomationConsoleAction : ButtonAction<AutomationMonitorDataContext>
    {
        #region Constructor(s)
        /// <summary>
        /// Download a job's universal log file action
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public OpenAutomationConsoleAction(ParameterResolverDelegate<AutomationMonitorDataContext> resolver)
            : base(resolver)
        {
        }
        #endregion // Constructor(s)

        #region ButtonAction<> Overrides
        /// <summary>
        /// Override to set logic against the can execute command handler.
        /// </summary>
        /// <param name="dc"></param>
        /// <returns></returns>
        public override bool CanExecute(AutomationMonitorDataContext dc)
        {
            return (true);
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="dc">
        /// The command parameter that has been requested.
        /// </param>
        public override async void Execute(AutomationMonitorDataContext dc)
        {
            IMessageBoxService msgBoxService = this.GetService<IMessageBoxService>();
            if (null == dc.SelectedAutomationService)
            {
                msgBoxService.ShowStandardErrorBox("No Automation Service selected.",
                    dc.Title);
                return;
            }

            // DHM TODO : this will not generally work across projects so might
            // cause issues with Liberty until we treat that as DLC.
            IEnvironment env = ConfigFactory.CreateEnvironment();
            env.Add("branch", dc.SelectedAutomationService.AutomationService.Console.BranchName);
            env.Add("host", dc.SelectedAutomationService.AutomationService.ServerHost.Host);
            String arguments = env.Subst(dc.SelectedAutomationService.AutomationService.Console.Arguments);

            System.Diagnostics.Process consoleProcess = new System.Diagnostics.Process();
            consoleProcess.StartInfo.FileName = Environment.ExpandEnvironmentVariables(
                @"%RS_TOOLSROOT%\ironlib\lib\RSG.Pipeline.Automation.Console.exe");
            consoleProcess.StartInfo.Arguments = arguments;
            consoleProcess.StartInfo.UseShellExecute = true;
            consoleProcess.Start();
        }
        #endregion // ButtonAction<> Overrides
    }

} // RSG.Automation.Commands namespace
