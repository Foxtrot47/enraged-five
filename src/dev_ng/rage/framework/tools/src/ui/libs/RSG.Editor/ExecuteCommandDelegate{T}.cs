﻿//---------------------------------------------------------------------------------------------
// <copyright file="ExecuteCommandDelegate{T}.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    /// <summary>
    /// Represents the method that will handle the implementation of a command.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the command parameter contained within the specified data.
    /// </typeparam>
    /// <param name="e">
    /// The object containing the data to use inside the implementation method.
    /// </param>
    public delegate void ExecuteCommandDelegate<T>(ExecuteCommandData<T> e);
} // RSG.Editor {Namespace}
