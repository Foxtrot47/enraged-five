﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using Tasks = System.Threading.Tasks;
using System.Windows.Media;
using RSG.Base.Configuration;
using RSG.Base.Configuration.Automation;
using RSG.Base.Configuration.Bugstar;
using RSG.Editor;
using RSG.Editor.Controls.AutomationMonitor.Commands.Definitions;
using RSG.Editor.Controls.AutomationMonitor.Commands.Instances;
using RSG.Editor.Controls.AutomationMonitor.ViewModel;
using RSG.Interop.Bugstar;
using RSG.Interop.Bugstar.Organisation;
using RSG.Pipeline.Automation.Common.Client;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Tasks;
using RSG.Pipeline.Automation.Consumers;

namespace RSG.Editor.Controls.AutomationMonitor
{

    /// <summary>
    /// Automation Monitor View-Model; abstract so implemention sets
    /// up the OnRefresh handler.
    /// </summary>
    public abstract class AutomationMonitorDataContext : ViewModelBase
    {
        #region Properties
        /// <summary>
        /// Whether the UI displays UTC timestamps.
        /// </summary>
        public bool UseUTC
        {
            get { return _useUTC; }
            private set
            {
                if (this._useUTC != value)
                {
                    this._useUTC = value;
                    NotifyPropertyChanged("UseUTC");
                }
            }
        }
        private bool _useUTC;
        
        /// <summary>
        /// Collection of projects (for user selection).
        /// </summary>
        public ObservableCollection<ProjectViewModel> Projects
        {
            get { return _projects; }
            private set
            {
                if (this._projects != value)
                {
                    this._projects = value;
                    NotifyPropertyChanged("Projects");
                }
            }
        }
        private ObservableCollection<ProjectViewModel> _projects;

        /// <summary>
        /// Automation Service connection.
        /// </summary>
        public AutomationServiceViewModel SelectedAutomationService
        {
            get { return _selectedAutomationService; }
            set
            {
                if (this._selectedAutomationService != value)
                {
                    this._selectedAutomationService = value;
                    NotifyPropertyChanged("SelectedAutomationService");
                }
            }
        }
        private AutomationServiceViewModel _selectedAutomationService;

        /// <summary>
        /// Whether we managed to connect to the selected Automation Service.
        /// </summary>
        public bool IsConnected
        {
            get { return _isConnected; }
            private set
            {
                if (this._isConnected != value)
                {
                    this._isConnected = value;
                    NotifyPropertyChanged("IsConnected");
                }
            }
        }
        private bool _isConnected;

        /// <summary>
        /// Whether the selected Automation Service has jobs to display.
        /// </summary>
        public bool HasJobs
        {
            get { return _hasJobs; }
            private set
            {
                if (this._hasJobs != value)
                {
                    this._hasJobs = value;
                    NotifyPropertyChanged("HasJobs");
                }
            }
        }
        private bool _hasJobs;

        /// <summary>
        /// Collection of jobs.
        /// </summary>
        public ObservableCollection<JobViewModel> Jobs
        {
            get { return _jobs; }
            private set
            {
                if (this._jobs != value)
                {
                    this._jobs = value;
                    NotifyPropertyChanged("Jobs");
                    this.HasJobs = (this._jobs.Count > 0);
                }
            }
        }
        private ObservableCollection<JobViewModel> _jobs;

        /// <summary>
        /// Gets or sets a iterator around the currently selected log message view models in the
        /// viewer controls data grid.
        /// </summary>
        public IEnumerable<JobViewModel> SelectedJobs
        {
            get { return _selectedJobs; }
            set
            {
                _selectedJobs = value;
                RefreshCommandParameters();
            }
        }
        private IEnumerable<JobViewModel> _selectedJobs;

        /// <summary>
        /// Jobs being downloaded; state is saved.
        /// </summary>
        public ObservableCollection<JobDownloadViewModel> DownloadJobs
        {
            get { return _downloadJobs; }
            private set
            {
                if (_downloadJobs != value)
                {
                    _downloadJobs = value;
                    NotifyPropertyChanged("DownloadJobs");
                }
            }
        }
        private ObservableCollection<JobDownloadViewModel> _downloadJobs;

        /// <summary>
        /// Whether the selected Automation Service has clients to display.
        /// </summary>
        public bool HasClients
        {
            get { return _hasClients; }
            private set
            {
                if (this._hasClients != value)
                {
                    this._hasClients = value;
                    NotifyPropertyChanged("HasClients");
                }
            }
        }
        private bool _hasClients;

        /// <summary>
        /// Collection of clients.
        /// </summary>
        public ObservableCollection<ClientViewModel> Clients
        {
            get { return _clients; }
            private set
            {
                if (this._clients != value)
                {
                    this._clients = value;
                    NotifyPropertyChanged("Clients");
                    this.HasClients = (this._clients.Count > 0);
                }
            }
        }
        private ObservableCollection<ClientViewModel> _clients;

        #region Display Option Properties
        /// <summary>
        /// Color of jobs where local user is owner.
        /// </summary>
        public Color OwnerColor
        {
            get { return _ownerColor; }
            set
            {
                if (this._ownerColor != value)
                {
                    this._ownerColor = value;
                    NotifyPropertyChanged("OwnerColor");
                }
            }
        }
        private Color _ownerColor;

        /// <summary>
        /// Color of jobs that are completed where local user is owner.
        /// </summary>
        public Color CompletedOwnerColor
        {
            get { return _completedOwnerColor; }
            set
            {
                if (this._completedOwnerColor != value)
                {
                    this._completedOwnerColor = value;
                    NotifyPropertyChanged("CompletedOwnerColor");
                }
            }
        }
        private Color _completedOwnerColor;

        /// <summary>
        /// Color of jobs for active downloads.
        /// </summary>
        public Color ActiveDownloadColor
        {
            get { return _activeDownloadColor; }
            set
            {
                if (this._activeDownloadColor != value)
                {
                    this._activeDownloadColor = value;
                    NotifyPropertyChanged("ActiveDownloadColor");
                }
            }
        }
        private Color _activeDownloadColor;
        #endregion // Display Option Properties
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Configuration data.
        /// </summary>
        protected IConfig _config;

        /// <summary>
        /// BUgstar configuration object.
        /// </summary>
        protected IBugstarConfig _bugstarConfig;
        
        /// <summary>
        /// Automation services configuration data.
        /// </summary>
        protected IEnumerable<IAutomationServiceConfig> _automationServicesConfig;
        private Object _automationLock = new Object();

        // Command Definitions (that require their Command Parameter refreshed).
        private AbortJobDefinition _abortJobDefinition;
        private DownloadJobLogDefinition _downloadJobLogDefinition;
        private DownloadJobOutputDefinition _downloadJobOutputDefinition;
        private PrioritiseJobDefinition _prioritiseJobDefinition;

        /// <summary>
        /// Bugstar user objects.
        /// </summary>
        private IEnumerable<RSG.Interop.Bugstar.Organisation.User> _bugstarUsers;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="config"></param>
        /// <param name="automationServicesConfig"></param>
        /// <param name="bugstarConfig"></param>
        public AutomationMonitorDataContext(IConfig config, IBugstarConfig bugstarConfig)
        {
            InitCommandDefinitions();
            AddCommands();
            this._config = config;
            this._bugstarConfig = bugstarConfig;
            this.IsConnected = false;
            this.Projects = new ObservableCollection<ProjectViewModel>();
            this.Jobs = new ObservableCollection<JobViewModel>();
            this.DownloadJobs = new ObservableCollection<JobDownloadViewModel>();
            this.Clients = new ObservableCollection<ClientViewModel>();
            this.OwnerColor = System.Windows.Media.Color.FromArgb(100, 0, 128, 0);
            this.CompletedOwnerColor = System.Windows.Media.Color.FromArgb(100, 0, 200, 0);
            this._selectedJobs = new JobViewModel[0]; // Initial empty selection.
            ReloadAutomationConfig();
            RefreshServicesAsync();

            // Bugstar User initialisation.
            BugstarConnection c = new BugstarConnection(_config, _bugstarConfig);
            c.Login(_bugstarConfig.ReadOnlyUsername, _bugstarConfig.ReadOnlyPassword, String.Empty);
            Project p = Project.GetProjectById(c, this._bugstarConfig.ProjectId);
            _bugstarUsers = p.Users;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Refresh view from Automation Service.
        /// </summary>
        public async void RefreshAsync()
        {
            if (null == this.SelectedAutomationService)
            {
                this.IsConnected = false;
                return;
            }

            bool successful = false;
            IEnumerable<WorkerStatus> clientStatus = null;
            IEnumerable<TaskStatus> taskStatus = null;
            Tasks.Task refreshTask = Tasks.Task.Factory.StartNew(() =>
            {
                try
                {
                    AutomationAdminConsumer consumer = new AutomationAdminConsumer(
                        new Uri(this.SelectedAutomationService.AutomationService.ServerHost, "automation.svc"));
#warning DHM FIX ME: consumers won't tell us whether there are no jobs/clients or connection problem.  This needs fixed.
                    clientStatus = consumer.Clients();
                    taskStatus = consumer.Monitor();
                    if (null == taskStatus || 0 == taskStatus.Count())
                        successful = false;
                    else
                        successful = true;
                }
                catch (Exception)
                {
                    successful = false;
                }
            });
            await Tasks.Task.WhenAll(refreshTask);

            this.IsConnected = successful;
            if (successful)
                RefreshAsync(taskStatus, clientStatus);
        }

        /// <summary>
        /// Refresh the Automation Services configuration.
        /// </summary>
        public async void RefreshServicesAsync()
        {
            // Reload Automation Config from disk.
            ReloadAutomationConfig();

            Tasks.Task<IEnumerable<ProjectViewModel>> refreshTask = 
                Tasks.Task.Factory.StartNew<IEnumerable<ProjectViewModel>>(RebuildProjectViewModels);
            await Tasks.Task.WhenAll(refreshTask);
            this.Projects = new ObservableCollection<ProjectViewModel>(refreshTask.Result);

            OnRefreshServices();
        }

        /// <summary>
        /// Refresh Automation services (rebuilds Job and Client View-Models).
        /// </summary>
        public async void RefreshAsync(IEnumerable<TaskStatus> taskStatus, 
            IEnumerable<WorkerStatus> clientStatus)
        {
            Tasks.Task<IEnumerable<JobViewModel>> refreshJobsTask = 
                Tasks.Task.Factory.StartNew<IEnumerable<JobViewModel>>(RebuildJobViewModels, taskStatus.FirstOrDefault());
            Tasks.Task<IEnumerable<ClientViewModel>> refreshClientsTask = 
                Tasks.Task.Factory.StartNew<IEnumerable<ClientViewModel>>(RebuildClientViewModels, clientStatus);
            await Tasks.Task.WhenAll(refreshJobsTask, refreshClientsTask);

            this.Jobs = new ObservableCollection<JobViewModel>(refreshJobsTask.Result);
            this.Clients = new ObservableCollection<ClientViewModel>(refreshClientsTask.Result);

            OnRefreshSelectedService();
        }
        #endregion // Controller Methods

        #region Abstract Methods
        /// <summary>
        /// Called when services are refreshed.
        /// </summary>
        protected abstract void OnRefreshServices();

        /// <summary>
        /// Called when selected service jobs and clients are refreshed.
        /// </summary>
        protected abstract void OnRefreshSelectedService();
        #endregion // Abstract Methods
        
        #region Private Methods
        /// <summary>
        /// Reload Automation Service config from disk.
        /// </summary>
        private void ReloadAutomationConfig()
        {
            lock (_automationLock)
                this._automationServicesConfig = ConfigFactory.CreateAutomationServiceConfig(_config.Project);
        }

        /// <summary>
        /// Rebuild Project View-Model objects (from config data).
        /// </summary>
        /// <returns></returns>
        private IEnumerable<ProjectViewModel> RebuildProjectViewModels()
        {
#warning DHM FIX ME: with IConfig2 this will be expanded for multiple-ProjectViewModels.

            List<ProjectViewModel> projectVMs = new List<ProjectViewModel>();
            lock (_automationLock)
            {
                projectVMs.Add(new ProjectViewModel(this._config.Project, true, 
                    this._automationServicesConfig));
            }
            return (projectVMs);
        }
        
        /// <summary>
        /// Rebuild Job View-Model objects (from model TaskStatus objects).
        /// </summary>
        /// <param name="parameter">TaskStatus object</param>
        /// <returns></returns>
        private IEnumerable<JobViewModel> RebuildJobViewModels(Object parameter)
        {
            if (null == parameter)
                return (new JobViewModel[0]);

            Debug.Assert(parameter is TaskStatus);
            TaskStatus taskStatus = (TaskStatus)parameter;

            List<JobViewModel> jobVMs = new List<JobViewModel>();
            Uri p4web = new Uri(String.Format("http://{0}", _config.Studios.ThisStudio.PerforceWebServer));
            foreach (IJob job in taskStatus.Jobs)
            {
                IAutomationServiceConfig config = this._selectedAutomationService.AutomationService;
                jobVMs.Add(new JobViewModel(config, job, p4web, this._bugstarUsers));
            }
            return (jobVMs);
        }

        /// <summary>
        /// Rebuild Client View-Model objects (from model WorkerStatus objects).
        /// </summary>
        /// <param name="parameter">WorkerStatus object</param>
        /// <returns></returns>
        private static IEnumerable<ClientViewModel> RebuildClientViewModels(Object parameter)
        {
            if (null == parameter)
                return (new ClientViewModel[0]);

            Debug.Assert(parameter is IEnumerable<WorkerStatus>);
            IEnumerable<WorkerStatus> clientStatus = (IEnumerable<WorkerStatus>)parameter;

            List<ClientViewModel> clientVMs = new List<ClientViewModel>();
            foreach (WorkerStatus worker in clientStatus)
            {
                clientVMs.Add(new ClientViewModel(worker));
            }
            return (clientVMs);
        }

        /// <summary>
        /// Initialise Command Definition objects.
        /// </summary>
        private void InitCommandDefinitions()
        {
            if (null == _abortJobDefinition)
            {
                _abortJobDefinition = new AbortJobDefinition(this);
                RockstarCommandManager.AddCommandDefinition(_abortJobDefinition);
            }
            if (null == _downloadJobLogDefinition)
            {
                _downloadJobLogDefinition = new DownloadJobLogDefinition(this);
                RockstarCommandManager.AddCommandDefinition(_downloadJobLogDefinition);
            }
            if (null == _downloadJobOutputDefinition)
            {
                _downloadJobOutputDefinition = new DownloadJobOutputDefinition(this);
                RockstarCommandManager.AddCommandDefinition(_downloadJobOutputDefinition);
            }
            if (null == _prioritiseJobDefinition)
            {
                _prioritiseJobDefinition = new PrioritiseJobDefinition(this);
                RockstarCommandManager.AddCommandDefinition(_prioritiseJobDefinition);
            }
        }

        /// <summary>
        /// Add commands.
        /// </summary>
        private void AddCommands()
        {
            // Job Context-Menu
            RockstarCommandManager.AddCommandBarItem(
                new DownloadJobOutputInstance(AutomationMonitorCommandIds.JobContextMenu,
                    new Guid("6FA2EDB3-081C-440C-B1FC-962264860FFF")));
            RockstarCommandManager.AddCommandBarItem(
                new DownloadJobLogInstance(AutomationMonitorCommandIds.JobContextMenu,
                    new Guid("3231EA7F-54D8-4834-881F-A288122B1237")));
            RockstarCommandManager.AddCommandBarItem(
                new PrioritiseJobInstance(AutomationMonitorCommandIds.JobContextMenu,
                    new Guid("5733BF5E-A527-49B5-901D-92A5BE50BED5")));
            RockstarCommandManager.AddCommandBarItem(
                new AbortJobInstance(AutomationMonitorCommandIds.JobContextMenu,
                    new Guid("3F7D5405-D18C-4923-B298-6890865CE174")));
        }

        /// <summary>
        /// Refresh Command Parameters notification; for selected job changes.
        /// </summary>
        private void RefreshCommandParameters()
        {
            if (null != _abortJobDefinition)
                _abortJobDefinition.RefreshCommandParameter();
            if (null != _downloadJobLogDefinition)
                _downloadJobLogDefinition.RefreshCommandParameter();
            if (null != _downloadJobOutputDefinition)
                _downloadJobOutputDefinition.RefreshCommandParameter();
            if (null != _prioritiseJobDefinition)
                _prioritiseJobDefinition.RefreshCommandParameter();
        }
        #endregion // Private Methods
    }

} // RSG.Editor.Controls.AutomationMonitor namespace
