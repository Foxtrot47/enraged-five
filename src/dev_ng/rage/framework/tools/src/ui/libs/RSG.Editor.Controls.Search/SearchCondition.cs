﻿//---------------------------------------------------------------------------------------------
// <copyright file="SearchCondition.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Search
{
    /// <summary>
    /// Defines all of the different conditions that can be associated with a search.
    /// </summary>
    public enum SearchCondition
    {
        /// <summary>
        /// Searches for equality. Valid for all types.
        /// </summary>
        Equals,

        /// <summary>
        /// Searches for inequality. Valid for all types.
        /// </summary>
        NotEquals,

        /// <summary>
        /// Searches for a regex match. Valid for strings only.
        /// </summary>
        Matches,

        /// <summary>
        /// Searches for a string containing the search value. Valid for strings only.
        /// </summary>
        Contains,

        /// <summary>
        /// Searches for a string starting with the search value. Valid for strings only.
        /// </summary>
        StartsWith,

        /// <summary>
        /// Searches for a string ending with the search value. Valid for strings only.
        /// </summary>
        EndsWith,

        /// <summary>
        /// Searches for a value greater than the search value. Valid for numerical
        /// searches only.
        /// </summary>
        GreaterThan,

        /// <summary>
        /// Searches for a value less than the search value. Valid for numerical searches only.
        /// </summary>
        LessThan,

        /// <summary>
        /// Searches for a value greater than or equal to the search value. Valid for
        /// numerical searches only.
        /// </summary>
        GreaterThanOrEqualTo,

        /// <summary>
        /// Searches for a value less than or equal to the search value. Valid for numerical
        /// searches only.
        /// </summary>
        LessThanOrEqualTo,
    } // RSG.Editor.Controls.Search.SearchCondition {Enum}
} // RSG.Editor.Controls.Search {Namespace}
