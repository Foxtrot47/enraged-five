﻿//---------------------------------------------------------------------------------------------
// <copyright file="RsDocumentPane.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Dock.Document
{
    using System;
    using System.Collections;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Data;
    using System.Windows.Input;
    using System.Windows.Threading;
    using RSG.Editor.Controls.AttachedDependencyProperties;
    using RSG.Editor.Controls.Dock.Commands;
    using RSG.Editor.Controls.Dock.ViewModel;
    using RSG.Editor.Controls.Helpers;
    using RSG.Editor.SharedCommands;

    /// <summary>
    /// Represents a control that contains <see cref="RsDocumentItem"/> controls that share the
    /// same screen space.
    /// </summary>
    [TemplatePart(Name = "PART_TabPanel", Type = typeof(RsDocumentTabPanel))]
    [TemplatePart(Name = "PART_SelectedContentHost", Type = typeof(ContentPresenter))]
    public class RsDocumentPane : TabControl
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="SelectedContainer"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty SelectedContainerProperty;

        /// <summary>
        /// The private field used for the <see cref="DocumentPanel"/> property.
        /// </summary>
        private RsDocumentTabPanel _panel;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="RsDocumentPane"/> class.
        /// </summary>
        static RsDocumentPane()
        {
            SelectedContainerProperty =
                DependencyProperty.Register(
                "SelectedContainer",
                typeof(RsDocumentItem),
                typeof(RsDocumentPane),
                new PropertyMetadata(null));

            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(RsDocumentPane),
                new FrameworkPropertyMetadata(typeof(RsDocumentPane)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RsDocumentPane"/> class.
        /// </summary>
        public RsDocumentPane()
        {
            this.CommandBindings.Add(
                new CommandBinding(
                    DockingCommands.ShiftLeft, this.OnShiftLeft, this.CanShiftLeft));
            this.CommandBindings.Add(
                new CommandBinding(
                    DockingCommands.ShiftRight, this.OnShiftRight, this.CanShiftRight));
            this.CommandBindings.Add(
                new CommandBinding(
                    DockingCommands.MoveItemLeft, this.OnMoveItemLeft, this.CanMoveItemLeft));
            this.CommandBindings.Add(
                new CommandBinding(
                    DockingCommands.MoveItemRight,
                    this.OnMoveItemRight,
                    this.CanMoveItemRight));

            ICommandAction action = new UndoDocumentAction(this.ActiveDocumentActionArgs);
            action.AddBinding(RockstarCommands.Undo, this);

            action = new RedoDocumentAction(this.ActiveDocumentActionArgs);
            action.AddBinding(RockstarCommands.Redo, this);

            action = new CopyFullPathAction(this.ActiveDocumentActionArgs);
            action.AddBinding(DockingCommands.CopyFullPath, this);

            action = new OpenContainingFolderAction(this.ActiveDocumentActionArgs);
            action.AddBinding(DockingCommands.OpenContainingFolder, this);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the currently selected container.
        /// </summary>
        public RsDocumentItem SelectedContainer
        {
            get { return (RsDocumentItem)this.GetValue(SelectedContainerProperty); }
            set { this.SetValue(SelectedContainerProperty, value); }
        }

        /// <summary>
        /// Gets the reference to the PART_TabPanel control that has been defined inside the
        /// control template.
        /// </summary>
        internal RsDocumentTabPanel DocumentPanel
        {
            get { return this._panel; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Gets called whenever the internal processes set the control template for
        /// this control.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            this._panel = this.GetTemplateChild("PART_TabPanel") as RsDocumentTabPanel;
        }

        /// <summary>
        /// Creates or identifies the element that is used to display the given item.
        /// </summary>
        /// <returns>
        /// The element that is used to display the given item.
        /// </returns>
        protected override DependencyObject GetContainerForItemOverride()
        {
            return new RsDocumentItem();
        }

        /// <summary>
        /// Determines if the specified item is (or is eligible to be) its own container.
        /// </summary>
        /// <param name="item">
        /// The item to check.
        /// </param>
        /// <returns>
        /// True if the item is (or is eligible to be) its own container; otherwise, false.
        /// </returns>
        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is RsDocumentItem;
        }

        /// <summary>
        /// Gets called whenever the selected item for this tab control changes.
        /// </summary>
        /// <param name="e">
        /// The System.Windows.Controls.SelectionChangedEventArgs data for the event.
        /// </param>
        protected override void OnSelectionChanged(SelectionChangedEventArgs e)
        {
            base.OnSelectionChanged(e);
            if (this._panel != null)
            {
                this._panel.EnsureSelectionIsVisible();
            }
        }

        /// <summary>
        /// The action resolver that returns the active document.
        /// </summary>
        /// <param name="commandData">
        /// The command data sent with the executing command.
        /// </param>
        /// <returns>
        /// The currently active document inside this pane.
        /// </returns>
        private DocumentItem ActiveDocumentActionArgs(CommandData commandData)
        {
            DocumentItem item = this.SelectedItem as DocumentItem;
            if (item == null)
            {
                RsDocumentItem container = this.SelectedItem as RsDocumentItem;
                if (container != null)
                {
                    item = container.DataContext as DocumentItem;
                }
            }

            return item;
        }

        /// <summary>
        /// Determines whether the <see cref="DockingCommands.ShiftRight"/> routed UI command
        /// can be executed.
        /// </summary>
        /// <param name="s">
        /// The object where the event handler is attached.
        /// </param>
        /// <param name="args">
        /// The System.Windows.Input.CanExecuteRoutedEventArgs data for the event.
        /// </param>
        private void CanMoveItemLeft(object s, CanExecuteRoutedEventArgs args)
        {
            RsDocumentItem item = args.OriginalSource as RsDocumentItem;
            if (item == null || this.Items.IndexOf(item) == 0)
            {
                args.CanExecute = false;
                return;
            }

            args.CanExecute = true;
        }

        /// <summary>
        /// Determines whether the <see cref="DockingCommands.ShiftRight"/> routed UI command
        /// can be executed.
        /// </summary>
        /// <param name="s">
        /// The object where the event handler is attached.
        /// </param>
        /// <param name="args">
        /// The System.Windows.Input.CanExecuteRoutedEventArgs data for the event.
        /// </param>
        private void CanMoveItemRight(object s, CanExecuteRoutedEventArgs args)
        {
            RsDocumentItem item = args.OriginalSource as RsDocumentItem;
            int index = this.Items.IndexOf(item);
            if (item == null || index == this.Items.Count - 1)
            {
                args.CanExecute = false;
                return;
            }

            args.CanExecute = true;
        }

        /// <summary>
        /// Determines whether the <see cref="DockingCommands.ShiftLeft"/> routed UI command
        /// can be executed.
        /// </summary>
        /// <param name="s">
        /// The object where the event handler is attached.
        /// </param>
        /// <param name="args">
        /// The System.Windows.Input.CanExecuteRoutedEventArgs data for the event.
        /// </param>
        private void CanShiftLeft(object s, CanExecuteRoutedEventArgs args)
        {
            if (this._panel == null || !this._panel.HasHiddenItemsAtStart)
            {
                args.CanExecute = false;
                return;
            }

            args.CanExecute = true;
        }

        /// <summary>
        /// Determines whether the <see cref="DockingCommands.ShiftRight"/> routed UI command
        /// can be executed.
        /// </summary>
        /// <param name="s">
        /// The object where the event handler is attached.
        /// </param>
        /// <param name="args">
        /// The System.Windows.Input.CanExecuteRoutedEventArgs data for the event.
        /// </param>
        private void CanShiftRight(object s, CanExecuteRoutedEventArgs args)
        {
            if (this._panel == null || !this._panel.HasHiddenItemsAtEnd)
            {
                args.CanExecute = false;
                return;
            }

            args.CanExecute = true;
        }

        /// <summary>
        /// Gets the tab item that is currently selected.
        /// </summary>
        /// <returns>
        /// The tab item that is currently selected.
        /// </returns>
        private TabItem GetSelectedTabItem()
        {
            object selected = this.SelectedItem;
            int index = this.SelectedIndex;
            if (selected == null)
            {
                return null;
            }

            TabItem tabItem = selected as TabItem;
            if (tabItem != null)
            {
                return tabItem;
            }

            tabItem = this.ItemContainerGenerator.ContainerFromIndex(index) as TabItem;
            object item = this.ItemContainerGenerator.ItemFromContainer(tabItem);
            if (tabItem == null || !Object.Equals(selected, item))
            {
                tabItem = this.ItemContainerGenerator.ContainerFromItem(selected) as TabItem;
            }

            return tabItem;
        }

        /// <summary>
        /// Get called once the <see cref="DockingCommands.CloseItem"/> routed UI command gets
        /// executed.
        /// </summary>
        /// <param name="s">
        /// The object where the event handler is attached.
        /// </param>
        /// <param name="args">
        /// The System.Windows.Input.ExecutedRoutedEventArgs data for the event.
        /// </param>
        private void OnCloseItem(object s, ExecutedRoutedEventArgs args)
        {
            RsDocumentItem item = args.OriginalSource as RsDocumentItem;
            if (item == null)
            {
                return;
            }

            CancelEventArgs e = new CancelEventArgs();
            item.FireClosingEvent(e);
            if (e.Cancel)
            {
                return;
            }

            IDockingPaneItem paneItem = item.DataContext as IDockingPaneItem;
            if (paneItem != null)
            {
                paneItem.OnClosing(e);
            }

            if (e.Cancel)
            {
                return;
            }

            int itemCount = this.Items.Count;
            if (this.ItemsSource == null)
            {
                this.Items.Remove(item);
            }
            else
            {
                if (paneItem == null)
                {
                    return;
                }

                IDockingParent dockingParent = this.DataContext as IDockingParent;
                if (dockingParent != null)
                {
                    dockingParent.RemoveItem(paneItem);
                }
            }

            if (this.Items.Count < itemCount)
            {
                item.FireClosedEvent();
            }
        }

        /// <summary>
        /// Get called once the <see cref="DockingCommands.ShiftRight"/> routed UI command gets
        /// executed.
        /// </summary>
        /// <param name="s">
        /// The object where the event handler is attached.
        /// </param>
        /// <param name="args">
        /// The System.Windows.Input.ExecutedRoutedEventArgs data for the event.
        /// </param>
        private void OnMoveItemLeft(object s, ExecutedRoutedEventArgs args)
        {
            RsDocumentItem item = args.OriginalSource as RsDocumentItem;
            if (item == null)
            {
                return;
            }

            if (this.ItemsSource == null)
            {
                int index = this.Items.IndexOf(item);
                if (index == -1 || index == 0)
                {
                    return;
                }

                object removedItem = this.Items[index - 1];
                this.Items.RemoveAt(index - 1);
                this.Items.Insert(index, removedItem);
            }
            else
            {
                int index = this.Items.IndexOf(item.DataContext);
                if (index == -1 || index == 0)
                {
                    return;
                }

                IDockingParent dockingParent = this.DataContext as IDockingParent;
                if (dockingParent != null)
                {
                    dockingParent.MoveItemLeft(index);
                }
            }
        }

        /// <summary>
        /// Get called once the <see cref="DockingCommands.ShiftRight"/> routed UI command gets
        /// executed.
        /// </summary>
        /// <param name="s">
        /// The object where the event handler is attached.
        /// </param>
        /// <param name="args">
        /// The System.Windows.Input.ExecutedRoutedEventArgs data for the event.
        /// </param>
        private void OnMoveItemRight(object s, ExecutedRoutedEventArgs args)
        {
            RsDocumentItem item = args.OriginalSource as RsDocumentItem;
            if (item == null)
            {
                return;
            }

            if (this.ItemsSource == null)
            {
                int index = this.Items.IndexOf(item);
                if (index == -1 || index == this.Items.Count - 1)
                {
                    return;
                }

                object removedItem = this.Items[index + 1];
                this.Items.RemoveAt(index + 1);
                this.Items.Insert(index, removedItem);
            }
            else
            {
                int index = this.Items.IndexOf(item.DataContext);
                if (index == -1 || index == this.Items.Count - 1)
                {
                    return;
                }

                IDockingParent dockingParent = this.DataContext as IDockingParent;
                if (dockingParent != null)
                {
                    dockingParent.MoveItemRight(index);
                }
            }
        }

        /// <summary>
        /// Get called once the <see cref="DockingCommands.ShiftLeft"/> routed UI command gets
        /// executed.
        /// </summary>
        /// <param name="s">
        /// The object where the event handler is attached.
        /// </param>
        /// <param name="args">
        /// The System.Windows.Input.ExecutedRoutedEventArgs data for the event.
        /// </param>
        private void OnShiftLeft(object s, ExecutedRoutedEventArgs args)
        {
            if (this._panel == null || !this._panel.HasHiddenItemsAtStart)
            {
                return;
            }

            this._panel.FirstVisibleTab = this._panel.FirstVisibleTab - 1;
            this._panel.StartPreventerTimer();
            this._panel.InvalidateMeasure();
        }

        /// <summary>
        /// Get called once the <see cref="DockingCommands.ShiftRight"/> routed UI command gets
        /// executed.
        /// </summary>
        /// <param name="s">
        /// The object where the event handler is attached.
        /// </param>
        /// <param name="args">
        /// The System.Windows.Input.ExecutedRoutedEventArgs data for the event.
        /// </param>
        private void OnShiftRight(object s, ExecutedRoutedEventArgs args)
        {
            if (this._panel == null || !this._panel.HasHiddenItemsAtEnd)
            {
                return;
            }

            this._panel.FirstVisibleTab = this._panel.FirstVisibleTab + 1;
            this._panel.StartPreventerTimer();
            this._panel.InvalidateMeasure();
        }
        #endregion Methods
    } // RSG.Editor.Controls.Dock.Document.RsDocumentPane {Class}
} // RSG.Editor.Controls.Dock.Document {Namespace}
