﻿//---------------------------------------------------------------------------------------------
// <copyright file="MultiCommandParameterConverter.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.Controls.Converters
{
    using System.Globalization;

    /// <summary>
    /// Converters a <see cref="IMultiCommandItem"/> object along with its raw command
    /// parameter and toggle state to its full command parameter, optionally inverting the
    /// toggle value.
    /// </summary>
    public class MultiCommandParameterConverter :
        ValueConverter<IMultiCommandItem, object, bool, IMultiCommandParameter>
    {
        #region Methods
        /// <summary>
        /// Converters the three specified source values to a single target value.
        /// </summary>
        /// <param name="value1">
        /// The first source value.
        /// </param>
        /// <param name="value2">
        /// The second source value.
        /// </param>
        /// <param name="value3">
        /// The third source value.
        /// </param>
        /// <param name="param">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A target value created by converting the two specified source values.
        /// </returns>
        protected override IMultiCommandParameter Convert(
            IMultiCommandItem value1,
            object value2,
            bool value3,
            object param,
            CultureInfo culture)
        {
            bool invertToggle = false;
            bool? paramValue = param as bool?;
            if (paramValue != null && paramValue.HasValue)
            {
                invertToggle = paramValue.Value;
            }

            IMultiCommandParameter commandParameter = value1.GetCommandParameter();
            if (invertToggle)
            {
                commandParameter.IsToggled = !commandParameter.IsToggled;
            }

            return commandParameter;
        }
        #endregion Methods
    } // RSG.Editor.Controls.Converters.MultiCommandParameterConverter {Class}
} // RSG.Editor.Controls.Converters {Namespace}
