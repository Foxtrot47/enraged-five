﻿//---------------------------------------------------------------------------------------------
// <copyright file="StableEnumerable.cs" company="Rockstar">
//     Copyright © Rockstar Games 2012. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor.View
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;

    /// <summary>
    /// Represents a IEnumerable that can handle changes being made to it while moving
    /// through the items.
    /// </summary>
    public class StableEnumerable : DisposableObject, IEnumerable
    {
        #region Fields
        /// <summary>
        /// The private list containing the stable items that are iterated through.
        /// </summary>
        private readonly IList _stableList;

        /// <summary>
        /// The private field used for the <see cref="DetectedChange"/> property.
        /// </summary>
        private bool _detectedChange;

        /// <summary>
        /// The private field used for the <see cref="Source"/> property.
        /// </summary>
        private INotifyCollectionChanged _source;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="StableEnumerable"/> class.
        /// </summary>
        /// <param name="source">
        /// The source collection to iterate through.
        /// </param>
        public StableEnumerable(IEnumerable source)
        {
            this._stableList = new List<object>(source.Cast<object>());
            this.Source = source as INotifyCollectionChanged;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets a value indicating whether the collection has changed during iteration.
        /// </summary>
        public bool DetectedChange
        {
            get { return this._detectedChange; }
        }

        /// <summary>
        /// Gets or sets the private reference to the source collection cast to a
        /// INotifyCollectionChanged object.
        /// </summary>
        private INotifyCollectionChanged Source
        {
            get
            {
                return this._source;
            }

            set
            {
                if (this._source != value)
                {
                    if (this._source != null)
                    {
                        this._source.CollectionChanged -= this.OnCollectionChanged;
                    }

                    this._source = value;
                    if (this._source != null)
                    {
                        this._source.CollectionChanged += this.OnCollectionChanged;
                    }
                }
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Retrieves an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// An enumerator that iterates through the collection.
        /// </returns>
        public IEnumerator GetEnumerator()
        {
            return this._stableList.GetEnumerator();
        }

        /// <summary>
        /// Disposes of the managed resources.
        /// </summary>
        protected override void DisposeManagedResources()
        {
            this.Source = null;
        }

        /// <summary>
        /// Called whenever the collection changes.
        /// </summary>
        /// <param name="sender">
        /// The collection that has changed.
        /// </param>
        /// <param name="e">
        /// The System.EventArgs data for the event.
        /// </param>
        private void OnCollectionChanged(object sender, EventArgs e)
        {
            this._detectedChange = true;
            this.Source = null;
        }
        #endregion
    } // RSG.Editor.View.StableEnumerable {Class}
} // RSG.Editor.View {Namespace}
