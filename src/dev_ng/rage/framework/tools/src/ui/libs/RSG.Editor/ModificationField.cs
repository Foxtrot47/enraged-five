﻿//---------------------------------------------------------------------------------------------
// <copyright file="ModificationField.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Editor
{
    using System.Xml.Serialization;
    using RSG.Base.Attributes;

    /// <summary>
    /// Defines all the different fields that can be modified for a command bar item.
    /// </summary>
    public enum ModificationField
    {
        /// <summary>
        /// Defines a modification made to the are multi items shown property of a command bar
        /// item.
        /// </summary>
        [XmlAttributeName("aremultiitemsshown")]
        AreMultiItemsShown,

        /// <summary>
        /// Defines a modification made to the display style property of a command bar item.
        /// </summary>
        [XmlAttributeName("style")]
        DisplayStyle,

        /// <summary>
        /// Defines a modification made to the each item starts group property of a command bar
        /// item.
        /// </summary>
        [XmlAttributeName("eachitemstartsgroup")]
        EachItemStartsGroup,

        /// <summary>
        /// Defines a modification made to the priority property of a command bar item.
        /// </summary>
        [XmlAttributeName("priority")]
        Priority,

        /// <summary>
        /// Defines a modification made to the show filter count property of a command bar
        /// item.
        /// </summary>
        [XmlAttributeName("showfiltercount")]
        ShowFilterCount,

        /// <summary>
        /// Defines a modification made to the starts group property of a command bar item.
        /// </summary>
        [XmlAttributeName("startsgroup")]
        StartsGroup,

        /// <summary>
        /// Defines a modification made to the text property of a command bar item.
        /// </summary>
        [XmlAttributeName("text")]
        Text,

        /// <summary>
        /// Defines a modification made to the is visible property of a command bar item.
        /// </summary>
        [XmlAttributeName("visible")]
        Visibility,

        /// <summary>
        /// Defines a modification made to the width property of a command bar item.
        /// </summary>
        [XmlAttributeName("width")]
        Width,
    } // RSG.Editor.ModificationField {Enum}
} // RSG.Editor {Namespace}
