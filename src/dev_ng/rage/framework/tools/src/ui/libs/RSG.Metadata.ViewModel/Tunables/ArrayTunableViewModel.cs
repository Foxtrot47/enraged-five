﻿//---------------------------------------------------------------------------------------------
// <copyright file="ArrayTunableViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Tunables
{
    using System.Collections.Specialized;
    using RSG.Base.Extensions;
    using RSG.Editor.View;
    using RSG.Metadata.Model.Definitions.Members;
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// The view model for a <see cref="ArrayTunable"/> model object.
    /// </summary>
    public class ArrayTunableViewModel
        : TunableViewModelBase<ArrayTunable>, ITunableViewModelParent
    {
        #region Fields
        /// <summary>
        /// A private value indicating whether the type value and the is expandable value have
        /// been initialised.
        /// </summary>
        private bool _initialised;

        /// <summary>
        /// The private field used for the <see cref="IsExpandable"/> property.
        /// </summary>
        private bool _isExpandable;

        /// <summary>
        /// The private field used for the <see cref="Tunables"/> property.
        /// </summary>
        private TunableCollection _tunables;

        /// <summary>
        /// The private field used for the <see cref="TypeValue"/> property.
        /// </summary>
        private string _typeValue;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ArrayTunableViewModel"/> class.
        /// </summary>
        /// <param name="tunable">
        /// The array tunable that this view model is representing.
        /// </param>
        /// <param name="parent">
        /// The parent tunable view model for this view model.
        /// </param>
        public ArrayTunableViewModel(ArrayTunable tunable, ITunableViewModel parent)
            : base(tunable, parent)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether this tunable can be expanded.
        /// </summary>
        public override bool IsExpandable
        {
            get
            {
                if (!this._initialised)
                {
                    this.Initialise();
                }

                return this._isExpandable;
            }
        }

        /// <summary>
        /// Gets the collection of child tunables for this structure.
        /// </summary>
        public IReadOnlyViewModelCollection<ITunableViewModel> Tunables
        {
            get
            {
                if (this._tunables == null)
                {
                    this._tunables = new TunableCollection(this.Model.Tunables, this);
                    this.AddCollectionHandlers();
                }

                return this._tunables;
            }
        }

        /// <summary>
        /// Gets the value of the type for this tunable view model that should be shown to the
        /// user.
        /// </summary>
        public override string TypeValue
        {
            get
            {
                if (!this._initialised)
                {
                    this.Initialise();
                }

                return this._typeValue;
            }
        }

        /// <summary>
        /// Gets the tunable at the specified item from within this tunables item collection if
        /// applicable.
        /// </summary>
        /// <param name="index">
        /// The zero-based index of the tunable to get.
        /// </param>
        /// <returns>
        /// The tunable at the specified index if applicable; otherwise, null.
        /// </returns>
        public override ITunableViewModel this[int index]
        {
            get { return this.Tunables[index]; }
        }

        /// <summary>
        /// Gets the tunable view model that belongs to this tunable that is currently wrapping
        /// the specified model.
        /// </summary>
        /// <param name="model">
        /// The model of the tunable view model to get.
        /// </param>
        /// <returns>
        /// The tunable view model that is wrapping the specified model if applicable;
        /// otherwise, null.
        /// </returns>
        public override ITunableViewModel this[ITunable model]
        {
            get
            {
                foreach (ITunableViewModel viewModel in this.Tunables)
                {
                    if (object.ReferenceEquals(viewModel.Model, model))
                    {
                        return viewModel;
                    }
                }

                return null;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Resets this tunable to have its default value. (i.e no source data).
        /// </summary>
        public override void ResetValueToDefault()
        {
            this.RemoveCollectionHandlers();
            base.ResetValueToDefault();
            this.AddCollectionHandlers();
            this.UpdateTypeValue();
            this.UpdateIsExpandable();
        }

        /// <summary>
        /// Adds the collection changed handlers from the tunables collection.
        /// </summary>
        private void AddCollectionHandlers()
        {
            if (this._tunables == null)
            {
                return;
            }

            INotifyCollectionChanged collection = this._tunables as INotifyCollectionChanged;
            if (collection != null)
            {
                collection.CollectionChanged += this.OnItemsChanged;
            }
        }

        /// <summary>
        /// Initialises the type value and is expandable value for the first time.
        /// </summary>
        private void Initialise()
        {
            if (this._initialised)
            {
                return;
            }

            this._initialised = true;
            this.UpdateTypeValue();
            this.UpdateIsExpandable();
        }

        /// <summary>
        /// Called whenever the items inside the tunable collection change.
        /// </summary>
        /// <param name="sender">
        /// The collection this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Collections.Specialized.NotifyCollectionChangedEventArgs containing the
        /// event data including the type of change that was made.
        /// </param>
        private void OnItemsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            this.UpdateTypeValue();
            this.UpdateIsExpandable();
        }

        /// <summary>
        /// Removes the collection changed handlers from the tunables collection.
        /// </summary>
        private void RemoveCollectionHandlers()
        {
            if (this._tunables == null)
            {
                return;
            }

            INotifyCollectionChanged collection = this._tunables as INotifyCollectionChanged;
            if (collection != null)
            {
                collection.CollectionChanged += this.OnItemsChanged;
            }
        }

        /// <summary>
        /// Updates the is expandable value for this tunable.
        /// </summary>
        private void UpdateIsExpandable()
        {
            bool value = this.Tunables.Count > 0;
            this.SetProperty(ref this._isExpandable, value, "IsExpandable");
        }

        /// <summary>
        /// Updates the type value property for this array based on the current number of items
        /// and array type.
        /// </summary>
        private void UpdateTypeValue()
        {
            string count = this.Tunables.Count.ToStringInvariant();
            IMember member = this.Model.ArrayMember.ElementType;
            string typeName = member.TypeName;
            PointerMember pointerMember = member as PointerMember;
            if (pointerMember != null && pointerMember.ReferencedStructure != null)
            {
                typeName = pointerMember.ReferencedStructure.ShortDataType + "*";
            }
            else
            {
                StructMember structMember = member as StructMember;
                if (structMember != null)
                {
                    typeName = structMember.ReferencedStructure.ShortDataType;
                }
            }

            string value = "{0}[{1}]".FormatCurrent(typeName, count);
            this.SetProperty(ref this._typeValue, value, "TypeValue");
        }
        #endregion Methods
    } // RSG.Metadata.ViewModel.Tunables.ArrayTunableViewModel {Class}
} // RSG.Metadata.ViewModel.Tunables {Namespace}
