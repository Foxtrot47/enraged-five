﻿//---------------------------------------------------------------------------------------------
// <copyright file="Vector3TunableViewModel.cs" company="Rockstar">
//     Copyright © Rockstar Games 3014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.ViewModel.Tunables
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Media;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Tunables;

    /// <summary>
    /// The view model for a <see cref="Vector3Tunable"/> model object.
    /// </summary>
    public class Vector3TunableViewModel : TunableViewModelBase<Vector3Tunable>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Vector3TunableViewModel"/> class.
        /// </summary>
        /// <param name="tunable">
        /// The 3d vector tunable that this view model is representing.
        /// </param>
        /// <param name="parent">
        /// The parent tunable view model for this view model.
        /// </param>
        public Vector3TunableViewModel(Vector3Tunable tunable, ITunableViewModel parent)
            : base(tunable, parent)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the incremental step for the X parameter.
        /// </summary>
        public float StepX
        {
            get { return this.Model.Vector3Member.Step[0]; }
        }

        /// <summary>
        /// Gets the incremental step for the Y parameter.
        /// </summary>
        public float StepY
        {
            get { return this.Model.Vector3Member.Step[1]; }
        }

        /// <summary>
        /// Gets the incremental step for the Z parameter.
        /// </summary>
        public float StepZ
        {
            get { return this.Model.Vector3Member.Step[2]; }
        }

        /// <summary>
        /// Gets or sets the value of the x component for this vector.
        /// </summary>
        public float X
        {
            get { return this.Model.X; }
            set { this.Model.X = value; }
        }

        /// <summary>
        /// Gets or sets the value of the y component for this vector.
        /// </summary>
        public float Y
        {
            get { return this.Model.Y; }
            set { this.Model.Y = value; }
        }

        /// <summary>
        /// Gets or sets the value of the z component for this vector.
        /// </summary>
        public float Z
        {
            get { return this.Model.Z; }
            set { this.Model.Z = value; }
        }

        /// <summary>
        /// Gets or sets the colour value represented by the three vector components.
        /// </summary>
        public Color Colour
        {
            get
            {
                byte red = (byte)(this.Model.X * 255.0f);
                byte green = (byte)(this.Model.Y * 255.0f);
                byte blue = (byte)(this.Model.Z * 255.0f);

                return Color.FromArgb(255, red, green, blue);
            }

            set
            {
                using (new UndoRedoBatch(this.Model.UndoEngine))
                {
                    this.Model.X = (float)value.R / 255.0f;
                    this.Model.Y = (float)value.G / 255.0f;
                    this.Model.Z = (float)value.B / 255.0f;
                }
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Makes sure if the any of the 3 x,y, and z components change on the model the view
        /// models colour property will also be signalled to change.
        /// </summary>
        /// <param name="propertyName">
        /// The original name of the property.
        /// </param>
        /// <returns>
        /// The given property name with Colour attached to it.
        /// </returns>
        protected override string ResolvePropertyNameOnChange(string propertyName)
        {
            if (propertyName == "X" || propertyName == "Y" || propertyName == "Z")
            {
                return propertyName + ",Colour";
            }

            return base.ResolvePropertyNameOnChange(propertyName);
        }

        /// <summary>
        /// Determines whether the validation pipeline should be started due to the property
        /// with the specified name changing.
        /// </summary>
        /// <param name="propertyName">
        /// The property name to test.
        /// </param>
        /// <returns>
        /// True if the validation should be run due to the specified property changing;
        /// otherwise, false.
        /// </returns>
        protected override bool ShouldRunValidation(string propertyName)
        {
            HashSet<string> propertyNames = new HashSet<string>()
            {
                "X",
                "Y",
                "Z"
            };

            if (propertyNames.Contains(propertyName))
            {
                return true;
            }

            return base.ShouldRunValidation(propertyName);
        }
        #endregion Methods
    } // RSG.Metadata.ViewModel.Tunables.Vector3TunableViewModel {Class}
} // RSG.Metadata.ViewModel.Tunables {Namespace}
