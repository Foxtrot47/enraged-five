﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DialogueStar.View
{
    /// <summary>
    /// Interaction logic for DialogueOptions.xaml
    /// </summary>
    public partial class DialogueOptions : UserControl
    {
        #region Constructor(s)

        /// <summary>
        /// Default constructor
        /// </summary>
        public DialogueOptions()
        {
            InitializeComponent();
        }

        #endregion // Constructor(s)

    } // DialogueOptions
} // DialogueStar.View
