﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using RSG.Base.Windows;
using RSG.Model.Dialogue;

namespace DialogueStar
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        #region Member Data

        /// <summary>
        /// The main controller instance that links the view to the model
        /// </summary>
        public static Controller Controller;

        #endregion // Member Data

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public App()
        {
            DialogueStar.Properties.Settings.Default.Upgrade();
        }

        #endregion // Constructor

        #region Event Handling

        /// <summary>
        /// Event fired when there has been an unhandled exception caught anywhere during the applications lifespan
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Application_DispatcherUnhandledException(Object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            ExceptionStackTraceDlg dlg = new ExceptionStackTraceDlg(e.Exception, "RSGEDI Tools", "*tools@rockstarnorth.com");
            dlg.ShowDialog();
        }

        /// <summary>
        /// Makes sure that the main controller that links the view to the model is initialised.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Application_Startup(Object sender, StartupEventArgs e)
        {
            Controller = new Controller();

            List<SearchAttribute> searchAttributes = SearchAttribute.CreateFromType(typeof(MissionDialogue));
            SortedDictionary<String, SearchAttribute> sortedSearchAttributes = new SortedDictionary<String, SearchAttribute>();
            foreach (SearchAttribute searchAttribute in searchAttributes)
            {
                sortedSearchAttributes.Add(searchAttribute.AttributeName, searchAttribute);
            }

            Controller.SearchAttributes = new ObservableCollection<SearchAttribute>();
            foreach (SearchAttribute searchAttribute in sortedSearchAttributes.Values)
            {
                Controller.SearchAttributes.Add(searchAttribute);
            }

            if (e.Args.Length > 0)
            {
                foreach (String filename in e.Args)
                {
                    if (System.IO.File.Exists(filename) && System.IO.Path.GetExtension(filename) == ".dstar")
                    {
                        App.Controller.OpenDialogue(filename);
                    }
                }
            }

            if (true)
                System.Windows.Media.RenderOptions.ProcessRenderMode = System.Windows.Interop.RenderMode.SoftwareOnly;

        }

        #endregion //Event Handling

    } // App
} // DialogueStar
