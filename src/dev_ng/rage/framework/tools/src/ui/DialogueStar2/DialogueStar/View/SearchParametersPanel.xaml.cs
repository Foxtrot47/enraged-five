﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DialogueStar;
namespace DialogueStar.View
{
    /// <summary>
    /// Interaction logic for SearchParametersPanel.xaml
    /// </summary>
    public partial class SearchParametersPanel : UserControl
    {
        #region Events

        /// <summary>
        /// This event gets fired when the user presses the Add button on this control.
        /// The event exists so that a parent control can react to this event.
        /// </summary>
        public event EventHandler AddPressed;

        /// <summary>
        /// This event gets fired when the user presses the Delete button on this control.
        /// The event exists so that a parent control can react to this event.
        /// </summary>
        public event EventHandler DeletePressed;

        /// <summary>
        /// This event gets fired whenever the validation is determined/changed this is so again, the 
        /// parent control can react to it.
        /// </summary>
        public event EventHandler ValidationChanged;

        #endregion

        #region Properties

        /// <summary>
        /// Property that represents the selected attribute that the user has selected
        /// </summary>
        public SearchAttribute SelectedSearchAttribute
        {
            get { return (SearchAttribute)GetValue(SelectedSearchAttributeProperty); }
            set { SetValue(SelectedSearchAttributeProperty, value); }
        }
        public static DependencyProperty SelectedSearchAttributeProperty = DependencyProperty.Register("SelectedSearchAttribute", typeof(SearchAttribute), typeof(SearchParametersPanel),
            new PropertyMetadata(new PropertyChangedCallback(Validate)));

        /// <summary>
        /// Property that represents the name of the selected attribute the user has selected.
        /// </summary>
        public String SearchField
        {
            get { return (String)GetValue(SearchFieldProperty); }
            set { SetValue(SearchFieldProperty, value); }
        }
        public static DependencyProperty SearchFieldProperty = DependencyProperty.Register("SearchField", typeof(String), typeof(SearchParametersPanel),
            new PropertyMetadata(new PropertyChangedCallback(Validate)));

        /// <summary>
        /// The comparison option the user has selected in this control
        /// </summary>
        public String SearchOption
        {
            get { return (String)GetValue(SearchOptionProperty); }
            set { SetValue(SearchOptionProperty, value); }
        }
        public static DependencyProperty SearchOptionProperty = DependencyProperty.Register("SearchOption", typeof(String), typeof(SearchParametersPanel),
            new PropertyMetadata(new PropertyChangedCallback(Validate)));

        /// <summary>
        /// The search text that the user has entered in the text box. The length of this needs to be greater than 0 to be valid.
        /// </summary>
        public String SearchText
        {
            get { return (String)GetValue(SearchTextProperty); }
            set { SetValue(SearchTextProperty, value); }
        }
        public static DependencyProperty SearchTextProperty = DependencyProperty.Register("SearchText", typeof(String), typeof(SearchParametersPanel),
            new PropertyMetadata(new PropertyChangedCallback(Validate)));

        /// <summary>
        /// The string representation of the numeric value entered into the numeric up/down control if you user is searching for a numeric 
        /// search
        /// </summary>
        public String NumericSearchText
        {
            get { return (String)GetValue(NumericSearchTextProperty); }
            set { SetValue(NumericSearchTextProperty, value); }
        }
        public static DependencyProperty NumericSearchTextProperty = DependencyProperty.Register("NumericSearchText", typeof(String), typeof(SearchParametersPanel),
            new PropertyMetadata(new PropertyChangedCallback(Validate)));

        /// <summary>
        /// Represents whether with the current options selected this contains valid search parameters
        /// </summary>
        public Boolean IsValid
        {
            get { return m_isValid; }
            set { m_isValid = value; }
        }
        private Boolean m_isValid;

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor
        /// </summary>
        public SearchParametersPanel()
        {
            InitializeComponent();
            this.m_isValid = false;
        }

        #endregion // Constructor(s)

        #region Static Function(s)

        /// <summary>
        /// Vaidates the set of search parameters in this control to see whether it would make a valid search. For a search
        /// to be valid it has to have a target property, a comparison option, and some search text, either text or numeric text.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void Validate(Object sender, DependencyPropertyChangedEventArgs e)
        {
            SearchParametersPanel thisPanel = (sender as SearchParametersPanel);

            thisPanel.IsValid = true;

            if (thisPanel.SelectedSearchAttribute == null)
            {
                thisPanel.IsValid = false;
                if (thisPanel.ValidationChanged != null)
                    thisPanel.ValidationChanged(thisPanel, new EventArgs());
                return;
            }
            if (String.IsNullOrEmpty(thisPanel.SearchField))
            {
                thisPanel.IsValid = false;
                if (thisPanel.ValidationChanged != null)
                    thisPanel.ValidationChanged(thisPanel, new EventArgs());
                return;
            }

            if (String.IsNullOrEmpty(thisPanel.SearchOption))
            {
                thisPanel.IsValid = false;
                if (thisPanel.ValidationChanged != null)
                    thisPanel.ValidationChanged(thisPanel, new EventArgs());
                return;
            }

            if (String.IsNullOrEmpty(thisPanel.SearchText) && thisPanel.SelectedSearchAttribute.SearchType != RSG.Model.Dialogue.Search.SearchType.Search_Numeric_Type)
            {
                thisPanel.IsValid = false;
                if (thisPanel.ValidationChanged != null)
                    thisPanel.ValidationChanged(thisPanel, new EventArgs());
                return;
            }

            if (String.IsNullOrEmpty(thisPanel.NumericSearchText) && thisPanel.SelectedSearchAttribute.SearchType == RSG.Model.Dialogue.Search.SearchType.Search_Numeric_Type)
            {
                thisPanel.IsValid = false;
                if (thisPanel.ValidationChanged != null)
                    thisPanel.ValidationChanged(thisPanel, new EventArgs());
                return;
            }

            if (thisPanel.ValidationChanged != null)
                thisPanel.ValidationChanged(thisPanel, new EventArgs());
            return;
        }

        #endregion // Static Function(s)

        #region Event Handlers

        /// <summary>
        /// This function is called whenever the user presses the Add button on this control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddButton_Click(Object sender, RoutedEventArgs e)
        {
            if (AddPressed != null)
                AddPressed(this, new EventArgs());
        }

        /// <summary>
        /// This function is called whenever the user presses the Delete button on this control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteButton_Click(Object sender, RoutedEventArgs e)
        {
            if (DeletePressed != null)
                DeletePressed(this, new EventArgs());
        }

        /// <summary>
        /// This function is called whenever the text in the search text box is changed, this is so we can make sure
        /// that the search options are set to either equal to ot not equal to if we are using wildcards.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchTextBox_TextChanged(Object sender, TextChangedEventArgs e)
        {
            if (SearchTextBox.Text.Contains('*') || SearchTextBox.Text.Contains('?'))
            {
                if ((String)SearchOptionCombo.SelectedValue != "Is equal to" && (String)SearchOptionCombo.SelectedValue != "Is not equal to")
                {
                    SearchOptionCombo.SelectedItem = "Is equal to";
                }
            }
        }

        #endregion // Event Handlers

    } // SearchParametersPanel
} // DialogueStar.View
