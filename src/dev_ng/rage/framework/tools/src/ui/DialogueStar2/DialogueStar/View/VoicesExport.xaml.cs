﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using RSG.Model.Dialogue;
using RSG.Base.Collections;

namespace DialogueStar.View
{
    /// <summary>
    /// Interaction logic for VoicesExport.xaml
    /// </summary>
    public partial class VoicesExport : Window, INotifyPropertyChanged
    {
        #region Property change events

        /// <summary>
        /// Property changed event fired when the any of the properties change
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Create the OnPropertyChanged method to raise the event
        /// </summary>
        /// <param name="name"></param>
        protected void OnPropertyChanged(String name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion // Property change events

        public System.Collections.ObjectModel.ObservableCollection<String> SelectedVoices
        {
            get { return (System.Collections.ObjectModel.ObservableCollection<String>)GetValue(SelectedVoicesProperty); }
            set { SetValue(SelectedVoicesProperty, value); }
        }
        private static DependencyProperty SelectedVoicesProperty = 
            DependencyProperty.Register("SelectedVoices",
            typeof(System.Collections.ObjectModel.ObservableCollection<String>), typeof(VoicesExport));

        public System.Collections.ObjectModel.ObservableCollection<String> UnselectedVoices
        {
            get { return (System.Collections.ObjectModel.ObservableCollection<String>)GetValue(UnSelectedVoicesProperty); }
            set { SetValue(UnSelectedVoicesProperty, value); }
        }
        private static DependencyProperty UnSelectedVoicesProperty = 
            DependencyProperty.Register("UnselectedVoices",
            typeof(System.Collections.ObjectModel.ObservableCollection<String>), typeof(VoicesExport));

        public VoicesExport()
        {
            this.SelectedVoices = new System.Collections.ObjectModel.ObservableCollection<String>();
            InitializeComponent();
        }

        /// <summary>
        /// The main cancel button: it closes the dialogue returning a false result
        /// so that the calling function can take the correct action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cancel_Click(Object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        /// <summary>
        /// The main save button: it closes the dialogue returning a true result
        /// so that the calling function can take the correct action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Export_Click(Object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void SelectVoice_Click(Object sender, RoutedEventArgs e)
        {
            if (this.UnselectedVoicesTree.SelectedItem is TreeViewItem)
            {
                foreach (String voice in this.UnselectedVoices)
                {
                    this.SelectedVoices.Add((String)voice);
                }
                this.UnselectedVoices.Clear();
            }

            if (this.UnselectedVoicesTree.SelectedItem is String)
            {
                String selectedVoice = (String)this.UnselectedVoicesTree.SelectedItem;

                this.UnselectedVoices.Remove(selectedVoice);
                this.SelectedVoices.Add(selectedVoice);
            }
        }

        private void UnselectVoice_Click(Object sender, RoutedEventArgs e)
        {
            if (this.SelectedVoicesTree.SelectedItem is TreeViewItem)
            {
                foreach (String voice in this.SelectedVoices)
                {
                    this.UnselectedVoices.Add(voice);
                }
                this.SelectedVoices.Clear();
            }

            if (this.SelectedVoicesTree.SelectedItem is String)
            {
                String selectedVoice = (String)this.SelectedVoicesTree.SelectedItem;

                this.SelectedVoices.Remove(selectedVoice);
                this.UnselectedVoices.Add(selectedVoice);
            }
        }


    } // VoicesExport
} // DialogueStar.View
