﻿#pragma checksum "..\..\..\..\View\Lines.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "4858FF4A642A113DB558EF32E8F9AC993FA6835BA1526E614D7E6FC187FDE4DD"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using DialogueStar;
using DialogueStar.View;
using RSG.Model.Dialogue;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace DialogueStar.View {
    
    
    /// <summary>
    /// Lines
    /// </summary>
    public partial class Lines : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector, System.Windows.Markup.IStyleConnector {
        
        
        #line 12 "..\..\..\..\View\Lines.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DialogueStar.View.Lines LinesControl;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\..\..\View\Lines.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button AddLine;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\..\..\View\Lines.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button InsertLine;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\..\..\View\Lines.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button DeleteLine;
        
        #line default
        #line hidden
        
        
        #line 68 "..\..\..\..\View\Lines.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button MagicFilenames;
        
        #line default
        #line hidden
        
        
        #line 154 "..\..\..\..\View\Lines.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox FilterBox;
        
        #line default
        #line hidden
        
        
        #line 181 "..\..\..\..\View\Lines.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox MassChangeComponent;
        
        #line default
        #line hidden
        
        
        #line 193 "..\..\..\..\View\Lines.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox MassChangeValue;
        
        #line default
        #line hidden
        
        
        #line 317 "..\..\..\..\View\Lines.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid LineGrid;
        
        #line default
        #line hidden
        
        
        #line 1134 "..\..\..\..\View\Lines.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.Popup DragPopup;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/DialogueStar;component/view/lines.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\View\Lines.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.LinesControl = ((DialogueStar.View.Lines)(target));
            
            #line 13 "..\..\..\..\View\Lines.xaml"
            this.LinesControl.Loaded += new System.Windows.RoutedEventHandler(this.LineUserControl_Loaded);
            
            #line default
            #line hidden
            
            #line 14 "..\..\..\..\View\Lines.xaml"
            this.LinesControl.Unloaded += new System.Windows.RoutedEventHandler(this.LineUserControl_UnLoaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.AddLine = ((System.Windows.Controls.Button)(target));
            
            #line 44 "..\..\..\..\View\Lines.xaml"
            this.AddLine.Click += new System.Windows.RoutedEventHandler(this.AddLine_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.InsertLine = ((System.Windows.Controls.Button)(target));
            
            #line 52 "..\..\..\..\View\Lines.xaml"
            this.InsertLine.Click += new System.Windows.RoutedEventHandler(this.InsertLine_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.DeleteLine = ((System.Windows.Controls.Button)(target));
            
            #line 62 "..\..\..\..\View\Lines.xaml"
            this.DeleteLine.Click += new System.Windows.RoutedEventHandler(this.DeleteLine_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.MagicFilenames = ((System.Windows.Controls.Button)(target));
            
            #line 70 "..\..\..\..\View\Lines.xaml"
            this.MagicFilenames.Click += new System.Windows.RoutedEventHandler(this.MagicFilenames_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.FilterBox = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 7:
            this.MassChangeComponent = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 8:
            this.MassChangeValue = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 9:
            
            #line 247 "..\..\..\..\View\Lines.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.MassChange_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.LineGrid = ((System.Windows.Controls.DataGrid)(target));
            
            #line 327 "..\..\..\..\View\Lines.xaml"
            this.LineGrid.DataContextChanged += new System.Windows.DependencyPropertyChangedEventHandler(this.LineGrid_DataContextChanged);
            
            #line default
            #line hidden
            
            #line 333 "..\..\..\..\View\Lines.xaml"
            this.LineGrid.PreviewKeyDown += new System.Windows.Input.KeyEventHandler(this.LineGrid_KeyDown_1);
            
            #line default
            #line hidden
            
            #line 334 "..\..\..\..\View\Lines.xaml"
            this.LineGrid.PreviewMouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(this.LineGrid_PreviewMouseLeftButtonUp);
            
            #line default
            #line hidden
            
            #line 335 "..\..\..\..\View\Lines.xaml"
            this.LineGrid.PreviewMouseMove += new System.Windows.Input.MouseEventHandler(this.LineGrid_MouseMove);
            
            #line default
            #line hidden
            
            #line 340 "..\..\..\..\View\Lines.xaml"
            this.LineGrid.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.LineGrid_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 11:
            
            #line 347 "..\..\..\..\View\Lines.xaml"
            ((System.Windows.Input.CommandBinding)(target)).CanExecute += new System.Windows.Input.CanExecuteRoutedEventHandler(this.NewCanExecute);
            
            #line default
            #line hidden
            
            #line 349 "..\..\..\..\View\Lines.xaml"
            ((System.Windows.Input.CommandBinding)(target)).Executed += new System.Windows.Input.ExecutedRoutedEventHandler(this.NewExecuted);
            
            #line default
            #line hidden
            return;
            case 12:
            
            #line 350 "..\..\..\..\View\Lines.xaml"
            ((System.Windows.Input.CommandBinding)(target)).CanExecute += new System.Windows.Input.CanExecuteRoutedEventHandler(this.CopyCanExecute);
            
            #line default
            #line hidden
            
            #line 352 "..\..\..\..\View\Lines.xaml"
            ((System.Windows.Input.CommandBinding)(target)).Executed += new System.Windows.Input.ExecutedRoutedEventHandler(this.CopyExecuted);
            
            #line default
            #line hidden
            return;
            case 13:
            
            #line 353 "..\..\..\..\View\Lines.xaml"
            ((System.Windows.Input.CommandBinding)(target)).CanExecute += new System.Windows.Input.CanExecuteRoutedEventHandler(this.PasteCanExecute);
            
            #line default
            #line hidden
            
            #line 355 "..\..\..\..\View\Lines.xaml"
            ((System.Windows.Input.CommandBinding)(target)).Executed += new System.Windows.Input.ExecutedRoutedEventHandler(this.PasteExecuted);
            
            #line default
            #line hidden
            return;
            case 14:
            
            #line 356 "..\..\..\..\View\Lines.xaml"
            ((System.Windows.Input.CommandBinding)(target)).CanExecute += new System.Windows.Input.CanExecuteRoutedEventHandler(this.CutCanExecute);
            
            #line default
            #line hidden
            
            #line 358 "..\..\..\..\View\Lines.xaml"
            ((System.Windows.Input.CommandBinding)(target)).Executed += new System.Windows.Input.ExecutedRoutedEventHandler(this.CutExecuted);
            
            #line default
            #line hidden
            return;
            case 15:
            
            #line 359 "..\..\..\..\View\Lines.xaml"
            ((System.Windows.Input.CommandBinding)(target)).CanExecute += new System.Windows.Input.CanExecuteRoutedEventHandler(this.DeleteCanExecute);
            
            #line default
            #line hidden
            
            #line 361 "..\..\..\..\View\Lines.xaml"
            ((System.Windows.Input.CommandBinding)(target)).Executed += new System.Windows.Input.ExecutedRoutedEventHandler(this.DeleteExecuted);
            
            #line default
            #line hidden
            return;
            case 20:
            this.DragPopup = ((System.Windows.Controls.Primitives.Popup)(target));
            return;
            }
            this._contentLoaded = true;
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target) {
            System.Windows.EventSetter eventSetter;
            switch (connectionId)
            {
            case 16:
            eventSetter = new System.Windows.EventSetter();
            eventSetter.Event = System.Windows.UIElement.GotFocusEvent;
            
            #line 367 "..\..\..\..\View\Lines.xaml"
            eventSetter.Handler = new System.Windows.RoutedEventHandler(this.LinesGridCell_GotFocus);
            
            #line default
            #line hidden
            ((System.Windows.Style)(target)).Setters.Add(eventSetter);
            break;
            case 17:
            
            #line 485 "..\..\..\..\View\Lines.xaml"
            ((System.Windows.Controls.Border)(target)).MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.HeaderBorder_MouseLeftButtonDown);
            
            #line default
            #line hidden
            break;
            case 18:
            
            #line 495 "..\..\..\..\View\Lines.xaml"
            ((System.Windows.Controls.Primitives.Thumb)(target)).PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.LineGrid_PreviewMouseLeftButtonDown);
            
            #line default
            #line hidden
            break;
            case 19:
            
            #line 687 "..\..\..\..\View\Lines.xaml"
            ((System.Windows.Controls.TextBox)(target)).PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.TripleClickMouseLeftButtonDown);
            
            #line default
            #line hidden
            break;
            }
        }
    }
}

