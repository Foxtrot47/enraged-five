﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DialogueStar.View
{
    /// <summary>
    /// Interaction logic for TabContents.xaml
    /// </summary>
    public partial class TabContents : UserControl
    {
        #region Properties

        /// <summary>
        /// Keeps the size of the rows so that we can change them 
        /// depending on the expander states
        /// </summary>
        GridLength[] rowHeight;

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor, stores the initial row heights for all rows
        /// calls the ExpandedOrCollapsed function for the two main expanders.
        /// </summary>
        public TabContents()
        {
            InitializeComponent();
            rowHeight = new GridLength[this.MainGrid.RowDefinitions.Count];
            for (int i = 0; i < this.MainGrid.RowDefinitions.Count; i++)
            {
                rowHeight[i] = this.MainGrid.RowDefinitions[i].Height;
            }

            ExpandedOrCollapsed(ConversationExpander);
            ExpandedOrCollapsed(LineExpander);
        }

        #endregion // Constructor(s)

        #region Expanded and collapse logic

        /// <summary>
        /// Used when the expanders changes state. This then calls the main 
        /// function to determine the size of the rows
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExpandedOrCollapsed(Object sender, RoutedEventArgs e)
        {
            ExpandedOrCollapsed(sender as Expander);
        }

        /// <summary>
        /// Determines the size of the rows that the two main expanders 
        /// occupy.If the expander is expanded the size of the row is just whatever size it is.
        /// However if the expander isn't exanded the size is set to Auto so that the other expander
        /// takes up the whole space available.
        /// Another thing we do is hide the splitter if both expanders aren't expanded as there
        /// is no need to show it in this case.
        /// </summary>
        /// <param name="expander"></param>
        void ExpandedOrCollapsed(Expander expander)
        {
            if (rowHeight == null) return;

            int rowIndex = Grid.GetRow(expander);
            RowDefinition row = this.MainGrid.RowDefinitions[rowIndex];
            if (expander.IsExpanded)
            {
                row.Height = rowHeight[rowIndex]; 
            } 
            else
            {
                rowHeight[rowIndex] = row.Height;
                row.Height = GridLength.Auto;
            }

            Splitter.Visibility = ConversationExpander.IsExpanded && LineExpander.IsExpanded ? Visibility.Visible : Visibility.Collapsed;
        }

        #endregion // Expanded and collapse logic

    } // TabContents
} // DialogueStar.View
