﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DialogueStar.View
{
    /// <summary>
    /// Interaction logic for FolderValidation.xaml
    /// </summary>
    public partial class FolderValidation : Window
    {
        public FolderValidation()
        {
            InitializeComponent();
        }

        public FolderValidation(List<string> syntaxErrors, List<string> numberErrors, List<string> existenceErrors, List<string> warnings)
        {
            InitializeComponent();

            if (syntaxErrors.Count == 0)
            {
                this.SyntaxHeader.Foreground = Brushes.DarkGreen;
                this.SyntaxHeader.Text += " Ok";
                this.SyntaxListBox.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                this.SyntaxHeader.Foreground = Brushes.DarkRed;
                this.SyntaxHeader.Text += " " + syntaxErrors.Count.ToString() + " Errors";
                this.SyntaxListBox.ItemsSource = syntaxErrors;
            }

            if (numberErrors.Count == 0)
            {
                this.NumbersHeader.Foreground = Brushes.DarkGreen;
                this.NumbersHeader.Text += " Ok";
                this.NumbersListBox.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                this.NumbersHeader.Foreground = Brushes.DarkRed;
                this.NumbersHeader.Text += " " + numberErrors.Count.ToString() + " Errors";
                this.NumbersListBox.ItemsSource = numberErrors;
            }

            if (existenceErrors.Count == 0)
            {
                this.ExistenceHeader.Foreground = Brushes.DarkGreen;
                this.ExistenceHeader.Text += " Ok";
                this.ExistenceListBox.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                this.ExistenceHeader.Foreground = Brushes.DarkRed;
                this.ExistenceHeader.Text += " " + existenceErrors.Count.ToString() + " Errors";
                this.ExistenceListBox.ItemsSource = existenceErrors;
            }

            if (warnings.Count == 0)
            {
                this.WarningHeader.Visibility = System.Windows.Visibility.Collapsed;
                this.WarningListBox.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                this.WarningHeader.Foreground = Brushes.Black;
                this.WarningHeader.Text = warnings.Count.ToString() + " Warnings";
                this.WarningListBox.ItemsSource = warnings;
            }
        }
    }
}
