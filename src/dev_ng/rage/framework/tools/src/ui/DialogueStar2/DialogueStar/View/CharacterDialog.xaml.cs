﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using RSG.Base.Collections;
using RSG.Model.Dialogue.Voices;

namespace DialogueStar.View
{
    /// <summary>
    /// Interaction logic for CharacterDialog.xaml
    /// </summary>
    public partial class CharacterDialog : Window , INotifyPropertyChanged
    {
        #region Events

        /// <summary>
        /// Property changed event fired when the name changes so that the 
        /// binding can be dynamic.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Create the OnPropertyChanged method to raise the event
        /// </summary>
        /// <param name="name"></param>
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion

        #region Dependency properties

        /// <summary>
        /// The list of characters that will be modified while this window is present.
        /// </summary>
        public ObservableDictionary<Guid, String> Characters
        {
            get { return m_characters; }
            set { m_characters = value; OnPropertyChanged("Characters"); }
        }
        private ObservableDictionary<Guid, String> m_characters;

        /// <summary>
        /// The list of characters that will be modified while this window is present.
        /// </summary>
        public ObservableDictionary<Guid, String> CharacterVoices
        {
            get { return m_characterVoices; }
            set { m_characterVoices = value; OnPropertyChanged("CharacterVoices"); }
        }
        private ObservableDictionary<Guid, String> m_characterVoices;

        /// <summary>
        /// The list of characters that will be modified while this window is present.
        /// </summary>
        public String CharacterVoice
        {
            get { return m_selectedVoice; }
            set { m_selectedVoice = value; OnCharacterVoiceChanged(); OnPropertyChanged("CharacterVoice"); }
        }
        private String m_selectedVoice;

        /// <summary>
        /// The list of voices that are currently stored on this pc.
        /// </summary>
        public VoiceRepository InstalledVoices
        {
            get { return m_installedVoices; }
            set { m_installedVoices = value; OnPropertyChanged("InstalledVoices"); }
        }
        private VoiceRepository m_installedVoices;

        /// <summary>
        /// True if the text box to enter a new character name is visible. Gets
        /// set to true if the add button is pressed.
        /// </summary>
        public Boolean AddExpanded
        {
            get { return (Boolean)GetValue(IsAddExpanded); }
            set { SetValue(IsAddExpanded, value); }
        }
        private static DependencyProperty IsAddExpanded = DependencyProperty.Register("AddExpanded", typeof(Boolean), typeof(CharacterDialog));

        /// <summary>
        /// True if the text box to rename a character name is visible. Gets
        /// set to true if the rename button is pressed.
        /// </summary>
        public Boolean RenameExpanded
        {
            get { return (Boolean)GetValue(IsRenameExpanded); }
            set { SetValue(IsRenameExpanded, value); }
        }
        private static DependencyProperty IsRenameExpanded = DependencyProperty.Register("RenameExpanded", typeof(Boolean), typeof(CharacterDialog));

        /// <summary>
        /// Determines if the current current name entered in the new character name text box is a valid name
        /// </summary>
        public Boolean CharacterNameValid
        {
            get { return (Boolean)GetValue(IsCharacterNameValid); }
            set { SetValue(IsCharacterNameValid, value); }
        }
        private static DependencyProperty IsCharacterNameValid = DependencyProperty.Register("CharacterNameValid", typeof(Boolean), typeof(CharacterDialog));

        /// <summary>
        /// This is the message displayed if there are any problems with the
        /// name that has been entered while adding or renameing a character.
        /// </summary>
        public String ValidationMessage
        {
            get { return (String)GetValue(ValidationMessageProperty); }
            set { SetValue(ValidationMessageProperty, value); }
        }
        private static DependencyProperty ValidationMessageProperty = DependencyProperty.Register("ValidationMessage", typeof(String), typeof(CharacterDialog));

        /// <summary>
        /// The currently selected character in the treeview. This is the character that will be remove/merged when
        /// those buttons are pressed
        /// </summary>
        public Boolean AreCharactersSelected
        {
            get { return m_selectedCharacter != Guid.Empty; }
        }
        public Guid SelectedCharacter
        {
            get { return m_selectedCharacter; }
            set { m_selectedCharacter = value; OnPropertyChanged("SelectedCharacter"); OnPropertyChanged("AreCharactersSelected"); }
        }
        private Guid m_selectedCharacter;

        #endregion // Dependency properties

        #region Constructor(s)

        public CharacterDialog()
        {
            InitializeComponent();

            this.AddExpanded = false;
            this.RenameExpanded = false;
            this.CharacterNameValid = false;
            this.SelectedCharacter = Guid.Empty;
            this.AddTextBox.Text = "";
            this.ValidateNewCharacterName(this.AddTextBox);
            this.RenameTextBox.Text = "";
            this.ValidateNewCharacterName(this.RenameTextBox);
        }

        #endregion // Constructor(s)

        #region Button Callbacks

        /// <summary>
        /// When the add button is pushed the add options are shown.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddButton_Click(Object sender, RoutedEventArgs e)
        {
            this.AddTextBox.Focus();
            AddExpanded = !AddExpanded;
            this.AddTextBox.Text = "";
            this.ValidateNewCharacterName(this.AddTextBox);
        }

        /// <summary>
        /// When this button is pushed the add options are hidden, also the validation text is reset.
        /// The main thing is that the character is added to the temp character property list of this object.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddConfirmButton_Click(Object sender, RoutedEventArgs e)
        {
            Guid newGuid = Guid.NewGuid();
            this.Characters.Add(newGuid, this.AddTextBox.Text);
            this.CharacterVoices.Add(newGuid, this.InstalledVoices.DefaultVoiceName);

            SortedDictionary<String, Guid> sortedList = new SortedDictionary<String, Guid>();
            foreach (KeyValuePair<Guid, String> character in this.Characters)
            {
                sortedList.Add(character.Value, character.Key);
            }

            if (sortedList.ContainsKey("SFX"))
            {
                this.Characters.Clear();
                foreach (KeyValuePair<String, Guid> character in sortedList)
                {
                    if (character.Key != "SFX")
                    {
                        this.Characters.Add(character.Value, character.Key);
                    }
                }

                this.Characters.Add(sortedList["SFX"], "SFX");
            }

            AddExpanded = !AddExpanded;
            this.AddTextBox.Text = "";
            this.ValidateNewCharacterName(this.AddTextBox);
        }

        /// <summary>
        /// When this button is pressed the add options are hidden and no manipulation functions are called.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CancelAddButton_Click(Object sender, RoutedEventArgs e)
        {
            AddExpanded = !AddExpanded;
            this.AddTextBox.Text = "";
            this.ValidateNewCharacterName(this.AddTextBox);
        }

        /// <summary>
        /// When the rename button is pushed the rename options are shown.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RenameButton_Click(Object sender, RoutedEventArgs e)
        {
            this.RenameTextBox.Focus();
            this.CharacterTreeView.IsEnabled = false;
            RenameExpanded = !RenameExpanded;
            this.RenameTextBox.Text = "";
            this.ValidateNewCharacterName(this.RenameTextBox);
        }

        /// <summary>
        /// When this button is pushed the rename options are hidden also the validation text is rest.
        /// The main thing is that the character is renamed in the temp character property list of this object.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RenameConfirmButton_Click(Object sender, RoutedEventArgs e)
        {
            this.Characters[m_selectedCharacter] = this.RenameTextBox.Text;

            SortedDictionary<String, Guid> sortedList = new SortedDictionary<String, Guid>();
            foreach (KeyValuePair<Guid, String> character in this.Characters)
            {
                sortedList.Add(character.Value, character.Key);
            }

            if (sortedList.ContainsKey("SFX"))
            {
                this.Characters.Clear();
                foreach (KeyValuePair<String, Guid> character in sortedList)
                {
                    if (character.Key != "SFX")
                    {
                        this.Characters.Add(character.Value, character.Key);
                    }
                }

                this.Characters.Add(sortedList["SFX"], "SFX");
            }

            this.CharacterTreeView.IsEnabled = true;
            RenameExpanded = !RenameExpanded;
            this.RenameTextBox.Text = "";
            this.ValidateNewCharacterName(this.RenameTextBox);
        }

        /// <summary>
        /// When this button is pressed the rename options are hidden and no manipulation functions are called.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CancelRenameButton_Click(Object sender, RoutedEventArgs e)
        {
            RenameExpanded = !RenameExpanded;
            this.RenameTextBox.Text = "";
            this.ValidateNewCharacterName(this.RenameTextBox);

            this.CharacterTreeView.IsEnabled = true;
        }

        /// <summary>
        /// Removes the selected character.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            this.Characters.Remove(m_selectedCharacter);
        }

        /// <summary>
        /// The main cancel button: it closes the dialogue returning a false result
        /// so that the calling function can take the correct action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cancel_Click(Object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        /// <summary>
        /// The main save button: it closes the dialogue returning a true result
        /// so that the calling function can take the correct action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Save_Click(Object sender, RoutedEventArgs e)
        {
            if (!App.Controller.ComfirmConfigFileIsValidForSave(true))
            {
                MessageBox.Show("Unable to save configuration file. Please make sure you currently have it checked out of perforce.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                DialogResult = true;
            }
        }

        #endregion // Button Callbacks

        #region TreeView Callbacks

        /// <summary>
        /// Make sure that we can not select the root node of the tree view, since we cannot remove, rename
        /// or merge this.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RootItem_Selected(Object sender, RoutedEventArgs e)
        {
            this.RootItem.IsSelected = false;
        }

        /// <summary>
        /// Sets the selected character property so we can remove/rename/merge characters
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CharacterTreeView_SelectedItemChanged(Object sender, RoutedPropertyChangedEventArgs<Object> e)
        {
            if (CharacterTreeView.SelectedItem != null)
            {
                if (CharacterTreeView.SelectedItem is KeyValuePair<Guid, String>)
                {
                    this.SelectedCharacter = ((KeyValuePair<Guid, String>)CharacterTreeView.SelectedItem).Key;
                    this.CharacterVoice = this.CharacterVoices[SelectedCharacter];
                }
                else
                {
                    this.SelectedCharacter = Guid.Empty;
                }
            }
            else
            {
                this.SelectedCharacter = Guid.Empty;
            }
        }

        private void OnCharacterVoiceChanged()
        {
            this.CharacterVoices[this.SelectedCharacter] = this.CharacterVoice != null ? this.CharacterVoice : String.Empty;
        }

        #endregion // TreeView Callbacks

        #region Name Validation

        /// <summary>
        /// Every time the text changes determine if the name is valid or not.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextBox_TextChanged(Object sender, TextChangedEventArgs e)
        {
            ValidateNewCharacterName(sender as TextBox);
        }

        /// <summary>
        /// Every time the text changes determine if the name is valid or not.
        /// </summary>
        private void ValidateNewCharacterName(TextBox textbox)
        {
            // Test to make sure that the length is not zero
            if (textbox.Text.Length <= 0)
            {
                this.CharacterNameValid = false;
                this.ValidationMessage = "Cannot be zero length.";
                return;
            }

            // Test to make sure that it only contains alpha numeric characters
            Boolean IsAlphaNumeric = Regex.IsMatch(textbox.Text, "^[a-zA-Z0-9_]*$");
            if (!IsAlphaNumeric)
            {
                this.CharacterNameValid = false;
                this.ValidationMessage = "Has to only contain alpha-numeric.";
                return;
            }

            // Test to make sure that it doesn't already belong to the character dictionary
            if (this.Characters.ContainsValue(textbox.Text))
            {
                this.CharacterNameValid = false;
                this.ValidationMessage = "Has to be unique.";
                return;
            }

            this.ValidationMessage = "";
            this.CharacterNameValid = true;
        }

        #endregion // Name Validation

    } // CharacterDialog
} // DialogueStar.View
