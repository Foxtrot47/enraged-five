﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.Diagnostics;

namespace DialogueStar.View
{
    /// <summary>
    /// Interaction logic for AboutDialog.xaml
    /// </summary>
    public partial class AboutDialog : Window
    {
        #region Constructor(s)

        public AboutDialog()
        {
            InitializeComponent();
        }

        #endregion // Constructor(s)

        #region Button Callbacks

        /// <summary>
        /// The main ok button: it just closes the window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Ok_Click(Object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        /// <summary>
        /// Not really a button but something that you press. This opens up the 
        /// web page in the hyperlink
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Hyperlink_RequestNavigate(Object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }

        #endregion // Button Callbacks

    } // AboutDialog
} // DialogueStar.View
