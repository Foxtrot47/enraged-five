﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Microsoft.Win32;
using System.Xml;
using System.Diagnostics;
using System.Reflection;

namespace DialogueStar
{
    /// <summary>
    /// Contains all of the code for a in menu recent file list
    /// </summary>
    public class RecentFileList : MenuItem
    {
        #region Interfaces

        /// <summary>
        /// Interface that gives something a persist attribute through application instances
        /// </summary>
        public interface IPersist
        {
            /// <summary>
            /// The actual list of filenames that have been opened recently
            /// </summary>
            /// <param name="max">The max number of recent files allowed</param>
            /// <returns></returns>
            ObservableCollection<String> RecentFiles(int max);

            /// <summary>
            /// Insert a filename into the recent list
            /// </summary>
            /// <param name="filepath"></param>
            /// <param name="max"></param>
            void InsertFile(String filepath, int max);

            /// <summary>
            /// Remove a filename from the recent list
            /// </summary>
            /// <param name="filepath"></param>
            /// <param name="max"></param>
            void RemoveFile(String filepath, int max);
        }

        #endregion // Interface

        #region Properties

        /// <summary>
        /// The interface instance that will be used to make sure that the filenames are persistent
        /// </summary>
        public IPersist Persister
        {
            get { return m_persister; }
            set { m_persister = value; }
        }
        private IPersist m_persister;

        /// <summary>
        /// The maximum number of recent files allowed by this recent file manager
        /// </summary>
        public int MaxNumberOfFiles
        {
            get { return m_maxNumberOfFiles; }
            set { m_maxNumberOfFiles = value; }
        }
        private int m_maxNumberOfFiles;

        /// <summary>
        /// The maximum character count to display the recent file with
        /// </summary>
        public int MaxPathLength
        {
            get { return m_maxPathLength; }
            set { m_maxPathLength = value; }
        }
        private int m_maxPathLength;

        /// <summary>
        /// The actual menu item that will be shown in the menu
        /// </summary>
        public MenuItem FileMenu
        {
            get { return m_fileMenu; }
            private set { m_fileMenu = value; }
        }
        private MenuItem m_fileMenu;

        /// <summary>
        /// Used in: String.Format( MenuItemFormat, index, filepath, displayPath );
        /// Default = "_{0}:  {2}" This is so all of the file names are aligned.
        /// </summary>
        public String MenuItemFormatOneToNine
        {
            get { return m_menuItemFormatOneToNine; }
            set { m_menuItemFormatOneToNine = value; } 
        }
        private String m_menuItemFormatOneToNine;

        /// <summary>
        /// Used in: String.Format( MenuItemFormat, index, filepath, displayPath );
        /// Default = "{0}:  {2}" This is so all of the file names are aligned.
        /// </summary>
        public String MenuItemFormatTenPlus
        {
            get { return m_menuItemFormatTenPlus; }
            set { m_menuItemFormatTenPlus = value; } 
        }
        private String m_menuItemFormatTenPlus;

        /// <summary>
        /// The list of recent file filepaths
        /// </summary>
        public ObservableCollection<String> RecentFiles
        {
            get { return Persister.RecentFiles(MaxNumberOfFiles); }
        }

        /// <summary>
        /// The list of recent files 
        /// </summary>
        public ObservableCollection<RecentFile> RecentFileObjects
        {
            get { return m_recentFileObjects; }
            set { m_recentFileObjects = value; }
        }
        private ObservableCollection<RecentFile> m_recentFileObjects;

        /// <summary>
        /// The bottom separate and the value that represents whether it should be shown
        /// </summary>
        public Boolean ShowBottomSeparator
        {
            get { return m_showBottomSeparate; }
            set { m_showBottomSeparate = value; }
        }
        private Boolean m_showBottomSeparate = true;
        private Separator m_bottomSeparator = null;

        /// <summary>
        /// The top separate and the value that represents whether it should be shown
        /// </summary>
        public Boolean ShowTopSeparator
        {
            get { return m_showTopSeparate; }
            set { m_showTopSeparate = value; }
        }
        private Boolean m_showTopSeparate = true;
        private Separator m_topSeparator = null;

        public event EventHandler<MenuClickEventArgs> MenuClick;

        #endregion // Properties

        #region Persistent Initalising

        /// <summary>
        /// Initialises the persistent object to use the registry to make sure the 
        /// filenames are persistent.
        /// </summary>
        public void UseRegistryPersister() 
        { 
            Persister = new RegistryPersister();
        }

        /// <summary>
        /// Initialises the persistent object to use the registry to make sure the 
        /// filenames are persistent.
        /// </summary>
        public void UseRegistryPersister(String key) 
        { 
            Persister = new RegistryPersister(key);
        }

        #endregion // Persistent Initalising

        #region Constructor(s)

        /// <summary>
        /// Default constructor that sets the properties to their default values
        /// </summary>
        public RecentFileList()
        {
            this.m_persister = new RegistryPersister();
            this.m_recentFileObjects = new ObservableCollection<RecentFile>();

            // Need to collapse this menu item as it will always be empty
            this.Visibility = System.Windows.Visibility.Collapsed;

            MaxNumberOfFiles = 9;
            MaxPathLength = 50;
            MenuItemFormatOneToNine = "_{0}:  {2}";
            MenuItemFormatTenPlus = "{0}:  {2}";

            this.Loaded += (s, e) => HookFileMenu();
        }

        #endregion // Constructor(s)

        #region Classes

        /// <summary>
        /// Defines a single recent file entry
        /// </summary>
        public class RecentFile
        {
            #region Properties 

            /// <summary>
            /// The index number of this recent file
            /// </summary>
            public int Number
            {
                get { return m_number; }
                set { m_number = value; }
            }
            private int m_number = 0;

            /// <summary>
            /// The filepath for this recent file
            /// </summary>
            public String Filepath
            {
                get { return m_filepath; }
                set { m_filepath = value; }
            }
            private String m_filepath = "";

            /// <summary>
            /// The menu item that is associated with this recent file
            /// </summary>
            public MenuItem MenuItem
            {
                get { return m_menuItem; }
                set { m_menuItem = value; }
            }
            public MenuItem m_menuItem = null;

            /// <summary>
            /// The path that is displayed
            /// </summary>
            public String DisplayPath
            {
                get { return Path.Combine(Path.GetDirectoryName(Filepath), Path.GetFileName(Filepath)); }
            }

            #endregion // Properties

            #region Constructor(s)

            /// <summary>
            /// Sets the index and the filepath by the given variables
            /// </summary>
            /// <param name="number"></param>
            /// <param name="filepath"></param>
            public RecentFile(int number, String filepath)
            {
                this.Number = number;
                this.Filepath = filepath;
            }

            #endregion // Constructor(s)

        } // RecentFile

        /// <summary>
        /// The args that will be used in the handler in the event that a menu item that
        /// represents a recent file will be clicked
        /// </summary>
        public class MenuClickEventArgs : EventArgs
        {
            #region Properties
            
            /// <summary>
            /// The filepath associated with the recent file
            /// </summary>
            public string Filepath 
            {
                get { return m_filepath; }
                private set { m_filepath = value; }
            }
            private String m_filepath;

            #endregion // Properties

            #region Constructor(s)

            /// <summary>
            /// Default constructor that just takes a filepath
            /// </summary>
            /// <param name="filepath"></param>
            public MenuClickEventArgs(string filepath)
            {
                this.Filepath = filepath;
            }

            #endregion // Constructor(s)

        } // MenuClickEventArgs

        #endregion // Classes

        #region Public Function(s)

        /// <summary>
        /// The main function to remove a file from the recent file list
        /// </summary>
        /// <param name="filepath"></param>
        public void RemoveFile(String filepath)
        { 
            Persister.RemoveFile(filepath, MaxNumberOfFiles);

            foreach (RecentFile r in m_recentFileObjects)
            {
                if (r.Filepath == filepath)
                {
                    FileMenu.Items.Remove(r.MenuItem);
                    m_recentFileObjects.Remove(r);
                    break;
                }
            }
        }

        /// <summary>
        /// The main function to insert a file into the recent file list
        /// </summary>
        /// <param name="filepath"></param>
        public void InsertFile(String filepath)
        { 
            Persister.InsertFile(filepath, MaxNumberOfFiles);
        }

        /// <summary>
        /// Empties the files in the recent file list so that nothing will be shown
        /// </summary>
        public void ClearAllFiles()
        {
            List<String> recentList = new List<String>(this.RecentFiles);

            foreach (String filename in recentList)
            {
                this.RemoveFile(filename);
            }
        }

        #endregion // Public Function(s)

        #region Private Function(s)

        /// <summary>
        /// Creates the sub menu of recent file menut items by loading the recent files and
        /// creating the menu items for them
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _FileMenu_SubmenuOpened(Object sender, RoutedEventArgs e)
        {
            RemoveMenuItems();

            LoadRecentFiles();

            InsertMenuItems();
        }

        /// <summary>
        /// Removes all of the menu items, i.e reset the control
        /// </summary>
        private void RemoveMenuItems()
        {
            if (m_bottomSeparator != null) FileMenu.Items.Remove(m_bottomSeparator);
            if (m_topSeparator != null) FileMenu.Items.Remove(m_topSeparator);

            if (m_recentFileObjects != null)
            {
                foreach (RecentFile r in m_recentFileObjects)
                {
                    if (r.MenuItem != null)
                    {
                        FileMenu.Items.Remove(r.MenuItem);
                    }
                }
            }

            m_bottomSeparator = null;
            m_topSeparator = null;
            m_recentFileObjects.Clear();
        }

        /// <summary>
        /// Loads the recent files
        /// </summary>
        private void LoadRecentFiles()
        {
            int index = 0;
            ObservableCollection<String> list = this.m_persister.RecentFiles(MaxNumberOfFiles);
            foreach (string filepath in list)
            {
                m_recentFileObjects.Add(new RecentFile(index++, filepath));
            }
        }

        /// <summary>
        /// Inserts the current list of recent files as menu items
        /// </summary>
        private void InsertMenuItems()
        {
            if (m_recentFileObjects == null) return;
            if (m_recentFileObjects.Count == 0) return;

            int menuItemIndex = FileMenu.Items.IndexOf(this);

            // Add the top separate if nedeed
            if (this.ShowTopSeparator == true)
            {
                m_topSeparator = new Separator();
                FileMenu.Items.Insert(++menuItemIndex, m_topSeparator);
            }

            // Add the recent files menu items
            foreach (RecentFile r in m_recentFileObjects)
            {
                String header = GetMenuItemText(r.Number + 1, r.Filepath, r.DisplayPath);

                r.MenuItem = new MenuItem { Header = header };
                if (Double.IsNaN(this.Height) == false)
                    r.MenuItem.MinHeight = this.Height;

                r.MenuItem.Click += MenuItem_Click;

                FileMenu.Items.Insert(++menuItemIndex, r.MenuItem);
            }

            // Add the bottom separate if nedeed
            if (this.ShowBottomSeparator == true)
            {
                m_bottomSeparator = new Separator();
                FileMenu.Items.Insert(++menuItemIndex, m_bottomSeparator);
            }
        }

        /// <summary>
        /// Gets the text that will be displayed as the menu item header for a recent file
        /// </summary>
        /// <param name="index"></param>
        /// <param name="filepath"></param>
        /// <param name="displaypath"></param>
        /// <returns></returns>
        private String GetMenuItemText(int index, String filepath, String displaypath)
        {
            String format = (index < 10 ? MenuItemFormatOneToNine : MenuItemFormatTenPlus);
            String shortPath = ShortenPathname(displaypath, MaxPathLength);

            return String.Format(format, index, filepath, shortPath);
        }

        /// <summary>
        /// Shortens a pathname for display purposes.
        /// </summary>
        /// <param labelName="pathname">The pathname to shorten.</param>
        /// <param labelName="maxLength">The maximum number of characters to be displayed.</param>
        /// <returns></returns>
        private String ShortenPathname(String pathname, int maxLength)
        {
            if (pathname.Length <= maxLength)
                return pathname;

            String root = Path.GetPathRoot(pathname);
            if (root.Length > 3)
                root += Path.DirectorySeparatorChar;

            String[] elements = pathname.Substring(root.Length).Split(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

            int filenameIndex = elements.GetLength(0) - 1;

            if (elements.GetLength(0) == 1) // pathname is just a root and filename
            {
                if (elements[0].Length > 5) // long enough to shorten
                {
                    // if path is a UNC path, root may be rather long
                    if (root.Length + 6 >= maxLength)
                    {
                        return root + elements[0].Substring(0, 3) + "...";
                    }
                    else
                    {
                        return pathname.Substring(0, maxLength - 3) + "...";
                    }
                }
            }
            else if ((root.Length + 4 + elements[filenameIndex].Length) > maxLength) // pathname is just a root and filename
            {
                root += "...\\";

                int len = elements[filenameIndex].Length;
                if (len < 6)
                    return root + elements[filenameIndex];

                if ((root.Length + 6) >= maxLength)
                {
                    len = 3;
                }
                else
                {
                    len = maxLength - root.Length - 3;
                }
                return root + elements[filenameIndex].Substring(0, len) + "...";
            }
            else if (elements.GetLength(0) == 2)
            {
                return root + "...\\" + elements[1];
            }
            else
            {
                int len = 0;
                int begin = 0;

                for (int i = 0; i < filenameIndex; i++)
                {
                    if (elements[i].Length > len)
                    {
                        begin = i;
                        len = elements[i].Length;
                    }
                }

                int totalLength = pathname.Length - len + 3;
                int end = begin + 1;

                while (totalLength > maxLength)
                {
                    if (begin > 0)
                        totalLength -= elements[--begin].Length - 1;

                    if (totalLength <= maxLength)
                        break;

                    if (end < filenameIndex)
                        totalLength -= elements[++end].Length - 1;

                    if (begin == 0 && end == filenameIndex)
                        break;
                }

                // assemble final string
                for (int i = 0; i < begin; i++)
                {
                    root += elements[i] + '\\';
                }

                root += "...\\";

                for (int i = end; i < filenameIndex; i++)
                {
                    root += elements[i] + '\\';
                }

                return root + elements[filenameIndex];
            }

            return pathname;
        }

        /// <summary>
        /// Loads the recent files, this is called every time the menu is loaded
        /// </summary>
        private ObservableCollection<RecentFile> LoadRecentFilesCore()
        {
            ObservableCollection<String> list = this.m_persister.RecentFiles(MaxNumberOfFiles); // RecentFiles;
            ObservableCollection<RecentFile> files = new ObservableCollection<RecentFile>();

            int i = 0;
            foreach (string filepath in list)
            {
                files.Add(new RecentFile(i++, filepath));
            }
            return files;
        }

        /// <summary>
        /// Event called when the menu item has been clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuItem_Click(Object sender, EventArgs e)
        {
            MenuItem menuItem = sender as MenuItem;

            OnMenuClick(menuItem);
        }

        /// <summary>
        /// Event called when the menu item has been clicked, gets the filepath associated
        /// with the clicked menu item and fires off the event
        /// </summary>
        /// <param name="menuItem"></param>
        protected virtual void OnMenuClick(MenuItem menuItem)
        {
            String filepath = GetFilepath(menuItem);

            if (String.IsNullOrEmpty(filepath)) return;

            EventHandler<MenuClickEventArgs> dMenuClick = MenuClick;

            if (dMenuClick != null)
                dMenuClick(menuItem, new MenuClickEventArgs(filepath));
        }

        /// <summary>
        /// Gets the filepath from the given menu item
        /// </summary>
        /// <param name="menuItem"></param>
        /// <returns></returns>
        private String GetFilepath(MenuItem menuItem)
        {
            foreach (RecentFile r in m_recentFileObjects)
            {
                if (r.MenuItem == menuItem)
                {
                    return r.Filepath;
                }
            }

            return String.Empty;
        }

        /// <summary>
        /// Called when first loaded. This loads the recent file list and sets the menu items
        /// </summary>
        private void HookFileMenu()
        {
            MenuItem parent = Parent as MenuItem;

            if (parent == null)
                throw new ApplicationException("Parent must be a MenuItem");

            if (FileMenu == parent) return;

            if (FileMenu != null)
                FileMenu.SubmenuOpened -= _FileMenu_SubmenuOpened;

            FileMenu = parent;
            FileMenu.SubmenuOpened += _FileMenu_SubmenuOpened;
        }

        #endregion // Private Function(s)

    } // RecentFileList

    /// <summary>
    /// Contains the static information about the application from using reflection
    /// </summary>
    static class ApplicationAttributes
    {
        #region Properties

        /// <summary>
        /// The main assembly varibale used to get the other attributes
        /// </summary>
        static readonly Assembly m_assembly = null;

        /// <summary>
        /// The title of the application
        /// </summary>
        public static String Title
        {
            get { return m_title; }
            private set { m_title = value; }
        }
        private static String m_title;
        static readonly AssemblyTitleAttribute m_titleAttributes = null;

        /// <summary>
        /// The company name of the application
        /// </summary>
        public static String CompanyName
        {
            get { return m_companyName; }
            private set { m_companyName = value; }
        }
        private static String m_companyName;
        static readonly AssemblyCompanyAttribute m_companyAttributes = null;

        /// <summary>
        /// The copyright of the application
        /// </summary>
        public static String Copyright
        {
            get { return m_copyright; }
            private set { m_copyright = value; }
        }
        private static String m_copyright;
        static readonly AssemblyCopyrightAttribute m_copyrightAttributes = null;

        /// <summary>
        /// The product name of the application
        /// </summary>
        public static String ProductName
        {
            get { return m_productName; }
            private set { m_productName = value; }
        }
        private static String m_productName;
        static readonly AssemblyProductAttribute m_productAttributes = null;

        /// <summary>
        /// The version number of the application
        /// </summary>
        public static String Version
        {
            get { return m_version; }
            private set { m_version = value; }
        }
        private static String m_version; 
        static Version m_assemblyVersion = null;

        /// <summary>
        /// A dictionary that contains all of the attributes in it so it can be
        /// set as a data context if needed.
        /// </summary>
        public static Dictionary<String, String> Attributes
        {
            get { return m_attributes; }
            private set { m_attributes = value; }
        }
        private static Dictionary<String, String> m_attributes;

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Gets all of the assembly information using reflection.
        /// </summary>
        static ApplicationAttributes()
        {
            Attributes = new Dictionary<String, String>();
            try
            {
                Title = String.Empty;
                CompanyName = String.Empty;
                Copyright = String.Empty;
                ProductName = String.Empty;
                Version = String.Empty;

                m_assembly = Assembly.GetEntryAssembly();

                if (m_assembly != null)
                {
                    object[] attributes = m_assembly.GetCustomAttributes(false);

                    foreach (object attribute in attributes)
                    {
                        Type type = attribute.GetType();

                        if (type == typeof(AssemblyTitleAttribute))
                            m_titleAttributes = (AssemblyTitleAttribute)attribute;

                        if (type == typeof(AssemblyCompanyAttribute))
                            m_companyAttributes = (AssemblyCompanyAttribute)attribute;

                        if (type == typeof(AssemblyCopyrightAttribute))
                            m_copyrightAttributes = (AssemblyCopyrightAttribute)attribute;

                        if (type == typeof(AssemblyProductAttribute))
                            m_productAttributes = (AssemblyProductAttribute)attribute;
                    }

                    m_assemblyVersion = m_assembly.GetName().Version;
                }

                if (m_titleAttributes != null)
                {
                    Title = m_titleAttributes.Title;
                    Attributes.Add("Title", m_titleAttributes.Title);
                }

                if (m_companyAttributes != null)
                {
                    CompanyName = m_companyAttributes.Company;
                    Attributes.Add("CompanyName", m_companyAttributes.Company);
                }

                if (m_copyrightAttributes != null)
                {
                    Copyright = m_copyrightAttributes.Copyright;
                    Attributes.Add("Copyright", m_copyrightAttributes.Copyright);
                }

                if (m_productAttributes != null)
                {
                    ProductName = m_productAttributes.Product;
                    Attributes.Add("ProductName", m_productAttributes.Product);
                }

                if (m_assemblyVersion != null)
                {
                    Version = m_assemblyVersion.ToString();
                    Attributes.Add("Version", m_assemblyVersion.ToString());
                }
            }
            catch
            {
            }
        }

        #endregion // Constructor(s)

    } // ApplicationAttributes

    /// <summary>
    /// Class that contains all that is needed to keep the filenames in the registry
    /// </summary>
    public class RegistryPersister : RecentFileList.IPersist
    {
        #region Properties

        /// <summary>
        /// The actual registry key for the applicaion
        /// </summary>
        public String RegistryKey
        {
            get { return m_registryKey; }
            set { m_registryKey = value; }
        }
        private String m_registryKey;

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Creates the registry key by using the applcation attributes
        /// </summary>
        public RegistryPersister()
        {
            RegistryKey = "Software\\" + ApplicationAttributes.CompanyName + "\\" + ApplicationAttributes.ProductName + "\\" + "RecentFileList";
        }

        /// <summary>
        /// Takes a given key and initalises this instance using that key
        /// </summary>
        /// <param name="key"></param>
        public RegistryPersister(String key)
        {
            RegistryKey = key;
        }

        #endregion // Constructor(s)

        #region Public Function(s)

        /// <summary>
        /// Gets the recent files from the registry and returns them in a list.
        /// </summary>
        /// <param name="max"></param>
        /// <returns></returns>
        public ObservableCollection<String> RecentFiles(int max)
        {
            RegistryKey k = Registry.CurrentUser.OpenSubKey(RegistryKey);
            if (k == null) k = Registry.CurrentUser.CreateSubKey(RegistryKey);

            ObservableCollection<String> list = new ObservableCollection<String>();

            for (int i = 0; i < max; i++)
            {
                String filename = (String)k.GetValue(Key(i));

                if (String.IsNullOrEmpty(filename)) break;

                list.Add(filename);
            }

            return list;
        }

        /// <summary>
        /// Inserts a new recent file into the list
        /// </summary>
        /// <param name="filepath"></param>
        /// <param name="max"></param>
        public void InsertFile(String filepath, int max)
        {
            RegistryKey k = Registry.CurrentUser.OpenSubKey(RegistryKey);
            if (k == null) Registry.CurrentUser.CreateSubKey(RegistryKey);
            k = Registry.CurrentUser.OpenSubKey(RegistryKey, true);

            RemoveFile(filepath, max);

            for (int i = max - 2; i >= 0; i--)
            {
                String sThis = Key(i);
                String sNext = Key(i + 1);

                object oThis = k.GetValue(sThis);
                if (oThis == null) continue;

                k.SetValue(sNext, oThis);
            }

            k.SetValue(Key(0), filepath);
        }

        /// <summary>
        /// Removes a file for the recent list
        /// </summary>
        /// <param name="filepath"></param>
        /// <param name="max"></param>
        public void RemoveFile(String filepath, int max)
        {
            RegistryKey k = Registry.CurrentUser.OpenSubKey(RegistryKey);
            if (k == null) return;

            for (int i = 0; i < max; i++)
            {
                String s = (String)k.GetValue(Key(i));
                if (s != null && s.Equals(filepath, StringComparison.CurrentCultureIgnoreCase))
                {
                    RemoveFile(i, max);
                    break;
                }
            }
        }

        #endregion // Public Function(s)

        #region Private Function(s)

        /// <summary>
        /// Removes a file from the register and makes sure that all the others are still there
        /// and that the max number is not broken
        /// </summary>
        /// <param name="index"></param>
        /// <param name="max"></param>
        private void RemoveFile(int index, int max)
        {
            RegistryKey k = Registry.CurrentUser.OpenSubKey(RegistryKey, true);
            if (k == null) return;

            k.DeleteValue(Key(index), false);

            // Need to 'move' the filenames that come after this down one
            for (int i = index; i < max - 1; i++)
            {
                String sThis = Key(i);
                String sNext = Key(i + 1);

                Object oNext = k.GetValue(sNext);
                if (oNext == null) break;

                k.SetValue(sThis, oNext);
                k.DeleteValue(sNext);
            }
        }

        /// <summary>
        /// Gets the key in a string format for the given index
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        private String Key(int i)
        {
            return i.ToString("00");
        }

        #endregion // Private Function(s)

    } // RegistryPersister
} // DialogueStar
