﻿using System;
using System.IO;
using System.Xml;
using System.Xml.XPath;
using System.Reflection;
using System.Windows;
using System.Timers;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Microsoft.Win32;
using RSG.Base.Collections;
using RSG.Model.Dialogue;
using RSG.Model.Dialogue.Config;
using RSG.Model.Dialogue.UndoManager;
using RSG.Model.Dialogue.DirtyManager;
using RSG.Model.Dialogue.Search;
using RSG.SourceControl.Perforce;
using RSG.Base.Logging.Universal;
using RSG.Base.Logging;
using DialogueStar.Properties;
using System.Diagnostics;
using System.Text.RegularExpressions;
using DialogueStar.View;
using RSG.Base.Configuration;
using System.Runtime.InteropServices;

namespace DialogueStar
{
    /// <summary>
    /// A class that calls all of the model functions to be used in the
    /// view.
    /// </summary>
    public class Controller : INotifyPropertyChanged
    {
        #region Property change events

        /// <summary>
        /// Property changed event fired when the any of the properties change
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Create the OnPropertyChanged method to raise the event
        /// </summary>
        /// <param name="name"></param>
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                try
                {
                    handler(this, new PropertyChangedEventArgs(name));
                }
                catch (System.InvalidOperationException)
                {
                    // Happens due to deferred refreshing on the wpf data grid (nothing can be done
                    // apart from one day change the data grid to a grid view).
                    // Try the operation again and see if it works this time, if not don't do anything.
                    try
                    {
                        handler(this, new PropertyChangedEventArgs(name));
                    }
                    catch (Exception ex)
                    {
                        RSG.Base.Logging.Log.Log__Exception(ex, "Deferred Refresh Exception?");
                    }
                }
            }
        }

        #endregion // Property change events

        #region Properties and associate data

        public DataObject InternalClipBoard;

        /// <summary>
        /// The current configurations for the application
        /// </summary>
        public DialogueConfigurations Configurations
        {
            get { return m_configurations; }
            set { m_configurations = value; }
        }
        private DialogueConfigurations m_configurations;

        /// <summary>
        /// A collection of undo managers one for every mission dialogue
        /// currently opened
        /// </summary>
        public System.Collections.ObjectModel.ObservableCollection<UndoManagerObject> UndoManagers
        {
            get { return m_undoManagers; }
            set { m_undoManagers = value; }
        }
        private System.Collections.ObjectModel.ObservableCollection<UndoManagerObject> m_undoManagers;

        /// <summary>
        /// A list of all the currently opened mission dialogue files,
        /// keyed with the file path or file name if it doesn't have a path.
        /// </summary>
        public System.Collections.ObjectModel.ObservableCollection<MissionDialogue> Missions
        {
            get { return m_missions; }
            set { m_missions = value; }
        }
        private System.Collections.ObjectModel.ObservableCollection<MissionDialogue> m_missions;

        /// <summary>
        /// A index that gets incremented everytime a new dialogue page gets created.
        /// </summary>
        private uint CreatedCount
        {
            get { return m_createdCount; }
            set { m_createdCount = value; }
        }
        private uint m_createdCount = 0;

        /// <summary>
        /// Represents the selected item in the main tab control, basically represents
        /// the dialogue that the user is currently looking at.
        /// </summary>
        public MissionDialogue SelectedDialogue
        {
            get { return m_selectedDialogue; }
            set
            {
                if (m_selectedDialogue != null)
                {
                    selectionHistory[m_selectedDialogue] = Math.Max(0, m_selectedDialogue.Conversations.IndexOf(m_selectedConversation));
                }

                m_selectedDialogue = value;

                // Select the first conversation of the newly selected mission if one exists.
                // And select the undo manager
                if (m_selectedDialogue != null)
                {
                    int conversationIndex = 0;
                    if (selectionHistory.ContainsKey(m_selectedDialogue))
                    {
                        conversationIndex = selectionHistory[m_selectedDialogue];
                    }

                    if (m_selectedDialogue.Conversations.Count > conversationIndex)
                    {
                        this.SelectedConversation = m_selectedDialogue.Conversations[conversationIndex];
                    }
                    int index = this.Missions.IndexOf(m_selectedDialogue);
                    this.SelectedUndoManager = this.UndoManagers[index];
                }
                else
                {
                    this.SelectedUndoManager = null;
                }

                OnPropertyChanged("SelectedDialogue");
            }
        }
        private MissionDialogue m_selectedDialogue;
        private Dictionary<MissionDialogue, int> selectionHistory = new Dictionary<MissionDialogue, int>();

        /// <summary>
        /// Represents the selected items undo manager in the main tab control, basically represents
        /// the undo manager for the dialogue that the user is currently looking at.
        /// </summary>
        public UndoManagerObject SelectedUndoManager
        {
            get { return m_selectedUndoManager; }
            set
            {
                m_selectedUndoManager = value;
                OnPropertyChanged("SelectedUndoManager");
            }
        }
        private UndoManagerObject m_selectedUndoManager;

        /// <summary>
        /// Represents the currently selected conversation if there is one.
        /// </summary>
        public Conversation SelectedConversation
        {
            get { return m_selectedConversation; }
            set
            {
                if (m_selectedConversation != null)
                {
                    selectionLineHistory[m_selectedConversation] = Math.Max(0, m_selectedConversation.Lines.IndexOf(m_selectedLine));
                }

                m_selectedConversation = value;
                if (m_selectedConversation != null)
                {
                    int lineIndex = 0;
                    if (selectionLineHistory.ContainsKey(m_selectedConversation))
                    {
                        lineIndex = selectionLineHistory[m_selectedConversation];
                    }

                    if (m_selectedConversation.Lines.Count > lineIndex)
                    {
                        this.SelectedLine = m_selectedConversation.Lines[lineIndex];
                    }
                }

                OnPropertyChanged("SelectedConversation");
            }
        }
        private Conversation m_selectedConversation;
        private Dictionary<Conversation, int> selectionLineHistory = new Dictionary<Conversation, int>();

        /// <summary>
        /// Represents the currently selected line if there is one.
        /// </summary>
        public Line SelectedLine
        {
            get { return m_selectedLine; }
            set { m_selectedLine = value; OnPropertyChanged("SelectedLine"); }
        }
        private Line m_selectedLine;

        /// <summary>
        /// A reference to the conversation control so we can manipulate the datagrid from the controller
        /// </summary>
        public DialogueStar.View.Conversations ConversationUserControl
        {
            get { return m_conversationUserControl; }
            set { m_conversationUserControl = value; }
        }
        private DialogueStar.View.Conversations m_conversationUserControl = null;

        /// <summary>
        /// A reference to the line control so we can manipulate the datagrid from the controller
        /// </summary>
        public DialogueStar.View.Lines LineUserControl
        {
            get { return m_lineUserControl; }
            set { m_lineUserControl = value; }
        }
        private DialogueStar.View.Lines m_lineUserControl = null;

        /// <summary>
        /// A reference to the main window of the application
        /// </summary>
        public DialogueStar.View.MainWindow MainWindow
        {
            get { return m_mainWindow; }
            set { m_mainWindow = value; }
        }
        private DialogueStar.View.MainWindow m_mainWindow = null;

        /// <summary>
        /// The game view for this session of dialogue star
        /// </summary>
        public IConfig GameView
        {
            get { return m_gameView; }
            set { m_gameView = value; }
        }
        private IConfig m_gameView;

        /// <summary>
        /// The game view for this session of dialogue star
        /// </summary>
        public IBranch Branch
        {
            get { return m_gameView.Project.DefaultBranch; }
        }

        /// <summary>
        /// The perforce object used by dialogue star
        /// </summary>
        public P4 P4
        {
            get { return m_p4; }
            set { m_p4 = value; }
        }
        private P4 m_p4;

        public System.Collections.ObjectModel.ObservableCollection<SearchAttribute> SearchAttributes
        {
            get { return m_searchAttributes; }
            set { m_searchAttributes = value; OnPropertyChanged("SearchAttributes"); }
        }
        private System.Collections.ObjectModel.ObservableCollection<SearchAttribute> m_searchAttributes;

        /// <summary>
        /// The path to the main config file for dialogue star.
        /// </summary>
        public String ConfigPath
        {
            get { return m_configPath; }
            set { m_configPath = value; }
        }
        private String m_configPath;

        /// <summary>
        /// the path to the dialogue star files
        /// </summary>
        public String AmericanDialogueDirectory
        {
            get { return m_americanDialogueDirectory; }
            set { m_americanDialogueDirectory = value; }
        }
        private String m_americanDialogueDirectory;

        /// <summary>
        /// The directory that the autosaves are kept
        /// </summary>
        public String AutoSaveDirectory
        {
            get { return m_autoSaveDirectory; }
            set { m_autoSaveDirectory = value; }
        }
        private String m_autoSaveDirectory;

        /// <summary>
        /// The timer that on elsapsed time will call the autosave routine
        /// </summary>
        public Timer AutoSaveTimer
        {
            get { return m_autoSaveTimer; }
            set { m_autoSaveTimer = value;  }
        }
        private Timer m_autoSaveTimer;

        /// <summary>
        /// List of valid models got from model_enums.sch script file
        /// </summary>
        public Dictionary<String, Double> ModelNames
        {
            get { return m_modelNames; }
            set { m_modelNames = value; }
        }
        private Dictionary<String, Double> m_modelNames;

        public System.Collections.ObjectModel.ObservableCollection<String> ModelStringNames
        {
            get { return m_modelStringNames; }
            set { m_modelStringNames = value; }
        }
        private System.Collections.ObjectModel.ObservableCollection<String> m_modelStringNames;

        private DialogueLanguage CurrentLanguage = DialogueLanguage.American;

        private string p4userName;
        #endregion // Properties and associate data

        #region Constructor(s)

        /// <summary>
        /// Default constructor, sets the members to their default values, creates the game view and the
        /// perforce object and makes sure that all paths are created using the gameview.
        /// </summary>
        public Controller()
        {
            // Create the log file for the application
            String path = Path.Combine(Directory.GetCurrentDirectory(), "DialogueStar.log");
            LogFactory.CreateUniversalLogFile(RSG.Base.Logging.Log.StaticLog);
            UniversalLogFile logFile = new UniversalLogFile(path, FileMode.CreateNew, FlushMode.Error, RSG.Base.Logging.Log.StaticLog);

            // Create the auto save directory this needs to be hard coded so that it is the same everytime.
            this.m_autoSaveDirectory = System.Environment.GetEnvironmentVariable("TEMP") + "\\DialogueStar_AutoSaves\\";
            Directory.CreateDirectory(this.m_autoSaveDirectory);

            // Load the configuration file, get the file path from the game view and grab latest before loading
            String currDir = Directory.GetCurrentDirectory();
            this.m_gameView = ConfigFactory.CreateConfig();
            this.m_configPath = Path.Combine(this.Branch.Assets, "Dialogue", "Config", "Configurations.xml");
            string configDirectory = Path.Combine(this.Branch.Assets, "Dialogue", "Config");
            this.m_americanDialogueDirectory = Path.Combine(this.Branch.Assets, "Dialogue", "American");

            String dialogueAssertPath = Path.Combine(this.Branch.Assets, "Dialogue\\...");

            if (!Directory.Exists(configDirectory))
                Directory.CreateDirectory(configDirectory);
            if (!Directory.Exists(this.m_americanDialogueDirectory))
                Directory.CreateDirectory(this.m_americanDialogueDirectory);
            if (!Directory.Exists(dialogueAssertPath))
                Directory.CreateDirectory(dialogueAssertPath);

            RSG.Base.Logging.Log.Log__Message("m_configPath {0}", this.m_configPath);
            this.P4 = new P4();
            this.P4.User = this.p4userName;
            RSG.Base.Logging.Log.Log__Message("{0} [{1}]: connect", P4.Port, P4.User);

            bool successful = false;
            string currentDirectory = Directory.GetCurrentDirectory();
            try
            {
                Directory.SetCurrentDirectory(m_gameView.Project.Root);
                this.P4.Connect();
                successful = this.GetLatestConfigFile(this.P4);

                if (successful)
                    P4.Run("sync", dialogueAssertPath);

                // Check that this is the latest version of Dialogue Star
                string applicationPath = Path.Combine(this.m_gameView.ToolsBin, "dialogueStar", "...");
                P4API.P4RecordSet assemblySet = P4.Run("sync", "-n", applicationPath);
                if (assemblySet.Records.Length > 0)
                {
                    MessageBox.Show(
                        "There is a new version of Dialogue Star available in perforce, please consider closing this session down and grabbing latest.",
                        "Version Warning",
                        MessageBoxButton.OK,
                        MessageBoxImage.Warning);
                }
            }
            catch (Exception ex)
            {
                 Log.Log__Exception(ex, "Unhandled Perforce exception while trying to connect to the p4 server.");
            }
            finally
            {
                Directory.SetCurrentDirectory(currentDirectory);
                P4.Disconnect();
            }

            if (!successful)
            {
                MessageBox.Show(
                        string.Format("Unable to open dialogue star without first grabbing the latest configuration file '{0}' and a perforce error has occurred.", m_configPath),
                        "Fatal Error",
                        MessageBoxButton.OK,
                        MessageBoxImage.Error);

                logFile.Flush();
                App.Current.Shutdown(1);
            }

            // Not loading the model enum file from the script anymore, just using the local file and if that doesn't exist warn the user.
            this.ModelNames = new Dictionary<String, double>();
            this.ModelStringNames = new System.Collections.ObjectModel.ObservableCollection<String>();

            String modelPath = Path.Combine(this.Branch.Script, "core", "game", "data", "model_enums.sch");
            if (System.IO.File.Exists(modelPath))
            {
                StreamReader streamReader = new StreamReader(modelPath);
                String line = streamReader.ReadLine();
                while (line != null)
                {
                    if (line.Contains('='))
                    {
                        line = line.TrimEnd(',');
                        int index = line.IndexOf('=');
                        String value = line.Substring(index + 1);
                        if (value.Contains(" "))
                        {
                            int breakIndex = value.IndexOf(" ");
                            value = value.Substring(0, breakIndex);
                            value = value.TrimEnd(',');
                        }

                        double enumValue = 0.0d;
                        if (Double.TryParse(value, out enumValue))
                        {
                            this.ModelNames.Add(line.Remove(index), enumValue);
                            this.ModelStringNames.Add(line.Remove(index));
                        }
                    }

                    line = streamReader.ReadLine();
                }

                streamReader.Close();
                streamReader = null;
            }

            this.m_configurations = new DialogueConfigurations(this.ConfigPath, this.m_gameView);
            this.m_missions = new System.Collections.ObjectModel.ObservableCollection<MissionDialogue>();
            this.m_undoManagers = new System.Collections.ObjectModel.ObservableCollection<UndoManagerObject>();
            this.m_searchAttributes = new System.Collections.ObjectModel.ObservableCollection<SearchAttribute>();
            this.m_selectedDialogue = null;
            this.m_selectedConversation = null;
            this.m_selectedLine = null;

            // Create the autosave timer, set the elapsed time to every 2 minutes and set the callback function for elapsed
            this.m_autoSaveTimer = new Timer();
            this.m_autoSaveTimer.Interval = 1000 * 60 * 2;
            this.m_autoSaveTimer.Elapsed += new ElapsedEventHandler(m_autoSaveTimer_Elapsed);
            this.m_autoSaveTimer.Enabled = true;
        }

        #endregion // Constructor(s)

        #region Public view function(s)

        /// <summary>
        /// Creates a new empty mission dialogue and adds it to the opened dictionary.
        /// </summary>
        public void CreateNewDialogue()
        {
            ++m_createdCount;
            String newDialogueName = "Dialogue" + (m_createdCount.ToString());

            // Create the empty dialogue, assign the new name to it and add it to the collection
            MissionDialogue newDialogue = new MissionDialogue(newDialogueName, newDialogueName, this.m_configurations);
            newDialogue.Name = newDialogueName;
            newDialogue.MissionName = newDialogueName;
            this.m_missions.Add(newDialogue);

            // Need to create the undo manager for this dialogue
            UndoManagerObject undoManager = new UndoManagerObject();
            undoManager.RegisterViewModel(newDialogue);
            this.m_undoManagers.Add(undoManager);

            this.AddNewConversationPrivately(newDialogue, -1);

            // Select the new dialogue sheet we have just created
            this.SelectedDialogue = newDialogue;
        }

        /// <summary>
        /// Closes the current dialogue tab that is selected. If no tab is selected then this doesn't
        /// so anything.
        /// </summary>
        public void CloseCurrentDialogue()
        {
            this.m_autoSaveTimer.Enabled = false;

            if (this.m_selectedDialogue == null)
            {
                this.m_autoSaveTimer.Enabled = true;
                return;
            }
            if (this.PromptSingleSave(this.SelectedDialogue) == false)
            {
                this.m_autoSaveTimer.Enabled = true;
                return;
            }

            // Need to remove the undomanager for this dialogue
            int index = this.m_missions.IndexOf(this.m_selectedDialogue);
            this.m_undoManagers[index].UnRegisterViewModel(this.m_selectedDialogue);
            this.m_undoManagers.RemoveAt(index);

            // Remove it from the collection of opened dialogue
            this.m_missions.Remove(this.m_selectedDialogue);

            this.m_autoSaveTimer.Enabled = true;
        }

        /// <summary>
        /// Closes the given dialogue.
        /// </summary>
        public void CloseDialogue(MissionDialogue dialogue)
        {
            this.m_autoSaveTimer.Enabled = false;

            if (dialogue == null)
            {
                this.m_autoSaveTimer.Enabled = true;
                return;
            }
            if (this.PromptSingleSave(dialogue) == false)
            {
                this.m_autoSaveTimer.Enabled = true;
                return;
            }

            // Need to remove the undomanager for this dialogue
            int index = this.m_missions.IndexOf(dialogue);
            this.m_undoManagers[index].UnRegisterViewModel(dialogue);
            this.m_undoManagers.RemoveAt(index);

            // Remove it from the collection of opened dialogue
            this.m_missions.Remove(dialogue);

            this.m_autoSaveTimer.Enabled = true;
        }

        /// <summary>
        /// Closes all of the tabs that are currently open.
        /// </summary>
        public Boolean CloseAllDialogue()
        {
            this.m_autoSaveTimer.Enabled = false;

            if (this.PromptMultiplySave() == false)
            {
                this.m_autoSaveTimer.Enabled = true;
                return false;
            }

            // Need to clear the undo manager collection as well making sure we unregister all the
            // event handlers first
            foreach (MissionDialogue mission in this.m_missions)
            {
                int index = this.m_missions.IndexOf(mission);
                this.m_undoManagers[index].UnRegisterViewModel(mission);
            }
            this.m_undoManagers.Clear();

            // Just clear the collection
            this.m_missions.Clear();

            this.m_autoSaveTimer.Enabled = true;

            return true;
        }

        /// <summary>
        /// Saves the current dialogue that is selected on the tab view. If
        /// there is no tab selected this doesn't do anything.
        /// </summary>
        public Boolean SaveDialogue(MissionDialogue dialogue, Boolean canContinue, ref String filename)
        {
            if (dialogue == null)
            {
                filename = String.Empty;
                return false;
            }

            List<String> errorMessages = new List<String>();
            if (!dialogue.IsMissionValid(ref errorMessages))
            {
                String errorMessage = String.Format("Unable to save the file {0} due to the following errors:\n\n", dialogue.Name);
                int index = 1;
                foreach (String error in errorMessages)
                {
                    errorMessage += String.Format("{0}. {1}\n", index.ToString(), error);
                    index++;
                }

                if (canContinue)
                {
                    errorMessage += "\nDouble you wish to continue with the operation?";

                    MessageBoxResult result = MessageBox.Show(errorMessage, "Save Error", MessageBoxButton.YesNo, MessageBoxImage.Error);
                    if (result == MessageBoxResult.No)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    MessageBoxResult result = MessageBox.Show(errorMessage, "Save Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return false;
                }
            }

            this.m_autoSaveTimer.Enabled = false;
            String previousDialogueName = dialogue.Name;

            // First make sure we have a valid filename to save this as, if not call the
            // save as function instead.
            if (dialogue.Filename == String.Empty)
            {
                this.SaveAsDialogue(dialogue, 1, canContinue, ref filename);
            }
            else
            {
                this.SaveDialogue(dialogue, previousDialogueName);
            }

            this.m_autoSaveTimer.Enabled = true;
            filename = dialogue.Filename;
            return true;
        }

        /// <summary>
        /// Saves the current dialogue that is selected on the tab view. If
        /// there is no tab selected this doesn't do anything. This function will always
        /// show the save dialogue so that you can select a location for the file.
        /// </summary>
        public Boolean SaveAsDialogue(MissionDialogue dialogue, int dummy, Boolean canContinue, ref String filename)
        {
            List<String> errorMessages = new List<String>();
            if (!dialogue.IsMissionValid(ref errorMessages))
            {
                String errorMessage = String.Format("Unable to save the file {0} due to the following errors:\n\n", dialogue.Name);
                int index = 1;
                foreach (String error in errorMessages)
                {
                    errorMessage += String.Format("{0}. {1}\n", index.ToString(), error);
                    index++;
                }

                if (canContinue)
                {
                    errorMessage += "\nDouble you wish to continue with the operation?";

                    MessageBoxResult result = MessageBox.Show(errorMessage, "Save Error", MessageBoxButton.YesNo, MessageBoxImage.Error);
                    if (result == MessageBoxResult.No)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    MessageBoxResult result = MessageBox.Show(errorMessage, "Save Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return false;
                }
            }

            if (dialogue == null)
            {
                filename = String.Empty;
                return false;
            }
            this.m_autoSaveTimer.Enabled = false;
            String previousDialogueName = dialogue.Name;

            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.DefaultExt = ".dstar";
            dlg.InitialDirectory = this.AmericanDialogueDirectory;
            dlg.Filter = "dialogue star project files (*.dstar)|*.dstar";

            Nullable<bool> dialogResult = dlg.ShowDialog();

            if (dialogResult == true)
            {
                // set filename
                dialogue.Filename = dlg.FileName;

                // save file
                this.SaveDialogue(dialogue, previousDialogueName);

                this.m_autoSaveTimer.Enabled = true;
                filename = dialogue.Filename;
                return true;
            }

            this.m_autoSaveTimer.Enabled = true;
            filename = String.Empty;
            return false;
        }

        /// <summary>
        /// Saves all of the dialogue star files that are currently opened.
        /// </summary>
        public List<String> SaveAll()
        {
            List<String> filenames = new List<String>();

            this.m_autoSaveTimer.Enabled = false;
            foreach (MissionDialogue dialogue in this.Missions)
            {
                if (dialogue.Filename == String.Empty)
                {
                    MessageBoxResult result = MessageBox.Show(dialogue.Name + " has not been saved before, do you wish to pick a save location for it now?", "Warning", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning);

                    if (result == MessageBoxResult.Yes)
                    {
                        if (dialogue.Filename == String.Empty)
                        {
                            String filename = String.Empty;
                            if (!this.SaveAsDialogue(dialogue, 1, true, ref filename))
                                break;
                        }
                        else
                        {
                            String filename = String.Empty;
                            if (!this.SaveDialogue(dialogue, true, ref filename))
                                break;
                        }
                    }
                    else if (result == MessageBoxResult.Cancel)
                    {
                        break;
                    }
                    else if (result == MessageBoxResult.No)
                    {
                        // We have now choosen not to save this dialogue so we no longer need the auto save file (if any exist) for it
                        // so delete it
                        String autoSaveFilename = Path.Combine(this.AutoSaveDirectory, dialogue.Name + "_Temp.das");
                        if (System.IO.File.Exists(autoSaveFilename) == true)
                        {
                            System.IO.File.Delete(autoSaveFilename);
                        }
                        continue;
                    }
                }
                else
                {
                    String filename = String.Empty;
                    if (!this.SaveDialogue(dialogue, true, ref filename))
                        break;
                }
                filenames.Add(dialogue.Filename);
            }

            this.m_autoSaveTimer.Enabled = true;
            return filenames;
        }

        /// <summary>
        /// Opens the open dialog and lets the user select a dstar file they wish to open
        /// </summary>
        public Boolean OpenDialogue(out List<String> filenames)
        {
            // Initialise the out parameter
            filenames = new List<String>();

            // Create OpenFileDialog
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            dlg.DefaultExt = ".dstar";
            dlg.Multiselect = true;
            dlg.Filter = "dialogue star project files (*.dstar)|*.dstar";

            // Display OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = dlg.ShowDialog();

            // Get the selected file name and load it
            if (result == true)
            {
                foreach (String filename in dlg.FileNames)
                {
                    // Make sure the filename exists and that it has a .dstar extension to it
                    if (!System.IO.File.Exists(filename) || Path.GetExtension(filename).ToLower() != ".dstar")
                    {
                        continue;
                    }

                    // Make sure it isn't already loaded, if it is just select it as the current mission dialogue.
                    MissionDialogue opened = this.IsFileAlreadyOpen(filename);

                    if (opened != null)
                    {
                        this.SelectedDialogue = opened;
                        filenames.Add(filename);
                        continue;
                    }
                    else
                    {
                        // Create the new dialogue by passing in the filename
                        MissionDialogue newDialogue = new MissionDialogue(filename, this.m_configurations);
                        this.m_missions.Add(newDialogue);

                        // Need to create the undo manager for this dialogue
                        UndoManagerObject undoManager = new UndoManagerObject();
                        undoManager.RegisterViewModel(newDialogue);
                        this.m_undoManagers.Add(undoManager);

                        // Select the new dialogue sheet we have just created
                        this.SelectedDialogue = newDialogue;

                        // Register the conversations and the lines to this undo manager as well.
                        foreach (Conversation conversation in newDialogue.Conversations)
                        {
                            foreach (Line line in conversation.Lines)
                            {
                                undoManager.RegisterViewModel(line);
                            }
                            undoManager.RegisterViewModel(conversation);
                        }
                    }

                    filenames.Add(filename);
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// Opens a new dialogue from the given filename and makes sure everything is
        /// linked to the correct undo manager
        /// </summary>
        /// <param name="filename"></param>
        public Boolean OpenDialogue(String filename)
        {
            // Make sure the filename exists and that it has a .dstar extension to it
            if (!System.IO.File.Exists(filename) || Path.GetExtension(filename).ToLower() != ".dstar")
                return false;

            // Make sure it isn't already loaded, if it is just select it as the current mission dialogue.
            MissionDialogue opened = this.IsFileAlreadyOpen(filename);
            if (opened != null)
            {
                this.SelectedDialogue = opened;
                return false;
            }

            // Create the new dialogue by passing in the filename
            MissionDialogue newDialogue = new MissionDialogue(filename, this.m_configurations);
            this.m_missions.Add(newDialogue);

            // Need to create the undo manager for this dialogue
            UndoManagerObject undoManager = new UndoManagerObject();
            undoManager.RegisterViewModel(newDialogue);
            this.m_undoManagers.Add(undoManager);

            // Select the new dialogue sheet we have just created
            this.SelectedDialogue = newDialogue;

            // Register the conversations and the lines to this undo manager as well.
            foreach (Conversation conversation in newDialogue.Conversations)
            {
                foreach (Line line in conversation.Lines)
                {
                    undoManager.RegisterViewModel(line);
                }
                undoManager.RegisterViewModel(conversation);
            }

            return true;
        }

        /// <summary>
        /// Exports all the mission type dsta files located in the dstar file directory to the three text files
        /// </summary>
        public void ExportMissionsForTranslation(bool placeHolder, bool bMultiplayer)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Title = "Save Export File";
            dialog.DefaultExt = ".txt";
            dialog.Filter = "dialogue export files (*.txt)|*.txt";
            dialog.OverwritePrompt = true;
            Nullable<bool> dialogResult = dialog.ShowDialog();
            if (dialogResult == false)
            {
                return;
            }

            this.m_autoSaveTimer.Enabled = false;

            if (this.PromptMultiplySave() == false)
            {
                this.m_autoSaveTimer.Enabled = true;
                return;
            }

            // First make sure that the dstar files are up to date
            String currDir = Directory.GetCurrentDirectory();
            this.P4.User = this.p4userName;
            RSG.Base.Logging.Log.Log__Message("{0} [{1}]: connect", P4.Port, P4.User);
            this.P4.Connect();
            try
            {
                Directory.SetCurrentDirectory(Path.GetDirectoryName(this.Configurations.RootExportPath));

                // Get latest dstar files
                String dialogueAssertPath = Path.Combine(this.Branch.Assets, "Dialogue\\...");
                P4.Run("sync", dialogueAssertPath);
            }
            catch (P4API.Exceptions.P4APIExceptions ex)
            {
                Log.Log__Exception(ex, "Unhandled Perforce exception");
            }
            finally
            {
                P4.Disconnect();
                Directory.SetCurrentDirectory(currDir);
            }

            // Get a list of all the files that need exporting from the dialogue star directory.
            String[] exportedFiles = Directory.GetFiles(this.AmericanDialogueDirectory, "*.dstar");


            TextWriter dialogueFile = new StreamWriter(dialog.FileName);
            Collection<MissionDialogue> dialogues = new Collection<MissionDialogue>();
            List<MissionDialogue> validDialogues = new List<MissionDialogue>();
            if (exportedFiles.Length > 0)
            {
                // Create a mission dialogue object for each file and export them
                foreach (String filename in exportedFiles)
                {
                    if (!this.IsFilenameARandomEvent(filename))
                    {
                        MissionDialogue dialogue = new MissionDialogue(filename, this.Configurations);
                        // Don't want to export the random events here.
                        if (dialogue.RandomEventMission == false)
                        {
                            if (dialogue.IsAmbient == false && dialogue.IsMultiplayer == bMultiplayer)
                            {
                                dialogues.Add(dialogue);
                            }
                        }
                    }
                }

                validDialogues = ValidateExport(dialogues);

                if (validDialogues.Count == 0)
                {
                    String emptyMessage = "No valid mission files to export.";
                    MessageBox.Show(emptyMessage);
                    return;
                }

                // Export the rest of the data into the three files
                foreach (MissionDialogue dialogue in validDialogues)
                {
                    dialogue.ExportForTranslation(placeHolder, dialogueFile, new Dictionary<String, int>());
                }
            }

            dialogueFile.WriteLine("[DUMMY_DLG]");
            dialogueFile.WriteLine("Dummy label.");
            dialogueFile.Close();

            String message = "Successfully exported " + validDialogues.Count + " mission files.";
            MessageBox.Show(message);
            dialogues = null;
            validDialogues = null;
            this.m_autoSaveTimer.Enabled = true;
        }

        /// <summary>
        /// Exports all the mission type dsta files located in the dstar file directory to the three text files
        /// </summary>
        public void ExportCurrentMissionsPlaceholder()
        {
            if (this.SelectedDialogue == null)
            {
                return;
            }

            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Title = "Save Export File";
            dialog.DefaultExt = ".txt";
            dialog.Filter = "dialogue export files (*.txt)|*.txt";
            dialog.OverwritePrompt = true;
            Nullable<bool> dialogResult = dialog.ShowDialog();
            if (dialogResult == false)
            {
                return;
            }

            this.m_autoSaveTimer.Enabled = false;

            if (this.PromptMultiplySave() == false)
            {
                this.m_autoSaveTimer.Enabled = true;
                return;
            }

            // First make sure that the dstar files are up to date
            String currDir = Directory.GetCurrentDirectory();
            this.P4.User = this.p4userName;
            RSG.Base.Logging.Log.Log__Message("{0} [{1}]: connect", P4.Port, P4.User);
            this.P4.Connect();
            try
            {
                Directory.SetCurrentDirectory(Path.GetDirectoryName(this.Configurations.RootExportPath));

                // Get latest dstar files
                String dialogueAssertPath = Path.Combine(this.Branch.Assets, "Dialogue\\...");
                P4.Run("sync", dialogueAssertPath);
            }
            catch (P4API.Exceptions.P4APIExceptions ex)
            {
                Log.Log__Exception(ex, "Unhandled Perforce exception");
            }
            finally
            {
                P4.Disconnect();
                Directory.SetCurrentDirectory(currDir);
            }

            // Get a list of all the files that need exporting from the dialogue star directory.
            String[] exportedFiles = Directory.GetFiles(this.AmericanDialogueDirectory, "*.dstar");

            TextWriter dialogueFile = new StreamWriter(dialog.FileName);

            Collection<MissionDialogue> dialogues = new Collection<MissionDialogue>() {  this.SelectedDialogue };
            List<MissionDialogue> validDialogues = ValidateExport(dialogues);

            if (validDialogues.Count == 0)
            {
                String emptyMessage = "No valid mission files to export.";
                MessageBox.Show(emptyMessage);
                return;
            }

            // Export the rest of the data into the three files
            foreach (MissionDialogue dialogue in validDialogues)
            {
                dialogue.ExportForTranslation(true, dialogueFile, new Dictionary<String, int>());
            }

            dialogueFile.WriteLine("[DUMMY_DLG]");
            dialogueFile.WriteLine("Dummy label.");
            dialogueFile.Close();

            String message = "Successfully exported " + validDialogues.Count + " mission files.";
            MessageBox.Show(message);
            dialogues = null;
            validDialogues = null;
            this.m_autoSaveTimer.Enabled = true;
        }

        /// <summary>
        /// Exports all the mission type dstar files located in the dstar file directory to the three text files
        /// </summary>
        public void ExportMissions(bool singleplayer, bool multiplayer)
        {
            if (singleplayer == false && multiplayer == false)
            {
                return;
            }

            this.m_autoSaveTimer.Enabled = false;

            if (this.PromptMultiplySave() == false)
            {
                this.m_autoSaveTimer.Enabled = true;
                return;
            }

            if (!this.ShowExportDialog(Application.Current.MainWindow))
            {
                return;
            }

            // First make sure that the export files are checked out of perforce.
            String currDir = Directory.GetCurrentDirectory();
            this.P4.User = this.p4userName;
            RSG.Base.Logging.Log.Log__Message("{0} [{1}]: connect", P4.Port, P4.User);

            Dictionary<ExportPath, string> exportPaths = this.Configurations.GetExportPaths(this.CurrentLanguage);
            bool canContinue = true;
            try
            {
                this.P4.Connect();
                Directory.SetCurrentDirectory(Path.GetDirectoryName(this.Configurations.RootExportPath));

                // Get latest dstar files
                P4.Run("sync", this.AmericanDialogueDirectory);

                // Get latest export files

                P4API.P4PendingChangelist changeList = P4.CreatePendingChangelist("Automatic mission dialogue star export");
                if (singleplayer)
                {
                    P4.Run("sync", exportPaths[ExportPath.MissionDialogue]);
                    P4.Run("sync", exportPaths[ExportPath.MissionDialogueFiles]);
                    P4.Run("sync", exportPaths[ExportPath.MissionMocap]);

                    P4.Run("edit", "-c", changeList.Number.ToString(), exportPaths[ExportPath.MissionDialogue]);
                    P4.Run("reopen", "-c", changeList.Number.ToString(), exportPaths[ExportPath.MissionDialogue]);
                    P4.Run("add", "-c", changeList.Number.ToString(), exportPaths[ExportPath.MissionDialogue]);
                    P4.Run("edit", "-c", changeList.Number.ToString(), exportPaths[ExportPath.MissionDialogueFiles]);
                    P4.Run("reopen", "-c", changeList.Number.ToString(), exportPaths[ExportPath.MissionDialogueFiles]);
                    P4.Run("add", "-c", changeList.Number.ToString(), exportPaths[ExportPath.MissionDialogueFiles]);
                    P4.Run("edit", "-c", changeList.Number.ToString(), exportPaths[ExportPath.MissionMocap]);
                    P4.Run("reopen", "-c", changeList.Number.ToString(), exportPaths[ExportPath.MissionMocap]);
                    P4.Run("add", "-c", changeList.Number.ToString(), exportPaths[ExportPath.MissionMocap]);
                }

                if (multiplayer)
                {
                    P4.Run("sync", exportPaths[ExportPath.NetDialogue]);
                    P4.Run("sync", exportPaths[ExportPath.NetDialogueFiles]);
                    P4.Run("sync", exportPaths[ExportPath.NetMocap]);

                    P4.Run("edit", "-c", changeList.Number.ToString(), exportPaths[ExportPath.NetDialogue]);
                    P4.Run("reopen", "-c", changeList.Number.ToString(), exportPaths[ExportPath.NetDialogue]);
                    P4.Run("add", "-c", changeList.Number.ToString(), exportPaths[ExportPath.NetDialogue]);
                    P4.Run("edit", "-c", changeList.Number.ToString(), exportPaths[ExportPath.NetDialogueFiles]);
                    P4.Run("reopen", "-c", changeList.Number.ToString(), exportPaths[ExportPath.NetDialogueFiles]);
                    P4.Run("add", "-c", changeList.Number.ToString(), exportPaths[ExportPath.NetDialogueFiles]);
                    P4.Run("edit", "-c", changeList.Number.ToString(), exportPaths[ExportPath.NetMocap]);
                    P4.Run("reopen", "-c", changeList.Number.ToString(), exportPaths[ExportPath.NetMocap]);
                    P4.Run("add", "-c", changeList.Number.ToString(), exportPaths[ExportPath.NetMocap]);
                }

                // Clear up any previous and now empty created changelists
                P4API.P4RecordSet currentChangeLists = P4.Run("changelists", "-l", "-s", "pending", "-u", P4.User, "-c", P4.Client);
                foreach (P4API.P4Record record in currentChangeLists.Records)
                {
                    if (record.Fields.ContainsKey("desc"))
                    {
                        String description = record.Fields["desc"];
                        if (description == "Automatic mission dialogue star export\n")
                        {
                            if (record.Fields.ContainsKey("change"))
                            {
                                P4.Run("change", "-d", record.Fields["change"]);
                            }
                        }
                    }
                }
            }
            catch (P4API.Exceptions.P4APIExceptions ex)
            {
                Log.Log__Exception(ex, "Unhandled Perforce exception");
                canContinue = false;
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Unhandled exception during export!");
                canContinue = false;
            }
            finally
            {
                P4.Disconnect();
                Directory.SetCurrentDirectory(currDir);
            }
            if (canContinue == false)
                return;

            // Get a list of all the files that need exporting from the dialogue star directory.
            String[] exportedFiles = Directory.GetFiles(this.AmericanDialogueDirectory, "*.dstar");

            Collection<MissionDialogue> singlePlayerDialogues = new Collection<MissionDialogue>();
            Collection<MissionDialogue> multiplayerDialogues = new Collection<MissionDialogue>();
            List<MissionDialogue> incompleteDialogues = new List<MissionDialogue>();
            List<MissionDialogue> missingSPFieldDialogues = new List<MissionDialogue>();
            List<MissionDialogue> missingRootDialogues = new List<MissionDialogue>();
            List<MissionDialogue> missingLineDialogues = new List<MissionDialogue>();
            int singleplayerExport = 0;
            int multiplayerExport = 0;
            if (exportedFiles.Length > 0)
            {
                // Create a mission dialogue object for each file and export them
                foreach (String filename in exportedFiles)
                {
                    if (!this.IsFilenameARandomEvent(filename))
                    {
                        MissionDialogue dialogue = new MissionDialogue(filename, this.Configurations);
                        // Don't want to export the random events here.
                        if (dialogue.RandomEventMission == false)
                        {
                            if (dialogue.IsMultiplayer && multiplayer)
                            {
                                multiplayerDialogues.Add(dialogue);
                            }
                            else if (singleplayer)
                            {
                                singlePlayerDialogues.Add(dialogue);
                            }
                        }
                    }
                }

                Hardware hardwareGeneration = Hardware.Generation8;
                IConfig config = ConfigFactory.CreateConfig();
                string rootExportPath = Path.GetFullPath(this.Configurations.RootExportPath);
                List<IProject> projects = new List<IProject>();
                projects.Add(config.Project);
                projects.AddRange(config.DLCProjects.Values);
                foreach (IProject project in projects)
                {
                    bool found = false;
                    foreach (IBranch branch in project.Branches.Values)
                    {
                        string assetsPath = Path.GetFullPath(branch.Assets) + @"\";
                        if (rootExportPath.StartsWith(assetsPath, StringComparison.OrdinalIgnoreCase))
                        {
                            if (!branch.Targets.ContainsKey(RSG.Platform.Platform.XboxOne))
                            {
                                if (!branch.Targets.ContainsKey(RSG.Platform.Platform.PS4))
                                {
                                    hardwareGeneration = Hardware.Generation7;
                                }
                            }

                            found = true;
                            break;
                        }
                    }

                    if (found)
                    {
                        break;
                    }
                }

                if (singleplayer)
                {
#if HACK_MP3
                // Create the textwriters
                TextWriter dialogueFile = new StreamWriter(exportPaths[ExportPath.MissionDialogue], false, Encoding.GetEncoding(1252));
                TextWriter mocapFile = new StreamWriter(exportPaths[ExportPath.MissionMocap], false, Encoding.GetEncoding(1252));
                TextWriter subtitleFile = new StreamWriter(exportPaths[ExportPath.MissionDialogueFiles], false, Encoding.GetEncoding(1252));
#else
                    TextWriter dialogueFile = new StreamWriter(exportPaths[ExportPath.MissionDialogue]);
                    TextWriter mocapFile = new StreamWriter(exportPaths[ExportPath.MissionMocap]);
                    TextWriter subtitleFile = new StreamWriter(exportPaths[ExportPath.MissionDialogueFiles]);
#endif

                    singleplayerExport = this.ExportDialogueFiles(singlePlayerDialogues, dialogueFile, mocapFile, subtitleFile, incompleteDialogues, missingLineDialogues, missingRootDialogues, missingLineDialogues, false, hardwareGeneration);
                    if (singleplayerExport == 0 && singlePlayerDialogues.Count != 0)
                    {
                        String emptyMessage = "No valid singleplayer mission files to export.";
                        MessageBox.Show(emptyMessage);
                    }
                }

                if (multiplayer)
                {
#if HACK_MP3
                // Create the textwriters
                TextWriter netDialogueFile = new StreamWriter(exportPaths[ExportPath.NetDialogue], false, Encoding.GetEncoding(1252));
                TextWriter netMocapFile = new StreamWriter(exportPaths[ExportPath.NetMocap], false, Encoding.GetEncoding(1252));
                TextWriter netSubtitleFile = new StreamWriter(exportPaths[ExportPath.NetDialogueFiles], false, Encoding.GetEncoding(1252));
#else
                    TextWriter netDialogueFile = new StreamWriter(exportPaths[ExportPath.NetDialogue]);
                    TextWriter netMocapFile = new StreamWriter(exportPaths[ExportPath.NetMocap]);
                    TextWriter netSubtitleFile = new StreamWriter(exportPaths[ExportPath.NetDialogueFiles]);
#endif

                    multiplayerExport = this.ExportDialogueFiles(multiplayerDialogues, netDialogueFile, netMocapFile, netSubtitleFile, incompleteDialogues, missingLineDialogues, missingRootDialogues, missingLineDialogues, true, hardwareGeneration);
                    if (multiplayerExport == 0 && multiplayerDialogues.Count != 0)
                    {
                        String emptyMessage = "No valid multiplayer mission files to export.";
                        MessageBox.Show(emptyMessage);
                    }
                }
            }

            String message = "Successfully exported ";
            if (singleplayer)
            {
                message += singleplayerExport + "singleplayer";
            }

            if (multiplayer)
            {
                if (singleplayer)
                {
                    message += " and ";
                }

                message += multiplayerExport + "multiplayer";
            }

            message += " mission files.";

            if (incompleteDialogues.Count > 0)
            {
                message += "\n\nWARNING: The following Missions have missing character IDs.";
                foreach (MissionDialogue dialogue in incompleteDialogues)
                {
                    message += "\n" + dialogue.Name;
                }
            }
            if (missingSPFieldDialogues.Count > 0)
            {
                message += "\n\nWARNING: The following Missions have blank speaker or listener fields.";
                foreach (MissionDialogue dialogue in missingSPFieldDialogues)
                {
                    message += "\n" + dialogue.Name;
                }
            }
            if (missingRootDialogues.Count > 0)
            {
                message += "\n\nWARNING: The following Missions contain conversations with blank root fields.";
                foreach (MissionDialogue dialogue in missingRootDialogues)
                {
                    message += "\n" + dialogue.Name;
                }
            }
            if (missingLineDialogues.Count > 0)
            {
                message += "\n\nWARNING: The following Missions contain lines with blank dialogue.";
                foreach (MissionDialogue dialogue in missingLineDialogues)
                {
                    message += "\n" + dialogue.Name;
                }
            }
            MessageBox.Show(message);
            singlePlayerDialogues = null;

            this.m_autoSaveTimer.Enabled = true;
        }

        private int ExportDialogueFiles(
            Collection<MissionDialogue> dialogues,
            TextWriter dialogueFile,
            TextWriter mocapFile,
            TextWriter subtitleFile,
            List<MissionDialogue> incompleteDialogues,
            List<MissionDialogue> missingSPFieldDialogues,
            List<MissionDialogue> missingRootDialogues,
            List<MissionDialogue> missingLineDialogues,
            bool multiplayer,
            Hardware hardwareGeneration)
        {
            List<MissionDialogue> validDialogues = ValidateExport(dialogues);
            if (validDialogues.Count != 0)
            {

                // First export the subtitle id list into the files file
                subtitleFile.WriteLine("start");
                foreach (MissionDialogue dialogue in validDialogues)
                {
                    dialogue.ExportSubtitleID(subtitleFile);
                }
                subtitleFile.WriteLine("end");
                subtitleFile.WriteLine("");

                // Export the rest of the data into the three files
                foreach (MissionDialogue dialogue in validDialogues)
                {
                    Boolean missingSPFields = false;
                    Boolean missingRoot = false;
                    Boolean missingLine = false;
                    if (dialogue.Export(dialogueFile, mocapFile, subtitleFile, new Dictionary<String, int>(), ref missingSPFields, ref missingRoot, ref missingLine, hardwareGeneration))
                    {
                        incompleteDialogues.Add(dialogue);
                    }
                    if (missingSPFields == true)
                    {
                        missingSPFieldDialogues.Add(dialogue);
                    }
                    if (missingRoot == true)
                    {
                        missingRootDialogues.Add(dialogue);
                    }
                    if (missingLine == true)
                    {
                        missingLineDialogues.Add(dialogue);
                    }
                }
            }

            if (multiplayer)
            {
                dialogueFile.WriteLine("[DUMMY_MPDLG]");
                mocapFile.WriteLine("[DUMMY_MPMOCAP]");
                subtitleFile.WriteLine("[DUMMY_MPDLGFILE]");
            }
            else
            {
                dialogueFile.WriteLine("[DUMMY_DLG]");
                mocapFile.WriteLine("[DUMMY_MOCAP]");
                subtitleFile.WriteLine("[DUMMY_DLGFILE]");
            }

            dialogueFile.WriteLine("Dummy label.");
            dialogueFile.Close();

            mocapFile.WriteLine("Dummy label.");
            mocapFile.Close();

            subtitleFile.WriteLine("Dummy label.");
            subtitleFile.Close();

            return validDialogues.Count;
        }

        /// <summary>
        /// Exports the random events for the writers and voice actors, shows a window before exporting so that the user
        /// can select which voices to export.
        /// </summary>
        public void ExportForWriters(Object sender)
        {
            this.m_autoSaveTimer.Enabled = false;

            if (this.PromptMultiplySave() == false)
            {
                this.m_autoSaveTimer.Enabled = true;
                return;
            }

            // First make sure that the dstar files are up to date
            String currDir = Directory.GetCurrentDirectory();
            this.P4.User = this.p4userName;
            RSG.Base.Logging.Log.Log__Message("{0} [{1}]: connect", P4.Port, P4.User);
            this.P4.Connect();
            try
            {
                Directory.SetCurrentDirectory(Path.GetDirectoryName(this.Configurations.RootExportPath));

                // Get latest dstar files
                String dialogueAssertPath = Path.Combine(this.Branch.Assets, "Dialogue\\...");
                P4.Run("sync", dialogueAssertPath);
            }
            catch (P4API.Exceptions.P4APIExceptions ex)
            {
                Log.Log__Exception(ex, "Unhandled Perforce exception");
            }
            finally
            {
                P4.Disconnect();
                Directory.SetCurrentDirectory(currDir);
            }

            // Get a list of all the files that need exporting from the dialogue star directory.
            String[] exportedFiles = Directory.GetFiles(this.AmericanDialogueDirectory, "*.dstar");

            // For this export we need a list of all conversations in alphapetic order based on the voice field.
            SortedDictionary<String, Collection<MissionDialogue>> voiceDataDictionary = new SortedDictionary<String, Collection<MissionDialogue>>();

            Collection<MissionDialogue> dialogues = new Collection<MissionDialogue>();
            if (exportedFiles.Length > 0)
            {
                // Create a mission dialogue object for each file
                foreach (String filename in exportedFiles)
                {
                    if (this.IsFilenameARandomEvent(filename))
                    {
                        MissionDialogue dialogue = new MissionDialogue(filename, this.Configurations);
                        // Want to include only the random events here.
                        if (dialogue.RandomEventMission == true)
                            dialogues.Add(dialogue);
                    }
                }

                foreach (MissionDialogue dialogue in dialogues)
                {
                    foreach (Conversation conversation in dialogue.Conversations)
                    {
                        String voiceValue = conversation.Voice;
                        if (voiceValue == String.Empty)
                            continue;

                        if (voiceDataDictionary.ContainsKey(voiceValue.ToLower()))
                        {
                            if (!voiceDataDictionary[voiceValue.ToLower()].Contains(dialogue))
                            {
                                voiceDataDictionary[voiceValue.ToLower()].Add(dialogue);
                            }
                        }
                        else
                        {
                            voiceDataDictionary.Add(voiceValue.ToLower(), new Collection<MissionDialogue>());
                            voiceDataDictionary[voiceValue.ToLower()].Add(dialogue);
                        }
                    }
                }

                if (ShowVoicesExportDialog(sender, voiceDataDictionary) == false)
                {
                    voiceDataDictionary = null;
                    dialogues = null;
                    return;
                }
                else
                {
                    if (voiceDataDictionary.Count == 0)
                    {
                        voiceDataDictionary = null;
                        dialogues = null;
                        return;
                    }
                }
                (sender as Window).Cursor = System.Windows.Input.Cursors.Wait;

                // Create the word document from the above sorted dictionary
                int tableRowCount = 0;
                List<int> tableRowCounts = new List<int>();
                Dictionary<String, Dictionary<String, List<Conversation>>> resolvedContextBlocks = new Dictionary<String, Dictionary<String, List<Conversation>>>();
                foreach (KeyValuePair<String, Collection<MissionDialogue>> voiceData in voiceDataDictionary)
                {
                    resolvedContextBlocks.Add(voiceData.Key, new Dictionary<String, List<Conversation>>());
                    tableRowCount = 1; // Row for voice information
                    foreach (MissionDialogue dialogue in voiceData.Value)
                    {
                        tableRowCount += 2; // 2 rows for every mission name headers
                        Dictionary<String, List<Conversation>> contextBlocks = new Dictionary<string, List<Conversation>>();
                        foreach (Conversation conversation in dialogue.Conversations)
                        {
                            if (!contextBlocks.ContainsKey(conversation.Root))
                                contextBlocks.Add(conversation.Root, new List<Conversation>());

                            contextBlocks[conversation.Root].Add(conversation);
                        }
                        foreach (KeyValuePair<String, List<Conversation>> contextBlock in contextBlocks)
                        {
                            if (contextBlock.Value.Count > 0)
                            {
                                resolvedContextBlocks[voiceData.Key].Add(contextBlock.Key, new List<Conversation>());
                                foreach (Conversation conversation in contextBlock.Value)
                                {
                                    if (conversation.Voice.ToLower() == voiceData.Key)
                                    {
                                        resolvedContextBlocks[voiceData.Key][contextBlock.Key].Add(conversation);
                                    }
                                }
                                if (resolvedContextBlocks[voiceData.Key][contextBlock.Key].Count == 0)
                                {
                                    resolvedContextBlocks[voiceData.Key][contextBlock.Key].Add(contextBlock.Value[0]);
                                }
                            }
                        }
                        foreach (List<Conversation> contextBlockConversations in resolvedContextBlocks[voiceData.Key].Values)
                        {
                            foreach (Conversation conversation in contextBlockConversations)
                            {
                                foreach (Line line in conversation.Lines)
                                {
                                    if (line.Filename != String.Empty && line.CharacterName != "SFX")
                                    {
                                        tableRowCount += 1;
                                    }
                                }
                            }
                        }
                    }
                    tableRowCounts.Add(tableRowCount);
                }

                Microsoft.Office.Interop.Word.Application WordApp = new Microsoft.Office.Interop.Word.Application();
                WordApp.Visible = true;
                Object missing = System.Reflection.Missing.Value;
                Object end = "\\endofdoc"; /* \endofdoc is a predefined bookmark */
                Object start = 0;

                Microsoft.Office.Interop.Word.Document document = WordApp.Documents.Add(ref missing, ref missing, ref missing, ref missing);
                Microsoft.Office.Interop.Word.Range range = document.Range(ref start, ref missing);

                int tableIndex = 0;
                foreach (KeyValuePair<String, Collection<MissionDialogue>> voiceData in voiceDataDictionary)
                {
                    int startingRowIndex = 1;
                    Microsoft.Office.Interop.Word.Table table;
                    Microsoft.Office.Interop.Word.Range tableRange = document.Bookmarks.get_Item(ref end).Range;
                    table = document.Tables.Add(tableRange, tableRowCounts[tableIndex], 5, ref missing, ref missing);
                    table.Range.ParagraphFormat.SpaceAfter = 6;
                    table.PreferredWidthType = Microsoft.Office.Interop.Word.WdPreferredWidthType.wdPreferredWidthPercent;
                    table.PreferredWidth = 100;
                    Object tableStyle = "Table Grid";
                    table.set_Style(ref tableStyle);
                    tableIndex++;

                    table.Rows[startingRowIndex].Range.Font.Name = "Calibri";
                    table.Rows[startingRowIndex].Range.Font.Bold = 1;
                    table.Rows[startingRowIndex].Range.Font.Size = 11.0f;
                    table.Rows[startingRowIndex].Cells.Merge();
                    table.Rows[startingRowIndex].Cells[1].Range.Text = "RANDOM EVENT DIALOGUE: " + voiceData.Key;

                    startingRowIndex += 1;

                    foreach (MissionDialogue dialogue in voiceData.Value)
                    {
                        table.Rows[startingRowIndex].Range.Font.Name = "Calibri";
                        table.Rows[startingRowIndex].Range.Font.Bold = 1;
                        table.Rows[startingRowIndex].Range.Font.Size = 11.0f;
                        table.Rows[startingRowIndex].Cells.Merge();
                        table.Rows[startingRowIndex].Cells[1].Range.Text = dialogue.MissionName;

                        startingRowIndex += 1;

                        table.Rows[startingRowIndex].Range.Font.Name = "Calibri";
                        table.Rows[startingRowIndex].Range.Font.Bold = 1;
                        table.Rows[startingRowIndex].Range.Font.Size = 11.0f;
                        table.Rows[startingRowIndex].Cells[1].Range.Text = "Voice/Character (Role)";
                        table.Rows[startingRowIndex].Cells[2].Range.Text = "Context";
                        table.Rows[startingRowIndex].Cells[3].Range.Text = "Filename";
                        table.Rows[startingRowIndex].Cells[4].Range.Text = "Request Line - PLEASE REWRITE";
                        table.Rows[startingRowIndex].Cells[5].Range.Text = "Recorded";

                        startingRowIndex += 1;

                        List<String> contextBlocks = new List<String>();
                        foreach (Conversation conversation in dialogue.Conversations)
                        {
                            if (!contextBlocks.Contains(conversation.Root))
                                contextBlocks.Add(conversation.Root);
                        }
                        foreach (String contextBlock in contextBlocks)
                        {
                            foreach (Conversation conversation in resolvedContextBlocks[voiceData.Key][contextBlock])
                            {
                                if (conversation.Voice != String.Empty || (conversation.Voice == String.Empty && conversation.UseVoice == false))
                                {
                                    foreach (Line line in conversation.Lines)
                                    {
                                        if (line.Filename != String.Empty && line.CharacterName != "SFX")
                                        {
                                            if (voiceData.Key != conversation.Voice.ToLower())
                                            {
                                                table.Rows[startingRowIndex].Range.Font.Name = "Calibri";
                                                table.Rows[startingRowIndex].Range.Font.Bold = 0;
                                                table.Rows[startingRowIndex].Range.Font.Italic = 1;
                                                table.Rows[startingRowIndex].Range.Font.Size = 8.0f;

                                                table.Rows[startingRowIndex].Cells[1].Range.HighlightColorIndex = Microsoft.Office.Interop.Word.WdColorIndex.wdGray25;
                                                table.Rows[startingRowIndex].Cells[1].Range.Text = (conversation.UseVoice == true ? conversation.Voice + " (" + conversation.Role + ")" : line.CharacterName + " (" + conversation.Role + ")");

                                                table.Rows[startingRowIndex].Cells[2].Range.HighlightColorIndex = Microsoft.Office.Interop.Word.WdColorIndex.wdGray25;
                                                table.Rows[startingRowIndex].Cells[2].Range.Text = conversation.Description;

                                                table.Rows[startingRowIndex].Cells[3].Range.HighlightColorIndex = Microsoft.Office.Interop.Word.WdColorIndex.wdGray25;
                                                table.Rows[startingRowIndex].Cells[3].Range.Text = line.Filename;

                                                table.Rows[startingRowIndex].Cells[4].Range.HighlightColorIndex = Microsoft.Office.Interop.Word.WdColorIndex.wdGray25;
                                                table.Rows[startingRowIndex].Cells[4].Range.Text = line.LineDialogue;

                                                table.Rows[startingRowIndex].Cells[5].Range.HighlightColorIndex = Microsoft.Office.Interop.Word.WdColorIndex.wdGray25;
                                                table.Rows[startingRowIndex].Cells[5].Range.Text = (line.Recorded == true ? "Yes" : "No");
                                            }
                                            else
                                            {
                                                table.Rows[startingRowIndex].Range.Font.Name = "Calibri";
                                                table.Rows[startingRowIndex].Range.Font.Bold = 0;
                                                table.Rows[startingRowIndex].Range.Font.Italic = 0;
                                                table.Rows[startingRowIndex].Range.Font.Size = 11.0f;

                                                table.Rows[startingRowIndex].Cells[1].Range.Text = (conversation.UseVoice == true ? conversation.Voice + " (" + conversation.Role + ")" : line.CharacterName + " (" + conversation.Role + ")");
                                                table.Rows[startingRowIndex].Cells[2].Range.Text = conversation.Description;
                                                table.Rows[startingRowIndex].Cells[3].Range.Text = line.Filename;
                                                table.Rows[startingRowIndex].Cells[4].Range.Text = line.LineDialogue;
                                                table.Rows[startingRowIndex].Cells[5].Range.Text = (line.Recorded == true ? "Yes" : "No");
                                            }
                                            startingRowIndex += 1;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (tableIndex != tableRowCounts.Count)
                    {
                        document.Bookmarks.get_Item(ref end).Range.InsertParagraphAfter();
                        tableRange = document.Bookmarks.get_Item(ref end).Range;
                        Object collapseEnd = Microsoft.Office.Interop.Word.WdCollapseDirection.wdCollapseEnd;
                        Object pageBreak = Microsoft.Office.Interop.Word.WdBreakType.wdPageBreak;
                        tableRange.Collapse(ref collapseEnd);
                        tableRange.InsertBreak(pageBreak);
                        tableRange.Collapse(ref collapseEnd);
                    }
                }
            }

            MessageBox.Show("Successfully exported " + voiceDataDictionary.Count + " voices for the writers and voice actors.");
            dialogues = null;

            this.m_autoSaveTimer.Enabled = true;
        }

        /// <summary>
        /// Exports all the random events using both identifiers and the normal method.
        /// </summary>
        public void ExportAllRandomMissions()
        {
            this.m_autoSaveTimer.Enabled = false;

            if (this.PromptMultiplySave() == false)
            {
                this.m_autoSaveTimer.Enabled = true;
                return;
            }

            if (!this.ShowExportDialog(Application.Current.MainWindow))
            {
                return;
            }

            // First make sure that the export files are checked out of perforce.
            String currDir = Directory.GetCurrentDirectory();
            this.P4.User = this.p4userName;
            RSG.Base.Logging.Log.Log__Message("{0} [{1}]: connect", P4.Port, P4.User);
            this.P4.Connect();
            Dictionary<ExportPath, string> exportPaths = this.Configurations.GetExportPaths(this.CurrentLanguage);
            bool canContinue = true;
            try
            {
                Directory.SetCurrentDirectory(Path.GetDirectoryName(this.Configurations.RootExportPath));

                // Get latest dstar files
                String dialogueAssertPath = Path.Combine(this.Branch.Assets, "Dialogue\\...");
                P4.Run("sync", dialogueAssertPath);

                // Get latest export file
                P4.Run("sync", exportPaths[ExportPath.RandomDialogue]);
                P4.Run("sync", exportPaths[ExportPath.RandomDialogueFiles]);
                P4.Run("sync", exportPaths[ExportPath.RandomMocap]);
                P4.Run("sync", exportPaths[ExportPath.RandomIdentifiers]);

                // Open the file for edit
                P4API.P4PendingChangelist changeList = P4.CreatePendingChangelist("Automatic random event dialogue star export");
                P4.Run("edit", "-c", changeList.Number.ToString(), exportPaths[ExportPath.RandomDialogue]);
                P4.Run("reopen", "-c", changeList.Number.ToString(), exportPaths[ExportPath.RandomDialogue]);
                P4.Run("add", "-c", changeList.Number.ToString(), exportPaths[ExportPath.RandomDialogue]);

                P4.Run("edit", "-c", changeList.Number.ToString(), exportPaths[ExportPath.RandomDialogueFiles]);
                P4.Run("reopen", "-c", changeList.Number.ToString(), exportPaths[ExportPath.RandomDialogueFiles]);
                P4.Run("add", "-c", changeList.Number.ToString(), exportPaths[ExportPath.RandomDialogueFiles]);

                P4.Run("edit", "-c", changeList.Number.ToString(), exportPaths[ExportPath.RandomMocap]);
                P4.Run("reopen", "-c", changeList.Number.ToString(), exportPaths[ExportPath.RandomMocap]);
                P4.Run("add", "-c", changeList.Number.ToString(), exportPaths[ExportPath.RandomMocap]);

                P4.Run("edit", "-c", changeList.Number.ToString(), exportPaths[ExportPath.RandomIdentifiers]);
                P4.Run("reopen", "-c", changeList.Number.ToString(), exportPaths[ExportPath.RandomIdentifiers]);
                P4.Run("add", "-c", changeList.Number.ToString(), exportPaths[ExportPath.RandomIdentifiers]);

                // Clear up any previous and now empty created changelists
                P4API.P4RecordSet currentChangeLists = P4.Run("changelists", "-l", "-s", "pending", "-u", P4.User, "-c", P4.Client);
                foreach (P4API.P4Record record in currentChangeLists.Records)
                {
                    if (record.Fields.ContainsKey("desc"))
                    {
                        String description = record.Fields["desc"];
                        if (description == "Automatic random event dialogue star export\n")
                        {
                            if (record.Fields.ContainsKey("change"))
                            {
                                P4.Run("change", "-d", record.Fields["change"]);
                            }
                        }
                    }
                }
            }
            catch (P4API.Exceptions.P4APIExceptions ex)
            {
                Log.Log__Exception(ex, "Unhandled Perforce exception");
                canContinue = false;
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Unhandled exception during export!");
                canContinue = false;
            }
            finally
            {
                P4.Disconnect();
                Directory.SetCurrentDirectory(currDir);
            }
            if (canContinue == false)
                return;

            // Get a list of all the files that need exporting from the dialogue star directory.
            String[] exportedFiles = Directory.GetFiles(this.AmericanDialogueDirectory, "*.dstar");

            List<String> incompleteDialogues = new List<String>();
            List<MissionDialogue> missingCharacterDialogues = new List<MissionDialogue>();
            List<MissionDialogue> missingSPFieldDialogues = new List<MissionDialogue>();
            List<MissionDialogue> missingRootDialogues = new List<MissionDialogue>();
            List<MissionDialogue> missingLines = new List<MissionDialogue>();
            Collection<MissionDialogue> dialogues = new Collection<MissionDialogue>();
            List<MissionDialogue> validDialogues = new List<MissionDialogue>();
            if (exportedFiles.Length > 0)
            {
                // Create a mission dialogue object for each file and export them
                foreach (String filename in exportedFiles)
                {
                    if (IsFilenameARandomEvent(filename))
                    {
                        MissionDialogue dialogue = new MissionDialogue(filename, this.Configurations);
                        // Want to export only the random events here.
                        if (dialogue.RandomEventMission == true)
                        {
                            dialogues.Add(dialogue);
                            System.Diagnostics.Debug.Print(dialogue.Name);
                        }
                    }
                }

#if HACK_MP3
                // Create the textwriters
                TextWriter mocapFile = new StreamWriter(exportPaths[ExportPath.RandomMocap], false, Encoding.GetEncoding(1252));
                TextWriter subtitleFile = new StreamWriter(exportPaths[ExportPath.RandomDialogueFiles], false, Encoding.GetEncoding(1252));
                TextWriter dialogueFile = new StreamWriter(exportPaths[ExportPath.RandomDialogue], false, Encoding.GetEncoding(1252));
                TextWriter exportFile = new StreamWriter(exportPaths[ExportPath.RandomIdentifiers], false, Encoding.GetEncoding(1252));
#else
                TextWriter mocapFile = new StreamWriter(exportPaths[ExportPath.RandomMocap]);
                TextWriter subtitleFile = new StreamWriter(exportPaths[ExportPath.RandomDialogueFiles]);
                TextWriter dialogueFile = new StreamWriter(exportPaths[ExportPath.RandomDialogue]);
                TextWriter exportFile = new StreamWriter(exportPaths[ExportPath.RandomIdentifiers]);
#endif

                validDialogues = ValidateExport(dialogues);
                if (validDialogues.Count == 0)
                {
                    // Add the dummy lines onto the files
                    exportFile.WriteLine("[DUMMY_RANDOMIDS]");
                    exportFile.WriteLine("Dummy label.");
                    exportFile.Close();

                    dialogueFile.WriteLine("[DUMMY_RANDOMDLG]");
                    dialogueFile.WriteLine("Dummy label.");
                    dialogueFile.Close();

                    mocapFile.WriteLine("[DUMMY_RNDMOCAP]");
                    mocapFile.WriteLine("Dummy label.");
                    mocapFile.Close();

                    subtitleFile.WriteLine("[DUMMY_RNDFILE]");
                    subtitleFile.WriteLine("Dummy label.");
                    subtitleFile.Close();

                    return;
                }

                // First export the subtitle id list into the files file
                subtitleFile.WriteLine("start");
                foreach (MissionDialogue dialogue in validDialogues)
                {
                    dialogue.ExportSubtitleID(subtitleFile);
                }
                subtitleFile.WriteLine("end");
                subtitleFile.WriteLine("");

                Dictionary<String, int> characterIndices = new Dictionary<String, int>();
                List<String> exportedCharacters = new List<String>();
                List<String> exportedVoices = new List<String>();

                Hardware hardwareGeneration = Hardware.Generation8;
                IConfig config = ConfigFactory.CreateConfig();
                string rootExportPath = Path.GetFullPath(this.Configurations.RootExportPath);
                List<IProject> projects = new List<IProject>();
                projects.Add(config.Project);
                projects.AddRange(config.DLCProjects.Values);
                foreach (IProject project in projects)
                {
                    bool found = false;
                    foreach (IBranch branch in project.Branches.Values)
                    {
                        string assetsPath = Path.GetFullPath(branch.Assets) + @"\";
                        if (rootExportPath.StartsWith(assetsPath, StringComparison.OrdinalIgnoreCase))
                        {
                            if (!branch.Targets.ContainsKey(RSG.Platform.Platform.XboxOne))
                            {
                                if (!branch.Targets.ContainsKey(RSG.Platform.Platform.PS4))
                                {
                                    hardwareGeneration = Hardware.Generation7;
                                }
                            }

                            found = true;
                            break;
                        }
                    }

                    if (found)
                    {
                        break;
                    }
                }

                foreach (MissionDialogue dialogue in validDialogues)
                {
                    characterIndices.Clear();
                    exportedCharacters.Clear();
                    exportedVoices.Clear();
                    characterIndices.Add("PLAYER_ZERO", 0);
                    characterIndices.Add("PLAYER_ONE", 1);
                    characterIndices.Add("PLAYER_TWO", 2);
                    int nextCharacterIndex = 3;

                    exportFile.WriteLine("");
                    exportFile.WriteLine("{" + dialogue.MissionName + "}");
                    exportFile.WriteLine("");

                    exportFile.WriteLine("[" + "PRE" + "_" + dialogue.MissionSubtitleId + "]");
                    exportFile.WriteLine(dialogue.MissionId);
                    exportFile.WriteLine("");

                    foreach (Conversation conversation in dialogue.Conversations)
                    {
                        if (this.ModelNames.ContainsKey(conversation.ModelName))
                        {
                            if (!exportedCharacters.Contains(conversation.ModelName))
                            {
                                exportFile.WriteLine("{" + conversation.ModelName + "}");
                                exportFile.WriteLine("[" + dialogue.MissionId + "_" + this.ModelNames[conversation.ModelName] + ":" + dialogue.MissionSubtitleId + "]");
                                if (!characterIndices.ContainsKey(conversation.ModelName))
                                {
                                    characterIndices.Add(conversation.ModelName, nextCharacterIndex);
                                    nextCharacterIndex++;
                                }
                                exportFile.WriteLine(characterIndices[conversation.ModelName].ToString());
                                exportFile.WriteLine("");

                                exportedCharacters.Add(conversation.ModelName);
                            }
                        }
                    }

                    foreach (Conversation conversation in dialogue.Conversations)
                    {
                        if (this.ModelNames.ContainsKey(conversation.ModelName))
                        {
                            if (!exportedVoices.Contains(conversation.ModelName) && !String.IsNullOrEmpty(conversation.Voice))
                            {
                                exportFile.WriteLine("[" + dialogue.MissionId + "_" + "VOICE" + "_" + this.ModelNames[conversation.ModelName] + ":" + dialogue.MissionSubtitleId + "]");
                                exportFile.WriteLine(conversation.Voice);
                                exportFile.WriteLine("");

                                exportedVoices.Add(conversation.ModelName);
                            }
                        }

                        if (String.IsNullOrEmpty(conversation.Voice) && conversation.UseVoice == true && !incompleteDialogues.Contains(dialogue.Name))
                        {
                            incompleteDialogues.Add(dialogue.Name);
                        }
                    }

                    // Export the rest of the data into the three files using this dialogues characterIndices
                    Boolean missingSPFields = false;
                    Boolean missingRoot = false;
                    Boolean missingLine = false;
                    if (dialogue.Export(dialogueFile, mocapFile, subtitleFile, characterIndices, ref missingSPFields, ref missingRoot, ref missingLine, hardwareGeneration))
                    {
                        missingCharacterDialogues.Add(dialogue);
                    }
                    if (missingSPFields == true)
                    {
                        missingSPFieldDialogues.Add(dialogue);
                    }
                    if (missingRoot == true)
                    {
                        missingRootDialogues.Add(dialogue);
                    }
                    if (missingLine == true)
                    {
                        missingLines.Add(dialogue);
                    }
                }

                // Add the dummy lines onto the files
                exportFile.WriteLine("[DUMMY_RANDOMIDS]");
                exportFile.WriteLine("Dummy label.");
                exportFile.Close();

                dialogueFile.WriteLine("[DUMMY_RANDOMDLG]");
                dialogueFile.WriteLine("Dummy label.");
                dialogueFile.Close();

                mocapFile.WriteLine("[DUMMY_RNDMOCAP]");
                mocapFile.WriteLine("Dummy label.");
                mocapFile.Close();

                subtitleFile.WriteLine("[DUMMY_RNDFILE]");
                subtitleFile.WriteLine("Dummy label.");
                subtitleFile.Close();
            }

            String message = "Successfully exported " + validDialogues.Count + " mission files.";
            if (incompleteDialogues.Count > 0)
            {
                message += "\n\nWARNING: The following Missions have missing voice data.";
                foreach (String dialogue in incompleteDialogues)
                {
                    message += "\n" + dialogue;
                }
            }
            if (missingCharacterDialogues.Count > 0)
            {
                message += "\n\nWARNING: The following Missions have missing character IDs.";
                foreach (MissionDialogue dialogue in missingCharacterDialogues)
                {
                    message += "\n" + dialogue.Name;
                }
            }
            if (missingSPFieldDialogues.Count > 0)
            {
                message += "\n\nWARNING: The following Missions have blank speaker or listener fields.";
                foreach (MissionDialogue dialogue in missingSPFieldDialogues)
                {
                    message += "\n" + dialogue.Name;
                }
            }
            if (missingRootDialogues.Count > 0)
            {
                message += "\n\nWARNING: The following Missions contain conversations with blank root fields.";
                foreach (MissionDialogue dialogue in missingRootDialogues)
                {
                    message += "\n" + dialogue.Name;
                }
            }
            if (missingLines.Count > 0)
            {
                message += "\n\nWARNING: The following Missions contain lines with blank dialogue.";
                foreach (MissionDialogue dialogue in missingLines)
                {
                    message += "\n" + dialogue.Name;
                }
            }
            MessageBox.Show(message);
            dialogues = null;
            validDialogues = null;

            this.m_autoSaveTimer.Enabled = true;
        }

        /// <summary>
        /// Show unused conversations.
        /// </summary>
        public void ShowUnusedConversationsWindow()
        {
            ShowUnusedConversations form = new ShowUnusedConversations();
            form.ShowDialog();
        }

        /// <summary>
        /// Exports either the selected mission or all missions to a word document in a specific format
        /// <param name="bAllMissions"></param>
        /// </summary>
        public void ExportConversationToWord()
        {
            if (this.SelectedConversation == null)
            {
                return;
            }

            try
            {
                Microsoft.Office.Interop.Word.Application WordApp = new Microsoft.Office.Interop.Word.Application();
                WordApp.Visible = true;
                Object missing = System.Reflection.Missing.Value;
                Object end = "\\endofdoc"; /* \endofdoc is a predefined bookmark */
                Object start = 0;

                Microsoft.Office.Interop.Word.Document document = WordApp.Documents.Add(ref missing, ref missing, ref missing, ref missing);
                Microsoft.Office.Interop.Word.Range range = document.Range(ref start, ref missing);
                range.set_Style(Microsoft.Office.Interop.Word.WdBuiltinStyle.wdStylePlainText);

                if (this.SelectedDialogue != null)
                {
                    this.WriteConversationToWord(document, this.SelectedConversation, end);
                }
            }
            catch (COMException ex)
            {
                MessageBox.Show("Appears Word has been closed before export has completed", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// Exports either the selected mission or all missions to a word document in a specific format
        /// <param name="bAllMissions"></param>
        /// </summary>
        public void ExportToWord(bool bAllMissions)
        {
            try
            {
                Microsoft.Office.Interop.Word.Application WordApp = new Microsoft.Office.Interop.Word.Application();
                WordApp.Visible = true;
                Object missing = System.Reflection.Missing.Value;
                Object end = "\\endofdoc"; /* \endofdoc is a predefined bookmark */
                Object start = 0;

                Microsoft.Office.Interop.Word.Document document = WordApp.Documents.Add(ref missing, ref missing, ref missing, ref missing);
                Microsoft.Office.Interop.Word.Range range = document.Range(ref start, ref missing);
                range.set_Style(Microsoft.Office.Interop.Word.WdBuiltinStyle.wdStylePlainText);

                if (bAllMissions)
                {
                    foreach (MissionDialogue mission in this.Missions)
                    {
                        WriteMissionToWord(document, mission, end);
                    }
                }
                else
                {
                    if (this.SelectedDialogue != null)
                    {
                        WriteMissionToWord(document, this.SelectedDialogue, end);
                    }
                }
            }
            catch (COMException ex)
            {
                MessageBox.Show("Appears Word has been closed before export has completed", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// Add a new conversation to the given mission.
        /// </summary>
        /// <param name="mission">The mission to add the conversation to</param>
        /// <param name="newIndex">The index value to insert the new conversation at</param>
        public void AddNewConversation(MissionDialogue mission, int newIndex)
        {
            if (mission == null) return;

            this.SelectedConversation = mission.AddNewConversation(newIndex);

            // Need to register this conversation with the undo manager that
            // looks after the parent mission dialogue
            int missionIndex = this.Missions.IndexOf(mission);

            if (missionIndex >= 0)
            {
                this.UndoManagers[missionIndex].RegisterViewModel(this.SelectedConversation);
            }

            this.AddNewLinePrivately(this.SelectedConversation, -1);
        }

        /// <summary>
        /// Adds a new conversation by using the AddNewConversationPrivately member in the mission
        /// dialogue, this means that this new conversation wont be able to be undone by the user.
        /// </summary>
        /// <param name="mission">The mission to add the conversation to</param>
        /// <param name="newIndex">The index value to insert the new conversation at</param>
        public void AddNewConversationPrivately(MissionDialogue mission, int newIndex)
        {
            if (mission == null) return;

            this.SelectedConversation = mission.AddNewConversationPrivately(newIndex);

            // Need to register this conversation with the undo manager that
            // looks after the parent mission dialogue
            int missionIndex = this.Missions.IndexOf(mission);

            if (missionIndex >= 0)
            {
                this.UndoManagers[missionIndex].RegisterViewModel(this.SelectedConversation);
            }

            this.AddNewLinePrivately(this.SelectedConversation, -1);
        }

        /// <summary>
        /// Removes the indexed conversation from the given mission
        /// </summary>
        /// <param name="mission">The mission to remove the conversation from</param>
        /// <param name="conversationIndex">The index of the conversation to remove</param>
        public void RemoveConversation(MissionDialogue mission, int conversationIndex)
        {
            if (mission == null) return;
            if (conversationIndex < 0 || conversationIndex > mission.Conversations.Count) return;

            mission.RemoveConversation(conversationIndex);

            // Need to select another conversation if there is one available
            if (this.SelectedDialogue.Conversations.Count > 0)
            {
                int selectedIndex = conversationIndex > mission.Conversations.Count - 1 ? conversationIndex - 1 : conversationIndex;
                this.SelectedConversation = mission.Conversations[selectedIndex];
            }
        }

        /// <summary>
        /// Moves the given conversation to the new index in the selected dialogue
        /// </summary>
        /// <param name="conversation">The conversation to move</param>
        /// <param name="newIndex">The new index of the conversation</param>
        public void MoveConversation(Conversation conversation, int newIndex)
        {
            int index = this.m_missions.IndexOf(this.m_selectedDialogue);
            int oldIndex = this.SelectedDialogue.Conversations.IndexOf(conversation);

            if (oldIndex == newIndex)
                return;

            this.m_undoManagers[index].StartCompositeUndo();

            this.SelectedDialogue.MoveConversation(conversation, newIndex);
            this.SelectedConversation = conversation;

            this.m_undoManagers[index].EndCompositeUndo();
        }

        /// <summary>
        /// Unlocks the given conversation
        /// </summary>
        /// <param name="conversation">The conversation to unlock</param>
        public void UnlockConversation(Conversation conversation)
        {
            conversation.Locked = false;
        }

        /// <summary>
        /// Unlocks all conversations for this given mission
        /// </summary>
        /// <param name="conversation">The mission to unlock</param>
        public void UnlockAllConversation(MissionDialogue mission)
        {
            if (mission != null)
            {
                foreach (Conversation conversation in mission.Conversations)
                {
                    conversation.Locked = false;
                }
            }
        }

        /// <summary>
        /// Adds a new line by using the AddNewConversationPrivately member in the conversation object
        /// this means that this new line wont be able to be undone by the user.
        /// </summary>
        /// <param name="conversation">The conversation to add the line to</param>
        /// <param name="newIndex">The index value to insert the new line at</param>
        public void AddNewLine(Conversation conversation, int newIndex)
        {
            if (conversation == null) return;

            this.SelectedLine = conversation.AddNewLine(newIndex);

            // Need to register this line with the undo manager that
            // looks after the parent mission dialogue
            int missionIndex = this.Missions.IndexOf(conversation.Mission);

            if (missionIndex >= 0)
            {
                this.UndoManagers[missionIndex].RegisterViewModel(this.SelectedLine);
            }
        }

        /// <summary>
        /// Adds a new conversation by using the AddNewConversationPrivately member in the mission
        /// dialogue, this means that this new conversation wont be able to be undone by the user.
        /// </summary>
        /// <param name="mission"></param>
        /// <param name="newIndex"></param>
        public void AddNewLinePrivately(Conversation conversation, int newIndex)
        {
            if (conversation == null) return;

            this.SelectedLine = conversation.AddNewLinePrivately(newIndex);

            // Need to register this line with the undo manager that
            // looks after the parent mission dialogue
            int missionIndex = this.Missions.IndexOf(conversation.Mission);

            if (missionIndex >= 0)
            {
                this.UndoManagers[missionIndex].RegisterViewModel(this.SelectedLine);
            }
        }

        /// <summary>
        /// Removes the indexed conversation from the given mission
        /// </summary>
        /// <param name="mission">The mission to remove the conversation from</param>
        /// <param name="conversationIndex">The index of the conversation to remove</param>
        public void RemoveLine(Conversation conversation, int lineIndex)
        {
            if (conversation == null)
            {
                return;
            }

            if (lineIndex < 0 || lineIndex > conversation.Lines.Count)
            {
                return;
            }

            int index = this.m_missions.IndexOf(this.m_selectedDialogue);
            this.m_undoManagers[index].StartCompositeUndo();

            conversation.RemoveLine(lineIndex);
            if (conversation.Random)
            {
                conversation.MoveLine(conversation.Lines[conversation.Lines.Count - 1], Math.Min(lineIndex, conversation.Lines.Count - 1));
            }

            // Need to select another conversation (basically the new that has replaced the removed one)
            if (this.SelectedConversation.Lines.Count > 0)
            {
                int selectedIndex = lineIndex > conversation.Lines.Count - 1 ? lineIndex - 1 : lineIndex;
                App.Controller.SelectedLine = conversation.Lines[selectedIndex];
            }

            this.m_undoManagers[index].EndCompositeUndo();
        }

        /// <summary>
        /// Moves the given line to the new index in the selected conversation
        /// </summary>
        /// <param name="conversation">The conversation to move</param>
        /// <param name="newIndex">The new index of the conversation</param>
        public void MoveLine(Line line, int newIndex)
        {
            int index = this.m_missions.IndexOf(this.m_selectedDialogue);
            int oldIndex = this.SelectedConversation.Lines.IndexOf(line);

            if (oldIndex == newIndex)
                return;

            this.m_undoManagers[index].StartCompositeUndo();

            this.SelectedConversation.MoveLine(line, newIndex);
            this.SelectedLine = line;

            this.m_undoManagers[index].EndCompositeUndo();
        }

        /// <summary>
        /// Generates the filenames for the selected conversation, if no conversation
        /// is selected this doesn't do anything
        /// </summary>
        public void AutoGenerateFilenamesForConversation()
        {
            int index = this.m_missions.IndexOf(this.m_selectedDialogue);
            if (this.SelectedConversation == null)
                return;

            AutoGenerateFilenamesForConversations(Enumerable.Repeat<Conversation>(this.SelectedConversation, 1), true);
        }

        /// <summary>
        /// Generates the filenames for all the conversations in the selected dialogue, if no dialogue
        /// is selected this doesn't do anything
        /// </summary>
        public void AutoGenerateFilenamesForAllConversations()
        {
            AutoGenerateFilenamesForConversations(this.SelectedDialogue.Conversations, false);
        }

        /// <summary>
        /// Generates the filenames for all the conversations in the selected dialogue, if no dialogue
        /// is selected this doesn't do anything
        /// </summary>
        public void AutoGenerateFilenamesForConversations(IEnumerable<Conversation> conversations, bool overwriteUnlocked)
        {
            int index = this.m_missions.IndexOf(this.m_selectedDialogue);
            if (this.SelectedConversation == null)
                return;

            this.m_undoManagers[index].StartCompositeUndo();
            foreach (Conversation conversation in conversations)
            {
                Collection<Line> generateLines;
                conversation.GetLinesThatNeedGenerating(out generateLines, overwriteUnlocked);
                foreach (Line line in generateLines)
                {
                    line.ResetFilename();
                }

                conversation.AutoGenerateFilenames(generateLines);
            }

            this.m_undoManagers[index].EndCompositeUndo();
        }

        /// <summary>
        /// Reload the configuration file
        /// </summary>
        public void ReloadConfigFile(Object sender)
        {
            // First make sure we have the latest config file
            bool successful = false;
            string currentDirectory = Directory.GetCurrentDirectory();
            try
            {
                Directory.SetCurrentDirectory(this.GameView.Project.Root);
                this.P4.User = this.p4userName;
                RSG.Base.Logging.Log.Log__Message("{0} [{1}]: connect", P4.Port, P4.User);
                this.P4.Connect();
                successful = this.GetLatestConfigFile(this.P4);
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Unhandled Perforce exception while trying to connect to the p4 server.");
            }
            finally
            {
                Directory.SetCurrentDirectory(currentDirectory);
                P4.Disconnect();
            }

            if (!successful)
            {
                MessageBox.Show(
                        "Unable to reload the configuration as a perforce error occurred and were unable to check you have the latest configuration file.",
                        "Warning",
                        MessageBoxButton.OK,
                        MessageBoxImage.Warning);

                return;
            }

            // Load the file
            DialogueConfigurations newConfigurations = new DialogueConfigurations(this.ConfigPath, this.m_gameView);
            this.Configurations.Characters = newConfigurations.Characters;
            this.Configurations.RegisterCharacterChange();
            MissionDialogue previousSelected = App.Controller.SelectedDialogue;
            App.Controller.SelectedDialogue = null;
            App.Controller.SelectedDialogue = previousSelected;
        }

        /// <summary>
        ///
        /// </summary>
        /// <remarks>
        /// For this to return true the config file has to be set to
        /// writtable and checked out of perforce.
        /// </remarks>
        public Boolean ComfirmConfigFileIsValidForSave(bool checkout)
        {
            Boolean result = false;
            Boolean configWrittable = false;
            if (File.Exists(m_configPath))
            {
                FileAttributes fileAttributes = File.GetAttributes(m_configPath);
                if ((fileAttributes & FileAttributes.ReadOnly) != FileAttributes.ReadOnly)
                {
                    // The config file is writable, make sure it is checked out by the user.
                    configWrittable = true;
                }
            }

            String currDir = Directory.GetCurrentDirectory();
            try
            {
                Directory.SetCurrentDirectory(Path.GetDirectoryName(this.ConfigPath));
                this.P4.User = this.p4userName;
                RSG.Base.Logging.Log.Log__Message("{0} [{1}]: connect", P4.Port, P4.User);
                this.P4.Connect();

                if (configWrittable == true)
                {
                    P4API.P4RecordSet recordSet = P4.Run("fstat", m_configPath);
                    if (recordSet.Records.Length == 1)
                    {
                        P4API.P4Record record = recordSet.Records[0];
                        if (record.Fields.ContainsKey("action"))
                        {
                            if (record["action"] == "edit")
                            {
                                result = true;
                            }
                        }
                    }
                }
                else if (checkout)
                {
                    P4API.P4PendingChangelist changeList = P4.CreatePendingChangelist("Automatic dialogue star config file edit");
                    P4.Run("edit", "-c", changeList.Number.ToString(), this.ConfigPath);
                    P4.Run("reopen", "-c", changeList.Number.ToString(), this.ConfigPath);

                    // Clear up any previous and now empty created changelists
                    P4API.P4RecordSet currentChangeLists = P4.Run("changelists", "-l", "-s", "pending", "-u", P4.User, "-c", P4.Client);
                    foreach (P4API.P4Record record in currentChangeLists.Records)
                    {
                        if (record.Fields.ContainsKey("desc"))
                        {
                            String description = record.Fields["desc"];
                            if (description == "Automatic dialogue star config file edit\n")
                            {
                                if (record.Fields.ContainsKey("change"))
                                {
                                    P4.Run("change", "-d", record.Fields["change"]);
                                }
                            }
                        }
                    }
                    P4API.P4RecordSet recordSet = P4.Run("fstat", m_configPath);
                    if (recordSet.Records.Length == 1)
                    {
                        P4API.P4Record record = recordSet.Records[0];
                        if (record.Fields.ContainsKey("action"))
                        {
                            if (record["action"] == "edit")
                            {
                                result = true;
                            }
                        }
                    }
                }
            }
            catch (P4API.Exceptions.P4APIExceptions ex)
            {
                Directory.SetCurrentDirectory(currDir);
                Log.Log__Exception(ex, "Unhandled Perforce exception while trying to connect to the p4 server.");
            }
            finally
            {
                P4.Disconnect();
                Directory.SetCurrentDirectory(currDir);
            }

            return result;
        }

        /// <summary>
        /// Saves the current configurations into the config path, makes sure it is checked out and isn't read only first
        /// </summary>
        public void SaveConfigFile()
        {
            if (this.ComfirmConfigFileIsValidForSave(true))
            {
                this.Configurations.SaveConfigurations(this.ConfigPath);
            }
            else
            {
                MessageBox.Show("Unable to save configuration file due to it currently being in an invalid state.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        ///  Returns true if the configuration.characters dictionary contains the
        ///  given name.
        /// </summary>
        /// <param name="?">The character name to test</param>
        /// <returns></returns>
        public Boolean ContainsCharacterName(String characterName)
        {
            return this.Configurations.Characters.ContainsValue(characterName);
        }

        /// <summary>
        /// Opens the character modifing window dialog so that the user can add, remove
        /// rename, and merge different characters
        /// </summary>
        /// <param name="sender">The object that sent the request to show this window</param>
        public void ShowCharacterModifyDialog(Object sender)
        {
            bool successful = false;
            string currentDirectory = Directory.GetCurrentDirectory();
            try
            {
                Directory.SetCurrentDirectory(this.GameView.Project.Root);
                this.P4.User = this.p4userName;
                RSG.Base.Logging.Log.Log__Message("{0} [{1}]: connect", P4.Client, P4.User);
                this.P4.Connect();
                successful = this.GetLatestConfigFile(this.P4);

                if (successful)
                {
                    RSG.Base.Logging.Log.Log__Message("Got latest config file");
                    successful = this.CheckoutConfigFile(this.P4);
                    if (!successful)
                    {
                        RSG.Base.Logging.Log.Log__Message("Checked out config file");
                        RSG.SourceControl.Perforce.FileState fs = new FileState(P4, m_configPath);
                        RSG.Base.Logging.Log.Log__Message("FState config file");
                        if (fs.OpenAction != FileAction.Edit)
                        {
                            if (fs.OtherLockedUsers != null && fs.OtherLockedUsers.Length > 0)
                            {
                                MessageBox.Show("Cannot edit characters at this time, unable to check out configuration file from" +
                                    string.Format(" perforce as someone else has it checked out ({0})", fs.OtherLockedUsers[0]), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                            else
                            {
                                MessageBox.Show("Cannot edit characters at this time, unable to check out configuration file from" +
                                    " perforce as someone else has it checked out", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Cannot edit characters at this time, unable to get latest version.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    successful = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Cannot edit characters at this time, unexpected error in the perforce API.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Log.Log__Exception(ex, "Unhandled Perforce exception while trying to connect to the p4 server.");
                successful = false;
            }
            finally
            {
                Directory.SetCurrentDirectory(currentDirectory);
                P4.Disconnect();
            }

            RSG.Base.Logging.Log.Log__Message("Perforce config file complete");

            if (!successful)
                return;
            else
                this.ReloadConfigFile(null);

            // Now we are up to date, checked out, and the characters in memory match the
            // characters in the file.

            RSG.Base.Logging.Log.Log__Message("Succesful and reloaded");

            // Get a temp copy of the current character dictionary in case the user cancels.
            ObservableDictionary<Guid, String> modifiedCharacters = this.Configurations.CloneCharacters();
            ObservableDictionary<Guid, String> modifiedCharacterVoices = this.Configurations.CloneCharacterVoices();

            View.CharacterDialog dlg = new View.CharacterDialog();
            dlg.DataContext = this.Configurations;
            dlg.Characters = modifiedCharacters;
            dlg.CharacterVoices = modifiedCharacterVoices;
            dlg.InstalledVoices = this.Configurations.InstalledVoices;
            dlg.Owner = sender as Window;
            if (dlg.ShowDialog() == (Boolean?)true)
            {
                this.Configurations.Characters = new ObservableDictionary<Guid, String>(modifiedCharacters);
                this.Configurations.CharacterVoices = new ObservableDictionary<Guid, String>(modifiedCharacterVoices);
                this.Configurations.RegisterCharacterChange();
                this.SaveConfigFile();
            }

            MissionDialogue previousSelected = App.Controller.SelectedDialogue;
            App.Controller.SelectedDialogue = null;
            App.Controller.SelectedDialogue = previousSelected;

            MessageBox.Show(
                "Performing this operation checked out the configuration file into a new change list with " +
                "a description of 'Automatic dialogue star config file edit'.\n\r\n\rPlease check in or revert this " +
                "change list asap as no one else can make changes to the configuration file until you do.",
                "Perforce", MessageBoxButton.OK, MessageBoxImage.Asterisk);
        }

        /// <summary>
        /// Adds a new character to the configurations
        /// </summary>
        /// <param name="characterName"></param>
        public void AddCharacter(String characterName)
        {
            this.Configurations.AddCharacter(characterName);
        }

        /// <summary>
        /// Removes a character from the configurations based on the given
        /// character guid.
        /// </summary>
        /// <param name="characterGuid">The character guid to remove</param>
        public void RemoveCharacter(Guid characterGuid)
        {
            this.Configurations.RemoveCharacter(characterGuid);
        }

        /// <summary>
        /// Renames the given character guid to the given name
        /// </summary>
        /// <param name="characterGuid">The guid for the character that will get renamed</param>
        /// <param name="newName">The new name of the character</param>
        public void RenameCharacter(Guid characterGuid, String newName)
        {
            this.Configurations.RenameCharacter(characterGuid, newName);
        }

        /// <summary>
        /// Opens the about window dialog so that the user can see the version number and a few more little
        /// things.
        /// </summary>
        /// <param name="sender"></param>
        public void ShowAboutDialog(Object sender)
        {
            View.AboutDialog dlg = new View.AboutDialog();
            dlg.DataContext = DialogueStar.ApplicationAttributes.Attributes;
            dlg.Owner = sender as Window;

            dlg.ShowDialog();
        }

        /// <summary>
        /// Opens the export preference window dialog so that the user can set the export paths
        /// </summary>
        /// <param name="sender"></param>
        public bool ShowExportDialog(Object sender)
        {
            // Keep a temp copy of the current export paths in case the user cancels
            View.ExportDialog dlg = new View.ExportDialog(this.Configurations, this.CurrentLanguage);

            if (String.IsNullOrWhiteSpace(Settings.Default.SourceDirectory))
            {
                dlg.SourceDirectory = this.m_americanDialogueDirectory;
            }
            else
            {
                dlg.SourceDirectory = Settings.Default.SourceDirectory;
                this.m_americanDialogueDirectory = dlg.SourceDirectory;
            }

            if (String.IsNullOrWhiteSpace(Settings.Default.ExportDirectory))
            {
                dlg.ExportDirectory = this.Configurations.GetExportRootPath();
            }
            else
            {
                dlg.ExportDirectory = Settings.Default.ExportDirectory;
                this.Configurations.SetExportRootPath(dlg.ExportDirectory);
            }

            Dictionary<ExportPath, string> exportPaths = this.Configurations.GetExportPaths(this.CurrentLanguage);

            dlg.MissionDialogueExport = exportPaths[ExportPath.MissionDialogue];
            dlg.MissionDialogueFilesExport = exportPaths[ExportPath.MissionDialogueFiles];
            dlg.MissionMocapExport = exportPaths[ExportPath.MissionMocap];
            dlg.RandomDialogueExport = exportPaths[ExportPath.RandomDialogue];
            dlg.RandomDialogueFilesExport = exportPaths[ExportPath.RandomDialogueFiles];
            dlg.RandomMocapExport = exportPaths[ExportPath.RandomMocap];
            dlg.RandomIdentifiersExport = exportPaths[ExportPath.RandomIdentifiers];
            dlg.NetDialogueExport = exportPaths[ExportPath.NetDialogue];
            dlg.NetDialogueFilesExport = exportPaths[ExportPath.NetDialogueFiles];
            dlg.NetMocapExport = exportPaths[ExportPath.NetMocap];

            dlg.Owner = sender as Window;
            bool result = dlg.ShowDialog() == true ? true : false;
            if (result)
            {
                this.m_americanDialogueDirectory = dlg.SourceDirectory;
                this.Configurations.SetExportRootPath(dlg.ExportDirectory);
                Settings.Default.SourceDirectory = dlg.SourceDirectory;
                Settings.Default.ExportDirectory = dlg.ExportDirectory;
                Settings.Default.Save();
            }

            return result;
        }

        /// <summary>
        /// Opens the character replacement dialog.
        /// </summary>
        /// <param name="sender"></param>
        public void ShowCharacterReplacementDialog(Object sender)
        {
            if (this.SelectedDialogue == null || this.SelectedDialogue.Conversations.Count == 0)
                return;

            IEnumerable<Line> lines = from c in this.SelectedDialogue.Conversations
                                      from l in c.Lines
                                      select l;

            List<string> usedCharacters = new List<string>();
            foreach (Line line in lines)
                usedCharacters.Add(line.CharacterName);

            Dictionary<Guid, string> allCharacters = new Dictionary<Guid, string>();
            foreach (KeyValuePair<Guid, string> character in this.Configurations.Characters)
                allCharacters.Add(character.Key, character.Value);

            View.CharacterReplacement dlg = new View.CharacterReplacement(usedCharacters, allCharacters);
            dlg.Lines = lines;

            dlg.Owner = sender as Window;
            dlg.ShowDialog();
        }

        /// <summary>
        /// Reloads the auto save file with the given filename
        /// </summary>
        public void LoadAutosaveFile(String filename)
        {
            // Create the new dialogue by passing in the filename
            MissionDialogue newDialogue = new MissionDialogue(filename, this.m_configurations);
            this.m_missions.Add(newDialogue);

            // Need to create the undo manager for this dialogue
            UndoManagerObject undoManager = new UndoManagerObject();
            undoManager.RegisterViewModel(newDialogue);
            this.m_undoManagers.Add(undoManager);

            // Select the new dialogue sheet we have just created
            this.SelectedDialogue = newDialogue;

            // Register the conversations and the lines to this undo manager as well.
            foreach (Conversation conversation in newDialogue.Conversations)
            {
                foreach (Line line in conversation.Lines)
                {
                    undoManager.RegisterViewModel(line);
                }
                undoManager.RegisterViewModel(conversation);
            }

            // Need to set the dirty state to true
            newDialogue.DirtyManager.Dirty = true;

            // Need to manually set the filename to empty and the name to that of the auto file
            newDialogue.Filename = String.Empty;
            String name = Path.GetFileNameWithoutExtension(filename);
            newDialogue.Name = name.TrimEnd("_Temp".ToCharArray());
            this.CreatedCount++; // Just in case
        }

        /// <summary>
        /// Shows the export window that displays all the unique voices in all of the dstar files and lets the user choose
        /// which voices to export.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="DataContext"></param>
        /// <returns></returns>
        public Boolean ShowVoicesExportDialog(Object sender, SortedDictionary<String, Collection<MissionDialogue>> DataContext)
        {
            View.VoicesExport dlg = new View.VoicesExport();
            dlg.DataContext = DataContext;
            dlg.UnselectedVoices = new System.Collections.ObjectModel.ObservableCollection<String>(DataContext.Keys);
            dlg.Owner = sender as Window;
            Boolean result = (Boolean)dlg.ShowDialog();

            List<String> removedVoices = new List<String>();
            foreach (var obj in DataContext)
            {
                if (!dlg.SelectedVoices.Contains(obj.Key))
                {
                    // The user has choosen not to export this voice
                    removedVoices.Add(obj.Key);
                }
            }
            foreach (String removed in removedVoices)
            {
                DataContext.Remove(removed);
            }

            return result;
        }

        /// <summary>
        /// Finds all of the model parameters that match the gven search parameters
        /// </summary>
        /// <param name="range">The range of the search, whether it should be kept within the current mission or all missions</param>
        /// <param name="searchField">The property name that we want to search in</param>
        /// <param name="comparisonOptions">The comparison option for this search (equal, !equal etc)</param>
        /// <param name="options">The additional search options</param>
        /// <param name="searchText">The actual text that we wish to find, can be a wildcard</param>
        /// <returns>The list of model objects that satisfies the search parameters</returns>
        public List<SearchResult> FindAll(SearchRanges range, String searchField, SearchType searchType, ComparisonOptions comparisonOptions, SearchOptions options, String searchText)
        {
            List<SearchResult> result = new List<SearchResult>();

            switch (range)
            {
                case SearchRanges.Current:
                    List<SearchResult> missionResults = this.SelectedDialogue.FindAll(searchField, searchType, comparisonOptions, options, searchText);
                    result.AddRange(missionResults);
                    break;

                case SearchRanges.Opened:
                    foreach (MissionDialogue mission in this.Missions)
                    {
                        List<SearchResult> singleResult = mission.FindAll(searchField, searchType, comparisonOptions, options, searchText);
                        result.AddRange(singleResult);
                    }
                    break;

                case SearchRanges.All:
                    // First loop through all of the opened files
                    List<String> openedFilenames = new List<String>();
                    foreach (MissionDialogue mission in this.Missions)
                    {
                        List<SearchResult> singleResult = mission.FindAll(searchField, searchType, comparisonOptions, options, searchText);

                        result.AddRange(singleResult);
                        openedFilenames.Add(mission.Filename.ToLower());
                    }
                    // Loop through all the dstar files one at a time and make sure that any files that are currently open are not searched again.
                    // Get a list of all the files in the dialogue star directory.
                    String[] dstarFiles = Directory.GetFiles(this.AmericanDialogueDirectory, "*.dstar");
                    foreach (String filename in dstarFiles)
                    {
                        // Check to see if it is already opened
                        if (openedFilenames.Contains(filename.ToLower()))
                            continue;

                        MissionDialogue loadedDialogue = new MissionDialogue(filename, this.Configurations);
                        List<SearchResult> singleResult = loadedDialogue.FindAll(searchField, searchType, comparisonOptions, options, searchText);
                        result.AddRange(singleResult);
                        loadedDialogue = null;
                    }
                    break;

                default:
                    break;
            }

            return result;
        }

        /// <summary>
        /// Loops through the current opened files and returns true if the
        /// given filename is among them.
        /// </summary>
        /// <param name="filename">The filename to test for</param>
        /// <returns>True if the file is already open</returns>
        public MissionDialogue IsFileAlreadyOpen(String filename)
        {
            foreach (MissionDialogue dialogue in this.m_missions)
            {
                if (dialogue.Filename == filename) return dialogue;
            }
            return null;
        }

        #endregion Public view function(s)// Public view function(s)

        #region Private Function(s)

        /// <summary>
        /// Checks to see if any of the opened sheets have empty mission IDs
        /// or empty Subtitle Group IDs
        /// </summary>
        private Boolean CanSaveOrExport(ref MissionDialogue missingDialogue)
        {
            foreach (MissionDialogue dialogue in this.Missions)
            {
                if (String.IsNullOrEmpty(dialogue.MissionId) || String.IsNullOrEmpty(dialogue.MissionSubtitleId))
                {
                    missingDialogue = dialogue;
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Saves a specific file to the filename in the object if there is none availible
        /// it returns without doing anything.
        /// </summary>
        /// <param name="dialogue"></param>
        private void SaveDialogue(MissionDialogue dialogue, String previousDialogueName)
        {
            if (dialogue == null) return;
            if (dialogue.Filename == String.Empty) return;
            if (dialogue.DirtyManager.Dirty == false) return;

            // First make sure we have the file checked out
            String currDir = Directory.GetCurrentDirectory();
            try
            {
                Directory.SetCurrentDirectory(this.AmericanDialogueDirectory);
                this.P4.User = this.p4userName;
                RSG.Base.Logging.Log.Log__Message("{0} [{1}]: connect", P4.Port, P4.User);
                this.P4.Connect();

                int changeListNumber = -1;
                // Open the file for edit in an existing changelist if one is available
                P4API.P4RecordSet previousChangeLists = P4.Run("changelists", "-l", "-s", "pending", "-u", P4.User, "-c", P4.Client);
                foreach (P4API.P4Record record in previousChangeLists.Records)
                {
                    if (record.Fields.ContainsKey("desc"))
                    {
                        String description = record.Fields["desc"];
                        if (description == "Automatic dialogue star file edit\n")
                        {
                            if (changeListNumber == -1)
                            {
                                int.TryParse(record.Fields["change"], out changeListNumber);
                            }
                        }
                    }
                }

                if (changeListNumber == -1)
                {
                    P4API.P4PendingChangelist changeList = P4.CreatePendingChangelist("Automatic dialogue star file edit");
                    changeListNumber = changeList.Number;
                }
                P4.Run("edit", "-c", changeListNumber.ToString(), dialogue.Filename);
                P4.Run("reopen", "-c", changeListNumber.ToString(), dialogue.Filename);
                P4.Run("add", "-c", changeListNumber.ToString(), dialogue.Filename);

                // Clear up any previous and now empty created changelists
                P4API.P4RecordSet currentChangeLists = P4.Run("changelists", "-l", "-s", "pending", "-u", P4.User, "-c", P4.Client);
                foreach (P4API.P4Record record in currentChangeLists.Records)
                {
                    if (record.Fields.ContainsKey("desc"))
                    {
                        String description = record.Fields["desc"];
                        if (description == "Automatic dialogue star file edit\n")
                        {
                            if (record.Fields.ContainsKey("change"))
                            {
                                P4.Run("change", "-d", record.Fields["change"]);
                            }
                        }
                    }
                }
            }
            catch (P4API.Exceptions.P4APIExceptions ex)
            {
                Log.Log__Exception(ex, "Unhandled Perforce exception");
            }
            finally
            {
                P4.Disconnect();
                Directory.SetCurrentDirectory(currDir);
            }

            dialogue.SaveMissionDialogue();

            // Need to clear the undo/redo stack for this mission dialogue now that it has been saved
            int index = this.m_missions.IndexOf(dialogue);
            this.m_undoManagers[index].UndoStack.Clear();
            this.m_undoManagers[index].RedoStack.Clear();

            // Make sure the dirty state is set back to false for the saved dialogue
            dialogue.DirtyManager.Dirty = false;

            // We have now saved this dialogue so we no longer need the auto save file (if any exist) for it
            // so delete it
            String autoSaveFilename = Path.Combine(this.AutoSaveDirectory, previousDialogueName + "_Temp.das");
            if (System.IO.File.Exists(autoSaveFilename) == true)
            {
                System.IO.File.Delete(autoSaveFilename);
            }
        }

        /// <summary>
        /// Prompts the user whether they want to save the given dialogue before doing a operation
        /// This returns the result of the message box
        /// </summary>
        private Boolean PromptSingleSave(MissionDialogue dialogue)
        {
            if (dialogue.DirtyManager.Dirty == false)
                return true;

            MessageBoxResult result = MessageBox.Show("Save changes to " + dialogue.Name + "?", "Warning", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning);

            if (result == MessageBoxResult.Yes)
            {
                String filename = String.Empty;
                return this.SaveDialogue(dialogue, true, ref filename);
            }
            else if (result == MessageBoxResult.No)
            {
                // We have now choosen not to save this dialogue so we no longer need the auto save file (if any exist) for it
                // so delete it
                String autoSaveFilename = Path.Combine(this.AutoSaveDirectory, dialogue.Name + "_Temp.das");
                if (System.IO.File.Exists(autoSaveFilename) == true)
                {
                    System.IO.File.Delete(autoSaveFilename);
                }
                return true;
            }
            else if (result == MessageBoxResult.Cancel)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Called when the user chooses to close all of the opened dialogue sheets
        /// at the same time, by either pressing the button or exiting the application
        /// </summary>
        /// <returns></returns>
        private Boolean PromptMultiplySave()
        {
            List<MissionDialogue> dirtyMissions = new List<MissionDialogue>();

            foreach (MissionDialogue mission in this.Missions)
            {
                if (mission.DirtyManager.Dirty == true)
                    dirtyMissions.Add(mission);
            }

            if (dirtyMissions.Count > 0)
            {
                foreach (MissionDialogue dialogue in dirtyMissions)
                {
                    MessageBoxResult result = MessageBox.Show("Save changes to " + dialogue.Name + "?", "Warning", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning);

                    if (result == MessageBoxResult.Yes)
                    {
                        String filename = String.Empty;
                        if (!this.SaveDialogue(dialogue, true, ref filename))
                        {
                            return false;
                        }
                        else
                        {
                            continue;
                        }
                    }
                    else if (result == MessageBoxResult.No)
                    {
                        // We have now choosen not to save this dialogue so we no longer need the auto save file (if any exist) for it
                        // so delete it
                        String autoSaveFilename = Path.Combine(this.AutoSaveDirectory, dialogue.Name + "_Temp.das");
                        if (System.IO.File.Exists(autoSaveFilename) == true)
                        {
                            System.IO.File.Delete(autoSaveFilename);
                        }
                        continue;
                    }
                    else if (result == MessageBoxResult.Cancel)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// This is called whenever we need to perform a autosave
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_autoSaveTimer_Elapsed(Object sender, ElapsedEventArgs e)
        {
            if (this.Missions.Count == 0)
                return;

            List<MissionDialogue> dirtyMissions = new List<MissionDialogue>();

            foreach (MissionDialogue dialogue in this.Missions)
            {
                if (dialogue.DirtyManager.Dirty == true)
                {
                    dirtyMissions.Add(dialogue);
                }
            }

            foreach (MissionDialogue dialogue in dirtyMissions)
            {
                // Save a autosave version of this file in the autosave directory
                String autoSaveFilename = Path.Combine(this.AutoSaveDirectory, dialogue.Name + "_Temp.das");

                // Need to save the file without clearing the undo stack and without resetting the dirty state
                // but we don't have to check anything out of perforce
                dialogue.SaveMissionDialogueInTemp(autoSaveFilename);
            }
        }

        private static readonly XPathExpression XmlMissionPath = XPathExpression.Compile("/MissionDialogue");
        private static readonly XPathExpression XmlAttributes = XPathExpression.Compile("/MissionDialogue/@*");
        private static readonly String TypeAttribute = "type";

        /// <summary>
        /// Determines if the given filename represents a dialogue star for a random event
        /// </summary>
        private Boolean IsFilenameARandomEvent(String filename)
        {
            XmlDocument document = new XmlDocument();
            try
            {
                document.Load(filename);
            }
            catch (XmlException ex)
            {
                RSG.Base.Logging.Log.Log__Exception(ex, "Unable to open the dstar file located at " + filename);
                return false;
            }

            XPathNavigator navigator = document.CreateNavigator();
            XPathNodeIterator missionIterator = navigator.Select(XmlMissionPath);

            if (missionIterator.Count == 0)
            {
                // This probably means that we are trying to load a old format dstar file.
                return false;
            }

            XPathNavigator missionNavigator = missionIterator.Current;
            XPathNodeIterator missionAttrIterator = missionNavigator.Select(XmlAttributes);

            // Collect the attributes for the mission
            while (missionAttrIterator.MoveNext())
            {
                if (typeof(String) != missionAttrIterator.Current.ValueType)
                    continue;
                String value = (missionAttrIterator.Current.TypedValue as String);

                if (missionAttrIterator.Current.Name == TypeAttribute)
                {
                    return (value == "Random Event");
                }
            }
            return false;
        }

        private List<MissionDialogue> ValidateExport(IEnumerable<MissionDialogue> missions)
        {
            List<MissionDialogue> validMissions = new List<MissionDialogue>();

            if (missions == null || missions.Count() == 0)
                return validMissions;

            foreach (MissionDialogue mission in missions)
            {
                List<String> errorMessages = new List<String>();
                if (!mission.IsMissionValid(ref errorMessages))
                {
                    String errorMessage = String.Format("Unable to export the file {0} due to the following errors:\n\n", mission.Name);
                    int index = 1;
                    foreach (String error in errorMessages)
                    {
                        errorMessage += String.Format("{0}. {1}\n", index.ToString(), error);
                        index++;
                    }
                    errorMessage += "\nDouble you wish to continue with the export?";

                    MessageBoxResult result = MessageBox.Show(errorMessage, "Export Error", MessageBoxButton.YesNo, MessageBoxImage.Error);
                    if (result == MessageBoxResult.No)
                    {
                        validMissions.Clear();
                        return validMissions;
                    }
                    else
                    {
                        continue;
                    }
                }
                else
                {
                    validMissions.Add(mission);
                    continue;
                }
            }

            return validMissions;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="p4"></param>
        /// <returns></returns>
        private bool GetLatestConfigFile(P4 p4)
        {
            Debug.Assert(p4.IsValidConnection(true, true));
            if (!p4.IsValidConnection(true, true))
            {
                return false;
            }

            RSG.SourceControl.Perforce.FileState fs = new FileState(P4, m_configPath);
            if (!fs.IsMapped)
            {
                return true;
            }

            if (fs.HaveRevision == fs.HeadRevision)
                return true;

            Boolean writtable = false;
            if (File.Exists(m_configPath))
            {
                FileAttributes fileAttributes = File.GetAttributes(m_configPath);
                if ((fileAttributes & FileAttributes.ReadOnly) != FileAttributes.ReadOnly)
                {
                    // The config file is writable, make sure it is checked out by the user
                    // otherwise it wont grab latest so ask the user to force grab it.
                    writtable = true;
                }
            }

            if (writtable == true)
            {
                Boolean forceGrab = true;
                if (fs.OpenAction == FileAction.Edit)
                    forceGrab = false;

                if (forceGrab == true)
                {
                    MessageBoxResult forceResult = MessageBox.Show("Unable to grab the latest config file because of a globber problem." +
                        "\nDo you wish to force grab it?", "Configuration.xml", MessageBoxButton.YesNo, MessageBoxImage.Question);

                    if (forceResult == MessageBoxResult.Yes)
                        P4.Run("sync", "-f", m_configPath);
                }
            }
            else
            {
                P4.Run("sync", m_configPath);
            }


            fs = new FileState(P4, m_configPath);
            if (fs.HaveRevision != fs.HeadRevision)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="p4"></param>
        /// <returns></returns>
        private bool CheckoutConfigFile(P4 p4)
        {
            Debug.Assert(p4.IsValidConnection(true, true));
            if (!p4.IsValidConnection(true, true))
            {
                return false;
            }

            RSG.SourceControl.Perforce.FileState fs = new FileState(P4, m_configPath);
            if (fs.OpenAction == FileAction.Edit)
                return true;

            P4API.P4PendingChangelist changeList = P4.CreatePendingChangelist("Automatic dialogue star config file edit");
            P4.Run("edit", "-c", changeList.Number.ToString(), this.ConfigPath);
            P4.Run("reopen", "-c", changeList.Number.ToString(), this.ConfigPath);
            P4.Run("add", "-c", changeList.Number.ToString(), this.ConfigPath);

            // Clear up any previous and now empty created changelists
            P4API.P4RecordSet currentChangeLists = P4.Run("changelists", "-l", "-s", "pending", "-u", P4.User, "-c", P4.Client);
            foreach (P4API.P4Record record in currentChangeLists.Records)
            {
                if (record.Fields.ContainsKey("desc"))
                {
                    String description = record.Fields["desc"];
                    if (description == "Automatic dialogue star config file edit\n")
                    {
                        if (record.Fields.ContainsKey("change"))
                        {
                            P4.Run("change", "-d", record.Fields["change"]);
                        }
                    }
                }
            }

            fs = new FileState(P4, m_configPath);
            if (fs.OpenAction == FileAction.Edit)
                return true;
            else
                return false;
        }
        #endregion // Private Function(s)

        public void FolderValidation()
        {
            // First get the user to select the folder on which to validate against.
            var dlg = new System.Windows.Forms.FolderBrowserDialog();
            dlg.ShowNewFolderButton = false;
            if (dlg.ShowDialog() != System.Windows.Forms.DialogResult.OK || !Directory.Exists(dlg.SelectedPath))
            {
                return;
            }

            string characterName = Path.GetFileNameWithoutExtension(dlg.SelectedPath);
            if (!this.Configurations.DoesCharacterExist(characterName))
            {
                System.Windows.MessageBox.Show("Unable to validate folder. Folder name should be equal to a valid character name.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            List<string> characterWavFiles = new List<string>();
            string[] allWavFiles = Directory.GetFiles(dlg.SelectedPath, "*.wav", SearchOption.TopDirectoryOnly);
            for (int i = 0; i < allWavFiles.Length; i++)
            {
                allWavFiles[i] = Path.GetFileNameWithoutExtension(allWavFiles[i]);
            }

            string[] characterDirectories = Directory.GetDirectories(dlg.SelectedPath);
            Dictionary<string, string> characterFilenames = new Dictionary<string, string>();
            List<string> allLineFilenames = new List<string>();
            String[] exportedFiles = Directory.GetFiles(this.AmericanDialogueDirectory, "*.dstar");
            List<string> missionIds = new List<string>();
            foreach (string filename in exportedFiles)
            {
                MissionDialogue dialogue = new MissionDialogue(filename, this.Configurations);
                missionIds.Add(dialogue.MissionId);
                foreach (Conversation conversation in dialogue.Conversations)
                {
                    int lineIndex = 1;
                    foreach (Line line in conversation.Lines)
                    {
                        if (line.Character.Value == "SFX")
                        {
                            continue;
                        }

                        string lineFilename = line.Filename;
                        if (string.IsNullOrEmpty(lineFilename))
                        {
                            continue;
                        }

                        if (conversation.Random)
                        {
                            lineFilename = lineFilename + "_" + lineIndex.ToString("D2");
                            lineIndex++;
                        }
                        else
                        {
                            if (!char.IsDigit(lineFilename[lineFilename.Length - 1]))
                            {
                                lineFilename = lineFilename + "_01";
                            }
                        }

                        allLineFilenames.Add(lineFilename);
                        if (!characterFilenames.ContainsKey(lineFilename))
                        {
                            characterFilenames.Add(lineFilename, line.CharacterName);
                        }
                    }
                }
            }

            // Check syntax
            List<string> syntaxErrors = this.CheckWavFilenameSyntax(allWavFiles);

            // Check syntax warnings
            List<string> warnings = this.CheckWavFilenameSyntaxWarnings(allWavFiles, missionIds);

            // Check number suffix
            List<string> numberErrors = this.CheckWavFilenameNumbers(allWavFiles);

            // Existence Check
            List<string> existenceErrors = this.CheckWavFilenameExistence(allWavFiles, characterFilenames, characterName);

            if (syntaxErrors.Count == 0 && numberErrors.Count == 0 && existenceErrors.Count == 0 && warnings.Count == 0)
            {
                System.Windows.MessageBox.Show("Validation passed successfully", "Validation", MessageBoxButton.OK, MessageBoxImage.None);
                return;
            }

            // We have errors!!!!!!
            DialogueStar.View.FolderValidation window = new View.FolderValidation(syntaxErrors, numberErrors, existenceErrors, warnings);
            window.Owner = Application.Current.MainWindow;
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            window.ShowDialog();
        }

        public void SfxValidation()
        {
            // Get a list of all the files that need exporting from the dialogue star directory.
            String[] exportedFiles = Directory.GetFiles(this.AmericanDialogueDirectory, "*.dstar");

            Collection<MissionDialogue> dialogues = new Collection<MissionDialogue>();
            if (exportedFiles.Length > 0)
            {
                // Create a mission dialogue object for each file and export them
                foreach (String filename in exportedFiles)
                {
                    MissionDialogue dialogue = new MissionDialogue(filename, this.Configurations);
                    dialogues.Add(dialogue);
                }

                // Export the rest of the data into the three files
                Dictionary<string, HashSet<string>> errors = new Dictionary<string, HashSet<string>>();
                int errorCount = 0;
                foreach (MissionDialogue dialogue in dialogues)
                {
                    foreach (Conversation conversation in dialogue.Conversations)
                    {
                        bool previousSfx = false;
                        foreach (Line line in conversation.Lines)
                        {
                            if (line.CharacterGuid != this.Configurations.SfxIdentifier)
                            {
                                previousSfx = false;
                                continue;
                            }

                            if (previousSfx)
                            {
                                HashSet<string> roots;
                                if (!errors.TryGetValue(dialogue.Name, out roots))
                                {
                                    roots = new HashSet<string>();
                                    errors.Add(dialogue.Name, roots);
                                }

                                roots.Add(conversation.Root);
                                errorCount++;
                                break;
                            }
                            else
                            {
                                previousSfx = true;
                                continue;
                            }
                        }
                    }
                }

                if (errors.Count == 0)
                {
                    MessageBox.Show("No validation errors found.", "Validation", MessageBoxButton.OK);
                    return;
                }

                string path = Path.ChangeExtension(Path.GetTempFileName(), "txt");
                using (StreamWriter writer = new StreamWriter(path))
                {
                    writer.WriteLine("{0} validation errors discovered.", errorCount);
                    foreach (KeyValuePair<string, HashSet<string>> error in errors)
                    {
                        writer.WriteLine("File: {0}; {1} Roots", error.Key, error.Value.Count);
                        foreach (string root in error.Value)
                        {
                            writer.WriteLine("Root: {0}", root);
                        }
                    }
                }

                Process.Start(path);
            }
        }

        private void WriteMissionToWord(Microsoft.Office.Interop.Word.Document document, MissionDialogue mission, Object end)
        {
            // Write mission name
            Microsoft.Office.Interop.Word.Range titleRange = document.Bookmarks.get_Item(ref end).Range;
            titleRange.Text = mission.MissionName + "\n\n";
            titleRange.Font.Bold = 1;
            titleRange.Font.Italic = 0;
            titleRange.Font.Size = 13.0f;
            titleRange.Font.Name = "Cambria";

            foreach (Conversation conversation in mission.Conversations)
            {
                WriteConversationToWord(document, conversation, end);
            }
        }

        private void WriteConversationToWord(Microsoft.Office.Interop.Word.Document document, Conversation conversation, Object end)
        {
            Microsoft.Office.Interop.Word.Range conversationRange = document.Bookmarks.get_Item(ref end).Range;
            String conversationHeader = conversation.Role + "/" + conversation.Root + "/" + conversation.Description + "/";
            conversationHeader += conversation.Random ? "Random/" : "Not Random/";
            conversationHeader += conversation.PlaceHolder ? "Placeholder" : "Not Placeholder";
            conversationRange.Text = conversationHeader + "\n\n";
            conversationRange.Font.Bold = 1;
            conversationRange.Font.Italic = 1;
            conversationRange.Font.Size = 11.0f;
            conversationRange.Font.Name = "Cambria";

            foreach (Line line in conversation.Lines)
            {
                Microsoft.Office.Interop.Word.Range lineRange = document.Bookmarks.get_Item(ref end).Range;
                String lineHeader = conversation.Role + "/" + line.CharacterName;
                lineRange.Text = lineHeader + "\n";
                lineRange.Font.Bold = 1;
                lineRange.Font.Italic = 1;
                lineRange.Font.Size = 11.0f;
                lineRange.Font.Name = "Cambria";
                lineRange.Paragraphs.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;

                Microsoft.Office.Interop.Word.Range lineDialogueRange = document.Bookmarks.get_Item(ref end).Range;
                lineDialogueRange.Text = line.LineDialogue + "\n\n";
                lineDialogueRange.Font.Bold = 0;
                lineDialogueRange.Font.Italic = 0;
                lineDialogueRange.Font.Size = 12.0f;
                lineDialogueRange.Font.Name = "Calibri";
                lineDialogueRange.Paragraphs.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;
            }
        }

        private List<string> CheckWavFilenameSyntax(string[] filenames)
        {
            List<string> syntaxErrors = new List<string>();
            String pattern = "^_([A-Z]{4})_(\\d{2,})$";
            Regex regex = new Regex(pattern);

            string msg = "The wav file '{0}.wav' appears to have the incorrect filename format.";
            foreach (string filename in filenames)
            {
                int missionIdLength = filename.IndexOf('_');
                if (missionIdLength == -1)
                {
                    syntaxErrors.Add(string.Format("The wav file '{0}.wav' doesn't start with a mission id followed by a underscore.", filename));
                    continue;
                }

                if (!regex.IsMatch(filename.Substring(missionIdLength)))
                {
                    syntaxErrors.Add(string.Format(msg, filename));
                }
            }

            return syntaxErrors;
        }

        private List<string> CheckWavFilenameSyntaxWarnings(string[] filenames, List<string> missionIds)
        {
            List<string> warnings = new List<string>();
            foreach (string filename in filenames)
            {
                int missionIdLength = filename.IndexOf('_');
                if (missionIdLength == -1)
                {
                    continue;
                }

                string missionId = filename.Substring(0, missionIdLength);
                if (!missionIds.Contains(missionId))
                {
                    if (missionIdLength == 5)
                    {
                        missionId = filename.Substring(0, missionIdLength - 1);
                        if (!missionIds.Contains(missionId))
                        {
                            warnings.Add(string.Format("The wav file '{0}.wav' appears to start with a mission id that doesn't seem to exist.", filename));
                            continue;
                        }
                    }
                    else
                    {
                        warnings.Add(string.Format("The wav file '{0}.wav' appears to start with a mission id that doesn't seem to exist.", filename));
                        continue;
                    }
                }
            }

            return warnings;
        }

        private List<string> CheckWavFilenameNumbers(IEnumerable<string> filenames)
        {
            List<string> syntaxErrors = new List<string>();
            string msg = "The wav file '{0}.wav' appears to be in a incorrect sequence.";

            Dictionary<string, List<int>> numberList = new Dictionary<string, List<int>>();
            Dictionary<string, int> numbers = new Dictionary<string, int>();
            foreach (string filename in filenames)
            {
                string filenumber = filename;
                string fileCode = filename;
                int index = filename.LastIndexOf('_');
                if (index != -1)
                {
                    filenumber = filename.Substring(index + 1);
                    fileCode = filename.Substring(0, index);
                    index = fileCode.IndexOf('_') + 1;
                    if (index != -1)
                    {
                        fileCode = fileCode.Substring(index);
                    }
                }

                int number = 0;
                if (!int.TryParse(filenumber, out number))
                {
                    number = 1;
                }

                List<int> list = null;
                if (!numberList.TryGetValue(fileCode, out list))
                {
                    list = new List<int>();
                    numberList.Add(fileCode, list);
                }

                numbers.Add(filename, number);
                numberList[fileCode].Add(number);
            }

            foreach (string filename in filenames)
            {
                if (numbers[filename] > 1)
                {
                    string fileCode = filename;
                    int index = filename.LastIndexOf('_');
                    if (index != -1)
                    {
                        fileCode = filename.Substring(0, index);
                        index = fileCode.IndexOf('_') + 1;
                        if (index != -1)
                        {
                            fileCode = fileCode.Substring(index);
                        }
                    }

                    if (!numberList[fileCode].Contains(numbers[filename] - 1))
                    {
                        syntaxErrors.Add(string.Format(msg, filename));
                    }
                }
            }

            return syntaxErrors;
        }

        private List<string> CheckWavFilenameExistence(IEnumerable<string> filenames, Dictionary<string, string> lineFilenames, string characterName)
        {
            List<string> existenceErrors = new List<string>();
            foreach (string filename in filenames)
            {
                string character = null;
                if (!lineFilenames.TryGetValue(filename, out character))
                {
                    existenceErrors.Add(string.Format("The wav file '{0}.wav' doesn't exist in any dstar file.", filename));
                    continue;
                }

                if (character != characterName)
                {
                    existenceErrors.Add(string.Format("The wav file '{0}.wav' isn't for then character '{1}'", filename, characterName));
                    continue;
                }
            }

            return existenceErrors;
        }
    } // Controller
} // DialogueStar
