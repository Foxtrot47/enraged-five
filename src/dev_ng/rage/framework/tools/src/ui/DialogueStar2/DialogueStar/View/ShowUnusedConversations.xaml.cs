﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.IO;
using RSG.Model.Dialogue.Export;


namespace DialogueStar.View
{
    /// <summary>
    /// Interaction logic for ShowUnusedConversations.xaml
    /// </summary>
    public partial class ShowUnusedConversations : Window
    {
        #region Public properties

        public string ScriptLiteralFile
        {
            get;
            set;
        }

        public string OutputFolder
        {
            get;
            set;
        }

        public string DStarFileFolder
        {
            get;
            set;
        }

        public string Output
        {
            get;
            set;
        }

        public bool IsRunable
        {
            get
            {
                return !String.IsNullOrEmpty(ScriptLiteralFile) &&
                       !String.IsNullOrEmpty(OutputFolder) &&
                       !String.IsNullOrEmpty(DStarFileFolder);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        public ShowUnusedConversations()
        {
            InitializeComponent();
        }

        #endregion

        #region Event handlers

        /// <summary>
        /// The main cancel button: it closes the dialogue returning a false result
        /// so that the calling function can take the correct action
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void Cancel_Click(Object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        
        /// <summary>
        /// Run the process.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void OK_Click(Object sender, RoutedEventArgs e)
        {
            Cursor currentCursor = Cursor;
            Cursor = Cursors.Wait;

            if (!IsRunable)
            {
                MessageBox.Show("You must specify the script literal file and the two folders");
                return;
            }

            ProcessData();

            Cursor = currentCursor;
        }

        /// <summary>
        /// Event handler to set a folder location.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void SelectFolder_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            TextBox box = sender as TextBox;
            System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();

            bool ok = dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK;
            if (ok)
            {
                box.Text = dialog.SelectedPath;
            }
        }

        /// <summary>
        /// Event handler to set a file location.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ScriptLiterals_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            TextBox box = sender as TextBox;
            Microsoft.Win32.OpenFileDialog ofd = new Microsoft.Win32.OpenFileDialog();
            ofd.Filter = "Text files (*.txt)|*.txt";
            bool? ok = ofd.ShowDialog();
            if (ok.HasValue && ok.Value)
            {
                box.Text = ofd.FileName;
            }
        }

        /// <summary>
        /// Key not found event handler.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Arguments.</param>
        private void Compare_KeyNotFound(object sender, KeyExtractorEventArgs e)
        {
            Output += e.ConversationKey + Console.Out.NewLine;
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Process the data.
        /// </summary>
        private void ProcessData()
        {
            string keyListFile = "keys.txt";
            string unusedKeys = "unusedKeys.txt";
            keyListFile = Path.Combine(OutputFolder, keyListFile);
            unusedKeys = Path.Combine(OutputFolder, unusedKeys);

            if (!Directory.Exists(OutputFolder)) { Directory.CreateDirectory(OutputFolder); }

            KeyExtractor extractor = new KeyExtractor(keyListFile, DStarFileFolder);
            extractor.Run();

            Output = "";

            CompareFiles compare = new CompareFiles();
            compare.KeyNotFound += new KeyExtractorEventHandler(Compare_KeyNotFound);
            compare.Compare(keyListFile, ScriptLiteralFile);
            compare.KeyNotFound -= Compare_KeyNotFound;

            Write(Output, unusedKeys);

            Conversations.Text = Output;
        }

        /// <summary>
        /// Write out the unused keys to a file.
        /// </summary>
        /// <param name="keys">Keys.</param>
        /// <param name="keyFile">File.</param>
        private void Write(string keys, string keyFile)
        {
            if (File.Exists(keyFile))
            {
                File.Delete(keyFile);
            }

            using (Stream s = File.Open(keyFile, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read))
            {
                using (TextWriter writer = new StreamWriter(s))
                {
                    writer.Write(keys);
                }
            }
        }

        #endregion
    }
}
