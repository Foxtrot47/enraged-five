﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace DialogueStar
{
    /// <summary>
    /// Class used to have an image that is able to be gray when the control is not enabled.
    /// </summary>
    public class AutoGreyscaleImage : Image
    {
        #region Constructor(s)

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        public AutoGreyscaleImage()
        {
            // Override the metadata of the IsEnabled property, in a try catch block because only need to do it once
            try
            {
                if (iOverridden == 0)
                {
                    IsEnabledProperty.AddOwner(typeof(AutoGreyscaleImage), new FrameworkPropertyMetadata(true, new PropertyChangedCallback(OnAutoGreyScaleImageIsEnabledPropertyChanged)));
                    iOverridden++;
                }
            }
            catch
            {
            }
        }

        #endregion // Constructor(s)

        #region Overridden Function(s)

        /// <summary>
        /// Need to make sure that images inside templates that get initialised i.e get a source after the enabled property changes also get 
        /// greyed out.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
            OnAutoGreyScaleImageInitialised(this, new DependencyPropertyChangedEventArgs(IsEnabledProperty, IsEnabled, IsEnabled));
        }

        #endregion // Overridden Function(s)

        #region Event Callbacks

        /// <summary>
        /// Called when auto grey scale image is enabled property changed.
        /// </summary>
        /// <param name="source">The source of the event</param>
        /// <param name="args">The arguments for the event</param>
        private static void OnAutoGreyScaleImageIsEnabledPropertyChanged(DependencyObject source, DependencyPropertyChangedEventArgs args)
        {
            var autoGreyScaleImg = source as AutoGreyscaleImage;
            var isEnable = Convert.ToBoolean(args.NewValue);

            // Make sure that the image has been initialised (i.e has a valid source), images that are 
            // inside a template will reach here before being initialised.
            if (autoGreyScaleImg.IsInitialized == false)
                return;

            if (autoGreyScaleImg != null)
            {
                if (!isEnable)
                {
                    // Get the source bitmap
                    BitmapImage bitmapImage = new BitmapImage(new Uri(autoGreyScaleImg.Source.ToString()));

                    // Convert it to Gray
                    autoGreyScaleImg.Source = new FormatConvertedBitmap(bitmapImage, PixelFormats.Gray32Float, null, 0);

                    // Create Opacity Mask for greyscale image as FormatConvertedBitmap does not keep transparency info
                    autoGreyScaleImg.OpacityMask = new ImageBrush(bitmapImage);
                }
                else
                {
                    // Set the Source property to the original value.
                    autoGreyScaleImg.Source = ((FormatConvertedBitmap)autoGreyScaleImg.Source).Source;

                    // Reset the Opcity Mask
                    autoGreyScaleImg.OpacityMask = null;
                }
            }
        }

        /// <summary>
        /// Need to make sure that images inside templates that get initialised i.e get a source after the enabled property changes also get 
        /// greyed out.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        private static void OnAutoGreyScaleImageInitialised(DependencyObject source, DependencyPropertyChangedEventArgs args)
        {
            var autoGreyScaleImg = source as AutoGreyscaleImage;
            var isEnable = Convert.ToBoolean(args.NewValue);

            if (autoGreyScaleImg != null)
            {
                if (!isEnable)
                {
                    // Get the source bitmap
                    if (autoGreyScaleImg.Source is FormatConvertedBitmap)
                    {
                        BitmapImage bitmapImage = new BitmapImage(new Uri((autoGreyScaleImg.Source as FormatConvertedBitmap).Source.ToString()));
                        autoGreyScaleImg.Source = new FormatConvertedBitmap(bitmapImage, PixelFormats.Gray32Float, null, 0);
                        autoGreyScaleImg.OpacityMask = new ImageBrush(bitmapImage);
                    }
                    else
                    {
                        BitmapImage bitmapImage = new BitmapImage(new Uri(autoGreyScaleImg.Source.ToString()));

                        // Convert it to Gray
                        autoGreyScaleImg.Source = new FormatConvertedBitmap(bitmapImage, PixelFormats.Gray32Float, null, 0);

                        // Create Opacity Mask for greyscale image as FormatConvertedBitmap does not keep transparency info
                        autoGreyScaleImg.OpacityMask = new ImageBrush(bitmapImage);
                    }
                }
            }
        }

        #endregion // Event Callbacks

        static int iOverridden = 0;
    }
}
