﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Base.Collections;

namespace DialogueStar.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Properties and associate member data

        /// <summary>
        /// Represents whether the search panel is currently visible this is used to bind the menu option to the actual visibility of the panel
        /// in the two way binding.
        /// </summary>
        public static DependencyProperty IsSearchPanelVisible = DependencyProperty.Register("SearchPanelVisible", typeof(Boolean), typeof(MainWindow));
        public Boolean SearchPanelVisible
        {
            get { return (Boolean)GetValue(IsSearchPanelVisible); }
            set 
            { 
                SetValue(IsSearchPanelVisible, value);
                SearchSeparator.Visibility = (value == true) ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        #endregion // Properties and associate member data

        #region Constructor(s)

        /// <summary>
        /// Default constructor that calls the automated initalizeComponent function located in MainWindow.g.i.cs
        /// It also sets default parameters and binds the main tab control to the list of opened dialogue files.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            DialogueTabControl1.DialogueTabControl.ItemsSource = App.Controller.Missions;
            this.DataContext = App.Controller;

            // Need to add the callback for the recent file menu items being clicked
            RecentFileList.MenuClick += (s, e) => OpenRecent_Click(e.Filepath);

            SearchPanelVisible = false;

            IsSearchPanelVisible.OverrideMetadata(typeof(MainWindow), new PropertyMetadata(new PropertyChangedCallback(OnSearchPanelVisibleChange)));
        }

        #endregion // Constructor(s)

        #region Menu Callbacks

        /// <summary>
        /// Creates a new dialogue sheet and adds to to the tab control binding
        /// then imports the script file into that new dialogue file
        /// </summary>
        private void Import_Click(Object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            dlg.Multiselect = false;
            dlg.Filter = "Word Documents (*.doc; *.docx)|*.doc;*.docx";
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                if (App.Controller.SelectedDialogue == null || App.Controller.Missions.Count == 0)
                {
                    App.Controller.CreateNewDialogue();
                    App.Controller.SelectedDialogue.Conversations.RemoveAt(0);
                }

                App.Controller.SelectedDialogue.MissingCharacterCallback = MissingCharacterCallback;
                App.Controller.SelectedDialogue.ImportScriptFile(dlg.FileName);
                App.Controller.SelectedDialogue = App.Controller.SelectedDialogue;
            }
        }

        private void MissingCharacterCallback(Object sender, RSG.Model.Dialogue.MissingCharacterCallbackEventArgs e)
        {
            MissingCharacterDialog dialog = new MissingCharacterDialog(e.Name);
            dialog.Owner = Application.Current.MainWindow;
            if (dialog.ShowDialog() == true)
            {
                Dictionary<Guid, String> characters = new Dictionary<Guid, String>(App.Controller.Configurations.Characters);
                characters.Add(Guid.NewGuid(), e.Name);
                SortedDictionary<String, Guid> sortedList = new SortedDictionary<String, Guid>();
                foreach (KeyValuePair<Guid, String> character in characters)
                {
                    sortedList.Add(character.Value, character.Key);
                }

                if (sortedList.ContainsKey("SFX"))
                {
                    characters.Clear();
                    foreach (KeyValuePair<String, Guid> character in sortedList)
                    {
                        if (character.Key != "SFX")
                        {
                            characters.Add(character.Value, character.Key);
                        }
                    }

                    characters.Add(sortedList["SFX"], "SFX");
                }

                App.Controller.Configurations.Characters = new ObservableDictionary<Guid, String>(characters);
                App.Controller.SaveConfigFile();
            }
        }

        /// <summary>
        /// Creates a new dialogue sheet and adds to to the tab control binding.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void New_Click(Object sender, RoutedEventArgs e)
        {
            App.Controller.CreateNewDialogue();
        }

        /// <summary>
        /// Closes the current dialogue that is selected on the tab view. If
        /// there is no tab selected this doesn't do anything.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Close_Click(Object sender, RoutedEventArgs e)
        {
            App.Controller.CloseCurrentDialogue();
        }

        /// <summary>
        /// Closes all of the dialogues that are open in the tab view. If
        /// there are no tabs open this doesn't do anything.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseAll_Click(Object sender, RoutedEventArgs e)
        {
            App.Controller.CloseAllDialogue();
        }

        /// <summary>
        /// Saves the current dialogue that is selected on the tab view. If
        /// there is no tab selected this doesn't do anything.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Save_Click(Object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            String filename = String.Empty;
            if (App.Controller.SaveDialogue(App.Controller.SelectedDialogue, false, ref filename))
            {
                if (filename != String.Empty)
                {
                    this.RecentFileList.InsertFile(filename);
                }
            }
            this.Cursor = Cursors.Arrow;
        }

        /// <summary>
        /// Saves the current dialogue that is selected on the tab view. If
        /// there is no tab selected this doesn't do anything. Makes sure that
        /// the save dialogue appears everytime
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveAs_Click(Object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            String filename = String.Empty;
            if (App.Controller.SaveAsDialogue(App.Controller.SelectedDialogue, 1, false, ref filename))
            {
                if (filename != String.Empty)
                {
                    this.RecentFileList.InsertFile(filename);
                }
            }
            this.Cursor = Cursors.Arrow;
        }

        /// <summary>
        /// Saves all of the dialogue files that are currently open
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveAll_Click(Object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            List<String> filenames = App.Controller.SaveAll();
            foreach (String filename in filenames)
            {
                this.RecentFileList.InsertFile(filename);
            }
            this.Cursor = Cursors.Arrow;
        }

        /// <summary>
        /// Opens the open dialog and lets the user select a dstar file they wish to open
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Open_Click(Object sender, RoutedEventArgs e)
        {
            List<String> filenames = null;
            if (App.Controller.OpenDialogue(out filenames))
            {
                foreach (String filename in filenames)
                {
                    this.RecentFileList.InsertFile(filename);
                }
            }
        }

        /// <summary>
        /// Opens a filename when the recent menu item has been clicked
        /// </summary>
        /// <param name="filename">The filename to open</param>
        private void OpenRecent_Click(String filename)
        {
            if (App.Controller.OpenDialogue(filename))
            {
                if (filename != String.Empty)
                {
                    this.RecentFileList.InsertFile(filename);
                }
            }
        }

        /// <summary>
        /// Clears the recent file list as long as the user presses yes in the confirmation message box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClearRecent_Click(Object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Are you sure you want to clear the recent file list?", "Dialogue Star", MessageBoxButton.YesNo);

            if (result == MessageBoxResult.Yes)
            {
                this.RecentFileList.ClearAllFiles();
            }
        }

        /// <summary>
        /// Exports all of the dstar files found in the dialogue directory that are of type mission
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MissionsExport_Click(Object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            App.Controller.ExportMissions(true, true);
            this.Cursor = Cursors.Arrow;
        }

        /// <summary>
        /// Exports all of the dstar files found in the dialogue directory that are of type mission
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MissionsPlaceholderSPExport_Click(Object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            App.Controller.ExportMissionsForTranslation(true, false);
            this.Cursor = Cursors.Arrow;
        }

        /// <summary>
        /// Exports all of the dstar files found in the dialogue directory that are of type mission
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MissionsPlaceholderMPExport_Click(Object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            App.Controller.ExportMissionsForTranslation(true, true);
            this.Cursor = Cursors.Arrow;
        }  

        /// <summary>
        /// Exports all of the dstar files found in the dialogue directory that are of type mission
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CurrentMissionPlaceholderExport_Click(Object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            App.Controller.ExportCurrentMissionsPlaceholder();
            this.Cursor = Cursors.Arrow;
        }

        /// <summary>
        /// Exports all of the dstar files found in the dialogue directory that are of type mission
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SPMissionsExport_Click(Object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            App.Controller.ExportMissions(true, false);
            this.Cursor = Cursors.Arrow;
        }

        /// <summary>
        /// Exports all of the dstar files found in the dialogue directory that are of type mission
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MPMissionsExport_Click(Object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            App.Controller.ExportMissions(false, true);
            this.Cursor = Cursors.Arrow;
        }

        /// <summary>
        /// Exports the random dialogue file with the identifiers on them first
        /// and then export the random files making sure that the character idices are the
        /// same on both files.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExportRandomMissions_Click(Object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            App.Controller.ExportAllRandomMissions();
            this.Cursor = Cursors.Arrow;
        }

        /// <summary>
        /// Exports the dialogue file for the writers and voice actors
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WriterExport_Click(Object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            App.Controller.ExportForWriters(this);
            this.Cursor = Cursors.Arrow;
        }

        /// <summary>
        /// Exports the current mission to a word document in a specific format
        /// </summary>
        private void ExportMissionWord_Click(Object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            App.Controller.ExportToWord(false);
            this.Cursor = Cursors.Arrow;
        }

        /// <summary>
        /// Exports the current mission to a word document in a specific format
        /// </summary>
        private void ExportMissionsWord_Click(Object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            App.Controller.ExportToWord(true);
            this.Cursor = Cursors.Arrow;
        }

        /// <summary>
        /// Exports the current mission to a word document in a specific format
        /// </summary>
        private void ExportConversationWord_Click(Object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            App.Controller.ExportConversationToWord();
            this.Cursor = Cursors.Arrow;
        }

        /// <summary>
        /// Opens the export modifing window dialog so that the user can change the export paths
        /// for the application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExportPreferences_Click(Object sender, RoutedEventArgs e)
        {
            App.Controller.ShowExportDialog(this);
        }

        /// <summary>
        /// Show the unused conversation dialog.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Args.</param>
        private void UnusedConvo_Click(Object sender, RoutedEventArgs e)
        {
            App.Controller.ShowUnusedConversationsWindow();
        }

        /// <summary>
        /// Reloads the config file and grads latest before doing so
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReloadConfig_Click(Object sender, RoutedEventArgs e)
        {
            App.Controller.ReloadConfigFile(this);
        }

        /// <summary>
        /// Opens the character modifing window dialog so that the user can add, remove
        /// rename, and merge different characters
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Characters_Click(Object sender, RoutedEventArgs e)
        {
            App.Controller.ShowCharacterModifyDialog(this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReplaceCharacter_Click(Object sender, RoutedEventArgs e)
        {
            App.Controller.ShowCharacterReplacementDialog(this);
        }

        /// <summary>
        /// Undoes a single operation on the currently selected dialogue.
        /// If no dialogue is selected this doesn't do anything.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Undo_Click(Object sender, RoutedEventArgs e)
        {
            int index = App.Controller.Missions.IndexOf(App.Controller.SelectedDialogue);
            if (App.Controller.UndoManagers[index].UndoStack.Count == 0)
                return;

            int previousConversationIndex = App.Controller.SelectedDialogue.Conversations.IndexOf(App.Controller.SelectedConversation);
            int previousLineIndex = App.Controller.SelectedConversation.Lines.IndexOf(App.Controller.SelectedLine);

            if (App.Controller.SelectedDialogue != null)
                App.Controller.SelectedDialogue.Undo();

            // Need to clear the dirty state if the undo stack is empty
            int dialogueIndex = App.Controller.Missions.IndexOf(App.Controller.SelectedDialogue);
            if (App.Controller.UndoManagers[dialogueIndex].UndoStack.Count == 0)
            {
                App.Controller.SelectedDialogue.DirtyManager.Dirty = false;
            
                // In this case also need to delete any auto save files this dialogue sheet might have
                String autoSaveFilename = System.IO.Path.Combine(App.Controller.AutoSaveDirectory, App.Controller.SelectedDialogue.Name + "_Temp.das");
                if (File.Exists(autoSaveFilename) == true)
                {
                    File.Delete(autoSaveFilename);
                }
            }

            // This could of created a situation where no conversation or line is selected
            if (App.Controller.SelectedConversation == null)
            {
                if (previousConversationIndex <= 0 && App.Controller.SelectedDialogue.Conversations.Count > 0)
                {
                    App.Controller.SelectedConversation = App.Controller.SelectedDialogue.Conversations[0];
                }
                else if (previousConversationIndex == App.Controller.SelectedDialogue.Conversations.Count && App.Controller.SelectedDialogue.Conversations.Count > 0)
                {
                    App.Controller.SelectedConversation = App.Controller.SelectedDialogue.Conversations[App.Controller.SelectedDialogue.Conversations.Count - 1];
                }
                else
                {
                    App.Controller.SelectedConversation = App.Controller.SelectedDialogue.Conversations[previousConversationIndex];
                }
            }
            if (App.Controller.SelectedLine == null)
            {
                if (previousLineIndex <= 0 && App.Controller.SelectedConversation.Lines.Count > 0)
                {
                    App.Controller.SelectedLine = App.Controller.SelectedConversation.Lines[0];
                }
                else if (previousLineIndex == App.Controller.SelectedConversation.Lines.Count && App.Controller.SelectedConversation.Lines.Count > 0)
                {
                    App.Controller.SelectedLine = App.Controller.SelectedConversation.Lines[App.Controller.SelectedConversation.Lines.Count - 1];
                }
                else
                {
                    App.Controller.SelectedLine = App.Controller.SelectedConversation.Lines[previousLineIndex];
                }
            }
        }

        /// <summary>
        /// Redoes a single operation on the currently selected dialogue
        /// If no dialogue is selected this doesn't do anything.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Redo_Click(Object sender, RoutedEventArgs e)
        {
            int index = App.Controller.Missions.IndexOf(App.Controller.SelectedDialogue);
            if (App.Controller.UndoManagers[index].RedoStack.Count == 0)
                return;

            if (App.Controller.SelectedDialogue != null)
                App.Controller.SelectedDialogue.Redo();

            int dialogueIndex = App.Controller.Missions.IndexOf(App.Controller.SelectedDialogue);
            if (App.Controller.UndoManagers[dialogueIndex].UndoStack.Count != 0)
            {
                App.Controller.SelectedDialogue.DirtyManager.Dirty = true;
            }
        }

        /// <summary>
        /// This closes all of the sheets currently opened in case any of them need saving
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Exit_Click(Object sender, RoutedEventArgs e)
        {
            App.Controller.CloseAllDialogue();

            this.Close();
        }

        /// <summary>
        /// Opens the about dialog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void About_Click(Object sender, RoutedEventArgs e)
        {
            App.Controller.ShowAboutDialog(this);
        }

        /// <summary>
        /// Validates the current opened file against a folder of wav files.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FolderValidation_Click(Object sender, RoutedEventArgs e)
        {
            App.Controller.FolderValidation();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        private void SfxValidation_Click(Object sender, RoutedEventArgs e)
        {
            App.Controller.SfxValidation();
        }
        #endregion // Menu Callbacks

        #region Command Bindings

        private void CommandBindingUndo_CanExecute(Object sender, CanExecuteRoutedEventArgs e)
        {
            if (App.Controller.SelectedDialogue == null)
            {
                e.CanExecute = false;
            }
            else
            {
                int index = App.Controller.Missions.IndexOf(App.Controller.SelectedDialogue);
                if (App.Controller.UndoManagers[index].UndoStack.Count == 0)
                {
                    e.CanExecute = false;
                }
                else
                {
                    e.CanExecute = true;
                }
            }

            e.Handled = true;
        }

        private void CommandBindingUndo_Executed(Object sender, ExecutedRoutedEventArgs e)
        {
            this.Undo_Click(this, new RoutedEventArgs());
        }

        private void CommandBindingRedo_CanExecute(Object sender, CanExecuteRoutedEventArgs e)
        {
            if (App.Controller.SelectedDialogue == null)
            {
                e.CanExecute = false;
            }
            else
            {
                int index = App.Controller.Missions.IndexOf(App.Controller.SelectedDialogue);
                if (App.Controller.UndoManagers[index].RedoStack.Count == 0)
                {
                    e.CanExecute = false;
                }
                else
                {
                    e.CanExecute = true;
                }
            }

            e.Handled = true;
        }

        private void CommandBindingRedo_Executed(Object sender, ExecutedRoutedEventArgs e)
        {
            this.Redo_Click(this, new RoutedEventArgs());
        }

        private void CommandBindingSave_CanExecute(Object sender, CanExecuteRoutedEventArgs e)
        {
            if (App.Controller.SelectedDialogue == null)
            {
                e.CanExecute = false;
            }
            else
            {
                e.CanExecute = true;
            }

            e.Handled = true;
        }

        private void CommandBindingSave_Executed(Object sender, ExecutedRoutedEventArgs e)
        {
            Save_Click(this, new RoutedEventArgs());
        }

        #endregion // Command Bindings

        #region Event Handling

        /// <summary>
        /// Need to make sure that any dirty missions get saved before the window closes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closing(Object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (App.Controller.CloseAllDialogue() == false)
                e.Cancel = true;
        }

        /// <summary>
        /// Get fired when something is dragged into the main window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainWindow_DragEnter(Object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                String[] filenames = (String[])e.Data.GetData(DataFormats.FileDrop);

                foreach (String filename in filenames)
                {
                    if (System.IO.Path.GetExtension(filename) != ".dstar")
                    {
                        e.Effects = DragDropEffects.None;
                        return;
                    }
                }
                e.Effects = DragDropEffects.Copy;
            }
            else
            {
                e.Effects = DragDropEffects.None;
            }
        }

        /// <summary>
        /// Get fired when something is dropped into the main window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainWindow_Drop(Object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                String[] filenames = (String[])e.Data.GetData(DataFormats.FileDrop);

                foreach (String filename in filenames)
                {
                    if (App.Controller.OpenDialogue(filename))
                    {
                        this.RecentFileList.InsertFile(filename);
                    }
                }
            }
        }

        /// <summary>
        /// Gets called as soon as the window is visible, this finds out if there are any auto saves and
        /// asks the user if they want to open them or discard them.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_ContentRendered(Object sender, EventArgs e)
        {
            App.Controller.AutoSaveTimer.Enabled = false;
            // Determine if there are any auto save files that need recovering
            String[] foundFiles = Directory.GetFiles(App.Controller.AutoSaveDirectory, "*.das");

            if (foundFiles.Length > 0)
            {
                MessageBoxResult result = MessageBox.Show("There appears to be auto save files to recover, would you like to recover them now, they will be discarded if not?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (result == MessageBoxResult.No)
                {
                    // Just delete the auto save files
                    foreach (String filename in foundFiles)
                    {
                        File.Delete(filename);
                    }
                }
                else if (result == MessageBoxResult.Yes)
                {
                    // Load up the auto save files and show them in the main tab window.
                    foreach (String filename in foundFiles)
                    {
                        App.Controller.LoadAutosaveFile(filename);
                    }
                }
            }
            App.Controller.AutoSaveTimer.Enabled = true;
        }

        /// <summary>
        /// Get fired if the user presses the close button in the search panel.
        /// All this does if set the dependency propery SearchPanelVisible to false and since the search 
        /// panels visibility is bound to this it works.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchPanel_CloseButtonClicked(object sender, EventArgs e)
        {
            SearchPanelVisible = false;
        }

        /// <summary>
        /// Gets called whenever the search panels visibility state gets changed so that we can make sure that the 
        /// row heights/visibility are sorted so that everything looks nice.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        private static void OnSearchPanelVisibleChange(DependencyObject source, DependencyPropertyChangedEventArgs args)
        {
            (source as MainWindow).SearchSeparator.Visibility = ((Boolean)args.NewValue == true) ? Visibility.Visible : Visibility.Collapsed;
            int rowIndex = Grid.GetRow((source as MainWindow).SearchPanel);
            RowDefinition row = (source as MainWindow).MainGrid.RowDefinitions[rowIndex];
            if ((Boolean)args.NewValue == true)
            {
                row.Height = GridLength.Auto;
                if (row.Height.Value < 150)
                {
                    row.Height = new GridLength(150);
                }
            }
            else
            {
                row.Height = new GridLength(0);
            }
        }

        /// <summary>
        /// Need to make sure that the controller has a reference to this main window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            App.Controller.MainWindow = this;
        }

        /// <summary>
        /// Need to make sure that the reference to this window in the controller is updated to null
        /// as well
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {
            App.Controller.MainWindow = null;
        }

        #endregion // Event Handling

        #region Debug

        /// <summary>
        /// Single function that can be called when the B key is pressed as well as the control key
        /// For debug purposes only
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_KeyDown(Object sender, KeyEventArgs e)
        {
            if (e.Key == Key.B && Keyboard.IsKeyDown(Key.LeftCtrl))
            {
            }
        }

        #endregion // Debug       

        private void CommandBindingFind_CanExecute(Object sender, CanExecuteRoutedEventArgs e)
        {
            if (App.Controller.Missions == null || App.Controller.Missions.Count == 0)
                e.CanExecute = false;
            else
                e.CanExecute = true;
        }

        private void CommandBindingFind_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.SearchPanel.SearchRange = SearchPanel.SearchRanges[0];
            this.SearchPanelVisible = true;
        }

        private void CommandBindingFindAll_CanExecute(Object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CommandBindingFindAll_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.SearchPanel.SearchRange = SearchPanel.SearchRanges[2];
            this.SearchPanelVisible = true;
        }

        private void TranslationSPExport_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            App.Controller.ExportMissionsForTranslation(false, false);
            this.Cursor = Cursors.Arrow;
        }

        private void TranslationMPExport_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            App.Controller.ExportMissionsForTranslation(false, true);
            this.Cursor = Cursors.Arrow;
        }

    } // MainWindow

    #region Commands

    /// <summary>
    /// Defines commands that can be applied to a dockable pane
    /// </summary>
    public sealed class WindowCommands
    {
        private static object syncRoot = new object();

        private static RoutedUICommand searchAllCommand = null;

        /// <summary>
        /// This command closes the <see cref="DockablePane"/> and closes all the contained <see cref="DockableContent"/>s inside it
        /// </summary>
        public static RoutedUICommand SearchAll
        {
            get
            {
                lock (syncRoot)
                {
                    if (null == searchAllCommand)
                    {
                        InputGestureCollection input = new InputGestureCollection();
                        input.Add(new KeyGesture(Key.F, ModifierKeys.Control | ModifierKeys.Shift));

                        searchAllCommand = new RoutedUICommand("Search in All DStar Files", "SearchAll", typeof(WindowCommands), input);
                    }
                }
                return searchAllCommand;
            }
        }
    }


    #endregion // Commands

    #region Converters

    /// <summary>
    /// Converter to convert an object value to a Boolean value based on the object type
    /// Object type Line = true
    /// Object type Anything else = false
    /// </summary>
    [ValueConversion(typeof(Object), typeof(Boolean))]
    public class LineToBooleanConverter : IValueConverter
    {
        #region Convert Functions

        /// <summary>
        /// Function to convert a object type to a boolean value
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is RSG.Model.Dialogue.Line)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Function to convert a boolean value into a object.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            throw new Exception();
        }

        #endregion // Convert Functions

    } // LineToBooleanConverter

    /// <summary>
    /// Converter to convert an object value to a Boolean value based on the object type
    /// Object type Conversation = true
    /// Object type Anything else = false
    /// </summary>
    [ValueConversion(typeof(Object), typeof(Boolean))]
    public class ConversationToBooleanConverter : IValueConverter
    {
        #region Convert Functions

        /// <summary>
        /// Function to convert a object type to a boolean value
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is RSG.Model.Dialogue.Conversation)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Function to convert a boolean value into a object.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            throw new Exception();
        }

        #endregion // Convert Functions

    } // ConversationToBooleanConverter

    /// <summary>
    /// Converter to convert an object value to a Boolean value based on the object type
    /// Object type MissionDialogue = true
    /// Object type Anything else = false
    /// </summary>
    [ValueConversion(typeof(Object), typeof(Boolean))]
    public class MissionDialogueToBooleanConverter : IValueConverter
    {
        #region Convert Functions

        /// <summary>
        /// Function to convert a object type to a boolean value
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is RSG.Model.Dialogue.MissionDialogue)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Function to convert a boolean value into a object.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            throw new Exception();
        }

        #endregion // Convert Functions

    } // MissionDialogueToBooleanConverter

    /// <summary>
    /// Converter to convert an Boolean value to a Boolean value
    /// False =  True
    /// True = False
    /// </summary>
    [ValueConversion(typeof(Boolean), typeof(Boolean))]
    public class InverseBooleanToBooleanConverter : IValueConverter
    {
        #region Convert Functions

        /// <summary>
        /// Function to convert a boolean to a visibility value
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            Boolean BoolValue = false;
            bool.TryParse(value.ToString(), out BoolValue);
            if (BoolValue == true)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Function to convert a visibility value into a Boolean.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            Boolean BoolValue = false;
            bool.TryParse(value.ToString(), out BoolValue);
            if (BoolValue == true)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion // Convert Functions

    } // InverseBooleanToBooleanConverter

    /// <summary>
    /// Converter to convert an Boolean value to a visibility value
    /// False =  Visible
    /// True = Collapsed
    /// </summary>
    public class InverseBooleanToVisibilityConverter : IValueConverter
    {
        #region Convert Functions

        /// <summary>
        /// Function to convert a boolean to a visibility value
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            Boolean BoolValue = false;
            bool.TryParse(value.ToString(), out BoolValue);
            if (BoolValue == true)
            {
                return Visibility.Collapsed;
            }
            else
            {
                return Visibility.Visible;
            }
        }

        /// <summary>
        /// Function to convert a visibility value into a Boolean.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            Boolean result = false;
            try
            {
                if (value is Visibility)
                {
                    Visibility x = (Visibility)value;
                    if (x == Visibility.Collapsed)
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
            }
            catch (Exception)
            {
            }

            return result;
        }

        #endregion // Convert Functions

    } // IntegerToVisibility

    /// <summary>
    /// Converter to convert an Visibility value to a Boolean value
    /// Collapsed/Hidden = False
    /// Visible = True
    /// False = Collapsed
    /// True = Visible
    /// </summary>
    [ValueConversion(typeof(Visibility), typeof(Boolean))]
    public class VisibilityToBooleanConverter : IValueConverter
    {
        #region Convert Functions

        /// <summary>
        /// Function to convert a visibility value to a boolean
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is Visibility)
            {
                if ((Visibility)value == Visibility.Visible)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Function to convert a boolean value into a visibility.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            Boolean BoolValue = false;
            bool.TryParse(value.ToString(), out BoolValue);
            if (BoolValue == true)
            {
                return Visibility.Collapsed;
            }
            else
            {
                return Visibility.Visible;
            }
        }

        #endregion // Convert Functions

    } // VisibilityToBooleanConverter

    /// <summary>
    /// Converter to convert an integer value to a visibility value
    /// 0 = Collapsed
    /// Anything else = Visible
    /// </summary>
    [ValueConversion(typeof(int), typeof(Visibility))]
    public class IntegerToVisibility : IValueConverter
    {
        #region Convert Functions

        /// <summary>
        /// Function to convert a integer to a visibility value
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            Visibility visibility = Visibility.Visible;
            try
            {
                int x = int.Parse(value.ToString());
                if (x != 0)
                {
                    visibility = Visibility.Visible;
                }
                else
                {
                    visibility = Visibility.Collapsed;
                }
            }
            catch (Exception)
            {
            }
            return visibility;
        }

        /// <summary>
        /// Function to convert a visibility value into a integer.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            int result = 0;
            try
            {
                if (value is Visibility)
                {
                    Visibility x = (Visibility)value;
                    if (x == Visibility.Collapsed)
                    {
                        result = 0;
                    }
                    else
                    {
                        result = 1;
                    }
                }
            }
            catch (Exception)
            {
            }

            return result;
        }

        #endregion // Convert Functions

    } // IntegerToVisibility

    /// <summary>
    /// Converter to convert an integer value to a visibility value
    /// 0 = Visible
    /// Anything else = Collapsed
    /// </summary>
    [ValueConversion(typeof(int), typeof(Visibility))]
    public class InverseIntegerToVisibility : IValueConverter
    {
        #region Convert Functions

        /// <summary>
        /// Function to convert a integer to a visibility value
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            Visibility visibility = Visibility.Visible;
            try
            {
                int x = int.Parse(value.ToString());
                if (x != 0)
                {
                    visibility = Visibility.Collapsed;
                }
                else
                {
                    visibility = Visibility.Visible;
                }
            }
            catch (Exception)
            {
            }
            return visibility;
        }

        /// <summary>
        /// Function to convert a visibility value into a integer.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            int result = 0;
            try
            {
                if (value is Visibility)
                {
                    Visibility x = (Visibility)value;
                    if (x == Visibility.Collapsed)
                    {
                        result = 1;
                    }
                    else
                    {
                        result = 0;
                    }
                }
            }
            catch (Exception)
            {
            }

            return result;
        }

        #endregion // Convert Functions

    } // InverseIntegerToVisibility

    /// <summary>
    /// Converter to convert an integer value to a boolean value
    /// 0 = False
    /// Anything else = True
    /// </summary>
    public class IntegerToBoolean : IValueConverter
    {
        #region Convert Functions

        /// <summary>
        /// Function to convert a integer to a boolean value
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            Boolean result = false;
            try
            {
                int x = int.Parse(value.ToString());
                if (x != 0)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception)
            {
            }
            return result;
        }

        /// <summary>
        /// Function to convert a boolean value into a integer.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            int result = 0;
            try
            {
                if (value is Boolean)
                {
                    Boolean x = (Boolean)value;
                    if (x == true)
                    {
                        result = 1;
                    }
                    else
                    {
                        result = 0;
                    }
                }
            }
            catch (Exception)
            {
            }

            return result;
        }

        #endregion // Convert Functions

    } // IntegerToBoolean

    /// <summary>
    /// Converter to convert an integer value to a boolean value
    /// 0/1 = False
    /// Anything else = True
    /// False = 0
    /// True = 2
    /// </summary>
    public class IntegerToBooleanSearch : IValueConverter
    {
        #region Convert Functions

        /// <summary>
        /// Function to convert a integer to a boolean value
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            Boolean result = false;
            try
            {
                int x = int.Parse(value.ToString());
                if (x == 0 || x == 1)
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
            }
            catch (Exception)
            {
            }
            return result;
        }

        /// <summary>
        /// Function to convert a boolean value into a integer.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            int result = 0;
            try
            {
                if (value is Boolean)
                {
                    Boolean x = (Boolean)value;
                    if (x == true)
                    {
                        result = 2;
                    }
                    else
                    {
                        result = 0;
                    }
                }
            }
            catch (Exception)
            {
            }

            return result;
        }

        #endregion // Convert Functions

    } // IntegerToBoolean

    /// <summary>
    /// Converter to convert an integer value to a boolean value
    /// Guid.Empty = False
    /// Anything else = True
    /// </summary>
    public class GuidToBoolean : IValueConverter
    {
        #region Convert Functions

        /// <summary>
        /// Function to convert a Guid to a boolean value
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            Boolean result = false;
            try
            {
                Guid x = (Guid)value;
                if (x != null && x != Guid.Empty)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception)
            {
            }
            return result;
        }

        /// <summary>
        /// Function to convert a Boolean value into a Guid.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion // Convert Functions

    } // GuidToBoolean

    /// <summary>
    /// Converter to convert an integer value to a brush
    /// >= 8 = Red
    /// Anything else = White
    /// </summary>
    [ValueConversion(typeof(int), typeof(Brush))]
    public class SubtitleIDLengthToColorConverter : IValueConverter
    {
        #region Convert Functions

        /// <summary>
        /// Function to convert a int to a brush
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            int SubtitleIDLength = int.Parse(value.ToString());
            return (SubtitleIDLength >= 8 ? Brushes.Red : Brushes.White);
        }

        /// <summary>
        /// Function to convert a brush to a int
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion // Convert Functions

    } // SubtitleIDLengthToColorConverter

    /// <summary>
    /// Converter to convert an integer value to a brush
    /// >= 13 = Red
    /// Anything else = White
    /// </summary>
    [ValueConversion(typeof(int), typeof(Brush))]
    public class RootLengthToColorConverter : IValueConverter
    {
        #region Convert Functions

        /// <summary>
        /// Function to convert a int to a brush
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            int SubtitleIDLength = int.Parse(value.ToString());
            return (SubtitleIDLength >= 13 ? Brushes.Red : Brushes.White);
        }

        /// <summary>
        /// Function to convert a brush to a int
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion // Convert Functions

    } // RootLengthToColorConverter

    /// <summary>
    /// Converter to convert a boolean value to a brush
    /// True = SystemColour.HighlightBrush
    /// Anything else = White
    /// </summary>
    [ValueConversion(typeof(Boolean), typeof(Brush))]
    public class BooleanToColorConverter : IValueConverter
    {
        #region Convert Functions

        /// <summary>
        /// Function to convert a int to a brush
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            Boolean BoolValue = false;
            bool.TryParse(value.ToString(), out BoolValue);
            if (BoolValue == true)
            {
                return System.Windows.SystemColors.HighlightBrush;
            }
            else
            {
                return Brushes.White;
            }
        }

        /// <summary>
        /// Function to convert a brush to a bool
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion // Convert Functions

    } // BooleanToColorConverter

    /// <summary>
    /// Converter to convert a boolean value to a brush for text
    /// True = White
    /// False = Black
    /// </summary>
    [ValueConversion(typeof(Boolean), typeof(Brush))]
    public class TextBooleanToColorConverter : IValueConverter
    {
        #region Convert Functions

        /// <summary>
        /// Function to convert a int to a brush
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            Boolean BoolValue = false;
            bool.TryParse(value.ToString(), out BoolValue);
            if (BoolValue == true)
            {
                return Brushes.White;
            }
            else
            {
                return Brushes.Black;
            }
        }

        /// <summary>
        /// Function to convert a brush to a bool
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion // Convert Functions

    } // TextBooleanToColorConverter

    #endregion // Converters

} // DialogueStar.View
