﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DialogueStar.View
{
    /// <summary>
    /// Interaction logic for MissingCharacterDialog.xaml
    /// </summary>
    public partial class MissingCharacterDialog : Window
    {
        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public String MissingCharacterName
        {
            get;
            private set;
        }

        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default Constructor
        /// </summary>
        public MissingCharacterDialog()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        public MissingCharacterDialog(String characterName)
        {
            InitializeComponent();
            this.MissingCharacterName = characterName;
        }

        #endregion // Constructor

        #region Button Callbacks

        /// <summary>
        /// 
        /// </summary>
        private void LeaveClick(Object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        /// <summary>
        /// 
        /// </summary>
        private void AddClick(Object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        #endregion // Button Callbacks
    } // MissingCharacterDialog
} // DialogueStar.View
