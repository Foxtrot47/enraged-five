﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Model.Dialogue;

namespace DialogueStar.View
{
    /// <summary>
    /// Interaction logic for Conversations.xaml
    /// </summary>
    public partial class Conversations : UserControl
    {
        #region Constructor(s)

        public Conversations()
        {
            InitializeComponent();
            if (App.Controller != null)
            {
                App.Controller.ConversationUserControl = this;
            }
        }

        #endregion // Constructor(s)

        #region Button Logic

        /// <summary>
        /// Adds a new conversation underneath the currently selected one or at the end if none are selected.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddConversation_Click(Object sender, RoutedEventArgs e)
        {
            App.Controller.AddNewConversation(App.Controller.SelectedDialogue, this.ConversationsGrid.SelectedIndex);

            // Bring the new item into view (use the selected conversation on the controller as this is changed in the controller automatically
            this.ConversationsGrid.ScrollIntoView(App.Controller.SelectedConversation);
        }

        /// <summary>
        /// Removes the currently selected conversation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteConversation_Click(Object sender, RoutedEventArgs e)
        {
            // Cannot remove the last conversation
            if (this.ConversationsGrid.Items.Count <= 1) return;

            App.Controller.RemoveConversation(App.Controller.SelectedDialogue, this.ConversationsGrid.SelectedIndex);
        }

        /// <summary>
        /// Auto generates the filenames for all of the conversations in the selected dialogue
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MagicConversation_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(App.Controller.SelectedDialogue.MissionId))
            {
                App.Controller.AutoGenerateFilenamesForAllConversations();
            }
            else
            {
                MessageBox.Show("Unable to generate filenames at this time due to the fact that the Mission ID field is empty", "Filename Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// Unlocks all the conversation in the mission
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Unlock_Click(Object sender, RoutedEventArgs e)
        {
            App.Controller.UnlockAllConversation(this.DataContext as MissionDialogue);
        }

        #endregion // Button Logic

        #region DataGrid Callbacks

        /// <summary>
        /// This function gets called everytime the data context for the grid changes (i.e everytime the user selects a 
        /// different conversation) this function passes down the data context to the columns since columns are not a visual
        /// child for the data gris they don't automatically inhert the data context and you are unable to find
        /// the data context using a relative source.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConversationGrid_DataContextChanged(Object sender, DependencyPropertyChangedEventArgs e)
        {
            DataGrid grid = sender as DataGrid;
            if (grid != null)
            {
                foreach (DataGridColumn col in grid.Columns)
                {
                    col.SetValue(FrameworkElement.DataContextProperty, e.NewValue);
                }
            }
        }

        /// <summary>
        /// Used to select the conversation the user has just clicked on.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HeaderBorder_MouseLeftButtonDown(Object sender, MouseButtonEventArgs e)
        {
            App.Controller.SelectedConversation = (RSG.Model.Dialogue.Conversation)(((Border)sender).DataContext);
        }

        #endregion // DataGrid Callbacks

        #region Drag & Drop Logic

            #region Properties

            /// <summary>
            /// DraggedItem Dependency Property
            /// </summary>
            public static readonly DependencyProperty DraggedConversationProperty = DependencyProperty.Register("DraggedConversation", typeof(Conversation), typeof(Conversations));
            public Conversation DraggedConversation
            {
                get { return (Conversation)GetValue(DraggedConversationProperty); }
                set { SetValue(DraggedConversationProperty, value); }
            }

            /// <summary>
            /// A boolean value representing whether we are currently dragging the data grid row
            /// </summary>
            private Boolean m_isDragging = false;

            /// <summary>
            /// The starting mouse position when the dragging began
            /// </summary>
            Point StartDraggingPoint;

            #endregion // Properties

            #region Helper UI Function(s)

            /// <summary>
            /// This method is an alternative to WPF's
            /// <see cref="VisualTreeHelper.GetParent"/> method, which also
            /// supports content elements. Keep in mind that for content element,
            /// this method falls back to the logical tree of the element!
            /// </summary>
            /// <param name="child">The item to be processed.</param>
            /// <returns>The submitted item's parent, if available. Otherwise
            /// null.</returns>
            public DependencyObject GetParentObject(DependencyObject child)
            {
                if (child == null) return null;

                //handle content elements separately
                ContentElement contentElement = child as ContentElement;

                if (contentElement != null)
                {
                    DependencyObject parent = ContentOperations.GetParent(contentElement);
                    if (parent != null) return parent;

                    FrameworkContentElement fce = contentElement as FrameworkContentElement;
                    return fce != null ? fce.Parent : null;
                }

                //also try searching for parent in framework elements (such as DockPanel, etc)
                FrameworkElement frameworkElement = child as FrameworkElement;
                if (frameworkElement != null)
                {
                    DependencyObject parent = frameworkElement.Parent;
                    if (parent != null) return parent;
                }

                //if it's not a ContentElement/FrameworkElement, rely on VisualTreeHelper
                return VisualTreeHelper.GetParent(child);
            }

            /// <summary>
            /// Finds a parent of a given item on the visual tree.
            /// </summary>
            /// <typeparam name="T">The type of the queried item.</typeparam>
            /// <param name="child">A direct or indirect child of the
            /// queried item.</param>
            /// <returns>The first parent item that matches the submitted
            /// type parameter. If not matching item can be found, a null
            /// reference is being returned.</returns>
            public T TryFindParent<T>(DependencyObject child) where T : DependencyObject
            {
                //get parent item
                DependencyObject parentObject = GetParentObject(child);

                //we've reached the end of the tree
                if (parentObject == null)
                    return null;

                //check if the parent matches the type we're looking for
                T parent = parentObject as T;
                if (parent != null)
                {
                    return parent;
                }
                else
                {
                    //use recursion to proceed with next level
                    return TryFindParent<T>(parentObject);
                }
            }

            /// <summary>
            /// Tries to locate a given item within the visual tree,
            /// starting with the dependency object at a given position. 
            /// </summary>
            /// <typeparam name="T">The type of the element to be found
            /// on the visual tree of the element at the given location.</typeparam>
            /// <param name="reference">The main element which is used to perform
            /// hit testing.</param>
            /// <param name="point">The position to be evaluated on the origin.</param>
            public T TryFindFromPoint<T>(UIElement reference, Point point) where T : DependencyObject
            {
                DependencyObject element = reference.InputHitTest(point) as DependencyObject;

                if (element == null)
                {
                    return TryFindParent<T>(reference);
                }
                else if (element is T)
                {
                    return (T)element;
                }
                else
                {
                    return TryFindParent<T>(element);
                }
            }

            public ScrollViewer FindScrollViewerFromUIElement(DependencyObject root)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(root); i++)
                {
                    var child = VisualTreeHelper.GetChild(root, i);
                    if (child is ScrollViewer)
                        return child as ScrollViewer;

                    ScrollViewer result = FindScrollViewerFromUIElement(child);
                    if (result != null)
                        return result;
                }
                return null;
            }


            #endregion // Helper UI Function(s)

            #region Private Function(s)

            /// <summary>
            /// Closes the popup and resets the
            /// grid to read-enabled mode.
            /// </summary>
            private void ResetDragDrop()
            {
                m_isDragging = false;
                if (this.DragPopup != null)
                    this.DragPopup.IsOpen = false;
                this.ConversationsGrid.IsReadOnly = false;
                for (int i = 0; i < this.ConversationsGrid.Items.Count; i++)
                {
                    DataGridRow row = ConversationsGrid.ItemContainerGenerator.ContainerFromIndex(i) as DataGridRow;
                    if (row != null)
                        row.BorderThickness = new Thickness(0, 0, 0, 0);
                }
            }

            /// <summary>
            /// Starts the dragging operation if the left mouse button is clicked over a row.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            private void ConversationsGrid_PreviewMouseLeftButtonDown(Object sender, MouseButtonEventArgs e)
            {
                if (this.ConversationsGrid != null)
                {
                    // Find the clicked row
                    StartDraggingPoint = e.GetPosition(ConversationsGrid);

                    m_isDragging = true;

                    var draggedRow = TryFindFromPoint<DataGridRow>((UIElement)sender, StartDraggingPoint);
                    if (draggedRow == null) return;

                    // Set flag that indicates we're capturing mouse movements
                    //m_isDragging = true;
                    DraggedConversation = (Conversation)draggedRow.Item;
                }
            }

            /// <summary>
            /// Ends the dragging operation by get the target index and if valid calls the controller
            /// function that moves conversations
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            private void ConversationsGrid_PreviewMouseLeftButtonUp(Object sender, MouseButtonEventArgs e)
            {
                if (!m_isDragging)
                {
                    ResetDragDrop();
                    return;
                }

                //get the target item
                Conversation targetItem = (Conversation)ConversationsGrid.SelectedItem;
                var draggedTarget = TryFindFromPoint<DataGridRow>((UIElement)sender, e.GetPosition(ConversationsGrid));
                if (draggedTarget != null && draggedTarget.DataContext != targetItem)
                {
                    ResetDragDrop();
                    return;
                }

                if (targetItem != null && !ReferenceEquals(DraggedConversation, targetItem))
                {
                    int newIndex = App.Controller.SelectedDialogue.Conversations.IndexOf(targetItem);
                    App.Controller.MoveConversation(this.DraggedConversation, newIndex);
                }

                //reset
                ResetDragDrop();
            }

            /// <summary>
            /// Get called whenever the mouse is moved over the conversation data grid, if we are currently dragging and the mouse
            /// has moved enough show the popup and set the grid to read only
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            private void ConversationsGrid_MouseMove(Object sender, MouseEventArgs e)
            {
                if (e.LeftButton != MouseButtonState.Pressed || m_isDragging == false)
                {
                    ResetDragDrop();
                    return;
                }

                var draggedRow = TryFindFromPoint<DataGridRow>((UIElement)sender, StartDraggingPoint);
                if (draggedRow == null)
                {
                    ResetDragDrop();
                    return;
                }

                Point currentPosition = e.GetPosition(ConversationsGrid);
                Vector diff = StartDraggingPoint - currentPosition;
                if (Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance || Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance)
                {
                    m_isDragging = true;

                    //display the popup if it hasn't been opened yet
                    if (!this.DragPopup.IsOpen)
                    {
                        //switch to read-only mode
                        ConversationsGrid.IsReadOnly = true;

                        //make sure the popup is visible
                        DragPopup.IsOpen = true;
                    }
                    Size popupSize = new Size(this.DragPopup.ActualWidth, this.DragPopup.ActualHeight);
                    this.DragPopup.PlacementRectangle = new Rect(e.GetPosition(this), popupSize);

                    //make sure the row under the grid is being selected
                    Point position = e.GetPosition(ConversationsGrid);
                    var row = TryFindFromPoint<DataGridRow>(ConversationsGrid, position);

                    if (row != null && ConversationsGrid.SelectedIndex >= 0 && ConversationsGrid.SelectedIndex < ConversationsGrid.Items.Count)
                    {
                        var lastSelectedRow = ConversationsGrid.ItemContainerGenerator.ContainerFromIndex(ConversationsGrid.SelectedIndex) as DataGridRow;
                        if (lastSelectedRow != null)
                            lastSelectedRow.BorderThickness = new Thickness(0, 0, 0, 0);

                        ConversationsGrid.SelectedItem = row.Item;

                        row.BorderBrush = Brushes.Black;
                        int newIndex = ConversationsGrid.ItemContainerGenerator.IndexFromContainer(row);
                        int draggedIndex = ConversationsGrid.Items.IndexOf(DraggedConversation);
                        if (newIndex == draggedIndex)
                        {
                            row.BorderThickness = new Thickness(0, 3, 0, 0);
                        }
                        else if (newIndex == ConversationsGrid.Items.Count - 1)
                        {
                            row.BorderThickness = new Thickness(0, 0, 0, 3);
                        }
                        else if (newIndex == 0)
                        {
                            row.BorderThickness = new Thickness(0, 3, 0, 0);
                        }
                        else if (draggedIndex > newIndex)
                        {
                            row.BorderThickness = new Thickness(0, 3, 0, 0);
                        }
                        else
                        {
                            row.BorderThickness = new Thickness(0, 0, 0, 3);
                        }

                        if (currentPosition.Y <= 50 || currentPosition.Y >= (ConversationsGrid.ActualHeight - 25))
                        {
                            // Find the scrollview if there is one
                            ScrollViewer scrollViewer = FindScrollViewerFromUIElement(this.ConversationsGrid);
                            if (scrollViewer != null)
                            {
                                if (currentPosition.Y <= 50)
                                {
                                    // Scroll Up
                                    scrollViewer.ScrollToVerticalOffset(System.Math.Max(0, scrollViewer.VerticalOffset - 1));
                                }
                                else
                                {
                                    // Scroll Down
                                    scrollViewer.ScrollToVerticalOffset(System.Math.Min(scrollViewer.ScrollableHeight, scrollViewer.VerticalOffset + 1));
                                }
                            }
                        }
                    }
                }
            }

            #endregion // Private Function(s)

        #endregion // Drag & Drop Logic

        #region Public Function(s)

        /// <summary>
        /// Makes sure that the selected item is brought into view. This is so that any user selected conversation will definitely be
        /// visible
        /// </summary>
        public void BringSelectedConversationIntoView()
        {
            this.ConversationsGrid.ScrollIntoView(App.Controller.SelectedConversation);
        }

        #endregion // Public Function(s)

        #region Event Handlers

        /// <summary>
        /// Make sure that the controller has a reference to this control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConversationUserControl_Loaded(Object sender, RoutedEventArgs e)
        {
            if (App.Controller != null)
            {
                App.Controller.ConversationUserControl = this;
            }
        }

        /// <summary>
        /// Make sure that if this control is unloaded the controller gets the reference to it removed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConversationUserControl_Unloaded(Object sender, RoutedEventArgs e)
        {
            App.Controller.ConversationUserControl = null;
        }

        /// <summary>
        /// Makes sure that when a cell gets focus it's it's actual control that gets the focus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConversationsGridCell_GotFocus(Object sender, RoutedEventArgs e)
        {
            DataGridCell cell = sender as DataGridCell;

            DependencyObject control = VisualTreeHelper.GetChild(cell, 0);
            int childrenCount = VisualTreeHelper.GetChildrenCount(control);

            while (childrenCount > 0 && (control != null))
            {
                control = VisualTreeHelper.GetChild(control, 0);
                childrenCount = VisualTreeHelper.GetChildrenCount(control);

                if (control is TextBox || control is ComboBox || control is CheckBox)
                {
                    break;
                }
            }

            if (control != null)
            {
                if (control is TextBox)
                {
                    (control as TextBox).Focus();
                }
                else if (control is ComboBox)
                {
                    (control as ComboBox).Focus();
                }
                else if (control is CheckBox)
                {
                    (control as CheckBox).Focus();
                }
            }
        }

        /// <summary>
        /// The model name has changed so we need to see if there is an apprioate voice to set.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComboBox_SelectionChanged(Object sender, SelectionChangedEventArgs e)
        {
            // Removing voice/model check due to the fact that it is too restrictive
            //String newModelName = (sender as ComboBox).SelectedItem as String;

            //DependencyObject parent = VisualTreeHelper.GetParent((sender as ComboBox));
            //DataGridRow row = parent as DataGridRow;
            //while (parent != null && row == null)
            //{
            //    parent = VisualTreeHelper.GetParent(parent);
            //    row = parent as DataGridRow;
            //}

            //if (row != null)
            //{
            //    Conversation targetConversation = row.DataContext as Conversation;

            //    if (targetConversation != null)
            //    {
            //        // See if there are any other instances of the new model name
            //        foreach (Conversation conversation in App.Controller.SelectedDialogue.Conversations.Where(c => c != targetConversation))
            //        {
            //            if (conversation.ModelName == newModelName)
            //            {
            //                if (!String.IsNullOrEmpty(conversation.Voice))
            //                {
            //                    targetConversation.Voice = conversation.Voice;
            //                    break;
            //                }
            //            }
            //        }
            //    }
            //}
        }

        /// <summary>
        /// Since the voice has been changed for one model make sure that the voice changes on all the other occurences of this model.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextBox_TextChanged(Object sender, TextChangedEventArgs e)
        {
            // Removing voice/model check due to the fact that it is too restrictive
            //String newVoiceName = (sender as TextBox).Text as String;

            //DependencyObject parent = VisualTreeHelper.GetParent((sender as TextBox));
            //DataGridRow row = parent as DataGridRow;
            //while (parent != null && row == null)
            //{
            //    parent = VisualTreeHelper.GetParent(parent);
            //    row = parent as DataGridRow;
            //}

            //if (row != null)
            //{
            //    Conversation targetConversation = row.DataContext as Conversation;

            //    if (targetConversation != null)
            //    {
            //        // See if there are any other instances of the new model name
            //        foreach (Conversation conversation in App.Controller.SelectedDialogue.Conversations.Where(c => c != targetConversation))
            //        {
            //            if (conversation.ModelName == targetConversation.ModelName)
            //            {
            //                conversation.Voice = targetConversation.Voice;
            //            }
            //        }
            //    }
            //}
        }

        /// <summary>
        /// Used to support triple click event to select all
        /// </summary>
        private void TripleClickMouseLeftButtonDown(Object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 3)
            {
                (sender as TextBox).SelectAll();
            }
        }

        private void NewCanExecute(Object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void NewExecuted(Object sender, ExecutedRoutedEventArgs e)
        {
            AddConversation_Click(null, new RoutedEventArgs());
            this.ConversationsGrid.Focus();
            e.Handled = false;
        }

        private void CopyCanExecute(Object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CopyExecuted(Object sender, ExecutedRoutedEventArgs e)
        {
            App.Controller.InternalClipBoard = new DataObject("RSG.Model.Dialogue.Conversation", this.ConversationsGrid.SelectedItem);
            this.ConversationsGrid.Focus();
            e.Handled = false;
        }

        private void PasteCanExecute(Object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void PasteExecuted(Object sender, ExecutedRoutedEventArgs e)
        {
            IDataObject dataObject = App.Controller.InternalClipBoard;
            if (dataObject == null)
            {
                return;
            }

            String[] formats = dataObject.GetFormats();
            if (dataObject.GetDataPresent("RSG.Model.Dialogue.Conversation", true))
            {
                RSG.Model.Dialogue.Conversation copiedConversation = dataObject.GetData(typeof(RSG.Model.Dialogue.Conversation)) as RSG.Model.Dialogue.Conversation;
                if (copiedConversation != null)
                {
                    AddConversation_Click(null, new RoutedEventArgs());
                    RSG.Model.Dialogue.Conversation conversation = App.Controller.SelectedConversation;

                    int index = App.Controller.Missions.IndexOf(App.Controller.SelectedDialogue);
                    App.Controller.UndoManagers[index].StartCompositeUndo();

                    conversation.PlaceHolder = copiedConversation.PlaceHolder;
                    conversation.Description = copiedConversation.Description;
                    conversation.Random = copiedConversation.Random;
                    conversation.Category = copiedConversation.Category;
                    conversation.Role = copiedConversation.Role;
                    conversation.ModelName = copiedConversation.ModelName;
                    conversation.Voice = copiedConversation.Voice;
                    conversation.UseVoice = copiedConversation.UseVoice;
                    conversation.Root = copiedConversation.Root;
                    conversation.Lines.Clear();
                    foreach (RSG.Model.Dialogue.Line line in copiedConversation.Lines)
                    {
                        conversation.Lines.Add(new RSG.Model.Dialogue.Line(conversation, line));
                    }

                    App.Controller.UndoManagers[index].EndCompositeUndo();

                    this.ConversationsGrid.Focus();
                    e.Handled = false;
                }
            }
        }

        private void CutCanExecute(Object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CutExecuted(Object sender, ExecutedRoutedEventArgs e)
        {
            App.Controller.InternalClipBoard = new DataObject("RSG.Model.Dialogue.Conversation", this.ConversationsGrid.SelectedItem);
            DeleteConversation_Click(null, new RoutedEventArgs());
            this.ConversationsGrid.Focus();
            e.Handled = false;
        }

        private void DeleteCanExecute(Object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void DeleteExecuted(Object sender, ExecutedRoutedEventArgs e)
        {
            DeleteConversation_Click(null, new RoutedEventArgs());
            this.ConversationsGrid.Focus();
            e.Handled = false;
        }

        #endregion // Event Handlers

        private object lastSelection = null;
        private void ConversationsGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!object.ReferenceEquals(sender, e.OriginalSource))
            {
                return;
            }

            if (object.ReferenceEquals(this.ConversationsGrid.SelectedItem, lastSelection))
            {
                return;
            }

            if (this.ConversationsGrid.SelectedItem != null)
            {
                this.ConversationsGrid.ScrollIntoView(this.ConversationsGrid.SelectedItem);
            }

            this.lastSelection = this.ConversationsGrid.SelectedItem;
        }

        private void ConversationsGrid_PreviewKeyDown_1(object sender, KeyEventArgs e)
        {
            DataGrid grid = sender as DataGrid;
            if (grid == null || e.Key != Key.Tab)
            {
                return;
            }

            if ((Keyboard.Modifiers & ModifierKeys.Shift) == 0)
            {
                return;
            }

            DependencyObject focused = Keyboard.FocusedElement as DependencyObject;
            DependencyObject parent = VisualTreeHelper.GetParent(focused);
            DataGridCell cell = focused as DataGridCell;
            while (parent != null && cell == null)
            {
                cell = parent as DataGridCell;
                parent = VisualTreeHelper.GetParent(parent);
            }

            if (cell == null)
            {
                return;
            }

            if (base.IsKeyboardFocusWithin && cell.MoveFocus(new TraversalRequest(FocusNavigationDirection.Previous)))
            {
                e.Handled = true;
                return;
            }
        }
    } // Conversations

    public class RootRule : ValidationRule
    {
        public override ValidationResult Validate(Object value, CultureInfo cultureInfo)
        {
            if ((value as string).Length > 15)
            {
                return new ValidationResult(false, "The root name cannot be more than 15 characters in length.");
            }

            for (int i = 0; i < (value as string).Length; i++)
            {
                if (char.IsWhiteSpace((value as string)[i]))
                {
                    return new ValidationResult(false, "The root name cannot contain white spaces.");
                }
            }

            if (App.Controller.SelectedDialogue.DetermineIfRootExists(value as String) && !String.IsNullOrEmpty(value as String) && !App.Controller.SelectedDialogue.RandomEventMission)
            {
                return new ValidationResult(false, "The root name has to be unique.");
            }
            else
            {
                return ValidationResult.ValidResult;
            }
        }
    }

    public class ErrorsToMessageConverter : IValueConverter
    {
        public object Convert(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            var sb = new StringBuilder();
            var errors = value as System.Collections.ObjectModel.ReadOnlyCollection<ValidationError>;
            if (errors != null)
            {
                foreach (var e in errors.Where(e => e.ErrorContent != null))
                { 
                    sb.AppendLine(e.ErrorContent.ToString());
                }
            }

            return sb.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        { 
            throw new NotImplementedException();
        }
    }


} // DialogueStar.View
