﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RSG.Model.Dialogue.UndoInterface;
using RSG.Model.Dialogue.Search;

namespace DialogueStar
{
    public enum SearchRanges
    {
        None = 0,
        Current = 1,
        Opened = 2,
        All = 3,
    }

    /// <summary>
    /// A searh attribute is a single search field that the user can select to search for. This class is used by the UI
    /// to give the user a nicer visual representation of the search their wish to carry out.
    /// </summary>
    public class SearchAttribute
    {
        #region Static Members

        /// <summary>
        /// A static dictionary that lists the search options for the different types of searches. This is used in the UI to give the user
        /// visual options to select.
        /// </summary>
        public static readonly Dictionary<SearchType, List<String>> SearchOptions = new Dictionary<SearchType, List<String>>
        {
            {SearchType.Search_None_Type, new List<String>() {}},
            {SearchType.Search_Boolean_Type, new List<String>() {"Is equal to", "Is not equal to"}},
            {SearchType.Search_String_Type, new List<String>() {"Contains", "Starts with", "Ends with", "Is equal to", "Is not equal to"}},
            {SearchType.Search_Enum_Type, new List<String>() {"Is equal to", "Is not equal to"}},
            {SearchType.Search_Numeric_Type, new List<String>() { "Is equal to", "Is not equal to", "Is greater than", "Is less than" }}
        };

        #endregion // Static Members

        #region Propertie(s)

        /// <summary>
        /// The name of the field that the user has selected to search for.
        /// </summary>
        public String AttributeName
        {
            get { return m_atrributeName; }
            set { m_atrributeName = value; }
        }
        private String m_atrributeName;

        /// <summary>
        /// The options that this attribute has on it, these are gotten by the above static dictionary
        /// </summary>
        public ObservableCollection<String> AttributeSearchOptions
        {
            get { return m_attributeSearchOptions; }
            set { m_attributeSearchOptions = value; }
        }
        private ObservableCollection<String> m_attributeSearchOptions;

        /// <summary>
        /// The search values if any this attribute can have, usually this is only used for Boolean values or Enum values.
        /// </summary>
        public Object SearchValues
        {
            get { return m_attributeSearchValues; }
            set { m_attributeSearchValues = value; }
        }
        private Object m_attributeSearchValues;

        /// <summary>
        /// The type of the search based on the property that the user is searching
        /// </summary>
        public SearchType SearchType
        {
            get { return m_searchType; }
            set { m_searchType = value; }
        }
        private SearchType m_searchType;

        #endregion // Propertie(s)

        #region Constructor(s)

        /// <summary>
        /// Default constructor
        /// </summary>
        public SearchAttribute()
        {
        }

        #endregion // Constructor(s)

        #region Static Public Function(s)

        /// <summary>
        /// Fills in the properties based on the given type
        /// </summary>
        /// <param name="type">The type to create the search attributes for</param>
        /// <returns>A list of search attributes one for each property in the given type that can be searched for</returns>
        public static List<SearchAttribute> CreateFromType(Type type)
        {
            List<SearchAttribute> result = new List<SearchAttribute>();

            PropertyInfo[] propertyInfos = type.GetProperties();

            foreach (PropertyInfo property in propertyInfos)
            {
                if (property.GetCustomAttributes(typeof(NonSearchable), true).Length != 0)
                    continue;

                String attributeName = String.Empty;
                if (property.GetCustomAttributes(typeof(DisplayNameAttribute), true).Length != 0)
                {
                    DisplayNameAttribute displayName = (DisplayNameAttribute)property.GetCustomAttributes(typeof(DisplayNameAttribute), true)[0];
                    if (displayName.DisplayName == "")
                    {
                        attributeName = property.Name;
                    }
                    else
                    {
                        attributeName = displayName.DisplayName;
                    }
                }
                else
                {
                    attributeName = property.Name;
                }

                if (property.PropertyType.Name == "Boolean")
                {
                    SearchAttribute newAttribute = new SearchAttribute();
                    newAttribute.AttributeName = attributeName;
                    newAttribute.SearchType = SearchType.Search_Boolean_Type;
                    newAttribute.AttributeSearchOptions = new ObservableCollection<String>(SearchAttribute.SearchOptions[newAttribute.SearchType]);
                    newAttribute.SearchValues = new ObservableCollection<String>() { "True", "False" };
                    result.Add(newAttribute);
                    continue;
                }
                else if (property.PropertyType.Name == "String")
                {
                    SearchAttribute newAttribute = new SearchAttribute();
                    newAttribute.AttributeName = attributeName;
                    newAttribute.SearchType = SearchType.Search_String_Type;
                    newAttribute.AttributeSearchOptions = new ObservableCollection<String>(SearchAttribute.SearchOptions[newAttribute.SearchType]);
                    newAttribute.SearchValues = null;
                    result.Add(newAttribute);
                    continue;
                }
                else if (property.PropertyType.BaseType != null && property.PropertyType.BaseType.Name == "Enum")
                {
                    SearchAttribute newAttribute = new SearchAttribute();
                    newAttribute.AttributeName = attributeName;
                    newAttribute.SearchType = SearchType.Search_Enum_Type;
                    newAttribute.AttributeSearchOptions = new ObservableCollection<String>(SearchAttribute.SearchOptions[newAttribute.SearchType]);
                    newAttribute.SearchValues = new ObservableCollection<String>(property.PropertyType.GetEnumNames());
                    result.Add(newAttribute);
                    continue;
                }
                else if (property.PropertyType.BaseType != null && property.PropertyType.BaseType.Name == "ValueType")
                {
                    SearchAttribute newAttribute = new SearchAttribute();
                    newAttribute.AttributeName = attributeName;
                    newAttribute.SearchType = SearchType.Search_Numeric_Type;
                    newAttribute.AttributeSearchOptions = new ObservableCollection<String>(SearchAttribute.SearchOptions[newAttribute.SearchType]);
                    newAttribute.SearchValues = null;
                    result.Add(newAttribute);
                    continue;
                }

                // Since this is a recurse function make sure that any collections are properties that have the ISearchable interface
                // get their properties also created into search attributes
                if (typeof(System.Collections.ICollection).IsAssignableFrom(property.PropertyType) ||
                    typeof(ICollection<>).IsAssignableFrom(property.PropertyType))
                {
                    Type[] genericTypes = property.PropertyType.GetGenericArguments();
                    foreach (Type genericType in genericTypes)
                    {
                        if (genericType.GetInterface(typeof(ISearchable).FullName) != null)
                        {
                            List<SearchAttribute> childrenList = CreateFromType(genericType);
                            result.AddRange(childrenList);
                        }
                    }
                }
            }

            return result;
        }

        #endregion // Static Public Function(s)

    }
}
