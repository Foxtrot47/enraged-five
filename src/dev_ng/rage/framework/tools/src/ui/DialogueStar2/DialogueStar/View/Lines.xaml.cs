﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Model.Dialogue;

namespace DialogueStar.View
{
    /// <summary>
    /// Interaction logic for Lines.xaml
    /// </summary>
    public partial class Lines : UserControl
    {
        #region Constructor(s)

        public Lines()
        {
            InitializeComponent();
            App.Controller.LineUserControl = this;

            this.FilterBox.SelectionChanged += FilterBox_SelectionChanged;
        }

        void FilterBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ICollectionView view = CollectionViewSource.GetDefaultView(this.LineGrid.ItemsSource);
            if (view != null)
            {
                if (view.Filter == null)
                {
                    view.Filter = FilterPredicate;
                }
                else
                {
                    view.Refresh();
                }
            }
        }

        private bool FilterPredicate(object item)
        {
            RSG.Model.Dialogue.Line line = item as RSG.Model.Dialogue.Line;
            ComboBoxItem selected = this.FilterBox.SelectedValue as ComboBoxItem;
            if (selected != null && (string)selected.Content == "All")
            {
                return true;
            }

            if (line == null) 
            {
                return true;
            }

            if (this.FilterBox.SelectedIndex == 1)
            {
                return line.Recorded;
            }

            if (this.FilterBox.SelectedIndex == 2)
            {
                return !line.Recorded;
            }

            string filter = this.FilterBox.SelectedItem as string;
            if (filter == null)
            {
                return true;
            }

            return line.CharacterName == filter;
        }
        #endregion // Constructor(s)

        #region Button Logic

        /// <summary>
        /// Adds a new line underneath the currently selected one or at the end if none are selected.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddLine_Click(Object sender, RoutedEventArgs e)
        {
            int index = this.LineGrid.SelectedIndex + 1;
            if (App.Controller.SelectedConversation.Random)
            {
                index = this.LineGrid.Items.Count;
            }

            App.Controller.AddNewLine(App.Controller.SelectedConversation, index);

            // Bring the new item into view (use the selected line on the controller as this is changed in the controller automatically
            this.LineGrid.ScrollIntoView(App.Controller.SelectedLine);
        }

        /// <summary>
        /// Inserts a new line above the currently selected one or at the end if none are selected.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InsertLine_Click(Object sender, RoutedEventArgs e)
        {
            int index = this.LineGrid.SelectedIndex;
            if (App.Controller.SelectedConversation.Random)
            {
                index = this.LineGrid.Items.Count - 1;
            }

            App.Controller.AddNewLine(App.Controller.SelectedConversation, index);

            // Bring the new item into view (use the selected line on the controller as this is changed in the controller automatically
            this.LineGrid.ScrollIntoView(App.Controller.SelectedLine);
        }

        /// <summary>
        /// Removes the currently selected line
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteLine_Click(Object sender, RoutedEventArgs e)
        {
            // Cannot remove the last line
            if (this.LineGrid.Items.Count <= 1)
            {
                return;
            }

            App.Controller.RemoveLine(App.Controller.SelectedConversation, this.LineGrid.SelectedIndex);
        }

        /// <summary>
        /// Autogenerates all the filenames for the conversation that this user control
        /// is bound to. (i.e the selected conversation)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MagicFilenames_Click(Object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(App.Controller.SelectedDialogue.MissionId))
            {
                App.Controller.AutoGenerateFilenamesForConversation();
            }
            else
            {
                MessageBox.Show("Unable to generate filenames at this time due to the fact that the Mission ID field is empty", "Filename Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// This function sets the selected line as to having a manual filename and then selects the 
        /// text box that is located in the filename column of the selected row.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PencilLine_Click(Object sender, RoutedEventArgs e)
        {
            if (App.Controller.SelectedLine.SentToBeRecorded == true)
            {
                MessageBox.Show("Unable to set this line to a manual filename due to the fact that it has already been sent to NY for recording", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            App.Controller.SelectedLine.ManualFilename = true;

            DataGridRow row = (DataGridRow)LineGrid.ItemContainerGenerator.ContainerFromItem(App.Controller.SelectedLine);
            DataGridCellsPresenter presenter = GetVisualChild<DataGridCellsPresenter>(row);

            DataGridCell cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(5);
            TextBox textbox = GetVisualChild<TextBox>(cell);

            textbox.Focus();
        }
        
        /// <summary>
        /// Unlocks the current conversation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Unlock_Click(Object sender, RoutedEventArgs e)
        {
            App.Controller.UnlockConversation(App.Controller.SelectedConversation);
        }

        #endregion // Button Logic

        #region DataGrid Callbacks

        /// <summary>
        /// This function gets called everytime the data context for the grid changes (i.e everytime the user selects a 
        /// different conversation) this function passes down the data context to the columns since columns are not a visual
        /// child for the data gris they don't automatically inhert the data context and you are unable to find
        /// the data context using a relative source.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LineGrid_DataContextChanged(Object sender, DependencyPropertyChangedEventArgs e)
        {
            DataGrid grid = sender as DataGrid;
            if (grid != null)
            {
                foreach (DataGridColumn col in grid.Columns)
                {
                    col.SetValue(FrameworkElement.DataContextProperty, e.NewValue);
                }
            }
        }

        /// <summary>
        /// Used to select the line the user has just clicked on.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HeaderBorder_MouseLeftButtonDown(Object sender, MouseButtonEventArgs e)
        {
            App.Controller.SelectedLine = (RSG.Model.Dialogue.Line)(((Border)sender).DataContext);
        }

        #endregion // DataGrid Callbacks
        
        #region Visual Helper Function(s)

        /// <summary>
        /// Find the first visual child of the given parent that is of the type given
        /// </summary>
        /// <typeparam name="T">The type to find in the visual tree</typeparam>
        /// <param name="parent">The visual item to start searching</param>
        /// <returns>The object if it is found and null if it's not found</returns>
        private static T GetVisualChild<T>(Visual parent) where T : Visual
        {
            T child = default(T);
            int numVisuals = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < numVisuals; i++)
            {
                Visual v = (Visual)VisualTreeHelper.GetChild(parent, i);
                child = v as T;
                if (child == null)
                {
                    child = GetVisualChild<T>(v);
                }
                if (child != null)
                {
                    break;
                }
            }
            return child;
        }

        #endregion // Visual Helper Function(s)

        #region Drag & Drop Logic

            #region Properties

            /// <summary>
            /// DraggedItem Dependency Property
            /// </summary>
            public static readonly DependencyProperty DraggedLineProperty = DependencyProperty.Register("DraggedLine", typeof(RSG.Model.Dialogue.Line), typeof(Lines));
            public RSG.Model.Dialogue.Line DraggedLine
            {
                get { return (RSG.Model.Dialogue.Line)GetValue(DraggedLineProperty); }
                set { SetValue(DraggedLineProperty, value); }
            }

            /// <summary>
            /// A boolean value representing whether we are currently dragging the data grid row
            /// </summary>
            private Boolean m_isDragging = false;

            /// <summary>
            /// The starting mouse position when the dragging began
            /// </summary>
            Point StartDraggingPoint;

            #endregion // Properties

            #region Helper UI Function(s)

            /// <summary>
            /// This method is an alternative to WPF's
            /// <see cref="VisualTreeHelper.GetParent"/> method, which also
            /// supports content elements. Keep in mind that for content element,
            /// this method falls back to the logical tree of the element!
            /// </summary>
            /// <param name="child">The item to be processed.</param>
            /// <returns>The submitted item's parent, if available. Otherwise
            /// null.</returns>
            public DependencyObject GetParentObject(DependencyObject child)
            {
                if (child == null) return null;

                //handle content elements separately
                ContentElement contentElement = child as ContentElement;

                if (contentElement != null)
                {
                    DependencyObject parent = ContentOperations.GetParent(contentElement);
                    if (parent != null) return parent;

                    FrameworkContentElement fce = contentElement as FrameworkContentElement;
                    return fce != null ? fce.Parent : null;
                }

                //also try searching for parent in framework elements (such as DockPanel, etc)
                FrameworkElement frameworkElement = child as FrameworkElement;
                if (frameworkElement != null)
                {
                    DependencyObject parent = frameworkElement.Parent;
                    if (parent != null) return parent;
                }

                //if it's not a ContentElement/FrameworkElement, rely on VisualTreeHelper
                return VisualTreeHelper.GetParent(child);
            }

            /// <summary>
            /// Finds a parent of a given item on the visual tree.
            /// </summary>
            /// <typeparam name="T">The type of the queried item.</typeparam>
            /// <param name="child">A direct or indirect child of the
            /// queried item.</param>
            /// <returns>The first parent item that matches the submitted
            /// type parameter. If not matching item can be found, a null
            /// reference is being returned.</returns>
            public T TryFindParent<T>(DependencyObject child) where T : DependencyObject
            {
                //get parent item
                DependencyObject parentObject = GetParentObject(child);

                //we've reached the end of the tree
                if (parentObject == null)
                    return null;

                //check if the parent matches the type we're looking for
                T parent = parentObject as T;
                if (parent != null)
                {
                    return parent;
                }
                else
                {
                    //use recursion to proceed with next level
                    return TryFindParent<T>(parentObject);
                }
            }

            /// <summary>
            /// Tries to locate a given item within the visual tree,
            /// starting with the dependency object at a given position. 
            /// </summary>
            /// <typeparam name="T">The type of the element to be found
            /// on the visual tree of the element at the given location.</typeparam>
            /// <param name="reference">The main element which is used to perform
            /// hit testing.</param>
            /// <param name="point">The position to be evaluated on the origin.</param>
            public T TryFindFromPoint<T>(UIElement reference, Point point) where T : DependencyObject
            {
                DependencyObject element = reference.InputHitTest(point) as DependencyObject;

                if (element == null)
                {
                    return TryFindParent<T>(reference);
                }
                else if (element is T)
                {
                    return (T)element;
                }
                else
                {
                    return TryFindParent<T>(element);
                }
            }

            public ScrollViewer FindScrollViewerFromUIElement(DependencyObject root)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(root); i++)
                {
                    var child = VisualTreeHelper.GetChild(root, i);
                    if (child is ScrollViewer)
                        return child as ScrollViewer;

                    ScrollViewer result = FindScrollViewerFromUIElement(child);
                    if (result != null)
                        return result;
                }
                return null;
            }

            #endregion // Helper UI Function(s)

            #region Private Function(s)

            /// <summary>
            /// Closes the popup and resets the
            /// grid to read-enabled mode.
            /// </summary>
            private void ResetDragDrop()
            {
                m_isDragging = false;
                if (this.DragPopup != null)
                    this.DragPopup.IsOpen = false;
                LineGrid.IsReadOnly = false;
                for (int i = 0; i < this.LineGrid.Items.Count; i++)
                {
                    DataGridRow row = LineGrid.ItemContainerGenerator.ContainerFromIndex(i) as DataGridRow;
                    if (row != null)
                        row.BorderThickness = new Thickness(0, 0, 0, 0);
                }
            }

            /// <summary>
            /// Starts the dragging operation if the left mouse button is clicked over a row.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            private void LineGrid_PreviewMouseLeftButtonDown(Object sender, MouseButtonEventArgs e)
            {
                if (this.LineGrid != null)
                {
                    // Find the clicked row
                    StartDraggingPoint = e.GetPosition(LineGrid);

                    m_isDragging = true;

                    var draggedRow = TryFindFromPoint<DataGridRow>((UIElement)sender, StartDraggingPoint);
                    if (draggedRow == null) return;

                    // Set flag that indicates we're capturing mouse movements
                    //m_isDragging = true;
                    DraggedLine = (RSG.Model.Dialogue.Line)draggedRow.Item;
                }
            }

            /// <summary>
            /// Ends the dragging operation by get the target index and if valid calls the controller
            /// function that moves conversations
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            private void LineGrid_PreviewMouseLeftButtonUp(Object sender, MouseButtonEventArgs e)
            {
                if (!m_isDragging)
                {
                    ResetDragDrop();
                    return;
                }

                //get the target item
                RSG.Model.Dialogue.Line targetItem = (RSG.Model.Dialogue.Line)LineGrid.SelectedItem;
                var draggedTarget = TryFindFromPoint<DataGridRow>((UIElement)sender, e.GetPosition(LineGrid));
                if (draggedTarget != null && draggedTarget.DataContext != targetItem)
                {
                    ResetDragDrop();
                    return;
                }

                if (targetItem != null && !ReferenceEquals(DraggedLine, targetItem))
                {
                    int newIndex = App.Controller.SelectedConversation.Lines.IndexOf(targetItem);
                    App.Controller.MoveLine(this.DraggedLine, newIndex);
                }

                //reset
                ResetDragDrop();
            }

            /// <summary>
            /// Get called whenever the mouse is moved over the conversation data grid, if we are currently dragging and the mouse
            /// has moved enough show the popup and set the grid to read only
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            private void LineGrid_MouseMove(Object sender, MouseEventArgs e)
            {
                if (e.LeftButton != MouseButtonState.Pressed || m_isDragging == false)
                {
                    ResetDragDrop();
                    return;
                }

                var draggedRow = TryFindFromPoint<DataGridRow>((UIElement)sender, StartDraggingPoint);
                if (draggedRow == null)
                {
                    ResetDragDrop();
                    return;
                }

                Point currentPosition = e.GetPosition(LineGrid);
                Vector diff = StartDraggingPoint - currentPosition;
                if (Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance || Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance)
                {
                    m_isDragging = true;

                    //display the popup if it hasn't been opened yet
                    if (!this.DragPopup.IsOpen)
                    {
                        //switch to read-only mode
                        LineGrid.IsReadOnly = true;

                        //make sure the popup is visible
                        DragPopup.IsOpen = true;
                    }
                    Size popupSize = new Size(this.DragPopup.ActualWidth, this.DragPopup.ActualHeight);
                    this.DragPopup.PlacementRectangle = new Rect(e.GetPosition(this), popupSize);

                    //make sure the row under the grid is being selected
                    Point position = e.GetPosition(LineGrid);
                    var row = TryFindFromPoint<DataGridRow>(LineGrid, position);

                    if (row != null && LineGrid.SelectedIndex >= 0 && LineGrid.SelectedIndex < LineGrid.Items.Count)
                    {
                        var lastSelectedRow = LineGrid.ItemContainerGenerator.ContainerFromIndex(LineGrid.SelectedIndex) as DataGridRow;
                        if (lastSelectedRow != null)
                            lastSelectedRow.BorderThickness = new Thickness(0, 0, 0, 0);
                        
                        LineGrid.SelectedItem = row.Item;

                        row.BorderBrush = Brushes.Black;
                        int newIndex = LineGrid.ItemContainerGenerator.IndexFromContainer(row);
                        int draggedIndex = LineGrid.Items.IndexOf(DraggedLine);
                        if (newIndex == draggedIndex)
                        {
                            row.BorderThickness = new Thickness(0, 3, 0, 0);
                        }
                        else if (newIndex == LineGrid.Items.Count - 1)
                        {
                            row.BorderThickness = new Thickness(0, 0, 0, 3);
                        }
                        else if (newIndex == 0)
                        {
                            row.BorderThickness = new Thickness(0, 3, 0, 0);
                        }
                        else if (draggedIndex > newIndex)
                        {
                            row.BorderThickness = new Thickness(0, 3, 0, 0);
                        }
                        else
                        {
                            row.BorderThickness = new Thickness(0, 0, 0, 3);
                        }

                        if (currentPosition.Y <= 50 || currentPosition.Y >= (LineGrid.ActualHeight - 25))
                        {
                            // Find the scrollview if there is one
                            ScrollViewer scrollViewer = FindScrollViewerFromUIElement(this.LineGrid);
                            if (scrollViewer != null)
                            {
                                if (currentPosition.Y <= 50)
                                {
                                    // Scroll Up
                                    scrollViewer.ScrollToVerticalOffset(System.Math.Max(0, scrollViewer.VerticalOffset - 1));
                                }
                                else
                                {
                                    // Scroll Down
                                    scrollViewer.ScrollToVerticalOffset(System.Math.Min(scrollViewer.ScrollableHeight, scrollViewer.VerticalOffset + 1));
                                }
                            }
                        }
                    }
                }
            }

            #endregion // Private Function(s)

        #endregion // Drag & Drop Logic

        #region Public Function(s)

        /// <summary>
        /// Makes sure that the selected item is brought into view. This is so that any user selected line will definitely be
        /// visible
        /// </summary>
        public void BringSelectedLineIntoView()
        {
            this.LineGrid.ScrollIntoView(App.Controller.SelectedLine);
        }

        #endregion // Public Function(s)

        #region Event Handlers

        /// <summary>
        /// Make sure that the controller has a reference to this control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LineUserControl_Loaded(Object sender, RoutedEventArgs e)
        {
            App.Controller.LineUserControl = this;
        }

        /// <summary>
        /// Make sure that if this control is unloaded the controller gets the reference to it removed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LineUserControl_UnLoaded(Object sender, RoutedEventArgs e)
        {
            App.Controller.LineUserControl = null;
        }

        /// <summary>
        /// Makes sure that when a cell gets focus it's it's actual control that gets the focus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LinesGridCell_GotFocus(Object sender, RoutedEventArgs e)
        {
            DataGridCell cell = sender as DataGridCell;

            DependencyObject control = VisualTreeHelper.GetChild(cell, 0);
            int childrenCount = VisualTreeHelper.GetChildrenCount(control);

            while (childrenCount > 0 && (control != null))
            {
                control = VisualTreeHelper.GetChild(control, 0);
                childrenCount = VisualTreeHelper.GetChildrenCount(control);

                if (control is TextBox || control is ComboBox || control is CheckBox)
                {
                    break;
                }
            }

            if (control != null)
            {
                if (control is TextBox)
                {
                    (control as TextBox).Focus();
                }
                else if (control is ComboBox)
                {
                    (control as ComboBox).Focus();
                }
                else if (control is CheckBox)
                {
                    (control as CheckBox).Focus();
                }
            }
        }

        /// <summary>
        /// Used to support triple click event to select all
        /// </summary>
        private void TripleClickMouseLeftButtonDown(Object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 3)
            {
                (sender as TextBox).SelectAll();
            }
        }

        private void NewCanExecute(Object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void NewExecuted(Object sender, ExecutedRoutedEventArgs e)
        {
            AddLine_Click(null, new RoutedEventArgs());
            this.LineGrid.Focus();
            e.Handled = false;
        }

        private void CopyCanExecute(Object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CopyExecuted(Object sender, ExecutedRoutedEventArgs e)
        {
            App.Controller.InternalClipBoard = new DataObject("RSG.Model.Dialogue.Line", this.LineGrid.SelectedItem);
            this.LineGrid.Focus();
            e.Handled = false;
        }

        private void PasteCanExecute(Object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void PasteExecuted(Object sender, ExecutedRoutedEventArgs e)
        {
            IDataObject dataObject = App.Controller.InternalClipBoard;
            if (dataObject == null)
                return;

            String[] formats = dataObject.GetFormats();
            if (dataObject.GetDataPresent("RSG.Model.Dialogue.Line", true))
            {
                RSG.Model.Dialogue.Line copiedLine = dataObject.GetData(typeof(RSG.Model.Dialogue.Line)) as RSG.Model.Dialogue.Line;
                if (copiedLine != null)
                {
                    AddLine_Click(null, new RoutedEventArgs());
                    RSG.Model.Dialogue.Line newLine = App.Controller.SelectedLine;

                    int index = App.Controller.Missions.IndexOf(App.Controller.SelectedDialogue);
                    App.Controller.UndoManagers[index].StartCompositeUndo();

                    newLine.Character = copiedLine.Character;
                    newLine.LineDialogue = copiedLine.LineDialogue;
                    newLine.Volume = copiedLine.Volume;
                    newLine.Speaker = copiedLine.Speaker;
                    newLine.Listener = copiedLine.Listener;
                    newLine.AudioType = copiedLine.AudioType;
                    newLine.Special = copiedLine.Special;

                    App.Controller.UndoManagers[index].EndCompositeUndo();

                    this.LineGrid.Focus();
                    e.Handled = false;
                }
            }
        }

        private void CutCanExecute(Object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CutExecuted(Object sender, ExecutedRoutedEventArgs e)
        {
            App.Controller.InternalClipBoard = new DataObject("RSG.Model.Dialogue.Line", this.LineGrid.SelectedItem);
            DeleteLine_Click(null, new RoutedEventArgs());
            this.LineGrid.Focus();
            e.Handled = false;
        }

        private void DeleteCanExecute(Object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void DeleteExecuted(Object sender, ExecutedRoutedEventArgs e)
        {
            DeleteLine_Click(null, new RoutedEventArgs());
            this.LineGrid.Focus();
            e.Handled = false;
        }

        #endregion // Event Handlers       

        private void LineGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!object.ReferenceEquals(sender, e.OriginalSource))
            {
                return;
            }

            if (this.LineGrid.SelectedItem != null)
            {
                this.LineGrid.ScrollIntoView(this.LineGrid.SelectedItem);
            }
        }

        private void LineGrid_KeyDown_1(object sender, KeyEventArgs e)
        {
            DataGrid grid = sender as DataGrid;
            if (grid == null || e.Key != Key.Tab)
            {
                return;
            }

            if ((Keyboard.Modifiers & ModifierKeys.Shift) == 0)
            {
                return;
            }

            DependencyObject focused = Keyboard.FocusedElement as DependencyObject;
            DependencyObject parent = VisualTreeHelper.GetParent(focused);
            DataGridCell cell = focused as DataGridCell;
            while (parent != null && cell == null)
            {
                cell = parent as DataGridCell;
                parent = VisualTreeHelper.GetParent(parent);
            }

            if (cell == null)
            {
                return;
            }

            if (base.IsKeyboardFocusWithin && cell.MoveFocus(new TraversalRequest(FocusNavigationDirection.Previous)))
            {
                e.Handled = true;
                return;
            }
        }

        private void MassChange_Click(object sender, RoutedEventArgs e)
        {
            Conversation conversation = this.DataContext as Conversation;
            if (conversation == null)
            {
                return;
            }

            foreach (RSG.Model.Dialogue.Line line in conversation.Lines)
            {
                if (this.FilterPredicate(line))
                {
                    switch (this.MassChangeComponent.SelectedIndex)
                    {
                        case 0:
                            line.Special = this.MassChangeValue.SelectedValue as string;
                            break;
                        case 1:
                            line.AudioType = this.MassChangeValue.SelectedValue as string;
                            break;
                        case 2:
                            line.Audibility = this.MassChangeValue.SelectedIndex;
                            break;
                        case 3:
                            line.DucksRadio = (bool)this.MassChangeValue.SelectedValue;
                            break;
                    }
                }
            }
        }
    } // Lines

    public class ListenerTextBox : TextBox
    {
        private String PreviousText = "0";
        private Boolean UpdatingText = false;

        protected override void OnTextChanged(TextChangedEventArgs e)
        {
            if (UpdatingText == false)
            {
                e.Handled = true;
                String result = PreviousText;
                String inputString = this.Text;
                if (inputString.Length == 1)
                {
                    int intValue = (int)inputString[0];
                    if (!(intValue >= 48 && intValue <= 57) && !(intValue >= 65 && intValue <= 90) && !(intValue >= 97 && intValue <= 122))
                    {
                        MessageBox.Show(String.Format("Invalid character entered {0}, value needs to be in the range [0-9, A-Z]", inputString[0]), "Invalid Input", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    else
                    {
                        result = inputString;
                    }
                }
                else
                {
                    result = inputString;
                }
                UpdatingText = true;
                this.Text = result;
                UpdatingText = false;

                base.OnTextChanged(e);

                this.PreviousText = this.Text;
            }
        }
    }

    public class SpeakerTextBox : TextBox
    {
        private String PreviousText = "0";
        private Boolean UpdatingText = false;

        protected override void OnTextChanged(TextChangedEventArgs e)
        {
            if (UpdatingText == false)
            {
                e.Handled = true;
                String result = PreviousText;
                String inputString = this.Text;
                if (inputString.Length == 1)
                {
                    int intValue = (int)inputString[0];
                    if (!(intValue >= 48 && intValue <= 56) && !(intValue >= 65 && intValue <= 90) && !(intValue >= 97 && intValue <= 122))
                    {
                        MessageBox.Show(String.Format("Invalid character entered {0}, value needs to be in the range [0-8, A-Z]", inputString[0]), "Invalid Input", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    else
                    {
                        result = inputString;
                    }
                }
                else
                {
                    result = inputString;
                }
                UpdatingText = true;
                this.Text = result;
                UpdatingText = false;

                base.OnTextChanged(e);

                this.PreviousText = this.Text;
            }
        }
    }

    public class NotLogicalConverter : IValueConverter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool flag = (bool)value;
            return !flag;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

} // DialogueStar.View
