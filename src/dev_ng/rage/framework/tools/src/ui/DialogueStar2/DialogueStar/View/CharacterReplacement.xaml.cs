﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DialogueStar.View
{
    /// <summary>
    /// Interaction logic for CharacterReplacement.xaml
    /// </summary>
    public partial class CharacterReplacement : Window
    {
        public Dictionary<Guid, string> AllCharacters
        {
            get;
            set;
        }

        public List<string> UsedCharacters
        {
            get;
            set;
        }


        public IEnumerable<RSG.Model.Dialogue.Line> Lines
        {
            get;
            set;
        }

        public CharacterReplacement()
        {
            InitializeComponent();
        }

        public CharacterReplacement(List<string> usedCharacters, Dictionary<Guid, string> allCharacters)
        {
            this.AllCharacters = allCharacters;
            this.UsedCharacters = usedCharacters;
            InitializeComponent();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void ReplaceButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.ReplaceValue.SelectedItem == null || this.ReplaceWithValue.SelectedItem == null)
                return;

            foreach (var line in this.Lines)
            {
                if (line.CharacterName != (string)this.ReplaceValue.SelectedItem)
                    continue;

                line.Character = (KeyValuePair<Guid, string>)this.ReplaceWithValue.SelectedItem;
            }

            this.DialogResult = true;
        }
    }
}
