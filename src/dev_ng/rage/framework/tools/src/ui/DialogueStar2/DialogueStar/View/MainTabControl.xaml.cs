﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Model.Dialogue;

namespace DialogueStar.View
{
    /// <summary>
    /// Interaction logic for MainTabControl.xaml
    /// </summary>
    public partial class MainTabControl : UserControl
    {
        #region Constructor(s)

        public MainTabControl()
        {
            InitializeComponent();
        }

        #endregion // Constructor(s)

        #region Startup

        /// <summary>
        /// This is called once the user control has been created. We set the data context of the control here
        /// such that it has a link to the main controller object that links the view to the model. With this it means
        /// that we can have two way binding on the selected item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserControl_Loaded(Object sender, RoutedEventArgs e)
        {
            this.DialogueTabControl.DataContext = App.Controller;
        }

        #endregion //Startup

        #region Callbacks

        /// <summary>
        /// Need to make sure the tab that was right clicked is now selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StackPanel_PreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            StackPanel panel = sender as StackPanel;
            String name = (panel.Children[0] as TextBlock).Text;

            foreach (MissionDialogue mission in App.Controller.Missions)
            {
                if (mission.Name == name)
                {
                    App.Controller.SelectedDialogue = mission;
                }
            }
        }

        private void SaveContext_Click(Object sender, RoutedEventArgs e)
        {
            String filename = String.Empty;
            App.Controller.SaveDialogue(App.Controller.SelectedDialogue, false, ref filename);
        }

        private void CloseContext_Click(Object sender, RoutedEventArgs e)
        {
            App.Controller.CloseCurrentDialogue();
        }

        private void CloseOthersContext_Click(Object sender, RoutedEventArgs e)
        {
            List<MissionDialogue> tempList = new List<MissionDialogue>(App.Controller.Missions);
            foreach (MissionDialogue mission in tempList)
            {
                if (mission != App.Controller.SelectedDialogue)
                {
                    App.Controller.CloseDialogue(mission);
                }
            }

            tempList.Clear();
            tempList = null;
        }

        private void CloseMenuContext_Click(Object sender, RoutedEventArgs e)
        {
        }

        #endregion // Callbacks

    } // MainTabControl
} // DialogueStar.View
