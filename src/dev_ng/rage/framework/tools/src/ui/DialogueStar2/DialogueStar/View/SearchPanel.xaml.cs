﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Model.Dialogue;
using RSG.Model.Dialogue.Search;


namespace DialogueStar.View
{
    /// <summary>
    /// Interaction logic for SearchPanel.xaml
    /// </summary>
    public partial class SearchPanel : UserControl
    {
        #region Events 

        /// <summary>
        /// This event gets fired when the user presses the close button
        /// </summary>
        public event EventHandler CloseButtonClicked;

        #endregion // Events

        #region Properties

        /// <summary>
        /// Lists the string representation of the search ranges.
        /// Current File: Only search the file that the user has focus on. If no file is selected this doesn't do anything.
        /// All Open Files: Searches through all the opened files. If no files are open this doesn't do anything.
        /// All DStar Files: Searches through the currently opened files and then loads up each DStar file in turn searching through them
        /// The DStar files that are loaded are the ones in the DStar directory from the Controller.
        /// </summary>
        public static ObservableCollection<String> SearchRanges = new ObservableCollection<String>() { "Current File", "All Open Files", "All DStar Files" };

        /// <summary>
        /// A collection of searc parameter controls that are currently shown in the UI.
        /// Upon pressing Run these are the searches that are made.
        /// </summary>
        public ObservableCollection<UIElement> SearchParameters
        {
            get { return (ObservableCollection<UIElement>)GetValue(SearchParametersProperty); }
            set { SetValue(SearchParametersProperty, value); }
        }
        public static DependencyProperty SearchParametersProperty = DependencyProperty.Register("SearchParameters", typeof(ObservableCollection<UIElement>), typeof(SearchPanel));

        /// <summary>
        /// A value representing if we currently have valid conditions for a search. For this to be true each search parameters in the above
        /// collection need to be valid.
        /// </summary>
        public Boolean ValidSearch
        {
            get { return (Boolean)GetValue(ValidSearchProperty); }
            set { SetValue(ValidSearchProperty, value); }
        }
        public static DependencyProperty ValidSearchProperty = DependencyProperty.Register("ValidSearch", typeof(Boolean), typeof(SearchPanel));

        /// <summary>
        /// Represents a string value of the current search range, this value is one of the values listed above in the SearchRanges static collection.
        /// </summary>
        public String SearchRange
        {
            get { return (String)GetValue(SearchRangeProperty); }
            set { SetValue(SearchRangeProperty, value); }
        }
        public static DependencyProperty SearchRangeProperty = DependencyProperty.Register("SearchRange", typeof(String), typeof(SearchPanel));

        /// <summary>
        /// A collection of the search results from the previous search (can be 0). This collection is represented in the search panel data grid
        /// The KeyValuePair in the value parameter specifes the conversation index and the line index for the search result.
        /// </summary>
        public RSG.Base.Collections.ObservableDictionary<SearchResult, KeyValuePair<int, int>> SearchResults
        {
            get { return (RSG.Base.Collections.ObservableDictionary<SearchResult, KeyValuePair<int, int>>)GetValue(SearchResultPropertyNew); }
            set { SetValue(SearchResultPropertyNew, value); }
        }
        public static DependencyProperty SearchResultPropertyNew = DependencyProperty.Register("SearchResults",
            typeof(RSG.Base.Collections.ObservableDictionary<SearchResult, KeyValuePair<int, int>>), typeof(SearchPanel));

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor
        /// </summary>
        public SearchPanel()
        {
            InitializeComponent();

            SearchParameters = new ObservableCollection<UIElement>();
            ValidSearch = false;
            SearchRange = "All DStar Files";
            SearchResults = new RSG.Base.Collections.ObservableDictionary<SearchResult, KeyValuePair<int, int>>();

            // Create the first search parameter control so we can always have at least one on show.
            SearchParametersPanel newPanel = new SearchParametersPanel() { Margin = new Thickness(0, 5, 0, 0) };
            newPanel.AddPressed += SearchParametersPanel_AddPressed;
            newPanel.DeletePressed += SearchParametersPanel_DeletePressed;
            newPanel.ValidationChanged += SearchParametersPanel_ValidationChanged;
            SearchParameters.Add(newPanel);
        }

        #endregion // Constructor(s)

        #region Callback Logic

        private void SearchParametersPanel_ValidationChanged(Object sender, EventArgs e)
        {
            DetermineValidation();
        }

        /// <summary>
        /// This gets called whenever one of the current search parameter controls has it's Add
        /// button pressed. This function creates a new one and makes sure that after it has added the
        /// correct event listeners it is added to the UI in the correct place.
        /// </summary>
        /// <param name="sender">The parameter control that fired the event</param>
        /// <param name="e">The arguments attached to the event</param>
        private void SearchParametersPanel_AddPressed(Object sender, EventArgs e)
        {
            int index = this.SearchParameters.IndexOf(sender as UIElement);
            SearchParametersPanel newPanel = new SearchParametersPanel() { Margin = new Thickness(0, 5, 0, 0) };
            newPanel.AddPressed += SearchParametersPanel_AddPressed;
            newPanel.DeletePressed += SearchParametersPanel_DeletePressed;
            newPanel.ValidationChanged += SearchParametersPanel_ValidationChanged;
            if (index != -1)
            {
                SearchParameters.Insert(index + 1, newPanel);
            }
            else
            {
                SearchParameters.Add(newPanel);
            }

            DetermineValidation();
        }

        /// <summary>
        /// This gets called whenever one of the current search parameter controls has it's Delete
        /// button pressed. This function removes the calling control from the UI first making sure that
        /// the events have been stopped listening to.
        /// </summary>
        /// <param name="sender">The parameter control that fired the event</param>
        /// <param name="e">The arguments attached to the event</param>
        private void SearchParametersPanel_DeletePressed(Object sender, EventArgs e)
        {
            int index = this.SearchParameters.IndexOf(sender as UIElement);
            if (index != -1)
            {
                (sender as SearchParametersPanel).AddPressed -= SearchParametersPanel_AddPressed;
                (sender as SearchParametersPanel).DeletePressed -= SearchParametersPanel_DeletePressed;
                (sender as SearchParametersPanel).ValidationChanged -= SearchParametersPanel_ValidationChanged;

                this.SearchParameters.RemoveAt(index);

                DetermineValidation();
            }
        }

        /// <summary>
        /// This is called whenever the close button is pressed on this control
        /// </summary>
        /// <param name="sender">The control that fired the event (this)</param>
        /// <param name="e">The arguments attached to the event</param>
        private void Close_Click(Object sender, RoutedEventArgs e)
        {
            if (CloseButtonClicked != null)
                CloseButtonClicked(this, new EventArgs());
        }

        /// <summary>
        /// This is called whenever the Run button is pressed. This function takes the current parameters
        /// of the searches and creates the searches and calls the apprioate function in the controller.
        /// </summary>
        /// <param name="sender">The control that fired the event (this)</param>
        /// <param name="e">The arguments attached to the event</param>
        private void Run_Click(Object sender, RoutedEventArgs e)
        {
            this.SearchResults.Clear();

            SearchRanges range = DialogueStar.SearchRanges.None;
            switch (this.SearchRange)
            {
                case "Current File":
                    range = DialogueStar.SearchRanges.Current;
                    if (App.Controller.SelectedDialogue == null)
                    {
                        this.SearchRange = "All DStar Files";
                        return;
                    }
                    break;
                case "All Opened Files":
                    range = DialogueStar.SearchRanges.Opened;
                    if (App.Controller.Missions.Count == 0)
                    {
                        this.SearchRange = "All DStar Files";
                        return;
                    }
                    break;
                case "All DStar Files":
                    range = DialogueStar.SearchRanges.All;
                    break;
                default:
                    break;
            }

            RSG.Base.Collections.ObservableDictionary<SearchResult, KeyValuePair<int, int>>[] resultsNew = new RSG.Base.Collections.ObservableDictionary<SearchResult, KeyValuePair<int, int>>[this.SearchParameters.Count];
            int index = 0;
            foreach (SearchParametersPanel searchParameters in this.SearchParameters)
            {
                resultsNew[index] = new RSG.Base.Collections.ObservableDictionary<SearchResult, KeyValuePair<int, int>>();
                

                SearchOptions searchOptions = SearchOptions.None;
                ComparisonOptions comparisonOptions = ComparisonOptions.None;
                switch (searchParameters.SearchOption)
                {
                    case "Contains":
                        comparisonOptions = ComparisonOptions.Contains;
                        break;
                    case "Starts with":
                        comparisonOptions = ComparisonOptions.StartsWith;
                        break;
                    case "Ends with":
                        comparisonOptions = ComparisonOptions.EndsWith;
                        break;
                    case "Is equal to":
                        comparisonOptions = ComparisonOptions.EqualTo;
                        break;
                    case "Is not equal to":
                        comparisonOptions = ComparisonOptions.NotEqualTo;
                        break;
                    case "Is less than":
                        comparisonOptions = ComparisonOptions.LessThan;
                        break;
                    case "Is greater than":
                        comparisonOptions = ComparisonOptions.GreaterThen;
                        break;
                    default:
                        break;
                }

                if ((Boolean)this.IgnoreCaseCheck.IsChecked)
                {
                    searchOptions = SearchOptions.IgnoreCase;
                }

                SearchType searchType = searchParameters.SelectedSearchAttribute.SearchType;

                List<SearchResult> results = null;
                if (searchType == SearchType.Search_Numeric_Type)
                {
                    results = App.Controller.FindAll(range, searchParameters.SearchField, searchType, comparisonOptions, searchOptions, searchParameters.NumericSearchText);
                }
                else
                {
                    results = App.Controller.FindAll(range, searchParameters.SearchField, searchType, comparisonOptions, searchOptions, searchParameters.SearchText);
                }

                foreach (SearchResult result in results)
                {
                    // Get the conversation index if needed
                    int conversationIndex = -1;
                    int lineIndex = -1;
                    if (result.ModelObject is Conversation || result.ModelObject is RSG.Model.Dialogue.Line)
                    {
                        if (result.ModelObject is Conversation)
                        {
                            Conversation conversation = (result.ModelObject as Conversation);
                            conversationIndex = conversation.Mission.Conversations.IndexOf(conversation) + 1;
                        }
                        else
                        {
                            Conversation conversation = (result.ModelObject as RSG.Model.Dialogue.Line).Conversation;
                            conversationIndex = conversation.Mission.Conversations.IndexOf(conversation) + 1;
                        }
                    }

                    // Get the line index if needed
                    if (result.ModelObject is RSG.Model.Dialogue.Line)
                    {
                        RSG.Model.Dialogue.Line line = result.ModelObject as RSG.Model.Dialogue.Line;
                        lineIndex = line.Conversation.Lines.IndexOf(line) + 1;
                    }

                    resultsNew[index].Add(result, new KeyValuePair<int, int>(conversationIndex, lineIndex));

                    //SearchResults.Add(result, new KeyValuePair<int, int>(conversationIndex, lineIndex));
                }

                index++;
            }

            if (this.SearchParameters.Count > 1)
            {
                // Need to take the union of all the search results
                foreach (RSG.Base.Collections.ObservableDictionary<SearchResult, KeyValuePair<int, int>> resultGroup in resultsNew)
                {
                    foreach (KeyValuePair<SearchResult, KeyValuePair<int, int>> result in resultGroup)
                    {
                        Boolean resultFoundInAllOtherGroups = true;
                        foreach (RSG.Base.Collections.ObservableDictionary<SearchResult, KeyValuePair<int, int>> otherGroup in resultsNew)
                        {
                            if (otherGroup == resultGroup)
                                continue;

                            Boolean resultFound = false;
                            foreach (KeyValuePair<SearchResult, KeyValuePair<int, int>> otherResult in otherGroup)
                            {
                                if (CompareResultKeyPairing(otherResult, result))
                                {
                                    resultFound = true;
                                    break;
                                }
                            }
                            if (resultFound == false)
                            {
                                resultFoundInAllOtherGroups = false;
                                break;
                            }
                        }

                        if (resultFoundInAllOtherGroups == true)
                        {
                            SearchResults.Add(result.Key, result.Value);
                        }
                    }

                    // Because we are dealing with the union of the dictionaries we only need to have looped through the first one
                    // to get all of the common entires so just break
                    break;
                }
            }
            else
            {
                foreach (KeyValuePair<SearchResult, KeyValuePair<int, int>> result in resultsNew[0])
                {
                    SearchResults.Add(result.Key, result.Value);
                }
            }
        }

        /// <summary>
        /// Determines is two search result key pairing with the SearchResult object and indices for the conversation and line are the same.
        /// </summary>
        /// <param name="result1"></param>
        /// <param name="result2"></param>
        /// <returns>True if the two are the same, false otherwise</returns>
        private Boolean CompareResultKeyPairing(KeyValuePair<SearchResult, KeyValuePair<int, int>> result1, KeyValuePair<SearchResult, KeyValuePair<int, int>> result2)
        {
            if (result1.Value.Key != result2.Value.Key || result1.Value.Value != result2.Value.Value)
            {
                return false;
            }

            if (result1.Key.ModelObject is MissionDialogue && result1.Key.ModelObject is MissionDialogue)
            {
                MissionDialogue dialogue1 = result1.Key.ModelObject as MissionDialogue;
                MissionDialogue dialogue2 = result2.Key.ModelObject as MissionDialogue;
                if (dialogue1.Filename == dialogue2.Filename)
                {
                    return true;
                }
            }
            else if (result1.Key.ModelObject is Conversation && result1.Key.ModelObject is Conversation)
            {
                Conversation conversation1 = result1.Key.ModelObject as Conversation;
                Conversation conversation2 = result2.Key.ModelObject as Conversation;
                if (conversation1.Mission.Filename == conversation2.Mission.Filename)
                {
                    return true;
                }
            }
            else if (result1.Key.ModelObject is RSG.Model.Dialogue.Line && result1.Key.ModelObject is RSG.Model.Dialogue.Line)
            {
                RSG.Model.Dialogue.Line line1 = result1.Key.ModelObject as RSG.Model.Dialogue.Line;
                RSG.Model.Dialogue.Line line2 = result2.Key.ModelObject as RSG.Model.Dialogue.Line;
                if (line1.Conversation.Mission.Filename == line2.Conversation.Mission.Filename)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Gets called whenever a search result in the data grid is double clicked. When this happens we want to
        /// load up the file (if needed) and show the user where the search was found by highlighting the correct
        /// Conversation and Line.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataGrid_MouseDoubleClick(Object sender, MouseButtonEventArgs e)
        {
            if ((sender as DataGrid).SelectedItem == null)
                return;

            SearchResult selectedResult = ((KeyValuePair<SearchResult, KeyValuePair<int, int>>)((sender as DataGrid).SelectedItem)).Key as SearchResult;

            if (selectedResult.ModelObject is MissionDialogue)
            {
                MissionDialogue mission = selectedResult.ModelObject as MissionDialogue;

                // Make sure that the file is properly loaded so that it has a undo manager etc
                if (App.Controller.Missions.Contains(mission) == false)
                {
                    if (mission.Filename != String.Empty)
                    {
                        App.Controller.OpenDialogue(mission.Filename); // This will automatically select it has the current dialogue
                    }
                    else
                    {
                        this.SearchResults.Remove(selectedResult);
                    }
                }
                else
                {
                    App.Controller.SelectedDialogue = App.Controller.Missions[App.Controller.Missions.IndexOf(mission)];
                }
            }
            else if (selectedResult.ModelObject is Conversation)
            {
                Conversation conversation = selectedResult.ModelObject as Conversation;

                // Make sure that the file is properly loaded so that it has a undo manager etc
                if (App.Controller.Missions.Contains(conversation.Mission) == false)
                {
                    if (conversation.Mission.Filename != String.Empty)
                    {
                        App.Controller.OpenDialogue(conversation.Mission.Filename); // This will automatically select it has the current dialogue
                        foreach (Conversation conv in App.Controller.SelectedDialogue.Conversations)
                        {
                            if (conv.Root == conversation.Root && conv.Description == conversation.Description)
                            {
                                int conversationIndex = App.Controller.SelectedDialogue.Conversations.IndexOf(conv);
                                App.Controller.SelectedConversation = App.Controller.SelectedDialogue.Conversations[conversationIndex];
                                break;
                            }
                        }
                    }
                    else
                    {
                        this.SearchResults.Remove(selectedResult);
                    }
                }
                else
                {
                    App.Controller.SelectedDialogue = App.Controller.Missions[App.Controller.Missions.IndexOf(conversation.Mission)];
                    foreach (Conversation conv in App.Controller.SelectedDialogue.Conversations)
                    {
                        if (conv.Root == conversation.Root && conv.Description == conversation.Description)
                        {
                            int conversationIndex = App.Controller.SelectedDialogue.Conversations.IndexOf(conv);
                            App.Controller.SelectedConversation = App.Controller.SelectedDialogue.Conversations[conversationIndex];
                            break;
                        }
                    }
                }
            }
            else if (selectedResult.ModelObject is RSG.Model.Dialogue.Line)
            {
                RSG.Model.Dialogue.Line line = selectedResult.ModelObject as RSG.Model.Dialogue.Line;

                // Make sure that the file is properly loaded so that it has a undo manager etc
                if (App.Controller.Missions.Contains(line.Conversation.Mission) == false)
                {
                    if (line.Conversation.Mission.Filename != String.Empty)
                    {
                        App.Controller.OpenDialogue(line.Conversation.Mission.Filename); // This will automatically select it has the current dialogue
                        // Find the correct conversation in the file and select it
                        foreach (Conversation conv in App.Controller.SelectedDialogue.Conversations)
                        {
                            if (conv.Root == line.Conversation.Root && conv.Description == line.Conversation.Description)
                            {
                                int conversationIndex = App.Controller.SelectedDialogue.Conversations.IndexOf(conv);
                                App.Controller.SelectedConversation = App.Controller.SelectedDialogue.Conversations[conversationIndex];
                                // Find the correct line in the conversation and select it
                                foreach (RSG.Model.Dialogue.Line l in App.Controller.SelectedConversation.Lines)
                                {
                                    if (l.CharacterName == line.CharacterName && l.LineDialogue == line.LineDialogue)
                                    {
                                        int lineIndex = App.Controller.SelectedConversation.Lines.IndexOf(l);
                                        App.Controller.SelectedLine = App.Controller.SelectedConversation.Lines[lineIndex];
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                    }
                    else
                    {
                        this.SearchResults.Remove(selectedResult);
                    }
                }
                else
                {
                    App.Controller.SelectedDialogue = App.Controller.Missions[App.Controller.Missions.IndexOf(line.Conversation.Mission)]; // This will automatically select it has the current dialogue
                    // Find the correct conversation in the file and select it
                    foreach (Conversation conv in App.Controller.SelectedDialogue.Conversations)
                    {
                        if (conv.Root == line.Conversation.Root && conv.Description == line.Conversation.Description)
                        {
                            int conversationIndex = App.Controller.SelectedDialogue.Conversations.IndexOf(conv);
                            App.Controller.SelectedConversation = App.Controller.SelectedDialogue.Conversations[conversationIndex];
                            // Find the correct line in the conversation and select it
                            // Find the correct line in the conversation and select it
                            foreach (RSG.Model.Dialogue.Line l in App.Controller.SelectedConversation.Lines)
                            {
                                if (l.CharacterName == line.CharacterName && l.LineDialogue == line.LineDialogue)
                                {
                                    int lineIndex = App.Controller.SelectedConversation.Lines.IndexOf(l);
                                    App.Controller.SelectedLine = App.Controller.SelectedConversation.Lines[lineIndex];
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }
            }

            // Make sure that the selected conversation and line can actual be seen in the window. This means everything has to be updated
            // and the specific objects have to be scrolled to.
            App.Controller.MainWindow.UpdateLayout();
            App.Controller.ConversationUserControl.UpdateLayout();
            App.Controller.ConversationUserControl.BringSelectedConversationIntoView();
            App.Controller.LineUserControl.UpdateLayout();
            App.Controller.LineUserControl.BringSelectedLineIntoView();
        }

        #endregion // Callback logic

        #region Private Function(s)

        /// <summary>
        /// Using the search parameter collection determines if all of the search fields are valid,
        /// and therefore is a search run would be valied
        /// </summary>
        private void DetermineValidation()
        {
            Boolean valid = true;
            foreach (UIElement element in SearchParameters)
            {
                if ((element as SearchParametersPanel).IsValid == false)
                {
                    valid = false;
                    break;
                }
            }

            ValidSearch = valid;
        }

        #endregion // Private Function(s)

    } // SearchPanel
} // DialogueStar.View
