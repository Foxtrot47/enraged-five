﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using RSG.Model.Dialogue.Config;

namespace DialogueStar.View
{
    /// <summary>
    /// Interaction logic for ExportDialog.xaml
    /// </summary>
    public partial class ExportDialog : Window
    {
        #region Properties
        /// <summary>
        /// Export path for the mission dialogue
        /// </summary>
        public String SourceDirectory
        {
            get { return m_sourceDirectory; }
            set { m_sourceDirectory = value; }
        }
        private String m_sourceDirectory;

        /// <summary>
        /// Export path for the mission dialogue
        /// </summary>
        public String ExportDirectory
        {
            get { return m_exportDirectory; }
            set { m_exportDirectory = value; }
        }
        private String m_exportDirectory;

        /// <summary>
        /// Export path for the mission dialogue
        /// </summary>
        public String MissionDialogueExport
        {
            get { return m_missionDialogueExport; }
            set { m_missionDialogueExport = value; }
        }
        private String m_missionDialogueExport;

        /// <summary>
        /// Export path for the mission dialogue files
        /// </summary>
        public String MissionDialogueFilesExport
        {
            get { return m_missionDialogueFilesExport; }
            set { m_missionDialogueFilesExport = value; }
        }
        private String m_missionDialogueFilesExport;

        /// <summary>
        /// Export path for the mission mocap conversations
        /// </summary>
        public String MissionMocapExport
        {
            get { return m_missionMocapExport; }
            set { m_missionMocapExport = value; }
        }
        private String m_missionMocapExport;

        /// <summary>
        /// Export path for the random event dialogue
        /// </summary>
        public String RandomDialogueExport
        {
            get { return m_randomDialogueExport; }
            set { m_randomDialogueExport = value; }
        }
        private String m_randomDialogueExport;

        /// <summary>
        /// Export path for the random event dialogue files
        /// </summary>
        public String RandomDialogueFilesExport
        {
            get { return m_randomDialogueFilesExport; }
            set { m_randomDialogueFilesExport = value; }
        }
        private String m_randomDialogueFilesExport;

        /// <summary>
        /// Export path for the random event mocap conversations
        /// </summary>
        public String RandomMocapExport
        {
            get { return m_randomMocapExport; }
            set { m_randomMocapExport = value; }
        }
        private String m_randomMocapExport;

        /// <summary>
        /// Export path for the random event identifiers
        /// </summary>
        public String RandomIdentifiersExport
        {
            get { return m_randomIdentifiersExport; }
            set { m_randomIdentifiersExport = value; }
        }
        private String m_randomIdentifiersExport;
        
        /// <summary>
        /// Export path for the random event dialogue files
        /// </summary>
        public String NetDialogueExport
        {
            get { return m_netDialogueExport; }
            set { m_netDialogueExport = value; }
        }
        private String m_netDialogueExport;

        /// <summary>
        /// Export path for the random event mocap conversations
        /// </summary>
        public String NetDialogueFilesExport
        {
            get { return m_netDialogueFilesExport; }
            set { m_netDialogueFilesExport = value; }
        }
        private String m_netDialogueFilesExport;

        /// <summary>
        /// Export path for the random event identifiers
        /// </summary>
        public String NetMocapExport
        {
            get { return m_netMocapExport; }
            set { m_netMocapExport = value; }
        }
        private String m_netMocapExport;

        private DialogueLanguage _language;

        private DialogueConfigurations _configurations;
        #endregion Properties

        #region Constructor(s)

        public ExportDialog(DialogueConfigurations configurations, DialogueLanguage language)
        {
            InitializeComponent();
            this._language = language;
            this.Title += "- " + language.ToString();
            this._configurations = configurations;
        }

        #endregion // Constructor(s)

        #region Private Function(s)
        /// <summary>
        /// Opens up the open dialog and returns the file name of the select path or a empty string 
        /// if the user cancels
        /// </summary>
        /// <returns></returns>
        private String GetNewFilename()
        {
            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            dlg.DefaultExt = ".txt";
            dlg.Multiselect = false;
            dlg.Filter = "Text Documents (*.txt)|*.txt";

            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {
                // Make sure the filename exists and that it has a .dstar extension to it
                if (!File.Exists(dlg.FileName) || System.IO.Path.GetExtension(dlg.FileName) != ".txt")
                {
                    return String.Empty;
                }

                return dlg.FileName;
            }

            return String.Empty;
        }


        /// <summary>
        /// Opens up the open dialog and returns the file name of the select path or a empty string 
        /// if the user cancels
        /// </summary>
        /// <returns></returns>
        private String GetFolder(string initialPath)
        {
            // Create OpenFileDialog
            System.Windows.Forms.FolderBrowserDialog dlg = new System.Windows.Forms.FolderBrowserDialog();
            dlg.SelectedPath = initialPath;

            // Display OpenFileDialog by calling ShowDialog method 
            System.Windows.Forms.DialogResult result = dlg.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.OK)
            {
                return dlg.SelectedPath;
            }

            return String.Empty;
        }
        #endregion // Private Function(s)

        #region Button Callbacks
        /// <summary>
        /// Opens up the open dialog so that the user can select a new path for the mission dialogue export
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SourceDirectory_Click(object sender, RoutedEventArgs e)
        {
            String newFilename = GetFolder(this.SourceDirectory);
            if (newFilename != String.Empty)
            {
                SourceDirectoryTextBox.Text = newFilename;
            }
        }

        /// <summary>
        /// Opens up the open dialog so that the user can select a new path for the mission dialogue export
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExportDirectory_Click(object sender, RoutedEventArgs e)
        {
            String newFilename = GetFolder(this.ExportDirectory);
            if (newFilename != String.Empty)
            {
                ExportDirectoryTextBox.Text = newFilename;
                Dictionary<ExportPath, string> exportPaths = this._configurations.GetExportPaths(newFilename, this._language);

                MissionDialoguePath.Text = exportPaths[ExportPath.MissionDialogue];
                MissionDialogueFilesPath.Text = exportPaths[ExportPath.MissionDialogueFiles];
                MissionMocapPath.Text = exportPaths[ExportPath.MissionMocap];
                RandomDialoguePath.Text = exportPaths[ExportPath.RandomDialogue];
                RandomDialogueFilesPath.Text = exportPaths[ExportPath.RandomDialogueFiles];
                RandomMocapPath.Text = exportPaths[ExportPath.RandomMocap];
                RandomIdentifierPath.Text = exportPaths[ExportPath.RandomIdentifiers];
                NetDialogueTextBox.Text = exportPaths[ExportPath.NetDialogue];
                this.NetDialogueFilesTextBox.Text = exportPaths[ExportPath.NetDialogueFiles];
                this.NetMocapTextBox.Text = exportPaths[ExportPath.NetMocap];
            }
        }

        /// <summary>
        /// The main cancel button: it closes the dialogue returning a false result
        /// so that the calling function can take the correct action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cancel_Click(Object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        /// <summary>
        /// The main cancel button: it closes the dialogue returning a false result
        /// so that the calling function can take the correct action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Ok_Click(Object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        /// <summary>
        /// The main save button: it closes the dialogue returning a true result
        /// so that the calling function can take the correct action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Save_Click(Object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
        #endregion Button Callbacks// Button Callbacks

    }
}
