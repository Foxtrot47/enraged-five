﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using RSG.Base.Logging;
using RSG.Editor;
using RSG.Editor.Controls;
using RSG.Editor.Controls.Helpers;
using RSG.Editor.SharedCommands;
using RSG.AnimationToolkit.ViewModel;
using RSG.AnimationToolkit.Commands;
using System.Collections.ObjectModel;

namespace AnimationToolkit
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : RsMainWindow
    {
        #region Static Fields
        /// <summary>
        /// The global identifier used for the action menu.
        /// </summary>
        private static readonly Guid _actionsMenu = new Guid("FB6395F0-ABBA-48DF-8995-3264BE70286F");

        #endregion Static Fields

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public MainWindow(RsApplication application)
        {
            InitializeComponent();
            DataContext = new AnimationToolkitDataContext();
            AddCommandInstances(application);
            AttachCommandBindings();
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Adds the commands/menus to the command manager.
        /// </summary>
        internal void AddCommandInstances(RsApplication application)
        {
            // Register the mru list.
            application.RegisterMruList(
                "Files",
                true,
                RockstarCommands.OpenFileFromMru,
                new Guid("FE2926AE-5067-478D-949A-557B76C41804"),
                new Guid("2C28F26B-2AD7-4D17-91C7-7D8901699A54"),
                new Guid("9B950624-0E34-4DE1-ADA2-2EE0656366D0"));

            RockstarCommandManager.AddActionsMenu(_actionsMenu);

            // File menu.
            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.OpenFile,
                0,
                false,
                "Open Source Folders",
                new Guid("AA9A7A22-0861-44C9-8551-754EF7A92B1E"),
                RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddCommandInstance(
               AnimationToolkitCommands.OpenSourceFolderAppend,
               0,
               false,
               "Open Source Folders (Append)",
               new Guid("E601F8EC-180F-4A2D-B595-3ED7779CFD64"),
               RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddCommandInstance(
                AnimationToolkitCommands.OpenConcatListFile,
                1,
                false,
                "Open Concat List Source Folders",
                new Guid("D7702BB9-B24D-46CE-879A-07A3D38C0410"),
                RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddCommandInstance(
                AnimationToolkitCommands.OpenConcatListFileAppend,
                1,
                false,
                "Open Concat List Source Folders (Append)",
                new Guid("8298A385-8B29-4045-B8C9-9F2A917E85AB"),
                RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddCommandInstance(
                AnimationToolkitCommands.OpenZipFolder,
                1,
                true,
                "Open Zip Folders",
                new Guid("D73F5522-D822-4102-88D9-D44486AC3B0D"),
                RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddCommandInstance(
               AnimationToolkitCommands.OpenZipFolderAppend,
               1,
               false,
               "Open Zip Folders (Append)",
               new Guid("19E36940-D989-4327-A793-431C2AD3EA7F"),
               RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddCommandInstance(
                AnimationToolkitCommands.OpenClipFiles,
                1,
                true,
                "Open Clip Folders",
                new Guid("FDE23B9F-FD9E-4A1C-A442-48E3286A0BD5"),
                RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddCommandInstance(
                AnimationToolkitCommands.OpenPresetFile,
                1,
                true,
                "Open Preset File",
                new Guid("A1460552-89D0-4BAB-9D6C-8F45F13371E3"),
                RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddCommandInstance(
                AnimationToolkitCommands.SavePresetFile,
                1,
                false,
                "Save Preset File",
                new Guid("E5DBC38A-8CA1-4735-BDA3-61C5ED85281A"),
                RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddCommandInstance(
                AnimationToolkitCommands.SelectAll,
                0,
                false,
                "Select All",
                new Guid("EF691894-B9BE-4A70-8800-73087BE8B9FC"),
                _actionsMenu);

            RockstarCommandManager.AddCommandInstance(
                AnimationToolkitCommands.DeSelectAll,
                0,
                false,
                "DeSelect All",
                new Guid("344336CF-D491-4DC0-BF43-5369F80C53B9"),
                _actionsMenu);
        }

        /// <summary>
        /// Attaches all of the necessary command bindings to this instance.
        /// </summary>
        private void AttachCommandBindings()
        {
            // File management commands.
            new OpenZipFolderAction(this.DataContextResolver)
                .AddBinding(RockstarCommands.OpenFile, this);
            new OpenRPFFolderAction(this.DataContextResolver)
                .AddBinding(AnimationToolkitCommands.OpenZipFolder, this);
            new OpenClipFilesAction(this.DataContextResolver)
                .AddBinding(AnimationToolkitCommands.OpenClipFiles, this);
            new SelectAllAction(this.DataContextResolver)
                .AddBinding(AnimationToolkitCommands.SelectAll, this);
            new DeSelectAllAction(this.DataContextResolver)
                .AddBinding(AnimationToolkitCommands.DeSelectAll, this);
            new OpenZipAppendFolderAction(this.DataContextResolver)
                .AddBinding(AnimationToolkitCommands.OpenSourceFolderAppend, this);
            new OpenRPFFolderAppendAction(this.DataContextResolver)
                .AddBinding(AnimationToolkitCommands.OpenZipFolderAppend, this);
            new OpenConcatListFileAction(this.DataContextResolver)
                .AddBinding(AnimationToolkitCommands.OpenConcatListFile, this);
            new OpenConcatListFileAppendAction(this.DataContextResolver)
                .AddBinding(AnimationToolkitCommands.OpenConcatListFileAppend, this);
            new OpenPresetFileAction(this.DataContextResolver)
                .AddBinding(AnimationToolkitCommands.OpenPresetFile, this);
            new SavePresetFileAction(this.DataContextResolver)
                .AddBinding(AnimationToolkitCommands.SavePresetFile, this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        private AnimationToolkitDataContext DataContextResolver(CommandData command)
        {
            AnimationToolkitDataContext dc = null;
            if (!this.Dispatcher.CheckAccess())
            {
                dc = this.Dispatcher.Invoke<AnimationToolkitDataContext>(() => this.DataContext as AnimationToolkitDataContext);
            }
            else
            {
                dc = this.DataContext as AnimationToolkitDataContext;
            }

            if (dc == null)
            {
                Debug.Fail("Data context isn't valid.");
                throw new ArgumentException("Data context isn't valid.");
            }
            return dc;
        }

        private void SelectItems(List<CheckedListItem> items)
        {
            if (items.Count > 0)
            {
                bool bState = !items[0].IsChecked;
                foreach (CheckedListItem it in items)
                {
                    it.IsChecked = bState;
                }
            }
        }

        #endregion // Methods

        private void MyZipDataGrid_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key ==  Key.Space)
            {
                List<CheckedListItem> items = MyIngameZipDataGrid.SelectedItems.OfType<CheckedListItem>().ToList();
                SelectItems(items);
            }
        }

        private void MyRpfDataGrid_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                List<CheckedListItem> items = MyIngameRPFDataGrid.SelectedItems.OfType<CheckedListItem>().ToList();
                SelectItems(items);
            }
        }

        private void MyClipDataGrid_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                List<CheckedListItem> items = MyIngameClipDataGrid.SelectedItems.OfType<CheckedListItem>().ToList();
                SelectItems(items);
            }
        }

        private void MyCutsceneZipDataGrid_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                List<CheckedListItem> items = MyCutsceneZipDataGrid.SelectedItems.OfType<CheckedListItem>().ToList();
                SelectItems(items);
            }
        }

        private void MyCutsceneRpfDataGrid_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                List<CheckedListItem> items = MyCutsceneRPFDataGrid.SelectedItems.OfType<CheckedListItem>().ToList();
                SelectItems(items);
            }
        }

    } // MainWindow
}
