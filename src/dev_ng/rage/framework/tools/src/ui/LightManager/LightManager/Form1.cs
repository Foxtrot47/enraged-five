﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using RSG.Base.Logging.Universal;
using RSG.Base.Logging;
using RSG.SourceControl.Perforce;
using P4API;

namespace LightManager
{
    public partial class Form1 : Form
    {
        class Log
        {
            private IUniversalLog log = null;
            private IUniversalLogTarget logTarget = null;

            public Log(string strLogFilename)
            {
                log = LogFactory.CreateUniversal<UniversalLog>(strLogFilename);
                logTarget = LogFactory.CreateUniversalLogFile(log);
            }

            ~Log()
            {
                CloseFile();
            }

            public void CloseFile()
            {
                if (logTarget != null && log != null)
                {
                    ((UniversalLogFile)logTarget).Flush();
                    logTarget.Disconnect(log);
                }
            }

            public void LogLine(string strLine)
            {
                strLine = strLine.Replace("\\", "/"); // Easier to copy lines out to debug in commandline
                if (logTarget != null && log != null)
                {
                    lock (log)
                    {
                        if (strLine.StartsWith("ERROR:"))
                        {
                            log.Error(strLine.Substring(7));
                        }
                        else if (strLine.StartsWith("WARNING:"))
                        {
                            log.Warning(strLine.Substring(9));
                        }
                        else if (strLine.StartsWith("DEBUG:"))
                        {
                            log.Debug(strLine.Substring(7));
                        }
                        else
                        {
                            log.Message(strLine);
                        }
                    }
                }
            }
        }

        class PartEntry
        {
            public string strParentFolder = string.Empty;
            public List<string> shotEntries = new List<string>();
        }

        List<PartEntry> lstParts = new List<PartEntry>();
        List<string> lstOrderedParts = new List<string>();
        private string strCutXMLConcatPath = System.Environment.GetEnvironmentVariable("RS_TOOLSROOT") + "/bin/cutscene/cutfconcat.exe";
        private string strLightMergePath = System.Environment.GetEnvironmentVariable("RS_TOOLSROOT") + "/bin/cutscene/cutflightmerge.exe";
        private string strLogDir = System.Environment.GetEnvironmentVariable("RS_TOOLSROOT") + "/logs/cutscene/process";
        private string strUniveralLogViewerPath = System.Environment.GetEnvironmentVariable("RS_TOOLSROOT") + "/bin/UniversalLogViewer/UniversalLogViewer.exe";
        private string strAnimConcatPath = System.Environment.GetEnvironmentVariable("RS_TOOLSROOT") + "/bin/anim/animconcat.exe";
        private string strClipConcatPath = System.Environment.GetEnvironmentVariable("RS_TOOLSROOT") + "/bin/cutscene/cutfclipconcat.exe";
        private string strTempDir = Path.GetTempPath() + "LightManager";
        string strOutputFolder = String.Empty;
        private Log lgLog;
        private P4 m_P4 = null;

        public Form1()
        {
            InitializeComponent();
        }

        private void Reset()
        {
            lstParts.Clear();
            lstOrderedParts.Clear();
            strOutputFolder = String.Empty;
        }

        private void CreateDirectory(string folder)
        {
            try
            {
                Directory.CreateDirectory(folder);
            }
            catch (Exception /*e*/) { }
        }

        private void DeleteFolderAndContents(string folder)
        {
            try
            {
                Directory.Delete(folder, true);
            }
            catch(Exception /*e*/){}
        }

        private void DeleteFile(string file)
        {
            try
            {
                File.Delete(file);
            }
            catch (Exception /*e*/) { }
        }

        // Get a list of all the animated lights in the cutfile, these are lights with a bStatic property of false
        private void GetAnimatedLights(string filename, ref List<string> lstLights)
        {
            using (XmlTextReader textReader = new XmlTextReader(filename))
            {
                textReader.ReadToFollowing("pCutsceneObjects");

                XmlReader objectReader = textReader.ReadSubtree();

                while (objectReader.ReadToFollowing("Item"))
                {
                    if (objectReader.HasAttributes)
                    {
                        if (objectReader.GetAttribute(0) == "rage__cutfLightObject")
                        {
                            XmlReader objectIdReader = objectReader.ReadSubtree();
                            XmlReader nameReader = objectReader.ReadSubtree();

                            string objectName = String.Empty;
                            if (nameReader.ReadToFollowing("cName"))
                            {
                                objectName = nameReader.ReadInnerXml();
                            }

                            if (objectIdReader.ReadToFollowing("bStatic"))
                            {
                                if (objectIdReader.HasAttributes)
                                {
                                    if (objectIdReader.GetAttribute(0) == "false")
                                    {
                                        lstLights.Add(objectName);
                                    }
                                }
                            }
                        }
                    }
                }

                textReader.Close();
            }
        }

        // Get the duration of the cutfile, this should always be the duration of the stream
        private bool GetDurationFromCutFile(string strCutFile, out double fDuration)
        {
            string strLine = String.Empty;
            System.IO.StreamReader srFile = new System.IO.StreamReader(strCutFile);

            while ((strLine = srFile.ReadLine()) != null)
            {
                if (strLine.Contains("fTotalDuration"))
                {
                    int index = strLine.IndexOf("\"");
                    int index2 = strLine.IndexOf("\"", index + 1);
                    string strRangeStart = strLine.Substring(index + 1, index2 - index - 1);

                    double dResult = 0;
                    if (Double.TryParse(strRangeStart, out dResult))
                    {
                        fDuration = (float)dResult;
                        return true;
                    }
                }
            }

            fDuration = 0;
            return false;
        }

        // Validate the light objects are unique between all parts, we dont want lights to have clashing names
        private bool ValidateLightsAreUnique()
        {
            Dictionary<string, string> dictLightList = new Dictionary<string, string>();

            for (int i = 0; i < lstParts.Count; ++i)
            {
                string filename = Path.Combine(lstParts[i].strParentFolder, "data.lightxml");

                using (XmlTextReader textReader = new XmlTextReader(filename))
                {
                    textReader.ReadToFollowing("pCutsceneObjects");

                    XmlReader objectReader = textReader.ReadSubtree();

                    while (objectReader.ReadToFollowing("Item"))
                    {
                        if (objectReader.HasAttributes)
                        {
                            if (objectReader.GetAttribute(0) == "rage__cutfLightObject")
                            {
                                XmlReader objectIdReader = objectReader.ReadSubtree();
                                XmlReader nameReader = objectReader.ReadSubtree();

                                string objectName = String.Empty;
                                if (nameReader.ReadToFollowing("cName"))
                                {
                                    objectName = nameReader.ReadInnerXml();
                                }

                                if (dictLightList.ContainsKey(objectName.ToLower()))
                                {
                                    string strValue = String.Empty;
                                    dictLightList.TryGetValue(objectName.ToLower(), out strValue);

                                    lgLog.LogLine("ERROR: Light object '" + objectName + "' in '" + filename + "' is already defined in '" + strValue + "'");
                                    return false;
                                }

                                dictLightList.Add(objectName.ToLower(), filename);
                            }
                        }
                    }
                }
            }

            return true;
        }

        // Process the animated lights (if any), this will create lights from the shots in the external concat directory.
        private bool ProcessAnimatedLights()
        {
            // Look through every part to see if it contains any animated lights
            for (int i = 0; i < lstOrderedParts.Count; ++i)
            {
                string strMainLightFile = Path.Combine(Path.GetDirectoryName(lstOrderedParts[i]), "data.lightxml");
                List<string> lstAnimatedLights = new List<string>();
                GetAnimatedLights(strMainLightFile, ref lstAnimatedLights);

                List<double> lstEntries = new List<double>();

                if (lstAnimatedLights.Count > 0)
                {
                    double iStartCount = 0.0f;
                    double iEndCount = 0.0f;

                    // Now iterate over every part again and create a list to create a concatted animation
                    for (int j = 0; j < lstOrderedParts.Count; ++j)
                    {
                        string strStreamFile = Path.Combine(lstOrderedParts[j].Replace(Path.GetExtension(lstOrderedParts[j]),""), "data_stream.cutxml");
                        if (!File.Exists(strStreamFile))
                        {
                            lgLog.LogLine("ERROR: " + strStreamFile + " does not exist.");
                            return false;
                        }

                        double fDuration = 0;
                        GetDurationFromCutFile(strStreamFile, out fDuration);

                        if (j < i)
                        {
                            iStartCount += fDuration;
                        }

                        if (j == i)
                        {
                            if (iStartCount != 0.0f)
                            {
                                lstEntries.Add(iStartCount);
                            }

                            lstEntries.Add(-1);
                        }

                        if (j > i)
                        {
                            iEndCount += fDuration;
                        }

                        if (j == lstOrderedParts.Count - 1)
                        {
                            if (iEndCount != 0.0f)
                            {
                                lstEntries.Add(iEndCount);
                            }
                        }
                    }

                    for (int k = 0; k < lstAnimatedLights.Count; ++k)
                    {
                        string strLightFile = strTempDir + "\\" + Path.GetFileName(lstOrderedParts[i].Replace(Path.GetExtension(lstOrderedParts[i]), "")) + "_" + lstAnimatedLights[k] + ".anim";
                        if (File.Exists(strLightFile))
                        {
                            RunP4Command("add", strOutputFolder, "", lstAnimatedLights[k] + ".anim");
                            RunP4Command("edit", strOutputFolder, "", lstAnimatedLights[k] + ".anim");

                            // We should only ever have a max of 3 entries, before/after and our scene
                            if (lstEntries.Count == 2)
                            {
                                if (lstEntries[0] == -1 && lstEntries[1] != -1) // Anim/Padding
                                {
                                    string strOutput = String.Empty;
                                    if (!RunProcess(strAnimConcatPath, "-anims=" + strLightFile + " -leadout=" + lstEntries[1] + " -overlap=0 -out=" + Path.Combine(strOutputFolder, lstAnimatedLights[k]) + " –absolutemover -superset" + " -ulog -nopopups", out strOutput, true))
                                    {
                                        return false;
                                    }
                                }

                                if (lstEntries[0] != -1 && lstEntries[1] == -1) // Padding/Anim
                                {
                                    string strOutput = String.Empty;
                                    if (!RunProcess(strAnimConcatPath, "-anims=" + strLightFile + " -leadin=" + lstEntries[0] + " -overlap=0 -out=" + Path.Combine(strOutputFolder, lstAnimatedLights[k]) + " –absolutemover -superset" + " -ulog -nopopups", out strOutput, true))
                                    {
                                        return false;
                                    }
                                }
                            }
                            else if (lstEntries.Count == 3) // If we have 3 this should always be Padding/Anim/Padding
                            {
                                if (lstEntries[1] == -1)
                                {
                                    string strTemp = Path.GetTempFileName();
                                    File.Delete(strTemp);

                                    string strOutput = String.Empty;
                                    if (!RunProcess(strAnimConcatPath, "-anims=" + strLightFile + " -leadin=" + lstEntries[0] + " -overlap=0 -out=" + strTemp + " –absolutemover -superset" + " -ulog -nopopups", out strOutput, true))
                                    {
                                        return false;
                                    } 
                                    
                                    if (!RunProcess(strAnimConcatPath, "-anims=" + strTemp + " -leadout=" + lstEntries[2] + " -overlap=0 -out=" + Path.Combine(strOutputFolder, lstAnimatedLights[k]) + " –absolutemover -superset" + " -ulog -nopopups", out strOutput, true))
                                    {
                                        DeleteFile(strTemp);
                                        return false;
                                    }

                                    DeleteFile(strTemp);
                                }
                            }
                        }
                        //else
                        //{
                        //    lgLog.LogLine("ERROR: " + strLightFile + " does not exist");
                        //    return false;
                        //}

                        string strLightClipFile = strTempDir + "\\" + Path.GetFileName(lstOrderedParts[i].Replace(Path.GetExtension(lstOrderedParts[i]), "")) + "_" + lstAnimatedLights[k] + ".clip";
                        if (File.Exists(strLightClipFile))
                        {
                            RunP4Command("add", strOutputFolder, "", lstAnimatedLights[k] + ".clip");
                            RunP4Command("edit", strOutputFolder, "", lstAnimatedLights[k] + ".clip");

                            string strOutput = String.Empty;
                            if (!RunProcess(strClipConcatPath, "-clips=" + strLightClipFile + " -ulog -nopopups -out=" + Path.Combine(strOutputFolder, lstAnimatedLights[k]), out strOutput, true))
                            {
                                return false;
                            }
                        }
                    }
                }
            }

            return true;
        }

        // Create part objects, we might have multiple parts per scene
        private void AddPart(string filename)
        {
            lstOrderedParts.Add(filename);

            for (int i = 0; i < lstParts.Count; ++i)
            {
                if (String.Compare(Path.GetDirectoryName(filename), lstParts[i].strParentFolder, StringComparison.OrdinalIgnoreCase) == 0)
                {
                    lstParts[i].shotEntries.Add(Path.GetFileNameWithoutExtension(filename));
                    return;
                }
            }

            PartEntry p = new PartEntry();
            p.strParentFolder = Path.GetDirectoryName(filename);
            p.shotEntries.Add(Path.GetFileNameWithoutExtension(filename));
            lstParts.Add(p);
        }

        public void ProcessIntoEntries(string strFile)
        {
            using (XmlTextReader textReader = new XmlTextReader(strFile))
            {
                textReader.ReadToFollowing("parts");

                XmlReader objectReader = textReader.ReadSubtree();

                while (objectReader.ReadToFollowing("Item"))
                {
                    XmlReader objectIdReader = objectReader.ReadSubtree();

                    if (objectIdReader.ReadToFollowing("filename"))
                    {
                        AddPart(objectIdReader.ReadInnerXml());
                    }
                }

            }

            using (XmlTextReader textReader = new XmlTextReader(strFile))
            {
                if (textReader.ReadToFollowing("path"))
                {
                    strOutputFolder = textReader.ReadInnerXml();
                }
            }
        }

        private string[] GenerateCommands(string[] strArgs, string strFile)
        {
            List<string> alCommands = new List<string>();

            foreach (string arg in strArgs)
            {
                alCommands.Add(arg);
            }

            alCommands.Add(strFile);

            return alCommands.ToArray();
        }

        private bool RunP4Command(string strCommand, string strOutputFolder, string strAdditionalFolder, string strFilename, params string[] strArgs)
        {
            //if (!m_P4Integration) return true;

            P4RecordSet recordSet = null;
            string strFullPath = Path.Combine(strOutputFolder, strAdditionalFolder, strFilename);
            try
            {
                lgLog.LogLine("DEBUG: P4 " + strCommand + ", " + strFullPath);
                string localPath = Path.Combine(strOutputFolder, strAdditionalFolder);
                localPath = Path.Combine(localPath, strFilename);

                lock (m_P4)
                {
                    m_P4.Connect();
                    m_P4.CWD = strOutputFolder;
                    recordSet = m_P4.Run(strCommand, GenerateCommands(strArgs, localPath));
                }
            }
            catch (Exception e)
            {
                if (e.Message.IndexOf("chmod") != -1)
                {
                    lgLog.LogLine("\twarning:" + e.Message);
                }
                else
                {
                    throw e;
                }
            }

            return true;
        }

        private bool RunProcess(string strExe, string strArguments, out string strOutput, bool bWaitForExit)
        {
            try
            {
                System.Diagnostics.Process pExe = new System.Diagnostics.Process();
                pExe.StartInfo.FileName = strExe;
                pExe.StartInfo.Arguments = strArguments;
                pExe.StartInfo.CreateNoWindow = true;
                pExe.StartInfo.UseShellExecute = false;
                pExe.StartInfo.RedirectStandardOutput = true;
                pExe.Start();

                lgLog.LogLine("DEBUG: " + strExe + " " + strArguments);

                strOutput = String.Empty;
                if (bWaitForExit)
                {
                    strOutput = pExe.StandardOutput.ReadToEnd();
                    pExe.WaitForExit();

                    if (pExe.ExitCode != 0)
                    {
                        lgLog.LogLine("ERROR: Executing " + strExe + " " + strArguments + "\n" + strOutput);
                        return false;
                    }
                }

                return true;
            }
            catch (System.ComponentModel.Win32Exception e)
            {
                lgLog.LogLine("ERROR: " + e.Message + " :" + strExe + " " + strArguments);
            }

            strOutput = String.Empty;
            return false;
        }

        // Split the parent internal concat light file into child part light files and save them into a temp dir.
        private bool SplitLightFiles()
        {
            for (int i = 0; i < lstParts.Count; ++i)
            {
                string strOutput = String.Empty;
                if (!RunProcess(strLightMergePath, "-split -cutLight=" + lstParts[i].strParentFolder + @"\data.lightxml -ulog -out=" + strTempDir, out strOutput, true))
                {
                    return false;
                }
            }

            return true;
        }

        private bool JoinLightFiles()
        {
            string strTempCutFilename = Path.GetTempFileName();
            TextWriter twCutFileList = new StreamWriter(strTempCutFilename);

            // Create the cutfile list which is used by the cutfconcat tool
            for (int iFolder = 0; iFolder < lstOrderedParts.Count; ++iFolder)
            {
                string[] paths = lstOrderedParts[iFolder].Split('\\');
                string s = Path.GetDirectoryName(lstOrderedParts[iFolder]);

                string lightFile = strTempDir + "\\" + paths[paths.Length - 2] + "_" + Path.GetFileNameWithoutExtension(lstOrderedParts[iFolder]) + ".lightxml";
                twCutFileList.WriteLine(lightFile);
            }

            twCutFileList.Close();

            RunP4Command("add", strOutputFolder, "", "data.lightxml");
            RunP4Command("edit", strOutputFolder, "", "data.lightxml");

            string strOutput = String.Empty;
            if (!RunProcess(strCutXMLConcatPath, "-cutlistFile=" + strTempCutFilename + " -outputCutfile=" + strOutputFolder + "\\data.lightxml -outputDir=" + strOutputFolder + " -ulog -output -hidealloc -external", out strOutput, true))
            {
                File.Delete(strTempCutFilename);
                return false;
            }

            File.Delete(strTempCutFilename);
            return true;
        }

        private bool Process(string filename)
        {
            Reset();
            // Clean up the temp light file dir
            DeleteFolderAndContents(strTempDir);
            CreateDirectory(strTempDir);
            // Clean up the log dir
            DeleteFolderAndContents(strLogDir);
            CreateDirectory(strLogDir);

            m_P4 = new P4();
            m_P4.Connect();

            lgLog = new Log(System.Environment.GetEnvironmentVariable("RS_TOOLSROOT") + "/logs/cutscene/process/lm");

            // Split the concatlist file into part entries to be processed
            ProcessIntoEntries(filename);

            bool bResult = true;

            bResult = bResult && ValidateLightsAreUnique();

            // Run lightmerge -Split with the output path and parent folder light file
            bResult = bResult && SplitLightFiles();

            bResult = bResult && ProcessAnimatedLights();

            // Concat all light files together
            bResult = bResult && JoinLightFiles();

            lgLog.CloseFile();

            if(!bResult) System.Diagnostics.Process.Start(strUniveralLogViewerPath, strLogDir);
           
            return bResult;
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == String.Empty) return;

            lgLog = new Log(System.Environment.GetEnvironmentVariable("RS_TOOLSROOT") + "/logs/cutscene/process/lm");

            Process(textBox1.Text);
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "concat list files (*.concatlist)|*.concatlist";
            dlg.InitialDirectory = Environment.GetEnvironmentVariable("RS_PROJROOT") + @"\assets\cuts\!!Cutlists";

            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textBox1.Text = dlg.FileName;
            }
        }
    }
}
