﻿using System;
using System.Windows.Input;

namespace UniversalLogViewer
{
    /// <summary>
    /// A simple delegate relay command to bind commands in the UI.
    /// </summary>
    public class SimpleCommand : ICommand
    {
        #region Fields
        /// <summary>
        /// A static instance of a empty command that doesn't do anything.
        /// </summary>
        public static SimpleCommand EmptyCommand;

        /// <summary>
        /// The action delegate that is called when the user executes this command.
        /// </summary>
        private readonly Action execute;

        /// <summary>
        /// The action delegate that is called when the user executes this command.
        /// </summary>
        private readonly Action<object> executeWithCommand;

        /// <summary>
        /// The predicate delegate that is called when the command manger is determining
        /// whether or not this command can be executed by the user.
        /// </summary>
        private readonly Predicate<object> predicate;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="UniversalLogViewer.SimpleCommand"/>
        /// class.
        /// </summary>
        static SimpleCommand()
        {
            EmptyCommand = new SimpleCommand(null, (param) => { return false; });
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="UniversalLogViewer.SimpleCommand"/>
        /// class with the specified execute delegate.
        /// </summary>
        /// <param name="execute">
        /// The action delegate that is called when the command is executed by the user.
        /// </param>
        public SimpleCommand(Action execute)
        {
            this.execute = execute;
            this.predicate = null;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="UniversalLogViewer.SimpleCommand"/>
        /// class with the specified execute delegate.
        /// </summary>
        /// <param name="execute">
        /// The action delegate that is called when the command is executed by the user.
        /// </param>
        public SimpleCommand(Action<object> execute)
        {
            this.executeWithCommand = execute;
            this.predicate = null;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="UniversalLogViewer.SimpleCommand"/>
        /// class with the specified execute delegate.
        /// </summary>
        /// <param name="execute">
        /// The action delegate that is called when the command is executed by the user.
        /// </param>
        /// <param name="predicate">
        /// The predicate delegate that is called to determine if this command can be executed
        /// by the user.
        /// </param>
        public SimpleCommand(Action execute, Predicate<object> predicate)
        {
            this.execute = execute;
            this.predicate = predicate;
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs when changes occur that affect whether or not the command should execute.
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Called when the command is determining whether or not it can be executed by the
        /// user.
        /// </summary>
        /// <param name="parameter">
        /// The parameter passed in from the view.
        /// </param>
        /// <returns>
        /// True if the this command can be executed; otherwise, false.
        /// </returns>
        public bool CanExecute(object parameter)
        {
            if (this.predicate == null)
                return true;

            return this.predicate(parameter);
        }

        /// <summary>
        /// Called when the command has been executed by the user.
        /// </summary>
        /// <param name="parameter">
        /// The parameter passed in from the view.
        /// </param>
        public void Execute(object parameter)
        {
            if (this.execute != null)
                this.execute();
            else if (this.executeWithCommand != null)
                this.executeWithCommand(parameter);
        }
        #endregion
    }
}
