﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;

using RSG.UniversalLog.ViewModel;

namespace UniversalLogViewer
{
    enum ProfileGraphViewSelection
    {
        VIEW_DEFAULT,
        VIEW_CONGLOMERATED
    }

    class ProfileGraphWindowViewModel : INotifyPropertyChanged
    {
        #region Fields  

        /// <summary>
        /// Private field for the <see cref="Filenames"/> property.
        /// </summary>
        private List<string> filenames;

        /// <summary>
        /// Private field for the <see cref="WindowTop"/> property.
        /// </summary>
        private double windowTop;

        /// <summary>
        /// Private field for the <see cref="WindowLeft"/> property.
        /// </summary>
        private double windowLeft;

        /// <summary>
        /// Private field for the <see cref="WindowWidth"/> property.
        /// </summary>
        private double windowWidth;

        /// <summary>
        /// Private field for the <see cref="WindowHeight"/> property.
        /// </summary>
        private double windowHeight;

        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the collection of universal log component view models that the
        /// view is currently bound to.
        /// </summary>
        public List<UniversalLogComponentViewModel> Components
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the single filename based on the files that is currently loaded by the viewer.
        /// </summary>
        public string Filename
        {
            get
            {
                if (this.Filenames == null || this.Filenames.Count == 0)
                    return null;

                if (this.Filenames.Count == 1)
                    return " - " + this.Filenames[0];

                return string.Format(" - {0} files loaded.", this.Filenames.Count);
            }
        }

        /// <summary>
        /// Gets or sets the filenames of the files that is currently loaded by the viewer.
        /// </summary>
        public List<string> Filenames
        {
            get { return this.filenames; }
            set { this.SetProperty(ref this.filenames, value, "Filename", "StatusText"); }
        }

        /// <summary>
        /// Gets or sets the main windows left position on the display.
        /// </summary>
        public double WindowLeft
        {
            get { return this.windowLeft; }
            set { this.SetProperty(ref this.windowLeft, value, "WindowLeft"); }
        }

        /// <summary>
        /// Gets or sets the main windows top position on the display.
        /// </summary>
        public double WindowTop
        {
            get { return this.windowTop; }
            set { this.SetProperty(ref this.windowTop, value, "WindowTop"); }
        }

        /// <summary>
        /// Gets or sets the main windows width in pixels.
        /// </summary>
        public double WindowWidth
        {
            get { return this.windowWidth; }
            set { this.SetProperty(ref this.windowWidth, value, "WindowWidth"); }
        }

        /// <summary>
        /// Gets or sets the main windows height in pixels.
        /// </summary>
        public double WindowHeight
        {
            get { return this.windowHeight; }
            set { this.SetProperty(ref this.windowHeight, value, "WindowHeight"); }
        }
        #endregion

        #region Constructor

        public ProfileGraphWindowViewModel(List<string> filenames, List<UniversalLogComponentViewModel> viewModels)
        {
            this.filenames = filenames;
            Components = viewModels;

            UniversalLogProfileGraphViewModel.GlobalViewModels = viewModels;  ///TODO: How do I usher model data back through to the viewmodel prior to/during its instantation?

            UniversalLogViewer.Properties.Settings settings = null;
            settings = UniversalLogViewer.Properties.Settings.Default;
            if (settings == null)
                return;

            this.windowLeft = settings.GraphWindowLeft;
            this.windowTop = settings.GraphWindowTop;
            this.windowWidth = settings.GraphWindowWidth;
            this.windowHeight = settings.GraphWindowHeight;
        }

        /// <summary>
        /// Destroys this instance of the
        /// <see cref="UniversalLogViewer.ProfileGraphWindowViewModel"/> class.
        /// </summary>
        ~ProfileGraphWindowViewModel()
        {
            
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Methods

        /// <summary>
        /// Sets the given field to the given value making sure the correct events are fired.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the property we wish to set.
        /// </typeparam>
        /// <param name="field">
        /// A reference to the field we wish to set.
        /// </param>
        /// <param name="value">
        /// The value we wish to set the field to.
        /// </param>
        /// <param name="names">
        /// A array of property names that are changed due to this one change.
        /// </param>
        private void SetProperty<T>(ref T field, T value, params string[] names)
        {
            if (EqualityComparer<T>.Default.Equals(field, value))
                return;

            field = value;
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler == null || names == null)
                return;

            foreach (string name in names)
                handler(this, new PropertyChangedEventArgs(name));
        }

        /// <summary>
        /// Event handler for this window on close.
        /// </summary>
        public void OnClosing()
        {
            UniversalLogViewer.Properties.Settings settings = null;
            settings = UniversalLogViewer.Properties.Settings.Default;
            if (settings == null)
                return;

            settings.GraphWindowLeft = this.windowLeft;
            settings.GraphWindowTop = this.windowTop;
            settings.GraphWindowWidth = this.windowWidth;
            settings.GraphWindowHeight = this.windowHeight;

            settings.Save();
        }

        #endregion
    }
}


