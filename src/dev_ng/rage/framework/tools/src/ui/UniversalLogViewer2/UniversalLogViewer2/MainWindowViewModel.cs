﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Microsoft.Win32;
using RSG.Base.Extensions;
using RSG.UniversalLog;
using RSG.UniversalLog.Controls;
using RSG.UniversalLog.ViewModel;
using RSG.UniversalLog.DefaultViewer;
using System.Globalization;
using RSG.Base.Logging.Universal;
using RSG.Base.Configuration;
using System.Windows.Media;
using System.Windows.Controls;

namespace UniversalLogViewer
{
    /// <summary>
    /// Provides the data context to the main window UI elements.
    /// </summary>
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        #region Constants
        private static readonly String URL_HELP = 
            "https://devstar.rockstargames.com/wiki/index.php/Universal_Log";
        #endregion // Constants

        #region Fields
        /// <summary>
        /// A generic object used to control the creation of commands from the UI.
        /// </summary>
        private static object sync;

        /// <summary>
        /// The private field for the <see cref="RefreshCommand"/> property.
        /// </summary>
        private SimpleCommand refreshCommand;

        /// <summary>
        /// The private field for the <see cref="OpenCommand"/> property.
        /// </summary>
        private SimpleCommand openCommand;

        /// <summary>
        /// The private field for the <see cref="OpenMergeCommand"/> property.
        /// </summary>
        private SimpleCommand openMergeCommand;

        /// <summary>
        /// The private field for the <see cref="OpenRecentCommand"/> property.
        /// </summary>
        private SimpleCommand openRecentCommand;

        /// <summary>
        /// The private field for the <see cref="ShowProfileGridCommand"/> property.
        /// </summary>
        private SimpleCommand showProfileGraphCommand;

        /// <summary>
        /// The private field for the <see cref="ExitCommand"/> property.
        /// </summary>
        private SimpleCommand exitCommand;

        /// <summary>
        /// The private field for the <see cref="EmailCommand"/> property.
        /// </summary>
        private SimpleCommand emailCommand;

        /// <summary>
        /// The private field for the <see cref="FindCommand"/> property.
        /// </summary>
        private SimpleCommand findCommand;

        /// <summary>
        /// The private field for the <see cref="FindNextCommand"/> property.
        /// </summary>
        private SimpleCommand findNextCommand;

        /// <summary>
        /// The private field for the <see cref="FindPreviousCommand"/> property.
        /// </summary>
        private SimpleCommand findPreviousCommand;

        /// <summary>
        /// The private field for the <see cref="CopyCommand"/> property.
        /// </summary>
        private SimpleCommand copyCommand;

        /// <summary>
        /// The private field for <see cref="HelpCommand"/> property.
        /// </summary>
        private SimpleCommand helpCommand;

        /// <summary>
        /// Private field for the <see cref="Filenames"/> property.
        /// </summary>
        private List<string> filenames;

        /// <summary>
        /// Private field for the <see cref="StatusText"/> property,
        /// </summary>
        private string statusText;

        /// <summary>
        /// Private field for the <see cref="ShowGrid"/> property.
        /// </summary>
        private bool showGrid;

        /// <summary>
        /// Private field for the <see cref="WordWrap"/> property.
        /// </summary>
        private bool wordWrap;

        /// <summary>
        /// Private field for the <see cref="ShowErrors"/> property.
        /// </summary>
        private bool showErrors;

        /// <summary>
        /// Private field for the <see cref="ShowWarnings"/> property.
        /// </summary>
        private bool showWarnings;

        /// <summary>
        /// Private field for the <see cref="ShowMessages"/> property.
        /// </summary>
        private bool showMessages;

        /// <summary>
        /// Private field for the <see cref="ShowDebugMessages"/> property.
        /// </summary>
        private bool showDebugMessages;

        /// <summary>
        /// Private field for the <see cref="FindBoxHasFocus"/> property.
        /// </summary>
        private bool findBoxHasFocus;

        /// <summary>
        /// Private field for the <see cref="SearchText"/> property.
        /// </summary>
        private string searchText;

        /// <summary>
        /// Private field for the <see cref="NoSearchResults"/> property.
        /// </summary>
        private bool noSearchResults;

        /// <summary>
        /// Private field for the <see cref="UseRegularExpression"/> property.
        /// </summary>
        private bool useRegularExpression;

        /// <summary>
        /// Private field for the <see cref="MatchWholeWords"/> property.
        /// </summary>
        private bool matchWholeWords;

        /// <summary>
        /// Private field for the <see cref="MatchCase"/> property.
        /// </summary>
        private bool matchCase;

        /// <summary>
        /// Private field used for the <see cref="OnlyShowMatching"/> property.
        /// </summary>
        private bool onlyShowMatching;

        /// <summary>
        /// Private field for the <see cref="MatchCase"/> property.
        /// </summary>
        private List<UniversalLogComponentViewModel> allViewModels;

        /// <summary>
        /// A valid indicting whether the opened file has been sent in a email to the
        /// address group.
        /// </summary>
        private bool emailSent;

        /// <summary>
        /// A dispatch timer used to make sure we are not sorting/filtering as the user
        /// is typing.
        /// </summary>
        private DispatcherTimer timer;

        /// <summary>
        /// Private field for the <see cref="SelectedText"/> property.
        /// </summary>
        private string selectedText;

        /// <summary>
        /// Private field for the <see cref="WindowTop"/> property.
        /// </summary>
        private double windowTop;

        /// <summary>
        /// Private field for the <see cref="WindowLeft"/> property.
        /// </summary>
        private double windowLeft;

        /// <summary>
        /// Private field for the <see cref="WindowWidth"/> property.
        /// </summary>
        private double windowWidth;

        /// <summary>
        /// Private field for the <see cref="WindowHeight"/> property.
        /// </summary>
        private double windowHeight;

        /// <summary>
        /// Private field for controlling the graph window.
        /// </summary>
        private ProfileGraphWindow graphWindow;

        /// <summary>
        /// The private field used for the <see cref="UseTimeout"/> property.
        /// </summary>
        private bool useTimeout;

        /// <summary>
        /// The private field used for the <see cref="TimeRemainding"/> property.
        /// </summary>
        private int timeRemainding;

        private RSG.UniversalLog.Controls.UniversalLogControl logControl;
        #endregion

        #region Constructor
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="UniversalLogViewer.MainWindowViewModel"/> class.
        /// </summary>
        static MainWindowViewModel()
        {
            sync = new object();
        }

        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="UniversalLogViewer.MainWindowViewModel"/> class.
        /// </summary>
        public MainWindowViewModel(RSG.UniversalLog.Controls.UniversalLogControl logControl)
        {
            this.logControl = logControl;
            this.Filenames = new List<string>();
            this.RecentFiles = new ObservableCollection<string>();
            this.Components = new ObservableCollection<UniversalLogComponentViewModel>();
            this.allViewModels = new List<UniversalLogComponentViewModel>();
            this.graphWindow = null;

            UniversalLogViewer.Properties.Settings settings = null;
            settings = UniversalLogViewer.Properties.Settings.Default;
            if (settings == null)
                return;

            this.showGrid = true;
            this.wordWrap = settings.WordWrap;

            this.showErrors = settings.ShowErrors;
            this.showWarnings = settings.ShowWarnings;
            this.showMessages = settings.ShowMessages;
            this.showDebugMessages = settings.ShowDebugMessages;

            this.useRegularExpression = settings.UseRegularExpression;
            this.matchWholeWords = settings.MatchWholeWords;
            this.matchCase = settings.MatchCase;
            this.onlyShowMatching = settings.OnlyShowMatching;

            this.windowLeft = settings.WindowLeft;
            this.windowTop = settings.WindowTop;
            this.windowWidth = settings.WindowWidth;
            this.windowHeight = settings.WindowHeight;

            if (settings.RecentFiles != null)
            {
                foreach (string recentFile in settings.RecentFiles)
                    this.RecentFiles.Add(recentFile);
            }

            String[] args = (Application.Current as App).CommandlineArguments;
            if (args == null || args.Length == 0)
                return;

            List<string> loadArguments = new List<string>();
            foreach (string arg in args)
            {
                if (arg.StartsWith("--timeout"))
                {
                    string timeoutString = arg.Replace("--timeout=", "");
                    this.useTimeout = int.TryParse(timeoutString, out this.timeRemainding);
                }
                else
                {
                    loadArguments.Add(arg);
                }
            }

            ICollection<String> filenames = new List<String>();
            foreach (String pathname in loadArguments)
            {
                if (String.IsNullOrEmpty(pathname))
                    continue;

                if (Directory.Exists(pathname))
                {
                    filenames.AddRange(Directory.GetFiles(pathname, "*.ulog"));
                }
                else if (File.Exists(pathname))
                {
                    filenames.Add(pathname);
                }
            }

            this.OpenFiles(filenames, true);
            if (this.useTimeout)
            {
                DispatcherTimer timer = new DispatcherTimer(
                    DispatcherPriority.Background, Application.Current.MainWindow.Dispatcher);

                timer.Interval = TimeSpan.FromMilliseconds(10);
                timer.Tick += this.AutoCloseTimerTick;
                timer.Start();
            }
        }

        /// <summary>
        /// Destroys this instance of the
        /// <see cref="UniversalLogViewer.MainWindowViewModel"/> class.
        /// </summary>
        ~MainWindowViewModel()
        {
            UniversalLogViewer.Properties.Settings settings = null;
            settings = UniversalLogViewer.Properties.Settings.Default;
            if (settings == null)
                return;

            settings.ShowGrid = true;
            settings.WordWrap = this.wordWrap;

            settings.ShowErrors = this.showErrors;
            settings.ShowWarnings = this.showWarnings;
            settings.ShowMessages = this.showMessages;
            settings.ShowDebugMessages = this.showDebugMessages;

            settings.UseRegularExpression = this.useRegularExpression;
            settings.MatchWholeWords = this.matchWholeWords;
            settings.MatchCase = this.matchCase;
            settings.OnlyShowMatching = this.onlyShowMatching;

            settings.WindowLeft = this.windowLeft;
            settings.WindowTop = this.windowTop;
            settings.WindowWidth = this.windowWidth;
            settings.WindowHeight = this.windowHeight;

            settings.RecentFiles = new StringCollection();
            foreach (string recentFile in this.RecentFiles)
                settings.RecentFiles.Add(recentFile);

            settings.Save();
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Occurs whenever the search text changes. 
        /// </summary>
        public event EventHandler SearchTextChanged;
        #endregion

        #region Properties
        /// <summary>
        /// Gets the command used to refresh the contents of the loaded universal log file.
        /// </summary>
        public SimpleCommand RefreshCommand
        {
            get
            {
                lock (sync)
                {
                    if (this.refreshCommand == null)
                    {
                        this.refreshCommand = new SimpleCommand(this.Refresh, this.CanRefresh);
                    }
                }
                return this.refreshCommand;
            }
        }

        /// <summary>
        /// Gets the command used to open a universal log file from windows explorer.
        /// </summary>
        public SimpleCommand OpenCommand
        {
            get
            {
                lock (sync)
                {
                    if (this.openCommand == null)
                    {
                        this.openCommand = new SimpleCommand(this.Open);
                    }
                }
                return this.openCommand;
            }
        }

        /// <summary>
        /// Gets the command used to open a universal log file from windows explorer and merge
        /// in into the existing data.
        /// </summary>
        public SimpleCommand OpenMergeCommand
        {
            get
            {
                lock (sync)
                {
                    if (this.openMergeCommand == null)
                    {
                        this.openMergeCommand = new SimpleCommand(this.OpenMerge);
                    }
                }
                return this.openMergeCommand;
            }
        }

        /// <summary>
        /// Gets the command used to open a universal log file from the recent list.
        /// </summary>
        public SimpleCommand OpenRecentCommand
        {
            get
            {
                lock (sync)
                {
                    if (this.openRecentCommand == null)
                    {
                        this.openRecentCommand = new SimpleCommand(this.OpenRecent);
                    }
                }
                return this.openRecentCommand;
            }
        }

        /// <summary>
        /// Gets the command used to open a universal log file from windows explorer and merge
        /// in into the existing data.
        /// </summary>
        public SimpleCommand ShowProfileGraphCommand
        {
            get
            {
                lock (sync)
                {
                    if (this.showProfileGraphCommand == null)
                    {
                        this.showProfileGraphCommand = new SimpleCommand(this.ShowProfileGraph);
                    }
                }
                return this.showProfileGraphCommand;
            }
        }

        /// <summary>
        /// Gets the command used to exit this application.
        /// </summary>
        public SimpleCommand ExitCommand
        {
            get
            {
                lock (sync)
                {
                    if (this.exitCommand == null)
                    {
                        this.exitCommand = new SimpleCommand(this.Exit);
                    }
                }
                return this.exitCommand;
            }
        }

        /// <summary>
        /// Gets the command used to send a email with the contents of the file as
        /// an attachment.
        /// </summary>
        public SimpleCommand EmailCommand
        {
            get
            {
                lock (sync)
                {
                    if (this.emailCommand == null)
                    {
                        this.emailCommand = new SimpleCommand(this.Email, this.CanEmail);
                    }
                }
                return this.emailCommand;
            }
        }

        /// <summary>
        /// Gets the command used to send a email with the contents of the file as
        /// an attachment.
        /// </summary>
        public SimpleCommand FindCommand
        {
            get
            {
                lock (sync)
                {
                    if (this.findCommand == null)
                    {
                        this.findCommand = new SimpleCommand(this.Find);
                    }
                }
                return this.findCommand;
            }
        }

        /// <summary>
        /// Gets the command used to send a email with the contents of the file as
        /// an attachment.
        /// </summary>
        public SimpleCommand FindNextCommand
        {
            get
            {
                lock (sync)
                {
                    if (this.findNextCommand == null)
                    {
                        this.findNextCommand = new SimpleCommand(this.FindNext, this.CanFindNext);
                    }
                }
                return this.findNextCommand;
            }
        }

        /// <summary>
        /// Gets the command used to send a email with the contents of the file as
        /// an attachment.
        /// </summary>
        public SimpleCommand FindPreviousCommand
        {
            get
            {
                lock (sync)
                {
                    if (this.findPreviousCommand == null)
                    {
                        this.findPreviousCommand = new SimpleCommand(this.FindPrevious, this.CanFindPrevious);
                    }
                }
                return this.findPreviousCommand;
            }
        }

        /// <summary>
        /// Gets the command used to copy the selected text to the clipboard.
        /// </summary>
        public SimpleCommand CopyCommand
        {
            get
            {
                lock (sync)
                {
                    if (this.copyCommand == null)
                    {
                        this.copyCommand = new SimpleCommand(this.Copy);
                    }
                }
                return this.copyCommand;
            }
        }

        /// <summary>
        /// Gets the command used to display Devstar help.
        /// </summary>
        public SimpleCommand HelpCommand
        {
            get
            {
                lock (sync)
                {
                    if (this.helpCommand == null)
                    {
                        this.helpCommand = new SimpleCommand(this.Help);
                    }
                }
                return (this.helpCommand);
            }
        }

        /// <summary>
        /// Gets or sets the list of recently opened file paths.
        /// </summary>
        public ObservableCollection<string> RecentFiles
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the collection of universal log component view models that the
        /// view is currently bound to.
        /// </summary>
        public ObservableCollection<UniversalLogComponentViewModel> Components
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the single filename based on the files that is currently loaded by the viewer.
        /// </summary>
        public string Filename
        {
            get
            {
                if (this.Filenames == null || this.Filenames.Count == 0)
                    return null;

                if (this.Filenames.Count == 1)
                    return " - " + this.Filenames[0];

                return string.Format(" - {0} files loaded.", this.Filenames.Count);
            }
        }

        /// <summary>
        /// Gets or sets the filenames of the files that is currently loaded by the viewer.
        /// </summary>
        public List<string> Filenames
        {
            get { return this.filenames; }
            set { this.SetProperty(ref this.filenames, value, "Filename", "StatusText"); }
        }

        /// <summary>
        /// Gets the text to show to the user on the status bar.
        /// </summary>
        public string StatusText
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.statusText))
                {
                    if (this.Filenames == null || this.Filenames.Count == 0)
                        return @"Ready";

                    if (this.Filenames.Count == 1)
                        return this.Filenames[0];

                    return string.Format("{0} loaded files", this.Filenames.Count);
                }
                else
                {
                    return this.statusText;
                }
            }

            set
            {
                this.SetProperty(ref this.statusText, value, "StatusText", "Filename");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the user is currently viewing the
        /// universal log messages in a grid or a string block.
        /// </summary>
        public bool ShowGrid
        {
            get { return this.showGrid; }
            set
            {
                string[] changedProperties = new string[]
                {
                    "ShowGrid", 
                    "GridVisibility",
                    "TextBoxVisibility",
                };
                this.SetProperty(ref this.showGrid, value, changedProperties);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the user is currently viewing the
        /// universal log messages in a grid or a string block.
        /// </summary>
        public Visibility GridVisibility
        {
            get { return this.ShowGrid == true ? Visibility.Visible : Visibility.Collapsed; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the user is currently viewing the
        /// universal log messages in a grid or a string block.
        /// </summary>
        public Visibility TextBoxVisibility
        {
            get { return this.ShowGrid == true ? Visibility.Collapsed : Visibility.Visible; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the user is currently viewing the 
        /// universal log messages with word wrap enabled or not.
        /// </summary>
        public bool WordWrap
        {
            get { return this.wordWrap; }
            set { this.SetProperty(ref this.wordWrap, value, "WordWrap"); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the user should be able to see the error
        /// messages or not.
        /// </summary>
        public bool ShowErrors
        {
            get
            {
                return this.showErrors;
            }

            set 
            { 
                this.SetProperty(ref this.showErrors, value, "ShowErrors");
                this.SortAndFilterComponents();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the user should be able to see the warning
        /// messages or not.
        /// </summary>
        public bool ShowWarnings
        {
            get
            {
                return this.showWarnings;
            }

            set
            {
                this.SetProperty(ref this.showWarnings, value, "ShowWarnings");
                this.SortAndFilterComponents();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the user should be able to see the normal
        /// messages or not.
        /// </summary>
        public bool ShowMessages
        {
            get
            {
                return this.showMessages;
            }

            set
            {
                this.SetProperty(ref this.showMessages, value, "ShowMessages");
                this.SortAndFilterComponents();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the user should be able to see the debug
        /// messages or not.
        /// </summary>
        public bool ShowDebugMessages
        {
            get
            {
                return this.showDebugMessages;
            }

            set
            {
                this.SetProperty(ref this.showDebugMessages, value, "ShowDebugMessages");
                this.SortAndFilterComponents();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the search box currently has
        /// keyboard focus.
        /// </summary>
        public bool FindBoxHasFocus
        {
            get { return this.findBoxHasFocus; }
            set { this.SetProperty(ref this.findBoxHasFocus, value, "FindBoxHasFocus"); }
        }

        /// <summary>
        /// Gets or sets the text currently in the search box.
        /// </summary>
        public string SearchText
        {
            get
            {
                return this.searchText;
            }

            set
            {
                this.SetProperty(ref this.searchText, value, "SearchText");
                if (this.timer != null)
                {
                    this.timer.Stop();
                    this.timer.Tick -= OnTimerElasped;
                    this.timer = null;
                }

                this.timer = new DispatcherTimer(DispatcherPriority.Normal, Application.Current.Dispatcher);
                this.timer.Interval = TimeSpan.FromMilliseconds(500.0);
                this.timer.IsEnabled = true;
                this.timer.Tick += new EventHandler(OnTimerElasped);
                this.timer.Start();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating that the currently search text has found
        /// no entries.
        /// </summary>
        public bool NoSearchResults
        {
            get { return this.noSearchResults; }
            set { this.SetProperty(ref this.noSearchResults, value, "NoSearchResults"); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the use of the search text should include
        /// regular expressions.
        /// </summary>
        public bool UseRegularExpression
        {
            get
            {
                return this.useRegularExpression;
            }

            set
            {
                this.SetProperty(ref this.useRegularExpression, value, "UseRegularExpression");
                SortAndFilterComponents();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the use of the search text should match
        /// whole words or not.
        /// </summary>
        public bool MatchWholeWords
        {
            get
            {
                return this.matchWholeWords;
            }

            set
            {
                this.SetProperty(ref this.matchWholeWords, value, "MatchWholeWords");
                SortAndFilterComponents();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the use of the search text should match
        /// the case or not.
        /// </summary>
        public bool MatchCase
        {
            get
            {
                return this.matchCase;
            }
            
            set
            {
                this.SetProperty(ref this.matchCase, value, "MatchCase");
                SortAndFilterComponents();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the results from the search box should be
        /// the only messages shown to the user.
        /// </summary>
        public bool OnlyShowMatching
        {
            get
            {
                return this.onlyShowMatching;
            }

            set
            {
                this.SetProperty(ref this.onlyShowMatching, value, "OnlyShowMatching");
                SortAndFilterComponents();
            }
        }

        /// <summary>
        /// Gets or sets the text that is currently selected in the view.
        /// </summary>
        public string SelectedText
        {
            get { return this.selectedText; }
            set { this.selectedText = value; }
        }

        /// <summary>
        /// Gets or sets the main windows left position on the display.
        /// </summary>
        public double WindowLeft
        {
            get { return this.windowLeft; } 
            set { this.SetProperty(ref this.windowLeft, value, "WindowLeft"); }
        }

        /// <summary>
        /// Gets or sets the main windows top position on the display.
        /// </summary>
        public double WindowTop
        {
            get { return this.windowTop; }
            set { this.SetProperty(ref this.windowTop, value, "WindowTop"); }
        }

        /// <summary>
        /// Gets or sets the main windows width in pixels.
        /// </summary>
        public double WindowWidth
        {
            get { return this.windowWidth; }
            set { this.SetProperty(ref this.windowWidth, value, "WindowWidth"); }
        }

        /// <summary>
        /// Gets or sets the main windows height in pixels.
        /// </summary>
        public double WindowHeight
        {
            get { return this.windowHeight; }
            set { this.SetProperty(ref this.windowHeight, value, "WindowHeight"); }
        }

        /// <summary>
        /// Gets a value indicating whether the window is in timeout mode and will close after
        /// a certain number of milliseconds.
        /// </summary>
        public bool UseTimeout
        {
            get { return this.useTimeout; }
            private set { this.SetProperty(ref this.useTimeout, value, "WindowHeight"); }
        }

        /// <summary>
        /// Gets the amount of time remainding before the window automatically closes.
        /// </summary>
        public int TimeRemainding
        {
            get { return this.timeRemainding; }
            private set { this.SetProperty(ref this.timeRemainding, value, "TimeRemainding"); }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Gets called when the search text has changed but not for the previous 500
        /// milliseconds.
        /// </summary>
        /// <param name="sender">
        /// The object this event is attached to.
        /// </param>
        /// <param name="e">
        /// The System.EventArgs data for this event.
        /// </param>
        private void OnTimerElasped(object sender, EventArgs e)
        {
            this.timer.Stop();

            SortAndFilterComponents();
            if (this.SearchTextChanged != null)
                this.SearchTextChanged(this, EventArgs.Empty);
        }

        /// <summary>
        /// Sets the given field to the given value making sure the correct events are fired.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the property we wish to set.
        /// </typeparam>
        /// <param name="field">
        /// A reference to the field we wish to set.
        /// </param>
        /// <param name="value">
        /// The value we wish to set the field to.
        /// </param>
        /// <param name="names">
        /// A array of property names that are changed due to this one change.
        /// </param>
        private void SetProperty<T>(ref T field, T value, params string[] names)
        {
            if (EqualityComparer<T>.Default.Equals(field, value))
                return;

            field = value;
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler == null || names == null)
                return;

            foreach (string name in names)
                handler(this, new PropertyChangedEventArgs(name));
        }

        /// <summary>
        /// Determines whether the refresh command can be executed by the user.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter passed in by the view.
        /// </param>
        /// <returns>
        /// True if the command can be executed; otherwise, false.
        /// </returns>
        private bool CanRefresh(object parameter)
        {
            if (this.Filenames == null || this.Filenames.Count == 0)
                return false;

            return true;
        }

        /// <summary>
        /// Refreshes the currently opened file by reloading the universal log model.
        /// </summary>
        private void Refresh()
        {
            if (this.Filenames == null || this.Filenames.Count == 0)
                return;

            UniversalLogFileListener ulog = new UniversalLogFileListener(this.Filenames);
            CreateComponents(ulog.Buffer, true);
            if (ulog.HasErrors)
            {
                CreateComponents(ulog.ParseErrors, false);
            }
        }

        /// <summary>
        /// Gets called when the open file command gets executed by the user.
        /// </summary>
        private void Open()
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Universal Log Files (*.ulog)|*.ulog";
            dlg.Title = "Open Universal Log";
            dlg.Multiselect = true;
            bool? openDialogResult = dlg.ShowDialog();

            if (openDialogResult != true || dlg.FileNames == null || dlg.FileNames.Length == 0)
                return;

            bool refresh = true;
            foreach (string filename in dlg.FileNames)
            {
                if (!this.Filenames.Contains(filename))
                {
                    refresh = false;
                    break;
                }
            }

            if (refresh && dlg.FileNames.Length == this.Filenames.Count)
            {
                this.Refresh();
            }
            else
            {
                this.OpenFiles(dlg.FileNames, true);
            }
        }

        /// <summary>
        /// Gets called when the open merge command gets executed by the user.
        /// </summary>
        private void OpenMerge()
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Universal Log Files (*.ulog)|*.ulog";
            dlg.Title = "Open Universal Log";
            dlg.Multiselect = true;
            bool? openDialogResult = dlg.ShowDialog();

            if (openDialogResult != true || dlg.FileNames == null || dlg.FileNames.Length == 0)
                return;

            bool refresh = true;
            List<string> validFiles = new List<string>();
            foreach (string filename in dlg.FileNames)
            {
                if (!this.Filenames.Contains(filename))
                {
                    refresh = false;
                    validFiles.Add(filename);
                }
            }

            if (refresh)
            {
                this.Refresh();
            }
            else
            {
                this.OpenFiles(validFiles, false);
            }
        }

        /// <summary>
        /// Gets called when the open recent file command gets executed by the user.
        /// </summary>
        private void OpenRecent(object parameter)
        {
            string filename = parameter as string;
            if (filename == null || !File.Exists(filename))
            {
                string msg = string.Format("Unable to locate the recent file '{0}'. Would you like to remove it from the list?", filename);
                MessageBoxImage image = MessageBoxImage.Warning;
                MessageBoxButton buttons = MessageBoxButton.YesNo;
                MessageBoxResult result = MessageBox.Show(msg, "Missing File", buttons, image);
                if (result == MessageBoxResult.Yes)
                    this.RecentFiles.Remove(filename);

                return;
            }
            
            this.OpenFiles(Enumerable.Repeat(filename, 1), true);
        }

        /// <summary>
        /// Opens the given universal log filename.
        /// </summary>
        /// <param name="filename">
        /// The filename to the universal log to open.
        /// </param>
        private void OpenFiles(IEnumerable<string> filenames, bool clearCurrent)
        {
            List<string> validFilenames = new List<string>();
            foreach (string filename in filenames)
            {
                if (File.Exists(filename))
                {
                    string extension = Path.GetExtension(filename);
                    //if (string.Compare(extension, ".ulog", true) == 0) // MPW : Remove this check so we can load ulog format files with multiple extensions
                    {
                        validFilenames.Add(filename);
                    }
                }
            }

            foreach (string filename in validFilenames)
            {
                this.RecentFiles.Remove(filename);
                this.RecentFiles.Insert(0, filename);
            }

            while (this.RecentFiles.Count > 5)
            {
                this.RecentFiles.RemoveAt(this.RecentFiles.Count - 1);
            }

            UniversalLogFileListener ulog = new UniversalLogFileListener(validFilenames);
            if (clearCurrent)
            {
                this.Filenames = validFilenames.ToList();
            }
            else
            {
                this.Filenames.AddRange(validFilenames);
            }

            this.emailSent = false;
            CreateComponents(ulog.Buffer, clearCurrent);
            if (ulog.HasErrors)
            {
                CreateComponents(ulog.ParseErrors, false);
            }
        }


        /// <summary>
        /// Gets called when the open recent file command gets executed by the user.
        /// </summary>
        private void ShowProfileGraph(object parameter)
        {
            if (graphWindow == null)
            {
                graphWindow = new ProfileGraphWindow(this.filenames, this.allViewModels);
                graphWindow.Closed += new EventHandler(OnProfileGraphClosed);
                graphWindow.Show();
            }
            else
            {
                graphWindow.Focus();
            }
        }

        /// <summary>
        /// Handles closing event for 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnProfileGraphClosed(object sender, EventArgs e)
        {
            graphWindow = null;
        }

        /// <summary>
        /// Creates the component view models from the universal log model.
        /// </summary>
        /// <param name="ulog"></param>
        private void CreateComponents(IEnumerable<InMemoryUniversalLogTarget.BufferedMessage> buffer, bool clearCurrent)
        {
            Cursor cursor = Application.Current.MainWindow.Cursor;
            Application.Current.MainWindow.Cursor = Cursors.Wait;
            try
            {
                if (clearCurrent)
                {
                    this.Components.Clear();
                    this.allViewModels.Clear();
                }

                List<UniversalLogComponentViewModel> viewModels = new List<UniversalLogComponentViewModel>();
                foreach (InMemoryUniversalLogTarget.BufferedMessage message in buffer)
                {
                    UniversalLogComponentViewModel vm = UniversalLogComponentViewModel.Create(message);
                    if (vm != null)
                    {
                        viewModels.Add(vm);
                    }
                }
                SortedList<DateTime, List<UniversalLogComponentViewModel>> sorted = new SortedList<DateTime,List<UniversalLogComponentViewModel>>();
                foreach (var vm in this.allViewModels)
                {
                    DateTime time = vm.DateTime;
                    if (!sorted.ContainsKey(time))
                        sorted.Add(time, new List<UniversalLogComponentViewModel>());

                    sorted[time].Add(vm);
                }
                foreach (var vm in viewModels)
                {
                    DateTime time = vm.DateTime;
                    if (!sorted.ContainsKey(time))
                        sorted.Add(time, new List<UniversalLogComponentViewModel>());

                    sorted[time].Add(vm);
                }
                this.allViewModels.Clear();
                foreach (var kvp in sorted)
                {
                    foreach (var vm in kvp.Value)
                    {
                        this.allViewModels.Add(vm);
                    }
                }

                this.SortAndFilterComponents();

                int errors = 0;
                int warnings = 0;
                int other = 0;
                foreach (UniversalLogComponentViewModel vm in this.allViewModels)
                {
                    if (vm.ComponentType == UniversalLogComponentTypes.Error)
                    {
                        errors++;
                    }
                    else if (vm.ComponentType == UniversalLogComponentTypes.Warning)
                    {
                       warnings++; 
                    }
                    else
                    {
                        other++;
                    }
                }

                string statusText = @"Errors {0}, Warnings {1}, Others {2}";
                this.StatusText = string.Format(statusText, errors, warnings, other);
            }
            catch
            {

            }
            finally
            {
                Application.Current.MainWindow.Cursor = cursor;
            }
        }

        /// <summary>
        /// Gets called when the exit command gets executed by the user.
        /// </summary>
        private void Exit()
        {
            Application.Current.Shutdown();
        }

        /// <summary>
        /// Determines whether the email command can be executed by the user.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter passed in by the view.
        /// </param>
        /// <returns>
        /// True if the command can be executed; otherwise, false.
        /// </returns>
        private bool CanEmail(object parameter)
        {
            if (this.Filenames == null || this.Filenames.Count == 0)
                return false;

            return true;
        }

        /// <summary>
        /// Gets called when requests to email the report to a specific user.
        /// </summary>
        private void Email()
        {
            if (this.Filenames == null || this.Filenames.Count == 0)
                return;

            if (this.emailSent)
            {
                string msgText = @"You have already sent this universal log in an email to the"
                    + " designated recipients. Are you sure you wish to send it again?";
                MessageBoxResult result = MessageBox.Show(msgText, @"Sanity Check", MessageBoxButton.OKCancel, MessageBoxImage.Stop);
                if (result == MessageBoxResult.Cancel)
                {
                    return;
                }
            }

            AdditionalInformationWindow info = new AdditionalInformationWindow();
            info.Owner = Application.Current.MainWindow;
            bool? dialogResult = info.ShowDialog();
            if (dialogResult == false)
            {
                return;
            }

            string additionalInfo = info.AdditionalInformationText.Text;
            this.emailSent = true;
            IConfig config = ConfigFactory.CreateConfig();

            List<string> recipients = new List<string>();
            recipients.Add("*tools@rockstarnorth.com");

            string userEmail = config.EmailAddress;
            if (userEmail != null && !recipients.Contains(userEmail))
            {
                recipients.Add(userEmail);
            }

            if (recipients == null || recipients.Count == 0)
            {
                return;
            }

            string[] attachments = this.Filenames.ToArray();
            string subject = @"Universal Log Report from " + Environment.UserName;
            string body = "Additional Information:\n\n" + additionalInfo;

            RSG.UniversalLog.Util.Email.SendEmail(config, recipients.ToArray(), subject, body, attachments);
        }

        /// <summary>
        /// Gets called when requests to email the report to a specific user.
        /// </summary>
        private void Find()
        {
            this.FindBoxHasFocus = true;
        }

        /// <summary>
        /// Gets called when requests to email the report to a specific user.
        /// </summary>
        private bool FindNextWithResult()
        {
            Regex rgx1 = null;
            if (this.UseRegularExpression)
            {
                if (!this.MatchCase)
                {
                    rgx1 = new Regex(this.searchText, RegexOptions.IgnoreCase);
                }
                else
                {
                    rgx1 = new Regex(this.searchText);
                }
            }

            bool found = false;
            DataGrid grid = GetVisualDescendent<DataGrid>(this.logControl);

            int startIndex = grid.SelectedIndex;
            UniversalLogComponentViewModel vm = null;
            while (found == false)
            {
                for (int i = 0; i < this.Components.Count; i++)
                {
                    if (i <= startIndex)
                    {
                        continue;
                    }

                    vm = this.Components[i];
                    bool valid = true;
                    string text = string.Format(ParagraphExtension.msgFmt + "\n", vm.TimeStamp, vm.Type, vm.Context, vm.Message);
                    if (this.UseRegularExpression)
                    {
                        if (!rgx1.IsMatch(text))
                        {
                            valid = false;
                        }
                    }
                    else
                    {
                        string search = this.SearchText;
                        if (!this.MatchCase)
                        {
                            search = search.ToLower();
                            text = text.ToLower();
                        }

                        if (!this.MatchWholeWords)
                        {
                            if (!text.Contains(search))
                            {
                                valid = false;
                            }
                        }
                        else
                        {
                            Regex rgx = new Regex("[^a-zA-Z0-9 ]");
                            text = rgx.Replace(text, "");
                            if (!text.EndsWith(" "))
                                text += " ";
                            if (!text.StartsWith(" "))
                                text = " " + search;

                            if (!search.EndsWith(" "))
                                search += " ";
                            if (!search.StartsWith(" "))
                                search = " " + search;

                            if (!text.Contains(search))
                                valid = false;
                        }
                    }

                    if (valid)
                    {
                        found = true;
                        break;
                    }
                }

                if (found == false && startIndex == -1)
                {
                    vm = null;
                    break;
                }

                startIndex = -1;
            }

            if (vm != null)
            {
                // Jump
                grid.SelectedItem = vm;
                grid.ScrollIntoView(vm);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Gets called when requests to email the report to a specific user.
        /// </summary>
        private void FindNext()
        {
            this.FindNextWithResult();
        }

        /// <summary>
        /// Gets called when requests to email the report to a specific user.
        /// </summary>
        private bool CanFindNext(object parameter)
        {
            if (this.NoSearchResults || string.IsNullOrEmpty(this.searchText))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Gets called when requests to email the report to a specific user.
        /// </summary>
        private void FindPrevious()
        {
            Regex rgx1 = null;
            if (this.UseRegularExpression)
            {
                if (!this.MatchCase)
                {
                    rgx1 = new Regex(this.searchText, RegexOptions.IgnoreCase);
                }
                else
                {
                    rgx1 = new Regex(this.searchText);
                }
            }

            bool found = false;
            DataGrid grid = GetVisualDescendent<DataGrid>(this.logControl);

            int startIndex = grid.SelectedIndex - 1;
            if (startIndex < 0)
            {
                startIndex = this.Components.Count - 1;
            }

            UniversalLogComponentViewModel lastMatch = null;
            while (found == false)
            {
                for (int i = startIndex; i >= 0; i--)
                {
                    UniversalLogComponentViewModel vm = this.Components[i];
                    bool valid = true;
                    string text = string.Format(ParagraphExtension.msgFmt + "\n", vm.TimeStamp, vm.Type, vm.Context, vm.Message);
                    if (this.UseRegularExpression)
                    {
                        if (!rgx1.IsMatch(text))
                        {
                            valid = false;
                        }
                    }
                    else
                    {
                        string search = this.SearchText;
                        if (!this.MatchCase)
                        {
                            search = search.ToLower();
                            text = text.ToLower();
                        }

                        if (!this.MatchWholeWords)
                        {
                            if (!text.Contains(search))
                            {
                                valid = false;
                            }
                        }
                        else
                        {
                            Regex rgx = new Regex("[^a-zA-Z0-9 ]");
                            text = rgx.Replace(text, "");
                            if (!text.EndsWith(" "))
                                text += " ";
                            if (!text.StartsWith(" "))
                                text = " " + search;

                            if (!search.EndsWith(" "))
                                search += " ";
                            if (!search.StartsWith(" "))
                                search = " " + search;

                            if (!text.Contains(search))
                                valid = false;
                        }
                    }

                    if (valid)
                    {
                        found = true;
                        lastMatch = vm;
                        break;
                    }
                }

                if (found == false && startIndex == this.Components.Count - 1)
                {
                    lastMatch = null;
                    break;
                }

                startIndex = this.Components.Count - 1;
            }

            if (lastMatch != null)
            {
                // Jump
                grid.SelectedItem = lastMatch;
                grid.ScrollIntoView(lastMatch);
            }
        }

        /// <summary>
        /// Gets called when requests to email the report to a specific user.
        /// </summary>
        private bool CanFindPrevious(object parameter)
        {
            if (this.NoSearchResults || string.IsNullOrEmpty(this.searchText))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Gets called when copy command gets executed by the user.
        /// </summary>
        private void Copy()
        {
            Clipboard.SetText(this.SelectedText);
        }

        /// <summary>
        /// Gets called when help command gets executed by the user.
        /// </summary>
        private void Help()
        {
            System.Diagnostics.Process.Start(URL_HELP);
        }

        /// <summary>
        /// Uses the current search text to determine which messages can be shown.
        /// </summary>
        private void DetermineSearchResults()
        {
            this.NoSearchResults = false;
            if (string.IsNullOrEmpty(this.SearchText))
                return;

            this.NoSearchResults = true;
        }

        /// <summary>
        /// 
        /// </summary>
        private void SortAndFilterComponents()
        {
            Cursor cursor = Application.Current.MainWindow.Cursor;
            Application.Current.MainWindow.Cursor = Cursors.Wait;
            try
            {
                if (this.OnlyShowMatching)
                {
                    SortAndFilterComponentsWithNonMatchingHidden();
                }
                else
                {
                    SortAndFilterComponentsWithNonMatchingVisible();
                }
            }
            finally
            {
                Application.Current.MainWindow.Cursor = cursor;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        private void SortAndFilterComponentsWithNonMatchingHidden()
        {
            int validCount = 0;
            this.NoSearchResults = false;
            this.Components.Clear();
            bool vaildSearchText = !string.IsNullOrEmpty(this.searchText);
            HashSet<UniversalLogComponentViewModel> hashSet = new HashSet<UniversalLogComponentViewModel>();
            foreach (UniversalLogComponentViewModel vm in this.Components)
            {
                hashSet.Add(vm);
            }

            Regex rgx1 = null;
            if (vaildSearchText)
            {
                if (this.UseRegularExpression)
                {
                    if (!this.MatchCase)
                    {
                        rgx1 = new Regex(this.searchText, RegexOptions.IgnoreCase);
                    }
                    else
                    {
                        rgx1 = new Regex(this.searchText);
                    }
                }
            }

            for (int i = 0; i < this.allViewModels.Count; i++)
            {
                UniversalLogComponentViewModel vm = this.allViewModels[i];
                bool valid = true;
                if (vm.ComponentType == UniversalLogComponentTypes.Profiling)
                {
                    valid = false;
                }

                if (vm.ComponentType == UniversalLogComponentTypes.Error && this.ShowErrors == false)
                {
                    valid = false;
                }

                if (vm.ComponentType == UniversalLogComponentTypes.Warning && this.ShowWarnings == false)
                {
                    valid = false;
                }

                if (vm.ComponentType == UniversalLogComponentTypes.Message && this.ShowMessages == false)
                {
                    valid = false;
                }

                if (vm.ComponentType == UniversalLogComponentTypes.DebugMessage && this.ShowDebugMessages == false)
                {
                    valid = false;
                }

                if (valid && vaildSearchText)
                {
                    string text = string.Format(ParagraphExtension.msgFmt + "\n", vm.TimeStamp, vm.Type, vm.Context, vm.Message);
                    if (this.UseRegularExpression)
                    {
                        if (!rgx1.IsMatch(text))
                        {
                            valid = false;
                        }
                    }
                    else
                    {
                        string search = this.SearchText;
                        if (!this.MatchCase)
                        {
                            search = search.ToLower();
                            text = text.ToLower();
                        }

                        if (!this.MatchWholeWords)
                        {
                            if (!text.Contains(search))
                            {
                                valid = false;
                            }
                        }
                        else
                        {
                            Regex rgx = new Regex("[^a-zA-Z0-9 ]");
                            text = rgx.Replace(text, "");
                            if (!text.EndsWith(" "))
                                text += " ";
                            if (!text.StartsWith(" "))
                                text = " " + search;

                            if (!search.EndsWith(" "))
                                search += " ";
                            if (!search.StartsWith(" "))
                                search = " " + search;

                            if (!text.Contains(search))
                                valid = false;
                        }
                    }
                }

                if (valid && !hashSet.Contains(vm))
                {
                    hashSet.Add(vm);
                    this.Components.Add(vm);
                    validCount++;
                    continue;
                }

                if (!valid && hashSet.Contains(vm))
                {
                    this.Components.Remove(vm);
                    validCount++;
                    continue;
                }
            }

            if (!string.IsNullOrWhiteSpace(this.searchText) && (this.ShowErrors || this.ShowWarnings || this.ShowMessages || this.ShowDebugMessages))
            {
                this.NoSearchResults = this.Components.Count == 0;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void SortAndFilterComponentsWithNonMatchingVisible()
        {
            this.NoSearchResults = false;
            this.Components.Clear();
            bool vaildSearchText = !string.IsNullOrEmpty(this.searchText);
            Regex rgx1 = null;
            if (vaildSearchText)
            {
                if (this.UseRegularExpression)
                {
                    if (!this.MatchCase)
                    {
                        rgx1 = new Regex(this.searchText, RegexOptions.IgnoreCase);
                    }
                    else
                    {
                        rgx1 = new Regex(this.searchText);
                    }
                }
            }

            for (int i = 0; i < this.allViewModels.Count; i++)
            {
                UniversalLogComponentViewModel vm = this.allViewModels[i];
                if (vm.ComponentType == UniversalLogComponentTypes.Profiling)
                {
                    continue;
                }

                if (vm.ComponentType == UniversalLogComponentTypes.Error && this.ShowErrors == false)
                {
                    continue;
                }

                if (vm.ComponentType == UniversalLogComponentTypes.Warning && this.ShowWarnings == false)
                {
                    continue;
                }

                if (vm.ComponentType == UniversalLogComponentTypes.Message && this.ShowMessages == false)
                {
                    continue;
                }

                if (vm.ComponentType == UniversalLogComponentTypes.DebugMessage && this.ShowDebugMessages == false)
                {
                    continue;
                }

                this.Components.Add(vm);
            }

            bool found = this.FindNextWithResult();
            if (!string.IsNullOrWhiteSpace(this.searchText) && (this.ShowErrors || this.ShowWarnings || this.ShowMessages || this.ShowDebugMessages))
            {
                this.NoSearchResults = !found;
            }
        }

        /// <summary>
        /// Finds the first occurrence of the qualifying type by looking down through the
        /// visual tree.
        /// </summary>
        /// <typeparam name="T">
        /// The type of visual that you want to find  in the visual tree.
        /// </typeparam>
        /// <param name="d">
        /// The dependency object to use as the starting root visual for the search.
        /// </param>
        /// <returns>
        /// A instance to the first occurrence of the qualifying type.
        /// </returns>
        public static T GetVisualDescendent<T>(DependencyObject d)
            where T : DependencyObject
        {
            return GetVisualDescendents<T>(d).FirstOrDefault();
        }

        /// <summary>
        /// Creates an Enumerable that can be used to get all or loop through all instances of
        /// a specific type in the visual tree below this dependency object.
        /// </summary>
        /// <typeparam name="T">
        /// The type of visual that you want to find in the visual tree.
        /// </typeparam>
        /// <param name="d">
        /// The dependency object to use as the starting root visual for the search.
        /// </param>
        /// <returns>
        /// Returns an Enumerable that can be used to get all or loop through all instances of
        /// a specific type in the visual tree below this dependency object.
        /// </returns>
        public static IEnumerable<T> GetVisualDescendents<T>(DependencyObject d)
            where T : DependencyObject
        {
            if (d == null)
            {
                yield break;
            }

            int childCount = VisualTreeHelper.GetChildrenCount(d);
            for (int i = 0; i < childCount; i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(d, i);
                if (child is T)
                {
                    yield return (T)child;
                }

                foreach (T match in GetVisualDescendents<T>(child))
                {
                    yield return match;
                }
            }

            yield break;
        }

        /// <summary>
        /// Called when the auto close timer comes to the end
        /// </summary>
        /// <param name="sender">
        /// The timer this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.EventArgs data for this event.
        /// </param>
        private void AutoCloseTimerTick(object sender, EventArgs e)
        {
            if (Application.Current == null)
            {
                (sender as DispatcherTimer).Stop();
                return;
            }

            this.TimeRemainding -= 10;
            if (this.TimeRemainding <= 0)
            {
                Application.Current.MainWindow.Close();
            }
        }
        #endregion
    } // MainWindowViewModel
} // UniversalLogViewer
