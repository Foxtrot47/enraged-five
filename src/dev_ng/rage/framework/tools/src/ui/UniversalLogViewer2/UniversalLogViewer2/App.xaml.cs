﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Windows;

namespace UniversalLogViewer
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        #region Constants
        private const String AUTHOR = "RSGEDI Tools";
        private const String AUTHOR_EMAIL = "*tools@rockstarnorth.com";
        #endregion // Constants

        #region Fields
        /// <summary>
        /// The command-line arguments used to start this application.
        /// </summary>
        internal String[] CommandlineArguments;
        #endregion

        #region Methods
        /// <summary>
        /// Gets called on start-up before the main window is created.
        /// </summary>
        /// <param name="e">
        /// The start-up event data, including the command-line arguments.
        /// </param>
        protected override void OnStartup(StartupEventArgs e)
        {
            this.DispatcherUnhandledException += Application_DispatcherUnhandledException;
            AppDomain.CurrentDomain.UnhandledException += Application_AppDomainUnhandledException;

            this.CommandlineArguments = e.Args;
            base.OnStartup(e);
        } 
        #endregion // Methods


        #region Application Events
        /// <summary>
        /// Dispatcher unhandled exception event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Application_DispatcherUnhandledException(Object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            RSG.Base.Windows.ExceptionStackTraceDlg dlg =
                new RSG.Base.Windows.ExceptionStackTraceDlg(e.Exception, AUTHOR, AUTHOR_EMAIL);
            dlg.ShowDialog();
            e.Handled = true;
        }

        /// <summary>
        /// AppDomain unhandled exception event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Application_AppDomainUnhandledException(Object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = (e.ExceptionObject as Exception);
            RSG.Base.Windows.ExceptionStackTraceDlg dlg =
                new RSG.Base.Windows.ExceptionStackTraceDlg(ex, AUTHOR, AUTHOR_EMAIL);
            dlg.ShowDialog();
        }
        #endregion // Application Events
    }

} // UniversalLogViewer namespace
