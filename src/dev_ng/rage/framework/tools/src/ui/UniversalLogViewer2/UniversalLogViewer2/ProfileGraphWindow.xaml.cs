﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using RSG.UniversalLog;
using RSG.UniversalLog.ViewModel;

namespace UniversalLogViewer
{
    /// <summary>
    /// Interaction logic for ProfileGraphWindow.xaml
    /// </summary>
    public partial class ProfileGraphWindow : Window
    {
        #region Fields
        /// <summary>
        /// View model object for this window.
        /// </summary>
        private ProfileGraphWindowViewModel viewModel;
        #endregion

        #region Constructor
        public ProfileGraphWindow(List<string> filenames, List<UniversalLogComponentViewModel> viewModels)
        {
            viewModel = new ProfileGraphWindowViewModel(filenames, viewModels);
            this.DataContext = viewModel;

            InitializeComponent();
        }
        #endregion

        #region Events
        /// <summary>
        /// Event handler on close of this window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProfileGraphWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            viewModel.OnClosing();
        }
        #endregion
    }
}
