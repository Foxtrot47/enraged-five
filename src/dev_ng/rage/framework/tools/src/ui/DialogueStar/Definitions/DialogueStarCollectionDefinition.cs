﻿//---------------------------------------------------------------------------------------------
// <copyright file="DialogueStarCollectionDefinition.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace DialogueStar.Definitions
{
    using System.IO;
    using System.Reflection;
    using System.Windows.Media.Imaging;
    using RSG.Editor;
    using RSG.Project.ViewModel;
    using RSG.Project.ViewModel.Definitions;

    /// <summary>
    /// Represents the collection definition that should be used with dialogue star.
    /// </summary>
    public class DialogueStarCollectionDefinition : ProjectCollectionDefinition
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DialogueStarCollectionDefinition"/>
        /// class.
        /// </summary>
        public DialogueStarCollectionDefinition()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the extension used for this defined collection.
        /// </summary>
        public override string Extension
        {
            get { return ".dsprojects"; }
        }

        /// <summary>
        /// Gets the string used as the filter in a open or save dialog for this defined
        /// collection.
        /// </summary>
        public override string Filter
        {
            get
            {
                return "Dialogue Star Project Collection Files (*.dsprojects)|*.dsprojects";
            }
        }

        /// <summary>
        /// Gets the icon that is used to display this project collection.
        /// </summary>
        public override BitmapSource Icon
        {
            get { return CommonIcons.Paste; }
        }

        /// <summary>
        /// Gets a byte array containing the data that is used to create a new instance of the
        /// defined project collection.
        /// </summary>
        public override byte[] Template
        {
            get
            {
                byte[] buffer = new byte[0];
                Assembly assembly = Assembly.GetExecutingAssembly();
                string name = "DialogueStar.Templates.DialogueStarCollectionTemplate.xml";
                using (Stream stream = assembly.GetManifestResourceStream(name))
                {
                    buffer = new byte[stream.Length];
                    stream.Read(buffer, 0, buffer.Length);
                }

                return buffer;
            }
        }
        #endregion Properties
    } // DialogueStar.Definitions.DialogueStarCollectionDefinition {Class}
} // DialogueStar.Definitions {Namespace}
