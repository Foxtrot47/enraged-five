﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProjectLineExportAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace DialogueStar.Commands
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using RSG.Editor;
    using RSG.Editor.Controls.Dock.ViewModel;
    using RSG.Interop.Microsoft.Office.Excel;
    using RSG.Project.Commands;
    using RSG.Project.Model;
    using RSG.Project.ViewModel;
    using RSG.Text.Model;
    using RSG.Text.ViewModel;

    /// <summary>
    /// Contains the logic for the <see cref="TextCommands.ExportProjectLinesToExcel"/> routed
    /// command. This class cannot be inherited.
    /// </summary>
    public sealed class ProjectLineExportAction : ButtonAction<ProjectCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ProjectLineExportAction"/>
        /// class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public ProjectLineExportAction(ProjectCommandResolver resolver)
            : base(new ParameterResolverDelegate<ProjectCommandArgs>(resolver))
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ProjectCommandArgs args)
        {
            if (args == null || args.ViewSite == null)
            {
                return false;
            }

            ProjectNode project = null;
            ProjectDocumentItem document = args.ViewSite.ActiveDocument as ProjectDocumentItem;
            if (document != null)
            {
                project = document.FileNode.ProjectNode;
            }
            else
            {
                project = this.ProjectSelection(args.SelectedNodes).FirstOrDefault();
            }

            if (project == null)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(ProjectCommandArgs args)
        {
            ExcelApplication application = ExcelApplicationCreator.Create();
            application.Visible = true;
            int i = 0;

            ProjectNode project = null;
            ProjectDocumentItem document = args.ViewSite.ActiveDocument as ProjectDocumentItem;
            if (document != null)
            {
                project = document.FileNode.ProjectNode;
            }
            else
            {
                project = this.ProjectSelection(args.SelectedNodes).FirstOrDefault();
            }

            if (project == null)
            {
                return;
            }

            string configurationPath = project.GetProperty("Configurations");
            string directory = Path.GetDirectoryName(project.FullPath);

            configurationPath = Path.Combine(directory, configurationPath);
            configurationPath = Path.GetFullPath(configurationPath);

            DialogueConfigurations configurations = new DialogueConfigurations();
            configurations.LoadConfigurations(configurationPath);

            List<Dialogue> models = new List<Dialogue>();
            foreach (ProjectItem item in project["File"])
            {
                string filename = item.GetMetadata("FullPath");
                Dialogue dialogue = this.IsOpen(filename, args);
                if (dialogue == null)
                {
                    using (XmlReader reader = XmlReader.Create(filename))
                    {
                        dialogue = new Dialogue(reader, configurations);
                    }
                }

                if (dialogue != null)
                {
                    models.Add(dialogue);
                }
            }

            ExcelWorksheet worksheet = application.Workbooks.Add("Dialogue Lines");
            Dictionary<Guid, string> characterNames = new Dictionary<Guid, string>();
            Dictionary<Guid, string> specialNames = new Dictionary<Guid, string>();
            Dictionary<Guid, string> audibilityNames = new Dictionary<Guid, string>();
            Dictionary<Guid, string> audiotypeNames = new Dictionary<Guid, string>();
            foreach (Dialogue dialogue in models)
            {
                int lineCount = 0;
                List<string> dialogueData = new List<string>();
                string missionName = dialogue.Name;
                foreach (Conversation conversation in dialogue.Conversations)
                {
                    foreach (ILine line in conversation.Lines)
                    {
                        string characterName = null;
                        Guid id = line.CharacterId;
                        if (!characterNames.TryGetValue(id, out characterName))
                        {
                            characterName = conversation.Configurations.GetCharacterName(id);
                            characterNames.Add(id, characterName);
                        }

                        string specialName = null;
                        id = line.Special;
                        if (!specialNames.TryGetValue(id, out specialName))
                        {
                            specialName = conversation.Configurations.GetSpecialName(id);
                            specialNames.Add(id, specialName);
                        }

                        string audibilityName = null;
                        id = line.Audibility;
                        if (!audibilityNames.TryGetValue(id, out audibilityName))
                        {
                            audibilityName = conversation.Configurations.GetAudibilityName(id);
                            audibilityNames.Add(id, audibilityName);
                        }

                        string audioTypeName = null;
                        id = line.AudioType;
                        if (!audiotypeNames.TryGetValue(id, out audioTypeName))
                        {
                            audioTypeName = conversation.Configurations.GetAudioTypeName(id);
                            audiotypeNames.Add(id, audioTypeName);
                        }

                        dialogueData.Add(characterName);
                        dialogueData.Add(line.Dialogue);
                        dialogueData.Add(line.AudioFilepath);
                        dialogueData.Add(line.SentForRecording.ToString());
                        dialogueData.Add(line.Recorded.ToString());
                        dialogueData.Add(line.Interruptible.ToString());
                        dialogueData.Add(line.DontInterruptForSpecialAbility.ToString());
                        dialogueData.Add(line.AlwaysSubtitled.ToString());
                        dialogueData.Add(line.DucksRadio.ToString());
                        dialogueData.Add(line.DucksScore.ToString());
                        dialogueData.Add(line.HeadsetSubmix.ToString());
                        dialogueData.Add(line.PadSpeaker.ToString());
                        dialogueData.Add(line.Speaker.ToString());
                        dialogueData.Add(line.Listener.ToString());
                        dialogueData.Add(specialName);
                        dialogueData.Add(audibilityName);
                        dialogueData.Add(audioTypeName);
                        dialogueData.Add(missionName);
                        dialogueData.Add(conversation.IsPlaceholder.ToString());
                        lineCount++;
                    }
                }

                worksheet.SetCsvRows(i, lineCount, 19, dialogueData);
                i += lineCount;
            }

            worksheet.SetCsvHeader(
                "Character",
                "Dialogue",
                "Filename",
                "Sent For Recording",
                "Recorded",
                "Interruptible",
                "Dont Interrupt For Special Ability",
                "Always Subtitled",
                "DucksRadio",
                "DucksScore",
                "HeadsetSubmix",
                "PadSpeaker",
                "Speaker",
                "Listener",
                "Special",
                "Audibility",
                "Audio Type",
                "Mission Name",
                "Is Placeholder");

            application.Visible = true;
        }

        /// <summary>
        /// Retrieves the project selection from the specified selection collection by
        /// collecting all of the selected nodes parent project and removing duplicates.
        /// </summary>
        /// <param name="selection">
        /// The collection containing all of the selected nodes.
        /// </param>
        /// <returns>
        /// The project selection from the specified selection collection.
        /// </returns>
        private List<ProjectNode> ProjectSelection(IEnumerable<IHierarchyNode> selection)
        {
            List<ProjectNode> projectSelection = new List<ProjectNode>();
            if (selection == null)
            {
                return projectSelection;
            }

            foreach (IHierarchyNode selectedNode in selection)
            {
                ProjectNode parentProject = selectedNode.ParentProject;
                if (parentProject != null && !projectSelection.Contains(parentProject))
                {
                    projectSelection.Add(parentProject);
                }
            }

            return projectSelection;
        }

        /// <summary>
        /// Determines whether there is a document open that represents the specified node.
        /// </summary>
        /// <param name="node">
        /// The node to test.
        /// </param>
        /// <returns>
        /// True if there is currently a document open that is attached to the specified node;
        /// otherwise, false.
        /// </returns>
        public Dialogue IsOpen(string fullPath, ProjectCommandArgs args)
        {
            foreach (DocumentItem document in args.OpenedDocuments)
            {
                ProjectDocumentItem projectDocument = document as ProjectDocumentItem;
                if (projectDocument == null)
                {
                    continue;
                }

                if (Object.ReferenceEquals(fullPath, projectDocument.FullPath))
                {
                    DialogueViewModel vm = document.Content as DialogueViewModel;
                    if (vm != null)
                    {
                        return vm.Model;
                    }
                }
            }

            return null;
        }
        #endregion Methods
    } // DialogueStar.Commands.ProjectLineExportAction {Class}
} // DialogueStar.Commands {Namespace}
