﻿//---------------------------------------------------------------------------------------------
// <copyright file="DialogueProjectWizard.xaml.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace DialogueStar
{
    using System;
    using System.IO;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Windows;
    using System.Xml;
    using DialogueStar.Resources;
    using Microsoft.Win32;
    using RSG.Base.Extensions;
    using RSG.Editor;
    using RSG.Editor.Controls;
    using RSG.Text.Model;

    /// <summary>
    /// Contains the code behind the dialogue projects wizard window.
    /// </summary>
    public partial class DialogueProjectWizard : RsWindow
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="ConfigPath"/> property.
        /// </summary>
        private string _configPath;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DialogueProjectWizard"/> class.
        /// </summary>
        public DialogueProjectWizard()
        {
            this.InitializeComponent();
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the full path to the configuration file that the user has selected.
        /// </summary>
        public string ConfigurationPath
        {
            get { return this._configPath; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Determines whether the
        /// <see cref="RSG.Editor.SharedCommands.RockstarViewCommands.ConfirmDialog"/> command
        /// can be executed at this level.
        /// </summary>
        /// <param name="e">
        /// The command data that is past from the command system.
        /// </param>
        /// <returns>
        /// True if the command can be executed here; otherwise, false.
        /// </returns>
        protected override bool CanConfirmDialog(CanExecuteCommandData e)
        {
            string newPattern = "\\A[a-zA-Z]:\\\\[^{0}]*.dstarconfig\\z";
            newPattern = String.Format(newPattern, new String(Path.GetInvalidPathChars()));

            string oldPattern = "\\A[a-zA-Z]:\\\\[^{0}]*.xml\\z";
            oldPattern = String.Format(oldPattern, new String(Path.GetInvalidPathChars()));

            if (this.UseLocalFileOption != null && this.UseLocalFileOption.IsChecked == true)
            {
                if (String.IsNullOrWhiteSpace(this.ConfigFileTextBox.Text))
                {
                    return false;
                }

                Regex regex = new Regex(newPattern);
                if (!regex.IsMatch(this.ConfigFileTextBox.Text))
                {
                    return false;
                }

                return true;
            }

            if (this.UpgradeOption != null && this.UpgradeOption.IsChecked == true)
            {
                if (String.IsNullOrWhiteSpace(this.OldConfigFileTextBox.Text))
                {
                    return false;
                }

                if (String.IsNullOrWhiteSpace(this.NewConfigFileTextBox.Text))
                {
                    return false;
                }

                Regex newRegex = new Regex(newPattern);
                if (!newRegex.IsMatch(this.NewConfigFileTextBox.Text))
                {
                    return false;
                }

                Regex oldRegex = new Regex(oldPattern);
                if (!oldRegex.IsMatch(this.OldConfigFileTextBox.Text))
                {
                    return false;
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// Handles the
        /// <see cref="RSG.Editor.SharedCommands.RockstarViewCommands.ConfirmDialog"/> command
        /// after it gets caught here.
        /// </summary>
        /// <param name="e">
        /// The command data that is past from the command system.
        /// </param>
        protected override void ConfirmDialog(ExecuteCommandData e)
        {
            ICommandServiceProvider provider = Application.Current as ICommandServiceProvider;
            ICommonDialogService service = provider.GetService<ICommonDialogService>();

            if (this.UpgradeOption != null && this.UpgradeOption.IsChecked == true)
            {
                this._configPath = this.NewConfigFileTextBox.Text;
                string fullPath = this.NewConfigFileTextBox.Text;
                FileInfo info = new FileInfo(fullPath);
                if (info.Exists)
                {
                    MessageBoxResult result = RsMessageBox.Show(
                        this,
                        StringTable.UpgradeOverrideDetectedMsg.FormatInvariant(fullPath),
                        null,
                        MessageBoxButton.YesNo,
                        MessageBoxImage.Warning);

                    if (result == MessageBoxResult.No)
                    {
                        return;
                    }

                    bool cancelled = false;
                    if (!service.HandleSavingOfReadOnlyFile(ref fullPath, ref cancelled))
                    {
                        return;
                    }

                    if (cancelled)
                    {
                        return;
                    }

                    this._configPath = fullPath;
                }

                if (!this.PerformUpgrade())
                {
                    return;
                }

                base.ConfirmDialog(e);
            }
            else
            {
                if (this.UseLocalFileOption != null)
                {
                    if (this.UseLocalFileOption.IsChecked == true)
                    {
                        this._configPath = this.ConfigFileTextBox.Text;
                        base.ConfirmDialog(e);
                    }
                }
            }
        }

        /// <summary>
        /// Performs the upgrade as set up by the user.
        /// </summary>
        /// <returns>
        /// A value indicating whether the upgrade finished successfully.
        /// </returns>
        private bool PerformUpgrade()
        {
            string oldPath = this.OldConfigFileTextBox.Text;
            string newPath = this._configPath;
            if (File.Exists(oldPath))
            {
                try
                {
                    DialogueConfigurations configurations = new DialogueConfigurations();
                    configurations.LoadOldFormat(oldPath);

                    XmlWriterSettings settings = new XmlWriterSettings();
                    settings.Indent = true;
                    settings.Encoding = Encoding.UTF8;
                    using (XmlWriter writer = XmlWriter.Create(newPath, settings))
                    {
                        writer.WriteStartDocument();
                        writer.WriteStartElement("Configurations");
                        configurations.Serialise(writer);
                        writer.WriteEndElement();
                        writer.WriteEndDocument();
                    }
                }
                catch (Exception ex)
                {
                    RsMessageBox.Show(
                        this,
                        StringTable.UpgradeExceptionMsg.FormatInvariant(ex.Message),
                        null,
                        MessageBoxButton.OK,
                        MessageBoxImage.Error);

                    return false;
                }
            }
            else
            {
                RsMessageBox.Show(
                    this,
                    StringTable.UpgradeFailedMsg.FormatInvariant(Path.GetFileName(oldPath)),
                    null,
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);

                return false;
            }

            return File.Exists(newPath);
        }

        /// <summary>
        /// Called when the browse button is pressed. Shows the select folder windows dialog
        /// for the user to select.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private void OnBrowseButtonClicked(object sender, RoutedEventArgs e)
        {
            string directory = null;
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Dialogue Configurations";
            dlg.Filter = "Dialogue Star Configuration File|*.dstarconfig";
            bool? result = dlg.ShowDialog();
            if (result == true)
            {
                directory = dlg.FileName;
            }
            else
            {
                return;
            }

            this.ConfigFileTextBox.Text = Path.GetFullPath(directory);
        }

        /// <summary>
        /// Called when the browse button is pressed. Shows the select folder windows dialog
        /// for the user to select.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private void OnOldBrowseButtonClicked(object sender, RoutedEventArgs e)
        {
            string path = null;
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Dialogue Configurations";
            dlg.Filter = "Old Dialogue Star Configuration File|*.xml";
            bool? result = dlg.ShowDialog();
            if (result == true)
            {
                path = dlg.FileName;
            }
            else
            {
                return;
            }

            string fullPath = Path.GetFullPath(path);
            this.OldConfigFileTextBox.Text = fullPath;
            this.NewConfigFileTextBox.Text = Path.ChangeExtension(fullPath, ".dstarconfig");
        }
        #endregion Methods
    } // MetadataEditor.DialogueStar {Class}
} // MetadataEditor {Namespace}
