﻿//---------------------------------------------------------------------------------------------
// <copyright file="FallbackSearchAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace DialogueStar.Commands
{
    using System.Threading.Tasks;
    using RSG.Editor;
    using RSG.Text.View;

    /// <summary>
    /// Implements the Rockstar search commands for the dialogue file control. This class
    /// cannot be inherited.
    /// </summary>
    public sealed class FallbackSearchAction : SearchAction<DialogueFileControl>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="FallbackSearchAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public FallbackSearchAction(ParameterResolverDelegate<DialogueFileControl> resolver)
            : base(resolver)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute handler for the
        /// <see cref="RockstarCommands.Find"/> command. Always returns false.
        /// </summary>
        /// <param name="commandParameter">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// Always returns false.
        /// </returns>
        protected override bool CanSearch(DialogueFileControl commandParameter)
        {
            return false;
        }

        /// <summary>
        /// Override to set logic against the execute command handler for the
        /// <see cref="RockstarCommands.Find"/> command when the search data indicates the
        /// search should be cleared.
        /// </summary>
        /// <param name="control">
        /// The dialogue file control resolved for this implementer.
        /// </param>
        protected override void ClearSearch(DialogueFileControl control)
        {
        }

        /// <summary>
        /// Modifies the search parameters so that the user cannot filter the search results.
        /// </summary>
        /// <param name="definition">
        /// The definition that has been created.
        /// </param>
        protected override void ModifyDefinition(ICommandWithSearchParameters definition)
        {
            definition.CanFilterResults = false;
        }

        /// <summary>
        /// Override to set logic against the execute command handler for the
        /// <see cref="RockstarCommands.Find"/> command.
        /// </summary>
        /// <param name="control">
        /// The dialogue file control resolved for this implementer.
        /// </param>
        /// <param name="data">
        /// The search data that has been sent with the command.
        /// </param>
        /// <returns>
        /// A task that represents the work done by the search handler.
        /// </returns>
        protected override async Task<bool> Search(
            DialogueFileControl control, SearchData data)
        {
            return await RSG.Editor.EmptyTask<bool>.CompletedTask;
        }
        #endregion Methods
    } // DialogueStar.Commands.FallbackSearchAction {Class}
} // DialogueStar.Commands
