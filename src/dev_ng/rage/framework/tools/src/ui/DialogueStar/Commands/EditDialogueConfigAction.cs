﻿//---------------------------------------------------------------------------------------------
// <copyright file="EditDialogueConfigAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace DialogueStar.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Windows;
    using System.Xml;
    using DialogueStar.Resources;
    using RSG.Editor;
    using RSG.Project.Commands;
    using RSG.Project.ViewModel;
    using RSG.Text.Model;
    using RSG.Text.View;
    using RSG.Text.ViewModel;

    /// <summary>
    /// Contains the logic for the <see cref="TextCommands.EditDialogueConfig"/> routed
    /// command. This class cannot be inherited.
    /// </summary>
    public class EditDialogueConfigAction : ButtonAction<ProjectCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="EditDialogueConfigAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public EditDialogueConfigAction(ProjectCommandResolver resolver)
            : base(new ParameterResolverDelegate<ProjectCommandArgs>(resolver))
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ProjectCommandArgs args)
        {
            if (args == null || this.ProjectSelection(args.SelectedNodes).Count != 1)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(ProjectCommandArgs args)
        {
            IMessageBoxService messageService = this.GetService<IMessageBoxService>();
            ICommonDialogService dlgService = this.GetService<ICommonDialogService>();
            if (messageService == null || dlgService == null)
            {
                Debug.Assert(false, "Unable to open as services are missing.");
                return;
            }

            List<ProjectNode> projects = this.ProjectSelection(args.SelectedNodes);
            ProjectNode project = projects.FirstOrDefault();
            string configurationPath = project.GetProperty("Configurations");
            string directory = Path.GetDirectoryName(project.FullPath);

            configurationPath = Path.Combine(directory, configurationPath);
            configurationPath = Path.GetFullPath(configurationPath);

            DialogueConfigurations configurations = new DialogueConfigurations();
            configurations.LoadConfigurations(configurationPath);

            EditDialogueConfigWindow window = new EditDialogueConfigWindow();
            window.Owner = Application.Current.MainWindow;
            window.DataContext = new EditDialogueConfigViewModel(configurations);
            if (window.ShowDialog() != true)
            {
                throw new OperationCanceledException();
            }

            bool cancelled = false;
            if (!dlgService.HandleSavingOfReadOnlyFile(ref configurationPath, ref cancelled))
            {
                throw new OperationCanceledException();
            }

            if (cancelled)
            {
                throw new OperationCanceledException();
            }

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            using (XmlWriter writer = XmlWriter.Create(configurationPath, settings))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Configurations");
                configurations.Serialise(writer);
                writer.WriteEndDocument();
            }

            messageService.Show(
                StringTable.CharacterSaveMsg,
                null,
                MessageBoxButton.OK,
                MessageBoxImage.Warning);
        }

        /// <summary>
        /// Retrieves the project selection from the specified selection collection by
        /// collecting all of the selected nodes parent project and removing duplicates.
        /// </summary>
        /// <param name="selection">
        /// The collection containing all of the selected nodes.
        /// </param>
        /// <returns>
        /// The project selection from the specified selection collection.
        /// </returns>
        private List<ProjectNode> ProjectSelection(IEnumerable<IHierarchyNode> selection)
        {
            List<ProjectNode> projectSelection = new List<ProjectNode>();
            if (selection == null)
            {
                return projectSelection;
            }

            foreach (IHierarchyNode selectedNode in selection)
            {
                ProjectNode parentProject = selectedNode.ParentProject;
                if (parentProject != null && !projectSelection.Contains(parentProject))
                {
                    projectSelection.Add(parentProject);
                }
            }

            return projectSelection;
        }
        #endregion Methods
    } // DialogueStar.Commands.EditDialogueConfigAction {Class}
} // DialogueStar.Commands {Namespace}
