﻿//---------------------------------------------------------------------------------------------
// <copyright file="AllDocumentConversationExportAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace DialogueStar.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Controls;
    using RSG.Editor;
    using RSG.Editor.Controls.Dock.ViewModel;
    using RSG.Interop.Microsoft.Office.Excel;
    using RSG.Project.Commands;
    using RSG.Text.Model;
    using RSG.Text.ViewModel;

    /// <summary>
    /// Contains the logic for the
    /// <see cref="TextCommands.ExportAllOpenConversationsToExcel"/> routed command. This
    /// class cannot be inherited.
    /// </summary>
    public sealed class AllDocumentConversationExportAction : ButtonAction<ProjectCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="AllDocumentConversationExportAction"/>
        /// class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public AllDocumentConversationExportAction(ProjectCommandResolver resolver)
            : base(new ParameterResolverDelegate<ProjectCommandArgs>(resolver))
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ProjectCommandArgs args)
        {
            if (args == null || args.ViewSite == null || !args.ViewSite.AllDocuments.Any())
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(ProjectCommandArgs args)
        {
            ExcelApplication application = ExcelApplicationCreator.Create();
            int i = 0;

            ExcelWorksheet worksheet = application.Workbooks.Add("Dialogue Conversations");
            Dictionary<Guid, string> characterNames = new Dictionary<Guid, string>();
            foreach (DocumentItem document in args.ViewSite.AllDocuments)
            {
                List<string> dialogueData = new List<string>();

                object content = document.Content;
                DialogueViewModel viewModel = (content as UserControl).DataContext as DialogueViewModel;
                foreach (Conversation conversation in viewModel.Model.Conversations)
                {
                    HashSet<string> usedCharactersSet = new HashSet<string>();
                    foreach (ILine line in conversation.Lines)
                    {
                        string characterName = null;
                        Guid id = line.CharacterId;
                        if (!characterNames.TryGetValue(id, out characterName))
                        {
                            characterName = conversation.Configurations.GetCharacterName(id);
                            characterNames.Add(id, characterName);
                        }

                        usedCharactersSet.Add(characterName);
                    }

                    string usedCharacters = String.Join(", ", usedCharactersSet);
                    dialogueData.Add(conversation.Root);
                    dialogueData.Add(conversation.Description);
                    dialogueData.Add(conversation.IsPlaceholder.ToString());
                    dialogueData.Add(conversation.IsRandom.ToString());
                    dialogueData.Add(conversation.CutsceneSubtitles.ToString());
                    dialogueData.Add(conversation.Interruptible.ToString());
                    dialogueData.Add(conversation.IsTriggeredByAnimation.ToString());
                    dialogueData.Add(conversation.Lines.Count.ToString());
                    dialogueData.Add(usedCharacters);
                }

                worksheet.SetCsvRows(i, viewModel.Model.Conversations.Count, 9, dialogueData);
                i += viewModel.Model.Conversations.Count;
            }

            worksheet.SetCsvHeader(
                "Root",
                "Description",
                "Placeholder",
                "Random",
                "Cutscene Subtitles",
                "Interruptible",
                "Triggered By Animation",
                "Line Count",
                "Used Characters");

            application.Visible = true;
        }
        #endregion Methods
    } // DialogueStar.Commands.AllDocumentConversationExportAction {Class}
} // DialogueStar.Commands {Namespace}
