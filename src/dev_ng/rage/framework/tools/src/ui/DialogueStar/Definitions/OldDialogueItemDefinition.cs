﻿//---------------------------------------------------------------------------------------------
// <copyright file="OldDialogueItemDefinition.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace DialogueStar.Definitions
{
    using System.IO;
    using System.Text;
    using System.Windows.Media.Imaging;
    using System.Xml;
    using RSG.Editor.Model;
    using RSG.Project.ViewModel;
    using RSG.Project.ViewModel.Definitions;
    using RSG.Text.Model;
    using RSG.Text.ViewModel;
    using RSG.Text.ViewModel.Project;

    /// <summary>
    /// Represents the definition for the dialogue star project item that contains the
    /// conversations and lines for the subtitles.
    /// </summary>
    public class OldDialogueItemDefinition : ProjectItemDefinition, IRequiresUpdatingDefinition
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="OldDialogueItemDefinition"/> class.
        /// </summary>
        public OldDialogueItemDefinition()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the name that is used as the default value for a new instance of this
        /// definition.
        /// </summary>
        public override string DefaultName
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the description for this item definition to be displayed in the add dialog.
        /// </summary>
        public override string Description
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the extension used for this defined item.
        /// </summary>
        public override string Extension
        {
            get { return ".dstar"; }
        }

        /// <summary>
        /// Gets the icon that is used to display this project.
        /// </summary>
        public override BitmapSource Icon
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the name for this defined project.
        /// </summary>
        public override string Name
        {
            get { return null; }
        }

        /// <summary>
        /// Gets a byte array containing the data that is used to create a new instance of the
        /// defined project.
        /// </summary>
        public override byte[] Template
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the type of this item to be displayed in the add dialog.
        /// </summary>
        public override string Type
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the extension that the old file is updated to.
        /// </summary>
        public string UpdatedExtension
        {
            get { return ".dstar2"; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Called to update an old file format to a new format.
        /// </summary>
        /// <param name="inStream">
        /// The stream that the old data can be read from.
        /// </param>
        /// <param name="outStream">
        /// The new stream that the new data can be written to.
        /// </param>
        /// <param name="project">
        /// The project the updated file is being added to.
        /// </param>
        public void Update(Stream inStream, Stream outStream, ProjectNode project)
        {
            DialogueConfigurations configurations = null;
            DialogueProjectNode dialogueProject = project as DialogueProjectNode;
            if (dialogueProject != null)
            {
                configurations = dialogueProject.Configurations;
            }

            Dialogue dialogue = new Dialogue((UndoEngine)null, configurations);
            using (XmlReader reader = XmlReader.Create(inStream))
            {
                dialogue.LoadOldFormat(reader);
            }

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = Encoding.UTF8;
            settings.Indent = true;
            using (XmlWriter writer = XmlWriter.Create(outStream, settings))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Dialogue");
                dialogue.Serialise(writer);
                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }
        #endregion Methods
    } // DialogueStar.Definitions.DialogueItemDefinition {Class}
} // DialogueStar.Definitions {Namespace}
