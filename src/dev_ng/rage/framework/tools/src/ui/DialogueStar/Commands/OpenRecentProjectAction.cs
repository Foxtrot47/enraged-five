﻿//---------------------------------------------------------------------------------------------
// <copyright file="EditDialogueConfigAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace DialogueStar.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Windows;
    using System.Xml;
    using DialogueStar.Resources;
    using RSG.Editor;
    using RSG.Project.Commands;
    using RSG.Project.ViewModel;
    using RSG.Text.Model;
    using RSG.Text.View;
    using RSG.Text.ViewModel;
    using System.Threading.Tasks;
    using RSG.Project.ViewModel.Definitions;
    using RSG.Project.Model;
    using RSG.Editor.Controls;

    /// <summary>
    /// Contains the logic for the <see cref="TextCommands.EditDialogueConfig"/> routed
    /// command. This class cannot be inherited.
    /// </summary>
    public class OpenRecentProjectAction : MruCommandAction<ProjectCommandArgs>
    {
        #region Fields
        /// <summary>
        /// The private reference to the open collection action to exceute when opening a
        /// collection file.
        /// </summary>
        private OpenCollectionAction _openCollectionAction;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="EditDialogueConfigAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public OpenRecentProjectAction(ProjectCommandResolver resolver, OpenCollectionAction openCollectionAction)
            : base(new ParameterResolverDelegate<ProjectCommandArgs>(resolver))
        {
            this._openCollectionAction = openCollectionAction;
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="viewModels">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="path">
        /// The path sent with the MRU command.
        /// </param>
        /// <param name="additional">
        /// The additional information for the path property sent with the command.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(
            ProjectCommandArgs args, string path, string[] additional)
        {
            if (args == null)
            {
                return false;
            }

            if (args.CollectionNode == null)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="viewModels">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="path">
        /// The path sent with the MRU command.
        /// </param>
        /// <param name="additional">
        /// The additional information for the path property sent with the command.
        /// </param>
        /// <returns>
        /// A task that represents the work done by the command handler.
        /// </returns>
        public override void Execute(
            ProjectCommandArgs args, string path, string[] additional)
        {
            if (additional.Length > 0 && additional[0] == "Project")
            {
                IHierarchyNode parent = args.SelectedNodes.FirstOrDefault();
                ProjectCollectionNode collection = args.CollectionNode;
                if (parent == null || !parent.CanHaveProjectsAdded || args.SelectionCount > 1)
                {
                    parent = args.CollectionNode;
                }

                string fullPath = path;
                string extension = Path.GetExtension(fullPath);
                ProjectDefinition selectedDefinition = null;
                StringComparison comparisonType = StringComparison.OrdinalIgnoreCase;
                foreach (ProjectDefinition def in collection.Definition.ProjectDefinitions)
                {
                    if (String.Equals(def.Extension, extension, comparisonType))
                    {
                        selectedDefinition = def;
                        break;
                    }
                }

                if (selectedDefinition == null)
                {
                    Debug.Assert(
                        false,
                        "Failed to find project definition for the extension",
                        "{0}",
                        extension);
                    return;
                }

                CloseCollectionAction close = new CloseCollectionAction(this.ServiceProvider);
                if (close.CanExecute(args))
                {
                    close.Execute(args);
                }

                ProjectItem newItem = collection.Model.AddNewProjectItem("Project", fullPath);
                ProjectNode projectNode = selectedDefinition.CreateProjectNode();
                projectNode.SetParentAndModel(collection, newItem);
                collection.AddChild(projectNode);
                projectNode.Load();

                collection.IsModified = true;
                collection.Loaded = true;
                collection.IsTemporaryFile = true;
                collection.Name = Path.GetFileNameWithoutExtension(fullPath);
                RsApplication app = Application.Current as RsApplication;
                if (app != null)
                {
                    app.AddToMruList(
                        "Projects", fullPath, new string[] { "Project" });
                }
            }
            else
            {
                this._openCollectionAction.Execute(args, path);
            }
        }
        #endregion Methods
    } // DialogueStar.Commands.EditDialogueConfigAction {Class}
} // DialogueStar.Commands {Namespace}
