﻿//---------------------------------------------------------------------------------------------
// <copyright file="SearchResults.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace DialogueStar.ViewModel
{
    using System.Windows;
    using DialogueStar.Resources;
    using RSG.Editor.Controls.Dock.ViewModel;

    /// <summary>
    /// Represents the search results tool window. This class cannot be inherited.
    /// </summary>
    internal sealed class SearchResults : ToolWindowItem
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SearchResults"/> class.
        /// </summary>
        /// <param name="collectionNode">
        /// The object that contains the loaded project collection.
        /// </param>
        /// <param name="parentPane">
        /// The parent docking pane.
        /// </param>
        public SearchResults(DockingPane parentPane)
            : base(parentPane, StringTable.SearchResultsTitle)
        {
            MainWindow mainWindow = Application.Current.MainWindow as MainWindow;
            this.Content = mainWindow.TryFindResource("SearchResults");
        }
        #endregion Constructors
    } // DialogueStar.ViewModel.SearchResults {Class}
} // DialogueStar.ViewModel {Namespace}
