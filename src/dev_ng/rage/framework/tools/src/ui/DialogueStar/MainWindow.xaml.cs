﻿//---------------------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace DialogueStar
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Interop;
    using System.Xml;
    using DialogueStar.Commands;
    using DialogueStar.Resources;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Base.Logging.Universal;
    using RSG.Editor;
    using RSG.Editor.Controls;
    using RSG.Editor.Controls.Dock;
    using RSG.Editor.Controls.Dock.Document;
    using RSG.Editor.Controls.Dock.ViewModel;
    using RSG.Editor.Controls.Perforce;
    using RSG.Editor.Controls.Perforce.Commands;
    using RSG.Editor.SharedCommands;
    using RSG.Interop.Microsoft.Office.Word;
    using RSG.Interop.Microsoft.Windows;
    using RSG.Project.Commands;
    using RSG.Project.View;
    using RSG.Project.ViewModel;
    using RSG.Text.Commands;
    using RSG.Text.Model;
    using RSG.Text.View;
    using RSG.Text.ViewModel;

    /// <summary>
    /// Interaction logic for the main window of the application.
    /// </summary>
    public partial class MainWindow : RsMainWindow
    {
        #region Fields
        /// <summary>
        /// The global identifier used for the build menu.
        /// </summary>
        private static Guid _buildMenu;

        /// <summary>
        /// The global identifier used for the edit menu.
        /// </summary>
        private static Guid _editMenu;

        /// <summary>
        /// The global identifier used for the export menu.
        /// </summary>
        private static Guid _exportMenu;

        /// <summary>
        /// The global identifier used for the project menu.
        /// </summary>
        private static Guid _projectMenu;

        /// <summary>
        /// The global identifier used for the perforce menu inside the main menu.
        /// </summary>
        private static Guid _perforceMenu;

        /// <summary>
        /// The global identifier used for the text menu.
        /// </summary>
        private static Guid _textMenu;

        /// <summary>
        /// The global identifier used for the text toolbar.
        /// </summary>
        private static Guid _standardToolbar;

        /// <summary>
        /// The global identifier used for the text commands toolbar.
        /// </summary>
        private static Guid _textToolbar;

        /// <summary>
        /// The global identifier used for the open menu inside the top level file menu.
        /// </summary>
        private static Guid _openFileMenu;

        /// <summary>
        /// The global identifier used for the new menu inside the top level file menu.
        /// </summary>
        private static Guid _newFileMenu;

        /// <summary>
        /// The global identifier used for the add menu inside the top level file menu.
        /// </summary>
        private static Guid _addFileMenu;

        /// <summary>
        /// The private reference to the data context object.
        /// </summary>
        private MainWindowDataContext _dataContext;

        /// <summary>
        /// The private reference to the project explorer control.
        /// </summary>
        private ProjectCollectionExplorer _explorer;

        /// <summary>
        /// A private collection of full paths to files that have been modified since the
        /// application was last active.
        /// </summary>
        private HashSet<string> _modifiedFiles;

        /// <summary>
        /// The main file watcher for the application that looks after all loaded projects and
        /// documents.
        /// </summary>
        private FileWatcherManager _fileWatcher;

        /// <summary>
        /// The main file backup manager for the application that backups all modified opened
        /// documents.
        /// </summary>
        private FileBackupManager _fileBackup;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="MainWindow" /> class.
        /// </summary>
        static MainWindow()
        {
            _projectMenu = new Guid("B3638964-C063-40B6-B323-E70DC68A5473");
            _editMenu = new Guid("B4775183-8E2D-4B22-91A6-87045FB9B0C7");
            _textMenu = new Guid("C18187BA-2ED5-46B5-9071-3954F23B7265");
            _standardToolbar = new Guid("E138F014-8147-4CDC-8825-20AA91DEF0FB");
            _textToolbar = new Guid("8AD34DCD-9067-422F-B714-71877F382505");
            _openFileMenu = new Guid("B0C43843-3670-4A8B-855C-B720C3538B59");
            _newFileMenu = new Guid("1A545F99-BA75-40BB-8246-2015037DB41F");
            _addFileMenu = new Guid("992E3823-C490-401E-9B7B-6BD70D59FBE1");
            _buildMenu = new Guid("58448177-DE41-4FE8-A9D6-AB7487C4780D");
            _exportMenu = new Guid("66231DAD-14E1-4431-AABE-F8E489FE76F3");
            _perforceMenu = new Guid("07FFF69B-E168-41B2-AC50-5F8038E18D83");
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        /// <param name="application">
        /// The instance to the application this is going to be the main window for.
        /// </param>
        public MainWindow(RsApplication application)
        {
            this._modifiedFiles = new HashSet<string>();
            this.InitializeComponent();
            MainWindowDataContext dataContext = new MainWindowDataContext();
            this.DataContext = dataContext;
            this._dataContext = dataContext;

            object resource = this.TryFindResource("Explorer");
            this._explorer = resource as ProjectCollectionExplorer;

            this.AddCommandInstances(application);
            this.AttachCommandBindings();

            this._fileWatcher = new FileWatcherManager();
            this._fileWatcher.ModifiedTimeChanged += this.OnModifiedTimeChanged;
            this._fileWatcher.ReadOnlyFlagChanged += this.OnReadOnlyFlagChanged;

            string temp = Path.GetTempPath();
            string backupPath = Path.Combine(temp, "Rockstar Games", application.UniqueName);
            this._fileBackup = new FileBackupManager(TimeSpan.FromMinutes(7), backupPath);

            dataContext.CollectionNode.ProjectLoaded += this.OnProjectLoaded;
            dataContext.CollectionNode.ProjectUnloaded += this.OnProjectUnloaded;
            dataContext.CollectionNode.ProjectFailed += this.OnProjectFailed;
            dataContext.CollectionNode.ProjectSaved += this.OnProjectSaved;
            dataContext.CollectionNode.ProjectCollectionSaved += this.OnProjectCollectionSaved;

            var docService = application.GetService<IDocumentManagerService>();
            if (docService != null)
            {
                docService.CollectionClosing += this.OnCollectionClosing;
                docService.DocumentClosing += this.OnDocumentClosing;
                docService.DocumentOpened += this.OnDocumentOpened;
            }

            App app = application as App;
            app.SetBackupManager(this._fileBackup);
            app.SetFileWatcher(this._fileWatcher);
            app.SetViewSite(dataContext.ViewSite);
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Retrieves the correct list of strings representing full paths to perform perforce
        /// operations on depending on the command source.
        /// </summary>
        /// <param name="commandData">
        /// The command data that has called this resolver.
        /// </param>
        /// <returns>
        /// The correct list of strings representing full paths to perform perforce operations
        /// on depending on the command source.
        /// </returns>
        internal IList<string> PerforceCommandResolver(CommandData commandData)
        {
            RsDocumentItem source = commandData.Source as RsDocumentItem;
            if (source == null)
            {
                return null;
            }

            DocumentItem item = source.DataContext as DocumentItem;
            if (item == null)
            {
                return null;
            }

            return new List<string>() { item.FullPath };
        }

        /// <summary>
        /// The action resolver that returns a instance of the project command args class that
        /// project commands can use to execute their logic.
        /// </summary>
        /// <param name="commandData">
        /// The command data sent with the executing command.
        /// </param>
        /// <returns>
        /// The currently active document item inside the view site.
        /// </returns>
        internal BuildProjectCommandArgs ProjectCommandResolver(CommandData commandData)
        {
            MainWindowDataContext dc = this._dataContext;
            if (dc == null)
            {
                return null;
            }

            BuildProjectCommandArgs args = new BuildProjectCommandArgs();
            args.CollectionNode = dc.CollectionNode;
            args.NodeSelectionDelegate = this.SelectNode;
            args.NodeRenameDelegate = this.RenameNode;
            args.OpenedDocuments = dc.ViewSite.AllDocuments;
            args.ViewSite = dc.ViewSite;
            args.ProjectBuilder = new ProjectBuilder();
            if (this._explorer != null)
            {
                args.SelectedNodes = this._explorer.SelectedItems;
            }

            return args;
        }

        internal ProjectCommandArgs CloseDocumentCmdResolver(CommandData commandData)
        {
            ProjectCommandArgs args = new ProjectCommandArgs();
            args.ViewSite = this._dataContext.ViewSite;
            return args;
        }

        /// <summary>
        /// Adds the commands instances and menu instances to the command manager.
        /// </summary>
        /// <param name="application">
        /// The current application instance.
        /// </param>
        private void AddCommandInstances(RsApplication application)
        {
            application.RegisterMruList(
                "Files",
                true,
                RockstarCommands.OpenFileFromMru,
                new Guid("6D9FDE6C-801B-4EE0-A70B-B3AA698584C0"),
                new Guid("E57BF66D-475A-4476-8CFF-2299E2C3D60E"),
                new Guid("460EA2A2-BEA6-4129-AA26-E55F13C4F76D"));

            application.RegisterMruList(
                "Projects",
                true,
                RockstarCommands.OpenProjectFromMru,
                new Guid("B50A0170-21C9-4894-823C-0DEA301B7452"),
                new Guid("DA0A4944-014F-4B9D-AE7C-95E748E8AAC8"),
                new Guid("1403DCF6-BC7F-440B-B5D5-82C869AE1523"));

            RockstarCommandManager.AddProjectMenu(_projectMenu);
            RockstarCommandManager.AddTopLevelMenu(5, "Perforce", _perforceMenu);
            RockstarCommandManager.AddTopLevelMenu(6, "Build", _buildMenu);
            RockstarCommandManager.AddTopLevelMenu(7, "Export", _exportMenu);
            RockstarCommandManager.AddTopLevelMenu(8, "Text", _textMenu);

            RockstarCommandManager.AddCommandDefinition(
                new ButtonCommand(RockstarCommands.SelectAll));

            this.AddCommandInstancesToFileMenu();
            this.AddCommandInstancesToBuildMenu();
            this.AddCommandInstancesToConversationContextMenu();
            this.AddCommandInstancesToCollectionContextMenu();
            this.AddCommandInstancesToCollectionFolderContextMenu();
            this.AddCommandInstancesToDocumentContextMenu();
            this.AddCommandInstancesToEditMenu();
            this.AddCommandInstancesToPerforceMenu();
            this.AddCommandInstancesToExportMenu();
            this.AddCommandInstancesToLineContextMenu();
            this.AddCommandInstancesToProjectContextMenu();
            this.AddCommandInstancesToProjectFolderContextMenu();
            this.AddCommandInstancesToProjectItemContextMenu();
            this.AddCommandInstancesToTextMenu();
            this.AddCommandInstancesToStandardToolBar();
            this.AddCommandInstancesToTextToolBar();
            this.AddCommandInstancesToProjectMenu();
        }

        /// <summary>
        /// Adds command instances to the command manager for commands that are located inside
        /// the main menus File menu.
        /// </summary>
        private void AddCommandInstancesToFileMenu()
        {
            ushort index = 0;
            RockstarCommandManager.AddMenu(
                index++, "New", _newFileMenu, RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddMenu(
                index++, "Open", _openFileMenu, RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddMenu(
                index++, true, "Add", _addFileMenu, RockstarCommandManager.FileMenuId);

            this.AddFileMenuNewCommands();
            this.AddFileMenuOpenCommands();
            this.AddFileMenuAddCommands();

            RockstarCommandManager.AddCommandInstance(
                DockingCommands.CloseItem,
                index++,
                true,
                "Close",
                new Guid("3C920F77-E443-45CA-A030-FF7E19FBE65A"),
                RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.CloseProjectCollection,
                index++,
                false,
                "Close Project Collection",
                new Guid("324C3328-2283-4217-99E0-306F34F2DA02"),
                RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Save,
                index++,
                true,
                "Save Selected Items",
                new Guid("3BAA7D00-DAF6-410C-A109-3DAB40A783E1"),
                RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.SaveAs,
                index++,
                false,
                "Save Selected Items As...",
                new Guid("123F7582-27AD-40CC-ABDE-D8D9D546FC29"),
                RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.SaveAll,
                index++,
                false,
                "Save All",
                new Guid("1C1F7391-2F12-4A09-8D5E-BDFC118A7AD1"),
                RockstarCommandManager.FileMenuId);
        }

        /// <summary>
        /// Adds command instances to the command manager for commands that are located inside
        /// the Add menu off of the File menu.
        /// </summary>
        private void AddFileMenuAddCommands()
        {
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddNewProject,
                index++,
                false,
                "New Project...",
                new Guid("B4E9D591-FF04-4507-90FE-83F628790143"),
                _addFileMenu);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddExistingProject,
                index++,
                false,
                "Existing Project...",
                new Guid("3588906C-3363-458A-B95F-CCAB4745D60E"),
                _addFileMenu);
        }

        /// <summary>
        /// Adds command instances to the command manager for commands that are located inside
        /// the New menu off of the File menu.
        /// </summary>
        private void AddFileMenuNewCommands()
        {
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.CreateNewProject,
                index++,
                false,
                "Project...",
                new Guid("F5CCF48B-358F-4E72-83DA-672CB2F929F7"),
                _newFileMenu);
        }

        /// <summary>
        /// Adds command instances to the command manager for commands that are located inside
        /// the Open menu off of the File menu.
        /// </summary>
        private void AddFileMenuOpenCommands()
        {
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.OpenProjectCollectionFile,
                index++,
                false,
                "Project Collection...",
                new Guid("9B0B8F66-C5F0-4887-BC81-8E68C25A887F"),
                _openFileMenu);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.OpenProjectFile,
                index++,
                false,
                "Project Inside New Collection...",
                new Guid("EE0E27F9-8720-49F6-80DC-C1DF1CA1C99C"),
                _openFileMenu);
        }

        /// <summary>
        /// 
        /// </summary>
        private void AddCommandInstancesToBuildMenu()
        {
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.BuildAllProjects,
                index++,
                false,
                "Build Collection",
                new Guid("DBB5338A-C707-4BF4-AE3E-AB3AF6233FE2"),
                _buildMenu);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.BuildSelectedProjects,
                index++,
                false,
                "Build Selection",
                new Guid("3990548F-2B77-4EC3-A3F8-6785D2708DC8"),
                _buildMenu);
        }

        /// <summary>
        /// 
        /// </summary>
        private void AddCommandInstancesToProjectMenu()
        {
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddNewItem,
                index++,
                true,
                "Add New Item...",
                new Guid("45B42657-BBA4-47DB-83A6-EEB722A53376"),
                _projectMenu);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddExistingItem,
                index++,
                false,
                "Add Existing Item...",
                new Guid("406BABF5-E07C-4814-8C57-996F1466F4DE"),
                _projectMenu);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.OpenProjectProperties,
                index++,
                true,
                "Properties",
                new Guid("F28FF7F3-E514-4232-ACD2-2358F06A15C6"),
                _projectMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.EditDialogueConfig,
                index++,
                false,
                "Edit Dialogue Config",
                new Guid("2470B94A-51DB-440E-B2DF-DFFE5EF2293C"),
                _projectMenu);
        }

        /// <summary>
        /// 
        /// </summary>
        private void AddCommandInstancesToCollectionContextMenu()
        {
            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.BuildAllProjects,
                0,
                false,
                "Build All Dialogue",
                new Guid("51D64D19-5178-46B9-B7EF-52473208D0FF"),
                ProjectCommandIds.CollectionContextMenu);

            Guid menuId = new Guid("735D62C6-1918-4004-822D-4B6FAB2296A4");
            RockstarCommandManager.AddMenu(
                1, true, "Add", menuId, ProjectCommandIds.CollectionContextMenu);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddNewProject,
                0,
                false,
                "New Project...",
                new Guid("15141C16-CD4F-464C-9DC6-2F493497AB43"),
                menuId);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddExistingProject,
                1,
                false,
                "Existing Project...",
                new Guid("83BF204B-FB00-4E0A-86B8-ECCA57E873B7"),
                menuId);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddCollectionFolder,
                2,
                true,
                "New Collection Folder...",
                new Guid("158F8A0E-C47C-4AEA-AF6B-B4EFF137ACA4"),
                menuId);

            Guid perforceMenu = new Guid("6A5116BB-2AF6-4A79-91BF-D01C026957DA");
            RockstarCommandManager.AddMenu(
                2,
                "Perforce",
                perforceMenu,
                ProjectCommandIds.CollectionContextMenu);

            PerforceCommands.CreateStandardMenu(
                perforceMenu,
                new Guid("AC86E6DA-8921-43ED-A48C-6EF1FE948A8A"),
                new Guid("FE8F6D47-F826-4E73-9C39-8111CADDDBE4"),
                new Guid("87E07721-6B6D-4AFB-B88B-C8A0620A7E20"),
                new Guid("7D87922F-E5D5-4A72-8C4A-C345B7F7D03B"),
                new Guid("EE23EA60-D9FA-4A05-9C8E-A9152A5E0226"),
                new Guid("607462F9-2AA0-426A-B0A6-8B64F5616854"));
        }

        /// <summary>
        /// 
        /// </summary>
        private void AddCommandInstancesToCollectionFolderContextMenu()
        {
            Guid menuId = new Guid("0375D3C4-7319-4D2F-AE30-C2E05BF392C0");
            RockstarCommandManager.AddMenu(
                0, "Add", menuId, ProjectCommandIds.CollectionFolderContextMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Delete,
                1,
                true,
                "Delete",
                new Guid("FD047478-7227-4224-ADE8-CD25205EFFAB"),
                ProjectCommandIds.CollectionFolderContextMenu);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddNewProject,
                0,
                false,
                "New Project...",
                new Guid("C8F36332-9CCF-4EFB-A6DC-49E345AF7271"),
                menuId);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddExistingProject,
                1,
                false,
                "Existing Project...",
                new Guid("98617249-02F9-4C9E-9274-E927E2F921DA"),
                menuId);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddCollectionFolder,
                2,
                true,
                "New Collection Folder...",
                new Guid("68F6AA63-143D-4B27-8010-FF077CEED8CA"),
                menuId);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Rename,
                2,
                false,
                "Rename",
                new Guid("E92FF961-9B3F-4EAA-9DF7-EA0C13E0AF33"),
                ProjectCommandIds.CollectionFolderContextMenu);
        }

        /// <summary>
        /// 
        /// </summary>
        private void AddCommandInstancesToConversationContextMenu()
        {
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Cut,
                index++,
                true,
                "Cut",
                new Guid("FB6C58FC-4E0E-4334-94EC-19E62379AF55"),
                TextViewCommandIds.ConversationContextMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Copy,
                index++,
                false,
                "Copy",
                new Guid("8DA6FAAB-5C11-4028-8A7E-5050AF481EB1"),
                TextViewCommandIds.ConversationContextMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Paste,
                index++,
                false,
                "Paste",
                new Guid("EBD62390-B0F7-4238-9EFD-ADF2B6D6FBFA"),
                TextViewCommandIds.ConversationContextMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Delete,
                index++,
                false,
                "Delete",
                new Guid("BFACCBF9-32F1-4467-820A-43E7DE6DD8BA"),
                TextViewCommandIds.ConversationContextMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.AddNewConversation,
                index++,
                true,
                "Add New Conversation",
                new Guid("4453D4DE-9214-4DC6-A9C8-791A75F18F35"),
                TextViewCommandIds.ConversationContextMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.InsertNewConversation,
                index++,
                false,
                "Insert New Conversation",
                new Guid("940D0BFB-8ECC-44FB-A8D7-36112370E54D"),
                TextViewCommandIds.ConversationContextMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.MoveConversationUp,
                index++,
                true,
                "Move Up",
                new Guid("EC137CAC-80ED-4647-A441-A0BC53493255"),
                TextViewCommandIds.ConversationContextMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.MoveConversationDown,
                index++,
                false,
                "Move Down",
                new Guid("467DEFD6-1D2E-4854-9A08-FA85301BAC9F"),
                TextViewCommandIds.ConversationContextMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.PlayConversation,
                index++,
                true,
                "Play Conversation",
                new Guid("A0E35F84-5F9A-4160-BD7A-F00D0F7B58B7"),
                TextViewCommandIds.ConversationContextMenu);
        }

        /// <summary>
        /// 
        /// </summary>
        private void AddCommandInstancesToDocumentContextMenu()
        {
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.LocateInExplorer,
                index++,
                false,
                "Locate In Explorer",
                new Guid("EAEFBFB8-E583-4C58-824D-65D338082B66"),
                DockingCommandIds.DocumentContextMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Save,
                index++,
                true,
                "Save",
                new Guid("39D18749-CCDF-4F24-A286-B627F4E202B4"),
                DockingCommandIds.DocumentContextMenu);

            RockstarCommandManager.AddCommandInstance(
                DockingCommands.CloseItem,
                index++,
                false,
                "Close",
                new Guid("12589F23-3043-4421-939E-222236482E23"),
                DockingCommandIds.DocumentContextMenu);

            RockstarCommandManager.AddCommandInstance(
                DockingCommands.CloseAll,
                index++,
                false,
                "Close All Documents",
                new Guid("13B46061-AB1B-4423-BDD9-6C82E8308379"),
                DockingCommandIds.DocumentContextMenu);

            RockstarCommandManager.AddCommandInstance(
                DockingCommands.CloseAllButItem,
                index++,
                false,
                "Close All But This",
                new Guid("63C9E8E9-2253-452E-84B6-CEEE6F016454"),
                DockingCommandIds.DocumentContextMenu);

            RockstarCommandManager.AddCommandInstance(
                DockingCommands.CopyFullPath,
                index++,
                true,
                "Copy Full Path",
                new Guid("4D6083F8-D5B9-4E53-A84E-E33F03AF05B6"),
                DockingCommandIds.DocumentContextMenu);

            RockstarCommandManager.AddCommandInstance(
                DockingCommands.OpenContainingFolder,
                index++,
                false,
                "Open Containing Folder",
                new Guid("AAFC27E3-CE9F-44C2-B294-97D5D758B5ED"),
                DockingCommandIds.DocumentContextMenu);
        }

        /// <summary>
        /// 
        /// </summary>
        private void AddCommandInstancesToEditMenu()
        {
            RockstarCommandManager.AddEditMenu(_editMenu);
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Undo,
                index++,
                true,
                "Undo",
                new Guid("1ED63D85-7BE8-4EAD-96A4-57ABF67057BF"),
                _editMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Redo,
                index++,
                false,
                "Redo",
                new Guid("AAEAB6F0-462C-47F6-829C-0962688BB688"),
                _editMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Cut,
                index++,
                true,
                "Cut",
                new Guid("609CAC4D-B340-4A4B-B4C4-E624C9EB15A7"),
                _editMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Copy,
                index++,
                false,
                "Copy",
                new Guid("732E962F-E31B-43CD-AED3-81CB9AABEA23"),
                _editMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.CopyFilename,
                index++,
                false,
                "Copy Filename",
                new Guid("5C13B950-B717-4F60-BD13-D70E75DE669D"),
                _editMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Paste,
                index++,
                false,
                "Paste",
                new Guid("CC1692F8-ACEA-41B9-825E-998F96F8D2BD"),
                _editMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Delete,
                index++,
                false,
                "Delete",
                new Guid("C6C84D73-FCDF-4B5B-9352-83144622DC74"),
                _editMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.SelectAll,
                index++,
                true,
                "Select All",
                new Guid("AE719AE8-EB04-4797-A9DB-4C9E667E66A1"),
                _editMenu);
        }

        /// <summary>
        /// 
        /// </summary>
        private void AddCommandInstancesToExportMenu()
        {
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                TextCommands.ExportConversationLinesToWord,
                index++,
                false,
                "Export Lines For Current Conversation...",
                new Guid("0DEF7A4A-D0B2-4761-9EFF-9BA38B8AED57"),
                _exportMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.ExportDocumentLinesToWord,
                index++,
                false,
                "Export Lines For Current Document...",
                new Guid("5783F3D0-72A9-4CF6-AEE1-7BDF774829D9"),
                _exportMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.ExportAllOpenLinesToWord,
                index++,
                false,
                "Export Lines For All Opened Documents...",
                new Guid("8646F227-4D07-4729-B321-AAC97DB0E459"),
                _exportMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.ExportDocumentConversationsToExcel,
                index++,
                false,
                "Export Conversations For Current Document",
                new Guid("A60A1102-0828-4631-86C4-8E97B7D8A7DC"),
                _exportMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.ExportAllOpenConversationsToExcel,
                index++,
                false,
                "Export Conversations For All Opened Documents",
                new Guid("3CE728F3-4BFB-44E0-8AF9-EEA4F7781A5E"),
                _exportMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.ExportProjectConversationsToExcel,
                index++,
                false,
                "Export Conversations For Current Project",
                new Guid("8F763C30-F54F-4032-8AFD-042BD0429D3E"),
                _exportMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.ExportDocumentLinesToExcel,
                index++,
                false,
                "Export Lines For Current Document",
                new Guid("C8315988-C0BD-4671-83D1-D340CA0309CD"),
                _exportMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.ExportAllOpenLinesToExcel,
                index++,
                false,
                "Export Lines For All Opened Documents",
                new Guid("89F98B28-D207-457B-B3F3-82D4A862B01D"),
                _exportMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.ExportProjectLinesToExcel,
                index++,
                false,
                "Export Lines For Current Project",
                new Guid("94BD1897-3338-47BE-998C-A89664CB9AEA"),
                _exportMenu);
        }

        /// <summary>
        /// 
        /// </summary>
        private void AddCommandInstancesToLineContextMenu()
        {
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Cut,
                index++,
                true,
                "Cut",
                new Guid("F14EC94B-8E6E-4A40-8D6C-35BCA128ACDB"),
                TextViewCommandIds.LineContextMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Copy,
                index++,
                false,
                "Copy",
                new Guid("B4F30A29-EEBE-4AF2-9E1B-624B83A729C9"),
                TextViewCommandIds.LineContextMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.CopyFilename,
                index++,
                false,
                "Copy Filename",
                new Guid("2609F70C-1E98-4B0A-A902-786422DF72CD"),
                TextViewCommandIds.LineContextMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Paste,
                index++,
                false,
                "Paste",
                new Guid("1B5200F7-2A3A-4A41-9537-DBC6D754C7E0"),
                TextViewCommandIds.LineContextMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Delete,
                index++,
                false,
                "Delete",
                new Guid("80047F38-65E9-4A5A-A873-58B0818B630B"),
                TextViewCommandIds.LineContextMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.AddNewLine,
                index++,
                true,
                "Add New Line",
                new Guid("5AC8A0DB-823F-402C-A7FF-48CC941AE058"),
                TextViewCommandIds.LineContextMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.InsertNewLine,
                index++,
                false,
                "Insert New Line",
                new Guid("ACB28E0F-9EB9-43A3-AA70-69A6CA2FDDD6"),
                TextViewCommandIds.LineContextMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.MoveLineUp,
                index++,
                false,
                "Move Up",
                new Guid("3E6F7EB3-0DFA-41AC-AB8B-1097DFA57127"),
                TextViewCommandIds.LineContextMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.MoveLineDown,
                index++,
                false,
                "Move Down",
                new Guid("B0799CBF-6526-43A0-9D5D-C77FF526ACBE"),
                TextViewCommandIds.LineContextMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.PlayLine,
                index++,
                true,
                "Play Line",
                new Guid("F1B5C356-19BA-4AFC-B685-9CD8223EF033"),
                TextViewCommandIds.LineContextMenu);
        }

        /// <summary>
        /// Adds command instances to the command manager for commands that are located inside
        /// the main menus Perforce menu.
        /// </summary>
        private void AddCommandInstancesToPerforceMenu()
        {
            PerforceCommands.CreateStandardMenu(
                _perforceMenu,
                new Guid("9AB2080A-16F3-4CEC-BE06-004318A31427"),
                new Guid("10DF2D36-A8A0-4696-A863-297D2DD8E27A"),
                new Guid("7EC35DB5-3B7B-411F-8CD6-9EAA6AF32CDF"),
                new Guid("40FEB9F7-573F-4060-AED1-1A6598128124"),
                new Guid("79ACC3C8-CCD5-4CA4-9048-C9F025FB927D"),
                new Guid("810F8DC5-6497-46F5-B184-E46428C2D8CF"));
        }

        /// <summary>
        /// 
        /// </summary>
        private void AddCommandInstancesToProjectContextMenu()
        {
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.BuildSelectedProjects,
                index++,
                false,
                "Build",
                new Guid("65C54808-3EF6-4827-848A-D96B50838276"),
                ProjectCommandIds.ProjectContextMenu);

            Guid menuId = new Guid("750B88B2-1C3B-4495-AE88-6EB04BE7F5B8");
            RockstarCommandManager.AddMenu(
                index++, true, "Add", menuId, ProjectCommandIds.ProjectContextMenu);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.ReloadProject,
                index++,
                true,
                "Reload",
                new Guid("D9D5EAFF-4F99-4582-A61E-70D6FB2B8B7C"),
                ProjectCommandIds.ProjectContextMenu);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.UnloadProject,
                index++,
                false,
                "Unload",
                new Guid("0E189D61-C7DB-45EC-A567-14FFF71C9BF5"),
                ProjectCommandIds.ProjectContextMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Delete,
                index++,
                true,
                "Delete",
                new Guid("A36836A4-F185-4A4C-8B3D-7DED7ECD6D25"),
                ProjectCommandIds.ProjectContextMenu);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.OpenProjectProperties,
                index++,
                true,
                "Properties",
                new Guid("DDBC84AB-44FB-48D6-92C5-67D0C8118B2B"),
                ProjectCommandIds.ProjectContextMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.EditDialogueConfig,
                index++,
                false,
                "Edit Dialogue Config",
                new Guid("F1D5F268-C628-412B-B129-C2E8413D499E"),
                ProjectCommandIds.ProjectContextMenu);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddNewItem,
                0,
                false,
                "New Item...",
                new Guid("AEEA8E27-CC34-4C8B-9DC0-08A66656CE7B"),
                menuId);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddExistingItem,
                1,
                false,
                "Existing Item...",
                new Guid("F36834F5-7C3F-4107-97EA-4D0636669888"),
                menuId);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddProjectFolder,
                2,
                false,
                "New Folder...",
                new Guid("99C0DACD-021F-4C74-B59C-5785775A2B58"),
                menuId);

            Guid perforceMenu = new Guid("87C0372E-403D-4D67-9F9D-B9A41901B707");
            RockstarCommandManager.AddMenu(
                index++,
                "Perforce",
                perforceMenu,
                ProjectCommandIds.ProjectContextMenu);

            PerforceCommands.CreateStandardMenu(
                perforceMenu,
                new Guid("8DA0D781-908A-404E-A5C0-7E9CBB4E0BBE"),
                new Guid("BD450321-AA7E-4BC5-A770-78D5F6E4B59A"),
                new Guid("301595ED-F2B1-44BD-8744-3A3DF7556E7B"),
                new Guid("33C8F10A-4523-4479-8919-49960C3B3422"),
                new Guid("C94A1007-210F-4B97-B841-D23380E65DA0"),
                new Guid("822CBBCF-F667-4AB7-8B17-5BDDD9FA25E2"));
        }

        /// <summary>
        /// 
        /// </summary>
        private void AddCommandInstancesToProjectFolderContextMenu()
        {
            ushort index = 0;
            Guid menuId = new Guid("EE30723F-34FE-456A-AB49-DAF08C0C2AF3");
            RockstarCommandManager.AddMenu(
                index++, "Add", menuId, ProjectCommandIds.ProjectFolderContextMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Delete,
                index++,
                true,
                "Delete",
                new Guid("2B04D2DB-B9A9-4E17-B745-23CB8CCFF00E"),
                ProjectCommandIds.ProjectFolderContextMenu);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddNewItem,
                0,
                false,
                "New Item...",
                new Guid("9D64AFCB-BD45-4E51-8AEB-025285AC4AAF"),
                menuId);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddExistingItem,
                1,
                false,
                "Existing Item...",
                new Guid("8BF26F61-C9E2-4271-BEF0-D564AF6157D4"),
                menuId);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddProjectFolder,
                2,
                false,
                "New Folder...",
                new Guid("12E65B4B-4C19-41B7-B08E-41E2E20BD8A9"),
                menuId);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Rename,
                index++,
                false,
                "Rename",
                new Guid("FBF8C124-3C04-4A67-8F6E-799D0077C2E8"),
                ProjectCommandIds.ProjectFolderContextMenu);
        }

        /// <summary>
        /// 
        /// </summary>
        private void AddCommandInstancesToProjectItemContextMenu()
        {
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.OpenFileFromProject,
                index++,
                false,
                "Open",
                new Guid("31517816-8920-47A7-8620-25E392ACF4D3"),
                ProjectCommandIds.ProjectItemContextMenu);

            Guid perforceMenu = new Guid("18B02A2B-A6C6-496A-8C1F-5A09CC556A57");
            RockstarCommandManager.AddMenu(
                index++,
                "Perforce",
                perforceMenu,
                ProjectCommandIds.ProjectItemContextMenu);

            PerforceCommands.CreateStandardMenu(
                perforceMenu,
                new Guid("AD25390A-479B-47AB-9669-DA688138CD00"),
                new Guid("2BEEE766-CDC7-45B7-B394-561BB6ABDE14"),
                new Guid("F9EAA22D-1496-4FFA-9045-E1C43678F88C"),
                new Guid("161C8E02-BFC9-44E3-BC51-CEC917AA5A93"),
                new Guid("ABC6C71D-A818-4B4A-A4F8-690C22FABF12"),
                new Guid("BF5991A7-8B3F-4B48-98CD-2585652F18BF"));
        }
        
        /// <summary>
        /// 
        /// </summary>
        private void AddCommandInstancesToTextMenu()
        {
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                TextCommands.AddNewLine,
                index++,
                false,
                "Add New Line",
                new Guid("19682EE1-8553-4A8A-895C-420A9E94BD90"),
                _textMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.InsertNewLine,
                index++,
                false,
                "Insert New Line",
                new Guid("590ED40D-2775-4DA7-8862-3600F3621C24"),
                _textMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.AddNewConversation,
                index++,
                true,
                "Add New Conversation",
                new Guid("498215C7-A5CE-474F-B7CB-6BD72158E7DD"),
                _textMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.InsertNewConversation,
                index++,
                false,
                "Insert New Conversation",
                new Guid("549DDACF-4444-41FB-9017-221D1017A042"),
                _textMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.GenerateFilenamesForConversations,
                index++,
                true,
                "Generate Filenames For All Conversations",
                new Guid("85E8E726-F1AB-48A3-8C71-1DF9917F1FFB"),
                _textMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.GenerateFilenamesForConversation,
                index++,
                false,
                "Generate Filenames For Current Conversation",
                new Guid("AE83C3BB-39D5-49F8-B238-DFEF6FB99A1D"),
                _textMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.MoveConversationUp,
                index++,
                true,
                "Move Conversation Up",
                new Guid("6B3D93F6-49BB-43FB-A7AB-0C005601CEC1"),
                _textMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.MoveConversationDown,
                index++,
                false,
                "Move Conversation Down",
                new Guid("A4999ACE-00F5-48AF-BC8A-5B5C2DB4D214"),
                _textMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.MoveLineUp,
                index++,
                false,
                "Move Line Up",
                new Guid("8A0D66AD-3766-4B45-B673-E4B97389744B"),
                _textMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.MoveLineDown,
                index++,
                false,
                "Move Line Down",
                new Guid("7D6EE0A9-A1A0-48C8-913A-4B84EA369B0B"),
                _textMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.PlayLine,
                index++,
                true,
                "Play Line",
                new Guid("E1824002-33EB-443B-A2BC-8F0441752628"),
                _textMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.PlayConversation,
                index++,
                false,
                "Play Conversation",
                new Guid("66A00750-B34D-400C-8B17-66FCF1874E0D"),
                _textMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.StopPlaying,
                index++,
                false,
                "Stop Playing",
                new Guid("D3B1BA48-2EDA-4826-9498-32815640C590"),
                _textMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.ValidateSfxLines,
                index++,
                true,
                "Validate SFX Lines",
                new Guid("82C8B67C-07A5-4998-9277-63595B823CE3"),
                _textMenu);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.ValidateAgainstFolder,
                index++,
                false,
                "Validate Against Folder",
                new Guid("0940403F-7B1E-4595-8573-78C05C5B2FA6"),
                _textMenu);
            
            RockstarCommandManager.AddCommandInstance(
                TextCommands.FilterConversations,
                index++,
                true,
                "Filter Conversations...",
                new Guid("8F270971-1607-4F38-8813-D57EBBCE2092"),
                _textMenu,
                new CommandInstanceCreationArgs()
                {
                    AreMultiItemsShown = true
                });

            RockstarCommandManager.AddCommandInstance(
                TextCommands.FilterLines,
                index++,
                true,
                "Filter Lines...",
                new Guid("AC84ECC7-1590-4686-9B83-3D036139BCFF"),
                _textMenu,
                new CommandInstanceCreationArgs()
                {
                    AreMultiItemsShown = true
                });
        }

        /// <summary>
        /// Creates Standard toolbar
        /// </summary>
        private void AddCommandInstancesToStandardToolBar()
        {
            RockstarCommandManager.AddCommandDefinition(
                new ButtonCommand(RockstarCommands.Undo));
            RockstarCommandManager.AddCommandDefinition(
                new ButtonCommand(RockstarCommands.Redo));

            RockstarCommandManager.AddToolbar(0, 0, "Standard", _standardToolbar);

            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Save,
                index++,
                true,
                "Save Selected Items",
                new Guid("7EB809AB-34BB-47D6-BDAD-D01B9E345A39"),
                _standardToolbar);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.SaveAll,
                index++,
                false,
                "Save All",
                new Guid("5E643486-E053-4244-A081-FAC6CFB851DC"),
                _standardToolbar);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Undo,
                index++,
                true,
                "Undo",
                new Guid("0D4A58D4-7A9C-4AA1-98D9-A5945159DD5D"),
                _standardToolbar);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Redo,
                index++,
                false,
                "Redo",
                new Guid("EF61DAA2-87E7-4D7F-BDA7-D27934F72E3E"),
                _standardToolbar);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Find,
                index++,
                true,
                "Find Text...",
                new Guid("908240AF-5A09-4D82-B7E6-D314A3D662F8"),
                _standardToolbar,
                new CommandInstanceCreationArgs() { Width = 200 });

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.FindPrevious,
                index++,
                false,
                "Find Previous",
                new Guid("CE6F657E-421F-48A8-80A0-CF7929662F5E"),
                _standardToolbar,
                new CommandInstanceCreationArgs());

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.FindNext,
                index++,
                false,
                "Find Next",
                new Guid("9DA895BC-E17E-4551-BB32-FBA51EBD1708"),
                _standardToolbar,
                new CommandInstanceCreationArgs());
        }

        /// <summary>
        /// Creates text command toolbar
        /// </summary>
        private void AddCommandInstancesToTextToolBar()
        {
            RockstarCommandManager.AddToolbar(0, 0, "Text", _textToolbar);
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                TextCommands.AddNewLine,
                index++,
                false,
                "Add New Line",
                new Guid("1C6A8D09-FC99-418E-BD83-3B0E96D221B3"),
                _textToolbar);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.InsertNewLine,
                index++,
                false,
                "Insert New Line",
                new Guid("4E575CCB-B161-4ADA-9573-408DB74B985B"),
                _textToolbar);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.AddNewConversation,
                index++,
                true,
                "Add New Conversation",
                new Guid("1FADC01E-F4A2-422E-9973-980DDC86C1F7"),
                _textToolbar);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.InsertNewConversation,
                index++,
                false,
                "Insert New Conversation",
                new Guid("A8EE8F94-5772-4454-B914-15138FFA3B82"),
                _textToolbar);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.GenerateFilenamesForConversations,
                index++,
                true,
                "Generate Filenames For All Conversations",
                new Guid("02BC660D-8A8B-4D94-9096-7A790EFD12C8"),
                _textToolbar);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.GenerateFilenamesForConversation,
                index++,
                false,
                "Generate Filenames For Current Conversation",
                new Guid("E6AFCDE6-0AEB-44E8-8596-E88500322BAF"),
                _textToolbar);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.PlayLine,
                index++,
                true,
                "Play Line",
                new Guid("BD9F931C-B1B8-439A-A7FE-12F2C0C9D5C1"),
                _textToolbar);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.PlayConversation,
                index++,
                false,
                "Play Conversation",
                new Guid("4DD4116F-39B2-42A8-960C-CA01326A6E3A"),
                _textToolbar);

            RockstarCommandManager.AddCommandInstance(
                TextCommands.StopPlaying,
                index++,
                false,
                "Stop Playing",
                new Guid("5897A05E-65FF-4746-9194-A5840260E42E"),
                _textToolbar);
        }

        /// <summary>
        /// Attaches all of the necessary command bindings to this instance.
        /// </summary>
        private void AttachCommandBindings()
        {
            OpenFileFromProjectAction openFileAction = new OpenFileFromProjectAction(this.ProjectCommandResolver);
            openFileAction.AddBinding(ProjectCommands.OpenFileFromProject, this);

            ICommandAction action = new CloseAllButActiveDocumentAction(this.ProjectCommandResolver);
            action.AddBinding(DockingCommands.CloseAllButItem, this);

            action = new CloseAllDocumentsAction(this.ProjectCommandResolver);
            action.AddBinding(DockingCommands.CloseAll, this);

            action = new CloseDocumentAction(this.CloseDocumentCmdResolver);
            action.AddBinding(DockingCommands.CloseItem, this);

            action = new SaveAllAction(this.ProjectCommandResolver);
            action.AddBinding(RockstarCommands.SaveAll, this);

            action = new SaveDocumentAction(this.ProjectCommandResolver);
            action.AddBinding(RockstarCommands.Save, typeof(RsDocumentPane));

            action = new SaveDocumentAsAction(this.ProjectCommandResolver);
            action.AddBinding(RockstarCommands.SaveAs, typeof(RsDocumentPane));

            OpenCollectionAction openCollectionAction = new OpenCollectionAction(this.ProjectCommandResolver);
            openCollectionAction.AddBinding(ProjectCommands.OpenProjectCollectionFile, this);
            openCollectionAction.CollectionOpened += this.OnCollectionOpened;

            action = new OpenRecentProjectAction(this.ProjectCommandResolver, openCollectionAction);
            action.AddBinding(RockstarCommands.OpenProjectFromMru, this);

            action = new CloseCollectionAction(this.ProjectCommandResolver);
            action.AddBinding(ProjectCommands.CloseProjectCollection, this);

            action = new AddExistingItemAction(this.ProjectCommandResolver);
            action.AddBinding(ProjectCommands.AddExistingItem, this);

            action = new AddNewItemAction(this.ProjectCommandResolver);
            action.AddBinding(ProjectCommands.AddNewItem, this);

            action = new AddProjectFolderAction(this.ProjectCommandResolver);
            action.AddBinding(ProjectCommands.AddProjectFolder, this);

            action = new AddCollectionFolderAction(this.ProjectCommandResolver);
            action.AddBinding(ProjectCommands.AddCollectionFolder, this);

            action = new AddNewProjectAction(this.ProjectCommandResolver);
            action.AddBinding(ProjectCommands.AddNewProject, this);

            action = new AddExistingProjectAction(this.ProjectCommandResolver);
            action.AddBinding(ProjectCommands.AddExistingProject, this);

            action = new CreateNewProjectAction(this.ProjectCommandResolver);
            action.AddBinding(ProjectCommands.CreateNewProject, this);

            action = new OpenProjectAction(this.ProjectCommandResolver);
            action.AddBinding(ProjectCommands.OpenProjectFile, this);
            ((OpenProjectAction)action).ProjectOpened += this.OnProjectOpened;

            action = new ReloadProjectAction(this.ProjectCommandResolver);
            action.AddBinding(ProjectCommands.ReloadProject, this);

            action = new UnloadProjectAction(this.ProjectCommandResolver);
            action.AddBinding(ProjectCommands.UnloadProject, this);

            action = new OpenProjectPropertiesAction(this.ProjectCommandResolver);
            action.AddBinding(ProjectCommands.OpenProjectProperties, this);

            action = new BuildSelectedProjectsAction(this.ProjectCommandResolver);
            action.AddBinding(ProjectCommands.BuildSelectedProjects, this);

            action = new BuildAllProjectsAction(this.ProjectCommandResolver);
            action.AddBinding(ProjectCommands.BuildAllProjects, this);

            action = new EditDialogueConfigAction(this.ProjectCommandResolver);
            action.AddBinding(TextCommands.EditDialogueConfig, this);

            action = new EditAction(this.PerforceCommandResolver);
            action.AddBinding(PerforceCommands.Edit, typeof(RsDocumentItem));

            action = new AddAction(this.PerforceCommandResolver);
            action.AddBinding(PerforceCommands.Add, typeof(RsDocumentItem));

            action = new OpenRecentFileAction(this.ProjectCommandResolver);
            action.AddBinding(RockstarCommands.OpenFileFromMru, this);

            action = new DocumentConversationExportAction(this.ProjectCommandResolver);
            action.AddBinding(TextCommands.ExportDocumentConversationsToExcel, this);

            action = new AllDocumentConversationExportAction(this.ProjectCommandResolver);
            action.AddBinding(TextCommands.ExportAllOpenConversationsToExcel, this);

            action = new ProjectConversationExportAction(this.ProjectCommandResolver);
            action.AddBinding(TextCommands.ExportProjectConversationsToExcel, this);

            action = new DocumentLineExportAction(this.ProjectCommandResolver);
            action.AddBinding(TextCommands.ExportDocumentLinesToExcel, this);

            action = new AllOpenLineExportAction(this.ProjectCommandResolver);
            action.AddBinding(TextCommands.ExportAllOpenLinesToExcel, this);

            action = new ProjectLineExportAction(this.ProjectCommandResolver);
            action.AddBinding(TextCommands.ExportProjectLinesToExcel, this);

            RockstarCommandManager.AddCommandDefinition(
                new ButtonCommand(ProjectCommands.LocateInExplorer));

            this.CommandBindings.Add(
                new CommandBinding(ProjectCommands.LocateInExplorer, this.OnLocateInExplorer));

            FallbackSearchAction searchAction = new FallbackSearchAction(this.SearchResolver);
            searchAction.AddBinding(this);

            this.AttachCommandBindingsToExplorer();

            RockstarCommandManager.AddBinding(
                this,
                TextCommands.ExportAllOpenLinesToWord,
                this.OnExportAllOpenLinesToWord,
                this.CanExportAllOpenLinesToWord);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnProjectOpened(object sender, ProjectOpenedEventArgs e)
        {
            RsApplication app = Application.Current as RsApplication;
            if (app != null)
            {
                app.AddToMruList("Projects", e.Project.FullPath, new string[] { "Project" });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnCollectionClosing(object sender, CollectionClosedEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(e.ProjectCollection.FullPath))
            {
                return;
            }

            IDocumentManagerService service = sender as IDocumentManagerService;
            Dictionary<string, int> openedDocuments = new Dictionary<string, int>();
            int index = 0;
            foreach (var openedDocument in service.OpenedDocuments)
            {
                openedDocuments[openedDocument.FullPath] = index++;
            }

            e.ProjectCollection.SetOpenedDocuments(openedDocuments);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnCollectionOpened(object sender, CollectionOpenedEventArgs e)
        {
            RsApplication app = Application.Current as RsApplication;
            MainWindowDataContext dc = this.DataContext as MainWindowDataContext;
            if (app != null && dc != null)
            {
                app.AddToMruList("Projects", e.FullPath, new string[0]);

                string temp = Path.GetTempPath();
                string path = Path.Combine(temp, "Rockstar Games", app.UniqueName);
                path = Path.Combine(path, "Backup Files", e.ProjectCollection.Name);
                this._fileBackup.BackupDirectory = path;

                if (!e.AlreadyOpened)
                {
                    // Need to open the previously opened documents.
                    foreach (string filename in e.ProjectCollection.PreviouslyOpenedDocuments)
                    {
                        FileNode node = e.ProjectCollection.FindNodeWithFullPath(filename) as FileNode;
                        if (node == null)
                        {
                            continue;
                        }

                        OpenFileFromProjectAction action = new OpenFileFromProjectAction(app);
                        BuildProjectCommandArgs args = new BuildProjectCommandArgs();
                        args.CollectionNode = dc.CollectionNode;
                        args.NodeSelectionDelegate = this.SelectNode;
                        args.NodeRenameDelegate = this.RenameNode;
                        args.OpenedDocuments = dc.ViewSite.AllDocuments;
                        args.ViewSite = dc.ViewSite;
                        args.ProjectBuilder = new ProjectBuilder();
                        if (this._explorer != null)
                        {
                            args.SelectedNodes = this._explorer.SelectedItems;
                        }

                        action.Execute(args, node);
                    }
                }
            }
        }

        /// <summary>
        /// Attaches all of the necessary command bindings to this instance or a control
        /// property of this instance.
        /// </summary>
        private void AttachCommandBindingsToExplorer()
        {
            if (this._explorer == null)
            {
                throw new InvalidOperationException();
            }

            ICommandAction action = new SaveItemAction(this.ProjectCommandResolver);
            action.AddBinding(RockstarCommands.Save, this._explorer);

            action = new SaveItemAsAction(this.ProjectCommandResolver);
            action.AddBinding(RockstarCommands.SaveAs, this._explorer);

            action = new DeleteItemAction(this.ProjectCommandResolver);
            action.AddBinding(RockstarCommands.Delete, this._explorer);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        private bool CanExportAllOpenLinesToWord(CanExecuteCommandData data)
        {
            MainWindowDataContext dc = this.DataContext as MainWindowDataContext;
            if (dc == null)
            {
                return false;
            }

            return dc.ViewSite.AllDocuments.Any();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnDocumentClosing(object sender, DocumentClosingEventArgs e)
        {
            App app = Application.Current as App;
            ProjectCollectionNode collection = this._dataContext.CollectionNode;
            if (String.IsNullOrWhiteSpace(collection.FullPath))
            {
                return;
            }

            ProjectDocumentItem projectDocument = e.DocumentItem as ProjectDocumentItem;
            if (projectDocument == null)
            {
                return;
            }

            DialogueFileControl control = e.DocumentItem.Content as DialogueFileControl;
            if (control == null || !control.HasBeenLoaded)
            {
                return;
            }

            string temp = Path.GetTempPath();
            string path = Path.Combine(temp, "Rockstar Games", app.UniqueName);
            path = Path.Combine(path, "Document States", projectDocument.FileNode.ParentProject.Text);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            string fullPath = Path.Combine(path, projectDocument.FileNode.Text + ".docstate");
            using (MemoryStream stream = new MemoryStream())
            {
                control.SerialiseDocumentState(stream);
                FileMode mode = FileMode.Create;
                FileAccess access = FileAccess.Write;
                using (FileStream file = new FileStream(fullPath, mode, access))
                {
                    stream.WriteTo(file);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnDocumentOpened(object sender, DocumentEventArgs e)
        {
            RsApplication app = Application.Current as RsApplication;
            ProjectDocumentItem projectDocument = e.DocumentItem as ProjectDocumentItem;
            if (app != null && projectDocument != null)
            {
                app.AddToMruList("Files", e.DocumentItem.FullPath, new string[] { projectDocument.FileNode.ProjectNode.FullPath });
            }

            ProjectCollectionNode collection = this._dataContext.CollectionNode;
            if (String.IsNullOrWhiteSpace(collection.FullPath))
            {
                return;
            }

            if (projectDocument == null)
            {
                return;
            }

            DialogueFileControl control = e.DocumentItem.Content as DialogueFileControl;
            if (control == null)
            {
                return;
            }

            string temp = Path.GetTempPath();
            string path = Path.Combine(temp, "Rockstar Games", app.UniqueName);
            path = Path.Combine(path, "Document States", projectDocument.FileNode.ParentProject.Text);
            string fullPath = Path.Combine(path, projectDocument.FileNode.Text + ".docstate");

            if (!File.Exists(fullPath))
            {
                return;
            }

            try
            {
                using (StreamReader stream = new StreamReader(fullPath))
                {
                    control.DeserialiseDocumentState(stream);
                }
            }
            catch (Exception)
            {
                // Ignore any exceptions that are thrown while parsing the layout file.
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data">
        /// 
        /// </param>
        private void OnExportAllOpenLinesToWord(ExecuteCommandData data)
        {
            MainWindowDataContext dc = this.DataContext as MainWindowDataContext;
            if (dc == null)
            {
                return;
            }

            List<Conversation> conversations = new List<Conversation>();
            foreach (DocumentItem item in dc.ViewSite.AllDocuments)
            {
                Dialogue dialogue = ((item.Content as DialogueFileControl).DataContext as DialogueViewModel).Model;
                if (dialogue == null)
                {
                    continue;
                }

                conversations.AddRange(dialogue.Conversations);
            }

            List<DialogueCharacter> characters = new List<DialogueCharacter>();
            List<Guid> characterIds = new List<Guid>();
            foreach (Conversation conversation in conversations)
            {
                DialogueConfigurations config = conversation.Configurations;
                foreach (ILine line in conversation.Lines)
                {
                    if (!characterIds.Contains(line.CharacterId))
                    {
                        characterIds.Add(line.CharacterId);
                        characters.Add(config.GetCharacter(line.CharacterId));
                    }
                }
            }

            ExportCharacterSelectWindow window = new ExportCharacterSelectWindow();
            window.CharacterItems = characters;
            if (window.ShowDialog() != true)
            {
                return;
            }

            WordApplication wordApplication = WordApplicationCreator.Create();
            string name = wordApplication.Name;
            string version = wordApplication.Version;

            WordDocument document = wordApplication.Documents.Add();
            foreach (DocumentItem item in dc.ViewSite.AllDocuments)
            {
                Dialogue dialogue = ((item.Content as DialogueFileControl).DataContext as DialogueViewModel).Model;
                if (dialogue == null)
                {
                    continue;
                }

                document.AppendLine(dialogue.Name, BuiltInStyle.Heading1);
                foreach (Conversation conversation in dialogue.Conversations)
                {
                    List<ExportLine> exportLines = new List<ExportLine>();
                    foreach (ILine line in conversation.Lines)
                    {
                        Tuple<string, bool> charData =
                            window.ExportCharacter(line.CharacterId);
                        if (charData.Item2)
                        {
                            exportLines.Add(new ExportLine(charData.Item1, line.Dialogue));
                        }
                    }

                    document.AppendLine();
                    StringBuilder header = new StringBuilder();
                    header.Append(conversation.Root);
                    header.Append("/");
                    header.Append(conversation.Description);
                    header.Append("/");
                    header.Append(conversation.IsRandom ? "Random" : "Not Random");
                    header.Append("/");
                    header.Append(
                        conversation.IsPlaceholder ? "Placeholder" : "Not Placeholder");

                    document.AppendLine(header.ToString(), true, true);
                    Alignment alignment = Alignment.AlignParagraphCenter;
                    foreach (ExportLine line in exportLines)
                    {
                        document.AppendLine(line.CharacterName, true, true, alignment);
                        document.AppendLine(line.Dialogue, false, false, alignment);
                    }
                }

                document.AppendLine();
            }

            wordApplication.Visible = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnLocateInExplorer(object sender, ExecutedRoutedEventArgs e)
        {
            RsDocumentItem item = e.OriginalSource as RsDocumentItem;
            RsDocumentPane pane = e.Source as RsDocumentPane;
            if (item == null)
            {
                item = pane.SelectedItem as RsDocumentItem;
                if (item == null)
                {
                    DependencyObject container =
                        pane.ItemContainerGenerator.ContainerFromItem(pane.SelectedItem);
                    item = container as RsDocumentItem;
                }

                if (item == null)
                {
                    return;
                }
            }

            ProjectDocumentItem document = item.DataContext as ProjectDocumentItem;
            if (document != null)
            {
                this.SelectNode(document.FileNode);
            }
        }

        /// <summary>
        /// Called whenever the last write time for a registered file gets updated.
        /// </summary>
        /// <param name="fullPath">
        /// The full path of the file that was modified.
        /// </param>
        private void OnModifiedTimeChanged(string fullPath)
        {
            this._modifiedFiles.Add(fullPath);
            IntPtr foreground = User32.GetForegroundWindow();
            foreach (Window window in Application.Current.Windows)
            {
                WindowInteropHelper helper = new WindowInteropHelper(window);
                if (helper == null)
                {
                    continue;
                }

                IntPtr handle = helper.Handle;
                if (foreground.ToInt64() == handle.ToInt64())
                {
                    this.HandleModification();
                    return;
                }
            }

            Application.Current.Activated += this.HandleModificationOnceActivated;
        }

        /// <summary>
        /// Called once the application becomes active so that previous modifications to
        /// registered files can be handled.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.EventArgs containing the event data. Always empty.
        /// </param>
        private void HandleModificationOnceActivated(object sender, EventArgs e)
        {
            Application.Current.Activated -= this.HandleModificationOnceActivated;
            this.HandleModification();
        }

        /// <summary>
        /// Handles the event of registered files being modified outside of the editor
        /// environment.
        /// </summary>
        private void HandleModification()
        {
            MainWindowDataContext dc = this.DataContext as MainWindowDataContext;
            if (dc == null)
            {
                return;
            }

            List<ISaveableDocument> items = new List<ISaveableDocument>();
            items.AddRange(dc.CollectionNode.GetProjects());
            items.AddRange(dc.ViewSite.AllDocuments);

            List<DocumentItem> modifiedDocuments = new List<DocumentItem>();
            List<ProjectNode> modifiedProjects = new List<ProjectNode>();
            while (this._modifiedFiles.Count > 0)
            {
                string fullPath = this._modifiedFiles.First();
                this._modifiedFiles.Remove(fullPath);
                foreach (DocumentItem item in dc.ViewSite.AllDocuments)
                {
                    if (!String.Equals(fullPath, item.FullPath))
                    {
                        continue;
                    }

                    modifiedDocuments.Add(item);
                }

                foreach (ProjectNode item in dc.CollectionNode.GetProjects())
                {
                    if (!String.Equals(fullPath, item.FullPath))
                    {
                        continue;
                    }

                    modifiedProjects.Add(item);
                }
            }

            bool yesToAll = false;
            bool noToAll = false;
            foreach (DocumentItem document in modifiedDocuments)
            {
                if (noToAll)
                {
                    return;
                }

                bool performReload = true;
                if (!yesToAll)
                {
                    string msg = StringTable.FileExternallyModifiedMsg;
                    if (document.IsModified)
                    {
                        msg = StringTable.ModifiedFileExternallyModifiedMsg;
                    }

                    IMessageBox msgBox = new RsMessageBox();
                    msgBox.Text = msg.FormatInvariant(document.FullPath);
                    msgBox.SetIconToExclamation();
                    long yesValueResult = 1;
                    long yesToAllValueResult = 2;
                    long noValueResult = 3;
                    long noToAllValueResult = 4;
                    msgBox.AddButton("Yes", yesValueResult, true, false);
                    msgBox.AddButton("Yes to All", yesToAllValueResult, false, false);
                    msgBox.AddButton("No", noValueResult, false, true);
                    msgBox.AddButton("No to All", noToAllValueResult, false, false);
                    long result = msgBox.ShowMessageBox(this);
                    if (result == yesToAllValueResult)
                    {
                        yesToAll = true;
                    }
                    else if (result == noValueResult)
                    {
                        performReload = false;
                    }
                    else if (result == noToAllValueResult)
                    {
                        performReload = false;
                        noToAll = true;
                    }
                }

                if (!performReload)
                {
                    continue;
                }

                ProjectDocumentItem projectDocument = document as ProjectDocumentItem;
                if (projectDocument != null)
                {
                    FileNode node = projectDocument.FileNode;
                    ICreatesDocumentContent creator = node as ICreatesDocumentContent;
                    if (creator != null)
                    {
                        object content = null;
                        try
                        {
                            IUniversalLog log =
                                LogFactory.CreateUniversalLog("Content Creation");
                            string logFlename = Path.Combine(
                                Path.GetTempPath(), "content_creation");

                            logFlename = Path.ChangeExtension(
                                logFlename, UniversalLogFile.EXTENSION);
                            UniversalLogFile target = new UniversalLogFile(
                                logFlename, FileMode.Create, FlushMode.Never, log);

                            ICreateDocumentContentController controller =
                                creator.CreateDocumentContentController;
                            if (controller != null)
                            {
                                CreateContentArgs e =
                                    new CreateContentArgs(
                                        log, dc.ViewSite, document.UndoEngine);
                                content = controller.CreateContent(node as HierarchyNode, e);
                            }
                        }
                        catch (Exception ex)
                        {
                            RsMessageBox.Show(
                                StringTable.FailedFileParsing.FormatInvariant(ex.Message),
                                null,
                                MessageBoxButton.OK,
                                MessageBoxImage.Error);
                        }

                        document.UndoEngine.ResetAllStacks();
                        document.Content = new DialogueFileControl(content as DialogueViewModel);
                    }
                }
            }

            foreach (ProjectNode project in modifiedProjects)
            {
                if (noToAll)
                {
                    return;
                }

                bool performReload = true;
                if (!yesToAll)
                {
                    string msg = StringTable.FileExternallyModifiedMsg;
                    if (project.IsModified)
                    {
                        msg = StringTable.ModifiedFileExternallyModifiedMsg;
                    }

                    IMessageBox msgBox = new RsMessageBox();
                    msgBox.Text = msg.FormatInvariant(project.FullPath);
                    msgBox.SetIconToExclamation();
                    long yesValueResult = 1;
                    long yesToAllValueResult = 2;
                    long noValueResult = 3;
                    long noToAllValueResult = 4;
                    msgBox.AddButton("Yes", yesValueResult, true, false);
                    msgBox.AddButton("Yes to All", yesToAllValueResult, false, false);
                    msgBox.AddButton("No", noValueResult, false, true);
                    msgBox.AddButton("No to All", noToAllValueResult, false, false);
                    long result = msgBox.ShowMessageBox(this);
                    if (result == yesToAllValueResult)
                    {
                        yesToAll = true;
                    }
                    else if (result == noValueResult)
                    {
                        performReload = false;
                    }
                    else if (result == noToAllValueResult)
                    {
                        performReload = false;
                        noToAll = true;
                    }
                }

                if (!performReload)
                {
                    continue;
                }

                Application app = Application.Current;
                ICommandServiceProvider provider = app as ICommandServiceProvider;
                ProjectCommandArgs args = new ProjectCommandArgs();
                args.SelectedNodes = Enumerable.Repeat(project, 1);
                args.OpenedDocuments = dc.ViewSite.AllDocuments;
                args.ViewSite = dc.ViewSite;

                List<string> openedFiles = new List<string>();
                foreach (DocumentItem document in dc.ViewSite.AllDocuments)
                {
                    ProjectDocumentItem projectDocument = document as ProjectDocumentItem;
                    if (projectDocument == null)
                    {
                        continue;
                    }

                    FileNode node = projectDocument.FileNode;
                    if (node == null)
                    {
                        continue;
                    }

                    if (!Object.ReferenceEquals(project, node.ParentProject))
                    {
                        continue;
                    }

                    openedFiles.Add(projectDocument.FullPath);
                }

                try
                {
                    UnloadProjectAction unloadAction = new UnloadProjectAction(provider);
                    unloadAction.Execute(args);

                    ReloadProjectAction reloadAction = new ReloadProjectAction(provider);
                    reloadAction.Execute(args);
                }
                catch (OperationCanceledException)
                {
                    return;
                }

                List<IHierarchyNode> openedNodes = new List<IHierarchyNode>();
                foreach (string openedFile in openedFiles)
                {
                    IHierarchyNode node = project.FindNodeWithFullPath(openedFile);
                    if (node == null)
                    {
                        continue;
                    }

                    openedNodes.Add(node);
                }

                args.SelectedNodes = openedNodes;
                args.OpenedDocuments = dc.ViewSite.AllDocuments;
                args.ViewSite = dc.ViewSite;
                OpenFileFromProjectAction openAction = new OpenFileFromProjectAction(provider);
                openAction.Execute(args, null);
            }
        }

        /// <summary>
        /// Called whenever the last write time for a registered file gets updated.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The RSG.Editor.ModifiedTimeChangedEventArgs containing the event data including the
        /// full path to the file that has been modified.
        /// </param>
        private void OnModifiedTimeChanged(object sender, ModifiedTimeChangedEventArgs e)
        {
            DispatcherHelper.CheckBeginInvokeOnUI(
                new Action(
               delegate
               {
                   this.OnModifiedTimeChanged(e.FullPath);
               }));
        }

        /// <summary>
        /// Called whenever the read-only flag for a registered document changes.
        /// </summary>
        /// <param name="fullPath">
        /// The full path of the file whose read-only flag was changed.
        /// </param>
        /// <param name="newValue">
        /// The new value of the read-only flag.
        /// </param>
        private void OnReadOnlyFlagChanged(string fullPath, bool newValue)
        {
            MainWindowDataContext dc = this.DataContext as MainWindowDataContext;
            if (dc == null || dc.ViewSite == null)
            {
                return;
            }

            IEnumerable<DocumentItem> documents = dc.ViewSite.AllDocuments;
            if (documents == null)
            {
                return;
            }

            foreach (ProjectDocumentItem item in documents)
            {
                if (!String.Equals(item.FullPath, fullPath))
                {
                    continue;
                }

                item.IsReadOnly = newValue;
            }
        }

        /// <summary>
        /// Called whenever the read-only flag for a registered document changes.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The RSG.Editor.ReadOnlyChangedEventArgs containing the event data including the
        /// full path to the file that has been modified.
        /// </param>
        private void OnReadOnlyFlagChanged(object sender, ReadOnlyChangedEventArgs e)
        {
            DispatcherHelper.CheckBeginInvokeOnUI(
                new Action(
                        delegate
                        {
                            this.OnReadOnlyFlagChanged(e.FullPath, e.NewValue);
                        }));
        }

        /// <summary>
        /// Called whenever a project failed to load either the first time or every time the
        /// user tries to reload it.
        /// </summary>
        /// <param name="sender">
        /// The collection this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The RSG.Project.ViewModel.ProjectLoadFailedEventArgs containing the event data.
        /// </param>
        private void OnProjectFailed(object sender, ProjectLoadFailedEventArgs e)
        {
            if (this.Dispatcher.CheckAccess())
            {
                ProjectNode node = e.Project;

                string name = Path.GetFileNameWithoutExtension(node.Model.Include);
                node.Text = name + " (load failed)";
                e.Project.NotifyPropertyChanged("Icon");
                e.Project.ClearChildren();
                HierarchyNode failedNode = new HierarchyNode();
                failedNode.Text = StringTable.ProjectLoadFailedNodeText;
                node.AddChild(failedNode);
                this.SelectNode(e.Project);

                if (e.Reloaded == true)
                {
                    RsMessageBox.Show(
                        e.Exception.Message,
                        String.Empty,
                        MessageBoxButton.OK,
                        MessageBoxImage.Error);
                }
            }
            else
            {
                this.Dispatcher.BeginInvoke(
                    new Action(
                        delegate
                        {
                            this.OnProjectFailed(sender, e);
                        }));
            }
        }

        /// <summary>
        /// Called whenever a project is loaded in the attached collection.
        /// </summary>
        /// <param name="sender">
        /// The collection this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The RSG.Project.ViewModel.ProjectLoadedStateChangedEventArgs containing the event
        /// data.
        /// </param>
        private void OnProjectLoaded(object sender, ProjectLoadedStateChangedEventArgs e)
        {
            lock (this._fileWatcher)
            {
                this._fileWatcher.RegisterFile(e.Project.FullPath);
            }
        }

        /// <summary>
        /// Called whenever a project is unloaded or removed from the attached collection.
        /// </summary>
        /// <param name="sender">
        /// The collection this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The RSG.Project.ViewModel.ProjectLoadedStateChangedEventArgs containing the event
        /// data.
        /// </param>
        private void OnProjectUnloaded(object sender, ProjectLoadedStateChangedEventArgs e)
        {
            lock (this._fileWatcher)
            {
                this._fileWatcher.UnregisterFile(e.Project.FullPath);
            }
        }

        private void OnProjectSaved(object sender, ProjectLoadedStateChangedEventArgs e)
        {
            RsApplication app = Application.Current as RsApplication;
            if (app != null)
            {
                app.AddToMruList("Projects", e.Project.FullPath, new string[] { "Project" });
            }
        }

        private void OnProjectCollectionSaved(object sender, EventArgs e)
        {
            RsApplication app = Application.Current as RsApplication;
            if (app != null)
            {
                ProjectCollectionNode node = sender as ProjectCollectionNode;
                if (node != null && node.FullPath != null)
                {
                    app.AddToMruList("Projects", node.FullPath, new string[0]);
                }
            }
        }

        /// <summary>
        /// Shows the rename edit control for the specified hierarchy node inside the project
        /// explorer.
        /// </summary>
        /// <param name="node">
        /// The node that should be renamed.
        /// </param>
        private void RenameNode(IHierarchyNode node)
        {
            if (node == null)
            {
                return;
            }

            DispatcherHelper.CheckBeginInvokeOnUI(
                new Action(
                delegate
                {
                    if (this._explorer != null)
                    {
                        this._explorer.RenameItem(node);
                    }
                }));
        }

        /// <summary>
        /// Retrieves the command argument used for the search action command on this control.
        /// </summary>
        /// <param name="commandData">
        /// The command data that has requested the argument.
        /// </param>
        /// <returns>
        /// Always returns null.
        /// </returns>
        private DialogueFileControl SearchResolver(CommandData commandData)
        {
            return null;
        }

        /// <summary>
        /// Selects the specified hierarchy node inside the project explorer.
        /// </summary>
        /// <param name="node">
        /// The node that should be selected.
        /// </param>
        private void SelectNode(IHierarchyNode node)
        {
            if (node == null)
            {
                return;
            }

            if (this.Dispatcher.CheckAccess())
            {
                if (this._explorer != null)
                {
                    this._explorer.SelectItem(node);
                }
            }
            else
            {
                this.Dispatcher.BeginInvoke(
                    new Action(
                        delegate
                        {
                            this.SelectNode(node);
                        }));
            }
        }
        #endregion Methods

        #region Structures
        /// <summary>
        /// 
        /// </summary>
        private struct ExportLine
        {
            /// <summary>
            /// 
            /// </summary>
            /// <param name="characterName"></param>
            /// <param name="dialogue"></param>
            public ExportLine(string characterName, string dialogue)
                : this()
            {
                this.CharacterName = characterName;
                this.Dialogue = dialogue;
            }

            /// <summary>
            /// 
            /// </summary>
            public string CharacterName
            {
                get;
                set;
            }

            /// <summary>
            /// 
            /// </summary>
            public string Dialogue
            {
                get;
                set;
            }
        } // DialogueFileControl.ExportLine {Structure}
        #endregion Structures
    } // DialogueStar.MainWindow {Class}
} // DialogueStar {Namespace}
