﻿//---------------------------------------------------------------------------------------------
// <copyright file="DialogueStarIcons.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace DialogueStar.Resources
{
    using System;
    using System.Windows.Media.Imaging;
    using RSG.Base.Extensions;

    /// <summary>
    /// Provides static properties that give access to common icons that can be used throughout
    /// a application.
    /// </summary>
    public static class DialogueStarIcons
    {
        #region Fields

        /// <summary>
        /// The private field used for the <see cref="DialogueProjectItemIcon"/> property.
        /// </summary>
        private static BitmapSource _dialogueProjectItemIcon;

        /// <summary>
        /// A generic object that provides thread safety access to the image sources.
        /// </summary>
        private static object _syncRoot = new object();

        /// <summary>
        /// The private field used for the <see cref="TextProjectIcon"/> property.
        /// </summary>
        private static BitmapSource _textProjectIcon;
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets the icon that is used for the dialogue project items definition.
        /// </summary>
        public static BitmapSource DialogueProjectItemIcon
        {
            get
            {
                if (_dialogueProjectItemIcon == null)
                {
                    lock (_syncRoot)
                    {
                        if (_dialogueProjectItemIcon == null)
                        {
                            EnsureLoaded(
                                ref _dialogueProjectItemIcon, "dialogueProjectItemIcon");
                        }
                    }
                }

                return _dialogueProjectItemIcon;
            }
        }

        /// <summary>
        /// Gets the icon that is used for the text project definition.
        /// </summary>
        public static BitmapSource TextProjectIcon
        {
            get
            {
                if (_textProjectIcon == null)
                {
                    lock (_syncRoot)
                    {
                        if (_textProjectIcon == null)
                        {
                            EnsureLoaded(ref _textProjectIcon, "textProjectIcon");
                        }
                    }
                }

                return _textProjectIcon;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Ensures that the specified image source is loaded with the specified resource name.
        /// </summary>
        /// <param name="source">
        /// The image source that should be loaded.
        /// </param>
        /// <param name="resourceName">
        /// The name of the resource the loaded image source should set as its source.
        /// </param>
        private static void EnsureLoaded(ref BitmapSource source, string resourceName)
        {
            if (source != null)
            {
                return;
            }

            string assemblyName = typeof(DialogueStarIcons).Assembly.GetName().Name;
            string path = "Resources/" + resourceName + ".png";
            string bitmapPath = "pack://application:,,,/{0};component/{1}";
            bitmapPath = bitmapPath.FormatInvariant(assemblyName, path);
            Uri uri = new Uri(bitmapPath, UriKind.RelativeOrAbsolute);

            source = new BitmapImage(uri);
            source.Freeze();
        }
        #endregion Methods
    } // DialogueStar.Resources.DialogueStarIcons {Class}
} // DialogueStar.Resources {Namespace}
