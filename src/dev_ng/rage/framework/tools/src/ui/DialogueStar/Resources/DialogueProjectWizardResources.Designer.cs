﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DialogueStar.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class DialogueProjectWizardResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal DialogueProjectWizardResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("DialogueStar.Resources.DialogueProjectWizardResources", typeof(DialogueProjectWizardResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Browse....
        /// </summary>
        public static string BrowseButtonContent {
            get {
                return ResourceManager.GetString("BrowseButtonContent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        public static string CancelButtonContent {
            get {
                return ResourceManager.GetString("CancelButtonContent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Configuration Path:.
        /// </summary>
        public static string ConfigPathLabel {
            get {
                return ResourceManager.GetString("ConfigPathLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please choose an existing (.dstarconfig) configuration file to use for this project or choose to upgrade an existing (.xml) configuration file that was being used by the old version of Dialogue Star..
        /// </summary>
        public static string Introduction {
            get {
                return ResourceManager.GetString("Introduction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Converted Configuration Path:.
        /// </summary>
        public static string NewConfigPathLabel {
            get {
                return ResourceManager.GetString("NewConfigPathLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to OK.
        /// </summary>
        public static string OkButtonContent {
            get {
                return ResourceManager.GetString("OkButtonContent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Old Format File:.
        /// </summary>
        public static string OldConfigPathLabel {
            get {
                return ResourceManager.GetString("OldConfigPathLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Upgrade Old Format File.
        /// </summary>
        public static string UpgradeButtonContent {
            get {
                return ResourceManager.GetString("UpgradeButtonContent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Use Local File.
        /// </summary>
        public static string UseLocalFileButtonContent {
            get {
                return ResourceManager.GetString("UseLocalFileButtonContent", resourceCulture);
            }
        }
    }
}
