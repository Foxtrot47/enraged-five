﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProjectConversationExportAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace DialogueStar.Commands
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using RSG.Editor;
    using RSG.Editor.Controls.Dock.ViewModel;
    using RSG.Interop.Microsoft.Office.Excel;
    using RSG.Project.Commands;
    using RSG.Project.Model;
    using RSG.Project.ViewModel;
    using RSG.Text.Model;
    using RSG.Text.ViewModel;

    /// <summary>
    /// Contains the logic for the
    /// <see cref="TextCommands.ExportProjectConversationsToExcel"/> routed command. This
    /// class cannot be inherited.
    /// </summary>
    public sealed class ProjectConversationExportAction : ButtonAction<ProjectCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ProjectConversationExportAction"/>
        /// class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public ProjectConversationExportAction(ProjectCommandResolver resolver)
            : base(new ParameterResolverDelegate<ProjectCommandArgs>(resolver))
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ProjectCommandArgs args)
        {
            if (args == null || args.ViewSite == null)
            {
                return false;
            }

            ProjectNode project = null;
            ProjectDocumentItem document = args.ViewSite.ActiveDocument as ProjectDocumentItem;
            if (document != null)
            {
                project = document.FileNode.ProjectNode;
            }
            else
            {
                project = this.ProjectSelection(args.SelectedNodes).FirstOrDefault();
            }

            if (project == null)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(ProjectCommandArgs args)
        {
            ExcelApplication application = ExcelApplicationCreator.Create();
            int i = 0;

            ProjectNode project = null;
            ProjectDocumentItem document = args.ViewSite.ActiveDocument as ProjectDocumentItem;
            if (document != null)
            {
                project = document.FileNode.ProjectNode;
            }
            else
            {
                project = this.ProjectSelection(args.SelectedNodes).FirstOrDefault();
            }

            if (project == null)
            {
                return;
            }

            string configurationPath = project.GetProperty("Configurations");
            string directory = Path.GetDirectoryName(project.FullPath);

            configurationPath = Path.Combine(directory, configurationPath);
            configurationPath = Path.GetFullPath(configurationPath);

            DialogueConfigurations configurations = new DialogueConfigurations();
            configurations.LoadConfigurations(configurationPath);

            List<Dialogue> models = new List<Dialogue>();
            foreach (ProjectItem item in project["File"])
            {
                string filename = item.GetMetadata("FullPath");
                Dialogue dialogue = this.IsOpen(filename, args);
                if (dialogue == null)
                {
                    using (XmlReader reader = XmlReader.Create(filename))
                    {
                        dialogue = new Dialogue(reader, configurations);
                    }
                }

                if (dialogue != null)
                {
                    models.Add(dialogue);
                }
            }

            ExcelWorksheet worksheet = application.Workbooks.Add("Dialogue Conversations");
            Dictionary<Guid, string> characterNames = new Dictionary<Guid, string>();
            foreach (Dialogue dialogue in models)
            {
                List<string> dialogueData = new List<string>();
                foreach (Conversation conversation in dialogue.Conversations)
                {
                    HashSet<string> usedCharactersSet = new HashSet<string>();
                    foreach (ILine line in conversation.Lines)
                    {
                        string characterName = null;
                        Guid id = line.CharacterId;
                        if (!characterNames.TryGetValue(id, out characterName))
                        {
                            characterName = conversation.Configurations.GetCharacterName(id);
                            characterNames.Add(id, characterName);
                        }

                        usedCharactersSet.Add(characterName);
                    }

                    string usedCharacters = String.Join(", ", usedCharactersSet);
                    dialogueData.Add(conversation.Root);
                    dialogueData.Add(conversation.Description);
                    dialogueData.Add(conversation.IsPlaceholder.ToString());
                    dialogueData.Add(conversation.IsRandom.ToString());
                    dialogueData.Add(conversation.CutsceneSubtitles.ToString());
                    dialogueData.Add(conversation.Interruptible.ToString());
                    dialogueData.Add(conversation.IsTriggeredByAnimation.ToString());
                    dialogueData.Add(conversation.Lines.Count.ToString());
                    dialogueData.Add(usedCharacters);
                }

                worksheet.SetCsvRows(i, dialogue.Conversations.Count, 9, dialogueData);
                i += dialogue.Conversations.Count;
            }

            worksheet.SetCsvHeader(
                "Root",
                "Description",
                "Placeholder",
                "Random",
                "Cutscene Subtitles",
                "Interruptible",
                "Triggered By Animation",
                "Line Count",
                "Used Characters");

            application.Visible = true;
        }

        /// <summary>
        /// Retrieves the project selection from the specified selection collection by
        /// collecting all of the selected nodes parent project and removing duplicates.
        /// </summary>
        /// <param name="selection">
        /// The collection containing all of the selected nodes.
        /// </param>
        /// <returns>
        /// The project selection from the specified selection collection.
        /// </returns>
        private List<ProjectNode> ProjectSelection(IEnumerable<IHierarchyNode> selection)
        {
            List<ProjectNode> projectSelection = new List<ProjectNode>();
            if (selection == null)
            {
                return projectSelection;
            }

            foreach (IHierarchyNode selectedNode in selection)
            {
                ProjectNode parentProject = selectedNode.ParentProject;
                if (parentProject != null && !projectSelection.Contains(parentProject))
                {
                    projectSelection.Add(parentProject);
                }
            }

            return projectSelection;
        }

        /// <summary>
        /// Determines whether there is a document open that represents the specified node.
        /// </summary>
        /// <param name="node">
        /// The node to test.
        /// </param>
        /// <returns>
        /// True if there is currently a document open that is attached to the specified node;
        /// otherwise, false.
        /// </returns>
        public Dialogue IsOpen(string fullPath, ProjectCommandArgs args)
        {
            foreach (DocumentItem document in args.OpenedDocuments)
            {
                ProjectDocumentItem projectDocument = document as ProjectDocumentItem;
                if (projectDocument == null)
                {
                    continue;
                }

                if (Object.ReferenceEquals(fullPath, projectDocument.FullPath))
                {
                    DialogueViewModel vm = document.Content as DialogueViewModel;
                    if (vm != null)
                    {
                        return vm.Model;
                    }
                }
            }

            return null;
        }
        #endregion Methods
    } // DialogueStar.Commands.ProjectConversationExportAction {Class}
} // DialogueStar.Commands {Namespace}
