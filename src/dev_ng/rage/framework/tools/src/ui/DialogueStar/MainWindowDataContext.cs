﻿//---------------------------------------------------------------------------------------------
// <copyright file="MainWindowDataContext.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace DialogueStar
{
    using System.Windows.Controls;
    using DialogueStar.Definitions;
    using DialogueStar.ViewModel;
    using RSG.Editor;
    using RSG.Editor.Controls.Dock;
    using RSG.Editor.Controls.Dock.ViewModel;
    using RSG.Editor.Controls.Perforce;
    using RSG.Project.ViewModel;
    using RSG.Project.ViewModel.Definitions;

    /// <summary>
    /// Represents the data context that should be attached to the main window.
    /// </summary>
    internal sealed class MainWindowDataContext : NotifyPropertyChangedBase
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="CollectionNode"/> property.
        /// </summary>
        private ProjectCollectionNode _collectionNode;

        /// <summary>
        /// The private reference to the tool window item that is representing the project
        /// explorer.
        /// </summary>
        private ProjectExplorer _projectExplorer;

        /// <summary>
        /// The private reference to the view site.
        /// </summary>
        private ViewSite _viewSite;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MainWindowDataContext"/> class.
        /// </summary>
        public MainWindowDataContext()
        {
            this.InitialiseProjectCollection();
            this.InitialiseViewSite();
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the collection node that represents the root of the project system.
        /// </summary>
        public ProjectCollectionNode CollectionNode
        {
            get { return this._collectionNode; }
        }

        /// <summary>
        /// Gets the main view site view model for the main window.
        /// </summary>
        public ViewSite ViewSite
        {
            get { return this._viewSite; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates the project collection definition that Dialogue Star uses.
        /// </summary>
        /// <returns>
        /// The project collection definition that Dialogue Star uses.
        /// </returns>
        private ProjectCollectionDefinition CreateProjectDefinitions()
        {
            ProjectItemDefinition itemDefinition = new DialogueItemDefinition();

            ProjectDefinition projectDefinition = new DialogueProjectDefinition();
            ProjectCollectionDefinition collection = new DialogueStarCollectionDefinition();
            collection.AddProjectDefinition(projectDefinition);
            return collection;
        }

        /// <summary>
        /// Creates the project collection using the metadata editor project item definitions.
        /// </summary>
        private void InitialiseProjectCollection()
        {
            ProjectCollectionDefinition definition = this.CreateProjectDefinitions();
            this._collectionNode = new ProjectCollectionNode(definition);
            this._collectionNode.PerforceService = new PerforceService();
        }

        /// <summary>
        /// Creates the main view site for the root group and adds the project explorer and the
        /// document pane to it.
        /// </summary>
        private void InitialiseViewSite()
        {
            this._viewSite = ViewSite.CreateMainViewSite();
            ToolWindowPane pane = new ToolWindowPane(this._viewSite);
            this._projectExplorer = new ProjectExplorer(this._collectionNode, pane);

            pane.SplitterLength = new SplitterLength(1, SplitterUnitType.Stretch);
            pane.InsertChild(0, this._projectExplorer);

            DocumentPane documentPane = new DocumentPane(this._viewSite);
            documentPane.SplitterLength = new SplitterLength(7, SplitterUnitType.Stretch);

            this._viewSite.RootGroup.InsertChild(0, pane);
            this._viewSite.RootGroup.InsertChild(1, documentPane);
        }
        #endregion Methods
    } // DialogueStar.MainWindowDataContext {Class}
} // DialogueStar {Namespace}
