﻿//---------------------------------------------------------------------------------------------
// <copyright file="DialogueStarCommonDialogService.cs" company="Rockstar">
//     Copyright Â© Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace DialogueStar
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Windows;
    using Microsoft.Win32;
    using RSG.Base.Extensions;
    using RSG.Editor;
    using RSG.Editor.Controls;
    using RSG.Editor.Controls.Resources;

    /// <summary>
    /// Represents the common dialog service this application uses.
    /// </summary>
    internal sealed class DialogueStarCommonDialogService : ICommonDialogService
    {
        #region Methods
        /// <summary>
        /// Handles the situation of trying to save to a read-only file.
        /// </summary>
        /// <param name="fullPath">
        /// The full path to the file being saved.
        /// </param>
        /// <param name="cancelled">
        /// A value that is set to true if during the handling the user chooses to cancel
        /// the operation.
        /// </param>
        /// <returns>
        /// True if the specified full path is need writable; otherwise, false.
        /// </returns>
        public bool HandleSavingOfReadOnlyFile(ref string fullPath, ref bool cancelled)
        {
            App app = Application.Current as App;
            IMessageBoxService messageService = app.GetService<IMessageBoxService>();
            IProductInfoService productInfoService = app.GetService<IProductInfoService>();
            IPerforceService perforceService = app.GetService<IPerforceService>();

            string filename = Path.GetFileName(fullPath);
            FileInfo fileInfo = new FileInfo(fullPath);
            if (fileInfo.Exists && fileInfo.IsReadOnly)
            {
                IMessageBox messageBox = messageService.CreateMessageBox();
                messageBox.SetIconToExclamation();
                messageBox.Caption = StringTable.WriteProtectedCaption;
                messageBox.Text = StringTable.WriteProtectedMsg.FormatCurrent(
                    filename, productInfoService.ProductName);
                messageBox.AddButton("Check Out", 4, true, false);
                messageBox.AddButton(StringTable.SaveAsButtonText, 1, false, false);
                messageBox.AddButton(StringTable.OverwriteButtonText, 2, false, false);
                messageBox.AddButton(StringTable.CancelButtonText, 3, false, true);

            UserChoice:
                switch (messageBox.ShowMessageBox())
                {
                    case 1:
                        string directory = Path.GetDirectoryName(fullPath);
                        if (!this.ShowSaveFile(directory, filename, out fullPath))
                        {
                            cancelled = true;
                            return false;
                        }

                        break;
                    case 2:
                        try
                        {
                            fileInfo.IsReadOnly = false;
                        }
                        catch
                        {
                            messageService.Show(
                                StringTable.OverwriteFailedMsg.FormatCurrent(filename),
                                String.Empty,
                                System.Windows.MessageBoxButton.OK,
                                System.Windows.MessageBoxImage.Error);

                            goto UserChoice;
                        }

                        break;
                    case 3:
                        cancelled = true;
                        return false;
                    case 4:
                        try
                        {
                            perforceService.EditFiles(new List<string>() { fullPath });
                        }
                        catch
                        {
                            string msg = "There was an error trying to check-out the file '{0}'.".FormatCurrent(filename);
                            messageService.ShowStandardErrorBox(msg, null);
                            goto UserChoice;
                        }

                        fileInfo = new FileInfo(fullPath);
                        if (fileInfo.Exists && fileInfo.IsReadOnly)
                        {
                            string msg = "There was an error trying to check-out the file '{0}'.".FormatCurrent(filename);
                            messageService.ShowStandardErrorBox(msg, null);
                            goto UserChoice;
                        }

                        break;
                    default:
                        Debug.Assert(false, "Unexpected result from message box");
                        cancelled = true;
                        return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Shows the open file dialog window to the user so that they can select a single
        /// file to read from.
        /// </summary>
        /// <param name="directory">
        /// The initial directory that is displayed by the file dialog.
        /// </param>
        /// <param name="filename">
        /// A string containing the initial full path of the file selected in the file
        /// dialog.
        /// </param>
        /// <param name="filter">
        /// The filter that determines what types of files are displayed in the file
        /// dialog.
        /// </param>
        /// <param name="filterIndex">
        /// The index of the filter currently selected in the file dialog.
        /// </param>
        /// <param name="location">
        /// When this method returns contains the full path to the file the user selected
        /// if valid.
        /// </param>
        /// <returns>
        ///  True if the user clicks the OK button; otherwise, false.
        /// </returns>
        public bool ShowOpenFile(
            string directory,
            string filename,
            string filter,
            int filterIndex,
            out string location)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            if (directory != null)
            {
                dlg.InitialDirectory = directory;
            }

            if (filename != null)
            {
                dlg.FileName = filename;
            }

            if (filter != null)
            {
                dlg.Filter = filter;
            }

            dlg.FilterIndex = filterIndex;

            bool? result = dlg.ShowDialog();
            if (result != true)
            {
                location = null;
            }
            else
            {
                location = dlg.FileName;
            }

            return (bool)result;
        }

        /// <summary>
        /// Shows the open file dialog window to the user so that they can select a single
        /// file to read from.
        /// </summary>
        /// <param name="directory">
        /// The initial directory that is displayed by the file dialog.
        /// </param>
        /// <param name="filename">
        /// A string containing the initial full path of the file selected in the file
        /// dialog.
        /// </param>
        /// <param name="filter">
        /// The filter that determines what types of files are displayed in the file
        /// dialog.
        /// </param>
        /// <param name="filterIndex">
        /// The index of the filter currently selected in the file dialog.
        /// </param>
        /// <param name="filepaths">
        /// When this method returns contains the full path to the files the user selected
        /// if valid.
        /// </param>
        /// <returns>
        ///  True if the user clicks the OK button; otherwise, false.
        /// </returns>
        public bool ShowOpenFile(
            string directory,
            string filename,
            string filter,
            int filterIndex,
            out string[] filepaths)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.InitialDirectory = directory;
            dlg.FileName = filename;
            dlg.Filter = filter;
            dlg.FilterIndex = filterIndex;
            dlg.Multiselect = true;

            bool? result = dlg.ShowDialog();
            if (result != true)
            {
                filepaths = null;
            }
            else
            {
                filepaths = dlg.FileNames;
            }

            return (bool)result;
        }

        /// <summary>
        /// Shows the save file dialog window to the user so that they can select a single
        /// file to write to.
        /// </summary>
        /// <param name="directory">
        /// The initial directory that is displayed by the file dialog.
        /// </param>
        /// <param name="filename">
        /// A string containing the initial full path of the file selected in the file
        /// dialog.
        /// </param>
        /// <param name="destination">
        /// When this method returns contains the full path to the file the user selected
        /// if valid.
        /// </param>
        /// <returns>
        ///  True if the user clicks the OK button; otherwise, false.
        /// </returns>
        public bool ShowSaveFile(string directory, string filename, out string destination)
        {
            return this.ShowSaveFile(directory, filename, null, 0, out destination);
        }

        /// <summary>
        /// Shows the save file dialog window to the user to that they can select a single
        /// file to write to.
        /// </summary>
        /// <param name="directory">
        /// The initial directory that is displayed by the file dialog.
        /// </param>
        /// <param name="filename">
        /// A string containing the initial full path of the file selected in the file
        /// dialog.
        /// </param>
        /// <param name="filter">
        /// The filter that determines what types of files are displayed in the file.
        /// </param>
        /// <param name="filterIndex">
        /// The index of the filter currently selected in the file dialog.
        /// </param>
        /// <param name="destination">
        /// When this method returns contains the full path to the file the user selected
        /// if valid.
        /// </param>
        /// <returns>
        ///  True if the user clicks the OK button; otherwise, false.
        /// </returns>
        public bool ShowSaveFile(
            string directory,
            string filename,
            string filter,
            int filterIndex,
            out string destination)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.InitialDirectory = directory;
            dlg.FileName = filename;
            dlg.Filter = filter;
            dlg.FilterIndex = filterIndex;

            bool? result = dlg.ShowDialog();
            if (result != true)
            {
                destination = null;
            }
            else
            {
                destination = dlg.FileName;
            }

            return (bool)result;
        }

        /// <summary>
        /// Shows the selected folder dialog to the user so that they can select a folder
        /// and returns the result back in the <paramref name="directory"/> parameter.
        /// </summary>
        /// <param name="id">
        /// The identifier to use to control the persistence of this dialog.
        /// </param>
        /// <param name="title">
        /// The title to show on the window.
        /// </param>
        /// <param name="directory">
        /// When this method returns contains the full path to the directory the user
        /// selected if valid.
        /// </param>
        /// <returns>
        /// True if the user selected a folder; otherwise, false.
        /// </returns>
        public bool ShowSelectFolder(Guid id, string title, out string directory)
        {
            RsSelectFolderDialog dlg = new RsSelectFolderDialog(id);
            dlg.Title = title;

            bool? result = dlg.ShowDialog();
            if (result != true)
            {
                directory = null;
            }
            else
            {
                directory = dlg.FileName;
            }

            return (bool)result;
        }

        /// <summary>
        /// Shows the selected folder dialog to the user so that they can select a folder
        /// and returns the result back in the <paramref name="directory"/> parameter.
        /// </summary>
        /// <param name="id">
        /// The identifier to use to control the persistence of this dialog.
        /// </param>
        /// <param name="initialDirectory">
        /// The initial directory that is displayed by the folder dialog.
        /// </param>
        /// <param name="title">
        /// The title to show on the window.
        /// </param>
        /// <param name="directory">
        /// When this method returns contains the full path to the directory the user
        /// selected if valid.
        /// </param>
        /// <returns>
        /// True if the user selected a folder; otherwise, false.
        /// </returns>
        public bool ShowSelectFolder(
            Guid id, string initialDirectory, string title, out string directory)
        {
            RsSelectFolderDialog dlg = new RsSelectFolderDialog(id);
            dlg.Title = title;
            dlg.InitialDirectory = initialDirectory;

            bool? result = dlg.ShowDialog();
            if (result != true)
            {
                directory = null;
            }
            else
            {
                directory = dlg.FileName;
            }

            return (bool)result;
        }

        /// <summary>
        /// Shows the selected folder dialog to the user so that they can select folders
        /// and returns the result back in the <paramref name="directories"/> parameter.
        /// </summary>
        /// <param name="id">
        /// The identifier to use to control the persistence of this dialog.
        /// </param>
        /// <param name="title">
        /// The title to show on the window.
        /// </param>
        /// <param name="directories">
        /// When this method returns contains an array of full paths to the directories the user
        /// selected if valid.
        /// </param>
        /// <returns>
        /// True if the user selected a folder; otherwise, false.
        /// </returns>
        public bool ShowMultiSelectFolder(Guid id, string title, out string[] directories)
        {
            RsSelectFolderDialog dlg = new RsSelectFolderDialog(id);
            dlg.Title = title;
            dlg.MultiSelect = true;

            bool? result = dlg.ShowDialog();
            if (result != true)
            {
                directories = null;
            }
            else
            {
                directories = dlg.FileNames;
            }

            return (bool)result;
        }

        /// <summary>
        /// Shows the selected folder dialog to the user so that they can select folders
        /// and returns the result back in the <paramref name="directories"/> parameter.
        /// </summary>
        /// <param name="id">
        /// The identifier to use to control the persistence of this dialog.
        /// </param>
        /// <param name="initialDirectory">
        /// The initial directory that is displayed by the folder dialog.
        /// </param>
        /// <param name="title">
        /// The title to show on the window.
        /// </param>
        /// <param name="directories">
        /// When this method returns contains an array of full paths to the directories the user
        /// selected if valid.
        /// </param>
        /// <returns>
        /// True if the user selected a folder; otherwise, false.
        /// </returns>
        public bool ShowMultiSelectFolder(
            Guid id, string initialDirectory, string title, out string[] directories)
        {
            RsSelectFolderDialog dlg = new RsSelectFolderDialog(id);
            dlg.Title = title;
            dlg.InitialDirectory = initialDirectory;
            dlg.MultiSelect = true;

            bool? result = dlg.ShowDialog();
            if (result != true)
            {
                directories = null;
            }
            else
            {
                directories = dlg.FileNames;
            }

            return (bool)result;
        }
        #endregion Methods
    } // DialogueStar.DialogueStarCommonDialogService {Class}
} // DialogueStar {Namespace}
