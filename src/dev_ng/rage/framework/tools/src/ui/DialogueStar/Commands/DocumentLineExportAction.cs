﻿//---------------------------------------------------------------------------------------------
// <copyright file="DocumentLineExportAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace DialogueStar.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Controls;
    using RSG.Editor;
    using RSG.Interop.Microsoft.Office.Excel;
    using RSG.Project.Commands;
    using RSG.Text.Model;
    using RSG.Text.ViewModel;

    /// <summary>
    /// Contains the logic for the <see cref="TextCommands.ExportDocumentLinesToExcel"/> routed
    /// command. This class cannot be inherited.
    /// </summary>
    public sealed class DocumentLineExportAction : ButtonAction<ProjectCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DocumentLineExportAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public DocumentLineExportAction(ProjectCommandResolver resolver)
            : base(new ParameterResolverDelegate<ProjectCommandArgs>(resolver))
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ProjectCommandArgs args)
        {
            if (args == null || args.ViewSite == null || args.ViewSite.ActiveDocument == null)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(ProjectCommandArgs args)
        {
            ExcelApplication application = ExcelApplicationCreator.Create();
            application.Visible = true;

            ExcelWorksheet worksheet = application.Workbooks.Add("Dialogue Lines");
            object content = args.ViewSite.ActiveDocument.Content;
            DialogueViewModel viewModel = (content as UserControl).DataContext as DialogueViewModel;
            int i = 0;
            Dictionary<Guid, string> characterNames = new Dictionary<Guid, string>();
            Dictionary<Guid, string> specialNames = new Dictionary<Guid, string>();
            Dictionary<Guid, string> audibilityNames = new Dictionary<Guid, string>();
            Dictionary<Guid, string> audiotypeNames = new Dictionary<Guid, string>();
            string missionName = viewModel.Name;
            foreach (Conversation conversation in viewModel.Model.Conversations)
            {
                foreach (ILine line in conversation.Lines)
                {
                    string characterName = null;
                    Guid id = line.CharacterId;
                    if (!characterNames.TryGetValue(id, out characterName))
                    {
                        characterName = conversation.Configurations.GetCharacterName(id);
                        characterNames.Add(id, characterName);
                    }

                    string specialName = null;
                    id = line.Special;
                    if (!specialNames.TryGetValue(id, out specialName))
                    {
                        specialName = conversation.Configurations.GetSpecialName(id);
                        specialNames.Add(id, specialName);
                    }

                    string audibilityName = null;
                    id = line.Audibility;
                    if (!audibilityNames.TryGetValue(id, out audibilityName))
                    {
                        audibilityName = conversation.Configurations.GetAudibilityName(id);
                        audibilityNames.Add(id, audibilityName);
                    }

                    string audioTypeName = null;
                    id = line.AudioType;
                    if (!audiotypeNames.TryGetValue(id, out audioTypeName))
                    {
                        audioTypeName = conversation.Configurations.GetAudioTypeName(id);
                        audiotypeNames.Add(id, audioTypeName);
                    }

                    worksheet.SetCsvRow(
                        i++,
                        characterName,
                        line.Dialogue,
                        line.AudioFilepath,
                        line.SentForRecording.ToString(),
                        line.Recorded.ToString(),
                        line.Interruptible.ToString(),
                        line.DontInterruptForSpecialAbility.ToString(),
                        line.AlwaysSubtitled.ToString(),
                        line.DucksRadio.ToString(),
                        line.DucksScore.ToString(),
                        line.HeadsetSubmix.ToString(),
                        line.PadSpeaker.ToString(),
                        line.Speaker.ToString(),
                        line.Listener.ToString(),
                        specialName,
                        audibilityName,
                        audioTypeName,
                        missionName,
                        conversation.IsPlaceholder.ToString());
                }
            }

            worksheet.SetCsvHeader(
                "Character",
                "Dialogue",
                "Filename",
                "Sent For Recording",
                "Recorded",
                "Interruptible",
                "Dont Interrupt For Special Ability",
                "Always Subtitled",
                "DucksRadio",
                "DucksScore",
                "HeadsetSubmix",
                "PadSpeaker",
                "Speaker",
                "Listener",
                "Special",
                "Audibility",
                "Audio Type",
                "Mission Name",
                "Is Placeholder");

            application.Visible = true;
        }
        #endregion Methods
    } // DialogueStar.Commands.DocumentLineExportAction {Class}
} // DialogueStar.Commands {Namespace}
