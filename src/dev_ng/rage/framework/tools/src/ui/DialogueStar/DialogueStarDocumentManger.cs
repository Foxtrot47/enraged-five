﻿//---------------------------------------------------------------------------------------------
// <copyright file="DialogueStarDocumentManger.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace DialogueStar
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Windows;
    using System.Xml;
    using DialogueStar.ViewModel;
    using RSG.Editor.Controls;
    using RSG.Editor.Controls.Dock.ViewModel;
    using RSG.Editor.Model;
    using RSG.Project.ViewModel;
    using RSG.Text.Model;
    using RSG.Text.ViewModel;

    /// <summary>
    /// Represents the main document manager for the dialogue star application.
    /// </summary>
    internal sealed class DialogueStarDocumentManger : ProjectDocumentManager
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="SupportedExtensions"/> property.
        /// </summary>
        private HashSet<string> _supportedExtensions;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DialogueStarDocumentManger"/> class.
        /// </summary>
        /// <param name="viewSite">
        /// The view site that will be used to open and show the documents.
        /// </param>
        public DialogueStarDocumentManger(ViewSite viewSite)
            : base(viewSite, Application.Current as RsApplication)
        {
            this._supportedExtensions = new HashSet<string>();
            this._supportedExtensions.Add(".dstar2");
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the set of supported extensions that can be opened using this project
        /// manager.
        /// </summary>
        protected override HashSet<string> SupportedExtensions
        {
            get { return this._supportedExtensions; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a project document items content for the specified full path and project.
        /// </summary>
        /// <param name="project">
        /// The project the file has been opened from.
        /// </param>
        /// <param name="undoEngine">
        /// The undo engine that any created model can use.
        /// </param>
        /// <param name="fullPath">
        /// The full path to the file that is being opened.
        /// </param>
        /// <returns>
        /// A new project document item content object for the specified full path.
        /// </returns>
        protected override object CreateContent(
            ProjectNode project, UndoEngine undoEngine, string fullPath)
        {
            if (String.Equals(Path.GetExtension(fullPath), ".dstar2"))
            {
                DialogueProjectNode dialogueProject = project as DialogueProjectNode;
                DialogueConfigurations configurations = null;
                if (dialogueProject != null)
                {
                    configurations = dialogueProject.Configurations;
                }

                using (XmlReader reader = XmlReader.Create(fullPath))
                {
                    Dialogue dialogue = new Dialogue(reader, undoEngine, configurations);
                    return new DialogueViewModel(dialogue);
                }
            }

            return null;
        }
        #endregion Methods
    } // DialogueStar.DialogueDocumentViewModel {Class}
} // DialogueStar {Namespace}
