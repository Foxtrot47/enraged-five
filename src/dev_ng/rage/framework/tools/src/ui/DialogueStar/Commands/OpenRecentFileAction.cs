﻿//---------------------------------------------------------------------------------------------
// <copyright file="EditDialogueConfigAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace DialogueStar.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Windows;
    using System.Xml;
    using DialogueStar.Resources;
    using RSG.Editor;
    using RSG.Project.Commands;
    using RSG.Project.ViewModel;
    using RSG.Text.Model;
    using RSG.Text.View;
    using RSG.Text.ViewModel;
    using System.Threading.Tasks;
    using RSG.Project.ViewModel.Definitions;
    using RSG.Project.Model;
    using RSG.Editor.Controls.Dock.ViewModel;
    using RSG.Base.Extensions;
    using RSG.Base.Logging.Universal;
    using RSG.Base.Logging;

    /// <summary>
    /// Contains the logic for the <see cref="TextCommands.EditDialogueConfig"/> routed
    /// command. This class cannot be inherited.
    /// </summary>
    public class OpenRecentFileAction : MruCommandAction<ProjectCommandArgs>
    {
        private ProjectCommandResolver _resolver;
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="EditDialogueConfigAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public OpenRecentFileAction(ProjectCommandResolver resolver)
            : base(new ParameterResolverDelegate<ProjectCommandArgs>(resolver))
        {
            _resolver = resolver;
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="viewModels">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="path">
        /// The path sent with the MRU command.
        /// </param>
        /// <param name="additional">
        /// The additional information for the path property sent with the command.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(
            ProjectCommandArgs args, string path, string[] additional)
        {
            if (args == null)
            {
                return false;
            }

            if (args.CollectionNode == null)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="viewModels">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="path">
        /// The path sent with the MRU command.
        /// </param>
        /// <param name="additional">
        /// The additional information for the path property sent with the command.
        /// </param>
        /// <returns>
        /// A task that represents the work done by the command handler.
        /// </returns>
        public override void Execute(
            ProjectCommandArgs args, string path, string[] additional)
        {
            if (!OpenFile(args, path))
            {
                if (additional.Length < 1)
                    return;

                OpenProject(args, additional[0]);
                OpenFile(args, path);
            }
        }

        private void OpenProject(ProjectCommandArgs args, string path)
        {
            IHierarchyNode parent = args.SelectedNodes.FirstOrDefault();
            ProjectCollectionNode collection = args.CollectionNode;
            if (parent == null || !parent.CanHaveProjectsAdded || args.SelectionCount > 1)
            {
                parent = args.CollectionNode;
            }

            string fullPath = path;
            string extension = Path.GetExtension(fullPath);
            ProjectDefinition selectedDefinition = null;
            StringComparison comparisonType = StringComparison.OrdinalIgnoreCase;
            foreach (ProjectDefinition definition in collection.Definition.ProjectDefinitions)
            {
                if (String.Equals(definition.Extension, extension, comparisonType))
                {
                    selectedDefinition = definition;
                    break;
                }
            }

            if (selectedDefinition == null)
            {
                Debug.Assert(
                    false,
                    "Failed to find project definition for the extension",
                    "{0}",
                    extension);
                return;
            }

            CloseCollectionAction close = new CloseCollectionAction(Application.Current as ICommandServiceProvider);
            if (close.CanExecute(args))
            {
                close.Execute(args);
            }

            ProjectItem newItem = collection.Model.AddNewProjectItem("Project", fullPath);
            ProjectNode projectNode = selectedDefinition.CreateProjectNode();
            projectNode.SetParentAndModel(collection, newItem);
            collection.AddChild(projectNode);
            projectNode.Load();

            collection.IsModified = true;
            collection.Loaded = true;
            collection.IsTemporaryFile = true;
            collection.Name = Path.GetFileNameWithoutExtension(fullPath);
        }

        private bool OpenFile(ProjectCommandArgs args, string path)
        {
            IDocumentManagerService docService = this.GetService<IDocumentManagerService>();
            if (docService == null)
            {
                Debug.Fail("Unable to open file due to the fact the service is missing.");
                return false;
            }

            foreach (var i in args.CollectionNode.Items)
            {
                var node = i.FindNodeWithFullPath(path);
                if (node != null)
                {
                    ICreatesDocumentContent contentCreator = node as ICreatesDocumentContent;
                    FileNode fileNode = node as FileNode;
                    if (fileNode == null || contentCreator == null)
                    {
                        continue;
                    }

                    string fullPath = node.ProjectItem.GetMetadata("FullPath");
                    if (fullPath == null)
                    {
                        Debug.Assert(false, "Unable to determine the full path to the project.");
                        return false;
                    }

                    foreach (DocumentItem openedDocument in args.OpenedDocuments)
                    {
                        if (String.Equals(openedDocument.FullPath, fullPath))
                        {
                            openedDocument.IsSelected = true;
                            return true;
                        }
                    }

                    DocumentPane pane = args.ViewSite.LastActiveDocumentPane;
                    if (pane == null)
                    {
                        pane = args.ViewSite.FirstDocumentPane;
                        if (pane == null)
                        {
                            Debug.Assert(false, "Cannot find the pane to open the document in.");
                            return false;
                        }
                    }

                    IMessageBoxService messageService = this.GetService<IMessageBoxService>();
                    if (messageService == null)
                    {
                        Debug.Assert(false, "Unable to open as services are missing.");
                        return false;
                    }
                    FileInfo info = new FileInfo(fullPath);
                    if (!info.Exists)
                    {
                        messageService.ShowStandardErrorBox(
                            StringTable.FileMissingMsg.FormatInvariant(fullPath),
                            null);

                        return false;
                    }

                    ProjectDocumentItem item = null;
                    IUniversalLog log = LogFactory.CreateUniversalLog("Content Creation");
                    string logFilename = Path.Combine(Path.GetTempPath(), "content_creation");
                    logFilename = Path.ChangeExtension(logFilename, UniversalLogFile.EXTENSION);
                    UniversalLogFile target = new UniversalLogFile(
                        logFilename, FileMode.Create, FlushMode.Never, log);

                    try
                    {
                        ICreatesProjectDocument documentCreator = node as ICreatesProjectDocument;
                        if (documentCreator != null)
                        {
                            item = documentCreator.CreateDocument(pane, fullPath);
                        }
                        else
                        {
                            item = new ProjectDocumentItem(pane, fullPath, fileNode);
                        }

                        ICreateDocumentContentController controller =
                            contentCreator.CreateDocumentContentController;
                        if (controller != null)
                        {
                            CreateContentArgs e =
                                new CreateContentArgs(log, args.ViewSite, item.UndoEngine);
                            item.Content = controller.CreateContent(node as HierarchyNode, e);
                        }

                        if (log.HasErrors || log.HasWarnings)
                        {
                            Process.Start(logFilename);
                        }
                    }
                    catch (Exception ex)
                    {
                        if (log.HasErrors || log.HasWarnings)
                        {
                            Process.Start(logFilename);
                        }

                        messageService.ShowStandardErrorBox(
                            StringTable.FailedFileParsing.FormatInvariant(fullPath, ex.Message),
                            null);
                        return false;
                    }

                    if (docService.FileWatcher != null)
                    {
                        docService.FileWatcher.RegisterFile(fullPath);
                    }

                    if (docService.BackupManager != null)
                    {
                        docService.BackupManager.RegisterFile(item);
                    }

                    item.IsReadOnly = info.IsReadOnly;
                    item.IsSelected = true;
                    pane.InsertChild(pane.Children.Count, item);
                    return true;
                }
            }
            return false;
        }

        #endregion Methods
    } // DialogueStar.Commands.EditDialogueConfigAction {Class}
} // DialogueStar.Commands {Namespace}
