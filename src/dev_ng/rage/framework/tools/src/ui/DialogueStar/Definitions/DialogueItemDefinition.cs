﻿//---------------------------------------------------------------------------------------------
// <copyright file="DialogueItemDefinition.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace DialogueStar.Definitions
{
    using System.IO;
    using System.Reflection;
    using System.Windows.Media.Imaging;
    using DialogueStar.Resources;
    using RSG.Project.ViewModel.Definitions;

    /// <summary>
    /// Represents the definition for the dialogue star project item that contains the
    /// conversations and lines for the subtitles.
    /// </summary>
    public class DialogueItemDefinition : ProjectItemDefinition
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DialogueItemDefinition"/> class.
        /// </summary>
        public DialogueItemDefinition()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the name that is used as the default value for a new instance of this
        /// definition.
        /// </summary>
        public override string DefaultName
        {
            get { return StringTable.DialogueItemDefinitionDefaultName; }
        }

        /// <summary>
        /// Gets the description for this item definition to be displayed in the add dialog.
        /// </summary>
        public override string Description
        {
            get { return StringTable.DialogueItemDefinitionDescription; }
        }

        /// <summary>
        /// Gets the extension used for this defined item.
        /// </summary>
        public override string Extension
        {
            get { return ".dstar2"; }
        }

        /// <summary>
        /// Gets the icon that is used to display this project.
        /// </summary>
        public override BitmapSource Icon
        {
            get { return DialogueStarIcons.DialogueProjectItemIcon; }
        }

        /// <summary>
        /// Gets the name for this defined project.
        /// </summary>
        public override string Name
        {
            get { return StringTable.DialogueItemDefinitionName; }
        }

        /// <summary>
        /// Gets a byte array containing the data that is used to create a new instance of the
        /// defined project.
        /// </summary>
        public override byte[] Template
        {
            get
            {
                byte[] buffer = new byte[0];
                Assembly assembly = Assembly.GetExecutingAssembly();
                string name = "DialogueStar.Templates.DialogueStarTwoFileTemplate.xml";
                using (Stream stream = assembly.GetManifestResourceStream(name))
                {
                    buffer = new byte[stream.Length];
                    stream.Read(buffer, 0, buffer.Length);
                }

                return buffer;
            }
        }

        /// <summary>
        /// Gets the type of this item to be displayed in the add dialog.
        /// </summary>
        public override string Type
        {
            get { return StringTable.DialogueItemDefinitionType; }
        }
        #endregion Properties
    } // DialogueStar.Definitions.DialogueItemDefinition {Class}
} // DialogueStar.Definitions {Namespace}
