﻿//---------------------------------------------------------------------------------------------
// <copyright file="DocumentConversationExportAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace DialogueStar.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Controls;
    using RSG.Editor;
    using RSG.Interop.Microsoft.Office.Excel;
    using RSG.Project.Commands;
    using RSG.Text.Model;
    using RSG.Text.ViewModel;

    /// <summary>
    /// Contains the logic for the
    /// <see cref="TextCommands.ExportDocumentConversationsToExcel"/> routed command. This
    /// class cannot be inherited.
    /// </summary>
    public sealed class DocumentConversationExportAction : ButtonAction<ProjectCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DocumentConversationExportAction"/>
        /// class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public DocumentConversationExportAction(ProjectCommandResolver resolver)
            : base(new ParameterResolverDelegate<ProjectCommandArgs>(resolver))
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ProjectCommandArgs args)
        {
            if (args == null || args.ViewSite == null || args.ViewSite.ActiveDocument == null)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(ProjectCommandArgs args)
        {
            ExcelApplication application = ExcelApplicationCreator.Create();
            application.Visible = true;

            ExcelWorksheet worksheet = application.Workbooks.Add("Dialogue Conversations");
            object content = args.ViewSite.ActiveDocument.Content;
            DialogueViewModel viewModel = (content as UserControl).DataContext as DialogueViewModel;
            int i = 0;
            Dictionary<Guid, string> characterNames = new Dictionary<Guid, string>();
            foreach (Conversation conversation in viewModel.Model.Conversations)
            {
                HashSet<string> usedCharactersSet = new HashSet<string>();
                foreach (ILine line in conversation.Lines)
                {
                    string characterName = null;
                    Guid id = line.CharacterId;
                    if (!characterNames.TryGetValue(id, out characterName))
                    {
                        characterName = conversation.Configurations.GetCharacterName(id);
                        characterNames.Add(id, characterName);
                    }

                    usedCharactersSet.Add(characterName);
                }

                string usedCharacters = String.Join(", ", usedCharactersSet);
                worksheet.SetCsvRow(
                    i++,
                    conversation.Root,
                    conversation.Description,
                    conversation.IsPlaceholder.ToString(),
                    conversation.IsRandom.ToString(),
                    conversation.CutsceneSubtitles.ToString(),
                    conversation.Interruptible.ToString(),
                    conversation.IsTriggeredByAnimation.ToString(),
                    usedCharacters);
            }

            worksheet.SetCsvHeader(
                "Root",
                "Description",
                "Placeholder",
                "Random",
                "Cutscene Subtitles",
                "Interruptible",
                "Triggered By Animation",
                "Used Characters");

            application.Visible = true;
        }
        #endregion Methods
    } // DialogueStar.Commands.DocumentConversationExportAction {Class}
} // DialogueStar.Commands {Namespace}
