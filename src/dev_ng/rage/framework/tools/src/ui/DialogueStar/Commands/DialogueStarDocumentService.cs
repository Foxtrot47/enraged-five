﻿//---------------------------------------------------------------------------------------------
// <copyright file="DocumentManagerService.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace DialogueStar.Commands
{
    using System;
    using System.Collections.Generic;
    using RSG.Base.Extensions;
    using RSG.Editor;
    using RSG.Editor.Controls;
    using RSG.Editor.Controls.Dock.ViewModel;
    using RSG.Project.Commands;
    using RSG.Text.Commands;
    using RSG.Text.Model;
    using RSG.Text.View;
    using RSG.Text.ViewModel;

    /// <summary>
    /// Implements the document manager service for Dialogue Star documents.
    /// </summary>
    public class DialogueStarDocumentService : DocumentManagerService
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DocumentManagerService"/> class.
        /// </summary>
        /// <param name="serviceProvider">
        /// A command service provider that can be used to get the command dialog service and
        /// the message service.
        /// </param>
        public DialogueStarDocumentService(ICommandServiceProvider serviceProvider)
            : base(serviceProvider)
        {
        }
        #endregion Constructors

        /// <summary>
        /// 
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public override System.Windows.FrameworkElement GetControlForDocument(object content)
        {
            return new DialogueFileControl(content as DialogueViewModel);
        }

        public override void SaveDocuments(IList<DocumentItem> documents, bool forceSaveAs)
        {
            List<DocumentItem> validDocuments = new List<DocumentItem>();
            foreach (DocumentItem document in documents)
            {
                DialogueFileControl content = document.Content as DialogueFileControl;
                if (content == null)
                {
                    continue;
                }

                DialogueViewModel dialogue = content.ViewModel;
                if (String.IsNullOrWhiteSpace(dialogue.Id))
                {
                    continue;
                }

                foreach (ConversationViewModel conversation in dialogue.Conversations)
                {
                    foreach (LineViewModel line in conversation.Lines)
                    {
                        if (line.ReadOnlyFilename && String.IsNullOrEmpty(line.AudioFilepath))
                        {
                            validDocuments.Add(document);
                            break;
                        }
                    }
                }
            }

            if (validDocuments.Count >= 1)
            {
                bool yesToAll = false;
                foreach (DocumentItem document in validDocuments)
                {
                    bool yes = yesToAll;
                    if (!yes)
                    {
                        IMessageBox msgBox = new RsMessageBox();
                        msgBox.Text = "The file '{0}' has missing audio line filenames. Do you wish to auto-generate them before saving?";
                        msgBox.Text = msgBox.Text.FormatCurrent(document.FullPath);
                        long yesValueResult = 1;
                        long yesToAllValueResult = 2;
                        long noValueResult = 3;
                        long noToAllValueResult = 4;
                        msgBox.AddButton("Yes", yesValueResult, true, false);
                        if (validDocuments.Count > 1)
                        {
                            msgBox.AddButton("Yes to All", yesToAllValueResult, false, false);
                        }

                        msgBox.AddButton("No", noValueResult, false, true);
                        if (validDocuments.Count > 1)
                        {
                            msgBox.AddButton("No to All", noToAllValueResult, false, false);
                        }

                        long result = msgBox.ShowMessageBox();
                        if (result == yesValueResult)
                        {
                            yes = true;
                        }
                        else if (result == yesToAllValueResult)
                        {
                            yes = yesToAll = true;
                        }
                    }

                    if (yes)
                    {
                        DialogueFileControl content = document.Content as DialogueFileControl;
                        DialogueViewModel dialogue = content.ViewModel;
                        List<Conversation> conversations = new List<Conversation>();
                        conversations.AddRange(dialogue.Model.Conversations);
                        GenerateFilenamesAction action =
                            new GenerateFilenamesAction(this.ServiceProvider);
                        action.Execute(conversations);
                    }
                }
            }

            base.SaveDocuments(documents, forceSaveAs);
        }
    } // DialogueStar.Commands.DialogueStarDocumentService {Class}
} // DialogueStar.Commands {Namespace}
