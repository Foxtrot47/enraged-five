﻿//---------------------------------------------------------------------------------------------
// <copyright file="DialogueProjectDefinition.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace DialogueStar.Definitions
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using System.Text;
    using System.Windows;
    using System.Windows.Media.Imaging;
    using DialogueStar.Resources;
    using RSG.Interop.Microsoft.Windows;
    using RSG.Project.ViewModel;
    using RSG.Project.ViewModel.Definitions;
    using RSG.Text.ViewModel;
    using RSG.Text.ViewModel.Project;

    /// <summary>
    /// Represents the definition for the dialogue star project that contains the dialogue
    /// files for missions and random events.
    /// </summary>
    public class DialogueProjectDefinition : ProjectDefinition
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DialogueProjectDefinition"/> class.
        /// </summary>
        public DialogueProjectDefinition()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the description for this defined project.
        /// </summary>
        public override string Description
        {
            get { return StringTable.TextProjectDefinitionDescription; }
        }

        /// <summary>
        /// Gets the name that is used as the default value for a new instance of this
        /// definition.
        /// </summary>
        public override string DefaultName
        {
            get { return StringTable.TextProjectDefinitionDefaultName; }
        }

        /// <summary>
        /// Gets the extension used for this defined project.
        /// </summary>
        public override string Extension
        {
            get { return ".dsproj"; }
        }

        /// <summary>
        /// Gets the string used as the filter in a open or save dialog for this defined
        /// project.
        /// </summary>
        public override string Filter
        {
            get { return StringTable.TextProjectDefinitionFilter; }
        }

        /// <summary>
        /// Gets the icon that is used to display this project inside the add new project
        /// dialog window.
        /// </summary>
        public override BitmapSource Icon
        {
            get { return DialogueStarIcons.TextProjectIcon; }
        }

        /// <summary>
        /// Gets the name for this defined project.
        /// </summary>
        public override string Name
        {
            get { return StringTable.TextProjectDefinitionName; }
        }

        /// <summary>
        /// Gets the filter to use in the Add Existing Item dialog window for this project.
        /// </summary>
        public override string NewItemFilter
        {
            get { return "Dialogue Star Files (*.dstar2)|*.dstar2|Old Dialogue Star Files (*.dstar)|*.dstar|All Dialogue Star Files (*.dstar, *.dstar2)|*.dstar;*.dstar2"; }
        }

        /// <summary>
        /// Gets a byte array containing the data that is used to create a new instance of the
        /// defined project.
        /// </summary>
        public override byte[] Template
        {
            get
            {
                byte[] buffer = new byte[0];
                Assembly assembly = Assembly.GetExecutingAssembly();
                string name = "DialogueStar.Templates.DialogueProjectTemplate.xml";
                using (Stream stream = assembly.GetManifestResourceStream(name))
                {
                    buffer = new byte[stream.Length];
                    stream.Read(buffer, 0, buffer.Length);
                }

                return buffer;
            }
        }

        /// <summary>
        /// Gets the type of this item to be displayed in the add dialog.
        /// </summary>
        public override string Type
        {
            get { return StringTable.TextProjectDefinitionType; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new instance of the node that is represented by this definition.
        /// </summary>
        /// <returns>
        /// A new instance of the node that is represented by this definition.
        /// </returns>
        public override ProjectNode CreateProjectNode()
        {
            return new DialogueProjectNode(
                this,
                new List<ProjectItemDefinition>() { new DialogueItemDefinition() });
        }

        /// <summary>
        /// Attempts to get the item definition for the specified extension.
        /// </summary>
        /// <param name="extension">
        /// The extension whose associated item definition should be retrieved.
        /// </param>
        /// <returns>
        /// The item definition that's associated with the specified extension.
        /// </returns>
        public override ProjectItemDefinition GetItemDefinition(string extension)
        {
            if (String.Equals(extension, ".dstar"))
            {
                return new OldDialogueItemDefinition();
            }
            else if (String.Equals(extension, ".dstar2"))
            {
                return new DialogueItemDefinition();
            }

            return null;
        }

        /// <summary>
        /// Gives the definition a opportunity to run a wizard before the project is created so
        /// that the user can set certain properties into the template before the file gets
        /// saved and replace all other variables.
        /// </summary>
        /// <param name="template">
        /// The template that is being used to create the project.
        /// </param>
        /// <param name="fullPath">
        /// The fullPath the new project is being saved to. So that relative paths can be
        /// determined.
        /// </param>
        /// <returns>
        /// A value indicating whether the wizard finished successfully. If false is returned
        /// the creation process is cancelled.
        /// </returns>
        /// <remarks>
        /// This method should modify the specified template with any additional properties
        /// from the wizard and replaced variables.
        /// </remarks>
        public override bool ReplaceVariables(ref byte[] template, string fullPath)
        {
            string text = Encoding.UTF8.GetString(template);

            text = text.Replace("$(*NewGuid)", Guid.NewGuid().ToString("D"));
            text = text.Replace("$(Name)", Path.GetFileNameWithoutExtension(fullPath));

            DialogueProjectWizard wizard = new DialogueProjectWizard();
            wizard.Owner = Application.Current.MainWindow;
            if (wizard.ShowDialog() != true)
            {
                return false;
            }

            string directory = Path.GetDirectoryName(fullPath);
            string configFullPath = wizard.ConfigurationPath;
            string path = Shlwapi.MakeRelativeFromDirectoryToFile(directory, configFullPath);
            text = text.Replace("$(ConfigurationPath)", path);
            template = Encoding.UTF8.GetBytes(text);
            return true;
        }
        #endregion Methods
    } // DialogueStar.Definitions.DialogueProjectDefinition {Class}
} // DialogueStar.Definitions {Namespace}
