﻿namespace SanScript.EditorExtensions.Outlining
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Linq;
    using Irony.Parsing;
    using Microsoft.VisualStudio.Text;
    using Microsoft.VisualStudio.Text.Tagging;

    /// <summary>
    /// Represents a region used for outlining.
    /// </summary>
    internal class OutliningRegion
    {
        #region Fields
        /// <summary>
        /// The hash set containing the strings that start a outlining region.
        /// </summary>
        private static HashSet<string> _regionStarts = CreateRegionStarts();

        /// <summary>
        /// The hash set containing the strings that end a outlining region.
        /// </summary>
        private static HashSet<string> _regionEnds = CreateRegionEnds();

        /// <summary>
        /// The private map containing the mapping between region start tags and their
        /// corresponding end tags.
        /// </summary>
        private static Dictionary<string, string> _regionMap = CreateRegionMap();

        /// <summary>
        /// The private field used for the <see cref="StartPoint"/> property.
        /// </summary>
        private SnapshotPoint _startPoint;

        /// <summary>
        /// The private field used for the <see cref="EndPoint"/> property.
        /// </summary>
        private SnapshotPoint _endPoint;

        /// <summary>
        /// The private field used for the <see cref="IsComment"/> property.
        /// </summary>
        private bool _isComment;

        /// <summary>
        /// The private field used for the <see cref="Tagger"/> property.
        /// </summary>
        private OutliningTagger _tagger;

        /// <summary>
        /// The private field used for the <see cref="Parent"/> property.
        /// </summary>
        private OutliningRegion _parent;

        /// <summary>
        /// The private field used for the <see cref="Children"/> property.
        /// </summary>
        private List<OutliningRegion> _children;

        private string _expectedEndTag;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="OutliningRegion"/> class.
        /// </summary>
        public OutliningRegion()
        {
            this._children = new List<OutliningRegion>();
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the start point for this region.
        /// </summary>
        public SnapshotPoint StartPoint
        {
            get { return this._startPoint; }
            set { this._startPoint = value; }
        }
        
        /// <summary>
        /// Gets or sets the end point for this region.
        /// </summary>
        public SnapshotPoint EndPoint
        {
            get { return this._endPoint; }
            set { this._endPoint = value; }
        }

        /// <summary>
        /// Gets the snapshot line that contains the start point.
        /// </summary>
        public ITextSnapshotLine StartLine
        {
            get { return StartPoint.GetContainingLine(); }
        }

        /// <summary>
        /// Gets the snapshot line that contains the end point.
        /// </summary>
        public ITextSnapshotLine EndLine
        {
            get { return EndPoint.GetContainingLine(); }
        }

        /// <summary>
        /// Gets the inner text for this region.
        /// </summary>
        public string InnerText
        {
            get
            {
                int startIndex = this._startPoint.Position;
                int length = this._endPoint.Position - this._startPoint.Position + 1;
                return StartPoint.Snapshot.GetText(startIndex, length);
            }
        }

        /// <summary>
        /// Gets a value indicating whether this region has an endpoint.
        /// </summary>
        public bool IsComplete
        {
            get { return this._endPoint.Snapshot != null; }
        }

        /// <summary>
        /// Gets or sets the tagger which created this outlining region.
        /// </summary>
        public OutliningTagger Tagger
        {
            get { return this._tagger; }
            set { this._tagger = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this region represents a comment.
        /// </summary>
        public bool IsComment
        {
            get { return this._isComment; }
            set { this._isComment = value; }
        }

        /// <summary>
        /// Get or sets the parent outlining region for this region.
        /// </summary>
        public OutliningRegion Parent
        {
            get { return this._parent; }
            set { this._parent = value; }
        }

        /// <summary>
        /// Gets or sets the child outlining regions for this region.
        /// </summary>
        public List<OutliningRegion> Children
        {
            get { return this._children; }
            set { this._children = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Uses the specified tokens to create a outlining region tree starting with the
        /// specified root.
        /// </summary>
        /// <param name="tokens">
        /// The tokens that will be parsed.
        /// </param>
        /// <param name="root">
        /// The root of the outlining tree that the tokens will be parsed into.
        /// </param>
        public static void ParseTokens(List<Token> tokens, ITextSnapshot snapshot, OutliningRegion root)
        {
            while (tokens.Count > 0)
            {
                Token token = tokens[0];
                tokens.RemoveAt(0);
                if (IsRegionStart(token))
                {
                    OutliningRegion region = new OutliningRegion();
                    region.Parent = root;
                    root.Children.Add(region);
                    region.StartPoint = new SnapshotPoint(snapshot, token.Location.Position);
                    region._expectedEndTag = GetEndString(token);
                    ParseTokens(tokens, snapshot, region);
                }
                else if (IsRegionEnd(token))
                {
                    OutliningRegion endRegion = root;
                    bool found = false;
                    while (endRegion != null && !found)
                    {
                        found = String.Equals(endRegion._expectedEndTag, token.Text.ToUpper());
                        if (!found)
                        {
                            endRegion = endRegion.Parent;
                        }
                    }

                    if (found)
                    {
                        int position = token.Location.Position + token.Length;
                        endRegion.EndPoint = new SnapshotPoint(snapshot, position);
                    }

                    if (endRegion != null && endRegion.Parent != null)
                    {
                        ParseTokens(tokens, snapshot, endRegion.Parent);
                    }
                    else
                    {
                        if (endRegion != null)
                        {
                            ParseTokens(tokens, snapshot, endRegion);
                        }
                        else
                        {
                            ParseTokens(tokens, snapshot, root);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Retrieves this outlining region as a tag span for the outlining tagger.
        /// </summary>
        /// <returns>
        /// A new tag span that represents this outlining region for the outlining tagger.
        /// </returns>
        public TagSpan<IOutliningRegionTag> AsOutliningRegionTag()
        {
            SnapshotSpan span = this.AsSnapshotSpan();
            string hoverText = span.GetText();
            string before = this.StartLine.GetText().Substring(0, this.StartPoint - this.StartLine.Start);
            string beforeSpace = String.Empty;
            for (int i = before.Length - 1; i >= 0; i--)
            {
                if (!Char.IsWhiteSpace(before[i]))
                {
                    break;
                }

                hoverText = before[i] + hoverText;
            }


            string[] lines = hoverText.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            string tabSpaces = new string(' ', this.Tagger.TabSize);

            int empty = 0;
            while (empty < lines.Length && string.IsNullOrWhiteSpace(lines[empty]))
            {
                empty++;
            }

            string[] textLines = new string[Math.Min(lines.Length - empty, 25)];
            for (int i = 0; i < textLines.Length; i++)
            {
                textLines[i] = lines[i + empty].Replace("\t", tabSpaces);
            }

            int minIndent = int.MaxValue;
            foreach (string s in textLines)
            {
                minIndent = Math.Min(minIndent, GetIndentation(s));
            }

            StringBuilder builder = new StringBuilder(hoverText.Length);
            for (int i = 0; i < textLines.Length; i++)
            {
                if (i == textLines.Length - 1)
                {
                    if (textLines.Length != lines.Length - empty)
                    {
                        builder.Append("...");
                        continue;
                    }
                }

                string value = String.Empty;
                if (textLines[i].Length > minIndent)
                {
                    value = textLines[i].Substring(minIndent);
                }

                builder.AppendLine(value);
            }

            string collapsedForm = textLines[0].Trim();// this.GetCollapsedText();
            if (this.IsComment)
            {
                collapsedForm = "/**/";
            }

            string collapsedHintForm = builder.ToString();
            OutliningRegionTag tag =
                new OutliningRegionTag(false, false, collapsedForm, collapsedHintForm);

            return new TagSpan<IOutliningRegionTag>(span, tag);
        }

        /// <summary>
        /// Retrieves this outlining region as a snapshot span.
        /// </summary>
        /// <returns>
        /// A new snapshot span that represents this outlining region.
        /// </returns>
        public SnapshotSpan AsSnapshotSpan()
        {
            return new SnapshotSpan(this.StartPoint, this.EndPoint);
        }

        /// <summary>
        /// Retrieves the collapsed text for this outlining region.
        /// </summary>
        /// <returns>
        /// The string that contains the text used for the collapsed text for this outlining
        /// region.
        /// </returns>
        private string GetCollapsedText()
        {
            return "...";
        }

        /// <summary>
        /// Gets line indent in whitespaces.
        /// </summary>
        /// <param name="s">
        /// The string to analyze.
        /// </param>
        /// <returns>
        /// The count of whitespaces at the beginning of string. If the string is made up of
        /// nothing but whitespaces then int.MaxValue is returned.
        /// </returns>
        private static int GetIndentation(string s)
        {
            int i = 0;
            while (i < s.Length && char.IsWhiteSpace(s[i]))
            {
                i++;
            }

            return i == s.Length ? int.MaxValue : i;
        }

        /// <summary>
        /// Determines whether the specified token is the start of a region.
        /// </summary>
        /// <param name="token">
        /// The token to analysis.
        /// </param>
        /// <returns>
        /// True if the specified token is the start of a region. Otherwise, false.
        /// </returns>
        private static bool IsRegionStart(Token token)
        {
            return _regionStarts.Contains(token.Text.ToUpper());
        }

        /// <summary>
        /// Determines whether the specified token is the end of a region.
        /// </summary>
        /// <param name="token">
        /// The token to analysis.
        /// </param>
        /// <returns>
        /// True if the specified token is the end of a region. Otherwise, false.
        /// </returns>
        private static bool IsRegionEnd(Token token)
        {
            return _regionEnds.Contains(token.Text.ToUpper());
        }

        /// <summary>
        /// Creates a hash set containing the strings that start a outlining region.
        /// </summary>
        /// <returns>
        /// A hash set containing the strings that start a outlining region.
        /// </returns>
        private static HashSet<string> CreateRegionStarts()
        {
            HashSet<string> values = new HashSet<string>();
            values.Add("SCOPE");
            values.Add("IF");
            values.Add("WHILE");
            values.Add("REPEAT");
            values.Add("FOR");
            values.Add("SWITCH");
            values.Add("SCRIPT");
            values.Add("GLOBALS");
            values.Add("STRUCT");
            values.Add("PROC");
            values.Add("FUNC");
            values.Add("ENUM");
            values.Add("#IF");
            values.Add("#IFDEF");
            return values;
        }

        /// <summary>
        /// Creates a hash set containing the strings that end a outlining region.
        /// </summary>
        /// <returns>
        /// A hash set containing the strings that end a outlining region.
        /// </returns>
        private static HashSet<string> CreateRegionEnds()
        {
            HashSet<string> values = new HashSet<string>();
            values.Add("ENDSCOPE");
            values.Add("ENDIF");
            values.Add("ENDWHILE");
            values.Add("ENDREPEAT");
            values.Add("ENDFOR");
            values.Add("ENDSWITCH");
            values.Add("ENDSCRIPT");
            values.Add("ENDGLOBALS");
            values.Add("ENDSTRUCT");
            values.Add("ENDPROC");
            values.Add("ENDFUNC");
            values.Add("ENDENUM");
            values.Add("#ENDIF");
            return values;
        }

        /// <summary>
        /// Creates a map that maps the strings that start a outlining region to their
        /// corresponding string that ends that region.
        /// </summary>
        /// <returns>
        /// A map containing the mapping between start and end region tags.
        /// </returns>
        private static Dictionary<string, string> CreateRegionMap()
        {
            Dictionary<string, string> map = new Dictionary<string, string>();
            map.Add("SCOPE", "ENDSCOPE");
            map.Add("IF", "ENDIF");
            map.Add("WHILE", "ENDWHILE");
            map.Add("REPEAT", "ENDREPEAT");
            map.Add("FOR", "ENDFOR");
            map.Add("SWITCH", "ENDSWITCH");
            map.Add("SCRIPT", "ENDSCRIPT");
            map.Add("GLOBALS", "ENDGLOBALS");
            map.Add("STRUCT", "ENDSTRUCT");
            map.Add("PROC", "ENDPROC");
            map.Add("FUNC", "ENDFUNC");
            map.Add("ENUM", "ENDENUM");
            map.Add("#IF", "#ENDIF");
            map.Add("#IFDEF", "#ENDIF");
            return map;
        }

        /// <summary>
        /// Retrieves the end string that completes the region for the specified token.
        /// </summary>
        /// <param name="token">
        /// The token whose end region needs to be found.
        /// </param>
        /// <returns>
        /// The string that completes the region that is started by the specified token.
        /// </returns>
        private static string GetEndString(Token token)
        {
            string endString = null;
            if (!_regionMap.TryGetValue(token.Text.ToUpper(), out endString))
            {
                return null;
            }

            return endString;
        }
        #endregion Methods
    } // SanScript.EditorExtensions.Outlining.OutliningRegion {Class}
} // SanScript.EditorExtensions.Outlining {Namespace}
