﻿namespace SanScript.Project
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Runtime.InteropServices;
    using Microsoft.VisualStudio.Project;

    /// <summary>
    /// Represents the properties shown in the property grid when a SanScript header or source
    /// file is selected.
    /// </summary>
    [ComVisible(true), CLSCompliant(false)]
    [Guid("519A6E9A-FA82-4BDC-8A0A-D742168A5C4D")]
    public class SanScriptFileNodeProperties : SingleFileGeneratorNodeProperties
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SanScriptFileNodeProperties"/> class
        /// to be used for the specified node.
        /// </summary>
        /// <param name="node">
        /// The node whose properties this contains.
        /// </param>
        public SanScriptFileNodeProperties(HierarchyNode node)
            : base(node)
        {
            this.BuildAction = BuildAction.Compile;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the current build action for the associated file.
        /// </summary>
        [SRCategoryAttribute(Microsoft.VisualStudio.Project.SR.Advanced)]
        [LocDisplayName(Microsoft.VisualStudio.Project.SR.BuildAction)]
        [SRDescriptionAttribute(Microsoft.VisualStudio.Project.SR.BuildActionDescription)]
        public virtual SanScriptBuildAction SanScriptBuildAction
        {
            get
            {
                string value = this.Node.ItemNode.ItemName;
                if (value == null || value.Length == 0)
                {
                    return SanScriptBuildAction.None;
                }

                return (SanScriptBuildAction)Enum.Parse(typeof(SanScriptBuildAction), value);
            }

            set
            {
                this.Node.ItemNode.ItemName = value.ToString();
            }
        }

        /// <summary>
        /// Gets or sets the current build action for the associated file. Overridden to hide
        /// the default one from the user and make <see cref="SanScriptBuildAction"/> the only
        /// one set.
        /// </summary>
        [Browsable(false)]
        public override BuildAction BuildAction
        {
            get
            {
                switch (this.SanScriptBuildAction)
                {
                    case SanScriptBuildAction.Compile:
                        return BuildAction.Compile;
                }

                string buildAction = this.SanScriptBuildAction.ToString();
                BuildAction result = BuildAction.None;
                bool parsed = Enum.TryParse<BuildAction>(buildAction, out result);
                Debug.Assert(parsed, "Invalid build action set, falling back to None");
                return result;
            }

            set
            {
                string buildAction = value.ToString();
                SanScriptBuildAction result = SanScriptBuildAction.None;
                bool parsed = Enum.TryParse<SanScriptBuildAction>(buildAction, out result);
                Debug.Assert(parsed, "Invalid build action set, falling back to None");
                this.SanScriptBuildAction = result;
            }
        }

        /// <summary>
        /// Gets or sets the custom tool for this file node. Overridden to hide it from the
        /// user.
        /// </summary>
        [SRCategoryAttribute(SR.Advanced)]
        [LocDisplayName(SR.CustomTool)]
        [SRDescriptionAttribute(SR.CustomToolDescription)]
        [Browsable(false)]
        public override string CustomTool
        {
            get { return base.CustomTool; }
            set { base.CustomTool = value; }
        }

        /// <summary>
        /// Gets or sets the custom tool namespace for this file node. Overridden to hide it
        /// from the user.
        /// </summary>
        [SRCategoryAttribute(Microsoft.VisualStudio.Project.SR.Advanced)]
        [LocDisplayName(SR.CustomToolNamespace)]
        [SRDescriptionAttribute(SR.CustomToolNamespaceDescription)]
        [Browsable(false)]
        public override string CustomToolNamespace
        {
            get { return base.CustomToolNamespace; }
            set { base.CustomToolNamespace = value; }
        }
        #endregion Properties
    } // SanScript.Project.SanScriptFileNodeProperties {Class}
} // SanScript.Project {Namespace}
