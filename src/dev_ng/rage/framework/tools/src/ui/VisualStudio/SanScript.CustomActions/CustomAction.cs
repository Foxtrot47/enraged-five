﻿namespace SanScript.CustomActions
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using Microsoft.Deployment.WindowsInstaller;

    public class CustomActions
    {
        [CustomAction]
        public static ActionResult ExecuteVsixFile(Session session)
        {
            session.Log("Begin MyCustomAction");

            string path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            path = Path.Combine(path, "Microsoft", "VisualStudio", "11.0", "Extensions");
            path = Path.Combine(path, "Rockstar Games", "SanScript", "1.0");
            path = Path.Combine(path, "SanScript.Project.vsix");

            Process process = Process.Start(path);
            process.WaitForExit();

            return ActionResult.Success;
        }
    }
}
