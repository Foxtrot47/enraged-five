﻿namespace SanScript.CompilerTask
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using Microsoft.Build.Framework;
    using Microsoft.Build.Utilities;

    /// <summary>
    /// 
    /// </summary>
    public class FilenameValidationTask : Task
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="SourceFiles"/> property.
        /// </summary>
        private string[] _sourceFiles;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="FilenameValidationTask"/> class.
        /// </summary>
        public FilenameValidationTask()
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the list of san script source and header files that should be
        /// compiled.
        /// </summary>
        public string[] SourceFiles
        {
            get { return this._sourceFiles; }
            set { this._sourceFiles = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Main entry point for the task
        /// </summary>
        /// <returns>
        /// True if the task ran successfully; otherwise, false.
        /// </returns>
        public override bool Execute()
        {
            if (this.SourceFiles == null)
            {
                return true;
            }

            HashSet<string> filenames = new HashSet<string>();
            foreach (string sourceFile in this.SourceFiles)
            {
                string filename = Path.GetFileName(sourceFile).ToLower();
                if (!filenames.Contains(filename))
                {
                    filenames.Add(filename);
                    continue;
                }

                BuildErrorEventArgs e = new BuildErrorEventArgs("Validation", "CS1026", sourceFile, 1, 1, 0, 0, "A duplicate filename '{0}' has been detected. This will result in the sco file being overwritten in the output directory.", "http://msdn.microsoft.com/en-us/library/vstudio/tc5zwdf7(v=vs.120).aspx", "SanScript Compiler", DateTime.Now, filename);
                this.BuildEngine4.LogErrorEvent(e);
                System.Threading.Thread.Sleep(100);
            }


            return filenames.Count == this.SourceFiles.Length;
        }
        #endregion Methods
    } // SanScript.CompilerTask.FilenameValidationTask {Class}
} // SanScript.CompilerTask {Namespace}
