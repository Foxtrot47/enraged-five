﻿namespace SanScript.Project.Library
{
    using System;
    using Microsoft.VisualStudio.Shell.Interop;

    /// <summary>
    /// Enumeration of the possible types of node. The type of a node can be the combination
    /// of one of more of these values.
    /// </summary>
    [Flags]
    public enum LibraryNodeTypes
    {
        /// <summary>
        /// Specifies that the library node has no type associated with it.
        /// </summary>
        None = 0,

        /// <summary>
        /// Specifies that the library node represents a hierarchy.
        /// </summary>
        Hierarchy = _LIB_LISTTYPE.LLT_HIERARCHY,

        /// <summary>
        /// Specifies that the library node represents a namespace.
        /// </summary>
        Namespaces = _LIB_LISTTYPE.LLT_NAMESPACES,

        /// <summary>
        /// Specifies that the library node represents a class.
        /// </summary>
        Classes = _LIB_LISTTYPE.LLT_CLASSES,

        /// <summary>
        /// Specifies that the library node represents a member.
        /// </summary>
        Members = _LIB_LISTTYPE.LLT_MEMBERS,

        /// <summary>
        /// Specifies that the library node represents a package.
        /// </summary>
        Package = _LIB_LISTTYPE.LLT_PACKAGE,

        /// <summary>
        /// Specifies that the library node represents a physical container.
        /// </summary>
        PhysicalContainer = _LIB_LISTTYPE.LLT_PHYSICALCONTAINERS,

        /// <summary>
        /// Specifies that the library node represents a containment object.
        /// </summary>
        Containment = _LIB_LISTTYPE.LLT_CONTAINMENT,

        /// <summary>
        /// Specifies that the library node represents a object that can be contained by
        /// another object.
        /// </summary>
        ContainedBy = _LIB_LISTTYPE.LLT_CONTAINEDBY,

        /// <summary>
        /// Specifies that the library node represents represents a object that uses classes.
        /// </summary>
        UsesClasses = _LIB_LISTTYPE.LLT_USESCLASSES,

        /// <summary>
        /// Specifies that the library node represents a object that can be used by classes.
        /// </summary>
        UsedByClasses = _LIB_LISTTYPE.LLT_USEDBYCLASSES,

        /// <summary>
        /// Specifies that the library node represents nested classes.
        /// </summary>
        NestedClasses = _LIB_LISTTYPE.LLT_NESTEDCLASSES,

        /// <summary>
        /// Specifies that the library node represents a object that has inherited a interface.
        /// </summary>
        InheritedInterface = _LIB_LISTTYPE.LLT_INHERITEDINTERFACES,

        /// <summary>
        /// Specifies that the library node represents a interface that can be used by classes.
        /// </summary>
        InterfaceUsedByClasses = _LIB_LISTTYPE.LLT_INTERFACEUSEDBYCLASSES,

        /// <summary>
        /// Specifies that the library node represents a definition.
        /// </summary>
        Definitions = _LIB_LISTTYPE.LLT_DEFINITIONS,

        /// <summary>
        /// Specifies that the library node represents a reference to a definition.
        /// </summary>
        References = _LIB_LISTTYPE.LLT_REFERENCES,

        /// <summary>
        /// Specifies that the library node represents a object whose expansion should be
        /// deferred.
        /// </summary>
        DeferExpansion = _LIB_LISTTYPE.LLT_DEFEREXPANSION,
    } // SanScript.Project.Library.LibraryNodeTypes {Enum}
} // SanScript.Project.Library {Namespace}
