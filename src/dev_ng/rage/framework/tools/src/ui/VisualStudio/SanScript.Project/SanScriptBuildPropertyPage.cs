﻿namespace SanScript.Project
{
    using System;
    using System.ComponentModel;
    using System.Runtime.InteropServices;
    using Microsoft.VisualStudio;
    using Microsoft.VisualStudio.Project;

    /// <summary>
    /// Describes the build property page for the san script project type.
    /// </summary>
    [ComVisible(true)]
    [Guid("A2F59A82-C68A-40FF-92A0-9B2FEA92AFF8")]
    public class SanScriptBuildPropertyPage : SettingsPage
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="OutputPath"/> property.
        /// </summary>
        private string _outputPath;

        /// <summary>
        /// The private field used for the <see cref="IncludeFile"/> property.
        /// </summary>
        private string _includeFile;

        /// <summary>
        /// The private field used for the <see cref="IncludeDirectories"/> property.
        /// </summary>
        private string[] _includeDirectories;

        /// <summary>
        /// The private field used for the <see cref="CompilerExecutable"/> property.
        /// </summary>
        private string _compilerExecutable;

        /// <summary>
        /// The private field used for the <see cref="DebugParser"/> property.
        /// </summary>
        private bool _debugParser;

        /// <summary>
        /// The private field used for the <see cref="DumpThreadState"/> property.
        /// </summary>
        private bool _dumpThreadState;

        /// <summary>
        /// The private field used for the <see cref="DisplayDisassembly"/> property.
        /// </summary>
        private bool _displayDisassembly;

        /// <summary>
        /// The private field used for the <see cref="EnableHsm"/> property.
        /// </summary>
        private bool _enableHsm;

        /// <summary>
        /// The private field used for the <see cref="TreatWarningsAsErrors"/> property.
        /// </summary>
        private bool _treatWarningsAsErrors;

        /// <summary>
        /// The private field used for the <see cref="CompileToConfigurationFolder"/> property.
        /// </summary>
        private bool _compileToConfigurationFolder;

        /// <summary>
        /// The private field used for the <see cref="CustomArguments"/> property.
        /// </summary>
        private string _customArguments;

        /// <summary>
        /// The private field used for the <see cref="CreateDependencyFile"/> property.
        /// </summary>
        private bool _createDependencyFile;

        /// <summary>
        /// The private field used for the <see cref="CreateNativeFile"/> property.
        /// </summary>
        private bool _createNativeFile;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SanScriptBuildPropertyPage"/> class.
        /// </summary>
        public SanScriptBuildPropertyPage()
        {
            this.Name = "Build";
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the additional custom arguments to add to the commandline for the
        /// script compilier.
        /// </summary>
        [Category("Build")]
        [DisplayName("Custom Arguments")]
        [Description("Additional custom arguments to add to the commandline for the script compilier.")]
        [DefaultValue("")]
        public string CustomArguments
        {
            get
            {
                return this._customArguments;
            }

            set
            {
                this._customArguments = value;
                this.IsDirty = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a .d dependency file is also created.
        /// </summary>
        [Category("Build")]
        [DisplayName("Create Dependency File")]
        [Description("Determines whether a .d dependency file is also created.")]
        [DefaultValue(false)]
        public bool CreateDependencyFile
        {
            get
            {
                return this._createDependencyFile;
            }

            set
            {
                this._createDependencyFile = value;
                this.IsDirty = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a .native.txt native file is also created.
        /// </summary>
        [Category("Build")]
        [DisplayName("Create Native File")]
        [Description("Determines whether a .native.txt native file is also created.")]
        [DefaultValue(false)]
        public bool CreateNativeFile
        {
            get
            {
                return this._createNativeFile;
            }

            set
            {
                this._createNativeFile = value;
                this.IsDirty = true;
            }
        }

        /// <summary>
        /// Gets or sets the path to the directory that the complied files get added to.
        /// </summary>
        [Category("Build")]
        [DisplayName("Output Directory")]
        [Description("The path to the primary output.")]
        [DefaultValue("")]
        public string OutputPath
        {
            get
            {
                return this._outputPath;
            }

            set
            {
                this._outputPath = value;
                this.IsDirty = true;
            }
        }

        /// <summary>
        /// Gets or sets the name of the forced include file for the build.
        /// </summary>
        [Category("Build")]
        [DisplayName("Include File")]
        [Description("One or more forced include files.")]
        [DefaultValue("")]
        public string IncludeFile
        {
            get
            {
                return this._includeFile;
            }

            set
            {
                this._includeFile = value;
                this.IsDirty = true;
            }
        }

        ///// <summary>
        ///// Gets or sets the path(s) to find include files in.
        ///// </summary>
        //[Category("Build")]
        //[DisplayName("Include Path")]
        //[Description("Specifices one or more directories to add to the include path; separate with semi-colons if more than one.")]
        //[DefaultValue("")]
        //public string IncludeDirectories
        //{
        //    get
        //    {
        //        return this._includeDirectories;
        //    }

        //    set
        //    {
        //        this._includeDirectories = value;
        //        this.IsDirty = true;
        //    }
        //}

        /// <summary>
        /// Gets or sets the path to the compiler executable to use during the build process.
        /// </summary>
        [Category("Build")]
        [DisplayName("Compiler Executable")]
        [Description("The full path to the compiler executable.")]
        [DefaultValue("")]
        public string CompilerExecutable
        {
            get
            {
                return this._compilerExecutable;
            }

            set
            {
                this._compilerExecutable = value;
                this.IsDirty = true;
            }
        }

        [Category("Build")]
        [DisplayName("Debug Parser")]
        [Description("Determines whether the build also creates debug symbol files.")]
        [DefaultValue(false)]
        public bool DebugParser
        {
            get
            {
                return this._debugParser;
            }

            set
            {
                this._debugParser = value;
                this.IsDirty = true;
            }
        }

        [Category("Build")]
        [DisplayName("Dump Thread State")]
        [Description("")]
        [DefaultValue(false)]
        public bool DumpThreadState
        {
            get
            {
                return this._dumpThreadState;
            }

            set
            {
                this._dumpThreadState = value;
                this.IsDirty = true;
            }
        }

        [Category("Build")]
        [DisplayName("Display Disassembly")]
        [Description("")]
        [DefaultValue(false)]
        public bool DisplayDisassembly
        {
            get
            {
                return this._displayDisassembly;
            }

            set
            {
                this._displayDisassembly = value;
                this.IsDirty = true;
            }
        }

        [Category("Build")]
        [DisplayName("Enable HSM")]
        [Description("")]
        [DefaultValue(false)]
        public bool EnableHsm
        {
            get
            {
                return this._enableHsm;
            }

            set
            {
                this._enableHsm = value;
                this.IsDirty = true;
            }
        }

        [Category("Build")]
        [DisplayName("Treat Warnings as Errors")]
        [Description("")]
        [DefaultValue(false)]
        public bool TreatWarningsAsErrors
        {
            get
            {
                return this._treatWarningsAsErrors;
            }

            set
            {
                this._treatWarningsAsErrors = value;
                this.IsDirty = true;
            }
        }

        [Category("Build")]
        [DisplayName("Compile to Configuration Folder")]
        [Description("")]
        [DefaultValue(false)]
        public bool CompileToConfigurationFolder
        {
            get
            {
                return this._compileToConfigurationFolder;
            }

            set
            {
                this._compileToConfigurationFolder = value;
                this.IsDirty = true;
            }
        }

        [Category("Build")]
        [DisplayName("Include Path")]
        [Description("Specifices one or more directories to add to the include path.")]
        [DefaultValue("")]
        public string[] IncludeDirectories
        {
            get
            {
                return this._includeDirectories;
            }

            set
            {
                this._includeDirectories = value;
                this.IsDirty = true;
            }
        }
        private string[] _testing;
        #endregion Properties

        #region Methods
        /// <summary>
        /// Binds the private fields for this settings page through the configuration
        /// manager.
        /// </summary>
        protected override void BindProperties()
        {
            ProjectConfig[] test = this.GetProjectConfigurations();

            this._customArguments = this.GetConfigProperty("CustomArguments");
            bool.TryParse(this.GetConfigProperty("CreateDependencyFile"), out this._createDependencyFile);
            bool.TryParse(this.GetConfigProperty("CreateNativeFile"), out this._createNativeFile);

            this._outputPath = this.GetConfigProperty("OutputPath");
            this._includeFile = this.GetConfigProperty("IncludeFile");
            //this._includeDirectories = this.GetConfigProperty("IncludeDirectories");
            this._compilerExecutable = this.GetConfigProperty("CompilerExecutable");

            string directories = this.GetConfigProperty("IncludeDirectories");
            if (directories != null)
            {
                this._includeDirectories = directories.Split(';');
            }
            else
            {
                this._includeDirectories = new string[0];
            }

            bool.TryParse(this.GetConfigProperty("DebugParser"), out this._debugParser);
            bool.TryParse(this.GetConfigProperty("DumpThreadState"), out this._dumpThreadState);
            bool.TryParse(this.GetConfigProperty("DisplayDisassembly"), out this._displayDisassembly);
            bool.TryParse(this.GetConfigProperty("EnableHsm"), out this._enableHsm);
            bool.TryParse(this.GetConfigProperty("TreatWarningsAsErrors"), out this._treatWarningsAsErrors);
            bool.TryParse(this.GetConfigProperty("CompileToConfigurationFolder"), out this._compileToConfigurationFolder);
        }

        /// <summary>
        /// Applies the changes made to these properties.
        /// </summary>
        /// <returns>
        /// Always returns VSConstants.S_OK.
        /// </returns>
        protected override int ApplyChanges()
        {
            this.SetConfigProperty("CustomArguments", this._customArguments);
            this.SetConfigProperty("CreateDependencyFile", this._createDependencyFile.ToString());
            this.SetConfigProperty("CreateNativeFile", this._createNativeFile.ToString());

            this.SetConfigProperty("OutputPath", this._outputPath);
            this.SetConfigProperty("IncludeFile", this._includeFile);
            //this.SetConfigProperty("IncludeDirectories", this._includeDirectories);
            this.SetConfigProperty("CompilerExecutable", this._compilerExecutable);
            this.SetConfigProperty("IncludeDirectories", String.Join(";", this._includeDirectories));

            this.SetConfigProperty("DebugParser", this._debugParser.ToString());
            this.SetConfigProperty("DumpThreadState", this._dumpThreadState.ToString());
            this.SetConfigProperty("DisplayDisassembly", this._displayDisassembly.ToString());
            this.SetConfigProperty("EnableHsm", this._enableHsm.ToString());
            this.SetConfigProperty("TreatWarningsAsErrors", this._treatWarningsAsErrors.ToString());
            this.SetConfigProperty("CompileToConfigurationFolder", this._compileToConfigurationFolder.ToString());

            this.IsDirty = false;
            return VSConstants.S_OK;
        }
        #endregion Methods
    } // SanScript.Project.SanScriptBuildPropertyPage {Class}
} // SanScript.Project {Namespace}
