﻿namespace SanScript.CompilerTask
{
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using Microsoft.Build.Framework;
    using Microsoft.Build.Utilities;

    /// <summary>
    /// 
    /// </summary>
    public class IncludeMapperTask : Task
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="IncludeDirectories"/> property.
        /// </summary>
        private string _includeDirectories;

        /// <summary>
        /// The private field used for the <see cref="OutputPath"/> property.
        /// </summary>
        private string _outputPath;

        /// <summary>
        /// The private field used for the <see cref="SourceFiles"/> property.
        /// </summary>
        private string[] _sourceFiles;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="IncludeMapperTask"/> class.
        /// </summary>
        public IncludeMapperTask()
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the include path(s) to push to the compiler executable.
        /// </summary>
        public string IncludeDirectories
        {
            get { return this._includeDirectories; }
            set { this._includeDirectories = value; }
        }

        /// <summary>
        /// Gets or sets the list of san script source and header files that should be
        /// compiled.
        /// </summary>
        public string[] SourceFiles
        {
            get { return this._sourceFiles; }
            set { this._sourceFiles = value; }
        }

        public string SelectedFilesOne
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the output path.
        /// </summary>
        public string OutputPath
        {
            get { return this._outputPath; }
            set { this._outputPath = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Main entry point for the task
        /// </summary>
        /// <returns></returns>
        public override bool Execute()
        {
            try
            {
                DependencyCreatorTask.sw = Stopwatch.StartNew();
                DependencyCreatorTask.Mapper = new Dictionary<string, string>();
                foreach (string includeDirectory in this.IncludeDirectories.Split(';'))
                {
                    string[] paths = Directory.GetFiles(includeDirectory.Trim(), "*.sc?");
                    foreach (string path in paths)
                    {
                        string filename = Path.GetFileName(path);
                        if (!DependencyCreatorTask.Mapper.ContainsKey(filename))
                        {
                            string output = Path.Combine(this.OutputPath, filename);
                            output = Path.ChangeExtension(output, ".d");
                            DependencyCreatorTask.Mapper.Add(Path.GetFileName(path), output);
                        }
                    }
                }

                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion Methods
    } // SanScript.CompilerTask.IncludeMapperTask {Class}
} // SanScript.CompilerTask {Namespace}
