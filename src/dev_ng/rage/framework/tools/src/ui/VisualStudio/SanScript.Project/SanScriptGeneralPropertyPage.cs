﻿namespace SanScript.Project
{
    using System;
    using System.ComponentModel;
    using System.Runtime.InteropServices;
    using Microsoft.VisualStudio;
    using Microsoft.VisualStudio.Project;

    /// <summary>
    /// 
    /// </summary>
    [ComVisible(true)]
    [Guid("8EA3289D-86FB-4E06-9F18-C5981DA46E00")]
    public class SanScriptGeneralPropertyPage : SettingsPage
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="AssemblyName"/> property.
        /// </summary>
        private string _assemblyName;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SanScriptGeneralPropertyPage"/> class.
        /// </summary>
        public SanScriptGeneralPropertyPage()
        {
            this.Name = "General";
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the path to the directory that the complied files get added to.
        /// </summary>
        [Category("General")]
        [DisplayName("Project Name")]
        [Description("The name of the project.")]
        public string AssemblyName
        {
            get { return this._assemblyName; }
            set { this._assemblyName = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Binds the private fields for this settings page through the configuration
        /// manager.
        /// </summary>
        protected override void BindProperties()
        {
            this._assemblyName = this.ProjectMgr.GetProjectProperty("AssemblyName", true);
        }

        /// <summary>
        /// Applies the changes made to these properties.
        /// </summary>
        /// <returns>
        /// Always returns VSConstants.S_OK.
        /// </returns>
        protected override int ApplyChanges()
        {
            this.ProjectMgr.SetProjectProperty("AssemblyName", this._assemblyName);
            this.IsDirty = false;

            return VSConstants.S_OK;
        }
        #endregion Methods
    } // SanScript.Project.SanScriptBuildPropertyPage {Class}
} // SanScript.Project {Namespace}
