﻿namespace SanScript.Project.Library
{
    using Microsoft.VisualStudio.Shell.Interop;

    /// <summary>
    /// Used to identify a module. The module is identified using the hierarchy that contains
    /// it and its item id inside the hierarchy.
    /// </summary>
    internal sealed class SanScriptModuleId
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Hierarchy"/> property.
        /// </summary>
        private IVsHierarchy _hierarchy;

        /// <summary>
        /// The private field used for the <see cref="ItemId"/> property.
        /// </summary>
        private uint _itemId;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SanScriptModuleId"/> class.
        /// </summary>
        /// <param name="hierarchy">
        /// The hierarchy that owns this module.
        /// </param>
        /// <param name="itemId">
        /// The identifier for the item within the hierarchy that is the module.
        /// </param>
        public SanScriptModuleId(IVsHierarchy hierarchy, uint itemId)
        {
            this._hierarchy = hierarchy;
            this._itemId = itemId;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets hierarchy that owns the module.
        /// </summary>
        public IVsHierarchy Hierarchy
        {
            get { return this._hierarchy; }
        }

        /// <summary>
        /// Gets the id of the item within the hierarchy that is the module.
        /// </summary>
        public uint ItemId
        {
            get { return this._itemId; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Retrieves the generated hash code for this object.
        /// </summary>
        /// <returns>
        /// The generated hash code for this object.
        /// </returns>
        public override int GetHashCode()
        {
            int hash = 0;
            if (null != this._hierarchy)
            {
                hash = this._hierarchy.GetHashCode();
            }

            hash = hash ^ (int)this._itemId;
            return hash;
        }

        /// <summary>
        /// Determines whether this object and the specified object are equal.
        /// </summary>
        /// <param name="obj">
        /// The other object to compare.
        /// </param>
        /// <returns>
        /// True if the specified item is equal to this object; otherwise, false.
        /// </returns>
        public override bool Equals(object obj)
        {
            SanScriptModuleId other = obj as SanScriptModuleId;
            if (null == other)
            {
                return false;
            }

            if (!this._hierarchy.Equals(other._hierarchy))
            {
                return false;
            }

            return this._itemId == other._itemId;
        }
        #endregion Methods
    } // SanScript.Project.Library.SanScriptModuleId {Class}
} // SanScript.Project.Library {Namespace}
