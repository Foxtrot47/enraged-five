﻿namespace SanScript.EditorExtensions.Outlining
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Threading;
    using Irony.Parsing;
    using Microsoft.VisualStudio.Text;
    using Microsoft.VisualStudio.Text.Editor;
    using Microsoft.VisualStudio.Text.Editor.OptionsExtensionMethods;
    using Microsoft.VisualStudio.Text.Tagging;
    using RSG.SanScript;

    /// <summary>
    /// 
    /// </summary>
    internal sealed class OutliningTagger : ITagger<IOutliningRegionTag>, IDisposable
    {
        #region Fields
        /// <summary>
        /// 
        /// </summary>
        private ITextBuffer _buffer;

        /// <summary>
        /// 
        /// </summary>
        private ITextSnapshot _snapshot;

        /// <summary>
        /// 
        /// </summary>
        private List<OutliningRegion> _regions;

        /// <summary>
        /// The update timer used to schedule the outline process.
        /// </summary>
        private DispatcherTimer _updateTimer;

        /// <summary>
        /// The private field used for the <see cref="TabSize"/> property.
        /// </summary>
        private int _tabSize;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="OutliningTagger"/> class.
        /// </summary>
        /// <param name="buffer">
        /// The buffer that is to be parsed for outlining.
        /// </param>
        /// <param name="editorOptions">
        /// The editor options being used for the specified buffer.
        /// </param>
        public OutliningTagger(ITextBuffer buffer, IEditorOptions editorOptions)
        {
            this._buffer = buffer;
            this._snapshot = buffer.CurrentSnapshot;
            this._regions = new List<OutliningRegion>();
            this._buffer.Changed += this.BufferChanged;

            editorOptions.OptionChanged += this.EditorOptionsChanged;
            this._tabSize = editorOptions.GetTabSize();

            this._updateTimer = new DispatcherTimer(DispatcherPriority.ApplicationIdle);
            this._updateTimer.Interval = TimeSpan.FromMilliseconds(300);
            this._updateTimer.Tick += (sender, args) =>
            {
                this._updateTimer.Stop();
                this.ReParse();
            };

            this.ReParse();
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// 
        /// </summary>
        public event EventHandler<SnapshotSpanEventArgs> TagsChanged;
        #endregion Events

        #region Properties
        /// <summary>
        /// Gets the tab size used for the associated text buffer.
        /// </summary>
        public int TabSize
        {
            get { return this._tabSize; }
            private set { this._tabSize = value; }
        }
        #endregion Properties

        #region Methods
        public IEnumerable<ITagSpan<IOutliningRegionTag>> GetTags(NormalizedSnapshotSpanCollection spans)
        {
            if (spans.Count == 0)
            {
                yield break;
            }

            List<OutliningRegion> currentRegions = this._regions;
            ITextSnapshot currentSnapshot = spans[0].Snapshot;
            SnapshotSpan entire = new SnapshotSpan(spans[0].Start, spans[spans.Count - 1].End).TranslateTo(currentSnapshot, SpanTrackingMode.EdgeExclusive);
            int startLineNumber = entire.Start.GetContainingLine().LineNumber;
            int endLineNumber = entire.End.GetContainingLine().LineNumber;
            foreach (var region in currentRegions)
            {
                if (region.StartLine.LineNumber > endLineNumber)
                {
                    continue;
                }

                if (region.EndLine.LineNumber < startLineNumber)
                {
                    continue;
                }

                yield return region.AsOutliningRegionTag();

                //var startLine = currentSnapshot.GetLineFromLineNumber(region.StartLine);
                //var endLine = currentSnapshot.GetLineFromLineNumber(region.EndLine);

                //var startPosition = startLine.Start.Position + region.StartOffset;
                //var length = (endLine.Start.Position + region.EndOffset) - startPosition;
                //if (region.IsComment)
                //{
                //    yield return new TagSpan<IOutliningRegionTag>(
                //        new SnapshotSpan(currentSnapshot,
                //            startLine.Start.Position + region.StartOffset, length),
                //        new OutliningRegionTag(false, false, "/**/", region.Tooltip));
                //}
                //else
                //{
                //    yield return new TagSpan<IOutliningRegionTag>(
                //        new SnapshotSpan(currentSnapshot,
                //            startLine.Start.Position + region.StartOffset, length),
                //        new OutliningRegionTag(false, false, this._ellipsis, region.Tooltip));
                //}
            }
        }

        private void BufferChanged(object sender, TextContentChangedEventArgs e)
        {
            this._updateTimer.Stop();
            this._updateTimer.Start();
        }

        private void ReParse()
        {
            ITextSnapshot snapshot = this._buffer.CurrentSnapshot;
            List<OutliningRegion> newRegions = new List<OutliningRegion>();

            Parser parser = new Parser(new SanScriptGrammar());
            parser.Context.Mode = ParseMode.VsLineScan;
            parser.Scanner.VsSetSource(snapshot.GetText(), 0);
            int state = 0;
            Token token = parser.Scanner.VsReadToken(ref state);
            List<Token> tokens = new List<Token>();
            List<List<Token>> commentBlocks = new List<List<Token>>();
            List<Token> currentCommandBlock = null;
            while (token != null)
            {
                if (token.Category == TokenCategory.Comment)
                {
                    if (String.Equals(token.Terminal.Name, "MultiLineComment"))
                    {
                        commentBlocks.Add(new List<Token>() { token });
                        currentCommandBlock = null;
                    }
                    else
                    {
                        if (currentCommandBlock == null)
                        {
                            currentCommandBlock = new List<Token>();
                            commentBlocks.Add(currentCommandBlock);
                        }

                        currentCommandBlock.Add(token);
                    }
                }
                else
                {
                    currentCommandBlock = null;
                    tokens.Add(token);
                }

                token = parser.Scanner.VsReadToken(ref state);
            }

            this.FindHiddenRegions(commentBlocks, tokens, newRegions, snapshot);

            List<Span> oldSpans = this._regions.ConvertAll(r => r.AsSnapshotSpan().TranslateTo(snapshot, SpanTrackingMode.EdgeExclusive).Span);
            List<Span> newSpans = newRegions.ConvertAll(r => r.AsSnapshotSpan().Span);

            NormalizedSpanCollection oldSpanCol = new NormalizedSpanCollection(oldSpans);
            NormalizedSpanCollection newSpanCol= new NormalizedSpanCollection(newSpans);

            NormalizedSpanCollection removed =
                NormalizedSpanCollection.Difference(oldSpanCol, newSpanCol);

            int changeStart = int.MaxValue;
            int changeEnd = -1;

            if (removed.Count > 0)
            {
                changeStart = removed[0].Start;
                changeEnd = removed[removed.Count - 1].End;
            }

            if (newSpans.Count > 0)
            {
                changeStart = Math.Min(changeStart, newSpans[0].Start);
                changeEnd = Math.Max(changeEnd, newSpans[newSpans.Count - 1].End);
            }

            this._snapshot = snapshot;
            this._regions = newRegions;

            if (changeStart > changeEnd || this.TagsChanged == null)
            {
                return;
            }

            SnapshotSpan snapshotSpan =
                new SnapshotSpan(this._snapshot, Span.FromBounds(changeStart, changeEnd));
            SnapshotSpanEventArgs args = new SnapshotSpanEventArgs(snapshotSpan);
            this.TagsChanged(this, args);
        }

        private void FindHiddenRegions(
            List<List<Token>> commentBlocks,
            List<Token> tokens,
            List<OutliningRegion> regions,
            ITextSnapshot snapshot)
        {
            string[] separator = new string[] { Environment.NewLine };
            StringSplitOptions options = StringSplitOptions.None;
            foreach (List<Token> commentBlock in commentBlocks)
            {
                SourceLocation location = commentBlock[0].Location;
                int position = location.Position;
                OutliningRegion region = new OutliningRegion();
                region.StartPoint = new SnapshotPoint(snapshot, position);

                if (commentBlock.Count == 1)
                {
                    string[] lines = commentBlock[0].Text.Split(separator, options);
                    if (lines.Length == 1)
                    {
                        continue;
                    }

                    position += commentBlock[0].Length;
                }
                else
                {
                    location = commentBlock.Last().Location;
                    position = location.Position + commentBlock.Last().Length;
                }

                region.EndPoint = new SnapshotPoint(snapshot, position);
                region.IsComment = true;
                region.Tagger = this;
                regions.Add(region);
            }

            OutliningRegion regionTree = new OutliningRegion();
            OutliningRegion.ParseTokens(tokens, snapshot, regionTree);
            regions.AddRange(this.GetRegionList(regionTree));


            //Stack<Token> stack = new Stack<Token>();
            //List<KeyValuePair<Token, Token>> blocks = new List<KeyValuePair<Token, Token>>();
            //foreach (Token token in tokens)
            //{
            //    if (String.Equals(token.Text, "PROC", StringComparison.OrdinalIgnoreCase) || String.Equals(token.Text, "SCRIPT", StringComparison.OrdinalIgnoreCase) || String.Equals(token.Text, "WHILE", StringComparison.OrdinalIgnoreCase))
            //    {
            //        stack.Push(token);
            //    }

            //    if (String.Equals(token.Text, "ENDPROC", StringComparison.OrdinalIgnoreCase) || String.Equals(token.Text, "ENDSCRIPT", StringComparison.OrdinalIgnoreCase) || String.Equals(token.Text, "ENDWHILE", StringComparison.OrdinalIgnoreCase))
            //    {
            //        blocks.Add(new KeyValuePair<Token, Token>(stack.Pop(), token));
            //    }
            //}

            //foreach (KeyValuePair<Token, Token> block in blocks)
            //{
            //    OutliningRegion region = new OutliningRegion();
            //    region.StartLine = block.Key.Location.Line - 1;
            //    region.StartOffset = block.Key.Location.Column;
            //    region.EndLine = block.Value.Location.Line - 1;
            //    region.EndOffset = block.Value.Location.Column + block.Value.Length;
            //    region.Tagger = this;

            //    if (region.StartLine == region.EndLine)
            //    {
            //        continue;
            //    }

            //    region.Tooltip = textLines[region.StartLine];
            //    for (int i = region.StartLine + 1; i <= region.EndLine; i++)
            //    {
            //        region.Tooltip += "\n" + textLines[i];

            //    }
                
            //    regions.Add(region);
            //}
        }

        private List<OutliningRegion> GetRegionList(OutliningRegion tree)
        {
            List<OutliningRegion> regions = new List<OutliningRegion>(tree.Children.Count);
            foreach (OutliningRegion region in tree.Children)
            {
                if (region.IsComplete)
                {
                    if (region.StartLine.LineNumber != region.EndLine.LineNumber)
                    {
                        regions.Add(region);
                    }
                }

                if (region.Children.Count != 0)
                {
                    regions.AddRange(this.GetRegionList(region));
                }
            }

            foreach (OutliningRegion region in regions)
            {
                region.Tagger = this;
            }

            return regions;
        }

        public void Dispose()
        {
            this._updateTimer.Stop();
            this._buffer.Changed -= this.BufferChanged;
        }

        /// <summary>
        /// Occurs when any option changes in the editor option class that is associated with
        /// the text buffer.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The data sent for this event.
        /// </param>
        private void EditorOptionsChanged(object sender, EditorOptionChangedEventArgs e)
        {
            IEditorOptions editorOptions = sender as IEditorOptions;
            if (editorOptions == null)
            {
                return;
            }

            this._tabSize = editorOptions.GetTabSize();
        }
        #endregion Methods
    } // SanScript.EditorExtensions.Outlining.OutliningTagger {Class}
} // SanScript.EditorExtensions.Outlining {Namespace}
