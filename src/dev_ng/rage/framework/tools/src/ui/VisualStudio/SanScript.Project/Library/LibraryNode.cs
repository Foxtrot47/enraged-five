﻿namespace SanScript.Project.Library
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.Design;
    using Microsoft.VisualStudio;
    using Microsoft.VisualStudio.OLE.Interop;
    using Microsoft.VisualStudio.Shell;
    using Microsoft.VisualStudio.Shell.Interop;

    /// <summary>
    /// Represents a single node in the library hierarchy of the SanScript project and project
    /// items.
    /// </summary>
    public class LibraryNode : IVsSimpleObjectList2, IVsNavInfoNode
    {
        #region Fields
        /// <summary>
        ///  The capabilities for this node which is used for a number of properties.
        /// </summary>
        private LibraryNodeCapabilities _capabilities;

        /// <summary>
        /// The children nodes for this node.
        /// </summary>
        private List<LibraryNode> _children;

        /// <summary>
        /// The clipboard formats supported for this node.
        /// </summary>
        private List<VSOBJCLIPFORMAT> _clipboardFormats;

        /// <summary>
        /// The display data for this node.
        /// </summary>
        private VSTREEDISPLAYDATA _displayData;

        /// <summary>
        /// The command id for this node used for the context menu.
        /// </summary>
        private CommandID _contextMenuID;

        /// <summary>
        /// A cache used for the different filtered views based on node type.
        /// </summary>
        private Dictionary<LibraryNodeTypes, LibraryNode> _filteredView;

        /// <summary>
        /// The visual studio tree flags used for this node.
        /// </summary>
        private _VSTREEFLAGS _flags;

        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private string _name;

        /// <summary>
        /// The private field used for the <see cref="ToolTipText"/> property.
        /// </summary>
        private string _toolTipText;

        /// <summary>
        /// The private field used for the <see cref="NodeType"/> property.
        /// </summary>
        private LibraryNodeTypes _nodeType;

        /// <summary>
        /// The private counter tracking the number of times this node has been updated.
        /// </summary>
        private uint _updateCount;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="LibraryNode"/> class with the
        /// specified name.
        /// </summary>
        /// <param name="name">
        /// The name of this library node.
        /// </param>
        public LibraryNode(string name)
        {
            this._capabilities = LibraryNodeCapabilities.None;
            this._contextMenuID = null;
            this._name = name;
            this._toolTipText = name;
            this._nodeType = LibraryNodeTypes.None;
            this._children = new List<LibraryNode>();
            this._clipboardFormats = new List<VSOBJCLIPFORMAT>();
            this._filteredView = new Dictionary<LibraryNodeTypes, LibraryNode>();
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="LibraryNode"/> class with the
        /// specified name and type.
        /// </summary>
        /// <param name="name">
        /// The name of this library node.
        /// </param>
        /// <param name="type">
        /// The type that this library node represents.
        /// </param>
        public LibraryNode(string name, LibraryNodeTypes type)
        {
            this._capabilities = LibraryNodeCapabilities.None;
            this._contextMenuID = null;
            this._name = name;
            this._toolTipText = name;
            this._nodeType = type;
            this._children = new List<LibraryNode>();
            this._clipboardFormats = new List<VSOBJCLIPFORMAT>();
            this._filteredView = new Dictionary<LibraryNodeTypes, LibraryNode>();
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="LibraryNode"/> class as a copy of the
        /// specified instance.
        /// </summary>
        /// <param name="node">
        /// The instance to copy.
        /// </param>
        public LibraryNode(LibraryNode node)
        {
            this._capabilities = node._capabilities;
            this._contextMenuID = node._contextMenuID;
            this._displayData = node._displayData;
            this._name = node._name;
            this._toolTipText = node._toolTipText;
            this._nodeType = node._nodeType;
            this._children = new List<LibraryNode>();
            foreach (LibraryNode child in node._children)
            {
                this._children.Add(child);
            }

            this._clipboardFormats = new List<VSOBJCLIPFORMAT>();
            foreach (VSOBJCLIPFORMAT format in node._clipboardFormats)
            {
                this._clipboardFormats.Add(format);
            }

            this._filteredView = new Dictionary<LibraryNodeTypes, LibraryNode>();
            this._updateCount = node._updateCount;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether the node can be deleted.
        /// </summary>
        public bool CanDelete
        {
            get { return 0 != (this._capabilities & LibraryNodeCapabilities.AllowDelete); }
            set { this.SetCapabilityFlag(LibraryNodeCapabilities.AllowDelete, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this node can take the user to the source.
        /// </summary>
        public bool CanGoToSource
        {
            get
            {
                return 0 != (this._capabilities & LibraryNodeCapabilities.HasSourceContext);
            }

            set
            {
                this.SetCapabilityFlag(LibraryNodeCapabilities.HasSourceContext, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this node can be renamed.
        /// </summary>
        public bool CanRename
        {
            get { return 0 != (this._capabilities & LibraryNodeCapabilities.AllowRename); }
            set { this.SetCapabilityFlag(LibraryNodeCapabilities.AllowRename, value); }
        }

        /// <summary>
        /// Gets or sets the capabilities of this node.
        /// </summary>
        public LibraryNodeCapabilities Capabilities
        {
            get { return this._capabilities; }
            set { this._capabilities = value; }
        }

        /// <summary>
        /// Gets or sets the tree flags that this node identifies.
        /// </summary>
        public _VSTREEFLAGS Flags
        {
            get { return this._flags; }
            set { this._flags = value; }
        }

        /// <summary>
        /// Gets or sets the name of this node.
        /// </summary>
        public string Name
        {
            get { return this._name; }
            set { this._name = value; }
        }

        /// <summary>
        /// Gets or sets the type of node this is.
        /// </summary>
        public LibraryNodeTypes NodeType
        {
            get { return this._nodeType; }
            set { this._nodeType = value; }
        }

        /// <summary>
        /// Gets or sets the tool tip text for this node.
        /// </summary>
        public string ToolTipText
        {
            get { return this._toolTipText; }
            set { this._toolTipText = value; }
        }

        /// <summary>
        /// Gets the unique name for this node.
        /// </summary>
        public virtual string UniqueName
        {
            get { return this.Name; }
        }

        /// <summary>
        /// Gets the browser object for this node.
        /// </summary>
        protected virtual object BrowseObject
        {
            get { return null; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Retrieves the name of the node.
        /// </summary>
        /// <param name="name">
        /// When this method return contains the name of the node.
        /// </param>
        /// <returns>
        /// Always returns VSConstants.S_OK.
        /// </returns>
        int IVsNavInfoNode.get_Name(out string name)
        {
            name = this.UniqueName;
            return VSConstants.S_OK;
        }

        /// <summary>
        /// Retrieves the type of the node.
        /// </summary>
        /// <param name="type">
        /// When this method returns contains the type for this node.
        /// </param>
        /// <returns>
        /// Always returns VSConstants.S_OK.
        /// </returns>
        int IVsNavInfoNode.get_Type(out uint type)
        {
            type = (uint)this._nodeType;
            return VSConstants.S_OK;
        }

        /// <summary>
        /// Determines whether the item at the specified index can be deleted.
        /// </summary>
        /// <param name="index">
        /// Specifies the index of the list item of interest.
        /// </param>
        /// <param name="result">
        /// When this method returns contains a flag indicating if the given list item can be
        /// deleted.
        /// </param>
        /// <returns>
        /// If the method succeeds, it returns Microsoft.VisualStudio.VSConstants.S_OK;
        /// otherwise, a error code.
        /// </returns>
        int IVsSimpleObjectList2.CanDelete(uint index, out int result)
        {
            if (index >= (uint)this._children.Count)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            result = this._children[(int)index].CanDelete ? 1 : 0;
            return VSConstants.S_OK;
        }

        /// <summary>
        /// Determines whether navigation to the given list item's source is supported.
        /// </summary>
        /// <param name="index">
        /// The index of the list item of interest.
        /// </param>
        /// <param name="type">
        /// Specifies the source type.
        /// </param>
        /// <param name="result">
        /// When this method returns contains a flag indicating whether navigation is
        /// supported.
        /// </param>
        /// <returns>
        /// If the method succeeds, it returns Microsoft.VisualStudio.VSConstants.S_OK;
        /// otherwise, a error code.
        /// </returns>
        int IVsSimpleObjectList2.CanGoToSource(
            uint index, VSOBJGOTOSRCTYPE type, out int result)
        {
            if (index >= (uint)this._children.Count)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            result = this._children[(int)index].CanGoToSource ? 1 : 0;
            return VSConstants.S_OK;
        }

        /// <summary>
        /// Determines whether the item at the specified index can be renamed.
        /// </summary>
        /// <param name="index">
        /// Specifies the index of the list item of interest.
        /// </param>
        /// <param name="name">
        /// The string containing the new name.
        /// </param>
        /// <param name="result">
        /// When this method returns contains a flag indicating if the given list item can be
        /// renamed.
        /// </param>
        /// <returns>
        /// If the method succeeds, it returns Microsoft.VisualStudio.VSConstants.S_OK;
        /// otherwise, a error code.
        /// </returns>
        int IVsSimpleObjectList2.CanRename(uint index, string name, out int result)
        {
            if (index >= (uint)this._children.Count)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            result = this._children[(int)index].CanRename ? 1 : 0;
            return VSConstants.S_OK;
        }

        /// <summary>
        /// Gets the hierarchy interface and the number of source items for the given list
        /// item.
        /// </summary>
        /// <param name="index">
        /// Specifies the index of the list item of interest.
        /// </param>
        /// <param name="hierarchy">
        /// When this method returns contains the hierarchy the source items are contained
        /// within.
        /// </param>
        /// <param name="id">
        /// Specifies the id of an item within the hierarchy.
        /// </param>
        /// <param name="result">
        /// When this method returns contains a flag indicating if the given list item can be
        /// deleted.
        /// </param>
        /// <returns>
        /// If the method succeeds, it returns Microsoft.VisualStudio.VSConstants.S_OK;
        /// otherwise, a error code.
        /// </returns>
        int IVsSimpleObjectList2.CountSourceItems(
            uint index, out IVsHierarchy hierarchy, out uint id, out uint result)
        {
            if (index >= (uint)this._children.Count)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            this._children[(int)index].SourceItems(out hierarchy, out id, out result);
            return VSConstants.S_OK;
        }

        /// <summary>
        /// Asks the given list item to do the delete operation.
        /// </summary>
        /// <param name="index">
        /// Specifies the index of the list item to delete.
        /// </param>
        /// <param name="flags">
        /// The visual studio object operation flags for this operation.
        /// </param>
        /// <returns>
        /// If the method succeeds, it returns Microsoft.VisualStudio.VSConstants.S_OK;
        /// otherwise, a error code.
        /// </returns>
        int IVsSimpleObjectList2.DoDelete(uint index, uint flags)
        {
            if (index >= (uint)this._children.Count)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            this._children[(int)index].Delete();
            this._children.RemoveAt((int)index);
            return VSConstants.S_OK;
        }

        /// <summary>
        /// Asks the given list item to handle a drag-and-drop operation.
        /// </summary>
        /// <param name="index">
        /// Specifies the index of the list item of interest.
        /// </param>
        /// <param name="data">
        /// The data being dropped.
        /// </param>
        /// <param name="state">
        ///  Current state of the keyboard and the mouse modifier keys.
        /// </param>
        /// <param name="effect">
        /// On input, the effect being requested. On output, the effect that your object list
        /// allows.
        /// </param>
        /// <returns>
        /// If the method succeeds, it returns Microsoft.VisualStudio.VSConstants.S_OK;
        /// otherwise, a error code.
        /// </returns>
        int IVsSimpleObjectList2.DoDragDrop(
            uint index, IDataObject data, uint state, ref uint effect)
        {
            if (index >= (uint)this._children.Count)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            OleDataObject dataObject = new OleDataObject(data);
            this._children[(int)index].DoDragDrop(dataObject, state, effect);
            return VSConstants.S_OK;
        }

        /// <summary>
        /// Asks the given list item to do the rename operation.
        /// </summary>
        /// <param name="index">
        /// Specifies the index of the list item of interest.
        /// </param>
        /// <param name="name">
        /// The string containing the new name.
        /// </param>
        /// <param name="flags">
        /// Flag indicating that Index is part of a multi-select.
        /// </param>
        /// <returns>
        /// If the method succeeds, it returns Microsoft.VisualStudio.VSConstants.S_OK;
        /// otherwise, a error code.
        /// </returns>
        int IVsSimpleObjectList2.DoRename(uint index, string name, uint flags)
        {
            if (index >= (uint)this._children.Count)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            this._children[(int)index].Rename(name, flags);
            return VSConstants.S_OK;
        }

        /// <summary>
        /// Asks the given list item to enumerate its supported clipboard formats.
        /// </summary>
        /// <param name="index">
        /// Specifies the index of the list item of interest.
        /// </param>
        /// <param name="flags">
        /// Specifies multi-selection.
        /// </param>
        /// <param name="celt">
        /// Specifies the element count of formats.
        /// </param>
        /// <param name="formats">
        /// Specifies an array of VSOBJCLIPFORMAT structures defining the formats supported.
        /// </param>
        /// <param name="actual">
        /// The count of formats in the formats array actually returned.
        /// </param>
        /// <returns>
        /// If the method succeeds, it returns Microsoft.VisualStudio.VSConstants.S_OK;
        /// otherwise, a error code.
        /// </returns>
        int IVsSimpleObjectList2.EnumClipboardFormats(
            uint index, uint flags, uint celt, VSOBJCLIPFORMAT[] formats, uint[] actual)
        {
            if (index >= (uint)this._children.Count)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            _VSOBJCFFLAGS objFlags = (_VSOBJCFFLAGS)flags;
            uint copied = this._children[(int)index].EnumClipboardFormats(objFlags, formats);
            if ((null != actual) && (actual.Length > 0))
            {
                actual[0] = copied;
            }

            return VSConstants.S_OK;
        }

        /// <summary>
        /// Asks the list item to provide description text to be used in the object browser.
        /// </summary>
        /// <param name="index">
        /// Specifies the index of the list item of interest.
        /// </param>
        /// <param name="options">
        /// Specifies description options.
        /// </param>
        /// <param name="description">
        /// Specifies the description interface.
        /// </param>
        /// <returns>
        /// If the method succeeds, it returns Microsoft.VisualStudio.VSConstants.S_OK;
        /// otherwise, a error code.
        /// </returns>
        int IVsSimpleObjectList2.FillDescription2(
            uint index, uint options, IVsObjectBrowserDescription3 description)
        {
            if (index >= (uint)this._children.Count)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            _VSOBJDESCOPTIONS descriptionOptions = (_VSOBJDESCOPTIONS)options;
            this._children[(int)index].FillDescription(descriptionOptions, description);
            return VSConstants.S_OK;
        }

        /// <summary>
        /// Retrieves the property browse IDispatch for the given list item.
        /// </summary>
        /// <param name="index">
        /// Specifies the index of the list item of interest.
        /// </param>
        /// <param name="browseObject">
        /// When this method returns contains the property browse IDispatch object for the
        /// given list item.
        /// </param>
        /// <returns>
        /// If the method succeeds, it returns Microsoft.VisualStudio.VSConstants.S_OK;
        /// otherwise, a error code.
        /// </returns>
        int IVsSimpleObjectList2.GetBrowseObject(uint index, out object browseObject)
        {
            if (index >= (uint)this._children.Count)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            browseObject = this._children[(int)index].BrowseObject;
            if (null == browseObject)
            {
                return VSConstants.E_NOTIMPL;
            }

            return VSConstants.S_OK;
        }

        /// <summary>
        /// Retrieves an object list's capabilities.
        /// </summary>
        /// <param name="capabilities">
        /// When this method returns contains the object list's capabilities.
        /// </param>
        /// <returns>
        /// If the method succeeds, it returns Microsoft.VisualStudio.VSConstants.S_OK;
        /// otherwise, a error code.
        /// </returns>
        int IVsSimpleObjectList2.GetCapabilities2(out uint capabilities)
        {
            capabilities = (uint)this.Capabilities;
            return VSConstants.S_OK;
        }

        /// <summary>
        /// Retrieves the value for the specified category for the given list item.
        /// </summary>
        /// <param name="index">
        /// Specifies the index of the list item of interest.
        /// </param>
        /// <param name="category">
        /// Specifies the category of interest.
        /// </param>
        /// <param name="field">
        /// When this method returns contains the value for the specified category for the
        /// given list item.
        /// </param>
        /// <returns>
        /// If the method succeeds, it returns Microsoft.VisualStudio.VSConstants.S_OK;
        /// otherwise, a error code.
        /// </returns>
        int IVsSimpleObjectList2.GetCategoryField2(uint index, int category, out uint field)
        {
            LibraryNode node;
            if ((uint)0xFFFFFFFF == index)
            {
                node = this;
            }
            else if (index < (uint)this._children.Count)
            {
                node = this._children[(int)index];
            }
            else
            {
                throw new ArgumentOutOfRangeException("index");
            }

            field = node.CategoryField((LIB_CATEGORY)category);
            return VSConstants.S_OK;
        }

        /// <summary>
        /// Asks the given list item to renders a specific clipboard format that it supports.
        /// </summary>
        /// <param name="index">
        /// Specifies the index of the list item of interest.
        /// </param>
        /// <param name="flags">
        /// Specifies multi-selection flags.
        /// </param>
        /// <param name="format">
        /// The clipboard format information.
        /// </param>
        /// <param name="medium">
        /// A object indicating the data transfer medium for each format.
        /// </param>
        /// <returns>
        /// Always VSConstants.E_NOTIMPL.
        /// </returns>
        int IVsSimpleObjectList2.GetClipboardFormat(
            uint index, uint flags, FORMATETC[] format, STGMEDIUM[] medium)
        {
            return VSConstants.E_NOTIMPL;
        }

        /// <summary>
        /// Allows the list to provide a different context menu and IOleCommandTarget for the
        /// given list item.
        /// </summary>
        /// <param name="index">
        ///  Specifies the index of the list item of interest.
        /// </param>
        /// <param name="active">
        /// When this method returns contains the CLSID of the menu group containing the menu.
        /// </param>
        /// <param name="menuId">
        /// When this method returns contains the menu id.
        /// </param>
        /// <param name="target">
        /// When this method returns specifies the command target interface.
        /// </param>
        /// <returns>
        /// If the method succeeds, it returns Microsoft.VisualStudio.VSConstants.S_OK;
        /// otherwise, a error code.
        /// </returns>
        int IVsSimpleObjectList2.GetContextMenu(
            uint index, out Guid active, out int menuId, out IOleCommandTarget target)
        {
            if (index >= (uint)this._children.Count)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            CommandID commandId = this._children[(int)index]._contextMenuID;
            if (null == commandId)
            {
                active = Guid.Empty;
                menuId = 0;
                target = null;
                return VSConstants.E_NOTIMPL;
            }

            active = commandId.Guid;
            menuId = commandId.ID;
            target = this._children[(int)index] as IOleCommandTarget;
            return VSConstants.S_OK;
        }

        /// <summary>
        /// Retrieves data to draw the requested tree list item.
        /// </summary>
        /// <param name="index">
        /// Specifies the index of the node of interest.
        /// </param>
        /// <param name="data">
        /// When this method returns contains the display data to use for this node.
        /// </param>
        /// <returns>
        /// If the method succeeds, it returns Microsoft.VisualStudio.VSConstants.S_OK;
        /// otherwise, a error code.
        /// </returns>
        int IVsSimpleObjectList2.GetDisplayData(uint index, VSTREEDISPLAYDATA[] data)
        {
            if (index >= (uint)this._children.Count)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            data[0] = this._children[(int)index]._displayData;
            return VSConstants.S_OK;
        }

        /// <summary>
        /// Retrieves a flag indicating the expandability of the given list item.
        /// </summary>
        /// <param name="index">
        /// Specifies the index of the list item of interest.
        /// </param>
        /// <param name="excluded">
        /// Specifies the list types to be excluded.
        /// </param>
        /// <param name="expandable">
        /// When this method return contains a flag indicating expandability.
        /// </param>
        /// <returns>
        /// Always returns VSConstants.E_NOTIMPL.
        /// </returns>
        int IVsSimpleObjectList2.GetExpandable3(uint index, uint excluded, out int expandable)
        {
            expandable = 1;
            return VSConstants.E_NOTIMPL;
        }

        /// <summary>
        /// Asks the given list item to renders a specific clipboard format as a variant.
        /// </summary>
        /// <param name="index">
        /// Specifies the index of the list item of interest.
        /// </param>
        /// <param name="flags">
        /// Specifies the multi-selection flags.
        /// </param>
        /// <param name="clipFormat">
        /// Defines the format requested.
        /// </param>
        /// <param name="format">
        /// When this method returns contains the variant where to render the data.
        /// </param>
        /// <returns>
        /// Always returns VSConstants.E_NOTIMPL.
        /// </returns>
        int IVsSimpleObjectList2.GetExtendedClipboardVariant(
            uint index, uint flags, VSOBJCLIPFORMAT[] clipFormat, out object format)
        {
            format = null;
            return VSConstants.E_NOTIMPL;
        }

        /// <summary>
        /// Retrieves the attributes of the current tree list.
        /// </summary>
        /// <param name="flags">
        /// When this method returns contains a variable indicating attributes of the current
        /// tree list.
        /// </param>
        /// <returns>
        /// Always returns VSConstants.S_OK.
        /// </returns>
        int IVsSimpleObjectList2.GetFlags(out uint flags)
        {
            flags = (uint)this.Flags;
            return VSConstants.S_OK;
        }

        /// <summary>
        /// Retrieves the number of items in this node.
        /// </summary>
        /// <param name="count">
        /// When this method returns is equal to the number of items in this node.
        /// </param>
        /// <returns>
        /// Always returns VSConstants.S_OK.
        /// </returns>
        int IVsSimpleObjectList2.GetItemCount(out uint count)
        {
            count = (uint)this._children.Count;
            return VSConstants.S_OK;
        }

        /// <summary>
        /// Retrieves a child list for the specified category.
        /// </summary>
        /// <param name="index">
        /// Specifies the index of the list item of interest.
        /// </param>
        /// <param name="type">
        /// Specifies the type of list being requested.
        /// </param>
        /// <param name="flags">
        /// Specifies the flags that control the request for object list information.
        /// </param>
        /// <param name="search">
        /// Should be ignored.
        /// </param>
        /// <param name="list">
        /// When this method returns contains the requested list.
        /// </param>
        /// <returns>
        /// If the method succeeds, it returns Microsoft.VisualStudio.VSConstants.S_OK;
        /// otherwise, a error code.
        /// </returns>
        int IVsSimpleObjectList2.GetList2(
            uint index,
            uint type,
            uint flags,
            VSOBSEARCHCRITERIA2[] search,
            out IVsSimpleObjectList2 list)
        {
            // TODO: Use the flags and list type to actually filter the result.
            if (index >= (uint)this._children.Count)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            list = this._children[(int)index].FilterView((LibraryNodeTypes)type);
            return VSConstants.S_OK;
        }

        /// <summary>
        /// Retrieves the hierarchy and item identifier for each selected item.
        /// </summary>
        /// <param name="index">
        /// Specifies the index of the list item of interest.
        /// </param>
        /// <param name="gsi">
        /// A flag providing information about how the selected items should be returned.
        /// </param>
        /// <param name="itemCount">
        ///  The number of items returned in selection.
        /// </param>
        /// <param name="selection">
        /// When this method returns contains the selected items.
        /// </param>
        /// <returns>
        /// Always returns VSConstants.E_NOTIMPL.
        /// </returns>
        int IVsSimpleObjectList2.GetMultipleSourceItems(
            uint index, uint gsi, uint itemCount, VSITEMSELECTION[] selection)
        {
            return VSConstants.E_NOTIMPL;
        }

        /// <summary>
        /// Reserved for future use.
        /// </summary>
        /// <param name="index">
        /// The index of the item for which to get navigation information.
        /// </param>
        /// <param name="navInformation">
        /// When this method return contains the requested navigation information for this
        /// specified list item.
        /// </param>
        /// <returns>
        /// Always returns VSConstants.E_NOTIMPL.
        /// </returns>
        int IVsSimpleObjectList2.GetNavInfo(uint index, out IVsNavInfo navInformation)
        {
            navInformation = null;
            return VSConstants.E_NOTIMPL;
        }

        /// <summary>
        /// Reserved for future use.
        /// </summary>
        /// <param name="index">
        /// The index of the item for which to get navigation information.
        /// </param>
        /// <param name="navNode">
        /// When this method returns contains the requested navigation information.
        /// </param>
        /// <returns>
        /// Always returns VSConstants.S_OK.
        /// </returns>
        int IVsSimpleObjectList2.GetNavInfoNode(uint index, out IVsNavInfoNode navNode)
        {
            if (index >= (uint)this._children.Count)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            navNode = this._children[(int)index] as IVsNavInfoNode;
            return VSConstants.S_OK;
        }

        /// <summary>
        /// Retrieves the specified property for the specified list item.
        /// </summary>
        /// <param name="index">
        /// The index of the item for which to get a property.
        /// </param>
        /// <param name="id">
        /// The id for the property to retrieve.
        /// </param>
        /// <param name="variable">
        /// When this method returns contains a variant object containing the requested value.
        /// </param>
        /// <returns>
        /// Always returns VSConstants.E_NOTIMPL.
        /// </returns>
        int IVsSimpleObjectList2.GetProperty(uint index, int id, out object variable)
        {
            variable = null;
            return VSConstants.E_NOTIMPL;
        }

        /// <summary>
        /// Retrieves a source filename and line number for the given list item.
        /// </summary>
        /// <param name="index">
        /// Specifies the index of the list item of interest.
        /// </param>
        /// <param name="filename">
        /// When this method returns contains the file name associated with the list item.
        /// </param>
        /// <param name="lineNumber">
        /// When this method returns contains the line number associated with the list item.
        /// </param>
        /// <returns>
        /// Always returns VSConstants.E_NOTIMPL.
        /// </returns>
        int IVsSimpleObjectList2.GetSourceContextWithOwnership(
            uint index, out string filename, out uint lineNumber)
        {
            filename = null;
            lineNumber = (uint)0;
            return VSConstants.E_NOTIMPL;
        }

        /// <summary>
        /// Retrieves the text representations for the requested tree list item.
        /// </summary>
        /// <param name="index">
        /// Specifies the index of the list item of interest.
        /// </param>
        /// <param name="options">
        /// Specifies the text type being requested.
        /// </param>
        /// <param name="text">
        /// When this method returns contains the text for the specified tree list item.
        /// </param>
        /// <returns>
        /// If the method succeeds, it returns Microsoft.VisualStudio.VSConstants.S_OK;
        /// otherwise, a error code.
        /// </returns>
        int IVsSimpleObjectList2.GetTextWithOwnership(
            uint index, VSTREETEXTOPTIONS options, out string text)
        {
            if (index >= (uint)this._children.Count)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            text = this._children[(int)index]._name;
            return VSConstants.S_OK;
        }

        /// <summary>
        /// Retrieves the tool tip text for the requested tree list item.
        /// </summary>
        /// <param name="index">
        /// Specifies the index of the list item of interest.
        /// </param>
        /// <param name="type">
        /// Specifies the type of tool tip text.
        /// </param>
        /// <param name="text">
        /// When this method returns contains the tree list item's tool tip text.
        /// </param>
        /// <returns>
        /// If the method succeeds, it returns Microsoft.VisualStudio.VSConstants.S_OK;
        /// otherwise, a error code.
        /// </returns>
        int IVsSimpleObjectList2.GetTipTextWithOwnership(
            uint index, VSTREETOOLTIPTYPE type, out string text)
        {
            if (index >= (uint)this._children.Count)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            text = this._children[(int)index].ToolTipText;
            return VSConstants.S_OK;
        }

        /// <summary>
        /// Retrieves the user context object for the given list item.
        /// </summary>
        /// <param name="index">
        /// Specifies the index of the list item of interest.
        /// </param>
        /// <param name="context">
        /// When this method returns contains a context bag returned as a IUnknown interface.
        /// </param>
        /// <returns>
        /// Always returns VSConstants.E_NOTIMPL.
        /// </returns>
        int IVsSimpleObjectList2.GetUserContext(uint index, out object context)
        {
            context = null;
            return VSConstants.E_NOTIMPL;
        }

        /// <summary>
        /// Navigates to the source for the given list item.
        /// </summary>
        /// <param name="index">
        /// Specifies the index of the list item of interest.
        /// </param>
        /// <param name="type">
        /// Specifies the source type.
        /// </param>
        /// <returns>
        /// If the method succeeds, it returns Microsoft.VisualStudio.VSConstants.S_OK;
        /// otherwise, a error code.
        /// </returns>
        int IVsSimpleObjectList2.GoToSource(uint index, VSOBJGOTOSRCTYPE type)
        {
            if (index >= (uint)this._children.Count)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            this._children[(int)index].GotoSource(type);
            return VSConstants.S_OK;
        }

        /// <summary>
        /// Reserved for future use.
        /// </summary>
        /// <param name="navigationNode">
        /// A object representing the navigation information for a node.
        /// </param>
        /// <param name="pulIndex">
        /// When this method returns contains the index of the list item that corresponds to
        /// the specified navigation node.
        /// </param>
        /// <returns>
        /// If the method succeeds, it returns Microsoft.VisualStudio.VSConstants.S_OK;
        /// otherwise, a error code.
        /// </returns>
        int IVsSimpleObjectList2.LocateNavInfoNode(
            IVsNavInfoNode navigationNode, out uint pulIndex)
        {
            if (null == navigationNode)
            {
                throw new ArgumentNullException("navigationNode");
            }

            pulIndex = (uint)0xFFFFFFFF;
            string name;
            ErrorHandler.ThrowOnFailure(navigationNode.get_Name(out name));
            for (int i = 0; i < this._children.Count; i++)
            {
                string uniqueName = this._children[i].UniqueName;
                if (0 == string.Compare(uniqueName, name, StringComparison.OrdinalIgnoreCase))
                {
                    pulIndex = (uint)i;
                    return VSConstants.S_OK;
                }
            }

            return VSConstants.S_FALSE;
        }

        /// <summary>
        /// Notifies the current tree list that it is being closed.
        /// </summary>
        /// <param name="actions">
        /// Specifies to the tree view the action to take when closing this tree list.
        /// </param>
        /// <returns>
        /// Always returns VSConstants.S_OK.
        /// </returns>
        int IVsSimpleObjectList2.OnClose(VSTREECLOSEACTIONS[] actions)
        {
            return VSConstants.S_OK;
        }

        /// <summary>
        /// Determines whether the given list item supports a drag-and-drop operation.
        /// </summary>
        /// <param name="index">
        /// Specifies the index of the list item of interest.
        /// </param>
        /// <param name="data">
        ///  Pointer to an IDataObject being dropped.
        /// </param>
        /// <param name="state">
        /// Current state of the keyboard and the mouse modifier keys.
        /// </param>
        /// <param name="effect">
        /// On input, the effect being requested. On output, the effect that your object list
        /// allows.
        /// </param>
        /// <returns>
        /// Always returns VSConstants.E_NOTIMPL.
        /// </returns>
        int IVsSimpleObjectList2.QueryDragDrop(
            uint index, IDataObject data, uint state, ref uint effect)
        {
            return VSConstants.E_NOTIMPL;
        }

        /// <summary>
        /// Allows the list to display help for the given list item.
        /// </summary>
        /// <param name="index">
        /// Specifies the index of the list item of interest.
        /// </param>
        /// <returns>
        /// Always returns VSConstants.E_NOTIMPL.
        /// </returns>
        int IVsSimpleObjectList2.ShowHelp(uint index)
        {
            return VSConstants.E_NOTIMPL;
        }

        /// <summary>
        /// Retrieves the current change counter for the tree list, and is used to indicate
        /// that the list contents have changed.
        /// </summary>
        /// <param name="count">
        /// When this method returns contains the current number of updates.
        /// </param>
        /// <returns>
        /// Always returns VSConstants.S_OK.
        /// </returns>
        int IVsSimpleObjectList2.UpdateCounter(out uint count)
        {
            count = this._updateCount;
            return VSConstants.S_OK;
        }

        /// <summary>
        /// Adds the specified node to this nodes children.
        /// </summary>
        /// <param name="node">
        /// The node to add.
        /// </param>
        internal void AddNode(LibraryNode node)
        {
            lock (this._children)
            {
                this._children.Add(node);
            }

            this._updateCount += 1;
        }

        /// <summary>
        /// Removes the first occurrence of the specified node from this nodes children.
        /// </summary>
        /// <param name="node">
        /// The node to remove.
        /// </param>
        internal void RemoveNode(LibraryNode node)
        {
            lock (this._children)
            {
                this._children.Remove(node);
            }

            this._updateCount += 1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="category">
        /// 
        /// </param>
        /// <returns>
        /// 
        /// </returns>
        protected virtual uint CategoryField(LIB_CATEGORY category)
        {
            uint fieldValue = 0;
            switch (category)
            {
                case LIB_CATEGORY.LC_LISTTYPE:
                    {
                        LibraryNodeTypes subTypes = LibraryNodeTypes.None;
                        foreach (LibraryNode node in _children)
                        {
                            subTypes |= node._nodeType;
                        }

                        fieldValue = (uint)subTypes;
                    }

                    break;
                case (LIB_CATEGORY)_LIB_CATEGORY2.LC_HIERARCHYTYPE:
                    fieldValue = (uint)_LIBCAT_HIERARCHYTYPE.LCHT_UNKNOWN;
                    break;
                default:
                    throw new NotImplementedException();
            }

            return fieldValue;
        }

        /// <summary>
        /// Creates a new instance that is a copy of this one.
        /// </summary>
        /// <returns>
        /// A new instance that is a copy of this one.
        /// </returns>
        protected virtual LibraryNode Clone()
        {
            return new LibraryNode(this);
        }

        /// <summary>
        /// Performs the operations needed to delete this node.
        /// </summary>
        protected virtual void Delete()
        {
        }

        /// <summary>
        /// Performs a Drag and Drop operation on this node.
        /// </summary>
        protected virtual void DoDragDrop(OleDataObject dataObject, uint keyState, uint effect)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="flagsArg">
        /// 
        /// </param>
        /// <param name="formats">
        /// 
        /// </param>
        /// <returns>
        /// 
        /// </returns>
        protected virtual uint EnumClipboardFormats(
            _VSOBJCFFLAGS flagsArg, VSOBJCLIPFORMAT[] formats)
        {
            if ((null == formats) || (formats.Length == 0))
            {
                return (uint)_clipboardFormats.Count;
            }

            uint itemsToCopy = (uint)_clipboardFormats.Count;
            if (itemsToCopy > (uint)formats.Length)
            {
                itemsToCopy = (uint)formats.Length;
            }

            Array.Copy(_clipboardFormats.ToArray(), formats, (int)itemsToCopy);
            return itemsToCopy;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="flagsArg">
        /// 
        /// </param>
        /// <param name="description">
        /// 
        /// </param>
        protected virtual void FillDescription(
            _VSOBJDESCOPTIONS flagsArg, IVsObjectBrowserDescription3 description)
        {
            description.ClearDescriptionText();
            description.AddDescriptionText3("SanScript Source File ", VSOBDESCRIPTIONSECTION.OBDS_MISC, null);
            description.AddDescriptionText3(this._name + "\n\n", VSOBDESCRIPTIONSECTION.OBDS_NAME, null);
            description.AddDescriptionText3("Description:\n", VSOBDESCRIPTIONSECTION.OBDS_NAME, null);
            description.AddDescriptionText3("", VSOBDESCRIPTIONSECTION.OBDS_MISC, null);
        }

        /// <summary>
        /// Filter the view based on the specified node type.
        /// </summary>
        /// <param name="filterType">
        /// The node type to filter on.
        /// </param>
        /// <returns>
        /// A new instance of a object list that contains the nodes that have been filtered.
        /// </returns>
        protected IVsSimpleObjectList2 FilterView(LibraryNodeTypes filterType)
        {
            LibraryNode filtered = null;
            if (_filteredView.TryGetValue(filterType, out filtered))
            {
                return filtered as IVsSimpleObjectList2;
            }

            filtered = this.Clone();
            for (int i = 0; i < filtered._children.Count; )
            {
                if (0 == (filtered._children[i]._nodeType & filterType))
                {
                    filtered._children.RemoveAt(i);
                }
                else
                {
                    i += 1;
                }
            }

            _filteredView.Add(filterType, filtered);
            return filtered as IVsSimpleObjectList2;
        }

        /// <summary>
        /// Navigates to the source for the given type.
        /// </summary>
        /// <param name="type">
        /// Specifies the source type.
        /// </param>
        protected virtual void GotoSource(VSOBJGOTOSRCTYPE type)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newName">
        /// The new name for this node.
        /// </param>
        /// <param name="flags">
        /// 
        /// </param>
        protected virtual void Rename(string newName, uint flags)
        {
            this._name = newName;
        }

        /// <summary>
        /// Sets the specified flag on the capabilities bitset to the specified value.
        /// </summary>
        /// <param name="flag">
        /// The flag to set in the capabilities bitset.
        /// </param>
        /// <param name="value">
        /// The value to set the specified flag.
        /// </param>
        protected void SetCapabilityFlag(LibraryNodeCapabilities flag, bool value)
        {
            if (value)
            {
                _capabilities |= flag;
            }
            else
            {
                _capabilities &= ~flag;
            }
        }

        /// <summary>
        /// Finds the source files associated with this node.
        /// </summary>
        /// <param name="hierarchy">
        /// The hierarchy containing the items.
        /// </param>
        /// <param name="itemId">
        /// The item id of the item.
        /// </param>
        /// <param name="itemsCount">
        /// Number of items.
        /// </param>
        protected virtual void SourceItems(
            out IVsHierarchy hierarchy, out uint itemId, out uint itemsCount)
        {
            hierarchy = null;
            itemId = 0;
            itemsCount = 0;
        }
        #endregion Methods
    } // SanScript.Project.Library.LibraryNode {Class}
} // SanScript.Project.Library {Namespace}
