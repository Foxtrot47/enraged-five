﻿namespace SanScript.Project
{
    using System;

    /// <summary>
    /// 
    /// </summary>
    internal static class GuidList
    {
        public const string guidSanScriptProjectPkgString =
            "917904A4-941D-451A-B1A6-E8CD8679AECD";
        public const string guidSanScriptProjectCmdSetString =
            "1D16ACB5-3C1E-4A02-BC24-B1E2E3E39E25";
        public const string guidSanScriptProjectFactoryString =
            "61EA5E1A-02CA-4CEC-AA0C-B7544E811F38";
        public const string guidLibraryManagerServiceGuidString =
            "B4D46D35-DE84-4F16-B693-D043D4874D0E";
        public const string guidLibraryManagerGuidString =
            "7C0A2250-D002-4A94-B30E-688D53C8E339";
        public const string guidSanScriptLanguageString =
            "27269118-7CEE-46C1-8D8F-8D2397FF4AD5";
        

        public static readonly Guid guidSanScriptProjectCmdSet =
            new Guid(guidSanScriptProjectCmdSetString);
    } // SanScript.Project.GuidList {Class}
} // SanScript.Project {Namespace}