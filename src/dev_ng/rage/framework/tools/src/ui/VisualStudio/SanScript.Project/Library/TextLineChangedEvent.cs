﻿namespace SanScript.Project.Library
{
    using Microsoft.VisualStudio.TextManager.Interop;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="s">
    /// 
    /// </param>
    /// <param name="changes">
    /// 
    /// </param>
    /// <param name="last">
    /// 
    /// </param>
    public delegate void TextLineChangeEvent(object s, TextLineChange[] changes, int last);
} // SanScript.Project.Library {namespace}
