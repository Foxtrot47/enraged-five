﻿namespace SanScript.Project
{
    using System;
    using Microsoft.VisualStudio.TextManager.Interop;

    /// <summary>
    /// Represents the event arguments for a change inside the hierarchy listener.
    /// </summary>
    internal class HierarchyEventArgs : EventArgs
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Id"/> property.
        /// </summary>
        private uint _id;

        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private string _name;

        /// <summary>
        /// The private field used for the <see cref="Buffer"/> property.
        /// </summary>
        private IVsTextLines _buffer;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="HierarchyEventArgs"/> class.
        /// </summary>
        /// <param name="id">
        /// The identifier of the item that has been effected by the event.
        /// </param>
        /// <param name="name">
        /// The name of the item that has been effected by the event.
        /// </param>
        public HierarchyEventArgs(uint id, string name)
        {
            this._id = id;
            this._name = name;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the text buffer that has been effected by the event.
        /// </summary>
        public IVsTextLines Buffer
        {
            get { return this._buffer; }
            set { this._buffer = value; }
        }

        /// <summary>
        /// Gets the identifier of the item that has been effected by the event.
        /// </summary>
        public uint Id
        {
            get { return this._id; }
        }

        /// <summary>
        /// Gets the name of the item that has been effected by the event.
        /// </summary>
        public string Name
        {
            get { return this._name; }
        }
        #endregion Properties
    } // SanScript.Project.HierarchyEventArgs {Class}
} // SanScript.Project {Namespace}
