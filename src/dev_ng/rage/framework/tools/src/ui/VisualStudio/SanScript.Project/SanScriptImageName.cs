﻿namespace SanScript.Project
{
    /// <summary>
    /// Defines the different images available to the node hierarchy with the integer values
    /// representing their offsets into the hierarchy image list.
    /// </summary>
    internal enum SanScriptImageName
    {
        /// <summary>
        /// The image used for the project node.
        /// </summary>
        Project = 0,

        /// <summary>
        /// The image used for the file node with the .sch extension.
        /// </summary>
        Header = 1,

        /// <summary>
        /// The image used for the file node with the .sc extension.
        /// </summary>
        Source = 2,
    } // SanScript.Project.SanScriptImageName {Enum}
} // SanScript.Project {Namespace}
