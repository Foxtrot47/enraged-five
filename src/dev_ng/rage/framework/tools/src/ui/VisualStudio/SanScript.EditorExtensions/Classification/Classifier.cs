﻿namespace SanScript.EditorExtensions.Classification
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.VisualStudio.Text.Classification;
    using Microsoft.VisualStudio.Text;
    using Irony.Parsing;
    using RSG.SanScript;

    /// <summary>
    /// 
    /// </summary>
    internal class Classifier : IClassifier
    {
        #region Fields
        /// <summary>
        /// The Microsoft.VisualStudio.Text.ITextBuffer to classify.
        /// </summary>
        private ITextBuffer _textBuffer;

        /// <summary>
        /// The service that maintains the collection of all known classification types.
        /// </summary>
        private IClassificationTypeRegistryService _classificationRegistryService;

        /// <summary>
        /// The irony parser used to produce the tokens from the text buffer.
        /// </summary>
        private Parser _parser;

        /// <summary>
        /// 
        /// </summary>
        private Dictionary<int, int> _lineStates;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Classifier"/> class.
        /// </summary>
        /// <param name="textBuffer">
        /// The Microsoft.VisualStudio.Text.ITextBuffer to classify.
        /// </param>
        /// <param name="classificationRegistryService">
        /// The service that maintains the collection of all known classification types.
        /// </param>
        internal Classifier(
            ITextBuffer textBuffer,
            IClassificationTypeRegistryService classificationRegistryService)
        {
            this._textBuffer = textBuffer;
            this._classificationRegistryService = classificationRegistryService;
            this._textBuffer.ReadOnlyRegionsChanged += this.TextBufferReadOnlyRegionsChanged;

            this._parser = new Parser(new SanScriptGrammar());
            this._parser.Context.Mode = ParseMode.VsLineScan;

            this._lineStates = new Dictionary<int, int>();
            _lineStates[0] = 0;
            foreach (ITextSnapshotLine line in textBuffer.CurrentSnapshot.Lines)
            {
                int state = 0;
                this._parser.Scanner.VsSetSource(line.GetText(), 0);

                Token dummyToken = this._parser.Scanner.VsReadToken(ref state);
                while (dummyToken != null)
                {
                    dummyToken = this._parser.Scanner.VsReadToken(ref state);
                }

                _lineStates[line.LineNumber + 1] = state;
            }
        }
        #endregion Constructors

        #region Events
        /// <summary>
        ///  Ocurs when the classification of a span of text has changed.
        /// </summary>
        public event EventHandler<ClassificationChangedEventArgs> ClassificationChanged;
        #endregion Events

        #region Methods
        /// <summary>
        /// Gets all the Microsoft.VisualStudio.Text.Classification.ClassificationSpan objects
        /// that overlap the given range of text.
        /// </summary>
        /// <param name="span">
        /// The snapshot span.
        /// </param>
        /// <returns>
        /// A list of Microsoft.VisualStudio.Text.Classification.ClassificationSpan objects
        /// that intersect with the given range.
        /// </returns>
        IList<ClassificationSpan> IClassifier.GetClassificationSpans(SnapshotSpan span)
        {
            List<ClassificationSpan> classifications = new List<ClassificationSpan>();

            ITextSnapshotLine startLine = span.Start.GetContainingLine();
            ITextSnapshotLine endLine = span.End.GetContainingLine();

            int startLineNumber = startLine.LineNumber;
            int endLineNumber = endLine.LineNumber;
            if (endLineNumber == startLineNumber)
            {
                endLineNumber++;
            }

           // Parser parser = new Parser(new SanScriptGrammar());
            // parser.Context.Mode = ParseMode.VsLineScan;
            this._parser.Scanner.VsSetSource(span.GetText(), 0);
            for (int i = startLineNumber; i < endLineNumber; i++)
            {
                ITextSnapshotLine line = span.Snapshot.GetLineFromLineNumber(i);


                int state = 0;
                this._lineStates.TryGetValue(i, out state);

                Token token = this._parser.Scanner.VsReadToken(ref state);
                while (token != null)
                {
                    int start = line.Start.Position + token.Location.Position;
                    int length = Math.Min(token.Length, line.Length);
                    SnapshotSpan location = new SnapshotSpan(span.Snapshot, start, length);
                    classifications.AddRange(this.GetClassificationSpans(location, token));

                    token = this._parser.Scanner.VsReadToken(ref state);
                }

                int oldState = 0;
                _lineStates.TryGetValue(i + 1, out oldState);
                _lineStates[i + 1] = state;

                if (oldState != state)
                {

                }
            }

            return classifications;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="location">
        /// 
        /// </param>
        /// <param name="token">
        /// 
        /// </param>
        /// <returns>
        /// 
        /// </returns>
        private List<ClassificationSpan> GetClassificationSpans(SnapshotSpan location, Token token)
        {
            List<ClassificationSpan> list = new List<ClassificationSpan>();
            string typeString = String.Empty;
            IClassificationType type = null;
            if (token.Category == TokenCategory.Comment)
            {
                if (String.Equals(token.Terminal.Name, "SingleLineComment"))
                {
                    if (token.Text.StartsWith("//"))
                    {
                        typeString = ClassificationTypes.CommentDelimiter;
                        type = this._classificationRegistryService.GetClassificationType(typeString);

                        ITextSnapshot snaphot = location.Snapshot;
                        int start = location.Start;
                        int length = location.Length;
                        location = new SnapshotSpan(snaphot, start, 2);
                        list.Add(new ClassificationSpan(location, type));

                        typeString = ClassificationTypes.CommentDefault;
                        start = Math.Min(snaphot.Length, Math.Max(0, start + 2));
                        length = Math.Max(0, length - 2);
                        if (start + length > snaphot.Length)
                        {
                            length = snaphot.Length - start;
                        }

                        location = new SnapshotSpan(snaphot, start, length);
                    }
                }
                else
                {
                    typeString = ClassificationTypes.CommentDelimiter;
                    type = this._classificationRegistryService.GetClassificationType(typeString);
                    ITextSnapshot snaphot = location.Snapshot;
                    int start = location.Start;
                    int length = location.Length;
                    int commentLength = location.Length;
                    int commentStart = location.Start;

                    if (token.Text.StartsWith("/*"))
                    {
                        location = new SnapshotSpan(snaphot, start, 2);
                        list.Add(new ClassificationSpan(location, type));
                        commentLength -= 2;
                        commentStart += 2;
                    }

                    if (token.Text.EndsWith("*/"))
                    {
                        location = new SnapshotSpan(snaphot, start + length - 2, 2);
                        list.Add(new ClassificationSpan(location, type));
                        commentLength -= 2;
                    }

                    location = new SnapshotSpan(snaphot, commentStart, commentLength);
                }

                typeString = ClassificationTypes.CommentDefault;
                type = this._classificationRegistryService.GetClassificationType(typeString);
                list.Add(new ClassificationSpan(location, type));
                return list;
            }
            else
            {
                StringLiteral stringLiteral = token.Terminal as StringLiteral;
                if (stringLiteral != null)
                {
                    typeString = ClassificationTypes.StringDelimiter;
                    type = this._classificationRegistryService.GetClassificationType(typeString);

                    ITextSnapshot snaphot = location.Snapshot;
                    int start = location.Start;
                    int length = location.Length;
                    location = new SnapshotSpan(snaphot, start, 1);
                    list.Add(new ClassificationSpan(location, type));

                    location = new SnapshotSpan(snaphot, start + length - 1, 1);
                    list.Add(new ClassificationSpan(location, type));

                    location = new SnapshotSpan(snaphot, start + 1, length - 2);
                    typeString = ClassificationTypes.StringDefault;

                    type = this._classificationRegistryService.GetClassificationType(typeString);
                    list.Add(new ClassificationSpan(location, type));

                    return list;
                }
                
                NumberLiteral numberLiteral = token.Terminal as NumberLiteral;
                if (numberLiteral != null)
                {
                    typeString = ClassificationTypes.Number;
                    type = this._classificationRegistryService.GetClassificationType(typeString);
                    list.Add(new ClassificationSpan(location, type));

                    return list;
                }


                if (token.KeyTerm != null)
                {
                    if (token.KeyTerm.Flags.HasFlag(TermFlags.IsPunctuation))
                    {
                        return new List<ClassificationSpan>();
                    }

                    typeString = ClassificationTypes.ReservedWord;
                }
                else
                {
                    typeString = ClassificationTypes.Operator;
                }
            }

            type = this._classificationRegistryService.GetClassificationType(typeString);
            list.Add(new ClassificationSpan(location, type));

            return list;
        }

        /// <summary>
        /// Called whenever the read only regions inside the assoicated text buffer changes.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The Microsoft.VisualStudio.Text.SnapshotSpanEventArgs data for this event.
        /// </param>
        private void TextBufferReadOnlyRegionsChanged(object sender, SnapshotSpanEventArgs e)
        {
            EventHandler<ClassificationChangedEventArgs> handler = this.ClassificationChanged;
            if (handler == null)
            {
                return;
            }

            SnapshotSpan span = new SnapshotSpan(this._textBuffer.CurrentSnapshot, e.Span);
            handler(this, new ClassificationChangedEventArgs(span));
        }
        #endregion Methods
    } // SanScript.EditorExtensions.Classification.Classifier {Class}
} // SanScript.EditorExtensions.Classification {Namespace}