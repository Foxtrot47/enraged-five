﻿namespace SanScript.CompilerTask
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using Microsoft.Build.Framework;
    using Microsoft.Build.Utilities;

    /// <summary>
    /// 
    /// </summary>
    public class CompilerTask : Task
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="CompilerExecutable"/> property.
        /// </summary>
        private string _compilerExecutable;

        /// <summary>
        /// The private field used for the <see cref="SourceFiles"/> property.
        /// </summary>
        private string[] _buildFiles;

        /// <summary>
        /// The private field used for the <see cref="OutputPath"/> property.
        /// </summary>
        private string _outputPath;

        /// <summary>
        /// The private field used for the <see cref="ProjectPath"/> property.
        /// </summary>
        private string _projectPath;

        /// <summary>
        /// The private field used for the <see cref="IncludeFile"/> property.
        /// </summary>
        private string _includeFile;

        /// <summary>
        /// The private field used for the <see cref="IncludeDirectories"/> property.
        /// </summary>
        private string _includeDirectories;

        /// <summary>
        /// The private field used for the <see cref="DebugParser"/> property.
        /// </summary>
        private bool _debugParser;

        /// <summary>
        /// The private field used for the <see cref="DumpThreadState"/> property.
        /// </summary>
        private bool _dumpThreadState;

        /// <summary>
        /// The private field used for the <see cref="DisplayDisassembly"/> property.
        /// </summary>
        private bool _displayDisassembly;

        /// <summary>
        /// The private field used for the <see cref="EnableHsm"/> property.
        /// </summary>
        private bool _enableHsm;

        /// <summary>
        /// The private field used for the <see cref="TreatWarningsAsErrors"/> property.
        /// </summary>
        private bool _treatWarningsAsErrors;

        /// <summary>
        /// The private field used for the <see cref="CustomArguments"/> property.
        /// </summary>
        private string _customArguments;

        /// <summary>
        /// The private field used for the <see cref="CreateDependencyFile"/> property.
        /// </summary>
        private bool _createDependencyFile;

        /// <summary>
        /// The private field used for the <see cref="CreateNativeFile"/> property.
        /// </summary>
        private bool _createNativeFile;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CompilerTask"/> class.
        /// </summary>
        public CompilerTask()
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the list of san script source and header files that should be
        /// compiled.
        /// </summary>
        public string[] BuildFiles
        {
            get { return this._buildFiles; }
            set { this._buildFiles = value; }
        }

        /// <summary>
        /// Gets or sets the output path.
        /// </summary>
        public string OutputPath
        {
            get { return this._outputPath; }
            set { this._outputPath = value; }
        }

        /// <summary>
        /// Gets or sets the full path to the compiler exe to run per file.
        /// </summary>
        public string CompilerExecutable
        {
            get { return this._compilerExecutable; }
            set { this._compilerExecutable = value; }
        }

        /// <summary>
        /// Gets or sets the project path. This should be set to $(MSBuildProjectDirectory)
        /// </summary>
        public string ProjectPath
        {
            get { return this._projectPath; }
            set { this._projectPath = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a .d dependency file is also created.
        /// </summary>
        public bool CreateDependencyFile
        {
            get { return this._createDependencyFile; }
            set { this._createDependencyFile = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a .native.txt native file is also created.
        /// </summary>
        public bool CreateNativeFile
        {
            get { return this._createNativeFile; }
            set { this._createNativeFile = value; }
        }

        /// <summary>
        /// Gets or sets the additional custom arguments to add to the commandline for the
        /// script compilier.
        /// </summary>
        public string CustomArguments
        {
            get { return this._customArguments; }
            set { this._customArguments = value; }
        }

        /// <summary>
        /// Gets or sets the include file to push to the compiler executable.
        /// </summary>
        public string IncludeFile
        {
            get { return this._includeFile; }
            set { this._includeFile = value; }
        }

        /// <summary>
        /// Gets or sets the include path(s) to push to the compiler executable.
        /// </summary>
        public string IncludeDirectories
        {
            get { return this._includeDirectories; }
            set { this._includeDirectories = value; }
        }

        public bool DebugParser
        {
            get { return this._debugParser; }
            set { this._debugParser = value; }
        }

        public bool DumpThreadState
        {
            get { return this._dumpThreadState; }
            set { this._dumpThreadState = value; }
        }

        public bool DisplayDisassembly
        {
            get { return this._displayDisassembly; }
            set { this._displayDisassembly = value; }
        }

        public bool EnableHsm
        {
            get { return this._enableHsm; }
            set { this._enableHsm = value; }
        }

        public bool TreatWarningsAsErrors
        {
            get { return this._treatWarningsAsErrors; }
            set { this._treatWarningsAsErrors = value; }
        }
        #endregion Properties

        #region Methods
        public override bool Execute()
        {
            try
            {
                bool successful = true;
                string filename = this.BuildFiles[0];
                string output = Path.Combine(this.ProjectPath, this.OutputPath, Path.GetFileName(filename));
                output = Path.ChangeExtension(output, ".sco");

                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = this.CompilerExecutable;
                startInfo.UseShellExecute = false;
                startInfo.CreateNoWindow = true;
                startInfo.RedirectStandardOutput = true;
                Log.LogMessage(MessageImportance.High, output);
                startInfo.Arguments = this.GenerateCommandLineArguments();

                using (Process process = Process.Start(startInfo))
                {
                    process.WaitForExit();
                    using (StreamReader reader = process.StandardOutput)
                    {
                        string result = reader.ReadToEnd();
                        if (!String.IsNullOrWhiteSpace(result))
                        {
                            string[] lines = result.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                            foreach (string line in lines)
                            {
                                Log.LogMessage(MessageImportance.High, line);
                            }
                        }
                    }

                    if (process.ExitCode != 0)
                    {
                        successful = false;
                    }
                }

                return successful;
            }
            catch (Exception ex)
            {
                Log.LogErrorFromException(ex);
                return false;
            }
        }

        protected string GenerateCommandLineArguments()
        {
            string filename = this._buildFiles[0];
            string output = Path.Combine(this.ProjectPath, this.OutputPath, Path.GetFileName(filename));
            output = Path.ChangeExtension(output, ".sco");

            string complete = String.Format("\"{0}\"{1} -output \"{2}\"", filename, this.GenerateCommandLine(), output);


            return complete;
        }

        private string GenerateCommandLine()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            if (this.CustomArguments != null)
            {
                sb.AppendFormat(" {0}", this.CustomArguments.Trim());
            }

            if (this.IncludeFile != null)
            {
                sb.AppendFormat(" -include {0}", this.IncludeFile);
            }

            if (this.DebugParser)
            {
                sb.Append(" -debugparser");
            }

            if (this.DumpThreadState)
            {
                sb.Append(" -dump");
            }

            if (this.DisplayDisassembly)
            {
                sb.Append(" -dis");
            }

            if (this.EnableHsm)
            {
                sb.Append(" -hsm");
            }

            if (this.TreatWarningsAsErrors)
            {
                sb.Append(" -werror");
            }

            if (this.IncludeDirectories != null)
            {
                sb.AppendFormat(" -ipath {0}", this.IncludeDirectories);
            }

            return sb.ToString();
        }
        #endregion Methods
    } // SanScript.CompilerTask.BuildEventTask {Class}
} // SanScript.CompilerTask {Namespace}
