﻿
namespace SanScript.Project.Library
{
    using System;
    using System.Globalization;
    using System.Runtime.InteropServices;
    using Irony.Parsing;
    using Microsoft.VisualStudio;
    using Microsoft.VisualStudio.Shell.Interop;
    using Microsoft.VisualStudio.TextManager.Interop;

    /// <summary>
    /// This is a specialised version of the <see cref="LibraryNode"/> class that handles the
    /// SanScript's items. The main difference from the generic one is that it supports
    /// navigation to the location inside the source code where the element is defined.
    /// </summary>
    public class SanScriptLibraryNode : LibraryNode
    {
        #region Fields
        /// <summary>
        /// 
        /// </summary>
        private IVsHierarchy _ownerHierarchy;

        /// <summary>
        /// 
        /// </summary>
        private uint _fileId;

        /// <summary>
        /// 
        /// </summary>
        private TextSpan _sourceSpan;
        
        /// <summary>
        /// 
        /// </summary>
        private string _filename;

        /// <summary>
        /// 
        /// </summary>
        private IServiceProvider _serviceProvider;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance
        /// </summary>
        /// <param name="scope"></param>
        /// <param name="name"></param>
        /// <param name="hierarchy"></param>
        /// <param name="itemId"></param>
        /// <param name="serviceProvider"></param>
        internal SanScriptLibraryNode(
            ParseTreeNode node,
            string name,
            IVsHierarchy hierarchy,
            uint itemId,
            IServiceProvider serviceProvider) :
            base(name)
        {
            this.NodeType = LibraryNodeTypes.Members;
            //FunctionNode functionNode = scope as FunctionNode;
            //if (functionNode != null)
            //{
            //    this.NodeType = LibraryNodeTypes.Members;
            //}
            //else
            //{
            //    ClassNode classNode = scope as ClassNode;
            //    if (classNode != null)
            //    {
            //        this.NodeType = LibraryNodeTypes.Classes;
            //        IFormatProvider provider = CultureInfo.InvariantCulture;
            //        this.Name = string.Format(provider, "{0}{1}", namePrefix, scope.Name);
            //    }
            //}

            this._serviceProvider = serviceProvider;
            this._ownerHierarchy = hierarchy;
            this._fileId = itemId;

            // Now check if we have all the information to navigate to the source location.
            if (this._ownerHierarchy == null || this._fileId == VSConstants.VSITEMID_NIL)
            {
                return;
            }

            if (node == null)
            {
                return;
            }

            //if (Location.Compare(Location.None, scope.Start) == 0)
            //{
            //    return;
            //}

            //if (Location.Compare(Location.None, scope.End) == 0)
            //{
            //    return;
            //}

            this._sourceSpan = new TextSpan();
            this._sourceSpan.iStartIndex = node.Span.Location.Column;
            if (node.Span.Location.Line > 0)
            {
                this._sourceSpan.iStartLine = node.Span.Location.Line;
            }

            this._sourceSpan.iEndIndex = node.Span.Location.Column + node.Span.Length;
            if (node.Span.Location.Line > 0)
            {
                this._sourceSpan.iStartLine = node.Span.Location.Line;
            }

            this.CanGoToSource = true;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="SanScriptLibraryNode"/> class as a
        /// copy of the specified instance.
        /// </summary>
        /// <param name="node">
        /// The instance to copy.
        /// </param>
        internal SanScriptLibraryNode(SanScriptLibraryNode node) :
            base(node)
        {
            this._fileId = node._fileId;
            this._ownerHierarchy = node._ownerHierarchy;
            this._filename = node._filename;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the unique name for this library node.
        /// </summary>
        public override string UniqueName
        {
            get
            {
                if (string.IsNullOrEmpty(this._filename))
                {
                    int test = _ownerHierarchy.GetCanonicalName(_fileId, out this._filename);
                    ErrorHandler.ThrowOnFailure(test);
                }


                IFormatProvider provider = CultureInfo.InvariantCulture;
                return string.Format(provider, "{0}/{1}", this._filename, this.Name);
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="category">
        /// 
        /// </param>
        /// <returns>
        /// 
        /// </returns>
        protected override uint CategoryField(LIB_CATEGORY category)
        {
            switch (category)
            {
                case (LIB_CATEGORY)_LIB_CATEGORY2.LC_MEMBERINHERITANCE:
                    if (this.NodeType == LibraryNodeTypes.Members)
                    {
                        return (uint)_LIBCAT_MEMBERINHERITANCE.LCMI_IMMEDIATE;
                    }

                    break;
            }

            return base.CategoryField(category);
        }

        /// <summary>
        /// Creates a deep copy of this instance.
        /// </summary>
        /// <returns>
        /// A new instance of the <see cref="SanScriptLibraryNode"/> class as a copy of this
        /// instance.
        /// </returns>
        protected override LibraryNode Clone()
        {
            return new SanScriptLibraryNode(this);
        }

        /// <summary>
        /// Navigates to the source for the given type.
        /// </summary>
        /// <param name="type">
        /// Specifies the source type.
        /// </param>
        protected override void GotoSource(VSOBJGOTOSRCTYPE type)
        {
            // We do not support the "Goto Reference"
            if (VSOBJGOTOSRCTYPE.GS_REFERENCE == type)
            {
                return;
            }

            // There is no difference between definition and declaration, so here we
            // don't check for the other flags.

            IVsWindowFrame frame = null;
            IntPtr document = this.FindDocumentFromRunningDocumentTable();
            try
            {
                // Now we can try to open the editor. We assume that the owner hierarchy is
                // a project and we want to use its OpenItem method.
                IVsProject3 project = this._ownerHierarchy as IVsProject3;
                if (project == null)
                {
                    return;
                }

                Guid viewGuid = VSConstants.LOGVIEWID_Code;
                int test = project.OpenItem(this._fileId, ref viewGuid, document, out frame);
                ErrorHandler.ThrowOnFailure(test);
            }
            finally
            {
                if (IntPtr.Zero != document)
                {
                    Marshal.Release(document);
                    document = IntPtr.Zero;
                }
            }

            // Make sure that the document window is visible.
            ErrorHandler.ThrowOnFailure(frame.Show());

            // Get the code window from the window frame.
            object docView;
            int property = (int)__VSFPROPID.VSFPROPID_DocView;
            ErrorHandler.ThrowOnFailure(frame.GetProperty(property, out docView));
            IVsCodeWindow codeWindow = docView as IVsCodeWindow;
            if (codeWindow == null)
            {
                object docData;
                property = (int)__VSFPROPID.VSFPROPID_DocData;
                ErrorHandler.ThrowOnFailure(frame.GetProperty(property, out docData));
                codeWindow = docData as IVsCodeWindow;
                if (codeWindow == null)
                {
                    return;
                }
            }

            // Get the primary view from the code window.
            IVsTextView textView;
            ErrorHandler.ThrowOnFailure(codeWindow.GetPrimaryView(out textView));

            // Set the cursor at the beginning of the declaration.
            ErrorHandler.ThrowOnFailure(
                textView.SetCaretPos(
                this._sourceSpan.iStartLine, this._sourceSpan.iStartIndex));

            // Make sure that the text is visible.
            TextSpan visibleSpan = new TextSpan();
            visibleSpan.iStartLine = _sourceSpan.iStartLine;
            visibleSpan.iStartIndex = _sourceSpan.iStartIndex;
            visibleSpan.iEndLine = _sourceSpan.iStartLine;
            visibleSpan.iEndIndex = _sourceSpan.iStartIndex + 1;
            ErrorHandler.ThrowOnFailure(textView.EnsureSpanVisible(visibleSpan));
        }

        /// <summary>
        /// Finds the source files associated with this node.
        /// </summary>
        /// <param name="hierarchy">
        /// The hierarchy containing the items.
        /// </param>
        /// <param name="itemId">
        /// The item id of the item.
        /// </param>
        /// <param name="itemsCount">
        /// Number of items.
        /// </param>
        protected override void SourceItems(
            out IVsHierarchy hierarchy, out uint itemId, out uint itemsCount)
        {
            hierarchy = _ownerHierarchy;
            itemId = _fileId;
            itemsCount = 1;
        }

        /// <summary>
        /// Retrieves the document data for the associated document from the running document
        /// table service in visual studio.
        /// </summary>
        /// <returns>
        /// The document data for the associated document
        /// </returns>
        private IntPtr FindDocumentFromRunningDocumentTable()
        {
            // Get a reference to the RDT.
            Type type = typeof(SVsRunningDocumentTable);
            object service = this._serviceProvider.GetService(type);
            IVsRunningDocumentTable rdt = service as IVsRunningDocumentTable;
            if (rdt == null)
            {
                return IntPtr.Zero;
            }

            // Get the enumeration of the running documents.
            IEnumRunningDocuments documents;
            ErrorHandler.ThrowOnFailure(rdt.GetRunningDocumentsEnum(out documents));

            IntPtr documentData = IntPtr.Zero;
            uint[] cookie = new uint[1];
            uint fetched;
            while (documents.Next(1, cookie, out fetched) == VSConstants.S_OK)
            {
                if (fetched != 1)
                {
                    break;
                }

                uint flags;
                uint editLocks;
                uint readLocks;
                string filename;
                IVsHierarchy docHierarchy;
                uint docId;
                IntPtr docData = IntPtr.Zero;
                try
                {                 
                    ErrorHandler.ThrowOnFailure(
                        rdt.GetDocumentInfo(
                        cookie[0],
                        out flags,
                        out readLocks,
                        out editLocks,
                        out filename,
                        out docHierarchy,
                        out docId,
                        out docData));

                    // Check if this document is the one we are looking for.
                    if ((docId == _fileId) && (_ownerHierarchy.Equals(docHierarchy)))
                    {
                        documentData = docData;
                        docData = IntPtr.Zero;
                        break;
                    }
                }
                finally
                {
                    if (IntPtr.Zero != docData)
                    {
                        Marshal.Release(docData);
                    }
                }
            }

            return documentData;
        }
        #endregion Methods
    } // SanScript.Project.Library.SanScriptLibraryNode {Class}
} // SanScript.Project.Library {Namespace}
