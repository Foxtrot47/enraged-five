﻿namespace SanScript.CompilerTask
{
    using System;
    using System.IO;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Reflection;
    using Microsoft.Build.CPPTasks;
    using Microsoft.Build.Utilities;
    using Microsoft.Build.Framework;
using System.Diagnostics;

    /// <summary>
    /// 
    /// </summary>
    public class DependencyCreatorTask : Task
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="SourceFiles"/> property.
        /// </summary>
        private string[] _sourceFiles;

        /// <summary>
        /// The private field used for the <see cref="OutputPath"/> property.
        /// </summary>
        private string _outputPath;

        /// <summary>
        /// The private field used for the <see cref="IncludeDirectories"/> property.
        /// </summary>
        private string _includeDirectories;

        /// <summary>
        /// The private field used for the <see cref="ProjectPath"/> property.
        /// </summary>
        private string _projectPath;

        /// <summary>
        /// The private field used for the <see cref="IncludeFile"/> property.
        /// </summary>
        private static Dictionary<string, string> _mapper;

        /// <summary>
        /// The stopwatch used to time specific parts of the compiler.
        /// </summary>
        internal static Stopwatch sw;

        /// <summary>
        /// The private field used for the <see cref="SourceDependencies"/> property.
        /// </summary>
        private bool _sourceDependencies;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DependencyCreatorTask"/> class.
        /// </summary>
        public DependencyCreatorTask()
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the list of san script source and header files that should be
        /// compiled.
        /// </summary>
        public string[] SourceFiles
        {
            get { return this._sourceFiles; }
            set { this._sourceFiles = value; }
        }

        public bool UsingSelected
        {
            get;
            set;
        }

        public string SelectedFiles
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the output path.
        /// </summary>
        public string OutputPath
        {
            get { return this._outputPath; }
            set { this._outputPath = value; }
        }

        /// <summary>
        /// Gets or sets the include path(s) to push to the compiler executable.
        /// </summary>
        public string IncludeDirectories
        {
            get { return this._includeDirectories; }
            set { this._includeDirectories = value; }
        }

        /// <summary>
        /// Gets or sets the project path. This should be set to $(MSBuildProjectDirectory)
        /// </summary>
        public string ProjectPath
        {
            get { return this._projectPath; }
            set { this._projectPath = value; }
        }

        /// <summary>
        /// Gets or sets the project path. This should be set to $(MSBuildProjectDirectory)
        /// </summary>
        internal static Dictionary<string, string> Mapper
        {
            get { return _mapper; }
            set { _mapper = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether its the source (.sc) files that are being
        /// worked on.
        /// </summary>
        public bool SourceDependencies
        {
            get { return this._sourceDependencies; }
            set { this._sourceDependencies = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Main entry point for the task
        /// </summary>
        /// <returns></returns>
        public override bool Execute()
        {
            try
            {
                System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
                bool result = false;
                if (SourceDependencies)
                {
                    if (this.UsingSelected)
                    {
                        foreach (string file in this.SelectedFiles.Split(';'))
                        {
                            result = this.ExecuteForSource(file);
                        }
                    }
                    else
                    {
                        result = this.ExecuteForSource(this.SourceFiles[0]);
                    }
                }
                else
                {
                    if (this.UsingSelected)
                    {
                        foreach (string file in this.SelectedFiles.Split(';'))
                        {
                            result = this.ExecuteForHeader(file);
                        }
                    }
                    else
                    {
                        foreach (string file in this.SourceFiles)
                        {
                            result = this.ExecuteForHeader(file);
                        }
                    }
                }

                sw.Stop();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool ExecuteForHeader(string filename)
        {
            FileInfo sourceInfo = new FileInfo(filename);
            if (!sourceInfo.Exists)
            {
                return false;
            }

            string output = Path.Combine(OutputPath, Path.GetFileName(filename));
            output = Path.ChangeExtension(output, "d");

            using (StreamWriter file = new StreamWriter(output))
            {
                foreach (string dependency in this.GetRefreshedDependencies(filename, false))
                {
                    file.WriteLine(dependency);
                }
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool ExecuteForSource(string filename)
        {
            FileInfo inputInfo = new FileInfo(filename);
            if (!inputInfo.Exists)
            {
                return false;
            }

            string output = Path.Combine(OutputPath, Path.GetFileName(filename));
            output = Path.ChangeExtension(output, "sd");
            FileInfo outputInfo = new FileInfo(output);
            bool update = false;
            if (!outputInfo.Exists || outputInfo.LastWriteTime < inputInfo.LastWriteTime)
            {
                update = true;
            }

            if (!update)
            {
                List<string> dependencies = this.GetDependencies(filename);
                foreach (string dependency in dependencies)
                {
                    FileInfo dependencyInfo = new FileInfo(dependency);
                    if (dependencyInfo.Exists && dependencyInfo.LastWriteTime > outputInfo.LastWriteTime)
                    {
                        update = true;
                        break;
                    }
                }
            }

            if (update)
            {
                using (StreamWriter file = new StreamWriter(output))
                {
                    file.WriteLine(filename);
                }
            }

            return true;
        }

        public List<string> GetRefreshedDependencies(string filename, bool recursive)
        {
            List<string> dependencies = new List<string>();
            using (StreamReader file = new StreamReader(filename))
            {
                string line;
                while ((line = file.ReadLine()) != null)
                {
                    line = line.Trim();
                    if (String.IsNullOrWhiteSpace(line))
                    {
                        continue;
                    }

                    if (!line.StartsWith("USING "))
                    {
                        if (dependencies.Count > 0)
                        {
                            if (!line.StartsWith("#IF") && !line.StartsWith("//") && !line.StartsWith("/*"))
                            {
                                break;
                            }
                        }

                        continue;
                    }

                    int startQuoteIndex = line.IndexOf('"');
                    if (startQuoteIndex == -1)
                    {
                        continue;
                    }

                    int endQuoteIndex = line.IndexOf('"', startQuoteIndex + 1);
                    if (endQuoteIndex == -1)
                    {
                        continue;
                    }

                    int length = endQuoteIndex - startQuoteIndex;
                    string include = line.Substring(startQuoteIndex + 1, length - 1);
                    string dependencyFilename;
                    if (!Mapper.TryGetValue(include, out dependencyFilename))
                    {
                        continue;
                    }

                    dependencies.Add(dependencyFilename);
                    if (recursive)
                    {
                        dependencies.AddRange(this.GetRefreshedDependencies(dependencyFilename, true));
                    }
                }
            }

            return dependencies;
        }

        public List<string> GetDependencies(string filename)
        {
            List<string> dependencies = new List<string>();
            foreach (string dependency in this.GetRefreshedDependencies(filename, false))
            {
                dependencies.Add(dependency);
                List<string> cacheDependencies = new List<string>();
                List<string> readFiles = new List<string>();
                this.GetDependenciesFromCache(dependency, true, cacheDependencies, readFiles);
                foreach (string child in cacheDependencies)
                {
                    if (dependencies.Contains(child))
                    {
                        continue;
                    }

                    dependencies.Add(child);
                }
            }

            return dependencies;
        }

        public void GetDependenciesFromCache(string filename, bool recursive, List<string> dependencies, List<string> readFiles)
        {
            readFiles.Add(filename);
            List<string> lines = new List<string>();
            using (StreamReader file = new StreamReader(filename))
            {
                string line;
                while ((line = file.ReadLine()) != null)
                {
                    lines.Add(line);
                }
            }

            foreach (string line in lines)
            {
                if (dependencies.Contains(line))
                {
                    continue;
                }

                dependencies.Add(line);
            }

            if (recursive)
            {
                foreach (string line in lines)
                {
                    if (readFiles.Contains(line))
                    {
                        continue;
                    }

                    this.GetDependenciesFromCache(line, true, dependencies, readFiles);
                }
            }
        }
        #endregion Methods
    }
}