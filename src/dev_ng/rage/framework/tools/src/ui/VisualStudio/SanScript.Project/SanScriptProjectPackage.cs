﻿namespace SanScript.Project
{
    using System;
    using System.ComponentModel.Design;
    using System.Diagnostics;
    using System.Runtime.InteropServices;
    using Microsoft.VisualStudio.OLE.Interop;
    using Microsoft.VisualStudio.Project;
    using Microsoft.VisualStudio.Shell;
    using SanScript.Project.Library;

    /// <summary>
    /// This is the class that implements the package exposed by this assembly.
    ///
    /// The minimum requirement for a class to be considered a valid package for Visual Studio
    /// is to implement the IVsPackage interface and register itself with the shell.
    /// This package uses the helper classes defined inside the Managed Package Framework (MPF)
    /// to do it: it derives from the Package class that provides the implementation of the 
    /// IVsPackage interface and uses the registration attributes defined in the framework to 
    /// register itself and its components with the shell.
    /// </summary>

    // This attribute tells the PkgDef creation utility (CreatePkgDef.exe) that this class is
    // a package.
    [PackageRegistration(UseManagedResourcesOnly = true)]

    // This attribute is used to register the information needed to show this package
    // in the Help/About dialog of Visual Studio.
    [InstalledProductRegistration("#110", "#112", "1.0", IconResourceID = 400)]

    // The attribute is used to register the information needed to open and show SanScript
    // project files in the solution explorer, open/save dialog, and new project dialog.
    [ProvideProjectFactory(typeof(SanScriptProjectFactory), null,
    "SanScript Project Files (*.ssproj);*.ssproj", "ssproj", "ssproj",
    ".\\NullPath", LanguageVsTemplate = "SanScript")]

    [ProvideMenuResource("Menus.ctmenu", 1)]
    [Guid(GuidList.guidSanScriptProjectPkgString)]

    [ProvideObject(typeof(SanScriptBuildPropertyPage))]
    [ProvideObject(typeof(SanScriptGeneralPropertyPage))]
    [ProvideObject(typeof(BuildEventPropertyPage))]
    [ProvideLanguageService(GuidList.guidSanScriptLanguageString, "SanScript", 101, RequestStockColors = true)]
    [ProvideLanguageExtension(GuidList.guidSanScriptLanguageString, ".sc")]
    [ProvideLanguageExtension(GuidList.guidSanScriptLanguageString, ".sch")]
    public sealed class SanScriptProjectPackage : ProjectPackage, IOleComponent
    {
        #region Fields
        /// <summary>
        /// The private reference to the library manager being used for this package.
        /// </summary>
        private SanScriptLibraryManager _libraryManager;

        /// <summary>
        /// The ole component id for this object.
        /// </summary>
        private uint _componentID;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SanScriptProjectPackage"/> class.
        /// </summary>
        public SanScriptProjectPackage()
        {
            Debug.WriteLine("Entering constructor for: SanScriptProjectPackage");
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the product user context. Always throws NotImplementedException.
        /// </summary>
        public override string ProductUserContext
        {
            get { throw new NotImplementedException(); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Releases the resources used by the this object.
        /// </summary>
        /// <param name="disposing">
        /// True if the object is being disposed, false if it is being finalised.
        /// </param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (this._libraryManager != null)
                {
                    this._libraryManager.Dispose();
                    this._libraryManager = null;
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        /// <summary>
        /// Initialisation of the package; this method is called right after the package is
        /// sited, so this is the place where you can put all the initialization code that rely
        /// on services provided by VisualStudio.
        /// </summary>
        protected override void Initialize()
        {
            Debug.WriteLine("Entering Initialize() of: SanScriptProjectPackage");
            base.Initialize();

            this.RegisterProjectFactory(new SanScriptProjectFactory(this));

            IServiceContainer container = this as IServiceContainer;
            container.AddService(typeof(ISanScriptLibraryManager), this.CreateService, true);
        }

        /// <summary>
        /// Creates the service for this specified type of service.
        /// </summary>
        /// <param name="container">
        /// The container that should contain the service object.
        /// </param>
        /// <param name="serviceType">
        /// The type of service that should be created.
        /// </param>
        /// <returns>
        /// The service object that is created.
        /// </returns>
        private object CreateService(IServiceContainer container, Type serviceType)
        {
            object service = null;
            if (serviceType == typeof(ISanScriptLibraryManager))
            {
                this._libraryManager = new SanScriptLibraryManager(this);
                service = this._libraryManager as ISanScriptLibraryManager;
                this.RegisterForIdleTime();
            }

            return service;
        }

        /// <summary>
        /// Registers this ole component with the component manager to retrieve the component
        /// id and requesting idle time.
        /// </summary>
        private void RegisterForIdleTime()
        {
            Type type = typeof(SOleComponentManager);
            IOleComponentManager mgr = this.GetService(type) as IOleComponentManager;
            if (this._componentID == 0 && mgr != null)
            {
                OLECRINFO[] info = new OLECRINFO[1];
                info[0].cbSize = (uint)Marshal.SizeOf(typeof(OLECRINFO));

                info[0].grfcrf = (uint)_OLECRF.olecrfNeedIdleTime;
                info[0].grfcrf |= (uint)_OLECRF.olecrfNeedPeriodicIdleTime;

                info[0].grfcadvf = (uint)_OLECADVF.olecadvfModal;
                info[0].grfcadvf |= (uint)_OLECADVF.olecadvfRedrawOff;
                info[0].grfcadvf |= (uint)_OLECADVF.olecadvfWarningsOff;
                info[0].uIdleTimeInterval = 1000;

                int hr = mgr.FRegisterComponent(this, info, out this._componentID);
            }
        }
        #endregion Methods

        #region IOldComponent Methods
        public int FContinueMessageLoop(uint uReason, IntPtr pvLoopData, MSG[] pMsgPeeked)
        {
            return 1;
        }

        public int FDoIdle(uint grfidlef)
        {
            if (this._libraryManager != null)
            {
                this._libraryManager.OnIdle();
            }

            return 0;
        }

        public int FPreTranslateMessage(MSG[] pMsg)
        {
            return 0;
        }

        public int FQueryTerminate(int fPromptUser)
        {
            return 1;
        }

        public int FReserved1(uint dwReserved, uint message, IntPtr wParam, IntPtr lParam)
        {
            return 1;
        }

        public IntPtr HwndGetWindow(uint dwWhich, uint dwReserved)
        {
            return IntPtr.Zero;
        }

        public void OnActivationChange(
            IOleComponent pic,
            int fSameComponent,
            OLECRINFO[] pcrinfo,
            int fHostIsActivating,
            OLECHOSTINFO[] pchostinfo,
            uint dwReserved)
        {
        }

        public void OnAppActivate(int fActive, uint dwOtherThreadID)
        {
        }

        public void OnEnterState(uint uStateID, int fEnter)
        {
        }

        public void OnLoseActivation()
        {
        }

        public void Terminate()
        {
        }
        #endregion IOldComponent Methods
    } // SanScript.Project.SanScriptProjectPackage {Class}
} // SanScript.Project {Namespace}
