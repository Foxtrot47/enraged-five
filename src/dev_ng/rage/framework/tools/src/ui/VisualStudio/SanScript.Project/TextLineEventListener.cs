﻿namespace SanScript.Project
{
    using System;
    using System.Diagnostics;
    using Microsoft.VisualStudio.OLE.Interop;
    using Microsoft.VisualStudio.TextManager.Interop;
    using SanScript.Project.Library;

    /// <summary>
    /// 
    /// </summary>
    internal class TextLineEventListener : IVsTextLinesEvents, IDisposable
    {
        #region Fields
        /// <summary>
        /// 
        /// </summary>
        private const int defaultDelay = 2000;

        /// <summary>
        /// 
        /// </summary>
        private IVsTextLines _buffer;

        /// <summary>
        /// 
        /// </summary>
        private uint _connectionCookie;

        /// <summary>
        /// 
        /// </summary>
        private IConnectionPoint _connectionPoint;

        /// <summary>
        /// 
        /// </summary>
        private SanScriptModuleId _fileId;

        /// <summary>
        /// 
        /// </summary>
        private string _fileName;

        /// <summary>
        /// 
        /// </summary>
        private bool _isDirty;

        /// <summary>
        /// 
        /// </summary>
        private EventHandler<HierarchyEventArgs> _onFileChanged;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="TextLineEventListener"/> class.
        /// </summary>
        /// <param name="buffer">
        /// 
        /// </param>
        /// <param name="fileName">
        /// 
        /// </param>
        /// <param name="id">
        /// 
        /// </param>
        public TextLineEventListener(
            IVsTextLines buffer, string fileName, SanScriptModuleId id)
        {
            this._buffer = buffer;
            this._fileId = id;
            this._fileName = fileName;
            IConnectionPointContainer container = buffer as IConnectionPointContainer;
            if (null != container)
            {
                Guid eventsGuid = typeof(IVsTextLinesEvents).GUID;
                container.FindConnectionPoint(ref eventsGuid, out this._connectionPoint);
                this._connectionPoint.Advise(this as IVsTextLinesEvents, out this._connectionCookie);
            }
        }
        #endregion Constructors

        #region Events
        /// <summary>
        /// 
        /// </summary>
        public event EventHandler<HierarchyEventArgs> OnFileChanged
        {
            add { _onFileChanged += value; }
            remove { _onFileChanged -= value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public event TextLineChangeEvent OnFileChangedImmediate;
        #endregion Events

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public SanScriptModuleId FileID
        {
            get { return this._fileId; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string FileName
        {
            get { return this._fileName; }
            set { this._fileName = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Disposes this instance.
        /// </summary>
        public void Dispose()
        {
            if (this._connectionPoint != null && this._connectionCookie != 0)
            {
                this._connectionPoint.Unadvise(this._connectionCookie);
                Debug.WriteLine("\n\tUnadvised from TextLinesEvents\n");
            }

            this._connectionCookie = 0;
            this._connectionPoint = null;
            this._buffer = null;
            this._fileId = null;
        }

        /// <summary>
        /// Called whenever a line attribute changes.
        /// </summary>
        /// <param name="firstLine">
        /// The index to the first line whose attributes have changed.
        /// </param>
        /// <param name="lastLine">
        /// The index to the last line whose attributes have changed.
        /// </param>
        void IVsTextLinesEvents.OnChangeLineAttributes(int firstLine, int lastLine)
        {
        }

        /// <summary>
        /// Called whenever a lines text changes.
        /// </summary>
        /// <param name="change">
        /// A TextLineChange structure that defines the shape of the old and new text.
        /// </param>
        /// <param name="last">
        /// Obsolete; Do not use.
        /// </param>
        void IVsTextLinesEvents.OnChangeLineText(TextLineChange[] change, int last)
        {
            TextLineChangeEvent handler = this.OnFileChangedImmediate;
            if (handler != null)
            {
                handler(this, change, last);
            }

            this._isDirty = true;
        }

        /// <summary>
        /// Called when the editor is in a idle state.
        /// </summary>
        public void OnIdle()
        {
            if (!this._isDirty)
            {
                return;
            }

            if (this._onFileChanged == null)
            {
                this._isDirty = false;
                return;
            }

            uint id = this._fileId.ItemId;
            if (this._onFileChanged != null)
            {
                HierarchyEventArgs args = new HierarchyEventArgs(id, this._fileName);
                args.Buffer = this._buffer;
                this._onFileChanged(this._fileId.Hierarchy, args);
            }

            this._isDirty = false;
        }
        #endregion Methods
    } // SanScript.Project.TextLineEventListener {Class}
} // SanScript.Project {Namespace}
