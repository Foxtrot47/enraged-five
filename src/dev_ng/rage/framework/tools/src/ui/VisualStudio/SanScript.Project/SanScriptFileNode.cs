﻿namespace SanScript.Project
{
    using Microsoft.VisualStudio.Project;
    using OleConstants = Microsoft.VisualStudio.OLE.Interop.Constants;
    using VsCommands = Microsoft.VisualStudio.VSConstants.VSStd97CmdID;
    using VsCommands2K = Microsoft.VisualStudio.VSConstants.VSStd2KCmdID;

    /// <summary>
    /// Represents the SanScript file node inside the solution explorer underneath the
    /// SanScript project node.
    /// </summary>
    internal class SanScriptFileNode : FileNode
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SanScriptFileNode"/> class.
        /// </summary>
        /// <param name="root">
        /// The root of the hierarchy this node belongs to.
        /// </param>
        /// <param name="element">
        /// The associated project element for this file.
        /// </param>
        public SanScriptFileNode(ProjectNode root, ProjectElement element)
            : base(root, element)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the offset of the project node bitmap in the image list.
        /// </summary>
        public override int ImageIndex
        {
            get
            {
                if (this.FileName.ToLower().EndsWith(".sc"))
                {
                    return SanScriptProjectNode.ImageOffset + (int)SanScriptImageName.Source;
                }
                else if (this.FileName.ToLower().EndsWith(".sch"))
                {
                    return SanScriptProjectNode.ImageOffset + (int)SanScriptImageName.Header;
                }

                return base.ImageIndex;
            }
        }

        /// <summary>
        /// Gets or sets the sub type of an SanScript FileNode.
        /// </summary>
        public string SubType
        {
            get { return this.ItemNode.GetMetadata(ProjectFileConstants.SubType); }
            set { this.ItemNode.SetMetadata(ProjectFileConstants.SubType, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override NodeProperties CreatePropertiesObject()
        {
            SanScriptFileNodeProperties properties = new SanScriptFileNodeProperties(this);
            return properties;
        }
        #endregion Methods
    } // SanScript.Project.SanScriptFileNode {Class}
} // SanScript.Project {Namespace}
