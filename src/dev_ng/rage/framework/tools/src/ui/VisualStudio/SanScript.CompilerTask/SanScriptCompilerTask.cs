﻿namespace SanScript.CompilerTask
{
    using System;
    using System.IO;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Reflection;
    using Microsoft.Build.CPPTasks;
    using Microsoft.Build.Utilities;
    using Microsoft.Build.Framework;
    using System.Diagnostics;

    /// <summary>
    /// 
    /// </summary>
    public class SanScriptCompilerTask : ToolTask
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="CompilerExecutable"/> property.
        /// </summary>
        private string _compilerExecutable;

        /// <summary>
        /// The private field used for the <see cref="SourceFiles"/> property.
        /// </summary>
        private string[] _sourceFiles;

        /// <summary>
        /// The private field used for the <see cref="OutputPath"/> property.
        /// </summary>
        private string _outputPath;

        /// <summary>
        /// The private field used for the <see cref="ProjectPath"/> property.
        /// </summary>
        private string _projectPath;

        /// <summary>
        /// The private field used for the <see cref="IncludeFile"/> property.
        /// </summary>
        private string _includeFile;

        /// <summary>
        /// The private field used for the <see cref="IncludeDirectories"/> property.
        /// </summary>
        private string _includeDirectories;

        /// <summary>
        /// The private field used for the <see cref="DebugParser"/> property.
        /// </summary>
        private bool _debugParser;

        /// <summary>
        /// The private field used for the <see cref="DumpThreadState"/> property.
        /// </summary>
        private bool _dumpThreadState;

        /// <summary>
        /// The private field used for the <see cref="DisplayDisassembly"/> property.
        /// </summary>
        private bool _displayDisassembly;

        /// <summary>
        /// The private field used for the <see cref="EnableHsm"/> property.
        /// </summary>
        private bool _enableHsm;

        /// <summary>
        /// The private field used for the <see cref="TreatWarningsAsErrors"/> property.
        /// </summary>
        private bool _treatWarningsAsErrors;

        /// <summary>
        /// The private field used for the <see cref="CompileToConfigurationFolder"/> property.
        /// </summary>
        private bool _compileToConfigurationFolder;

        /// <summary>
        /// The private field used for the <see cref="MsBuildProjectFilePath"/> property.
        /// </summary>
        private string _msBuildProjectFilePath;

        /// <summary>
        /// The private field used for the <see cref="CustomArguments"/> property.
        /// </summary>
        private string _customArguments;

        /// <summary>
        /// The private field used for the <see cref="CreateDependencyFile"/> property.
        /// </summary>
        private bool _createDependencyFile;

        /// <summary>
        /// The private field used for the <see cref="CreateNativeFile"/> property.
        /// </summary>
        private bool _createNativeFile;

        private bool _buildingSelection;

        private string _commandLine;

        private bool _cancelled;
        #endregion Fields

        #region Constructors
        /// <summary>
		/// Initialises a new instance of the <see cref="SanScriptCompilerTask"/> class.
		/// </summary>
        public SanScriptCompilerTask()
		{
		}
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the additional custom arguments to add to the commandline for the
        /// script compilier.
        /// </summary>
        public string CustomArguments
        {
            get { return this._customArguments; }
            set { this._customArguments = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a .d dependency file is also created.
        /// </summary>
        public bool CreateDependencyFile
        {
            get { return this._createDependencyFile; }
            set { this._createDependencyFile = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a .native.txt native file is also created.
        /// </summary>
        public bool CreateNativeFile
        {
            get { return this._createNativeFile; }
            set { this._createNativeFile = value; }
        }

        /// <summary>
        /// Gets or sets the list of san script source and header files that should be
        /// compiled.
        /// </summary>
        public string[] SourceFiles
        {
            get { return this._sourceFiles; }
            set { this._sourceFiles = value; }
        }

        /// <summary>
        /// Gets or sets the output path.
        /// </summary>
        public string OutputPath
        {
            get { return this._outputPath; }
            set { this._outputPath = value; }
        }

        /// <summary>
        /// Gets or sets the full path to the compiler exe to run per file.
        /// </summary>
        public string CompilerExecutable
        {
            get { return this._compilerExecutable; }
            set { this._compilerExecutable = value; }
        }

        /// <summary>
        /// Gets or sets the include file to push to the compiler executable.
        /// </summary>
        public string IncludeFile
        {
            get { return this._includeFile; }
            set { this._includeFile = value; }
        }

        /// <summary>
        /// Gets or sets the include path(s) to push to the compiler executable.
        /// </summary>
        public string IncludeDirectories
        {
            get { return this._includeDirectories; }
            set { this._includeDirectories = value; }
        }

        /// <summary>
        /// Gets or sets the project path. This should be set to $(MSBuildProjectDirectory)
        /// </summary>
        public string ProjectPath
        {
            get { return this._projectPath; }
            set { this._projectPath = value; }
        }

        public bool DebugParser
        {
            get { return this._debugParser; } 
            set { this._debugParser = value; }
        }

        public bool DumpThreadState
        {
            get { return this._dumpThreadState; } 
            set { this._dumpThreadState = value; }
        }

        public bool DisplayDisassembly
        {
            get { return this._displayDisassembly; } 
            set { this._displayDisassembly = value; }
        }

        public bool EnableHsm
        {
            get { return this._enableHsm; } 
            set { this._enableHsm = value; }
        }

        public bool TreatWarningsAsErrors
        {
            get { return this._treatWarningsAsErrors; } 
            set { this._treatWarningsAsErrors = value; }
        }

        public bool CompileToConfigurationFolder
        {
            get { return this._compileToConfigurationFolder; } 
            set { this._compileToConfigurationFolder = value; }
        }

        public string MsBuildProjectFilePath
        {
            get { return this._msBuildProjectFilePath; } 
            set { this._msBuildProjectFilePath = value; }
        }

        public bool BuildingSelection
        {
            get { return this._buildingSelection; }
            set { this._buildingSelection = value; }
        }

        public string DependencyPath
        {
            get;
            set;
        }

        protected override string ToolName
        {
            get { throw new NotImplementedException(); }
        }
        #endregion Properties

        #region Methods
        protected override string GenerateFullPathToTool()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Main entry point for the task
        /// </summary>
        /// <returns></returns>
        public override bool Execute()
        {
            try
            {
                FileInfo projectInfo = new FileInfo(MsBuildProjectFilePath);
                DateTime projectLastWriteTime = new DateTime();
                if (projectInfo.Exists)
                {
                    projectLastWriteTime = projectInfo.LastWriteTime;
                }

                List<string> compile = new List<string>();
                foreach (string sourceFile in this.SourceFiles)
                {
                    string output = Path.Combine(this.ProjectPath, this.OutputPath, Path.GetFileName(sourceFile));
                    output = Path.ChangeExtension(output, ".sco");
                    FileInfo sourceInfo = new FileInfo(sourceFile);
                    bool update = false;
                    if (this.BuildingSelection)
                    {
                        update = true;
                    }
                    else
                    {
                        FileInfo outputInfo = new FileInfo(output);
                        if (!update)
                        {
                            if (sourceInfo.Exists)
                            {
                                if (outputInfo.LastWriteTime < projectLastWriteTime)
                                {
                                    update = true;
                                }
                            }
                        }

                        if (!update)
                        {
                            if (outputInfo.Exists)
                            {
                                if (outputInfo.LastWriteTime < sourceInfo.LastWriteTime)
                                {
                                    update = true;
                                }
                            }
                            else
                            {
                                update = true;
                            }
                        }

                        if (!update)
                        {
                            string sdFile = Path.Combine(this.DependencyPath, Path.GetFileName(sourceFile));
                            sdFile = Path.ChangeExtension(sdFile, ".sd");
                            FileInfo sdFileInfo = new FileInfo(sdFile);
                            if (sdFileInfo.Exists)
                            {
                                if (sdFileInfo.LastWriteTime > outputInfo.LastWriteTime)
                                {
                                    update = true;
                                }
                            }
                            else
                            {
                                update = true;
                            }
                        }
                    }

                    if (update)
                    {
                        compile.Add(sourceFile);
                    }
                }

                bool result = true;
                if (compile.Count > 0)
                {
                    result = this.Compile(compile);
                }

                System.Threading.Thread.Sleep(100);
                string msg = String.Format("Compiled {0} files, {1} files up-to-date...", compile.Count, this.SourceFiles.Length - compile.Count);
                Log.LogMessage(MessageImportance.High, msg);
                System.Threading.Thread.Sleep(100);
                return result;
            }
            catch (Exception exception)
            {
                Log.LogErrorFromException(exception);
                return false;
            }
        }

        public override void Cancel()
        {
            base.Cancel();
            this._cancelled = true;
        }

        private bool Compile(List<string> filenames)
        {
            bool successful = true;
            this._cancelled = false;
            this._commandLine = this.GenerateCommandLine();
            foreach (string filename in filenames)
            {
                if (this._cancelled)
                {
                    break;
                }

                string output = Path.Combine(this.ProjectPath, this.OutputPath, Path.GetFileName(filename));
                output = Path.ChangeExtension(output, ".sco");

                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = this.CompilerExecutable;
                startInfo.UseShellExecute = false;
                startInfo.CreateNoWindow = true;
                startInfo.RedirectStandardOutput = true;
                Log.LogMessage(MessageImportance.High, output);
                startInfo.Arguments = String.Format("\"{0}\"{1} -output \"{2}\" rem IncrediBuild_OutputFile \"{2}\" rem IncrediBuild_OutputDir \"{3}\"", filename, this._commandLine, output, Path.GetDirectoryName(output));

                using (Process process = Process.Start(startInfo))
                {
                    process.WaitForExit();
                    using (StreamReader reader = process.StandardOutput)
                    {
                        string result = reader.ReadToEnd();
                        if (!String.IsNullOrWhiteSpace(result))
                        {
                            string[] lines = result.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                            foreach (string line in lines)
                            {
                                Log.LogMessage(MessageImportance.High, line);
                            }
                        }
                    }

                    if (process.ExitCode != 0)
                    {
                        successful = false;
                    }
                }
            }

            return successful;
        }

        private string GenerateCommandLine()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            if (this.CustomArguments != null)
            {
                sb.AppendFormat(" {0}", this.CustomArguments.Trim());
            }

            if (this.IncludeFile != null)
            {
                sb.AppendFormat(" -include {0}", this.IncludeFile);
            }

            if (this.DebugParser)
            {
                sb.Append(" -debugparser");
            }

            if (this.DumpThreadState)
            {
                sb.Append(" -dump");
            }

            if (this.DisplayDisassembly)
            {
                sb.Append(" -dis");
            }

            if (this.EnableHsm)
            {
                sb.Append(" -hsm");
            }

            if (this.TreatWarningsAsErrors)
            {
                sb.Append(" -werror");
            }

            if (this.IncludeDirectories != null)
            {
                sb.AppendFormat(" -ipath {0}", this.IncludeDirectories);
            }

            sb.AppendFormat(" rem IncrediBuild_AllowRemote");

            return sb.ToString();
        }
        #endregion Methods
    } // SanScript.CompilerTask.SanScriptCompilerTask {Class}
} // SanScript.CompilerTask {Namespace}
