﻿namespace SanScript.EditorExtensions.Classification
{
    using System.ComponentModel.Composition;
    using System.Windows.Media;
    using Microsoft.VisualStudio.Text.Classification;
    using Microsoft.VisualStudio.Utilities;

    /// <summary>
    /// Classification definitions
    /// </summary>
    internal class ClassificationDefinitions
    {
        #region Classes
        /// <summary>
        /// 
        /// </summary>
        [Export(typeof(EditorFormatDefinition))]
        [UserVisible(true)]
        [ClassificationType(ClassificationTypeNames = ClassificationTypes.ReservedWord)]
        [Name("SanScriptReservedWordFormatDefinition")]
        [Order]
        internal sealed class ReservedWordDefinition : ClassificationFormatDefinition
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="ReservedWordDefinition"/> class.
            /// </summary>
            internal ReservedWordDefinition()
            {
                this.ForegroundColor = Colors.Blue;
                this.DisplayName = "San Script Reserved Word";
            }
            #endregion Constructors
        } // ClassificationDefinitions.ReservedWordDefinition {Class}

        /// <summary>
        /// 
        /// </summary>
        [Export(typeof(EditorFormatDefinition))]
        [UserVisible(true)]
        [ClassificationType(ClassificationTypeNames = ClassificationTypes.NativeType)]
        [Name("SanScriptNativeTypeFormatDefinition")]
        [Order(After="SanScriptReservedWordFormatDefinition")]
        internal sealed class NativeTypeDefinition : ClassificationFormatDefinition
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="NativeTypeDefinition"/> class.
            /// </summary>
            internal NativeTypeDefinition()
            {
                this.ForegroundColor = Colors.Blue;
                this.DisplayName = "San Script Native Type";
            }
            #endregion Constructors
        } // ClassificationDefinitions.NativeTypeDefinition {Class}

        /// <summary>
        /// 
        /// </summary>
        [Export(typeof(EditorFormatDefinition))]
        [UserVisible(true)]
        [ClassificationType(ClassificationTypeNames = ClassificationTypes.Operator)]
        [Name("SanScriptOperatorFormatDefinition")]
        [Order(After="SanScriptNativeTypeFormatDefinition")]
        internal sealed class OperatorDefinition : ClassificationFormatDefinition
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="OperatorDefinition"/> class.
            /// </summary>
            internal OperatorDefinition()
            {
                this.DisplayName = "San Script Operator";
            }
            #endregion Constructors
        } // ClassificationDefinitions.OperatorDefinition {Class}

        /// <summary>
        /// 
        /// </summary>
        [Export(typeof(EditorFormatDefinition))]
        [UserVisible(true)]
        [ClassificationType(ClassificationTypeNames = ClassificationTypes.ReservedWordOperator)]
        [Name("SanScriptReservedWordOperatorFormatDefinition")]
        [Order(After="SanScriptOperatorFormatDefinition")]
        internal sealed class ReservedWordOperatorDefinition : ClassificationFormatDefinition
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="ReservedWordOperatorDefinition"/>
            /// class.
            /// </summary>
            internal ReservedWordOperatorDefinition()
            {
                this.ForegroundColor = Colors.Blue;
                this.DisplayName = "San Script Reserved Word Operator";
            }
            #endregion Constructors
        } // ClassificationDefinitions.ReservedWordOperatorDefinition {Class}

        /// <summary>
        /// 
        /// </summary>
        [Export(typeof(EditorFormatDefinition))]
        [UserVisible(true)]
        [ClassificationType(ClassificationTypeNames = ClassificationTypes.Number)]
        [Name("SanScriptNumberFormatDefinition")]
        [Order(After="SanScriptReservedWordOperatorFormatDefinition")]
        internal sealed class NumberDefinition : ClassificationFormatDefinition
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="NumberDefinition"/>
            /// class.
            /// </summary>
            internal NumberDefinition()
            {
                this.ForegroundColor = Colors.Black;
                this.DisplayName = "San Script Number";
            }
            #endregion Constructors
        } // ClassificationDefinitions.NumberDefinition {Class}

        /// <summary>
        /// 
        /// </summary>
        [Export(typeof(EditorFormatDefinition))]
        [UserVisible(true)]
        [ClassificationType(ClassificationTypeNames = ClassificationTypes.StringDelimiter)]
        [Name("SanScriptStringDelimiterFormatDefinition")]
        [Order(After="SanScriptNumberFormatDefinition")]
        internal sealed class StringDelimiterDefinition : ClassificationFormatDefinition
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="StringDelimiterDefinition"/>
            /// class.
            /// </summary>
            internal StringDelimiterDefinition()
            {
                this.ForegroundColor = Colors.Black;
                this.DisplayName = "San Script String Delimiter";
            }
            #endregion Constructors
        } // ClassificationDefinitions.StringDelimiterDefinition {Class}

        /// <summary>
        /// 
        /// </summary>
        [Export(typeof(EditorFormatDefinition))]
        [UserVisible(true)]
        [ClassificationType(ClassificationTypeNames = ClassificationTypes.StringDefault)]
        [Name("SanScriptStringDefaultFormatDefinition")]
        [Order(After="SanScriptStringDelimiterFormatDefinition")]
        internal sealed class StringDefaultDefinition : ClassificationFormatDefinition
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="StringDefaultDefinition"/> class.
            /// </summary>
            internal StringDefaultDefinition()
            {
                this.ForegroundColor = Colors.Gray;
                this.DisplayName = "San Script Literal String";
            }
            #endregion Constructors
        } // ClassificationDefinitions.StringDefaultDefinition {Class}

        /// <summary>
        /// 
        /// </summary>
        [Export(typeof(EditorFormatDefinition))]
        [UserVisible(true)]
        [ClassificationType(ClassificationTypeNames = ClassificationTypes.CommentDelimiter)]
        [Name("SanScriptCommentDelimiterFormatDefinition")]
        [Order(After="SanScriptStringDefaultFormatDefinition")]
        internal sealed class CommentDelimiterDefinition : ClassificationFormatDefinition
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="CommentDelimiterDefinition"/>
            /// class.
            /// </summary>
            internal CommentDelimiterDefinition()
            {
                this.ForegroundColor = Colors.Green;
                this.DisplayName = "San Script Comment Delimiter";
            }
            #endregion Constructors
        } // ClassificationDefinitions.CommentDelimiterDefinition {Class}

        /// <summary>
        /// 
        /// </summary>
        [Export(typeof(EditorFormatDefinition))]
        [UserVisible(true)]
        [ClassificationType(ClassificationTypeNames = ClassificationTypes.CommentDefault)]
        [Name("SanScriptCommentDefaultFormatDefinition")]
        [Order(After="SanScriptCommentDelimiterFormatDefinition")]
        internal sealed class CommentDefaultDefinition : ClassificationFormatDefinition
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="CommentDefaultDefinition"/> class.
            /// </summary>
            internal CommentDefaultDefinition()
            {
                this.ForegroundColor = Colors.Green;
                this.DisplayName = "San Script Comment";
            }
            #endregion Constructors
        } // ClassificationDefinitions.CommentDefaultDefinition {Class}

        /// <summary>
        /// 
        /// </summary>
        [Export(typeof(EditorFormatDefinition))]
        [UserVisible(true)]
        [ClassificationType(ClassificationTypeNames = ClassificationTypes.BuiltInWord)]
        [Name("SanScriptBuiltInWordFormatDefinition")]
        [Order(After="SanScriptCommentDefaultFormatDefinition")]
        internal sealed class BuiltInWordDefinition : ClassificationFormatDefinition
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="BuiltInWordDefinition"/> class.
            /// </summary>
            internal BuiltInWordDefinition()
            {
                this.ForegroundColor = Colors.Brown;
                this.DisplayName = "San Script Built In Word";
            }
            #endregion Constructors
        } // ClassificationDefinitions.BuiltInWordDefinition {Class}

        /// <summary>
        /// 
        /// </summary>
        [Export(typeof(EditorFormatDefinition))]
        [UserVisible(true)]
        [ClassificationType(ClassificationTypeNames = ClassificationTypes.UserWord)]
        [Name("SanScriptUserWordFormatDefinition")]
        [Order(After="SanScriptBuiltInWordFormatDefinition")]
        internal sealed class UserWordDefinition : ClassificationFormatDefinition
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="UserWordDefinition"/> class.
            /// </summary>
            internal UserWordDefinition()
            {
                this.ForegroundColor = Colors.Teal;
                this.DisplayName = "San Script User Word";
            }
            #endregion Constructors
        } // ClassificationDefinitions.UserWordDefinition {Class}

        /// <summary>
        /// 
        /// </summary>
        [Export(typeof(EditorFormatDefinition))]
        [UserVisible(true)]
        [ClassificationType(ClassificationTypeNames = ClassificationTypes.PreprocessorMacro)]
        [Name("SanScriptPreprocessorMacroFormatDefinition")]
        [Order(After="SanScriptUserWordFormatDefinition")]
        internal sealed class PreprocessorMacroDefinition : ClassificationFormatDefinition
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="PreprocessorMacroDefinition"/>
            /// class.
            /// </summary>
            internal PreprocessorMacroDefinition()
            {
                this.ForegroundColor = Colors.Blue;
                this.DisplayName = "San Script Preprocessor Macro";
            }
            #endregion Constructors
        } // ClassificationDefinitions.PreprocessorMacroDefinition {Class}

        /// <summary>
        /// 
        /// </summary>
        [Export(typeof(EditorFormatDefinition))]
        [UserVisible(true)]
        [ClassificationType(ClassificationTypeNames = ClassificationTypes.PreprocessorDisabled)]
        [Name("SanScripPreprocessorDisabledFormatDefinition")]
        [Order(After="SanScriptPreprocessorMacroFormatDefinition")]
        internal sealed class PreprocessorDisabledDefinition : ClassificationFormatDefinition
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="PreprocessorDisabledDefinition"/>
            /// class.
            /// </summary>
            internal PreprocessorDisabledDefinition()
            {
                this.ForegroundColor = Colors.Gray;
                this.DisplayName = "San Script Preprocessor Disabled";
            }
            #endregion Constructors
        } // ClassificationDefinitions.PreprocessorDisabledDefinition {Class}

        /// <summary>
        /// 
        /// </summary>
        [Export(typeof(EditorFormatDefinition))]
        [UserVisible(true)]
        [ClassificationType(ClassificationTypeNames = ClassificationTypes.BracketHighlighting)]
        [Name("SanScripBracketHighlightingFormatDefinition")]
        [Order(After="SanScripPreprocessorDisabledFormatDefinition")]
        internal sealed class BracketHighlightingDefinition : ClassificationFormatDefinition
        {
            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="BracketHighlightingDefinition"/>
            /// class.
            /// </summary>
            internal BracketHighlightingDefinition()
            {
                this.BackgroundColor = Colors.Silver;
                this.DisplayName = "San Script Bracket Highlighting";
            }
            #endregion Constructors
        } // ClassificationDefinitions.BracketHighlightingDefinition {Class}
        #endregion Classes
    } // SanScript.EditorExtensions.Classification.ClassificationDefinitions {Class}
} // SanScript.EditorExtensions.Classification {Namespace}
