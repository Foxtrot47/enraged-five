﻿using System.ComponentModel.Composition;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Utilities;
namespace SanScript.EditorExtensions.Classification
{
    /// <summary>
    /// Defines the different classification types used by the San Script editor.
    /// </summary>
    internal class ClassificationTypes
    {
        #region Fields
        /// <summary>
        /// The classification constant string used for reserved words.
        /// </summary>
        internal const string ReservedWord = "SanScriptReservedWord";

        /// <summary>
        /// The classification constant string used for native types.
        /// </summary>
        internal const string NativeType = "SanScriptNativeType";

        /// <summary>
        /// The classification constant string used for operators.
        /// </summary>
        internal const string Operator = "SanScriptOperator";

        /// <summary>
        /// The classification constant string used for reserved word operators.
        /// </summary>
        internal const string ReservedWordOperator = "SanScriptReservedWordOperator";

        /// <summary>
        /// The classification constant string used for numbers.
        /// </summary>
        internal const string Number = "SanScriptNumber";

        /// <summary>
        /// The classification constant string used for string delimiters.
        /// </summary>
        internal const string StringDelimiter = "SanScriptStringDelimiter";

        /// <summary>
        /// The classification constant string used for strings.
        /// </summary>
        internal const string StringDefault = "SanScriptStringDefault";

        /// <summary>
        /// The classification constant string used for comment delimiters.
        /// </summary>
        internal const string CommentDelimiter = "SanScriptCommentDelimiter";

        /// <summary>
        /// The classification constant string used for comments.
        /// </summary>
        internal const string CommentDefault = "SanScriptCommentDefault";

        /// <summary>
        /// The classification constant string used for built in words.
        /// </summary>
        internal const string BuiltInWord = "SanScriptBuiltInWord";

        /// <summary>
        /// The classification constant string used for user words.
        /// </summary>
        internal const string UserWord = "SanScriptUserWord";

        /// <summary>
        /// The classification constant string used for preprocessor macros.
        /// </summary>
        internal const string PreprocessorMacro = "SanScriptPreprocessorMacro";

        /// <summary>
        /// The classification constant string used for native types.
        /// </summary>
        internal const string PreprocessorDisabled = "SanScriptPreprocessorDisabled";

        /// <summary>
        /// The classification constant string used for bracket highlights.
        /// </summary>
        internal const string BracketHighlighting = "SanScriptBracketHighlighting";
        #endregion Fields

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [Name(ClassificationTypes.ReservedWord), Export]
        internal ClassificationTypeDefinition SanScriptReservedWordClassificationType
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [Name(ClassificationTypes.NativeType), Export]
        internal ClassificationTypeDefinition SanScriptNativeTypeClassificationType
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [Name(ClassificationTypes.Operator), Export]
        internal ClassificationTypeDefinition SanScriptOperatorClassificationType
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [Name(ClassificationTypes.ReservedWordOperator), Export]
        internal ClassificationTypeDefinition SanScriptReservedWordOperatorClassificationType
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [Name(ClassificationTypes.Number), Export]
        internal ClassificationTypeDefinition SanScriptNumberClassificationType
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [Name(ClassificationTypes.StringDelimiter), Export]
        internal ClassificationTypeDefinition SanScriptStringDelimiterClassificationType
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [Name(ClassificationTypes.StringDefault), Export]
        internal ClassificationTypeDefinition SanScriptStringDefaultClassificationType
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [Name(ClassificationTypes.CommentDelimiter), Export]
        internal ClassificationTypeDefinition SanScriptCommentDelimiterClassificationType
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [Name(ClassificationTypes.CommentDefault), Export]
        internal ClassificationTypeDefinition SanScriptCommentDefaultClassificationType
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [Name(ClassificationTypes.BuiltInWord), Export]
        internal ClassificationTypeDefinition SanScriptBuiltInWordClassificationType
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [Name(ClassificationTypes.UserWord), Export]
        internal ClassificationTypeDefinition SanScriptUserWordClassificationType
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [Name(ClassificationTypes.PreprocessorMacro), Export]
        internal ClassificationTypeDefinition SanScriptPreprocessorMacroClassificationType
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [Name(ClassificationTypes.PreprocessorDisabled), Export]
        internal ClassificationTypeDefinition SanScriptPreprocessorDisabledClassificationType
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [Name(ClassificationTypes.BracketHighlighting), Export]
        internal ClassificationTypeDefinition SanScriptBracketHighlightingClassificationType
        {
            get;
            set;
        }
        #endregion Properties
    } // SanScript.EditorExtensions.Classification.ClassificationTypes {Class}
} // SanScript.EditorExtensions.Classification {Namespace}
