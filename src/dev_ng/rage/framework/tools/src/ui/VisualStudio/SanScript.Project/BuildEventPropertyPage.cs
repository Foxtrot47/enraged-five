﻿namespace SanScript.Project
{
    using System;
    using System.ComponentModel;
    using System.Runtime.InteropServices;
    using Microsoft.VisualStudio;
    using Microsoft.VisualStudio.Project;

    /// <summary>
    /// Describes the pre-build property page for the san script project type.
    /// </summary>
    [ComVisible(true)]
    [Guid("2F67AB50-1A78-403F-8A4E-48F757C26B50")]
    public class BuildEventPropertyPage : SettingsPage
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="PreCommandLine"/> property.
        /// </summary>
        private string _preCommandLine;

        /// <summary>
        /// The private field used for the <see cref="UsePreInBuild"/> property.
        /// </summary>
        private bool _usePreInBuild;

        /// <summary>
        /// The private field used for the <see cref="PostCommandLine"/> property.
        /// </summary>
        private string _postCommandLine;

        /// <summary>
        /// The private field used for the <see cref="UsePostInBuild"/> property.
        /// </summary>
        private bool _usePostInBuild;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BuildEventPropertyPage"/> class.
        /// </summary>
        public BuildEventPropertyPage()
        {
            this.Name = "Build Event";
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the command line for the pre-build event tool to run.
        /// </summary>
        [Category("Pre-Build Event")]
        [DisplayName("Command Line")]
        [Description("Specifies a command line for the pre-build event tool to run.")]
        public string PreCommandLine
        {
            get
            {
                return this._preCommandLine;
            }

            set
            {
                this._preCommandLine = value;
                this.IsDirty = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this build event is excluded from the build
        /// for the current configuration.
        /// </summary>
        [Category("Pre-Build Event")]
        [DisplayName("Use In Build")]
        [Description("Specifies whether this build event is excluded from the build for the current configuration.")]
        public bool UsePreInBuild
        {
            get
            {
                return this._usePreInBuild;
            }

            set
            {
                this._usePreInBuild = value;
                this.IsDirty = true;
            }
        }

        /// <summary>
        /// Gets or sets the command line for the pre-build event tool to run.
        /// </summary>
        [Category("Post-Build Event")]
        [DisplayName("Command Line")]
        [Description("Specifies a command line for the post-build event tool to run.")]
        public string PostCommandLine
        {
            get
            {
                return this._postCommandLine;
            }

            set
            {
                this._postCommandLine = value;
                this.IsDirty = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this build event is excluded from the build
        /// for the current configuration.
        /// </summary>
        [Category("Post-Build Event")]
        [DisplayName("Use In Build")]
        [Description("Specifies whether this build event is excluded from the build for the current configuration.")]
        public bool UsePostInBuild
        {
            get
            {
                return this._usePostInBuild;
            }

            set
            {
                this._usePostInBuild = value;
                this.IsDirty = true;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Binds the private fields for this settings page through the configuration
        /// manager.
        /// </summary>
        protected override void BindProperties()
        {
            ProjectConfig[] test = this.GetProjectConfigurations();

            this._preCommandLine = this.GetConfigProperty("PreBuildCommandLine");
            bool.TryParse(this.GetConfigProperty("UsePreBuildStep"), out this._usePreInBuild);

            this._postCommandLine = this.GetConfigProperty("PostBuildCommandLine");
            bool.TryParse(this.GetConfigProperty("UsePostBuildStep"), out this._usePostInBuild);
        }

        /// <summary>
        /// Applies the changes made to these properties.
        /// </summary>
        /// <returns>
        /// Always returns VSConstants.S_OK.
        /// </returns>
        protected override int ApplyChanges()
        {
            this.SetConfigProperty("PreBuildCommandLine", this._preCommandLine);
            this.SetConfigProperty("UsePreBuildStep", this._usePreInBuild.ToString());

            this.SetConfigProperty("PostBuildCommandLine", this._postCommandLine);
            this.SetConfigProperty("UsePostBuildStep", this._usePostInBuild.ToString());

            this.IsDirty = false;
            return VSConstants.S_OK;
        }
        #endregion Methods
    } // SanScript.Project.PreBuildEventPropertyPage {Class}
} // SanScript.Project {Namespace}
