﻿namespace SanScript.Project.Library
{
    using System;
    using System.Runtime.InteropServices;
    using Microsoft.VisualStudio.Shell.Interop;
    using Microsoft.VisualStudio.TextManager.Interop;

    /// <summary>
    /// This interface defines the service that finds IronPython files inside a hierarchy
    /// and builds the informations to expose to the class view or object browser.
    /// </summary>
    [Guid(GuidList.guidLibraryManagerServiceGuidString)]
    public interface ISanScriptLibraryManager
    {
        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="hierarchy">
        /// 
        /// </param>
        void RegisterHierarchy(IVsHierarchy hierarchy);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hierarchy">
        /// 
        /// </param>
        void UnregisterHierarchy(IVsHierarchy hierarchy);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="document">
        /// 
        /// </param>
        /// <param name="lineChanged">
        /// 
        /// </param>
        /// <param name="onIdle">
        /// 
        /// </param>
        void RegisterLineChangeHandler(
            uint document, TextLineChangeEvent lineChanged, Action<IVsTextLines> onIdle);
        #endregion Methods
    } // SanScript.Project.Library {Class}
} // SanScript.Project.Library {namespace}
