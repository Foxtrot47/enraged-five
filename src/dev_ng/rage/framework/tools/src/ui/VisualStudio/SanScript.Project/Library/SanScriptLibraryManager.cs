﻿namespace SanScript.Project.Library
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Threading;
    using Irony.Parsing;
    using Microsoft.VisualStudio;
    using Microsoft.VisualStudio.Shell.Interop;
    using Microsoft.VisualStudio.TextManager.Interop;
    using RSG.SanScript;

    /// <summary>
    /// 
    /// </summary>
    [Guid(GuidList.guidLibraryManagerGuidString)]
    internal class SanScriptLibraryManager
        : ISanScriptLibraryManager, IVsRunningDocTableEvents, IDisposable
    {
        #region Fields
        /// <summary>
        /// 
        /// </summary>
        private IServiceProvider provider;

        /// <summary>
        /// 
        /// </summary>
        private uint objectManagerCookie;
        
        /// <summary>
        /// 
        /// </summary>
        private uint runningDocTableCookie;

        /// <summary>
        /// 
        /// </summary>
        private Dictionary<uint, TextLineEventListener> documents;

        /// <summary>
        /// 
        /// </summary>
        private Dictionary<IVsHierarchy, HierarchyListener> hierarchies;

        /// <summary>
        /// 
        /// </summary>
        private Dictionary<SanScriptModuleId, LibraryNode> files;

        /// <summary>
        /// 
        /// </summary>
        private Library library;

        /// <summary>
        /// 
        /// </summary>
        private Thread parseThread;

        /// <summary>
        /// 
        /// </summary>
        private ManualResetEvent requestPresent;

        /// <summary>
        /// 
        /// </summary>
        private ManualResetEvent shutDownStarted;

        /// <summary>
        /// 
        /// </summary>
        private Queue<LibraryTask> requests;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="provider">
        /// 
        /// </param>
        public SanScriptLibraryManager(IServiceProvider provider)
        {
            documents = new Dictionary<uint, TextLineEventListener>();
            hierarchies = new Dictionary<IVsHierarchy, HierarchyListener>();
            library = new Library(new Guid("0925166E-A743-49E2-9224-BBE206545104"));
            library.LibraryCapabilities = (_LIB_FLAGS2)_LIB_FLAGS.LF_PROJECT;
            files = new Dictionary<SanScriptModuleId, LibraryNode>();
            this.provider = provider;
            requests = new Queue<LibraryTask>();
            requestPresent = new ManualResetEvent(false);
            shutDownStarted = new ManualResetEvent(false);
           // parseThread = new Thread(new ThreadStart(ParseThread));
           // parseThread.Start();
        }
        #endregion Constructors


        private void RegisterForRDTEvents() {
            if (0 != runningDocTableCookie) {
                return;
            }
            IVsRunningDocumentTable rdt = provider.GetService(typeof(SVsRunningDocumentTable)) as IVsRunningDocumentTable;
            if (null != rdt) {
                // Do not throw here in case of error, simply skip the registration.
                rdt.AdviseRunningDocTableEvents(this, out runningDocTableCookie);
            }
        }
        private void UnregisterRDTEvents() {
            if (0 == runningDocTableCookie) {
                return;
            }
            IVsRunningDocumentTable rdt = provider.GetService(typeof(SVsRunningDocumentTable)) as IVsRunningDocumentTable;
            if (null != rdt) {
                // Do not throw in case of error.
                rdt.UnadviseRunningDocTableEvents(runningDocTableCookie);
            }
            runningDocTableCookie = 0;
        }

        #region IDisposable Members
        public void Dispose() {
            // Make sure that the parse thread can exit.
            if (null != shutDownStarted) {
                shutDownStarted.Set();
            }
            if ((null != parseThread) && parseThread.IsAlive) {
                parseThread.Join(500);
                if (parseThread.IsAlive) {
                    parseThread.Abort();
                }
                parseThread = null;
            }

            requests.Clear();

            // Dispose all the listeners.
            foreach (HierarchyListener listener in hierarchies.Values) {
                listener.Dispose();
            }
            hierarchies.Clear();

            foreach(TextLineEventListener textListener in documents.Values) {
                textListener.Dispose();
            }
            documents.Clear();

            // Remove this library from the object manager.
            if (0 != objectManagerCookie) {
                IVsObjectManager2 mgr = provider.GetService(typeof(SVsObjectManager)) as IVsObjectManager2;
                if (null != mgr) {
                    mgr.UnregisterLibrary(objectManagerCookie);
                }
                objectManagerCookie = 0;
            }

            // Unregister this object from the RDT events.
            UnregisterRDTEvents();

            // Dispose the events used to syncronize the threads.
            if (null != requestPresent) {
                requestPresent.Close();
                requestPresent = null;
            }
            if (null != shutDownStarted) {
                shutDownStarted.Close();
                shutDownStarted = null;
            }
        }
        #endregion

        #region IPythonLibraryManager
        public void RegisterHierarchy(IVsHierarchy hierarchy)
        {
            if ((null == hierarchy) || hierarchies.ContainsKey(hierarchy))
            {
                return;
            }

            if (0 == objectManagerCookie)
            {
                IVsObjectManager2 objManager = provider.GetService(typeof(SVsObjectManager)) as IVsObjectManager2;
                if (null == objManager)
                {
                    return;
                }

                Microsoft.VisualStudio.ErrorHandler.ThrowOnFailure(
                    objManager.RegisterSimpleLibrary(library, out objectManagerCookie));
            }

            HierarchyListener listener = new HierarchyListener(hierarchy);
            //listener.OnAddItem += new EventHandler<HierarchyEventArgs>(OnNewFile);
            //listener.OnDeleteItem += new EventHandler<HierarchyEventArgs>(OnDeleteFile);
            //listener.StartListening(true);
            hierarchies.Add(hierarchy, listener);
            RegisterForRDTEvents();
        }

        public void UnregisterHierarchy(IVsHierarchy hierarchy)
        {
            if ((null == hierarchy) || !hierarchies.ContainsKey(hierarchy))
            {
                return;
            }

            HierarchyListener listener = hierarchies[hierarchy];
            if (null != listener)
            {
                listener.Dispose();
            }

            hierarchies.Remove(hierarchy);
            if (0 == hierarchies.Count)
            {
                UnregisterRDTEvents();
            }

            lock (files)
            {
                SanScriptModuleId[] keys = new SanScriptModuleId[files.Keys.Count];
                files.Keys.CopyTo(keys, 0);
                foreach (SanScriptModuleId id in keys)
                {
                    if (hierarchy.Equals(id.Hierarchy))
                    {
                        library.RemoveNode(files[id]);
                        files.Remove(id);
                    }
                }
            }

            // Remove the document listeners.
            uint[] docKeys = new uint[documents.Keys.Count];
            documents.Keys.CopyTo(docKeys, 0);
            foreach (uint id in docKeys)
            {
                TextLineEventListener docListener = documents[id];
                if (hierarchy.Equals(docListener.FileID.Hierarchy))
                {
                    documents.Remove(id);
                    docListener.Dispose();
                }
            }
        }

        public void RegisterLineChangeHandler(uint document,
            TextLineChangeEvent lineChanged, Action<IVsTextLines> onIdle) {
            documents[document].OnFileChangedImmediate +=
                delegate(object sender, TextLineChange[] changes, int fLast)
                {
                    lineChanged(sender, changes, fLast);
                };

            documents[document].OnFileChanged +=
                delegate(object sender, HierarchyEventArgs args)
                {
                    onIdle(args.Buffer);
                };
        }

        #endregion

        #region Parse Thread
        /// <summary>
        /// Main function of the parsing thread.
        /// This function waits on the queue of the parsing requests and build the parsing tree for
        /// a specific file. The resulting tree is built using LibraryNode objects so that it can
        /// be used inside the class view or object browser.
        /// </summary>
        private void ParseThread()
        {
            const int waitTimeout = 500;
            // Define the array of events this function is interest in.
            WaitHandle[] eventsToWait = new WaitHandle[] { requestPresent, shutDownStarted };
            // Execute the tasks.
            while (true)
            {
                // Wait for a task or a shutdown request.
                int waitResult = WaitHandle.WaitAny(eventsToWait, waitTimeout, false);
                if (1 == waitResult)
                {
                    // The shutdown of this component is started, so exit the thread.
                    return;
                }

                LibraryTask task = null;
                lock (requests)
                {
                    if (0 != requests.Count)
                    {
                        task = requests.Dequeue();
                    }

                    if (0 == requests.Count)
                    {
                        requestPresent.Reset();
                    }
                }

                if (null == task)
                {
                    continue;
                }

                Parser parser = new Parser(new SanScriptGrammar());
                ParseTree tree = null;
                if (task.Text == null)
                {
                    if (!File.Exists(task.FileName))
                    {
                        continue;
                    }

                    tree = parser.Parse(File.ReadAllText(task.FileName), task.FileName);
                }
                else
                {
                    tree = parser.Parse(task.Text);
                }

                if (tree == null)
                {
                    continue;
                }

                string name = Path.GetFileName(task.FileName);
                LibraryNode module = new LibraryNode(name, LibraryNodeTypes.PhysicalContainer);
                CreateModuleTree(module, module, tree.Root, "", task.ModuleID);
                if (null != task.ModuleID)
                {
                    LibraryNode previousItem = null;
                    lock (files)
                    {
                        if (files.TryGetValue(task.ModuleID, out previousItem))
                        {
                            files.Remove(task.ModuleID);
                        }
                    }

                    library.RemoveNode(previousItem);
                }

                library.AddNode(module);
                if (null != task.ModuleID)
                {
                    lock(files)
                    {
                        files.Add(task.ModuleID, module);
                    }
                }
            }
        }

        private void CreateModuleTree(LibraryNode root, LibraryNode current, ParseTreeNode node, string namePrefix, SanScriptModuleId moduleId)
        {
            //if (root == null || node == null || node.ChildNodes == null)
            //{
            //    return;
            //}

            //foreach (ParseTreeNode subItem in node.ChildNodes)
            //{
                //SanScriptLibraryNode newNode = null;
                //if (String.Equals(subItem.Term.Name, "script_statement"))
                //{
                //    CreateModuleTree(root, current, subItem, "", moduleId);
                //    continue;
                //}

                //if (String.Equals(subItem.Term.Name, "using_statement"))
                //{
                //    string name = "USING ";
                //    IVsHierarchy hierarchy = moduleId.Hierarchy;
                //    uint itemId = moduleId.ItemId;
                //    foreach (ParseTreeNode usingItems in subItem.ChildNodes)
                //    {
                //        if (!String.Equals(usingItems.Term.Name, "StringLiteral"))
                //        {
                //            continue;
                //        }

                //        name += usingItems.Token.Text;
                //    }

                //    newNode = new SanScriptLibraryNode(subItem, name, hierarchy, itemId, provider);
                //    current.AddNode(newNode);
                //    continue;
                //}

                //SanScriptLibraryNode newNode = new SanScriptLibraryNode(subItem, subItem.Term.Name, moduleId.Hierarchy, moduleId.ItemId, provider);
                //current.AddNode(newNode);

                //string newNamePrefix = namePrefix;

                //// The classes are always added to the root node, the functions to the
                //// current node.
                //if ((newNode.NodeType & LibraryNode.LibraryNodeType.Members) != LibraryNode.LibraryNodeType.None)
                //{
                //    current.AddNode(newNode);
                //}
                //else if ((newNode.NodeType & LibraryNode.LibraryNodeType.Classes) != LibraryNode.LibraryNodeType.None)
                //{
                //    // Classes are always added to the root.
                //    root.AddNode(newNode);
                //    newNamePrefix = newNode.Name + ".";
                //}

                // Now use recursion to get the other types.
                //CreateModuleTree(root, newNode, subItem, "", moduleId);
            //}
        }
        #endregion

        private void CreateParseRequest(string file, string text, SanScriptModuleId id)
        {
            LibraryTask task = new LibraryTask(file, text);
            task.ModuleID = id;
            lock (requests)
            {
                requests.Enqueue(task);
            }

            requestPresent.Set();
        }

        #region Hierarchy Events
        private void OnNewFile(object sender, HierarchyEventArgs args)
        {
            IVsHierarchy hierarchy = sender as IVsHierarchy;
            if (null == hierarchy)
            {
                return;
            }

            string fileText = null;
            if (null != args.Buffer)
            {
                int lastLine;
                int lastIndex;
                int hr = args.Buffer.GetLastLineIndex(out lastLine, out lastIndex);
                if (Microsoft.VisualStudio.ErrorHandler.Failed(hr))
                {
                    return;
                }

                hr = args.Buffer.GetLineText(0, 0, lastLine, lastIndex, out fileText);
                if (Microsoft.VisualStudio.ErrorHandler.Failed(hr))
                {
                    return;
                }
            }

            CreateParseRequest(args.Name, fileText, new SanScriptModuleId(hierarchy, args.Id));
        }

        private void OnDeleteFile(object sender, HierarchyEventArgs args)
        {
            IVsHierarchy hierarchy = sender as IVsHierarchy;
            if (null == hierarchy)
            {
                return;
            }

            SanScriptModuleId id = new SanScriptModuleId(hierarchy, args.Id);
            LibraryNode node = null;
            lock (files)
            {
                if (files.TryGetValue(id, out node))
                {
                    files.Remove(id);
                }
            }

            if (null != node)
            {
                library.RemoveNode(node);
            }
        }
        #endregion

        #region IVsRunningDocTableEvents Members

        public int OnAfterAttributeChange(uint docCookie, uint grfAttribs)
        {
            if ((grfAttribs & (uint)(__VSRDTATTRIB.RDTA_MkDocument)) == (uint)__VSRDTATTRIB.RDTA_MkDocument)
            {
                IVsRunningDocumentTable rdt = provider.GetService(typeof(SVsRunningDocumentTable)) as IVsRunningDocumentTable;
                if (rdt != null)
                {
                    uint flags, readLocks, editLocks, itemid;
                    IVsHierarchy hier;
                    IntPtr docData = IntPtr.Zero;
                    string moniker;
                    int hr;
                    try
                    {
                        hr = rdt.GetDocumentInfo(docCookie, out flags, out readLocks, out editLocks, out moniker, out hier, out itemid, out docData);
                        TextLineEventListener listner;
                        if (documents.TryGetValue(docCookie, out listner))
                        {
                            listner.FileName = moniker;
                        }
                    }
                    finally
                    {
                        if (IntPtr.Zero != docData)
                        {
                            Marshal.Release(docData);
                        }
                    }
                }
            }
            return VSConstants.S_OK;
        }

        public int OnAfterDocumentWindowHide(uint docCookie, IVsWindowFrame pFrame)
        {
            return VSConstants.S_OK;
        }

        public int OnAfterFirstDocumentLock(uint docCookie, uint dwRDTLockType, uint dwReadLocksRemaining, uint dwEditLocksRemaining)
        {
            return VSConstants.S_OK;
        }

        public int OnAfterSave(uint docCookie)
        {
            return VSConstants.S_OK;
        }

        public int OnBeforeDocumentWindowShow(uint docCookie, int fFirstShow, IVsWindowFrame pFrame)
        {
            // Check if this document is in the list of the documents.
            if (documents.ContainsKey(docCookie))
            {
                return VSConstants.S_OK;
            }
            // Get the information about this document from the RDT.
            IVsRunningDocumentTable rdt = provider.GetService(typeof(SVsRunningDocumentTable)) as IVsRunningDocumentTable;
            if (null != rdt)
            {
                // Note that here we don't want to throw in case of error.
                uint flags;
                uint readLocks;
                uint writeLoks;
                string documentMoniker;
                IVsHierarchy hierarchy;
                uint itemId;
                IntPtr unkDocData;
                int hr = rdt.GetDocumentInfo(docCookie, out flags, out readLocks, out writeLoks,
                                             out documentMoniker, out hierarchy, out itemId, out unkDocData);
                try
                {
                    if (Microsoft.VisualStudio.ErrorHandler.Failed(hr) || (IntPtr.Zero == unkDocData))
                    {
                        return VSConstants.S_OK;
                    }
                    // Check if the herarchy is one of the hierarchies this service is monitoring.
                    if (!hierarchies.ContainsKey(hierarchy)) {
                        // This hierarchy is not monitored, we can exit now.
                        return VSConstants.S_OK;
                    }

                    // Check the extension of the file to see if a listener is required.
                    string extension = System.IO.Path.GetExtension(documentMoniker);
                    if (0 != string.Compare(extension, ".sc", StringComparison.OrdinalIgnoreCase))
                    {
                        if (0 != string.Compare(extension, ".sch", StringComparison.OrdinalIgnoreCase))
                        {
                            return VSConstants.S_OK;
                        }
                    }

                    // Create the module id for this document.
                    SanScriptModuleId docId = new SanScriptModuleId(hierarchy, itemId);

                    // Try to get the text buffer.
                    IVsTextLines buffer = Marshal.GetObjectForIUnknown(unkDocData) as IVsTextLines;

                    // Create the listener.
                    TextLineEventListener listener = new TextLineEventListener(buffer, documentMoniker, docId);
                    // Set the event handler for the change event. Note that there is no difference
                    // between the AddFile and FileChanged operation, so we can use the same handler.
                    listener.OnFileChanged += new EventHandler<HierarchyEventArgs>(OnNewFile);
                    // Add the listener to the dictionary, so we will not create it anymore.
                    documents.Add(docCookie, listener);
                }
                finally
                {
                    if (IntPtr.Zero != unkDocData)
                    {
                        Marshal.Release(unkDocData);
                    }
                }
            }
            // Always return success.
            return VSConstants.S_OK;
        }

        public int OnBeforeLastDocumentUnlock(uint docCookie, uint dwRDTLockType, uint dwReadLocksRemaining, uint dwEditLocksRemaining)
        {
            if ((0 != dwEditLocksRemaining) || (0 != dwReadLocksRemaining))
            {
                return VSConstants.S_OK;
            }

            TextLineEventListener listener;
            if (!documents.TryGetValue(docCookie, out listener) || (null == listener))
            {
                return VSConstants.S_OK;
            }

            using (listener)
            {
                documents.Remove(docCookie);
                // Now make sure that the information about this file are up to date (e.g. it is
                // possible that Class View shows something strange if the file was closed without
                // saving the changes).
                HierarchyEventArgs args = new HierarchyEventArgs(listener.FileID.ItemId, listener.FileName);
                OnNewFile(listener.FileID.Hierarchy, args);
            }

            return VSConstants.S_OK;
        }

        #endregion

        public void OnIdle() {
            foreach(TextLineEventListener listener in documents.Values) {
                listener.OnIdle();
            }
        }
    } // SanScript.Project.SanScriptLibraryManager {Class}
} // SanScript.Project.Library {namespace}
