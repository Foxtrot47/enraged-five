﻿namespace SanScript.CompilerTask
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using Microsoft.Build.Framework;
    using Microsoft.Build.Utilities;

    /// <summary>
    /// 
    /// </summary>
    public class SourceDependencyTracerTask : Task
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="IncludeDirectories"/> property.
        /// </summary>
        private string _includeDirectories;

        /// <summary>
        /// The private field used for the <see cref="OutputPath"/> property.
        /// </summary>
        private string _outputPath;

        /// <summary>
        /// The private field used for the <see cref="SourceFiles"/> property.
        /// </summary>
        private string[] _sourceFiles;

        /// <summary>
        /// </summary>
        private Dictionary<string, string> _mapper;

        /// <summary>
        /// </summary>
        private Dictionary<string, string> _headerMapper;

        private static Dictionary<string, HashSet<string>> _dependencies = new Dictionary<string, HashSet<string>>();

        private HashSet<string> _created;

        private Dictionary<string, bool> _updateCache;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SourceDependencyTracerTask"/> class.
        /// </summary>
        public SourceDependencyTracerTask()
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the include path(s) to push to the compiler executable.
        /// </summary>
        public string IncludeDirectories
        {
            get { return this._includeDirectories; }
            set { this._includeDirectories = value; }
        }

        /// <summary>
        /// Gets or sets the list of san script source and header files that should be
        /// compiled.
        /// </summary>
        public string[] SourceFiles
        {
            get { return this._sourceFiles; }
            set { this._sourceFiles = value; }
        }

        /// <summary>
        /// Gets or sets the output path.
        /// </summary>
        public string OutputPath
        {
            get { return this._outputPath; }
            set { this._outputPath = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Main entry point for the task
        /// </summary>
        /// <returns></returns>
        public override bool Execute()
        {
            try
            {
                Stopwatch sw = Stopwatch.StartNew();
                HashSet<string> includePaths = new HashSet<string>();
                foreach (string include in this.IncludeDirectories.Split(';'))
                {
                    includePaths.Add(include.ToLower());
                }

                foreach (string path in this.SourceFiles)
                {
                    includePaths.Add(Path.GetDirectoryName(path).ToLower());
                }

                this._mapper = new Dictionary<string, string>();
                this._headerMapper = new Dictionary<string, string>();
                string[] paths = null;
                foreach (string includeDirectory in includePaths)
                {
                    if (!Directory.Exists(includeDirectory.Trim()))
                    {
                        Log.LogWarning("Unable to find the included directory '{0}' specified in the include paths.", includeDirectory.Trim());
                        continue;
                    }

                    paths = Directory.GetFiles(includeDirectory.Trim(), "*.sch");
                    foreach (string path in paths)
                    {
                        string filename = Path.GetFileName(path).ToLower();
                        if (!this._mapper.ContainsKey(filename))
                        {
                            string output = Path.Combine(this.OutputPath, filename);
                            output = Path.ChangeExtension(output, ".d");
                            this._mapper.Add(filename, output.ToLower());
                        }

                        if (!this._headerMapper.ContainsKey(filename))
                        {
                            this._headerMapper.Add(filename, path.ToLower());
                        }
                    }
                }

                this._created = new HashSet<string>();
                this._updateCache = new Dictionary<string, bool>();
                foreach (string path in this.SourceFiles)
                {
                    bool update = this.DetermineIfSourceNeedsUpdating(path);
                    if (!update)
                    {
                        continue;
                    }

                    string output = Path.Combine(this.OutputPath, Path.GetFileName(path));
                    output = Path.ChangeExtension(output, ".sd");
                    using (StreamWriter file = new StreamWriter(output))
                    {
                        foreach (string dependency in this.GetDependencies(path))
                        {
                            file.WriteLine(dependency);
                        }

                        file.WriteLine(path);
                    }
                }

                sw.Stop();
                return true;
            }
            catch (Exception exception)
            {
                Log.LogErrorFromException(exception);
                return false;
            }
        }

        private bool DetermineIfSourceNeedsUpdating(string filename)
        {
            bool update = false;
            bool alreadyUpdated = false;
            FileInfo inputInfo = new FileInfo(filename);
            if (!inputInfo.Exists)
            {
                return false;
            }

            string sdFile = Path.Combine(this.OutputPath, Path.GetFileName(filename));
            sdFile = Path.ChangeExtension(sdFile, ".sd").ToLower();

            FileInfo sdFileInfo = new FileInfo(sdFile);
            if (!sdFileInfo.Exists)
            {
                update = true;
                alreadyUpdated = true;
                string output = Path.Combine(this.OutputPath, Path.GetFileName(filename));
                output = Path.ChangeExtension(output, ".sd");
                HashSet<string> sourceDependencies = this.GetDependencies(filename);
                _dependencies[Path.GetFileName(output).ToLower()] = sourceDependencies;
                using (StreamWriter file = new StreamWriter(output))
                {
                    foreach (string dependency in sourceDependencies)
                    {
                        file.WriteLine(dependency);
                    }

                    file.WriteLine(filename);
                }
            }
            else
            {
                if (inputInfo.LastWriteTime > sdFileInfo.LastWriteTime)
                {
                    update = true;
                    alreadyUpdated = true;
                    string output = Path.Combine(this.OutputPath, Path.GetFileName(filename));
                    output = Path.ChangeExtension(output, ".sd");
                    HashSet<string> sourceDependencies = this.GetDependencies(filename);
                    _dependencies[Path.GetFileName(output).ToLower()] = sourceDependencies;
                    using (StreamWriter file = new StreamWriter(output))
                    {
                        foreach (string dependency in sourceDependencies)
                        {
                            file.WriteLine(dependency);
                        }

                        file.WriteLine(filename);
                    }
                }
            }

            HashSet<string> readFiles = new HashSet<string>();
            bool cacheUpdate = this.GetDependenciesFromCache(sdFile, readFiles);
            return (update || cacheUpdate) && !alreadyUpdated;
        }

        public HashSet<string> GetDependencies(string filename)
        {
            HashSet<string> dependencies = new HashSet<string>();
            using (StreamReader file = new StreamReader(filename))
            {
                string line;
                while ((line = file.ReadLine()) != null)
                {
                    line = line.Trim();
                    if (String.IsNullOrWhiteSpace(line))
                    {
                        continue;
                    }

                    if (!line.StartsWith("USING "))
                    {
                        if (dependencies.Count > 0)
                        {
                            if (line.StartsWith("#IF") || line.StartsWith("#ENDIF"))
                            {
                                continue;
                            }

                            if (line.StartsWith("//") || line.StartsWith("/*"))
                            {
                                continue;
                            }

                            break;
                        }

                        continue;
                    }

                    int startQuoteIndex = line.IndexOf('"');
                    if (startQuoteIndex == -1)
                    {
                        continue;
                    }

                    int endQuoteIndex = line.IndexOf('"', startQuoteIndex + 1);
                    if (endQuoteIndex == -1)
                    {
                        continue;
                    }

                    int length = endQuoteIndex - startQuoteIndex;
                    string include = line.Substring(startQuoteIndex + 1, length - 1);
                    string dependencyFilename;
                    if (!this._mapper.TryGetValue(Path.GetFileName(include.ToLower()), out dependencyFilename))
                    {
                        Log.LogError("Unable to resolve using statement '{0}'.", include);
                        continue;
                    }

                    dependencies.Add(dependencyFilename);
                }
            }

            return dependencies;
        }

        public bool GetDependenciesFromCache(string filename, HashSet<string> readFiles)
        {
            bool update = false;
            if (this._updateCache.TryGetValue(Path.GetFileName(filename), out update))
            {
                return update;
            }

            update = false;
            readFiles.Add(filename);
            HashSet<string> lines = new HashSet<string>();
            string name = Path.GetFileName(filename);
            if (!_dependencies.TryGetValue(name, out lines))
            {
                lines = new HashSet<string>();
                using (StreamReader file = new StreamReader(filename))
                {
                    string line;
                    while ((line = file.ReadLine()) != null)
                    {
                        if (line.EndsWith("d"))
                        {
                            lines.Add(line);
                        }
                    }
                }

                _dependencies[name] = lines;
            }

            foreach (string line in lines)
            {
                if (readFiles.Contains(line))
                {
                    continue;
                }

                string mappedheader;
                string header = Path.GetFileNameWithoutExtension(line) + ".sch";
                if (!this._headerMapper.TryGetValue(header, out mappedheader))
                {
                    Log.LogError("The header file {0} hasn't been mapped. This probably means it doesn't exist inside any of the specified include directories", header);
                    return true;
                }

                bool createDependencyFile = false;
                FileInfo lineInfo = new FileInfo(line);
                if (!lineInfo.Exists)
                {
                    createDependencyFile = true;
                    update = true;
                }
                else
                {
                    if (!this._created.Contains(name))
                    {
                        FileInfo headerInfo = new FileInfo(mappedheader);
                        if (headerInfo.LastWriteTime > lineInfo.LastWriteTime)
                        {
                            update = true;
                            createDependencyFile = true;
                        }
                    }
                    else
                    {
                        update = true;
                    }
                }

                if (createDependencyFile)
                {
                    this._created.Add(name);
                    HashSet<string> headerDependencies = this.GetDependencies(mappedheader);
                    _dependencies[Path.GetFileName(mappedheader)] = headerDependencies;
                    using (StreamWriter file = new StreamWriter(line))
                    {
                        foreach (string dependency in headerDependencies)
                        {
                            file.WriteLine(dependency);
                        }
                    }
                }

                if (this.GetDependenciesFromCache(line, readFiles))
                {
                    update = true;
                }
            }

            this._updateCache[Path.GetFileName(filename)] = update;
            return update;
        }
        #endregion Methods
    } // SanScript.CompilerTask.SourceDependencyTracerTask {Class}
} // SanScript.CompilerTask {Namespace}
