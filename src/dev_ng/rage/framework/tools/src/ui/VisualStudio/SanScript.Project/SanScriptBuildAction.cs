﻿namespace SanScript.Project
{
    /// <summary>
    /// Defines the different build actions that can be applied to a SanScript file.
    /// </summary>
    public enum SanScriptBuildAction
    {
        /// <summary>
        /// Specifies that no build action should be taken on the associated file.
        /// </summary>
        None,

        /// <summary>
        /// Specifies that the associated file should be compiled into the output file.
        /// </summary>
        Compile,
    } // SanScript.Project.SanScriptBuildAction {Enum}
} // SanScript.Project {Namespace}
