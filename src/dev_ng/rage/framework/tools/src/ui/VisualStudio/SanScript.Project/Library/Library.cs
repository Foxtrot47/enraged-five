﻿namespace SanScript.Project.Library
{
    using System;
    using Microsoft.VisualStudio;
    using Microsoft.VisualStudio.OLE.Interop;
    using Microsoft.VisualStudio.Shell.Interop;

    /// <summary>
    /// Describes the library used by the Object Manager to access pertinent sybols for the
    /// SanScript project and project items.
    /// </summary>
    public class Library : IVsSimpleLibrary2
    {
        #region Fields
        /// <summary>
        /// The unique identifier for this library.
        /// </summary>
        private Guid _guid;

        /// <summary>
        /// The private field used for the <see cref="Capabilities"/> property.
        /// </summary>
        private _LIB_FLAGS2 capabilities;

        /// <summary>
        /// The private reference to the root node for this library.
        /// </summary>
        private LibraryNode root;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Library"/> class.
        /// </summary>
        /// <param name="libraryGuid">
        /// The unique identifier used for this library class.
        /// </param>
        public Library(Guid guid)
        {
            this._guid = guid;
            root = new LibraryNode("", LibraryNodeTypes.Package);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the capabilities of this library.
        /// </summary>
        public _LIB_FLAGS2 LibraryCapabilities
        {
            get { return this.capabilities; }
            set { this.capabilities = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Adds a browse container to be browsed by the library.
        /// </summary>
        /// <param name="component">
        /// A VSCOMPONENTSELECTORDATA object which specifies attributes that identify the added
        /// component.
        /// </param>
        /// <param name="options">
        /// A flag containing the options for the add operation.
        /// </param>
        /// <param name="componentAdded">
        /// When this method returns contains the text to display for the added component.
        /// </param>
        /// <returns>
        /// Always returns VSConstants.E_NOTIMPL.
        /// </returns>
        public int AddBrowseContainer(
            VSCOMPONENTSELECTORDATA[] component, ref uint options, out string componentAdded)
        {
            componentAdded = null;
            return VSConstants.E_NOTIMPL;
        }

        /// <summary>
        /// Reserved for future use.
        /// </summary>
        /// <param name="nodes">
        /// An array of SYMBOL_DESCRIPTION_NODE objects describing each node in the tree.
        /// </param>
        /// <param name="count">
        /// The number of objects in the <paramref name="nodes"/> array.
        /// </param>
        /// <param name="navigationInfo">
        /// When this method returns contains the IVsNavInfo object representing the navigation
        /// information for all nodes in the tree.
        /// </param>
        /// <returns>
        /// Always returns VSConstants.E_NOTIMPL.
        /// </returns>
        public int CreateNavInfo(
            SYMBOL_DESCRIPTION_NODE[] nodes, uint count, out IVsNavInfo navigationInfo)
        {
            navigationInfo = null;
            return VSConstants.E_NOTIMPL;
        }

        /// <summary>
        /// Returns an array of Browse Containers that correspond to the specified hierarchy.
        /// </summary>
        /// <param name="hierarchy">
        /// An IVsHierarchy object representing the hierarchy.
        /// </param>
        /// <param name="count">
        /// The number of elements in the <paramref name="containers"/> array.
        /// </param>
        /// <param name="containers">
        /// When this method returns contains an array of containers that correspond to the
        /// specified hierarchy.
        /// </param>
        /// <param name="actual">
        /// The actual number of containers that were returned in the
        /// <paramref name="containers"/> array.
        /// </param>
        /// <returns>
        /// Always returns VSConstants.E_NOTIMPL.
        /// </returns>
        public int GetBrowseContainersForHierarchy(
            IVsHierarchy hierarchy, uint count, VSBROWSECONTAINER[] containers, uint[] actual)
        {
            return VSConstants.E_NOTIMPL;
        }

        /// <summary>
        /// Returns the unique identifier of the library.
        /// </summary>
        /// <param name="guid">
        /// When this method returns contains the unique identifier for this library.
        /// </param>
        /// <returns>
        /// Always returns VSConstants.S_OK.
        /// </returns>
        public int GetGuid(out Guid guid)
        {
            guid = _guid;
            return VSConstants.S_OK;
        }

        /// <summary>
        /// Returns flags associated with the library.
        /// </summary>
        /// <param name="flags">
        /// When this method returns contains a combination of flags from the _LIB_FLAGS2
        /// enumeration that apply to the current library.
        /// </param>
        /// <returns>
        /// Always returns VSConstants.S_OK.
        /// </returns>
        public int GetLibFlags2(out uint flags)
        {
            flags = (uint)LibraryCapabilities;
            return VSConstants.S_OK;
        }

        /// <summary>
        /// Returns the requested list of symbols as an IVsSimpleObjectList2 interface.
        /// </summary>
        /// <param name="type">
        /// Specifies the list type.
        /// </param>
        /// <param name="flags">
        /// Specifies flags.
        /// </param>
        /// <param name="searchCriteria">
        /// Describes the search criteria.
        /// </param>
        /// <param name="list">
        /// When this method returns contains the list of requested items.
        /// </param>
        /// <returns>
        /// Always returns VSConstants.S_OK.
        /// </returns>
        public int GetList2(
            uint type,
            uint flags,
            VSOBSEARCHCRITERIA2[] searchCriteria,
            out IVsSimpleObjectList2 list)
        {
            list = root as IVsSimpleObjectList2;
            return VSConstants.S_OK;
        }

        /// <summary>
        /// Returns the string used to separate symbols for this type of Browse Container.
        /// </summary>
        /// <param name="separator">
        /// When this method returns contains the language specific scope operator.
        /// </param>
        /// <returns>
        /// Always returns VSConstants.S_OK.
        /// </returns>
        public int GetSeparatorStringWithOwnership(out string separator)
        {
            separator = ".";
            return VSConstants.S_OK;
        }

        /// <summary>
        /// Returns the category values supported by the library for a specified category.
        /// </summary>
        /// <param name="category">
        /// Specifies a library's category type.
        /// </param>
        /// <param name="field">
        /// When this method returns contains a category field object.
        /// </param>
        /// <returns>
        /// Always returns VSConstants.S_OK.
        /// </returns>
        public int GetSupportedCategoryFields2(int category, out uint field)
        {
            field = (uint)_LIB_CATEGORY2.LC_HIERARCHYTYPE;
            field |= (uint)_LIB_CATEGORY2.LC_PHYSICALCONTAINERTYPE;
            return VSConstants.S_OK;
        }

        /// <summary>
        /// Asks the library to load its persisted global Browse Containers.
        /// </summary>
        /// <param name="stream">
        /// An Microsoft.VisualStudio.OLE.Interop.IStream object to read from.
        /// </param>
        /// <param name="type">
        /// Specifies the persisted type of the library.
        /// </param>
        /// <returns>
        /// Always returns VSConstants.S_OK.
        /// </returns>
        public int LoadState(IStream stream, LIB_PERSISTTYPE type)
        {
            return VSConstants.S_OK;
        }

        /// <summary>
        /// Removes a Browse Container being browsed by the library.
        /// </summary>
        /// <param name="reserved">
        /// This parameter is not used.
        /// </param>
        /// <param name="name">
        /// A string containing the library name.
        /// </param>
        /// <returns>
        /// Always returns VSConstants.E_NOTIMPL.
        /// </returns>
        public int RemoveBrowseContainer(uint reserved, string name)
        {
            return VSConstants.E_NOTIMPL;
        }

        /// <summary>
        /// Asks the library to save its persisted global Browse Containers.
        /// </summary>
        /// <param name="stream">
        /// An Microsoft.VisualStudio.OLE.Interop.IStream object to write to.
        /// </param>
        /// <param name="type">
        /// Specifies the persisted type of the library.
        /// </param>
        /// <returns>
        /// Always returns VSConstants.S_OK.
        /// </returns>
        public int SaveState(IStream stream, LIB_PERSISTTYPE type)
        {
            return VSConstants.S_OK;
        }

        /// <summary>
        /// Returns the current change counter for the library and is used to indicate that the
        /// library contents have changed.
        /// </summary>
        /// <param name="updateCounter">
        /// When this method returns contains the current update count for this library.
        /// </param>
        /// <returns>
        /// If the method succeeds, it returns Microsoft.VisualStudio.VSConstants.S_OK. If it
        /// fails, it returns an error code.
        /// </returns>
        public int UpdateCounter(out uint updateCounter)
        {
            return ((IVsSimpleObjectList2)root).UpdateCounter(out updateCounter);
        }

        /// <summary>
        /// Adds the specified node to this library.
        /// </summary>
        /// <param name="node">
        /// The node to add to this library.
        /// </param>
        internal void AddNode(LibraryNode node)
        {
            lock (this)
            {
                root = new LibraryNode(root);
                root.AddNode(node);
            }
        }

        /// <summary>
        /// Removes the specified node from this library.
        /// </summary>
        /// <param name="node">
        /// The node to remove from this library.
        /// </param>
        internal void RemoveNode(LibraryNode node)
        {
            lock (this)
            {
                root = new LibraryNode(root);
                root.RemoveNode(node);
            }
        }
        #endregion Methods
    } // SanScript.Project.Library.Library {Class}
} // SanScript.Project.Library {Namespace}
