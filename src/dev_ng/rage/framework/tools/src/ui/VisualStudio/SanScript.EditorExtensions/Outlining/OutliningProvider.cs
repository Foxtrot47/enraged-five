﻿namespace SanScript.EditorExtensions.Outlining
{
    using System;
    using System.ComponentModel.Composition;
    using Microsoft.VisualStudio.Text;
    using Microsoft.VisualStudio.Text.Editor;
    using Microsoft.VisualStudio.Text.Tagging;
    using Microsoft.VisualStudio.Utilities;

    /// <summary>
    /// Represents the tagger used to tag a given text buffer with
    /// <see cref="IOutliningRegionTag"/> tags.
    /// </summary>
    [Export(typeof(ITaggerProvider))]
    [TagType(typeof(IOutliningRegionTag))]
    [ContentType("SanScript")]
    internal sealed class OutliningProvider : ITaggerProvider
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="EditorOptionsFactory"/> property.
        /// </summary>
        private IEditorOptionsFactoryService _editorOptionsFactory;
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets or sets the editor options factory.
        /// </summary>
        [Import]
        private IEditorOptionsFactoryService EditorOptionsFactory
        {
            get { return this._editorOptionsFactory; }
            set { this._editorOptionsFactory = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a tag provider for the specified buffer.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the tag.
        /// </typeparam>
        /// <param name="buffer">
        /// The Microsoft.VisualStudio.Text.ITextBuffer.
        /// </param>
        /// <returns>
        /// The Microsoft.VisualStudio.Text.Tagging.ITagger<T>.
        /// </returns>
        public ITagger<T> CreateTagger<T>(ITextBuffer buffer) where T : ITag
        {
            IEditorOptions editorOptions = _editorOptionsFactory.GetOptions(buffer);


            Func<ITagger<T>> sc =
                delegate()
                {
                    return new OutliningTagger(buffer, editorOptions) as ITagger<T>;
                };

            return buffer.Properties.GetOrCreateSingletonProperty<ITagger<T>>(sc);
        }
        #endregion Methods
    } // SanScript.EditorExtensions.Outlining.OutliningProvider {Class}
} // SanScript.EditorExtensions.Outlining {Namespace}
