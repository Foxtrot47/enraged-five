﻿namespace SanScript.Project.Library
{
    /// <summary>
    /// 
    /// </summary>
    internal class LibraryTask
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Filename"/> property.
        /// </summary>
        private string _fileName;

        /// <summary>
        /// The private field used for the <see cref="ModuleID"/> property.
        /// </summary>
        private SanScriptModuleId _moduleId;

        /// <summary>
        /// The private field used for the <see cref="Text"/> property.
        /// </summary>
        private string _text;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="LibraryTask"/> class.
        /// </summary>
        /// <param name="fileName">
        /// 
        /// </param>
        /// <param name="text">
        /// 
        /// </param>
        public LibraryTask(string fileName, string text)
        {
            this._fileName = fileName;
            this._text = text;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets
        /// </summary>
        public string FileName
        {
            get { return this._fileName; }
        }

        /// <summary>
        /// Gets or sets
        /// </summary>
        public SanScriptModuleId ModuleID
        {
            get { return this._moduleId; }
            set { this._moduleId = value; }
        }

        /// <summary>
        /// Gets
        /// </summary>
        public string Text
        {
            get { return this._text; }
        }
        #endregion Properties
    } // SanScript.Project.LibraryTask {Class}
} // SanScript.Project.Library {namespace}
