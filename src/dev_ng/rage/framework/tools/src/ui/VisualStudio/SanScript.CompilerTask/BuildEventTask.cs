﻿namespace SanScript.CompilerTask
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using Microsoft.Build.Framework;
    using Microsoft.Build.Utilities;

    /// <summary>
    /// 
    /// </summary>
    public class BuildEventTask : ToolTask
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="CommandLine"/> property.
        /// </summary>
        private string _commandLine;

        /// <summary>
        /// The private field used for the <see cref="ShouldPerform"/> property.
        /// </summary>
        private bool _shouldPerform;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BuildEventTask"/> class.
        /// </summary>
        public BuildEventTask()
        {
            this.UseCommandProcessor = true;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the command line that will be executed.
        /// </summary>
        public string CommandLine
        {
            get { return this._commandLine; }
            set { this._commandLine = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this event should be performed.
        /// </summary>
        public bool ShouldPerform
        {
            get { return this._shouldPerform; }
            set { this._shouldPerform = value; }
        }

        /// <summary>
        /// Gets the name of the executable file to run.
        /// </summary>
        protected override string ToolName
        {
            get { return String.Empty; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Returns the fully qualified path to the executable file.
        /// </summary>
        /// <returns>
        /// The fully qualified path to the executable file.
        /// </returns>
        protected override string GenerateFullPathToTool()
        {
            return String.Empty;
        }

        /// <summary>
        /// Main entry point for the task
        /// </summary>
        /// <returns></returns>
        public override bool Execute()
        {
            try
            {
                if (!ShouldPerform || String.IsNullOrWhiteSpace(this.CommandLine))
                {
                    return true;
                }

                bool result = base.Execute();
                return result;
            }
            catch
            {
                return false;
            }
        }

        protected override string GenerateCommandLineCommands()
        {
            return this.CommandLine;
        } 
        #endregion Methods
    } // SanScript.CompilerTask.BuildEventTask {Class}
} // SanScript.CompilerTask {Namespace}
