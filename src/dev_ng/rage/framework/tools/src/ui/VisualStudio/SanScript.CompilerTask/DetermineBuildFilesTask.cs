﻿namespace SanScript.CompilerTask
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using Microsoft.Build.Framework;
    using Microsoft.Build.Utilities;

    public class DetermineBuildFilesTask : Task
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="SourceFiles"/> property.
        /// </summary>
        private string[] _sourceFiles;

        /// <summary>
        /// The private field used for the <see cref="OutputPath"/> property.
        /// </summary>
        private string _outputPath;

        /// <summary>
        /// The private field used for the <see cref="ProjectPath"/> property.
        /// </summary>
        private string _projectPath;

        /// <summary>
        /// The private field used for the <see cref="CompileToConfigurationFolder"/> property.
        /// </summary>
        private bool _compileToConfigurationFolder;

        /// <summary>
        /// The private field used for the <see cref="MsBuildProjectFilePath"/> property.
        /// </summary>
        private string _msBuildProjectFilePath;

        private bool _buildingSelection;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DetermineBuildFilesTask"/> class.
		/// </summary>
        public DetermineBuildFilesTask()
		{
		}
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the list of san script source and header files that should be
        /// compiled.
        /// </summary>
        public string[] SourceFiles
        {
            get { return this._sourceFiles; }
            set { this._sourceFiles = value; }
        }

        /// <summary>
        /// Gets or sets the output path.
        /// </summary>
        public string OutputPath
        {
            get { return this._outputPath; }
            set { this._outputPath = value; }
        }

        /// <summary>
        /// Gets or sets the project path. This should be set to $(MSBuildProjectDirectory)
        /// </summary>
        public string ProjectPath
        {
            get { return this._projectPath; }
            set { this._projectPath = value; }
        }

        public string MsBuildProjectFilePath
        {
            get { return this._msBuildProjectFilePath; }
            set { this._msBuildProjectFilePath = value; }
        }

        public bool BuildingSelection
        {
            get { return this._buildingSelection; }
            set { this._buildingSelection = value; }
        }

        public string DependencyPath
        {
            get;
            set;
        }

        [Output]
        public ITaskItem[] BuildFiles
        {
            get;
            set;
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Main entry point for the task
        /// </summary>
        /// <returns></returns>
        public override bool Execute()
        {
            try
            {
                FileInfo projectInfo = new FileInfo(MsBuildProjectFilePath);
                DateTime projectLastWriteTime = new DateTime();
                if (projectInfo.Exists)
                {
                    projectLastWriteTime = projectInfo.LastWriteTime;
                }

                List<string> compile = new List<string>();
                foreach (string sourceFile in this.SourceFiles)
                {
                    string output = Path.Combine(this.ProjectPath, this.OutputPath, Path.GetFileName(sourceFile));
                    output = Path.ChangeExtension(output, ".sco");
                    FileInfo sourceInfo = new FileInfo(sourceFile);
                    bool update = false;
                    if (this.BuildingSelection)
                    {
                        update = true;
                    }
                    else
                    {
                        FileInfo outputInfo = new FileInfo(output);
                        if (!update)
                        {
                            if (sourceInfo.Exists)
                            {
                                if (outputInfo.LastWriteTime < projectLastWriteTime)
                                {
                                    update = true;
                                }
                            }
                        }

                        if (!update)
                        {
                            if (outputInfo.Exists)
                            {
                                if (outputInfo.LastWriteTime < sourceInfo.LastWriteTime)
                                {
                                    update = true;
                                }
                            }
                            else
                            {
                                update = true;
                            }
                        }

                        if (!update)
                        {
                            string sdFile = Path.Combine(this.DependencyPath, Path.GetFileName(sourceFile));
                            sdFile = Path.ChangeExtension(sdFile, ".sd");
                            FileInfo sdFileInfo = new FileInfo(sdFile);
                            if (sdFileInfo.Exists)
                            {
                                if (sdFileInfo.LastWriteTime > outputInfo.LastWriteTime)
                                {
                                    update = true;
                                }
                            }
                            else
                            {
                                update = true;
                            }
                        }
                    }

                    if (update)
                    {
                        compile.Add(sourceFile);
                    }
                }

                bool result = true;
                this.BuildFiles = new TaskItem[compile.Count];
                for (int i = 0; i < compile.Count; i++)
                {
                    this.BuildFiles[i] = new TaskItem(compile[i]);
                }

                System.Threading.Thread.Sleep(100);
                string msg = String.Format("Compiling {0} files, {1} files up-to-date...", compile.Count, this.SourceFiles.Length - compile.Count);
                Log.LogMessage(MessageImportance.High, msg);
                System.Threading.Thread.Sleep(100);
                return result;
            }
            catch (Exception exception)
            {
                Log.LogErrorFromException(exception);
                return false;
            }
        }
        #endregion Methods
    }
}
